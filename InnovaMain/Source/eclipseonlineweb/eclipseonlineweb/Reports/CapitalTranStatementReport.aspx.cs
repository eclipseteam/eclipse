﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using C1.C1Preview;
using System.Drawing;
using C1.Web.Wijmo.Controls.C1ReportViewer;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Utilities;

namespace eclipseonlineweb
{
    public partial class CapitalTranStatementRepor : UMABasePage
    {
       
        private List<AssetEntity> assets = null;

        public override void PopulatePage(DataSet ds)
        {
            base.PopulatePage(ds);
            this.cid = Request.QueryString["ins"].ToString();
            ReportClick();
        }

        protected void Navigation_MenuItemClick(object sender, MenuEventArgs e)
        {
            SetReportMenus(e);
        }

        public C1PrintDocument MakeDoc()
        {
          var ds=  this.PresentationData as HoldingSummaryReportDS;
            C1PrintDocument doc = C1ReportViewer.CreateC1PrintDocument();
            DataRow clientSummaryRow = this.PresentationData.Tables[HoldingSummaryReportDS.CLIENTSUMMARYTABLE].Rows[0];
            AddClientHeaderToReport(doc, clientSummaryRow, ds.DateInfo(), "INVESTMENT TRANSACTION LISTING");

            SetCapitalReport(ds, doc);

            return doc;
        }

        private static void SetCapitalReport(HoldingSummaryReportDS ds, C1PrintDocument doc)
        {
            DataView secSummaryTableView = new DataView(ds.Tables[HoldingSummaryReportDS.CAPITALTRANTABLE]);
            secSummaryTableView.Sort = HoldingSummaryReportDS.SECODE + " ASC";

            DataTable secFilteredTable = secSummaryTableView.ToTable();

            RenderTable secSumTable = new RenderTable();
            secSumTable.RowGroups[0, 1].PageHeader = true; 
            RenderTable secSummaryTotalTable = new RenderTable();
            secSummaryTotalTable.RowGroups[0, 1].PageHeader = true; 
            secSummaryTotalTable.Style.FontSize = 6;

            secSummaryTotalTable.Rows[0].Height = .2;
            secSummaryTotalTable.Rows[0].Style.FontBold = true;
            secSummaryTotalTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[0].Style.FontBold = true;
            secSummaryTotalTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

            secSummaryTotalTable.Cells[0, 0].Text = "Code";
            secSummaryTotalTable.Cells[0, 1].Text = "Security Name";
            secSummaryTotalTable.Cells[0, 2].Text = "Product Type";
            secSummaryTotalTable.Cells[0, 3].Text = "Transaction Type";
            secSummaryTotalTable.Cells[0, 4].Text = "Account No.";
            secSummaryTotalTable.Cells[0, 5].Text = "Trade Date";

            secSummaryTotalTable.Cells[0, 6].Text = "Settlement Date";

            secSummaryTotalTable.Cells[0, 7].Text = "Units";
            secSummaryTotalTable.Cells[0, 7].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 8].Text = "Trade Price";
            secSummaryTotalTable.Cells[0, 8].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 9].Text = "Gross Value";
            secSummaryTotalTable.Cells[0, 9].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 10].Text = "Fees";
            secSummaryTotalTable.Cells[0, 10].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cells[0, 11].Text = "Net Value";
            secSummaryTotalTable.Cells[0, 11].Style.TextAlignHorz = AlignHorzEnum.Right;

            secSummaryTotalTable.Cols[0].Width = 2;
            secSummaryTotalTable.Cols[1].Width = 7;
            secSummaryTotalTable.Cols[2].Width = 2.5;
            secSummaryTotalTable.Cols[3].Width = 3.5;
            secSummaryTotalTable.Cols[4].Width = 2.5;
            secSummaryTotalTable.Cols[5].Width = 3;
            secSummaryTotalTable.Cols[6].Width = 3;
            secSummaryTotalTable.Cols[7].Width = 2.5;
            secSummaryTotalTable.Cols[8].Width = 2.5;
            secSummaryTotalTable.Cols[9].Width = 2.5;
            secSummaryTotalTable.Cols[10].Width = 2.5;
            secSummaryTotalTable.Cols[11].Width = 2.5;

            RenderText gridHeaderTotal = new RenderText();
            gridHeaderTotal.Text = "Investment Transactions\n\n";
            gridHeaderTotal.Style.TextColor = Color.FromArgb(46, 44, 83);
            gridHeaderTotal.Style.FontSize = 11;
            gridHeaderTotal.Style.FontBold = true;

            RenderText lineBreakTotal = new RenderText();
            lineBreakTotal.Text = "\n\n\n";

            doc.Body.Children.Add(gridHeaderTotal);
            doc.Body.Children.Add(secSummaryTotalTable);
            doc.Body.Children.Add(lineBreakTotal);
            int totalIndex = 1;

            //Security Code	Security Name	Product Type	TransactionType	AccountNo	TradeDate	SettlementDate	Units	Trade Price	Gross Value	Fees &Brokerage 	NetValue


            foreach (DataRow secRow in secFilteredTable.Rows)
            {
                secSummaryTotalTable.Cells[totalIndex, 0].Text = secRow[HoldingSummaryReportDS.SECODE].ToString();
                secSummaryTotalTable.Cells[totalIndex, 1].Text = secRow[HoldingSummaryReportDS.SECNAME].ToString();
                secSummaryTotalTable.Cells[totalIndex, 2].Text = secRow[HoldingSummaryReportDS.PROTYPE].ToString();
                secSummaryTotalTable.Cells[totalIndex, 3].Text = secRow[HoldingSummaryReportDS.TRANTYPE].ToString();
                secSummaryTotalTable.Cells[totalIndex, 4].Text = secRow[HoldingSummaryReportDS.ACCNO].ToString();
                secSummaryTotalTable.Cells[totalIndex, 5].Text = ((DateTime)secRow[HoldingSummaryReportDS.TRADEDATE]).ToString("dd/MM/yyyy");
                secSummaryTotalTable.Cells[totalIndex, 6].Text = ((DateTime)secRow[HoldingSummaryReportDS.SETTLEDATE]).ToString("dd/MM/yyyy");

                if (secRow[HoldingSummaryReportDS.UNITS] != null & secRow[HoldingSummaryReportDS.UNITS] != DBNull.Value)
                    secSummaryTotalTable.Cells[totalIndex, 7].Text = Convert.ToDecimal(secRow[HoldingSummaryReportDS.UNITS]).ToString("N4");
                else
                    secSummaryTotalTable.Cells[totalIndex, 7].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 7].Style.TextAlignHorz = AlignHorzEnum.Right;

                if (secRow[HoldingSummaryReportDS.TRADEPRICE] != null & secRow[HoldingSummaryReportDS.TRADEPRICE] != DBNull.Value)
                    secSummaryTotalTable.Cells[totalIndex, 8].Text = Convert.ToDecimal(secRow[HoldingSummaryReportDS.TRADEPRICE]).ToString("N4");
                else
                    secSummaryTotalTable.Cells[totalIndex, 8].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 8].Style.TextAlignHorz = AlignHorzEnum.Right;


                if (secRow[HoldingSummaryReportDS.GROSSVAL] != null & secRow[HoldingSummaryReportDS.GROSSVAL] != DBNull.Value)
                    secSummaryTotalTable.Cells[totalIndex, 9].Text = Convert.ToDecimal(secRow[HoldingSummaryReportDS.GROSSVAL]).ToString("C");
                else
                    secSummaryTotalTable.Cells[totalIndex, 9].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 9].Style.TextAlignHorz = AlignHorzEnum.Right;

                if (secRow[HoldingSummaryReportDS.FEES] != null & secRow[HoldingSummaryReportDS.FEES] != DBNull.Value)
                    secSummaryTotalTable.Cells[totalIndex, 10].Text = Convert.ToDecimal(secRow[HoldingSummaryReportDS.FEES]).ToString("C");
                else
                    secSummaryTotalTable.Cells[totalIndex, 10].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 10].Style.TextAlignHorz = AlignHorzEnum.Right;

                if (secRow[HoldingSummaryReportDS.NETVALUE] != null & secRow[HoldingSummaryReportDS.NETVALUE] != DBNull.Value)
                    secSummaryTotalTable.Cells[totalIndex, 11].Text = Convert.ToDecimal(secRow[HoldingSummaryReportDS.NETVALUE]).ToString("C");
                else
                    secSummaryTotalTable.Cells[totalIndex, 11].Text = "-";
                secSummaryTotalTable.Cells[totalIndex, 11].Style.TextAlignHorz = AlignHorzEnum.Right;

                totalIndex++;
            }

            secSummaryTotalTable.Rows[totalIndex].Height = .2;
            secSummaryTotalTable.Rows[totalIndex].Style.FontBold = true;
            secSummaryTotalTable.Rows[totalIndex].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[totalIndex].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            secSummaryTotalTable.Rows[totalIndex].Style.FontBold = true;
            secSummaryTotalTable.Rows[totalIndex].Style.TextAlignVert = AlignVertEnum.Center;
        }

        protected void GenerateAssetSummaryReport(object sender, EventArgs e)
        {
            ReportClick();
        }

        protected void ReportClick()
        {
            IBrokerManagedComponent clientData = this.UMABroker.GetBMCInstance(new Guid(Request.QueryString["ins"].ToString()));
            IOrganization organization = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            this.assets = organization.Assets;
            
            SetReportMenuForEclipseSuper(clientData.ComponentInformation.DisplayName);

            HoldingSummaryReportDS ds = new HoldingSummaryReportDS();
            if (InputEndDate.DateInput.SelectedDate.HasValue)
                 ds.EndDate =  AccountingFinancialYear.LastDayOfMonthFromDateTime(this.InputEndDate.DateInput.SelectedDate.Value);

            if (InputStartDate.DateInput.SelectedDate.HasValue)
                 ds.StartDate = AccountingFinancialYear.FirstDayOfMonthFromDateTime(this.InputStartDate.DateInput.SelectedDate.Value);

            clientData.GetData(ds);
            this.PresentationData = ds;
            DataRow clientSummaryRow = ds.Tables[HoldingSummaryReportDS.CLIENTSUMMARYTABLE].Rows[0];
            string reportName = "Report_" + Guid.NewGuid().ToString();
            C1ReportViewer.RegisterDocument(reportName, MakeDoc);
            ReportViewer.FileName = reportName;
            ReportViewer.ReportName = reportName;
            InputEndDate.DbSelectedDate = ds.EndDate;
            InputStartDate.DbSelectedDate = ds.StartDate; 
            UMABroker.ReleaseBrokerManagedComponent(clientData);
            UMABroker.ReleaseBrokerManagedComponent(organization);
        }
    }
}
