﻿<%@ Page Title="e-Clipse Online Portal" EnableViewState="true" Language="C#" MasterPageFile="~/Site.master"
    AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="eclipseonlineweb._Default" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1GridView"
    TagPrefix="c1" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="SideMenu" ContentPlaceHolderID="SidebarMenu" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="height: 300px">
                <asp:Menu ID="FinancialNavigationMenu" SkipLinkText="" StaticDisplayLevels="2" Width="200px"
                    runat="server" CssClass="LeftNav-Sub" EnableViewState="false" IncludeStyleBlock="false"
                    StaticSubMenuIndent="80px" Orientation="Vertical" OnMenuItemClick="NavigationMenu_MenuItemClick">
                    <Items>
                        <asp:MenuItem Text="ALL ACCOUNTS" />
                        <asp:MenuItem Text="UMA ACCOUNTS - $FUM" />
                        <asp:MenuItem Text="ACCOUNTS SEARCH" />
                    </Items>
                </asp:Menu>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script type="text/javascript">
        function hintContentPie() {
            return Globalize.format(this.y, "N");
        }
        //Resetting menu index
        localStorage['MenuIndex'] = '';
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td width="20%">
                            <asp:ImageButton runat="server" ImageUrl="~/images/download.png" OnClick="DownloadXLS"
                                ID="btnDownload" />
                        </td>
                        <td width="100%" align="right">
                            <asp:Label Font-Bold="true" runat="server" ID="lblDesc" Text="Accounts"></asp:Label>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <c1:C1GridView ShowFilter="true" OnFiltering="Filter" RowStyle-Font-Size="Smaller"
                AllowSorting="true" OnSorting="SortMainList" FilterStyle-Font-Size="Smaller"
                HeaderStyle-Font-Size="Smaller" EnableTheming="true" Width="100%" ID="ClientList"
                CallbackSettings-Action="None" OnPageIndexChanging="ClientList_PageIndexChanging"
                PagerSettings-Mode="Numeric" runat="server" AutogenerateColumns="False" AllowPaging="true"
                PageSize="15" ClientSelectionMode="None" CallbackSettings-Mode="Partial" OnSelectedIndexChanging="C1GridView1_SelectedIndexChanging">
                <SelectedRowStyle />
                <Columns>
                    <c1:C1BoundField Visible="false" DataField="ENTITYCIID_FIELD" HeaderText="ID" SortExpression="ID"
                        FilterOperator="Contains">
                        <HeaderStyle HorizontalAlign="Left" />
                    </c1:C1BoundField>
                    <c1:C1ButtonField Width="10%" DataTextField="ClientID" ButtonType="Link" CommandName="Select">
                        <ItemStyle HorizontalAlign="Center" />
                    </c1:C1ButtonField>
                    <c1:C1BoundField Width="90%" DataField="ENTITYNAME_FIELD" HeaderText="Account Name"
                        SortExpression="ENTITYNAME_FIELD" FilterOperator="Contains">
                        <HeaderStyle HorizontalAlign="Left" />
                    </c1:C1BoundField>
                </Columns>
            </c1:C1GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
