﻿using System;
using System.Threading;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Security;
using Oritax.TaxSimp.Services.CMBroker;
using Oritax.TaxSimp.Utilities;

namespace eclipseonlineweb
{
    public partial class Home : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Master != null) ((HiddenField)Master.FindControl("hfMainMenuText")).Value = "HOME";

            UMABroker = new CMBroker(Context.User, DBConnection.Connection.ConnectionString);
            UMABroker.SetStart();

            var objUser = (DBUser)UMABroker.GetBMCInstance(Page.User.Identity.Name, "DBUser_1_1");
            var dbUserDetailsDs = new DBUserDetailsDS();
            objUser.GetData(dbUserDetailsDs);

            if (Page.User.Identity.Name.ToLower() != "administrator")
            {
                if (objUser.UserType == UserType.Client ||
                    objUser.UserType == UserType.Advisor)
                {
                    liConsolidation.Visible = false;
                }
            }
        }

        private static ICMBroker UMABroker
        {
            get
            {
                var brokerSlot = Thread.GetNamedDataSlot("Broker");

                return (ICMBroker)Thread.GetData(brokerSlot);
            }
            set
            {
                var brokerSlot = Thread.GetNamedDataSlot("Broker");

                if (value == null)
                    Thread.FreeNamedDataSlot("Broker");

                Thread.SetData(brokerSlot, value);
            }
        }
    }
}