﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" AutoEventWireup="True" CodeBehind="ChangePasswordUsingLink.aspx.cs"
    Inherits="eclipseonlineweb.Account.ChangePasswordUsingLink" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1ProgressBar"
    TagPrefix="c1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>e-Clipse Online Portal - Login</title>
    <link rel="stylesheet" href="../Styles/style.css" media="all" />
    <style type="text/css">
        .barBorder
        {
            border: solid 1px red;
            width: 320px;
        }
        .barInternal
        {
            background: yellow;
        }
        .barInternalGreen
        {
            background: green;
        }
    </style>
</head>
<!--[if gte IE 8]>
<style type="text/css">

#Menu{
	background:#474373;
	}
#TopLinks{
	background:#f8f8f8;
	}
#Search-btn{
   background:#e8eb1c;
   }
#hor-zebra th{
   background:#f9f9f9;
   }
#News h1{
   background:#f9f9f9;
   }
.bottom{
   background:#f9f9f9;
   }
.MediumButton {
    background:#e8eb1c;
}
   
</style>
<![endif]-->
<body>
    <form id="form1" runat="server">
    <input runat="server" id="isForwardedAutomatically" value="false" type="hidden" />
    <div class="Container">
        <div id="Content">
            <h2 align="left">
                Change Password
            </h2>
            <p runat="server" id="Success" visible="false">
                Password changed successfully. Proceed to <a href="Login.aspx">Login Form</a> or
                wait and you would be forwarded automatically.
            </p>
            <p runat="server" id="Header1">
                Use the form below to change your password.
            </p>
            <span class="failureNotification" style="font-size: Small">
                <asp:Literal ID="FailureText" runat="server"></asp:Literal>
            </span>
            <br />
            <asp:ScriptManager ID="ScriptManager" runat="server">
            </asp:ScriptManager>
            <asp:ChangePassword ID="ChangeUserPassword" runat="server" CancelDestinationPageUrl="Login.aspx"
                EnableViewState="false" RenderOuterTable="false" SuccessPageUrl="ChangePasswordSuccessUsingLink.aspx">
                <ChangePasswordTemplate>
                    <span class="failureNotification" style="font-size: x-small">
                        <asp:Literal ID="FailureText" runat="server"></asp:Literal>
                    </span>
                    <asp:ValidationSummary ID="ChangeUserPasswordValidationSummary" runat="server" CssClass="failureNotification"
                        Font-Size="Small" ValidationGroup="ChangeUserPasswordValidationGroup" />
                    <div class="accountInfo">
                        <fieldset class="changePassword" style="width: 400px; padding-left: 20px">
                            <legend>Account Information</legend>
                            <asp:Label ID="lblLabel" runat="server"></asp:Label>
                            <p style="display: none">
                                <asp:Label ID="CurrentPasswordLabel" runat="server" AssociatedControlID="CurrentPassword">Old Password:</asp:Label><br />
                                <asp:TextBox ID="CurrentPassword" runat="server" CssClass="Txt-Field-Pwd" TextMode="Password"
                                    Width="250px"></asp:TextBox><br />
                                <%--                            <asp:RequiredFieldValidator ID="CurrentPasswordRequired" runat="server" ControlToValidate="CurrentPassword"
                                CssClass="failureNotification" ErrorMessage="Password is required." ToolTip="Old Password is required."
                                ValidationGroup="ChangeUserPasswordValidationGroup">*</asp:RequiredFieldValidator>--%>
                            </p>
                            <p>
                                <asp:Label ID="NewPasswordLabel" runat="server" AssociatedControlID="NewPassword">New Password:</asp:Label><br />
                                <asp:TextBox ID="NewPassword" runat="server" CssClass="Txt-Field-Pwd" TextMode="Password"
                                    Width="250px"></asp:TextBox><br />
                                <asp:RequiredFieldValidator ID="NewPasswordRequired" runat="server" ControlToValidate="NewPassword"
                                    CssClass="failureNotification" ErrorMessage="New Password is required." ToolTip="New Password is required."
                                    ValidationGroup="ChangeUserPasswordValidationGroup">*</asp:RequiredFieldValidator>
                            </p>
                            <p>
                                <asp:Label ID="ConfirmNewPasswordLabel" runat="server" AssociatedControlID="ConfirmNewPassword">Confirm New Password:</asp:Label><br />
                                <asp:TextBox ID="ConfirmNewPassword" runat="server" CssClass="Txt-Field-Pwd" TextMode="Password"
                                    Width="250px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="ConfirmNewPasswordRequired" runat="server" ControlToValidate="ConfirmNewPassword"
                                    CssClass="failureNotification" Display="Dynamic" ErrorMessage="Confirm New Password is required."
                                    ToolTip="Confirm New Password is required." ValidationGroup="ChangeUserPasswordValidationGroup">*</asp:RequiredFieldValidator>
                                <br />
                                <asp:PasswordStrength ID="TextBox1_PasswordStrength" runat="server" Enabled="True"
                                    TargetControlID="NewPassword" MinimumNumericCharacters="1" MinimumSymbolCharacters="1"
                                    MinimumUpperCaseCharacters="1" PreferredPasswordLength="10" DisplayPosition="BelowLeft"
                                    StrengthIndicatorType="BarIndicator" BarBorderCssClass="barBorder" HelpStatusLabelID="Label1"
                                    StrengthStyles="barInternal;barInternalGreen">
                                </asp:PasswordStrength>
                                <br />
                                <asp:Label ID="Label1" runat="server" AssociatedControlID="NewPassword">New Password:</asp:Label>
                                <asp:CompareValidator ID="NewPasswordCompare" runat="server" ControlToCompare="NewPassword"
                                    ControlToValidate="ConfirmNewPassword" CssClass="failureNotification" Display="Dynamic"
                                    ErrorMessage="The Confirm New Password must match the New Password entry." ValidationGroup="ChangeUserPasswordValidationGroup">*</asp:CompareValidator>
                            </p>
                        </fieldset>
                        <p class="submitButton">
                            <asp:Button ID="CancelPushButton" runat="server" CausesValidation="False" CommandName="Cancel"
                                Text="Cancel" />
                            <asp:Button ID="ChangePasswordPushButton" runat="server" OnClick="ChangePasswordClick"
                                Text="Change Password" ValidationGroup="ChangeUserPasswordValidationGroup" />
                        </p>
                    </div>
                </ChangePasswordTemplate>
            </asp:ChangePassword>
            <div class="statusbar">
                <div class="progress" style="display: none">
                    <c1:C1ProgressBar ID="C1ProgressBar1" runat="server" UseEmbeddedjQuery="false" AnimationDelay="0"
                        AnimationOptions-Duration="800" Height="20px" Width="100%" LabelAlign="Center" />
                </div>
            </div>
        </div>
        <script type="text/javascript">
            function hintContent() {
                return Globalize.format(this.y, "c");
            }

            $(document).ready(function () {
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_initializeRequest(initializeRequest);
                prm.add_beginRequest(beginRequest);
                prm.add_endRequest(endRequest);


            });
            function initializeRequest(sender, args) {
                $(".progress").show();
                $("#<%:C1ProgressBar1.ClientID %>").c1progressbar("option", "value", 25);
            }
            function beginRequest(sender, args) {
                $("#<%:C1ProgressBar1.ClientID %>").c1progressbar("option", "animationOptions", { duration: 2500 });
                $("#<%:C1ProgressBar1.ClientID %>").c1progressbar("option", "value", 80);
            }
            function endRequest(sender, args) {
                $("#<%:C1ProgressBar1.ClientID %>").c1progressbar("option", "animationOptions", { duration: 400 });
                $("#<%:C1ProgressBar1.ClientID %>").c1progressbar("option", "value", 100);
                window.setTimeout(resetIt, 500);
            }
            function resetIt() {
                $("#<%:C1ProgressBar1.ClientID %>").c1progressbar("option", "value", 0);
                $(".progress").hide();
            }
        </script>
        <p>
            <telerik:RadGrid ID="PresentationGrid" runat="server" ShowStatusBar="true" AutoGenerateColumns="False"
                PageSize="20" AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True"
                GridLines="None" AllowAutomaticDeletes="True" AllowFilteringByColumn="true" AllowAutomaticInserts="True"
                AllowAutomaticUpdates="True" EnableViewState="true" ShowFooter="false" OnNeedDataSource="PresentationGrid_OnNeedDataSource"
                Visible="false">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                    Name="Banks" TableLayout="Fixed">
                    <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                    <HeaderStyle Font-Bold="True"></HeaderStyle>
                    <Columns>
                        <telerik:GridBoundColumn SortExpression="Description" ReadOnly="true" HeaderText=""
                            AutoPostBackOnFilter="false" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="Description" UniqueName="Description">
                        </telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
            <p>
            </p>
            <p>
            </p>
        </p>
    </div>
    </form>
    <script language="javascript">
        if (document.getElementById("isForwardedAutomatically").value == "true") {
            setTimeout(Forward, 5000);

        }

        function Forward() {
            location.href = "Login.aspx";
        }
    </script>
</body>
</html>
