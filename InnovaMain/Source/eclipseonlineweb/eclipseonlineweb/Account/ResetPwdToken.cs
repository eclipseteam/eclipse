﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace eclipseonlineweb.Account
{
    [Serializable]
    class ResetPwdToken
    {
        public string UserName;
        public string EmailAddress;
        public Guid Token;
        public DateTime IssueDate;
    }
}
