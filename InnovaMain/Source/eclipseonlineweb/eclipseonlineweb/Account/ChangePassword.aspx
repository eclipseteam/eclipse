﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="~/Site.Master"
    AutoEventWireup="True" CodeBehind="ChangePassword.aspx.cs" Inherits="eclipseonlineweb.Account.ChangePassword" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1ProgressBar"
    TagPrefix="c1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <style>
        .barBorder
        {
            border: solid 1px red;
            width: 320px;
        }
        .barInternal
        {
            background: yellow;
        }
        .barInternalGreen
        {
            background: green;
        }
    </style>
</asp:Content>
<asp:Content ID="SideMenu" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="Content">
     <fieldset class="changePassword" style="width: 100%; padding-left: 50px">
                        <legend>Change Password</legend>
      
        <p>
            Use the form below to change your password.
        </p>
        <span class="failureNotification" style="font-size: Small">
            <asp:Literal ID="FailureText" runat="server"></asp:Literal>
        </span>
        <br />
        <asp:ChangePassword ID="ChangeUserPassword" runat="server" CancelDestinationPageUrl="~/"
            EnableViewState="false" RenderOuterTable="false" SuccessPageUrl="ChangePasswordSuccess.aspx">
            <ChangePasswordTemplate>
                <span class="failureNotification" style="font-size: x-small">
                    <asp:Literal ID="FailureText" runat="server"></asp:Literal>
                </span>
                <asp:ValidationSummary ID="ChangeUserPasswordValidationSummary" runat="server" CssClass="failureNotification"
                    Font-Size="Small" ValidationGroup="ChangeUserPasswordValidationGroup" />
                <div class="accountInfo">
                    <fieldset class="changePassword" style="width: 400px; padding-left: 20px">
                        <asp:Label ID="lblLabel" runat="server"></asp:Label>
                        <p>
                            <asp:Label ID="CurrentPasswordLabel" runat="server" AssociatedControlID="CurrentPassword">Old Password:</asp:Label><br />
                            <asp:TextBox ID="CurrentPassword" runat="server" CssClass="Txt-Field-Pwd" TextMode="Password"
                                Width="250px"></asp:TextBox><br />
                            <asp:RequiredFieldValidator ID="CurrentPasswordRequired" runat="server" ControlToValidate="CurrentPassword"
                                CssClass="failureNotification" ErrorMessage="Password is required." ToolTip="Old Password is required."
                                ValidationGroup="ChangeUserPasswordValidationGroup">*</asp:RequiredFieldValidator>
                        </p>
                        <p>
                            <asp:Label ID="NewPasswordLabel" runat="server" AssociatedControlID="NewPassword">New Password:</asp:Label><br />
                            <asp:TextBox ID="NewPassword" runat="server" CssClass="Txt-Field-Pwd" TextMode="Password"
                                Width="250px"></asp:TextBox><br />
                            <asp:RequiredFieldValidator ID="NewPasswordRequired" runat="server" ControlToValidate="NewPassword"
                                CssClass="failureNotification" ErrorMessage="New Password is required." ToolTip="New Password is required."
                                ValidationGroup="ChangeUserPasswordValidationGroup">*</asp:RequiredFieldValidator>
                        </p>
                        <p>
                            <asp:Label ID="ConfirmNewPasswordLabel" runat="server" AssociatedControlID="ConfirmNewPassword">Confirm New Password:</asp:Label><br />
                            <asp:TextBox ID="ConfirmNewPassword" runat="server" CssClass="Txt-Field-Pwd" TextMode="Password"
                                Width="250px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="ConfirmNewPasswordRequired" runat="server" ControlToValidate="ConfirmNewPassword"
                                CssClass="failureNotification" Display="Dynamic" ErrorMessage="Confirm New Password is required."
                                ToolTip="Confirm New Password is required." ValidationGroup="ChangeUserPasswordValidationGroup">*</asp:RequiredFieldValidator>
                            <br />
                            <asp:PasswordStrength ID="TextBox1_PasswordStrength" runat="server" Enabled="True"
                                TargetControlID="NewPassword" MinimumNumericCharacters="1" MinimumSymbolCharacters="1"
                                MinimumUpperCaseCharacters="1" PreferredPasswordLength="10" DisplayPosition="BelowLeft"
                                StrengthIndicatorType="BarIndicator" BarBorderCssClass="barBorder" HelpStatusLabelID="Label1"
                                StrengthStyles="barInternal;barInternalGreen">
                            </asp:PasswordStrength>
                            <br />
                            <asp:Label ID="Label1" runat="server" AssociatedControlID="NewPassword">New Password:</asp:Label>
                            <asp:CompareValidator ID="NewPasswordCompare" runat="server" ControlToCompare="NewPassword"
                                ControlToValidate="ConfirmNewPassword" CssClass="failureNotification" Display="Dynamic"
                                ErrorMessage="The Confirm New Password must match the New Password entry." ValidationGroup="ChangeUserPasswordValidationGroup">*</asp:CompareValidator>
                        </p>
                    </fieldset>
                    <p class="submitButton">
                        <asp:Button ID="CancelPushButton" runat="server" CausesValidation="False" CommandName="Cancel"
                            Text="Cancel" />
                        <asp:Button ID="ChangePasswordPushButton" runat="server" OnClick="ChangePasswordClick"
                            Text="Change Password" ValidationGroup="ChangeUserPasswordValidationGroup" />
                    </p>
                </div>
            </ChangePasswordTemplate>
        </asp:ChangePassword>
        <div class="statusbar">
            <div class="progress" style="display: none">
                <c1:C1ProgressBar ID="C1ProgressBar1" runat="server" UseEmbeddedjQuery="false" AnimationDelay="0"
                    AnimationOptions-Duration="800" Height="20px" Width="100%" LabelAlign="Center" />
            </div>
        </div>
        </fieldset>
    </div>
    <script type="text/javascript">
        function hintContent() {
            return Globalize.format(this.y, "c");
        }

        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(initializeRequest);
            prm.add_beginRequest(beginRequest);
            prm.add_endRequest(endRequest);


        });
        function initializeRequest(sender, args) {
            $(".progress").show();
            $("#<%:C1ProgressBar1.ClientID %>").c1progressbar("option", "value", 25);
        }
        function beginRequest(sender, args) {
            $("#<%:C1ProgressBar1.ClientID %>").c1progressbar("option", "animationOptions", { duration: 2500 });
            $("#<%:C1ProgressBar1.ClientID %>").c1progressbar("option", "value", 80);
        }
        function endRequest(sender, args) {
            $("#<%:C1ProgressBar1.ClientID %>").c1progressbar("option", "animationOptions", { duration: 400 });
            $("#<%:C1ProgressBar1.ClientID %>").c1progressbar("option", "value", 100);
            window.setTimeout(resetIt, 500);
        }
        function resetIt() {
            $("#<%:C1ProgressBar1.ClientID %>").c1progressbar("option", "value", 0);
            $(".progress").hide();
        }
    </script>
</asp:Content>

