﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="~/NoMenu.Master" AutoEventWireup="true"
    CodeBehind="PermissionDenied.aspx.cs" Inherits="eclipseonlineweb.Account.PermissionDenied" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
<h1>Permission Denied</h1>
<p>You do not have permission to access this page, please refer to your system administrator. Please <INPUT TYPE="button" VALUE="BACK" onClick="history.go(-1);"> to go back to previous page <b>OR</b> Please <a href="../AccountsFUM.aspx">CLICK</a> here to go back to home page</p>
</asp:Content>
