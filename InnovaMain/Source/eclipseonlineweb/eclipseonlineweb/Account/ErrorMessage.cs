﻿using System;

namespace eclipseonlineweb.Account
{
    public class ErrorMessage
    {
       
    public static Exception LastException
    {
        get
        {
            return _LastException;
        }
        set
        {
            if (value != _LastException)
            {
                _LastException = value;
            }
        }
    }

    public static Exception InnerException
    {
        get
        {
            return _InnerException;
        }
        set
        {
            if (value != _InnerException)
            {
                _InnerException = value;
            }
        }
    }

    private static Exception _LastException;
    private static Exception _InnerException;
    }
}