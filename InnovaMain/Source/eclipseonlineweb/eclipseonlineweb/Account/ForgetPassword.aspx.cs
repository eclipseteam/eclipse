﻿using System;
using System.Web;
using System.Web.Security;
using Oritax.TaxSimp.Security;
using System.Data;
using System.Linq;
using Oritax.TaxSimp.SAS70Password;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Utilities;
using System.Globalization;
using System.Configuration;
using Oritax.TaxSimp.Services;
using System.Collections.Generic;

namespace eclipseonlineweb.Account
{
    public partial class ResetPassword : UMABasePage
    {

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (!IsPostBack)
                this.UserName.Text = Request.QueryString["User"];
        }
        private DataSet ConstructAuthenticationDataSet()
        {
            AuthenticateUserDS authenticateDS = new AuthenticateUserDS();
            DataRow drUser;

            if (authenticateDS.Tables.Contains(AuthenticateUserDS.AUTHENTICATION_DETAIL_TABLE))
            {
                drUser = authenticateDS.Tables[AuthenticateUserDS.AUTHENTICATION_DETAIL_TABLE].NewRow();
                drUser[AuthenticateUserDS.AUTHENTICATION_USERNAME_FIELD] = this.UserName.Text;
                drUser[AuthenticateUserDS.AUTHENTICATION_PASSWORD_FIELD] = this.Password.Text;

                authenticateDS.Tables[AuthenticateUserDS.AUTHENTICATION_DETAIL_TABLE].Rows.Add(drUser);

                return authenticateDS;
            }
            else
            {
                throw new Exception("Login.ConstructAuthenticationDataSet - Authentication table not found.");
            }
        }

        public void btnlogin_Click(object sender, System.EventArgs e)
        {

            DBUser objUser;
            DBUserDetailsDS dsUserDetail;



            try
            {
                //Construct a DBUser object and pass username/password details to this object via a AuthenticateUserDS dataset.
                //IIS cachce has little no value going forward  hence clearing cache has no impact
                objUser = (DBUser)UMABroker.GetBMCInstance(this.UserName.Text, "DBUser_1_1");
                if (objUser == null)
                {
                    lblError.Text = AccountStrings.UserUnmatched;
                    return;
                }
                dsUserDetail = new DBUserDetailsDS();
                objUser.GetData(dsUserDetail);
                if (dsUserDetail.Tables[DBUserDetailsDS.USER_DETAIL_TABLE].Rows.Count > 0)
                {
                    DataRow drUserRow = dsUserDetail.Tables[DBUserDetailsDS.USER_DETAIL_TABLE].Rows[0];

                    // Check user is active
                    if (drUserRow[DBUserDetailsDS.USER_EMAILADDRESS_FIELD].ToString() == this.EmailAddress.Text)
                    {
                        var TokenListSource = new WebUtilities.ObjectToFile<List<ResetPwdToken>>(Server.MapPath(AccountStrings.TokenFilePath));
                        var listOfToken = TokenListSource.AccessFile(null, false);
                        var token = new ResetPwdToken()
                        {
                            EmailAddress = EmailAddress.Text,
                            UserName = UserName.Text,
                            Token = Guid.NewGuid(),
                            IssueDate = DateTime.Now
                        };


                        listOfToken = RemoveInvalidTokens(listOfToken);
                        listOfToken.Add(token);

                        TokenListSource.AccessFile(listOfToken, true);
                        WebUtilities.MailHelper.SendMailMessage(AccountStrings.From, EmailAddress.Text, null, null, AccountStrings.Subject, string.Format(AccountStrings.EmailMessage, this.UserName.Text, this.EmailAddress.Text, token.Token));


                        lblError.Text = AccountStrings.EmailSent;
                    }
                    else
                    {
                        lblError.Text = AccountStrings.EmailUnMatched;
                    }

                }


            }
            catch (Exception ex)
            {
                lblError.Text = "Error authenticating. " + ex.Message;
            }
        }

        private List<ResetPwdToken> RemoveInvalidTokens(List<ResetPwdToken> listOfToken)
        {
            //Remove Older Token of same user 
            listOfToken = listOfToken.Where(p => p.UserName != UserName.Text).ToList();
            //Remove 48 Hours old Tokens
            listOfToken = listOfToken.Where(p => DateTime.Now < p.IssueDate.AddDays(2)).ToList();
            return listOfToken;
        }
    }
}
