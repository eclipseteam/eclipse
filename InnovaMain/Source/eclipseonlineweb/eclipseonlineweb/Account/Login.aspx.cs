﻿using System;
using System.Web;
using System.Web.Security;
using Oritax.TaxSimp.Security;
using System.Data;
using Oritax.TaxSimp.SAS70Password;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Utilities;
using System.Globalization;
using System.Configuration;
using Oritax.TaxSimp.Services;
using eclipseonlineweb.WebUtilities;

namespace eclipseonlineweb.Account
{
    public partial class Login : UMABasePage
    {
        private DataSet ConstructAuthenticationDataSet()
        {
            AuthenticateUserDS authenticateDS = new AuthenticateUserDS();
            DataRow drUser;

            if (authenticateDS.Tables.Contains(AuthenticateUserDS.AUTHENTICATION_DETAIL_TABLE))
            {
                drUser = authenticateDS.Tables[AuthenticateUserDS.AUTHENTICATION_DETAIL_TABLE].NewRow();
                drUser[AuthenticateUserDS.AUTHENTICATION_USERNAME_FIELD] = this.UserName.Text;
                drUser[AuthenticateUserDS.AUTHENTICATION_PASSWORD_FIELD] = this.Password.Text;

                authenticateDS.Tables[AuthenticateUserDS.AUTHENTICATION_DETAIL_TABLE].Rows.Add(drUser);

                return authenticateDS;
            }
            else
            {
                throw new Exception("Login.ConstructAuthenticationDataSet - Authentication table not found.");
            }
        }

        public void btnlogin_Click(object sender, System.EventArgs e)
        {
            FormsAuthenticationTicket authTicket = null;
            HttpCookie authCookie = null;

            DBUser objUser;
            SAS70PasswordSM pwdSM;
            AuthenticateUserDS dsAuthenticate;
            DBSecurityGroupDetailsDS dsSecurityGroup;
            PasswordDS dsPassword = new PasswordDS();
            DataRow drUser = null;

            string redirectWorkpaper = "~/Default.aspx";

            try
            {
                //Construct a DBUser object and pass username/password details to this object via a AuthenticateUserDS dataset.
                //IIS cachce has little no value going forward  hence clearing cache has no impact

                objUser = (DBUser)UMABroker.GetBMCInstance(this.UserName.Text, "DBUser_1_1");
                pwdSM = (SAS70PasswordSM)this.UMABroker.GetWellKnownBMC(WellKnownCM.SecurityProvider);

                if (objUser != null && pwdSM != null)
                {
                    // User name is valid, so ExtractData to perform password comparison.
                    dsAuthenticate = (AuthenticateUserDS)this.ConstructAuthenticationDataSet();
                    objUser.GetData(dsAuthenticate);
                    objUser.GetData(dsPassword);

                    // Analyize returned dataset ( from objUser.ExtractData ), 
                    drUser = dsAuthenticate.Tables[AuthenticateUserDS.AUTHENTICATION_DETAIL_TABLE].Rows[0];
                    if (Convert.ToInt32(drUser[AuthenticateUserDS.AUTHENTICATION_AUTHENTICATE_FIELD].ToString()) == AuthenticateUserDS.AUTHENTICATE_SUCCESS && objUser.UserType != UserType.Services)
                    {
                        // Load and add Security Groups to the Users cookie.
                        dsSecurityGroup = new DBSecurityGroupDetailsDS();
                        objUser.GetData(dsSecurityGroup);

                        string userdata = "";
                        userdata += objUser.CID.ToString() + "|";
                        DataTable dtSecurityGroup = dsSecurityGroup.Tables[DBSecurityGroupDetailsDS.SECGRP_DETAIL_TABLE];
                        foreach (DataRow drSecurityGroup in dtSecurityGroup.Rows)
                            userdata += drSecurityGroup[DBSecurityGroupDetailsDS.SECGRP_SECURITYNAME_FIELD] + "|";

                        int timeout = int.Parse(ConfigurationManager.AppSettings["AuthenticationTimeout"]);
                        authTicket = new FormsAuthenticationTicket(1, this.UserName.Text, DateTime.Now, DateTime.Now.AddMinutes(timeout), false, userdata);
                        string encryptedTicket = FormsAuthentication.Encrypt(authTicket);
                        authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                        Response.Cookies.Add(authCookie);

                        UserAuthentication.LogUserIn(this.UserName.Text);
                    }
                    else if (objUser.UserType == UserType.Services)
                    {
                        DataRow drAuthenticate = dsAuthenticate.Tables[AuthenticateUserDS.AUTHENTICATION_DETAIL_TABLE].Rows[0];
                        int failedAttempts = Convert.ToInt32(drAuthenticate[AuthenticateUserDS.AUTHENTICATION_FAILEDATTEMPTS_FIELD], NumberFormatInfo.InvariantInfo);
                        this.lblError.Text = "Services User Cannot access main online site";
                        this.UMABroker.LogEvent(EventType.LoginUnsuccessful, String.Format("'{0} unsuccessful login attempts {1}: Service user tried to login to main site.", this.UserName.Text, failedAttempts.ToString(CultureInfo.InvariantCulture)), EventType.SYSTEM_EVENT_TYPE, Guid.Empty);
                    }
                    else
                    {
                        DataRow drAuthenticate = dsAuthenticate.Tables[AuthenticateUserDS.AUTHENTICATION_DETAIL_TABLE].Rows[0];
                        int failedAttempts = Convert.ToInt32(drAuthenticate[AuthenticateUserDS.AUTHENTICATION_FAILEDATTEMPTS_FIELD], NumberFormatInfo.InvariantInfo);
                        switch (Convert.ToInt32(drUser[AuthenticateUserDS.AUTHENTICATION_AUTHENTICATE_FIELD].ToString()))
                        {

                            case AuthenticateUserDS.AUTHENTICATE_BADUSER:
                                this.lblError.Text = AuthenticateUserDS.MSG_BADPASSWORD;
                                this.UMABroker.LogEvent(EventType.LoginUnsuccessful, String.Format("'{0} unsuccessful login attempts {1}: Invalid login name", this.UserName.Text, failedAttempts.ToString(CultureInfo.InvariantCulture)), EventType.SYSTEM_EVENT_TYPE, Guid.Empty);
                                break;

                            case AuthenticateUserDS.AUTHENTICATE_BADPASSWORD:
                                this.lblError.Text = AuthenticateUserDS.MSG_BADPASSWORD;
                                this.UMABroker.LogEvent(EventType.LoginUnsuccessful, String.Format("{0} unsuccessful login attempts {1}: Incorrect password", this.UserName.Text, failedAttempts.ToString(CultureInfo.InvariantCulture)), EventType.SYSTEM_EVENT_TYPE, Guid.Empty);
                                if (failedAttempts >= pwdSM.MaxFailureAttempts)
                                {
                                    this.UMABroker.LogEvent(EventType.UserLocked, "User account locked: " + this.UserName.Text.Trim(), EventType.SYSTEM_EVENT_TYPE, Guid.Empty);
                                }
                                break;

                            case AuthenticateUserDS.AUTHENTICATE_DISABLED:
                                this.lblError.Text = AuthenticateUserDS.MSG_DISABLED;
                                this.UMABroker.LogEvent(EventType.LoginUnsuccessful, String.Format("{0} unsuccessful login: Account is disabled", this.UserName.Text), EventType.SYSTEM_EVENT_TYPE, Guid.Empty);
                                break;

                            case AuthenticateUserDS.AUTHENTICATE_LOCKED:
                                this.lblError.Text = AuthenticateUserDS.MSG_LOCKED;
                                this.UMABroker.LogEvent(EventType.LoginUnsuccessful, String.Format("{0} unsuccessful login: Account is locked", this.UserName.Text), EventType.SYSTEM_EVENT_TYPE, Guid.Empty);
                                break;
                        }
                    }
                }
                else
                {
                    this.lblError.Text = AuthenticateUserDS.MSG_BADPASSWORD;
                    this.UMABroker.LogEvent(EventType.LoginUnsuccessful, String.Format("{0} unsuccessful login: Invalid login name", this.UserName.Text), EventType.SYSTEM_EVENT_TYPE, Guid.Empty);
                }

                if (authCookie != null)
                {
                    if (objUser != null && pwdSM != null && drUser != null)
                    {
                        bool isPwdInReminder = pwdSM.IsInReminderTimeSpan(objUser.CID, DateTime.Now);
                        // bool isPwdExpired = pwdSM.IsPasswordExpired(objUser.CID, DateTime.Now);
                        bool isPwdValid = pwdSM.CheckAllRulesAtLogin(objUser.CID, objUser.Name, this.Password.Text, dsPassword.Tables[PasswordDS.PasswordRulesTable], DateTime.Now, false);

                        //if (isPwdValid && !isPwdInReminder)  // override password expiry reminder for now
                        //if (isPwdValid)
                        //{
                        this.UMABroker.LogEvent(EventType.LoginSuccessful, "Login successful '" + this.UserName.Text + "'", this.UserName.Text, Guid.Empty);
                        Response.Redirect(redirectWorkpaper, false);
                        //}
                    }
                }

                if (objUser != null)
                {
                    this.Session[DBUser.sessionUserNameKey] = objUser.Name;
                    this.Session[DBUser.SessionUserPasswordKey] = this.Password.Text;

                    if (objUser.Name.ToLower() == "administrator".ToLower())
                        this.Session[DBUser.sessionIsAdminKey] = true;
                    else
                        this.Session[DBUser.sessionIsAdminKey] = objUser.Administrator;

                    this.Session[DBUser.sessionUserCIDKey] = objUser.CID.ToString();
                    this.Session["UserType"] = objUser.UserType;
                }

                this.UMABroker.ReleaseBrokerManagedComponent(objUser);
                this.UMABroker.ReleaseBrokerManagedComponent(pwdSM);
            }
            catch (Exception ex)
            {
                lblError.Text = "Error authenticating. " + ex.Message;
                Utilities.LogException(ex, Request, Context);
            }
        }
    }
}