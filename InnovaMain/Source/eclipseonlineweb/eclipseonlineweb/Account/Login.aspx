﻿<%@ Page Language="C#" Title="e-Clipse Online Portal - Log In" AutoEventWireup="true"
    CodeBehind="Login.aspx.cs" Inherits="eclipseonlineweb.Account.Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>e-Clipse Online Portal - Login</title>
    <link rel="stylesheet" href="../Styles/style.css" media="all" />
    <script language="javascript">
        function ToForgotPassword() {
            location.href = "ForgetPassword.aspx?User=" + document.getElementById("UserName").value;
        }
    </script>
</head>
<!--[if gte IE 8]>
<style type="text/css">

#Menu{
	background:#474373;
	}
#TopLinks{
	background:#f8f8f8;
	}
#Search-btn{
   background:#e8eb1c;
   }
#hor-zebra th{
   background:#f9f9f9;
   }
#News h1{
   background:#f9f9f9;
   }
.bottom{
   background:#f9f9f9;
   }
.MediumButton {
    background:#e8eb1c;
}
   
</style>
<![endif]-->
<body>
    <form id="form1" runat="server">
    <div class="Container">
        <!--Container Start-->
        <div class="Logo">
            <img src="../images/eclipse-logo1.png" width="184" height="81" alt="Eclipse-Logo" /></div>
        <!--Logo-->
        <div class="LoginBox">
            <!--Login Box Start-->
            <div class="LoginForm">
                <!--Login Form Start-->
                <div>
                    <em>Username</em>
                    <%--<input name="" class="TextField" type="text" />--%>
                    <asp:TextBox ID="UserName" ClientIDMode="Inherit" runat="server" CssClass="TextField"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                        CssClass="failureNotification" ErrorMessage="User Name is required." ToolTip="User Name is required."
                        ValidationGroup="LoginUserValidationGroup">*</asp:RequiredFieldValidator>
                </div>
                <div id="Space">
                    <em>Password</em>
                    <%--<input name="" class="TextField" type="text" />--%>
                    <asp:TextBox ID="Password" runat="server" CssClass="TextField" TextMode="Password"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password"
                        CssClass="failureNotification" ErrorMessage="Password is required." ToolTip="Password is required."
                        ValidationGroup="LoginUserValidationGroup">*</asp:RequiredFieldValidator>
                </div>
                <div style="padding: 15px 40px 10px 60px;">
                    <%--<input name="" type="button" id="button" />--%>
                   <%-- <a onclick="ToForgotPassword()" style="cursor: pointer; color: Yellow; margin-left: 40px">
                        Forgot Password?</a>--%>
                    <asp:Button ID="LoginButton" runat="server" CommandName="Login" Text="Log in" ValidationGroup="LoginUserValidationGroup"
                        Style="margin-right: -5px" CssClass="MediumButton" OnClick="btnlogin_Click" />
                </div>
                <div>
                    <asp:Label ForeColor="Yellow" ID="lblError" CssClass="errortext" runat="server" Font-Bold="false"></asp:Label>
                </div>
            </div>
            <!--Login Form End-->
        </div>
        <!--Login Box End-->
    </div>
    <!--Conatiner End-->
    </form>
</body>
</html>
