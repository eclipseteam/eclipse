﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eclipseonlineweb.Account
{
    public class AccountStrings
    {
        public const string TokenFilePath = @"~\App_data\ResetPwdToken.bin";
        public const string From = @"E-Clipse Support <eclipsesupport@objectsynergy.com>";
        public const string Subject = @"E-Clipse user account";
        public const string EmailMessage =
            @"<html>
	            <body>
		            <p>
			            We have received your request to reset your E-Clipse user account password. To reset your password, you must:</p>
		            <p>
			            1. Visit this URL<br />
			            <a class=""external-link"" href=""http://localhost/EclipseOnlineWeb/Account/ChangePasswordUsingLink.aspx?user={0}&email={1}&token={2}"" title=""Follow link"">Click here to reset your password.</a><br />Or copy paste the link below into a browser. <BR/>
                        http://localhost/EclipseOnlineWeb/Account/ChangePasswordUsingLink.aspx?user={0}&email={1}&token={2} <BR/><BR/>
			            2. Enter a new password.<br />
			            3. Confirm your new password.</p>
		            <p>
			            If you have received this message in error, no further action is required and your original password will continue to function. This URL will expire in 48 hours.
		            <p>
			            Thank you,<br />
			            E-Clipse Support Services</p>
	            </body>
            </html>
            ";
        public const string EmailUnMatched = @"Your e-mail address doesn't match our records. Please try again.";
        public const string EmailSent = @"The e-mail has been sent. Use the link contained in the email to change your password. Please ensure that the email is not blocked by spam filters.";
        public const string UserUnmatched = @"Your User Name doesn't match our records. Please try again.";
        public const string LinkExpired = @"The link has already been used or expired. <a href='ForgetPassword.aspx?User={0}'>Click here</a> to regenerate the password change request and retry. ";
    }
}