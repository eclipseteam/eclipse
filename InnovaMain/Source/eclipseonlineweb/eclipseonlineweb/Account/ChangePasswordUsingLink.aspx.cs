﻿using System;
using System.Linq;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.SAS70Password;
using System.Data;
using Oritax.TaxSimp.Security;
using System.Collections.Generic;
using Telerik.Web.UI;

namespace eclipseonlineweb.Account
{
    public partial class ChangePasswordUsingLink : UMABasePage
    {
        private int pwdMinLength = 0;
        private int pwdMaxLength = 0;
        private int repeatingCharLength = 0;
        private int historyReq = 0;
        private SAS70PasswordSM pwdSM = null;
        private DataView _dataView;

        protected override void GetBMCData()
        {
            base.GetBMCData();
            PasswordDS ds = new PasswordDS();
            IBrokerManagedComponent ibmc = UMABroker.GetBMCInstance(Request.QueryString["User"], "DBUser_1_1");
            ibmc.GetData(ds);
            PresentationData = ds;
            _dataView = ds.Tables[PasswordDS.PasswordRulesTable].DefaultView;
            UMABroker.ReleaseBrokerManagedComponent(ibmc);
        }

        public override void PopulatePage(DataSet ds)
        {
            if (ds.Tables[PasswordDS.PasswordDetailsTable].Rows.Count > 0)
            {
                //DataRow drPassword = ds.Tables[PasswordDS.PasswordDetailsTable].Rows[0];
                //ViewState["HashedCurrentPassword"] = drPassword[PasswordDS.PasswordPasswordField].ToString();
                if (!String.IsNullOrEmpty(Request.QueryString["Message"]))
                {
                    FailureText.Text = Request.QueryString["Message"];
                    ChangeUserPassword.Visible = false;
                    Header1.Visible = false;
                }
                if (!ValidateToken())
                {
                    FailureText.Text = string.Format(AccountStrings.LinkExpired, Request.QueryString["User"]);
                    ChangeUserPassword.Visible = false;
                    Header1.Visible = false;
                }
                InitiateVariables();
            }
        }



        protected void ChangePasswordClick(object sender, EventArgs e)
        {
            UMABroker.SaveOverride = true;

            InitiateVariables();
            DataRow[] drCollection = PresentationData.Tables[PasswordDS.PasswordDetailsTable].Select(PasswordDS.PasswordUsernameField + " = '" + Request.QueryString["User"] + "'", String.Empty, DataViewRowState.CurrentRows);
            Guid userID = new Guid(drCollection[0][PasswordDS.PasswordUserIdField].ToString());
            string userName = String.Empty;

            if (drCollection.Length == 1)
            {
                drCollection[0][PasswordDS.PasswordPasswordField] = Oritax.TaxSimp.Utilities.Encryption.EncryptData(ChangeUserPassword.NewPassword);
                userName = drCollection[0][PasswordDS.PasswordUsernameField].ToString();
                //LogEvent(EventType.ChangePassword, "Password of user '" + drCollection[0][PasswordDS.PasswordUsernameField].ToString() + "' Updated");
            }
            else
            {
                DataRow drPassword = PresentationData.Tables[PasswordDS.PasswordDetailsTable].NewRow();

                drPassword[PasswordDS.PasswordPasswordField] = Oritax.TaxSimp.Utilities.Encryption.EncryptData(ChangeUserPassword.NewPassword);
                PresentationData.Tables[PasswordDS.PasswordDetailsTable].Rows.Add(drPassword);
                userName = drPassword[PasswordDS.PasswordUsernameField].ToString();
                //LogEvent(EventType.ChangePassword, "Password of user '" + drPassword[PasswordDS.PasswordUsernameField].ToString() + "' Updated");
            }

            IBrokerManagedComponent objUser = UMABroker.GetBMCInstance(Request.QueryString["User"], "DBUser_1_1");

            bool isPwdInReminder = pwdSM.IsInReminderTimeSpan(objUser.CID, DateTime.Now);
            // bool isPwdExpired = pwdSM.IsPasswordExpired(objUser.CID, DateTime.Now);
            bool isPwdValid = pwdSM.CheckAllRulesAtLogin(objUser.CID, objUser.Name, ChangeUserPassword.NewPassword, PresentationData.Tables[PasswordDS.PasswordRulesTable], DateTime.Now, false);

            //if (!isPwdValid)
            //    FailureText.Text = "New Password does not meet min. requirement.";
            if (ValidateToken())
            {
                DeleteToken();
            };
            objUser.SetData(PresentationData);
            UMABroker.ReleaseBrokerManagedComponent(objUser);

            pwdSM.InsertCurrentPassword(userID, Request.QueryString["User"], ChangeUserPassword.NewPassword);
            UMABroker.SetComplete();

            ShowSuccess();
            //Response.Redirect("ChangePasswordSuccessUsingLink.aspx");
        }

        private void ShowSuccess()
        {
            ChangeUserPassword.Visible = false;
            FailureText.Visible = false;
            Header1.Visible = false;
            Success.Visible = true;
            isForwardedAutomatically.Value = "true";
        }

        private void DeleteToken()
        {
            var TokenListSource = new WebUtilities.ObjectToFile<List<ResetPwdToken>>(Server.MapPath(AccountStrings.TokenFilePath));
            var listOfToken = TokenListSource.AccessFile(null, false);
            var token = listOfToken.FirstOrDefault(p => p.Token == new Guid(Request.QueryString["Token"]) && p.IssueDate.AddDays(2) > DateTime.Now && Request.QueryString["User"].ToLower() == p.UserName.ToLower());
            listOfToken.Remove(token);
            TokenListSource.AccessFile(listOfToken, true);
        }

        private bool ValidateToken()
        {
            var TokenListSource = new WebUtilities.ObjectToFile<List<ResetPwdToken>>(Server.MapPath(AccountStrings.TokenFilePath));
            var listOfToken = TokenListSource.AccessFile(null, false);
            var token = listOfToken.FirstOrDefault(p => p.Token == new Guid(Request.QueryString["Token"]) && p.IssueDate.AddDays(2) > DateTime.Now && Request.QueryString["User"].ToLower() == p.UserName.ToLower());
            if (token == null)
            {
                return false;
            }
            else
                return true;

        }

        private void InitiateVariables()
        {
            pwdSM = (SAS70PasswordSM)UMABroker.GetWellKnownBMC(WellKnownCM.SecurityProvider);
            pwdMinLength = pwdSM.PasswordMinLength;
            pwdMaxLength = pwdSM.PasswordMaxLength;
            repeatingCharLength = pwdSM.MaxRepeatingChars;
            historyReq = pwdSM.PasswordHistory;
        }

        protected void PresentationGrid_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            PresentationGrid.DataSource = _dataView.ToTable();
        }
    }
}
