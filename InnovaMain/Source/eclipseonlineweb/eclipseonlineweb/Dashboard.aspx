﻿<%@ Page Title="e-Clipse Online Portal" Language="C#" MasterPageFile="~/Home.master" EnableEventValidation="false"
    AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="eclipseonlineweb.Dashboard" %>

<%@ Register TagPrefix="uc1" TagName="BreadCrumb" Src="~/Controls/BreadCrumb.ascx" %>
<%@ Register TagPrefix="uc1" TagName="AdminDashboardControl" Src="Controls/AdminDashboard.ascx" %>
<%@ Register TagPrefix="uc1" TagName="AdviserDashboardControl" Src="Controls/AdviserDashboard.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .RadGrid_Metro .rgHeader, .RadGrid_Metro .rgHeader a
        {
            font-size: 8pt !important;
        }
        .RadGrid_Metro .rgRow a, .RadGrid_Metro .rgAltRow a, .RadGrid_Metro tr.rgEditRow a, .RadGrid_Metro .rgFooter a, .RadGrid_Metro .rgEditForm a
        {
            font-size: 8pt !important;
        }
        .MyGridClass .rgDataDiv
        {
            height: auto !important;
        }
    </style>
    <script type="text/javascript">
        //Resetting menu index
        localStorage['MenuIndex'] = '0';
    </script>
    <script>
        $(document).ready(function () {
            $('.rgDataDiv').height('135px')
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td width="100%" class="breadcrumbgap">
                            <uc1:BreadCrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <asp:Label Font-Bold="true" runat="server" ID="lblDesc" Text="Dashboard"></asp:Label>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <div id="divAdmin" runat="server" Visible="False">
                <uc1:AdminDashboardControl ID="dashAdmin" runat="server"></uc1:AdminDashboardControl>
            </div>
            <div id="divAdviser" runat="server" Visible="False">
                <uc1:AdviserDashboardControl ID="dashAdviser" runat="server"></uc1:AdviserDashboardControl>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
