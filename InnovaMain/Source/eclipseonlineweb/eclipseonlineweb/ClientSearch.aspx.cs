﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using Oritax.TaxSimp.CM.Organization.Data;
using Oritax.TaxSimp.Security;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Utilities;
using Telerik.Web.UI;
using eclipseonlineweb.WebUtilities;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using System.Collections.Generic;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.CM;
using System.Linq; 
using Oritax.TaxSimp.CM.Group;
using Oritax.TaxSimp.Calculation;

namespace eclipseonlineweb
{
    public partial class ClientSearch : UMABasePage
    {
        string searchFilter = string.Empty;

        public override void LoadPage()
        {
            if (User.Identity.Name.ToLower() == "administrator")
                pnlSuperAdmin.Visible = true;
            else
            {
                PresentationGrid.Columns.FindByUniqueNameSafe("DeleteColumn").Visible = false;
                btnSetTransactions.Visible = false;
                pnlAdminFunctions.Visible = false;
            }

            ScriptManager sm = ScriptManager.GetCurrent(Page);
            if (sm != null)
                sm.RegisterPostBackControl(btnExportAdvisers);

            if (!string.IsNullOrEmpty(Request.Params["mode"]) && !string.IsNullOrEmpty(Request.Params["name"]) && txtSearchBox.Text == string.Empty)
            {
                searchFilter = Request.Params["mode"];
                txtSearchBox.Text = Request.Params["name"];
            }
        }

        protected void BtnSearchClick(object sender, EventArgs e)
        {
            PresentationGrid.Rebind();
        }

        protected void btnFixPhoneNumbersClick(object sender, EventArgs e)
        {
            OrganisationListingDS organisationListingDS = new OrganisationListingDS();
            organisationListingDS.OrganisationListingOperationType = OrganisationListingOperationType.ClientsWithFUM;
            organisationListingDS.AsOfDate = DateTime.Now;

            IOrganization iorgCM = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            iorgCM.GetData(organisationListingDS);


            IBrokerManagedComponent user = this.UMABroker.GetBMCInstance("Administrator", "DBUser_1_1");
            object[] args = new Object[3] { iorgCM.CLID, user.CID, EnableSecurity.SecuritySetting };
            DataView view = new DataView(this.UMABroker.GetBMCDetailList(OrganizationInstall.ASSEMBLY_NAME, args).Tables["Entities_Table"]);
            view.RowFilter = organisationListingDS.GetRowFilter();
            view.Sort = "ENTITYNAME_FIELD ASC";
            DataTable orgFiteredTable = view.ToTable();
            foreach (DataRow row in orgFiteredTable.Rows)
            {
                Guid cid = new Guid(row["ENTITYCIID_FIELD"].ToString());
                IOrganizationUnit organizationUnit = this.UMABroker.GetBMCInstance(cid) as IOrganizationUnit;
                ClientDataOperationDS clientDataOperationDS = new ClientDataOperationDS();
                clientDataOperationDS.CommandType = ClientDataSetOperationCommandTypes.PhoneFix;

                if (organizationUnit.IsInvestableClient)
                    SaveData(organizationUnit.CID.ToString(), clientDataOperationDS);
            }
        }
        
        protected void deleteWrongAttachments(object sender, EventArgs e)
        {
            IBrokerManagedComponent user = this.UMABroker.GetBMCInstance(UMABroker.UserContext.Identity.Name, "DBUser_1_1");
            IOrganization iorgCM = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            object[] args = new Object[3] { iorgCM.CLID, user.CID, EnableSecurity.SecuritySetting };
            DataView view = new DataView(this.UMABroker.GetBMCDetailList(OrganizationInstall.ASSEMBLY_NAME, args).Tables["Entities_Table"]);
            view.RowFilter = new OrganisationListingDS().GetRowFilter(); 
            view.Sort = "ENTITYNAME_FIELD ASC";
            DataTable orgFiteredTable = view.ToTable();

            foreach (DataRow row in orgFiteredTable.Rows)
            {
                Guid cid = new Guid(row["ENTITYCIID_FIELD"].ToString());
                UMABroker.SetStart();
                IOrganizationUnit clientData = this.UMABroker.GetBMCInstance(cid) as IOrganizationUnit;
                UMABroker.SaveOverride = true;

               AttachmentListDS attachmentListDS = new AttachmentListDS();
               clientData.GetData(attachmentListDS);

                DataTable objAttachmentsTable = attachmentListDS.Tables[AttachmentListDS.ATTACHMENTTABLE];

                if (objAttachmentsTable.Columns.Contains(AttachmentListDS.ATTACHMENTTYPENAME_FIELD))
                    objAttachmentsTable.Columns.Remove(AttachmentListDS.ATTACHMENTTYPENAME_FIELD);

                objAttachmentsTable.AcceptChanges();

                foreach (DataRow objRow in objAttachmentsTable.Rows)
                {
                    if (objRow[AttachmentListDS.ATTACHMENTORIGINPATH_FIELD].ToString().EndsWith("- Annual Report.pdf") || objRow[AttachmentListDS.ATTACHMENTORIGINPATH_FIELD].ToString().EndsWith("- 2013 Annual Report.pdf"))
                    {
                        objRow.Delete();
                        this.UMABroker.LogEvent(EventType.UMAClientDoocumentDelete, clientData.CID, "The attachments of Client:" + clientData.ClientId + "-" + clientData.Name + "is Deleted");
                    }
                }
                clientData.SetData(attachmentListDS);
                UMABroker.SetComplete();
                UMABroker.SetStart();
            }
        }

        protected void btnFixIndividualCinstanceNameClick(object sender, EventArgs e)
        {
            IBrokerManagedComponent user = this.UMABroker.GetBMCInstance(UMABroker.UserContext.Identity.Name, "DBUser_1_1");
            IOrganization iorgCM = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            OrganisationListingDS orgDS = new OrganisationListingDS();
            orgDS.OrganisationListingOperationType = OrganisationListingOperationType.IndividualListOnlyNames;
            iorgCM.GetData(orgDS);

            foreach (DataRow row in orgDS.Tables[OrganisationListingDS.INDIVIDUALLISTWITHOUTDETAILSTABLENAME].Rows)
            {
                Guid cid = new Guid(row["ENTITYCIID_FIELD"].ToString());
                IOrganizationUnit organizationUnit = this.UMABroker.GetBMCInstance(cid) as IOrganizationUnit;
                UMABroker.SaveOverride = true;
                IndividualEntity individualEntity = organizationUnit.ClientEntity as IndividualEntity;
                organizationUnit.Name = individualEntity.Fullname;
                ILogicalModule logicalLM = UMABroker.GetLogicalCM(organizationUnit.CLID);
                logicalLM.Name = organizationUnit.Name; 
                organizationUnit.CalculateToken(true);
                logicalLM.CalculateToken(true); 
                UMABroker.SetComplete();
                UMABroker.SetStart();
            }
        }

        protected void btnFixAdviserinstanceNameClick(object sender, EventArgs e)
        {
            IBrokerManagedComponent user = this.UMABroker.GetBMCInstance(UMABroker.UserContext.Identity.Name, "DBUser_1_1");
            IOrganization iorgCM = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            OrganisationListingDS orgDS = new OrganisationListingDS();
            orgDS.OrganisationListingOperationType = OrganisationListingOperationType.AdvisersBasicList;
            iorgCM.GetData(orgDS);

            foreach (DataRow row in orgDS.Tables[OrganisationListingDS.ADVISERLISTINGTABLE].Rows)
            {
                Guid cid = new Guid(row["ENTITYCIID_FIELD"].ToString());
                IOrganizationUnit organizationUnit = this.UMABroker.GetBMCInstance(cid) as IOrganizationUnit;
                UMABroker.SaveOverride = true;
                AdvisorEntity advisorEntity = organizationUnit.ClientEntity as AdvisorEntity;
                organizationUnit.Name = advisorEntity.FullName;
                ILogicalModule logicalLM = UMABroker.GetLogicalCM(organizationUnit.CLID);
                logicalLM.Name = organizationUnit.Name;
                organizationUnit.CalculateToken(true);
                logicalLM.CalculateToken(true);
                UMABroker.SetComplete();
                UMABroker.SetStart();
            }
        }

        protected void btnClearDeletedRecordsMIS(object sender, EventArgs e)
        {
            IBrokerManagedComponent user = this.UMABroker.GetBMCInstance(UMABroker.UserContext.Identity.Name, "DBUser_1_1");
            IOrganization iorgCM = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            object[] args = new Object[3] { iorgCM.CLID, user.CID, EnableSecurity.SecuritySetting };
            DataView view = new DataView(this.UMABroker.GetBMCDetailList(OrganizationInstall.ASSEMBLY_NAME, args).Tables["Entities_Table"]);
            view.RowFilter = new OrganisationListingDS().GetRowFilter(); 
            view.Sort = "ENTITYNAME_FIELD ASC";
            DataTable orgFiteredTable = view.ToTable();

            IOrganizationUnit manageFundBMC = UMABroker.GetBMCInstance(new Guid("b354e928-a3c2-43d7-b61e-feb215cf1bd3")) as IOrganizationUnit;
            ManagedInvestmentSchemesAccountEntity manageEntity = manageFundBMC.ClientEntity as ManagedInvestmentSchemesAccountEntity;

            foreach (var fundAccount in manageEntity.FundAccounts)
            {
                if(fundAccount.DeletedFundTransactions != null) 
                    fundAccount.DeletedFundTransactions.Clear();
                
                var fundAccountTransGroup = fundAccount.FundTransactions.GroupBy(tran => tran.ClientID);

                foreach (var fundAccountGroupItem in fundAccountTransGroup)
                {
                    var foundRows = orgFiteredTable.Select().Where(row => row["ENTITYECLIPSEID_FIELD"].ToString() == fundAccountGroupItem.Key);

                    bool foundIt = false;
                    if (foundRows.Count() > 0)
                    {
                        foundIt = true;//Do no do anything just for debug
                    }
                    else
                    {
                        var transactionsToDelete = fundAccountGroupItem.ToList();
                        foreach (var transactionToDel in transactionsToDelete)
                        {
                            fundAccount.FundTransactions.Remove(transactionToDel);
                            fundAccount.DeletedFundTransactions.Add(transactionToDel); 
                        }
                    }
                }
            }

            SaveData(manageFundBMC as IBrokerManagedComponent); 
        }

        protected void btnFixEclipseSuperNameClick(object sender, EventArgs e)
        {
            IBrokerManagedComponent user = this.UMABroker.GetBMCInstance(UMABroker.UserContext.Identity.Name, "DBUser_1_1");
            IOrganization iorgCM = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            OrganisationListingDS orgDS = new OrganisationListingDS();
            orgDS.OrganisationListingOperationType = OrganisationListingOperationType.EclipseSuper;
            iorgCM.GetData(orgDS);

            foreach (DataRow row in orgDS.Tables[OrganisationListingDS.ECLIPSESUPERTABLENAME].Rows)
            {
                Guid cid = new Guid(row["ENTITYCIID_FIELD"].ToString());
                IOrganizationUnit organizationUnit = this.UMABroker.GetBMCInstance(cid) as IOrganizationUnit;
                UMABroker.SaveOverride = true;
                string[] names = organizationUnit.Name.Split(':');
                if (names.Length == 2)
                {
                    organizationUnit.OtherID = names[0];
                    organizationUnit.Name = names[1].Trim();

                    ILogicalModule logicalLM = UMABroker.GetLogicalCM(organizationUnit.CLID);
                    logicalLM.Name = organizationUnit.Name; 

                    IClientUMAData clientUMAData = organizationUnit.ClientEntity as IClientUMAData;
                    clientUMAData.Name =  organizationUnit.Name;
                    clientUMAData.LegalName = organizationUnit.Name;
                    logicalLM.CalculateToken(true); 
                    organizationUnit.CalculateToken(true);
                    UMABroker.SetComplete();
                    UMABroker.SetStart();
                }
            }

            object[] args = new Object[3] { iorgCM.CLID, user.CID, EnableSecurity.SecuritySetting };
            DataView view = new DataView(this.UMABroker.GetBMCDetailList(OrganizationInstall.ASSEMBLY_NAME, args).Tables["Entities_Table"]);
            view.RowFilter = orgDS.GetRowFilterAllClientsExceptEclipse();
            view.Sort = "ENTITYNAME_FIELD ASC";
            DataTable orgFiteredTable = view.ToTable();
            foreach (DataRow row in orgFiteredTable.Rows)
            {
                Guid cid = new Guid(row["ENTITYCIID_FIELD"].ToString());
                IOrganizationUnit organizationUnit = this.UMABroker.GetBMCInstance(cid) as IOrganizationUnit;
                UMABroker.SaveOverride = true;
                organizationUnit.OtherID = string.Empty;
                organizationUnit.CalculateToken(true);
                UMABroker.SetComplete();
                UMABroker.SetStart();
           
            }
        }

        protected void btnFixEclipseSuperNameDIFM(object sender, EventArgs e)
        {
            IBrokerManagedComponent user = this.UMABroker.GetBMCInstance(UMABroker.UserContext.Identity.Name, "DBUser_1_1");
            IOrganization iorgCM = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            OrganisationListingDS orgDS = new OrganisationListingDS();
            orgDS.OrganisationListingOperationType = OrganisationListingOperationType.EclipseSuper;
            iorgCM.GetData(orgDS);

            foreach (DataRow row in orgDS.Tables[OrganisationListingDS.ECLIPSESUPERTABLENAME].Rows)
            {
                Guid cid = new Guid(row["ENTITYCIID_FIELD"].ToString());
                IOrganizationUnit organizationUnit = this.UMABroker.GetBMCInstance(cid) as IOrganizationUnit;
                IClientUMAData clientUMAData = organizationUnit.ClientEntity as IClientUMAData;
                if (organizationUnit.StatusType == StatusType.Active)
                {
                    clientUMAData.Servicetype.DO_IT_FOR_ME = true;
                    clientUMAData.Servicetype.DO_IT_WITH_ME = true;
                    this.SaveData(organizationUnit as IBrokerManagedComponent);
                }
            }
        }

        protected void btnFixEclipseSuperClearTDTrans(object sender, EventArgs e)
        {
            IBrokerManagedComponent user = this.UMABroker.GetBMCInstance(UMABroker.UserContext.Identity.Name, "DBUser_1_1");
            IOrganization iorgCM = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            OrganisationListingDS orgDS = new OrganisationListingDS();
            orgDS.OrganisationListingOperationType = OrganisationListingOperationType.EclipseSuper;
            iorgCM.GetData(orgDS);

            foreach (DataRow row in orgDS.Tables[OrganisationListingDS.ECLIPSESUPERTABLENAME].Rows)
            {
                Guid cid = new Guid(row["ENTITYCIID_FIELD"].ToString());
                IOrganizationUnit organizationUnit = this.UMABroker.GetBMCInstance(cid) as IOrganizationUnit;
                IClientEntityOrgUnits clientUMAData = organizationUnit.ClientEntity as IClientEntityOrgUnits;
                foreach (var accountProcess in clientUMAData.ListAccountProcess)
                {
                    if (accountProcess.LinkedEntityType == OrganizationType.TermDepositAccount && accountProcess.LinkedEntity.Clid != null && accountProcess.LinkedEntity.Clid != Guid.Empty)
                    {
                        IOrganizationUnit bankAccount = UMABroker.GetCMImplementation(accountProcess.LinkedEntity.Clid, accountProcess.LinkedEntity.Csid) as IOrganizationUnit;
                        if (bankAccount != null)
                        {
                            BankAccountEntity bankAccountEntity = bankAccount.ClientEntity as BankAccountEntity;
                            bankAccountEntity.CashManagementTransactions.Clear();
                            bankAccountEntity.DeletedCashManagementTransactions.Clear();
                            bankAccountEntity.Holding = 0;

                            this.SaveData(bankAccount as IBrokerManagedComponent); 
                        }
                    }
                }
            }
        }

        protected void btnBankAccountID(object sender, EventArgs e)
        {
            IBrokerManagedComponent user = this.UMABroker.GetBMCInstance("Administrator", "DBUser_1_1");
            IOrganization iorgCM = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            object[] args = new Object[3] { iorgCM.CLID, user.CID, EnableSecurity.SecuritySetting };
            OrganisationListingDS orgDS = new OrganisationListingDS();
            DataView view = new DataView(this.UMABroker.GetBMCDetailList(OrganizationInstall.ASSEMBLY_NAME, args).Tables["Entities_Table"]);
            view.RowFilter = orgDS.GetRowFilter();
            view.Sort = "ENTITYNAME_FIELD ASC";
            DataTable orgFiteredTable = view.ToTable();
            foreach (DataRow row in orgFiteredTable.Rows)
            {
                Guid cid = new Guid(row["ENTITYCIID_FIELD"].ToString());
                IOrganizationUnit organizationUnit = this.UMABroker.GetBMCInstance(cid) as IOrganizationUnit;
                IClientUMAData clientUMAData = organizationUnit.ClientEntity as IClientUMAData;

                foreach (var bankAccountIdentity in clientUMAData.BankAccounts)
                {
                    IOrganizationUnit bankUnit = null;

                    if(bankAccountIdentity.Cid != Guid.Empty)
                        bankUnit = UMABroker.GetBMCInstance(bankAccountIdentity.Cid) as IOrganizationUnit; 

                    if(bankUnit == null)
                        bankUnit = UMABroker.GetCMImplementation(bankAccountIdentity.Clid, bankAccountIdentity.Csid) as IOrganizationUnit;

                    if (bankUnit != null)
                    {
                        BankAccountEntity bankEntity = bankUnit.ClientEntity as BankAccountEntity;
                        bankEntity.ParentCID = organizationUnit.CID;
                        SaveData(bankUnit as IBrokerManagedComponent); 
                    }
                }
            }
        }

        protected void btnSetDefaultProcess(object sender, EventArgs e)
        {
            IBrokerManagedComponent user = this.UMABroker.GetBMCInstance("Administrator", "DBUser_1_1");
            IOrganization iorgCM = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            object[] args = new Object[3] { iorgCM.CLID, user.CID, EnableSecurity.SecuritySetting };
            OrganisationListingDS orgDS = new OrganisationListingDS();
            DataView view = new DataView(this.UMABroker.GetBMCDetailList(OrganizationInstall.ASSEMBLY_NAME, args).Tables["Entities_Table"]);
            view.RowFilter = orgDS.GetRowFilter();
            view.Sort = "ENTITYNAME_FIELD ASC";
            DataTable orgFiteredTable = view.ToTable();
            foreach (DataRow row in orgFiteredTable.Rows)
            {
                UMABroker.SaveOverride = true;
                Guid cid = new Guid(row["ENTITYCIID_FIELD"].ToString());
                IOrganizationUnit organizationUnit = this.UMABroker.GetBMCInstance(cid) as IOrganizationUnit;
                organizationUnit.StatusType = StatusType.Active;
                organizationUnit.CalculateToken(true);
                UMABroker.SetComplete();
                UMABroker.SetStart();
            }
        }

        protected void btnFixBankCustNo(object sender, EventArgs e)
        {
            IBrokerManagedComponent user = this.UMABroker.GetBMCInstance(UMABroker.UserContext.Identity.Name, "DBUser_1_1");
            IOrganization iorgCM = this.UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            OrganisationListingDS orgDS = new OrganisationListingDS();
            orgDS.OrganisationListingOperationType = OrganisationListingOperationType.BankAccountsListWithoutFUM;
            iorgCM.GetData(orgDS);

            foreach (DataRow row in orgDS.Tables[OrganisationListingDS.BANKACCOUNTLISTTABLENAME].Rows)
            {
                Guid cid = new Guid(row["ENTITYCIID_FIELD"].ToString());
                IOrganizationUnit organizationUnit = this.UMABroker.GetBMCInstance(cid) as IOrganizationUnit;
                BankAccountEntity bankEntity = organizationUnit.ClientEntity as BankAccountEntity; 
                organizationUnit.OtherID = bankEntity.CustomerNo;
                this.SaveData(organizationUnit as IBrokerManagedComponent); 
            }
        }

        private void SearchClient()
        {
            IBrokerManagedComponent iBMC = UMABroker.GetWellKnownBMC(WellKnownCM.Organization);
            var ds = new OrganisationListingDS();

            if (searchFilter == "SearchByName")
                ds.OrganisationFilterType = OrganisationFilterType.ClientByName;
            else if (searchFilter == "SearchByID")
                ds.OrganisationFilterType = OrganisationFilterType.ClientByID;

            ds.OrganisationListingOperationType = OrganisationListingOperationType.ClientsSearch;
            if (!IsPostBack && string.IsNullOrEmpty(txtSearchBox.Text.Trim()))
                ds.ClientNameSearchFilter = "-9999-";
            else
                ds.ClientNameSearchFilter = txtSearchBox.Text.Trim();
            
            iBMC.GetData(ds);

            var entityView = new DataView(ds.Tables["Entities_Table"]) { RowFilter = "IsClient='true'", Sort = "ENTITYNAME_FIELD ASC" };
            DataTable entityTable = entityView.ToTable();

            var presentationDS = new DataSet();
            presentationDS.Tables.Add(entityTable);
            PopulatePage(presentationDS);
            UMABroker.ReleaseBrokerManagedComponent(iBMC);
        }

        protected void BtnSetTransaction(object sender, EventArgs e)
        {
            UMABroker.SaveOverride = true;
            IBrokerManagedComponent iBMC = UMABroker.GetWellKnownBMC(WellKnownCM.Organization);
            var ds = new OrganisationListingDS { OrganisationListingOperationType = OrganisationListingOperationType.SetBankTransactions };
            iBMC.GetData(ds);
            UMABroker.SetComplete();
            UMABroker.SetStart();
        }

        protected void BtnExportAdvisersClick(object sender, EventArgs e)
        {
            var orgCM = UMABroker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;

            var organisationListingDS = new OrganisationListingDS
                                            {
                                                OrganisationListingOperationType = OrganisationListingOperationType.AdvisersBasicList
                                            };
            if (orgCM != null) orgCM.GetData(organisationListingDS);

            var excelDataset = new DataSet();
            organisationListingDS.Tables["ADVISERLISTINGTABLE"].Columns.Remove("ENTITYCIID_FIELD");
            organisationListingDS.Tables["ADVISERLISTINGTABLE"].Columns.Remove("ENTITYCSID_FIELD");
            organisationListingDS.Tables["ADVISERLISTINGTABLE"].Columns.Remove("ENTITYSORT_FIELD");
            organisationListingDS.Tables["ADVISERLISTINGTABLE"].Columns.Remove("ENTITYCLID_FIELD");
            organisationListingDS.Tables["ADVISERLISTINGTABLE"].Columns.Remove("ENTITYCTID_FIELD");
            organisationListingDS.Tables["ADVISERLISTINGTABLE"].Columns.Remove("ENTITYSCTYPE_FIELD");
            organisationListingDS.Tables["ADVISERLISTINGTABLE"].Columns.Remove("ENTITYSCSTATUS_FIELD");

            organisationListingDS.Tables["ADVISERLISTINGTABLE"].Columns["ENTITYNAME_FIELD"].ColumnName = "Adviser Name";
            organisationListingDS.Tables["ADVISERLISTINGTABLE"].Columns["AdviserID"].ColumnName = "Adviser ID";
            organisationListingDS.Tables["ADVISERLISTINGTABLE"].Columns["IFAName"].ColumnName = "IFA Name";

            excelDataset.Merge(organisationListingDS.Tables["ADVISERLISTINGTABLE"], true, MissingSchemaAction.Add);
            ExcelHelper.ToExcel(excelDataset, "AdvisersListing-" + DateTime.Today.ToString("dd-MMM-yyyy") + ".xls", Page.Response);
            UMABroker.ReleaseBrokerManagedComponent(orgCM);
        }

        public override void PopulatePage(DataSet ds)
        {
            PresentationData = ds;
            PresentationGrid.DataSource = ds.Tables[0];
        }

        protected void PresentationGrid_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            SearchClient();
        }

        protected void PresentationGrid_OnItemCommand(object sender, GridCommandEventArgs e)
        {
            if (!(e.Item is GridDataItem)) return;

            var dataItem = (GridDataItem)e.Item;
            switch (e.CommandName.ToLower())
            {
                case "select":
                    e.Item.Edit = false;
                    e.Canceled = true;
                    var instanceId = dataItem["ENTITYCIID_FIELD"].Text;
                    Response.Redirect("ClientViews/ClientMainView.aspx?ins=" + instanceId);
                    break;
                case "delete":
                    e.Item.Edit = false;
                    e.Canceled = true;
                    var requestInstanceId = new Guid(dataItem["ENTITYCIID_FIELD"].Text);
                    UMABroker.SaveOverride = true;
                    UMABroker.DeleteCMInstance(requestInstanceId);
                    UMABroker.SetComplete();
                    UMABroker.SetStart();
                    PresentationGrid.Rebind();
                    break;
            }
        }
    }
}
