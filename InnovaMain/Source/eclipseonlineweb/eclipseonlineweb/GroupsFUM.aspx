﻿<%@ Page Title="e-Clipse Online Portal" EnableViewState="true" Language="C#" MasterPageFile="~/Home.master"
    AutoEventWireup="true" CodeBehind="GroupsFUM.aspx.cs" Inherits="eclipseonlineweb.GroupsFUM" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1Input"
    TagPrefix="c1" %>
<%@ Register TagPrefix="uc1" TagName="breadcrumb" Src="~/Controls/BreadCrumb.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <fieldset>
                <table width="100%">
                    <tr>
                        <td>
                            <c1:C1InputDate DateFormat="dd/MMM/yyyy" ID="C1FinancialSummaryDate" runat="server"
                                ShowTrigger="true">
                            </c1:C1InputDate>
                        </td>
                        <%-- <td width="60%">
                            <asp:RadioButtonList EnableViewState="true" runat="server" ID="rblServiceType" RepeatDirection="Horizontal">
                                <asp:ListItem Selected="True" Value="all" Text="All"></asp:ListItem>
                                <asp:ListItem Value="doitforme" Text="Do It For Me"></asp:ListItem>
                                <asp:ListItem Value="doitwithme" Text="Do It With Me"></asp:ListItem>
                                <asp:ListItem Value="doityourself" Text="Do It Yourself"></asp:ListItem>
                            </asp:RadioButtonList>
                            <wijmo:C1ComboBox Visible="false" ID="ServiceTypeCombo" runat="server">
                                <Items>
                                    <wijmo:C1ComboBoxItem Value="doitforme" Text="Do It For Me" />
                                    <wijmo:C1ComboBoxItem Value="doitwithme" Text="Do It With Me" />
                                    <wijmo:C1ComboBoxItem Value="doityourself" Text="Do It Yourself" />
                                    <wijmo:C1ComboBoxItem Value="all" Text="All" Selected="true" />
                                </Items>
                            </wijmo:C1ComboBox>
                        </td>--%>
                        <td>
                            <asp:ImageButton runat="server" ImageUrl="~/images/download.png" OnClick="DownloadXLS"
                                ID="btnDownload" />
                        </td>
                        <td>
                            <asp:ImageButton OnClick="GenerateAccountsFUM" runat="server" ID="ImageButton2" ImageUrl="~/images/database.png" />
                        </td>
                        <td width="100%" class="breadcrumbgap">
                            <uc1:breadcrumb ID="BreadCrumb1" runat="server" />
                            <br />
                            <asp:Label Font-Bold="true" runat="server" ID="lblDesc" Text="UMA Accounts - $FUM"></asp:Label>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br />
            <telerik:RadGrid ID="PresentationGrid" runat="server" ShowStatusBar="true" AutoGenerateColumns="False"
                PageSize="15" AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True"
                OnItemCommand="PresentationGrid_ItemCommand" GridLines="None" AllowAutomaticDeletes="True"
                AllowFilteringByColumn="true" AllowAutomaticInserts="True" AllowAutomaticUpdates="True"
                EnableViewState="true" ShowFooter="false" OnNeedDataSource="PresentationGrid_OnNeedDataSource">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                    Name="ActiveOrders" TableLayout="Fixed">
                    <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                    <HeaderStyle Font-Bold="True"></HeaderStyle>
                    <Columns>
                        <telerik:GridBoundColumn SortExpression="ENTITYCIID_FIELD" ReadOnly="true" HeaderText="ID"
                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                            HeaderButtonType="TextButton" DataField="ENTITYCIID_FIELD" UniqueName="ENTITYCIID_FIELD"
                            Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridTemplateColumn UniqueName="selec" HeaderText="" ShowFilterIcon="false"
                            AllowFiltering="false" HeaderStyle-Width="70px">
                            <ItemTemplate>
                                <asp:LinkButton ID="Definition" runat="server" Text="Select" CommandName="Select"
                                    CommandArgument='<%# Eval("ENTITYCIID_FIELD") %>'></asp:LinkButton>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn HeaderStyle-Width="120px" SortExpression="ClientID" ReadOnly="true"
                            HeaderText="Client ID" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="ClientID" UniqueName="ClientID">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderStyle-Width="200px" FilterControlWidth="160px" SortExpression="ClientType"
                            ReadOnly="true" HeaderText="Type" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="ClientType" UniqueName="ClientType">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterControlWidth="60%" SortExpression="ENTITYNAME_FIELD"
                            HeaderText="Account Name" ReadOnly="true" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="ENTITYNAME_FIELD"
                            UniqueName="ENTITYNAME_FIELD">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn ReadOnly="true" SortExpression="Holding" ShowFilterIcon="true"
                            HeaderText="Settled Holding ($)" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            HeaderButtonType="TextButton" DataField="Holding" UniqueName="Holding" DataFormatString="{0:C}"
                            Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn ReadOnly="true" SortExpression="Unsettled" ShowFilterIcon="true"
                            HeaderText="Unsettled Holding ($)" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                            HeaderButtonType="TextButton" DataField="Unsettled" UniqueName="Unsettled" DataFormatString="{0:C}"
                            Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderStyle-Width="200px" FilterControlWidth="160px" ReadOnly="true"
                            SortExpression="Total" ShowFilterIcon="true" HeaderText="Total Holding ($)" AutoPostBackOnFilter="true"
                            CurrentFilterFunction="Contains" HeaderButtonType="TextButton" DataField="Total"
                            UniqueName="Total" DataFormatString="{0:C}">
                            <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                        </telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
