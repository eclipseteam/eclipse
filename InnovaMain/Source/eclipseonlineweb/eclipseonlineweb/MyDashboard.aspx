﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MyDashboard.aspx.cs" Inherits="eclipseonlineweb.MyDashboard"
    MasterPageFile="~/Home.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        localStorage['MenuIndex'] = '0';
    </script>
    <style type="text/css">
        body
        {
            font-family:Arial,Helvetica,sans-serif;
        }
        h4 {
             font-family:Arial,Helvetica,sans-serif;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>
     <%--   <asp:TextBox runat="server" ID="txtLayoutID"/>
        <asp:Button ID="Button1" runat="server" Text="Test Layout" 
            onclick="Button1_Click"/> 
        <br />--%>
        <asp:Panel GroupingText="Main Dashboard" runat="server">
            <br />
            <jdash:ResourceManager ID="ResourceManager1" runat="server"  />
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <asp:Repeater runat="server" ID="modulesList" OnItemCommand="modulesList_ItemCommand">
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton1" runat="server" Text='<%# Eval("title") %>' CommandArgument='<%# Eval("id") %>'></asp:LinkButton>
                        </ItemTemplate>
                    </asp:Repeater>
                </ContentTemplate>
            </asp:UpdatePanel>
            <jdash:DashboardView ID="myDashboard" UserDesignMode="full" runat="server"
            Font-Names="Arial" />
        </asp:Panel>
    </div>
</asp:Content>
