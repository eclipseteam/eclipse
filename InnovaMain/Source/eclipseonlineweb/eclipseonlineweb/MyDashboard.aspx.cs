﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using JDash;
using JDash.Models;
using JDash.WebForms;

namespace eclipseonlineweb
{
    public partial class MyDashboard : UMABasePage
    {
        public override void LoadPage()
        {
            base.LoadPage();
            if (!Page.IsPostBack)
            {
                
                if (JDashManager.Provider.SearchDashboards().data != null
                    && JDashManager.Provider.SearchDashboards().data.Any())
                {
                    myDashboard.DashboardId = JDashManager.Provider.SearchDashboards().data.First().id;
                    myDashboard.DataBind();
                }

                // This is kept for creating dashlets
                //if (JDashManager.Provider.SearchDashletModules().data != null
                //    && JDashManager.Provider.SearchDashletModules().data.Any())
                //{
                //    modulesList.DataSource = JDashManager.Provider.SearchDashletModules().data;
                //    modulesList.DataBind();
                //}
            }
        }

        private DashletContext context;

        /// <summary>
        /// Called on initialization
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        [JEventHandler(JEvent.InitContext)]
        public void InitContext(object sender, JEventArgs args)
        {
            this.context = args.Event.Parameters.Get<DashletContext>("context");
           
        }

        protected void modulesList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            var moduleId = e.CommandArgument.ToString();
            var module = JDashManager.Provider.GetDashletModule(moduleId);
            var newDashlet = new DashletModel(module);
            myDashboard.CreateDashlet(newDashlet);
        }

        // This is kept for changing dashlets layout
        //protected void Button1_Click(object sender, EventArgs e)
        //{
        //    var layout = LayoutModel.GetPredefinedGridLayout(txtLayoutID.Text);
        //    myDashboard.ChangeLayout(layout);
        //}
    }
}