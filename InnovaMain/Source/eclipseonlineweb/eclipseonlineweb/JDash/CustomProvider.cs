﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JDash.SqlProvider;
using Oritax.TaxSimp.Utilities;

namespace eclipseonlineweb.JDash
{
    public class CustomProvider : Provider
    {
        public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config)
        {
            base.Initialize(name, config);
            ConnectionString += 
                String.Format("\"{0}\"", DBConnection.Connection.ConnectionString); 
               
            
        }

    }
}