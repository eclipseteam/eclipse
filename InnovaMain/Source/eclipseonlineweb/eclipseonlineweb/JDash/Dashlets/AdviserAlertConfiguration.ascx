﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdviserAlertConfiguration.ascx.cs" Inherits="eclipseonlineweb.JDash.Dashlets.AdviserAlertConfiguration" %>
<%@ Register TagName="AlertConfiguration" TagPrefix="uc" Src="~/JDash/Dashlets/AlertConfigurationControl.ascx" %>
<script type="text/javascript">

    function RequestStart() {
        document.getElementById('ShowOverlay').value = "False";
    }

    function RequestEnd() {
        document.getElementById('ShowOverlay').value = "True";
    }

</script>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server"> </telerik:RadAjaxLoadingPanel>
<telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" EnableAJAX="True" LoadingPanelID="RadAjaxLoadingPanel1" ClientEvents-OnRequestStart="RequestStart" ClientEvents-OnResponseEnd="RequestEnd">
<div id="dvSearch" runat="server">
<fieldset style="padding: 5px; margin-bottom: 5px;">
    <div style="text-align: right; padding-right: 5px; margin-bottom: 5px; display: none;">
        <span>Search Adviser: </span><telerik:RadTextBox DisplayText="Type in Adviser ID or Name"
        Height="32px" runat="server" ID="txtSearchBox" Width="450px">
        </telerik:RadTextBox>
        <asp:Button ID="BtnSearch" runat="server" CssClass="MediumButton" Text="Search" OnClick="BtnSearchClick" />
    </div>
    <div style="height: 190px;">
        <telerik:RadGrid ID="gd_Advisor" runat="server" AutoGenerateColumns="False" PageSize="20"
        AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="true" GridLines="None"
        AllowAutomaticInserts="false" AllowFilteringByColumn="true" EnableViewState="true" Width="100%" Height="100%"
        ShowFooter="false" OnNeedDataSource="gd_Advisor_OnNeedDataSource"  OnItemCommand="GdAdvisorItemCommand">
        <PagerStyle Mode="NumericPages"></PagerStyle>
        <ClientSettings>
            <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="true">
            </Scrolling>
        </ClientSettings>
        <MasterTableView AutoGenerateColumns="false" CommandItemDisplay="None" Width="100%">
            <Columns>
                <telerik:GridBoundColumn SortExpression="Cid" HeaderStyle-Width="5%"
                    ItemStyle-Width="5%" HeaderText="CID" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                    HeaderButtonType="TextButton" DataField="Cid" UniqueName="Cid" Display="false" FilterControlWidth="70%">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn SortExpression="Clid" HeaderStyle-Width="5%"
                    ItemStyle-Width="5%" HeaderText="CLID" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                    HeaderButtonType="TextButton" DataField="Clid" UniqueName="Clid" Visible="false" FilterControlWidth="70%">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn SortExpression="Csid" HeaderStyle-Width="5%"
                    ItemStyle-Width="5%" HeaderText="CSID" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                    HeaderButtonType="TextButton" DataField="Csid" UniqueName="Csid" Visible="false" FilterControlWidth="70%">
                </telerik:GridBoundColumn>
                <telerik:GridButtonColumn CommandName="select" Text="Select" ButtonType="LinkButton"
                    HeaderStyle-Width="2%">
                </telerik:GridButtonColumn>
                <telerik:GridBoundColumn SortExpression="ClientID" HeaderStyle-Width="5%"
                    ItemStyle-Width="5%" HeaderText="Client ID" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                    HeaderButtonType="TextButton" DataField="ClientID" UniqueName="ClientID" FilterControlWidth="70%">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderStyle-Width="15%" ItemStyle-Width="15%"
                    SortExpression="Name" HeaderText="Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                    HeaderButtonType="TextButton" DataField="Name" UniqueName="Name" FilterControlWidth="70%">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderStyle-Width="8%" ItemStyle-Width="8%"
                    SortExpression="Type" HeaderText="Type" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                    DataField="Type" UniqueName="Type" FilterControlWidth="70%">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderStyle-Width="5%" ItemStyle-Width="5%"
                    SortExpression="Status" HeaderText="Status" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                    HeaderButtonType="TextButton" DataField="Status" UniqueName="Status" FilterControlWidth="70%">
                </telerik:GridBoundColumn>
            </Columns>
        </MasterTableView>
    </telerik:RadGrid>
    </div>
</fieldset>
</div>

<div  id="dvAlerts" runat="server" Visible="False" style="padding-top:5px;">
<fieldset style="padding: 5px; margin-bottom: 5px;">
    <legend>Adviser Alert Configuration Settings</legend>
     <div>
        <asp:Label runat="server" ID="lblAdviserName" />
        <asp:Label runat="server" ID="lblAdviserCID" Visible="false" />
    </div>   
    <div style="text-align: center"><asp:Label runat="server" ID="lblError" EnableViewState="False"></asp:Label></div>   
    
    <div>
        <uc:AlertConfiguration runat="server" ID="AlertConfiguration1" />
    </div>
</fieldset>
</div>
</telerik:RadAjaxPanel>
