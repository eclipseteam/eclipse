﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.Security;
using Telerik.Web.UI;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.DataSets;
using DataSet = C1.C1Preview.DataBinding.DataSet;
using Oritax.TaxSimp.SM.Workflows;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.SM.Workflows.Data;
using System.Drawing;

namespace eclipseonlineweb.Controls
{
    public partial class AlertConfigurationControl : System.Web.UI.UserControl
    {      
        protected void Page_Load(object sender, EventArgs e)
        {
           
        }       
        
        /// <summary>
        /// Binds Alert Configuration data to the grid.
        /// Specially for other controls.
        /// </summary>
        public void BindGrid(Guid Cid,Guid parentCID=default(Guid))
        {
            IBrokerManagedComponent bmc = Broker.CreateTransientComponentInstance(new Guid("15DE2644-C1A0-41D5-8293-2794EF31840D"));
            var alertConfigurationDS = new AlertConfigurationDS();
            var user = (Page as UMABasePage).GetCurrentUser();
            
            //Assing Cid to label for saving purpose.
            lblAlertCid.Text = Cid.ToString();

            alertConfigurationDS.CID = new Guid(lblAlertCid.Text);
            alertConfigurationDS.ParentID = parentCID;           
            bmc.GetData(alertConfigurationDS);

            grdAlertConfig.DataSource = alertConfigurationDS.configurationPresentationTable;
            grdAlertConfig.DataBind();
        }
        /// <summary>
        /// Binds Alert Configuration data to the grid.
        /// Default binding, usually work for admin.
        /// </summary>
        public void BindGrid()
        {
            IBrokerManagedComponent bmc = Broker.CreateTransientComponentInstance(new Guid("15DE2644-C1A0-41D5-8293-2794EF31840D"));
            var alertConfigurationDS = new AlertConfigurationDS();
            var user = (Page as UMABasePage).GetCurrentUser();
            lblAlertCid.Text = lblAlertCid.Text == string.Empty ? Guid.Empty.ToString() : lblAlertCid.Text;
            alertConfigurationDS.CID = new Guid(lblAlertCid.Text);
            if (user.IsAdmin)
            {              
                alertConfigurationDS.ParentID = Guid.Empty;
            }
            if (user.UserType==(int)UserType.Advisor)
            {
                alertConfigurationDS.ParentID = Guid.Empty;
            }
            if (user.UserType==(int)UserType.Client)
            {
                //todo: get client parent id.
                //alertConfigurationDS.ParentID = Guid.Empty;
            }
            bmc.GetData(alertConfigurationDS);

            grdAlertConfig.DataSource = alertConfigurationDS.configurationPresentationTable;
        }
        protected void grdAlertConfig_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            BindGrid();
        }

        protected void grdAlertConfig_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                var gDataItem = e.Item as GridDataItem;
                for (int i = 1; i <= 4; i++)
                {
                    string contName = string.Format("ddlDurationType{0}", i);
                    var ddlDurationType = (DropDownList)gDataItem.FindControl(contName);
                    //Filling DropDown by Enums.
                    FillEnumDropDown(ddlDurationType, typeof(AlertDurationType));

                    contName = string.Format("lblDurationType{0}", i);
                    var lblDurationType = (Label)gDataItem.FindControl(contName);
                    ddlDurationType.SelectedValue = lblDurationType.Text;

                    contName = string.Format("ddlExecutionMode{0}", i);
                    var ddlExecutionMode = (DropDownList)gDataItem.FindControl(contName);
                    //Filling DropDown by Enums.
                    FillEnumDropDown(ddlExecutionMode, typeof(AlertExecutionMode));

                    contName = string.Format("lblExecutionMode{0}", i);
                    var lblExecutionMode = (Label)gDataItem.FindControl(contName);
                    ddlExecutionMode.SelectedValue = lblExecutionMode.Text;

                    contName = string.Format("txtReminderDuration{0}", i);
                    var txtReminderDuration = (RadNumericTextBox)gDataItem.FindControl(contName);

                    if (ddlExecutionMode.SelectedItem.Text == "OnDueDate")
                    {
                        txtReminderDuration.Enabled = false;
                        ddlDurationType.Enabled = false;
                        txtReminderDuration.Text = "0";
                    }
                }
            }
        }
        protected void DropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            var ddlExecutionMode = (DropDownList)sender;
            var gDataItem = (GridDataItem)ddlExecutionMode.NamingContainer;
            var contNumber = ddlExecutionMode.ID.Replace("ddlExecutionMode", "");



            var contName = string.Format("txtReminderDuration{0}", contNumber);
            var txtReminderDuration = (RadNumericTextBox)gDataItem.FindControl(contName);

            contName = string.Format("lblPreserveReminderDuration{0}", contNumber);
            var lblPreserveReminderDuration = (Label)gDataItem.FindControl(contName);

            contName = string.Format("ddlDurationType{0}", contNumber);
            var ddlDurationType = (DropDownList)gDataItem.FindControl(contName);

            if (ddlExecutionMode.SelectedItem.Text == "OnDueDate")
            {
                txtReminderDuration.Enabled = false;
                ddlDurationType.Enabled = false;
                lblPreserveReminderDuration.Text = txtReminderDuration.Text;
                txtReminderDuration.Text = "0";
            }
            else
            {
                txtReminderDuration.Enabled = true;
                ddlDurationType.Enabled = true;
                txtReminderDuration.Text = lblPreserveReminderDuration.Text;
            }

        }

        protected void btnSave_Onclick(object sender, EventArgs e)
        {
            try
            {
                if (UpdateConfiguration())
                    grdAlertConfig.MasterTableView.Rebind();
            }
            catch (Exception ex)
            {
                string strScript = ex.ToString().Replace("'", "");

                if (ex.InnerException != null)
                {
                    strScript = ex.InnerException.ToString().Replace("'", "");

                    if (ex.InnerException.ToString().Contains("dbo.AlertConfiguration"))
                    {
                        strScript =
                            "<span style=\"color:red;\">WARNING!! Alert Configuration table does not exist in the database.<br/>Configuration settings did not save.</span>";
                    }
                }
                lblError.Text = strScript;
            }

        }
        protected void lnkReset_Onclick(object sender, EventArgs e)
        {
            grdAlertConfig.Rebind();
        }


        private ICMBroker Broker
        {
            get
            {

                return (Page is UMABasePage) ? (Page as UMABasePage).UMABroker : null;
            }
        }

        public DataSet PresentationData
        {
            get;
            set;
        }
        public static string AlertCid { get; set; }
        #region Private Methods

        private void FillEnumDropDown(DropDownList ddl, Type enumType)
        {

            Array itemNames = System.Enum.GetNames(enumType);
            foreach (String name in itemNames)
            {
                //get the enum item value
                var value = (Int32)Enum.Parse(enumType, name);
                var listItem = new ListItem(name, value.ToString());
                ddl.Items.Add(listItem);
            }
        }

        private bool UpdateConfiguration()
        {
            var alertConfigurationDS = new AlertConfigurationDS();
            alertConfigurationDS.CommandType = DatasetCommandTypes.Update;

            var dtAlertConfiguration = alertConfigurationDS.Tables[alertConfigurationDS.configurationTable.TableName];

            var umaBasePage = Page as UMABasePage;
            if (umaBasePage != null)
            {
                var user = umaBasePage.GetCurrentUser();

                //GridDataItem gDataItem in grdAdminAlertConfig.Columns
                foreach (GridDataItem gDataItem in grdAlertConfig.Items)
                {
                    for (int i = 1; i <= 4; i++)
                    {
                        string contName = string.Format("lblConfigurationID{0}", i);
                        var lblConfigurationID = (Label)gDataItem.FindControl(contName);

                        contName = string.Format("lblWorkflowID{0}", i);
                        var lblWorkflowID = (Label)gDataItem.FindControl(contName);

                        contName = string.Format("lblCID{0}", i);
                        var lblCID = (Label)gDataItem.FindControl(contName);

                        contName = string.Format("txtReminderDuration{0}", i);
                        var txtReminderDuration = (RadNumericTextBox)gDataItem.FindControl(contName);

                        contName = string.Format("ddlDurationType{0}", i);
                        var ddlDurationType = (DropDownList)gDataItem.FindControl(contName);
                        //ddlDurationType.SelectedValue = lblDurationType1.Text;

                        contName = string.Format("ddlExecutionMode{0}", i);
                        var ddlExecutionMode = (DropDownList)gDataItem.FindControl(contName);

                        contName = string.Format("chkViewOnline{0}", i);
                        var chkViewOnline = (RadButton)gDataItem.FindControl(contName);

                        contName = string.Format("chkSendEmail{0}", i);
                        var chkSendEmail = (RadButton)gDataItem.FindControl(contName);

                        contName = string.Format("lblUserType{0}", i);
                        var lblUserType = (Label)gDataItem.FindControl(contName);
                        lblUserType.Text = lblUserType.Text == string.Empty ? user.UserType.ToString() : lblUserType.Text;

                        contName = string.Format("lblExecutionDays{0}", i);
                        var lblExecutionDays = (Label)gDataItem.FindControl(contName);

                        int reminderDuration = Convert.ToInt16(txtReminderDuration.Text);
                        var durationType = (AlertDurationType)Enum.Parse(typeof(AlertDurationType), ddlDurationType.SelectedValue, true);
                        var executionMode = (AlertExecutionMode)Enum.Parse(typeof(AlertExecutionMode), ddlExecutionMode.SelectedValue, true);

                        var executionDays = WebUtilities.Utilities.CalculateExecutionDays(reminderDuration, durationType, executionMode);

                        //DataTable Routine
                        var dr = dtAlertConfiguration.NewRow();
                        dr[alertConfigurationDS.configurationTable.CONFIGURATIONID] = lblConfigurationID.Text;
                        dr[alertConfigurationDS.configurationTable.WORKFLOWID] = lblWorkflowID.Text;
                        dr[alertConfigurationDS.configurationTable.CID] = lblAlertCid.Text; //AlertCid is CID set by Admin/Adviser/Client.
                        dr[alertConfigurationDS.configurationTable.REMINDERDURATION] = txtReminderDuration.Text;
                        dr[alertConfigurationDS.configurationTable.DURATIONTYPE] = ddlDurationType.SelectedValue;
                        dr[alertConfigurationDS.configurationTable.EXECUTIONMODE] = ddlExecutionMode.SelectedValue;
                        dr[alertConfigurationDS.configurationTable.VIEWONLINE] = chkViewOnline.Checked;
                        dr[alertConfigurationDS.configurationTable.SENDEMAIL] = chkSendEmail.Checked;
                        dr[alertConfigurationDS.configurationTable.USERTYPE] = lblUserType.Text;
                        dr[alertConfigurationDS.configurationTable.CREATEDON] = DateTime.Now;
                        dr[alertConfigurationDS.configurationTable.MODIFIEDON] = DateTime.Now;
                        dr[alertConfigurationDS.configurationTable.UPDATEBY] = user.CurrentUserName;
                        dr[alertConfigurationDS.configurationTable.EXECUTIONDAYS] = executionDays;
                        dtAlertConfiguration.Rows.Add(dr);

                        //Assigning execution days value to find the columns on duplications.
                        lblExecutionDays.Text = executionDays.ToString();
                    }
                }
            }

            if (!CheckDuplicateDurationExist(alertConfigurationDS))
            {
                SaveWorkflowData(alertConfigurationDS);
                return true;
            }
            else
            {
                var errorMessage =
                        "<span style=\"color:red;\">Reminders with unique periods are allowed. Please remove the duplication. | </span><a href=\"#\" onClick=\"ResetError();\">Dismiss</a><span>";                
                lblError.Text = errorMessage;                
                return false;
            }

        }

        private void SaveWorkflowData(System.Data.DataSet ds)
        {
            IBrokerManagedComponent bmc = Broker.CreateTransientComponentInstance(new Guid("15DE2644-C1A0-41D5-8293-2794EF31840D"));
            bmc.SetData(ds);
        }

        private bool CheckDuplicateDurationExist(AlertConfigurationDS alertConfigurationDS)
        {
            var configurationRows = alertConfigurationDS.configurationTable.AsEnumerable().GroupBy(r => r[alertConfigurationDS.configurationTable.EXECUTIONDAYS]).Where(gr => gr.Count() > 1);

            var enumerable = configurationRows as IGrouping<object, DataRow>[] ?? configurationRows.ToArray();
            foreach (var configurationRow in enumerable)
            {
                var firstOrDefault = configurationRow.FirstOrDefault();
                if (firstOrDefault != null)
                {
                    var executionDays = firstOrDefault[alertConfigurationDS.configurationTable.EXECUTIONDAYS];
                    IndicateDuplicateRadColumn(executionDays);
                }
            }



            return (enumerable.Any());



        }

        private void IndicateDuplicateRadColumn(object executionDays)
        {
            foreach (GridDataItem gDataItem in grdAlertConfig.Items)
            {
                for (var i = 1; i <= 4; i++)
                {
                    var contName = string.Format("lblExecutionDays{0}", i);
                    var lblExecutionDays = (Label)gDataItem.FindControl(contName);
                    if (lblExecutionDays.Text != executionDays.ToString()) continue;
                    contName = string.Format("txtReminderDuration{0}", i);
                    var txtReminderDuration = (RadNumericTextBox)gDataItem.FindControl(contName);

                    contName = string.Format("ddlDurationType{0}", i);
                    var ddlDurationType = (DropDownList)gDataItem.FindControl(contName);
                    //ddlDurationType.SelectedValue = lblDurationType1.Text;

                    contName = string.Format("ddlExecutionMode{0}", i);
                    var ddlExecutionMode = (DropDownList)gDataItem.FindControl(contName);

                    //Applying Color
                    txtReminderDuration.BorderColor = Color.Red;
                    ddlDurationType.BorderColor = Color.Red;
                    ddlExecutionMode.BorderColor = Color.Red;
                }
            }
        }

        #endregion
    }   
}