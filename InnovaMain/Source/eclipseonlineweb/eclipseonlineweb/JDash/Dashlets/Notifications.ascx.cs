﻿using System;
using System.Data;
using JDash.WebForms;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.SM.Workflows;
using Telerik.Web.UI;
using Oritax.TaxSimp.SM.Workflows.Data;

namespace eclipseonlineweb.JDash.Dashlets
{
    public partial class Notifications : System.Web.UI.UserControl
    {
       
        private DashletContext _context;

        protected string Cid = string.Empty;

        private ICMBroker Broker
        {
            get
            {
                return (Page is UMABasePage)? (Page as UMABasePage).UMABroker : null;
            }
        }

      
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        
        [JEventHandler(JEvent.InitContext)]
        public void InitContext(object sender, JEventArgs args)
        {
            _context = args.Event.Parameters.Get<DashletContext>("context");
        }

        public override void DataBind()
        {
            BindGrid();
            _context.RenderDashlet();
            base.DataBind();
        }

        /// <summary>
        /// Used on dashlet refresh by JDash Framework
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        [JEventHandler(JEvent.Refresh)]
        public void Refresh(object sender, JEventArgs args)
        {
            DataBind();
        }

        private void BindGrid()
        {
            IBrokerManagedComponent bmc = Broker.CreateTransientComponentInstance(new Guid("15DE2644-C1A0-41D5-8293-2794EF31840D"));
            var workflowsDS = new WorkflowsDS();
            bmc.GetData(workflowsDS);
            var view = new DataView(workflowsDS.Tables[WorkflowsDS.NotificationsTable])
                {
                    Sort = "CreatedDate DESC",
                    RowFilter = string.Format("NotificationType>{0}", (int) NotificationType.MDA)
                };
            var dtNotificaions = view.ToTable();
            grdNotification.DataSource = dtNotificaions;
            if (dtNotificaions.Rows.Count<=0)
            {
                grdNotification.Visible = false;
                lblMessage.Text = "No records to display.";
            }

        }

        /// <summary>
        /// Used before dashlet remove by JDash Framework
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        [JEventHandler(JEvent.PreRemove)]
        public void Remove(object sender, JEventArgs args)
        {
            args.Rejected = true;
        }

        protected void grdNotification_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            BindGrid();
        }
    }
}