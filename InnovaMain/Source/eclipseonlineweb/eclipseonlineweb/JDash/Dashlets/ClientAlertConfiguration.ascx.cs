﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.Security;
using Telerik.Web.UI;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.DataSets;
using System.Drawing;
using Oritax.TaxSimp.CM.Organization.Data;
using Oritax.TaxSimp.Data;



namespace eclipseonlineweb.JDash.Dashlets
{
    public partial class ClientAlertConfiguration : System.Web.UI.UserControl
    {
        #region Search Client
        protected void BtnSearchClick(object sender, EventArgs e)
        {
            PresentationGrid.Rebind();
        }
        private void SearchClient()
        {
            IBrokerManagedComponent iBMC = Broker.GetWellKnownBMC(WellKnownCM.Organization);
            var ds = new OrganisationListingDS();

           ds.OrganisationListingOperationType = OrganisationListingOperationType.ClientsSearch;
            if (!IsPostBack && string.IsNullOrEmpty(txtSearchBox.Text.Trim()))
                ds.ClientNameSearchFilter = "-9999-";
            else
                ds.ClientNameSearchFilter = txtSearchBox.Text.Trim();

            iBMC.GetData(ds);

            var entityView = new DataView(ds.Tables["Entities_Table"]) { RowFilter = "IsClient='true'", Sort = "ENTITYNAME_FIELD ASC" };
            DataTable entityTable = entityView.ToTable();

            var presentationDS = new DataSet();
            presentationDS.Tables.Add(entityTable);
            PopulatePage(presentationDS);
            Broker.ReleaseBrokerManagedComponent(iBMC);
        }
        public void PopulatePage(DataSet ds)
        {
            PresentationGrid.DataSource = ds.Tables[0];            
        }
        protected void PresentationGrid_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            SearchClient();
        }
        protected void PresentationGrid_OnItemCommand(object sender, GridCommandEventArgs e)
        {
            if (!(e.Item is GridDataItem)) return;

            var dataItem = (GridDataItem)e.Item;
            switch (e.CommandName.ToLower())
            {
                case "select":
                    dvAlerts.Visible = true;
                    var ClientCID = new Guid(dataItem["ENTITYCIID_FIELD"].Text);
                    var ParentCID = new Guid(dataItem["AdviserCID"].Text);
                    var ClientName = dataItem["ENTITYNAME_FIELD"].Text;
                    var ClientID = dataItem["ClientID"].Text;
                    
                     lblClientName.Text = string.Format("Selected Client: {0} ({1})", ClientName, ClientID);
                     lblClientCID.Text = ClientCID.ToString();

                    //Binding Configuration data in AlertConfiguration Control.
                     AlertConfiguration1.BindGrid(ClientCID, ParentCID);
                    break;                
            }
        }       
       #endregion

        private ICMBroker Broker
        {
            get
            {

                return (Page is UMABasePage) ? (Page as UMABasePage).UMABroker : null;
            }
        }

      
    }    
}