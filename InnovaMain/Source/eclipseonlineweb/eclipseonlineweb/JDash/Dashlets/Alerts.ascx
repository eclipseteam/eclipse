﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Alerts.ascx.cs" Inherits="eclipseonlineweb.JDash.Dashlets.Alerts" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60623.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<style type="text/css">
    .default
    {
        cursor: default;
    }
</style>
<script language="javascript" type="text/javascript">
    function ShowModalPopup(id) {
        window.$find(id).show();
    }

    function HideModalPop(id) {
        window.$find(id).hide();
    }




    function HideModalPopup() {
        window.$find("ModalBehaviourMDADate").hide();
        window.$find("ModalBehaviourAttach").hide();

        if (typeof window.Page_Validators != 'undefined') {
            for (var i = 0; i <= window.Page_Validators.length; i++) {
                if (window.Page_Validators[i] != null) {
                    window.ValidatorEnable(window.Page_Validators[i], false);
                }
            }
        };
    }
    function validateRadUpload(source, e) {
        e.IsValid = false;
        var upload = $find("<%= rauUploadMDA.ClientID %>");
        var inputs = upload.getUploadedFiles();
        for (var i = 0; i < inputs.length; i++) {
            //check for empty string or invalid extension     
            if (inputs[i].value != "" && upload.isExtensionValid(inputs[i].value)) {
                e.IsValid = true;
                break;
            }
        }
    };
</script>
<telerik:RadGrid ID="grdAlerts" runat="server" Width="100%" ShowStatusBar="False"
    Height="270px" AutoGenerateColumns="False" PageSize="10" AllowSorting="True"
    AllowMultiRowSelection="False" AllowPaging="True" GridLines="None" AllowAutomaticDeletes="True"
    AllowFilteringByColumn="True" AllowAutomaticInserts="True" AllowAutomaticUpdates="True"
    EnableViewState="True" ShowFooter="False" OnNeedDataSource="grdAlerts_OnNeedDataSource"
    OnItemCommand="grdAlerts_ItemCommand" OnItemDataBound="grdAlerts_ItemDataBound" OnItemCreated="grdAlerts_ItemCreated"
    OnPreRender="grdAlerts_PreRender">
    <PagerStyle Mode="NumericPages"></PagerStyle>
    <ClientSettings>
        <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="true">
        </Scrolling>
    </ClientSettings>
    <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
        Name="ClientList" TableLayout="Fixed">
        <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
        <CommandItemTemplate>
          <div style="text-align: right; padding:3px;" runat="server" id="dvConfiguration">
              <a href='<%= Page.ResolveClientUrl("~/AlertConfiguration.aspx")%>' title="Alert Configuration Settings">
                  <img src='<%= Page.ResolveClientUrl("~/images/alert_setting.jpg")%>' alt="Settings" width="18px" height="18px"/>
                  Alert Configuration
              
              </a>
          </div>
        </CommandItemTemplate>
        <HeaderStyle Font-Bold="True"></HeaderStyle>
        <Columns>
            <telerik:GridBoundColumn SortExpression="ActionID" ReadOnly="true" HeaderText="ActionID"
                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                HeaderButtonType="TextButton" DataField="ActionID" UniqueName="ActionID" Display="false">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn SortExpression="ClientCID" ReadOnly="true" HeaderText="ClientCID"
                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                HeaderButtonType="TextButton" DataField="ClientCID" UniqueName="ClientCID" Display="false">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn SortExpression="AttachmentID" ReadOnly="true" HeaderText="AttachmentID"
                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                HeaderButtonType="TextButton" DataField="AttachmentID" UniqueName="AttachmentID"
                Display="false">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn SortExpression="NotificationID" ReadOnly="true" HeaderText="NotificationID"
                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                HeaderButtonType="TextButton" DataField="NotificationID" UniqueName="NotificationID"
                Display="false">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn SortExpression="FileDownLoadURLDownload" ReadOnly="true"
                HeaderText="FileDownLoadURLDownload" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                ShowFilterIcon="true" HeaderButtonType="TextButton" DataField="FileDownLoadURLDownload"
                UniqueName="FileDownLoadURLDownload" Display="false">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn SortExpression="FileName" ReadOnly="True" HeaderText="FileName"
                AutoPostBackOnFilter="False" CurrentFilterFunction="Contains" DataField="FileName"
                UniqueName="FileName" Display="False" />
            <telerik:GridBoundColumn SortExpression="ActionTypeDetail" ReadOnly="true" HeaderText="ActionTypeDetail"
                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                HeaderButtonType="TextButton" DataField="ActionTypeDetail" UniqueName="ActionTypeDetail"
                Display="false">
            </telerik:GridBoundColumn>
            <telerik:GridTemplateColumn ReadOnly="true" HeaderText="Document" DataField="ClientID" HeaderStyle-Width="6%"
                Visible="false">
                <ItemTemplate>
                    <asp:ImageButton ID="iBtnGenerateDocument" runat="server" ToolTip="Download MDA Agreement"
                        AlternateText="Download" ImageUrl="~/images/pdf.png" CommandName="generate" Width="15px">
                    </asp:ImageButton>
                    <asp:ImageButton ID="iBtnEmailDocument" runat="server" ToolTip="Email MDA Agreement"
                        AlternateText="Email" ImageUrl="~/images/email_icon.png" CommandName="email" Width="15px"></asp:ImageButton>
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridHyperLinkColumn DataTextFormatString="{0}" DataNavigateUrlFields="ClientCID"
                SortExpression="ClientID" UniqueName="ClientID" DataNavigateUrlFormatString="~/ClientViews/ClientMainView.aspx?ins={0}"
                HeaderText="Client ID" DataTextField="ClientID" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                HeaderButtonType="TextButton" ShowFilterIcon="true" HeaderStyle-Width="6%" FilterControlWidth="70%"
                Display="False">
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridHyperLinkColumn>
            <telerik:GridHyperLinkColumn DataTextFormatString="{0}" DataNavigateUrlFields="ClientCID"
                SortExpression="ClientName" UniqueName="ClientName" DataNavigateUrlFormatString="~/ClientViews/ClientMainView.aspx?ins={0}"
                HeaderText="Client Name" DataTextField="ClientName" AutoPostBackOnFilter="true"
                CurrentFilterFunction="Contains" HeaderButtonType="TextButton" ShowFilterIcon="true"
                HeaderStyle-Width="13%" FilterControlWidth="70%">
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridHyperLinkColumn>
            <telerik:GridBoundColumn SortExpression="Adviser" ReadOnly="true" HeaderText="Adviser"
                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                HeaderButtonType="TextButton" DataField="AdviserName" UniqueName="AdviserName"
                HeaderStyle-Width="8%" FilterControlWidth="70%">
            </telerik:GridBoundColumn>
            <telerik:GridTemplateColumn ReadOnly="true" HeaderText="Event" DataField="Event"
                HeaderStyle-Width="10%" FilterControlWidth="70%">
                <ItemTemplate>
                    <asp:Label runat="server" ID="lblEvent" Text='<%# Eval("Event") %>'></asp:Label>
                    <asp:Label runat="server" ID="lblActionTypeDetail" Text='<%# Eval("ActionTypeDetail") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridTemplateColumn>
            <telerik:GridBoundColumn SortExpression="DueDate" ReadOnly="true" HeaderText="Due Date"
                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                HeaderButtonType="TextButton" DataField="DueDate" UniqueName="DueDate" DataFormatString="{0:dd/MM/yyyy}"
                HeaderStyle-Width="5%" FilterControlWidth="60%">
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn SortExpression="RaisedDate" ReadOnly="true" HeaderText="Raised On"
                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                HeaderButtonType="TextButton" DataField="RaisedDate" UniqueName="RaisedDate"
                DataFormatString="{0:dd/MM/yyyy H:mm:ss tt}" HeaderStyle-Width="10%" FilterControlWidth="70%">
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn SortExpression="Status" ReadOnly="true" HeaderText="Status"
                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                HeaderButtonType="TextButton" DataField="Status" UniqueName="Status" HeaderStyle-Width="5%"
                FilterControlWidth="60%">
                <ItemStyle HorizontalAlign="Left" />
            </telerik:GridBoundColumn>
            <telerik:GridTemplateColumn ReadOnly="true" HeaderText="Actions" DataField="Attached"
                AllowFiltering="false" HeaderStyle-Width="11%">
                <ItemTemplate>
                    <asp:LinkButton ID="btnGenerateDocument" runat="server" Text="Generate" ToolTip="Generate & download MDA agreement draft"
                        CommandName="generate"></asp:LinkButton>
                    <a id="hlinkDownload" runat="server" visible="false">Attached</a>
                    <asp:LinkButton ID="btnAttach" runat="server" Text="| Attach" ToolTip="Attach signed MDA agreement"
                        CommandName="upload"></asp:LinkButton>
                    <asp:LinkButton ID="btnApprove" runat="server" Text="| Complete" ToolTip="Mark the Alert complete"
                        Visible="False" CommandName="Approve" CommandArgument='<%#Eval("Status") %>'
                        ValidationGroup='<%#Eval("ActionID") %>' CausesValidation="False"></asp:LinkButton>
                    <asp:LinkButton ID="btnDisApprove" runat="server" Text="| Active" ToolTip="Make the Alert active again"
                        CommandName="Approve" CommandArgument='<%#Eval("Status") %>' Visible="False"></asp:LinkButton>
                    <asp:Label runat="server" ID="lblApprove" Text='<%# string.Format("{0} |",GetApproveText(Eval("Status"))) %>'
                        Visible="false"></asp:Label>
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn ReadOnly="False" HeaderText="Attachment" HeaderStyle-Width="6%"
                AllowFiltering="False">
                <ItemTemplate>
                    <asp:ImageButton runat="server" ID="ImgDownloadLink" ImageAlign="AbsMiddle"
                        CssClass="default" CommandName="download" />
                    <asp:LinkButton ID="btnDownload" runat="server" ToolTip="Download signed MDA agreement"
                        AlternateText="Attachment" Text="Download" CommandName="download" Visible="false"></asp:LinkButton>
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn UniqueName="MDAExecutionDate" ReadOnly="true" HeaderText="MDA Execution Date"
                AllowFiltering="False" HeaderStyle-Width="12%" Visible="False">
                <ItemTemplate>
                    <telerik:RadDatePicker ID="rdtpExecDate" runat="server" Width="110px" TimePopupButton="true">
                        <DateInput ID="DateInput1" runat="server" DateFormat="dd/MM/yyyy" ReadOnly="true"
                            DisplayDateFormat="dd/MM/yyyy">
                        </DateInput>
                    </telerik:RadDatePicker>
                    <asp:RequiredFieldValidator ID="rfvExecutionDate" runat="server" ControlToValidate="rdtpExecDate"
                        Text="Required" ForeColor="Red" ValidationGroup='<%#Eval("ActionID") %>' Display="Dynamic">
                    </asp:RequiredFieldValidator>
                </ItemTemplate>
            </telerik:GridTemplateColumn>
        </Columns>
    </MasterTableView>
</telerik:RadGrid>
<asp:Label runat="server" ID="lblShowPopupAttach" Style="display: none;" />
<asp:ModalPopupExtender runat="server" ID="popupUploadDoc" TargetControlID="lblShowPopupAttach"
    BackgroundCssClass="ModalPopupBG" Drag="False" PopupControlID="pnlUploadDoc"
    BehaviorID="ModalBehaviourAttach">
</asp:ModalPopupExtender>
<asp:Panel runat="server" ID="pnlUploadDoc" BackColor="White" Style="display: none">
    <div class="popup_Container">
        <div class="popup_Titlebar" id="popupHeaderUploadDoc">
            <div class="TitlebarLeft">
                <span style="font-family: Arial; font-size: 12px">Upload Signed MDA Agreement</span>
            </div>
            <div id="divCloseUploadDoc" class="TitlebarRight" onclick="HideModalPop('ModalBehaviourAttach');">
            </div>
        </div>
        <div class="popupBody" style="width: 100%;">
            <br />
            <table>
                <tr>
                    <td style="vert-align: top; width: 150px; font-family: Arial;">
                        <span style="font-family: Arial; font-size: 12px">Select File: </span>
                    </td>
                    <td width="250px">
                        <telerik:RadAsyncUpload ID="rauUploadMDA" runat="server" MultipleFileSelection="Disabled"
                            MaxFileInputsCount="1" />
                    </td>
                </tr>
                <tr>
                    <td width="150px">
                        <span style="font-family: Arial; font-size: 12px">File Description & Type: </span>
                        <asp:Label runat="server" ID="lblAttachClientID" Visible="False"></asp:Label>
                        <asp:Label runat="server" ID="lblAttachNotificationID" Visible="False"></asp:Label>
                    </td>
                    <td width="250px">
                        <telerik:RadTextBox runat="server" ID="txtDescription" Text="MDA Renewed" ReadOnly="true"
                            Enabled="False" Width="133px">
                        </telerik:RadTextBox>
                        <telerik:RadButton runat="server" ID="btnAttacheDoc" Text="Upload" OnClick="btnAttacheDoc_Click"
                            ValidationGroup="Uploader">
                        </telerik:RadButton>
                        <br />
                    </td>
                </tr>
                <tr>
                    <td width="150px">
                    </td>
                    <td width="250px" style="text-align: left;">
                        <br />
                        <telerik:RadButton runat="server" ID="btnCancelUpLoadMDA" Text="Cancel" OnClientClicked="HideModalPop('ModalBehaviourAttach');"
                            Visible="False">
                        </telerik:RadButton>
                        <asp:CustomValidator ID="CustomValidator1" ValidationGroup="Uploader" runat="server"
                            ForeColor="Red" ClientValidationFunction="validateRadUpload" ErrorMessage="Please select file"></asp:CustomValidator>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Panel>
<asp:Label runat="server" ID="lblShowPopupMDADate" Style="display: none;" />
<asp:ModalPopupExtender ID="popupTradeDate" runat="server" TargetControlID="lblShowPopupMDADate"
    PopupControlID="pnlTradeDate" PopupDragHandleControlID="PopupHeader1" Drag="true"
    BackgroundCssClass="ModalPopupBG" BehaviorID="ModalBehaviourMDADate">
</asp:ModalPopupExtender>
<asp:Panel ID="pnlTradeDate" runat="server" BackColor="White" Style="display: none">
    <div class="popup_Container">
        <div class="popup_Titlebar" id="Div2">
            <div class="TitlebarLeft">
                <span style="font-family: Arial; font-size: 12px">MDA Execution Date </span>
            </div>
            <div class="TitlebarRight" onclick="HideModalPop('ModalBehaviourMDADate');">
            </div>
        </div>
        <div class="PopupBody">
            <table>
                <tr>
                    <td>
                        <span style="font-family: Arial; font-size: 12px">Execution Date:</span>
                        <asp:Label runat="server" ID="lblMDAClientID" Visible="False"></asp:Label>
                        <asp:Label runat="server" ID="lblActionID" Visible="False"></asp:Label>
                    </td>
                    <td>
                        <telerik:RadDatePicker ID="dtMDADate" runat="server" Calendar-RangeMinDate="1/01/2000 12:00:00 AM"
                            DateInput-MinDate="1/01/2000 12:00:00 AM" MinDate="1/01/1900 12:00:00 AM" ClientIDMode="AutoID"
                            ZIndex="10000000">
                            <DateInput DateFormat="dd/MM/yyyy" ReadOnly="true" DisplayDateFormat="dd/MM/yyyy">
                            </DateInput>
                        </telerik:RadDatePicker>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="right">
                        <telerik:RadButton runat="server" ID="btnMDADateComplete" OnClick="btnMDADateComplete_OnClick"
                            Text="Complete">
                        </telerik:RadButton>
                        &nbsp;
                        <telerik:RadButton runat="server" ID="btnCancel" Text="Cancel" OnClientClicked="HideModalPop('ModalBehaviourMDADate');"
                            Visible="False">
                        </telerik:RadButton>
                    </td>
                </tr>
                <br />
            </table>
        </div>
    </div>
</asp:Panel>
