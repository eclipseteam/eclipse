﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AlertConfigurationControl.ascx.cs" Inherits="eclipseonlineweb.Controls.AlertConfigurationControl" %>

<script type="text/javascript">
    function ResetError() {        
        $('.error').text("");        
    }

</script>
<style type="text/css">
    .error {color:red;}
</style>
<div style="margin: 5px;">
        <telerik:RadButton runat="server" ID="RadButton1" ToolTip="Save Changes" OnClick="btnSave_Onclick"
        Width="70px" Height="20px">
        <ContentTemplate>
            <img src='<%= Page.ResolveClientUrl("~/images/Save-Icon.png")%>' alt="" class="btnImageWithText" />
            <span class="riLabel">Save</span>
        </ContentTemplate>
    </telerik:RadButton>
    <asp:Label runat="server" ID="lblAlertCid" Visible="false"></asp:Label>
</div>
<div style="text-align: center">
    <asp:Label runat="server" ID="lblError" CssClass="error" EnableViewState="False">
                
    </asp:Label>       
</div>

<telerik:RadGrid runat="server" ID="grdAlertConfig" Width="99%" ShowStatusBar="False"
    Height="100px" AutoGenerateColumns="False" PageSize="10" AllowSorting="True"
    AllowMultiRowSelection="False" AllowPaging="False" GridLines="None" AllowAutomaticDeletes="True"
    AllowFilteringByColumn="False" AllowAutomaticInserts="false" AllowAutomaticUpdates="True" 
    EnableViewState="True" ShowFooter="False" OnNeedDataSource="grdAlertConfig_OnNeedDataSource"
    OnItemDataBound="grdAlertConfig_ItemDataBound" >
    <ClientSettings>
        <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="true">
        </Scrolling>
    </ClientSettings>
    <MasterTableView AllowMultiColumnSorting="false" Width="100%" CommandItemDisplay="Top" 
        Name="ReminderList" TableLayout="Fixed">
         <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
        <HeaderStyle Font-Bold="True"></HeaderStyle>
        <Columns>
            <telerik:GridTemplateColumn ReadOnly="true" UniqueName="Reminder1" HeaderText="Reminder 1" DataField="Description1" AllowFiltering="False">
                <ItemTemplate>
                    <asp:Label runat="server" ID="lblConfigurationID1" Text='<%# Eval("ConfigurationID1") %>' Visible="False"></asp:Label>
                    <asp:Label runat="server" ID="lblWorkflowID1" Text='<%# Eval("workflowID1") %>'  Visible="False"></asp:Label>
                    <asp:Label runat="server" ID="lblCID1" Text='<%# Eval("CID1") %>'  Visible="false"></asp:Label>                    
                    <telerik:RadNumericTextBox runat="server" ID="txtReminderDuration1" MinValue="0" MaxValue="30" ShowSpinButtons="True"
                        EnableViewState="true" Value='<%# Convert.ToInt16(Eval("ReminderDuration1").ToString()) %>' Width="50px">
                        <NumberFormat GroupSeparator="" DecimalDigits="0"  />
                   </telerik:RadNumericTextBox>
                   <asp:Label runat="server" ID="lblPreserveReminderDuration1" Text='<%# Eval("ReminderDuration1").ToString() %>' Visible="False" ></asp:Label>
                    <asp:Label runat="server" ID="lblDurationType1" Text='<%# Eval("DurationType1") %>' Visible="False"></asp:Label>
                    <asp:DropDownList runat="server" ID="ddlDurationType1"/>
                    
                    <asp:Label runat="server" ID="lblExecutionMode1" Text='<%# Eval("ExecutionMode1") %>' Visible="False"></asp:Label>
                    <asp:DropDownList runat="server" ID="ddlExecutionMode1" AutoPostBack="True" OnSelectedIndexChanged="DropDownList_SelectedIndexChanged" />
                    <br/>
                    <telerik:RadButton ID="chkViewOnline1" runat="server" AutoPostBack="false" Value="2" 
                        ToggleType="CheckBox" ButtonType="ToggleButton"
                        Text="View Online" Checked='<%# Eval("ViewOnline1") %>' GroupName="filters">
                    </telerik:RadButton>
                    <telerik:RadButton ID="chkSendEmail1" runat="server" AutoPostBack="false" Value="2" 
                        ToggleType="CheckBox" ButtonType="ToggleButton"
                        Text="Send Email" Checked='<%# Eval("SendEmail1") %>' GroupName="filters">
                    </telerik:RadButton>
                    <br/>

                    <asp:Label runat="server" ID="lblUserType1" Text='<%# Eval("UserType1") %>' Visible="False"></asp:Label>
                    <asp:Label runat="server" ID="lblCreatedOn1" Text='<%# Eval("CreatedOn1") %>' Visible="False"></asp:Label>
                    <asp:Label runat="server" ID="ModifiedOn1" Text='<%# Eval("ModifiedOn1") %>' Visible="False"></asp:Label>
                    <asp:Label runat="server" ID="lblUpdateBy1" Text='<%# Eval("UpdateBy1") %>' Visible="False"></asp:Label>
                    <asp:Label runat="server" ID="lblExecutionDays1" Visible="False"></asp:Label>
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn ReadOnly="true" UniqueName="Reminder2" HeaderText="Reminder 2" DataField="Description2" AllowFiltering="False">
                <ItemTemplate>
                    <asp:Label runat="server" ID="lblConfigurationID2" Text='<%# Eval("ConfigurationID2") %>' Visible="False"></asp:Label>
                    <asp:Label runat="server" ID="lblWorkflowID2" Text='<%# Eval("workflowID2") %>'  Visible="False"></asp:Label>
                    <asp:Label runat="server" ID="lblCID2" Text='<%# Eval("CID2") %>'  Visible="False"></asp:Label>
                    <telerik:RadNumericTextBox runat="server" ID="txtReminderDuration2" MinValue="0" MaxValue="30" ShowSpinButtons="True"
                        EnableViewState="true" Value='<%# Convert.ToInt16(Eval("ReminderDuration2").ToString()) %>' Width="50px">
                        <NumberFormat GroupSeparator="" DecimalDigits="0"  />
                   </telerik:RadNumericTextBox>
                   <asp:Label runat="server" ID="lblPreserveReminderDuration2"  Text='<%# Eval("ReminderDuration2").ToString() %>' Visible="False" ></asp:Label>
                    <asp:Label runat="server" ID="lblDurationType2" Text='<%# Eval("DurationType2") %>' Visible="False"></asp:Label>
                    <asp:DropDownList runat="server" ID="ddlDurationType2"/>
                    
                    <asp:Label runat="server" ID="lblExecutionMode2" Text='<%# Eval("ExecutionMode2") %>' Visible="False"></asp:Label>
                    <asp:DropDownList runat="server" ID="ddlExecutionMode2" AutoPostBack="True" OnSelectedIndexChanged="DropDownList_SelectedIndexChanged"/>
                    <br/>
                     <telerik:RadButton ID="chkViewOnline2" runat="server" AutoPostBack="false" Value="2" 
                        ToggleType="CheckBox" ButtonType="ToggleButton"
                        Text="View Online" Checked='<%# Eval("ViewOnline2") %>' GroupName="filters">
                    </telerik:RadButton>
                    <telerik:RadButton ID="chkSendEmail2" runat="server" AutoPostBack="false" Value="2" 
                        ToggleType="CheckBox" ButtonType="ToggleButton"
                        Text="Send Email" Checked='<%# Eval("SendEmail2") %>' GroupName="filters">
                    </telerik:RadButton>
                    <br/>

                    <asp:Label runat="server" ID="lblUserType2" Text='<%# Eval("UserType2") %>' Visible="False"></asp:Label>
                    <asp:Label runat="server" ID="lblCreatedOn2" Text='<%# Eval("CreatedOn2") %>' Visible="False"></asp:Label>
                    <asp:Label runat="server" ID="ModifiedOn2" Text='<%# Eval("ModifiedOn2") %>' Visible="False"></asp:Label>
                    <asp:Label runat="server" ID="lblUpdateBy2" Text='<%# Eval("UpdateBy2") %>' Visible="False"></asp:Label>
                    <asp:Label runat="server" ID="lblExecutionDays2" Visible="False"></asp:Label>
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn ReadOnly="true" UniqueName="Reminder3" HeaderText="Reminder 3" DataField="Description3" AllowFiltering="False">
                <ItemTemplate>
                    <asp:Label runat="server" ID="lblConfigurationID3" Text='<%# Eval("ConfigurationID3") %>' Visible="False"></asp:Label>
                    <asp:Label runat="server" ID="lblWorkflowID3" Text='<%# Eval("workflowID3") %>'  Visible="False"></asp:Label>
                    <asp:Label runat="server" ID="lblCID3" Text='<%# Eval("CID3") %>'  Visible="False"></asp:Label>
                    <telerik:RadNumericTextBox runat="server" ID="txtReminderDuration3" MinValue="0" MaxValue="30" ShowSpinButtons="True"
                        EnableViewState="true" Value='<%# Convert.ToInt16(Eval("ReminderDuration3").ToString()) %>' Width="50px">
                        <NumberFormat GroupSeparator="" DecimalDigits="0"  />
                   </telerik:RadNumericTextBox>
                   <asp:Label runat="server" ID="lblPreserveReminderDuration3"  Text='<%# Eval("ReminderDuration3").ToString() %>'  Visible="False"></asp:Label>
                    <asp:Label runat="server" ID="lblDurationType3" Text='<%# Eval("DurationType3") %>' Visible="False"></asp:Label>
                    <asp:DropDownList runat="server" ID="ddlDurationType3"/>
                    
                    <asp:Label runat="server" ID="lblExecutionMode3" Text='<%# Eval("ExecutionMode3") %>' Visible="False"></asp:Label>
                    <asp:DropDownList runat="server" ID="ddlExecutionMode3" AutoPostBack="True" OnSelectedIndexChanged="DropDownList_SelectedIndexChanged"/>
                    <br/>
                     <telerik:RadButton ID="chkViewOnline3" runat="server" AutoPostBack="false" Value="2" 
                        ToggleType="CheckBox" ButtonType="ToggleButton"
                        Text="View Online" Checked='<%# Eval("ViewOnline3") %>' GroupName="filters">
                    </telerik:RadButton>
                    <telerik:RadButton ID="chkSendEmail3" runat="server" AutoPostBack="false" Value="2" 
                        ToggleType="CheckBox" ButtonType="ToggleButton"
                        Text="Send Email" Checked='<%# Eval("SendEmail3") %>' GroupName="filters">
                    </telerik:RadButton>
                    <br/>

                    <asp:Label runat="server" ID="lblUserType3" Text='<%# Eval("UserType3") %>' Visible="False"></asp:Label>
                    <asp:Label runat="server" ID="lblCreatedOn3" Text='<%# Eval("CreatedOn3") %>' Visible="False"></asp:Label>
                    <asp:Label runat="server" ID="ModifiedOn3" Text='<%# Eval("ModifiedOn3") %>' Visible="False"></asp:Label>
                    <asp:Label runat="server" ID="lblUpdateBy3" Text='<%# Eval("UpdateBy3") %>' Visible="False"></asp:Label>
                    <asp:Label runat="server" ID="lblExecutionDays3" Visible="False"></asp:Label>
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn ReadOnly="true" UniqueName="Reminder4" HeaderText="Reminder 4" DataField="Description4" AllowFiltering="False">
                <ItemTemplate>
                    <asp:Label runat="server" ID="lblConfigurationID4" Text='<%# Eval("ConfigurationID4") %>' Visible="False"></asp:Label>
                    <asp:Label runat="server" ID="lblWorkflowID4" Text='<%# Eval("workflowID4") %>'  Visible="False"></asp:Label>
                    <asp:Label runat="server" ID="lblCID4" Text='<%# Eval("CID4") %>'  Visible="False"></asp:Label>
                    <telerik:RadNumericTextBox runat="server" ID="txtReminderDuration4" MinValue="0" MaxValue="30" ShowSpinButtons="True"
                        EnableViewState="true" Value='<%# Convert.ToInt16(Eval("ReminderDuration4").ToString()) %>' Width="50px">
                        <NumberFormat GroupSeparator="" DecimalDigits="0"  />
                   </telerik:RadNumericTextBox>
                   <asp:Label runat="server" ID="lblPreserveReminderDuration4"  Text='<%# Eval("ReminderDuration4").ToString() %>'  Visible="False"></asp:Label>
                    <asp:Label runat="server" ID="lblDurationType4" Text='<%# Eval("DurationType4") %>' Visible="False"></asp:Label>
                    <asp:DropDownList runat="server" ID="ddlDurationType4"/>
                    
                    <asp:Label runat="server" ID="lblExecutionMode4" Text='<%# Eval("ExecutionMode4") %>' Visible="False"></asp:Label>
                    <asp:DropDownList runat="server" ID="ddlExecutionMode4" AutoPostBack="True" OnSelectedIndexChanged="DropDownList_SelectedIndexChanged"/>
                    <br/>
                     <telerik:RadButton ID="chkViewOnline4" runat="server" AutoPostBack="false" Value="2" 
                        ToggleType="CheckBox" ButtonType="ToggleButton"
                        Text="View Online" Checked='<%# Eval("ViewOnline4") %>' GroupName="filters">
                    </telerik:RadButton>
                    <telerik:RadButton ID="chkSendEmail4" runat="server" AutoPostBack="false" Value="2" 
                        ToggleType="CheckBox" ButtonType="ToggleButton"
                        Text="Send Email" Checked='<%# Eval("SendEmail4") %>' GroupName="filters">
                    </telerik:RadButton>
                    <br/>

                    <asp:Label runat="server" ID="lblUserType4" Text='<%# Eval("UserType4") %>' Visible="False"></asp:Label>
                    <asp:Label runat="server" ID="lblCreatedOn4" Text='<%# Eval("CreatedOn4") %>' Visible="False"></asp:Label>
                    <asp:Label runat="server" ID="ModifiedOn4" Text='<%# Eval("ModifiedOn4") %>' Visible="False"></asp:Label>
                    <asp:Label runat="server" ID="lblUpdateBy4" Text='<%# Eval("UpdateBy4") %>' Visible="False"></asp:Label>
                    <asp:Label runat="server" ID="lblExecutionDays4" Visible="False"></asp:Label>
                </ItemTemplate>
            </telerik:GridTemplateColumn>
        </Columns>
    </MasterTableView>
</telerik:RadGrid>
