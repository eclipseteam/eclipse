﻿using System;
using JDash.WebForms;
using Oritax.TaxSimp.CalculationInterface;
using Telerik.Web.UI;
using Oritax.TaxSimp.DataSets;
using System.Data;
using Telerik.Web.UI.HtmlChart;

namespace eclipseonlineweb.JDash.Dashlets
{
    public partial class BestRates : System.Web.UI.UserControl
    {
        private ICMBroker Broker
        {
            get
            {
                return ((UMABasePage) Page).UMABroker;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {
                GetTDRates();
            }
        }

        private DashletContext _context;

        [JEventHandler(JEvent.InitContext)]
        public void InitContext(object sender, JEventArgs args)
        {
            _context = args.Event.Parameters.Get<DashletContext>("context");
        }

        public override void DataBind()
        {
            _context.RenderDashlet();
            base.DataBind();
        }

        [JEventHandler(JEvent.Refresh)]
        public void Refresh(object sender, JEventArgs args)
        {
            DataBind();
        }

        [JEventHandler(JEvent.PreRemove)]
        public void Remove(object sender, JEventArgs args)
        {
            args.Rejected = true;

        }

        #region Best Rates

        protected void GridTDRates_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GetTDRates();
        }

        private void GetTDRates()
        {
            IBrokerManagedComponent iBmc = Broker.GetWellKnownBMC(WellKnownCM.Organization);
            var ds = new OrderPadRatesDS();
            iBmc.GetData(ds);
            Broker.ReleaseBrokerManagedComponent(iBmc);

            //DataTable dt = GetMaxRatesTable_TD(ds.Tables[OrderPadRatesDS.ORDERPADMAXRATESTABLE]);
            DataTable dt = GetMaxRatesTable_TD(ds);
            GridTDRates.DataSource = dt;
            ChartTDRate.DataSource = dt;
            ChartTDRate.DataBind();
        }

        private DataTable GetMaxRatesTable_TD(OrderPadRatesDS ds)
        {
            DataTable pdt = ds.Tables[OrderPadRatesDS.ORDERPADMAXRATESTABLE];
            DataTable pdtAll = ds.Tables[OrderPadRatesDS.ORDERPADRATESTABLE];

            var dt = new DataTable();

            const string durationCol = "Duration";
            const string rateCol = "Rate";
            const string chartrateCol = "ChartRate";
            const string tooltipCol = "TooltipData";

            dt.Columns.Add(durationCol, typeof(string));
            dt.Columns.Add(rateCol, typeof(decimal));
            dt.Columns.Add(chartrateCol, typeof(decimal));
            dt.Columns.Add(tooltipCol, typeof(string));

            if (pdt.Rows.Count > 0)
            {
                GetRateRow_TD(dt, durationCol, "30-D", rateCol, chartrateCol, tooltipCol, pdt.Rows[0][OrderPadRatesDS.DAYS30], pdtAll, OrderPadRatesDS.DAYS30);
                GetRateRow_TD(dt, durationCol, "60-D", rateCol, chartrateCol, tooltipCol, pdt.Rows[0][OrderPadRatesDS.DAYS60], pdtAll, OrderPadRatesDS.DAYS60);
                GetRateRow_TD(dt, durationCol, "90-D", rateCol, chartrateCol, tooltipCol, pdt.Rows[0][OrderPadRatesDS.DAYS90], pdtAll, OrderPadRatesDS.DAYS90);
                GetRateRow_TD(dt, durationCol, "120-D", rateCol, chartrateCol, tooltipCol, pdt.Rows[0][OrderPadRatesDS.DAYS120], pdtAll, OrderPadRatesDS.DAYS120);
                GetRateRow_TD(dt, durationCol, "150-D", rateCol, chartrateCol, tooltipCol, pdt.Rows[0][OrderPadRatesDS.DAYS150], pdtAll, OrderPadRatesDS.DAYS150);
                GetRateRow_TD(dt, durationCol, "180-D", rateCol, chartrateCol, tooltipCol, pdt.Rows[0][OrderPadRatesDS.DAYS180], pdtAll, OrderPadRatesDS.DAYS180);
                GetRateRow_TD(dt, durationCol, "270-D", rateCol, chartrateCol, tooltipCol, pdt.Rows[0][OrderPadRatesDS.DAYS270], pdtAll, OrderPadRatesDS.DAYS270);
                GetRateRow_TD(dt, durationCol, "Y-1", rateCol, chartrateCol, tooltipCol, pdt.Rows[0][OrderPadRatesDS.YEARS1], pdtAll, OrderPadRatesDS.YEARS1);
                GetRateRow_TD(dt, durationCol, "Y-2", rateCol, chartrateCol, tooltipCol, pdt.Rows[0][OrderPadRatesDS.YEARS2], pdtAll, OrderPadRatesDS.YEARS2);
                GetRateRow_TD(dt, durationCol, "Y-3", rateCol, chartrateCol, tooltipCol, pdt.Rows[0][OrderPadRatesDS.YEARS3], pdtAll, OrderPadRatesDS.YEARS3);
                GetRateRow_TD(dt, durationCol, "Y-4", rateCol, chartrateCol, tooltipCol, pdt.Rows[0][OrderPadRatesDS.YEARS4], pdtAll, OrderPadRatesDS.YEARS4);
                GetRateRow_TD(dt, durationCol, "Y-5", rateCol, chartrateCol, tooltipCol, pdt.Rows[0][OrderPadRatesDS.YEARS5], pdtAll, OrderPadRatesDS.YEARS5);
            }

            return dt;
        }

        private void GetRateRow_TD(DataTable pDt, string pDurationCol, string pDuration, string pRateCol, string chartRateCol,string ptooltipCol, object pRate, DataTable pDtAll, string rateColAll)
        {
            var row = pDt.NewRow();
            row[pDurationCol] = pDuration;
            if (pRate != DBNull.Value)
                row[pRateCol] = ((decimal)pRate);

            if (pRate != DBNull.Value)
                row[chartRateCol] = ((decimal)pRate * 100);


            DataView dv = pDtAll.DefaultView;
            const string instName = "PriceInstituteName";
            dv.Sort = instName;
            string s = rateColAll + " = " + pRate;
            dv.RowFilter = s;
            DataTable distinctValues = dv.ToTable(true, instName);
            DataView dv2 = distinctValues.DefaultView;

            string str = string.Empty;
            for (int i = 0; i < dv2.Count; i++)
            {
                if (str == string.Empty)
                {
                    str += "&nbsp" + dv2[i][instName] + "&nbsp";
                }
                else
                {
                    str += "<br>" + "&nbsp" + dv2[i][instName] + "&nbsp";
                }
                
            }

            row[ptooltipCol] = str;

            pDt.Rows.Add(row);

        }

        protected void GridAtCallRates_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            //GridAtCallRates.DataSource = new DataTable();
        }

        #endregion

    }
}