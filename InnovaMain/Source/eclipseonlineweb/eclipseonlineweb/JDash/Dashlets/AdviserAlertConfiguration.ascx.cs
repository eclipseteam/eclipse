﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.Security;
using Telerik.Web.UI;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.DataSets;
using DataSet = C1.C1Preview.DataBinding.DataSet;
using Oritax.TaxSimp.SM.Workflows;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.SM.Workflows.Data;
using System.Drawing;
using Oritax.TaxSimp.Commands;


namespace eclipseonlineweb.JDash.Dashlets
{
    public partial class AdviserAlertConfiguration : System.Web.UI.UserControl
    {
        private static string _adviserSearch=string.Empty;
        #region Search Adviser
        protected void BtnSearchClick(object sender, EventArgs e)
        {
            _adviserSearch = txtSearchBox.Text;

            gd_Advisor.DataSource = BindAdviserGrid(_adviserSearch);
            gd_Advisor.Rebind();
        }
        protected void gd_Advisor_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            gd_Advisor.DataSource = BindAdviserGrid(_adviserSearch);

        }
        protected void GdAdvisorItemCommand(object sender, GridCommandEventArgs e)
        {
            if (!(e.Item is GridDataItem)) return;

            var dataItem = (GridDataItem)e.Item;
            var adviserCid =new Guid(dataItem["Cid"].Text);
            switch (e.CommandName.ToLower())
            {

                case "select":
                    dvAlerts.Visible = true;
                    var adviserID = dataItem["ClientID"].Text;
                    var adviserName = dataItem["Name"].Text;
                    var type = dataItem["Type"].Text;
                    lblAdviserName.Text = string.Format("Selected {0}: {1} ({2})", type, adviserName, adviserID);
                    lblAdviserCID.Text = adviserCid.ToString();

                    //Binding Configuration data in AlertConfiguration Control.
                    AlertConfiguration1.BindGrid(adviserCid);
                    break;
            }
        }

        private DataTable BindAdviserGrid(string searchText)
        {
            var advisorDs = GetAdviers();

            var rowData = from adviserRows in advisorDs.AdvisorTable.AsEnumerable()
                          where adviserRows.Field<string>(advisorDs.AdvisorTable.CLIENTID).Contains(searchText) ||
                                adviserRows.Field<string>(advisorDs.AdvisorTable.NAME).Contains(searchText)
                          select adviserRows;

            return rowData.CopyToDataTable(); 
        }

        private AdvisorDS GetAdviers()
        {
            var org = Broker.GetWellKnownBMC(WellKnownCM.Organization);

            var advisorDs = new AdvisorDS
            {
                CommandType = DatasetCommandTypes.Get,
                Command = (int)WebCommands.GetOrganizationUnitsByType,
                Unit = new OrganizationUnit
                {
                    CurrentUser = (Page as UMABasePage).GetCurrentUser(),
                    Type = ((int)OrganizationType.Adviser).ToString(),
                }
            };
            org.GetData(advisorDs);
            Broker.ReleaseBrokerManagedComponent(org);
            return advisorDs;
        }

        #endregion

        private ICMBroker Broker
        {
            get
            {

                return (Page is UMABasePage) ? (Page as UMABasePage).UMABroker : null;
            }
        }
    }    
}