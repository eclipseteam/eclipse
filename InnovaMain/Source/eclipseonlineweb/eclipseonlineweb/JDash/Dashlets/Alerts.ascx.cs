﻿using System;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using JDash.WebForms;
using Oritax.TaxSimp;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.SM.Workflows;
using Oritax.TaxSimp.Security;
using Oritax.TaxSimp.Utilities;
using Telerik.Web.UI;
using System.Web.UI.WebControls;
using Oritax.TaxSimp.CM.Organization;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace eclipseonlineweb.JDash.Dashlets
{
    public partial class Alerts : UserControl
    {
        private ICMBroker Broker
        {
            get
            {

                return (Page is UMABasePage) ? (Page as UMABasePage).UMABroker : null;
            }
        }

        public DataSet PresentationData
        {
            get;
            set;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                grdAlerts.MasterTableView.FilterExpression = "(it[\"Status\"].ToString().Contains(\"Active\"))";
                GridColumn column = grdAlerts.MasterTableView.GetColumnSafe("Status");
                column.CurrentFilterFunction = GridKnownFunction.Contains;
                column.CurrentFilterValue = "Active";
                grdAlerts.MasterTableView.Rebind();
            }

        }

        private DashletContext _context;

        [JEventHandler(JEvent.InitContext)]
        public void InitContext(object sender, JEventArgs args)
        {
            _context = args.Event.Parameters.Get<DashletContext>("context");
        }

        public override void DataBind()
        {
            _context.RenderDashlet();
            base.DataBind();
        }

        /// <summary>
        /// Used on dashlet refresh by JDash Framework
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        [JEventHandler(JEvent.Refresh)]
        public void Refresh(object sender, JEventArgs args)
        {
            BindAlertGrid();
            _context.RenderDashlet();
            base.DataBind();
        }

        /// <summary>
        /// Used before dashlet remove by JDash Framework
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        [JEventHandler(JEvent.PreRemove)]
        public void Remove(object sender, JEventArgs args)
        {
            args.Rejected = true;
        }



        /// <summary>
        /// Binds Alert data to the grid
        /// </summary>
        private void BindAlertGrid()
        {
            //Working code
            MDAAlertDS alertDS = GetAlertData();
            DataView mdaAlertTable = alertDS.Tables[MDAAlertDS.MDAALERTTABLE].DefaultView;
            grdAlerts.DataSource = mdaAlertTable;
        }

        protected void grdAlerts_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            BindAlertGrid();
        }

        protected void grdAlerts_ItemCommand(object sender, GridCommandEventArgs e)
        {
            switch (e.CommandName.ToLower())
            {
                case "upload":
                    {
                        var gDataItem = e.Item as GridDataItem;
                        if (gDataItem != null)
                        {
                            string cid = gDataItem["ClientCID"].Text;
                            string notificationId = gDataItem["NotificationID"].Text;

                            lblAttachClientID.Text = cid;
                            lblAttachNotificationID.Text = notificationId;
                            popupUploadDoc.Show();
                        }
                    }
                    break;
                case "download":
                    {
                        //Download had bind on attribute in ItemDataBond with java script.

                        var gDataItem = e.Item as GridDataItem;
                        if (gDataItem != null)
                        {
                            string cid = gDataItem["ClientCID"].Text;
                            string attachmentID = gDataItem["AttachmentID"].Text;
                            DownloadMDA(cid, attachmentID);
                        }
                    }
                    break;
                case "approve":
                    {

                        string commandArgument = e.CommandArgument.ToString();
                        var actionStatus = (ActionStatus)Enum.Parse(typeof(ActionStatus), commandArgument, ignoreCase: true);
                        if (actionStatus == ActionStatus.Active)
                        {
                            var gDataItem = e.Item as GridDataItem;
                            if (gDataItem != null)
                            {
                                string cid = gDataItem["ClientCID"].Text;
                                string actionID = gDataItem["ActionID"].Text;

                                lblMDAClientID.Text = cid;
                                lblActionID.Text = actionID;
                                if (dtMDADate.SelectedDate.HasValue)
                                    dtMDADate.SelectedDate = null;


                                popupTradeDate.Show();
                            }
                        }
                        else if (actionStatus == ActionStatus.Completed)
                        {
                            var gDataItem = e.Item as GridDataItem;
                            if (gDataItem != null)
                            {
                                string actionID = gDataItem["ActionID"].Text;
                                UpdateActionStatus(actionID, ActionStatus.Active);
                            }
                            grdAlerts.Rebind();

                        }
                    }
                    break;
                case "generate":
                    {
                        var gDataItem = e.Item as GridDataItem;
                        if (gDataItem != null)
                        {
                            string clientCID = gDataItem["ClientCID"].Text;
                            GeneratAdnDownloadMDADocument(clientCID);
                        }
                    }
                    break;
                case "email":
                    {
                        var gDataItem = e.Item as GridDataItem;
                        if (gDataItem != null)
                        {
                            string clientCID = gDataItem["ClientCID"].Text;
                            EmailMDADocument(clientCID);
                        }
                    }
                    break;
            }
        }
        protected void grdAlerts_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridCommandItem)
            {
                GridCommandItem commandItem = e.Item as GridCommandItem;
                HtmlGenericControl dvConfiguration = (HtmlGenericControl)commandItem.FindControl("dvConfiguration");
                try
                {
                    var page = Page as UMABasePage;
                    if (page.GetCurrentUser() != null)
                    {
                        var user = (Page as UMABasePage).GetCurrentUser();
                        if (!user.IsAdmin)
                        {
                            dvConfiguration.Visible = false;
                        }
                    }
                }
                catch { }                
            }
        }
        private string FileFormate(string fileName)
        {
            if (fileName != string.Empty && fileName != "&nbsp;")
            {
                var getFileName = fileName.Split('.');
                return getFileName[1].ToLower();
            }
            return string.Empty;
        }

        protected void grdAlerts_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                var gDataItem = e.Item as GridDataItem;
                //Finding controls.
                var approveButton = (LinkButton)gDataItem.FindControl("btnApprove");
                var disapproveButton = (LinkButton)gDataItem.FindControl("btnDisApprove");
                var rdtpExecDate = (RadDatePicker)gDataItem.FindControl("rdtpExecDate");
                var btnAttach = (LinkButton)gDataItem.FindControl("btnAttach");
                var lblApprove = (Label)gDataItem.FindControl("lblApprove");
                var btnDownload = (LinkButton)gDataItem.FindControl("btnDownload");
                var lblActionTypeDetail = (Label)gDataItem.FindControl("lblActionTypeDetail");
                var lblEvent = (Label)gDataItem.FindControl("lblEvent"); 
                var btnGenerateDocument = (LinkButton)gDataItem.FindControl("btnGenerateDocument");
                var imgDownLoadLink = (ImageButton)gDataItem.FindControl("ImgDownloadLink");                
                var fileNameFormate = gDataItem["FileName"].Text;
                var fileExtension = FileFormate(fileNameFormate);


                if (fileExtension != string.Empty && fileExtension != "&nbsp;")
                    btnDownload.ToolTip = "Download signed MDA agreement: " + fileNameFormate;


                switch (fileExtension)
                {
                    case "xps":
                    case "pdf":
                        imgDownLoadLink.ImageUrl = "../../images/FileTypeIcons/pdf.gif";
                        break;

                    case "xls":
                    case "xlsx":
                    case "csv":
                        imgDownLoadLink.ImageUrl = "../../images/FileTypeIcons/excel-file.gif";
                        break;

                    case "doc":
                    case "docx":
                        imgDownLoadLink.ImageUrl = "../../images/FileTypeIcons/ms-word.gif";
                        break;
                    default:
                        {
                            imgDownLoadLink.ImageUrl = "../../images/FileTypeIcons/attach.png";
                            break;
                        }

                }
                approveButton.OnClientClick = "ShowModalPopup";
                string attachmentID = gDataItem["AttachmentID"].Text;
                var clientID = (HyperLink)gDataItem["ClientID"].Controls[0];
                var clientName = (HyperLink)gDataItem["ClientName"].Controls[0];

                clientName.ToolTip = string.Format("Client ID: {0}", clientID.Text);
                string dueDate = gDataItem["DueDate"].Text;
                var info = new DateTimeFormatInfo { ShortDatePattern = "dd/MM/yyyy" };

                DateTime dtimeDueDate;
                if (!DateTime.TryParseExact(dueDate, "dd/MM/yyyy", info, DateTimeStyles.None, out dtimeDueDate))
                {
                    //exepection
                }
                var status = (ActionStatus)Enum.Parse(typeof(ActionStatus), gDataItem["Status"].Text);

                // Get alert/action type.   
                var actionType = (MDAActionTypeDetail)Enum.Parse(typeof(MDAActionTypeDetail), gDataItem["ActionTypeDetail"].Text);

                SetAlertTypeTextAndToolTip(actionType, lblActionTypeDetail, lblEvent);

                //Allowing approve/disapprove action for admin only.
                //There are two buttons for Approve/Disapprove. Only one will visible as ActionStatus.
                disapproveButton.Visible = !(approveButton.Visible = status == ActionStatus.Active);
                approveButton.Visible = !(approveButton.Visible = status == ActionStatus.Completed);

                //There are two buttons for Approve/Disapprove. Only one will visible as ActionStatus.
                disapproveButton.Visible = !(approveButton.Visible = status == ActionStatus.Active);

                //Here We Check User Type if UserType Admin or Innova Then Complete and Active Link Visible
                var user = ((UMABasePage)Page).GetCurrentUser();
                bool isUserInooveorAdmin = (user.UserType.ToString() == UserType.Innova.ToString() || user.IsAdmin);

                rdtpExecDate.Enabled = isUserInooveorAdmin;
                if (!isUserInooveorAdmin)
                {
                    approveButton.Visible = false;
                    disapproveButton.Visible = false;
                    lblApprove.Visible = false;
                }

                // If an alert is not complete 2 weeks prior the due date the alert becomes red
                if (DateTime.Now.Date >= dtimeDueDate.AddDays(-7 * 2).Date)
                {
                    gDataItem.ForeColor = Color.Red;
                    clientID.ForeColor = Color.Red;
                    clientName.ForeColor = Color.Red;
                    approveButton.ForeColor = Color.Red;
                    disapproveButton.ForeColor = Color.Red;
                    btnAttach.ForeColor = Color.Red;
                    btnGenerateDocument.ForeColor = Color.Red;
                    btnDownload.ForeColor = Color.Red;
                }

                ////Applying RED color on Active & Expired alerts.
                //if (dtimeDueDate <= DateTime.Now && status == ActionStatus.Active)
                //{
                //    gDataItem.ForeColor = Color.Red;
                //    clientID.ForeColor = Color.Red;
                //    clientName.ForeColor = Color.Red;
                //    approveButton.ForeColor = Color.Red;
                //    disapproveButton.ForeColor = Color.Red;
                //    btnAttach.ForeColor = Color.Red;
                //    btnGenerateDocument.ForeColor = Color.Red;
                //    btnDownload.ForeColor = Color.Red;
                //}

                if (!string.IsNullOrEmpty(attachmentID) && attachmentID != Guid.Empty.ToString())
                {
                    var downloadUrl = gDataItem["FileDownLoadURLDownload"].Text;
                    btnAttach.Text = "| Reattach";
                    btnAttach.ToolTip = "Reattach signed MDA agreement";
                    btnDownload.Visible = imgDownLoadLink.Visible = true;
                    var script = string.Format("window.open('{0}',null,'height=200,width=400,status=yes,toolbar=no,menubar=no,location=no');", downloadUrl);
                    btnDownload.Attributes.Add("onclick", script);
                    imgDownLoadLink.Attributes.Add("onclick", script);
                }
                else
                {
                    imgDownLoadLink.ImageUrl = "";
                    imgDownLoadLink.Visible = false;
                }
            }
        }

        protected void grdAlerts_PreRender(object sender, EventArgs e)
        {
            //Allowing MDA Execution Date for admin only.
            var user = ((UMABasePage)Page).GetCurrentUser();
            if (!user.IsAdmin) //Not Admin
            {
                grdAlerts.MasterTableView.GetColumn("MDAExecutionDate").Visible = false;
                grdAlerts.Rebind();
            }

        }

        private void UpdateNotificationAttachment(string notificationID, string attachmentID, string fileDownloadUrl)
        {
            WorkflowsDS workflowsDS = GetWorkflowData();
            var notifications = from notification in workflowsDS.Tables[WorkflowsDS.NotificationsTable].AsEnumerable()
                                where notification[NotificationsTable.NOTIFICATIONID_FIELD].ToString() == notificationID
                                select notification;

            if (notifications.Any())
            {
                var notification = notifications.First();
                notification[NotificationsTable.ATTACHMENTID_FIELD] = attachmentID;
                notification[NotificationsTable.FILEDOWNLOADEDURLDOWNLOAD_FIELD] = fileDownloadUrl;
                workflowsDS.DataSetOperationType = DataSetOperationType.UpdateSingle;
                workflowsDS.TableToUpdate = WorkflowsDS.NotificationsTable;
                workflowsDS.RecordToUpdate = notification;
                SaveWorkflowData(workflowsDS);

            }
        }

        private void UpdateActionStatus(string actionID, ActionStatus status)
        {
            WorkflowsDS workflowsDS = GetWorkflowData();

            var alerts = from action in workflowsDS.Tables[WorkflowsDS.ActionsTable].AsEnumerable()
                         where action[ActionsTable.ACTIONID_FIELD].ToString() == actionID
                         select action;

            if (alerts.Any())
            {
                var alert = alerts.First();
                alert["Status"] = status.ToString();
                workflowsDS.DataSetOperationType = DataSetOperationType.UpdateSingle;
                workflowsDS.TableToUpdate = WorkflowsDS.ActionsTable;
                workflowsDS.RecordToUpdate = alert;
                SaveWorkflowData(workflowsDS);
            }

        }


        private void UpdateMDAExecutionDate(string cid, DateTime? mdaExecDate)
        {
            var ds = new MDAExecutionDS();
            DataRow newRow = ds.Tables[MDAExecutionTable.TABLENAME].NewRow();
            newRow[MDAExecutionTable.MDAEXECUTIONDATE] = mdaExecDate;
            ds.DataSetOperationType = DataSetOperationType.UpdateSingle;
            ds.Tables[MDAExecutionTable.TABLENAME].Rows.Add(newRow);
            SaveData(new Guid(cid), ds);
        }

        private void DownloadMDA(string cid, string attachmentID)
        {
            var clientData = Broker.GetBMCInstance(new Guid(cid)) as IOrganizationUnit;
            var attachmentListDs = new AttachmentListDS();
            if (clientData != null)
            {
                clientData.GetData(attachmentListDs);
                Broker.ReleaseBrokerManagedComponent(clientData);
                DataTable objAttachmentsTable = attachmentListDs.Tables[AttachmentListDS.ATTACHMENTTABLE];

                DataRow row = objAttachmentsTable.Select(
                    string.Format("{0} = '{1}'", AttachmentListDS.ATTACHMENTID_FIELD,
                                  attachmentID)).First();
                Response.Redirect(row[AttachmentListDS.ATTACHMENTDOWNLOADURL_FIELD].ToString());

            }

        }

        private void SaveMDA(string cid, string fileName, string description, Stream inputStream,
            out Guid attachmentID, out string fileDownloadURL)
        {

            var clientData = Broker.GetBMCInstance(new Guid(cid)) as IOrganizationUnit;
            var objUser = (DBUser)Broker.GetBMCInstance(Broker.UserContext.Identity.Name, "DBUser_1_1");

            var attachmentListDS = new AttachmentListDS();

            if (clientData != null)
            {
                clientData.GetData(attachmentListDS);
                Broker.ReleaseBrokerManagedComponent(clientData);

                DataTable objAttachmentsTable = attachmentListDS.Tables[AttachmentListDS.ATTACHMENTTABLE];
                objAttachmentsTable.AcceptChanges();
                DataRow objDataRow = objAttachmentsTable.NewRow();
                Guid aID = Guid.NewGuid();
                objDataRow[AttachmentListDS.ATTACHMENTID_FIELD] = aID;
                objDataRow[AttachmentListDS.ATTACHMENTDESCRIPTION_FIELD] = description ?? fileName;
                objDataRow[AttachmentListDS.ATTACHMENTDATEIMPORTED_FIELD] = DateTime.Now;
                objDataRow[AttachmentListDS.ATTACHMENTORIGINPATH_FIELD] = fileName;
                objDataRow[AttachmentListDS.ATTACHMENTIMPORTEDBY_FIELD] = objUser.Name;
                objDataRow[AttachmentListDS.ATTACHMENTFILE_FIELD] = inputStream;
                objDataRow[AttachmentListDS.ATTACHMENTTYPE_FIELD] = AttachmentType.MDAOperatorsAgreement;
                objDataRow[AttachmentListDS.ATTACHMENTTYPENAME_FIELD] =
                    Enumeration.GetDescription(AttachmentType.MDAOperatorsAgreement);
                objDataRow[AttachmentListDS.ATTACHMENTLINKED_FIELD] = false;
                objAttachmentsTable.Rows.Add(objDataRow);

                SaveData(new Guid(cid), attachmentListDS);
                Broker.LogEvent(EventType.UMAClientDoocumentUpload, clientData.CID,
                                     "The attachments of Client: " + clientData.EclipseClientID + "-" + clientData.Name +
                                     " Added");

                attachmentID = aID;
                fileDownloadURL = attachmentListDS.FileDownloadURL;
            }
            else
            {
                attachmentID = Guid.Empty;
                fileDownloadURL = String.Empty;
            }
        }

        private void SaveData(Guid cid, DataSet ds)
        {
            Broker.SaveOverride = true;
            IBrokerManagedComponent clientData = Broker.GetBMCInstance(cid);
            clientData.SetData(ds);
            Broker.SetComplete();
            Broker.SetStart();
        }

        protected string GetApproveText(object status)
        {
            var actionStatus = (ActionStatus)Enum.Parse(typeof(ActionStatus), status.ToString(), ignoreCase: true);
            return actionStatus == ActionStatus.Active ? "Complete" : "Completed";
        }


        private static void SetAlertTypeTextAndToolTip(MDAActionTypeDetail actionType, Label lblActionTypeDetail, Label lblEvent)
        {
            string actionTypeText=string.Empty;
            string actionTypeToolTip=string.Empty;
            switch (actionType)
            {
                case MDAActionTypeDetail.None:
                    actionTypeText = "Invalid Alert Type";
                    actionTypeToolTip = "Alert generated correctly, but no type defined.";
                    break;
                case MDAActionTypeDetail.FirstAlert:
                    actionTypeText = "1st Alert";
                    actionTypeToolTip = "This is the first alert, which is raised 8 weeks before due date.";
                    break;
                case MDAActionTypeDetail.SecondAlert:
                    actionTypeText = "2nd Alert";
                    actionTypeToolTip = "This is the second alert, which is raised 2 weeks before due date.";
                    break;
                case MDAActionTypeDetail.ThirdAlert:
                    actionTypeText = "3rd Alert";
                    actionTypeToolTip = " This is the third alert, which is raised on due date along with an email to Adviser.";
                    break;
                case MDAActionTypeDetail.FourthAlert:
                    actionTypeText = "4th Alert";
                    actionTypeToolTip = "This is the fourth alert, which is raised 4 weeks after due date.";
                    break;
                case MDAActionTypeDetail.Custom:
                    try
                    {
                        if (!string.IsNullOrEmpty(lblEvent.Text))
                        {
                            var eventDetail = lblEvent.Text.Replace("MDA Reminder", "").Trim();
                            lblEvent.Text = "MDA Reminder";
                            var eventDuration = eventDetail.Replace("W", " Week").Replace("D", " Day").Replace("M", " Month");
                            actionTypeText = eventDetail;
                            actionTypeToolTip = string.Format("This is the custom alert, which is raised {0}.", eventDuration);
                        }
                        else
                        {
                            actionTypeText = "Custom Alert";
                            actionTypeToolTip = "Generated by alert configuration settings.";
                        }
                        
                    }
                    catch { }
                    break;
                default:
                    actionTypeText = "Default Alert";
                    actionTypeToolTip = "Default Alert";
                    break;
            }
            lblActionTypeDetail.Text = string.Format("({0})", actionTypeText);
            lblActionTypeDetail.ToolTip = actionTypeToolTip;
        }

        private MDAAlertDS GetAlertData()
        {
            IBrokerManagedComponent bmc = Broker.CreateTransientComponentInstance(new Guid("15DE2644-C1A0-41D5-8293-2794EF31840D"));
            var alertDS = new MDAAlertDS { Unit = new Oritax.TaxSimp.Data.OrganizationUnit { CurrentUser = ((UMABasePage)Page).GetCurrentUser() } };
            bmc.GetData(alertDS);
            PresentationData = alertDS;

            return alertDS;
        }

        private void GeneratAdnDownloadMDADocument(string clientCID)
        {
            IBrokerManagedComponent bmc = Broker.GetBMCInstance(new Guid(clientCID));
            var alertDS = new MDAAlertDS { Unit = new Oritax.TaxSimp.Data.OrganizationUnit { CurrentUser = ((UMABasePage)Page).GetCurrentUser() } };
            bmc.GetData(alertDS);

            var org = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;

            DataRow row = alertDS.Tables[MDAAlertDS.CLIENTDETAILTABLE].Rows[0];
            if (row != null)
            {
                var objMDADocument = new MDADocumentGenerator();
                if (org != null)
                {
                    bool serviceTypeDifm = false;
                    bool serviceTypeDiwm = false;
                    string downFile = string.Empty;
                    if (row[ClientDetailTable.HASDIFM].ToString() != string.Empty)
                        serviceTypeDifm = Convert.ToBoolean(row[ClientDetailTable.HASDIFM]);

                    if (row[ClientDetailTable.HASDIWM].ToString() != string.Empty)
                        serviceTypeDiwm = Convert.ToBoolean(row[ClientDetailTable.HASDIWM]);

                    downFile = objMDADocument.CreateMDARenewalDocument(row, org.Model, serviceTypeDifm, serviceTypeDiwm);

                    string fileName = Path.GetFileName(downFile);
                    string extension = Path.GetExtension(downFile);
                    if (extension != null)
                    {
                        string fileType = extension.Replace(".", "");

                        if (fileName != null) fileName = fileName.Replace("." + fileType, "");

                        string strURL = downFile;
                        byte[] bytes = File.ReadAllBytes(strURL);
                        Session["ExportFileBytes"] = bytes;
                        if (File.Exists(downFile))
                            File.Delete(downFile);

                        var basePath = Page.ResolveClientUrl("~/");
                        string script = string.Format("window.open('{2}ExportReportPopup.aspx?fileName={0}&filetype={1}',null,'height=200,width=400,status=yes,toolbar=no,menubar=no,location=no');", fileName, fileType, basePath);
                        ScriptManager.RegisterStartupScript(this, GetType(), "OpenNewWindow", script, true);
                    }
                }
            }
        }


        private void EmailMDADocument(string clientCID)
        {
            IBrokerManagedComponent bmc = Broker.GetBMCInstance(new Guid(clientCID));
            var alertDS = new MDAAlertDS { Unit = new Oritax.TaxSimp.Data.OrganizationUnit { CurrentUser = ((UMABasePage)Page).GetCurrentUser() } };
            bmc.GetData(alertDS);

            var org = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;

            DataRow row = alertDS.Tables[MDAAlertDS.CLIENTDETAILTABLE].Rows[0];
            if (row != null)
            {
                var objMDADocument = new MDAUtil();
                if (org != null) objMDADocument.GeneratMDADocumentAndSendToAdviser(row, org.Model, false);
            }

            Broker.ReleaseBrokerManagedComponent(org);

        }

        private WorkflowsDS GetWorkflowData()
        {
            IBrokerManagedComponent bmc = Broker.CreateTransientComponentInstance(new Guid("15DE2644-C1A0-41D5-8293-2794EF31840D"));
            var workflowDS = new WorkflowsDS();
            bmc.GetData(workflowDS);
            return workflowDS;
        }

        private void SaveWorkflowData(DataSet ds)
        {
            IBrokerManagedComponent bmc = Broker.CreateTransientComponentInstance(new Guid("15DE2644-C1A0-41D5-8293-2794EF31840D"));
            bmc.SetData(ds);
        }

        protected void btnAttacheDoc_Click(object sender, EventArgs e)
        {
            var cid = lblAttachClientID.Text;
            var notificationId = lblAttachNotificationID.Text;

            //return from method if no file is provided
            if (rauUploadMDA != null && (rauUploadMDA.UploadedFiles == null || rauUploadMDA.UploadedFiles.Count == 0))
                return;

            if (rauUploadMDA != null)
            {
                UploadedFile uploadedFile = rauUploadMDA.UploadedFiles[0];
                Guid aID = Guid.Empty;
                string fileDownloadUrl = null;
                if (txtDescription != null)
                    SaveMDA(cid, uploadedFile.FileName, txtDescription.Text, uploadedFile.InputStream, out aID, out fileDownloadUrl);
                if (aID != Guid.Empty)
                {
                    UpdateNotificationAttachment(notificationId, aID.ToString(), fileDownloadUrl);
                    grdAlerts.Rebind();
                }
            }
            popupUploadDoc.Hide();
        }

        protected void btnMDADateComplete_OnClick(object sender, EventArgs e)
        {
            if (dtMDADate != null)
            {
                UpdateMDAExecutionDate(lblMDAClientID.Text, dtMDADate.SelectedDate);
                UpdateActionStatus(lblActionID.Text, ActionStatus.Completed);
            }
            popupTradeDate.Hide();
            grdAlerts.Rebind();
        }
    }
}