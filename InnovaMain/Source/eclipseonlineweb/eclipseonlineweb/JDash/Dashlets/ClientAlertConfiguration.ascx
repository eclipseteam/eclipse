﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClientAlertConfiguration.ascx.cs" Inherits="eclipseonlineweb.JDash.Dashlets.ClientAlertConfiguration" %>
<%@ Register TagName="AlertConfiguration" TagPrefix="uc" Src="~/JDash/Dashlets/AlertConfigurationControl.ascx" %>
<script type="text/javascript">

    function RequestStart() {
        document.getElementById('ShowOverlay').value = "False";
    }

    function RequestEnd() {
        document.getElementById('ShowOverlay').value = "True";
    }

</script>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server"> </telerik:RadAjaxLoadingPanel>
<telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" EnableAJAX="True" LoadingPanelID="RadAjaxLoadingPanel1" ClientEvents-OnRequestStart="RequestStart" ClientEvents-OnResponseEnd="RequestEnd">
<div id="dvSearch" runat="server">
<fieldset style="padding: 5px; margin-bottom: 5px;">
    <div style="text-align: right; padding-right: 5px; margin-bottom: 5px;">
        <asp:Panel runat="server" ID="pnlSearch" DefaultButton="BtnSearch">
            <span>Search Client: </span><telerik:RadTextBox DisplayText="Type in Client Name, e-Clipse ID or e-Clipse Super Member ID"
            Height="32px" runat="server" ID="txtSearchBox" Width="450px">
            </telerik:RadTextBox>
            <asp:Button ID="BtnSearch" runat="server" CssClass="MediumButton" Text="Search" OnClick="BtnSearchClick" />
        </asp:Panel>
    </div>
    <div style="height: 190px;">
        <asp:GridView runat="server" ID="grdasp">

        </asp:GridView>
        <telerik:RadGrid ID="PresentationGrid"  runat="server" AutoGenerateColumns="False" PageSize="20"
                AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="true" GridLines="None"
                AllowAutomaticInserts="false" AllowFilteringByColumn="true" EnableViewState="true" Width="100%" Height="100%"
                ShowFooter="false" OnNeedDataSource="PresentationGrid_OnNeedDataSource" OnItemCommand="PresentationGrid_OnItemCommand">
                    <PagerStyle Mode="NumericPages"></PagerStyle>
                    <ClientSettings>
                        <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="true">
                        </Scrolling>
                    </ClientSettings>
                    <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="none"
                        Name="Banks" TableLayout="Fixed">
                        <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                        <HeaderStyle Font-Bold="True"></HeaderStyle>
                        <Columns>
                            <telerik:GridButtonColumn ButtonType="LinkButton" CommandName="Select" Text="Select"
                                UniqueName="DetailColumn">
                                <HeaderStyle Width="3%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" CssClass="MyLinkButton"></ItemStyle>
                            </telerik:GridButtonColumn>
                            <telerik:GridBoundColumn SortExpression="ENTITYCIID_FIELD" ReadOnly="true" HeaderText="ENTITYCIID_FIELD"
                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="ENTITYCIID_FIELD" UniqueName="ENTITYCIID_FIELD"
                                Visible="true" Display="false"  FilterControlWidth="70%">
                            </telerik:GridBoundColumn>
                           <telerik:GridBoundColumn SortExpression="ClientID" ReadOnly="true" HeaderText="e-Clipse ID"
                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="ClientID" UniqueName="ClientID"  FilterControlWidth="70%">
                                <HeaderStyle Width="6%"></HeaderStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="OtherID" ReadOnly="true" HeaderText="Member ID"
                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="OtherID" UniqueName="OtherID"  FilterControlWidth="70%">
                                <HeaderStyle Width="7%"></HeaderStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="ENTITYNAME_FIELD" ReadOnly="true" HeaderText="Account Name"
                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="ENTITYNAME_FIELD" UniqueName="ENTITYNAME_FIELD"  FilterControlWidth="70%">
                                <HeaderStyle Width="20%"></HeaderStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="AccountType" ReadOnly="true" HeaderText="Account Type"
                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="AccountType" UniqueName="AccountType"  FilterControlWidth="70%">
                                <HeaderStyle Width="10%"></HeaderStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="Status" ReadOnly="true" HeaderText="Status"
                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="Status" UniqueName="Status"  FilterControlWidth="70%">
                                <HeaderStyle Width="6%"></HeaderStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="AdviserFullName" ReadOnly="true" HeaderText="Adviser"
                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="AdviserFullName" UniqueName="AdviserFullName"  FilterControlWidth="70%">
                                <HeaderStyle Width="12%"></HeaderStyle>
                            </telerik:GridBoundColumn>                            
                            <telerik:GridBoundColumn SortExpression="AdviserCID" ReadOnly="true" HeaderText="Adviser CID"
                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                HeaderButtonType="TextButton" DataField="AdviserCID" UniqueName="AdviserCID"  FilterControlWidth="70%" Display="false">
                                <HeaderStyle Width="12%"></HeaderStyle>
                            </telerik:GridBoundColumn>                            
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
    </div>
</fieldset>
</div>

<div  id="dvAlerts" runat="server" Visible="False" style="padding-top:5px;">
<fieldset style="padding: 5px; margin-bottom: 5px;">
    <legend>Client Alert Configuration Settings</legend>
     <div>
        <asp:Label runat="server" ID="lblClientName" />
        <asp:Label runat="server" ID="lblClientCID" Visible="false" />
    </div>   
    <div style="text-align: center"><asp:Label runat="server" ID="lblError" EnableViewState="False"></asp:Label></div>   
    
    <div>
        <uc:AlertConfiguration runat="server" ID="AlertConfiguration1" />
    </div>
</fieldset>
</div>
</telerik:RadAjaxPanel>
