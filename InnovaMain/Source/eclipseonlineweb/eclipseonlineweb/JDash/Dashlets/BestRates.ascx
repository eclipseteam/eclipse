﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BestRates.ascx.cs" Inherits="eclipseonlineweb.JDash.Dashlets.BestRates" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="c1" Namespace="C1.Web.Wijmo.Controls.C1Chart" Assembly="C1.Web.Wijmo.Controls.4, Version=4.0.20121.56, Culture=neutral, PublicKeyToken=9b75583953471eea" %>
<%@ Register TagPrefix="a" Namespace="Telerik.Web.UI.HtmlChart.PlotArea" Assembly="Telerik.Web.UI" %>

<script type="text/javascript">

    function RequestStart() {
        document.getElementById('ShowOverlay').value = "False";
    }

    function RequestEnd() {
        document.getElementById('ShowOverlay').value = "True";
    }

</script>
<div>
    
    <asp:Accordion ID="MyAccordion" runat="server" SelectedIndex="0" FadeTransitions="true"
        FramesPerSecond="40" TransitionDuration="250" AutoSize="None" HeaderCssClass="accordionHeader"
        ContentCssClass="accordionContent">
        <Panes>
            <asp:AccordionPane runat="server" ID="TDPane">
                <Header>
                    <span class="riLabelBold">TD Rates</span>
                </Header>
                <Content>
                    <table>
                        <tr>
                            <td valign="top" style="width: 27%; height: 260px;">
                                <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server"> </telerik:RadAjaxLoadingPanel>
                                <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" EnableAJAX="True" LoadingPanelID="RadAjaxLoadingPanel1" ClientEvents-OnRequestStart="RequestStart" ClientEvents-OnResponseEnd="RequestEnd">
                                <telerik:RadGrid ID="GridTDRates" runat="server" ShowStatusBar="False" AutoGenerateColumns="False"
                                    PageSize="6" AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True"
                                    GridLines="None" AllowAutomaticDeletes="True" AllowFilteringByColumn="True" AllowAutomaticInserts="True"
                                    AllowAutomaticUpdates="True" EnableViewState="True" ShowFooter="False" OnNeedDataSource="GridTDRates_OnNeedDataSource">
                                    <PagerStyle Mode="NumericPages"></PagerStyle>
                                    <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                                        Name="ClientList" TableLayout="Fixed">
                                        <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                                        <HeaderStyle Font-Bold="True"></HeaderStyle>
                                        <Columns>
                                            <telerik:GridBoundColumn SortExpression="Duration" ReadOnly="true" HeaderText="Duration"
                                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                                HeaderButtonType="TextButton" DataField="Duration" UniqueName="Duration">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn SortExpression="Rate" ReadOnly="true" HeaderText="Rate"
                                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                                HeaderButtonType="TextButton" DataField="Rate" UniqueName="Rate" DataFormatString="{0:P}">
                                            </telerik:GridBoundColumn>
                                        </Columns>
                                    </MasterTableView>
                                </telerik:RadGrid>
                                </telerik:RadAjaxPanel>
                            </td>
                            <td valign="top" style="width: 0.2%; height: 260px;">
                                &nbsp;
                            </td>
                            <td valign="top" style="width: 75%; height: 260px;">
                                <div>
                                    <fieldset>
                                        <div>
                                            <table style="width: 100%; height: 247px;">
                                                <tr>
                                                    <td>
                                                        <telerik:RadHtmlChart runat="server" ID="ChartTDRate" Transitions="true" Height="240px"
                                                            Skin="Metro">
                                                            <ChartTitle Text="Best TD Rates">
                                                                <Appearance Align="Center" Position="Top"></Appearance>
                                                            </ChartTitle>
                                                            <PlotArea>
                                                                <Series>
                                                                    <telerik:ColumnSeries Name="" DataFieldY="ChartRate">
                                                                        <TooltipsAppearance BackgroundColor="lightblue" ClientTemplate="#=dataItem.TooltipData#"></TooltipsAppearance>
                                                                        <LabelsAppearance DataFormatString="{0:N2}%">
                                                                            <TextStyle FontSize="11"></TextStyle>
                                                                        </LabelsAppearance>
                                                                    </telerik:ColumnSeries>
                                                                </Series>
                                                                <XAxis AxisCrossingValue="0" MajorTickType="Outside" MinorTickType="Outside" Reversed="false"
                                                                    MajorTickSize="0" MinorTickSize="0" DataLabelsField="Duration">
                                                                    <TitleAppearance Text="Duration"></TitleAppearance>
                                                                    <LabelsAppearance DataFormatString="{0}" RotationAngle="0" />
                                                                    <MajorGridLines Color="#EFEFEF" Width="0" />
                                                                    <MinorGridLines Color="#F7F7F7" Width="0" />
                                                                </XAxis>
                                                                <YAxis AxisCrossingValue="0" MajorTickSize="0" MajorTickType="Outside" MinorTickSize="0"
                                                                    MinorTickType="Outside" MinValue="0" Reversed="false" Step="2">
                                                                    <TitleAppearance Text="Rate"></TitleAppearance>
                                                                    <LabelsAppearance DataFormatString="{0}%" RotationAngle="0" />
                                                                    <MajorGridLines Color="#EFEFEF" Width="0" />
                                                                    <MinorGridLines Color="#F7F7F7" Width="0" />
                                                                </YAxis>
                                                            </PlotArea>
                                                            <Legend>
                                                                <Appearance Position="Right" />
                                                            </Legend>
                                                        </telerik:RadHtmlChart>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </fieldset>
                                </div>
                            </td>
                        </tr>
                    </table>
                </Content>
            </asp:AccordionPane>
            <asp:AccordionPane runat="server" ID="AtCallPane">
                <Header>
                    <span class="riLabelBold">At Call Rates</span>
                </Header>
                <Content>
                    <telerik:RadGrid ID="GridAtCallRates" runat="server" ShowStatusBar="False" AutoGenerateColumns="False"
                        PageSize="5" AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True"
                        GridLines="None" AllowAutomaticDeletes="True" AllowFilteringByColumn="True" AllowAutomaticInserts="True"
                        AllowAutomaticUpdates="True" EnableViewState="True" ShowFooter="False" OnNeedDataSource="GridAtCallRates_OnNeedDataSource">
                        <PagerStyle Mode="NumericPages"></PagerStyle>
                        <MasterTableView AllowMultiColumnSorting="True" Width="100%" CommandItemDisplay="Top"
                            Name="ClientList" TableLayout="Fixed">
                            <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
                            <HeaderStyle Font-Bold="True"></HeaderStyle>
                            <Columns>
                                <telerik:GridBoundColumn SortExpression="Applicability" ReadOnly="true" HeaderText="Applicability"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                    HeaderButtonType="TextButton" DataField="Applicability" UniqueName="Applicability">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn SortExpression="Rate" ReadOnly="true" HeaderText="Rate"
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                                    HeaderButtonType="TextButton" DataField="Rate" UniqueName="Rate" DataFormatString="{0:P}">
                                </telerik:GridBoundColumn>
                            </Columns>
                        </MasterTableView>
                    </telerik:RadGrid>
                </Content>
            </asp:AccordionPane>
        </Panes>
    </asp:Accordion>
</div>
