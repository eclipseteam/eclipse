﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Notifications.ascx.cs"
    Inherits="eclipseonlineweb.JDash.Dashlets.Notifications" %>

<asp:Label runat="server" ID="lblMessage"></asp:Label>
<telerik:RadGrid ID="grdNotification" Width="99%" runat="server" ShowStatusBar="False" Height="270px"
        AutoGenerateColumns="False" PageSize="10" AllowSorting="True" AllowMultiRowSelection="False"
        AllowPaging="True" GridLines="None" AllowAutomaticDeletes="True" AllowFilteringByColumn="True"
        AllowAutomaticInserts="True" AllowAutomaticUpdates="True" EnableViewState="True"        
        ShowFooter="False" OnNeedDataSource="grdNotification_OnNeedDataSource">
        <PagerStyle Mode="NumericPages"></PagerStyle>
        <ClientSettings>
            <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="true"></Scrolling>
        </ClientSettings>
        <MasterTableView AllowMultiColumnSorting="True" Width="99%" CommandItemDisplay="Top"
            Name="ClientList" TableLayout="Fixed">
            <CommandItemSettings ShowAddNewRecordButton="false"></CommandItemSettings>
            <HeaderStyle Font-Bold="True"></HeaderStyle>
            <Columns>                
                <telerik:GridBoundColumn SortExpression="ClientCID" ReadOnly="true" HeaderText="ClientCID"
                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                    HeaderButtonType="TextButton" DataField="ClientCID" UniqueName="ClientCID" Visible="false">
                </telerik:GridBoundColumn>
                <telerik:GridHyperLinkColumn DataTextFormatString="{0}" DataNavigateUrlFields="ClientCID"
                    SortExpression="ClientID" UniqueName="ClientID" DataNavigateUrlFormatString="~/ClientViews/ClientMainView.aspx?ins={0}"
                    HeaderText="Client ID" DataTextField="ClientID" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                    HeaderButtonType="TextButton" ShowFilterIcon="true">
                </telerik:GridHyperLinkColumn>
                <telerik:GridBoundColumn SortExpression="ClientName" ReadOnly="true" HeaderText="Client Name"
                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                    HeaderButtonType="TextButton" DataField="ClientName" UniqueName="ClientName">
                    <HeaderStyle Width="250px"></HeaderStyle>
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn SortExpression="EventDate" ReadOnly="true" HeaderText="Event Date"
                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                    HeaderButtonType="TextButton" DataField="EventDate" UniqueName="EventDate">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn SortExpression="Title" ReadOnly="true" HeaderText="Title"
                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                    HeaderButtonType="TextButton" DataField="Title" UniqueName="Title">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn SortExpression="SupportingDocument" ReadOnly="true" HeaderText="Supporting Document"
                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                    HeaderButtonType="TextButton" DataField="SupportingDocument" UniqueName="SupportingDocument">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn SortExpression="Status" ReadOnly="true" HeaderText="Status"
                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                    HeaderButtonType="TextButton" DataField="Status" UniqueName="Status">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn SortExpression="ServiceType" ReadOnly="true" HeaderText="Service Type"
                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                    HeaderButtonType="TextButton" DataField="ServiceType" UniqueName="ServiceType">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn SortExpression="Adviser" ReadOnly="true" HeaderText="Adviser"
                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"
                    HeaderButtonType="TextButton" DataField="Adviser" UniqueName="Adviser">
                </telerik:GridBoundColumn>
            </Columns>
        </MasterTableView>
    </telerik:RadGrid>



