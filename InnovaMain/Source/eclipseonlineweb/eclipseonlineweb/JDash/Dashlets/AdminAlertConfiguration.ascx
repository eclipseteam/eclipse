﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdminAlertConfiguration.ascx.cs" Inherits="eclipseonlineweb.JDash.Dashlets.AdminAlertConfiguration" %>
<%@ Register TagName="AlertConfiguration" TagPrefix="uc" Src="~/JDash/Dashlets/AlertConfigurationControl.ascx" %>

<script type="text/javascript">

    function RequestStart() {
        document.getElementById('ShowOverlay').value = "False";
    }

    function RequestEnd() {
        document.getElementById('ShowOverlay').value = "True";
    }

</script>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server"> </telerik:RadAjaxLoadingPanel>
<telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" EnableAJAX="True" LoadingPanelID="RadAjaxLoadingPanel1" ClientEvents-OnRequestStart="RequestStart" ClientEvents-OnResponseEnd="RequestEnd">
<div>
    <uc:AlertConfiguration runat="server" ID="AlertConfiguration1" />
</div>
</telerik:RadAjaxPanel>