﻿using System;
using JDash.WebForms;

namespace eclipseonlineweb.JDash.Dashlets
{
    public partial class Holdings : System.Web.UI.UserControl
    {
        private DashletContext context;
        
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [JEventHandler(JEvent.InitContext)]
        public void InitContext(object sender, JEventArgs args)
        {
            this.context = args.Event.Parameters.Get<DashletContext>("context");
            
        }

        public override void DataBind()
        {
            context.RenderDashlet();
            base.DataBind();
        }

        /// <summary>
        /// Used on dashlet refresh by JDash Framework
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        [JEventHandler(JEvent.Refresh)]
        public void Refresh(object sender, JEventArgs args)
        {
            DataBind();
        }

        /// <summary>
        /// Used before dashlet remove by JDash Framework
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        [JEventHandler(JEvent.PreRemove)]
        public void Remove(object sender, JEventArgs args)
        {
            args.Rejected = true;
        }
    }
}