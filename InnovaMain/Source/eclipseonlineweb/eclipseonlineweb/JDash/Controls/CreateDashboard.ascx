﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CreateDashboard.ascx.cs"
    Inherits="eclipseonlineweb.JDash.Controls.CreateDashboard" %>
<table style="width: 100%">
    <tr style="height: 2px" />
    <tr>
        <telerik:RadTextBox runat="server" ID="txtTitle" Label="Title:" Width="100%">
        </telerik:RadTextBox>
    </tr>
    <tr style="height: 2px" />
    <tr>
        <telerik:RadTextBox runat="server" ID="txtDescription" Label="Description:" TextMode="MultiLine"
            Width="100%" Rows="10">
        </telerik:RadTextBox>
    </tr>
    <tr style="height: 2px" />
    <tr style="width: 100%;">
        <td style="text-align: right">
            <telerik:RadButton runat="server" ID="btnCreate" OnClick="btnCreate_Click" Text="Create">
            </telerik:RadButton>
        </td>
    </tr>
</table>
