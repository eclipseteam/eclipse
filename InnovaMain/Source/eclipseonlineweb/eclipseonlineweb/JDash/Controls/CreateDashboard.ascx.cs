﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using JDash;
using JDash.Models;

namespace eclipseonlineweb.JDash.Controls
{
    public partial class CreateDashboard : System.Web.UI.UserControl
    {
        #region events
        public int TestingValue { get; set; }

        public Action<bool, DashboardModel> DashboardCreated
        {
            get; 
            set;
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
         
        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            var newDashboard = new DashboardModel()
                {
                    title = txtTitle.Text
                };

            newDashboard.metaData.description = txtDescription.Text;
            newDashboard.metaData.created = DateTime.Now;
            
            newDashboard.authorization.Add(
                new KeyValuePair<string, PermissionModel>
                    ("test",
                     new PermissionModel()
                         {
                             authTarget = AuthTarget.userName,
                             permission = Permission.view
                         }));

            JDashManager.Provider.CreateDashboard(newDashboard);
            if (DashboardCreated != null)
            {
                DashboardCreated(true, newDashboard);
            }
        }
    }
}