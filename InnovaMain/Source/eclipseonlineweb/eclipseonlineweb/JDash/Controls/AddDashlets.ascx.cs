﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using JDash;

namespace eclipseonlineweb.JDash.Controls
{
    public partial class AddDashlets : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                rptDashlets.DataSource = JDashManager.Provider.SearchDashletModules().data;
                rptDashlets.DataBind();

            }
        }

        protected void rptDashlets_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
        }

        protected void rptDashlets_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
        }
    }
}