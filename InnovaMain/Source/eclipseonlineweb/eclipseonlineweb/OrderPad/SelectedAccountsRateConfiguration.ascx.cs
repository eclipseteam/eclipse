﻿using System;
using System.Web.UI;

namespace eclipseonlineweb.OrderPad
{
    [Serializable]
    public class TDInstruction
    {
        public Guid ID { get; set; }
        public string InstName = string.Empty;
        public string Broker = string.Empty;

        public string clientCID = string.Empty;
        public string clientName = string.Empty;

        public string RateID = string.Empty;
        public string RateTerm = string.Empty;
        public string Rate = string.Empty;

        public string min = string.Empty;
        public string max = string.Empty;

        public string serviceType = string.Empty;
        public string availableCash = string.Empty;

        public string orderAmount = string.Empty;
    }


    public partial class SelectedAccountsRateConfiguration : System.Web.UI.UserControl
    {
        public Action<string> Removed { get; set; }

        public TDInstruction tDInstruction;
        protected void Page_Load(object sender, EventArgs e)
        {
            //    ddlServiceType.Attributes["onchange"] = string.Format("dropDownListOnChange(this,'{0}','{1}','{2}','{3}');", lblAvailableCash.ClientID, lblDIFMCash.ClientID, lblDIWMCash.ClientID,lblDIYCash.ClientID);
        }


        protected void ImageButton1_OnClick(object sender, ImageClickEventArgs e)
        {
            if (this.Parent != null)
            {
                this.Parent.Controls.Remove(this);
                if (Removed != null)
                {
                    Removed(InstructionID.Value);
                }
            }

        }

        public void SetClientID(string instructionID)
        {

            InstructionID.Value = instructionID;

        }

        protected void ddlServiceType_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlServiceType.SelectedValue == "Do It For Me")
            {
                lblAvailableCash.Text = lblDIFMCash.Value;
            }
            else if (ddlServiceType.SelectedValue == "Do It With Me")
            {
                lblAvailableCash.Text = lblDIWMCash.Value;

            }
            else if (ddlServiceType.SelectedValue == "Do It Yourself")
            {
                lblAvailableCash.Text = lblDIYCash.Value;

            }
        }

        public bool Validate()
        {
            bool result = true;


            return result;
        }
    }
}