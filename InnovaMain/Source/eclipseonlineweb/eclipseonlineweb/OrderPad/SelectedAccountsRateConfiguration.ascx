﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SelectedAccountsRateConfiguration.ascx.cs"
    Inherits="eclipseonlineweb.OrderPad.SelectedAccountsRateConfiguration" %>
<asp:HiddenField ID="lblDIFMCash" runat="server" Value="0" />
<asp:HiddenField ID="lblDIWMCash" runat="server" Value="0" />
<asp:HiddenField ID="lblDIYCash" runat="server" Value="0" />
<asp:HiddenField runat="server" ID="InstructionID" Value="0" />
<table class="out" onmouseover="className='over';" onmouseout="className='out';"
    style="width: 100%; margin: 5px; border-style: solid; border-width: thin; border-color: #dddddd;">
    <tr style="background: #35428B; color: white; font-weight: bold; font-size: larger;
        width: 100%">
        <td colspan="9" style="width: 100%">
            <table style="width: 100%">
                <tr style="width: 100%">
                    <td style="width: 95%">
                        <asp:Label ID="lblClientName" runat="server" Font-Bold="true" Text="ClientName" />
                    </td>
                    <td style="text-align: right; width: 5%">
                        <asp:ImageButton ImageAlign="Middle" ID="CloseButton" runat="server" ImageUrl="~/images/cross_icon_normal.png"
                            OnClick="ImageButton1_OnClick" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr style="width: 100%">
        <td colspan="9" style="width: 100%">
            <table style="width: 100%">
                <tr style="width: 100%">
                    <td style="width: 10%;">
                        <strong>Broker</strong>
                    </td>
                    <td style="width: 12%;">
                        <strong>Institute</strong>
                    </td>
                    <td style="width: 10%;">
                        <strong>Rate</strong>
                    </td>
                    <td style="width: 10%;">
                        <strong>Rate Term</strong>
                    </td>
                    <td style="width: 12%;">
                        <strong>Min</strong>
                    </td>
                    <td style="width: 10%;">
                        <strong>Max</strong>
                    </td>
                    <td style="width: 12%;">
                        <strong>Service Type</strong>
                    </td>
                    <td style="width: 12%;">
                        <strong>Available Cash</strong>
                    </td>
                    <td style="width: 12%;">
                        <strong>Order Amount</strong>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr style="width: 100%">
        <td colspan="9" style="width: 100%">
            <table style="width: 100%">
                <tr style="width: 100%">
                    <td style="width: 10%;">
                        <asp:Label ID="lblBroker" runat="server" Text="" />
                    </td>
                    <td style="width: 12%;">
                        <asp:Label ID="lblInsName" runat="server" Text="" />
                    </td>
                    <td style="width: 10%;">
                        <asp:Label ID="lblRate" runat="server" Text="" />
                    </td>
                    <td style="width: 10%;">
                        <asp:Label ID="lblRateTerm" runat="server" Text="" />
                    </td>
                    <td style="width: 12%;">
                        <asp:Label ID="lblMin" runat="server" Text="" />
                    </td>
                    <td style="width: 10%;">
                        <asp:Label ID="lblMax" runat="server" Text="" />
                    </td>
                    <td style="width: 12%;">
                        <asp:DropDownList ID="ddlServiceType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlServiceType_OnSelectedIndexChanged">
                            <asp:ListItem Selected="True" Text="---------" Value="" />
                        </asp:DropDownList>
                    </td>
                    <td style="width: 12%;">
                        <asp:Label ID="lblAvailableCash" runat="server" Text="" />
                    </td>
                    <td style="width: 12%;">
                        <asp:TextBox ID="txtOrderCash" runat="server" Text="" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
