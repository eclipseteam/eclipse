<%@ Assembly Name="FileImport_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215" %>
<%@ Page language="c#" Codebehind="FileImportList_1_1.aspx.cs" AutoEventWireup="false" Inherits="Oritax.TaxSimp.FileImport.lbFileImportList" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="Menu_1_1.ascx" %>
<%@ Register TagPrefix="uc1" TagName="TitleBar" Src="TitleBar_1_1.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>FileImport</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="./Styles/Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="FileImport" method="post" encType="multipart/form-data" runat="server">
			<TABLE id="Table1" cellSpacing="1" cellPadding="1" width="770" border="0">
				<TR>
					<TD style="HEIGHT: 33px" vAlign="top" align="middle" colSpan="2"><uc1:titlebar id="TitleBar1" runat="server"></uc1:titlebar></TD>
				</TR>
				<TR>
					<TD vAlign="top" width="150"><uc1:menu id="Menu1" runat="server"></uc1:menu></TD>
					<TD vAlign="top" bgColor="#333333" colSpan="1">
						<table cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="BreadcrumbTrail" style="HEIGHT: 2px" colSpan="2"><asp:label id="label_BreadCrumb" runat="server" Width="587px"></asp:label></td>
							</tr>
							<tr>
								<td style="HEIGHT: 2px" colSpan="2">
									<h1 class="NavigationHeading1">File import</h1>
								</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td>
									<table cellSpacing="0" cellPadding="1" width="100%" border="0">
										<tr>
											<td valign="top" width="300">
												<asp:ListBox id="lbImportList" runat="server" Width="300px" Height="300px"></asp:ListBox></td>
											<td valign="top"><asp:button id="btnAddImport" runat="server" Height="24px" Width="180px" BackColor="#FF8629" Text="Import file"></asp:button><br>
												<asp:button id="btnImportDetails" runat="server" Height="24px" Width="180px" BackColor="#FF8629" Text="Edit import details"></asp:button><br>
												<asp:button id="btnDeleteImport" runat="server" Height="24px" Width="180px" BackColor="#FF8629" Text="Delete imported file"></asp:button><br>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td><asp:label id="lblMessages" style="COLOR: white" runat="server"></asp:label></td>
							</tr>
						</table>
					</TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
