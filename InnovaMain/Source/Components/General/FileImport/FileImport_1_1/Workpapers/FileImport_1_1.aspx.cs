using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.FileImport
{
	/// <summary>
	/// Summary description for FileImport.
	/// </summary>
	public class FileImport : Oritax.TaxSimp.Calculation.WorkPaper
	{
		protected override void PopulateWP()
		{
			Guid gFileUploadID = new Guid(Request["FileID"].ToString());

			broker.SetStart(); 
			
			
		
			// Import the stream into a FileImportSM
			FileImportSM smFileImport = (FileImportSM) this.broker.GetBMCInstance(gFileUploadID);
			
			FileImportDS dsFileImport=new FileImportDS();
			smFileImport.ExtractData(dsFileImport);

			
			Response.ContentType = "application/octet-stream";
			
			int iLastIndex = dsFileImport.OriginPath.LastIndexOf("\\");  
			String sDownLoadFileName = "attachment; filename=";
			
			if (iLastIndex > 0)
			{
				sDownLoadFileName += dsFileImport.OriginPath.Substring(iLastIndex + 1);
			}
			else
			{
				sDownLoadFileName += dsFileImport.OriginPath;
			}

			Response.AddHeader( "content-disposition", sDownLoadFileName);
			
			long lFileSize = dsFileImport.Stream.Length; 
			byte[] byteContent = new byte[(int)lFileSize]; 
			dsFileImport.Stream.Read(byteContent, 0, (int)lFileSize); 
			dsFileImport.Stream.Close(); 

			Response.BinaryWrite(byteContent); 

			broker.SetComplete();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion

		
	}
}
