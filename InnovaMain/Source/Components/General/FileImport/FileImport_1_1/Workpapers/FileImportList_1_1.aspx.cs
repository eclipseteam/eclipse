using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.FileImport
{
	/// <summary>
	/// Summary description for FileImport.
	/// </summary>
	public class lbFileImportList : WorkPaper
	{
		protected System.Web.UI.WebControls.Label label_BreadCrumb;
		protected System.Web.UI.WebControls.Label lblMessages;
		protected System.Web.UI.WebControls.Button btnImportDetails;
		protected System.Web.UI.WebControls.Button btnDeleteImport;
		protected System.Web.UI.WebControls.ListBox lbImportList;
		protected System.Web.UI.WebControls.Button btnAddImport;

		protected override string OwnerTypeName
		{
			get { return "FileImport"; }
		}

		protected override IBrokerManagedComponent CreateNewOwner(Guid ownerGuid)
		{
			IBrokerManagedComponent iBMC=broker.CreateBMCInstance("FileImport", "");
			iBMC.CID=ownerGuid;

			return iBMC;
		}

		protected override void InitializeWP()
		{
			base.InitializeWP();

			data=new FileImportDS();
		}

		protected void BindImportList()
		{
			DataRowCollection drc=(DataRowCollection)broker.GetBCMList("FileImport");

			DataTable dt=new DataTable();
			dt.Columns.Add("NAME", typeof(string));
			dt.Columns.Add("ID", typeof(Guid));

			foreach(DataRow dr in drc)
			{
				DataRow ndr=dt.NewRow();
				ndr["NAME"]=dr[DSBMCInfo.CMNAME_FIELD].ToString();
				ndr["ID"]=dr[DSBMCInfo.CMID_FIELD].ToString();
				dt.Rows.Add(ndr);
			}

			dt.DefaultView.Sort="NAME";
			dt.DefaultView.ApplyDefaultSort=true;

			lbImportList.DataSource=dt.DefaultView;
			lbImportList.DataTextField="NAME";
			lbImportList.DataValueField="ID";
			lbImportList.DataBind();
		}

		protected override void PopulateWP()
		{
			base.PopulateWP();

			BindImportList();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnAddImport.Click += new System.EventHandler(this.btnAddImport_Click);
			this.btnImportDetails.Click += new System.EventHandler(this.btnImportDetails_Click);
			this.btnDeleteImport.Click += new System.EventHandler(this.btnDeleteImport_Click);
			this.ID = "lbFileImportList";
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnAddImport_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("FileImport." + VersionString + ".aspx");
		}

		private void btnImportDetails_Click(object sender, System.EventArgs e)
		{
			if(lbImportList.SelectedItem!=null)
				Response.Redirect("FileImport." + VersionString + ".aspx?CID=" + lbImportList.SelectedItem.Value);		
		}

		private void btnDeleteImport_Click(object sender, System.EventArgs e)
		{
			if(lbImportList.SelectedItem!=null)
			{
				broker.SetStart();
				try
				{
					broker.DeleteCMInstance(new Guid(lbImportList.SelectedItem.Value));

					broker.SetComplete();
					Response.Redirect("FileImportList." + VersionString + ".aspx");
				}
				catch(Exception ex)
				{
					broker.SetAbort();
					lblMessages.Text=ex.Message;
				}
			}
		}
	}
}
