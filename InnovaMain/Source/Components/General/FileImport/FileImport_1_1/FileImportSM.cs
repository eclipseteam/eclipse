using System;
using System.Collections;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Runtime.Serialization;
using System.Security.Permissions;

using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.FileImport
{
	[Serializable]
	public class FileImportSM : SystemModuleBase, ISerializable
	{
		//USING CUSTOM SERIALIZATION.
		//Still mark the fields as [NonSerialized] for your own infomation but this is ignored now by .NET
		//You must add your own serialzation code for each instance
		//See GetObjectData & Special Constructor

		#region INSTALLATION PROPERTIES
		public override string InstallTypeID{get{return "B575C57C-15BC-4b02-85B5-DDC4C005A8E6";}}
		public override string InstallTypeName{get{return "FileImport";}}
		public override string InstallDisplayName{get{return "File Import";}}
		public override string InstallCategory{get{return "Accounting";}}
		public override string InstallAssemblyName{get{return "FileImport, Version=1.0.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";}}
		public override string InstallClassName{get{return "Oritax.TaxSimp.General.FileImport.FileImportSM";}}
		public override string InstallPersisterAssembly{get{return "FileImport, Version=1.0.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";}}
		public override string InstallPersisterClass{get{return "Oritax.TaxSimp.General.FileImport.Persister";}}

		public override int InstallMajVersion{get{return Assembly.GetExecutingAssembly().GetName().Version.Major;}}
		public override int InstallMinVersion{get{return Assembly.GetExecutingAssembly().GetName().Version.Minor;}}
		public override int InstallRelease{get{return Assembly.GetExecutingAssembly().GetName().Version.Revision;}}
		#endregion

		[NonSerialized]
		private FileImportPersistentDS primaryDataSet=new FileImportPersistentDS();

		protected class FileImportData
		{
			public DateTime DateImported=DateTime.Now;
			public byte[] Bytes=new byte[0];
			public string ImportedBy=String.Empty;
			public string OriginPath=String.Empty;

			public void ReadStream(Stream stream)
			{
				byte[] newBytes=new byte[stream.Length];
				stream.Read(newBytes, 0, (int)stream.Length);

				this.Bytes=newBytes;
			}

			public MemoryStream CreateMemoryStream() { return new MemoryStream(Bytes); }

			public void ReadImportData(DataTable fileImportTable)
			{
				ReadImportData(fileImportTable, (DataTable)null);
			}

			public void ReadImportData(DataTable fileImportTable, Stream stream)
			{
				this.ReadStream(stream);

				ReadImportData(fileImportTable, (DataTable)null);
			}

			public void ReadImportData(DataTable fileImportTable, DataTable blobPersistTable)
			{
				// grab the byte array from the blob persist table
				if(blobPersistTable!=null && blobPersistTable.Rows.Count==1)
				{
					DataRow blobPersistRow=blobPersistTable.Rows[0];
					this.Bytes=(byte[])blobPersistRow[FileImportPersistentDS.BLOBPERSIST_PERSISTENT_DATA_FIELD];
				}

				// grab the file import data from the file import table
				if(fileImportTable!=null && fileImportTable.Rows.Count==1)
				{
					DataRow fileImportRow=fileImportTable.Rows[0];
					this.DateImported=(DateTime)fileImportRow[FileImportPersistentDS.FILEIMPORT_DATEIMPORTED_FIELD];
					this.ImportedBy=(string)fileImportRow[FileImportPersistentDS.FILEIMPORT_IMPORTEDBY_FIELD];
					this.OriginPath=(string)fileImportRow[FileImportPersistentDS.FILEIMPORT_ORIGINPATH_FIELD];
				}
			}

			public void WriteImportData(DataTable fileImportTable, FileImportSM sm)
			{
				fileImportTable.Rows.Clear();

				DataRow fileImportRow=fileImportTable.NewRow();

				fileImportRow[FileImportDS.FILEIMPORT_ID_FIELD]=sm.CID;
				fileImportRow[FileImportDS.FILEIMPORT_DATEIMPORTED_FIELD]=this.DateImported;
				fileImportRow[FileImportDS.FILEIMPORT_IMPORTEDBY_FIELD]=this.ImportedBy;
				fileImportRow[FileImportDS.FILEIMPORT_ORIGINPATH_FIELD]=this.OriginPath;

				fileImportTable.Rows.Add(fileImportRow);
			}

			public void WriteImportData(DataTable fileImportTable, DataTable blobPersistTable, FileImportSM sm)
			{
				// grab the byte array from the blob persist table
				if(blobPersistTable!=null)
				{
					blobPersistTable.Rows.Clear();

					DataRow blobPersistRow=blobPersistTable.NewRow();
					blobPersistRow[FileImportPersistentDS.BLOBPERSIST_INSTANCEID_FIELD]=sm.CID;
					blobPersistRow[FileImportPersistentDS.BLOBPERSIST_NAME_FIELD]=sm.Name;
					blobPersistRow[FileImportPersistentDS.BLOBPERSIST_TYPE_FIELD]=new Guid(sm.InstallTypeID);
					blobPersistRow[FileImportPersistentDS.BLOBPERSIST_PERSISTENT_DATA_FIELD]=this.Bytes.Clone();

					blobPersistTable.Rows.Add(blobPersistRow);
				}

				WriteImportData(fileImportTable, sm);
			}
		}

		protected FileImportData fileImportData=new FileImportData();

		public FileImportSM()
		{
		}

		protected FileImportSM(SerializationInfo si, StreamingContext context) : base(si,context)
		{
		}

		// Only allow the .NET Serialization core code to call this function
		[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]
		public override void GetObjectData(SerializationInfo si, StreamingContext context)
		{
			base.GetObjectData(si,context);

			Serialize.AddSerializedValue(si,"fi_importedData",this.fileImportData.Bytes);
		}

		public override DataSet PrimaryDataSet
		{
			get 
			{ 
				FileImportPersistentDS ds=new FileImportPersistentDS();

				return ds; 
			} 
		}

		private new void DeliverPrimaryData(DataSet data)
		{
			FileImportPersistentDS ds=data as FileImportPersistentDS;

			if(ds!=null)
			{
				// grab the other FileImport data
				DataRow fileImportRow=ds.Tables[FileImportPersistentDS.FILEIMPORT_TABLE].Rows[0];
				this.CID=(Guid)fileImportRow[FileImportPersistentDS.FILEIMPORT_ID_FIELD];
			}
		}

		public override DataSet MigrateDataSet(DataSet data)
		{
			//Migration up to 2004.3 will not be carried forward. After .3 it will need to be migrated.
			// All will be removed in MigrationComplete.

			DataSet baseDataSet = base.MigrateDataSet(data);

			FileImportDS ds=new FileImportDS();

			ds.Merge(data.Tables[FileImportDS.FILEIMPORTTABLE], false, MissingSchemaAction.Ignore);
			ds.Stream = Stream.Null;
			ds.Merge(baseDataSet);
			return ds;
		}

		public override void MigrationCompleted()
		{
			base.MigrationCompleted ();
			//Remove all instances from DB as this is data that is not needed from .2 to .3
			this.Broker.DeleteCMInstance(this.CID);
		}


		protected ModifiedState OnSetData(FileImportPersistentDS data)
		{
			this.fileImportData.ReadImportData(data.Tables[FileImportPersistentDS.FILEIMPORT_TABLE], data.Tables[FileImportPersistentDS.BLOBPERSIST_TABLE]);

			return ModifiedState.UNCHANGED;
		}

		protected override ModifiedState OnSetData(DataSet data)
		{
			base.OnSetData(data);

			if (data is FileImportDS)
			{
				this.fileImportData.ReadImportData(((FileImportDS)data).Tables[FileImportDS.FILEIMPORTTABLE], ((FileImportDS)data).Stream);
			}
			else if (data is FileImportDetailsDS)
			{
				//base.OnSetData(data);
				OnSetData((FileImportDetailsDS)data);
			}
			else if (data is FileImportPersistentDS)
			{
				OnSetData((FileImportPersistentDS)data);
			}
			return ModifiedState.MODIFIED;
		}

		protected ModifiedState OnSetData(FileImportDetailsDS data)
		{
			this.fileImportData.ReadImportData(data.Tables[FileImportDS.FILEIMPORTTABLE]);

			return ModifiedState.MODIFIED;
		}

		protected override void OnGetData(DataSet data)
		{
			base.OnGetData(data);
			if (data is FileImportPersistentDS)
			{
				FileImportPersistentDS fileData = data as FileImportPersistentDS;
				this.fileImportData.WriteImportData(fileData.Tables[FileImportPersistentDS.FILEIMPORT_TABLE], fileData.Tables[FileImportPersistentDS.BLOBPERSIST_TABLE], this);
			}
			else if (data is FileImportDS)
			{
				this.OnGetData((FileImportDS)data);
			}
			else if (data is FileImportDetailsDS)
			{
				this.OnGetData((FileImportDetailsDS)data);
			}
		}

		protected void OnGetData(FileImportDS data)
		{
			this.fileImportData.WriteImportData(data.Tables[FileImportDS.FILEIMPORTTABLE], this);
			data.Stream=this.fileImportData.CreateMemoryStream();
		}

		protected void OnGetData(FileImportDetailsDS data)
		{
			this.fileImportData.WriteImportData(data.Tables[FileImportDS.FILEIMPORTTABLE], this);
		}

		public bool NameExists(string name)
		{
			IBrokerManagedComponent bmc=Broker.GetBMCInstance(name, "FileImport");
			return bmc!=null;
		}

		#region STATIC METHODS FOR PARSING FILES
		/// <summary>
		/// Parses a single line of a character separated values file. 
		/// This method is static only to simplify the unit test process.
		/// </summary>
		/// <param name="s">The string to be parsed. This represents a single line in an imported file</param>
		/// <param name="sep"></param>
		/// <returns>A string array containing all values in the line, in order of appearance from left to right</returns>
		public static string[] ParseLine(string s, char sep)
		{
			ArrayList alValues=new ArrayList();
			StringBuilder sbValue=new StringBuilder();

			bool inquote=false, scanning=true;
			string v;

			foreach(char c in s.ToCharArray())
			{
				// if we meet a separator and we're not currently inside a quote
				// we assume the separator marks the end of a value
				if(c==sep && !inquote)
				{
					v=sbValue.ToString().Trim();
					if(v.Length>1 && v[0]=='"' && v[v.Length-1]=='"') v=v.Substring(1, v.Length-2);

					// in which case we add the value to our array
					alValues.Add(v);
					sbValue.Length=0;
					scanning=true;
					inquote=false;
				}
				else
				{
					if(c=='"') inquote=scanning;

					if((scanning && c!=' ') || !scanning)
					{
						sbValue.Append(c);
						scanning=false;
					}
				}
			}

			v=sbValue.ToString().Trim();
			if(v.Length>1 && v[0]=='"' && v[v.Length-1]=='"') v=v.Substring(1, v.Length-2);

			// catch the last value
			if(scanning)
				alValues.Add(v.Length>0?v:"");
			else
				alValues.Add(v);

			// return a string array
			return alValues.ToArray(typeof(string)) as string[];
		}
		#endregion
	}
}
