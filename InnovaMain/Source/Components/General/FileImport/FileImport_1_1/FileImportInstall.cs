using System;

namespace Oritax.TaxSimp.FileImport
{
	/// <summary>
	/// Summary description for Install.
	/// </summary>
	public class FileImportInstall
	{
		#region INSTALLATION PROPERTIES

		// Assembly Installation Properties
		public const string ASSEMBLY_ID="B575C57C-15BC-4B02-85B5-DDC4C005A8E6";
		public const string ASSEMBLY_NAME="FileImport_1_1";
		public const string ASSEMBLY_DISPLAYNAME="File Import V1.1";
		public const string ASSEMBLY_MAJORVERSION="1";
		public const string ASSEMBLY_MINORVERSION="1";
		public const string ASSEMBLY_DATAFORMAT="0";
	//	public const string ASSEMBLY_REVISION="0";  
		public const string ASSEMBLY_REVISION="0"; //2005.1 
			
		// Component Installation Properties
		public const string COMPONENT_ID="9E6A9C5F-A9B4-4d0e-AF98-7D0029B62421";
		public const string COMPONENT_NAME="FileImport";
		public const string COMPONENT_DISPLAYNAME="File Import";
		public const string COMPONENT_CATEGORY="Utility";

		// Component Version Installation Properties
		public const string COMPONENTVERSION_STARTDATE="01/01/2004 00:00:00 AM";
		public const string COMPONENTVERSION_ENDDATE="31/12/2004 12:59:59 AM";
		public const string COMPONENTVERSION_PERSISTERASSEMBLY="FileImport_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";
		public const string COMPONENTVERSION_PERSISTERCLASS="Oritax.TaxSimp.FileImport.Persister";
		public const string COMPONENTVERSION_IMPLEMENTATIONCLASS="Oritax.TaxSimp.FileImport.FileImportSM";

		// Data Model
		public const string ASSEMBLY_PERSISTERASSEMBLY="FileImport_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";
		public const string ASSEMBLY_PERSISTERCLASS="Oritax.TaxSimp.FileImport.Persister";

		// Associated File Installation Properties
		// Workpapers
		public const string	ASSOCIATEDFILE_WP_FILEIMPORT_FILENAME="FileImport_1_1.aspx";

		#endregion
	}
}
