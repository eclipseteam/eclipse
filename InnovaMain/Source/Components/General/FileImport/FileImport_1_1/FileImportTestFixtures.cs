using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Reflection;

using NUnit.Framework;

using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.Services.CMBroker;

namespace Oritax.TaxSimp.FileImport
{
	[TestFixture]
	public class PersisterTests
	{
		SqlConnection sqlCon = null;

		[Conditional("DEBUG")] [SetUp] public void PersisterTestSetup()
		{
			string sConn = ConfigurationSettings.AppSettings["TX360ENTERPRISE_DBCONNECTIONSTRING"];
			sqlCon = new SqlConnection(sConn);
			sqlCon.Open();
		}

		[Conditional("DEBUG")] [TearDown] public void PersisterTestTearDown()
		{
			sqlCon.Close();
			sqlCon.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterConstructionTest()
		{
			Persister fiPersister = new Persister(sqlCon);

			FieldInfo dataAdaptInfo = fiPersister.GetType().GetField("sdaFileImport", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlDataAdapter dsCommand = (SqlDataAdapter)dataAdaptInfo.GetValue(fiPersister);
			Assertion.AssertNotNull("SqlDataAdapter dsBMCCommand is Null",dsCommand);
			Assertion.AssertNotNull("sdaFileImport.SelectCommand is Null",dsCommand.SelectCommand);
			Assertion.AssertNotNull("sdaFileImport.InsertCommand is Null",dsCommand.InsertCommand);
			Assertion.AssertNotNull("sdaFileImport.UpdateCommand is Null",dsCommand.UpdateCommand);
			Assertion.AssertNotNull("sdaFileImport.DeleteCommand is Null",dsCommand.DeleteCommand);

			dataAdaptInfo = fiPersister.GetType().GetField("sdaBlobPersist", BindingFlags.NonPublic | BindingFlags.Instance);
			dsCommand = (SqlDataAdapter)dataAdaptInfo.GetValue(fiPersister);
			Assertion.AssertNotNull("SqlDataAdapter dsBMCCommand is Null",dsCommand);
			Assertion.AssertNotNull("sdaBlobPersist.SelectCommand is Null",dsCommand.SelectCommand);
			Assertion.AssertNotNull("sdaBlobPersist.InsertCommand is Null",dsCommand.InsertCommand);
			Assertion.AssertNotNull("sdaBlobPersist.UpdateCommand is Null",dsCommand.UpdateCommand);
			Assertion.AssertNotNull("sdaBlobPersist.DeleteCommand is Null",dsCommand.DeleteCommand);

			FieldInfo sqlConnectionInfo = fiPersister.GetType().GetField("sqlConnection", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlConnection sqlConnection = (SqlConnection)sqlConnectionInfo.GetValue(fiPersister);
			Assertion.AssertEquals("Connection has not been set", sqlConnection,sqlCon);

			FieldInfo sqlTransactionInfo = fiPersister.GetType().GetField("sqlTransaction", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlTransaction sqlTransaction = (SqlTransaction)sqlTransactionInfo.GetValue(fiPersister);
			Assertion.AssertNull("sqlTransaction is not null", sqlTransaction);

			fiPersister.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterConnectionTest()
		{
			Persister fiPersister = new Persister(sqlCon);
			fiPersister.Connection = sqlCon;

			FieldInfo sqlConnectionInfo = fiPersister.GetType().GetField("sqlConnection", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlConnection sqlConnection = (SqlConnection)sqlConnectionInfo.GetValue(fiPersister);
			Assertion.AssertEquals("Connection has not been set", sqlConnection,sqlCon);

			fiPersister.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterTransactionTest()
		{
			Persister fiPersister = new Persister(sqlCon);
			SqlTransaction sqlTran = sqlCon.BeginTransaction();
			fiPersister.Transaction = sqlTran;
			FieldInfo sqlTransactionInfo = fiPersister.GetType().GetField("sqlTransaction", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlTransaction sqlTransaction = (SqlTransaction)sqlTransactionInfo.GetValue(fiPersister);
			Assertion.AssertEquals("sqlTransaction has not been set", sqlTransaction, sqlTran);
			sqlTran.Commit();
			sqlTran.Dispose();
			fiPersister.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterEstablishDBTest()
		{
			Persister fiPersister = new Persister(sqlCon);

			SqlTransaction sqlTran = sqlCon.BeginTransaction();
			fiPersister.Transaction = sqlTran;
			try
			{
				fiPersister.EstablishDatabase();
			}
			finally
			{
				sqlTran.Rollback();	//DO NOT CORRUPT THE DATABASE
			}
			fiPersister.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterClearDBTest()
		{
			Persister fiPersister = new Persister(sqlCon);

			SqlTransaction sqlTran = sqlCon.BeginTransaction();
			fiPersister.Transaction = sqlTran;
			try
			{
				fiPersister.ClearDatabase();
			}
			finally
			{
				sqlTran.Rollback();	//DO NOT CORRUPT THE DATABASE
			}
			fiPersister.Dispose();
		}


		private CMBroker broker=new CMBroker();
		[Conditional("DEBUG")] [Test] public void SimpleSaveTest()
		{
			broker.SetStart();

			Guid CID=Guid.Empty;

			try
			{
				// Create an instance of FileImportSM
				FileImportSM sm=broker.CreateBMCInstance("FileImport", "") as FileImportSM;
				CID=sm.CID;
				sm.Name="SimpleSaveTest";
				Assertion.Assert("Failed to create instance of FileImportSM", sm!=null);

				// Create and extract DataSet from SM
				FileImportDS ds=new FileImportDS();
				sm.ExtractData(ds);
				Assertion.Assert("Expected 1 row in extracted data", ds.Tables[FileImportDS.FILEIMPORTTABLE].Rows.Count==1);

				// Populate the DataSet with some test data
				ds.ImportedBy="TEST FIXTURE";
				ds.DateImported=new DateTime(2003, 6, 7);
				ds.OriginPath=@"C:\NOT\USED\FOR\THIS\TEST.CSV";

				// Deliver data
				sm.DeliverData(ds);

				// Done
				broker.SetComplete();

				// Clear the cache
				broker.SetStart();
				broker.ClearCache();
				broker.SetComplete();

				// Now get the SM again
				broker.SetStart();
				sm=broker.GetBMCInstance(CID) as FileImportSM;
				Assertion.Assert("Failed to retrieve instance of FileImportSM", sm!=null);
				Assertion.AssertEquals("Imported By text is not correct", ds.ImportedBy, "TEST FIXTURE");
				Assertion.Assert("Date Imported is not correct", ds.DateImported.Year==2003 && ds.DateImported.Month==6 && ds.DateImported.Day==7);
				broker.SetComplete();
			}
			catch
			{
				broker.SetAbort();
				throw;
			}
			finally
			{
				// Finally we'll delete this test CM
				broker.SetStart();
				broker.DeleteCMInstance(CID);
				broker.SetComplete();
			}
		}

		[Conditional("DEBUG")] [Test] public void FileSaveTest()
		{
			byte[] importFile=System.Text.Encoding.ASCII.GetBytes("Hello World");
			MemoryStream stream=new MemoryStream(importFile);

			broker.SetStart();

			Guid CID=Guid.Empty;

			try
			{
				// Create an instance of FileImportSM
				FileImportSM sm=broker.CreateBMCInstance("FileImport", "") as FileImportSM;
				CID=sm.CID;
				sm.Name="FileSaveTest";

				Assertion.Assert("Failed to create instance of FileImportSM", sm!=null);

				// Create and extract DataSet from SM
				FileImportDS ds=new FileImportDS();
				sm.ExtractData(ds);
				Assertion.Assert("Expected 1 row in data", ds.Tables[FileImportDS.FILEIMPORTTABLE].Rows.Count==1);

				// Populate the DataSet with some test data
				ds.ImportedBy="TEST FIXTURE";
				ds.DateImported=new DateTime(2003, 6, 7);
				ds.OriginPath=@"C:\NOT\USED\FOR\THIS\TEST.CSV";
				ds.Stream=stream;

				// Deliver data
				sm.DeliverData(ds);

				// Done
				broker.SetComplete();

				broker.SetStart();
				broker.ClearCache();
				broker.SetComplete();

				ds.Clear();

				// Now retrieve the SM
				broker.SetStart();
				sm=broker.GetBMCInstance(CID) as FileImportSM;
				sm.ExtractData(ds);
				TextReader tr=StreamReader.Synchronized(new StreamReader(ds.Stream, System.Text.Encoding.UTF8));
				Assertion.AssertEquals("Data test failed", "Hello World", tr.ReadToEnd());
				broker.SetComplete();
			}
			catch
			{
				broker.SetAbort();
				throw;
			}
			finally
			{
				broker.SetStart();
				broker.DeleteCMInstance(CID);
				broker.SetComplete();
			}
		}

		[Conditional("DEBUG")] [Test] public void BigFileSaveTest()
		{
			byte[] importFile=new byte[256*30000];

			for(int i=0; i<30000; i++)
				for(int j=0; j<256; j++)
					importFile[i*256+j]=(byte)(j%0xFF);

			MemoryStream stream=new MemoryStream(importFile);

			broker.SetStart();

			Guid CID=Guid.Empty;

			try
			{
				// Create an instance of FileImportSM
				FileImportSM sm=broker.CreateBMCInstance("FileImport", "") as FileImportSM;
				CID=sm.CID;
				sm.Name="BigFileSaveTest";
				Assertion.Assert("Failed to create instance of FileImportSM", sm!=null);

				// Create and extract DataSet from SM
				FileImportDS ds=new FileImportDS();
				sm.ExtractData(ds);
				Assertion.Assert("Expected 1 row in extracted data", ds.Tables[FileImportDS.FILEIMPORTTABLE].Rows.Count==1);

				// Populate the DataSet with some test data
				ds.ImportedBy="TEST FIXTURE";
				ds.DateImported=new DateTime(2003, 6, 7);
				ds.OriginPath=@"C:\NOT\USED\FOR\THIS\TEST.CSV";
				ds.Stream=stream;

				// Deliver data
				sm.DeliverData(ds);

				// Done
				broker.SetComplete();

				broker.SetStart();
				broker.ClearCache();
				broker.SetComplete();

				ds.Clear();

				// Now retrieve the SM
				broker.SetStart();

				DateTime startTime=DateTime.Now;

				sm=broker.GetBMCInstance(CID) as FileImportSM;
				sm.ExtractData(ds);

				TimeSpan ts=DateTime.Now-startTime;

				for(int i=0; i<30000; i++)
				{
					for(int j=0; j<256; j++)
					{
						int expected=importFile[i*256+j];
						int actual=ds.Stream.ReadByte();
						Assertion.AssertEquals("Persisted data is not consistent", expected, actual);
					}
				}

				broker.SetComplete();
			}
			catch
			{
				broker.SetAbort();
				throw;
			}
			finally
			{
				broker.SetStart();
				broker.DeleteCMInstance(CID);
				broker.SetComplete();
			}
		}
	}

	[TestFixture] public class ParserSuccessCasesTestFixture
	{
		[Conditional("DEBUG")] [Test] public void BlankTest()
		{
			string [] expected=new string[] { "", "", "", "", "", "" };

			ValidateParserResult("Blank fields Test", expected,	FileImportSM.ParseLine(",,,,,", ','));
			ValidateParserResult("Blank fields Test", expected,	FileImportSM.ParseLine("\t\t\t\t\t", '\t'));
			ValidateParserResult("Blank fields Test", expected,	FileImportSM.ParseLine(";;;;;", ';'));
		}


		[Conditional("DEBUG")] [Test] public void BlankSpaceTest()
		{
			string [] expected=new string[] { "", "", "", "", "", "" };

			ValidateParserResult("Blank fields Test", expected,	FileImportSM.ParseLine(" , , , , , ", ','));
			ValidateParserResult("Blank fields Test", expected,	FileImportSM.ParseLine(" \t \t \t \t \t ", '\t'));
			ValidateParserResult("Blank fields Test", expected,	FileImportSM.ParseLine(" ; ; ; ; ; ", ';'));
		}

		[Conditional("DEBUG")] [Test] public void BlankQuotedTest()
		{
			string [] expected=new string[] { "", "", "", "", "", "" };

			ValidateParserResult("Blank fields Test", expected,	FileImportSM.ParseLine("\"\",\"\",\"\",\"\",\"\",\"\"", ','));
			ValidateParserResult("Blank fields Test", expected,	FileImportSM.ParseLine("\"\"\t\"\"\t\"\"\t\"\"\t\"\"\t\"\"", '\t'));
			ValidateParserResult("Blank fields Test", expected,	FileImportSM.ParseLine("\"\";\"\";\"\";\"\";\"\";\"\"", ';'));
		}

		[Conditional("DEBUG")] [Test] public void BlankQuotedSpaceTest()
		{
			string [] expected=new string[] { "", "", "", "", "", "" };

			ValidateParserResult("Blank fields Test", expected,	FileImportSM.ParseLine(" \"\" , \"\" , \"\" , \"\" , \"\" , \"\" ", ','));
			ValidateParserResult("Blank fields Test", expected,	FileImportSM.ParseLine(" \"\" \t \"\" \t \"\" \t \"\" \t \"\" \t \"\"", '\t'));
			ValidateParserResult("Blank fields Test", expected,	FileImportSM.ParseLine(" \"\" ; \"\" ; \"\" ; \"\" ; \"\" ; \"\"", ';'));
		}

		[Conditional("DEBUG")] [Test] public void EmbeddedSpacesTest()
		{
			string [] expected=new string[] { "one embedded space", "two embedded space", 
												"three embedded space", "four embedded space", 
												"five embedded space", "six embedded space" };

			ValidateParserResult("Embedded space Test", expected,
				FileImportSM.ParseLine("one embedded space, two embedded space, three embedded space, four embedded space, five embedded space, six embedded space", ','));

			ValidateParserResult("Embedded space Test", expected,
				FileImportSM.ParseLine("one embedded space\t two embedded space\t three embedded space\t four embedded space\t five embedded space\t six embedded space", '\t'));

			ValidateParserResult("Embedded space Test", expected,
				FileImportSM.ParseLine("one embedded space; two embedded space; three embedded space; four embedded space; five embedded space; six embedded space", ';'));
		}

		[Conditional("DEBUG")] [Test] public void QuotedEmbeddedSpacesTest()
		{
			string [] expected=new string[] { "one embedded space", "two embedded space", 
												"three embedded space", "four embedded space", 
												"five embedded space", "six embedded space" };

			ValidateParserResult("Embedded space Test", expected,
				FileImportSM.ParseLine("\"one embedded space\", \"two embedded space\", \"three embedded space\", \"four embedded space\", \"five embedded space\", \"six embedded space\"", ','));

			ValidateParserResult("Embedded space Test", expected,
				FileImportSM.ParseLine("\"one embedded space\"\t \"two embedded space\"\t \"three embedded space\"\t \"four embedded space\"\t \"five embedded space\"\t \"six embedded space\"", '\t'));

			ValidateParserResult("Embedded space Test", expected,
				FileImportSM.ParseLine("\"one embedded space\"; \"two embedded space\"; \"three embedded space\"; \"four embedded space\"; \"five embedded space\"; \"six embedded space\"", ';'));
		}

		[Conditional("DEBUG")] [Test] public void QuotedEmbeddedSeperatorTest()
		{
			ValidateParserResult("Embedded space Test", 
				new string[] { "one,embedded,seperator", "two,embedded,seperator", 
								 "three,embedded,seperator", "four,embedded,seperator", 
								 "five,embedded,seperator", "six,embedded,seperator" },
				FileImportSM.ParseLine("\"one,embedded,seperator\", \"two,embedded,seperator\", \"three,embedded,seperator\", \"four,embedded,seperator\", \"five,embedded,seperator\", \"six,embedded,seperator\"", ','));

			ValidateParserResult("Embedded tab Test", 
				new string[] { "one\tembedded\tseperator", "two\tembedded\tseperator", 
								 "three\tembedded\tseperator", "four\tembedded\tseperator", 
								 "five\tembedded\tseperator", "six\tembedded\tseperator" },
				FileImportSM.ParseLine("\"one\tembedded\tseperator\"\t \"two\tembedded\tseperator\"\t \"three\tembedded\tseperator\"\t \"four\tembedded\tseperator\"\t \"five\tembedded\tseperator\"\t \"six\tembedded\tseperator\"", '\t'));

			ValidateParserResult("Embedded space Test", 
				new string[] { "one;embedded;seperator", "two;embedded;seperator", 
								 "three;embedded;seperator", "four;embedded;seperator", 
								 "five;embedded;seperator", "six;embedded;seperator" },
				FileImportSM.ParseLine("\"one;embedded;seperator\"; \"two;embedded;seperator\"; \"three;embedded;seperator\"; \"four;embedded;seperator\"; \"five;embedded;seperator\"; \"six;embedded;seperator\"", ';'));
		}

		[Conditional("DEBUG")] [Test] public void SimpleTest()
		{
			string [] expected=new string[] { "one", "two", "three", "four", "five", "six" };

			// Test simple CSV line
			ValidateParserResult("Simple CSV Test", expected,
				FileImportSM.ParseLine("one,two,three,four,five,six", ','));

			// Test simple TAB SV line
			ValidateParserResult("Simple TAB SV Test", expected,
				FileImportSM.ParseLine("one\ttwo\tthree\tfour\tfive\tsix", '\t'));

			// Test simple Semi-colon SV line
			ValidateParserResult("Simple Semi-colon SV Test", expected,
				FileImportSM.ParseLine("one;two;three;four;five;six", ';'));
		}

		[Conditional("DEBUG")] [Test] public void ScanningTest()
		{
			string[] expected=new string[] { "one", "two", "three", "four", "five", "" };

			// Test simple CSV line for limbo value
			ValidateParserResult("Simple CSV Test", expected,
				FileImportSM.ParseLine("one,two,three,four,five,", ','));

			// Test simple TAB SV line for limbo value
			ValidateParserResult("Simple TAB SV Test", expected,
				FileImportSM.ParseLine("one\ttwo\tthree\tfour\tfive\t", '\t'));

			// Test simple Semi-colon SV line for limbo value
			ValidateParserResult("Simple Semi-colon SV Test", expected,
				FileImportSM.ParseLine("one;two;three;four;five;", ';'));
		}

		[Conditional("DEBUG")] [Test] public void QuotedSimpleTest()
		{
			string[] expected=new string[] { "one", "two", "three", "four", "five", "six" };

			// Test simple quoted CSV
			ValidateParserResult("Simple quoted CSV Test", expected,
				FileImportSM.ParseLine("\"one\",\"two\",\"three\",\"four\",\"five\",\"six\"", ','));

			// Test simple quoted TAB SV
			ValidateParserResult("Simple quoted CSV Test", expected,
				FileImportSM.ParseLine("\"one\"\t\"two\"\t\"three\"\t\"four\"\t\"five\"\t\"six\"", '\t'));

			// Test simple quoted semi-colon SV
			ValidateParserResult("Simple quoted semi-colon Test", expected,
				FileImportSM.ParseLine("\"one\";\"two\";\"three\";\"four\";\"five\";\"six\"", ';'));
		}

		[Conditional("DEBUG")] [Test] public void SimpleSpaceAfterTest()
		{
			string[] expected=new string[] { "one", "two", "three", "four", "five", "six" };

			// Test simple quoted CSV
			ValidateParserResult("Simple quoted spaced CSV Test", expected,
				FileImportSM.ParseLine("one, two, three, four, five, six", ','));

			// Test simple quoted TAB SV
			ValidateParserResult("Simple quoted spaced CSV Test", expected,
				FileImportSM.ParseLine("one\t two\t three\t four\t five\t six", '\t'));

			// Test simple quoted semi-colon SV
			ValidateParserResult("Simple quoted spaced semi-colon Test", expected,
				FileImportSM.ParseLine("one; two; three; four; five; six", ';'));
		}

		[Conditional("DEBUG")] [Test] public void SimpleSpaceBeforeTest()
		{
			string[] expected=new string[] { "one", "two", "three", "four", "five", "six" };

			// Test simple quoted CSV
			ValidateParserResult("Simple quoted spaced CSV Test", expected,
				FileImportSM.ParseLine("one ,two ,three ,four , five ,six ", ','));

			// Test simple quoted TAB SV
			ValidateParserResult("Simple quoted spaced CSV Test", expected,
				FileImportSM.ParseLine("one \ttwo \tthree \tfour \tfive \tsix", '\t'));

			// Test simple quoted semi-colon SV
			ValidateParserResult("Simple quoted spaced semi-colon Test", expected,
				FileImportSM.ParseLine("one ;two ;three ;four ;five ;six", ';'));
		}

		[Conditional("DEBUG")] [Test] public void QuotedSpaceBothTest()
		{
			string[] expected=new string[] { "one", "two", "three", "four", "five", "six" };

			// Test simple quoted CSV
			ValidateParserResult("Simple quoted spaced CSV Test", expected,
				FileImportSM.ParseLine(" \"one\" , \"two\" , \"three\" , \"four\" , \"five\" , \"six\"" , ','));

			// Test simple quoted TAB SV
			ValidateParserResult("Simple quoted spaced CSV Test", expected,
				FileImportSM.ParseLine(" \"one\" \t \"two\" \t \"three\" \t \"four\" \t \"five\" \t \"six\"", '\t'));

			// Test simple quoted semi-colon SV
			ValidateParserResult("Simple quoted spaced semi-colon Test", expected,
				FileImportSM.ParseLine(" \"one\" ; \"two\" ; \"three\" ; \"four\" ; \"five\" ; \"six\"", ';'));
		}

		[Conditional("DEBUG")] [Test] public void SimpleEmbeddedQuoteTest()
		{
			string[] expected=new string[] { "one\"quote", "two\"quote", "three\"quote", "four\"quote", "five\"quote", "six\"quote" };

			// Test simple quoted CSV
			ValidateParserResult("Simple quoted spaced CSV Test", expected,
				FileImportSM.ParseLine("one\"quote,two\"quote,three\"quote,four\"quote,five\"quote,six\"quote" , ','));

			// Test simple quoted TAB SV
			ValidateParserResult("Simple quoted spaced CSV Test", expected,
				FileImportSM.ParseLine("one\"quote\ttwo\"quote\tthree\"quote\tfour\"quote\tfive\"quote\tsix\"quote", '\t'));

			// Test simple quoted semi-colon SV
			ValidateParserResult("Simple quoted spaced semi-colon Test", expected,
				FileImportSM.ParseLine("one\"quote;two\"quote;three\"quote;four\"quote;five\"quote;six\"quote", ';'));
		}

		[Conditional("DEBUG")] [Test] public void SimpleEmbeddedQuoteEndTest()
		{
			string[] expected=new string[] { "one\"", "two\"", "three\"", "four\"", "five\"", "six\"" };

			// Test simple quoted CSV
			ValidateParserResult("Simple quoted spaced CSV Test", expected,
				FileImportSM.ParseLine("one\",two\",three\",four\",five\",six\"" , ','));

			// Test simple quoted TAB SV
			ValidateParserResult("Simple quoted spaced CSV Test", expected,
				FileImportSM.ParseLine("one\"\ttwo\"\tthree\"\tfour\"\tfive\"\tsix\"", '\t'));

			// Test simple quoted semi-colon SV
			ValidateParserResult("Simple quoted spaced semi-colon Test", expected,
				FileImportSM.ParseLine("one\";two\";three\";four\";five\";six\"", ';'));
		}

		[Conditional("DEBUG")] private void ValidateParserResult(string testname, string[] expected, string[] actual)
		{
			// Test that the number of results is equal
			Assertion.AssertEquals(testname + ": parser failed to extract correct number of records.", expected.Length, actual.Length);

			// Now iterate through the actual results
			for(int i=0; i<actual.Length; i++) Assertion.AssertEquals(expected[i], actual[i]);
		}
	}
}