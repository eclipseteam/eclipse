using System;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using Oritax.TaxSimp.Calculation;

namespace Oritax.TaxSimp.FileImport
{
	/// <summary>
	/// DataService Class That Sets up a DataAdpater for the FileReference Table
	/// Done so it is easy to Unit Test
	/// </summary>
	public class AttachmentWrapper : IDisposable
	{		
		internal const string ATTACHMENT_SELECT_COMMAND="SELECT ID, BMCID, FILEIMPORTID FROM ATTACHMENT WHERE FILEIMPORTID=@FILEIMPORTID";
		internal const string ATTACHMENT_INSERT_COMMAND="INSERT INTO ATTACHMENT (ID, BMCID, FILEIMPORTID) VALUES(@ID, @BMCID, @FILEIMPORTID)";
		internal const string ATTACHMENT_DELETE_COMMAND="DELETE FROM ATTACHMENT WHERE ID=@ID";
		internal const string ATTACHMENT_UPDATE_COMMAND="UPDATE ATTACHMENT SET ID=@ID,BMCID=@BMCID,FILEIMPORTID=@FILEIMPORTID WHERE ID=@ID";
		
		private System.Data.SqlClient.SqlDataAdapter sdaAttachment = new SqlDataAdapter();
		private bool disposed = false;

		public AttachmentWrapper()
		{
			InitializeAttachmentWrapper();
		}

		public SqlConnection Connection
		{
			set
			{
				sdaAttachment.SelectCommand.Connection=
					sdaAttachment.UpdateCommand.Connection=
					sdaAttachment.DeleteCommand.Connection=
					sdaAttachment.InsertCommand.Connection=value;
			}
		}

		public SqlTransaction Transaction
		{
			set
			{
				sdaAttachment.SelectCommand.Transaction=
					sdaAttachment.DeleteCommand.Transaction=
					sdaAttachment.UpdateCommand.Transaction=
					sdaAttachment.InsertCommand.Transaction=value;
			}
		}

		private void InitializeAttachmentWrapper()
		{
			sdaAttachment.MissingSchemaAction = MissingSchemaAction.Ignore;

			sdaAttachment.SelectCommand=new SqlCommand(ATTACHMENT_SELECT_COMMAND);
			sdaAttachment.SelectCommand.CommandType=CommandType.Text;
			sdaAttachment.SelectCommand.Parameters.Add("@FILEIMPORTID", SqlDbType.UniqueIdentifier, 16,  FileImportDS.ATTACHMENT_FILEIMPORTID_FIELD);

			sdaAttachment.InsertCommand=new SqlCommand(ATTACHMENT_INSERT_COMMAND);
			sdaAttachment.InsertCommand.CommandType=CommandType.Text;
			sdaAttachment.InsertCommand.Parameters.Add("@ID", SqlDbType.UniqueIdentifier, 16, FileImportDS.ATTACHMENT_ID_FIELD);
			sdaAttachment.InsertCommand.Parameters.Add("@BMCID", SqlDbType.UniqueIdentifier, 16, FileImportDS.ATTACHMENT_BMCID_FIELD);
			sdaAttachment.InsertCommand.Parameters.Add("@FILEIMPORTID", SqlDbType.UniqueIdentifier, 16, FileImportDS.ATTACHMENT_FILEIMPORTID_FIELD);

			sdaAttachment.UpdateCommand=new SqlCommand(ATTACHMENT_UPDATE_COMMAND);
			sdaAttachment.UpdateCommand.CommandType=CommandType.Text;
			sdaAttachment.UpdateCommand.Parameters.Add("@ID", SqlDbType.UniqueIdentifier, 16, FileImportDS.ATTACHMENT_ID_FIELD);
			sdaAttachment.UpdateCommand.Parameters.Add("@BMCID", SqlDbType.UniqueIdentifier, 16, FileImportDS.ATTACHMENT_BMCID_FIELD);
			sdaAttachment.UpdateCommand.Parameters.Add("@FILEIMPORTID", SqlDbType.UniqueIdentifier, 16, FileImportDS.ATTACHMENT_FILEIMPORTID_FIELD);

			sdaAttachment.DeleteCommand=new SqlCommand(ATTACHMENT_DELETE_COMMAND);
			sdaAttachment.DeleteCommand.CommandType=CommandType.Text;
			sdaAttachment.DeleteCommand.Parameters.Add("@ID", SqlDbType.UniqueIdentifier, 16,  FileImportDS.ATTACHMENT_ID_FIELD);
		}

		#region IDisposable Implementation ---------------------------------------

		/// <summary>
		///		Performs application-defined tasks associated with freeing, releasing, 
		///		or resetting unmanaged resources.
		/// </summary>
		public void Dispose() 
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		/// <summary>
		///		Performs application-defined tasks associated with freeing, releasing, 
		///		or resetting unmanaged resources.
		/// </summary>
		/// <param name="disposing">
		///		True if called by the Dispose() method, false if called by the finalizer.
		/// </param>
		protected void Dispose(bool disposing) 
		{
			if (!disposed)
			{
				if (disposing)
				{
					sdaAttachment.Dispose();
				}

				disposed = true;
			}
		}

		#endregion // IDisposable Implementation

		public void Fill(DataTable oDS, Guid gBMC)
		{
			sdaAttachment.SelectCommand.Parameters["@FILEIMPORTID"].Value = gBMC;
			sdaAttachment.Fill(oDS);
		}

		public void Update(DataTable oDS)
		{
			sdaAttachment.Update(oDS);
		}
	}
}
