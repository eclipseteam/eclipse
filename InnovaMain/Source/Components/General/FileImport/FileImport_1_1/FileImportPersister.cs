using System;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;

using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.DSCalculation;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.FileImport
{
	public class Persister : BrokerManagedPersister
	{
		public Persister(SqlConnection connection, SqlTransaction transaction) : base(connection, transaction){}

		public override void EstablishDatabase()
		{
			Guid typeID=new Guid(FileImportInstall.ASSEMBLY_ID);
			PrimaryDS primaryDataSet=new FileImportPersistentDS();

			EstablishDatabase(typeID,primaryDataSet);
		}

		/// <summary>
		/// Gets a list of BMCs that use the specified BMC as an attachment
		/// </summary>
		/// <param name="attacherCollectionSpecifier">Specifier for the attacher.</param>
		/// <returns></returns>
		protected virtual DataTable ListBMCCollection(BrokerManagedComponent.AttacherCollectionSpecifier attacherCollectionSpecifier)
		{
			SqlDataAdapter collectionAdapter=new SqlDataAdapter();
			collectionAdapter.SelectCommand = DBCommandFactory.Instance.NewSqlCommand();
			collectionAdapter.SelectCommand.Connection = this.Connection;
			collectionAdapter.SelectCommand.Transaction = this.Transaction;
			collectionAdapter.SelectCommand.CommandText="SELECT ID,BMCCID,ATTACHEDBMCID,IMPORTEDBY,DATEIMPORTED,ORIGINPATH,DESCRIPTION FROM "+ BrokerManagedComponentDS.ATTACHMENT_TABLE+" WHERE ATTACHEDBMCID='"+attacherCollectionSpecifier.AttachmentID.ToString()+"'";

			DataTable attacherTable=new BrokerManagedComponent.AttacherDT();
			collectionAdapter.Fill(attacherTable);

			return attacherTable;
		}
	}
}
