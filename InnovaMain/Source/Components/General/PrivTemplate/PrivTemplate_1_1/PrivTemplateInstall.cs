using System;

namespace Oritax.TaxSimp.Security
{
	/// <summary>
	/// Summary description for PrivTemplateInstall.
	/// </summary>
	public class PrivTemplateInstall
	{
		#region INSTALLATION PROPERTIES

		// Assembly Installation Properties
		public const string ASSEMBLY_ID="{CC81653F-9D70-42B3-8722-6EBC43CC83A2}";
		public const string ASSEMBLY_NAME="PrivTemplate_1_1";
		public const string ASSEMBLY_DISPLAYNAME="Privilege Template V1.1";
		public const string ASSEMBLY_MAJORVERSION="1";
		public const string ASSEMBLY_MINORVERSION="1";
		public const string ASSEMBLY_DATAFORMAT="0";
//		public const string ASSEMBLY_REVISION="0";   
		public const string ASSEMBLY_REVISION="1"; //2005.1   


		// Component Installation Properties
		public const string COMPONENT_ID="2E0A06D3-2B32-40da-8056-F45D2D0FBDC8";
		public const string COMPONENT_NAME="PrivTemplate";
		public const string COMPONENT_DISPLAYNAME="Privilege Template";
		public const string COMPONENT_CATEGORY="Security";

		// Component Version Installation Properties
		public const string COMPONENTVERSION_STARTDATE="01/01/1990 00:00:00 AM";
		public const string COMPONENTVERSION_ENDDATE="31/12/2050 12:59:59 AM";
		public const string COMPONENTVERSION_PERSISTERASSEMBLY="PrivTemplate_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";
		public const string COMPONENTVERSION_PERSISTERCLASS="Oritax.TaxSimp.Security.PrivTemplatePersister";
		public const string COMPONENTVERSION_IMPLEMENTATIONCLASS="Oritax.TaxSimp.Security.PrivTemplateSM";

		#endregion
	}
}
