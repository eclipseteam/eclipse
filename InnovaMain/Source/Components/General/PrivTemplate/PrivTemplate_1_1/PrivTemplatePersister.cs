using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Reflection;

using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.DSCalculation;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.Security
{
	/// <summary>
	/// The ComponentManagedPersister persists a component to the database by obtaining a copy of
	/// its primary dataset and persisting each row in each table to corresponding tables in the
	/// database.  This persister leaves the component responsible for mapping between its internal
	/// objects and its primary dataset, with the assumption that the primary dataset is replicated
	/// in the database.
	/// </summary>
	public class PrivTemplatePersister : BrokerManagedComponentPersister, ICPersister, IDisposable
	{
		private SqlDataAdapter privTemplateEntryCommand;			// Adapter for Priv Templates
		private bool disposed = false;

		#region IDisposable Implementation ---------------------------------------

		/// <summary>
		///		Performs application-defined tasks associated with freeing, releasing, 
		///		or resetting unmanaged resources.
		/// </summary>
		public new void Dispose() 
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		/// <summary>
		///		Performs application-defined tasks associated with freeing, releasing, 
		///		or resetting unmanaged resources.
		/// </summary>
		/// <param name="disposing">
		///		True if called by the Dispose() method, false if called by the finalizer.
		/// </param>
		protected void Dispose(bool disposing) 
		{
			if (!disposed)
			{
				if (disposing)
				{
					base.Dispose();
					privTemplateEntryCommand.Dispose();
				}
				disposed = true;
			}
		}

		#endregion // IDisposable Implementation

		public PrivTemplatePersister(SqlConnection connection, SqlTransaction transaction)
			: base(connection, transaction)
		{
			//
			// Create the adapter for logical CMs
			//
			privTemplateEntryCommand = new SqlDataAdapter();	// Adapter for Logical CMs
			//
			// Create the DataSetCommand SelectCommand
			//
			privTemplateEntryCommand.SelectCommand = DBCommandFactory.Instance.NewSqlCommand();
			privTemplateEntryCommand.SelectCommand.CommandType = CommandType.Text;

			privTemplateEntryCommand.SelectCommand.Connection= this.Connection;
			privTemplateEntryCommand.SelectCommand.Transaction = this.Transaction;

			privTemplateEntryCommand.InsertCommand = DBCommandFactory.Instance.NewSqlCommand();
			privTemplateEntryCommand.InsertCommand.CommandText = "INSERT INTO PRIVTEMPLATEENTRY (PRIVTEMPLATEID,PRIVILEGETYPE,PRIVILEGEVALUE) VALUES (@PRIVTEMPLATEID,@PRIVILEGETYPE,@PRIVILEGEVALUE)";
			privTemplateEntryCommand.InsertCommand.CommandType = CommandType.Text;
			privTemplateEntryCommand.InsertCommand.Parameters.Add(new SqlParameter("@PRIVTEMPLATEID", SqlDbType.UniqueIdentifier,16,PrivTemplateDS.PRIVTEMPLATEID_FIELD));
			privTemplateEntryCommand.InsertCommand.Parameters.Add(new SqlParameter("@PRIVILEGETYPE", SqlDbType.Int,4,PrivTemplateDS.PRIVILEGETYPE_FIELD));
			privTemplateEntryCommand.InsertCommand.Parameters.Add(new SqlParameter("@PRIVILEGEVALUE", SqlDbType.Int,4,PrivTemplateDS.PRIVILEGEVALUE_FIELD));
			privTemplateEntryCommand.InsertCommand.Connection=privTemplateEntryCommand.SelectCommand.Connection;
			privTemplateEntryCommand.InsertCommand.Transaction=privTemplateEntryCommand.SelectCommand.Transaction;

			privTemplateEntryCommand.UpdateCommand = DBCommandFactory.Instance.NewSqlCommand();
			privTemplateEntryCommand.UpdateCommand.CommandText = "UPDATE PRIVTEMPLATEENTRY SET PRIVTEMPLATEENTRY=@PRIVTEMPLATEENTRY,NAME=@NAME,CURRCSID=@CURRCSID,CTID=@CTID,PLID=@PLID,BSCONSOLIDATIONMODE=@BSCONSOLIDATIONMODE,BSPERIODTYPE=@BSPERIODTYPE,BSCONSOL=@BSCONSOL,BSOUTYPE=@BSOUTYPE  WHERE CLID=@CLID";
			privTemplateEntryCommand.UpdateCommand.CommandType = CommandType.Text;
			privTemplateEntryCommand.UpdateCommand.Parameters.Add(new SqlParameter("@PRIVTEMPLATEID", SqlDbType.UniqueIdentifier,16,PrivTemplateDS.PRIVTEMPLATEID_FIELD));
			privTemplateEntryCommand.UpdateCommand.Parameters.Add(new SqlParameter("@PRIVILEGETYPE", SqlDbType.Int,4,PrivTemplateDS.PRIVILEGETYPE_FIELD));
			privTemplateEntryCommand.UpdateCommand.Parameters.Add(new SqlParameter("@PRIVILEGEVALUE", SqlDbType.Int,4,PrivTemplateDS.PRIVILEGEVALUE_FIELD));
			privTemplateEntryCommand.UpdateCommand.Connection=privTemplateEntryCommand.SelectCommand.Connection;
			privTemplateEntryCommand.UpdateCommand.Transaction=privTemplateEntryCommand.SelectCommand.Transaction;

			privTemplateEntryCommand.DeleteCommand = DBCommandFactory.Instance.NewSqlCommand();
			privTemplateEntryCommand.DeleteCommand.CommandText = "DELETE PRIVTEMPLATEENTRY WHERE PRIVTEMPLATEENTRY.PRIVTEMPLATEID=@PRIVTEMPLATEID";
			privTemplateEntryCommand.DeleteCommand.CommandType = CommandType.Text;
			privTemplateEntryCommand.DeleteCommand.Parameters.Add(new SqlParameter("@PRIVTEMPLATEID", SqlDbType.UniqueIdentifier,16,PrivTemplateDS.PRIVTEMPLATEID_FIELD));
			privTemplateEntryCommand.DeleteCommand.Connection=privTemplateEntryCommand.SelectCommand.Connection;
			privTemplateEntryCommand.DeleteCommand.Transaction=privTemplateEntryCommand.SelectCommand.Transaction;

			privTemplateEntryCommand.TableMappings.Add("Table", LogicalModuleBaseDS.CMLOGICAL_TABLE);
		}

		public DataSet Update(DataSet data)
		{
			return data;
		}

		public override IBrokerManagedComponent Get(Guid instanceId, ref DataSet primaryDS)
		{
			// Get the Logical CM that has the specified instance ID, that is, its CLID.  The Logical Module also contains
			// all of the scenarios associated with it.
			PrivTemplateSM privTemplateSM=new PrivTemplateSM();

			privTemplateEntryCommand.SelectCommand.CommandText = "SELECT PRIVTEMPLATEENTRY.PRIVTEMPLATEID,PRIVTEMPLATEENTRY.PRIVILEGETYPE,PRIVTEMPLATEENTRY.PRIVILEGEVALUE"
				+" FROM PRIVTEMPLATEENTRY"
				+" WHERE PRIVTEMPLATEENTRY.PRIVTEMPLATEID='{"+instanceId.ToString()+"}'";

			PrivTemplateDS privTemplateDS=new PrivTemplateDS();
			privTemplateEntryCommand.Fill(privTemplateDS.Tables[PrivTemplateDS.PRIVTEMPLATEENTRY_TABLE]);

			privTemplateSM.DeliverData(privTemplateDS);

			base.Hydrate((IBrokerManagedComponent)privTemplateSM,instanceId,new BrokerManagedComponentDS(), ref primaryDS);
			privTemplateSM.Modified=false;

			return privTemplateSM;
		}

		public override void Save(IBrokerManagedComponent iBMC, ref DataSet persistedDS)
		{
			Guid cLID=iBMC.CID;

			privTemplateEntryCommand.SelectCommand.CommandText = "SELECT PRIVTEMPLATEENTRY.PRIVTEMPLATEID,PRIVTEMPLATEENTRY.PRIVILEGETYPE,PRIVTEMPLATEENTRY.PRIVILEGEVALUE"
				+" FROM PRIVTEMPLATEENTRY"
				+" WHERE PRIVTEMPLATEENTRY.PRIVTEMPLATEID='{"+iBMC.CID.ToString()+"}'";

			PrivTemplateDS privTemplateDSTarget=new PrivTemplateDS();
			privTemplateEntryCommand.Fill(privTemplateDSTarget.Tables[PrivTemplateDS.PRIVTEMPLATEENTRY_TABLE]);

			PrivTemplateDS privTemplateDSSource=new PrivTemplateDS();
			iBMC.ExtractData(privTemplateDSSource);
			privTemplateDSTarget.Merge(privTemplateDSSource,false,MissingSchemaAction.Ignore);

			privTemplateEntryCommand.Update(privTemplateDSTarget.Tables[PrivTemplateDS.PRIVTEMPLATEENTRY_TABLE]);

			base.Save(iBMC,new BrokerManagedComponentDS(), ref persistedDS);
		}

		public override void Delete(IBrokerManagedComponent iBMC)
		{
			Guid cLID=iBMC.CID;

			privTemplateEntryCommand.SelectCommand.CommandText = "SELECT PRIVTEMPLATEENTRY.PRIVTEMPLATEID,PRIVTEMPLATEENTRY.PRIVILEGETYPE,PRIVTEMPLATEENTRY.PRIVILEGEVALUE"
				+" FROM PRIVTEMPLATEENTRY"
				+" WHERE PRIVTEMPLATEENTRY.PRIVTEMPLATEID='{"+iBMC.CID.ToString()+"}'";

			PrivTemplateDS privTemplateDSTarget=new PrivTemplateDS();
			privTemplateEntryCommand.Fill(privTemplateDSTarget.Tables[PrivTemplateDS.PRIVTEMPLATEENTRY_TABLE]);

			foreach(DataRow row in privTemplateDSTarget.Tables[PrivTemplateDS.PRIVTEMPLATEENTRY_TABLE].Rows)
			{
				row.Delete();
			}

			privTemplateEntryCommand.Update(privTemplateDSTarget.Tables[PrivTemplateDS.PRIVTEMPLATEENTRY_TABLE]);
		}

		public override DataSet FindDetailsByTypeId(Guid typeId, object[] parameters)
		{
			return new DataSet();
		}

		public override SqlConnection Connection
		{
			set
			{
				base.Connection=value;
				privTemplateEntryCommand.InsertCommand.Connection=value;
				privTemplateEntryCommand.SelectCommand.Connection=value;
				privTemplateEntryCommand.UpdateCommand.Connection=value;
				privTemplateEntryCommand.DeleteCommand.Connection=value;
			}
		}

		public override SqlTransaction Transaction
		{
			set
			{
				base.Transaction=value;
				privTemplateEntryCommand.InsertCommand.Transaction=value;
				privTemplateEntryCommand.SelectCommand.Transaction=value;
				privTemplateEntryCommand.UpdateCommand.Transaction=value;
				privTemplateEntryCommand.DeleteCommand.Transaction=value;
			}
		}
		public override void EstablishDatabase()
		{
			SqlCommand adminCommand = DBCommandFactory.Instance.NewSqlCommand();
            adminCommand.Connection = this.Connection;
			adminCommand.Transaction = this.Transaction;

			adminCommand.CommandText="if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PRIVTEMPLATEENTRY]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)"
			+"CREATE TABLE [dbo].[PRIVTEMPLATEENTRY] ("
			+"	[PRIVTEMPLATEID] [uniqueidentifier] NOT NULL ,"
			+"	[PRIVILEGETYPE] [int] NOT NULL ,"
			+"	[PRIVILEGEVALUE] [int] NOT NULL "
			+") ON [PRIMARY]";
			adminCommand.ExecuteNonQuery();

			adminCommand.CommandText="if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[privtemplate]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)"
			+"CREATE TABLE [dbo].[privtemplate] ("
			+"[ID] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL "
			+") ON [PRIMARY]";
			adminCommand.ExecuteNonQuery();
		}

		public override void ClearDatabase()
		{
            SqlCommand adminCommand = DBCommandFactory.Instance.NewSqlCommand();
            adminCommand.Connection = this.Connection;
			adminCommand.Transaction = this.Transaction;
			adminCommand.CommandText="if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PRIVTEMPLATEENTRY]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)"
				+"DELETE PRIVTEMPLATEENTRY";
			adminCommand.ExecuteNonQuery();

			adminCommand.CommandText="if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[privtemplate]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)"
				+"DELETE privtemplate";
			adminCommand.ExecuteNonQuery();
		}

		public override void RemoveDatabase()
		{

		}
	}
}
