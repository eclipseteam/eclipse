using System;
using System.Diagnostics;
using System.Configuration;
using System.Data.SqlClient;
using System.Reflection;
using System.Data;

using NUnit.Framework;



namespace Oritax.TaxSimp.Security
{
	/// <summary>
	/// Summary description for PrivTemplatePersisterTestFixture.
	/// </summary>
	/// 
	[TestFixture]
	public class PrivTemplatePersisterTestFixture
	{
		SqlConnection sqlCon = null;

		[Conditional("DEBUG")] [SetUp] public void PersisterTestSetup()
		{
			string sConn = ConfigurationSettings.AppSettings["TX360ENTERPRISE_DBCONNECTIONSTRING"];
			sqlCon = new SqlConnection(sConn);
			sqlCon.Open();
		}

		[Conditional("DEBUG")] [TearDown] public void PersisterTestTearDown()
		{
			sqlCon.Close();
			sqlCon.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterConstructionTest()
		{
			PrivTemplatePersister ptPersister = new PrivTemplatePersister(sqlCon);

			FieldInfo dataAdaptInfo = ptPersister.GetType().GetField("privTemplateEntryCommand", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlDataAdapter dsCommand = (SqlDataAdapter)dataAdaptInfo.GetValue(ptPersister);
			Assertion.AssertNotNull("SqlDataAdapter privTemplateEntryCommand is Null",dsCommand);
			Assertion.AssertNotNull("privTemplateEntryCommand.SelectCommand is Null",dsCommand.SelectCommand);
			Assertion.AssertNotNull("privTemplateEntryCommand.InsertCommand is Null",dsCommand.InsertCommand);
			Assertion.AssertNotNull("privTemplateEntryCommand.UpdateCommand is Null",dsCommand.UpdateCommand);
			Assertion.AssertNotNull("privTemplateEntryCommand.DeleteCommand is Null",dsCommand.DeleteCommand);

			FieldInfo sqlConnectionInfo = ptPersister.GetType().GetField("sqlConnection", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlConnection sqlConnection = (SqlConnection)sqlConnectionInfo.GetValue(ptPersister);
			Assertion.AssertEquals("Connection has not been set", sqlConnection,sqlCon);

			FieldInfo sqlTransactionInfo = ptPersister.GetType().GetField("sqlTransaction", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlTransaction sqlTransaction = (SqlTransaction)sqlTransactionInfo.GetValue(ptPersister);
			Assertion.AssertNull("sqlTransaction is not null", sqlTransaction);

			ptPersister.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterConnectionTest()
		{
			PrivTemplatePersister ptPersister = new PrivTemplatePersister(sqlCon);
			ptPersister.Connection = sqlCon;

			FieldInfo sqlConnectionInfo = ptPersister.GetType().GetField("sqlConnection", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlConnection sqlConnection = (SqlConnection)sqlConnectionInfo.GetValue(ptPersister);
			Assertion.AssertEquals("Connection has not been set", sqlConnection,sqlCon);

			ptPersister.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterTransactionTest()
		{
			PrivTemplatePersister ptPersister = new PrivTemplatePersister(sqlCon);
			SqlTransaction sqlTran = sqlCon.BeginTransaction();
			ptPersister.Transaction = sqlTran;
			FieldInfo sqlTransactionInfo = ptPersister.GetType().GetField("sqlTransaction", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlTransaction sqlTransaction = (SqlTransaction)sqlTransactionInfo.GetValue(ptPersister);
			Assertion.AssertEquals("sqlTransaction has not been set", sqlTransaction, sqlTran);
			sqlTran.Commit();
			sqlTran.Dispose();
			ptPersister.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterEstablishDBTest()
		{
			PrivTemplatePersister ptPersister = new PrivTemplatePersister(sqlCon);

			SqlTransaction sqlTran = sqlCon.BeginTransaction();
			ptPersister.Transaction = sqlTran;
			try
			{
				ptPersister.EstablishDatabase();
			}
			finally
			{
				sqlTran.Rollback();	//DO NOT CORRUPT THE DATABASE
			}
			ptPersister.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterClearDBTest()
		{
			PrivTemplatePersister ptPersister = new PrivTemplatePersister(sqlCon);

			SqlTransaction sqlTran = sqlCon.BeginTransaction();
			ptPersister.Transaction = sqlTran;
			try
			{
				ptPersister.ClearDatabase();
			}
			finally
			{
				sqlTran.Rollback();	//DO NOT CORRUPT THE DATABASE
			}
			ptPersister.Dispose();
		}
	}
}
