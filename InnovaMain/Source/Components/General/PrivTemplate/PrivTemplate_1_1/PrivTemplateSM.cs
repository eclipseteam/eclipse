using System;
using System.Collections;
using System.Data;

using Oritax.TaxSimp.Calculation;

namespace Oritax.TaxSimp.Security
{
	public class PrivTemplateSM : SystemModuleBase, IPrivilegeTemplate
	{
		PrivEntrySet privEntrySet=new PrivEntrySet();

		public PrivEntrySet PrivilegeEntries{get{return privEntrySet;}}

		public PrivTemplateSM()
		{

		}

		public override void DeliverData(DataSet data)
		{
			base.DeliverData(data);

			if(data is PrivTemplateDS)
			{
				PrivTemplateDS privTemplateDS=data as PrivTemplateDS;
				privEntrySet.Clear();
				foreach(DataRow dataRow in privTemplateDS.Tables[PrivTemplateDS.PRIVTEMPLATEENTRY_TABLE].Rows)
				{
					privEntrySet.Add(new PrivTemplateEntry(
						this.CID,
						new PrivilegeType((int)dataRow[PrivTemplateDS.PRIVILEGETYPE_FIELD]),
						new PrivilegeOption((int)dataRow[PrivTemplateDS.PRIVILEGEVALUE_FIELD])));
				}
			}
		}

		protected override void OnGetData(DataSet data)
		{
			base.OnGetData(data);

			if(data is PrivTemplateDS)
			{
				PrivTemplateDS privTemplateDS=data as PrivTemplateDS;

				foreach(PrivTemplateEntry privTemplateEntry in privEntrySet)
				{
					DataRow privEntryRow=privTemplateDS.Tables[PrivTemplateDS.PRIVTEMPLATEENTRY_TABLE].NewRow();
					privEntryRow[PrivTemplateDS.PRIVTEMPLATEID_FIELD]=privTemplateEntry.ID;
					privEntryRow[PrivTemplateDS.PRIVILEGETYPE_FIELD]=privTemplateEntry.PrivType.ToInt();
					privEntryRow[PrivTemplateDS.PRIVILEGEVALUE_FIELD]=privTemplateEntry.PrivValue.ToInt();
					privTemplateDS.Tables[PrivTemplateDS.PRIVTEMPLATEENTRY_TABLE].Rows.Add(privEntryRow);
				}
			}
		}
	}
}
