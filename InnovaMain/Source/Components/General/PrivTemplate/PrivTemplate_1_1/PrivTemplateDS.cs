using System;
using System.Data;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Calculation;

namespace Oritax.TaxSimp.Security
{
	/// <summary>
	/// DSCalculationModule is a DataSet that contains all of the persistent data for a CM instance.
	/// </summary>
	[Serializable] 
	public class PrivTemplateDS : BrokerManagedComponentDS
	{
		public const string PRIVTEMPLATEENTRY_TABLE		= "PRIVTEMPLATEENTRY";
		public const string PRIVTEMPLATEID_FIELD		= "PRIVTEMPLATEID";
		public new const string PRIVILEGETYPE_FIELD			= "PRIVILEGETYPE";
		public new const string PRIVILEGEVALUE_FIELD		= "PRIVILEGEVALUE";

		public PrivTemplateDS()
		{
			DataTable table;

			table = new DataTable(PRIVTEMPLATEENTRY_TABLE);
			table.Columns.Add(PRIVTEMPLATEID_FIELD, typeof(System.Guid));
			table.Columns.Add(PRIVILEGETYPE_FIELD, typeof(System.Int32));
			table.Columns.Add(PRIVILEGEVALUE_FIELD, typeof(System.Int32));
			DataColumn[] prPrimaryKey={table.Columns[PRIVILEGETYPE_FIELD]};
			table.PrimaryKey=prPrimaryKey;
			this.Tables.Add(table);
		}
	}
}

