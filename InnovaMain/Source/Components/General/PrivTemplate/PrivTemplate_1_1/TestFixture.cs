using System;
using System.Data;

using NUnit.Framework;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.DSCalculation;
using Oritax.TaxSimp.Security;
using Oritax.TaxSimp.Services.CMBroker;

namespace Oritax.TaxSimp.Security
{

	[TestFixture] 
	public class PrivTemplateTestFixture
	{
		CMBroker broker;

		Guid templateCID;

		public PrivTemplateTestFixture()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		[SetUp] 
		public void Init()
		{
			broker=new CMBroker();
		} 
    
		[TearDown] 
		public void Dispose()
		{
		}

		[Test]
		public void testPrivTemplateCreate()
		{
			try
			{
				broker.SetStart();
				IBrokerManagedComponent iBMC=broker.CreateBMCInstance("PrivTemplate","MyTemplate");
				templateCID=iBMC.CID;
				PrivTemplateDS privTemplateDS=new PrivTemplateDS();
				DataRow newRow;

				newRow=privTemplateDS.Tables[PrivTemplateDS.PRIVTEMPLATEENTRY_TABLE].NewRow();
				newRow[PrivTemplateDS.PRIVTEMPLATEID_FIELD]=Guid.Empty;
				newRow[PrivTemplateDS.PRIVILEGETYPE_FIELD]=PrivilegeType.EntModifyEntityDetails;
				newRow[PrivTemplateDS.PRIVILEGEVALUE_FIELD]=PrivilegeOption.Allow;
				privTemplateDS.Tables[PrivTemplateDS.PRIVTEMPLATEENTRY_TABLE].Rows.Add(newRow);

				newRow=privTemplateDS.Tables[PrivTemplateDS.PRIVTEMPLATEENTRY_TABLE].NewRow();
				newRow[PrivTemplateDS.PRIVTEMPLATEID_FIELD]=Guid.Empty;
				newRow[PrivTemplateDS.PRIVILEGETYPE_FIELD]=PrivilegeType.CalRemoveCalculationModule;
				newRow[PrivTemplateDS.PRIVILEGEVALUE_FIELD]=PrivilegeOption.Allow;
				privTemplateDS.Tables[PrivTemplateDS.PRIVTEMPLATEENTRY_TABLE].Rows.Add(newRow);

				iBMC.DeliverData(privTemplateDS);

				broker.SetComplete();

				broker.SetStart();
				broker.ClearCache();
				broker.SetComplete();

				broker.SetStart();
				IBrokerManagedComponent iBMC1=broker.GetBMCInstance(templateCID);
				PrivTemplateDS privTemplateDS1=new PrivTemplateDS();
				iBMC1.ExtractData(privTemplateDS1);
				broker.SetComplete();
			}
			catch(Exception ex)
			{
				string message=ex.Message;
				broker.SetAbort();
				throw ex;
			}
		}
	}
}

