using System;

namespace Oritax.TaxSimp.CM.Entity
{
    public class NonIndividualInstall
	{

        #region INSTALLATION PROPERTIES

        // Assembly Installation Properties
        public const string ASSEMBLY_ID = "10E95F10-0C46-4C35-A1E2-182285C7E7CD";
        public const string ASSEMBLY_NAME = "NonIndividual";
        public const string ASSEMBLY_DISPLAYNAME = "NonIndividual";
        public const string ASSEMBLY_MAJORVERSION = "1";
        public const string ASSEMBLY_MINORVERSION = "1";
        public const string ASSEMBLY_DATAFORMAT = "0";
        public const string ASSEMBLY_REVISION = "0";

        // Component Installation Properties
        public const string COMPONENT_ID = "D5627073-2271-4FBA-92D8-ED7B51518DDE";
        public const string COMPONENT_NAME = "NonIndividual";
        public const string COMPONENT_DISPLAYNAME = "NonIndividual";
        public const string COMPONENT_CATEGORY = "BusinessEntity";

        // Component Version Installation Properties
        public const string COMPONENTVERSION_STARTDATE = "01/01/1990 00:00:00 AM";
        public const string COMPONENTVERSION_ENDDATE = "31/12/2050 12:59:59 AM";
        public const string COMPONENTVERSION_PERSISTERASSEMBLY = "CommonPersisters_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";
        public const string COMPONENTVERSION_PERSISTERCLASS = "Oritax.TaxSimp.CommonPersistence.BlobPersister";
        public const string COMPONENTVERSION_IMPLEMENTATIONCLASS = "Oritax.TaxSimp.CM.Entity.NonIndividualCM";

        #endregion

	

	}
}
