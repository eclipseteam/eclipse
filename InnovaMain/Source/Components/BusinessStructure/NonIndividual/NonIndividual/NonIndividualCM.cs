using System;
using System.Data;
using System.Runtime.Serialization;
using System.Security.Permissions;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Enum;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;


namespace Oritax.TaxSimp.CM.Entity
{
    [Serializable]
    public partial class NonIndividualCM : EntityCM, ISerializable, IHasNonIndividualEntity
    {
        private Oritax.TaxSimp.Common.NonIndividualEntity _NonIndividual;

        public Oritax.TaxSimp.Common.NonIndividualEntity NonIndividual
        {
            get { return _NonIndividual; }
            set { _NonIndividual = value; }
        }

        public NonIndividualCM()
            : base()
        {
        }

        protected NonIndividualCM(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            _NonIndividual =
                info.GetValue<Oritax.TaxSimp.Common.NonIndividualEntity>("Oritax.TaxSimp.CM.Entity.NonIndividual");
        }



        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValueX("Oritax.TaxSimp.CM.Entity.NonIndividual", _NonIndividual);
        }

        public override IClientEntity ClientEntity
        {
            get { return this.NonIndividual; }

            set { this.NonIndividual = value as NonIndividualEntity; }
        }

        public override string OnUpdateDataStream(int type, string xml)
        {
            switch ((CmCommand)type)
            {
                case CmCommand.SPAddLibrary:
                    var _value = xml.ToNewOrData<Oritax.TaxSimp.Common.SPLibraryDocument>();
                    if (_value != null)
                    {
                        SPLibrary.AddLibraryToClient(_value.CurrentUser.CurrentUserName, _value.Name);
                    }
                    break;
                case CmCommand.DeleteEntitySecurity:
                    xml = DeleteEntitySecurity(xml);
                    break;
                case CmCommand.AddEntitySecurity:
                    xml = AddEntitySecurity(xml);
                    break;
                case CmCommand.GetEntitySecurity:
                    xml = GetEntitySecurity();
                    break;
                default:
                    Oritax.TaxSimp.Common.NonIndividualEntity NonIndividual =
                        xml.ToNewOrData<Oritax.TaxSimp.Common.NonIndividualEntity>();
                    this.Name = NonIndividual.Name;
                    _NonIndividual = NonIndividual;
                    break;
            }
            return xml;
        }

        public override string GetDataStream(int type, string data)
        {
            string xml = string.Empty;
            switch ((CmCommand)type)
            {
                case CmCommand.GetEntitySecurity:
                    xml = GetEntitySecurity();
                    break;
                case CmCommand.FullName:
                    xml = this._NonIndividual.Name;
                    break;
                default:
                    xml = this._NonIndividual.ToXmlString();
                    if (string.IsNullOrEmpty(ClientId))
                    {
                        UpDateEventLog.UpdateLog("NonIndividual", _NonIndividual.Name, type, this.Broker);
                    }
                    else
                    {
                        UpDateEventLog.UpdateLog("NonIndividual", _NonIndividual.Name + " (" + ClientId + ")", type,
                                                 this.Broker);
                    }
                    break;
            }
            return xml;
        }

        protected override void OnGetData(DataSet data)
        {
            base.OnGetData(data);
            if (data is NonIndividualDS)
            {
                var dataSet = data as NonIndividualDS;
                switch (dataSet.CommandType)
                {
                    case DatasetCommandTypes.Get:
                        GetNonIndividual(data, dataSet);
                        break;
                    case DatasetCommandTypes.Details:

                        DataRow dr = data.Tables[dataSet.NonIndividualTable.TABLENAME].NewRow();
                        dr[dataSet.NonIndividualTable.CLIENTID] = _NonIndividual.ClientId;
                        dr[dataSet.NonIndividualTable.NAME] = _NonIndividual.Name;
                        dr[dataSet.NonIndividualTable.EMAILADDRESS] = _NonIndividual.EmailAddress;

                        dr[dataSet.NonIndividualTable.MOBILEPHONENUMBER] = _NonIndividual.MobilePhoneNumber.MobileNumber;

                        dr[dataSet.NonIndividualTable.TFN] = _NonIndividual.TFN;

                        if (_NonIndividual.DateOfCreation.HasValue)
                        {
                            dr[dataSet.NonIndividualTable.DATEOFCREATION] = _NonIndividual.DateOfCreation.Value;
                        }
                        else
                        {
                            dr[dataSet.NonIndividualTable.DATEOFCREATION] = DBNull.Value;
                        }
                        dr[dataSet.NonIndividualTable.MAILINGADDRESSSAME] = _NonIndividual.MailingAddressSame;
                        dr[dataSet.NonIndividualTable.HASADDRESS] = _NonIndividual.HasAddress;

                        dataSet.Tables[dataSet.NonIndividualTable.TABLENAME].Rows.Add(dr);

                        break;
                }
            }
        }

        private void GetNonIndividual(DataSet data, NonIndividualDS dataSet)
        {
            DataRow dr = data.Tables[dataSet.NonIndividualTable.TABLENAME].NewRow();
            dr[dataSet.NonIndividualTable.CLIENTID] = this.ClientId;
            dr[dataSet.NonIndividualTable.CID] = CID;
            dr[dataSet.NonIndividualTable.CLID] = CLID;
            dr[dataSet.NonIndividualTable.CSID] = CSID;

            dr[dataSet.NonIndividualTable.TYPE] = NonIndividual.Type.ToString();
            dr[dataSet.NonIndividualTable.NAME] = NonIndividual.Name;
            dr[dataSet.NonIndividualTable.EMAILADDRESS] = NonIndividual.EmailAddress;
            dr[dataSet.NonIndividualTable.DESCRIPTION] = NonIndividual.Description;

            dr[dataSet.NonIndividualTable.MOBILECOUNTRYCODE] = NonIndividual.MobilePhoneNumber.CountryCode;
            dr[dataSet.NonIndividualTable.MOBILEPHONENUMBER] = NonIndividual.MobilePhoneNumber.MobileNumber;
            dr[dataSet.NonIndividualTable.OFFICECOUNTRYCODE] = NonIndividual.OfficePhoneNumber.CountryCode;
            dr[dataSet.NonIndividualTable.OFFICECITYCODE] = NonIndividual.OfficePhoneNumber.CityCode;
            dr[dataSet.NonIndividualTable.OFFICEPHONENUMBER] = NonIndividual.OfficePhoneNumber.PhoneNumber;
            dr[dataSet.NonIndividualTable.OFFICECOUNTRYCODE2] = NonIndividual.OfficePhoneNumber2.CountryCode;
            dr[dataSet.NonIndividualTable.OFFICECITYCODE2] = NonIndividual.OfficePhoneNumber2.CityCode;
            dr[dataSet.NonIndividualTable.OFFICEPHONENUMBER2] = NonIndividual.OfficePhoneNumber2.PhoneNumber;
            dr[dataSet.NonIndividualTable.FACSIMILECOUNTRYCODE] = NonIndividual.Facsimile.CountryCode;
            dr[dataSet.NonIndividualTable.FACSIMILECITYCODE] = NonIndividual.Facsimile.CityCode;
            dr[dataSet.NonIndividualTable.FACSIMILEPHONENUMBER] = NonIndividual.Facsimile.PhoneNumber;
            dr[dataSet.NonIndividualTable.TFN] = NonIndividual.TFN;
            dr[dataSet.NonIndividualTable.ABN] = NonIndividual.ABN;

            dr[dataSet.NonIndividualTable.RESIDENTIALADDRESSLINE1] = NonIndividual.ResidentialAddress.Addressline1;
            dr[dataSet.NonIndividualTable.RESIDENTIALADDRESSLINE2] = NonIndividual.ResidentialAddress.Addressline2;
            dr[dataSet.NonIndividualTable.RESIDENTIALADDRESSSUBRUB] = NonIndividual.ResidentialAddress.Suburb;
            dr[dataSet.NonIndividualTable.RESIDENTIALADDRESSPOSTALCODE] = NonIndividual.ResidentialAddress.PostCode;
            dr[dataSet.NonIndividualTable.RESIDENTIALADDRESSSTATE] = NonIndividual.ResidentialAddress.State;
            dr[dataSet.NonIndividualTable.RESIDENTIALADDRESSCOUNTRY] = NonIndividual.ResidentialAddress.Country;

            dr[dataSet.NonIndividualTable.MAILINGADDRESSLINE1] = NonIndividual.MailingAddress.Addressline1;
            dr[dataSet.NonIndividualTable.MAILINGADDRESSLINE2] = NonIndividual.MailingAddress.Addressline2;
            dr[dataSet.NonIndividualTable.MAILINGADDRESSSUBRUB] = NonIndividual.MailingAddress.Suburb;
            dr[dataSet.NonIndividualTable.MAILINGADDRESSPOSTALCODE] = NonIndividual.MailingAddress.PostCode;
            dr[dataSet.NonIndividualTable.MAILINGADDRESSSTATE] = NonIndividual.MailingAddress.State;
            dr[dataSet.NonIndividualTable.MAILINGADDRESSCOUNTRY] = NonIndividual.MailingAddress.Country;


            if (NonIndividual.DateOfCreation.HasValue)
            {
                dr[dataSet.NonIndividualTable.DATEOFCREATION] = NonIndividual.DateOfCreation.Value;
            }
            else
            {
                dr[dataSet.NonIndividualTable.DATEOFCREATION] = DBNull.Value;
            }
            dr[dataSet.NonIndividualTable.MAILINGADDRESSSAME] = NonIndividual.MailingAddressSame;
            dr[dataSet.NonIndividualTable.HASADDRESS] = NonIndividual.HasAddress;

            dataSet.Tables[dataSet.NonIndividualTable.TABLENAME].Rows.Add(dr);
        }

        protected override Calculation.BrokerManagedComponent.ModifiedState OnSetData(System.Data.DataSet data)
        {
            base.OnSetData(data);

            if (data is NonIndividualDS)
            {
                var ds = (NonIndividualDS)data;
                DataRow row = ds.NonIndividualTable.Rows[0];

                switch (ds.CommandType)
                {
                    case DatasetCommandTypes.Add:
                        this._NonIndividual = new NonIndividualEntity();
                        SetNonIndividual(row, ds);
                        break;

                    case DatasetCommandTypes.Update:
                        SetNonIndividual(row, ds);
                        break;

                }
            }

            return ModifiedState.MODIFIED;
        }

        private void SetNonIndividual(DataRow row, NonIndividualDS ds)
        {
            NonIndividual.Type = (NonIndividualsType)System.Enum.Parse(typeof(NonIndividualsType), row[ds.NonIndividualTable.TYPE].ToString());
            NonIndividual.Name = row[ds.NonIndividualTable.NAME].ToString();
            NonIndividual.EmailAddress = row[ds.NonIndividualTable.EMAILADDRESS].ToString();
            if (row[ds.NonIndividualTable.DATEOFCREATION] != DBNull.Value)
                NonIndividual.DateOfCreation = (DateTime)row[ds.NonIndividualTable.DATEOFCREATION];
            else
                NonIndividual.DateOfCreation = null;

            NonIndividual.MobilePhoneNumber = new MobileNumberEntity()
                {
                    CountryCode = row[ds.NonIndividualTable.MOBILECOUNTRYCODE].ToString(),
                    MobileNumber = row[ds.NonIndividualTable.MOBILEPHONENUMBER].ToString()
                };

            NonIndividual.OfficePhoneNumber = new PhoneNumberEntity()
                {
                    CountryCode = row[ds.NonIndividualTable.OFFICECOUNTRYCODE].ToString(),
                    CityCode = row[ds.NonIndividualTable.OFFICECITYCODE].ToString(),
                    PhoneNumber = row[ds.NonIndividualTable.OFFICEPHONENUMBER].ToString()
                };

            NonIndividual.OfficePhoneNumber2 = new PhoneNumberEntity()
                {
                    CountryCode = row[ds.NonIndividualTable.OFFICECOUNTRYCODE2].ToString(),
                    CityCode = row[ds.NonIndividualTable.OFFICECITYCODE2].ToString(),
                    PhoneNumber = row[ds.NonIndividualTable.OFFICEPHONENUMBER2].ToString()
                };

            NonIndividual.Facsimile = new PhoneNumberEntity()
                {
                    CountryCode = row[ds.NonIndividualTable.FACSIMILECOUNTRYCODE].ToString(),
                    CityCode = row[ds.NonIndividualTable.FACSIMILECITYCODE].ToString(),
                    PhoneNumber = row[ds.NonIndividualTable.FACSIMILEPHONENUMBER].ToString()
                };

            NonIndividual.TFN = row[ds.NonIndividualTable.TFN].ToString();
            NonIndividual.ABN = row[ds.NonIndividualTable.ABN].ToString();
            NonIndividual.Description = row[ds.NonIndividualTable.DESCRIPTION].ToString();
            NonIndividual.ResidentialAddress = new Oritax.TaxSimp.Common.AddressEntity()
                {
                    Addressline1 = row[ds.NonIndividualTable.RESIDENTIALADDRESSLINE1].ToString(),
                    Addressline2 = row[ds.NonIndividualTable.RESIDENTIALADDRESSLINE2].ToString(),
                    Suburb = row[ds.NonIndividualTable.RESIDENTIALADDRESSSUBRUB].ToString(),
                    PostCode = row[ds.NonIndividualTable.RESIDENTIALADDRESSPOSTALCODE].ToString(),
                    Country = row[ds.NonIndividualTable.RESIDENTIALADDRESSCOUNTRY].ToString(),
                    State = row[ds.NonIndividualTable.RESIDENTIALADDRESSSTATE].ToString()
                };

            NonIndividual.MailingAddress = new Oritax.TaxSimp.Common.AddressEntity()
                {
                    Addressline1 = row[ds.NonIndividualTable.MAILINGADDRESSLINE1].ToString(),
                    Addressline2 = row[ds.NonIndividualTable.MAILINGADDRESSLINE2].ToString(),
                    Suburb = row[ds.NonIndividualTable.MAILINGADDRESSSUBRUB].ToString(),
                    PostCode = row[ds.NonIndividualTable.MAILINGADDRESSPOSTALCODE].ToString(),
                    Country = row[ds.NonIndividualTable.MAILINGADDRESSCOUNTRY].ToString(),
                    State = row[ds.NonIndividualTable.MAILINGADDRESSSTATE].ToString()
                };

            NonIndividual.MailingAddressSame = Convert.ToBoolean(row[ds.NonIndividualTable.MAILINGADDRESSSAME]);
        }

        private string GetEntitySecurity()
        {
            EntitySecurity entity = new EntitySecurity();

            PartyDS ds = new PartyDS();
            base.OnGetData(ds);
            foreach (DataRow row in ds.Tables[PartyDS.INCLUDEDPARTY_TABLE].Rows)
            {
                entity.IncludedUsers.Add(new UserEntity()
                    {
                        CID = new Guid(row[PartyDS.PARTYCID_FIELD].ToString()),
                        CurrentUserName = row[PartyDS.PARTYNAME_FIELD].ToString()
                    });
            }
            foreach (DataRow row in ds.Tables[PartyDS.AVAILABLEPARTY_TABLE].Rows)
            {
                if (row[PartyDS.PARTYNAME_FIELD].ToString().ToLower() != "Administrator, Administrator".ToLower())
                {
                    entity.AvailableUsers.Add(new UserEntity()
                        {
                            CID = new Guid(row[PartyDS.PARTYCID_FIELD].ToString()),
                            CurrentUserName = row[PartyDS.PARTYNAME_FIELD].ToString()
                        });
                }
            }

            return entity.ToXmlString();
        }

        private string DeleteEntitySecurity(string UserEntity)
        {
            UserEntity entity = UserEntity.ToData<UserEntity>();

            this.Broker.SetWriteStart();

            PartyDS ds = new PartyDS();
            base.OnGetData(ds);
            ds.AcceptChanges();

            DataRow[] drCollection = ds.Tables[PartyDS.INCLUDEDPARTY_TABLE].Select(PartyDS.PARTYCID_FIELD + " = '" + entity.CID + "'", String.Empty);
            drCollection[0].Delete();
            this.SetData(ds);

            return string.Empty;
        }

    }
}