using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Security.Permissions;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Enum;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Utilities;
using OTC = Oritax.TaxSimp.Common;

namespace Oritax.TaxSimp.CM.Group
{
    [Serializable]
    public sealed class CorporateEntityCM : GroupCM, ISerializable,IIdentityCM, ICmHasParent,ICmHasClient
    {
        public IdentityCM Parent { get; set; }
        public bool IsClient { get; set; }                

        private CorporateEntity _Corporate;

        public CorporateEntityCM(): base()
        {
            Parent = IdentityCM.Null;
            IsClient = false;
            _Corporate = new CorporateEntity();
        }

        public CorporateEntityCM(SerializationInfo info, StreamingContext context): base(info, context)
        {
            Parent = info.GetValue<IdentityCM>("Oritax.TaxSimp.CM.Group.CorporateEntityCM.Parent");
            IsClient = info.GetBoolean("Oritax.TaxSimp.CM.Group.CorporateEntityCM.IsClient");
            _Corporate = info.GetValue<CorporateEntity>("Oritax.TaxSimp.CM.Group.CorporateEntityCM.Corporate");
        }

        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValueX("Oritax.TaxSimp.CM.Group.CorporateEntityCM.Parent", Parent);
            info.AddValueX("Oritax.TaxSimp.CM.Group.CorporateEntityCM.Corporate", _Corporate);
            info.AddValue("Oritax.TaxSimp.CM.Group.CorporateEntityCM.IsClient", IsClient);
        }

        public override string GetDataStream(int type, string data)
        {
            return _Corporate.ToXmlString();
        }

        public override string OnUpdateDataStream(int type, string data)
        {
            string xml = string.Empty;
            IsClient = true;
            switch ((CmCommand)type)
            {
                case CmCommand.AttachToParent:
                    if (Parent != IdentityCM.Null) throw new Exception("Client is already part of Advisor");
                    IdentityCM parent = GetParent(data);
                    Parent = parent;
                    break;
                case CmCommand.DeAttachFromParent:
                    Parent = IdentityCM.Null;
                    break;
                default:
                    CorporateEntity corporate = data.ToNewOrData<CorporateEntity>();
                    _Corporate.Clid = CLID;
                    _Corporate.Csid = CSID;

                    _Corporate.Name = Name;
                    _Corporate.InvestorType = corporate.InvestorType;
                    _Corporate.ClientName = corporate.ClientName;
                    _Corporate.TrusteeType = corporate.TrusteeType;
                    _Corporate.CorporateTrusteeName = corporate.CorporateTrusteeName;
                    _Corporate.TrustPassword = corporate.TrustPassword;
                    _Corporate.Country = corporate.Country;
                    _Corporate.ACN = corporate.ACN;
                    _Corporate.ABN = corporate.ABN;
                    _Corporate.TFN = corporate.TFN;
                    _Corporate.TIN = corporate.TIN;
                    _Corporate.AdvisorCode = corporate.AdvisorCode;

                    _Corporate.Contacts.Clear();
                    _Corporate.Contacts.AddRange(corporate.Contacts);
                    xml = _Corporate.ToXmlString();
                    if (ClientId == "" || ClientId == null)
                    {
                        UpDateEventLog.UpdateLog("Corporate Entity", _Corporate.Name, type, this.Broker);
                    }
                    else
                    {
                        UpDateEventLog.UpdateLog("Corporate Entity", _Corporate.Name + " (" + ClientId + ")", type, this.Broker);
                    }
                    
                    break;
            }            
            return xml;
        }

        private IdentityCM GetParent(string data)
        {
            return data.ToIdentity();
        }

    }

    [Serializable]
    public class ContactEntity
    {
        public int Id { get; set; }
        public string ContactName { get; set; }
        public string BusinessNumber { get; set; }
        public string MobileNumber { get; set; }
        public string HomeNumber { get; set; }
        public string Facsimile { get; set; }
        public string EmailAddress { get; set; }
    }

    [Serializable]
    public class CorporateEntity
    {
        public Guid Clid { get; set; }
        public Guid Csid { get; set; }
        public string Name { get; set; }
        public string InvestorType { get; set; }
        public string ClientName { get; set; }
        public string TrusteeType { get; set; }
        public string CorporateTrusteeName { get; set; }
        public string TrustPassword { get; set; }
        public string Country { get; set; }
        public string ACN { get; set; }
        public string ABN { get; set; }
        public string TFN { get; set; }
        public string TIN { get; set; }
        public string AdvisorCode { get; set; }
        public List<ContactEntity> Contacts { get; set; }
        public List<Guid> Signatories { get; set; }
        public CorporateEntity()
        {
            Contacts = new List<ContactEntity>();
            Signatories = new List<Guid>();
        }
    }

}
