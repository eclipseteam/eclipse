#region CHANGE_HISTORY
/*
 $Header: /2002/3. Implementation/NewConstruction/Components/BusinessStructure/ConsolAdjustmentEntity/ConsolAdjustmentEntity-1-0/ConsolAdjus $
 $History: ConsolAdjustmentEntityCM.cs $
 * 
 * *****************  Version 5  *****************
 * User: Secampbell   Date: 2/09/03    Time: 12:31p
 * Updated in $/2002/3. Implementation/NewConstruction/Components/BusinessStructure/ConsolAdjustmentEntity/ConsolAdjustmentEntity-1-0
 * Added ISerialization interface & Migration code
 * 
 * *****************  Version 4  *****************
 * User: Pveitch      Date: 26/06/03   Time: 2:02p
 * Updated in $/2002/3. Implementation/NewConstruction/Components/BusinessStructure/ConsolAdjustmentEntity/ConsolAdjustmentEntity-1-0
 * Updates to bring Australian Group into line with the use cases
 * 
 * *****************  Version 3  *****************
 * User: Paubailey    Date: 28/04/03   Time: 12:38p
 * Updated in $/2002/3. Implementation/Construction/BusinessStructureCM/ConsolAdjustmentEntity/ConsolAdjustmentEntity-1-0
 * Fix for Tax years, SAP
 * 
 * *****************  Version 2  *****************
 * User: Secampbell   Date: 14/04/03   Time: 12:19p
 * Updated in $/2002/3. Implementation/Construction/BusinessStructureCM/ConsolAdjustmentEntity/ConsolAdjustmentEntity-1-0
 * 
 * *****************  Version 3  *****************
 * User: Paubailey    Date: 11/04/03   Time: 11:17a
 * Updated in $/2002/3. Implementation/C2I2 Fixes 2/BusinessStructureCM/ConsolAdjustmentEntity/ConsolAdjustmentEntity-1-0
 * See CQ issue 1463.
 * 
 * *****************  Version 1  *****************
 * User: Paubailey    Date: 6/02/03    Time: 2:45p
 * Created in $/2002/3. Implementation/Construction/BusinessStructureCM/ConsolAdjustmentEntity/ConsolAdjustmentEntity-1-1-1
 * 
 * *****************  Version 1  *****************
 * User: Paubailey    Date: 25/11/02   Time: 4:44p
 * Created in $/2002/3. Implementation/Elaboration4/CM/ConsolAdjustmentEntity
 * New CM created to handle consolidation adjustments. See CQ issue 677.
 * 
 * *****************  Version 2  *****************
 * User: Secampbell   Date: 7/11/02    Time: 11:59a
 * Updated in $/2002/3. Implementation/Elaboration4/CM/AustralianEntity
 * Added Change History Header
*/
#endregion

using System;
using System.Data;
using System.Collections;
using System.Web.UI.WebControls;
using System.Reflection;
using System.Runtime.Serialization;
using System.Security.Permissions;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.DSCalculation;
using Oritax.TaxSimp.CM.Entity;
using Oritax.TaxSimp.CM.Group;
using Oritax.TaxSimp.BusinessStructureUtilities;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.SAPBase;

namespace Oritax.TaxSimp.CM.Entity.ConsolAdjustmentEntity
{
	/// <summary>
	/// Summary description for CalculationModule.
	/// </summary>
	///
	[Serializable]
	public class ConsolAdjustmentEntityCM : EntityCM, ISerializable
	{
		#region INTERNAL CLASSES
		
		#endregion
		#region CONSTRUCTORS

		//USING CUSTOM SERIALIZATION.
		//Still mark the fields as [NonSerialized] for your own infomation but this is ignored now by .NET
		//You must add your own serialzation code for each instance
		//See GetObjectData & Special Constructor

		public ConsolAdjustmentEntityCM()
			: base()
		{
		}

		protected ConsolAdjustmentEntityCM(SerializationInfo si, StreamingContext context)
			:base(si,context)
		{
			// If required to manage state at a future time, add Serialize.Getters here.
		}
	
		//Only allow the .NET Serialization core code to call this function
		[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]
		public override void GetObjectData(SerializationInfo si, StreamingContext context)
		{
			base.GetObjectData(si,context);
		}

		#endregion
		#region STATEFUL VARIABLES
		
		#endregion
		#region PROPERTIES
		public override string ConfigureWPURL{get{return "";}}
		public override IEnumerable WorkPapers
		{
			get
			{
				return null;
			}
		}

		public override ISubstitutedAccountingPeriods SAPs
		{
			get
			{
				
				DataRowCollection consolParentCMRows=(DataRowCollection)this.Broker.GetConsolidationParentsList(this.CLID, this.CSID);
				 
				if(consolParentCMRows.Count > 0)
				{
					Guid gdConsolParentCIID = new Guid(consolParentCMRows[0][DSBMCInfo.CMID_FIELD].ToString());
					IGroup objConsolidationGroup = (IGroup)this.Broker.GetBMCInstance(gdConsolParentCIID);

					try
					{
						return objConsolidationGroup.SAPs;
					}
					finally
					{
						this.Broker.ReleaseBrokerManagedComponent(objConsolidationGroup);
					}
				}
				else
					return null;
			}
		}
		#endregion
		#region OVERRIDE MEMBERS FOR CM BEHAVIOR
		
		public override DataSet PrimaryDataSet{get{return new ConsolAdjustmentEntityDS();}}

		#endregion
		#region OTHER MEMBER FUNCTIONS
		//Override GetTaxYear as the adjustment entity is a virtual entity and needs
		//to navigate back to its owner group to retrieve the tax year info
		public override TaxYear GetTaxYear( DateTime objPeriodStartDate, DateTime objPeriodEndDate )
		{
			//Get a list of the CM's which contain the entity to be deleted as a consolidation member
			DataRowCollection consolParentCMRows=(DataRowCollection)this.Broker.GetConsolidationParentsList(this.CLID, this.CSID);
			if(consolParentCMRows.Count == 0) 
				throw new Exception( "ConsolAdjustmentEntityCM -> GetTaxYear(), Error retrieving adjustment's parent " );
			else
			{
				IOrganizationUnit groupCM = (IOrganizationUnit)this.Broker.GetBMCInstance((Guid)consolParentCMRows[0][DSBMCInfo.CMID_FIELD]);

				try
				{
					return groupCM.GetTaxYear(objPeriodStartDate,objPeriodEndDate);
				}
				finally
				{
					this.Broker.ReleaseBrokerManagedComponent(groupCM);
				}
			}
		}

		//Override GetFinancialYear as the adjustment entity is a virtual entity and needs
		//to navigate back to its owner group to retrieve the financial year info
		public override FinancialYear GetFinancialYear( DateTime objPeriodStartDate, DateTime objPeriodEndDate )
		{
			//Get a list of the CM's which contain the entity to be deleted as a consolidation member
			DataRowCollection consolParentCMRows=(DataRowCollection)this.Broker.GetConsolidationParentsList(this.CLID, this.CSID);
			if(consolParentCMRows.Count == 0) 
				throw new Exception( "ConsolAdjustmentEntityCM -> GetFinancialYear(), Error retrieving adjustment's parent " );
			else
			{
				IOrganizationUnit groupCM = (IOrganizationUnit)this.Broker.GetBMCInstance((Guid)consolParentCMRows[0][DSBMCInfo.CMID_FIELD]);

				try
				{
					return groupCM.GetFinancialYear(objPeriodStartDate,objPeriodEndDate);
				}
				finally
				{
					this.Broker.ReleaseBrokerManagedComponent(groupCM);
				}
			}
		}

		#endregion
	}
}
