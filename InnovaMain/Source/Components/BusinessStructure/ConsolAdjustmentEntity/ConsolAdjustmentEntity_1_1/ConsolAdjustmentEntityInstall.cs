using System;

namespace Oritax.TaxSimp.CM.Entity.ConsolAdjustmentEntity
{
	/// <summary>
	/// Summary description for ConsolAdjustmentEntityInstall.
	/// </summary>
	public class ConsolAdjustmentEntityInstall
	{
		#region INSTALLATION PROPERTIES

		// Assembly Installation Properties
		public const string ASSEMBLY_ID="5A5BC2D6-50CB-412A-B151-BF8E16E17403";
		public const string ASSEMBLY_NAME="ConsolAdjustmentEntity_1_1";
		public const string ASSEMBLY_DISPLAYNAME="Consolidation adjustment entity V1.1";
		public const string ASSEMBLY_MAJORVERSION="1";
		public const string ASSEMBLY_MINORVERSION="1";
		public const string ASSEMBLY_DATAFORMAT="0";
//		public const string ASSEMBLY_REVISION="0";  
		public const string ASSEMBLY_REVISION="1"; //2005.1  

		// Component Installation Properties
		public const string COMPONENT_ID="D5D34D91-5BD5-4ca5-8353-9478299E4188";
		public const string COMPONENT_NAME="ConsolAdjustmentEntity";
		public const string COMPONENT_DISPLAYNAME="Consolidation adjustment entity";
		public const string COMPONENT_CATEGORY="BusinessEntity";

		// Component Version Installation Properties
		public const string COMPONENTVERSION_STARTDATE="01/01/1990 00:00:00 AM";
		public const string COMPONENTVERSION_ENDDATE="31/12/2050 12:59:59 AM";
		public const string COMPONENTVERSION_PERSISTERASSEMBLY="CommonPersisters_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";
		public const string COMPONENTVERSION_PERSISTERCLASS="Oritax.TaxSimp.CommonPersistence.BlobPersister";
		public const string COMPONENTVERSION_IMPLEMENTATIONCLASS="Oritax.TaxSimp.CM.Entity.ConsolAdjustmentEntity.ConsolAdjustmentEntityCM";

		#endregion
	}
}
