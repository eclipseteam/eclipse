#region CHANGE_HISTORY
/*
 $Header: /2002/3. Implementation/NewConstruction/Components/BusinessStructure/ConsolAdjustmentEntity/ConsolAdjustmentEntity-1-0/ConsolAdjus $
 $History: ConsolAdjustmentEntityDS.cs $
 * 
 * *****************  Version 1  *****************
 * User: Secampbell   Date: 2/09/03    Time: 12:31p
 * Created in $/2002/3. Implementation/NewConstruction/Components/BusinessStructure/ConsolAdjustmentEntity/ConsolAdjustmentEntity-1-0
 * 
 * *****************  Version 1  *****************
 * User: Paubailey    Date: 6/02/03    Time: 2:45p
 * Created in $/2002/3. Implementation/Construction/BusinessStructureCM/ConsolAdjustmentEntity/ConsolAdjustmentEntity-1-1-1
 * 
 * *****************  Version 1  *****************
 * User: Paubailey    Date: 25/11/02   Time: 4:44p
 * Created in $/2002/3. Implementation/Elaboration4/CM/ConsolAdjustmentEntity
 * New CM created to handle consolidation adjustments. See CQ issue 677.
 * 
 * *****************  Version 2  *****************
 * User: Secampbell   Date: 7/11/02    Time: 11:59a
 * Updated in $/2002/3. Implementation/Elaboration4/CM/AustralianEntity
 * Added Change History Header
*/
#endregion

using System;
using System.Data;

namespace Oritax.TaxSimp.CM.Entity.ConsolAdjustmentEntity
{
	/// <summary>
	/// DSCalculationModule is a DataSet that contains all of the persistent data for a CM instance.
	/// </summary>
	[Serializable] 
	public class ConsolAdjustmentEntityDS : EntityDS
	{
		public ConsolAdjustmentEntityDS() : base()
		{
			
		}
	}
}

