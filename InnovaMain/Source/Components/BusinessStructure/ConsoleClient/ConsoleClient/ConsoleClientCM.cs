using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using Aspose.Words;
using Aspose.Words.Tables;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.Entity;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Enum;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.IOrganizationUnits;
using Oritax.TaxSimp.Utilities;
using AddressEntity = Oritax.TaxSimp.Common.AddressEntity;
using IdentityCMDetail = Oritax.TaxSimp.Common.IdentityCMDetail;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Common.Data;


namespace Oritax.TaxSimp.CM.Group
{
    [Serializable]
    public sealed class ConsoleClientCM : GroupCM, ISerializable, IIdentityCM
    {
        #region PROPERTIES
        public IdentityCM Parent { get; set; }
        public bool IsClient { get; set; }
        private List<ClientFeeDistribution> clientFeeDistributionList = new List<ClientFeeDistribution>();
        #endregion 

        public List<ClientFeeDistribution> ClientFeeDistributionList
        {
            get { return clientFeeDistributionList; }
        }

        #region PRIVATE VARIABLES
        private ConsoleClient _entity;
        #endregion

        #region CONSTRUCTOR
        public ConsoleClientCM()
            : base()
        {
            Aspose.Words.License license = new Aspose.Words.License();
            license.SetLicense("Aspose.Words.lic");

            Parent = IdentityCM.Null;
            IsClient = false;
            _entity = new ConsoleClient();
        }

        public ConsoleClientCM(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            Parent = info.GetValue<IdentityCM>("Oritax.TaxSimp.CM.Group.ConsoleClientCM.Parent");
            IsClient = info.GetBoolean("Oritax.TaxSimp.CM.Group.ConsoleClientCM.IsClient");
            _entity = info.GetValue<ConsoleClient>("Oritax.TaxSimp.CM.Group.ConsoleClientCM.Corporate");
            clientFeeDistributionList = info.GetValue<List<ClientFeeDistribution>>("ClientFeeDistributionList");
        }

        #endregion

        #region OVERRIDE METHODS
        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);

            info.AddValueX("Oritax.TaxSimp.CM.Group.ConsoleClientCM.Parent", Parent);
            info.AddValueX("Oritax.TaxSimp.CM.Group.ConsoleClientCM.Corporate", _entity);
            info.AddValue("Oritax.TaxSimp.CM.Group.ConsoleClientCM.IsClient", IsClient);
            info.AddValue("ClientFeeDistributionList", clientFeeDistributionList);
        }

        public override string GetDataStream(int type, string data)
        {
            string xml = string.Empty;

            _entity.IsActive = this.IsInvestableClient;
            _entity.IsClient = _entity.IsActive;
            _entity.OrganizationStatus = this.OrganizationStatus;
            _entity.ClientId = this.ClientId;
            xml = _entity.ToXmlString();


            return xml;
        }

        public override string OnUpdateDataStream(int type, string data)
        {
            string xml = string.Empty;
            //IdentityCM parent = null;
            xml = (this as IOrganizationUnit).OnUpdateClient(type, data);
            if (!string.IsNullOrEmpty(xml)) return xml;

            IsClient = true;


            _entity = data.ToNewOrData<ConsoleClient>();
            _entity.Clid = CLID;
            _entity.Csid = CSID;
            this.IsInvestableClient = _entity.IsActive;
            _entity.IsClient = _entity.IsActive;
            this.OrganizationStatus = _entity.OrganizationStatus;
            this.Name = _entity.Name;

            UpDateEventLog.UpdateLog("Console Client", _entity.Name + " (" + EclipseClientID + ")", type, this.Broker);

            xml = _entity.ToXmlString();

            return xml;

        }



        #endregion

        #region PRIVATE METHODS
        private IdentityCM GetParent(string data)
        {
            return data.ToIdentity();
        }

        private void GetParent()
        {
            var advisor = this.GetParentReportingUnit();
        }

        private void AddRemoveParent(CmCommand optype, IdentityCM parent)
        {
            ILogicalModule parentLM = Broker.GetLogicalCM(parent.Clid);
            ICalculationModule parentCM = (ICalculationModule)this.Broker.GetCMImplementation(parent.Clid, parent.Csid);
            ILogicalModule CurentClientLM = Broker.GetLogicalCM(this.Clid);
            switch (optype)
            {
                case CmCommand.AttachToParent:
                    parentLM.AddChildLogicalCM(CurentClientLM, parentCM.CSID);
                    break;
                case CmCommand.DeAttachFromParent:
                    parentLM.RemoveChildLogicalCM(CurentClientLM, parentCM.CSID);
                    break;
            }
        }

        #endregion

        #region Security

        private string GetEntitySecurity()
        {
            EntitySecurity entity = new EntitySecurity();

            PartyDS ds = new PartyDS();
            base.OnGetData(ds);
            foreach (DataRow row in ds.Tables[PartyDS.INCLUDEDPARTY_TABLE].Rows)
            {
                entity.IncludedUsers.Add(new UserEntity() { CID = new Guid(row[PartyDS.PARTYCID_FIELD].ToString()), CurrentUserName = row[PartyDS.PARTYNAME_FIELD].ToString() });
            }
            foreach (DataRow row in ds.Tables[PartyDS.AVAILABLEPARTY_TABLE].Rows)
            {
                if (row[PartyDS.PARTYNAME_FIELD].ToString().ToLower() != "Administrator, Administrator".ToLower())
                {
                    entity.AvailableUsers.Add(new UserEntity() { CID = new Guid(row[PartyDS.PARTYCID_FIELD].ToString()), CurrentUserName = row[PartyDS.PARTYNAME_FIELD].ToString() });
                }
            }

            return entity.ToXmlString();
        }

        //private string AddEntitySecurity(string UserEntity)
        //{
        //    UserEntity newentity = UserEntity.ToData<UserEntity>();

        //    this.Broker.SetWriteStart(); 

        //    PartyDS ds = new PartyDS();
        //    base.OnGetData(ds);

        //    DataRow partyRow = ds.Tables[PartyDS.INCLUDEDPARTY_TABLE].NewRow();
        //    partyRow[PartyDS.PARTYNAME_FIELD] = newentity.CurrentUserName;
        //    partyRow[PartyDS.PARTYCID_FIELD] = newentity.CID;
        //    ds.Tables[PartyDS.INCLUDEDPARTY_TABLE].Rows.Add(partyRow);

        //    this.SetData(ds);

        //    return string.Empty;
        //}

        private string DeleteEntitySecurity(string UserEntity)
        {
            UserEntity entity = UserEntity.ToData<UserEntity>();

            this.Broker.SetWriteStart();

            PartyDS ds = new PartyDS();
            base.OnGetData(ds);
            ds.AcceptChanges();

            DataRow[] drCollection = ds.Tables[PartyDS.INCLUDEDPARTY_TABLE].Select(PartyDS.PARTYCID_FIELD + " = '" + entity.CID + "'", String.Empty);
            drCollection[0].Delete();
            this.SetData(ds);

            return string.Empty;
        }

        #endregion

        public ConsoleClient Entity { get { return _entity; } set { _entity = value; } }
        public string XName { get { return "ConsoleClient"; } }

        #region OnSetData
        protected override BrokerManagedComponent.ModifiedState OnSetData(DataSet data)
        {
            base.OnSetData(data);

            if (data is ConsolidationFeeDistributionDS)
            {
                ConsolidationFeeDistributionDS consolidationFeeDistributionDS = (ConsolidationFeeDistributionDS)data;
                
                if (consolidationFeeDistributionDS.DataSetOperationType == DataSetOperationType.UpdateBulk)
                {
                    GroupMembersDS groupMemberDS = new GroupMembersDS();
                    this.GetData(groupMemberDS);

                    foreach (DataRow memberRow in groupMemberDS.Tables[GroupMembersDS.INCLUDEDENTITIES_TABLE].Rows)
                    {
                        Guid clid = new Guid(memberRow[GroupMembersDS.ENTITYCLID_FIELD].ToString());
                        Guid csid = new Guid(memberRow[GroupMembersDS.ENTITYCSID_FIELD].ToString());
                        IOrganizationUnit orgUnit = this.Broker.GetCMImplementation(clid, csid) as IOrganizationUnit;

                        ClientFeeDistribution clientFeeDistribution = new ClientFeeDistribution();
                        clientFeeDistribution.ClientCID = orgUnit.CID;
                        clientFeeDistribution.ClientID = orgUnit.EclipseClientID;
                        clientFeeDistribution.ClientName = orgUnit.Name;
                        clientFeeDistribution.FeeDistribution = 0;

                        if (clientFeeDistributionList.Where(t => t.ClientCID == orgUnit.CID).FirstOrDefault() == null)
                            this.clientFeeDistributionList.Add(clientFeeDistribution);
                    }
                }
                else if (consolidationFeeDistributionDS.DataSetOperationType == DataSetOperationType.UpdateSingle)
                {
                    var feeDistributionToSet = clientFeeDistributionList.Where(t => t.ClientCID == consolidationFeeDistributionDS.OrgCID).FirstOrDefault();

                    if (feeDistributionToSet != null)
                        feeDistributionToSet.FeeDistribution = consolidationFeeDistributionDS.DistributionPercentage; 
                }
            }

            if (data is ConsolidationFeeDS)
            {
                ConsolidationFeeDS consolidationFeeDS = (ConsolidationFeeDS)data;

                ConsolidationFeeRatioDS consolidationFeeRatioDS = new ConsolidationFeeRatioDS();

                IFeeRun feeRun = Broker.GetBMCInstance(consolidationFeeDS.FeeRunID) as IFeeRun;

                consolidationFeeRatioDS.FeeRunMonth = new DateTime(feeRun.Year, feeRun.Month, 1);
                consolidationFeeRatioDS.FeeRunID = feeRun.CID;
                GroupMembersDS groupMemberDS = new GroupMembersDS();
                this.GetData(groupMemberDS);

                foreach (DataRow memberRow in groupMemberDS.Tables[GroupMembersDS.INCLUDEDENTITIES_TABLE].Rows)
                {
                    Guid clid = new Guid(memberRow[GroupMembersDS.ENTITYCLID_FIELD].ToString());
                    Guid csid = new Guid(memberRow[GroupMembersDS.ENTITYCSID_FIELD].ToString());
                    IOrganizationUnit orgUnit = this.Broker.GetCMImplementation(clid, csid) as IOrganizationUnit;
                    orgUnit.GetData(consolidationFeeRatioDS);
                }

                consolidationFeeRatioDS.CalculateFeeRatio();
                consolidationFeeDS.FeeRatios = consolidationFeeRatioDS.FeeRatios;

                foreach (DataRow memberRow in groupMemberDS.Tables[GroupMembersDS.INCLUDEDENTITIES_TABLE].Rows)
                {
                    consolidationFeeDS.DataSetOperationType = DataSetOperationType.UpdateSingle;
                    Guid clid = new Guid(memberRow[GroupMembersDS.ENTITYCLID_FIELD].ToString());
                    Guid csid = new Guid(memberRow[GroupMembersDS.ENTITYCSID_FIELD].ToString());
                    IOrganizationUnit orgUnit = this.Broker.GetCMImplementation(clid, csid) as IOrganizationUnit;
                    orgUnit.SetData(consolidationFeeDS);
                    orgUnit.CalculateToken(true);
                }

                this.GetData(consolidationFeeDS);
             
                decimal totalFeeAdjust = consolidationFeeDS.Tables[ConsolidationFeeDS.CONSOLIDATIONFEETABLE].Select().Sum(c => (decimal)c[ConsolidationFeeDS.FEETOTALADJUST]);
                var feeGroup = consolidationFeeDS.Tables[ConsolidationFeeDS.CONSOLIDATIONFEETABLE].Select().GroupBy(c => c[ConsolidationFeeDS.CLIENTCID]);

                foreach (var groupItem in feeGroup)
                {
                    decimal subFeeTotal = groupItem.Sum(c => (decimal)c[ConsolidationFeeDS.FEETOTALADJUST]);
                    decimal distribution = clientFeeDistributionList.Where(c => c.ClientCID.ToString() == groupItem.Key.ToString()).FirstOrDefault().FeeDistribution;
                    foreach (var subFeeItem in groupItem)
                    {
                        decimal adjustedFeeTotal = (decimal)subFeeItem[ConsolidationFeeDS.FEETOTALADJUST];
                        decimal manTotalAdjustFee = (adjustedFeeTotal / subFeeTotal) * (totalFeeAdjust * (distribution / 100));
                        decimal manAdjustment = manTotalAdjustFee - adjustedFeeTotal;
                        subFeeItem[ConsolidationFeeDS.FEETOTALMANADJUST] = manAdjustment;
                    }
                }

                foreach (DataRow memberRow in groupMemberDS.Tables[GroupMembersDS.INCLUDEDENTITIES_TABLE].Rows)
                {
                    consolidationFeeDS.DataSetOperationType = DataSetOperationType.UpdateBulk;
                    Guid clid = new Guid(memberRow[GroupMembersDS.ENTITYCLID_FIELD].ToString());
                    Guid csid = new Guid(memberRow[GroupMembersDS.ENTITYCSID_FIELD].ToString());
                    IOrganizationUnit orgUnit = this.Broker.GetCMImplementation(clid, csid) as IOrganizationUnit;
                    orgUnit.SetData(consolidationFeeDS);
                    orgUnit.CalculateToken(true);
                }
            }

            return ModifiedState.UNCHANGED;
        }
        #endregion

        #region OnGetData

        protected override void OnGetData(DataSet data)
        {
            base.OnGetData(data);

            FinancialDataUtility FinancialDataUtility = new FinancialDataUtility();
            HoldingDataReportUtility HoldingDataReportUtility = new HoldingDataReportUtility();
            CapitalFlowReportUtility CapitalFlowReportUtility = new CapitalFlowReportUtility();
            PerformanceFlowReportUtility PerformanceFlowReportUtility = new PerformanceFlowReportUtility();
            SecurityDataReportUtility securityDataReportUtility = new SecurityDataReportUtility();
            GainsAndLossReportUtility gainsAndLossReportUtility = new GainsAndLossReportUtility();
            HoldingStatementReportUtility holdingStatementReportUtility = new HoldingStatementReportUtility();

            if (data is HoldingRptDataSet)
            {
                HoldingRptDataSet masterholdingRptDataSet = data as HoldingRptDataSet;

                GroupMembersDS groupMemberDS = new GroupMembersDS();
                this.GetData(groupMemberDS);
                foreach (DataRow memberRow in groupMemberDS.Tables[GroupMembersDS.INCLUDEDENTITIES_TABLE].Rows)
                {
                    Guid clid = new Guid(memberRow[GroupMembersDS.ENTITYCLID_FIELD].ToString());
                    Guid csid = new Guid(memberRow[GroupMembersDS.ENTITYCSID_FIELD].ToString());

                    IOrganizationUnit orgUnit = this.Broker.GetCMImplementation(clid, csid) as IOrganizationUnit;
                    HoldingRptDataSet holdingDS = new HoldingRptDataSet();
                    holdingDS.ValuationDate = masterholdingRptDataSet.ValuationDate;
                    orgUnit.GetData(holdingDS);
                    masterholdingRptDataSet.Tables[masterholdingRptDataSet.ProductBreakDownTable.TableName].Merge(holdingDS.Tables[masterholdingRptDataSet.ProductBreakDownTable.TableName]);
                    masterholdingRptDataSet.Tables[masterholdingRptDataSet.HoldingSummaryTable.TableName].Merge(holdingDS.Tables[masterholdingRptDataSet.HoldingSummaryTable.TableName]);
                    masterholdingRptDataSet.Tables[masterholdingRptDataSet.TDBreakDownTable.TableName].Merge(holdingDS.Tables[masterholdingRptDataSet.TDBreakDownTable.TableName]);
                }
                DataTable clientSummaryTable = masterholdingRptDataSet.Tables[HoldingRptDataSet.CLIENTSUMMARYTABLE];
                DataRow clientSummaryRow = clientSummaryTable.NewRow();
                HoldingDataReportUtility.BasicClientDetails(ConsoleClientCMInstall.COMPONENT_DISPLAYNAME, this.EclipseClientID, this.Name, this.Parent, Broker, "", "", new Common.DualAddressEntity(), this.ClientApplicabilityType, clientSummaryRow, CID);
                clientSummaryTable.Rows.Add(clientSummaryRow);
            }
            if (data is ConsolidationFeeDS)
            {
                ConsolidationFeeDS consolidationFeeDS = (ConsolidationFeeDS)data;

                GroupMembersDS groupMemberDS = new GroupMembersDS();
                this.GetData(groupMemberDS);

                foreach (DataRow memberRow in groupMemberDS.Tables[GroupMembersDS.INCLUDEDENTITIES_TABLE].Rows)
                {
                    Guid clid = new Guid(memberRow[GroupMembersDS.ENTITYCLID_FIELD].ToString());
                    Guid csid = new Guid(memberRow[GroupMembersDS.ENTITYCSID_FIELD].ToString());
                    IOrganizationUnit orgUnit = this.Broker.GetCMImplementation(clid, csid) as IOrganizationUnit;
                    orgUnit.GetData(consolidationFeeDS);
                }
            }

            else if (data is BankTransactionDS)
            {
                BankTransactionDS masterBankTransactionDS = data as BankTransactionDS;

                GroupMembersDS groupMemberDS = new GroupMembersDS();
                this.GetData(groupMemberDS);
                foreach (DataRow memberRow in groupMemberDS.Tables[GroupMembersDS.INCLUDEDENTITIES_TABLE].Rows)
                {

                    Guid clid = new Guid(memberRow[GroupMembersDS.ENTITYCLID_FIELD].ToString());
                    Guid csid = new Guid(memberRow[GroupMembersDS.ENTITYCSID_FIELD].ToString());

                    IOrganizationUnit orgUnit = this.Broker.GetCMImplementation(clid, csid) as IOrganizationUnit;
                    BankTransactionDS bankTransactionDS = new BankTransactionDS();
                    orgUnit.GetData(bankTransactionDS);
                    masterBankTransactionDS.Merge(bankTransactionDS);
                    this.Broker.ReleaseBrokerManagedComponent(orgUnit);
                }
            }

            else if (data is ClientDetailsDS)
            {
                ClientDetailsDS clientDetailsDS = (ClientDetailsDS)data;
                Entity.Name = this.Name;
                ClientDetailsUtilities.GetClientDetailsDs(this.ClientId, clientDetailsDS, Entity, Broker, TypeName);
            }

            else if (data is ConsolidationFeeDistributionDS)
            {

                ConsolidationFeeDistributionDS consolidationFeeDistributionDS = (ConsolidationFeeDistributionDS)data;
                foreach (ClientFeeDistribution feeDistribution in clientFeeDistributionList)
                {
                    DataRow row = consolidationFeeDistributionDS.Tables[ConsolidationFeeDistributionDS.CONSOLIDATIONFEEDISTRIBUTIONTABLE].NewRow();
                    row[ConsolidationFeeDistributionDS.CLIENTCID] = feeDistribution.ClientCID;
                    row[ConsolidationFeeDistributionDS.CLIENTID] = feeDistribution.ClientID;
                    row[ConsolidationFeeDistributionDS.CLIENTNAME] = feeDistribution.ClientName;
                    row[ConsolidationFeeDistributionDS.DISTRIBUTEDVALUE] = feeDistribution.FeeDistribution;

                    consolidationFeeDistributionDS.Tables[ConsolidationFeeDistributionDS.CONSOLIDATIONFEEDISTRIBUTIONTABLE].Rows.Add(row); 
                }
            }
        }

        #endregion

        public override bool IsConsolCM
        {
            get
            {
                return true;
            }
        }
    }
}