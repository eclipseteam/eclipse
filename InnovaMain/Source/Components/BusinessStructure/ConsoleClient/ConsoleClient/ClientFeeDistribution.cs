﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.CM.Group
{
    [Serializable]
    public class ClientFeeDistribution
    {
        string clientName = string.Empty;

        public string ClientName
        {
            get { return clientName; }
            set { clientName = value; }
        }

        Guid clientCID = Guid.Empty;

        public Guid ClientCID
        {
            get { return clientCID; }
            set { clientCID = value; }
        }

        string clientID = string.Empty;

        public string ClientID
        {
            get { return clientID; }
            set { clientID = value; }
        }

        decimal feeDistribution = 0;

        public decimal FeeDistribution
        {
            get { return feeDistribution; }
            set { feeDistribution = value; }
        }
    }
}
