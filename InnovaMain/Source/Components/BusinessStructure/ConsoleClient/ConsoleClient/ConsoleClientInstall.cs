namespace Oritax.TaxSimp.CM.Group
{
    public class ConsoleClientCMInstall
	{
		#region INSTALLATION PROPERTIES

		// Assembly Installation Properties
       
        public const string ASSEMBLY_ID = "D565CDB2-D71D-46f6-8ADB-080E981EBA7E";
        public const string ASSEMBLY_NAME = "ConsoleClient";
        public const string ASSEMBLY_DISPLAYNAME = "Console Client";
		public const string ASSEMBLY_MAJORVERSION="1";
		public const string ASSEMBLY_MINORVERSION="0";
        public const string ASSEMBLY_DATAFORMAT="0"; 
		public const string ASSEMBLY_REVISION="0"; 

     
		// Component Installation Properties
        public const string COMPONENT_ID = "D5E72B84-EB01-4060-8746-80ADF576D750";
        public const string COMPONENT_NAME = "ConsoleClient";
        public const string COMPONENT_DISPLAYNAME = "Consolidation Group";
		public const string COMPONENT_CATEGORY="BusinessEntity";

		// Component Version Installation Properties
		public const string COMPONENTVERSION_STARTDATE="01/01/1900 00:00:00 AM";
		public const string COMPONENTVERSION_ENDDATE="31/12/2050 12:59:59 AM";
		public const string COMPONENTVERSION_PERSISTERASSEMBLY="CommonPersisters_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";
		public const string COMPONENTVERSION_PERSISTERCLASS="Oritax.TaxSimp.CommonPersistence.GroupPersister";
        public const string COMPONENTVERSION_IMPLEMENTATIONCLASS = "Oritax.TaxSimp.CM.Group.ConsoleClientCM";
        	

		// Data Model
		public const string DATAMODEL_MANAGERASSEMBLY="CommonPersisters_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";
		public const string DATAMODEL_MANAGERCLASS="Oritax.TaxSimp.CommonPersistence.GroupPersister";

		#endregion
	}
}