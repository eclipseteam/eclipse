using System;

namespace Oritax.TaxSimp.CM.Entity
{
    public class DesktopBrokerAccountInstall
	{
		#region INSTALLATION PROPERTIES
        
		// Assembly Installation Properties
        public const string ASSEMBLY_ID = "8AA0A349-B5EC-4A32-9E39-7538606D2F70";
        public const string ASSEMBLY_NAME = "DesktopBrokerAccount";
        public const string ASSEMBLY_DISPLAYNAME = "Desktop Broker Account";
		public const string ASSEMBLY_MAJORVERSION="1";
		public const string ASSEMBLY_MINORVERSION="0";
		public const string ASSEMBLY_DATAFORMAT="0";
		public const string ASSEMBLY_REVISION="0";		

        public const string COMPONENT_ID = "80291F22-DF69-48BA-B2B9-09F1F0E5D1F6";
        public const string COMPONENT_NAME = "DesktopBrokerAccount";
        public const string COMPONENT_DISPLAYNAME = "Desktop Broker Account";
		public const string COMPONENT_CATEGORY="BusinessEntity";

		// Component Version Installation Properties
		public const string COMPONENTVERSION_STARTDATE="01/01/1990 00:00:00 AM";
		public const string COMPONENTVERSION_ENDDATE="31/12/2050 12:59:59 AM";
		public const string COMPONENTVERSION_PERSISTERASSEMBLY="CommonPersisters_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";
		public const string COMPONENTVERSION_PERSISTERCLASS="Oritax.TaxSimp.CommonPersistence.BlobPersister";
		public const string COMPONENTVERSION_IMPLEMENTATIONCLASS="Oritax.TaxSimp.CM.Entity.DesktopBrokerAccountCM";
		#endregion
	}
}
