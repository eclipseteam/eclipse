﻿using System.Reflection;

using Oritax.TaxSimp.InstallInfo;
using Oritax.TaxSimp.CM.Entity;

[assembly: AssemblyTitle("")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("")]
[assembly: AssemblyCopyright("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: AssemblyVersion(DesktopBrokerAccountInstall.ASSEMBLY_MAJORVERSION + "." + DesktopBrokerAccountInstall.ASSEMBLY_MINORVERSION)]
[assembly: AssemblyInformationalVersion(DesktopBrokerAccountInstall.ASSEMBLY_MAJORVERSION + "." + DesktopBrokerAccountInstall.ASSEMBLY_MINORVERSION + "." + DesktopBrokerAccountInstall.ASSEMBLY_DATAFORMAT + "." + DesktopBrokerAccountInstall.ASSEMBLY_REVISION)]
[assembly: AssemblyInstallInfo(DesktopBrokerAccountInstall.ASSEMBLY_ID, DesktopBrokerAccountInstall.ASSEMBLY_NAME, DesktopBrokerAccountInstall.ASSEMBLY_DISPLAYNAME)]
[assembly: ComponentInstallInfo(DesktopBrokerAccountInstall.COMPONENT_ID, DesktopBrokerAccountInstall.COMPONENT_NAME, DesktopBrokerAccountInstall.COMPONENT_DISPLAYNAME, DesktopBrokerAccountInstall.COMPONENT_CATEGORY)]
[assembly: ComponentVersionInstallInfo(DesktopBrokerAccountInstall.COMPONENTVERSION_STARTDATE, DesktopBrokerAccountInstall.COMPONENTVERSION_ENDDATE, DesktopBrokerAccountInstall.COMPONENTVERSION_PERSISTERASSEMBLY, DesktopBrokerAccountInstall.COMPONENTVERSION_PERSISTERCLASS, DesktopBrokerAccountInstall.COMPONENTVERSION_IMPLEMENTATIONCLASS)]

[assembly: AssemblyDelaySign(false)]
