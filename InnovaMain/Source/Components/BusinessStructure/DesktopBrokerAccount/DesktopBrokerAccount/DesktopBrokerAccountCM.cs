using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Enum;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.IOrganizationUnits;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.CalculationInterface;
using System.Linq;
using System.Collections.ObjectModel;
using System.Globalization;
using Oritax.TaxSimp.DataSets;
using System.Data;

namespace Oritax.TaxSimp.CM.Entity
{
    [Serializable]
    public partial class DesktopBrokerAccountCM : EntityCM, ISerializable, IDesktopBrokerAccount, IOrganizationChartData, IHaveAssociation, IHasImportTransaction
    {
        #region Private Variable
        private DesktopBrokerAccountEntity _DesktopBrokerAccount;
        #endregion

        #region Public Properties
        public DesktopBrokerAccountEntity DesktopBrokerAccountEntity
        {
            get { return _DesktopBrokerAccount; }
        }
        #endregion

        #region Constructor
        public DesktopBrokerAccountCM() : base() { }

        protected DesktopBrokerAccountCM(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            _DesktopBrokerAccount = info.GetValue<DesktopBrokerAccountEntity>("Oritax.TaxSimp.CM.Entity.DesktopBrokerAccount");
        }
        #endregion

        #region Override Method
        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValueX("Oritax.TaxSimp.CM.Entity.DesktopBrokerAccount", _DesktopBrokerAccount);
        }

        public override string OnUpdateDataStream(int type, string data)
        {

            string xml = string.Empty;
            switch ((CmCommand)type)
            {
                case CmCommand.AccountRelationAdd:
                    xml = AddAccountRelation(data);
                    break;
                case CmCommand.AccountRelationAddReference:
                    xml = AddAccountRelationReference(data);
                    break;
                case CmCommand.AccountRelationDelete:
                    DeleteAccountRelation(data);
                    break;
                case CmCommand.AccountRelationDeleteRefrence:
                    DeleteAccountRelationReference(data);
                    break;
                default:
                    DesktopBrokerAccountEntity entity = data.ToNewOrData<DesktopBrokerAccountEntity>();
                    this.Name = entity.Name;
                    this.OrganizationStatus = entity.OrganizationStatus;
                    _DesktopBrokerAccount = entity;


                    if (ClientId == "" || ClientId == null)
                    {
                        UpDateEventLog.UpdateLog("ASX Broker Account", _DesktopBrokerAccount.Name, type, this.Broker);
                    }
                    else
                    {
                        UpDateEventLog.UpdateLog("ASX Broker Account", _DesktopBrokerAccount.Name + " (" + ClientId + ")", type, this.Broker);
                    }

                    return data;
            }
            return xml;


        }

        public override string GetDataStream(int type, string data)
        {
            string xml = string.Empty;
            switch ((CmCommand)type)
            {
                case CmCommand.AccountRelationGetList:
                    xml = this._DesktopBrokerAccount.LinkedAccounts.ToXmlString();
                    break;
                case CmCommand.AccountRelationGetEntity:
                    var entity = new AccountRelation();
                    entity.AccountName = this._DesktopBrokerAccount.Name + "-" + this._DesktopBrokerAccount.AccountNumber;
                    entity.AccountType = this.TypeName;
                    entity.Clid = this.Clid;
                    entity.Csid = this.Csid;

                    xml = entity.ToXmlString();
                    break;
                case CmCommand.XMLData:
                    xml = ExtractXML(data);
                    break;
                case CmCommand.CSVData:
                    xml = GetCSVObject();
                    break;
                case CmCommand.AccountNumber:
                    xml = _DesktopBrokerAccount.AccountNumber ?? string.Empty;
                    break;
                default:
                    this._DesktopBrokerAccount.OrganizationStatus = this.OrganizationStatus;

                    SetAllUnsettledOrders();
                    xml = this._DesktopBrokerAccount.ToXmlString();


                    break;
            }
            return xml;
        }

        public override string GetAllDataStream(int type)
        {
            return this._DesktopBrokerAccount.ToXmlString();
        }
        #endregion

        #region GetXMLData

        public override string GetXmlData()
        {
            string xmlco = _DesktopBrokerAccount.ToXmlString();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmlco);
            XmlAttribute rootAtt = doc.CreateAttribute("Type");
            rootAtt.Value = this.TypeName;
            doc.DocumentElement.Attributes.Append(rootAtt);
            return doc.OuterXml;
        }

        private string ExtractXML(string data)
        {
            string xml = string.Empty;
            XDocument document = XDocument.Load(XmlReader.Create(new StringReader(this._DesktopBrokerAccount.ToXmlString())));
            document.Root.Name = data;
            document.Root.RemoveAttributes();
            xml = document.ToString().Trim();
            return xml;
        }

        #endregion

        #region GetCSVData
        private string GetCSVObject()
        {
            return _DesktopBrokerAccount.ToXmlString();
        }
        #endregion
        public override IClientEntity ClientEntity
        {
            get
            {
                return this._DesktopBrokerAccount;
            }

            set
            {
                this._DesktopBrokerAccount = value as DesktopBrokerAccountEntity;
            }
        }
        public void SetASXAdjustmentTransaction(ObservableCollection<HoldingTransactions> transactions, ASXAdjustmentTransactionsDS asxAdjustmentTransactionsDS)
        {
            var transaction = transactions.Where(tran => tran.TranstactionUniqueID == asxAdjustmentTransactionsDS.ParentTransactionID).FirstOrDefault();
            if (transaction != null)
            {
                if (asxAdjustmentTransactionsDS.DataSetOperationType == DataSetOperationType.DeletSingle)
                {
                    if (asxAdjustmentTransactionsDS.Tables[ASXAdjustmentTransactionsDS.ASXADJTABLE].Rows.Count > 0)
                    {
                        DataRow row = asxAdjustmentTransactionsDS.Tables[ASXAdjustmentTransactionsDS.ASXADJTABLE].Rows[0];
                        Guid tranID = (Guid)row[ASXAdjustmentTransactionsDS.TRANID];
                        var adjTranObj = transaction.AdjustmentTransactions.Where(adjTran => adjTran.TransactionID == tranID).FirstOrDefault();
                        transaction.AdjustmentTransactions.Remove(adjTranObj);
                    }
                }
                else if (asxAdjustmentTransactionsDS.DataSetOperationType == DataSetOperationType.UpdateBulk)
                {
                    if (transaction.AdjustmentTransactions == null)
                        transaction.AdjustmentTransactions = new ObservableCollection<AdjustmentTransaction>();
                    transaction.AdjustmentTransactions.Clear();

                    foreach (DataRow row in asxAdjustmentTransactionsDS.Tables[ASXAdjustmentTransactionsDS.ASXADJTABLE].Rows)
                    {
                        AdjustmentTransaction adjustmentTransaction = new AdjustmentTransaction();
                        adjustmentTransaction.ParentID = transaction.TranstactionUniqueID;
                        adjustmentTransaction.TransactionID = (Guid)row[ASXAdjustmentTransactionsDS.TRANID];

                        adjustmentTransaction.Type = (string)row[ASXAdjustmentTransactionsDS.TYPE];
                        adjustmentTransaction.TradeDate = (DateTime)row[ASXAdjustmentTransactionsDS.TRADEDATE];
                        adjustmentTransaction.Units = (decimal)row[ASXAdjustmentTransactionsDS.UNITS];
                        adjustmentTransaction.BrokerageAmount = (decimal)row[ASXAdjustmentTransactionsDS.BROKERAGEAMT];
                        adjustmentTransaction.TransactionType = (string)row[ASXAdjustmentTransactionsDS.TRANSACTIONTYPE];
                        adjustmentTransaction.UnitPrice = (decimal)row[ASXAdjustmentTransactionsDS.UNITPRICE];
                        adjustmentTransaction.NetValue = (adjustmentTransaction.UnitPrice * adjustmentTransaction.Units) + adjustmentTransaction.BrokerageAmount;
                        transaction.AdjustmentTransactions.Add(adjustmentTransaction);
                    }
                }

                transaction.NetValue = transaction.AdjustmentTransactions.Sum(adj => adj.NetValue);
                transaction.BrokerageGST = transaction.AdjustmentTransactions.Sum(adj => adj.BrokerageAmount) * new decimal(0.090909);
                transaction.BrokerageAmount = transaction.AdjustmentTransactions.Sum(adj => adj.BrokerageAmount) - transaction.BrokerageAmount;
            }
        }

        public void GetASXAdjustmentTransaction(ObservableCollection<HoldingTransactions> transactions, ASXAdjustmentTransactionsDS asxAdjustmentTransactionsDS)
        {
            var transaction = transactions.Where(tran => tran.TranstactionUniqueID == asxAdjustmentTransactionsDS.ParentTransactionID).FirstOrDefault();
            if (transaction.AdjustmentTransactions == null)
                transaction.AdjustmentTransactions = new ObservableCollection<AdjustmentTransaction>();

            if (transaction != null)
            {
                DataTable AsxAdjustmentTable = asxAdjustmentTransactionsDS.Tables[ASXAdjustmentTransactionsDS.ASXADJTABLE];

                foreach (AdjustmentTransaction adjustmentTransaction in transaction.AdjustmentTransactions)
                {
                    DataRow row = AsxAdjustmentTable.NewRow();
                    row[ASXAdjustmentTransactionsDS.PARENTTRANID] = transaction.TranstactionUniqueID;
                    row[ASXAdjustmentTransactionsDS.TRANID] = adjustmentTransaction.TransactionID;
                    row[ASXAdjustmentTransactionsDS.INVESTCODE] = adjustmentTransaction.InvestmentCode;

                    if (adjustmentTransaction.Type == null)
                    {
                        var type = AdjustmentTransaction.TransactionTypes.Where(t => t.TransactionDescription == adjustmentTransaction.TransactionType).FirstOrDefault();
                        if (type != null)
                            adjustmentTransaction.Type = type.Type; 
                    }
                 
                    row[ASXAdjustmentTransactionsDS.TYPE] = adjustmentTransaction.Type;
                    row[ASXAdjustmentTransactionsDS.TRADEDATE] = adjustmentTransaction.TradeDate;
                    row[ASXAdjustmentTransactionsDS.UNITS] = adjustmentTransaction.Units;
                    row[ASXAdjustmentTransactionsDS.NETVALUE] = adjustmentTransaction.NetValue;
                    row[ASXAdjustmentTransactionsDS.BROKERAGEAMT] = adjustmentTransaction.BrokerageAmount;
                    row[ASXAdjustmentTransactionsDS.TRANSACTIONTYPE] = adjustmentTransaction.TransactionType;
                    row[ASXAdjustmentTransactionsDS.UNITPRICE] = adjustmentTransaction.UnitPrice;

                    AsxAdjustmentTable.Rows.Add(row);
                }
            }
        }

        protected override void OnGetData(DataSet data)
        {
            base.OnGetData(data);
            _DesktopBrokerAccount.Name = this.Name;
            _DesktopBrokerAccount.OrganizationStatus = this.OrganizationStatus;
            if (data is ASXAdjustmentTransactionsDS)
            {
                ASXAdjustmentTransactionsDS asxAdjustmentTransactionsDS = data as ASXAdjustmentTransactionsDS;
                GetASXAdjustmentTransaction(this._DesktopBrokerAccount.holdingTransactions, asxAdjustmentTransactionsDS);
            }
            else if (data is DesktopBrokerAccountDS)
            {
                var dataSet = data as DesktopBrokerAccountDS;
                DataRow dr = data.Tables[dataSet.DesktopBrokerAccountsTable.TABLENAME].NewRow();
                dr[dataSet.DesktopBrokerAccountsTable.CID] = CID;
                dr[dataSet.DesktopBrokerAccountsTable.CLID] = CLID;
                dr[dataSet.DesktopBrokerAccountsTable.CSID] = CSID;
                dr[dataSet.DesktopBrokerAccountsTable.ACCOUNTNAME] = DesktopBrokerAccountEntity.Name;
                dr[dataSet.DesktopBrokerAccountsTable.ACCOUNTNO] = DesktopBrokerAccountEntity.AccountNumber;
                dr[dataSet.DesktopBrokerAccountsTable.ACCOUNTDESIGNATION] = DesktopBrokerAccountEntity.AccountDesignation;
                dr[dataSet.DesktopBrokerAccountsTable.HINNUMBER] = DesktopBrokerAccountEntity.HINNumber;
                dr[dataSet.DesktopBrokerAccountsTable.STATUS] = DesktopBrokerAccountEntity.OrganizationStatus;
                if (DesktopBrokerAccountEntity.holdingTransactions!=null)
                dr[dataSet.DesktopBrokerAccountsTable.UNITSHELD] = DesktopBrokerAccountEntity.holdingTransactions.Sum(ss=>ss.Units);
                dataSet.Tables[dataSet.DesktopBrokerAccountsTable.TABLENAME].Rows.Add(dr);
            }
            if (data is ClientTotalUnitsDS)
            {
                var ds = data as ClientTotalUnitsDS;
                if (DesktopBrokerAccountEntity.holdingTransactions != null)
                {
                    var units = Convert.ToInt32(DesktopBrokerAccountEntity.holdingTransactions.Where(r => r.InvestmentCode == ds.code).Sum(dd => dd.Units));
                    DataRow dr = ds.clientTotalUnitsTable.NewRow();
                    dr[ds.clientTotalUnitsTable.TOTALUNITS] = units.ToString();
                    ds.clientTotalUnitsTable.Rows.Add(dr);
                }
            }
            else if (data is SecurityTransferInReportDS)
            {
                var ds = data as SecurityTransferInReportDS;
                if (DesktopBrokerAccountEntity.holdingTransactions != null)
                {
                    var transIn = DesktopBrokerAccountEntity.holdingTransactions.Where(r => r.TransactionType == "Transfer In");
                    var org = Broker.GetWellKnownCM(WellKnownCM.Organization) as IOrganization;
                    if (org != null)
                    {
                        foreach (var holdingTransactionse in transIn)
                        {
                            var rows = ds.TransferInReportTable.Select(string.Format("{0}='{1}'", ds.TransferInReportTable.SECURITYCODE, holdingTransactionse.InvestmentCode));
                            string SecurityName = string.Empty;
                            DateTime? PriceDate;
                            double? Price;

                            GetSecuirtyPriceAndDate(org, holdingTransactionse.InvestmentCode, out SecurityName, out PriceDate, out Price);


                            if (rows.Length > 0)
                            {
                                DataRow dr = rows[0];

                                if (holdingTransactionse.TradeDate.Date < ((DateTime)dr[ds.TransferInReportTable.DATEOFEARLIESTTRANSFERIN]).Date)
                                {
                                    if (PriceDate.HasValue)
                                        dr[ds.TransferInReportTable.EARLIESTPRICEDATE] = PriceDate.Value;
                                    if (Price.HasValue)
                                        dr[ds.TransferInReportTable.PRICEATEARLIESTPRICEDATE] = Price;

                                    dr[ds.TransferInReportTable.DATEOFEARLIESTTRANSFERIN] = holdingTransactionse.TradeDate;
                                    dr[ds.TransferInReportTable.CLIENTCODEFORTHISTRANSFER] = ds.CurrentClientID;
                                    dr[ds.TransferInReportTable.CLIENTCIDFORTHISTRANSFER] = ds.CurrentClientCID;
                                    dr[ds.TransferInReportTable.ERROR] = !PriceDate.HasValue || holdingTransactionse.TradeDate.Date < PriceDate.Value.Date;
                                }

                            }
                            else
                            {
                                DataRow dr = ds.TransferInReportTable.NewRow();
                                dr[ds.TransferInReportTable.SECURITYCODE] = holdingTransactionse.InvestmentCode;
                                dr[ds.TransferInReportTable.SECURITYNAME] = SecurityName;
                                if (PriceDate.HasValue)
                                    dr[ds.TransferInReportTable.EARLIESTPRICEDATE] = PriceDate.Value;
                                if (Price.HasValue)
                                    dr[ds.TransferInReportTable.PRICEATEARLIESTPRICEDATE] = Price;
                                dr[ds.TransferInReportTable.DATEOFEARLIESTTRANSFERIN] = holdingTransactionse.TradeDate;
                                dr[ds.TransferInReportTable.CLIENTCODEFORTHISTRANSFER] = ds.CurrentClientID;
                                dr[ds.TransferInReportTable.CLIENTCIDFORTHISTRANSFER] = ds.CurrentClientCID;
                                dr[ds.TransferInReportTable.ERROR] = !PriceDate.HasValue || holdingTransactionse.TradeDate.Date < PriceDate.Value.Date;

                                ds.TransferInReportTable.Rows.Add(dr);
                            }
                        }

                        Broker.ReleaseBrokerManagedComponent(org);
                    }
                }
            }





        }

        private void GetSecuirtyPriceAndDate(IOrganization org, string secCode, out string securityName, out DateTime? EarliestPriceDate, out double? PriceEarliestPriceDate)
        {
            securityName = string.Empty;
            EarliestPriceDate = null;
            PriceEarliestPriceDate = null;
            var sec = org.Securities.FirstOrDefault(ss => ss.AsxCode == secCode);
            if (sec != null)
            {
                securityName = sec.CompanyName;
                var price = sec.ASXSecurity.OrderBy(ss => ss.Date).FirstOrDefault();
                if (price != null)
                {
                    EarliestPriceDate = price.Date;
                    PriceEarliestPriceDate = price.UnitPrice;

                }
            }

        }

        protected override Calculation.BrokerManagedComponent.ModifiedState OnSetData(System.Data.DataSet data)
        {
            base.OnSetData(data);

            if (data is SMAImportProcessDS)
            {
                SMAImportProcessDS smaImportProcessDS = data as SMAImportProcessDS;
                if (smaImportProcessDS.Tables["InvestmentViewModel"] != null && smaImportProcessDS.Tables["InvestmentViewModel"].Rows.Count > 0)
                {
                    var equityRows = smaImportProcessDS.Tables["InvestmentViewModel"].Select().Where(row => row["InvestmentClass"].ToString().ToLower() == "equity");
                    foreach (DataRow equityRow in equityRows)
                    {
                        string assetCode = equityRow["LongAssetCode"].ToString();
                        foreach (DataTable table in smaImportProcessDS.Tables)
                        {
                            if (table.TableName.Contains(assetCode + "_ASX"))
                            {
                                foreach (DataRow row in table.Rows)
                                {
                                    HoldingTransactions tran = CreateTransactionSMA(row, assetCode, smaImportProcessDS.ClientID);
                                    this._DesktopBrokerAccount.holdingTransactions.Add(tran);
                                }
                            }
                        }
                    }
                }
            }

            if (data is SecTransactionDetailsDS)
            {
                SecTransactionDetailsDS secTransactionDetailsDS = (SecTransactionDetailsDS)data;

                if (secTransactionDetailsDS.DataSetOperationType == DataSetOperationType.UpdateSingle)
                {
                    DataRow rowEntity = data.Tables[SecTransactionDetailsDS.SECTRANSDETAILSTABLE].Rows[0];
                    Guid id = (Guid)rowEntity[SecTransactionDetailsDS.ID];

                    var linkedTran = this.DesktopBrokerAccountEntity.holdingTransactions.Where(tran => tran.TranstactionUniqueID == id).FirstOrDefault();
                    if (linkedTran != null)
                    {
                        linkedTran.Units = Convert.ToDecimal(rowEntity[SecTransactionDetailsDS.UNITSONHAND]);
                        linkedTran.GrossValue = Convert.ToDecimal(rowEntity[SecTransactionDetailsDS.UNITSPRICE]) * linkedTran.Units;
                        linkedTran.SettlementDate = (DateTime)rowEntity[SecTransactionDetailsDS.SETTLEDATE];
                        linkedTran.TradeDate = (DateTime)rowEntity[SecTransactionDetailsDS.TRANDATE];
                        linkedTran.TransactionType = (String)rowEntity[SecTransactionDetailsDS.TRANTYPE];
                        linkedTran.Type = HoldingTransactions.TransactionTypes.Where(tranType => tranType.TransactionDescription == linkedTran.TransactionType).FirstOrDefault().Type;
                        linkedTran.BrokerageAmount = Convert.ToDecimal(rowEntity[SecTransactionDetailsDS.BROKERAGE]);
                        linkedTran.BrokerageGST = linkedTran.BrokerageAmount - (linkedTran.BrokerageAmount * (110 / 100));
                        linkedTran.NetValue = linkedTran.GrossValue + linkedTran.BrokerageAmount;
                        linkedTran.Narrative = (String)rowEntity[SecTransactionDetailsDS.NARRATIVE];
                    }
                }
                else if (secTransactionDetailsDS.DataSetOperationType == DataSetOperationType.NewSingle)
                {
                    DataRow rowEntity = data.Tables[SecTransactionDetailsDS.SECTRANSDETAILSTABLE].Rows[0];
                    HoldingTransactions holdingTran = new HoldingTransactions();
                    holdingTran.TranstactionUniqueID = Guid.NewGuid();
                    holdingTran.InvestmentCode = (String)rowEntity[SecTransactionDetailsDS.INVESTCODE];
                    holdingTran.Units = Convert.ToDecimal(rowEntity[SecTransactionDetailsDS.UNITSONHAND]);
                    holdingTran.GrossValue = Convert.ToDecimal(rowEntity[SecTransactionDetailsDS.UNITSPRICE]) * holdingTran.Units;
                    holdingTran.SettlementDate = (DateTime)rowEntity[SecTransactionDetailsDS.SETTLEDATE];
                    holdingTran.TradeDate = (DateTime)rowEntity[SecTransactionDetailsDS.TRANDATE];
                    holdingTran.TransactionType = (String)rowEntity[SecTransactionDetailsDS.TRANTYPE];
                    holdingTran.Type = HoldingTransactions.TransactionTypes.Where(tranType => tranType.TransactionDescription == holdingTran.TransactionType).FirstOrDefault().Type;
                    holdingTran.BrokerageAmount = Convert.ToDecimal(rowEntity[SecTransactionDetailsDS.BROKERAGE]);
                    holdingTran.BrokerageGST = holdingTran.BrokerageAmount - (holdingTran.BrokerageAmount * (110 / 100));
                    holdingTran.NetValue = holdingTran.GrossValue + holdingTran.BrokerageAmount;
                    holdingTran.Confirmed = true;
                    holdingTran.Charges = 0;
                    holdingTran.Narrative = (String)rowEntity[SecTransactionDetailsDS.NARRATIVE];
                    holdingTran.FPSInstructionID = "Manual";
                    holdingTran.Status = "Manual";
                    this.DesktopBrokerAccountEntity.holdingTransactions.Add(holdingTran);
                    secTransactionDetailsDS.ParentID = holdingTran.TranstactionUniqueID;
                }
                else if (secTransactionDetailsDS.DataSetOperationType == DataSetOperationType.DeletSingle)
                {
                    DataRow rowEntity = data.Tables[SecTransactionDetailsDS.SECTRANSDETAILSTABLE].Rows[0];
                    Guid secID = (Guid)rowEntity[SecTransactionDetailsDS.ID];

                    var sectran = this.DesktopBrokerAccountEntity.holdingTransactions.Where(tran => tran.TranstactionUniqueID == secID).FirstOrDefault();
                    if (sectran != null)
                    {
                        if (DesktopBrokerAccountEntity.DeletedHoldingTransactions == null)
                        {
                            DesktopBrokerAccountEntity.DeletedHoldingTransactions = new ObservableCollection<HoldingTransactions>();
                        }
                        DesktopBrokerAccountEntity.DeletedHoldingTransactions.Add(sectran);
                        DesktopBrokerAccountEntity.holdingTransactions.Remove(sectran);
                    }
                }


            }
            else if (data is ASXAdjustmentTransactionsDS)
            {
                ASXAdjustmentTransactionsDS asxAdjustmentTransactionsDS = data as ASXAdjustmentTransactionsDS;
                SetASXAdjustmentTransaction(this._DesktopBrokerAccount.holdingTransactions, asxAdjustmentTransactionsDS);
            }
            else if (data is ImportProcessDS)
            {
                ImportProcessDS importProcessDs = data as ImportProcessDS;

                var ds = data as ImportProcessDS;
                switch ((WebCommands)ds.Command)
                {
                    case WebCommands.ImportASXHistoricalCostBaseOfTransferredAssets:
                        ImportHistoricalCostBaseOfTransferredAssets(ds);
                        break;
                }
            }
            else if (data is DesktopBrokerAccountDS)
            {
                DesktopBrokerAccountDS desktopBrokerAccountDs = data as DesktopBrokerAccountDS;
                if (_DesktopBrokerAccount == null)
                {
                    _DesktopBrokerAccount = new DesktopBrokerAccountEntity();
                }

                _DesktopBrokerAccount.AccountDesignation = desktopBrokerAccountDs.Tables[desktopBrokerAccountDs.DesktopBrokerAccountsTable.TABLENAME].Rows[0][desktopBrokerAccountDs.DesktopBrokerAccountsTable.ACCOUNTDESIGNATION].ToString();
                _DesktopBrokerAccount.AccountNumber = desktopBrokerAccountDs.Tables[desktopBrokerAccountDs.DesktopBrokerAccountsTable.TABLENAME].Rows[0][desktopBrokerAccountDs.DesktopBrokerAccountsTable.ACCOUNTNO].ToString();
                _DesktopBrokerAccount.HINNumber = desktopBrokerAccountDs.Tables[desktopBrokerAccountDs.DesktopBrokerAccountsTable.TABLENAME].Rows[0][desktopBrokerAccountDs.DesktopBrokerAccountsTable.HINNUMBER].ToString();
                Name = _DesktopBrokerAccount.Name = desktopBrokerAccountDs.Tables[desktopBrokerAccountDs.DesktopBrokerAccountsTable.TABLENAME].Rows[0][desktopBrokerAccountDs.DesktopBrokerAccountsTable.ACCOUNTNAME].ToString();
                OrganizationStatus = _DesktopBrokerAccount.OrganizationStatus = desktopBrokerAccountDs.Tables[desktopBrokerAccountDs.DesktopBrokerAccountsTable.TABLENAME].Rows[0][desktopBrokerAccountDs.DesktopBrokerAccountsTable.STATUS].ToString();
                if (!string.IsNullOrEmpty(desktopBrokerAccountDs.RepUnitSuperID))
                    this.OtherID = desktopBrokerAccountDs.RepUnitSuperID;
                if (string.IsNullOrEmpty(ClientId))
                {
                    UpDateEventLog.UpdateLog("ASX Broker Account", _DesktopBrokerAccount.Name, desktopBrokerAccountDs.Command, Broker);
                }
                else
                {
                    UpDateEventLog.UpdateLog("ASX Broker Account", _DesktopBrokerAccount.Name + " (" + ClientId + ")", desktopBrokerAccountDs.Command, Broker);
                }

            }
            this.Name = _DesktopBrokerAccount.Name;
            this.OrganizationStatus = _DesktopBrokerAccount.OrganizationStatus;
            

            return ModifiedState.MODIFIED;
        }

        private void ImportHistoricalCostBaseOfTransferredAssets(ImportProcessDS ds)
        {
            DataRow[] drs = ds.Tables[0].Select(string.Format("HasErrors='false' and {0}='{1}'", "[ASXCID]", CID));


            foreach (DataRow dr in drs)
            {
                // var adtrans=row valida
                #region Return from Validate
                AdjustmentTransaction adjustmentTransaction = new AdjustmentTransaction();

                adjustmentTransaction.TradeDate = Convert.ToDateTime(dr["Transaction Date"]);
                adjustmentTransaction.UnitPrice = Convert.ToDecimal(dr["Unit Price"]);
                adjustmentTransaction.Units = Convert.ToDecimal(dr["Units "]);
                adjustmentTransaction.BrokerageAmount = Convert.ToDecimal(dr["Brokerage"]);
                adjustmentTransaction.NetValue = Convert.ToDecimal(dr["Net Value"]);

                if (Convert.ToDecimal(dr["Net Value"]) >= 0)
                {
                    adjustmentTransaction.TransactionType = "Buy / Purchase";
                }
                else
                {
                    adjustmentTransaction.TransactionType = "Sale";
                }
                #endregion

                var s =
                    _DesktopBrokerAccount.holdingTransactions.Where(ss => ss.InvestmentCode == dr["Investment Code"].ToString());

                var tran = s.FirstOrDefault();

                bool isDuplicate = false;
                if (tran != null && tran.TransactionType.ToLower() == "transfer in")
                {
                    if (tran.AdjustmentTransactions != null)
                    {
                        foreach (var adjustmentTrans in tran.AdjustmentTransactions)
                        {
                            if (tran.InvestmentCode == dr["Investment Code"].ToString() &&
                                adjustmentTrans.TradeDate == Convert.ToDateTime(dr["Transaction Date"]) &&
                                adjustmentTrans.UnitPrice == Convert.ToDecimal(dr["Unit Price"]) &&
                                adjustmentTrans.Units == Convert.ToDecimal(dr["Units "]) &&
                                adjustmentTrans.BrokerageAmount == Convert.ToDecimal(dr["Brokerage"]) &&
                                adjustmentTrans.NetValue == Convert.ToDecimal(dr["Net Value"]))
                            {

                                dr["Message"] += " Duplicate Transaction";
                                dr["HasErrors"] = true;
                                // Duplicate Transaction
                                isDuplicate = true;
                                break;
                            }
                        }
                    }

                    if (!isDuplicate)
                    {
                        if (tran.AdjustmentTransactions == null)
                        {
                            tran.AdjustmentTransactions =
                                new ObservableCollection<AdjustmentTransaction>();
                        }
                        tran.AdjustmentTransactions.Add(adjustmentTransaction);
                    }
                    isDuplicate = false;
                }
            }
        }

        #region ToXElement
        public override XElement ToXElement()
        {
            return _DesktopBrokerAccount.ToXElement("DesktopBrokerAccount");
        }
        #endregion

        #region DoTagged
        public override void DoTagged(EntityTag tag, Guid modelId)
        {
            string appender = tag.Appender;
            _DesktopBrokerAccount.DoTaggedExtension<DesktopBrokerAccountEntity>(tag);
            _DesktopBrokerAccount.DoTaggedExtension<DesktopBrokerAccountEntity>(tag.SetAppender(appender + ".DesktopBroker".ToUpper()));
            tag.SetAppender(appender);

        }
        #endregion

        #region OrganizationsChart

        public OrganizationChart GetOrganizationChart()
        {
            OrganizationChart chart = new OrganizationChart();

            chart.Clid = this.CLID;
            chart.Csid = this.CSID;
            chart.NodeName = this._DesktopBrokerAccount.Name;

            return chart;
        }

        public OrganizationChart GetAssociation(IIdentityCM identitycm, OrganizationType organizationtype)
        {
            OrganizationChart OChart = new OrganizationChart();
            return OChart;
        }

        public OrganizationChart GetEntityParent(OrganizationChart organizationchart)
        {
            return null;
        }

        public OrganizationChart GetEntityChild()
        {
            return null;
        }

        public OrganizationChart GetEntityChildWithEntity()
        {
            return null;
        }

        #endregion

        #region AccountRelationList

        public string AddAccountRelation(string data)
        {
            AccountRelation newitem = data.ToNewOrData<AccountRelation>();
            if (this._DesktopBrokerAccount.LinkedAccounts.Exists(p => p == newitem)) return "false";

            this._DesktopBrokerAccount.LinkedAccounts.Add(newitem);
            IBrokerManagedComponent component = Broker.GetCMImplementation(newitem.Clid, newitem.Csid) as IBrokerManagedComponent;
            component.UpdateDataStream((int)CmCommand.AccountRelationAddReference, (new AccountRelation() { Clid = this.Clid, Csid = this.Csid, AccountType = newitem.AccountType, ServiceType = newitem.ServiceType }).ToXmlString());

            return "true";
        }
        public string AddAccountRelationReference(string data)
        {
            AccountRelation newitem = data.ToNewOrData<AccountRelation>();
            if (this._DesktopBrokerAccount.LinkedAccounts.Exists(p => p == newitem)) return "false";
            this._DesktopBrokerAccount.LinkedAccounts.Add(newitem);
            return "true";
        }

        public string DeleteAccountRelation(string data)
        {
            AccountRelation newitem = data.ToNewOrData<AccountRelation>();

            var list = this._DesktopBrokerAccount.LinkedAccounts.Where(e => e.Clid == newitem.Clid && e.Csid == newitem.Csid).ToList();
            foreach (var each in list)
            {
                this._DesktopBrokerAccount.LinkedAccounts.Remove(each);
            }

            IBrokerManagedComponent component = Broker.GetCMImplementation(newitem.Clid, newitem.Csid) as IBrokerManagedComponent;
            component.UpdateDataStream((int)CmCommand.AccountRelationDeleteRefrence, (new AccountRelation() { Clid = this.Clid, Csid = this.Csid, AccountType = newitem.AccountType }).ToXmlString());

            return "true";
        }
        public string DeleteAccountRelationReference(string data)
        {
            AccountRelation newitem = data.ToNewOrData<AccountRelation>();
            var list = this._DesktopBrokerAccount.LinkedAccounts.Where(e => e.Clid == newitem.Clid && e.Csid == newitem.Csid).ToList();
            foreach (var each in list)
            {
                this._DesktopBrokerAccount.LinkedAccounts.Remove(each);
            }
            return "true";
        }

        #endregion

        #region importBankWest Accounts Transactions

        public override List<ImportMessages> ImportTransactions(System.Xml.Linq.XElement root)
        {
            return ImportTransactionsFromXml(root);
        }

        private List<ImportMessages> ImportTransactionsFromXml(XElement root, DataSet ds = null)
        {
            List<ImportMessages> messages = new List<ImportMessages>();
            string AccountNumber = _DesktopBrokerAccount.AccountNumber;
            XElement[] elements = root.XPathSelectElements("//Accounts//Account [Id='" + AccountNumber + "']").ToArray();

            foreach (var each in elements)
            {
                ProcessTransactions(each, messages, AccountNumber, ds);
            }

            _DesktopBrokerAccount.UploadTime = DateTime.Now;
            return messages;
        }

        public List<ImportMessages> ImportTransactions(XElement xml, DataSet ds)
        {
            return ImportTransactionsFromXml(xml, ds);

        }


        private void ProcessTransactions(XElement accountXml, List<ImportMessages> messages, string accountNumber, DataSet ds)
        {
            if (_DesktopBrokerAccount.holdingTransactions == null)
            {
                _DesktopBrokerAccount.holdingTransactions = new ObservableCollection<HoldingTransactions>();
            }
            XElement[] elements = accountXml.XPathSelectElements("InvestmentHoldings//InvestmentHolding").ToArray();
            foreach (var xElement in elements)
            {
                GetTransactionData(xElement, messages, accountNumber, ds);
            }
        }


        public string getAccountName()
        {
            return this._DesktopBrokerAccount.Name;
        }

        public string getAccountNumber()
        {
            return this._DesktopBrokerAccount.AccountNumber;
        }

        public override void DeleteAllHoldingBalances_DesktopBroker()
        {
            _DesktopBrokerAccount.HoldingBalances = new ObservableCollection<HoldingBalance>();
        }

        public override void DeleteAllHoldingTransaction_DesktopBroker()
        {
            _DesktopBrokerAccount.holdingTransactions = new ObservableCollection<HoldingTransactions>();
        }

        private void GetTransactionData(XElement InvestmentHolding, List<ImportMessages> messages, string accountNumber, DataSet ds)
        {
            string InvestmentCode = InvestmentHolding.XPathSelectElement("InvestmentCode").Value;
            XElement[] elements = InvestmentHolding.XPathSelectElements("HoldingTransactions//Transactions//Transaction").ToArray();
            foreach (var xElement in elements)
            {
                HoldingTransactions trans = CreateTransaction(xElement, InvestmentCode, messages, accountNumber);
                if (trans != null)
                {
                    if (_DesktopBrokerAccount.holdingTransactions == null)
                        _DesktopBrokerAccount.holdingTransactions = new ObservableCollection<HoldingTransactions>();

                    if (_DesktopBrokerAccount.PaidOrdersTranactions == null)
                        _DesktopBrokerAccount.PaidOrdersTranactions = new ObservableCollection<HoldingTransactions>();

                    #region Transaction and UnsettledOrder
                    if (trans.TransactionStatus == "Unconfirmed")// Unsettled Order
                    {
                        if (_DesktopBrokerAccount.holdingTransactions != null)
                        {
                            //settle if transaction already exists in holding transactions
                            var temtrans =
                                _DesktopBrokerAccount.holdingTransactions.Where(sh => sh.Id == trans.Id).FirstOrDefault();

                            if (temtrans != null)
                            {
                                trans.IsSettled = true;
                                trans.SettelerTransactionID = temtrans.Id;
                            }
                        }

                        trans.SettledStatus = "Uploaded";
                        _DesktopBrokerAccount.PaidOrdersTranactions.Add(trans);
                    }
                    else    // Add transaction
                    {
                        var paidorder = _DesktopBrokerAccount.PaidOrdersTranactions.Where(order => order.IsSettled == false && order.Id == trans.Id).FirstOrDefault();
                        if (paidorder != null && _DesktopBrokerAccount.UnSettledOrders != null)
                        {
                            paidorder.IsSettled = true;
                            paidorder.SettelerTransactionID = trans.Id;
                        }
                        _DesktopBrokerAccount.holdingTransactions.Add(trans);
                    }

                    #endregion
                    SetUnsettledOrder(trans.InvestmentCode);
                    if (ds != null && trans.TransactionStatus != "Unconfirmed")
                    {
                        DataRow[] drs =
                            ds.Tables["ClientASXAccounts"].Select(string.Format("AccountNumber='{0}'", this.DesktopBrokerAccountEntity.AccountNumber));
                        if (drs.Length > 0)
                        {

                            SetDividendsInClients(trans, (Guid)drs[0]["ClientCID"]);
                            SetSettledUnsettledTransactions(ds, trans, drs);
                        }
                    }

                }
            }


            #region holging balance
            XElement[] holdingBalances = InvestmentHolding.XPathSelectElements("HoldingBalances").ToArray();
            foreach (var xElement in holdingBalances)
            {
                HoldingBalance balance = new HoldingBalance();
                balance.InvestmentCode = InvestmentCode;
                decimal temp = 0;

                if (decimal.TryParse(xElement.XPathSelectElement("Actual").Value, out  temp))
                    balance.Actual = temp;

                if (decimal.TryParse(xElement.XPathSelectElement("Unconfirmed").Value, out  temp))
                    balance.Unconfirmed = temp;

                DateTime date = DateTime.Now;
                if (DateTime.TryParse(xElement.XPathSelectElement("AsAtDate").Value, out  date))
                    balance.AsAtDate = date;

                if (_DesktopBrokerAccount.HoldingBalances == null)
                {
                    _DesktopBrokerAccount.HoldingBalances = new ObservableCollection<HoldingBalance>();
                }
                var previs = _DesktopBrokerAccount.HoldingBalances.Where(ss => ss.InvestmentCode == InvestmentCode);
                for (int i = previs.Count() - 1; i >= 0; i--)
                {
                    _DesktopBrokerAccount.HoldingBalances.Remove(previs.ElementAt(i));
                }
                _DesktopBrokerAccount.HoldingBalances.Add(balance);
            }

            #endregion
        }

        private void SetDividendsInClients(HoldingTransactions trans, Guid ClientCID)
        {

            if (ClientCID != Guid.Empty)
            {
                var client = (OrganizationUnitCM) Broker.GetBMCInstance(ClientCID);
                if (client != null)
                {
                    client.Broker.SetStart();
                    if (client.IsCorporateType())
                    {
                        var cooperate = (Common.CorporateEntity) client.ClientEntity;

                        if (cooperate != null)
                        {
                            MatchTransactionWithDividend(trans, cooperate.DividendCollection);
                        }

                    }
                    else
                    {
                        var individual = (Common.ClientIndividualEntity) client.ClientEntity;
                        MatchTransactionWithDividend(trans, individual.DividendCollection);
                    }
                    client.CalculateToken(true);
                }
            }
        }

        private void MatchTransactionWithDividend(HoldingTransactions trans, ObservableCollection<DividendEntity> dividendCollection)
        {

            if (dividendCollection != null && dividendCollection.Count > 0)
            {
                if (trans.DividendStatus == TransectionStatus.Matched)// if transaction is already matched
                {
                    var div = dividendCollection.FirstOrDefault(dv => dv.ID == trans.DividendID);
                    if (div != null)
                    {
                        decimal divunit = GetUnits(div.InvestmentCode, div.RecordDate);
                        decimal divDateUnitPrice = GetAsOfDataPrices(div.InvestmentCode, div.RecordDate);
                        if (!trans.IsDividentMatchWithTrans(div, divunit, divDateUnitPrice))
                        {
                            trans.DividendID = Guid.Empty;
                            trans.DividendStatus = TransectionStatus.Loaded;
                            div.Status = DividendStatus.Accrued;
                            div.DesktopbrokerTransID = string.Empty;



                            SetAlreadyAttachedDividStatus(div, divunit, divDateUnitPrice);
                        }
                        else
                        {
                            return;// dividend and transactions already matching
                        }
                    }
                }
                var dividends = dividendCollection.Where(ss => ss.Status != DividendStatus.Matched).ToList();
                SetTransStatus(dividends, trans);
            }

        }

        private void SetAlreadyAttachedDividStatus(DividendEntity div, decimal divunit, decimal asofDateUnitPrice)
        {
            var trans = _DesktopBrokerAccount.holdingTransactions.FirstOrDefault(ee => ee.DividendStatus != TransectionStatus.Matched && ee.IsDividentMatchWithTrans(div, divunit, asofDateUnitPrice));
            if (trans != null)
            {
                trans.DividendID = div.ID;
                trans.DividendStatus = TransectionStatus.Matched;
                div.Status = DividendStatus.Matched;
                div.DesktopbrokerTransID = trans.TranstactionUniqueID.ToString();
            }

        }



        public bool SetTransStatus(List<DividendEntity> dividends, HoldingTransactions unMatchedTrans)
        {
            bool matched = false;
            var dividendEntity = dividends.FirstOrDefault(ee => unMatchedTrans.IsDividentMatchWithTrans(ee, GetUnits(ee.InvestmentCode, ee.RecordDate), GetAsOfDataPrices(ee.InvestmentCode, ee.RecordDate)));
            if (dividendEntity != null)
            {
                matched = true;
                unMatchedTrans.DividendID = dividendEntity.ID;
                unMatchedTrans.DividendStatus = TransectionStatus.Matched;
                dividendEntity.Status = DividendStatus.Matched;
                dividendEntity.DesktopbrokerTransID = unMatchedTrans.TranstactionUniqueID.ToString();
            }
            return matched;
        }


        private decimal GetAsOfDataPrices(string investmentCode, DateTime? date)
        {
            var org = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            decimal asOfDateUnitPrice = 0;
            if (org != null)
            {
                var sec = org.Securities.FirstOrDefault(ss => ss.AsxCode == investmentCode);
                if (sec != null)
                {

                    var securityprice = sec.ASXSecurity.OrderByDescending(ee => ee.Date).FirstOrDefault(ss => ss.Date <= date);


                    if (securityprice != null && securityprice.UnitPrice != 0.0)
                    {
                        asOfDateUnitPrice = (decimal)securityprice.UnitPrice;
                    }
                }
            }
            Broker.ReleaseBrokerManagedComponent(org);

            return asOfDateUnitPrice;
        }

        public decimal GetUnits(string investmentCode, DateTime? date)
        {

            decimal units = 0;
            var DesktopValue = DesktopBrokerAccountEntity.holdingTransactions.Where(fund => fund.InvestmentCode == investmentCode && fund.TradeDate <= date).ToList();
            if (DesktopValue != null)
            {
                decimal transactionShares = 0;
                transactionShares = DesktopValue.Sum(dd => dd.Units);
                units = transactionShares;
            }



            return units;
        }


        private void SetSettledUnsettledTransactions(DataSet ds, HoldingTransactions trans, DataRow[] drs)
        {
            Guid cid = CheckIfSettelledCmExists(drs[0]["ClientID"].ToString(), Broker,
                                                (ds as IHasOrganizationUnit).Unit.CurrentUser);

            if (cid != Guid.Empty)
            {
                SettledUnsetteledTransactionDS settledUnsetteledTransactionDs = new SettledUnsetteledTransactionDS();
                settledUnsetteledTransactionDs.Unit = new Data.OrganizationUnit
                    {
                        Name = drs[0]["ClientID"].ToString(),
                        ClientId = drs[0]["ClientID"].ToString(),
                        Type = ((int)OrganizationType.SettledUnsettled).ToString(),
                        CurrentUser =
                            (drs[0].Table.DataSet as IHasOrganizationUnit).Unit.CurrentUser,
                    };
                settledUnsetteledTransactionDs.CommandType = DatasetCommandTypes.Settle;


                DataTable dt = new DataTable("ASXTransactions");
                dt.Columns.Add("Amount");
                dt.Columns.Add("Units");
                dt.Columns.Add("TradeDate");
                dt.Columns.Add("InvestmentCode");
                dt.Columns.Add("BrokerageAmount");
                dt.Columns.Add("TransactionType");
                DataColumn dc = new DataColumn("TransactionID", typeof(Guid));
                dc.DefaultValue = trans.TranstactionUniqueID;
                dt.Columns.Add(dc);

                DataRow dr = dt.NewRow();
                dr["Amount"] = trans.NetValue;
                dr["TradeDate"] = trans.TradeDate.ToString("dd/MM/yyyy");
                dr["Units"] = trans.Units;
                dr["InvestmentCode"] = trans.InvestmentCode;
                dr["BrokerageAmount"] = trans.BrokerageAmount;
                dr["TransactionType"] = trans.TransactionType;
                dt.Rows.Add(dr);

                settledUnsetteledTransactionDs.Tables.Add(dt);
                var settledUnsettledCm = Broker.GetBMCInstance(cid);
                settledUnsettledCm.SetData(settledUnsetteledTransactionDs);
                Broker.ReleaseBrokerManagedComponent(settledUnsettledCm);
            }
        }

        private static Guid CheckIfSettelledCmExists(string clientID, ICMBroker broker, UserEntity user)
        {
            var ds = new SettledUnsetteledDS
            {
                Unit = new Data.OrganizationUnit
                {
                    Name = clientID,
                    CurrentUser = user,
                    Type = ((int)OrganizationType.SettledUnsettled).ToString(),
                },
                CommandType = DatasetCommandTypes.Check,
                Command = (int)WebCommands.GetOrganizationUnitsByType

            };

            var org = broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            org.GetData(ds);
            broker.ReleaseBrokerManagedComponent(org);

            return ds.Unit.Cid;
        }


        public void SetAllUnsettledOrders()
        {
            //remove all unsettled order values if no paid order exists
            if (_DesktopBrokerAccount.PaidOrdersTranactions == null)
            {
                _DesktopBrokerAccount.UnSettledOrders = new ObservableCollection<ASXUnSettledOrderEntity>();
                return;
            }

            var investmentcodes =
                                          from t in _DesktopBrokerAccount.PaidOrdersTranactions
                                          group t by t.InvestmentCode into g
                                          select g.FirstOrDefault();

            foreach (var holdingTransactionse in investmentcodes)
            {
                SetUnsettledOrder(holdingTransactionse.InvestmentCode);
            }

        }


        private void SetUnsettledOrder(string investmentCode)
        {
            //remove all unsettled order values if no paid order exists
            if (_DesktopBrokerAccount.PaidOrdersTranactions == null)
            {
                _DesktopBrokerAccount.UnSettledOrders = new ObservableCollection<ASXUnSettledOrderEntity>();
                return;
            }

            if (_DesktopBrokerAccount.UnSettledOrders == null)
                _DesktopBrokerAccount.UnSettledOrders = new ObservableCollection<ASXUnSettledOrderEntity>();


            var unsettledOrders =
                _DesktopBrokerAccount.PaidOrdersTranactions.Where(
                    temp => temp.IsSettled == false && temp.InvestmentCode == investmentCode);

            decimal unsttelamount = 0;
            if (unsettledOrders != null)
            {

                foreach (var holdingTransactionse in unsettledOrders)
                {
                    var holdingTransactionsTransactionType = HoldingTransactions.TransactionTypes.Where(tra => tra.Type == holdingTransactionse.Type.Trim().ToUpper()).FirstOrDefault();
                    decimal unsettledValue = holdingTransactionse.NetValue;
                    if (holdingTransactionsTransactionType != null)
                    {
                        switch (holdingTransactionsTransactionType.AllowablePolarity)
                        {
                            case AllowablePolarityType.Negative:
                                if (unsettledValue > 0)//convert it to negative
                                {
                                    unsettledValue *= -1;
                                }
                                break;
                            case AllowablePolarityType.Positive:
                                if (unsettledValue < 0)//convert it to positive
                                {
                                    unsettledValue *= -1;
                                }

                                break;
                            case AllowablePolarityType.NegativeOrPositive:
                                break;
                        }
                    }
                    unsttelamount += unsettledValue;


                }

            }//end of if set amount

            var unseOrder = _DesktopBrokerAccount.UnSettledOrders.Where(sh => sh.InvestmentCode == investmentCode).FirstOrDefault();

            if (unseOrder == null)
                _DesktopBrokerAccount.UnSettledOrders.Add(new ASXUnSettledOrderEntity() { ID = Guid.NewGuid(), InvestmentCode = investmentCode, UnsettledOrders = unsttelamount });
            else
                unseOrder.UnsettledOrders = unsttelamount;
        }

        private void SetSMATransactionCategory(HoldingTransactions tran, string smaTransactionCategory)
        {
            switch (smaTransactionCategory)
            {
                case "Investment Application":
                case "Investment Application Confirmation":
                    {
                        var asxTranType = AdjustmentTransaction.TransactionTypes.Where(t => t.Type == "AN").FirstOrDefault();
                        tran.Type = asxTranType.Type;
                        tran.TransactionType = asxTranType.TransactionDescription;
                        break;
                    }
                case "Investment Redemption":
                    {
                        var asxTranType = AdjustmentTransaction.TransactionTypes.Where(t => t.Type == "RA").FirstOrDefault();
                        tran.Type = asxTranType.Type;
                        tran.TransactionType = asxTranType.TransactionDescription;
                        break;
                    }
                default:
                    throw new Exception("Desktop Broker Super transaction type now found during import");
            }

        }

        private HoldingTransactions CreateTransactionSMA(DataRow tranRow, string InvestmentCode, string clientID)
        {
            HoldingTransactions trans = new HoldingTransactions();
            BellDirectMessages message = new BellDirectMessages();
            DateTime dateofTrans;
            DateTimeFormatInfo info = new DateTimeFormatInfo();
            info.ShortDatePattern = "dd/MM/yyyy";
            try
            {
                trans.TranstactionUniqueID = Guid.NewGuid();
                trans.InvestmentCode = InvestmentCode;
                trans.Id = String.Empty;
                SetSMATransactionCategory(trans, tranRow["Description"].ToString());
                trans.DividendStatus = TransectionStatus.Loaded;
                message.MessageType = ImportMessageType.Sucess;
                message.Status = ImportMessageStatus.SuccessFullyAdded;
                message.TransactionID = trans.Id;
                message.InvestmentCode = InvestmentCode;
                message.AccountNumber = "173000 - " + clientID;

                if (DateTime.TryParse(tranRow["TransactionDate"].ToString(), info, DateTimeStyles.None, out dateofTrans))
                    message.TransactionDate = dateofTrans.ToString();
                else
                {
                    tranRow["Message"] += "Invalid transaction date,";
                    tranRow["HasErrors"] = true;
                }

                trans.TradeDate = dateofTrans;
                trans.SettlementDate = dateofTrans;
                decimal units = decimal.Parse(tranRow["TransactionUnits"].ToString());
                decimal unitPrice = Math.Round(decimal.Parse(tranRow["TransactionPrice"].ToString()), 6);
                decimal totalAmount = decimal.Parse(tranRow["TransactionAmount"].ToString());
                trans.Currency = "AUD";
                trans.GrossValue = totalAmount;
                trans.Units = units;
                trans.BrokerageAmount = 20;
                double gst = 20.00 - (20.00 / 1.10);
                trans.BrokerageGST = Convert.ToDecimal(gst);

                var holdingTransactionsTransactionType = HoldingTransactions.TransactionTypes.Where(tra => tra.Type == trans.Type.Trim().ToUpper()).FirstOrDefault();
                if (holdingTransactionsTransactionType != null)
                {
                    trans.TransactionType = holdingTransactionsTransactionType.TransactionDescription;

                    switch (holdingTransactionsTransactionType.AllowablePolarity)
                    {
                        case AllowablePolarityType.Negative:
                            //gross amount + ,quatity -
                            if (trans.GrossValue < 0)
                            {
                                trans.GrossValue *= -1;//change it to plus
                            }

                            if (trans.Units > 0)
                            {
                                trans.Units *= -1;//change it to minus
                            }
                            break;
                        case AllowablePolarityType.Positive:
                            //gross amount - ,quatity +
                            if (trans.GrossValue > 0)
                            {
                                trans.GrossValue *= -1;//change it to minus
                            }
                            if (trans.Units < 0)
                            {
                                trans.Units *= -1;//change it to plus
                            }

                            break;
                        case AllowablePolarityType.NegativeOrPositive:
                            break;
                    }
                }

                trans.Confirmed = true;
                trans.Charges = 0;
                trans.Status = "Uploaded";

                if (message.MessageType != ImportMessageType.Error)
                {

                    if (_DesktopBrokerAccount.PaidOrdersTranactions != null)
                    {

                        var oldTransactions = _DesktopBrokerAccount.PaidOrdersTranactions.Where(temp => temp.Id == trans.Id && trans.TransactionStatus == "Unconfirmed");
                        if (oldTransactions != null && oldTransactions.Count() > 0)
                        {
                            for (int i = oldTransactions.Count() - 1; i >= 0; i--)
                            {
                                _DesktopBrokerAccount.PaidOrdersTranactions.Remove(oldTransactions.ElementAt(i));
                            }
                            message.Message += "Paid order updated,";
                        }
                    }
                }

                if (message.MessageType != ImportMessageType.Error)
                {
                    IEnumerable<HoldingTransactions> oldtrans = null;
                    if (!(string.IsNullOrEmpty(trans.Id) || trans.Id == Guid.Empty.ToString()))
                        oldtrans = _DesktopBrokerAccount.holdingTransactions.Where(temp => temp.Id == trans.Id);
                    else
                        oldtrans = _DesktopBrokerAccount.holdingTransactions.Where(temp => temp.InvestmentCode == trans.InvestmentCode && temp.TradeDate == trans.TradeDate && (temp.Units == trans.Units || temp.GrossValue == trans.GrossValue));

                    if (oldtrans != null && oldtrans.Count() > 0)// remove transaction already exits 
                    {
                        if (oldtrans.FirstOrDefault().Type == "AT" && oldtrans.FirstOrDefault().AdjustmentTransactions.Count > 0)
                            trans = oldtrans.FirstOrDefault();
                        var tt = oldtrans.FirstOrDefault();
                        trans.TranstactionUniqueID = tt.TranstactionUniqueID;
                        trans.DividendID = tt.DividendID;
                        trans.DividendStatus = tt.DividendStatus;
                        for (int i = oldtrans.Count() - 1; i >= 0; i--)
                            _DesktopBrokerAccount.holdingTransactions.Remove(oldtrans.ElementAt(i));

                        message.Message += "Transaction updated,";
                    }
                }

                if (!string.IsNullOrEmpty(message.Message) && message.Message.Contains(','))
                    message.Message = message.Message.Remove(message.Message.LastIndexOf(','));
                else
                    message.Message = "";
            }
            catch
            {
                trans = null;
            }

            if (message.MessageType == ImportMessageType.Error)
                trans = null;

            return trans;
        }

        private HoldingTransactions CreateTransaction(XElement transaction, string InvestmentCode, List<ImportMessages> messages, string accountNumber)
        {
            HoldingTransactions trans = new HoldingTransactions();
            BellDirectMessages message = new BellDirectMessages();

            DateTimeFormatInfo info = new DateTimeFormatInfo();
            info.ShortDatePattern = "dd/MM/yyyy";
            DateTime date;
            decimal utemp;
            try
            {
                trans.TranstactionUniqueID = Guid.NewGuid();
                trans.InvestmentCode = InvestmentCode;
                trans.Id = transaction.XPathSelectElement("Id").Value;
                trans.Delete = transaction.XPathSelectElement("Delete").Value;
                trans.Type = transaction.XPathSelectElement("Type").Value;
                trans.DividendStatus = TransectionStatus.Loaded;
                message.MessageType = ImportMessageType.Sucess;
                message.Status = ImportMessageStatus.SuccessFullyAdded;
                message.TransactionID = trans.Id;
                message.TransactionDate = transaction.XPathSelectElement("TradeDate").Value;
                message.InvestmentCode = InvestmentCode;
                message.AccountNumber = accountNumber;

                if (DateTime.TryParse(transaction.XPathSelectElement("TradeDate").Value, info, DateTimeStyles.None, out date))
                {
                    trans.TradeDate = date;
                    message.TransactionDate = date.ToString("dd/MM/yyyy");
                }
                else
                {
                    message.Message += "Invalid transaction date,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                }

                if (!string.IsNullOrEmpty(transaction.XPathSelectElement("Units").Value))
                    if (decimal.TryParse(transaction.XPathSelectElement("Units").Value, out utemp))
                    {
                        trans.Units = utemp;
                    }
                    else
                    {
                        message.Message += "Invalid units,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                    }

                if (!string.IsNullOrEmpty(transaction.XPathSelectElement("GrossValue").Value))
                    if (decimal.TryParse(transaction.XPathSelectElement("GrossValue").Value, out utemp))
                    {
                        trans.GrossValue = utemp;
                    }
                    else
                    {
                        message.Message += "Invalid gross value,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                    }


                if (!string.IsNullOrEmpty(transaction.XPathSelectElement("NetValue").Value))
                    if (decimal.TryParse(transaction.XPathSelectElement("NetValue").Value, out utemp))
                    {
                        trans.NetValue = utemp;
                    }
                    else
                    {
                        message.Message += "Invalid net value,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                    }


                if (!string.IsNullOrEmpty(transaction.XPathSelectElement("CostBase").Value))
                    if (decimal.TryParse(transaction.XPathSelectElement("CostBase").Value, out utemp))
                    {
                        trans.CostBase = utemp;
                    }
                    else
                    {
                        message.Message += "Invalid cost base,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                    }

                trans.TransactionStatus = transaction.XPathSelectElement("TransactionStatus").Value;

                if (DateTime.TryParse(transaction.XPathSelectElement("SettlementDate").Value, info, DateTimeStyles.None, out date))
                    trans.SettlementDate = date;
                else
                {
                    if (!string.IsNullOrEmpty(transaction.XPathSelectElement("SettlementDate").Value))
                    {

                        message.Message += "Invalid settlement date,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                    }
                    else
                    {
                        trans.SettlementDate = null;
                    }
                }

                if (!string.IsNullOrEmpty(transaction.XPathSelectElement("Fees//Total").Value))
                    if (decimal.TryParse(transaction.XPathSelectElement("Fees//Total").Value, out utemp))
                    {
                        trans.FeesTotal = utemp;
                    }
                    else
                    {
                        message.Message += "Invalid fees/total,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                    }

                if (!string.IsNullOrEmpty(transaction.XPathSelectElement("Brokerage//Amount").Value))
                    if (decimal.TryParse(transaction.XPathSelectElement("Brokerage//Amount").Value, out utemp))
                    {
                        trans.BrokerageAmount = utemp;
                    }
                    else
                    {
                        message.Message += "Invalid brokerage amount,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                    }

                if (!string.IsNullOrEmpty(transaction.XPathSelectElement("Brokerage//GSTOnBrokerage").Value))
                    if (decimal.TryParse(transaction.XPathSelectElement("Brokerage//GSTOnBrokerage").Value, out utemp))
                    {
                        trans.BrokerageGST = utemp;
                    }
                    else
                    {
                        message.Message += "Invalid GST on brokerage,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                    }


                trans.Narrative = transaction.XPathSelectElement("Narrative").Value;
                trans.FPSInstructionID = transaction.XPathSelectElement("STPInstructionDetails//FPSInstructionID").Value;
                trans.ReversedTransactionID = transaction.XPathSelectElement("ReversedTransactionID").Value;

                trans.Currency = "AUD";
                var holdingTransactionsTransactionType = HoldingTransactions.TransactionTypes.Where(tra => tra.Type == trans.Type.Trim().ToUpper()).FirstOrDefault();

                if (holdingTransactionsTransactionType != null)
                {
                    trans.TransactionType = holdingTransactionsTransactionType.TransactionDescription;

                    switch (holdingTransactionsTransactionType.AllowablePolarity)
                    {
                        case AllowablePolarityType.Negative:
                            //gross amount + ,quatity -
                            if (trans.GrossValue < 0)
                            {
                                trans.GrossValue *= -1;//change it to plus
                            }

                            if (trans.Units > 0)
                            {
                                trans.Units *= -1;//change it to minus
                            }
                            break;
                        case AllowablePolarityType.Positive:
                            //gross amount - ,quatity +
                            if (trans.GrossValue > 0)
                            {
                                trans.GrossValue *= -1;//change it to minus
                            }
                            if (trans.Units < 0)
                            {
                                trans.Units *= -1;//change it to plus
                            }

                            break;
                        case AllowablePolarityType.NegativeOrPositive:
                            break;
                    }
                }

                trans.Confirmed = true;
                trans.Charges = 0;

                trans.Status = "Uploaded";

                if (message.MessageType != ImportMessageType.Error)
                {

                    if (_DesktopBrokerAccount.PaidOrdersTranactions != null)
                    {

                        var oldTransactions = _DesktopBrokerAccount.PaidOrdersTranactions.Where(temp => temp.Id == trans.Id && trans.TransactionStatus == "Unconfirmed");
                        if (oldTransactions != null && oldTransactions.Count() > 0)
                        {
                            for (int i = oldTransactions.Count() - 1; i >= 0; i--)
                            {
                                _DesktopBrokerAccount.PaidOrdersTranactions.Remove(oldTransactions.ElementAt(i));
                            }
                            message.Message += "Paid order updated,";
                        }
                    }
                }

                if (message.MessageType != ImportMessageType.Error)
                {
                    var oldtrans = _DesktopBrokerAccount.holdingTransactions.Where(temp => temp.Id == trans.Id);
                    if (oldtrans != null && oldtrans.Count() > 0)// remove transaction already exits 
                    {
                        if (oldtrans.FirstOrDefault().Type == "AT" && oldtrans.FirstOrDefault().AdjustmentTransactions.Count > 0)
                            trans = oldtrans.FirstOrDefault();
                        var tt = oldtrans.FirstOrDefault();
                        trans.TranstactionUniqueID = tt.TranstactionUniqueID;
                        trans.DividendID = tt.DividendID;
                        trans.DividendStatus = tt.DividendStatus;
                        for (int i = oldtrans.Count() - 1; i >= 0; i--)
                            _DesktopBrokerAccount.holdingTransactions.Remove(oldtrans.ElementAt(i));

                        message.Message += "Transaction updated,";
                    }
                }

                if (!string.IsNullOrEmpty(message.Message) && message.Message.Contains(','))
                    message.Message = message.Message.Remove(message.Message.LastIndexOf(','));
                else
                    message.Message = "";
            }
            catch
            {
                trans = null;
            }

            messages.Add(message);

            if (message.MessageType == ImportMessageType.Error)
                trans = null;

            return trans;
        }
        #endregion

        #region old  Order Paid import code not used now

        public override List<ImportMessages> ImportPaidOrders(System.Xml.Linq.XElement root)
        {
            List<ImportMessages> messages = new List<ImportMessages>();


            string BsBNAccountNumber = this._DesktopBrokerAccount.AccountNumber;
            XElement[] elements = root.XPathSelectElements("//PaidOrder[BookedACC = '" + BsBNAccountNumber + "']").ToArray();

            foreach (var each in elements)
            {
                CreateOrderPaids(each, messages);
                this._DesktopBrokerAccount.UploadTime = DateTime.Now;
            }




            return messages;
        }

        private void CreateOrderPaids(XElement each, List<ImportMessages> messages)
        {
            //Old code for Order Pad ...No need for this code now

            /*  
          
              ASXPaidOrderEntity asxPaidOrderEntity = CreateMISOrderPaidMessage(each, messages);

               if (asxPaidOrderEntity != null)
                   {
                   this._DesktopBrokerAccount.UploadTime = DateTime.Now;

                   if (_DesktopBrokerAccount.PaidOrders == null)
                       {
                       _DesktopBrokerAccount.PaidOrders = new ObservableCollection<ASXPaidOrderEntity>();
                       }

                   if (_DesktopBrokerAccount.UnSettledOrders == null)
                       {
                       _DesktopBrokerAccount.UnSettledOrders = new ObservableCollection<ASXUnSettledOrderEntity>();


                       }


                   var unseOrder = _DesktopBrokerAccount.UnSettledOrders.Where(sh => sh.InvestmentCode == asxPaidOrderEntity.InvestmentCode).FirstOrDefault();
                   if (unseOrder == null)
                       {
                       _DesktopBrokerAccount.UnSettledOrders.Add(new ASXUnSettledOrderEntity() { ID = Guid.NewGuid(), InvestmentCode = asxPaidOrderEntity.InvestmentCode, UnsettledOrders = asxPaidOrderEntity.QTY*asxPaidOrderEntity.PriceAVG });
                       }
                   else
                       {
                       unseOrder.UnsettledOrders += (asxPaidOrderEntity.QTY * asxPaidOrderEntity.PriceAVG);

                       }




                   _DesktopBrokerAccount.PaidOrders.Add(asxPaidOrderEntity);
                   }
               //xmlValues += string.Format(" <PaidOrder> \n" +
               //                             "<ParentAcc>{0}</ParentAcc> \n" +
               //                             "<BookedACC>{1}</BookedACC> \n" +
               //                             "<BuySale>{2}</BuySale>\n " +
               //                             "<QTY>{3}</QTY> \n" +
               //                              "<Stock>{4}</Stock> \n" +
               //                             "<PriceAVG>{5}</PriceAVG> \n" +
               //                             "<TradeDate>{6}</TradeDate> \n" +
               //                             "<ClientAccountName>{7}</ClientAccountName> \n" +
               //                             "<Status>{8}</Status> \n" +



               //                             "</PaidOrder>


               */

        }

        private ASXPaidOrderEntity CreateMISOrderPaidMessage(XElement asxPrice, List<ImportMessages> messages)
        {


            ASXPaidOrderEntity fundTrans = new ASXPaidOrderEntity();




            ASXPaidOrderMessage message = new ASXPaidOrderMessage();


            DateTimeFormatInfo info = new DateTimeFormatInfo();
            info.ShortDatePattern = "dd/MM/yyyy";

            string ParentAcc = asxPrice.XPathSelectElement("ParentAcc").Value;
            string BookedACC = asxPrice.XPathSelectElement("BookedACC").Value;
            string BuySale = asxPrice.XPathSelectElement("BuySale").Value;
            string QTY = asxPrice.XPathSelectElement("QTY").Value;
            string Stock = asxPrice.XPathSelectElement("Stock").Value;
            string PriceAVG = asxPrice.XPathSelectElement("PriceAVG").Value;
            string TradeDate = asxPrice.XPathSelectElement("TradeDate").Value;
            string ClientAccountName = asxPrice.XPathSelectElement("ClientAccountName").Value;
            string Status = asxPrice.XPathSelectElement("Status").Value;



            DateTime date;
            decimal utemp;
            int itemp;
            try
            {
                fundTrans.ID = Guid.NewGuid();
                message.Status = ImportMessageStatus.Sucess;

                message.MessageType = ImportMessageType.Sucess;
                message.ParentAcc = ParentAcc;
                message.BookedACC = BookedACC;
                message.BuySale = BuySale;
                message.QTY = QTY;
                message.Stock = Stock;
                message.PriceAVG = PriceAVG;
                message.TradeDate = TradeDate;
                message.ClientAccountName = ClientAccountName;


                fundTrans.ParentAcc = ParentAcc;
                fundTrans.BuySale = BuySale;
                //  fundTrans.QTY = QTY;
                fundTrans.InvestmentCode = Stock;
                // fundTrans.PriceAVG = PriceAVG;
                // fundTrans.TradeDate = TradeDate;
                fundTrans.Status = Status;





                if (!DateTime.TryParse(TradeDate, info, DateTimeStyles.None, out date))
                {

                    message.Message += "Invalid Trade Date,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                }
                else
                {
                    message.TradeDate = date.ToString("dd/MM/yyyy");
                    fundTrans.TradeDate = date;
                }



                if (!string.IsNullOrEmpty(PriceAVG))
                    if (!decimal.TryParse(PriceAVG, NumberStyles.Currency, CultureInfo.CurrentCulture, out utemp))
                    {
                        message.Message += "Invalid price,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                    }
                    else
                    {

                        // string format = "$#,##0.00;-$#,##0.00;$0";

                        message.PriceAVG = utemp.ToString();
                        fundTrans.PriceAVG = utemp;
                    }


                if (!string.IsNullOrEmpty(QTY))
                    if (!int.TryParse(QTY, NumberStyles.Integer, CultureInfo.CurrentCulture, out itemp))
                    {
                        message.Message += "Invalid Quantity,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                    }
                    else
                    {

                        // string format = "$#,##0.00;-$#,##0.00;$0";

                        message.QTY = itemp.ToString();
                        fundTrans.QTY = itemp;
                    }

                ICalculationModule objOrganisation = (Broker.GetWellKnownCM(WellKnownCM.Organization));
                ISecuritesStructure organization = (objOrganisation as ISecuritesStructure);
                var security = organization.Securities.Where(sec => sec.AsxCode == Stock).FirstOrDefault();


                if (security == null)
                {
                    message.Message += "Invalid investment code,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                }


                //if (message.MessageType != ImportMessageType.Error)
                //    {

                //    if (_DesktopBrokerAccount.PaidOrders != null)
                //        {
                //        var oldTransactions = _DesktopBrokerAccount.PaidOrders.Where(ee => ee.QTY == fundTrans.QTY && ee.PriceAVG == fundTrans.PriceAVG && ee.TradeDate == fundTrans.TradeDate);
                //        if (oldTransactions != null && oldTransactions.Count() > 0)
                //            {

                //            message.Message += "paid Order already exists,";
                //            message.Status = ImportMessageStatus.Failed;
                //            message.MessageType = ImportMessageType.Error;
                //            }
                //        }
                //    }


                if (!string.IsNullOrEmpty(message.Message) && message.Message.Contains(','))
                {
                    message.Message = message.Message.Remove(message.Message.LastIndexOf(','));
                }
                else
                {
                    message.Message = "";
                }

            }
            catch
            {

                message = null;
            }
            messages.Add(message);

            if (message.MessageType == ImportMessageType.Error)
            {
                fundTrans = null;
            }

            return fundTrans;
        }

        #endregion
    }
}
