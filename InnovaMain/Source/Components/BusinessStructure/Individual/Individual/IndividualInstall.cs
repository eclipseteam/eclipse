using System;

namespace Oritax.TaxSimp.CM.Entity
{
    public class IndividualInstall
	{
		#region INSTALLATION PROPERTIES

		// Assembly Installation Properties
        public const string ASSEMBLY_ID = "9EC83666-D774-4BFE-895C-1E8D42325C50";
        public const string ASSEMBLY_NAME = "Individual";
        public const string ASSEMBLY_DISPLAYNAME = "Individual V 1.0";
		public const string ASSEMBLY_MAJORVERSION="1";
		public const string ASSEMBLY_MINORVERSION="0";
		public const string ASSEMBLY_DATAFORMAT="0";
		public const string ASSEMBLY_REVISION="0";		//2005.1

		// Component Installation Properties
        public const string COMPONENT_ID = "46581D50-6BF9-4AAF-B980-21E03CD76317";
        public const string COMPONENT_NAME = "Individual";
        public const string COMPONENT_DISPLAYNAME = "Individual";
		public const string COMPONENT_CATEGORY="BusinessEntity";

		// Component Version Installation Properties
		public const string COMPONENTVERSION_STARTDATE="01/01/1990 00:00:00 AM";
		public const string COMPONENTVERSION_ENDDATE="31/12/2050 12:59:59 AM";
		public const string COMPONENTVERSION_PERSISTERASSEMBLY="CommonPersisters_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";
		public const string COMPONENTVERSION_PERSISTERCLASS="Oritax.TaxSimp.CommonPersistence.BlobPersister";
		public const string COMPONENTVERSION_IMPLEMENTATIONCLASS="Oritax.TaxSimp.CM.Entity.IndividualCM";

		#endregion

	}
}
