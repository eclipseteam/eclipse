
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, and Azure
-- --------------------------------------------------
-- Date Created: 10/22/2013 23:37:43
-- Generated from EDMX file: C:\Projects\eclipse2\InnovaMain\Source\Components\BusinessStructure\Individual\Individual\IndividualDataModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [TestInnova2011];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_IndividualEntityIdentificationEntity]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[IdentificationEntities] DROP CONSTRAINT [FK_IndividualEntityIdentificationEntity];
GO
IF OBJECT_ID(N'[dbo].[FK_IndividualEntityIndividualAddress]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[IndividualAddresses] DROP CONSTRAINT [FK_IndividualEntityIndividualAddress];
GO
IF OBJECT_ID(N'[dbo].[FK_IdentificationEntityIndividualAddress]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[IdentificationEntities] DROP CONSTRAINT [FK_IdentificationEntityIndividualAddress];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[IndividualEnts1]', 'U') IS NOT NULL
    DROP TABLE [dbo].[IndividualEnts1];
GO
IF OBJECT_ID(N'[dbo].[IdentificationEntities]', 'U') IS NOT NULL
    DROP TABLE [dbo].[IdentificationEntities];
GO
IF OBJECT_ID(N'[dbo].[IndividualAddresses]', 'U') IS NOT NULL
    DROP TABLE [dbo].[IndividualAddresses];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'IndividualEnts'
CREATE TABLE [dbo].[IndividualEnts] (
    [CID] uniqueidentifier  NOT NULL,
    [Title] nvarchar(max)  NULL,
    [Surname] nvarchar(max)  NULL,
    [FirstName] nvarchar(max)  NULL,
    [MiddleName] nvarchar(max)  NULL,
    [EmailAddress] nvarchar(max)  NULL,
    [WorkPHCountryCode] decimal(18,0)  NULL,
    [WorkPHCityCode] decimal(18,0)  NULL,
    [WorkPHNumber] decimal(18,0)  NULL,
    [HomePHCountryCode] decimal(18,0)  NULL,
    [HomePHCityCode] decimal(18,0)  NULL,
    [HomePHNumber] decimal(18,0)  NULL,
    [MobileCountryCode] decimal(18,0)  NULL,
    [MobileCountryNumber] decimal(18,0)  NULL,
    [TFN] decimal(18,0)  NULL,
    [TIN] decimal(18,0)  NULL,
    [FaxCountryCode] decimal(18,0)  NULL,
    [FaxCityCode] decimal(18,0)  NULL,
    [FaxNumber] decimal(18,0)  NULL,
    [DateOfBirth] datetime  NULL,
    [Occupation] nvarchar(max)  NULL,
    [Employer] nvarchar(max)  NULL,
    [Gender] nvarchar(max)  NULL
);
GO

-- Creating table 'IdentificationEntities'
CREATE TABLE [dbo].[IdentificationEntities] (
    [ID] uniqueidentifier  NOT NULL,
    [AttachementID] uniqueidentifier  NULL,
    [DocNumber] nvarchar(max)  NOT NULL,
    [IsPhotoShown] bit  NULL,
    [IsAddressMatch] bit  NULL,
    [IssueDate] nvarchar(max)  NULL,
    [ExpiryDate] nvarchar(max)  NULL,
    [IssuePlace] nvarchar(max)  NULL,
    [Points] decimal(18,0)  NULL,
    [CertifiedCopy] nvarchar(max)  NULL,
    [OrganizationName] nvarchar(max)  NULL,
    [WorkPHCountryCode] decimal(18,0)  NULL,
    [WorkPHCityCode] decimal(18,0)  NULL,
    [WorkPHNumber] decimal(18,0)  NULL,
    [IndividualEntityCID] uniqueidentifier  NOT NULL,
    [BillLetterSighted] bit  NULL,
    [FileDownLoadURL] nvarchar(max)  NULL,
    [AccreditedEnglishTranslationSighted] bit  NULL,
    [IndividualAddress_ID] uniqueidentifier  NOT NULL
);
GO

-- Creating table 'IndividualAddresses'
CREATE TABLE [dbo].[IndividualAddresses] (
    [ID] uniqueidentifier  NOT NULL,
    [AddressLine1] nvarchar(max)  NULL,
    [AddressLine2] nvarchar(max)  NULL,
    [CitySuburb] nvarchar(max)  NULL,
    [ProvinceState] nvarchar(max)  NULL,
    [Country] nvarchar(max)  NULL,
    [PostCode] nvarchar(max)  NULL,
    [IndividualEntityCID] uniqueidentifier  NOT NULL,
    [Type] nvarchar(max)  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [CID] in table 'IndividualEnts'
ALTER TABLE [dbo].[IndividualEnts]
ADD CONSTRAINT [PK_IndividualEnts]
    PRIMARY KEY CLUSTERED ([CID] ASC);
GO

-- Creating primary key on [ID] in table 'IdentificationEntities'
ALTER TABLE [dbo].[IdentificationEntities]
ADD CONSTRAINT [PK_IdentificationEntities]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'IndividualAddresses'
ALTER TABLE [dbo].[IndividualAddresses]
ADD CONSTRAINT [PK_IndividualAddresses]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [IndividualEntityCID] in table 'IdentificationEntities'
ALTER TABLE [dbo].[IdentificationEntities]
ADD CONSTRAINT [FK_IndividualEntityIdentificationEntity]
    FOREIGN KEY ([IndividualEntityCID])
    REFERENCES [dbo].[IndividualEnts]
        ([CID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_IndividualEntityIdentificationEntity'
CREATE INDEX [IX_FK_IndividualEntityIdentificationEntity]
ON [dbo].[IdentificationEntities]
    ([IndividualEntityCID]);
GO

-- Creating foreign key on [IndividualEntityCID] in table 'IndividualAddresses'
ALTER TABLE [dbo].[IndividualAddresses]
ADD CONSTRAINT [FK_IndividualEntityIndividualAddress]
    FOREIGN KEY ([IndividualEntityCID])
    REFERENCES [dbo].[IndividualEnts]
        ([CID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_IndividualEntityIndividualAddress'
CREATE INDEX [IX_FK_IndividualEntityIndividualAddress]
ON [dbo].[IndividualAddresses]
    ([IndividualEntityCID]);
GO

-- Creating foreign key on [IndividualAddress_ID] in table 'IdentificationEntities'
ALTER TABLE [dbo].[IdentificationEntities]
ADD CONSTRAINT [FK_IdentificationEntityIndividualAddress]
    FOREIGN KEY ([IndividualAddress_ID])
    REFERENCES [dbo].[IndividualAddresses]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_IdentificationEntityIndividualAddress'
CREATE INDEX [IX_FK_IdentificationEntityIndividualAddress]
ON [dbo].[IdentificationEntities]
    ([IndividualAddress_ID]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------