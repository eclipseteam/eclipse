using System;
using System.IO;
using System.Data;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Xml;
using System.Xml.Linq;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Enum;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.CalculationInterface;
using System.Linq;
using ClientShare = Oritax.TaxSimp.Common.ClientShare;


namespace Oritax.TaxSimp.CM.Entity
{
    [Serializable]
    public partial class IndividualCM : EntityCM, ISerializable, IOrganizationChartData, IHaveAssociation, ISupportMigration, IIdentityCM, IHasIndividualEntity
    {
        private Oritax.TaxSimp.Common.IndividualEntity _Individual;

        public Oritax.TaxSimp.Common.IndividualEntity Individual
        {
            get { return _Individual; }
            set { _Individual = value; }
        }

        public IndividualCM() : base() { }

        protected IndividualCM(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            _Individual = info.GetValue<Oritax.TaxSimp.Common.IndividualEntity>("Oritax.TaxSimp.CM.Entity.Individual");
        }

        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValueX("Oritax.TaxSimp.CM.Entity.Individual", _Individual);
        }

        public override IClientEntity ClientEntity
        {
            get
            {
                return this.Individual;
            }

            set
            {
                this.Individual = value as IndividualEntity;
            }
        }


        public override string OnUpdateDataStream(int type, string xml)
        {
            switch ((CmCommand)type)
            {
                case CmCommand.SPAddLibrary:
                    var _value = xml.ToNewOrData<Oritax.TaxSimp.Common.SPLibraryDocument>();
                    if (_value != null)
                    {
                        SPLibrary.AddLibraryToClient(_value.CurrentUser.CurrentUserName, _value.Name);
                    }
                    break;
                case CmCommand.DeleteEntitySecurity:
                    xml = DeleteEntitySecurity(xml);
                    break;
                case CmCommand.AddEntitySecurity:
                    xml = AddEntitySecurity(xml);
                    break;
                case CmCommand.GetEntitySecurity:
                    xml = GetEntitySecurity();
                    break;
                default:
                    Oritax.TaxSimp.Common.IndividualEntity individual = xml.ToNewOrData<Oritax.TaxSimp.Common.IndividualEntity>();
                    this.Name = individual.Name;
                    this.OrganizationStatus = individual.OrganizationStatus;
                    _Individual = individual;
                    break;
            }
            return xml;
        }

        public override string GetDataStream(int type, string data)
        {
            string xml = string.Empty;
            switch ((CmCommand)type)
            {
                case CmCommand.XMLData:
                    xml = ExtractXML(data);
                    break;
                case CmCommand.CSVData:
                    xml = GetCSVObject();
                    break;
                case CmCommand.GetEntitySecurity:
                    xml = GetEntitySecurity();
                    break;
                case CmCommand.FullName:
                    xml = this._Individual.Name + " " + this._Individual.Surname;
                    break;
                case CmCommand.Email:
                    xml = this._Individual.EmailAddress ?? string.Empty;
                    break;
                case CmCommand.PhoneCityCode:
                    if (this._Individual.WorkPhoneNumber != null)
                        xml = this._Individual.WorkPhoneNumber.CityCode;
                    else if (this._Individual.HomePhoneNumber != null)
                        xml = this._Individual.HomePhoneNumber.CityCode;

                    break;
                case CmCommand.PhoneNumber:
                    if (this._Individual.WorkPhoneNumber != null)
                        xml = this._Individual.WorkPhoneNumber.PhoneNumber;
                    else if (this._Individual.HomePhoneNumber != null)
                        xml = this._Individual.HomePhoneNumber.PhoneNumber;
                    break;
                case CmCommand.FullPhoneNumber:
                    if (this._Individual.WorkPhoneNumber != null)
                        xml = this._Individual.WorkPhoneNumber.ToString();
                    else if (this._Individual.HomePhoneNumber != null)
                        xml = this._Individual.HomePhoneNumber.ToString();
                    break;
                default:
                    this._Individual.OrganizationStatus = this.OrganizationStatus;
                    xml = this._Individual.ToXmlString();
                    if (ClientId == "" || ClientId == null)
                    {
                        UpDateEventLog.UpdateLog("Individual", _Individual.Name, type, this.Broker);
                    }
                    else
                    {
                        UpDateEventLog.UpdateLog("Individual", _Individual.Name + " (" + ClientId + ")", type, this.Broker);
                    }

                    break;
            }
            return xml;
        }



        private string GetEntitySecurity()
        {
            EntitySecurity entity = new EntitySecurity();

            PartyDS ds = new PartyDS();
            base.OnGetData(ds);
            foreach (DataRow row in ds.Tables[PartyDS.INCLUDEDPARTY_TABLE].Rows)
            {
                entity.IncludedUsers.Add(new UserEntity() { CID = new Guid(row[PartyDS.PARTYCID_FIELD].ToString()), CurrentUserName = row[PartyDS.PARTYNAME_FIELD].ToString() });
            }
            foreach (DataRow row in ds.Tables[PartyDS.AVAILABLEPARTY_TABLE].Rows)
            {
                if (row[PartyDS.PARTYNAME_FIELD].ToString().ToLower() != "Administrator, Administrator".ToLower())
                {
                    entity.AvailableUsers.Add(new UserEntity() { CID = new Guid(row[PartyDS.PARTYCID_FIELD].ToString()), CurrentUserName = row[PartyDS.PARTYNAME_FIELD].ToString() });
                }
            }

            return entity.ToXmlString();
        }

        private string DeleteEntitySecurity(string UserEntity)
        {
            UserEntity entity = UserEntity.ToData<UserEntity>();

            this.Broker.SetWriteStart();

            PartyDS ds = new PartyDS();
            base.OnGetData(ds);
            ds.AcceptChanges();

            DataRow[] drCollection = ds.Tables[PartyDS.INCLUDEDPARTY_TABLE].Select(PartyDS.PARTYCID_FIELD + " = '" + entity.CID + "'", String.Empty);
            drCollection[0].Delete();
            this.SetData(ds);

            return string.Empty;
        }
        #region GetXMLData

        public override string GetXmlData()
        {
            string xmlco = _Individual.ToXmlString();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmlco);
            XmlAttribute rootAtt = doc.CreateAttribute("Type");
            rootAtt.Value = this.TypeName;
            doc.DocumentElement.Attributes.Append(rootAtt);
            return doc.OuterXml;
        }

        private string ExtractXML(string data)
        {
            string xml = string.Empty;
            string xmlco = _Individual.ToXmlString();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmlco);

            XMLExtensions.AddNode("Relationship", data, doc);
            XMLExtensions.AddNode("Title", this._Individual.PersonalTitle.Title, doc);
            XMLExtensions.AddNode("LastName", this._Individual.Surname, doc);
            XMLExtensions.AddNode("FirstName", this._Individual.Name, doc);
            XMLExtensions.AddNode("OtherNames", "NaN", doc);
            XMLExtensions.AddNode("Email", this._Individual.EmailAddress, doc);
            XMLExtensions.AddNode("WorkPhone", this._Individual.WorkPhoneNumber.PhoneNumber, doc);
            XMLExtensions.AddNode("HomePhone", this._Individual.HomePhoneNumber.PhoneNumber, doc);
            XMLExtensions.AddNode("Mobile", this._Individual.MobilePhoneNumber.MobileNumber, doc);
            XMLExtensions.AddNode("Fax", "NaN", doc);
            XMLExtensions.AddNode("Address", this._Individual.ResidentialAddress.Addressline1, doc);
            XMLExtensions.AddNode("Street", this._Individual.ResidentialAddress.Addressline2, doc);
            XMLExtensions.AddNode("Suburb", this._Individual.ResidentialAddress.Suburb, doc);
            XMLExtensions.AddNode("State", this._Individual.ResidentialAddress.State, doc);
            XMLExtensions.AddNode("Postcode", this._Individual.ResidentialAddress.PostCode, doc);
            XMLExtensions.AddNode("Country", this._Individual.ResidentialAddress.Country, doc);

            XMLExtensions.RemoveNode("PersonalTitle", doc);
            XMLExtensions.RemoveNode("Name", doc);
            XMLExtensions.RemoveNode("Surname", doc);
            XMLExtensions.RemoveNode("EmailAddress", doc);
            XMLExtensions.RemoveNode("HasAddress", doc);
            XMLExtensions.RemoveNode("MailingAddressSame", doc);
            XMLExtensions.RemoveNode("MobilePhoneNumber", doc);
            XMLExtensions.RemoveNode("HomePhoneNumber", doc);
            XMLExtensions.RemoveNode("WorkPhoneNumber", doc);

            XMLExtensions.RemoveNode("LicenseNumber", doc);
            XMLExtensions.RemoveNode("PassportNumber", doc);
            XMLExtensions.RemoveNode("Password", doc);
            XMLExtensions.RemoveNode("TFN", doc);
            XMLExtensions.RemoveNode("TIN", doc);

            XmlAttribute rootAtt = doc.CreateAttribute("Type");
            rootAtt.Value = this.TypeName;
            doc.DocumentElement.Attributes.Append(rootAtt);
            xml = doc.OuterXml;
            XDocument document = XDocument.Load(XmlReader.Create(new StringReader(xml)));
            document.Root.Name = data;
            document.Root.RemoveAttributes();
            xml = document.ToString().Trim();
            return xml;
        }

        #endregion

        #region GetCSVData
        private string GetCSVObject()
        {
            return _Individual.ToXmlString();
        }
        #endregion

        public void Export(XElement root)
        {
            _Individual.OrganizationStatus = this.OrganizationStatus;
            root.Add(_Individual.ToXElement("Individual"));
        }

        public void Import(XElement root, IdentityCmMap map)
        {
            _Individual = root.FromXElement<Oritax.TaxSimp.Common.IndividualEntity>("Individual");
            OrganizationStatus = _Individual.OrganizationStatus;
            Name = _Individual.Name;
        }

        public override string GetAllDataStream(int type)
        {
            return this._Individual.ToXmlString();
        }

        public override void DoTagged(EntityTag tag, Guid modelId)
        {
            string appender = tag.Appender;
            _Individual.DoTaggedExtension<Oritax.TaxSimp.Common.IndividualEntity>(tag);

            _Individual.ResidentialAddress.DoTaggedExtension<Oritax.TaxSimp.Common.AddressEntity>(tag.SetAppender(appender + ".ResidentialAddress"));




            if (!_Individual.MailingAddressSame)
                _Individual.MailingAddress.DoTaggedExtension<Oritax.TaxSimp.Common.AddressEntity>(tag.SetAppender(appender + ".MailingAddress"));

            tag.SetAppender(appender);
        }


        public override List<IDictionary<string, string>> DoPartialTag(EntityTag tag)
        {
            List<IDictionary<string, string>> list = new List<IDictionary<string, string>>();
            string appender = tag.Appender;
            Dictionary<string, string> newDictionary = new Dictionary<string, string>();
            foreach (var each in _Individual.DoPartialTagExtension<Oritax.TaxSimp.Common.IndividualEntity>(tag))
            {
                newDictionary.Add(each.Key, each.Value);
            }
            foreach (var each in _Individual.ResidentialAddress.DoPartialTagExtension<Oritax.TaxSimp.Common.AddressEntity>(tag.SetAppender(appender + ".ResidentialAddress")))
            {
                newDictionary.Add(each.Key, each.Value);
            }



            if (!_Individual.MailingAddressSame)
            {
                foreach (var each in _Individual.MailingAddress.DoPartialTagExtension<Oritax.TaxSimp.Common.AddressEntity>(
                    tag.SetAppender(appender + ".MailingAddress")))
                {
                    newDictionary.Add(each.Key, each.Value);
                }
            }

            int count = 1;
            if (_Individual.HasDocDrivingLicence)
            {

                foreach (var each in _Individual.DocDrivingLicence.DoPartialTagExtension<Oritax.TaxSimp.Common.DocumentTypeEntity>(
                    tag.SetAppender(appender + ".Document" + count)))
                {
                    newDictionary.Add(each.Key, each.Value);
                }
                count++;
            }

            if (_Individual.HasDocAustralianPassport)
            {

                foreach (var each in _Individual.DocAustralianPassport.DoPartialTagExtension<Oritax.TaxSimp.Common.DocumentTypeEntity>(
                    tag.SetAppender(appender + ".Document" + count)))
                {
                    newDictionary.Add(each.Key, each.Value);
                }
                count++;
            }

            if (_Individual.HasDocForeignPassport)
            {

                foreach (var each in _Individual.DocForeignPassport.DoPartialTagExtension<Oritax.TaxSimp.Common.DocumentTypeEntity>(
                    tag.SetAppender(appender + ".Document" + count)))
                {
                    newDictionary.Add(each.Key, each.Value);
                }
                count++;
            }

            if (_Individual.HasDocProofOfAge)
            {

                foreach (var each in _Individual.DocProofOfAge.DoPartialTagExtension<Oritax.TaxSimp.Common.DocumentTypeEntity>(
                    tag.SetAppender(appender + ".Document" + count)))
                {
                    newDictionary.Add(each.Key, each.Value);
                }
                count++;
            }


            if (_Individual.HasDocVEDA)
            {

                foreach (var each in _Individual.DocVEDA.DoPartialTagExtension<Oritax.TaxSimp.Common.DocumentTypeEntity>(
                    tag.SetAppender(appender + ".Document" + count)))
                {
                    newDictionary.Add(each.Key, each.Value);
                }
                count++;
            }



            tag.SetAppender(appender);
            list.Add(newDictionary);
            return list;
        }




        public bool UpdateModels(Oritax.TaxSimp.Common.IndividualEntity entity)
        {

            var models = new List<string>();
            //models.Add(entity.Servicetype.DoItForMe.ProgramCode);
            //models.Add(entity.Servicetype.DoItWithMe.ProgramCode);
            //models.Add(entity.Servicetype.DoItYourSelf.ProgramCode);
            return this.AssociateModel(models);
        }


        #region OrganizationsChart

        public OrganizationChart GetOrganizationChart()
        {
            OrganizationChart chart = new OrganizationChart();

            chart.Clid = this.CLID;
            chart.Csid = this.CSID;
            chart.NodeName = this._Individual.Name;

            return chart;
        }
        protected override void OnGetData(DataSet data)
        {
            base.OnGetData(data);
            _Individual.OrganizationStatus = this.OrganizationStatus;
            if (data is IndividualDS)
            {
                var dataSet = data as IndividualDS;
                DataRow dr = data.Tables[dataSet.IndividualTable.TABLENAME].NewRow();
                dr[dataSet.IndividualTable.CLIENTID] = this.ClientId;
                dr[dataSet.IndividualTable.CID] = CID;
                dr[dataSet.IndividualTable.CLID] = CLID;
                dr[dataSet.IndividualTable.CSID] = CSID;
                dr[dataSet.IndividualTable.PERSONALTITLE] = Individual.PersonalTitle.Title;
                dr[dataSet.IndividualTable.NAME] = Individual.Name;
                dr[dataSet.IndividualTable.SURNAME] = Individual.Surname;
                dr[dataSet.IndividualTable.FULLNAME] = Individual.Fullname;
                dr[dataSet.IndividualTable.MIDDLENAME] = Individual.MiddleName;
                dr[dataSet.IndividualTable.EMAILADDRESS] = Individual.EmailAddress;
                dr[dataSet.IndividualTable.GENDER] = Individual.Gender;
                dr[dataSet.IndividualTable.WORKCOUNTRYCODE] = Individual.WorkPhoneNumber.CountryCode;
                dr[dataSet.IndividualTable.WORKCITYCODE] = Individual.WorkPhoneNumber.CityCode;
                dr[dataSet.IndividualTable.WORKPHONENUMBER] = Individual.WorkPhoneNumber.PhoneNumber;

                dr[dataSet.IndividualTable.HOMECOUNTRYCODE] = Individual.HomePhoneNumber.CountryCode;
                dr[dataSet.IndividualTable.HOMECITYCODE] = Individual.HomePhoneNumber.CityCode;
                dr[dataSet.IndividualTable.HOMEPHONENUMBER] = Individual.HomePhoneNumber.PhoneNumber;

                dr[dataSet.IndividualTable.MOBILECOUNTRYCODE] = Individual.MobilePhoneNumber.CountryCode;
                dr[dataSet.IndividualTable.MOBILEPHONENUMBER] = Individual.MobilePhoneNumber.MobileNumber;

                dr[dataSet.IndividualTable.TFN] = Individual.TFN;
                dr[dataSet.IndividualTable.TIN] = Individual.TIN;

                dr[dataSet.IndividualTable.FACSIMILECOUNTYCODE] = Individual.Facsimile.CountryCode;
                dr[dataSet.IndividualTable.FACSIMILECITYCODE] = Individual.Facsimile.CityCode;
                dr[dataSet.IndividualTable.FACSIMILE] = Individual.Facsimile.PhoneNumber;

                if (Individual.DOB.HasValue)
                {
                    dr[dataSet.IndividualTable.DOB] = Individual.DOB.Value;
                }
                else
                {
                    dr[dataSet.IndividualTable.DOB] = DBNull.Value;
                }
                dr[dataSet.IndividualTable.PASSWORD] = Individual.Password;
                dr[dataSet.IndividualTable.OCCUPATION] = Individual.Occupation;
                dr[dataSet.IndividualTable.EMPLOYER] = Individual.Employer;

                dr[dataSet.IndividualTable.LICENSENUMBER] = Individual.LicenseNumber;
                dr[dataSet.IndividualTable.PASSPORTNUMBER] = Individual.PassportNumber;

                dr[dataSet.IndividualTable.RESIDENTIALADDRESSLINE1] = Individual.ResidentialAddress.Addressline1;
                dr[dataSet.IndividualTable.RESIDENTIALADDRESSLINE2] = Individual.ResidentialAddress.Addressline2;
                dr[dataSet.IndividualTable.RESIDENTIALADDRESSSUBRUB] = Individual.ResidentialAddress.Suburb;
                dr[dataSet.IndividualTable.RESIDENTIALADDRESSSTATE] = Individual.ResidentialAddress.State;
                dr[dataSet.IndividualTable.RESIDENTIALADDRESSPOSTALCODE] = Individual.ResidentialAddress.PostCode;
                dr[dataSet.IndividualTable.RESIDENTIALADDRESSCOUNTRY] = Individual.ResidentialAddress.Country;

                dr[dataSet.IndividualTable.MAILINGADDRESSLINE1] = Individual.MailingAddress.Addressline1;
                dr[dataSet.IndividualTable.MAILINGADDRESSLINE2] = Individual.MailingAddress.Addressline2;
                dr[dataSet.IndividualTable.MAILINGADDRESSSUBRUB] = Individual.MailingAddress.Suburb;
                dr[dataSet.IndividualTable.MAILINGADDRESSSTATE] = Individual.MailingAddress.State;
                dr[dataSet.IndividualTable.MAILINGADDRESSPOSTALCODE] = Individual.MailingAddress.PostCode;
                dr[dataSet.IndividualTable.MAILINGADDRESSCOUNTRY] = Individual.MailingAddress.Country;

                dr[dataSet.IndividualTable.MAILINGADDRESSSAME] = Individual.MailingAddressSame;
                dr[dataSet.IndividualTable.ORGANIZATIONSTATUS] = Individual.OrganizationStatus;
                this.OrganizationStatus = Individual.OrganizationStatus;
                dr[dataSet.IndividualTable.HASADDRESS] = Individual.HasAddress;
                dr[dataSet.IndividualTable.ORGANIZATIONTYPE] = this.TypeName;

                if (_Individual.Advisor != null)
                {
                    Guid adviserCid = _Individual.Advisor.Cid;
                    if (_Individual.Advisor.Clid != Guid.Empty && _Individual.Advisor.Csid != Guid.Empty && _Individual.Advisor.Cid == Guid.Empty)
                    {
                        var comp = Broker.GetCMImplementation(_Individual.Advisor.Clid, _Individual.Advisor.Csid);
                        if (comp != null)
                            adviserCid = comp.CID;
                        Broker.ReleaseBrokerManagedComponent(comp);
                    }
                    dr[dataSet.IndividualTable.ADVISERCID] = adviserCid;
                    dr[dataSet.IndividualTable.ADVISERCLID] = _Individual.Advisor.Clid;
                    dr[dataSet.IndividualTable.ADVISERCSID] = _Individual.Advisor.Csid;
                }

                dataSet.Tables[dataSet.IndividualTable.TABLENAME].Rows.Add(dr);
            }
            else if (data is InstitutionContactsDS)
            {
                var dataSet = data as InstitutionContactsDS;
                DataRow dr = data.Tables[dataSet.IndividualTable.TABLENAME].NewRow();
                dr[dataSet.IndividualTable.CID] = CID;
                dr[dataSet.IndividualTable.CLID] = CLID;
                dr[dataSet.IndividualTable.CSID] = CSID;
                dr[dataSet.IndividualTable.PERSONALTITLE] = Individual.PersonalTitle.Title;
                dr[dataSet.IndividualTable.NAME] = Individual.Name;
                dr[dataSet.IndividualTable.SURNAME] = Individual.Surname;
                dr[dataSet.IndividualTable.FULLNAME] = Individual.Fullname;
                dr[dataSet.IndividualTable.MIDDLENAME] = Individual.MiddleName;
                dr[dataSet.IndividualTable.EMAILADDRESS] = Individual.EmailAddress;
                dr[dataSet.IndividualTable.GENDER] = Individual.Gender;
                dr[dataSet.IndividualTable.WORKCOUNTRYCODE] = Individual.WorkPhoneNumber.CountryCode;
                dr[dataSet.IndividualTable.WORKCITYCODE] = Individual.WorkPhoneNumber.CityCode;
                dr[dataSet.IndividualTable.WORKPHONENUMBER] = Individual.WorkPhoneNumber.PhoneNumber;

                dr[dataSet.IndividualTable.HOMECOUNTRYCODE] = Individual.HomePhoneNumber.CountryCode;
                dr[dataSet.IndividualTable.HOMECITYCODE] = Individual.HomePhoneNumber.CityCode;
                dr[dataSet.IndividualTable.HOMEPHONENUMBER] = Individual.HomePhoneNumber.PhoneNumber;

                dr[dataSet.IndividualTable.MOBILECOUNTRYCODE] = Individual.MobilePhoneNumber.CountryCode;
                dr[dataSet.IndividualTable.MOBILEPHONENUMBER] = Individual.MobilePhoneNumber.MobileNumber;

                dr[dataSet.IndividualTable.TFN] = Individual.TFN;
                dr[dataSet.IndividualTable.TIN] = Individual.TIN;

                dr[dataSet.IndividualTable.FACSIMILECOUNTYCODE] = Individual.Facsimile.CountryCode;
                dr[dataSet.IndividualTable.FACSIMILECITYCODE] = Individual.Facsimile.CityCode;
                dr[dataSet.IndividualTable.FACSIMILE] = Individual.Facsimile.PhoneNumber;

                if (Individual.DOB.HasValue)
                {
                    dr[dataSet.IndividualTable.DOB] = Individual.DOB.Value;
                }
                else
                {
                    dr[dataSet.IndividualTable.DOB] = DBNull.Value;
                }
                dr[dataSet.IndividualTable.PASSWORD] = Individual.Password;
                dr[dataSet.IndividualTable.OCCUPATION] = Individual.Occupation;
                dr[dataSet.IndividualTable.EMPLOYER] = Individual.Employer;

                dr[dataSet.IndividualTable.LICENSENUMBER] = Individual.LicenseNumber;
                dr[dataSet.IndividualTable.PASSPORTNUMBER] = Individual.PassportNumber;

                dr[dataSet.IndividualTable.RESIDENTIALADDRESSLINE1] = Individual.ResidentialAddress.Addressline1;
                dr[dataSet.IndividualTable.RESIDENTIALADDRESSLINE2] = Individual.ResidentialAddress.Addressline2;
                dr[dataSet.IndividualTable.RESIDENTIALADDRESSSUBRUB] = Individual.ResidentialAddress.Suburb;
                dr[dataSet.IndividualTable.RESIDENTIALADDRESSSTATE] = Individual.ResidentialAddress.State;
                dr[dataSet.IndividualTable.RESIDENTIALADDRESSPOSTALCODE] = Individual.ResidentialAddress.PostCode;
                dr[dataSet.IndividualTable.RESIDENTIALADDRESSCOUNTRY] = Individual.ResidentialAddress.Country;

                dr[dataSet.IndividualTable.MAILINGADDRESSLINE1] = Individual.MailingAddress.Addressline1;
                dr[dataSet.IndividualTable.MAILINGADDRESSLINE2] = Individual.MailingAddress.Addressline2;
                dr[dataSet.IndividualTable.MAILINGADDRESSSUBRUB] = Individual.MailingAddress.Suburb;
                dr[dataSet.IndividualTable.MAILINGADDRESSSTATE] = Individual.MailingAddress.State;
                dr[dataSet.IndividualTable.MAILINGADDRESSPOSTALCODE] = Individual.MailingAddress.PostCode;
                dr[dataSet.IndividualTable.MAILINGADDRESSCOUNTRY] = Individual.MailingAddress.Country;

                dr[dataSet.IndividualTable.MAILINGADDRESSSAME] = Individual.MailingAddressSame;
                dr[dataSet.IndividualTable.ORGANIZATIONSTATUS] = Individual.OrganizationStatus;
                this.OrganizationStatus = Individual.OrganizationStatus;
                dr[dataSet.IndividualTable.HASADDRESS] = Individual.HasAddress;
                dr[dataSet.IndividualTable.ORGANIZATIONTYPE] = this.TypeName;
                if (_Individual.Advisor != null)
                {
                    Guid adviserCid = _Individual.Advisor.Cid;
                    if (_Individual.Advisor.Clid != Guid.Empty && _Individual.Advisor.Csid != Guid.Empty && _Individual.Advisor.Cid == Guid.Empty)
                    {
                        var comp = Broker.GetCMImplementation(_Individual.Advisor.Clid, _Individual.Advisor.Csid);
                        adviserCid = comp.CID;
                        Broker.ReleaseBrokerManagedComponent(comp);
                    }
                    dr[dataSet.IndividualTable.ADVISERCID] = adviserCid;
                    dr[dataSet.IndividualTable.ADVISERCLID] = _Individual.Advisor.Clid;
                    dr[dataSet.IndividualTable.ADVISERCSID] = _Individual.Advisor.Csid;
                }
                dataSet.Tables[dataSet.IndividualTable.TABLENAME].Rows.Add(dr);
            }
            else if (data is ClientDetailsDS)
            {
                GetIndividualDetailsDs(this.ClientId, data as ClientDetailsDS, Individual, Broker, TypeName);
            }
            else if (data is IndividualClientsMappingDS)
            {
                GetIndividualClientsDetailsDs(data as IndividualClientsMappingDS, Individual.ClientShares);
            }

            else if (data is IndividualCurrentAssetsDS)
            {
                GetIndividualAssestsDetailss(this.ClientId, data as IndividualCurrentAssetsDS, Individual, Broker, TypeName);
            }

        }

        private void GetIndividualClientsDetailsDs(IndividualClientsMappingDS ds, List<Common.ClientShare> clientShares)
        {
            if (clientShares != null && clientShares.Count > 0)
            {
                foreach (var clientShare in clientShares)
                {
                    var client = Broker.GetBMCInstance(clientShare.ClientCid) as IOrganizationUnit;
                    if (client != null)
                    {
                        ds.ClientMappingTable.AddRow(this.CID, this.Clid, this.Csid, client.EclipseClientID, client.CID, client.CLID, client.CSID, client.Name, client.TypeName, clientShare.Share);
                        Broker.ReleaseBrokerManagedComponent(client);
                    }
                }
            }
        }


        private void GetIndividualAssestsDetailss(string clientId, IndividualCurrentAssetsDS clientDetailsDs, IndividualEntity individual, ICMBroker broker, string s)
        {
            if (individual.ClientShares != null && individual.ClientShares.Count > 0)
            {
                foreach (var clientShare in individual.ClientShares)
                {
                    var holdings = GetClientData(clientShare.ClientCid);
                    if (clientShare.Share.HasValue && clientShare.Share.Value > 0)
                        AddClientRecord(clientShare.ClientID, clientDetailsDs, holdings, (double)clientShare.Share.Value);
                }

            }
        }

        private void AddClientRecord(string ClientID, IndividualCurrentAssetsDS clientDetailsDs, HoldingRptDataSet holdings, double share)
        {
            var dr = clientDetailsDs.assetsDetailsTable.NewRow();
            dr[clientDetailsDs.assetsDetailsTable.CLIENTID] = ClientID;
            var query = string.Format("{0}='AEQ'", holdings.HoldingSummaryTable.ASSETNAME);
            dr[clientDetailsDs.assetsDetailsTable.AUSTRALIANEQUITIES] = GetColumnSum(holdings, query, holdings.HoldingSummaryTable.TOTAL) * (share / 100);

            query = string.Format("{0}='FI'", holdings.HoldingSummaryTable.ASSETNAME);
            dr[clientDetailsDs.assetsDetailsTable.AUSTRALIANFIXEDINTEREST] = GetColumnSum(holdings, query, holdings.HoldingSummaryTable.TOTAL) * (share / 100);

            query = string.Format("{0}='GS'", holdings.HoldingSummaryTable.ASSETNAME);
            dr[clientDetailsDs.assetsDetailsTable.INTERNATIONALEQUITIES] = GetColumnSum(holdings, query, holdings.HoldingSummaryTable.TOTAL) * (share / 100);

            query = string.Format("{0}='Cash'", holdings.HoldingSummaryTable.ASSETNAME);
            dr[clientDetailsDs.assetsDetailsTable.CASH] = GetColumnSum(holdings, query, holdings.HoldingSummaryTable.TOTAL) * (share / 100);

            query = string.Format("{0}='PRO'", holdings.HoldingSummaryTable.ASSETNAME);
            dr[clientDetailsDs.assetsDetailsTable.PROPERTY] = GetColumnSum(holdings, query, holdings.HoldingSummaryTable.TOTAL) * (share / 100);

            query = string.Format("{0}='ALT'", holdings.HoldingSummaryTable.ASSETNAME);
            dr[clientDetailsDs.assetsDetailsTable.GOLD] = GetColumnSum(holdings, query, holdings.HoldingSummaryTable.TOTAL) * (share / 100);


            clientDetailsDs.assetsDetailsTable.Rows.Add(dr);
        }

        private static double GetColumnSum(HoldingRptDataSet holdings, string query, string columnName)
        {
            double sum = 0;
            var drs = holdings.HoldingSummaryTable.Select(query);

            foreach (DataRow dataRow in drs)
            {
                double holding;
                if (double.TryParse(dataRow[columnName].ToString(), out holding))
                    sum += holding;
            }

            return sum;
        }

        private HoldingRptDataSet GetClientData(Guid cid)
        {
            IBrokerManagedComponent clientData = Broker.GetBMCInstance(cid);
            var ds = new HoldingRptDataSet();
            clientData.GetData(ds);
            Broker.ReleaseBrokerManagedComponent(clientData);
            return ds;
        }
        private void GetIndividualDetailsDs(string ClientID, ClientDetailsDS dataSet, IndividualEntity clientDetails, ICMBroker Broker, string type)
        {
            DataRow dr = dataSet.Tables[dataSet.ClientDetailsTable.TABLENAME].NewRow();
            dr[dataSet.ClientDetailsTable.CLIENTID] = ClientID;
            dr[dataSet.ClientDetailsTable.CLIENTNAME] = clientDetails.Fullname;

            dr[dataSet.ClientDetailsTable.INVESTORTYPE] = type;

            dr[dataSet.ClientDetailsTable.STATUSFLAG] = clientDetails.OrganizationStatus;

            dataSet.Tables[dataSet.ClientDetailsTable.TABLENAME].Rows.Add(dr);
        }
        protected override Calculation.BrokerManagedComponent.ModifiedState OnSetData(System.Data.DataSet data)
        {
            base.OnSetData(data);

            if (data is ClientDataOperationDS)
            {
                ClientDataOperationDS clientDataOperationDS = (ClientDataOperationDS)data;
                if (clientDataOperationDS.CommandType == ClientDataSetOperationCommandTypes.PhoneFix)
                {
                    PhoneNumberEntity.FixPhoneNumbersAndMobile(Individual.HomePhoneNumber, Individual.MobilePhoneNumber);
                    PhoneNumberEntity.FixPhoneNumbersAndMobile(Individual.WorkPhoneNumber, Individual.MobilePhoneNumber);
                }
            }
            else if (data is IndividualDS)
                {
                    var individualDS = (IndividualDS)data;
                    DataRow row = individualDS.IndividualTable.Rows[0];

                    if (individualDS.CommandType == DatasetCommandTypes.UpdateAdviser)
                    {
                        if (_Individual.Advisor == null)
                            _Individual.Advisor = new Common.IdentityCMDetail();

                        _Individual.Advisor.Cid = (Guid)row[individualDS.IndividualTable.ADVISERCID];
                        _Individual.Advisor.Clid = (Guid)row[individualDS.IndividualTable.ADVISERCLID];
                        _Individual.Advisor.Csid = (Guid)row[individualDS.IndividualTable.ADVISERCSID];
                    }
                    else if (individualDS.CommandType == DatasetCommandTypes.RemoveAdviser)
                    {
                        _Individual.Advisor = new Common.IdentityCMDetail();
                    }
                    else
                    {
                        if (Individual == null)
                        {
                            _Individual = new IndividualEntity();

                        }
                        Individual.PersonalTitle.Title = row[individualDS.IndividualTable.PERSONALTITLE].ToString();
                        Individual.Name = row[individualDS.IndividualTable.NAME].ToString();
                        Individual.Surname = row[individualDS.IndividualTable.SURNAME].ToString();
                        Individual.Fullname = row[individualDS.IndividualTable.FULLNAME].ToString();
                        Individual.MiddleName = row[individualDS.IndividualTable.MIDDLENAME].ToString();
                        Individual.EmailAddress = row[individualDS.IndividualTable.EMAILADDRESS].ToString();
                        Individual.Gender = row[individualDS.IndividualTable.GENDER].ToString();
                        Individual.WorkPhoneNumber.CountryCode =
                            row[individualDS.IndividualTable.WORKCOUNTRYCODE].ToString();
                        Individual.WorkPhoneNumber.CityCode = row[individualDS.IndividualTable.WORKCITYCODE].ToString();
                        Individual.WorkPhoneNumber.PhoneNumber =
                            row[individualDS.IndividualTable.WORKPHONENUMBER].ToString();

                        Individual.HomePhoneNumber.CountryCode =
                            row[individualDS.IndividualTable.HOMECOUNTRYCODE].ToString();
                        Individual.HomePhoneNumber.CityCode = row[individualDS.IndividualTable.HOMECITYCODE].ToString();
                        Individual.HomePhoneNumber.PhoneNumber =
                            row[individualDS.IndividualTable.HOMEPHONENUMBER].ToString();

                        Individual.MobilePhoneNumber.CountryCode =
                            row[individualDS.IndividualTable.MOBILECOUNTRYCODE].ToString();
                        Individual.MobilePhoneNumber.MobileNumber =
                            row[individualDS.IndividualTable.MOBILEPHONENUMBER].ToString();


                        Individual.TFN = row[individualDS.IndividualTable.TFN].ToString();
                        Individual.TIN = row[individualDS.IndividualTable.TIN].ToString();

                        Individual.Facsimile.CountryCode = row[individualDS.IndividualTable.FACSIMILECOUNTYCODE].ToString();
                        Individual.Facsimile.CityCode = row[individualDS.IndividualTable.FACSIMILECITYCODE].ToString();
                        Individual.Facsimile.PhoneNumber = row[individualDS.IndividualTable.FACSIMILE].ToString();
                        if (row[individualDS.IndividualTable.DOB] != DBNull.Value)
                            Individual.DOB = (DateTime)row[individualDS.IndividualTable.DOB];
                        else
                        {
                            Individual.DOB = null;
                        }
                        Individual.Password = row[individualDS.IndividualTable.PASSWORD].ToString();
                        Individual.Occupation = row[individualDS.IndividualTable.OCCUPATION].ToString();
                        Individual.Employer = row[individualDS.IndividualTable.EMPLOYER].ToString();
                        Individual.LicenseNumber = row[individualDS.IndividualTable.LICENSENUMBER].ToString();
                        Individual.PassportNumber = row[individualDS.IndividualTable.PASSPORTNUMBER].ToString();

                        Individual.ResidentialAddress.Addressline1 =
                            row[individualDS.IndividualTable.RESIDENTIALADDRESSLINE1].ToString();
                        Individual.ResidentialAddress.Addressline2 =
                            row[individualDS.IndividualTable.RESIDENTIALADDRESSLINE2].ToString();
                        Individual.ResidentialAddress.Suburb =
                            row[individualDS.IndividualTable.RESIDENTIALADDRESSSUBRUB].ToString();
                        Individual.ResidentialAddress.State =
                            row[individualDS.IndividualTable.RESIDENTIALADDRESSSTATE].ToString();
                        Individual.ResidentialAddress.PostCode =
                            row[individualDS.IndividualTable.RESIDENTIALADDRESSPOSTALCODE].ToString();
                        Individual.ResidentialAddress.Country =
                            row[individualDS.IndividualTable.RESIDENTIALADDRESSCOUNTRY].ToString();

                        Individual.MailingAddress.Addressline1 =
                            row[individualDS.IndividualTable.MAILINGADDRESSLINE1].ToString();
                        Individual.MailingAddress.Addressline2 =
                            row[individualDS.IndividualTable.MAILINGADDRESSLINE2].ToString();
                        Individual.MailingAddress.Suburb = row[individualDS.IndividualTable.MAILINGADDRESSSUBRUB].ToString();
                        Individual.MailingAddress.State = row[individualDS.IndividualTable.MAILINGADDRESSSTATE].ToString();
                        Individual.MailingAddress.PostCode =
                            row[individualDS.IndividualTable.MAILINGADDRESSPOSTALCODE].ToString();
                        Individual.MailingAddress.Country =
                            row[individualDS.IndividualTable.MAILINGADDRESSCOUNTRY].ToString();

                        Individual.MailingAddressSame =
                            Convert.ToBoolean(row[individualDS.IndividualTable.MAILINGADDRESSSAME]);

                        this.OrganizationStatus = Individual.OrganizationStatus = row[individualDS.IndividualTable.ORGANIZATIONSTATUS].ToString();
                        Individual.HasAddress = Convert.ToBoolean(row[individualDS.IndividualTable.HASADDRESS]);
                    }
                }
            else if (data is IndividualClientsMappingDS)
                    {
                        if (Individual.ClientShares == null)
                            Individual.ClientShares = new List<ClientShare>();
                        SetIndividualClientsDetailsDs(data as IndividualClientsMappingDS, Individual.ClientShares);
                    }
            #region set default values
            this.Name = _Individual.Fullname;
            this.OrganizationStatus = _Individual.OrganizationStatus;
            #endregion

            return ModifiedState.MODIFIED;
        }

        private void SetIndividualClientsDetailsDs(IndividualClientsMappingDS ds, List<ClientShare> clientShares)
        {
            if (ds.ClientMappingTable != null)
            {
                var rows=ds.ClientMappingTable.Select(ds.ClientMappingTable.INDICID+"='"+CID+"'");//select Individual data only
                foreach (DataRow row in rows)
                {
                    row[ds.ClientMappingTable.INDICLID] = CLID;
                    row[ds.ClientMappingTable.INDICSID] = CSID;
                    var clientCid = (Guid) row[ds.ClientMappingTable.CID];
                    var clientShare = clientShares.FirstOrDefault(ss => ss.ClientCid == clientCid);
                    var client = Broker.GetBMCInstance(clientCid);
                    if (client != null)
                    {
                        client.SetData(ds); //take actions on client client first
                    }
                    switch (ds.CommandType)
                    {
                        case DatasetCommandTypes.Update:
                            if (clientShare != null)
                            {
                                clientShare.Share = (decimal?) (row[ds.ClientMappingTable.INDISHARE] == DBNull.Value ? null : row[ds.ClientMappingTable.INDISHARE]);
                            }
                            break;
                        case DatasetCommandTypes.Add:
                            if (clientShare == null)
                            {
                                clientShares.Add(new ClientShare {ClientCid = (Guid) row[ds.ClientMappingTable.CID], ClientClid = (Guid) row[ds.ClientMappingTable.CLID], ClientCsid = (Guid) row[ds.ClientMappingTable.CSID], ClientID = (string) row[ds.ClientMappingTable.CLIENTID], Share = (decimal?) (row[ds.ClientMappingTable.INDISHARE] == DBNull.Value ? null : row[ds.ClientMappingTable.INDISHARE])});
                            }
                            break;
                        case DatasetCommandTypes.Delete:
                            if (clientShare != null)
                            {
                                clientShares.Remove(clientShare);
                            }

                            break;

                    }
                }
            }
        }

        public OrganizationChart GetAssociation(IIdentityCM identitycm, OrganizationType organizationtype)
        {
            OrganizationChart OChart = new OrganizationChart();
            return OChart;
        }

        public OrganizationChart GetEntityParent(OrganizationChart organizationchart)
        {
            return null;
        }

        public OrganizationChart GetEntityChild()
        {
            return null;
        }

        public OrganizationChart GetEntityChildWithEntity()
        {
            return null;
        }

        #endregion

    }
}
