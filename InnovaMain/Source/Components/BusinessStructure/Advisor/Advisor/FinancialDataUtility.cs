#region Using
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.Entity;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.Extensions;
using System.Collections.ObjectModel;
#endregion

namespace Oritax.TaxSimp.CM
{
    [Serializable]
    public class FinancialDataUtility
    {
        public void GetBankAccountDataSet(BankTransactionDS bankTransactionDS, IOrganization orgCM, List<AccountProcessTaskEntity> ListAccountProcess, ICMBroker Broker)
        {
            foreach (var accountProcessTaskEntity in ListAccountProcess)
            {
                if (accountProcessTaskEntity.LinkedEntity != null && accountProcessTaskEntity.LinkedEntity.Clid != Guid.Empty)
                {
                    var asset = orgCM.Assets.Where(var => var.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                    var model = orgCM.Model.Where(mod => mod.ID == accountProcessTaskEntity.ModelID).FirstOrDefault();
                    var linkedAsset = model.Assets.Where(ast => ast.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                    var product = linkedAsset.Products.Where(pro => pro.ID == accountProcessTaskEntity.TaskID).FirstOrDefault();

                    if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.BankAccount)
                    {
                        BankAccountCM bankAccount = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as BankAccountCM;

                        if (bankAccount.BankAccountEntity.CashManagementTransactions.Count > 0)
                        {

                            DataSet ds =
                            BaseClientDetails.ConvertXMLToDataSet(bankAccount.BankAccountEntity.CashManagementTransactions.ToXmlString());
                            ds.Tables[0].TableName = "Cash Transactions";
                            ds.Tables[0].Columns.Add("BSB");
                            ds.Tables[0].Columns.Add("AccountType");
                            ds.Tables[0].Columns.Add("AccountNo");
                            ds.Tables[0].Columns.Add("AmountTotal", typeof(decimal));
                            ds.Tables[0].Columns.Add("BankAmount", typeof(decimal));
                            ds.Tables[0].Columns.Add("BankAdjustment", typeof(decimal));
                            ds.Tables[0].Columns.Add("BankTransactionDate", typeof(DateTime));

                            foreach (DataRow row in ds.Tables[0].Rows)
                            {
                                row["BSB"] = bankAccount.BankAccountEntity.BSB;
                                row["AccountType"] = bankAccount.BankAccountEntity.AccoutType.ToUpper();
                                row["AccountNo"] = bankAccount.BankAccountEntity.AccountNumber;
                                row["AmountTotal"] = bankAccount.BankAccountEntity.CashManagementTransactions.Where(cashTran => cashTran.ID == new Guid(row["ID"].ToString())).FirstOrDefault().TotalAmount;
                                row["BankAmount"] = bankAccount.BankAccountEntity.CashManagementTransactions.Where(cashTran => cashTran.ID == new Guid(row["ID"].ToString())).FirstOrDefault().Amount;
                                row["BankAdjustment"] = bankAccount.BankAccountEntity.CashManagementTransactions.Where(cashTran => cashTran.ID == new Guid(row["ID"].ToString())).FirstOrDefault().Adjustment;
                                row["BankTransactionDate"] = DateTime.Parse(row["TransactionDate"].ToString());
                            }

                            bankTransactionDS.Merge(ds, false, MissingSchemaAction.Add);
                        }
                    }
                }
            }
        }

        public void GetTDsAccountDataSet(TDTransactionDS tdTransactionDS, IOrganization orgCM, List<AccountProcessTaskEntity> ListAccountProcess, ICMBroker Broker)
        {
            foreach (var accountProcessTaskEntity in ListAccountProcess)
            {
                if (accountProcessTaskEntity.LinkedEntity != null && accountProcessTaskEntity.LinkedEntity.Clid != Guid.Empty)
                {
                    var asset = orgCM.Assets.Where(var => var.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                    var model = orgCM.Model.Where(mod => mod.ID == accountProcessTaskEntity.ModelID).FirstOrDefault();
                    var linkedAsset = model.Assets.Where(ast => ast.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                    var product = linkedAsset.Products.Where(pro => pro.ID == accountProcessTaskEntity.TaskID).FirstOrDefault();

                    if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.TermDepositAccount)
                    {
                        BankAccountCM bankAccount = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as BankAccountCM;
                        if (bankAccount.BankAccountEntity.CashManagementTransactions.Count > 0)
                        {
                            DataSet ds =
                            BaseClientDetails.ConvertXMLToDataSet(bankAccount.BankAccountEntity.CashManagementTransactions.ToXmlString());
                            ds.Tables[0].TableName = "TDs Transactions";
                            ds.Tables[0].Columns.Add("BSB");
                            ds.Tables[0].Columns.Add("AccountType");
                            ds.Tables[0].Columns.Add("AccountNo");
                            ds.Tables[0].Columns.Add("InstituteName");
                            ds.Tables[0].Columns.Add("AmountTotal", typeof(decimal));
                            ds.Tables[0].Columns.Add("BankAmount", typeof(decimal));
                            ds.Tables[0].Columns.Add("BankAdjustment", typeof(decimal));
                            ds.Tables[0].Columns.Add("BankTransactionDate", typeof(DateTime));
                            ds.Tables[0].Columns.Add("BankMaturityDate", typeof(DateTime));

                            foreach (DataRow row in ds.Tables[0].Rows)
                            {
                                var inst = orgCM.Institution.Where(var => var.ID == new Guid(row["InstitutionID"].ToString())).FirstOrDefault();
                                row["BSB"] = bankAccount.BankAccountEntity.BSB;
                                row["AccountType"] = bankAccount.BankAccountEntity.AccoutType.ToUpper();
                                row["AccountNo"] = bankAccount.BankAccountEntity.AccountNumber;
                                row["AmountTotal"] = bankAccount.BankAccountEntity.CashManagementTransactions.Where(cashTran => cashTran.ID == new Guid(row["ID"].ToString())).FirstOrDefault().TotalAmount;
                                row["BankAmount"] = bankAccount.BankAccountEntity.CashManagementTransactions.Where(cashTran => cashTran.ID == new Guid(row["ID"].ToString())).FirstOrDefault().Amount;
                                row["BankAdjustment"] = bankAccount.BankAccountEntity.CashManagementTransactions.Where(cashTran => cashTran.ID == new Guid(row["ID"].ToString())).FirstOrDefault().Adjustment;
                                row["InstituteName"] = inst.Name;
                                row["BankMaturityDate"] = DateTime.Parse(row["MaturityDate"].ToString());
                                row["BankTransactionDate"] = DateTime.Parse(row["TransactionDate"].ToString());
                            }

                            tdTransactionDS.Merge(ds, false, MissingSchemaAction.Add);
                        }
                    }
                }
            }
        }

        public void GetASXTransactionDS(ASXTransactionDS asxTransactionDS, IOrganization orgCM, List<AccountProcessTaskEntity> ListAccountProcess, ICMBroker Broker)
        {
            DataTable dt = new DataTable("DesktopBroker Transactions");
            dt.Columns.Add("InvestmentCode");
            dt.Columns.Add("Name");
            dt.Columns.Add("TransactionType");
            dt.Columns.Add("AccountNo");
            dt.Columns.Add("FPSInstructionID");
            dt.Columns.Add("TradeDate", typeof(DateTime));
            dt.Columns.Add("SettlementDate", typeof(DateTime));
            dt.Columns.Add("Units");
            dt.Columns.Add("GrossValue", typeof(decimal));
            dt.Columns.Add("BrokerageAmount", typeof(decimal));
            dt.Columns.Add("Charges", typeof(decimal));
            dt.Columns.Add("BrokerageGST", typeof(decimal));
            dt.Columns.Add("NetValue", typeof(decimal));
            dt.Columns.Add("Narrative");

            foreach (var accountProcessTaskEntity in ListAccountProcess)
            {
                if (accountProcessTaskEntity.LinkedEntity != null && accountProcessTaskEntity.LinkedEntity.Clid != Guid.Empty)
                {
                    var asset = orgCM.Assets.Where(var => var.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                    var model = orgCM.Model.Where(mod => mod.ID == accountProcessTaskEntity.ModelID).FirstOrDefault();
                    var linkedAsset = model.Assets.Where(ast => ast.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                    var product = linkedAsset.Products.Where(pro => pro.ID == accountProcessTaskEntity.TaskID).FirstOrDefault();

                    if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.DesktopBrokerAccount)
                    {
                        DesktopBrokerAccountCM desktopBrokerAccountCM = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as DesktopBrokerAccountCM;

                        if (desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Count > 0)
                        {
                            foreach (HoldingTransactions holdingTransactions in desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions)
                            {
                                DataRow row = dt.NewRow();

                                row["Name"] = desktopBrokerAccountCM.DesktopBrokerAccountEntity.Name;
                                row["AccountNo"] = desktopBrokerAccountCM.DesktopBrokerAccountEntity.AccountNumber;
                                row["FPSInstructionID"] = holdingTransactions.FPSInstructionID;
                                row["InvestmentCode"] = holdingTransactions.InvestmentCode;
                                row["TransactionType"] = holdingTransactions.TransactionType;
                                row["TradeDate"] = holdingTransactions.TradeDate;
                                if (holdingTransactions.SettlementDate != null)
                                    row["SettlementDate"] = holdingTransactions.SettlementDate;
                                else
                                    row["SettlementDate"] = holdingTransactions.TradeDate;
                                row["Units"] = holdingTransactions.Units;
                                row["GrossValue"] = holdingTransactions.GrossValue;
                                row["BrokerageAmount"] = holdingTransactions.BrokerageAmount;
                                row["Charges"] = holdingTransactions.Charges;
                                row["BrokerageGST"] = holdingTransactions.BrokerageGST;
                                row["BrokerageAmount"] = holdingTransactions.BrokerageAmount;
                                row["NetValue"] = holdingTransactions.NetValue;
                                row["Narrative"] = holdingTransactions.Narrative;

                                dt.Rows.Add(row);
                            }

                            asxTransactionDS.Merge(dt, false, MissingSchemaAction.Add);
                        }
                    }
                }
            }
        }

        public void GetMISTransactionDS(MISTransactionDS misTransactionDS, IOrganization orgCM, List<AccountProcessTaskEntity> ListAccountProcess, ICMBroker Broker, string clientID)
        {
            DataSet ds = new DataSet();
            ds.Tables.Add("MIS Transactions");

            ds.Tables[0].Columns.Add("Code");
            ds.Tables[0].Columns.Add("Description");
            ds.Tables[0].Columns.Add("ID");
            ds.Tables[0].Columns.Add("TradeDate", typeof(DateTime));
            ds.Tables[0].Columns.Add("ClientID");
            ds.Tables[0].Columns.Add("TransactionType");
            ds.Tables[0].Columns.Add("Shares");
            ds.Tables[0].Columns.Add("UnitPrice", typeof(decimal));
            ds.Tables[0].Columns.Add("Amount", typeof(decimal));
            ds.Tables[0].Columns.Add("Status");

            foreach (var accountProcessTaskEntity in ListAccountProcess)
            {
                if (accountProcessTaskEntity.LinkedEntity != null && accountProcessTaskEntity.LinkedEntity.Clid != Guid.Empty)
                {
                    var asset = orgCM.Assets.Where(var => var.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                    var model = orgCM.Model.Where(mod => mod.ID == accountProcessTaskEntity.ModelID).FirstOrDefault();
                    var linkedAsset = model.Assets.Where(ast => ast.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                    var product = linkedAsset.Products.Where(pro => pro.ID == accountProcessTaskEntity.TaskID).FirstOrDefault();

                    if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.ManagedInvestmentSchemesAccount)
                    {
                        ManagedInvestmentSchemesAccountCM managedInvestmentSchemesAccountCM = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as ManagedInvestmentSchemesAccountCM;

                        var linkedMISAccount = managedInvestmentSchemesAccountCM.ManagedInvestmentSchemesAccountEntity.FundAccounts.Where(fa => product.TaskDescription.Contains(fa.Code)).FirstOrDefault();
                        var filteredHoldingTransactions = linkedMISAccount.FundTransactions.Where(cliFundTrans => cliFundTrans.ClientID == clientID);


                        foreach (MISFundTransactionEntity misFundTransactionEntity in filteredHoldingTransactions)
                        {
                            DataRow row = ds.Tables[0].NewRow();

                            row["Code"] = linkedMISAccount.Code;
                            row["Description"] = linkedMISAccount.Description;
                            row["ID"] = misFundTransactionEntity.ID;
                            row["TradeDate"] = misFundTransactionEntity.TradeDate;
                            row["ClientID"] = misFundTransactionEntity.ClientID;
                            row["TransactionType"] = misFundTransactionEntity.TransactionType;
                            row["Shares"] = misFundTransactionEntity.Shares;
                            row["UnitPrice"] = Math.Round(misFundTransactionEntity.UnitPrice, 4);
                            row["Amount"] = misFundTransactionEntity.Amount;
                            row["Status"] = misFundTransactionEntity.Status;

                            ds.Tables[0].Rows.Add(row);
                        }

                        
                    }
                }
            }

            misTransactionDS.Merge(ds, false, MissingSchemaAction.Add);
        }

        public void GetDIVTransactionDS(DIVTransactionDS divTransactionDS, IOrganization orgCM, ObservableCollection<DividendEntity> DividendCollection, ICMBroker Broker)
        {
            DataTable dt = new DataTable("Income Transactions");
            dt.Columns.Add("InvestmentCode");
            dt.Columns.Add("TransactionType");
            dt.Columns.Add("RecordDate", typeof(DateTime));
            dt.Columns.Add("PaymentDate", typeof(DateTime));
            dt.Columns.Add("UnitsOnHand", typeof(decimal));
            dt.Columns.Add("CentsPerShare", typeof(decimal));
            dt.Columns.Add("InvestmentAmount", typeof(decimal));
            dt.Columns.Add("PaidDividend", typeof(decimal));
            dt.Columns.Add("InterestPaid", typeof(decimal));
            dt.Columns.Add("InterestRate", typeof(decimal));

            foreach (DividendEntity entity in DividendCollection)
            {
                DataRow row = dt.NewRow();

                row["InvestmentCode"] = entity.InvestmentCode;
                row["TransactionType"] = entity.TransactionType;
                row["RecordDate"] = entity.RecordDate;
                row["PaymentDate"] = entity.PaymentDate;
                row["UnitsOnHand"] = entity.UnitsOnHand;
                row["CentsPerShare"] = entity.CentsPerShare;
                row["InvestmentAmount"] = entity.InvestmentAmount;
                row["PaidDividend"] = entity.PaidDividend;
                row["InterestPaid"] = entity.InterestPaid;
                row["InterestRate"] = entity.InterestRate;

                dt.Rows.Add(row);
            }

            divTransactionDS.Merge(dt, false, MissingSchemaAction.Add);
        }

        public void GetManualTransactionDS(ManualTransactionDS manualTransactionDS, IOrganization orgCM, ObservableCollection<ClientManualAssetEntity> ClientManualAssetCollection, ICMBroker Broker)
        {
             
            DataTable dt = new DataTable("Manual Transactions");
            dt.Columns.Add("ID");
            dt.Columns.Add("AdministrationSystem");
            dt.Columns.Add("InvestmentCode", typeof(string));
            dt.Columns.Add("InvestmentDesc", typeof(string));
            dt.Columns.Add("SettlementDate", typeof(DateTime));
            dt.Columns.Add("TransactionDate", typeof(DateTime));
            dt.Columns.Add("UnitsOnHand", typeof(decimal));
            dt.Columns.Add("UnitPrice", typeof(decimal));
            dt.Columns.Add("Holding", typeof(decimal));

            foreach (ClientManualAssetEntity entity in ClientManualAssetCollection)
            {
                var manualAssetDetails = orgCM.ManualAsset.Where(manAsset => manAsset.ID == entity.InvestmentCode).FirstOrDefault();
                var latestPrice = manualAssetDetails.ASXSecurity.OrderByDescending(unitprice => unitprice.Date).FirstOrDefault();
                
                DataRow row = dt.NewRow();

                row["ID"] = entity.InvestmentCode;
                row["TransactionDate"] = entity.TransactionDate;
                row["SettlementDate"] = entity.SettlementDate;
                row["AdministrationSystem"] = entity.AdministrationSystem;
                row["InvestmentCode"] = manualAssetDetails.AsxCode;
                row["InvestmentDesc"] = manualAssetDetails.CompanyName;
                row["UnitsOnHand"] = entity.UnitsOnHand;
                row["UnitPrice"] = Convert.ToDecimal(latestPrice.UnitPrice);
                row["Holding"] = entity.UnitsOnHand * Convert.ToDecimal(latestPrice.UnitPrice);

                dt.Rows.Add(row);
            }

            manualTransactionDS.Merge(dt, false, MissingSchemaAction.Add);
        }
    }
}