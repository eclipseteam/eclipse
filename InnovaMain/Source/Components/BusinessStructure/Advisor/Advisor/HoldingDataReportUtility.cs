﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.DataSets;
using System.Data;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.CM.Group;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CM.Entity;
using System.Collections.ObjectModel;

namespace Oritax.TaxSimp.CM
{
    public class HoldingDataReportUtility
    {
        public void OnGetDataHoldingRptDataSetCorporate(HoldingRptDataSet data, string clientType, string clientID, string clientName, IdentityCM Parent, ICMBroker Broker, CorporateEntity Entity)
        {
            DataTable clientSummaryTable = data.Tables[HoldingRptDataSet.CLIENTSUMMARYTABLE];
            DataTable contactTable = data.Tables[HoldingRptDataSet.CONTACTTABLE];

            DataRow clientSummaryRow = clientSummaryTable.NewRow();
            DataRow contactRow = contactTable.NewRow();

            BasicClientDetails(clientType, clientID, clientName, Parent, Broker,Entity.ABN, Entity.ACN, Entity.Address, clientSummaryRow);

            CSVInstance csvinstance = new CSVInstance(Broker);
            CorporateCSV _entitycsv = csvinstance.GetEntityConcreteInstance(Entity);
            foreach (var contact in _entitycsv.Contacts)
            {
                contactRow[HoldingRptDataSet.CONTACT_TITLE] = contact.PersonalTitle;
                contactRow[HoldingRptDataSet.CONTACT_NAME] = contact.Fullname;
                contactRow[HoldingRptDataSet.CONTACT_BIZNO] = contact.WorkPhoneNumber;
                contactRow[HoldingRptDataSet.CONTACT_MOBNO] = contact.MobilePhoneNumber;
                contactRow[HoldingRptDataSet.CONTACT_HNO] = contact.HomePhoneNumber;
                contactRow[HoldingRptDataSet.CONTACT_FAXNO] = contact.Facsimile;
                contactRow[HoldingRptDataSet.CONTACT_EMAIL] = contact.EmailAddress;
            }

            clientSummaryTable.Rows.Add(clientSummaryRow);
            contactTable.Rows.Add(contactRow);

            IOrganization orgCM = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            HoldingRptDataSet holdingReportDataSet = data as HoldingRptDataSet;

            DataTable holdingSummaryTable = holdingReportDataSet.Tables[HoldingRptDataSet.HOLDINGSUMMARYTABLE];
            DataTable productBreakDownTable = holdingReportDataSet.Tables[HoldingRptDataSet.PRODUCTBREAKDOWNTABLE];
            DataTable TDBreakDownTable = holdingReportDataSet.Tables[HoldingRptDataSet.TDBREAKDOWNTABLE];

            ManualTransactions(clientID, Entity.ClientManualAssetCollection, orgCM, holdingReportDataSet);

            GetTransactionsInfoFromLinkProcess(clientID, Broker, Entity.ListAccountProcess, orgCM, holdingReportDataSet, TDBreakDownTable);
        }

        private static void BasicClientDetails(string clientType, string clientID, string clientName, IdentityCM Parent, ICMBroker Broker, string ABN, string ACN, Common.DualAddressEntity Address, DataRow clientSummaryRow)
        {
            clientSummaryRow[HoldingRptDataSet.CLIENTID] = clientID;
            clientSummaryRow[HoldingRptDataSet.CLIENTNAME] = clientName;
            clientSummaryRow[HoldingRptDataSet.CIENTABN] = ABN;
            clientSummaryRow[HoldingRptDataSet.CLIENTACN] = ACN;
            clientSummaryRow[HoldingRptDataSet.CLIENTTYPE] = clientType;

            if (Parent != null && Parent.Clid != Guid.Empty && Parent.Csid != Guid.Empty)
            {
                AdvisorCM adviserUnit = Broker.GetCMImplementation(Parent.Clid, Parent.Csid) as AdvisorCM;
                if(adviserUnit!= null)
                    clientSummaryRow[HoldingRptDataSet.ADVISERFULLNAME] = adviserUnit.AdvisorEntity.FullName;
                Broker.ReleaseBrokerManagedComponent(adviserUnit);
            }

            clientSummaryRow[HoldingRptDataSet.ADDRESSLINE1] = Address.BusinessAddress.Addressline1;
            clientSummaryRow[HoldingRptDataSet.ADDRESSLINE2] = Address.BusinessAddress.Addressline2;
            clientSummaryRow[HoldingRptDataSet.SUBURB] = Address.BusinessAddress.Suburb;
            clientSummaryRow[HoldingRptDataSet.STATE] = Address.BusinessAddress.State;
            clientSummaryRow[HoldingRptDataSet.POSTCODE] = Address.BusinessAddress.PostCode;
            clientSummaryRow[HoldingRptDataSet.COUNTRY] = Address.BusinessAddress.Country;
        }

        private static void GetTransactionsInfoFromLinkProcess(string clientID, ICMBroker Broker, List<AccountProcessTaskEntity> ListAccountProcess, IOrganization orgCM, HoldingRptDataSet holdingReportDataSet, DataTable TDBreakDownTable)
        {
            if (ListAccountProcess != null)
            {
                foreach (var accountProcessTaskEntity in ListAccountProcess)
                {
                    if (accountProcessTaskEntity.LinkedEntity != null && accountProcessTaskEntity.LinkedEntity.Clid != Guid.Empty)
                    {
                        var asset = orgCM.Assets.Where(var => var.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                        var model = orgCM.Model.Where(mod => mod.ID == accountProcessTaskEntity.ModelID).FirstOrDefault();
                        var linkedAsset = model.Assets.Where(ast => ast.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                        if (linkedAsset != null)
                        {
                            var product = linkedAsset.Products.Where(pro => pro.ID == accountProcessTaskEntity.TaskID).FirstOrDefault();

                            if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.BankAccount)
                                BankAccountTransctions(Broker, holdingReportDataSet, accountProcessTaskEntity, asset, model, product);
                            else if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.TermDepositAccount)
                                TDTransactions(Broker, orgCM, holdingReportDataSet, TDBreakDownTable, accountProcessTaskEntity, asset, model, product);
                            else if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.DesktopBrokerAccount)
                                DesktopBrokerTransactions(Broker, orgCM, holdingReportDataSet, accountProcessTaskEntity, asset, model, product);
                            else if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.ManagedInvestmentSchemesAccount)
                                ManageInvestmentSchemes(clientID, Broker, orgCM, holdingReportDataSet, accountProcessTaskEntity, asset, model, product);
                        }
                    }
                }
            }
        }

        private static void ManageInvestmentSchemes(string clientID, ICMBroker Broker, IOrganization orgCM, HoldingRptDataSet holdingReportDataSet, AccountProcessTaskEntity accountProcessTaskEntity, AssetEntity asset, ModelEntity model, ProductEntity product)
        {
            ManagedInvestmentSchemesAccountCM managedInvestmentSchemesAccountCM = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as ManagedInvestmentSchemesAccountCM;
            var linkedMISAccount = managedInvestmentSchemesAccountCM.ManagedInvestmentSchemesAccountEntity.FundAccounts.Where(fa => product.TaskDescription.Contains(fa.Code)).FirstOrDefault();
            var filteredHoldingTransactionsByValuationDate = linkedMISAccount.FundTransactions.Where(cliFundTrans => cliFundTrans.TradeDate <= holdingReportDataSet.ValuationDate);
            var clientFundTransactions = filteredHoldingTransactionsByValuationDate.Where(cliFundTrans => cliFundTrans.ClientID == clientID);
            decimal totalShares = clientFundTransactions.Sum(ft => ft.Shares);
            var linkedmissec = orgCM.Securities.Where(missec => missec.AsxCode == linkedMISAccount.Code).FirstOrDefault();

            decimal currentValue = 0;

            if (linkedmissec != null)
            {
                var latestMisPriceObject = linkedmissec.ASXSecurity.Where(mishis => mishis.Date <= holdingReportDataSet.ValuationDate).OrderByDescending(mishis => mishis.Date).FirstOrDefault();
                decimal unitPrice = 0;
                if (latestMisPriceObject != null)
                    unitPrice = Convert.ToDecimal(latestMisPriceObject.UnitPrice);
                currentValue = totalShares * unitPrice;
                holdingReportDataSet.AddProductDetailRow(product, unitPrice, linkedmissec.AsxCode, linkedmissec.CompanyName, totalShares, currentValue, model.ServiceType.ToString());
            }

            holdingReportDataSet.AddHoldingRow(asset.Name, model.ServiceType.ToString(), product.Name, product.TaskDescription, clientID, currentValue, product.ID.ToString(), string.Empty);
        }

        private static void DesktopBrokerTransactions(ICMBroker Broker, IOrganization orgCM, HoldingRptDataSet holdingReportDataSet, AccountProcessTaskEntity accountProcessTaskEntity, AssetEntity asset, ModelEntity model, ProductEntity product)
        {
            DesktopBrokerAccountCM desktopBrokerAccountCM = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid,
                                                                        accountProcessTaskEntity.LinkedEntity.Csid) as DesktopBrokerAccountCM;
            decimal totalHolding = 0;

            //If it is linked with product securities for split type calculation
            if (product.ProductSecuritiesId != Guid.Empty)
            {
                var productSecurity = orgCM.ProductSecurities.Where(proseclist => proseclist.ID == product.ProductSecuritiesId).FirstOrDefault();
                var linkedAssets = model.Assets.Where(ast => ast.ID == accountProcessTaskEntity.AssetID && accountProcessTaskEntity.ModelID == model.ID);
                double totalPercentage = HoldingRptDataSet.CalculateTotalPercentageForSplit(linkedAssets);

                foreach (var productSecurityDetail in productSecurity.Details)
                {
                    var asxBroker = orgCM.Securities.Where(sec => sec.ID == productSecurityDetail.SecurityCodeId).FirstOrDefault();
                    var filteredHoldingTransactionsByValuationDate = desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Where(holdTrans =>
                                                                                                  holdTrans.TradeDate <= holdingReportDataSet.ValuationDate);
                    var filteredHoldingTransactions = filteredHoldingTransactionsByValuationDate.Where(holdTrans =>
                                                                                                  holdTrans.InvestmentCode == asxBroker.AsxCode);
                    var count = filteredHoldingTransactions.Count();

                    if (count > 0)
                    {
                        totalPercentage = product.SharePercentage;

                        if (product.SharePercentage > 0)
                        {

                            foreach (var linkedasst in linkedAssets)
                            {
                                var otherProducts = linkedasst.Products.Where(linkedAsstPro => linkedAsstPro.ProductSecuritiesId != productSecurity.ID);
                                foreach (var OtherproductSecurity in otherProducts)
                                {
                                    var OtherproductSecurityDetails = orgCM.ProductSecurities.Where(otherpro => otherpro.ID == OtherproductSecurity.ProductSecuritiesId).FirstOrDefault();

                                    if (OtherproductSecurityDetails != null)
                                    {
                                        var otherAsxBroker = OtherproductSecurityDetails.Details.Where(otherProDetail => otherProDetail.SecurityCodeId == asxBroker.ID);

                                        if (otherAsxBroker.Count() > 0)
                                            totalPercentage += OtherproductSecurity.SharePercentage;
                                    }
                                }
                            }

                            var hisSecurity = asxBroker.ASXSecurity.Where(secpri => secpri.Date <= holdingReportDataSet.ValuationDate).OrderByDescending(secpri => secpri.Date).FirstOrDefault();
                            decimal holding = filteredHoldingTransactions.Sum(filteredHolding => filteredHolding.Units);
                            decimal unitPrice = 0;
                            if (hisSecurity != null)
                                unitPrice = Convert.ToDecimal(hisSecurity.UnitPrice);

                            decimal currentValue = holding * unitPrice;
                            currentValue = currentValue * Convert.ToDecimal((product.SharePercentage / totalPercentage));
                            holdingReportDataSet.AddProductDetailRow(product, unitPrice, asxBroker.AsxCode,asxBroker.CompanyName, holding, currentValue, model.ServiceType.ToString());
                            totalHolding += currentValue;
                        }
                    }
                }
            }
            else
            {
                var filteredHoldingTransactionsByValuationDate = desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Where(holdTrans =>
                                                                                                  holdTrans.TradeDate <= holdingReportDataSet.ValuationDate);

                var groupedSecurities = filteredHoldingTransactionsByValuationDate.GroupBy(holdTran => holdTran.InvestmentCode);

                foreach (var security in groupedSecurities)
                {
                    decimal totalSecHoldingUnits = security.Sum(sec => sec.Units);
                    var asxBroker = orgCM.Securities.Where(sec => sec.AsxCode == security.Key).FirstOrDefault();
                    var hisSecurity = asxBroker.ASXSecurity.Where(secpri => secpri.Date <= holdingReportDataSet.ValuationDate).OrderByDescending(secpri => secpri.Date).FirstOrDefault();
                    decimal unitPrice = 0;

                    if (hisSecurity != null)
                        unitPrice = Convert.ToDecimal(hisSecurity.UnitPrice);

                    decimal currentValue = totalSecHoldingUnits * unitPrice;

                    holdingReportDataSet.AddProductDetailRow(product, unitPrice, asxBroker.AsxCode,asxBroker.CompanyName, totalSecHoldingUnits, currentValue, model.ServiceType.ToString());
                    totalHolding += currentValue;
                }
            }

            holdingReportDataSet.AddHoldingRow(asset.Name, model.ServiceType.ToString(), product.Name, product.TaskDescription, desktopBrokerAccountCM.DesktopBrokerAccountEntity.AccountNumber, totalHolding, product.ID.ToString(), string.Empty);
        }

        private static void TDTransactions(ICMBroker Broker, IOrganization orgCM, HoldingRptDataSet holdingReportDataSet, DataTable TDBreakDownTable, AccountProcessTaskEntity accountProcessTaskEntity, AssetEntity asset, ModelEntity model, ProductEntity product)
        {
            BankAccountCM bankAccount = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as BankAccountCM;
            decimal holdingTotal = 0;

            var filteredTransactions = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate <= holdingReportDataSet.ValuationDate);
            var tdGroupsType = filteredTransactions.GroupBy(cashMan => cashMan.TransactionType);

            foreach (var tdType in tdGroupsType)
            {
                var groupByIns = tdType.GroupBy(tdtypetran => tdtypetran.InstitutionID);

                foreach (var groupins in groupByIns)
                {
                    var transaction = groupins.FirstOrDefault();
                    decimal holding = groupins.Sum(inssum => inssum.Amount);

                    DataRow tDBreakDownTableRow = TDBreakDownTable.NewRow();

                    var inst = orgCM.Institution.Where(ins => ins.ID == groupins.Key).FirstOrDefault();
                    
                    string instName = string.Empty;
                    if (inst != null)
                        instName = inst.Name;

                    holdingReportDataSet.TDBreakDownRow(instName, model.ServiceType.ToString(), tdType.Key, transaction.AdministrationSystem, holding, bankAccount.BankAccountEntity.InstitutionID.ToString(), tDBreakDownTableRow);
                    TDBreakDownTable.Rows.Add(tDBreakDownTableRow);

                    holdingTotal += holding;
                }
            }
            holdingReportDataSet.AddHoldingRow(asset.Name, model.ServiceType.ToString(), product.Name,
                                                product.TaskDescription, bankAccount.BankAccountEntity.AccountNumber, holdingTotal, string.Empty, bankAccount.BankAccountEntity.InstitutionID.ToString());
        }

        private static void BankAccountTransctions(ICMBroker Broker, HoldingRptDataSet holdingReportDataSet, AccountProcessTaskEntity accountProcessTaskEntity, AssetEntity asset, ModelEntity model, ProductEntity product)
        {
            BankAccountCM bankAccount = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as BankAccountCM;
            var filteredTransactions = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate <= holdingReportDataSet.ValuationDate);
            decimal holdingTotal = filteredTransactions.Sum(calc => calc.TotalAmount);

            holdingReportDataSet.AddHoldingRow(asset.Name, model.ServiceType.ToString(), product.Name,
                                                product.TaskDescription, bankAccount.BankAccountEntity.AccountNumber, holdingTotal, string.Empty, string.Empty);
        }

        private static void ManualTransactions(string clientID, ObservableCollection<ClientManualAssetEntity> ClientManualAssetCollection, IOrganization orgCM, HoldingRptDataSet holdingReportDataSet)
        {
            if (ClientManualAssetCollection != null)
            {
                var filteredManualAssets = ClientManualAssetCollection.Where(man => man.TransactionDate <= holdingReportDataSet.ValuationDate);

                foreach (var manualAsset in filteredManualAssets)
                {
                    decimal currentValue = 0;

                    var systemManualAsset = orgCM.ManualAsset.Where(manList => manList.ID == manualAsset.InvestmentCode).FirstOrDefault();
                    var systemManualAssetPriceHistory = systemManualAsset.ASXSecurity.Where(manualHIS => manualHIS.Date <= holdingReportDataSet.ValuationDate).OrderByDescending(manualHis => manualHis.Date).FirstOrDefault();
                    if (systemManualAssetPriceHistory != null)
                        currentValue = manualAsset.UnitsOnHand * Convert.ToDecimal(systemManualAssetPriceHistory.UnitPrice);
                    holdingReportDataSet.AddHoldingRow("ManualAsset", "ManualAsset", systemManualAsset.AsxCode, systemManualAsset.CompanyName, clientID, currentValue, string.Empty, string.Empty);
                }
            }
        }

        public void OnGetDataHoldingRptDataSetIndividual(HoldingRptDataSet data, string clientType, string clientID, string clientName, IdentityCM Parent, ICMBroker Broker, ClientIndividualEntity Entity)
        {
            DataTable clientSummaryTable = data.Tables[HoldingRptDataSet.CLIENTSUMMARYTABLE];
            DataTable contactTable = data.Tables[HoldingRptDataSet.CONTACTTABLE];

            DataRow clientSummaryRow = clientSummaryTable.NewRow();
            DataRow contactRow = contactTable.NewRow();

            BasicClientDetails(clientType, clientID, clientName, Parent, Broker, Entity.ABN, string.Empty, Entity.Address, clientSummaryRow);

            CSVInstance csvinstance = new CSVInstance(Broker);
            ClientIndividualEntityCSV _entitycsv = csvinstance.GetEntityConcreteInstance(Entity);
            foreach (var contact in _entitycsv.Applicants)
            {
                contactRow[HoldingRptDataSet.CONTACT_TITLE] = contact.PersonalTitle;
                contactRow[HoldingRptDataSet.CONTACT_NAME] = contact.Fullname;
                contactRow[HoldingRptDataSet.CONTACT_BIZNO] = contact.WorkPhoneNumber;
                contactRow[HoldingRptDataSet.CONTACT_MOBNO] = contact.MobilePhoneNumber;
                contactRow[HoldingRptDataSet.CONTACT_HNO] = contact.HomePhoneNumber;
                contactRow[HoldingRptDataSet.CONTACT_FAXNO] = contact.Facsimile;
                contactRow[HoldingRptDataSet.CONTACT_EMAIL] = contact.EmailAddress;
            }

            clientSummaryTable.Rows.Add(clientSummaryRow);
            contactTable.Rows.Add(contactRow);

            IOrganization orgCM = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            HoldingRptDataSet holdingReportDataSet = data as HoldingRptDataSet;

            DataTable holdingSummaryTable = holdingReportDataSet.Tables[HoldingRptDataSet.HOLDINGSUMMARYTABLE];
            DataTable productBreakDownTable = holdingReportDataSet.Tables[HoldingRptDataSet.PRODUCTBREAKDOWNTABLE];
            DataTable TDBreakDownTable = holdingReportDataSet.Tables[HoldingRptDataSet.TDBREAKDOWNTABLE];

            ManualTransactions(clientID, Entity.ClientManualAssetCollection, orgCM, holdingReportDataSet);

            GetTransactionsInfoFromLinkProcess(clientID, Broker, Entity.ListAccountProcess, orgCM, holdingReportDataSet, TDBreakDownTable);
        }
    }
}
