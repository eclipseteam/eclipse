#region Using
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Xml.Linq;
using EPI.Data;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using OTD = Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Enum;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Utilities;
using OTC = Oritax.TaxSimp.Common;
using System.Xml;
using System.IO;
using IdentityCMDetail = Oritax.TaxSimp.Common.IdentityCMDetail;
using System.Text;
using Oritax.TaxSimp.DataSets;

#endregion

namespace Oritax.TaxSimp.CM.Group
{
    [Serializable]
    public sealed class AdvisorCM : GroupCM, ICmHasChildren, ISerializable, IIdentityCM, ICmHasParent, ICanCreateDefaultUser, IHaveAssociation, ISupportMigration, ICsvData
    {
        #region PRIVATE
        private AdvisorEntity _AdvisorEntity;

        public AdvisorEntity AdvisorEntity
        {
            get { return _AdvisorEntity; }
            set { _AdvisorEntity = value; }
        }

        public override IClientEntity ClientEntity
        {
            get
            {
                return AdvisorEntity;
            }
        }

        private List<IdentityCM> _Children;
        #endregion

        #region PUBLIC
        public IdentityCM Parent { get; set; }
        public List<IdentityCM> Children { get { return _Children; } set { _Children = value; } }
        #endregion

        #region Constructor
        public AdvisorCM()
            : base()
        {
            Parent = IdentityCM.Null;
            _Children = new List<IdentityCM>();
            _AdvisorEntity = new AdvisorEntity();
        }

        public AdvisorCM(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            Parent = info.GetValue<IdentityCM>("Oritax.TaxSimp.CM.Group.AdvisorCM.Parent");
            _Children = info.GetValue<List<IdentityCM>>("Oritax.TaxSimp.CM.Group.AdvisorCM.Children");
            _AdvisorEntity = info.GetValue<AdvisorEntity>("Oritax.TaxSimp.CM.Group.AdvisorCM.AdvisorEntity");
        }
        #endregion

        #region Override Methods
        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValueX("Oritax.TaxSimp.CM.Group.AdvisorCM.Parent", Parent);
            info.AddValueX("Oritax.TaxSimp.CM.Group.AdvisorCM.Children", _Children);
            info.AddValueX("Oritax.TaxSimp.CM.Group.AdvisorCM.AdvisorEntity", _AdvisorEntity);
        }

        public override string GetDataStream(int type, string data)
        {
            //type = System.Math.Abs(type);
            if (type == -911)
                type = (int)CmCommand.EditOrganizationUnit;
            string xml = string.Empty;
            switch ((CmCommand)type)
            {
                case CmCommand.GetIncludedClientList:
                    xml = GetOrganizationUnitNameList(_Children).ToXmlString();
                    break;
                case CmCommand.FullName:
                    xml = this._AdvisorEntity.Name + " " + this._AdvisorEntity.Surname;
                    break;
                case CmCommand.Email:
                    xml = this._AdvisorEntity.Email ?? string.Empty;
                    break;
                case CmCommand.GetEntitySecurity:
                    xml = GetEntitySecurity();
                    break;
                case CmCommand.XMLData:
                    xml = ExtractXML(data);
                    break;
                // code changes from here
                case CmCommand.GetBTWRAPCode:
                    xml = _AdvisorEntity.BTWRAPAdviserCode;
                    break;
                case CmCommand.EditOrganizationUnit:

                    _AdvisorEntity.OrganizationStatus = this.OrganizationStatus;
                    _AdvisorEntity.ClientId = this.ClientId;
                    xml = _AdvisorEntity.ToXmlString();
                    break;
            }
            return xml;
        }

        protected override void OnGetData(DataSet data)
        {
            base.OnGetData(data);

            if (data is ClientsByIFADS)
            {
                IBrokerManagedComponent objUser = Broker.GetBMCInstance("Administrator", "DBUser_1_1");
                UserEntity UserEntity = new UserEntity();
                UserEntity.CID = objUser.CID;
                UserEntity.CurrentUserName = "Administrator";
                UserEntity.IsAdmin = true;
                UserEntity.IsWebUser = true;
                UserEntity.UserType = 0;

                var ds = new MembershipDS
                {
                    CommandType = DatasetCommandTypes.GetChildren,
                    Unit = new Oritax.TaxSimp.Data.OrganizationUnit
                    {
                        CurrentUser = UserEntity,
                    }
                };
                this.GetData(ds);

                foreach (DataRow row in ds.membershipTable.Rows)
                {
                    Guid clientCSID = new Guid(row[ds.membershipTable.CSID].ToString());
                    Guid clientCLID = new Guid(row[ds.membershipTable.CLID].ToString());

                    IBrokerManagedComponent ibmc = Broker.GetCMImplementation(clientCLID, clientCSID) as IBrokerManagedComponent;

                    if (ibmc != null)
                    {
                        ibmc.GetData(data);
                        Broker.ReleaseBrokerManagedComponent(ibmc);
                    }
                }
            }
            else if (data is ExportDS)// this should be last if statement because it runs commands.
            {
                ExportAdvisorDetails(data, _AdvisorEntity, Broker, this.ClientId, this.Parent);
            }
            else if (data is AdvisorDS)
            {
                var ds = data as AdvisorDS;

                switch (ds.CommandType)
                {
                    case DatasetCommandTypes.Get:
                        {
                            DataRow dr = ds.AdvisorTable.NewRow();
                            dr[ds.AdvisorTable.CID] = CID;
                            dr[ds.AdvisorTable.CLID] = Clid;
                            dr[ds.AdvisorTable.CSID] = Csid;
                            dr[ds.AdvisorTable.CLIENTID] = ClientId;
                            dr[ds.AdvisorTable.NAME] = _AdvisorEntity.FullName;
                            dr[ds.AdvisorTable.TYPE] = typeName;
                            dr[ds.AdvisorTable.STATUS] = OrganizationStatus;
                            dr[ds.AdvisorTable.OTHERID] = OtherID;
                            ds.AdvisorTable.Rows.Add(dr);
                        }
                        break;
                    case DatasetCommandTypes.GetChildren:
                        {
                            DataRow dr = ds.AdvisorTable.NewRow();
                            dr[ds.AdvisorTable.CID] = CID;
                            dr[ds.AdvisorTable.CLID] = Clid;
                            dr[ds.AdvisorTable.CSID] = Csid;
                            dr[ds.AdvisorTable.CLIENTID] = ClientId;
                            dr[ds.AdvisorTable.NAME] = _AdvisorEntity.FullName;
                            dr[ds.AdvisorTable.TYPE] = typeName;
                            dr[ds.AdvisorTable.STATUS] = OrganizationStatus;
                            dr[ds.AdvisorTable.OTHERID] = OtherID;
                            dr[ds.AdvisorTable.FIRSTNAME] = _AdvisorEntity.Name;
                            dr[ds.AdvisorTable.SURNAME] = _AdvisorEntity.Surname;
                            dr[ds.AdvisorTable.AUTHORISEDREPNO] = _AdvisorEntity.AuthorisedRepNo;
                            dr[ds.AdvisorTable.HOMEPHONECOUNTRYCODE] = _AdvisorEntity.HomePhoneNumber.CountryCode;
                            dr[ds.AdvisorTable.HOMEPHONECITYCODE] = _AdvisorEntity.HomePhoneNumber.CityCode;
                            dr[ds.AdvisorTable.HOMEPHONENUMBER] = _AdvisorEntity.HomePhoneNumber.PhoneNumber;
                            dr[ds.AdvisorTable.WORKPHONECOUNTRYCODE] = _AdvisorEntity.WorkPhoneNumber.CountryCode;
                            dr[ds.AdvisorTable.WORKPHONECITYCODE] = _AdvisorEntity.WorkPhoneNumber.CityCode;
                            dr[ds.AdvisorTable.WORKPHONENUMBER] = _AdvisorEntity.WorkPhoneNumber.PhoneNumber;
                            dr[ds.AdvisorTable.MOBILEPHONECOUNTRYCODE] = _AdvisorEntity.MobilePhoneNumber.CountryCode;
                            dr[ds.AdvisorTable.MOBILEPHONENUMBER] = _AdvisorEntity.MobilePhoneNumber.MobileNumber;
                            dr[ds.AdvisorTable.FACSIMILECOUNTRYCODE] = _AdvisorEntity.Facsimile.CountryCode;
                            dr[ds.AdvisorTable.FACSIMILECITYCODE] = _AdvisorEntity.Facsimile.CityCode;
                            dr[ds.AdvisorTable.FACSIMILENUMBER] = _AdvisorEntity.Facsimile.PhoneNumber;
                            dr[ds.AdvisorTable.TFN] = _AdvisorEntity.TFN;
                            dr[ds.AdvisorTable.EMAIL] = _AdvisorEntity.Email;
                            if (_AdvisorEntity.AppointedDate.HasValue)
                                dr[ds.AdvisorTable.APPOINTEDDATE] = _AdvisorEntity.AppointedDate;
                            if (_AdvisorEntity.TerminatedDate.HasValue)
                                dr[ds.AdvisorTable.TERMINATEDDATE] = _AdvisorEntity.TerminatedDate;
                            dr[ds.AdvisorTable.REASONTERMINATED] = _AdvisorEntity.ReasonTerminated;
                            dr[ds.AdvisorTable.GENDER] = _AdvisorEntity.Gender;
                            dr[ds.AdvisorTable.PREFERREDNAME] = _AdvisorEntity.PreferredName;
                            dr[ds.AdvisorTable.TITLE] = _AdvisorEntity.Title;
                            dr[ds.AdvisorTable.OCCUPATION] = _AdvisorEntity.Occupation;
                            dr[ds.AdvisorTable.BTWRAPADVISERCODE] = _AdvisorEntity.BTWRAPAdviserCode;
                            dr[ds.AdvisorTable.DESKTOPBROKERADVISERCODE] = _AdvisorEntity.DesktopBrokerAdviserCode;
                            dr[ds.AdvisorTable.AFSLICENSEENAME] = _AdvisorEntity.AFSLicenseeName;
                            dr[ds.AdvisorTable.AFSLICENCENUMBER] = _AdvisorEntity.AFSLicenceNumber;
                            dr[ds.AdvisorTable.BANKWESTADVISORCODE] = _AdvisorEntity.BankWestAdvisorCode;

                            ds.AdvisorTable.Rows.Add(dr);
                        }
                        break;
                }
            }
            else if (data is MembershipDS)
            {
                var ds = data as MembershipDS;
                switch (ds.CommandType)
                {
                    case DatasetCommandTypes.GetChildren:
                        GetChildren(Children, ds, Broker);
                        GetParentTable(Parent, ds, Broker);
                        break;
                }

            }
            else if (data is AddressDetailsDS)
            {
                var ds = data as AddressDetailsDS;
                GetAdvisorAddressDetails(ds, _AdvisorEntity);
            }
            else if (data is BankAccountDS)
            {
                if (_AdvisorEntity.BankAccounts == null)
                    _AdvisorEntity.BankAccounts = new List<Oritax.TaxSimp.Common.IdentityCMDetail>();
                GetBankAccountDs(ClientId, data as BankAccountDS, _AdvisorEntity.BankAccounts, Broker);
            }
            else if (data is IndividualDS)
            {
                var individualDs = data as IndividualDS;
                if (_AdvisorEntity.Individual == null)
                    _AdvisorEntity.Individual = new IdentityCMDetail();

                switch (individualDs.IndividualsType)
                {
                    case IndividualsType.Individual:
                        GetIndividualDs(individualDs, _AdvisorEntity.Individual, Broker);
                        break;
                }
            }
            else if (data is MDAAlertDS)
            {
                var ds = data as MDAAlertDS;
                var adviserentity = this.ClientEntity as IAdviserEntity;
                var rows = ds.ClientDetailTable.Select(string.Format("{0}='{1}'", ClientDetailTable.ADVISERID, this.CID));
                foreach (var row in rows)
                {
                    row[ClientDetailTable.ADVISEREMAIL] = adviserentity.Email;
                    row[ClientDetailTable.ADVISERCODE] = this.ClientId;
                    row[ClientDetailTable.ADVISERNAME] = string.Format("{0} {1}", adviserentity.PreferredName, adviserentity.Surname);
                }

                if (rows.Length > 0 && this.Parent != null)
                {
                    var ifa = Broker.GetBMCInstance(this.Parent.Cid);
                    if (ifa == null)
                    {
                        ifa = Broker.GetCMImplementation(this.Parent.Clid, Parent.Csid);
                    }

                    if (ifa != null)
                    {
                        foreach (var row in rows)
                        { 
                            row[ClientDetailTable.IFACID] = ifa.CID; 
                        }
                        ifa.GetData(ds);
                        Broker.ReleaseBrokerManagedComponent(ifa);
                    }
                }
            }
        }

        private void GetIndividualDs(IndividualDS individualDs, IdentityCMDetail individual, ICMBroker broker)
        {
            if (individual != null)
            {
                var indvCM = broker.GetCMImplementation(individual.Clid, individual.Csid);
                if (indvCM != null)
                {
                    indvCM.GetData(individualDs);
                    if (individual.BusinessTitle != null)
                        individualDs.IndividualTable.Rows[individualDs.IndividualTable.Rows.Count - 1][individualDs.IndividualTable.BUSINESSTITLE] = individual.BusinessTitle.Title;
                    broker.ReleaseBrokerManagedComponent(indvCM);
                }
            }
        }

        public static void GetBankAccountDs(string clientId, BankAccountDS dataSet, List<Oritax.TaxSimp.Common.IdentityCMDetail> bankAccounts, ICMBroker broker)
        {
            if (bankAccounts != null && bankAccounts.Count > 0)
            {
                foreach (var bank in bankAccounts)
                {
                    var entity = broker.GetCMImplementation(bank.Clid, bank.Csid);
                    if (entity != null)
                    {

                        entity.GetData(dataSet);
                        broker.ReleaseBrokerManagedComponent(entity);
                    }
                }
            }
        }


        private void GetAdvisorAddressDetails(AddressDetailsDS ds, Group.AdvisorEntity _AdvisorEntity)
        {

            if (_AdvisorEntity.Address == null)
            {

                _AdvisorEntity.Address = new Oritax.TaxSimp.Common.DualAddressEntity();

            }
            FillAdvisorAddresses(ds, _AdvisorEntity.Address);
        }
        private static void FillAdvisorAddresses(AddressDetailsDS ds, Oritax.TaxSimp.Common.DualAddressEntity address)
        {
            #region Get Business Address


            if (address.BusinessAddress != null)
            {
                FillAddress(ds, address.BusinessAddress, Oritax.TaxSimp.DataSets.AddressType.BusinessAddress);
            }
            if (address.MailingAddress != null)
            {
                FillAddress(ds, address.MailingAddress, Oritax.TaxSimp.DataSets.AddressType.MailingAddress);
            }
            if (address.RegisteredAddress != null)
            {
                FillAddress(ds, address.RegisteredAddress, Oritax.TaxSimp.DataSets.AddressType.RegisteredAddress);
            }
            if (address.ResidentialAddress != null)
            {
                FillAddress(ds, address.ResidentialAddress, Oritax.TaxSimp.DataSets.AddressType.ResidentialAddress);
            }
            if (address.DuplicateMailingAddress != null)
            {
                FillAddress(ds, address.DuplicateMailingAddress, Oritax.TaxSimp.DataSets.AddressType.DuplicateMailingAddress);
            }

            #endregion


        }
        private static void FillAddress(AddressDetailsDS ds, Oritax.TaxSimp.Common.AddressEntity address, Oritax.TaxSimp.DataSets.AddressType addressType)
        {

            DataRow drBusinessAddress = ds.ClientDetailsTable.NewRow();
            drBusinessAddress[ds.ClientDetailsTable.ADDRESSLINE1] = address.Addressline1;
            drBusinessAddress[ds.ClientDetailsTable.ADDRESSLINE2] = address.Addressline2;
            drBusinessAddress[ds.ClientDetailsTable.SUBURB] = address.Suburb;
            drBusinessAddress[ds.ClientDetailsTable.STATE] = address.State;
            drBusinessAddress[ds.ClientDetailsTable.COUNTRY] = address.Country;
            drBusinessAddress[ds.ClientDetailsTable.POSTCODE] = address.PostCode;
            drBusinessAddress[ds.ClientDetailsTable.ADDRESSTYPE] = addressType;
            ds.ClientDetailsTable.Rows.Add(drBusinessAddress);
        }
        #region Export Advisor Details
        public static void ExportAdvisorDetails(DataSet value, Oritax.TaxSimp.CM.Group.AdvisorEntity _AdvisorEntity, ICMBroker Broker, string clientID, IdentityCM Parent)
        {

            CSVInstance csvinstance = new CSVInstance(Broker);
            string ifaName = string.Empty;
            OrganizationUnitCM ifa = null;
            ICalculationModule parentCalcModule = null;

            if (Parent != null)
                parentCalcModule = Broker.GetCMImplementation(Parent.Clid, Parent.Csid);

            if (parentCalcModule != null)
                ifa = (OrganizationUnitCM)parentCalcModule;

            if (ifa != null)
                ifaName = ifa.Name;

            IOrganizationUnit dealerGroup = null;

            if (ifa != null && ((OrganizationUnitCM)ifa).ConfiguredParent != null)
                dealerGroup = (IOrganizationUnit)Broker.GetCMImplementation(((OrganizationUnitCM)ifa).ConfiguredParent.Clid, ((OrganizationUnitCM)ifa).ConfiguredParent.Csid);

            string dealerGroupName = String.Empty;

            if (dealerGroup != null)
                dealerGroupName = dealerGroup.Name;

            AdviserDetailsCSVColumns obj = new AdviserDetailsCSVColumns();
            obj.SetColumnValue(obj.Col_ID, clientID);
            obj.SetColumnValue(obj.Col_Name, _AdvisorEntity.Name);
            obj.SetColumnValue(obj.Col_Surname, _AdvisorEntity.Surname);
            obj.SetColumnValue(obj.Col_Addressline1, _AdvisorEntity.Address.BusinessAddress.Addressline1);
            obj.SetColumnValue(obj.Col_Addressline2, _AdvisorEntity.Address.BusinessAddress.Addressline2);
            obj.SetColumnValue(obj.Col_AFSLicenceNumber, _AdvisorEntity.AFSLicenceNumber);
            obj.SetColumnValue(obj.Col_AFSLicenseeName, _AdvisorEntity.AFSLicenseeName);
            obj.SetColumnValue(obj.Col_AuthorisedRepNo, _AdvisorEntity.AuthorisedRepNo);
            obj.SetColumnValue(obj.Col_BankWestAdvisorCode, _AdvisorEntity.BankWestAdvisorCode);
            obj.SetColumnValue(obj.Col_BTWRAPAdviserCode, _AdvisorEntity.BTWRAPAdviserCode);
            obj.SetColumnValue(obj.Col_Country, _AdvisorEntity.Address.BusinessAddress.Country);
            obj.SetColumnValue(obj.Col_DealerGroup, dealerGroupName);
            obj.SetColumnValue(obj.Col_DesktopBrokerAdviserCode, _AdvisorEntity.DesktopBrokerAdviserCode);
            obj.SetColumnValue(obj.Col_Email, _AdvisorEntity.Email);
            obj.SetColumnValue(obj.Col_Facsimile, _AdvisorEntity.Facsimile.PhoneNumber);
            obj.SetColumnValue(obj.Col_HomePhoneNumber, _AdvisorEntity.HomePhoneNumber.PhoneNumber);
            obj.SetColumnValue(obj.Col_IFA, ifaName);
            obj.SetColumnValue(obj.Col_MobilePhoneNumber, _AdvisorEntity.MobilePhoneNumber.MobileNumber);
            obj.SetColumnValue(obj.Col_Occupation, _AdvisorEntity.Occupation);
            obj.SetColumnValue(obj.Col_PostCode, _AdvisorEntity.Address.BusinessAddress.PostCode);
            obj.SetColumnValue(obj.Col_State, _AdvisorEntity.Address.BusinessAddress.State);
            obj.SetColumnValue(obj.Col_Suburb, _AdvisorEntity.Address.BusinessAddress.Suburb);
            obj.SetColumnValue(obj.Col_Title, _AdvisorEntity.Title);
            obj.SetColumnValue(obj.Col_WorkPhoneNumber, _AdvisorEntity.WorkPhoneNumber.PhoneNumber);

            Broker.ReleaseBrokerManagedComponent(ifa);
            Broker.ReleaseBrokerManagedComponent(dealerGroup);

            value.Tables[0].Merge(obj.Table);
            // value.Tables[0].ImportRow(obj.Table.Rows[0]);


        }
        #endregion
        public override string OnUpdateDataStream(int type, string data)
        {
            string xml = string.Empty;
            AdvisorEntity _advisorentity = null;
            IdentityCM child = null;
            List<OTC.OrganizationUnit> list = null;
            OTC.OrganizationUnit ou = null;
            switch ((CmCommand)type)
            {
                case CmCommand.AttachToParent:
                    if (Parent != IdentityCM.Null)
                    {
                        var component = Broker.GetCMImplementation(Parent.Clid, Parent.Csid) as OrganizationUnitCM;
                        if (component == null)
                            component = Broker.GetBMCInstance(Parent.Cid) as OrganizationUnitCM;
                        if (component != null)
                        {
                            string name = component.GetDataStream((int)CmCommand.FullName, "");
                            Broker.ReleaseBrokerManagedComponent(component);
                            throw new Exception("Adviser (" + _AdvisorEntity.FullName + ") is already part of IFA (" + name + ")");
                        }
                    }
                    IdentityCM parent = GetParent(data);
                    Parent = parent;
                    SetAddressToAdvisor(data);
                    break;
                case CmCommand.DeAttachFromParent:
                    Parent = IdentityCM.Null;
                    SetAddressToAdvisor(data);
                    break;
                case CmCommand.Add_Single:
                    child = GetChild(data);
                    BMCUpdateComponent(child, CmCommand.Add_Single);
                    _Children.Add(child);
                    break;
                case CmCommand.Remove_Single:
                    child = GetChild(data);
                    BMCUpdateComponent(child, CmCommand.Remove_Single);
                    _Children.RemoveAll(c => c == child);
                    break;
                case CmCommand.SPAddLibrary:
                    var _value = data.ToNewOrData<Oritax.TaxSimp.Common.SPLibraryDocument>();
                    if (_value != null)
                    {
                        SPLibrary.AddLibraryToClient(_value.CurrentUser.CurrentUserName, _value.Name);
                    }
                    break;
                case CmCommand.Add_List:
                    list = data.ToNewOrData<List<OTC.OrganizationUnit>>();
                    foreach (var each in list)
                    {
                        child = GetChild(each.ToXmlString());
                        BMCUpdateComponent(child, CmCommand.Add_Single);
                        _Children.Add(child);
                    }
                    break;
                case CmCommand.Remove_List:
                    list = data.ToNewOrData<List<OTC.OrganizationUnit>>();
                    foreach (var each in list)
                    {
                        child = GetChild(each.ToXmlString());
                        BMCUpdateComponent(child, CmCommand.Remove_Single);
                        _Children.RemoveAll(c => c.Clid == child.Clid && c.Csid == child.Csid);
                    }
                    break;
                case CmCommand.EditOrganizationUnit:
                    _advisorentity = data.ToNewOrData<AdvisorEntity>();
                    this.Name = _advisorentity.Name;
                    _AdvisorEntity = _advisorentity;
                    this.OrganizationStatus = _advisorentity.OrganizationStatus;
                    ou = new OTC.OrganizationUnit();
                    ou.Clid = this.Clid;
                    ou.Csid = this.Csid;
                    ou.Name = _advisorentity.Name;

                    xml = ou.ToXmlString();
                    break;
                case CmCommand.AddEntitySecurity:
                    xml = AddEntitySecurity(data);

                    SetChildAccountEntitySecuirty(type, data);


                    break;
                case CmCommand.DeleteEntitySecurity:
                    xml = DeleteEntitySecurity(data);
                    SetChildAccountEntitySecuirty(type, data);
                    break;
                case CmCommand.GetEntitySecurity:
                    xml = GetEntitySecurity();
                    break;
                case CmCommand.UpdateClientID:// for the purpose of updating client id's
                    break;
                default:
                    _advisorentity = data.ToNewOrData<AdvisorEntity>();
                    this.Name = _advisorentity.Name;
                    this.OrganizationStatus = _advisorentity.OrganizationStatus;
                    _AdvisorEntity = _advisorentity;
                    if (ClientId == "" || ClientId == null)
                    {
                        UpDateEventLog.UpdateLog("Advisor", _AdvisorEntity.Name, type, this.Broker);
                    }
                    else
                    {
                        UpDateEventLog.UpdateLog("Advisor", _AdvisorEntity.Name + " (" + ClientId + ")", type, this.Broker);
                    }

                    //UserName;
                    break;
            }
            return xml;
        }
        protected override ModifiedState OnSetData(DataSet data)
        {
            base.OnSetData(data);

            if (data is MembershipDS)
            {
                var ds = data as MembershipDS;
                IdentityCM child;
                switch (ds.CommandType)
                {
                    case DatasetCommandTypes.Add:
                        foreach (DataRow dr in ds.membershipTable.Rows)
                        {
                            child = new IdentityCM();
                            child.Cid = new Guid(dr[ds.membershipTable.CID].ToString());
                            child.Clid = new Guid(dr[ds.membershipTable.CLID].ToString());
                            child.Csid = new Guid(dr[ds.membershipTable.CSID].ToString());
                            var existingChild = Children.Where(ss => ss.Clid == child.Clid && ss.Csid == child.Csid).FirstOrDefault();
                            if (existingChild == null)
                            {
                                try
                                {
                                    BMCUpdateComponentParent(child, new IdentityCM { Clid = this.Clid, Csid = this.CSID, Cid = this.CID }, CmCommand.Add_Single, Broker);
                                }
                                catch (Exception ex)
                                {
                                    dr.RowError = ex.Message;
                                }
                            }
                        }
                        break;
                    case DatasetCommandTypes.Delete:
                        foreach (DataRow dr in ds.membershipTable.Rows)
                        {
                            child = new IdentityCM();
                            child.Cid = new Guid(dr[ds.membershipTable.CID].ToString());
                            child.Clid = new Guid(dr[ds.membershipTable.CLID].ToString());
                            child.Csid = new Guid(dr[ds.membershipTable.CSID].ToString());
                            if (Children.Count(ss => ss.Clid == child.Clid && ss.Csid == child.Csid) > 0)
                            {

                                BMCUpdateComponentParent(child, new IdentityCM { Clid = this.Clid, Csid = this.CSID, Cid = this.CID }, CmCommand.Remove_Single, Broker);
                                Children.Remove(child);


                            }
                        }
                        break;
                }
            }
            else if (data is AdvisorDS)
            {
                var ds = data as AdvisorDS;
                switch (ds.CommandType)
                {
                    case DatasetCommandTypes.Add:
                        AddAdviser(ds);
                        break;
                    case DatasetCommandTypes.Update:
                        UpdateAdviser(ds);
                        break;
                }
            }
            else if (data is AddressDetailsDS)
            {
                var ds = data as AddressDetailsDS;
                SetAdvisorAddresses(ds, _AdvisorEntity.Address);
            }
            else if (data is BankAccountDS)
            {
                if (_AdvisorEntity.BankAccounts == null)
                    _AdvisorEntity.BankAccounts = new List<Oritax.TaxSimp.Common.IdentityCMDetail>();
                var ds = data as BankAccountDS;
                if (ds.CommandType == DatasetCommandTypes.Delete)
                    RemoveBankAccount(ds, AdvisorEntity.BankAccounts, Broker);
                else if (ds.CommandType == DatasetCommandTypes.Update)
                    AssociateBankAccount(ds, AdvisorEntity.BankAccounts);
            }
            else if (data is IndividualDS)
            {
                var ds = data as IndividualDS;

                switch (ds.IndividualsType)
                {
                    case IndividualsType.Individual:
                        if (_AdvisorEntity.Individual == null)
                            _AdvisorEntity.Individual = new IdentityCMDetail();

                        if (ds.CommandType == DatasetCommandTypes.Delete)
                        {
                            RemoveIndividualsAdviser(ds, _AdvisorEntity.Individual, Broker);
                            _AdvisorEntity.Individual = new IdentityCMDetail();
                        }
                        else if (ds.CommandType == DatasetCommandTypes.Update)
                            AssociateIndividual(ds, _AdvisorEntity.Individual);

                        break;
                }

            }

            return ModifiedState.MODIFIED;
        }

        /// <summary>
        /// Remove Individual from an Adviser
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="individual"></param>
        /// <param name="broker"></param>
        private void RemoveIndividualsAdviser(IndividualDS ds, IdentityCMDetail individual, ICMBroker broker)
        {
            if (individual != null && ds.Tables[ds.IndividualTable.TABLENAME].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[ds.IndividualTable.TABLENAME].Rows)
                {
                    var cid = new Guid(dr[ds.IndividualTable.CID].ToString());

                    var comp = Broker.GetBMCInstance(cid);
                    ds.CommandType = DatasetCommandTypes.RemoveAdviser;
                    comp.SetData(ds);
                    Broker.ReleaseBrokerManagedComponent(comp);
                }
            }
        }

        /// <summary>
        /// Associate Individual to an Adviser
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="individual"></param>
        private void AssociateIndividual(IndividualDS ds, IdentityCMDetail individual)
        {
            if (individual != null && ds.Tables[ds.IndividualTable.TABLENAME].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[ds.IndividualTable.TABLENAME].Rows)
                {
                    var clid = new Guid(dr[ds.IndividualTable.CLID].ToString());
                    var cid = new Guid(dr[ds.IndividualTable.CID].ToString());
                    var csid = new Guid(dr[ds.IndividualTable.CSID].ToString());
                    var businessttitle = dr[ds.IndividualTable.BUSINESSTITLE].ToString();

                    individual.Clid = clid;
                    individual.Csid = csid;
                    individual.Cid = cid;
                    individual.BusinessTitle = new TitleEntity { Title = businessttitle };

                    //setting Adviser Ids to update in individual
                    dr[ds.IndividualTable.ADVISERCID] = CID;
                    dr[ds.IndividualTable.ADVISERCLID] = CLID;
                    dr[ds.IndividualTable.ADVISERCSID] = CSID;

                    var comp = Broker.GetBMCInstance(cid);
                    ds.CommandType = DatasetCommandTypes.UpdateAdviser;
                    comp.SetData(ds);
                    Broker.ReleaseBrokerManagedComponent(comp);
                }
            }
        }

        public static void AssociateBankAccount(BankAccountDS ds, List<Oritax.TaxSimp.Common.IdentityCMDetail> bankAccounts)
        {
            if (ds.Tables[ds.BankAccountsTable.TABLENAME].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[ds.BankAccountsTable.TABLENAME].Rows)
                {
                    var clid = new Guid(dr[ds.BankAccountsTable.CLID].ToString());
                    var csid = new Guid(dr[ds.BankAccountsTable.CSID].ToString());
                    var asxAccount = bankAccounts.FirstOrDefault(ss => ss.Clid == new Guid(dr[ds.BankAccountsTable.CLID].ToString()) && ss.Csid == new Guid(dr[ds.BankAccountsTable.CSID].ToString()));
                    if (asxAccount == null)
                    {
                        if (bankAccounts == null)
                        {
                            bankAccounts = new List<Oritax.TaxSimp.Common.IdentityCMDetail>();
                        }
                        bankAccounts.Add(new Oritax.TaxSimp.Common.IdentityCMDetail { Clid = clid, Csid = csid });

                    }
                }
            }
        }

        public static void RemoveBankAccount(BankAccountDS ds, List<Oritax.TaxSimp.Common.IdentityCMDetail> bankAccounts, ICMBroker broker)
        {
            if (ds.Tables[ds.BankAccountsTable.TABLENAME].Rows.Count > 0)
            {
                var clid = (Guid)ds.Tables[ds.BankAccountsTable.TABLENAME].Rows[0][ds.BankAccountsTable.CLID];
                var csid = (Guid)ds.Tables[ds.BankAccountsTable.TABLENAME].Rows[0][ds.BankAccountsTable.CSID];

                //Remove Bank Account
                var bank = bankAccounts.FirstOrDefault(ss => ss.Clid == clid && ss.Csid == csid);
                if (bank != null)
                {

                    bankAccounts.Remove(bank);
                    ds.ExtendedProperties.Add("Message", "Account has been deleted successfully");


                }

            }
        }
        private static void SetAdvisorAddresses(AddressDetailsDS ds, Oritax.TaxSimp.Common.DualAddressEntity Address)
        {
            foreach (DataRow dr in ds.ClientDetailsTable.Rows)
            {
                if ((Oritax.TaxSimp.DataSets.AddressType)System.Enum.Parse(typeof(Oritax.TaxSimp.DataSets.AddressType), dr[ds.ClientDetailsTable.ADDRESSTYPE].ToString()) == Oritax.TaxSimp.DataSets.AddressType.BusinessAddress)
                {
                    SetAddress(ds.ClientDetailsTable, Address.BusinessAddress, dr);
                }
                else if (
                    (Oritax.TaxSimp.DataSets.AddressType)System.Enum.Parse(typeof(Oritax.TaxSimp.DataSets.AddressType), dr[ds.ClientDetailsTable.ADDRESSTYPE].ToString()) == Oritax.TaxSimp.DataSets.AddressType.RegisteredAddress)
                {
                    SetAddress(ds.ClientDetailsTable, Address.RegisteredAddress, dr);
                }
                else if (
                    (Oritax.TaxSimp.DataSets.AddressType)
                    System.Enum.Parse(typeof(Oritax.TaxSimp.DataSets.AddressType), dr[ds.ClientDetailsTable.ADDRESSTYPE].ToString()) == Oritax.TaxSimp.DataSets.AddressType.ResidentialAddress)
                {
                    SetAddress(ds.ClientDetailsTable, Address.ResidentialAddress, dr);
                }
                else if (
                    (Oritax.TaxSimp.DataSets.AddressType)
                    System.Enum.Parse(typeof(Oritax.TaxSimp.DataSets.AddressType), dr[ds.ClientDetailsTable.ADDRESSTYPE].ToString()) == Oritax.TaxSimp.DataSets.AddressType.MailingAddress)
                {
                    SetAddress(ds.ClientDetailsTable, Address.MailingAddress, dr);
                }
                else if (
                    (Oritax.TaxSimp.DataSets.AddressType)
                    System.Enum.Parse(typeof(Oritax.TaxSimp.DataSets.AddressType), dr[ds.ClientDetailsTable.ADDRESSTYPE].ToString()) == Oritax.TaxSimp.DataSets.AddressType.DuplicateMailingAddress)
                {
                    SetAddress(ds.ClientDetailsTable, Address.DuplicateMailingAddress, dr);
                }
            }
        }



        private static void SetAddress(AddressDetails ClientDetailsTable, Oritax.TaxSimp.Common.AddressEntity Address, DataRow dr)
        {
            Address.Addressline1 = dr[ClientDetailsTable.ADDRESSLINE1].ToString();
            Address.Addressline2 = dr[ClientDetailsTable.ADDRESSLINE2].ToString();
            Address.Suburb = dr[ClientDetailsTable.SUBURB].ToString();
            Address.State = dr[ClientDetailsTable.STATE].ToString();
            Address.Country = dr[ClientDetailsTable.COUNTRY].ToString();
            Address.PostCode = dr[ClientDetailsTable.POSTCODE].ToString();
        }

        private void UpdateAdviser(AdvisorDS ds)
        {
            foreach (DataRow dr in ds.AdvisorTable.Rows)
            {
                OtherID = dr[ds.AdvisorTable.OTHERID].ToString();
                _AdvisorEntity.AFSLicenceNumber = dr[ds.AdvisorTable.AFSLICENCENUMBER].ToString();
                _AdvisorEntity.AFSLicenseeName = dr[ds.AdvisorTable.AFSLICENSEENAME].ToString();
                _AdvisorEntity.AppointedDate = dr[ds.AdvisorTable.APPOINTEDDATE] as DateTime?;
                _AdvisorEntity.AuthorisedRepNo = dr[ds.AdvisorTable.AUTHORISEDREPNO].ToString();
                _AdvisorEntity.BTWRAPAdviserCode = dr[ds.AdvisorTable.BTWRAPADVISERCODE].ToString();
                _AdvisorEntity.BankWestAdvisorCode = dr[ds.AdvisorTable.BANKWESTADVISORCODE].ToString();
                _AdvisorEntity.DesktopBrokerAdviserCode = dr[ds.AdvisorTable.DESKTOPBROKERADVISERCODE].ToString();
                _AdvisorEntity.Email = dr[ds.AdvisorTable.EMAIL].ToString();
                _AdvisorEntity.Facsimile = new PhoneNumberEntity
                                               {
                                                   CountryCode = dr[ds.AdvisorTable.FACSIMILECOUNTRYCODE].ToString(),
                                                   CityCode = dr[ds.AdvisorTable.FACSIMILECITYCODE].ToString(),
                                                   PhoneNumber = dr[ds.AdvisorTable.FACSIMILENUMBER].ToString()
                                               };
                _AdvisorEntity.Gender = dr[ds.AdvisorTable.GENDER].ToString();
                _AdvisorEntity.HomePhoneNumber = new PhoneNumberEntity
                                                     {
                                                         CountryCode = dr[ds.AdvisorTable.HOMEPHONECOUNTRYCODE].ToString(),
                                                         CityCode = dr[ds.AdvisorTable.HOMEPHONECITYCODE].ToString(),
                                                         PhoneNumber = dr[ds.AdvisorTable.HOMEPHONENUMBER].ToString()
                                                     };
                _AdvisorEntity.MobilePhoneNumber = new MobileNumberEntity
                                                       {
                                                           CountryCode = dr[ds.AdvisorTable.MOBILEPHONECOUNTRYCODE].ToString(),
                                                           MobileNumber = dr[ds.AdvisorTable.MOBILEPHONENUMBER].ToString()
                                                       };
                _AdvisorEntity.Name = dr[ds.AdvisorTable.FIRSTNAME].ToString();
                _AdvisorEntity.Occupation = dr[ds.AdvisorTable.OCCUPATION].ToString();
                _AdvisorEntity.OrganizationStatus = dr[ds.AdvisorTable.STATUS].ToString();
                _AdvisorEntity.PreferredName = dr[ds.AdvisorTable.PREFERREDNAME].ToString();
                _AdvisorEntity.ReasonTerminated = dr[ds.AdvisorTable.AFSLICENCENUMBER].ToString();
                _AdvisorEntity.Surname = dr[ds.AdvisorTable.SURNAME].ToString();
                _AdvisorEntity.TFN = dr[ds.AdvisorTable.TFN].ToString();
                _AdvisorEntity.TerminatedDate = dr[ds.AdvisorTable.TERMINATEDDATE] as DateTime?;
                _AdvisorEntity.Title = dr[ds.AdvisorTable.TITLE].ToString();
                _AdvisorEntity.WorkPhoneNumber = new PhoneNumberEntity
                                                     {
                                                         CountryCode = dr[ds.AdvisorTable.WORKPHONECOUNTRYCODE].ToString(),
                                                         CityCode = dr[ds.AdvisorTable.WORKPHONECITYCODE].ToString(),
                                                         PhoneNumber = dr[ds.AdvisorTable.WORKPHONENUMBER].ToString()
                                                     };
                if (ds.ExtendedProperties["Message"] == null)
                {
                    ds.ExtendedProperties.Add("Message", "Adviser updated successfully.");
                }
                else
                {
                    ds.ExtendedProperties["Message"] = "Adviser updated successfully.";
                }
            }
        }

        private void AddAdviser(AdvisorDS ds)
        {
            foreach (DataRow dr in ds.AdvisorTable.Rows)
            {
                var adviserEntity = new AdvisorEntity();
                adviserEntity.ClientId = EclipseClientID;
                OtherID = dr[ds.AdvisorTable.OTHERID].ToString();
                adviserEntity.AFSLicenceNumber = dr[ds.AdvisorTable.AFSLICENCENUMBER].ToString();
                adviserEntity.AFSLicenseeName = dr[ds.AdvisorTable.AFSLICENSEENAME].ToString();
                adviserEntity.AppointedDate = dr[ds.AdvisorTable.APPOINTEDDATE] as DateTime?;
                adviserEntity.AuthorisedRepNo = dr[ds.AdvisorTable.AUTHORISEDREPNO].ToString();
                adviserEntity.BTWRAPAdviserCode = dr[ds.AdvisorTable.BTWRAPADVISERCODE].ToString();
                adviserEntity.BankWestAdvisorCode = dr[ds.AdvisorTable.BANKWESTADVISORCODE].ToString();
                adviserEntity.DesktopBrokerAdviserCode = dr[ds.AdvisorTable.DESKTOPBROKERADVISERCODE].ToString();
                adviserEntity.Email = dr[ds.AdvisorTable.EMAIL].ToString();
                adviserEntity.Facsimile = new PhoneNumberEntity
                                              {
                                                  CountryCode = dr[ds.AdvisorTable.FACSIMILECOUNTRYCODE].ToString(),
                                                  CityCode = dr[ds.AdvisorTable.FACSIMILECITYCODE].ToString(),
                                                  PhoneNumber = dr[ds.AdvisorTable.FACSIMILENUMBER].ToString()
                                              };
                adviserEntity.Gender = dr[ds.AdvisorTable.GENDER].ToString();
                adviserEntity.HomePhoneNumber = new PhoneNumberEntity
                                                    {
                                                        CountryCode = dr[ds.AdvisorTable.HOMEPHONECOUNTRYCODE].ToString(),
                                                        CityCode = dr[ds.AdvisorTable.HOMEPHONECITYCODE].ToString(),
                                                        PhoneNumber = dr[ds.AdvisorTable.HOMEPHONENUMBER].ToString()
                                                    };
                adviserEntity.MobilePhoneNumber = new MobileNumberEntity
                                                      {
                                                          CountryCode = dr[ds.AdvisorTable.MOBILEPHONECOUNTRYCODE].ToString(),
                                                          MobileNumber = dr[ds.AdvisorTable.MOBILEPHONENUMBER].ToString()
                                                      };
                adviserEntity.Name = dr[ds.AdvisorTable.FIRSTNAME].ToString();
                adviserEntity.Occupation = dr[ds.AdvisorTable.OCCUPATION].ToString();
                adviserEntity.OrganizationStatus = dr[ds.AdvisorTable.STATUS].ToString();
                adviserEntity.PreferredName = dr[ds.AdvisorTable.PREFERREDNAME].ToString();
                adviserEntity.ReasonTerminated = dr[ds.AdvisorTable.AFSLICENCENUMBER].ToString();
                adviserEntity.Surname = dr[ds.AdvisorTable.SURNAME].ToString();
                adviserEntity.TFN = dr[ds.AdvisorTable.TFN].ToString();
                adviserEntity.TerminatedDate = dr[ds.AdvisorTable.TERMINATEDDATE] as DateTime?;
                adviserEntity.Title = dr[ds.AdvisorTable.TITLE].ToString();
                adviserEntity.WorkPhoneNumber = new PhoneNumberEntity
                                                    {
                                                        CountryCode = dr[ds.AdvisorTable.WORKPHONECOUNTRYCODE].ToString(),
                                                        CityCode = dr[ds.AdvisorTable.WORKPHONECITYCODE].ToString(),
                                                        PhoneNumber = dr[ds.AdvisorTable.WORKPHONENUMBER].ToString()
                                                    };
                _AdvisorEntity = adviserEntity;
                if (ds.ExtendedProperties["Message"] == null)
                {
                    ds.ExtendedProperties.Add("Message", "Adviser added successfully.");
                }
                else
                {
                    ds.ExtendedProperties["Message"] = "Adviser added successfully.";
                }
            }
        }

        private void SetChildAccountEntitySecuirty(int type, string data)
        {
            if (_Children != null)
                foreach (IdentityCM cm in _Children)
                {
                    var comp = this.Broker.GetCMImplementation(cm.Clid, cm.Csid);
                    if (comp != null)
                    {
                        comp.UpdateDataStream(type, data);
                    }
                }
        }

        #endregion

        #region Private Methods
        private void SetAddressToAdvisor(string data)
        {
            IFAtoAdvisor ifatoadvisor = data.ToNewOrData<IFAtoAdvisor>();
            _AdvisorEntity.Address = ifatoadvisor.Address;
            _AdvisorEntity.WorkPhoneNumber = ifatoadvisor.WorkPhoneNumber;
            _AdvisorEntity.Facsimile = ifatoadvisor.FaxNumber;
        }

        private IdentityCM GetChild(string data)
        {
            return data.ToIdentity();
        }

        private IdentityCM GetParent(string data)
        {
            return data.ToIdentity();
        }

        private void BMCUpdateComponent(IdentityCM child, CmCommand cmd)
        {
            IBrokerManagedComponent component = base.Broker.GetCMImplementation(child.Clid, child.Csid) as IBrokerManagedComponent;
            switch (cmd)
            {
                case CmCommand.Add_Single:
                    Broker.SetWriteStart();
                    component.UpdateDataStream((int)CmCommand.AttachToParent, this.ToIdentityXml());
                    break;
                case CmCommand.Remove_Single:
                    Broker.SetWriteStart();
                    component.UpdateDataStream((int)CmCommand.DeAttachFromParent, this.ToIdentityXml());
                    break;
            }
        }






        private List<OTC.OrganizationUnit> GetOrganizationUnitNameList(List<IdentityCM> value)
        {
            IBrokerManagedComponent component = null;
            List<OTC.OrganizationUnit> entities = new List<OTC.OrganizationUnit>();
            foreach (var each in value)
            {
                component = base.Broker.GetCMImplementation(each.Clid, each.Csid) as IBrokerManagedComponent;
                if (component == null) continue;
                entities.Add(new OTC.OrganizationUnit()
                {
                    Clid = each.Clid,
                    Csid = each.Csid,
                    Name = component.Name,
                    Type = component.TypeName
                });
            }
            return entities;
        }

        ApplicationUser ICanCreateDefaultUser.GetDefaultUser()
        {
            ApplicationUser user = new ApplicationUser();
            user.Email = _AdvisorEntity.Email;
            user.FirstName = _AdvisorEntity.Name;
            user.IsAdministrator = false;
            user.LastName = _AdvisorEntity.Surname;
            user.LoginId = _AdvisorEntity.Email;
            user.OrganizationId = this.Broker.GetWellKnownCM(WellKnownCM.Organization).CIID;
            user.OrganizationName = "Default";
            user.Password = "$Admin#1";
            return user;
        }
        #endregion

        public string GetCsvData(CSVType csvtype)
        {
            this.GetDataStream(-999, "");
            string xml = string.Empty;

            return this.GetCsvData_AllDetails();
        }


        public void Export(XElement root)
        {
            _AdvisorEntity.OrganizationStatus = this.OrganizationStatus;
            _AdvisorEntity.ClientId = this.ClientId;
            root.Add(_AdvisorEntity.ToXElement("Adviser"));
            root.Add(Parent.ToXElement("Parent"));
            root.Add(_Children.ToXElement("Children"));
        }

        public void Import(XElement root, IdentityCmMap map)
        {
            _AdvisorEntity = root.FromXElement<AdvisorEntity>("Adviser");
            map.ReplaceOrRemove(_AdvisorEntity.BankAccounts);
            Name = _AdvisorEntity.Name;
            OrganizationStatus = _AdvisorEntity.OrganizationStatus;

            Parent = root.FromXElement<IdentityCM>("Parent");
            map.ReplaceOrNull(Parent);

            _Children = root.FromXElement<List<IdentityCM>>("Children");
            map.ReplaceOrRemove(_Children);
        }

        public override List<IDictionary<string, string>> DoPartialTag(EntityTag tag)
        {
            List<IDictionary<string, string>> list = new List<IDictionary<string, string>>();
            list.Add(_AdvisorEntity.DoPartialTagExtension<AdvisorEntity>(tag));
            return list;
        }

        public override string FullName()
        {
            return _AdvisorEntity.FullName;
        }

        public override void DoTagged(EntityTag tag, Guid modelId)
        {
            string appender = tag.Appender;


            _AdvisorEntity.DoTaggedExtension<AdvisorEntity>(tag.SetAppenderExt(appender, ""));

            if (this.Parent != null)
            {

                var orgnizationunitcm = this.Parent.ToOrganizationUnit(Broker);
                if (orgnizationunitcm != null)
                    orgnizationunitcm.DoTagged(tag.SetAppenderExt(appender, "IFA"), modelId);


            }

            #region Address

            _AdvisorEntity.Address.BusinessAddress.DoTaggedExtension<Oritax.TaxSimp.Common.AddressEntity>(tag.SetAppenderExt(appender, "BusinessAddress"));

            if (!_AdvisorEntity.Address.RegisteredAddressSame)
                _AdvisorEntity.Address.RegisteredAddress.DoTaggedExtension<Oritax.TaxSimp.Common.AddressEntity>(tag.SetAppenderExt(appender, "RegisteredAddress"));

            _AdvisorEntity.Address.ResidentialAddress.DoTaggedExtension<Oritax.TaxSimp.Common.AddressEntity>(tag.SetAppenderExt(appender, "ResidentialAddress"));

            if (!_AdvisorEntity.Address.MailingAddressSame)
                _AdvisorEntity.Address.MailingAddress.DoTaggedExtension<Oritax.TaxSimp.Common.AddressEntity>(tag.SetAppenderExt(appender, "MailingAddress"));

            if (!_AdvisorEntity.Address.DuplicateMailingAddressSame)
                _AdvisorEntity.Address.DuplicateMailingAddress.DoTaggedExtension<Oritax.TaxSimp.Common.AddressEntity>(tag.SetAppenderExt(appender, "DuplicateMailingAddress"));

            _AdvisorEntity.Address.DoTaggedExtension<Oritax.TaxSimp.Common.DualAddressEntity>(tag.SetAppenderExt(appender, ""));

            #endregion


            tag.SetAppender(appender);

        }

        #region Security

        private string GetEntitySecurity()
        {
            EntitySecurity entity = new EntitySecurity();

            PartyDS ds = new PartyDS();
            base.OnGetData(ds);
            foreach (DataRow row in ds.Tables[PartyDS.INCLUDEDPARTY_TABLE].Rows)
            {
                entity.IncludedUsers.Add(new UserEntity() { CID = new Guid(row[PartyDS.PARTYCID_FIELD].ToString()), CurrentUserName = row[PartyDS.PARTYNAME_FIELD].ToString() });
            }
            foreach (DataRow row in ds.Tables[PartyDS.AVAILABLEPARTY_TABLE].Rows)
            {
                if (row[PartyDS.PARTYNAME_FIELD].ToString().ToLower() != "Administrator, Administrator".ToLower())
                {
                    entity.AvailableUsers.Add(new UserEntity() { CID = new Guid(row[PartyDS.PARTYCID_FIELD].ToString()), CurrentUserName = row[PartyDS.PARTYNAME_FIELD].ToString() });
                }
            }

            return entity.ToXmlString();
        }

        private string DeleteEntitySecurity(string UserEntity)
        {
            UserEntity entity = UserEntity.ToData<UserEntity>();

            this.Broker.SetWriteStart();

            PartyDS ds = new PartyDS();
            base.OnGetData(ds);
            ds.AcceptChanges();

            DataRow[] drCollection = ds.Tables[PartyDS.INCLUDEDPARTY_TABLE].Select(PartyDS.PARTYCID_FIELD + " = '" + entity.CID + "'", String.Empty);
            drCollection[0].Delete();
            this.SetData(ds);

            return string.Empty;
        }

        #endregion

        #region OrganizationsChart

        public OrganizationChart GetOrganizationChart()
        {
            OrganizationChart chart = new OrganizationChart();

            chart.Clid = this.CLID;
            chart.Csid = this.CSID;
            chart.NodeName = this._AdvisorEntity.Name;

            return chart;
        }

        public OrganizationChart GetAssociation(IIdentityCM identitycm, OrganizationType organizationtype)
        {
            OrganizationChart OChart = new OrganizationChart();
            switch (organizationtype)
            {
                case OrganizationType.BankAccount:
                    if (this._AdvisorEntity.BankAccounts.Where(e => e.Clid == identitycm.Clid && e.Csid == identitycm.Csid).ToList().Count > 0)
                    {
                        OChart.Clid = this.Clid;
                        OChart.Csid = this.Csid;
                        OChart.NodeName = this.Name;
                    }
                    break;
                case OrganizationType.Adviser:
                    if (identitycm.Clid == this.Clid && identitycm.Csid == this.Csid)
                    {
                        OChart = new OrganizationChart() { Clid = this.Clid, Csid = this.Csid, NodeName = this._AdvisorEntity.Name + " " + this._AdvisorEntity.Surname, TypeName = OrganizationType.Adviser.ToString() };
                        //OChart.Subordinates.Add(this._Children.ToOrganizationChart(this.Broker, "Clients"));
                        OChart.Subordinates.Add(GetEntityChild());
                        OChart.Subordinates.Add(this._AdvisorEntity.BankAccounts.ToOrganizationChart(this.Broker, "Bank Account(s)"));

                        OChart.Subordinates.RemoveAll(e => e == null);
                        OChart = GetEntityParent(OChart);
                    }
                    break;
                default:
                    break;
            }
            return OChart;
        }

        public OrganizationChart GetEntityParent(OrganizationChart organizationchart)
        {
            ICalculationModule module = null;
            OrganizationChart association = new OrganizationChart() { Clid = this.Clid, Csid = this.Csid, NodeName = this._AdvisorEntity.Name + " " + this._AdvisorEntity.Surname, TypeName = OrganizationType.Adviser.ToString() };
            if (organizationchart.Clid == this.Clid && organizationchart.Csid == this.Csid)
            {
                association = organizationchart;
            }
            else
            {
                association.Subordinates.Add(organizationchart);
            }
            module = Broker.GetCMImplementation(this.Parent.Clid, this.Parent.Csid);
            if (module != null && module is IHaveAssociation)
            {
                association = (module as IHaveAssociation).GetEntityParent(association);
            }
            return association;
        }

        public OrganizationChart GetEntityChild()
        {
            ICalculationModule module = null;
            //OrganizationChart association = new OrganizationChart() { Clid = this.Clid, Csid = this.Csid, NodeName = this._AdvisorEntity.Name + " " + this._AdvisorEntity.Surname, TypeName = OrganizationType.Advisor.ToString() };
            OrganizationChart children = new OrganizationChart();
            children.NodeName = "Clients";
            foreach (var each in this._Children)
            {
                module = Broker.GetCMImplementation(each.Clid, each.Csid);
                if (module != null && module is IHaveAssociation)
                {
                    children.Subordinates.Add((module as IHaveAssociation).GetEntityChildWithEntity());
                }
            }
            //association.Subordinates.Add(children);
            return children.Subordinates.Count <= 0 ? null : children;
        }

        public OrganizationChart GetEntityChildWithEntity()
        {
            ICalculationModule module = null;
            OrganizationChart association = new OrganizationChart() { Clid = this.Clid, Csid = this.Csid, NodeName = this._AdvisorEntity.Name + " " + this._AdvisorEntity.Surname, TypeName = OrganizationType.Adviser.ToString() };
            OrganizationChart children = new OrganizationChart();
            children.NodeName = "Clients";
            foreach (var each in this._Children)
            {
                module = Broker.GetCMImplementation(each.Clid, each.Csid);
                if (module != null && module is IHaveAssociation)
                {
                    children.Subordinates.Add((module as IHaveAssociation).GetEntityChildWithEntity());
                }
            }
            association.Subordinates.Add(children.Subordinates.Count <= 0 ? null : children);
            association.Subordinates.Add(this._AdvisorEntity.BankAccounts.ToOrganizationChart(this.Broker, "Bank Account(s)"));
            association.Subordinates.RemoveAll(e => e == null);
            return association;
        }

        #endregion

        #region GetXMLData

        private string ExtractXML(string data)
        {
            string xmlco = _AdvisorEntity.ToXmlString();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmlco);
            XMLExtensions.SetNode(_AdvisorEntity.BankAccounts, "BankAccounts", "BankAccount", doc, this.Broker);
            XmlAttribute rootAtt = doc.CreateAttribute("Type");
            rootAtt.Value = this.TypeName;
            doc.DocumentElement.Attributes.Append(rootAtt);
            string xml = doc.OuterXml;
            XDocument document = XDocument.Load(XmlReader.Create(new StringReader(xml)));
            document.Root.Name = data;
            document.Root.RemoveAttributes();
            xml = document.ToString().Trim();
            return xml;
        }

        public override string GetXmlData()
        {
            string xmlco = _AdvisorEntity.ToXmlString();
            XmlDocument doc = new XmlDocument();
            doc.AppendChild(doc.CreateElement("XYZ"));
            //doc.LoadXml(xmlco);
            //XMLExtensions.SetNode(_AdvisorEntity.BankAccounts, "BankAccounts", "BankAccount", doc, this.Broker);

            //XMLExtensions.SetParentNode(this.Parent, "Adviser", doc, this.Broker);
            IdentityCM BusinessGroupIdentityCM = this.Parent;//XMLExtensions.GetParentIDs(this.Parent, this.Broker);
            XMLExtensions.SetParentNode(BusinessGroupIdentityCM, "AdviserBusinessGroup", doc, this.Broker);
            IdentityCM InvestorGroupIdentityCM = XMLExtensions.GetParentIDs(BusinessGroupIdentityCM, this.Broker);
            XMLExtensions.SetParentNode(InvestorGroupIdentityCM, "InvestorGroup", doc, this.Broker);

            XMLExtensions.AddNode("AdviserId", this.ClientId, doc);
            XMLExtensions.AddNode("AdviserFirstName", this._AdvisorEntity.Name, doc);
            XMLExtensions.AddNode("AdviserLastName", this._AdvisorEntity.Surname, doc);
            XMLExtensions.AddNode("AdviserEmail", this._AdvisorEntity.Email, doc);

            XmlAttribute rootAtt = doc.CreateAttribute("Type");
            rootAtt.Value = this.TypeName;
            doc.DocumentElement.Attributes.Append(rootAtt);
            return doc.OuterXml;
        }

        #endregion

        private string GetCsvData_AllDetails()
        {
            StringBuilder csvstring = new StringBuilder();

            CSVInstance csvinstance = new CSVInstance(this.Broker);

            string ifaName = string.Empty;

            OrganizationUnitCM ifa = null;

            ICalculationModule parentCalcModule = null;

            if (this.Parent != null)
                parentCalcModule = this.Broker.GetCMImplementation(Parent.Clid, Parent.Csid);

            if (parentCalcModule != null)
                ifa = (OrganizationUnitCM)parentCalcModule;

            if (ifa != null)
                ifaName = ifa.Name;

            IOrganizationUnit dealerGroup = null;

            if (ifa != null && ((OrganizationUnitCM)ifa).ConfiguredParent != null)
                dealerGroup = (IOrganizationUnit)this.Broker.GetCMImplementation(((OrganizationUnitCM)ifa).ConfiguredParent.Clid, ((OrganizationUnitCM)ifa).ConfiguredParent.Csid);

            string dealerGroupName = String.Empty;

            if (dealerGroup != null)
                dealerGroupName = dealerGroup.Name;

            AdviserDetailsCSVColumns obj = new AdviserDetailsCSVColumns();
            obj.SetColumnValue(obj.Col_ID, this.ClientId);
            obj.SetColumnValue(obj.Col_Name, this._AdvisorEntity.Name);
            obj.SetColumnValue(obj.Col_Surname, this._AdvisorEntity.Surname);
            obj.SetColumnValue(obj.Col_Addressline1, this._AdvisorEntity.Address.BusinessAddress.Addressline1);
            obj.SetColumnValue(obj.Col_Addressline2, this._AdvisorEntity.Address.BusinessAddress.Addressline2);
            obj.SetColumnValue(obj.Col_AFSLicenceNumber, this._AdvisorEntity.AFSLicenceNumber);
            obj.SetColumnValue(obj.Col_AFSLicenseeName, this._AdvisorEntity.AFSLicenseeName);
            obj.SetColumnValue(obj.Col_AuthorisedRepNo, this._AdvisorEntity.AuthorisedRepNo);
            obj.SetColumnValue(obj.Col_BankWestAdvisorCode, this._AdvisorEntity.BankWestAdvisorCode);
            obj.SetColumnValue(obj.Col_BTWRAPAdviserCode, this._AdvisorEntity.BTWRAPAdviserCode);
            obj.SetColumnValue(obj.Col_Country, this._AdvisorEntity.Address.BusinessAddress.Country);
            obj.SetColumnValue(obj.Col_DealerGroup, dealerGroupName);
            obj.SetColumnValue(obj.Col_DesktopBrokerAdviserCode, this._AdvisorEntity.DesktopBrokerAdviserCode);
            obj.SetColumnValue(obj.Col_Email, this._AdvisorEntity.Email);
            obj.SetColumnValue(obj.Col_Facsimile, this._AdvisorEntity.Facsimile.PhoneNumber);
            obj.SetColumnValue(obj.Col_HomePhoneNumber, this._AdvisorEntity.HomePhoneNumber.PhoneNumber);
            obj.SetColumnValue(obj.Col_IFA, ifaName);
            obj.SetColumnValue(obj.Col_MobilePhoneNumber, this._AdvisorEntity.MobilePhoneNumber.MobileNumber);
            obj.SetColumnValue(obj.Col_Occupation, this._AdvisorEntity.Occupation);
            obj.SetColumnValue(obj.Col_PostCode, this._AdvisorEntity.Address.BusinessAddress.PostCode);
            obj.SetColumnValue(obj.Col_State, this._AdvisorEntity.Address.BusinessAddress.State);
            obj.SetColumnValue(obj.Col_Suburb, this._AdvisorEntity.Address.BusinessAddress.Suburb);
            obj.SetColumnValue(obj.Col_Title, this._AdvisorEntity.Title);
            obj.SetColumnValue(obj.Col_WorkPhoneNumber, this._AdvisorEntity.WorkPhoneNumber.PhoneNumber);

            csvstring.Append(obj.ToCSV());

            Broker.ReleaseBrokerManagedComponent(ifa);
            Broker.ReleaseBrokerManagedComponent(dealerGroup);

            return csvstring.ToString();
        }




        public override void FillEntityDetailsInObjects(object target, int type)
        {
            switch (type)
            {
                case 1: // epi data response

                    var advisor = (target as EPIDataResponseAdviser);
                    advisor.LastModified = DateTime.Now;
                    advisor.AFSL = _AdvisorEntity.AFSLicenceNumber.ReplaceEmptyWithNull();
                    if (_AdvisorEntity.Address == null)
                        _AdvisorEntity.Address = new OTC.DualAddressEntity();

                    var ContactDetails = new ContactDetails()
                                                            {

                                                                Addresses =
                                                                    new ContactDetailsAddressesAddress[]
                                                                                               {
                                                                                                   new ContactDetailsAddressesAddress
                                                                                                       ()
                                                                                                       {
                                                                                                           Addressee =_AdvisorEntity.Name.ReplaceEmptyWithNull(),
                                                                                                           Country =Country.AU,
                                                                                                          
                                                                                                           
                                                                                                           Line1 =_AdvisorEntity.Address.BusinessAddress.Addressline1.ReplaceEmptyWithNull()??"line1",
                                                                                                           Line2 =_AdvisorEntity.Address.BusinessAddress.Addressline2.ReplaceEmptyWithNull(),
                                                                                                           
                                                                                                           PostCode =_AdvisorEntity.Address.BusinessAddress.PostCode.ReplaceEmptyWithNull()??"123",
                                                                                                           Preferred =true,
                                                                                                           State =_AdvisorEntity.Address.BusinessAddress.State.ReplaceEmptyWithNull()??"NSW",
                                                                                                           Suburb =_AdvisorEntity.Address.BusinessAddress.Suburb.ReplaceEmptyWithNull()??"subrub",
                                                                                                           Type =EPI.Data.AddressType.Business
                                                                                                       }
                                                                                               },









                                                                PreferredContactMethod = ContactMethod.Address

                                                            };

                    if (_AdvisorEntity.WorkPhoneNumber.PhoneNumber.ReplaceEmptyWithNull() != null)
                    {

                        ContactDetails.PhoneNumbers =
                            new ContactDetailsPhoneNumbersPhoneNumber
                                []
                            {
                                new ContactDetailsPhoneNumbersPhoneNumber
                                    ()
                                    {
                                        AreaCode = _AdvisorEntity.WorkPhoneNumber.CityCode.ReplaceEmptyWithNull(),
                                        CountryCode = _AdvisorEntity.WorkPhoneNumber.CountryCode.ReplaceEmptyWithNull(),
                                        Number = _AdvisorEntity.WorkPhoneNumber.PhoneNumber.ReplaceEmptyWithNull(),
                                        Preferred = false,
                                        Type = PhoneNumberType.Business
                                    }
                            };
                        ContactDetails.PreferredContactMethod = ContactMethod.Phone;
                    }

                    if (_AdvisorEntity.Email.ReplaceEmptyWithNull() != null)
                    {
                        ContactDetails.EmailAddresses =
                            new ContactDetailsEmailAddressesEmailAddress
                                []
                                {
                                    new ContactDetailsEmailAddressesEmailAddress
                                        ()
                                        {
                                            Address = _AdvisorEntity.Email.ReplaceEmptyWithNull(),
                                            Preferred = true,
                                            Type = EmailAddressType.Business
                                        }
                                };
                        ContactDetails.PreferredContactMethod = ContactMethod.Email;
                    }



                    advisor.ContactDetails = ContactDetails;

                    // advisor.FPSId = "xxxx";
                    advisor.FirstName = _AdvisorEntity.Name.ReplaceEmptyWithNull();
                    advisor.MiddleName = advisor.MiddleName.ReplaceEmptyWithNull();
                    advisor.LastName = _AdvisorEntity.Surname.ReplaceEmptyWithNull();
                    Gender gender;
                    if (System.Enum.TryParse(_AdvisorEntity.Gender, true, out gender))
                    {
                        advisor.Gender = gender;


                    }

                    advisor.Id = this.ClientId.ReplaceEmptyWithNull();

                    Title title;
                    if (System.Enum.TryParse(_AdvisorEntity.Title, true, out title))
                    {
                        advisor.Title = title;


                    }





                    DealerGroup dealg = null;
                    if (this.Parent != null && this.Parent.Clid != Guid.Empty && this.Parent.Csid != Guid.Empty)
                    {
                        var ifa = this.Broker.GetCMImplementation(Parent.Clid, Parent.Csid);
                        if (ifa != null && ifa is ICmHasParent)
                        {
                            var ifaParent = (ifa as ICmHasParent).Parent;


                            if (ifaParent != null && ifaParent.Clid != Guid.Empty && ifaParent.Csid != Guid.Empty)
                            {

                                var group = this.Broker.GetCMImplementation(ifaParent.Clid, ifaParent.Csid);

                                dealg = new DealerGroup() { Id = (group as OrganizationUnitCM).ClientId, };
                                (group as OrganizationUnitCM).FillEntityDetailsInObjects(dealg, 1);
                            }



                        }

                    }

                    // businessDetails.Add(new AdviserBusiness() { AFSL = _AdvisorEntity.AFSLicenseeName, });

                    if (dealg != null)
                    {
                        List<object> businessDetails = new List<object>();
                        businessDetails.Add(dealg);
                        advisor.BusinessDetails = businessDetails.ToArray();
                    }


                    break;

            }



        }
    }

    [Serializable]
    public class AdvisorEntity : IClientEntity, IAdviserEntity
    {
        public String Name { get; set; }
        public String LegalName { get; set; }
        public String Surname { get; set; }
        public String AuthorisedRepNo { get; set; }
        public Oritax.TaxSimp.Common.DualAddressEntity Address { get; set; }
        public PhoneNumberEntity HomePhoneNumber { get; set; }
        public PhoneNumberEntity WorkPhoneNumber { get; set; }
        public MobileNumberEntity MobilePhoneNumber { get; set; }
        public PhoneNumberEntity Facsimile { get; set; }
        public String TFN { get; set; }
        public String Email { get; set; }
        public DateTime? AppointedDate { get; set; }
        public String Status { get; set; }
        public DateTime? TerminatedDate { get; set; }
        public String ReasonTerminated { get; set; }
        public String Gender { get; set; }
        public String PreferredName { get; set; }
        public String Title { get; set; }
        public String Occupation { get; set; }
        public String BTWRAPAdviserCode { get; set; }
        public String DesktopBrokerAdviserCode { get; set; }
        public String OrganizationStatus { get; set; }
        public String ClientId { get; set; }
        public List<Oritax.TaxSimp.Common.IdentityCMDetail> BankAccounts { get; set; }
        public string AFSLicenseeName { get; set; }
        public string AFSLicenceNumber { get; set; }
        public string BankWestAdvisorCode { get; set; }
        public IdentityCM Parent { get; set; }
        public List<IdentityCM> Children { get; set; }
        public IdentityCMDetail Individual { get; set; }


        public String FullName
        {
            get
            {
                return Name + " " + Surname;

            }


        }

    }
}