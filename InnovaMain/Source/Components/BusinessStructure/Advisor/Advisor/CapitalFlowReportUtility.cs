#region Using
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Xml.Linq;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.Group;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CM.Entity;
using Oritax.TaxSimp.Extensions;
using System.Collections.ObjectModel;
#endregion

namespace Oritax.TaxSimp.CM
{
    public class CapitalFlowReportUtility
    {
        public DateTime LastDayOfMonthFromDateTime(DateTime dateTime)
        {
            DateTime firstDayOfTheMonth = new DateTime(dateTime.Year, dateTime.Month, 1);
            return firstDayOfTheMonth.AddMonths(1).AddDays(-1);
        }

        public DateTime FirstDayOfMonthFromDateTime(DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, 1);
        }

        public void OnGetDataCapitalFlowDataSetCorporateInd(CapitalReportDS data, string clientType, string clientID, string clientName, IdentityCM Parent, ICMBroker Broker, ClientIndividualEntity Entity)
        {

            DataTable clientSummaryTable = data.Tables[CapitalReportDS.CLIENTSUMMARYTABLE];
            DataTable contactTable = data.Tables[CapitalReportDS.CONTACTTABLE];

            DataRow clientSummaryRow = clientSummaryTable.NewRow();
            DataRow contactRow = contactTable.NewRow();

            clientSummaryRow[CapitalReportDS.CLIENTID] = clientID;
            clientSummaryRow[CapitalReportDS.CLIENTNAME] = clientName;
            clientSummaryRow[CapitalReportDS.CIENTABN] = Entity.ABN;
            clientSummaryRow[CapitalReportDS.CLIENTTYPE] = clientType;

            if (Parent != null && Parent.Clid != Guid.Empty && Parent.Csid != Guid.Empty)
            {
                AdvisorCM adviserUnit = Broker.GetCMImplementation(Parent.Clid, Parent.Csid) as AdvisorCM;
                clientSummaryRow[HoldingRptDataSet.ADVISERFULLNAME] = adviserUnit.AdvisorEntity.FullName;
                Broker.ReleaseBrokerManagedComponent(adviserUnit);
            }

            clientSummaryRow[CapitalReportDS.ADDRESSLINE1] = Entity.Address.BusinessAddress.Addressline1;
            clientSummaryRow[CapitalReportDS.ADDRESSLINE2] = Entity.Address.BusinessAddress.Addressline2;
            clientSummaryRow[CapitalReportDS.SUBURB] = Entity.Address.BusinessAddress.Suburb;
            clientSummaryRow[CapitalReportDS.STATE] = Entity.Address.BusinessAddress.State;
            clientSummaryRow[CapitalReportDS.POSTCODE] = Entity.Address.BusinessAddress.PostCode;
            clientSummaryRow[CapitalReportDS.COUNTRY] = Entity.Address.BusinessAddress.Country;

            CSVInstance csvinstance = new CSVInstance(Broker);
            ClientIndividualEntityCSV _entitycsv = csvinstance.GetEntityConcreteInstance(Entity);
            foreach (var contact in _entitycsv.Applicants)
            {
                contactRow[CapitalReportDS.CONTACT_TITLE] = contact.PersonalTitle;
                contactRow[CapitalReportDS.CONTACT_NAME] = contact.Fullname;
                contactRow[CapitalReportDS.CONTACT_BIZNO] = contact.WorkPhoneNumber;
                contactRow[CapitalReportDS.CONTACT_MOBNO] = contact.MobilePhoneNumber;
                contactRow[CapitalReportDS.CONTACT_HNO] = contact.HomePhoneNumber;
                contactRow[CapitalReportDS.CONTACT_FAXNO] = contact.Facsimile;
                contactRow[CapitalReportDS.CONTACT_EMAIL] = contact.EmailAddress;
            }

            clientSummaryTable.Rows.Add(clientSummaryRow);
            contactTable.Rows.Add(contactRow);

            IOrganization orgCM = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            CapitalReportDS capitalReportDS = data as CapitalReportDS;

            DataTable CapitalSummaryTable = capitalReportDS.Tables[CapitalReportDS.CAPITALFLOWSUMMARY];
            DataTable productBreakDownTable = capitalReportDS.Tables[CapitalReportDS.PRODUCTBREAKDOWNTABLE];
            DataTable TDBreakDownTable = capitalReportDS.Tables[CapitalReportDS.TDBREAKDOWNTABLE];

            DataTable CapitalFlowSummaryReport = ExtractCapitalSummaryTableByCategoryInd(Broker, Entity, capitalReportDS, clientID, orgCM);

            if (Entity.ClientManualAssetCollection != null)
            {
                var groupedManualTransactions = Entity.ClientManualAssetCollection.GroupBy(man => man.InvestmentCode);

                foreach (var groupedManualTransaction in groupedManualTransactions)
                {
                    var systemManualAsset = orgCM.ManualAsset.Where(manList => manList.ID == groupedManualTransaction.Key).FirstOrDefault();
                    
                    decimal holdingLatest  = 0;
                    decimal holdingEndDate = 0;
                    decimal holdingStartDate = 0;
                    decimal unitholdingEndDate = 0;
                    decimal unitholdingStartDate = 0;

                    double unitPriceLatest = systemManualAsset.ASXSecurity.OrderByDescending(manualHis => manualHis.Date).FirstOrDefault().UnitPrice;
                    double unitPriceStartDate = systemManualAsset.ASXSecurity.OrderByDescending(manualHis => manualHis.Date).FirstOrDefault().UnitPrice; 

                    unitholdingStartDate = groupedManualTransaction.Where(manTran => manTran.TransactionDate <= capitalReportDS.StartDate).Sum(manTran => manTran.UnitsOnHand);
                    unitholdingEndDate = groupedManualTransaction.Where(manTran => manTran.TransactionDate <= capitalReportDS.EndDate).Sum(manTran => manTran.UnitsOnHand); 

                    //Calculate Holding based on unit price for each transactiondate
                    foreach (var manualTransaction in groupedManualTransaction)
                    {
                        decimal holdingPerTransaction = 0;
                        double unitPrice = 0;
                        
                        var manualHistory = systemManualAsset.ASXSecurity.Where(manualHis => manualHis.Date <= manualTransaction.TransactionDate).OrderByDescending(manualHis => manualHis.Date).FirstOrDefault();
                        if (manualHistory != null)
                            unitPrice = manualHistory.UnitPrice;
                      
                        holdingPerTransaction = manualTransaction.UnitsOnHand * Convert.ToDecimal(unitPrice);
                        holdingEndDate += holdingPerTransaction;
                    }

                    holdingStartDate = unitholdingStartDate * Convert.ToDecimal(unitPriceStartDate);
                    holdingLatest = unitholdingEndDate * Convert.ToDecimal(unitPriceLatest);

                    capitalReportDS.AddCapitalRow("ManualAsset", "ManualAsset", systemManualAsset.AsxCode, systemManualAsset.CompanyName, clientID, holdingLatest,holdingEndDate, holdingStartDate, string.Empty, string.Empty);
                }
            }

            foreach (var accountProcessTaskEntity in Entity.ListAccountProcess)
            {
                if (accountProcessTaskEntity.LinkedEntity != null && accountProcessTaskEntity.LinkedEntity.Clid != Guid.Empty)
                {
                    var asset = orgCM.Assets.Where(var => var.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                    var model = orgCM.Model.Where(mod => mod.ID == accountProcessTaskEntity.ModelID).FirstOrDefault();
                    var linkedAsset = model.Assets.Where(ast => ast.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                    var product = linkedAsset.Products.Where(pro => pro.ID == accountProcessTaskEntity.TaskID).FirstOrDefault();

                    if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.BankAccount)
                    {
                        BankAccountCM bankAccount = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as BankAccountCM;
                        var filteredTransactionsStart = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate <= capitalReportDS.StartDate);
                        decimal holdingTotalStart = filteredTransactionsStart.Sum(calc => calc.TotalAmount);

                        var filteredTransactionsEnd = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate <= capitalReportDS.EndDate);
                        decimal holdingTotalEnd = filteredTransactionsEnd.Sum(calc => calc.TotalAmount);

                        capitalReportDS.AddCapitalRow(asset.Name, model.ServiceType.ToString(), product.Name,
                                                            product.TaskDescription, bankAccount.BankAccountEntity.AccountNumber, holdingTotalEnd, holdingTotalEnd, holdingTotalStart, string.Empty, string.Empty);
                    }
                    else if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.TermDepositAccount)
                    {
                        BankAccountCM bankAccount = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as BankAccountCM;
                        decimal holdingTotalStart = 0;
                        decimal holdingTotalEnd= 0;

                        var filteredTransactions = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate <= capitalReportDS.StartDate);
                        var tdGroupsType = filteredTransactions.GroupBy(cashMan => cashMan.TransactionType);

                        foreach (var tdType in tdGroupsType)
                        {
                            var groupByIns = tdType.GroupBy(tdtypetran => tdtypetran.InstitutionID);

                            foreach (var groupins in groupByIns)
                            {
                                var transaction = groupins.FirstOrDefault();
                                decimal holding = groupins.Sum(inssum => inssum.Amount);

                                DataRow tDBreakDownTableRow = TDBreakDownTable.NewRow();
                                capitalReportDS.TDBreakDownRow(orgCM.Institution.Where(ins => ins.ID == groupins.Key).FirstOrDefault().Name, model.ServiceType.ToString(), tdType.Key, transaction.AdministrationSystem, holding, bankAccount.BankAccountEntity.InstitutionID.ToString(), tDBreakDownTableRow);
                                TDBreakDownTable.Rows.Add(tDBreakDownTableRow);

                                holdingTotalStart += holding;
                            }
                        }

                        filteredTransactions = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate <= capitalReportDS.EndDate);
                        tdGroupsType = filteredTransactions.GroupBy(cashMan => cashMan.TransactionType);

                        foreach (var tdType in tdGroupsType)
                        {
                            var groupByIns = tdType.GroupBy(tdtypetran => tdtypetran.InstitutionID);

                            foreach (var groupins in groupByIns)
                            {
                                var transaction = groupins.FirstOrDefault();
                                decimal holding = groupins.Sum(inssum => inssum.Amount);

                                DataRow tDBreakDownTableRow = TDBreakDownTable.NewRow();
                                capitalReportDS.TDBreakDownRow(orgCM.Institution.Where(ins => ins.ID == groupins.Key).FirstOrDefault().Name, model.ServiceType.ToString(), tdType.Key, transaction.AdministrationSystem, holding, bankAccount.BankAccountEntity.InstitutionID.ToString(), tDBreakDownTableRow);
                                TDBreakDownTable.Rows.Add(tDBreakDownTableRow);

                                holdingTotalEnd += holding;
                            }
                        }

                        capitalReportDS.AddCapitalRow(asset.Name, model.ServiceType.ToString(), product.Name,
                                                            product.TaskDescription, bankAccount.BankAccountEntity.AccountNumber, holdingTotalEnd, holdingTotalEnd,holdingTotalStart, string.Empty, bankAccount.BankAccountEntity.InstitutionID.ToString());
                    }
                    else if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.DesktopBrokerAccount)
                    {
                        DesktopBrokerAccountCM desktopBrokerAccountCM = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid,
                                                                                    accountProcessTaskEntity.LinkedEntity.Csid) as DesktopBrokerAccountCM;
                        decimal presentHoldingTotal = 0;
                        decimal endHoldingTotal = 0;
                        decimal startHoldingTotal = 0;

                        //If it is linked with product securities for split type calculation
                        if (product.ProductSecuritiesId != Guid.Empty)
                        {
                            var productSecurity = orgCM.ProductSecurities.Where(proseclist => proseclist.ID == product.ProductSecuritiesId).FirstOrDefault();
                            var linkedAssets = model.Assets.Where(ast => ast.ID == accountProcessTaskEntity.AssetID && accountProcessTaskEntity.ModelID == model.ID);
                            double totalPercentage = HoldingRptDataSet.CalculateTotalPercentageForSplit(linkedAssets);

                            foreach (var productSecurityDetail in productSecurity.Details)
                            {
                                var asxBroker = orgCM.Securities.Where(sec => sec.ID == productSecurityDetail.SecurityCodeId).FirstOrDefault();
                                var filteredHoldingTransactionsByStartDate = desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Where(holdTrans =>
                                                                                                              holdTrans.TradeDate <= capitalReportDS.StartDate);
                                var filteredHoldingTransactionsStartDate = filteredHoldingTransactionsByStartDate.Where(holdTrans =>
                                                                                                              holdTrans.InvestmentCode == asxBroker.AsxCode);

                                var filteredHoldingTransactionsByEndDate = desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Where(holdTrans =>
                                                                                                              holdTrans.TradeDate <= capitalReportDS.EndDate);
                                var filteredHoldingTransactionsEndDate = filteredHoldingTransactionsByEndDate.Where(holdTrans =>
                                                                                                              holdTrans.InvestmentCode == asxBroker.AsxCode);

                                var count = filteredHoldingTransactionsByEndDate.Count();

                                if (count > 0)
                                {
                                    totalPercentage = product.SharePercentage;

                                    foreach (var linkedasst in linkedAssets)
                                    {
                                        var otherProducts = linkedasst.Products.Where(linkedAsstPro => linkedAsstPro.ProductSecuritiesId != productSecurity.ID);
                                        foreach (var OtherproductSecurity in otherProducts)
                                        {
                                            var OtherproductSecurityDetails = orgCM.ProductSecurities.Where(otherpro => otherpro.ID == OtherproductSecurity.ProductSecuritiesId).FirstOrDefault();

                                            if (OtherproductSecurityDetails != null)
                                            {
                                                var otherAsxBroker = OtherproductSecurityDetails.Details.Where(otherProDetail => otherProDetail.SecurityCodeId == asxBroker.ID);

                                                if (otherAsxBroker.Count() > 0)
                                                    totalPercentage += OtherproductSecurity.SharePercentage;
                                            }
                                        }
                                    }

                                    decimal unitPriceStart = 0;
                                    decimal unitPriceEnd = 0;
                                    
                                    var hisSecurityStart = asxBroker.ASXSecurity.Where(his => his.Date <= capitalReportDS.StartDate).OrderByDescending(secpri => secpri.Date).FirstOrDefault();
                                    decimal startHoldingUnits = filteredHoldingTransactionsStartDate.Sum(filteredHolding => filteredHolding.Units);

                                    if (hisSecurityStart != null)
                                        unitPriceStart = Convert.ToDecimal(hisSecurityStart.UnitPrice);

                                    decimal startHoldingValue = startHoldingUnits * unitPriceStart;

                                    var hisSecurityLatest = asxBroker.ASXSecurity.Where(his => his.Date <= capitalReportDS.EndDate).OrderByDescending(secpri => secpri.Date).FirstOrDefault();
                                    decimal endHoldingUnits = filteredHoldingTransactionsEndDate.Sum(filteredHolding => filteredHolding.Units);

                                    if (hisSecurityLatest != null)
                                        unitPriceEnd = Convert.ToDecimal(hisSecurityLatest.UnitPrice);

                                    decimal latestHoldingValue = endHoldingUnits * unitPriceEnd;

                                    decimal endHoldingValue = filteredHoldingTransactionsEndDate.Sum(filteredHolding => filteredHolding.NetValue);

                                    startHoldingValue = startHoldingValue * Convert.ToDecimal((product.SharePercentage / totalPercentage));
                                    latestHoldingValue = latestHoldingValue * Convert.ToDecimal((product.SharePercentage / totalPercentage));
                                    endHoldingValue = endHoldingValue * Convert.ToDecimal((product.SharePercentage / totalPercentage));

                                    capitalReportDS.AddProductDetailRow(product, unitPriceEnd, asxBroker.AsxCode, endHoldingUnits, endHoldingUnits,
                                                                           startHoldingUnits, latestHoldingValue, startHoldingValue,endHoldingValue, model.ServiceType.ToString());

                                    presentHoldingTotal += latestHoldingValue;
                                    endHoldingTotal += endHoldingValue;
                                    startHoldingTotal += startHoldingValue;
                                }
                            }
                        }
                        else
                        {
                            var filteredHoldingTransactionsByStartDate = desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Where(holdTrans =>
                                                                                                              holdTrans.TradeDate <= capitalReportDS.StartDate);

                            var filteredHoldingTransactionsByEndDate = desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Where(holdTrans =>
                                                                                  holdTrans.TradeDate <= capitalReportDS.EndDate);

                            var groupedSecurities = filteredHoldingTransactionsByEndDate.GroupBy(holdTran => holdTran.InvestmentCode);

                            foreach (var security in groupedSecurities)
                            {
                                decimal totalSecHoldingUnits = security.Sum(sec => sec.Units);
                                var asxBroker = orgCM.Securities.Where(sec => sec.AsxCode == security.Key).FirstOrDefault();


                                var hisSecurityStart = asxBroker.ASXSecurity.Where(his => his.Date <= capitalReportDS.StartDate).OrderByDescending(secpri => secpri.Date).FirstOrDefault();
                                decimal startHoldingUnits = filteredHoldingTransactionsByStartDate.Sum(filteredHolding => filteredHolding.Units);

                                decimal startUnitPrice = 0;
                                decimal endUnitPrice = 0; 

                                if (hisSecurityStart != null)
                                    startUnitPrice = Convert.ToDecimal(hisSecurityStart.UnitPrice);

                                decimal startHoldingValue = startHoldingUnits * startUnitPrice;

                                var hisSecurityLatest = asxBroker.ASXSecurity.Where(his => his.Date <= capitalReportDS.EndDate).OrderByDescending(secpri => secpri.Date).FirstOrDefault();

                                if (hisSecurityLatest != null)
                                    endUnitPrice = Convert.ToDecimal(hisSecurityLatest.UnitPrice);

                                decimal endHoldingUnits = filteredHoldingTransactionsByEndDate.Where(tran => tran.InvestmentCode == security.Key).Sum(filteredHolding => filteredHolding.Units);
                                decimal latestHoldingValue = endHoldingUnits * endUnitPrice;

                                decimal endHoldingValue = filteredHoldingTransactionsByEndDate.Where(tran => tran.InvestmentCode == security.Key).Sum(filteredHolding => filteredHolding.NetValue);

                                capitalReportDS.AddProductDetailRow(product, endUnitPrice, asxBroker.AsxCode, endHoldingUnits, endHoldingUnits,
                                                                       startHoldingUnits, latestHoldingValue, startHoldingValue, endHoldingValue, model.ServiceType.ToString());

                                presentHoldingTotal += latestHoldingValue;
                                endHoldingTotal += endHoldingValue;
                                startHoldingTotal += startHoldingValue;
                            }
                        }

                        capitalReportDS.AddCapitalRow(asset.Name, model.ServiceType.ToString(), product.Name, product.TaskDescription, desktopBrokerAccountCM.DesktopBrokerAccountEntity.AccountNumber 
                                                                                                ,presentHoldingTotal,endHoldingTotal,startHoldingTotal, product.ID.ToString(), string.Empty);
                    }
                    else if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.ManagedInvestmentSchemesAccount)
                    {
                        ManagedInvestmentSchemesAccountCM managedInvestmentSchemesAccountCM = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as ManagedInvestmentSchemesAccountCM;
                        var linkedMISAccount = managedInvestmentSchemesAccountCM.ManagedInvestmentSchemesAccountEntity.FundAccounts.Where(fa => product.TaskDescription.Contains(fa.Code)).FirstOrDefault();

                        var filteredHoldingTransactionsByStartDate = linkedMISAccount.FundTransactions.Where(cliFundTrans => cliFundTrans.TradeDate <= capitalReportDS.StartDate);
                        var filteredHoldingTransactionsByEndDate = linkedMISAccount.FundTransactions.Where(cliFundTrans => cliFundTrans.TradeDate <= capitalReportDS.EndDate);

                        var clientFundTransactionsStart = filteredHoldingTransactionsByStartDate.Where(cliFundTrans => cliFundTrans.ClientID == clientID);
                        decimal totalSharesStart = clientFundTransactionsStart.Sum(ft => ft.Shares);

                        var clientFundTransactionsEnd = filteredHoldingTransactionsByEndDate.Where(cliFundTrans => cliFundTrans.ClientID == clientID);
                        decimal totalSharesEnd = clientFundTransactionsEnd.Sum(ft => ft.Shares);

                        var linkedmissec = orgCM.Securities.Where(missec => missec.AsxCode == linkedMISAccount.Code).FirstOrDefault();

                        decimal currentValue = 0;
                        decimal startValue = 0;
                        decimal endValue = 0;

                        if (linkedmissec != null)
                        {   
                            decimal startUnitPrice = 0;
                            decimal endUnitPrice = 0;

                            var latestMisPriceObjectStart = linkedmissec.ASXSecurity.Where(mishis => mishis.Date <= capitalReportDS.StartDate).OrderByDescending(mishis => mishis.Date).FirstOrDefault();
                            var latestMisPriceObjectEnd = linkedmissec.ASXSecurity.Where(mishis => mishis.Date <= capitalReportDS.EndDate).OrderByDescending(mishis => mishis.Date).FirstOrDefault();

                            if (latestMisPriceObjectStart != null)
                                startUnitPrice = Convert.ToDecimal(latestMisPriceObjectStart.UnitPrice);

                            if (latestMisPriceObjectEnd != null)
                                endUnitPrice = Convert.ToDecimal(latestMisPriceObjectEnd.UnitPrice);

                            startValue = totalSharesStart * startUnitPrice;
                            currentValue = totalSharesEnd * endUnitPrice;

                            endValue = clientFundTransactionsEnd.Sum(tran => tran.Amount);

                            capitalReportDS.AddProductDetailRow(product, endUnitPrice, linkedmissec.AsxCode, totalSharesEnd, totalSharesEnd, totalSharesStart,
                                                                                                currentValue, startValue, endValue, model.ServiceType.ToString());
                        }

                        capitalReportDS.AddCapitalRow(asset.Name, model.ServiceType.ToString(), product.Name, product.TaskDescription, clientID,currentValue,endValue, startValue, product.ID.ToString(), string.Empty);
                    }
                }
            }
            
            GetBankAccountDataSet(orgCM, Entity.ListAccountProcess, Broker, data);
            GetASXTransactionDS(orgCM, Entity.ListAccountProcess, Broker, data);
            Broker.ReleaseBrokerManagedComponent(orgCM);
        }

        private DataTable ExtractCapitalSummaryTableByCategoryInd(ICMBroker Broker, ClientIndividualEntity Entity, CapitalReportDS capitalReportDS, string ClientID, IOrganization orgCM)
        {
            DataTable CapitalFlowSummaryReport = capitalReportDS.Tables[CapitalReportDS.CAPITALFLOWSUMMARYCAT];
            
            int nMonthDiff_FirstToFirst = capitalReportDS.EndDate.Month - capitalReportDS.StartDate.Month + ((capitalReportDS.EndDate.Year - capitalReportDS.StartDate.Year) * 12);
            double dMonthDiff = (double)nMonthDiff_FirstToFirst + (capitalReportDS.EndDate - capitalReportDS.StartDate.AddMonths(nMonthDiff_FirstToFirst)).TotalDays / (double)DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month);

            for (int monthCounter = 0; monthCounter <= dMonthDiff; monthCounter++)
            {
                CapitalMovementByCategoryTotal capitalMovementByCategoryTotal = new CapitalMovementByCategoryTotal();
            
                DateTime startdate = FirstDayOfMonthFromDateTime(capitalReportDS.StartDate.AddMonths(monthCounter));
                DateTime endDate = LastDayOfMonthFromDateTime(capitalReportDS.StartDate.AddMonths(monthCounter));

                ManualTransactions(ClientID, Entity.ClientManualAssetCollection, orgCM, capitalReportDS, capitalMovementByCategoryTotal, startdate, endDate);
                GetTransactionsInfoFromLinkProcess(ClientID, Broker, Entity.ListAccountProcess, orgCM, capitalReportDS, capitalMovementByCategoryTotal, startdate, endDate);

                foreach(Oritax.TaxSimp.Common.IdentityCMDetail desktopBrokerAccounts in Entity.DesktopBrokerAccounts)
                {
                    DesktopBrokerAccountCM desktopBrokerAccountCM = Broker.GetCMImplementation(desktopBrokerAccounts.Clid,
                                                                        desktopBrokerAccounts.Csid) as DesktopBrokerAccountCM;
                    var filteredTransactionsByMonth = desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Where(tran => tran.TradeDate <= endDate && tran.TradeDate >= startdate);

                    foreach (HoldingTransactions holdingTransactions in filteredTransactionsByMonth)
                    {
                    
                        if(holdingTransactions.Type == "RJ" || holdingTransactions.Type == "AJ")
                        {
                            decimal unitprice = 0; 

                            var unitPriceObj = orgCM.Securities.Where(sec => sec.AsxCode == holdingTransactions.InvestmentCode).FirstOrDefault().ASXSecurity.Where(his => his.Date <= holdingTransactions.TradeDate).OrderByDescending(his => his.Date).FirstOrDefault();

                            if (unitPriceObj != null)
                                unitprice = Convert.ToDecimal(unitPriceObj.UnitPrice);

                            holdingTransactions.NetValue = holdingTransactions.Units * unitprice;

                            if (holdingTransactions.Type == "AJ")
                                holdingTransactions.NetValue = holdingTransactions.NetValue * -1; 
                        }
                    }

                    decimal transferInASX = filteredTransactionsByMonth.Where(tran => tran.Type == "AT").Sum(tran => tran.NetValue);
                    decimal transferOutASX = filteredTransactionsByMonth.Where(tran => tran.Type == "RT").Sum(tran => tran.NetValue);
                    decimal adjustmentUp = filteredTransactionsByMonth.Where(tran => tran.Type == "RJ").Sum(tran => tran.NetValue);
                    decimal adjustmentDown = filteredTransactionsByMonth.Where(tran => tran.Type == "AJ").Sum(tran => tran.NetValue);
                    decimal sell = filteredTransactionsByMonth.Where(tran => tran.Type == "RA").Sum(tran => tran.NetValue);
                    decimal buyPurchase = filteredTransactionsByMonth.Where(tran => tran.Type == "AN").Sum(tran => tran.NetValue);

                    capitalMovementByCategoryTotal.TransferInOutTotal += transferInASX;
                    capitalMovementByCategoryTotal.TransferInOutTotal += transferOutASX;
                    capitalMovementByCategoryTotal.TransferInOutTotal += adjustmentUp;
                    capitalMovementByCategoryTotal.TransferInOutTotal += adjustmentDown;
                    capitalMovementByCategoryTotal.ApplicationRedTotal += (sell*-1);
                    capitalMovementByCategoryTotal.ApplicationRedTotal += buyPurchase;
                }

                DataRow row = CapitalFlowSummaryReport.NewRow();

                row[CapitalReportDS.MONTH] = startdate;
                row[CapitalReportDS.OPENINGBAL] = capitalMovementByCategoryTotal.OpeningBalTotal;
                row[CapitalReportDS.TRANSFEROUT] = capitalMovementByCategoryTotal.TransferInOutTotal;
                row[CapitalReportDS.INCOME] = capitalMovementByCategoryTotal.IncomeTotal;
                row[CapitalReportDS.APPLICATIONREDEMPTION] = capitalMovementByCategoryTotal.ApplicationRedTotal;
                row[CapitalReportDS.TAXINOUT] = capitalMovementByCategoryTotal.TaxInOutTotal;
                row[CapitalReportDS.EXPENSE] = capitalMovementByCategoryTotal.ExpenseTotal;
                row[CapitalReportDS.INTERNALCASHMOVEMENT] = capitalMovementByCategoryTotal.InternalCashMovementTotal;
                row[CapitalReportDS.CLOSINGBALANCE] = capitalMovementByCategoryTotal.ClosingBalanceTotal;
                row[CapitalReportDS.CHANGEININVESTMENTVALUE] = capitalMovementByCategoryTotal.ChangeInInvestTotal;
                CapitalFlowSummaryReport.Rows.Add(row);

               }

            return CapitalFlowSummaryReport;
        }

        public void OnGetDataCapitalFlowDataSetCorporate(CapitalReportDS data, string clientType, string clientID, string clientName, IdentityCM Parent, ICMBroker Broker, CorporateEntity Entity)
        {

            DataTable clientSummaryTable = data.Tables[CapitalReportDS.CLIENTSUMMARYTABLE];
            DataTable contactTable = data.Tables[CapitalReportDS.CONTACTTABLE];

            DataRow clientSummaryRow = clientSummaryTable.NewRow();
            DataRow contactRow = contactTable.NewRow();

            clientSummaryRow[CapitalReportDS.CLIENTID] = clientID;
            clientSummaryRow[CapitalReportDS.CLIENTNAME] = clientName;
            clientSummaryRow[CapitalReportDS.CIENTABN] = Entity.ABN;
            clientSummaryRow[CapitalReportDS.CLIENTACN] = Entity.ACN;
            clientSummaryRow[CapitalReportDS.CLIENTTYPE] = clientType;

            if (Parent != null && Parent.Clid != Guid.Empty && Parent.Csid != Guid.Empty)
            {
                AdvisorCM adviserUnit = Broker.GetCMImplementation(Parent.Clid, Parent.Csid) as AdvisorCM;
                clientSummaryRow[HoldingRptDataSet.ADVISERFULLNAME] = adviserUnit.AdvisorEntity.FullName;
                Broker.ReleaseBrokerManagedComponent(adviserUnit);
            }

            clientSummaryRow[CapitalReportDS.ADDRESSLINE1] = Entity.Address.BusinessAddress.Addressline1;
            clientSummaryRow[CapitalReportDS.ADDRESSLINE2] = Entity.Address.BusinessAddress.Addressline2;
            clientSummaryRow[CapitalReportDS.SUBURB] = Entity.Address.BusinessAddress.Suburb;
            clientSummaryRow[CapitalReportDS.STATE] = Entity.Address.BusinessAddress.State;
            clientSummaryRow[CapitalReportDS.POSTCODE] = Entity.Address.BusinessAddress.PostCode;
            clientSummaryRow[CapitalReportDS.COUNTRY] = Entity.Address.BusinessAddress.Country;

            CSVInstance csvinstance = new CSVInstance(Broker);
            CorporateCSV _entitycsv = csvinstance.GetEntityConcreteInstance(Entity);
            foreach (var contact in _entitycsv.Contacts)
            {
                contactRow[CapitalReportDS.CONTACT_TITLE] = contact.PersonalTitle;
                contactRow[CapitalReportDS.CONTACT_NAME] = contact.Fullname;
                contactRow[CapitalReportDS.CONTACT_BIZNO] = contact.WorkPhoneNumber;
                contactRow[CapitalReportDS.CONTACT_MOBNO] = contact.MobilePhoneNumber;
                contactRow[CapitalReportDS.CONTACT_HNO] = contact.HomePhoneNumber;
                contactRow[CapitalReportDS.CONTACT_FAXNO] = contact.Facsimile;
                contactRow[CapitalReportDS.CONTACT_EMAIL] = contact.EmailAddress;
            }

            clientSummaryTable.Rows.Add(clientSummaryRow);
            contactTable.Rows.Add(contactRow);

            IOrganization orgCM = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            CapitalReportDS capitalReportDS = data as CapitalReportDS;

            DataTable CapitalSummaryTable = capitalReportDS.Tables[CapitalReportDS.CAPITALFLOWSUMMARY];
            DataTable productBreakDownTable = capitalReportDS.Tables[CapitalReportDS.PRODUCTBREAKDOWNTABLE];
            DataTable TDBreakDownTable = capitalReportDS.Tables[CapitalReportDS.TDBREAKDOWNTABLE];

            DataTable CapitalFlowSummaryReport = ExtractCapitalSummaryTableByCategory(Broker, Entity, capitalReportDS, clientID, orgCM);

            if (Entity.ClientManualAssetCollection != null)
            {
                var groupedManualTransactions = Entity.ClientManualAssetCollection.GroupBy(man => man.InvestmentCode);

                foreach (var groupedManualTransaction in groupedManualTransactions)
                {
                    var systemManualAsset = orgCM.ManualAsset.Where(manList => manList.ID == groupedManualTransaction.Key).FirstOrDefault();

                    decimal holdingLatest = 0;
                    decimal holdingEndDate = 0;
                    decimal holdingStartDate = 0;
                    decimal unitholdingEndDate = 0;
                    decimal unitholdingStartDate = 0;

                    double unitPriceLatest = systemManualAsset.ASXSecurity.OrderByDescending(manualHis => manualHis.Date).FirstOrDefault().UnitPrice;
                    double unitPriceStartDate = systemManualAsset.ASXSecurity.OrderByDescending(manualHis => manualHis.Date).FirstOrDefault().UnitPrice;

                    unitholdingStartDate = groupedManualTransaction.Where(manTran => manTran.TransactionDate <= capitalReportDS.StartDate).Sum(manTran => manTran.UnitsOnHand);
                    unitholdingEndDate = groupedManualTransaction.Where(manTran => manTran.TransactionDate <= capitalReportDS.EndDate).Sum(manTran => manTran.UnitsOnHand);

                    //Calculate Holding based on unit price for each transactiondate
                    foreach (var manualTransaction in groupedManualTransaction)
                    {
                        decimal holdingPerTransaction = 0;
                        double unitPrice = 0;

                        var manualHistory = systemManualAsset.ASXSecurity.Where(manualHis => manualHis.Date <= manualTransaction.TransactionDate).OrderByDescending(manualHis => manualHis.Date).FirstOrDefault();
                        if (manualHistory != null)
                            unitPrice = manualHistory.UnitPrice;

                        holdingPerTransaction = manualTransaction.UnitsOnHand * Convert.ToDecimal(unitPrice);
                        holdingEndDate += holdingPerTransaction;
                    }

                    holdingStartDate = unitholdingStartDate * Convert.ToDecimal(unitPriceStartDate);
                    holdingLatest = unitholdingEndDate * Convert.ToDecimal(unitPriceLatest);

                    capitalReportDS.AddCapitalRow("ManualAsset", "ManualAsset", systemManualAsset.AsxCode, systemManualAsset.CompanyName, clientID, holdingLatest, holdingEndDate, holdingStartDate, string.Empty, string.Empty);
                }
            }

            foreach (var accountProcessTaskEntity in Entity.ListAccountProcess)
            {
                if (accountProcessTaskEntity.LinkedEntity != null && accountProcessTaskEntity.LinkedEntity.Clid != Guid.Empty)
                {
                    var asset = orgCM.Assets.Where(var => var.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                    var model = orgCM.Model.Where(mod => mod.ID == accountProcessTaskEntity.ModelID).FirstOrDefault();
                    var linkedAsset = model.Assets.Where(ast => ast.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                    var product = linkedAsset.Products.Where(pro => pro.ID == accountProcessTaskEntity.TaskID).FirstOrDefault();

                    if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.BankAccount)
                    {
                        BankAccountCM bankAccount = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as BankAccountCM;
                        var filteredTransactionsStart = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate <= capitalReportDS.StartDate);
                        decimal holdingTotalStart = filteredTransactionsStart.Sum(calc => calc.TotalAmount);

                        var filteredTransactionsEnd = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate <= capitalReportDS.EndDate);
                        decimal holdingTotalEnd = filteredTransactionsEnd.Sum(calc => calc.TotalAmount);

                        capitalReportDS.AddCapitalRow(asset.Name, model.ServiceType.ToString(), product.Name,
                                                            product.TaskDescription, bankAccount.BankAccountEntity.AccountNumber, holdingTotalEnd, holdingTotalEnd, holdingTotalStart, string.Empty, string.Empty);
                    }
                    else if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.TermDepositAccount)
                    {
                        BankAccountCM bankAccount = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as BankAccountCM;
                        decimal holdingTotalStart = 0;
                        decimal holdingTotalEnd = 0;

                        var filteredTransactions = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate <= capitalReportDS.StartDate);
                        var tdGroupsType = filteredTransactions.GroupBy(cashMan => cashMan.TransactionType);

                        foreach (var tdType in tdGroupsType)
                        {
                            var groupByIns = tdType.GroupBy(tdtypetran => tdtypetran.InstitutionID);

                            foreach (var groupins in groupByIns)
                            {
                                var transaction = groupins.FirstOrDefault();
                                decimal holding = groupins.Sum(inssum => inssum.Amount);

                                DataRow tDBreakDownTableRow = TDBreakDownTable.NewRow();
                                capitalReportDS.TDBreakDownRow(orgCM.Institution.Where(ins => ins.ID == groupins.Key).FirstOrDefault().Name, model.ServiceType.ToString(), tdType.Key, transaction.AdministrationSystem, holding, bankAccount.BankAccountEntity.InstitutionID.ToString(), tDBreakDownTableRow);
                                TDBreakDownTable.Rows.Add(tDBreakDownTableRow);

                                holdingTotalStart += holding;
                            }
                        }

                        filteredTransactions = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate <= capitalReportDS.EndDate);
                        tdGroupsType = filteredTransactions.GroupBy(cashMan => cashMan.TransactionType);

                        foreach (var tdType in tdGroupsType)
                        {
                            var groupByIns = tdType.GroupBy(tdtypetran => tdtypetran.InstitutionID);

                            foreach (var groupins in groupByIns)
                            {
                                var transaction = groupins.FirstOrDefault();
                                decimal holding = groupins.Sum(inssum => inssum.Amount);

                                DataRow tDBreakDownTableRow = TDBreakDownTable.NewRow();
                                capitalReportDS.TDBreakDownRow(orgCM.Institution.Where(ins => ins.ID == groupins.Key).FirstOrDefault().Name, model.ServiceType.ToString(), tdType.Key, transaction.AdministrationSystem, holding, bankAccount.BankAccountEntity.InstitutionID.ToString(), tDBreakDownTableRow);
                                TDBreakDownTable.Rows.Add(tDBreakDownTableRow);

                                holdingTotalEnd += holding;
                            }
                        }

                        capitalReportDS.AddCapitalRow(asset.Name, model.ServiceType.ToString(), product.Name,
                                                            product.TaskDescription, bankAccount.BankAccountEntity.AccountNumber, holdingTotalEnd, holdingTotalEnd, holdingTotalStart, string.Empty, bankAccount.BankAccountEntity.InstitutionID.ToString());
                    }
                    else if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.DesktopBrokerAccount)
                    {
                        DesktopBrokerAccountCM desktopBrokerAccountCM = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid,
                                                                                    accountProcessTaskEntity.LinkedEntity.Csid) as DesktopBrokerAccountCM;
                        decimal presentHoldingTotal = 0;
                        decimal endHoldingTotal = 0;
                        decimal startHoldingTotal = 0;

                        //If it is linked with product securities for split type calculation
                        if (product.ProductSecuritiesId != Guid.Empty)
                        {
                            var productSecurity = orgCM.ProductSecurities.Where(proseclist => proseclist.ID == product.ProductSecuritiesId).FirstOrDefault();
                            var linkedAssets = model.Assets.Where(ast => ast.ID == accountProcessTaskEntity.AssetID && accountProcessTaskEntity.ModelID == model.ID);
                            double totalPercentage = HoldingRptDataSet.CalculateTotalPercentageForSplit(linkedAssets);

                            foreach (var productSecurityDetail in productSecurity.Details)
                            {
                                var asxBroker = orgCM.Securities.Where(sec => sec.ID == productSecurityDetail.SecurityCodeId).FirstOrDefault();
                                var filteredHoldingTransactionsByStartDate = desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Where(holdTrans =>
                                                                                                              holdTrans.TradeDate <= capitalReportDS.StartDate);
                                var filteredHoldingTransactionsStartDate = filteredHoldingTransactionsByStartDate.Where(holdTrans =>
                                                                                                              holdTrans.InvestmentCode == asxBroker.AsxCode);

                                var filteredHoldingTransactionsByEndDate = desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Where(holdTrans =>
                                                                                                              holdTrans.TradeDate <= capitalReportDS.EndDate);
                                var filteredHoldingTransactionsEndDate = filteredHoldingTransactionsByEndDate.Where(holdTrans =>
                                                                                                              holdTrans.InvestmentCode == asxBroker.AsxCode);

                                var count = filteredHoldingTransactionsByEndDate.Count();

                                if (count > 0)
                                {
                                    totalPercentage = product.SharePercentage;

                                    foreach (var linkedasst in linkedAssets)
                                    {
                                        var otherProducts = linkedasst.Products.Where(linkedAsstPro => linkedAsstPro.ProductSecuritiesId != productSecurity.ID);
                                        foreach (var OtherproductSecurity in otherProducts)
                                        {
                                            var OtherproductSecurityDetails = orgCM.ProductSecurities.Where(otherpro => otherpro.ID == OtherproductSecurity.ProductSecuritiesId).FirstOrDefault();

                                            if (OtherproductSecurityDetails != null)
                                            {
                                                var otherAsxBroker = OtherproductSecurityDetails.Details.Where(otherProDetail => otherProDetail.SecurityCodeId == asxBroker.ID);

                                                if (otherAsxBroker.Count() > 0)
                                                    totalPercentage += OtherproductSecurity.SharePercentage;
                                            }
                                        }
                                    }

                                    decimal unitPriceStart = 0;
                                    decimal unitPriceEnd = 0;

                                    var hisSecurityStart = asxBroker.ASXSecurity.Where(his => his.Date <= capitalReportDS.StartDate).OrderByDescending(secpri => secpri.Date).FirstOrDefault();
                                    decimal startHoldingUnits = filteredHoldingTransactionsStartDate.Sum(filteredHolding => filteredHolding.Units);

                                    if (hisSecurityStart != null)
                                        unitPriceStart = Convert.ToDecimal(hisSecurityStart.UnitPrice);

                                    decimal startHoldingValue = startHoldingUnits * unitPriceStart;

                                    var hisSecurityLatest = asxBroker.ASXSecurity.Where(his => his.Date <= capitalReportDS.EndDate).OrderByDescending(secpri => secpri.Date).FirstOrDefault();
                                    decimal endHoldingUnits = filteredHoldingTransactionsEndDate.Sum(filteredHolding => filteredHolding.Units);

                                    if (hisSecurityLatest != null)
                                        unitPriceEnd = Convert.ToDecimal(hisSecurityLatest.UnitPrice);

                                    decimal latestHoldingValue = endHoldingUnits * unitPriceEnd;

                                    decimal endHoldingValue = filteredHoldingTransactionsEndDate.Sum(filteredHolding => filteredHolding.NetValue);

                                    startHoldingValue = startHoldingValue * Convert.ToDecimal((product.SharePercentage / totalPercentage));
                                    latestHoldingValue = latestHoldingValue * Convert.ToDecimal((product.SharePercentage / totalPercentage));
                                    endHoldingValue = endHoldingValue * Convert.ToDecimal((product.SharePercentage / totalPercentage));

                                    capitalReportDS.AddProductDetailRow(product, unitPriceEnd, asxBroker.AsxCode, endHoldingUnits, endHoldingUnits,
                                                                           startHoldingUnits, latestHoldingValue, startHoldingValue, endHoldingValue, model.ServiceType.ToString());

                                    presentHoldingTotal += latestHoldingValue;
                                    endHoldingTotal += endHoldingValue;
                                    startHoldingTotal += startHoldingValue;
                                }
                            }
                        }
                        else
                        {
                            var filteredHoldingTransactionsByStartDate = desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Where(holdTrans =>
                                                                                                              holdTrans.TradeDate <= capitalReportDS.StartDate);

                            var filteredHoldingTransactionsByEndDate = desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Where(holdTrans =>
                                                                                  holdTrans.TradeDate <= capitalReportDS.EndDate);

                            var groupedSecurities = filteredHoldingTransactionsByEndDate.GroupBy(holdTran => holdTran.InvestmentCode);

                            foreach (var security in groupedSecurities)
                            {
                                decimal totalSecHoldingUnits = security.Sum(sec => sec.Units);
                                var asxBroker = orgCM.Securities.Where(sec => sec.AsxCode == security.Key).FirstOrDefault();


                                var hisSecurityStart = asxBroker.ASXSecurity.Where(his => his.Date <= capitalReportDS.StartDate).OrderByDescending(secpri => secpri.Date).FirstOrDefault();
                                decimal startHoldingUnits = filteredHoldingTransactionsByStartDate.Sum(filteredHolding => filteredHolding.Units);

                                decimal startUnitPrice = 0;
                                decimal endUnitPrice = 0;

                                if (hisSecurityStart != null)
                                    startUnitPrice = Convert.ToDecimal(hisSecurityStart.UnitPrice);

                                decimal startHoldingValue = startHoldingUnits * startUnitPrice;

                                var hisSecurityLatest = asxBroker.ASXSecurity.Where(his => his.Date <= capitalReportDS.EndDate).OrderByDescending(secpri => secpri.Date).FirstOrDefault();

                                if (hisSecurityLatest != null)
                                    endUnitPrice = Convert.ToDecimal(hisSecurityLatest.UnitPrice);

                                decimal endHoldingUnits = filteredHoldingTransactionsByEndDate.Where(tran => tran.InvestmentCode == security.Key).Sum(filteredHolding => filteredHolding.Units);
                                decimal latestHoldingValue = endHoldingUnits * endUnitPrice;

                                decimal endHoldingValue = filteredHoldingTransactionsByEndDate.Where(tran => tran.InvestmentCode == security.Key).Sum(filteredHolding => filteredHolding.NetValue);

                                capitalReportDS.AddProductDetailRow(product, endUnitPrice, asxBroker.AsxCode, endHoldingUnits, endHoldingUnits,
                                                                       startHoldingUnits, latestHoldingValue, startHoldingValue, endHoldingValue, model.ServiceType.ToString());

                                presentHoldingTotal += latestHoldingValue;
                                endHoldingTotal += endHoldingValue;
                                startHoldingTotal += startHoldingValue;
                            }
                        }

                        capitalReportDS.AddCapitalRow(asset.Name, model.ServiceType.ToString(), product.Name, product.TaskDescription, desktopBrokerAccountCM.DesktopBrokerAccountEntity.AccountNumber
                                                                                                , presentHoldingTotal, endHoldingTotal, startHoldingTotal, product.ID.ToString(), string.Empty);
                    }
                    else if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.ManagedInvestmentSchemesAccount)
                    {
                        ManagedInvestmentSchemesAccountCM managedInvestmentSchemesAccountCM = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as ManagedInvestmentSchemesAccountCM;
                        var linkedMISAccount = managedInvestmentSchemesAccountCM.ManagedInvestmentSchemesAccountEntity.FundAccounts.Where(fa => product.TaskDescription.Contains(fa.Code)).FirstOrDefault();

                        var filteredHoldingTransactionsByStartDate = linkedMISAccount.FundTransactions.Where(cliFundTrans => cliFundTrans.TradeDate <= capitalReportDS.StartDate);
                        var filteredHoldingTransactionsByEndDate = linkedMISAccount.FundTransactions.Where(cliFundTrans => cliFundTrans.TradeDate <= capitalReportDS.EndDate);

                        var clientFundTransactionsStart = filteredHoldingTransactionsByStartDate.Where(cliFundTrans => cliFundTrans.ClientID == clientID);
                        decimal totalSharesStart = clientFundTransactionsStart.Sum(ft => ft.Shares);

                        var clientFundTransactionsEnd = filteredHoldingTransactionsByEndDate.Where(cliFundTrans => cliFundTrans.ClientID == clientID);
                        decimal totalSharesEnd = clientFundTransactionsEnd.Sum(ft => ft.Shares);

                        var linkedmissec = orgCM.Securities.Where(missec => missec.AsxCode == linkedMISAccount.Code).FirstOrDefault();

                        decimal currentValue = 0;
                        decimal startValue = 0;
                        decimal endValue = 0;

                        if (linkedmissec != null)
                        {
                            decimal startUnitPrice = 0;
                            decimal endUnitPrice = 0;

                            var latestMisPriceObjectStart = linkedmissec.ASXSecurity.Where(mishis => mishis.Date <= capitalReportDS.StartDate).OrderByDescending(mishis => mishis.Date).FirstOrDefault();
                            var latestMisPriceObjectEnd = linkedmissec.ASXSecurity.Where(mishis => mishis.Date <= capitalReportDS.EndDate).OrderByDescending(mishis => mishis.Date).FirstOrDefault();

                            if (latestMisPriceObjectStart != null)
                                startUnitPrice = Convert.ToDecimal(latestMisPriceObjectStart.UnitPrice);

                            if (latestMisPriceObjectEnd != null)
                                endUnitPrice = Convert.ToDecimal(latestMisPriceObjectEnd.UnitPrice);

                            startValue = totalSharesStart * startUnitPrice;
                            currentValue = totalSharesEnd * endUnitPrice;

                            endValue = clientFundTransactionsEnd.Sum(tran => tran.Amount);

                            capitalReportDS.AddProductDetailRow(product, endUnitPrice, linkedmissec.AsxCode, totalSharesEnd, totalSharesEnd, totalSharesStart,
                                                                                                currentValue, startValue, endValue, model.ServiceType.ToString());
                        }

                        capitalReportDS.AddCapitalRow(asset.Name, model.ServiceType.ToString(), product.Name, product.TaskDescription, clientID, currentValue, endValue, startValue, product.ID.ToString(), string.Empty);
                    }
                }
            }

            GetBankAccountDataSet(orgCM, Entity.ListAccountProcess, Broker, data);
            GetASXTransactionDS(orgCM, Entity.ListAccountProcess, Broker, data);
            Broker.ReleaseBrokerManagedComponent(orgCM);
        }

        public void GetBankAccountDataSet(IOrganization orgCM, List<AccountProcessTaskEntity> ListAccountProcess, ICMBroker Broker, CapitalReportDS data)
        {
            foreach (var accountProcessTaskEntity in ListAccountProcess)
            {
                if (accountProcessTaskEntity.LinkedEntity != null && accountProcessTaskEntity.LinkedEntity.Clid != Guid.Empty)
                {
                    var asset = orgCM.Assets.Where(var => var.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                    var model = orgCM.Model.Where(mod => mod.ID == accountProcessTaskEntity.ModelID).FirstOrDefault();
                    var linkedAsset = model.Assets.Where(ast => ast.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                    var product = linkedAsset.Products.Where(pro => pro.ID == accountProcessTaskEntity.TaskID).FirstOrDefault();

                    if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.BankAccount)
                    {
                        BankAccountCM bankAccount = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as BankAccountCM;

                        if (bankAccount.BankAccountEntity.CashManagementTransactions.Count > 0)
                        {

                            var filteredTransactions = bankAccount.BankAccountEntity.CashManagementTransactions.Where(tran => tran.TransactionDate <= data.EndDate && tran.TransactionDate >= data.StartDate);

                            DataTable dt = data.Tables[CapitalReportDS.BANKTRANSACTIONSTABLE];

                            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashManagementEntity in filteredTransactions )
                            {
                                DataRow row = dt.NewRow(); 

                                row[CapitalReportDS.BSB] = bankAccount.BankAccountEntity.BSB;
                                row[CapitalReportDS.ACCOUNTTYPE] = bankAccount.BankAccountEntity.AccoutType.ToUpper();
                                row[CapitalReportDS.ACCOUNTNO] = bankAccount.BankAccountEntity.AccountNumber;
                                row[CapitalReportDS.ACCOUNTNAME] = bankAccount.BankAccountEntity.Name;
                                row[CapitalReportDS.AMOUNTTOTAL] = cashManagementEntity.TotalAmount;
                                row[CapitalReportDS.BANKAMOUNT] = cashManagementEntity.Amount;
                                row[CapitalReportDS.BANKADJUSTMENT] = cashManagementEntity.Adjustment;
                                row[CapitalReportDS.BANKTRANSDATE] = cashManagementEntity.TransactionDate;

                                row[CapitalReportDS.IMPTRANTYPE] = cashManagementEntity.ImportTransactionType;
                                row[CapitalReportDS.SYSTRANTYPE] = cashManagementEntity.SystemTransactionType;
                                row[CapitalReportDS.CATEGORY] = cashManagementEntity.Category;
                                row[CapitalReportDS.COMMENT] = cashManagementEntity.Comment;

                                dt.Rows.Add(row);
                            }

                            data.Merge(dt, false, MissingSchemaAction.Add);
                        }
                    }
                }
            }
        }

        public void GetASXTransactionDS(IOrganization orgCM, List<AccountProcessTaskEntity> ListAccountProcess, ICMBroker Broker, CapitalReportDS data)
        {
            foreach (var accountProcessTaskEntity in ListAccountProcess)
            {
                if (accountProcessTaskEntity.LinkedEntity != null && accountProcessTaskEntity.LinkedEntity.Clid != Guid.Empty)
                {
                    var asset = orgCM.Assets.Where(var => var.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                    var model = orgCM.Model.Where(mod => mod.ID == accountProcessTaskEntity.ModelID).FirstOrDefault();
                    var linkedAsset = model.Assets.Where(ast => ast.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                    var product = linkedAsset.Products.Where(pro => pro.ID == accountProcessTaskEntity.TaskID).FirstOrDefault();

                    if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.DesktopBrokerAccount)
                    {
                        DesktopBrokerAccountCM desktopBrokerAccountCM = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as DesktopBrokerAccountCM;

                        if (desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Count > 0)
                        {

                            DataTable dt = data.Tables[CapitalReportDS.ASXTRANSTABLE];

                            var filteredTransactions = desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Where(tran => tran.TradeDate <= data.EndDate && tran.TradeDate >= data.StartDate);

                            foreach (HoldingTransactions holdingTransactions in desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions)
                            {
                                DataRow row = dt.NewRow();

                                row[CapitalReportDS.NAME] = desktopBrokerAccountCM.DesktopBrokerAccountEntity.Name;
                                row[CapitalReportDS.ACCOUNTNO] = desktopBrokerAccountCM.DesktopBrokerAccountEntity.AccountNumber;
                                row[CapitalReportDS.FPSID] = holdingTransactions.FPSInstructionID;
                                row[CapitalReportDS.INVESMENTCODE] = holdingTransactions.InvestmentCode;
                                row[CapitalReportDS.TRANSACTIONTYPE] = holdingTransactions.TransactionType;
                                row[CapitalReportDS.TRADEDATE] = holdingTransactions.TradeDate;

                                if(holdingTransactions.SettlementDate != null)
                                    row[CapitalReportDS.SETTLEDATE] = holdingTransactions.SettlementDate;
                                else
                                    row[CapitalReportDS.SETTLEDATE] = holdingTransactions.TradeDate;
                                
                                row[CapitalReportDS.UNITS] = holdingTransactions.Units;
                                row[CapitalReportDS.GROSSVALUE] = holdingTransactions.GrossValue;
                                row[CapitalReportDS.BROKERAGEAMOUNT] = holdingTransactions.BrokerageAmount;
                                row[CapitalReportDS.CHARGES] = holdingTransactions.Charges;
                                row[CapitalReportDS.BROKERGST] = holdingTransactions.BrokerageGST;
                                row[CapitalReportDS.BROKERAGEAMOUNT] = holdingTransactions.BrokerageAmount;
                                row[CapitalReportDS.NETVALUE] = holdingTransactions.NetValue;
                                row[CapitalReportDS.NARRATIVE] = holdingTransactions.Narrative;
                                
                                dt.Rows.Add(row);
                            }

                            data.Merge(dt, false, MissingSchemaAction.Add);
                        }
                    }
                }
            }
        }

        private DataTable ExtractCapitalSummaryTableByCategory(ICMBroker Broker, CorporateEntity Entity, CapitalReportDS capitalReportDS, string ClientID, IOrganization orgCM)
        {
            DataTable CapitalFlowSummaryReport = capitalReportDS.Tables[CapitalReportDS.CAPITALFLOWSUMMARYCAT];
            
            int nMonthDiff_FirstToFirst = capitalReportDS.EndDate.Month - capitalReportDS.StartDate.Month + ((capitalReportDS.EndDate.Year - capitalReportDS.StartDate.Year) * 12);
            double dMonthDiff = (double)nMonthDiff_FirstToFirst + (capitalReportDS.EndDate - capitalReportDS.StartDate.AddMonths(nMonthDiff_FirstToFirst)).TotalDays / (double)DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month);

            for (int monthCounter = 0; monthCounter <= dMonthDiff; monthCounter++)
            {
                CapitalMovementByCategoryTotal capitalMovementByCategoryTotal = new CapitalMovementByCategoryTotal();
            
                DateTime startdate = FirstDayOfMonthFromDateTime(capitalReportDS.StartDate.AddMonths(monthCounter));
                DateTime endDate = LastDayOfMonthFromDateTime(capitalReportDS.StartDate.AddMonths(monthCounter));

                ManualTransactions(ClientID, Entity.ClientManualAssetCollection, orgCM, capitalReportDS, capitalMovementByCategoryTotal, startdate, endDate);
                GetTransactionsInfoFromLinkProcess(ClientID, Broker, Entity.ListAccountProcess, orgCM, capitalReportDS, capitalMovementByCategoryTotal, startdate, endDate);

                foreach(Oritax.TaxSimp.Common.IdentityCMDetail desktopBrokerAccounts in Entity.DesktopBrokerAccounts)
                {
                    DesktopBrokerAccountCM desktopBrokerAccountCM = Broker.GetCMImplementation(desktopBrokerAccounts.Clid,
                                                                        desktopBrokerAccounts.Csid) as DesktopBrokerAccountCM;
                    var filteredTransactionsByMonth = desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Where(tran => tran.TradeDate <= endDate && tran.TradeDate >= startdate);

                    foreach (HoldingTransactions holdingTransactions in filteredTransactionsByMonth)
                    {
                    
                        if(holdingTransactions.Type == "RJ" || holdingTransactions.Type == "AJ")
                        {
                            decimal unitprice = 0; 

                            var unitPriceObj = orgCM.Securities.Where(sec => sec.AsxCode == holdingTransactions.InvestmentCode).FirstOrDefault().ASXSecurity.Where(his => his.Date <= holdingTransactions.TradeDate).OrderByDescending(his => his.Date).FirstOrDefault();

                            if (unitPriceObj != null)
                                unitprice = Convert.ToDecimal(unitPriceObj.UnitPrice);

                            holdingTransactions.NetValue = holdingTransactions.Units * unitprice;

                            if (holdingTransactions.Type == "AJ")
                                holdingTransactions.NetValue = holdingTransactions.NetValue * -1; 
                        }
                    }

                    decimal transferInASX = filteredTransactionsByMonth.Where(tran => tran.Type == "AT").Sum(tran => tran.NetValue);
                    decimal transferOutASX = filteredTransactionsByMonth.Where(tran => tran.Type == "RT").Sum(tran => tran.NetValue);
                    decimal adjustmentUp = filteredTransactionsByMonth.Where(tran => tran.Type == "RJ").Sum(tran => tran.NetValue);
                    decimal adjustmentDown = filteredTransactionsByMonth.Where(tran => tran.Type == "AJ").Sum(tran => tran.NetValue);
                    decimal sell = filteredTransactionsByMonth.Where(tran => tran.Type == "RA").Sum(tran => tran.NetValue);
                    decimal buyPurchase = filteredTransactionsByMonth.Where(tran => tran.Type == "AN").Sum(tran => tran.NetValue);

                    capitalMovementByCategoryTotal.TransferInOutTotal += transferInASX;
                    capitalMovementByCategoryTotal.TransferInOutTotal += transferOutASX;
                    capitalMovementByCategoryTotal.TransferInOutTotal += adjustmentUp;
                    capitalMovementByCategoryTotal.TransferInOutTotal += adjustmentDown;
                    capitalMovementByCategoryTotal.ApplicationRedTotal += (sell*-1);
                    capitalMovementByCategoryTotal.ApplicationRedTotal += buyPurchase;
                }

                DataRow row = CapitalFlowSummaryReport.NewRow();

                row[CapitalReportDS.MONTH] = startdate;
                row[CapitalReportDS.OPENINGBAL] = capitalMovementByCategoryTotal.OpeningBalTotal;
                row[CapitalReportDS.TRANSFEROUT] = capitalMovementByCategoryTotal.TransferInOutTotal;
                row[CapitalReportDS.INCOME] = capitalMovementByCategoryTotal.IncomeTotal;
                row[CapitalReportDS.APPLICATIONREDEMPTION] = capitalMovementByCategoryTotal.ApplicationRedTotal;
                row[CapitalReportDS.TAXINOUT] = capitalMovementByCategoryTotal.TaxInOutTotal;
                row[CapitalReportDS.EXPENSE] = capitalMovementByCategoryTotal.ExpenseTotal;
                row[CapitalReportDS.INTERNALCASHMOVEMENT] = capitalMovementByCategoryTotal.InternalCashMovementTotal;
                row[CapitalReportDS.CLOSINGBALANCE] = capitalMovementByCategoryTotal.ClosingBalanceTotal;
                row[CapitalReportDS.CHANGEININVESTMENTVALUE] = capitalMovementByCategoryTotal.ChangeInInvestTotal;
                CapitalFlowSummaryReport.Rows.Add(row);

               }

            return CapitalFlowSummaryReport;
        }

        private static void GetTransactionsInfoFromLinkProcess(string clientID, ICMBroker Broker, List<AccountProcessTaskEntity> ListAccountProcess, IOrganization orgCM, CapitalReportDS capitalReportDS, CapitalMovementByCategoryTotal capitalMovementByCategoryTotal, DateTime startDate, DateTime endDate)
        {
            foreach (var accountProcessTaskEntity in ListAccountProcess)
            {
                if (accountProcessTaskEntity.LinkedEntity != null && accountProcessTaskEntity.LinkedEntity.Clid != Guid.Empty)
                {
                    var asset = orgCM.Assets.Where(var => var.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                    var model = orgCM.Model.Where(mod => mod.ID == accountProcessTaskEntity.ModelID).FirstOrDefault();
                    var linkedAsset = model.Assets.Where(ast => ast.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                    var product = linkedAsset.Products.Where(pro => pro.ID == accountProcessTaskEntity.TaskID).FirstOrDefault();

                    if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.BankAccount)
                        BankAccountTransctions(Broker, capitalReportDS, accountProcessTaskEntity, asset, model, product, capitalMovementByCategoryTotal, startDate, endDate);
                    else if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.TermDepositAccount)
                        TDTransactions(Broker, orgCM, capitalReportDS, accountProcessTaskEntity, asset, model, product, capitalMovementByCategoryTotal, startDate, endDate);
                    else if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.DesktopBrokerAccount)
                        DesktopBrokerTransactions(Broker, orgCM, capitalReportDS, accountProcessTaskEntity, asset, model, product, capitalMovementByCategoryTotal, startDate, endDate);
                    else if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.ManagedInvestmentSchemesAccount)
                        ManageInvestmentSchemes(clientID, Broker, orgCM, capitalReportDS, accountProcessTaskEntity, asset, model, product, capitalMovementByCategoryTotal, startDate, endDate);
                }
            }
        }

        private static void ManageInvestmentSchemes(string clientID, ICMBroker Broker, IOrganization orgCM, CapitalReportDS capitalReportDS, AccountProcessTaskEntity accountProcessTaskEntity, AssetEntity asset, ModelEntity model, ProductEntity product, CapitalMovementByCategoryTotal capitalMovementByCategoryTotal, DateTime startDate, DateTime endDate)
        {
            ManagedInvestmentSchemesAccountCM managedInvestmentSchemesAccountCM = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as ManagedInvestmentSchemesAccountCM;
            var linkedMISAccount = managedInvestmentSchemesAccountCM.ManagedInvestmentSchemesAccountEntity.FundAccounts.Where(fa => product.TaskDescription.Contains(fa.Code)).FirstOrDefault();

            var filteredHoldingTransactions = linkedMISAccount.FundTransactions.Where(cliFundTrans => cliFundTrans.TradeDate >= startDate && cliFundTrans.TradeDate <= endDate && cliFundTrans.ClientID == clientID);

            decimal buyPurchase = filteredHoldingTransactions.Where(tran => tran.TransactionType == "Purchase").Sum(tran => tran.Amount);
            decimal sell = filteredHoldingTransactions.Where(tran => tran.TransactionType == "Redemption").Sum(tran => tran.Amount);

            var filteredHoldingTransactionsByOpeningDate = linkedMISAccount.FundTransactions.Where(cliFundTrans => cliFundTrans.TradeDate <= startDate.AddDays(-1));
            var clientFundTransactionsOpening = filteredHoldingTransactionsByOpeningDate.Where(cliFundTrans => cliFundTrans.ClientID == clientID);
            decimal totalSharesOpening = clientFundTransactionsOpening.Sum(ft => ft.Shares);
            var linkedmissec = orgCM.Securities.Where(missec => missec.AsxCode == linkedMISAccount.Code).FirstOrDefault();

            decimal openingBalance = 0;

            if (linkedmissec != null)
            {
                var latestMisPriceObject = linkedmissec.ASXSecurity.Where(mishis => mishis.Date <= startDate.AddDays(-1)).OrderByDescending(mishis => mishis.Date).FirstOrDefault();
                decimal unitPrice = 0;
                if (latestMisPriceObject != null)
                    unitPrice = Convert.ToDecimal(latestMisPriceObject.UnitPrice);
                openingBalance = totalSharesOpening * unitPrice;
            }

            var filteredHoldingTransactionsByClosingDate = linkedMISAccount.FundTransactions.Where(cliFundTrans => cliFundTrans.TradeDate <= endDate);
            var clientFundTransactionsCLosing = filteredHoldingTransactionsByClosingDate.Where(cliFundTrans => cliFundTrans.ClientID == clientID);
            decimal totalSharesClosing = clientFundTransactionsCLosing.Sum(ft => ft.Shares);
          
            decimal closingBalance = 0;

            if (linkedmissec != null)
            {
                var latestMisPriceObject = linkedmissec.ASXSecurity.Where(mishis => mishis.Date <= endDate).OrderByDescending(mishis => mishis.Date).FirstOrDefault();
                decimal unitPrice = 0;
                if (latestMisPriceObject != null)
                    unitPrice = Convert.ToDecimal(latestMisPriceObject.UnitPrice);
                closingBalance = totalSharesClosing * unitPrice;
            }

            capitalMovementByCategoryTotal.OpeningBalTotal += openingBalance;
            capitalMovementByCategoryTotal.ClosingBalanceTotal += closingBalance;
        
            capitalMovementByCategoryTotal.ApplicationRedTotal += buyPurchase;
            capitalMovementByCategoryTotal.ApplicationRedTotal += sell;
        }

        private static void DesktopBrokerTransactions(ICMBroker Broker, IOrganization orgCM, CapitalReportDS capitalReportDS, AccountProcessTaskEntity accountProcessTaskEntity, AssetEntity asset, ModelEntity model, ProductEntity product, CapitalMovementByCategoryTotal capitalMovementByCategoryTotal, DateTime startDate, DateTime endDate)
        {
            DesktopBrokerAccountCM desktopBrokerAccountCM = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid,
                                                                        accountProcessTaskEntity.LinkedEntity.Csid) as DesktopBrokerAccountCM;
            decimal totalOpeiningBalance = 0;
            decimal totalClosingBalance = 0;

            if (product.ProductSecuritiesId != Guid.Empty)
            {
                var productSecurity = orgCM.ProductSecurities.Where(proseclist => proseclist.ID == product.ProductSecuritiesId).FirstOrDefault();
                var linkedAssets = model.Assets.Where(ast => ast.ID == accountProcessTaskEntity.AssetID && accountProcessTaskEntity.ModelID == model.ID);
                double totalPercentage = HoldingRptDataSet.CalculateTotalPercentageForSplit(linkedAssets);

                foreach (var productSecurityDetail in productSecurity.Details)
                {
                    var asxBroker = orgCM.Securities.Where(sec => sec.ID == productSecurityDetail.SecurityCodeId).FirstOrDefault();
                    
                    var filteredHoldingTransactionsByOpening = desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Where(holdTrans =>
                                                                                              holdTrans.TradeDate <= startDate.AddDays(-1));

                    var filteredHoldingTransactionsByClosing = desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Where(holdTrans =>
                                                                                  holdTrans.TradeDate <= endDate);

                    var filteredHoldingTransactionsOpening = filteredHoldingTransactionsByOpening.Where(holdTrans =>
                                                                                                  holdTrans.InvestmentCode == asxBroker.AsxCode);

                    var filteredHoldingTransactionsClosing = filteredHoldingTransactionsByClosing.Where(holdTrans =>
                                                                              holdTrans.InvestmentCode == asxBroker.AsxCode);

                    SetProductSecurity(orgCM, product, startDate.AddDays(-1), productSecurity, linkedAssets, ref totalPercentage, asxBroker, filteredHoldingTransactionsOpening, ref totalOpeiningBalance);

                    SetProductSecurity(orgCM, product, endDate, productSecurity, linkedAssets, ref totalPercentage, asxBroker, filteredHoldingTransactionsClosing, ref totalClosingBalance);
                }
            }
            
            else
                CalculateDesktopBrokerWithoutProductSecurity(orgCM, startDate, endDate, desktopBrokerAccountCM, ref totalOpeiningBalance, ref totalClosingBalance);

            capitalMovementByCategoryTotal.OpeningBalTotal += totalOpeiningBalance;
            capitalMovementByCategoryTotal.ClosingBalanceTotal += totalClosingBalance;
        }

        private static void SetProductSecurity(IOrganization orgCM, ProductEntity product, DateTime datetime, ProductSecuritiesEntity productSecurity, IEnumerable<AssetEntity> linkedAssets, ref double totalPercentage, SecuritiesEntity asxBroker, IEnumerable<HoldingTransactions> filteredHoldingTransactions, ref decimal totalHolding)
        {
            if (filteredHoldingTransactions.Count() > 0)
            {
                totalPercentage = product.SharePercentage;

                foreach (var linkedasst in linkedAssets)
                {
                    var otherProducts = linkedasst.Products.Where(linkedAsstPro => linkedAsstPro.ProductSecuritiesId != productSecurity.ID);
                    foreach (var OtherproductSecurity in otherProducts)
                    {
                        var OtherproductSecurityDetails = orgCM.ProductSecurities.Where(otherpro => otherpro.ID == OtherproductSecurity.ProductSecuritiesId).FirstOrDefault();

                        if (OtherproductSecurityDetails != null)
                        {
                            var otherAsxBroker = OtherproductSecurityDetails.Details.Where(otherProDetail => otherProDetail.SecurityCodeId == asxBroker.ID);

                            if (otherAsxBroker.Count() > 0)
                                totalPercentage += OtherproductSecurity.SharePercentage;
                        }
                    }
                }

                var hisSecurity = asxBroker.ASXSecurity.Where(secpri => secpri.Date <= datetime).OrderByDescending(secpri => secpri.Date).FirstOrDefault();
                decimal holding = filteredHoldingTransactions.Sum(filteredHolding => filteredHolding.Units);
                decimal unitPrice = 0;
                if (hisSecurity != null)
                    unitPrice = Convert.ToDecimal(hisSecurity.UnitPrice);

                decimal currentValue = holding * unitPrice;
                currentValue = currentValue * Convert.ToDecimal((product.SharePercentage / totalPercentage));
                totalHolding += currentValue;
            }
        }

        private static void CalculateDesktopBrokerWithoutProductSecurity(IOrganization orgCM, DateTime startDate, DateTime endDate, DesktopBrokerAccountCM desktopBrokerAccountCM, ref decimal totalOpeiningBalance, ref decimal totalClosingBalance)
        {
            var filteredHoldingTransactionsByOpening = desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Where(holdTrans =>
                                                                                              holdTrans.TradeDate <= startDate.AddDays(-1));

            var filteredHoldingTransactionsByClosing = desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Where(holdTrans =>
                                                                                  holdTrans.TradeDate <= endDate);


            var groupedSecuritiesOpening = filteredHoldingTransactionsByOpening.GroupBy(holdTran => holdTran.InvestmentCode);

            foreach (var security in groupedSecuritiesOpening)
            {
                decimal totalSecHoldingUnits = security.Sum(sec => sec.Units);
                var asxBroker = orgCM.Securities.Where(sec => sec.AsxCode == security.Key).FirstOrDefault();
                var hisSecurity = asxBroker.ASXSecurity.Where(secpri => secpri.Date <= startDate.AddDays(-1)).OrderByDescending(secpri => secpri.Date).FirstOrDefault();
                decimal unitPrice = 0;

                if (hisSecurity != null)
                    unitPrice = Convert.ToDecimal(hisSecurity.UnitPrice);

                decimal currentValue = totalSecHoldingUnits * unitPrice;
                totalOpeiningBalance += currentValue;
            }

            var groupedSecuritiesClosing = filteredHoldingTransactionsByClosing.GroupBy(holdTran => holdTran.InvestmentCode);

            foreach (var security in groupedSecuritiesClosing)
            {
                decimal totalSecHoldingUnits = security.Sum(sec => sec.Units);
                var asxBroker = orgCM.Securities.Where(sec => sec.AsxCode == security.Key).FirstOrDefault();
                var hisSecurity = asxBroker.ASXSecurity.Where(secpri => secpri.Date <= endDate).OrderByDescending(secpri => secpri.Date).FirstOrDefault();
                decimal unitPrice = 0;

                if (hisSecurity != null)
                    unitPrice = Convert.ToDecimal(hisSecurity.UnitPrice);

                decimal currentValue = totalSecHoldingUnits * unitPrice;
                totalClosingBalance += currentValue;
            }
        }

        private static void TDTransactions(ICMBroker Broker, IOrganization orgCM, CapitalReportDS capitalReportDS, AccountProcessTaskEntity accountProcessTaskEntity, AssetEntity asset, ModelEntity model, ProductEntity product, CapitalMovementByCategoryTotal capitalMovementByCategoryTotal, DateTime startDate, DateTime endDate)
        {
            BankAccountCM bankAccount = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as BankAccountCM;
            decimal openingBalance = 0;
            decimal closingBalance = 0;

            var filteredTransactionsOpening = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate <= startDate.AddDays(-1));
            var filteredTransactionsClosing = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate <= endDate);

            var tdGroupsOpeningType = filteredTransactionsOpening.GroupBy(cashMan => cashMan.TransactionType);

            foreach (var tdType in tdGroupsOpeningType)
            {
                var groupByIns = tdType.GroupBy(tdtypetran => tdtypetran.InstitutionID);

                foreach (var groupins in groupByIns)
                {
                    var transaction = groupins.FirstOrDefault();
                    decimal holding = groupins.Sum(inssum => inssum.Amount);
                    openingBalance += holding;
                }
            }

            var tdGroupsClosingType = filteredTransactionsClosing.GroupBy(cashMan => cashMan.TransactionType);

            foreach (var tdType in tdGroupsClosingType)
            {
                var groupByIns = tdType.GroupBy(tdtypetran => tdtypetran.InstitutionID);

                foreach (var groupins in groupByIns)
                {
                    var transaction = groupins.FirstOrDefault();
                    decimal holding = groupins.Sum(inssum => inssum.Amount);
                    closingBalance += holding;
                }
            }

            var filteredTransactions = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate <= endDate && trans.TransactionDate >= startDate);
            decimal application = filteredTransactions.Where(tran => tran.SystemTransactionType == "Application").Sum(tran => tran.TotalAmount);
            decimal redemption = filteredTransactions.Where(tran => tran.SystemTransactionType == "Redemption").Sum(tran => tran.TotalAmount);

            capitalMovementByCategoryTotal.OpeningBalTotal += openingBalance;
            capitalMovementByCategoryTotal.ClosingBalanceTotal += closingBalance;
            capitalMovementByCategoryTotal.ApplicationRedTotal += application;
            capitalMovementByCategoryTotal.ApplicationRedTotal += redemption;
        }

        private static void BankAccountTransctions(ICMBroker Broker, CapitalReportDS capitalReportDS, AccountProcessTaskEntity accountProcessTaskEntity, AssetEntity asset, ModelEntity model, ProductEntity product,
            CapitalMovementByCategoryTotal capitalMovementByCategoryTotal, DateTime startDate, DateTime endDate)
        {
            BankAccountCM bankAccount = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as BankAccountCM;
            var filteredTransactionsStartDate = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate <= startDate.AddDays(-1));
            var filteredTransactionsEndDat = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate <= endDate);
            decimal openingBalance = filteredTransactionsStartDate.Sum(calc => calc.TotalAmount);
            decimal closingBalance = filteredTransactionsEndDat.Sum(calc => calc.TotalAmount);

            var totalMonthTransactions = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate <= endDate && trans.TransactionDate >= startDate);

            decimal contributions = totalMonthTransactions.Where(tran => tran.Category == "Contribution").Sum(tran => tran.TotalAmount);
            decimal income = totalMonthTransactions.Where(tran => tran.Category == "Income").Sum(tran => tran.TotalAmount);
            decimal transferin = totalMonthTransactions.Where(tran => tran.Category == "Income" && tran.Category == "Transfer In").Sum(tran => tran.TotalAmount);
            decimal internalCashMovementIncome = totalMonthTransactions.Where(tran => tran.Category == "Income" && tran.SystemTransactionType == "Internal Cash Movement").Sum(tran => tran.TotalAmount);

            income = income - transferin - internalCashMovementIncome;

            decimal taxinout = totalMonthTransactions.Where(tran => tran.Category == "TAX").Sum(tran => tran.TotalAmount); ;
            decimal benefitPayment = totalMonthTransactions.Where(tran => tran.Category == "Benefit Payment").Sum(tran => tran.TotalAmount);
            decimal investment = totalMonthTransactions.Where(tran => tran.Category == "Investment").Sum(tran => tran.TotalAmount);

            decimal expense = totalMonthTransactions.Where(tran => tran.Category == "Expense").Sum(tran => tran.TotalAmount);
            decimal transferOut = totalMonthTransactions.Where(tran => tran.Category == "Expense" && tran.SystemTransactionType == "Transfer Out").Sum(tran => tran.TotalAmount);
            decimal internalCashMovementExpense = totalMonthTransactions.Where(tran => tran.Category == "Expense" && tran.SystemTransactionType == "Internal Cash Movement").Sum(tran => tran.TotalAmount);
            expense = expense - transferOut - internalCashMovementExpense;

            capitalMovementByCategoryTotal.TransferInOutTotal += contributions;
            capitalMovementByCategoryTotal.TransferInOutTotal += benefitPayment;
            capitalMovementByCategoryTotal.TransferInOutTotal += transferOut;
            capitalMovementByCategoryTotal.TransferInOutTotal += transferin;

            capitalMovementByCategoryTotal.InternalCashMovementTotal += internalCashMovementIncome;
            capitalMovementByCategoryTotal.InternalCashMovementTotal += internalCashMovementExpense;

            capitalMovementByCategoryTotal.ApplicationRedTotal += investment;

            capitalMovementByCategoryTotal.TaxInOutTotal += taxinout;

            capitalMovementByCategoryTotal.ExpenseTotal += expense;
            capitalMovementByCategoryTotal.ExpenseTotal += taxinout;

            capitalMovementByCategoryTotal.IncomeTotal += income;

            capitalMovementByCategoryTotal.OpeningBalTotal += openingBalance;
            capitalMovementByCategoryTotal.ClosingBalanceTotal += closingBalance;
        }

        private static void ManualTransactions(string clientID, ObservableCollection<ClientManualAssetEntity> ClientManualAssetCollection, IOrganization orgCM, CapitalReportDS capitalReportDS, CapitalMovementByCategoryTotal capitalMovementByCategoryTotal, DateTime startDate, DateTime endDate)
        {
            if (ClientManualAssetCollection != null)
            {
                var filteredManualAssetsStartDate = ClientManualAssetCollection.Where(man => man.TransactionDate <= startDate);
                var filteredManualAssetsEndDate = ClientManualAssetCollection.Where(man => man.TransactionDate <= endDate);

                decimal openingBalance = 0;

                foreach (var manualAsset in filteredManualAssetsStartDate)
                {
                    var systemManualAsset = orgCM.ManualAsset.Where(manList => manList.ID == manualAsset.InvestmentCode).FirstOrDefault();
                    var systemManualAssetPriceHistory = systemManualAsset.ASXSecurity.Where(manualHIS => manualHIS.Date <= startDate.AddDays(-1)).OrderByDescending(manualHis => manualHis.Date).FirstOrDefault();
                    if (systemManualAssetPriceHistory != null)
                        openingBalance += manualAsset.UnitsOnHand * Convert.ToDecimal(systemManualAssetPriceHistory.UnitPrice);
                }

                decimal closingBalance = 0;

                foreach (var manualAsset in filteredManualAssetsEndDate)
                {
                    var systemManualAsset = orgCM.ManualAsset.Where(manList => manList.ID == manualAsset.InvestmentCode).FirstOrDefault();
                    var systemManualAssetPriceHistory = systemManualAsset.ASXSecurity.Where(manualHIS => manualHIS.Date <= endDate).OrderByDescending(manualHis => manualHis.Date).FirstOrDefault();
                    if (systemManualAssetPriceHistory != null)
                        closingBalance += manualAsset.UnitsOnHand * Convert.ToDecimal(systemManualAssetPriceHistory.UnitPrice);
                }

                capitalMovementByCategoryTotal.OpeningBalTotal += openingBalance;
                capitalMovementByCategoryTotal.ClosingBalanceTotal += closingBalance;

                var filteredTransactionsByMonth = ClientManualAssetCollection.Where(man => man.TransactionDate <= startDate);
                
                foreach (ClientManualAssetEntity clientManualAssetEntity in filteredTransactionsByMonth)
                {
                   decimal netValue = 0;

                    var systemManualAsset = orgCM.ManualAsset.Where(manList => manList.ID == clientManualAssetEntity.InvestmentCode).FirstOrDefault();
                    var systemManualAssetPriceHistory = systemManualAsset.ASXSecurity.Where(manualHIS => manualHIS.Date <= clientManualAssetEntity.TransactionDate).OrderByDescending(manualHis => manualHis.Date).FirstOrDefault();
                    if (systemManualAssetPriceHistory != null)
                        closingBalance = clientManualAssetEntity.UnitsOnHand * Convert.ToDecimal(systemManualAssetPriceHistory.UnitPrice);
                }
            }
        }
    }
}