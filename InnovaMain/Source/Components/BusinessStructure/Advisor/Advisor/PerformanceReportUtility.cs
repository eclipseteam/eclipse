#region Using
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Xml.Linq;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.Group;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CM.Entity;
using Oritax.TaxSimp.Extensions;
using System.Collections.ObjectModel;
#endregion

namespace Oritax.TaxSimp.CM
{
    public class PerformanceFlowReportUtility
    {
        public DateTime LastDayOfMonthFromDateTime(DateTime dateTime)
        {
            DateTime firstDayOfTheMonth = new DateTime(dateTime.Year, dateTime.Month, 1);
            return firstDayOfTheMonth.AddMonths(1).AddDays(-1);
        }

        public PerformanceFlowReportUtility()
        { }

        public DateTime FirstDayOfMonthFromDateTime(DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, 1);
        }

        public void OnGetDataCapitalFlowDataSetCorporateInd(PerfReportDS data, string clientType, string clientID, string clientName, IdentityCM Parent, ICMBroker Broker, ClientIndividualEntity Entity)
        {

            DataTable clientSummaryTable = data.Tables[CapitalReportDS.CLIENTSUMMARYTABLE];
            DataTable contactTable = data.Tables[CapitalReportDS.CONTACTTABLE];

            DataRow clientSummaryRow = clientSummaryTable.NewRow();
            DataRow contactRow = contactTable.NewRow();

            clientSummaryRow[CapitalReportDS.CLIENTID] = clientID;
            clientSummaryRow[CapitalReportDS.CLIENTNAME] = clientName;
            clientSummaryRow[CapitalReportDS.CIENTABN] = Entity.ABN;
            clientSummaryRow[CapitalReportDS.CLIENTTYPE] = clientType;

            if (Parent != null && Parent.Clid != Guid.Empty && Parent.Csid != Guid.Empty)
            {
                AdvisorCM adviserUnit = Broker.GetCMImplementation(Parent.Clid, Parent.Csid) as AdvisorCM;
                clientSummaryRow[HoldingRptDataSet.ADVISERFULLNAME] = adviserUnit.AdvisorEntity.FullName;
                Broker.ReleaseBrokerManagedComponent(adviserUnit);
            }

            clientSummaryRow[CapitalReportDS.ADDRESSLINE1] = Entity.Address.BusinessAddress.Addressline1;
            clientSummaryRow[CapitalReportDS.ADDRESSLINE2] = Entity.Address.BusinessAddress.Addressline2;
            clientSummaryRow[CapitalReportDS.SUBURB] = Entity.Address.BusinessAddress.Suburb;
            clientSummaryRow[CapitalReportDS.STATE] = Entity.Address.BusinessAddress.State;
            clientSummaryRow[CapitalReportDS.POSTCODE] = Entity.Address.BusinessAddress.PostCode;
            clientSummaryRow[CapitalReportDS.COUNTRY] = Entity.Address.BusinessAddress.Country;

            CSVInstance csvinstance = new CSVInstance(Broker);
            ClientIndividualEntityCSV _entitycsv = csvinstance.GetEntityConcreteInstance(Entity);
            foreach (var contact in _entitycsv.Applicants)
            {
                contactRow[CapitalReportDS.CONTACT_TITLE] = contact.PersonalTitle;
                contactRow[CapitalReportDS.CONTACT_NAME] = contact.Fullname;
                contactRow[CapitalReportDS.CONTACT_BIZNO] = contact.WorkPhoneNumber;
                contactRow[CapitalReportDS.CONTACT_MOBNO] = contact.MobilePhoneNumber;
                contactRow[CapitalReportDS.CONTACT_HNO] = contact.HomePhoneNumber;
                contactRow[CapitalReportDS.CONTACT_FAXNO] = contact.Facsimile;
                contactRow[CapitalReportDS.CONTACT_EMAIL] = contact.EmailAddress;
            }

            clientSummaryTable.Rows.Add(clientSummaryRow);
            contactTable.Rows.Add(contactRow);

            IOrganization orgCM = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            PerfReportDS perfReportDS = data as PerfReportDS;

            DataTable CapitalSummaryTable = perfReportDS.Tables[CapitalReportDS.CAPITALFLOWSUMMARY];
            DataTable productBreakDownTable = perfReportDS.Tables[CapitalReportDS.PRODUCTBREAKDOWNTABLE];
            DataTable TDBreakDownTable = perfReportDS.Tables[CapitalReportDS.TDBREAKDOWNTABLE];

            DataTable CapitalFlowSummaryReport = ExtractCapitalSummaryTableByCategoryInd(Broker, Entity, perfReportDS, clientID, orgCM);

            if (Entity.ClientManualAssetCollection != null)
            {
                var groupedManualTransactions = Entity.ClientManualAssetCollection.GroupBy(man => man.InvestmentCode);

                foreach (var groupedManualTransaction in groupedManualTransactions)
                {
                    var systemManualAsset = orgCM.ManualAsset.Where(manList => manList.ID == groupedManualTransaction.Key).FirstOrDefault();
                    
                    decimal holdingLatest  = 0;
                    decimal holdingEndDate = 0;
                    decimal holdingStartDate = 0;
                    decimal unitholdingEndDate = 0;
                    decimal unitholdingStartDate = 0;

                    double unitPriceLatest = systemManualAsset.ASXSecurity.OrderByDescending(manualHis => manualHis.Date).FirstOrDefault().UnitPrice;
                    double unitPriceStartDate = systemManualAsset.ASXSecurity.OrderByDescending(manualHis => manualHis.Date).FirstOrDefault().UnitPrice;

                    unitholdingStartDate = groupedManualTransaction.Where(manTran => manTran.TransactionDate <= perfReportDS.StartDate).Sum(manTran => manTran.UnitsOnHand);
                    unitholdingEndDate = groupedManualTransaction.Where(manTran => manTran.TransactionDate <= perfReportDS.EndDate).Sum(manTran => manTran.UnitsOnHand); 

                    //Calculate Holding based on unit price for each transactiondate
                    foreach (var manualTransaction in groupedManualTransaction)
                    {
                        decimal holdingPerTransaction = 0;
                        double unitPrice = 0;
                        
                        var manualHistory = systemManualAsset.ASXSecurity.Where(manualHis => manualHis.Date <= manualTransaction.TransactionDate).OrderByDescending(manualHis => manualHis.Date).FirstOrDefault();
                        if (manualHistory != null)
                            unitPrice = manualHistory.UnitPrice;
                      
                        holdingPerTransaction = manualTransaction.UnitsOnHand * Convert.ToDecimal(unitPrice);
                        holdingEndDate += holdingPerTransaction;
                    }

                    holdingStartDate = unitholdingStartDate * Convert.ToDecimal(unitPriceStartDate);
                    holdingLatest = unitholdingEndDate * Convert.ToDecimal(unitPriceLatest);

                    perfReportDS.AddCapitalRow("ManualAsset", "ManualAsset", systemManualAsset.AsxCode, systemManualAsset.CompanyName, clientID, holdingLatest, holdingEndDate, holdingStartDate, string.Empty, string.Empty);
                }
            }

            foreach (var accountProcessTaskEntity in Entity.ListAccountProcess)
            {
                if (accountProcessTaskEntity.LinkedEntity != null && accountProcessTaskEntity.LinkedEntity.Clid != Guid.Empty)
                {
                    var asset = orgCM.Assets.Where(var => var.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                    var model = orgCM.Model.Where(mod => mod.ID == accountProcessTaskEntity.ModelID).FirstOrDefault();
                    var linkedAsset = model.Assets.Where(ast => ast.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                    var product = linkedAsset.Products.Where(pro => pro.ID == accountProcessTaskEntity.TaskID).FirstOrDefault();

                    if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.BankAccount)
                    {
                        BankAccountCM bankAccount = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as BankAccountCM;
                        var filteredTransactionsStart = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate <= perfReportDS.StartDate);
                        decimal holdingTotalStart = filteredTransactionsStart.Sum(calc => calc.TotalAmount);

                        var filteredTransactionsEnd = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate <= perfReportDS.EndDate);
                        decimal holdingTotalEnd = filteredTransactionsEnd.Sum(calc => calc.TotalAmount);

                        perfReportDS.AddCapitalRow(asset.Name, model.ServiceType.ToString(), product.Name,
                                                            product.TaskDescription, bankAccount.BankAccountEntity.AccountNumber, holdingTotalEnd, holdingTotalEnd, holdingTotalStart, string.Empty, string.Empty);
                    }
                    else if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.TermDepositAccount)
                    {
                        BankAccountCM bankAccount = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as BankAccountCM;
                        decimal holdingTotalStart = 0;
                        decimal holdingTotalEnd= 0;

                        var filteredTransactions = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate <= perfReportDS.StartDate);
                        var tdGroupsType = filteredTransactions.GroupBy(cashMan => cashMan.TransactionType);

                        foreach (var tdType in tdGroupsType)
                        {
                            var groupByIns = tdType.GroupBy(tdtypetran => tdtypetran.InstitutionID);

                            foreach (var groupins in groupByIns)
                            {
                                var transaction = groupins.FirstOrDefault();
                                decimal holding = groupins.Sum(inssum => inssum.Amount);

                                DataRow tDBreakDownTableRow = TDBreakDownTable.NewRow();
                                perfReportDS.TDBreakDownRow(orgCM.Institution.Where(ins => ins.ID == groupins.Key).FirstOrDefault().Name, model.ServiceType.ToString(), tdType.Key, transaction.AdministrationSystem, holding, bankAccount.BankAccountEntity.InstitutionID.ToString(), tDBreakDownTableRow);
                                TDBreakDownTable.Rows.Add(tDBreakDownTableRow);

                                holdingTotalStart += holding;
                            }
                        }

                        filteredTransactions = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate <= perfReportDS.EndDate);
                        tdGroupsType = filteredTransactions.GroupBy(cashMan => cashMan.TransactionType);

                        foreach (var tdType in tdGroupsType)
                        {
                            var groupByIns = tdType.GroupBy(tdtypetran => tdtypetran.InstitutionID);

                            foreach (var groupins in groupByIns)
                            {
                                var transaction = groupins.FirstOrDefault();
                                decimal holding = groupins.Sum(inssum => inssum.Amount);

                                DataRow tDBreakDownTableRow = TDBreakDownTable.NewRow();
                                perfReportDS.TDBreakDownRow(orgCM.Institution.Where(ins => ins.ID == groupins.Key).FirstOrDefault().Name, model.ServiceType.ToString(), tdType.Key, transaction.AdministrationSystem, holding, bankAccount.BankAccountEntity.InstitutionID.ToString(), tDBreakDownTableRow);
                                TDBreakDownTable.Rows.Add(tDBreakDownTableRow);

                                holdingTotalEnd += holding;
                            }
                        }

                        perfReportDS.AddCapitalRow(asset.Name, model.ServiceType.ToString(), product.Name,
                                                            product.TaskDescription, bankAccount.BankAccountEntity.AccountNumber, holdingTotalEnd, holdingTotalEnd,holdingTotalStart, string.Empty, bankAccount.BankAccountEntity.InstitutionID.ToString());
                    }
                    else if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.DesktopBrokerAccount)
                    {
                        DesktopBrokerAccountCM desktopBrokerAccountCM = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid,
                                                                                    accountProcessTaskEntity.LinkedEntity.Csid) as DesktopBrokerAccountCM;
                        decimal presentHoldingTotal = 0;
                        decimal endHoldingTotal = 0;
                        decimal startHoldingTotal = 0;

                        //If it is linked with product securities for split type calculation
                        if (product.ProductSecuritiesId != Guid.Empty)
                        {
                            var productSecurity = orgCM.ProductSecurities.Where(proseclist => proseclist.ID == product.ProductSecuritiesId).FirstOrDefault();
                            var linkedAssets = model.Assets.Where(ast => ast.ID == accountProcessTaskEntity.AssetID && accountProcessTaskEntity.ModelID == model.ID);
                            double totalPercentage = HoldingRptDataSet.CalculateTotalPercentageForSplit(linkedAssets);

                            foreach (var productSecurityDetail in productSecurity.Details)
                            {
                                var asxBroker = orgCM.Securities.Where(sec => sec.ID == productSecurityDetail.SecurityCodeId).FirstOrDefault();
                                var filteredHoldingTransactionsByStartDate = desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Where(holdTrans =>
                                                                                                              holdTrans.TradeDate <= perfReportDS.StartDate);
                                var filteredHoldingTransactionsStartDate = filteredHoldingTransactionsByStartDate.Where(holdTrans =>
                                                                                                              holdTrans.InvestmentCode == asxBroker.AsxCode);

                                var filteredHoldingTransactionsByEndDate = desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Where(holdTrans =>
                                                                                                              holdTrans.TradeDate <= perfReportDS.EndDate);
                                var filteredHoldingTransactionsEndDate = filteredHoldingTransactionsByEndDate.Where(holdTrans =>
                                                                                                              holdTrans.InvestmentCode == asxBroker.AsxCode);

                                var count = filteredHoldingTransactionsByEndDate.Count();

                                if (count > 0)
                                {
                                    totalPercentage = product.SharePercentage;

                                    foreach (var linkedasst in linkedAssets)
                                    {
                                        var otherProducts = linkedasst.Products.Where(linkedAsstPro => linkedAsstPro.ProductSecuritiesId != productSecurity.ID);
                                        foreach (var OtherproductSecurity in otherProducts)
                                        {
                                            var OtherproductSecurityDetails = orgCM.ProductSecurities.Where(otherpro => otherpro.ID == OtherproductSecurity.ProductSecuritiesId).FirstOrDefault();

                                            if (OtherproductSecurityDetails != null)
                                            {
                                                var otherAsxBroker = OtherproductSecurityDetails.Details.Where(otherProDetail => otherProDetail.SecurityCodeId == asxBroker.ID);

                                                if (otherAsxBroker.Count() > 0)
                                                    totalPercentage += OtherproductSecurity.SharePercentage;
                                            }
                                        }
                                    }

                                    decimal unitPriceStart = 0;
                                    decimal unitPriceEnd = 0;

                                    var hisSecurityStart = asxBroker.ASXSecurity.Where(his => his.Date <= perfReportDS.StartDate).OrderByDescending(secpri => secpri.Date).FirstOrDefault();
                                    decimal startHoldingUnits = filteredHoldingTransactionsStartDate.Sum(filteredHolding => filteredHolding.Units);

                                    if (hisSecurityStart != null)
                                        unitPriceStart = Convert.ToDecimal(hisSecurityStart.UnitPrice);

                                    decimal startHoldingValue = startHoldingUnits * unitPriceStart;

                                    var hisSecurityLatest = asxBroker.ASXSecurity.Where(his => his.Date <= perfReportDS.EndDate).OrderByDescending(secpri => secpri.Date).FirstOrDefault();
                                    decimal endHoldingUnits = filteredHoldingTransactionsEndDate.Sum(filteredHolding => filteredHolding.Units);

                                    if (hisSecurityLatest != null)
                                        unitPriceEnd = Convert.ToDecimal(hisSecurityLatest.UnitPrice);

                                    decimal latestHoldingValue = endHoldingUnits * unitPriceEnd;

                                    decimal endHoldingValue = filteredHoldingTransactionsEndDate.Sum(filteredHolding => filteredHolding.NetValue);

                                    startHoldingValue = startHoldingValue * Convert.ToDecimal((product.SharePercentage / totalPercentage));
                                    latestHoldingValue = latestHoldingValue * Convert.ToDecimal((product.SharePercentage / totalPercentage));
                                    endHoldingValue = endHoldingValue * Convert.ToDecimal((product.SharePercentage / totalPercentage));

                                    perfReportDS.AddProductDetailRow(product, unitPriceEnd, asxBroker.AsxCode, endHoldingUnits, endHoldingUnits,
                                                                           startHoldingUnits, latestHoldingValue, startHoldingValue,endHoldingValue, model.ServiceType.ToString());

                                    presentHoldingTotal += latestHoldingValue;
                                    endHoldingTotal += endHoldingValue;
                                    startHoldingTotal += startHoldingValue;
                                }
                            }
                        }
                        else
                        {
                            var filteredHoldingTransactionsByStartDate = desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Where(holdTrans =>
                                                                                                              holdTrans.TradeDate <= perfReportDS.StartDate);

                            var filteredHoldingTransactionsByEndDate = desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Where(holdTrans =>
                                                                                  holdTrans.TradeDate <= perfReportDS.EndDate);

                            var groupedSecurities = filteredHoldingTransactionsByEndDate.GroupBy(holdTran => holdTran.InvestmentCode);

                            foreach (var security in groupedSecurities)
                            {
                                decimal totalSecHoldingUnits = security.Sum(sec => sec.Units);
                                var asxBroker = orgCM.Securities.Where(sec => sec.AsxCode == security.Key).FirstOrDefault();


                                var hisSecurityStart = asxBroker.ASXSecurity.Where(his => his.Date <= perfReportDS.StartDate).OrderByDescending(secpri => secpri.Date).FirstOrDefault();
                                decimal startHoldingUnits = filteredHoldingTransactionsByStartDate.Sum(filteredHolding => filteredHolding.Units);

                                decimal startUnitPrice = 0;
                                decimal endUnitPrice = 0; 

                                if (hisSecurityStart != null)
                                    startUnitPrice = Convert.ToDecimal(hisSecurityStart.UnitPrice);

                                decimal startHoldingValue = startHoldingUnits * startUnitPrice;

                                var hisSecurityLatest = asxBroker.ASXSecurity.Where(his => his.Date <= perfReportDS.EndDate).OrderByDescending(secpri => secpri.Date).FirstOrDefault();

                                if (hisSecurityLatest != null)
                                    endUnitPrice = Convert.ToDecimal(hisSecurityLatest.UnitPrice);

                                decimal endHoldingUnits = filteredHoldingTransactionsByEndDate.Where(tran => tran.InvestmentCode == security.Key).Sum(filteredHolding => filteredHolding.Units);
                                decimal latestHoldingValue = endHoldingUnits * endUnitPrice;

                                decimal endHoldingValue = filteredHoldingTransactionsByEndDate.Where(tran => tran.InvestmentCode == security.Key).Sum(filteredHolding => filteredHolding.NetValue);

                                perfReportDS.AddProductDetailRow(product, endUnitPrice, asxBroker.AsxCode, endHoldingUnits, endHoldingUnits,
                                                                       startHoldingUnits, latestHoldingValue, startHoldingValue, endHoldingValue, model.ServiceType.ToString());

                                presentHoldingTotal += latestHoldingValue;
                                endHoldingTotal += endHoldingValue;
                                startHoldingTotal += startHoldingValue;
                            }
                        }

                        perfReportDS.AddCapitalRow(asset.Name, model.ServiceType.ToString(), product.Name, product.TaskDescription, desktopBrokerAccountCM.DesktopBrokerAccountEntity.AccountNumber 
                                                                                                ,presentHoldingTotal,endHoldingTotal,startHoldingTotal, product.ID.ToString(), string.Empty);
                    }
                    else if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.ManagedInvestmentSchemesAccount)
                    {
                        ManagedInvestmentSchemesAccountCM managedInvestmentSchemesAccountCM = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as ManagedInvestmentSchemesAccountCM;
                        var linkedMISAccount = managedInvestmentSchemesAccountCM.ManagedInvestmentSchemesAccountEntity.FundAccounts.Where(fa => product.TaskDescription.Contains(fa.Code)).FirstOrDefault();

                        var filteredHoldingTransactionsByStartDate = linkedMISAccount.FundTransactions.Where(cliFundTrans => cliFundTrans.TradeDate <= perfReportDS.StartDate);
                        var filteredHoldingTransactionsByEndDate = linkedMISAccount.FundTransactions.Where(cliFundTrans => cliFundTrans.TradeDate <= perfReportDS.EndDate);

                        var clientFundTransactionsStart = filteredHoldingTransactionsByStartDate.Where(cliFundTrans => cliFundTrans.ClientID == clientID);
                        decimal totalSharesStart = clientFundTransactionsStart.Sum(ft => ft.Shares);

                        var clientFundTransactionsEnd = filteredHoldingTransactionsByEndDate.Where(cliFundTrans => cliFundTrans.ClientID == clientID);
                        decimal totalSharesEnd = clientFundTransactionsEnd.Sum(ft => ft.Shares);

                        var linkedmissec = orgCM.Securities.Where(missec => missec.AsxCode == linkedMISAccount.Code).FirstOrDefault();

                        decimal currentValue = 0;
                        decimal startValue = 0;
                        decimal endValue = 0;

                        if (linkedmissec != null)
                        {   
                            decimal startUnitPrice = 0;
                            decimal endUnitPrice = 0;

                            var latestMisPriceObjectStart = linkedmissec.ASXSecurity.Where(mishis => mishis.Date <= perfReportDS.StartDate).OrderByDescending(mishis => mishis.Date).FirstOrDefault();
                            var latestMisPriceObjectEnd = linkedmissec.ASXSecurity.Where(mishis => mishis.Date <= perfReportDS.EndDate).OrderByDescending(mishis => mishis.Date).FirstOrDefault();

                            if (latestMisPriceObjectStart != null)
                                startUnitPrice = Convert.ToDecimal(latestMisPriceObjectStart.UnitPrice);

                            if (latestMisPriceObjectEnd != null)
                                endUnitPrice = Convert.ToDecimal(latestMisPriceObjectEnd.UnitPrice);

                            startValue = totalSharesStart * startUnitPrice;
                            currentValue = totalSharesEnd * endUnitPrice;

                            endValue = clientFundTransactionsEnd.Sum(tran => tran.Amount);

                            perfReportDS.AddProductDetailRow(product, endUnitPrice, linkedmissec.AsxCode, totalSharesEnd, totalSharesEnd, totalSharesStart,
                                                                                                currentValue, startValue, endValue, model.ServiceType.ToString());
                        }

                        perfReportDS.AddCapitalRow(asset.Name, model.ServiceType.ToString(), product.Name, product.TaskDescription, clientID, currentValue, endValue, startValue, product.ID.ToString(), string.Empty);
                    }
                }
            }
            
            GetBankAccountDataSet(orgCM, Entity.ListAccountProcess, Broker, data);
            GetASXTransactionDS(orgCM, Entity.ListAccountProcess, Broker, data);
            Broker.ReleaseBrokerManagedComponent(orgCM);
        }

        private DataTable ExtractCapitalSummaryTableByCategoryInd(ICMBroker Broker, ClientIndividualEntity Entity, PerfReportDS capitalReportDS, string ClientID, IOrganization orgCM)
        {
            DataTable CapitalFlowSummaryReport = capitalReportDS.Tables[CapitalReportDS.CAPITALFLOWSUMMARYCAT];
            DataTable PerformanceTable = new DataTable("PerformanceTableOverall");
            PerformanceTable.Columns.Add(CapitalReportDS.MONTH, typeof(DateTime));
            PerformanceTable.Columns.Add(CapitalReportDS.OPENINGBAL);
            PerformanceTable.Columns.Add(CapitalReportDS.CLOSINGBALANCE);
            PerformanceTable.Columns.Add("WeightedCf", typeof(decimal));
            PerformanceTable.Columns.Add("CfTotal", typeof(decimal));
            PerformanceTable.Columns.Add("ModDietz", typeof(decimal));
            capitalReportDS.Tables.Add(PerformanceTable);

            DataTable PerformanceTableDIFM = new DataTable("PerformanceTableDIFM");
            PerformanceTableDIFM.Columns.Add(CapitalReportDS.MONTH, typeof(DateTime));
            PerformanceTableDIFM.Columns.Add(CapitalReportDS.OPENINGBAL);
            PerformanceTableDIFM.Columns.Add(CapitalReportDS.CLOSINGBALANCE);
            PerformanceTableDIFM.Columns.Add("WeightedCf", typeof(decimal));
            PerformanceTableDIFM.Columns.Add("CfTotal", typeof(decimal));
            PerformanceTableDIFM.Columns.Add("ModDietz", typeof(decimal));
            capitalReportDS.Tables.Add(PerformanceTableDIFM);

            DataTable PerformanceTableDIWM = new DataTable("PerformanceTableDIWM");
            PerformanceTableDIWM.Columns.Add(CapitalReportDS.MONTH, typeof(DateTime));
            PerformanceTableDIWM.Columns.Add(CapitalReportDS.OPENINGBAL);
            PerformanceTableDIWM.Columns.Add(CapitalReportDS.CLOSINGBALANCE);
            PerformanceTableDIWM.Columns.Add("WeightedCf", typeof(decimal));
            PerformanceTableDIWM.Columns.Add("CfTotal", typeof(decimal));
            PerformanceTableDIWM.Columns.Add("ModDietz", typeof(decimal));
            capitalReportDS.Tables.Add(PerformanceTableDIWM);

            DataTable PerformanceTableDIY = new DataTable("PerformanceTableDIY");
            PerformanceTableDIY.Columns.Add(CapitalReportDS.MONTH, typeof(DateTime));
            PerformanceTableDIY.Columns.Add(CapitalReportDS.OPENINGBAL);
            PerformanceTableDIY.Columns.Add(CapitalReportDS.CLOSINGBALANCE);
            PerformanceTableDIY.Columns.Add("WeightedCf", typeof(decimal));
            PerformanceTableDIY.Columns.Add("CfTotal", typeof(decimal));
            PerformanceTableDIY.Columns.Add("ModDietz", typeof(decimal));
            capitalReportDS.Tables.Add(PerformanceTableDIY);

            DataTable PerformanceTableMAN = new DataTable("PerformanceTableMAN");
            PerformanceTableMAN.Columns.Add(CapitalReportDS.MONTH, typeof(DateTime));
            PerformanceTableMAN.Columns.Add(CapitalReportDS.OPENINGBAL);
            PerformanceTableMAN.Columns.Add(CapitalReportDS.CLOSINGBALANCE);
            PerformanceTableMAN.Columns.Add("WeightedCf", typeof(decimal));
            PerformanceTableMAN.Columns.Add("CfTotal", typeof(decimal));
            PerformanceTableMAN.Columns.Add("ModDietz", typeof(decimal));
            capitalReportDS.Tables.Add(PerformanceTableMAN);

            int nMonthDiff_FirstToFirst = capitalReportDS.EndDate.Month - capitalReportDS.StartDate.Month + ((capitalReportDS.EndDate.Year - capitalReportDS.StartDate.Year) * 12);
            double dMonthDiff = (double)nMonthDiff_FirstToFirst + (capitalReportDS.EndDate - capitalReportDS.StartDate.AddMonths(nMonthDiff_FirstToFirst)).TotalDays / (double)DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month);

            for (int monthCounter = 0; monthCounter <= dMonthDiff; monthCounter++)
            {
                CapitalMovementByCategoryTotal capitalMovementByCategoryTotal = new CapitalMovementByCategoryTotal();
                ModifiedDietz modifiedDietz = new CM.ModifiedDietz();
                ModifiedDietz modifiedDietzDIY = new CM.ModifiedDietz();
                ModifiedDietz modifiedDietzDIWM = new CM.ModifiedDietz();
                ModifiedDietz modifiedDietzDIFM = new CM.ModifiedDietz();
                ModifiedDietz modifiedDietzMAN = new CM.ModifiedDietz();

                DateTime startdate = FirstDayOfMonthFromDateTime(capitalReportDS.StartDate.AddMonths(monthCounter));
                DateTime endDate = LastDayOfMonthFromDateTime(capitalReportDS.StartDate.AddMonths(monthCounter));

                ManualTransactions(ClientID, Entity.ClientManualAssetCollection, orgCM, capitalReportDS, capitalMovementByCategoryTotal, startdate, endDate, modifiedDietz, modifiedDietzMAN);
                GetTransactionsInfoFromLinkProcess(ClientID, Broker, Entity.ListAccountProcess, orgCM, capitalReportDS, capitalMovementByCategoryTotal, startdate, endDate, modifiedDietz, modifiedDietzDIY, modifiedDietzDIWM, modifiedDietzDIFM);

                foreach (Oritax.TaxSimp.Common.IdentityCMDetail desktopBrokerAccounts in Entity.DesktopBrokerAccounts)
                {
                    DesktopBrokerAccountCM desktopBrokerAccountCM = Broker.GetCMImplementation(desktopBrokerAccounts.Clid,
                                                                        desktopBrokerAccounts.Csid) as DesktopBrokerAccountCM;
                    var filteredTransactionsByMonth = desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Where(tran => tran.TradeDate <= endDate && tran.TradeDate >= startdate);

                    foreach (HoldingTransactions holdingTransactions in filteredTransactionsByMonth)
                    {
                        if (holdingTransactions.Type == "RA")
                            modifiedDietz.AddModDietzEntity(holdingTransactions.NetValue * -1, holdingTransactions.TradeDate, "ASX");
                        else
                            modifiedDietz.AddModDietzEntity(holdingTransactions.NetValue, holdingTransactions.TradeDate, "ASX");

                        if (holdingTransactions.Type == "RJ" || holdingTransactions.Type == "AJ")
                        {
                            decimal unitprice = 0;

                            var unitPriceObj = orgCM.Securities.Where(sec => sec.AsxCode == holdingTransactions.InvestmentCode).FirstOrDefault().ASXSecurity.Where(his => his.Date <= holdingTransactions.TradeDate).OrderByDescending(his => his.Date).FirstOrDefault();

                            if (unitPriceObj != null)
                                unitprice = Convert.ToDecimal(unitPriceObj.UnitPrice);

                            holdingTransactions.NetValue = holdingTransactions.Units * unitprice;

                            if (holdingTransactions.Type == "AJ")
                                holdingTransactions.NetValue = holdingTransactions.NetValue * -1;
                        }
                    }

                    decimal transferInASX = filteredTransactionsByMonth.Where(tran => tran.Type == "AT").Sum(tran => tran.NetValue);
                    decimal transferOutASX = filteredTransactionsByMonth.Where(tran => tran.Type == "RT").Sum(tran => tran.NetValue);
                    decimal adjustmentUp = filteredTransactionsByMonth.Where(tran => tran.Type == "RJ").Sum(tran => tran.NetValue);
                    decimal adjustmentDown = filteredTransactionsByMonth.Where(tran => tran.Type == "AJ").Sum(tran => tran.NetValue);
                    decimal sell = filteredTransactionsByMonth.Where(tran => tran.Type == "RA").Sum(tran => tran.NetValue);
                    decimal buyPurchase = filteredTransactionsByMonth.Where(tran => tran.Type == "AN").Sum(tran => tran.NetValue);

                    capitalMovementByCategoryTotal.TransferInOutTotal += transferInASX;
                    capitalMovementByCategoryTotal.TransferInOutTotal += transferOutASX;
                    capitalMovementByCategoryTotal.TransferInOutTotal += adjustmentUp;
                    capitalMovementByCategoryTotal.TransferInOutTotal += adjustmentDown;
                    capitalMovementByCategoryTotal.ApplicationRedTotal += (sell * -1);
                    capitalMovementByCategoryTotal.ApplicationRedTotal += buyPurchase;
                }

                DataTable dt = new DataTable();
                dt.Columns.Add("NetValue");
                dt.Columns.Add("WCF");
                dt.Columns.Add("TYPE");
                dt.Columns.Add("TDATE");

                foreach (ModifiedDietzTransactionEntity ModifiedDietzTransactionEntity in modifiedDietz.ModifiedDietzTransactionEntityCol)
                {
                    DataRow dr = dt.NewRow();
                    dr["NetValue"] = ModifiedDietzTransactionEntity.NetValue;
                    dr["WCF"] = ModifiedDietzTransactionEntity.WeightedCf;
                    dr["TYPE"] = ModifiedDietzTransactionEntity.Type;
                    dr["TDATE"] = ModifiedDietzTransactionEntity.TransactionDate;

                    dt.Rows.Add(dr);
                }

                modifiedDietz.CalculatedDietz();
                modifiedDietzDIY.CalculatedDietz();
                modifiedDietzDIWM.CalculatedDietz();
                modifiedDietzDIFM.CalculatedDietz();
                modifiedDietzMAN.CalculatedDietz();

                DataRow row = CapitalFlowSummaryReport.NewRow();

                row[CapitalReportDS.MONTH] = startdate;
                row[CapitalReportDS.OPENINGBAL] = capitalMovementByCategoryTotal.OpeningBalTotal;
                row[CapitalReportDS.TRANSFEROUT] = capitalMovementByCategoryTotal.TransferInOutTotal;
                row[CapitalReportDS.INCOME] = capitalMovementByCategoryTotal.IncomeTotal;
                row[CapitalReportDS.APPLICATIONREDEMPTION] = capitalMovementByCategoryTotal.ApplicationRedTotal;
                row[CapitalReportDS.TAXINOUT] = capitalMovementByCategoryTotal.TaxInOutTotal;
                row[CapitalReportDS.EXPENSE] = capitalMovementByCategoryTotal.ExpenseTotal;
                row[CapitalReportDS.INTERNALCASHMOVEMENT] = capitalMovementByCategoryTotal.InternalCashMovementTotal;
                row[CapitalReportDS.CLOSINGBALANCE] = capitalMovementByCategoryTotal.ClosingBalanceTotal;
                row[CapitalReportDS.CHANGEININVESTMENTVALUE] = capitalMovementByCategoryTotal.ChangeInInvestTotal;
                row[CapitalReportDS.MODDIETZ] = modifiedDietz.ModDietz;
                row[CapitalReportDS.SUMCF] = modifiedDietz.CfTotal;
                row[CapitalReportDS.SUMWEIGHTCF] = modifiedDietz.WeightedCfTotal;
                CapitalFlowSummaryReport.Rows.Add(row);

                DataRow performRow = PerformanceTable.NewRow();
                performRow[CapitalReportDS.MONTH] = startdate;
                performRow[CapitalReportDS.OPENINGBAL] = modifiedDietz.OpeningBalTotal;
                performRow[CapitalReportDS.CLOSINGBALANCE] = modifiedDietz.ClosingBalanceTotal;
                performRow["WeightedCf"] = modifiedDietz.WeightedCfTotal;
                performRow["CfTotal"] = modifiedDietz.CfTotal;
                performRow["ModDietz"] = modifiedDietz.ModDietz;
                PerformanceTable.Rows.Add(performRow);

                DataRow performRowDIY = PerformanceTableDIY.NewRow();
                performRowDIY[CapitalReportDS.MONTH] = startdate;
                performRowDIY[CapitalReportDS.OPENINGBAL] = modifiedDietzDIY.OpeningBalTotal;
                performRowDIY[CapitalReportDS.CLOSINGBALANCE] = modifiedDietzDIY.ClosingBalanceTotal;
                performRowDIY["WeightedCf"] = modifiedDietzDIY.WeightedCfTotal;
                performRowDIY["CfTotal"] = modifiedDietzDIY.CfTotal;
                performRowDIY["ModDietz"] = modifiedDietzDIY.ModDietz;
                PerformanceTableDIY.Rows.Add(performRowDIY);

                DataRow performRowDIWM = PerformanceTableDIWM.NewRow();
                performRowDIWM[CapitalReportDS.MONTH] = startdate;
                performRowDIWM[CapitalReportDS.OPENINGBAL] = modifiedDietzDIWM.OpeningBalTotal;
                performRowDIWM[CapitalReportDS.CLOSINGBALANCE] = modifiedDietzDIWM.ClosingBalanceTotal;
                performRowDIWM["WeightedCf"] = modifiedDietzDIWM.WeightedCfTotal;
                performRowDIWM["CfTotal"] = modifiedDietzDIWM.CfTotal;
                performRowDIWM["ModDietz"] = modifiedDietzDIWM.ModDietz;
                PerformanceTableDIWM.Rows.Add(performRowDIWM);

                DataRow performRowDIFM = PerformanceTableDIFM.NewRow();
                performRowDIFM[CapitalReportDS.MONTH] = startdate;
                performRowDIFM[CapitalReportDS.OPENINGBAL] = modifiedDietzDIFM.OpeningBalTotal;
                performRowDIFM[CapitalReportDS.CLOSINGBALANCE] = modifiedDietzDIFM.ClosingBalanceTotal;
                performRowDIFM["WeightedCf"] = modifiedDietzDIFM.WeightedCfTotal;
                performRowDIFM["CfTotal"] = modifiedDietzDIFM.CfTotal;
                performRowDIFM["ModDietz"] = modifiedDietzDIFM.ModDietz;
                PerformanceTableDIFM.Rows.Add(performRowDIFM);

                DataRow performRowMAN = PerformanceTableMAN.NewRow();
                performRowMAN[CapitalReportDS.MONTH] = startdate;
                performRowMAN[CapitalReportDS.OPENINGBAL] = modifiedDietzMAN.OpeningBalTotal;
                performRowMAN[CapitalReportDS.CLOSINGBALANCE] = modifiedDietzMAN.ClosingBalanceTotal;
                performRowMAN["WeightedCf"] = modifiedDietzMAN.WeightedCfTotal;
                performRowMAN["CfTotal"] = modifiedDietzMAN.CfTotal;
                performRowMAN["ModDietz"] = modifiedDietzMAN.ModDietz;
                PerformanceTableMAN.Rows.Add(performRowMAN);
            }

            return CapitalFlowSummaryReport;


        }

        public void OnGetDataCapitalFlowDataSetCorporate(PerfReportDS data, string clientType, string clientID, string clientName, IdentityCM Parent, ICMBroker Broker, CorporateEntity Entity)
        {

            DataTable clientSummaryTable = data.Tables[CapitalReportDS.CLIENTSUMMARYTABLE];
            DataTable contactTable = data.Tables[CapitalReportDS.CONTACTTABLE];

            DataRow clientSummaryRow = clientSummaryTable.NewRow();
            DataRow contactRow = contactTable.NewRow();

            clientSummaryRow[CapitalReportDS.CLIENTID] = clientID;
            clientSummaryRow[CapitalReportDS.CLIENTNAME] = clientName;
            clientSummaryRow[CapitalReportDS.CIENTABN] = Entity.ABN;
            clientSummaryRow[CapitalReportDS.CLIENTACN] = Entity.ACN;
            clientSummaryRow[CapitalReportDS.CLIENTTYPE] = clientType;

            if (Parent != null && Parent.Clid != Guid.Empty && Parent.Csid != Guid.Empty)
            {
                AdvisorCM adviserUnit = Broker.GetCMImplementation(Parent.Clid, Parent.Csid) as AdvisorCM;
                clientSummaryRow[HoldingRptDataSet.ADVISERFULLNAME] = adviserUnit.AdvisorEntity.FullName;
                Broker.ReleaseBrokerManagedComponent(adviserUnit);
            }

            clientSummaryRow[CapitalReportDS.ADDRESSLINE1] = Entity.Address.BusinessAddress.Addressline1;
            clientSummaryRow[CapitalReportDS.ADDRESSLINE2] = Entity.Address.BusinessAddress.Addressline2;
            clientSummaryRow[CapitalReportDS.SUBURB] = Entity.Address.BusinessAddress.Suburb;
            clientSummaryRow[CapitalReportDS.STATE] = Entity.Address.BusinessAddress.State;
            clientSummaryRow[CapitalReportDS.POSTCODE] = Entity.Address.BusinessAddress.PostCode;
            clientSummaryRow[CapitalReportDS.COUNTRY] = Entity.Address.BusinessAddress.Country;

            CSVInstance csvinstance = new CSVInstance(Broker);
            CorporateCSV _entitycsv = csvinstance.GetEntityConcreteInstance(Entity);
            foreach (var contact in _entitycsv.Contacts)
            {
                contactRow[CapitalReportDS.CONTACT_TITLE] = contact.PersonalTitle;
                contactRow[CapitalReportDS.CONTACT_NAME] = contact.Fullname;
                contactRow[CapitalReportDS.CONTACT_BIZNO] = contact.WorkPhoneNumber;
                contactRow[CapitalReportDS.CONTACT_MOBNO] = contact.MobilePhoneNumber;
                contactRow[CapitalReportDS.CONTACT_HNO] = contact.HomePhoneNumber;
                contactRow[CapitalReportDS.CONTACT_FAXNO] = contact.Facsimile;
                contactRow[CapitalReportDS.CONTACT_EMAIL] = contact.EmailAddress;
            }

            clientSummaryTable.Rows.Add(clientSummaryRow);
            contactTable.Rows.Add(contactRow);

            IOrganization orgCM = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            PerfReportDS perfReportDS = data as PerfReportDS;

            DataTable CapitalSummaryTable = perfReportDS.Tables[CapitalReportDS.CAPITALFLOWSUMMARY];
            DataTable productBreakDownTable = perfReportDS.Tables[CapitalReportDS.PRODUCTBREAKDOWNTABLE];
            DataTable TDBreakDownTable = perfReportDS.Tables[CapitalReportDS.TDBREAKDOWNTABLE];

            DataTable CapitalFlowSummaryReport = ExtractCapitalSummaryTableByCategory(Broker, Entity, perfReportDS, clientID, orgCM);

            if (Entity.ClientManualAssetCollection != null)
            {
                var groupedManualTransactions = Entity.ClientManualAssetCollection.GroupBy(man => man.InvestmentCode);

                foreach (var groupedManualTransaction in groupedManualTransactions)
                {
                    var systemManualAsset = orgCM.ManualAsset.Where(manList => manList.ID == groupedManualTransaction.Key).FirstOrDefault();

                    decimal holdingLatest = 0;
                    decimal holdingEndDate = 0;
                    decimal holdingStartDate = 0;
                    decimal unitholdingEndDate = 0;
                    decimal unitholdingStartDate = 0;

                    double unitPriceLatest = systemManualAsset.ASXSecurity.OrderByDescending(manualHis => manualHis.Date).FirstOrDefault().UnitPrice;
                    double unitPriceStartDate = systemManualAsset.ASXSecurity.OrderByDescending(manualHis => manualHis.Date).FirstOrDefault().UnitPrice;

                    unitholdingStartDate = groupedManualTransaction.Where(manTran => manTran.TransactionDate <= perfReportDS.StartDate).Sum(manTran => manTran.UnitsOnHand);
                    unitholdingEndDate = groupedManualTransaction.Where(manTran => manTran.TransactionDate <= perfReportDS.EndDate).Sum(manTran => manTran.UnitsOnHand);

                    //Calculate Holding based on unit price for each transactiondate
                    foreach (var manualTransaction in groupedManualTransaction)
                    {
                        decimal holdingPerTransaction = 0;
                        double unitPrice = 0;

                        var manualHistory = systemManualAsset.ASXSecurity.Where(manualHis => manualHis.Date <= manualTransaction.TransactionDate).OrderByDescending(manualHis => manualHis.Date).FirstOrDefault();
                        if (manualHistory != null)
                            unitPrice = manualHistory.UnitPrice;

                        holdingPerTransaction = manualTransaction.UnitsOnHand * Convert.ToDecimal(unitPrice);
                        holdingEndDate += holdingPerTransaction;
                    }

                    holdingStartDate = unitholdingStartDate * Convert.ToDecimal(unitPriceStartDate);
                    holdingLatest = unitholdingEndDate * Convert.ToDecimal(unitPriceLatest);

                    perfReportDS.AddCapitalRow("ManualAsset", "ManualAsset", systemManualAsset.AsxCode, systemManualAsset.CompanyName, clientID, holdingLatest, holdingEndDate, holdingStartDate, string.Empty, string.Empty);
                }
            }

            foreach (var accountProcessTaskEntity in Entity.ListAccountProcess)
            {
                if (accountProcessTaskEntity.LinkedEntity != null && accountProcessTaskEntity.LinkedEntity.Clid != Guid.Empty)
                {
                    var asset = orgCM.Assets.Where(var => var.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                    var model = orgCM.Model.Where(mod => mod.ID == accountProcessTaskEntity.ModelID).FirstOrDefault();
                    var linkedAsset = model.Assets.Where(ast => ast.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                    var product = linkedAsset.Products.Where(pro => pro.ID == accountProcessTaskEntity.TaskID).FirstOrDefault();

                    if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.BankAccount)
                    {
                        BankAccountCM bankAccount = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as BankAccountCM;
                        var filteredTransactionsStart = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate <= perfReportDS.StartDate);
                        decimal holdingTotalStart = filteredTransactionsStart.Sum(calc => calc.TotalAmount);

                        var filteredTransactionsEnd = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate <= perfReportDS.EndDate);
                        decimal holdingTotalEnd = filteredTransactionsEnd.Sum(calc => calc.TotalAmount);

                        perfReportDS.AddCapitalRow(asset.Name, model.ServiceType.ToString(), product.Name,
                                                            product.TaskDescription, bankAccount.BankAccountEntity.AccountNumber, holdingTotalEnd, holdingTotalEnd, holdingTotalStart, string.Empty, string.Empty);
                    }
                    else if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.TermDepositAccount)
                    {
                        BankAccountCM bankAccount = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as BankAccountCM;
                        decimal holdingTotalStart = 0;
                        decimal holdingTotalEnd = 0;

                        var filteredTransactions = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate <= perfReportDS.StartDate);
                        var tdGroupsType = filteredTransactions.GroupBy(cashMan => cashMan.TransactionType);

                        foreach (var tdType in tdGroupsType)
                        {
                            var groupByIns = tdType.GroupBy(tdtypetran => tdtypetran.InstitutionID);

                            foreach (var groupins in groupByIns)
                            {
                                var transaction = groupins.FirstOrDefault();
                                decimal holding = groupins.Sum(inssum => inssum.Amount);

                                DataRow tDBreakDownTableRow = TDBreakDownTable.NewRow();
                                perfReportDS.TDBreakDownRow(orgCM.Institution.Where(ins => ins.ID == groupins.Key).FirstOrDefault().Name, model.ServiceType.ToString(), tdType.Key, transaction.AdministrationSystem, holding, bankAccount.BankAccountEntity.InstitutionID.ToString(), tDBreakDownTableRow);
                                TDBreakDownTable.Rows.Add(tDBreakDownTableRow);

                                holdingTotalStart += holding;
                            }
                        }

                        filteredTransactions = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate <= perfReportDS.EndDate);
                        tdGroupsType = filteredTransactions.GroupBy(cashMan => cashMan.TransactionType);

                        foreach (var tdType in tdGroupsType)
                        {
                            var groupByIns = tdType.GroupBy(tdtypetran => tdtypetran.InstitutionID);

                            foreach (var groupins in groupByIns)
                            {
                                var transaction = groupins.FirstOrDefault();
                                decimal holding = groupins.Sum(inssum => inssum.Amount);

                                DataRow tDBreakDownTableRow = TDBreakDownTable.NewRow();
                                perfReportDS.TDBreakDownRow(orgCM.Institution.Where(ins => ins.ID == groupins.Key).FirstOrDefault().Name, model.ServiceType.ToString(), tdType.Key, transaction.AdministrationSystem, holding, bankAccount.BankAccountEntity.InstitutionID.ToString(), tDBreakDownTableRow);
                                TDBreakDownTable.Rows.Add(tDBreakDownTableRow);

                                holdingTotalEnd += holding;
                            }
                        }

                        perfReportDS.AddCapitalRow(asset.Name, model.ServiceType.ToString(), product.Name,
                                                            product.TaskDescription, bankAccount.BankAccountEntity.AccountNumber, holdingTotalEnd, holdingTotalEnd, holdingTotalStart, string.Empty, bankAccount.BankAccountEntity.InstitutionID.ToString());
                    }
                    else if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.DesktopBrokerAccount)
                    {
                        DesktopBrokerAccountCM desktopBrokerAccountCM = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid,
                                                                                    accountProcessTaskEntity.LinkedEntity.Csid) as DesktopBrokerAccountCM;
                        decimal presentHoldingTotal = 0;
                        decimal endHoldingTotal = 0;
                        decimal startHoldingTotal = 0;

                        //If it is linked with product securities for split type calculation
                        if (product.ProductSecuritiesId != Guid.Empty)
                        {
                            var productSecurity = orgCM.ProductSecurities.Where(proseclist => proseclist.ID == product.ProductSecuritiesId).FirstOrDefault();
                            var linkedAssets = model.Assets.Where(ast => ast.ID == accountProcessTaskEntity.AssetID && accountProcessTaskEntity.ModelID == model.ID);
                            double totalPercentage = HoldingRptDataSet.CalculateTotalPercentageForSplit(linkedAssets);

                            foreach (var productSecurityDetail in productSecurity.Details)
                            {
                                var asxBroker = orgCM.Securities.Where(sec => sec.ID == productSecurityDetail.SecurityCodeId).FirstOrDefault();
                                var filteredHoldingTransactionsByStartDate = desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Where(holdTrans =>
                                                                                                              holdTrans.TradeDate <= perfReportDS.StartDate);
                                var filteredHoldingTransactionsStartDate = filteredHoldingTransactionsByStartDate.Where(holdTrans =>
                                                                                                              holdTrans.InvestmentCode == asxBroker.AsxCode);

                                var filteredHoldingTransactionsByEndDate = desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Where(holdTrans =>
                                                                                                              holdTrans.TradeDate <= perfReportDS.EndDate);
                                var filteredHoldingTransactionsEndDate = filteredHoldingTransactionsByEndDate.Where(holdTrans =>
                                                                                                              holdTrans.InvestmentCode == asxBroker.AsxCode);

                                var count = filteredHoldingTransactionsByEndDate.Count();

                                if (count > 0)
                                {
                                    totalPercentage = product.SharePercentage;

                                    foreach (var linkedasst in linkedAssets)
                                    {
                                        var otherProducts = linkedasst.Products.Where(linkedAsstPro => linkedAsstPro.ProductSecuritiesId != productSecurity.ID);
                                        foreach (var OtherproductSecurity in otherProducts)
                                        {
                                            var OtherproductSecurityDetails = orgCM.ProductSecurities.Where(otherpro => otherpro.ID == OtherproductSecurity.ProductSecuritiesId).FirstOrDefault();

                                            if (OtherproductSecurityDetails != null)
                                            {
                                                var otherAsxBroker = OtherproductSecurityDetails.Details.Where(otherProDetail => otherProDetail.SecurityCodeId == asxBroker.ID);

                                                if (otherAsxBroker.Count() > 0)
                                                    totalPercentage += OtherproductSecurity.SharePercentage;
                                            }
                                        }
                                    }

                                    decimal unitPriceStart = 0;
                                    decimal unitPriceEnd = 0;

                                    var hisSecurityStart = asxBroker.ASXSecurity.Where(his => his.Date <= perfReportDS.StartDate).OrderByDescending(secpri => secpri.Date).FirstOrDefault();
                                    decimal startHoldingUnits = filteredHoldingTransactionsStartDate.Sum(filteredHolding => filteredHolding.Units);

                                    if (hisSecurityStart != null)
                                        unitPriceStart = Convert.ToDecimal(hisSecurityStart.UnitPrice);

                                    decimal startHoldingValue = startHoldingUnits * unitPriceStart;

                                    var hisSecurityLatest = asxBroker.ASXSecurity.Where(his => his.Date <= perfReportDS.EndDate).OrderByDescending(secpri => secpri.Date).FirstOrDefault();
                                    decimal endHoldingUnits = filteredHoldingTransactionsEndDate.Sum(filteredHolding => filteredHolding.Units);

                                    if (hisSecurityLatest != null)
                                        unitPriceEnd = Convert.ToDecimal(hisSecurityLatest.UnitPrice);

                                    decimal latestHoldingValue = endHoldingUnits * unitPriceEnd;

                                    decimal endHoldingValue = filteredHoldingTransactionsEndDate.Sum(filteredHolding => filteredHolding.NetValue);

                                    startHoldingValue = startHoldingValue * Convert.ToDecimal((product.SharePercentage / totalPercentage));
                                    latestHoldingValue = latestHoldingValue * Convert.ToDecimal((product.SharePercentage / totalPercentage));
                                    endHoldingValue = endHoldingValue * Convert.ToDecimal((product.SharePercentage / totalPercentage));

                                    perfReportDS.AddProductDetailRow(product, unitPriceEnd, asxBroker.AsxCode, endHoldingUnits, endHoldingUnits,
                                                                           startHoldingUnits, latestHoldingValue, startHoldingValue, endHoldingValue, model.ServiceType.ToString());

                                    presentHoldingTotal += latestHoldingValue;
                                    endHoldingTotal += endHoldingValue;
                                    startHoldingTotal += startHoldingValue;
                                }
                            }
                        }
                        else
                        {
                            var filteredHoldingTransactionsByStartDate = desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Where(holdTrans =>
                                                                                                              holdTrans.TradeDate <= perfReportDS.StartDate);

                            var filteredHoldingTransactionsByEndDate = desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Where(holdTrans =>
                                                                                  holdTrans.TradeDate <= perfReportDS.EndDate);

                            var groupedSecurities = filteredHoldingTransactionsByEndDate.GroupBy(holdTran => holdTran.InvestmentCode);

                            foreach (var security in groupedSecurities)
                            {
                                decimal totalSecHoldingUnits = security.Sum(sec => sec.Units);
                                var asxBroker = orgCM.Securities.Where(sec => sec.AsxCode == security.Key).FirstOrDefault();


                                var hisSecurityStart = asxBroker.ASXSecurity.Where(his => his.Date <= perfReportDS.StartDate).OrderByDescending(secpri => secpri.Date).FirstOrDefault();
                                decimal startHoldingUnits = filteredHoldingTransactionsByStartDate.Sum(filteredHolding => filteredHolding.Units);

                                decimal startUnitPrice = 0;
                                decimal endUnitPrice = 0;

                                if (hisSecurityStart != null)
                                    startUnitPrice = Convert.ToDecimal(hisSecurityStart.UnitPrice);

                                decimal startHoldingValue = startHoldingUnits * startUnitPrice;

                                var hisSecurityLatest = asxBroker.ASXSecurity.Where(his => his.Date <= perfReportDS.EndDate).OrderByDescending(secpri => secpri.Date).FirstOrDefault();

                                if (hisSecurityLatest != null)
                                    endUnitPrice = Convert.ToDecimal(hisSecurityLatest.UnitPrice);

                                decimal endHoldingUnits = filteredHoldingTransactionsByEndDate.Where(tran => tran.InvestmentCode == security.Key).Sum(filteredHolding => filteredHolding.Units);
                                decimal latestHoldingValue = endHoldingUnits * endUnitPrice;

                                decimal endHoldingValue = filteredHoldingTransactionsByEndDate.Where(tran => tran.InvestmentCode == security.Key).Sum(filteredHolding => filteredHolding.NetValue);

                                perfReportDS.AddProductDetailRow(product, endUnitPrice, asxBroker.AsxCode, endHoldingUnits, endHoldingUnits,
                                                                       startHoldingUnits, latestHoldingValue, startHoldingValue, endHoldingValue, model.ServiceType.ToString());

                                presentHoldingTotal += latestHoldingValue;
                                endHoldingTotal += endHoldingValue;
                                startHoldingTotal += startHoldingValue;
                            }
                        }

                        perfReportDS.AddCapitalRow(asset.Name, model.ServiceType.ToString(), product.Name, product.TaskDescription, desktopBrokerAccountCM.DesktopBrokerAccountEntity.AccountNumber
                                                                                                , presentHoldingTotal, endHoldingTotal, startHoldingTotal, product.ID.ToString(), string.Empty);
                    }
                    else if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.ManagedInvestmentSchemesAccount)
                    {
                        ManagedInvestmentSchemesAccountCM managedInvestmentSchemesAccountCM = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as ManagedInvestmentSchemesAccountCM;
                        var linkedMISAccount = managedInvestmentSchemesAccountCM.ManagedInvestmentSchemesAccountEntity.FundAccounts.Where(fa => product.TaskDescription.Contains(fa.Code)).FirstOrDefault();

                        var filteredHoldingTransactionsByStartDate = linkedMISAccount.FundTransactions.Where(cliFundTrans => cliFundTrans.TradeDate <= perfReportDS.StartDate);
                        var filteredHoldingTransactionsByEndDate = linkedMISAccount.FundTransactions.Where(cliFundTrans => cliFundTrans.TradeDate <= perfReportDS.EndDate);

                        var clientFundTransactionsStart = filteredHoldingTransactionsByStartDate.Where(cliFundTrans => cliFundTrans.ClientID == clientID);
                        decimal totalSharesStart = clientFundTransactionsStart.Sum(ft => ft.Shares);

                        var clientFundTransactionsEnd = filteredHoldingTransactionsByEndDate.Where(cliFundTrans => cliFundTrans.ClientID == clientID);
                        decimal totalSharesEnd = clientFundTransactionsEnd.Sum(ft => ft.Shares);

                        var linkedmissec = orgCM.Securities.Where(missec => missec.AsxCode == linkedMISAccount.Code).FirstOrDefault();

                        decimal currentValue = 0;
                        decimal startValue = 0;
                        decimal endValue = 0;

                        if (linkedmissec != null)
                        {
                            decimal startUnitPrice = 0;
                            decimal endUnitPrice = 0;

                            var latestMisPriceObjectStart = linkedmissec.ASXSecurity.Where(mishis => mishis.Date <= perfReportDS.StartDate).OrderByDescending(mishis => mishis.Date).FirstOrDefault();
                            var latestMisPriceObjectEnd = linkedmissec.ASXSecurity.Where(mishis => mishis.Date <= perfReportDS.EndDate).OrderByDescending(mishis => mishis.Date).FirstOrDefault();

                            if (latestMisPriceObjectStart != null)
                                startUnitPrice = Convert.ToDecimal(latestMisPriceObjectStart.UnitPrice);

                            if (latestMisPriceObjectEnd != null)
                                endUnitPrice = Convert.ToDecimal(latestMisPriceObjectEnd.UnitPrice);

                            startValue = totalSharesStart * startUnitPrice;
                            currentValue = totalSharesEnd * endUnitPrice;

                            endValue = clientFundTransactionsEnd.Sum(tran => tran.Amount);

                            perfReportDS.AddProductDetailRow(product, endUnitPrice, linkedmissec.AsxCode, totalSharesEnd, totalSharesEnd, totalSharesStart,
                                                                                                currentValue, startValue, endValue, model.ServiceType.ToString());
                        }

                        perfReportDS.AddCapitalRow(asset.Name, model.ServiceType.ToString(), product.Name, product.TaskDescription, clientID, currentValue, endValue, startValue, product.ID.ToString(), string.Empty);
                    }
                }
            }

            GetBankAccountDataSet(orgCM, Entity.ListAccountProcess, Broker, data);
            GetASXTransactionDS(orgCM, Entity.ListAccountProcess, Broker, data);
            Broker.ReleaseBrokerManagedComponent(orgCM);
        }

        public void GetBankAccountDataSet(IOrganization orgCM, List<AccountProcessTaskEntity> ListAccountProcess, ICMBroker Broker, PerfReportDS data)
        {
            foreach (var accountProcessTaskEntity in ListAccountProcess)
            {
                if (accountProcessTaskEntity.LinkedEntity != null && accountProcessTaskEntity.LinkedEntity.Clid != Guid.Empty)
                {
                    var asset = orgCM.Assets.Where(var => var.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                    var model = orgCM.Model.Where(mod => mod.ID == accountProcessTaskEntity.ModelID).FirstOrDefault();
                    var linkedAsset = model.Assets.Where(ast => ast.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                    var product = linkedAsset.Products.Where(pro => pro.ID == accountProcessTaskEntity.TaskID).FirstOrDefault();

                    if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.BankAccount)
                    {
                        BankAccountCM bankAccount = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as BankAccountCM;

                        if (bankAccount.BankAccountEntity.CashManagementTransactions.Count > 0)
                        {

                            var filteredTransactions = bankAccount.BankAccountEntity.CashManagementTransactions.Where(tran => tran.TransactionDate <= data.EndDate && tran.TransactionDate >= data.StartDate);

                            DataTable dt = data.Tables[CapitalReportDS.BANKTRANSACTIONSTABLE];

                            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashManagementEntity in filteredTransactions )
                            {
                                DataRow row = dt.NewRow(); 

                                row[CapitalReportDS.BSB] = bankAccount.BankAccountEntity.BSB;
                                row[CapitalReportDS.ACCOUNTTYPE] = bankAccount.BankAccountEntity.AccoutType.ToUpper();
                                row[CapitalReportDS.ACCOUNTNO] = bankAccount.BankAccountEntity.AccountNumber;
                                row[CapitalReportDS.ACCOUNTNAME] = bankAccount.BankAccountEntity.Name;
                                row[CapitalReportDS.AMOUNTTOTAL] = cashManagementEntity.TotalAmount;
                                row[CapitalReportDS.BANKAMOUNT] = cashManagementEntity.Amount;
                                row[CapitalReportDS.BANKADJUSTMENT] = cashManagementEntity.Adjustment;
                                row[CapitalReportDS.BANKTRANSDATE] = cashManagementEntity.TransactionDate;

                                row[CapitalReportDS.IMPTRANTYPE] = cashManagementEntity.ImportTransactionType;
                                row[CapitalReportDS.SYSTRANTYPE] = cashManagementEntity.SystemTransactionType;
                                row[CapitalReportDS.CATEGORY] = cashManagementEntity.Category;
                                row[CapitalReportDS.COMMENT] = cashManagementEntity.Comment;

                                dt.Rows.Add(row);
                            }

                            data.Merge(dt, false, MissingSchemaAction.Add);
                        }
                    }
                }
            }
        }

        public void GetASXTransactionDS(IOrganization orgCM, List<AccountProcessTaskEntity> ListAccountProcess, ICMBroker Broker, PerfReportDS data)
        {
            foreach (var accountProcessTaskEntity in ListAccountProcess)
            {
                if (accountProcessTaskEntity.LinkedEntity != null && accountProcessTaskEntity.LinkedEntity.Clid != Guid.Empty)
                {
                    var asset = orgCM.Assets.Where(var => var.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                    var model = orgCM.Model.Where(mod => mod.ID == accountProcessTaskEntity.ModelID).FirstOrDefault();
                    var linkedAsset = model.Assets.Where(ast => ast.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                    var product = linkedAsset.Products.Where(pro => pro.ID == accountProcessTaskEntity.TaskID).FirstOrDefault();

                    if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.DesktopBrokerAccount)
                    {
                        DesktopBrokerAccountCM desktopBrokerAccountCM = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as DesktopBrokerAccountCM;

                        if (desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Count > 0)
                        {

                            DataTable dt = data.Tables[CapitalReportDS.ASXTRANSTABLE];

                            var filteredTransactions = desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Where(tran => tran.TradeDate <= data.EndDate && tran.TradeDate >= data.StartDate);

                            foreach (HoldingTransactions holdingTransactions in desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions)
                            {
                                DataRow row = dt.NewRow();

                                row[CapitalReportDS.NAME] = desktopBrokerAccountCM.DesktopBrokerAccountEntity.Name;
                                row[CapitalReportDS.ACCOUNTNO] = desktopBrokerAccountCM.DesktopBrokerAccountEntity.AccountNumber;
                                row[CapitalReportDS.FPSID] = holdingTransactions.FPSInstructionID;
                                row[CapitalReportDS.INVESMENTCODE] = holdingTransactions.InvestmentCode;
                                row[CapitalReportDS.TRANSACTIONTYPE] = holdingTransactions.TransactionType;
                                row[CapitalReportDS.TRADEDATE] = holdingTransactions.TradeDate;

                                if(holdingTransactions.SettlementDate != null)
                                    row[CapitalReportDS.SETTLEDATE] = holdingTransactions.SettlementDate;
                                else
                                    row[CapitalReportDS.SETTLEDATE] = holdingTransactions.TradeDate;
                                
                                row[CapitalReportDS.UNITS] = holdingTransactions.Units;
                                row[CapitalReportDS.GROSSVALUE] = holdingTransactions.GrossValue;
                                row[CapitalReportDS.BROKERAGEAMOUNT] = holdingTransactions.BrokerageAmount;
                                row[CapitalReportDS.CHARGES] = holdingTransactions.Charges;
                                row[CapitalReportDS.BROKERGST] = holdingTransactions.BrokerageGST;
                                row[CapitalReportDS.BROKERAGEAMOUNT] = holdingTransactions.BrokerageAmount;
                                row[CapitalReportDS.NETVALUE] = holdingTransactions.NetValue;
                                row[CapitalReportDS.NARRATIVE] = holdingTransactions.Narrative;
                                
                                dt.Rows.Add(row);
                            }

                            data.Merge(dt, false, MissingSchemaAction.Add);
                        }
                    }
                }
            }
        }

        private DataTable ExtractCapitalSummaryTableByCategory(ICMBroker Broker, CorporateEntity Entity, PerfReportDS perfReportDS, string ClientID, IOrganization orgCM)
        {
            DataTable CapitalFlowSummaryReport = perfReportDS.Tables[CapitalReportDS.CAPITALFLOWSUMMARYCAT];
            
            ObservableCollection<ModifiedDietz> modifiedDietzCol = new ObservableCollection<ModifiedDietz>();
            ObservableCollection<ModifiedDietz> modifiedDietzDIYCol = new ObservableCollection<ModifiedDietz>();
            ObservableCollection<ModifiedDietz> modifiedDietzDIWMCol = new ObservableCollection<ModifiedDietz>();
            ObservableCollection<ModifiedDietz> modifiedDietzDIFMCol = new ObservableCollection<ModifiedDietz>();
            ObservableCollection<ModifiedDietz> modifiedDietzMANCol = new ObservableCollection<ModifiedDietz>();

            int nMonthDiff_FirstToFirst = perfReportDS.EndDate.Month - perfReportDS.StartDate.Month + ((perfReportDS.EndDate.Year - perfReportDS.StartDate.Year) * 12);
            double dMonthDiff = (double)nMonthDiff_FirstToFirst + (perfReportDS.EndDate - perfReportDS.StartDate.AddMonths(nMonthDiff_FirstToFirst)).TotalDays / (double)DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month);

            for (int monthCounter = 0; monthCounter <= dMonthDiff; monthCounter++)
            {
                CapitalMovementByCategoryTotal capitalMovementByCategoryTotal = new CapitalMovementByCategoryTotal();
                ModifiedDietz modifiedDietz = new CM.ModifiedDietz();
                ModifiedDietz modifiedDietzDIY = new CM.ModifiedDietz();
                ModifiedDietz modifiedDietzDIWM = new CM.ModifiedDietz();
                ModifiedDietz modifiedDietzDIFM = new CM.ModifiedDietz();
                ModifiedDietz modifiedDietzMAN = new CM.ModifiedDietz();

                DateTime startdate = FirstDayOfMonthFromDateTime(perfReportDS.StartDate.AddMonths(monthCounter));
                DateTime endDate = LastDayOfMonthFromDateTime(perfReportDS.StartDate.AddMonths(monthCounter));

                ManualTransactions(ClientID, Entity.ClientManualAssetCollection, orgCM, perfReportDS, capitalMovementByCategoryTotal, startdate, endDate, modifiedDietz, modifiedDietzMAN);
                GetTransactionsInfoFromLinkProcess(ClientID, Broker, Entity.ListAccountProcess, orgCM, perfReportDS, capitalMovementByCategoryTotal, startdate, endDate, modifiedDietz, modifiedDietzDIY, modifiedDietzDIWM, modifiedDietzDIFM);

                foreach(Oritax.TaxSimp.Common.IdentityCMDetail desktopBrokerAccounts in Entity.DesktopBrokerAccounts)
                {
                    DesktopBrokerAccountCM desktopBrokerAccountCM = Broker.GetCMImplementation(desktopBrokerAccounts.Clid,
                                                                        desktopBrokerAccounts.Csid) as DesktopBrokerAccountCM;
                    var filteredTransactionsByMonth = desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Where(tran => tran.TradeDate <= endDate && tran.TradeDate >= startdate);

                    foreach (HoldingTransactions holdingTransactions in filteredTransactionsByMonth)
                    {
                        if(holdingTransactions.Type == "RA")
                            modifiedDietz.AddModDietzEntity(holdingTransactions.NetValue * -1, holdingTransactions.TradeDate, "ASX");
                        else
                            modifiedDietz.AddModDietzEntity(holdingTransactions.NetValue, holdingTransactions.TradeDate, "ASX");

                        if(holdingTransactions.Type == "RJ" || holdingTransactions.Type == "AJ")
                        {
                            decimal unitprice = 0; 

                            var unitPriceObj = orgCM.Securities.Where(sec => sec.AsxCode == holdingTransactions.InvestmentCode).FirstOrDefault().ASXSecurity.Where(his => his.Date <= holdingTransactions.TradeDate).OrderByDescending(his => his.Date).FirstOrDefault();

                            if (unitPriceObj != null)
                                unitprice = Convert.ToDecimal(unitPriceObj.UnitPrice);

                            holdingTransactions.NetValue = holdingTransactions.Units * unitprice;

                            if (holdingTransactions.Type == "AJ")
                                holdingTransactions.NetValue = holdingTransactions.NetValue * -1; 
                        }
                    }

                    decimal transferInASX = filteredTransactionsByMonth.Where(tran => tran.Type == "AT").Sum(tran => tran.NetValue);
                    decimal transferOutASX = filteredTransactionsByMonth.Where(tran => tran.Type == "RT").Sum(tran => tran.NetValue);
                    decimal adjustmentUp = filteredTransactionsByMonth.Where(tran => tran.Type == "RJ").Sum(tran => tran.NetValue);
                    decimal adjustmentDown = filteredTransactionsByMonth.Where(tran => tran.Type == "AJ").Sum(tran => tran.NetValue);
                    decimal sell = filteredTransactionsByMonth.Where(tran => tran.Type == "RA").Sum(tran => tran.NetValue);
                    decimal buyPurchase = filteredTransactionsByMonth.Where(tran => tran.Type == "AN").Sum(tran => tran.NetValue);

                    capitalMovementByCategoryTotal.TransferInOutTotal += transferInASX;
                    capitalMovementByCategoryTotal.TransferInOutTotal += transferOutASX;
                    capitalMovementByCategoryTotal.TransferInOutTotal += adjustmentUp;
                    capitalMovementByCategoryTotal.TransferInOutTotal += adjustmentDown;
                    capitalMovementByCategoryTotal.ApplicationRedTotal += (sell*-1);
                    capitalMovementByCategoryTotal.ApplicationRedTotal += buyPurchase;
                }

                DataTable dt = new DataTable();
                dt.Columns.Add("NetValue");
                dt.Columns.Add("WCF");
                dt.Columns.Add("TYPE");
                dt.Columns.Add("TDATE");

                foreach (ModifiedDietzTransactionEntity ModifiedDietzTransactionEntity in modifiedDietz.ModifiedDietzTransactionEntityCol)
                {
                    DataRow dr = dt.NewRow();
                    dr["NetValue"]=ModifiedDietzTransactionEntity.NetValue;
                    dr["WCF"]=ModifiedDietzTransactionEntity.WeightedCf;
                    dr["TYPE"]=ModifiedDietzTransactionEntity.Type;
                    dr["TDATE"] = ModifiedDietzTransactionEntity.TransactionDate;

                    dt.Rows.Add(dr);
                }

                modifiedDietz.CalculatedDietz();
                modifiedDietzDIY.CalculatedDietz();
                modifiedDietzDIWM.CalculatedDietz();
                modifiedDietzDIFM.CalculatedDietz();
                modifiedDietzMAN.CalculatedDietz();

                modifiedDietz.Month = startdate;
                modifiedDietzDIY.Month = startdate;
                modifiedDietzDIWM.Month = startdate;
                modifiedDietzDIFM.Month = startdate;
                modifiedDietzMAN.Month = startdate; 

                DataRow row = CapitalFlowSummaryReport.NewRow();

                row[CapitalReportDS.MONTH] = startdate;
                row[CapitalReportDS.OPENINGBAL] = capitalMovementByCategoryTotal.OpeningBalTotal;
                row[CapitalReportDS.TRANSFEROUT] = capitalMovementByCategoryTotal.TransferInOutTotal;
                row[CapitalReportDS.INCOME] = capitalMovementByCategoryTotal.IncomeTotal;
                row[CapitalReportDS.APPLICATIONREDEMPTION] = capitalMovementByCategoryTotal.ApplicationRedTotal;
                row[CapitalReportDS.TAXINOUT] = capitalMovementByCategoryTotal.TaxInOutTotal;
                row[CapitalReportDS.EXPENSE] = capitalMovementByCategoryTotal.ExpenseTotal;
                row[CapitalReportDS.INTERNALCASHMOVEMENT] = capitalMovementByCategoryTotal.InternalCashMovementTotal;
                row[CapitalReportDS.CLOSINGBALANCE] = capitalMovementByCategoryTotal.ClosingBalanceTotal;
                row[CapitalReportDS.CHANGEININVESTMENTVALUE] = capitalMovementByCategoryTotal.ChangeInInvestTotal;
                row[CapitalReportDS.MODDIETZ] = modifiedDietz.ModDietz;
                row[CapitalReportDS.SUMCF] = modifiedDietz.CfTotal;
                row[CapitalReportDS.SUMWEIGHTCF] = modifiedDietz.WeightedCfTotal;
                CapitalFlowSummaryReport.Rows.Add(row);

                modifiedDietzCol.Add(modifiedDietz);
                modifiedDietzDIYCol.Add(modifiedDietzDIY);
                modifiedDietzDIWMCol.Add(modifiedDietzDIWM);
                modifiedDietzDIFMCol.Add(modifiedDietzDIFM);
                modifiedDietzMANCol.Add(modifiedDietzMAN);
            }

            ModifiedDietz.CalculateChainLinksForModifiedDietzCollection(modifiedDietzCol);
            ModifiedDietz.CalculateChainLinksForModifiedDietzCollection(modifiedDietzDIYCol);
            ModifiedDietz.CalculateChainLinksForModifiedDietzCollection(modifiedDietzDIWMCol);
            ModifiedDietz.CalculateChainLinksForModifiedDietzCollection(modifiedDietzDIFMCol);
            ModifiedDietz.CalculateChainLinksForModifiedDietzCollection(modifiedDietzMANCol);

            SetModifiedDietzData(perfReportDS.Tables[PerfReportDS.OVERALLPERFTABLE], modifiedDietzCol);
            SetModifiedDietzData(perfReportDS.Tables[PerfReportDS.DIYPERFTABLE], modifiedDietzDIYCol);
            SetModifiedDietzData(perfReportDS.Tables[PerfReportDS.DIWMPERFTABLE], modifiedDietzDIWMCol);
            SetModifiedDietzData(perfReportDS.Tables[PerfReportDS.DIFMPERFTABLE], modifiedDietzDIFMCol);
            SetModifiedDietzData(perfReportDS.Tables[PerfReportDS.MANPERFTABLE], modifiedDietzMANCol);

            SetPerformanceSummaryData(perfReportDS.Tables[PerfReportDS.PERFSUMMARYTABLE], modifiedDietzCol, "Overall");
            SetPerformanceSummaryData(perfReportDS.Tables[PerfReportDS.PERFSUMMARYTABLE], modifiedDietzDIYCol, "DO IT YOURSELF");
            SetPerformanceSummaryData(perfReportDS.Tables[PerfReportDS.PERFSUMMARYTABLE], modifiedDietzDIWMCol, "DO IT WITH ME");
            SetPerformanceSummaryData(perfReportDS.Tables[PerfReportDS.PERFSUMMARYTABLE], modifiedDietzDIFMCol, "DO IT FOR ME");
            SetPerformanceSummaryData(perfReportDS.Tables[PerfReportDS.PERFSUMMARYTABLE], modifiedDietzMANCol, "MANUAL ASSETS");

            return CapitalFlowSummaryReport;
        }

        private void SetModifiedDietzData(DataTable PerformanceTable, ObservableCollection<ModifiedDietz> modifiedDietzCol)
        {
            foreach (ModifiedDietz modifiedDietz in modifiedDietzCol)
            {
                DataRow performRow = PerformanceTable.NewRow();
                performRow[CapitalReportDS.MONTH] = modifiedDietz.Month;
                performRow[CapitalReportDS.OPENINGBAL] = modifiedDietz.OpeningBalTotal;
                performRow[CapitalReportDS.CLOSINGBALANCE] = modifiedDietz.ClosingBalanceTotal;
                performRow[PerfReportDS.SUMWEIGHTCF] = modifiedDietz.WeightedCfTotal;
                performRow[PerfReportDS.SUMCF] = modifiedDietz.CfTotal;
                performRow[PerfReportDS.MODDIETZ] = modifiedDietz.ModDietz;
                performRow[PerfReportDS.CHAINLINKDATA] = modifiedDietz.ChainLinkData;

                PerformanceTable.Rows.Add(performRow);
            }
        }

        private void SetPerformanceSummaryData(DataTable PerformanceSummaryTable, ObservableCollection<ModifiedDietz> modifiedDietzCol, string desc)
        {
            string oneMonth = "-";
            string threeMonth = "-";
            string sixMonth = "-";
            string oneYear = "-";
            string twoYear = "-";
            string threeYear = "-";
            string fiveYear = "-";

            var oneMonthData = modifiedDietzCol.OrderByDescending(mod => mod.Month).FirstOrDefault();

            if (oneMonthData != null)
            {
                oneMonth = oneMonthData.ChainLinkData.ToString("P");

                var threeMonthData = modifiedDietzCol.Where(mod => mod.Month == oneMonthData.Month.AddMonths(-3)).FirstOrDefault();
                if (threeMonthData != null)
                    threeMonth = threeMonthData.ChainLinkData.ToString("P");

                var sixMonthData = modifiedDietzCol.Where(mod => mod.Month == oneMonthData.Month.AddMonths(-6)).FirstOrDefault();
                if (sixMonthData != null)
                    sixMonth = sixMonthData.ChainLinkData.ToString("P");

                var oneYearData = modifiedDietzCol.Where(mod => mod.Month == oneMonthData.Month.AddYears(-1)).FirstOrDefault();
                if (oneYearData != null)
                    oneYear = oneYearData.ChainLinkData.ToString("P");

                var twoYearData = modifiedDietzCol.Where(mod => mod.Month == oneMonthData.Month.AddYears(-2)).FirstOrDefault();
                if (twoYearData != null)
                    twoYear = twoYearData.ChainLinkData.ToString("P");

                var threeYearData = modifiedDietzCol.Where(mod => mod.Month == oneMonthData.Month.AddYears(-3)).FirstOrDefault();
                if (threeYearData != null)
                    threeYear = threeYearData.ChainLinkData.ToString("P");

                var fiveYearData = modifiedDietzCol.Where(mod => mod.Month == oneMonthData.Month.AddYears(-5)).FirstOrDefault();
                if (fiveYearData != null)
                    fiveYear = fiveYearData.ChainLinkData.ToString("P");
            }
                
            DataRow performRow = PerformanceSummaryTable.NewRow();
            performRow[PerfReportDS.DESCRIPTION] = desc;
            performRow[PerfReportDS.ONEMONTH] = oneMonth;
            performRow[PerfReportDS.THREEMONTH] = threeMonth;
            performRow[PerfReportDS.SIXMONTH] = sixMonth;
            performRow[PerfReportDS.TWELVEMONTH] = oneYear;
            performRow[PerfReportDS.ONEYEAR] = oneYear;
            performRow[PerfReportDS.TWOYEAR] = twoYear;
            performRow[PerfReportDS.THREEYEAR] = threeYear;
            performRow[PerfReportDS.FIVEYEAR] = fiveYear;
            PerformanceSummaryTable.Rows.Add(performRow);
        }

        private static void GetTransactionsInfoFromLinkProcess(string clientID, ICMBroker Broker, List<AccountProcessTaskEntity> ListAccountProcess, IOrganization orgCM, PerfReportDS capitalReportDS, CapitalMovementByCategoryTotal capitalMovementByCategoryTotal, DateTime startDate, DateTime endDate, ModifiedDietz modifiedDietz, ModifiedDietz modifiedDietzDIY, ModifiedDietz modifiedDietzDIWM, ModifiedDietz modifiedDietzDIFM)
        {
            foreach (var accountProcessTaskEntity in ListAccountProcess)
            {
                if (accountProcessTaskEntity.LinkedEntity != null && accountProcessTaskEntity.LinkedEntity.Clid != Guid.Empty)
                {
                    var asset = orgCM.Assets.Where(var => var.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                    var model = orgCM.Model.Where(mod => mod.ID == accountProcessTaskEntity.ModelID).FirstOrDefault();
                    var linkedAsset = model.Assets.Where(ast => ast.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                    var product = linkedAsset.Products.Where(pro => pro.ID == accountProcessTaskEntity.TaskID).FirstOrDefault();

                    if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.BankAccount)
                        BankAccountTransctions(Broker, capitalReportDS, accountProcessTaskEntity, asset, model, product, capitalMovementByCategoryTotal, startDate, endDate, modifiedDietz,modifiedDietzDIY,modifiedDietzDIWM,modifiedDietzDIFM,orgCM);
                    else if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.TermDepositAccount)
                        TDTransactions(Broker, orgCM, capitalReportDS, accountProcessTaskEntity, asset, model, product, capitalMovementByCategoryTotal, startDate, endDate, modifiedDietz,modifiedDietzDIY,modifiedDietzDIWM,modifiedDietzDIFM);
                    else if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.DesktopBrokerAccount)
                        DesktopBrokerTransactions(Broker, orgCM, capitalReportDS, accountProcessTaskEntity, asset, model, product, capitalMovementByCategoryTotal, startDate, endDate, modifiedDietz,modifiedDietzDIY,modifiedDietzDIWM,modifiedDietzDIFM);
                    else if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.ManagedInvestmentSchemesAccount)
                        ManageInvestmentSchemes(clientID, Broker, orgCM, capitalReportDS, accountProcessTaskEntity, asset, model, product, capitalMovementByCategoryTotal, startDate, endDate, modifiedDietz,modifiedDietzDIY,modifiedDietzDIWM,modifiedDietzDIFM);
                }
            }
        }

        private static void ManageInvestmentSchemes(string clientID, ICMBroker Broker, IOrganization orgCM, PerfReportDS capitalReportDS, AccountProcessTaskEntity accountProcessTaskEntity, AssetEntity asset, ModelEntity model, ProductEntity product, CapitalMovementByCategoryTotal capitalMovementByCategoryTotal, DateTime startDate, DateTime endDate, ModifiedDietz modifiedDietz, ModifiedDietz modifiedDietzDIY, ModifiedDietz modifiedDietzDIWM, ModifiedDietz modifiedDietzDIFM)
        {
            ManagedInvestmentSchemesAccountCM managedInvestmentSchemesAccountCM = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as ManagedInvestmentSchemesAccountCM;
            var linkedMISAccount = managedInvestmentSchemesAccountCM.ManagedInvestmentSchemesAccountEntity.FundAccounts.Where(fa => product.TaskDescription.Contains(fa.Code)).FirstOrDefault();

            var filteredHoldingTransactions = linkedMISAccount.FundTransactions.Where(cliFundTrans => cliFundTrans.TradeDate >= startDate && cliFundTrans.TradeDate <= endDate && cliFundTrans.ClientID == clientID);

            decimal buyPurchase = filteredHoldingTransactions.Where(tran => tran.TransactionType == "Purchase").Sum(tran => tran.Amount);
            decimal sell = filteredHoldingTransactions.Where(tran => tran.TransactionType == "Redemption").Sum(tran => tran.Amount);

            var filteredHoldingTransactionsByOpeningDate = linkedMISAccount.FundTransactions.Where(cliFundTrans => cliFundTrans.TradeDate <= startDate.AddDays(-1));
            var clientFundTransactionsOpening = filteredHoldingTransactionsByOpeningDate.Where(cliFundTrans => cliFundTrans.ClientID == clientID);
            decimal totalSharesOpening = clientFundTransactionsOpening.Sum(ft => ft.Shares);
            var linkedmissec = orgCM.Securities.Where(missec => missec.AsxCode == linkedMISAccount.Code).FirstOrDefault();

            decimal openingBalance = 0;

            if (linkedmissec != null)
            {
                var latestMisPriceObject = linkedmissec.ASXSecurity.Where(mishis => mishis.Date <= startDate.AddDays(-1)).OrderByDescending(mishis => mishis.Date).FirstOrDefault();
                decimal unitPrice = 0;
                if (latestMisPriceObject != null)
                    unitPrice = Convert.ToDecimal(latestMisPriceObject.UnitPrice);
                openingBalance = totalSharesOpening * unitPrice;
            }

            var filteredHoldingTransactionsByClosingDate = linkedMISAccount.FundTransactions.Where(cliFundTrans => cliFundTrans.TradeDate <= endDate);
            var clientFundTransactionsCLosing = filteredHoldingTransactionsByClosingDate.Where(cliFundTrans => cliFundTrans.ClientID == clientID);
            decimal totalSharesClosing = clientFundTransactionsCLosing.Sum(ft => ft.Shares);
          
            decimal closingBalance = 0;

            if (linkedmissec != null)
            {
                var latestMisPriceObject = linkedmissec.ASXSecurity.Where(mishis => mishis.Date <= endDate).OrderByDescending(mishis => mishis.Date).FirstOrDefault();
                decimal unitPrice = 0;
                if (latestMisPriceObject != null)
                    unitPrice = Convert.ToDecimal(latestMisPriceObject.UnitPrice);
                closingBalance = totalSharesClosing * unitPrice;
            }

            foreach (MISFundTransactionEntity misFundTransactionEntity in filteredHoldingTransactions)
                modifiedDietz.AddModDietzEntity(misFundTransactionEntity.Amount, misFundTransactionEntity.TradeDate, "MIS");

            modifiedDietz.OpeningBalTotal += openingBalance;
            modifiedDietz.ClosingBalanceTotal += closingBalance;

            capitalMovementByCategoryTotal.OpeningBalTotal += openingBalance;
            capitalMovementByCategoryTotal.ClosingBalanceTotal += closingBalance;
        
            capitalMovementByCategoryTotal.ApplicationRedTotal += buyPurchase;
            capitalMovementByCategoryTotal.ApplicationRedTotal += sell;

            if (model.ServiceType == ServiceTypes.DoItForMe)
            {
                SetServiceTypeModifiedDietz(modifiedDietzDIFM, openingBalance, closingBalance);

                foreach (MISFundTransactionEntity misFundTransactionEntity in filteredHoldingTransactions)
                    modifiedDietzDIFM.AddModDietzEntity(misFundTransactionEntity.Amount, misFundTransactionEntity.TradeDate, "MIS");
            }
            else if (model.ServiceType == ServiceTypes.DoItWithMe)
            {
                SetServiceTypeModifiedDietz(modifiedDietzDIWM, openingBalance, closingBalance);

                foreach (MISFundTransactionEntity misFundTransactionEntity in filteredHoldingTransactions)
                        modifiedDietzDIWM.AddModDietzEntity(misFundTransactionEntity.Amount, misFundTransactionEntity.TradeDate, "MIS");
            }
            else if (model.ServiceType == ServiceTypes.DoItYourSelf)
            {
                SetServiceTypeModifiedDietz(modifiedDietzDIY, openingBalance, closingBalance);

                foreach (MISFundTransactionEntity misFundTransactionEntity in filteredHoldingTransactions)
                    modifiedDietzDIY.AddModDietzEntity(misFundTransactionEntity.Amount, misFundTransactionEntity.TradeDate, "MIS");
            }
        }

        private static void DesktopBrokerTransactions(ICMBroker Broker, IOrganization orgCM, PerfReportDS capitalReportDS, AccountProcessTaskEntity accountProcessTaskEntity, AssetEntity asset, ModelEntity model, ProductEntity product, CapitalMovementByCategoryTotal capitalMovementByCategoryTotal, DateTime startDate, DateTime endDate, ModifiedDietz modifiedDietz, ModifiedDietz modifiedDietzDIY, ModifiedDietz modifiedDietzDIWM, ModifiedDietz modifiedDietzDIFM)
        {
            DesktopBrokerAccountCM desktopBrokerAccountCM = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid,
                                                                        accountProcessTaskEntity.LinkedEntity.Csid) as DesktopBrokerAccountCM;
            decimal totalOpeiningBalance = 0;
            decimal totalClosingBalance = 0;

            if (product.ProductSecuritiesId != Guid.Empty)
            {
                var productSecurity = orgCM.ProductSecurities.Where(proseclist => proseclist.ID == product.ProductSecuritiesId).FirstOrDefault();
                var linkedAssets = model.Assets.Where(ast => ast.ID == accountProcessTaskEntity.AssetID && accountProcessTaskEntity.ModelID == model.ID);
                double totalPercentage = HoldingRptDataSet.CalculateTotalPercentageForSplit(linkedAssets);

                foreach (var productSecurityDetail in productSecurity.Details)
                {
                    var asxBroker = orgCM.Securities.Where(sec => sec.ID == productSecurityDetail.SecurityCodeId).FirstOrDefault();
                    
                    var filteredHoldingTransactionsByOpening = desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Where(holdTrans =>
                                                                                              holdTrans.TradeDate <= startDate.AddDays(-1));

                    var filteredHoldingTransactionsByClosing = desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Where(holdTrans =>
                                                                                  holdTrans.TradeDate <= endDate);

                    var filteredHoldingTransactionsOpening = filteredHoldingTransactionsByOpening.Where(holdTrans =>
                                                                                                  holdTrans.InvestmentCode == asxBroker.AsxCode);

                    var filteredHoldingTransactionsClosing = filteredHoldingTransactionsByClosing.Where(holdTrans =>
                                                                              holdTrans.InvestmentCode == asxBroker.AsxCode);

                    SetProductSecurity(orgCM, product, startDate.AddDays(-1), productSecurity, linkedAssets, ref totalPercentage, asxBroker, filteredHoldingTransactionsOpening, ref totalOpeiningBalance);

                    SetProductSecurity(orgCM, product, endDate, productSecurity, linkedAssets, ref totalPercentage, asxBroker, filteredHoldingTransactionsClosing, ref totalClosingBalance);
                }
            }
            
            else
                CalculateDesktopBrokerWithoutProductSecurity(orgCM, startDate, endDate, desktopBrokerAccountCM, ref totalOpeiningBalance, ref totalClosingBalance);

            capitalMovementByCategoryTotal.OpeningBalTotal += totalOpeiningBalance;
            capitalMovementByCategoryTotal.ClosingBalanceTotal += totalClosingBalance;
            modifiedDietz.OpeningBalTotal += totalOpeiningBalance;
            modifiedDietz.ClosingBalanceTotal += totalClosingBalance;

            var filteredTransactionsByMonth = desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Where(tran => tran.TradeDate <= endDate && tran.TradeDate >= startDate);

            if (model.ServiceType == ServiceTypes.DoItForMe)
            {
                  foreach (HoldingTransactions holdingTransactions in filteredTransactionsByMonth)
                  {
                        if(holdingTransactions.Type == "RA")
                            modifiedDietzDIFM.AddModDietzEntity(holdingTransactions.NetValue * -1, holdingTransactions.TradeDate, "ASX");
                        else
                            modifiedDietzDIFM.AddModDietzEntity(holdingTransactions.NetValue, holdingTransactions.TradeDate, "ASX");
                  }

                SetServiceTypeModifiedDietz(modifiedDietzDIFM, totalOpeiningBalance, totalClosingBalance);
            }
            else if (model.ServiceType == ServiceTypes.DoItWithMe)
            {
                 foreach (HoldingTransactions holdingTransactions in filteredTransactionsByMonth)
                 {
                        if(holdingTransactions.Type == "RA")
                            modifiedDietzDIWM.AddModDietzEntity(holdingTransactions.NetValue * -1, holdingTransactions.TradeDate, "ASX");
                        else
                            modifiedDietzDIWM.AddModDietzEntity(holdingTransactions.NetValue, holdingTransactions.TradeDate, "ASX");
                 }

                SetServiceTypeModifiedDietz(modifiedDietzDIWM, totalOpeiningBalance, totalClosingBalance);
            }
            else if (model.ServiceType == ServiceTypes.DoItYourSelf)
            {
                foreach (HoldingTransactions holdingTransactions in filteredTransactionsByMonth)
                {
                        if(holdingTransactions.Type == "RA")
                            modifiedDietzDIY.AddModDietzEntity(holdingTransactions.NetValue * -1, holdingTransactions.TradeDate, "ASX");
                        else
                            modifiedDietzDIY.AddModDietzEntity(holdingTransactions.NetValue, holdingTransactions.TradeDate, "ASX");

                }

                SetServiceTypeModifiedDietz(modifiedDietzDIY, totalOpeiningBalance, totalClosingBalance);
            }
        }

        private static void SetProductSecurity(IOrganization orgCM, ProductEntity product, DateTime datetime, ProductSecuritiesEntity productSecurity, IEnumerable<AssetEntity> linkedAssets, ref double totalPercentage, SecuritiesEntity asxBroker, IEnumerable<HoldingTransactions> filteredHoldingTransactions, ref decimal totalHolding)
        {
            if (filteredHoldingTransactions.Count() > 0)
            {
                totalPercentage = product.SharePercentage;

                foreach (var linkedasst in linkedAssets)
                {
                    var otherProducts = linkedasst.Products.Where(linkedAsstPro => linkedAsstPro.ProductSecuritiesId != productSecurity.ID);
                    foreach (var OtherproductSecurity in otherProducts)
                    {
                        var OtherproductSecurityDetails = orgCM.ProductSecurities.Where(otherpro => otherpro.ID == OtherproductSecurity.ProductSecuritiesId).FirstOrDefault();

                        if (OtherproductSecurityDetails != null)
                        {
                            var otherAsxBroker = OtherproductSecurityDetails.Details.Where(otherProDetail => otherProDetail.SecurityCodeId == asxBroker.ID);

                            if (otherAsxBroker.Count() > 0)
                                totalPercentage += OtherproductSecurity.SharePercentage;
                        }
                    }
                }

                var hisSecurity = asxBroker.ASXSecurity.Where(secpri => secpri.Date <= datetime).OrderByDescending(secpri => secpri.Date).FirstOrDefault();
                decimal holding = filteredHoldingTransactions.Sum(filteredHolding => filteredHolding.Units);
                decimal unitPrice = 0;
                if (hisSecurity != null)
                    unitPrice = Convert.ToDecimal(hisSecurity.UnitPrice);

                decimal currentValue = holding * unitPrice;
                currentValue = currentValue * Convert.ToDecimal((product.SharePercentage / totalPercentage));
                totalHolding += currentValue;
            }
        }

        private static void CalculateDesktopBrokerWithoutProductSecurity(IOrganization orgCM, DateTime startDate, DateTime endDate, DesktopBrokerAccountCM desktopBrokerAccountCM, ref decimal totalOpeiningBalance, ref decimal totalClosingBalance)
        {
            var filteredHoldingTransactionsByOpening = desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Where(holdTrans =>
                                                                                              holdTrans.TradeDate <= startDate.AddDays(-1));

            var filteredHoldingTransactionsByClosing = desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Where(holdTrans =>
                                                                                  holdTrans.TradeDate <= endDate);


            var groupedSecuritiesOpening = filteredHoldingTransactionsByOpening.GroupBy(holdTran => holdTran.InvestmentCode);

            foreach (var security in groupedSecuritiesOpening)
            {
                decimal totalSecHoldingUnits = security.Sum(sec => sec.Units);
                var asxBroker = orgCM.Securities.Where(sec => sec.AsxCode == security.Key).FirstOrDefault();
                var hisSecurity = asxBroker.ASXSecurity.Where(secpri => secpri.Date <= startDate.AddDays(-1)).OrderByDescending(secpri => secpri.Date).FirstOrDefault();
                decimal unitPrice = 0;

                if (hisSecurity != null)
                    unitPrice = Convert.ToDecimal(hisSecurity.UnitPrice);

                decimal currentValue = totalSecHoldingUnits * unitPrice;
                totalOpeiningBalance += currentValue;
            }

            var groupedSecuritiesClosing = filteredHoldingTransactionsByClosing.GroupBy(holdTran => holdTran.InvestmentCode);

            foreach (var security in groupedSecuritiesClosing)
            {
                decimal totalSecHoldingUnits = security.Sum(sec => sec.Units);
                var asxBroker = orgCM.Securities.Where(sec => sec.AsxCode == security.Key).FirstOrDefault();
                var hisSecurity = asxBroker.ASXSecurity.Where(secpri => secpri.Date <= endDate).OrderByDescending(secpri => secpri.Date).FirstOrDefault();
                decimal unitPrice = 0;

                if (hisSecurity != null)
                    unitPrice = Convert.ToDecimal(hisSecurity.UnitPrice);

                decimal currentValue = totalSecHoldingUnits * unitPrice;
                totalClosingBalance += currentValue;
            }
        }

        private static void TDTransactions(ICMBroker Broker, IOrganization orgCM, PerfReportDS capitalReportDS, AccountProcessTaskEntity accountProcessTaskEntity, AssetEntity asset, ModelEntity model, ProductEntity product, CapitalMovementByCategoryTotal capitalMovementByCategoryTotal, DateTime startDate, DateTime endDate, ModifiedDietz modifiedDietz, ModifiedDietz modifiedDietzDIY, ModifiedDietz modifiedDietzDIWM, ModifiedDietz modifiedDietzDIFM)
        {
            BankAccountCM bankAccount = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as BankAccountCM;
            decimal openingBalance = 0;
            decimal closingBalance = 0;

            var filteredTransactionsOpening = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate <= startDate.AddDays(-1));
            var filteredTransactionsClosing = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate <= endDate);

            var tdGroupsOpeningType = filteredTransactionsOpening.GroupBy(cashMan => cashMan.TransactionType);

            foreach (var tdType in tdGroupsOpeningType)
            {
                var groupByIns = tdType.GroupBy(tdtypetran => tdtypetran.InstitutionID);

                foreach (var groupins in groupByIns)
                {
                    var transaction = groupins.FirstOrDefault();
                    decimal holding = groupins.Sum(inssum => inssum.Amount);
                    openingBalance += holding;
                }
            }

            var tdGroupsClosingType = filteredTransactionsClosing.GroupBy(cashMan => cashMan.TransactionType);

            foreach (var tdType in tdGroupsClosingType)
            {
                var groupByIns = tdType.GroupBy(tdtypetran => tdtypetran.InstitutionID);

                foreach (var groupins in groupByIns)
                {
                    var transaction = groupins.FirstOrDefault();
                    decimal holding = groupins.Sum(inssum => inssum.Amount);
                    closingBalance += holding;
                }
            }

            var filteredTransactions = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate <= endDate && trans.TransactionDate >= startDate);
            decimal application = filteredTransactions.Where(tran => tran.SystemTransactionType == "Application").Sum(tran => tran.TotalAmount);
            decimal redemption = filteredTransactions.Where(tran => tran.SystemTransactionType == "Redemption").Sum(tran => tran.TotalAmount);

            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashManagementEntity in filteredTransactions)
                modifiedDietz.AddModDietzEntity(cashManagementEntity.TotalAmount, cashManagementEntity.TransactionDate, "TD");

            modifiedDietz.OpeningBalTotal += openingBalance;
            modifiedDietz.ClosingBalanceTotal += closingBalance;

            capitalMovementByCategoryTotal.OpeningBalTotal += openingBalance;
            capitalMovementByCategoryTotal.ClosingBalanceTotal += closingBalance;
            capitalMovementByCategoryTotal.ApplicationRedTotal += application;
            capitalMovementByCategoryTotal.ApplicationRedTotal += redemption;

            if (model.ServiceType == ServiceTypes.DoItForMe)
                SetServiceTypeCashModifiedDietz(modifiedDietzDIFM, openingBalance, closingBalance, filteredTransactions);
            else if (model.ServiceType == ServiceTypes.DoItWithMe)
                SetServiceTypeCashModifiedDietz(modifiedDietzDIWM, openingBalance, closingBalance, filteredTransactions);
            else if (model.ServiceType == ServiceTypes.DoItYourSelf)
                SetServiceTypeCashModifiedDietz(modifiedDietzDIY, openingBalance, closingBalance, filteredTransactions);
        }

        private static void BankAccountTransctions(ICMBroker Broker, PerfReportDS capitalReportDS, AccountProcessTaskEntity accountProcessTaskEntity, AssetEntity asset, ModelEntity model, ProductEntity product,
            CapitalMovementByCategoryTotal capitalMovementByCategoryTotal, DateTime startDate, DateTime endDate, ModifiedDietz modifiedDietz, ModifiedDietz modifiedDietzDIY, ModifiedDietz modifiedDietzDIWM, ModifiedDietz modifiedDietzDIFM, IOrganization orgCM)
        {
            BankAccountCM bankAccount = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as BankAccountCM;
            var filteredTransactionsStartDate = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate <= startDate.AddDays(-1));
            var filteredTransactionsEndDat = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate <= endDate);
            decimal openingBalance = filteredTransactionsStartDate.Sum(calc => calc.TotalAmount);
            decimal closingBalance = filteredTransactionsEndDat.Sum(calc => calc.TotalAmount);

            var totalMonthTransactions = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate <= endDate && trans.TransactionDate >= startDate);

            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashManagementEntity in totalMonthTransactions)
                modifiedDietz.AddModDietzEntity(cashManagementEntity.TotalAmount, cashManagementEntity.TransactionDate, "CASH");

            decimal contributions = totalMonthTransactions.Where(tran => tran.Category == "Contribution").Sum(tran => tran.TotalAmount);
            decimal income = totalMonthTransactions.Where(tran => tran.Category == "Income").Sum(tran => tran.TotalAmount);
            decimal transferin = totalMonthTransactions.Where(tran => tran.Category == "Income" && tran.Category == "Transfer In").Sum(tran => tran.TotalAmount);
            decimal internalCashMovementIncome = totalMonthTransactions.Where(tran => tran.Category == "Income" && tran.SystemTransactionType == "Internal Cash Movement").Sum(tran => tran.TotalAmount);

            income = income - transferin - internalCashMovementIncome;

            decimal taxinout = totalMonthTransactions.Where(tran => tran.Category == "TAX").Sum(tran => tran.TotalAmount); ;
            decimal benefitPayment = totalMonthTransactions.Where(tran => tran.Category == "Benefit Payment").Sum(tran => tran.TotalAmount);
            decimal investment = totalMonthTransactions.Where(tran => tran.Category == "Investment").Sum(tran => tran.TotalAmount);

            decimal expense = totalMonthTransactions.Where(tran => tran.Category == "Expense").Sum(tran => tran.TotalAmount);
            decimal transferOut = totalMonthTransactions.Where(tran => tran.Category == "Expense" && tran.SystemTransactionType == "Transfer Out").Sum(tran => tran.TotalAmount);
            decimal internalCashMovementExpense = totalMonthTransactions.Where(tran => tran.Category == "Expense" && tran.SystemTransactionType == "Internal Cash Movement").Sum(tran => tran.TotalAmount);
            expense = expense - transferOut - internalCashMovementExpense;

            capitalMovementByCategoryTotal.TransferInOutTotal += contributions;
            capitalMovementByCategoryTotal.TransferInOutTotal += benefitPayment;
            capitalMovementByCategoryTotal.TransferInOutTotal += transferOut;
            capitalMovementByCategoryTotal.TransferInOutTotal += transferin;

            capitalMovementByCategoryTotal.InternalCashMovementTotal += internalCashMovementIncome;
            capitalMovementByCategoryTotal.InternalCashMovementTotal += internalCashMovementExpense;

            capitalMovementByCategoryTotal.ApplicationRedTotal += investment;

            capitalMovementByCategoryTotal.TaxInOutTotal += taxinout;

            capitalMovementByCategoryTotal.ExpenseTotal += expense;
            capitalMovementByCategoryTotal.ExpenseTotal += taxinout;

            capitalMovementByCategoryTotal.IncomeTotal += income;

            capitalMovementByCategoryTotal.OpeningBalTotal += openingBalance;
            capitalMovementByCategoryTotal.ClosingBalanceTotal += closingBalance;

            modifiedDietz.OpeningBalTotal += openingBalance;
            modifiedDietz.ClosingBalanceTotal += closingBalance;

            if (model.ServiceType == ServiceTypes.DoItForMe)
                SetServiceTypeCashModifiedDietz(modifiedDietzDIFM, openingBalance, closingBalance, totalMonthTransactions);
            else if (model.ServiceType == ServiceTypes.DoItWithMe)
                SetServiceTypeCashModifiedDietz(modifiedDietzDIWM, openingBalance, closingBalance, totalMonthTransactions);
            else if (model.ServiceType == ServiceTypes.DoItYourSelf)
                SetServiceTypeCashModifiedDietz(modifiedDietzDIY, openingBalance, closingBalance, totalMonthTransactions);
        }

        private static void SetServiceTypeCashModifiedDietz(ModifiedDietz modifiedDietz, decimal openingBalance, decimal closingBalance, IEnumerable<Common.CashManagementEntity> totalMonthTransactions)
        {
            modifiedDietz.OpeningBalTotal += openingBalance;
            modifiedDietz.ClosingBalanceTotal += closingBalance;

            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashManagementEntity in totalMonthTransactions)
                modifiedDietz.AddModDietzEntity(cashManagementEntity.TotalAmount, cashManagementEntity.TransactionDate, "CASH");
        }

        private static void SetServiceTypeModifiedDietz(ModifiedDietz modifiedDietz, decimal openingBalance, decimal closingBalance)
        {
            modifiedDietz.OpeningBalTotal += openingBalance;
            modifiedDietz.ClosingBalanceTotal += closingBalance;
        }

        private static void ManualTransactions(string clientID, ObservableCollection<ClientManualAssetEntity> ClientManualAssetCollection, IOrganization orgCM, PerfReportDS capitalReportDS, CapitalMovementByCategoryTotal capitalMovementByCategoryTotal, DateTime startDate, DateTime endDate, ModifiedDietz modifiedDietz, ModifiedDietz modifiedDietzMAN)
        {
            if (ClientManualAssetCollection != null)
            {
                var filteredManualAssetsStartDate = ClientManualAssetCollection.Where(man => man.TransactionDate <= startDate);
                var filteredManualAssetsEndDate = ClientManualAssetCollection.Where(man => man.TransactionDate <= endDate);

                decimal openingBalance = 0;

                foreach (var manualAsset in filteredManualAssetsStartDate)
                {
                    var systemManualAsset = orgCM.ManualAsset.Where(manList => manList.ID == manualAsset.InvestmentCode).FirstOrDefault();
                    var systemManualAssetPriceHistory = systemManualAsset.ASXSecurity.Where(manualHIS => manualHIS.Date <= startDate.AddDays(-1)).OrderByDescending(manualHis => manualHis.Date).FirstOrDefault();
                    if (systemManualAssetPriceHistory != null)
                        openingBalance += manualAsset.UnitsOnHand * Convert.ToDecimal(systemManualAssetPriceHistory.UnitPrice);
                }

                decimal closingBalance = 0;

                foreach (var manualAsset in filteredManualAssetsEndDate)
                {
                    var systemManualAsset = orgCM.ManualAsset.Where(manList => manList.ID == manualAsset.InvestmentCode).FirstOrDefault();
                    var systemManualAssetPriceHistory = systemManualAsset.ASXSecurity.Where(manualHIS => manualHIS.Date <= endDate).OrderByDescending(manualHis => manualHis.Date).FirstOrDefault();
                    if (systemManualAssetPriceHistory != null)
                        closingBalance += manualAsset.UnitsOnHand * Convert.ToDecimal(systemManualAssetPriceHistory.UnitPrice);
                }

                capitalMovementByCategoryTotal.OpeningBalTotal += openingBalance;
                capitalMovementByCategoryTotal.ClosingBalanceTotal += closingBalance;

                var filteredTransactionsByMonth = ClientManualAssetCollection.Where(man => man.TransactionDate <= startDate);

                modifiedDietz.OpeningBalTotal += openingBalance;
                modifiedDietz.ClosingBalanceTotal += closingBalance;

                modifiedDietzMAN.OpeningBalTotal += openingBalance;
                modifiedDietzMAN.ClosingBalanceTotal += closingBalance;
                
                foreach (ClientManualAssetEntity clientManualAssetEntity in filteredTransactionsByMonth)
                {
                   decimal netValue = 0;

                    var systemManualAsset = orgCM.ManualAsset.Where(manList => manList.ID == clientManualAssetEntity.InvestmentCode).FirstOrDefault();
                    var systemManualAssetPriceHistory = systemManualAsset.ASXSecurity.Where(manualHIS => manualHIS.Date <= clientManualAssetEntity.TransactionDate).OrderByDescending(manualHis => manualHis.Date).FirstOrDefault();
                    if (systemManualAssetPriceHistory != null)
                        closingBalance = clientManualAssetEntity.UnitsOnHand * Convert.ToDecimal(systemManualAssetPriceHistory.UnitPrice);
                    modifiedDietz.AddModDietzEntity(netValue, clientManualAssetEntity.TransactionDate, "Manual");
                    modifiedDietzMAN.AddModDietzEntity(netValue, clientManualAssetEntity.TransactionDate, "Manual");
                }
            }
        }
    }

    public class CapitalMovementByCategoryTotal
    {

        public decimal OpeningBalTotal = 0;
        public decimal IncomeTotal = 0;
        public decimal ApplicationRedTotal = 0;
        public decimal TransferInOutTotal = 0;
        public decimal ExpenseTotal = 0;
        public decimal TaxInOutTotal = 0;
        public decimal ClosingBalanceTotal = 0;
        public decimal InternalCashMovementTotal = 0;

        public decimal ChangeInInvestTotal
        {
            get
            {
                return ClosingBalanceTotal - (OpeningBalTotal + IncomeTotal + ApplicationRedTotal + TransferInOutTotal + ExpenseTotal + TaxInOutTotal + InternalCashMovementTotal);
            }
        }
        
    
    }

    public class ModifiedDietz
    {
        DateTime month;
        ObservableCollection<ModifiedDietzTransactionEntity> modifiedDietzTransactionEntityCol = new ObservableCollection<ModifiedDietzTransactionEntity>();
        public decimal OpeningBalTotal;
        public decimal ClosingBalanceTotal;
        public decimal ModDietz;
        decimal chainLinkData;
        
        public decimal ChainLinkData
        {
            get
            {
                return chainLinkData;
            }
            set
            {
                chainLinkData = value;
            }
        }

        public void AddModDietzEntity(decimal netValue, DateTime transactionDate, string type)
        {
            ModifiedDietzTransactionEntity entity = new ModifiedDietzTransactionEntity();
            entity.NetValue = netValue;
            entity.TransactionDate = transactionDate;
            entity.Type = type;
            entity.CalculatedModifiedDietzTransactionEntity();
            this.ModifiedDietzTransactionEntityCol.Add(entity);
        }

        public DateTime Month
        {
            get
            {
                return month;
            }
            set
            {
                this.month = value;
            }
        }

        public decimal WeightedCfTotal
        {
            get
            {
                return modifiedDietzTransactionEntityCol.Sum(dietz => dietz.WeightedCf);
            }
        }

        public decimal CfTotal
        {
            get
            {
                return modifiedDietzTransactionEntityCol.Sum(dietz => dietz.NetValue);
            }
        }

        public ObservableCollection<ModifiedDietzTransactionEntity> ModifiedDietzTransactionEntityCol
        {
            get
            {
                return modifiedDietzTransactionEntityCol;
            }
        }

        public void CalculatedDietz()
        {
            try
            {
                this.ModDietz = (this.ClosingBalanceTotal - this.OpeningBalTotal - this.CfTotal) / (this.OpeningBalTotal + this.WeightedCfTotal);
            }
            catch { this.ModDietz = 0; }
            finally {  }
        }

        public void CalculatedChainLink(decimal previousMontModDietz)
        {
            try
            {
                this.chainLinkData = (previousMontModDietz)*(1+this.ModDietz)-1;
            }
            catch { this.chainLinkData = 0; }
            finally { }
        }

        public static void CalculateChainLinksForModifiedDietzCollection(ObservableCollection<ModifiedDietz> modifiedDietzCol)
        {
            var sortedmodifiedDietzCol = modifiedDietzCol.OrderByDescending(dietz => dietz.Month);

            foreach (ModifiedDietz modifiedDietz in sortedmodifiedDietzCol)
            {
                DateTime previousMonth = modifiedDietz.Month.AddMonths(1);
                var previousMonthModifiedDietz =  sortedmodifiedDietzCol.Where(dietz => dietz.Month == previousMonth).FirstOrDefault();

                if (previousMonthModifiedDietz == null)
                    modifiedDietz.ChainLinkData = modifiedDietz.ModDietz;
                else
                    modifiedDietz.CalculatedChainLink(1 + previousMonthModifiedDietz.ModDietz);
            }
        }

    }

    public class ModifiedDietzTransactionEntity
    {
        DateTime transactionDate;
        decimal weightedCf;
        decimal netValue;
        
        public DateTime TransactionDate
        {
            get
            {
                return transactionDate;
            }
            set
            {
                this.transactionDate = value;
            }

        } 

        public decimal NetValue
        {
            get
            {
                return netValue;
            }
            set
            {
                this.netValue = value;
            }
        }
    

        public string Type;

        public decimal WeightedCf
        {
            get
            {
                return weightedCf;
            }
        }

        public void CalculatedModifiedDietzTransactionEntity()
        {
           int totalDaysInMonth = DateTime.DaysInMonth(transactionDate.Year, transactionDate.Month);
           int daysRemaining = totalDaysInMonth - transactionDate.Day;
           this.weightedCf = (Convert.ToDecimal(daysRemaining) / Convert.ToDecimal(totalDaysInMonth)) * netValue;
        }
    }

}