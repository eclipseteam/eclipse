Declare @AssemblyID varchar(100)
Declare @ComponentID varchar(100)

Declare @AssemblyStrongName varchar(100)
Declare @ImplementationCMClass varchar(100)
Declare @PersisterClass varchar(100)
Declare @CategoryName varchar(100)
set @AssemblyID='986E40E6-19F6-49FA-B94D-36A7C3D2558C'
set @ComponentID='3CE11574-F30B-4050-911F-02D798EFBFC9'
set @AssemblyStrongName='OBP'
set @ImplementationCMClass='Oritax.TaxSimp.OBP.OBPCM'
set @PersisterClass='Oritax.TaxSimp.OBP.OBPPersister'
set @CategoryName='OBP'

DELETE FROM [dbo].[ASSEMBLY]
      WHERE ID = @AssemblyID



INSERT INTO [dbo].[ASSEMBLY]
           ([ID]
           ,[NAME]
           ,[DISPLAYNAME]
           ,[STRONGNAME]
           ,[MAJORVERSION]
           ,[MINORVERSION]
           ,[DATAFORMAT]
           ,[REVISION])
     VALUES
     (
           @AssemblyID,
           @AssemblyStrongName,
           @AssemblyStrongName, 
           @AssemblyStrongName+', Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215',
           1,
           1,
           0,
           0
           )


DELETE FROM [dbo].[COMPONENT]
      WHERE ID = @ComponentID



INSERT INTO [dbo].[COMPONENT]
           ([ID]
           ,[NAME]
           ,[DISPLAYNAME]
           ,[CATEGORY]
           ,[APPLICABILITY]
           ,[OBSOLETE])
     VALUES (
           @ComponentID,
           @AssemblyStrongName,
           @AssemblyStrongName,
           @CategoryName,
           3,
           'False'
           )


DELETE FROM [dbo].[COMPONENTVERSION]
      WHERE ID = @AssemblyID



INSERT INTO [dbo].[COMPONENTVERSION]
           ([ID]
           ,[VERSIONNAME]
           ,[COMPONENTID]
           ,[STARTDATE]
           ,[ENDDATE]
           ,[PERSISTERASSEMBLYSTRONGNAME]
           ,[PERSISTERCLASS]
           ,[IMPLEMENTATIONCLASS]
           ,[OBSOLETE])
     VALUES
     (
           @AssemblyID,
           '',
           @ComponentID,
           '1990-01-01 00:00:00.000',
           '2050-12-31 00:59:59.000',
            @AssemblyStrongName+', Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215',
           @PersisterClass,
           @ImplementationCMClass,
           'False'
           )
