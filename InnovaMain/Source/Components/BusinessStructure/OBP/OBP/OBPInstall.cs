using System;

namespace Oritax.TaxSimp.OBP
{
    public class OBPInstall
	{
		#region INSTALLATION PROPERTIES

		// Assembly Installation Properties
        public const string ASSEMBLY_ID = "986E40E6-19F6-49FA-B94D-36A7C3D2558C";
        public const string ASSEMBLY_NAME = "OBP";
        public const string ASSEMBLY_DISPLAYNAME = "OBP";
		public const string ASSEMBLY_MAJORVERSION="1";
		public const string ASSEMBLY_MINORVERSION="1";
		public const string ASSEMBLY_DATAFORMAT="0";
		public const string ASSEMBLY_REVISION="0";		

		// Component Installation Properties
        public const string COMPONENT_ID = "3CE11574-F30B-4050-911F-02D798EFBFC9";
        public const string COMPONENT_NAME = "OBP";
        public const string COMPONENT_DISPLAYNAME = "OBP";
		public const string COMPONENT_CATEGORY="OBP";

		// Component Version Installation Properties
		public const string COMPONENTVERSION_STARTDATE="01/01/1990 00:00:00 AM";
		public const string COMPONENTVERSION_ENDDATE="31/12/2050 12:59:59 AM";
		public const string COMPONENTVERSION_PERSISTERASSEMBLY="OBP, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";
        public const string COMPONENTVERSION_PERSISTERCLASS = "Oritax.TaxSimp.OBP.OBPPersister";
		public const string COMPONENTVERSION_IMPLEMENTATIONCLASS="Oritax.TaxSimp.OBP.OBPCM";


		#endregion
	}
}
