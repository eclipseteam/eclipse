﻿using System.Reflection;

using Oritax.TaxSimp.InstallInfo;
using Oritax.TaxSimp.OBP;

[assembly: AssemblyTitle("")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("")]
[assembly: AssemblyCopyright("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: AssemblyVersion(OBPInstall.ASSEMBLY_MAJORVERSION + "." + OBPInstall.ASSEMBLY_MINORVERSION)]
[assembly: AssemblyInformationalVersion(OBPInstall.ASSEMBLY_MAJORVERSION + "." + OBPInstall.ASSEMBLY_MINORVERSION + "." + OBPInstall.ASSEMBLY_DATAFORMAT + "." + OBPInstall.ASSEMBLY_REVISION)]
[assembly: AssemblyInstallInfo(OBPInstall.ASSEMBLY_ID, OBPInstall.ASSEMBLY_NAME, OBPInstall.ASSEMBLY_DISPLAYNAME)]
[assembly: ComponentInstallInfo(OBPInstall.COMPONENT_ID, OBPInstall.COMPONENT_NAME, OBPInstall.COMPONENT_DISPLAYNAME, OBPInstall.COMPONENT_CATEGORY)]
[assembly: ComponentVersionInstallInfo(OBPInstall.COMPONENTVERSION_STARTDATE, OBPInstall.COMPONENTVERSION_ENDDATE, OBPInstall.COMPONENTVERSION_PERSISTERASSEMBLY, OBPInstall.COMPONENTVERSION_PERSISTERCLASS, OBPInstall.COMPONENTVERSION_IMPLEMENTATIONCLASS)]

[assembly: AssemblyDelaySign(false)]
