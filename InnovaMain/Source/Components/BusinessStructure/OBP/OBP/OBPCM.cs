using System;
using System.Collections.Generic;
using System.Data;
using System.Runtime.Serialization;
using System.Security.Permissions;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.DataSets;
using System.Linq;

namespace Oritax.TaxSimp.OBP
{
    [Serializable]
    public class OBPCM : SystemModuleBase, ISerializable, IDisposable
    {
        #region Private Variable
        private List<Advice> _advices=new List<Advice>();
        [NonSerialized]
        private bool disposed;
        #endregion

        #region Public Properties
        public List<Advice> Advices
        {
            get { return _advices; }
            set { _advices = value; }
        }
       	
        #endregion

        #region Constructor
        public OBPCM() { }

        protected OBPCM(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            _advices = info.GetValue<List<Advice>>("Oritax.TaxSimp.OBP.Advices");
        }
        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValueX("Oritax.TaxSimp.OBP.Advices", _advices);
        }

        #endregion

        #region Override Method
        protected override void OnGetData(DataSet data)
        {
            base.OnGetData(data);
            if (data is AdvicesDS)
            {
                var ds = data as AdvicesDS;
                if (Advices != null)
                {
                    switch (ds.CommandType)
                    {
                        case DatasetCommandTypes.Details:
                            {
                                var advice = Advices.FirstOrDefault(ad => ad.ID == ds.AdviceID);
                                if (advice != null)
                                {
                                    SetAdviceRow(advice, ds);
                                    SetPersonalDetailsRow(advice, ds);
                                    SetCurrentAssetsRow(advice, ds);
                                    SetActionsRow(advice, ds);
                                    SetGoalsRow(advice, ds);
                                }
                            }
                            break;
                        case DatasetCommandTypes.Check:
                            {
                                var individualAdvices = Advices;

                                if (ds.IndividualCID != Guid.Empty) 
                                {
                                    individualAdvices = Advices.Where(ad => ad.IndividualCid == ds.IndividualCID && (ds.Unit.CurrentUser.CurrentUserName.ToLower() == "administrator" || ad.Users.Count(ss => ss == ds.Unit.CurrentUser.CID) > 0)).ToList();
                                }

                                foreach (var advice in individualAdvices)
                                {
                                    AdvicesRow adviceDr = ds.AdvicesTable.GetNewRow();
                                    adviceDr[ds.AdvicesTable.ID] = advice.ID;
                                    adviceDr[ds.AdvicesTable.TITLE] = advice.Title;
                                    adviceDr[ds.AdvicesTable.CID] = CID;
                                    adviceDr[ds.AdvicesTable.INDIVIDUALCID] = advice.IndividualCid;
                                    adviceDr[ds.AdvicesTable.INDIVIDUALCLIENTID] = advice.IndividualClientId;
                                    adviceDr[ds.AdvicesTable.CLIENTCID] = advice.ClientCid;
                                    adviceDr[ds.AdvicesTable.CLIENTID] = advice.ClientId;
                                    adviceDr[ds.AdvicesTable.CREATEDON] = advice.CreatedOn;
                                    adviceDr[ds.AdvicesTable.MODIFIEDON] = advice.ModifiedOn;
                                    adviceDr[ds.AdvicesTable.INVESTMENTSTRATEGY] = advice.InvestmentStrategy;
                                    adviceDr.UserList = advice.Users;
                                    adviceDr.MillimanResultSet = advice.MillimanResultSet;
                                    
                                    ds.AdvicesTable.Add(adviceDr);
                                }
                            }
                            break;
                    }
                }
            }
        }

        protected override ModifiedState OnSetData(DataSet data)
        {
            base.OnSetData(data);
            if (_advices == null)
            {
                _advices = new List<Advice>();
            }

            if (data is AdvicesDS)
            {
                var ds = data as AdvicesDS;

                switch (ds.CommandType)
                {
                    case DatasetCommandTypes.Add:
                        if (ds.AdvicesTable.Rows.Count > 0)
                        {
                            AdvicesRow dr = (AdvicesRow)ds.AdvicesTable.Rows[0];
                            var advice = new Advice();

                            SetAdviceObject(advice, ds, dr);
                            SetPersonalDetailsObject(advice, ds, ds.PersonalDetailsTable.Rows[0]);
                            SetCurrentAssetsObject(advice, ds);
                            SetActionsObject(advice, ds);
                            SetGoalsObject(advice, ds);

                            Advices.Add(advice);
                            ds.ExtendedProperties.Add("Result", OperationResults.Successfull);
                        }
                        break;
                    case DatasetCommandTypes.Delete:

                        if (ds.AdvicesTable.Rows.Count > 0)
                        {
                            DataRow dr = ds.AdvicesTable.Rows[0];

                            if (Advices.RemoveAll(ss => ss.ID == (Guid)dr[ds.AdvicesTable.ID]) > 0)
                            {
                                ds.ExtendedProperties.Add("Result", OperationResults.Successfull);
                            }
                            else { ds.ExtendedProperties.Add("Result", OperationResults.Failed); }
                        }
                        break;


                }
            }
            return ModifiedState.MODIFIED;
        }

        #endregion

        #region Private Methods

        private void SetAdviceRow(Advice advice, AdvicesDS ds)
        {
            AdvicesRow adviceDr = ds.AdvicesTable.GetNewRow();
            adviceDr[ds.AdvicesTable.ID] = advice.ID;
            adviceDr[ds.AdvicesTable.TITLE] = advice.Title;
            adviceDr[ds.AdvicesTable.CLIENTCID] = advice.ClientCid;
            adviceDr[ds.AdvicesTable.CLIENTID] = advice.ClientId;
            adviceDr[ds.AdvicesTable.INDIVIDUALCID] = advice.IndividualCid;
            adviceDr[ds.AdvicesTable.INDIVIDUALCLIENTID] = advice.IndividualClientId;
            adviceDr[ds.AdvicesTable.USER] = advice.Users.FirstOrDefault();

            if (advice.MillimanResultSet != null)
            {
                if (adviceDr.MillimanResultSet == null)
                    adviceDr.MillimanResultSet = new SortedList<string, DataSet>();
                
                foreach (var result in advice.MillimanResultSet)
                {
                    adviceDr.MillimanResultSet.Add(result.Key, result.Value);
                }
            }

            adviceDr[ds.AdvicesTable.CREATEDON] = advice.CreatedOn;
            adviceDr[ds.AdvicesTable.MODIFIEDON] = advice.ModifiedOn;
            adviceDr[ds.AdvicesTable.INVESTMENTSTRATEGY] = advice.InvestmentStrategy;
            ds.AdvicesTable.Rows.Add(adviceDr);
        }

        private void SetAdviceObject(Advice advice, AdvicesDS ds, AdvicesRow dr)
        {
            advice.Title = dr[ds.AdvicesTable.TITLE].ToString();
            advice.ClientCid = (Guid)dr[ds.AdvicesTable.CLIENTCID];
            advice.ClientId = dr[ds.AdvicesTable.CLIENTID].ToString();
            advice.IndividualCid = (Guid)dr[ds.AdvicesTable.INDIVIDUALCID];
            advice.IndividualClientId = dr[ds.AdvicesTable.INDIVIDUALCLIENTID].ToString();
            if (dr[ds.AdvicesTable.USER] != DBNull.Value && (Guid)dr[ds.AdvicesTable.USER] != Guid.Empty)
            {
                if (advice.Users == null)
                { advice.Users = new List<Guid>(); }
         
                var usercid = (Guid)dr[ds.AdvicesTable.USER];
                
                if (!advice.Users.Contains(usercid))
                    advice.Users.Add(usercid);
            }

            if (dr.MillimanResultSet != null)
            {
                if (advice.MillimanResultSet == null)
                    advice.MillimanResultSet = new SortedList<string, DataSet>();

                foreach (var result in dr.MillimanResultSet)
                {
                    advice.MillimanResultSet.Add(result.Key, result.Value);
                }
            }

            advice.CreatedOn = (DateTime)dr[ds.AdvicesTable.CREATEDON];
            advice.ModifiedOn = (DateTime)dr[ds.AdvicesTable.MODIFIEDON];
            advice.InvestmentStrategy = dr[ds.AdvicesTable.INVESTMENTSTRATEGY].ToString();
        }

        private void SetPersonalDetailsRow(Advice advice, AdvicesDS ds)
        {
            DataRow personalDetailsDr = ds.PersonalDetailsTable.NewRow();
            personalDetailsDr[ds.PersonalDetailsTable.ADVICESID] = advice.ID;

            if (advice.PersonalDetails.DOB != null)
                personalDetailsDr[ds.PersonalDetailsTable.DOB] = advice.PersonalDetails.DOB;
            
            if (advice.PersonalDetails.PlanAge != null)
                personalDetailsDr[ds.PersonalDetailsTable.PLANAGE] = advice.PersonalDetails.PlanAge;

            if (advice.PersonalDetails.RetirementAge != null)
                personalDetailsDr[ds.PersonalDetailsTable.RETIREMENTAGE] = advice.PersonalDetails.RetirementAge;

            if (advice.PersonalDetails.Salary != null)
                personalDetailsDr[ds.PersonalDetailsTable.SALARY] = advice.PersonalDetails.Salary;

            ds.PersonalDetailsTable.Rows.Add(personalDetailsDr);
        }

        private void SetPersonalDetailsObject(Advice advice, AdvicesDS ds, DataRow dr)
        {
            if (advice.PersonalDetails == null)
                advice.PersonalDetails = new PersonalDetail();

            advice.PersonalDetails.Name = dr[ds.PersonalDetailsTable.NAME].ToString();
            advice.PersonalDetails.Age = dr[ds.PersonalDetailsTable.AGE] != DBNull.Value ? Convert.ToInt32(dr[ds.PersonalDetailsTable.AGE]) : (int?)null;
            advice.PersonalDetails.DOB = dr[ds.PersonalDetailsTable.DOB] != DBNull.Value ? Convert.ToDateTime(dr[ds.PersonalDetailsTable.DOB]) : (DateTime?)null;
            advice.PersonalDetails.PlanAge = dr[ds.PersonalDetailsTable.PLANAGE] != DBNull.Value ? Convert.ToInt32(dr[ds.PersonalDetailsTable.PLANAGE]) : (int?)null;
            advice.PersonalDetails.RetirementAge = dr[ds.PersonalDetailsTable.RETIREMENTAGE] != DBNull.Value ? Convert.ToInt32(dr[ds.PersonalDetailsTable.RETIREMENTAGE]) : (int?)null;
            advice.PersonalDetails.Salary = dr[ds.PersonalDetailsTable.SALARY] != DBNull.Value ? Convert.ToDecimal(dr[ds.PersonalDetailsTable.SALARY]) : (decimal?)null;
        }

        private void SetCurrentAssetsRow(Advice advice, AdvicesDS ds)
        {
            foreach (var asset in advice.CurrentAssets)
            {
                DataRow currentAssetsDr = ds.CurrentAssetsTable.NewRow();
                currentAssetsDr[ds.CurrentAssetsTable.ASSETTYPE] = asset.AssetType;
                currentAssetsDr[ds.CurrentAssetsTable.ADVICESID] = advice.ID;
                currentAssetsDr[ds.CurrentAssetsTable.CASH] = asset.Cash;
                currentAssetsDr[ds.CurrentAssetsTable.GOLD] = asset.Gold;
                currentAssetsDr[ds.CurrentAssetsTable.EQUITY] = asset.Equity;
                currentAssetsDr[ds.CurrentAssetsTable.FIXEDINTEREST] = asset.FixedInterest;
                currentAssetsDr[ds.CurrentAssetsTable.INTERNATIONALEQUITY] = asset.InternationalEquity;
                currentAssetsDr[ds.CurrentAssetsTable.PROPERTY] = asset.Property;
                ds.CurrentAssetsTable.Rows.Add(currentAssetsDr);
            }
        }

        private void SetCurrentAssetsObject(Advice advice, AdvicesDS ds)
        {
            if (advice.CurrentAssets == null)
                advice.CurrentAssets = new List<CurrentAsset>();

            foreach (DataRow dataRow in ds.CurrentAssetsTable.Rows)
            {
                var AssetType = (AssetType)dataRow[ds.CurrentAssetsTable.ASSETTYPE];
                var asset = advice.CurrentAssets.FirstOrDefault(s => s.AssetType == AssetType);
                if (asset == null)
                {
                    asset = new CurrentAsset();
                    SetCurrentAssetRow(ds, dataRow, asset);
                    advice.CurrentAssets.Add(asset);
                }
                else
                {
                    SetCurrentAssetRow(ds, dataRow, asset);
                }
            }
        }

        private static void SetCurrentAssetRow(AdvicesDS ds, DataRow dataRow, CurrentAsset asset)
        {
            asset.AssetType = (AssetType)dataRow[ds.CurrentAssetsTable.ASSETTYPE];
            asset.Cash = dataRow[ds.CurrentAssetsTable.CASH] != DBNull.Value ? Convert.ToDecimal(dataRow[ds.CurrentAssetsTable.CASH]) : (decimal?)null;
            asset.Gold = dataRow[ds.CurrentAssetsTable.GOLD] != DBNull.Value ? Convert.ToDecimal(dataRow[ds.CurrentAssetsTable.GOLD]) : (decimal?)null;
            asset.Equity = dataRow[ds.CurrentAssetsTable.EQUITY] != DBNull.Value ? Convert.ToDecimal(dataRow[ds.CurrentAssetsTable.EQUITY]) : (decimal?)null;
            asset.FixedInterest = dataRow[ds.CurrentAssetsTable.FIXEDINTEREST] != DBNull.Value ? Convert.ToDecimal(dataRow[ds.CurrentAssetsTable.FIXEDINTEREST]) : (decimal?)null;
            asset.InternationalEquity = dataRow[ds.CurrentAssetsTable.INTERNATIONALEQUITY] != DBNull.Value ? Convert.ToDecimal(dataRow[ds.CurrentAssetsTable.INTERNATIONALEQUITY]) : (decimal?)null;
            asset.Property = dataRow[ds.CurrentAssetsTable.PROPERTY] != DBNull.Value ? Convert.ToDecimal(dataRow[ds.CurrentAssetsTable.PROPERTY]) : (decimal?)null;
        }

        private void SetActionsRow(Advice advice, AdvicesDS ds)
        {
            foreach (var action in advice.Actions)
            {
                DataRow actionDr = ds.AdviceActionTable.NewRow();
                actionDr[ds.AdviceActionTable.ADVICESID] = advice.ID;

                actionDr[ds.AdviceActionTable.ACTIONTYPE] = action.ActionType;
                
                if (action.Amount != null)
                    actionDr[ds.AdviceActionTable.AMOUNT] = action.Amount;

                if (action.ContributionStartAge != null)
                    actionDr[ds.AdviceActionTable.CONTRIBUTIONSTARTAGE] = action.ContributionStartAge;

                if (action.ContributionEndAge != null)
                    actionDr[ds.AdviceActionTable.CONTRIBUTIONENDAGE] = action.ContributionEndAge;

                if (action.PercentOfSalary != null)
                    actionDr[ds.AdviceActionTable.PERCENTOFSALARY] = action.PercentOfSalary;

                if (action.SavingStartAge != null)
                    actionDr[ds.AdviceActionTable.SAVINGSTARTAGE] = action.SavingStartAge;

                if (action.SavingEndAge != null)
                    actionDr[ds.AdviceActionTable.SAVINGENDAGE] = action.SavingEndAge;

                ds.AdviceActionTable.Rows.Add(actionDr);
            }
        }

        private void SetActionsObject(Advice advice, AdvicesDS ds)
        {
            if (advice.Actions == null)
                advice.Actions = new List<AdviceAction>();

            foreach (DataRow dataRow in ds.AdviceActionTable.Rows)
            {
                var ActionType = (ActionType)dataRow[ds.AdviceActionTable.ACTIONTYPE];
                var action = advice.Actions.FirstOrDefault(s => s.ActionType == ActionType);

                if (action == null)
                {
                    action = new AdviceAction();
                    SetActionRow(ds, dataRow, action);
                    advice.Actions.Add(action);
                }
                else
                { SetActionRow(ds, dataRow, action); }
            }
        }

        private static void SetActionRow(AdvicesDS ds, DataRow dataRow, AdviceAction action)
        {
            action.ActionType = (ActionType)dataRow[ds.AdviceActionTable.ACTIONTYPE];
            action.Amount = dataRow[ds.AdviceActionTable.AMOUNT] != DBNull.Value ? Convert.ToDecimal(dataRow[ds.AdviceActionTable.AMOUNT]) : (decimal?)null;
            action.ContributionStartAge = dataRow[ds.AdviceActionTable.CONTRIBUTIONSTARTAGE] != DBNull.Value ? Convert.ToInt32(dataRow[ds.AdviceActionTable.CONTRIBUTIONSTARTAGE]) : (int?)null;
            action.ContributionEndAge = dataRow[ds.AdviceActionTable.CONTRIBUTIONENDAGE] != DBNull.Value ? Convert.ToInt32(dataRow[ds.AdviceActionTable.CONTRIBUTIONENDAGE]) : (int?)null;
            action.PercentOfSalary = dataRow[ds.AdviceActionTable.PERCENTOFSALARY] != DBNull.Value ? Convert.ToInt32(dataRow[ds.AdviceActionTable.PERCENTOFSALARY]) : (int?)null;
            action.SavingStartAge = dataRow[ds.AdviceActionTable.SAVINGSTARTAGE] != DBNull.Value ? Convert.ToInt32(dataRow[ds.AdviceActionTable.SAVINGSTARTAGE]) : (int?)null;
            action.SavingEndAge = dataRow[ds.AdviceActionTable.SAVINGENDAGE] != DBNull.Value ? Convert.ToInt32(dataRow[ds.AdviceActionTable.SAVINGENDAGE]) : (int?)null;
        }

        private void SetGoalsRow(Advice advice, AdvicesDS ds)
        {
            foreach (var goal in advice.Goals)
            {
                if (goal.ID == Guid.Empty)
                    goal.ID = Guid.NewGuid();
                DataRow goalDr = ds.GoalsTable.NewRow();
                goalDr[ds.GoalsTable.GOALTYPE] = goal.GoalType;
                goalDr[ds.GoalsTable.ADVICESID] = advice.ID;
                goalDr[ds.GoalsTable.GOALID] = goal.ID;
                goalDr[ds.GoalsTable.AGE] = goal.Age;
                goalDr[ds.GoalsTable.AMOUNT] = goal.Amount;
                ds.GoalsTable.Rows.Add(goalDr);
            }
        }

        private void SetGoalsObject(Advice advice, AdvicesDS ds)
        {
            //if (advice.Goals == null)
                advice.Goals = new List<Goal>();

            foreach (DataRow dataRow in ds.GoalsTable.Rows)
            {
                Guid goalID = new Guid(dataRow[ds.GoalsTable.GOALID].ToString());
                var goal = advice.Goals.FirstOrDefault(s => s.ID == goalID);

                if (goal == null)
                {
                    goal = new Goal();
                }
                SetGoalRow(ds, dataRow, goal);
                advice.Goals.Add(goal);
            }
        }

        private static void SetGoalRow(AdvicesDS ds, DataRow dataRow, Goal goal)
        {
            goal.GoalType = (GoalType)dataRow[ds.GoalsTable.GOALTYPE];
            goal.Amount = dataRow[ds.GoalsTable.AMOUNT] != DBNull.Value ? Convert.ToDecimal(dataRow[ds.GoalsTable.AMOUNT]) : (decimal?)null;
            goal.Age = dataRow[ds.GoalsTable.AGE] != DBNull.Value ? Convert.ToInt32(dataRow[ds.GoalsTable.AGE]) : (int?)null;
            goal.ID = dataRow[ds.GoalsTable.GOALID] != DBNull.Value ? new Guid(dataRow[ds.GoalsTable.GOALID].ToString()) : new Guid();
            if (goal.ID == Guid.Empty)
                goal.ID = Guid.NewGuid();
        }

        #endregion

        #region IDisposable Implementation ---------------------------------------

        /// <summary>
        ///		Performs application-defined tasks associated with freeing, releasing, 
        ///		or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        ///		Performs application-defined tasks associated with freeing, releasing, 
        ///		or resetting unmanaged resources.
        /// </summary>
        /// <param name="disposing">
        ///		True if called by the Dispose() method, false if called by the finalizer.
        /// </param>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                   
                }
                disposed = true;
            }
        }

        #endregion // IDisposable Implementation
   
    }
}
