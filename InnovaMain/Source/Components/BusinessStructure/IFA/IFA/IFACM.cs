using System;
using System.Collections.Generic;
using System.Data;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Xml.Linq;
using System.Linq;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Enum;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Utilities;
using System.Xml;
using System.IO;
using Oritax.TaxSimp.DataSets;

namespace Oritax.TaxSimp.CM.Group
{
    [Serializable]
    public sealed class IFACM : GroupCM, ISerializable, ICmHasParent, IHaveAssociation, ISupportMigration, ICmHasChildren
    {
        #region PRIVATE
        private List<IdentityCM> _Children;
        private IFAEntity _IFAEntity;
        #endregion

        #region PUBLIC
        public IdentityCM Parent { get; set; }
        public List<IdentityCM> Children { get { return _Children; } set { _Children = value; } }
        #endregion

        public IFACM()
            : base()
        {
            Parent = IdentityCM.Null;
            _Children = new List<IdentityCM>();
            _IFAEntity = new IFAEntity();
        }

        public IFACM(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            Parent = info.GetValue<IdentityCM>("Oritax.TaxSimp.CM.Group.IFACM.Parent");
            _Children = info.GetValue<List<IdentityCM>>("Oritax.TaxSimp.CM.Group.IFACM.Children");
            _IFAEntity = info.GetValue<IFAEntity>("Oritax.TaxSimp.CM.Group.IFACM.IFAEntity");
        }

        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValueX("Oritax.TaxSimp.CM.Group.IFACM.Parent", Parent);
            info.AddValueX("Oritax.TaxSimp.CM.Group.IFACM.Children", _Children);
            info.AddValueX("Oritax.TaxSimp.CM.Group.IFACM.IFAEntity", _IFAEntity);
        }

        public override IdentityCM ConfiguredParent
        {
            get
            {
                return this.Parent;
            }
        }

        public override string GetDataStream(int type, string data)
        {

            if (type == -911)
                type = (int)CmCommand.EditOrganizationUnit;

            string xml = string.Empty;
            switch ((CmCommand)type)
            {
                case CmCommand.GetIncludedAdvisorList:
                    xml = GetOrganizationUnitNameList(_Children).ToXmlString();
                    break;
                case CmCommand.EditOrganizationUnit:
                    _IFAEntity.OrganizationStatus = this.OrganizationStatus;
                    _IFAEntity.ClientId = this.ClientId;
                    xml = _IFAEntity.ToXmlString();
                    break;
                case CmCommand.XMLData:
                    xml = ExtractXML(data);
                    break;
                case CmCommand.GetEntitySecurity:
                    xml = GetEntitySecurity();
                    break;
                case CmCommand.FullName:
                    xml = _IFAEntity.Name;
                    break;
                case CmCommand.GetType:
                    xml = _IFAEntity.IFAType.ToString();
                    break;
            }
            return xml;
        }

        protected override void OnGetData(DataSet data)
        {
            base.OnGetData(data);

            #region Set Default  fields
            _IFAEntity.OrganizationStatus = this.OrganizationStatus;
            _IFAEntity.ClientId = this.ClientId;
            this.Name = _IFAEntity.Name;
            #endregion

            if (data is IFADS)
            {
                var ds = data as IFADS;
                switch (ds.CommandType)
                {
                    case DatasetCommandTypes.Details:
                        {
                            AddDetailsRow(ds.ifaTable);
                        }
                        break;

                    default:
                        {
                            DataRow r = ds.ifaTable.NewRow();
                            r[ds.ifaTable.CLIENTID] = ClientId;
                            r[ds.ifaTable.TRADINGNAME] = _IFAEntity.Name;
                            r[ds.ifaTable.TYPE] = typeName;
                            r[ds.ifaTable.IFATYPE] = _IFAEntity.IFAType;
                            r[ds.ifaTable.PHONENUMBERCOUNTRYCODE] = _IFAEntity.PhoneNumber.CountryCode;
                            r[ds.ifaTable.PHONENUMBERCITYCODE] = _IFAEntity.PhoneNumber.CityCode;
                            r[ds.ifaTable.PHONENUBER] = _IFAEntity.PhoneNumber.PhoneNumber;
                            r[ds.ifaTable.EMAIL] = _IFAEntity.Email??string.Empty;
                            r[ds.ifaTable.SKCODE] = _IFAEntity.SKCode;
                            r[ds.ifaTable.STATUS] = OrganizationStatus;
                            r[ds.ifaTable.CID] = CID;
                            r[ds.ifaTable.CLID] = Clid;
                            r[ds.ifaTable.CSID] = Csid;
                            ds.ifaTable.Rows.Add(r);
                        }
                        break;
                }
            }
            else if (data is MembershipDS)
            {
                var ds = data as MembershipDS;
                switch (ds.CommandType)
                {
                    case DatasetCommandTypes.GetChildren:
                        GetChildren(Children, ds, Broker);
                        GetParentTable(Parent, ds, Broker);
                        break;
                }

            }
            else if (data is AddressDetailsDS)
            {
                var ds = data as AddressDetailsDS;
                GetIfaAddressDetails(ds, _IFAEntity);
            }
            else if (data is BankAccountDS)
            {
                if (_IFAEntity.BankAccounts == null)
                    _IFAEntity.BankAccounts = new List<Oritax.TaxSimp.Common.IdentityCMDetail>();
                GetBankAccountDs(data as BankAccountDS, _IFAEntity.BankAccounts, Broker);
            }
            else if (data is IndividualDS)
            {
                GetIndividualDs(data as IndividualDS, _IFAEntity, Broker);
            }
            else if (data is ClientsByIFADS)
            {
                ClientsByIFADS clientsByIFADS = (ClientsByIFADS)data;

                if (this.CID.ToString() == "4217bb03-d9df-4a51-b9b9-24fee00e11af")
                    clientsByIFADS.IncludeCheckForCID = false;

                foreach (IdentityCM identityCM in this._Children)
                {
                    IBrokerManagedComponent ibmc = Broker.GetCMImplementation(identityCM.Clid, identityCM.Csid) as IBrokerManagedComponent;

                    if (ibmc != null)
                    {
                        ibmc.GetData(clientsByIFADS);
                        Broker.ReleaseBrokerManagedComponent(ibmc);
                    }
                }
            }
            else if (data is InstanceCMDS)
            {
                var ds = data as InstanceCMDS;
                GetInstanceDetails(ds, _IFAEntity, Broker);
            }
            else if (data is MDAAlertDS)
            {
                var ds = data as MDAAlertDS;

                var rows = ds.ClientDetailTable.Select(string.Format("{0}='{1}'", ClientDetailTable.IFACID, this.CID));
                foreach (var row in rows)
                {
                    row[ClientDetailTable.IFACODE] = this.ClientId;
                    row[ClientDetailTable.IFANAME] = _IFAEntity.Name;
                    row[ClientDetailTable.IFAEMAIL] = _IFAEntity.Email??string.Empty;
                }

                if (rows.Length > 0 && this.Parent != null)
                {
                    var dg = Broker.GetBMCInstance(this.Parent.Cid);
                    if (dg == null)
                    {
                        dg = Broker.GetCMImplementation(this.Parent.Clid, Parent.Csid);
                    }

                    if (dg != null)
                    {
                        foreach (var row in rows)
                        {
                            row[ClientDetailTable.DGCID] = dg.CID;
                        }
                        dg.GetData(ds);
                        Broker.ReleaseBrokerManagedComponent(dg);
                    }
                }

            }
        }
        private void GetInstanceDetails(InstanceCMDS ds, IFAEntity ifaEntity, ICMBroker broker)
        {
            List<Common.IdentityCMDetail> units = null;
            if (ds.CommandType == DatasetCommandTypes.GetChildren)
            {
                switch (ds.UnitPropertyType)
                {
                    case UnitPropertyType.ShareHolders:
                        units = ifaEntity.ShareHolders;

                        break;
                    case UnitPropertyType.Partnerships:
                        units = ifaEntity.Partners;
                        break;

                    case UnitPropertyType.Corporates:
                        units = new List<Oritax.TaxSimp.Common.IdentityCMDetail>();
                        if (ifaEntity.CorporatePrivate != null)
                            foreach (var identityCm in ifaEntity.CorporatePrivate)
                            {
                                units.Add(Oritax.TaxSimp.Common.IdentityCMDetail.FromIdetityCM(identityCm));
                            }
                        if (ifaEntity.CorporatePublic != null)
                            foreach (var identityCm in ifaEntity.CorporatePublic)
                            {
                                units.Add(Oritax.TaxSimp.Common.IdentityCMDetail.FromIdetityCM(identityCm));
                            }

                        break;

                }
                if (units != null)
                {
                    foreach (var identityCM in units)
                    {
                        var bmc = broker.GetCMImplementation(identityCM.Clid, identityCM.Csid);
                        if (bmc != null)
                        {
                            DataRow dr = ds.InstanceCMDetailsTable.NewRow();
                            dr[ds.InstanceCMDetailsTable.CID] = bmc.CID;
                            dr[ds.InstanceCMDetailsTable.CLID] = bmc.CLID;
                            dr[ds.InstanceCMDetailsTable.CSID] = bmc.CSID;
                            dr[ds.InstanceCMDetailsTable.NAME] = bmc.Name;
                            dr[ds.InstanceCMDetailsTable.TYPE] = bmc.TypeName;
                            dr[ds.InstanceCMDetailsTable.STATUS] = (bmc as OrganizationUnitCM).OrganizationStatus;
                            ds.InstanceCMDetailsTable.Rows.Add(dr);
                            broker.ReleaseBrokerManagedComponent(bmc);
                        }
                    }
                }
            }
        }

        private void GetIndividualDs(IndividualDS individualDs, IFAEntity ifaEntity, ICMBroker broker)
        {
            List<Oritax.TaxSimp.Common.IdentityCMDetail> individuals = null;

            switch (individualDs.IndividualsType)
            {
                case IndividualsType.Applicants:
                    individuals = ifaEntity.Applicants;
                    break;
                case IndividualsType.Signatories:
                    individuals = ifaEntity.Signatories;
                    break;
                case IndividualsType.Directors:
                    individuals = ifaEntity.Directors;
                    break;
            }

            if (individuals != null)
                foreach (Oritax.TaxSimp.Common.IdentityCMDetail identityCM in individuals)
                {
                    var indvCM = broker.GetCMImplementation(identityCM.Clid, identityCM.Csid);
                    if (indvCM != null)
                    {
                        indvCM.GetData(individualDs);
                        if (identityCM.BusinessTitle != null)
                            individualDs.IndividualTable.Rows[individualDs.IndividualTable.Rows.Count - 1][individualDs.IndividualTable.BUSINESSTITLE] = identityCM.BusinessTitle.Title;
                        broker.ReleaseBrokerManagedComponent(indvCM);
                    }
                }
        }

        public static void GetBankAccountDs(BankAccountDS dataSet, List<Oritax.TaxSimp.Common.IdentityCMDetail> bankAccounts, ICMBroker broker)
        {
            if (bankAccounts != null && bankAccounts.Count > 0)
            {
                foreach (var bank in bankAccounts)
                {
                    var entity = broker.GetCMImplementation(bank.Clid, bank.Csid);
                    if (entity != null)
                    {
                        entity.GetData(dataSet);
                        broker.ReleaseBrokerManagedComponent(entity);
                    }
                }
            }
        }

        private void GetIfaAddressDetails(AddressDetailsDS ds, IFAEntity ifaEntity)
        {

            if (ifaEntity.Address == null)
            {

                ifaEntity.Address = new Common.DualAddressEntity();

            }
            FillIfaAddresses(ds, ifaEntity.Address);
        }

        private static void FillIfaAddresses(AddressDetailsDS ds, Common.DualAddressEntity address)
        {
            #region Get Business Address

            if (address.BusinessAddress != null)
            {
                FillAddress(ds, address.BusinessAddress, AddressType.BusinessAddress);
            }
            if (address.MailingAddress != null)
            {
                FillAddress(ds, address.MailingAddress, AddressType.MailingAddress);
            }
            if (address.RegisteredAddress != null)
            {
                FillAddress(ds, address.RegisteredAddress, AddressType.RegisteredAddress);
            }
            if (address.ResidentialAddress != null)
            {
                FillAddress(ds, address.ResidentialAddress, AddressType.ResidentialAddress);
            }
            if (address.DuplicateMailingAddress != null)
            {
                FillAddress(ds, address.DuplicateMailingAddress, AddressType.DuplicateMailingAddress);
            }
            #endregion
        }

        private static void FillAddress(AddressDetailsDS ds, Common.AddressEntity address, AddressType addressType)
        {

            DataRow drBusinessAddress = ds.ClientDetailsTable.NewRow();
            drBusinessAddress[ds.ClientDetailsTable.ADDRESSLINE1] = address.Addressline1;
            drBusinessAddress[ds.ClientDetailsTable.ADDRESSLINE2] = address.Addressline2;
            drBusinessAddress[ds.ClientDetailsTable.SUBURB] = address.Suburb;
            drBusinessAddress[ds.ClientDetailsTable.STATE] = address.State;
            drBusinessAddress[ds.ClientDetailsTable.COUNTRY] = address.Country;
            drBusinessAddress[ds.ClientDetailsTable.POSTCODE] = address.PostCode;
            drBusinessAddress[ds.ClientDetailsTable.ADDRESSTYPE] = addressType;
            ds.ClientDetailsTable.Rows.Add(drBusinessAddress);
        }

        private void AddDetailsRow(IFATable ifaTable)
        {
            DataRow r = ifaTable.NewRow();
            r[ifaTable.CLIENTID] = ClientId;
            r[ifaTable.TRADINGNAME] = _IFAEntity.Name;
            r[ifaTable.LEGALNAME] = _IFAEntity.LegalName;
            r[ifaTable.IFATYPE] = _IFAEntity.IFAType;
            r[ifaTable.EMAIL] = _IFAEntity.Email??string.Empty;
            r[ifaTable.TYPE] = typeName;
            r[ifaTable.STATUS] = OrganizationStatus;
            r[ifaTable.CID] = CID;
            r[ifaTable.CLID] = Clid;
            r[ifaTable.CSID] = Csid;

            switch (_IFAEntity.IFAType)
            {
                case IFAEntityType.IFACompanyControl:
                    {
                        if (_IFAEntity.Facsimile != null)
                        {
                            r[ifaTable.FACSIMILECOUNTRYCODE] = _IFAEntity.Facsimile.CountryCode;
                            r[ifaTable.FACSIMILECITYCODE] = _IFAEntity.Facsimile.CityCode;
                            r[ifaTable.FACSIMILE] = _IFAEntity.Facsimile.PhoneNumber;
                        }

                        if (_IFAEntity.PhoneNumber != null)
                        {
                            r[ifaTable.PHONENUMBERCOUNTRYCODE] = _IFAEntity.PhoneNumber.CountryCode;
                            r[ifaTable.PHONENUMBERCITYCODE] = _IFAEntity.PhoneNumber.CityCode;
                            r[ifaTable.PHONENUBER] = _IFAEntity.PhoneNumber.PhoneNumber;
                        }
                        r[ifaTable.SKCODE] = _IFAEntity.SKCode;
                        r[ifaTable.WEBSITEADDRESS] = _IFAEntity.Websiteaddress;
                        r[ifaTable.ACN] = _IFAEntity.ACN;
                        r[ifaTable.LASTAUDITEDDATE] = _IFAEntity.lastAuditedDate;
                        r[ifaTable.TURNOVER] = _IFAEntity.Turnover;
                        r[ifaTable.ABN] = _IFAEntity.ABN;
                        r[ifaTable.TFN] = _IFAEntity.TFN;
                        r[ifaTable.PREVIOUSPRACTICE] = _IFAEntity.PreviousPracticeName;
                        r[ifaTable.FUM] = _IFAEntity.FUM;
                    }
                    break;

                case IFAEntityType.IFAIndividualControl:
                    {
                        if (_IFAEntity.Facsimile != null)
                        {
                            r[ifaTable.FACSIMILECOUNTRYCODE] = _IFAEntity.Facsimile.CountryCode;
                            r[ifaTable.FACSIMILECITYCODE] = _IFAEntity.Facsimile.CityCode;
                            r[ifaTable.FACSIMILE] = _IFAEntity.Facsimile.PhoneNumber;
                        }

                        if (_IFAEntity.PhoneNumber != null)
                        {
                            r[ifaTable.PHONENUMBERCOUNTRYCODE] = _IFAEntity.PhoneNumber.CountryCode;
                            r[ifaTable.PHONENUMBERCITYCODE] = _IFAEntity.PhoneNumber.CityCode;
                            r[ifaTable.PHONENUBER] = _IFAEntity.PhoneNumber.PhoneNumber;
                        }
                        r[ifaTable.SKCODE] = _IFAEntity.SKCode;
                        r[ifaTable.WEBSITEADDRESS] = _IFAEntity.Websiteaddress;
                        r[ifaTable.ABN] = _IFAEntity.ABN;
                        r[ifaTable.ACN] = _IFAEntity.ACN;
                        r[ifaTable.LASTAUDITEDDATE] = _IFAEntity.lastAuditedDate;
                        r[ifaTable.PREVIOUSPRACTICE] = _IFAEntity.PreviousPracticeName;
                        r[ifaTable.FUM] = _IFAEntity.FUM;
                        r[ifaTable.TURNOVER] = _IFAEntity.Turnover;
                    }
                    break;
                case IFAEntityType.IFAPartnershipControl:
                    {
                        if (_IFAEntity.Facsimile != null)
                        {
                            r[ifaTable.FACSIMILECOUNTRYCODE] = _IFAEntity.Facsimile.CountryCode;
                            r[ifaTable.FACSIMILECITYCODE] = _IFAEntity.Facsimile.CityCode;
                            r[ifaTable.FACSIMILE] = _IFAEntity.Facsimile.PhoneNumber;
                        }

                        if (_IFAEntity.PhoneNumber != null)
                        {
                            r[ifaTable.PHONENUMBERCOUNTRYCODE] = _IFAEntity.PhoneNumber.CountryCode;
                            r[ifaTable.PHONENUMBERCITYCODE] = _IFAEntity.PhoneNumber.CityCode;
                            r[ifaTable.PHONENUBER] = _IFAEntity.PhoneNumber.PhoneNumber;
                        }
                        r[ifaTable.SKCODE] = _IFAEntity.SKCode;
                        r[ifaTable.WEBSITEADDRESS] = _IFAEntity.Websiteaddress;
                        r[ifaTable.ABN] = _IFAEntity.ABN;
                        r[ifaTable.TFN] = _IFAEntity.TFN;
                        r[ifaTable.LASTAUDITEDDATE] = _IFAEntity.lastAuditedDate;
                        r[ifaTable.PREVIOUSPRACTICE] = _IFAEntity.PreviousPracticeName;
                        r[ifaTable.FUM] = _IFAEntity.FUM;
                        r[ifaTable.TURNOVER] = _IFAEntity.Turnover;
                    }
                    break;
                case IFAEntityType.IFATrustControl:
                    {
                        if (_IFAEntity.Facsimile != null)
                        {
                            r[ifaTable.FACSIMILECOUNTRYCODE] = _IFAEntity.Facsimile.CountryCode;
                            r[ifaTable.FACSIMILECITYCODE] = _IFAEntity.Facsimile.CityCode;
                            r[ifaTable.FACSIMILE] = _IFAEntity.Facsimile.PhoneNumber;
                        }

                        if (_IFAEntity.PhoneNumber != null)
                        {
                            r[ifaTable.PHONENUMBERCOUNTRYCODE] = _IFAEntity.PhoneNumber.CountryCode;
                            r[ifaTable.PHONENUMBERCITYCODE] = _IFAEntity.PhoneNumber.CityCode;
                            r[ifaTable.PHONENUBER] = _IFAEntity.PhoneNumber.PhoneNumber;
                        }
                        r[ifaTable.SKCODE] = _IFAEntity.SKCode;
                        r[ifaTable.WEBSITEADDRESS] = _IFAEntity.Websiteaddress;
                        r[ifaTable.ABN] = _IFAEntity.ABN;
                        r[ifaTable.ACN] = _IFAEntity.ACN;
                        r[ifaTable.LASTAUDITEDDATE] = _IFAEntity.lastAuditedDate;
                        r[ifaTable.PREVIOUSPRACTICE] = _IFAEntity.PreviousPracticeName;
                        r[ifaTable.FUM] = _IFAEntity.FUM;
                        r[ifaTable.TURNOVER] = _IFAEntity.Turnover;
                    }
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
            ifaTable.Rows.Add(r);
        }

        protected override ModifiedState OnSetData(System.Data.DataSet data)
        {
            base.OnSetData(data);

            if (data is MembershipDS)
            {
                var ds = data as MembershipDS;
                IdentityCM child;
                switch (ds.CommandType)
                {
                    case DatasetCommandTypes.Add:
                        foreach (DataRow dr in ds.membershipTable.Rows)
                        {
                            child = new IdentityCM();
                            child.Cid = new Guid(dr[ds.membershipTable.CID].ToString());
                            child.Clid = new Guid(dr[ds.membershipTable.CLID].ToString());
                            child.Csid = new Guid(dr[ds.membershipTable.CSID].ToString());
                            if (Children.Count(ss => ss.Clid == child.Clid && ss.Csid == child.Csid) == 0)
                            {
                                try
                                {
                                    BMCUpdateComponentParent(child, CmCommand.Add_Single, Broker);
                                    Children.Add(child);
                                }
                                catch (Exception ex)
                                {

                                    dr.RowError = ex.Message;
                                }
                            }
                        }
                        break;
                    case DatasetCommandTypes.Delete:
                        foreach (DataRow dr in ds.membershipTable.Rows)
                        {
                            child = new IdentityCM();
                            child.Cid = new Guid(dr[ds.membershipTable.CID].ToString());
                            child.Clid = new Guid(dr[ds.membershipTable.CLID].ToString());
                            child.Csid = new Guid(dr[ds.membershipTable.CSID].ToString());
                            if (Children.Count(ss => ss.Clid == child.Clid && ss.Csid == child.Csid) > 0)
                            {
                                BMCUpdateComponentParent(child, CmCommand.Remove_Single, Broker);
                                Children.Remove(child);
                            }
                        }
                        break;
                }
            }
            else if (data is IFADS)
            {
                var ds = data as IFADS;
                switch (ds.CommandType)
                {
                    case DatasetCommandTypes.Add:

                        if (ds.ifaTable.Rows.Count > 0)
                        {
                            var ifaEntity = new IFAEntity();
                            ifaEntity.ClientId = ClientId;
                            SetEntityDetails(ifaEntity, ds.ifaTable);
                            _IFAEntity = ifaEntity;
                        }
                        break;
                    case DatasetCommandTypes.Update:
                        if (ds.ifaTable.Rows.Count > 0)
                        {
                            SetEntityDetails(_IFAEntity, ds.ifaTable);
                        }

                        break;
                }
            }
            else if (data is AddressDetailsDS)
            {
                var ds = data as AddressDetailsDS;
                SetIfaAddresses(ds, _IFAEntity.Address);
            }
            else if (data is BankAccountDS)
            {
                if (_IFAEntity.BankAccounts == null)
                    _IFAEntity.BankAccounts = new List<Oritax.TaxSimp.Common.IdentityCMDetail>();
                var ds = data as BankAccountDS;
                if (ds.CommandType == DatasetCommandTypes.Delete)
                    RemoveBankAccount(ds, _IFAEntity.BankAccounts, Broker);
                else if (ds.CommandType == DatasetCommandTypes.Update)
                    AssociateBankAccount(ds, _IFAEntity.BankAccounts);
            }
            else if (data is IndividualDS)
            {
                var ds = data as IndividualDS;

                List<Oritax.TaxSimp.Common.IdentityCMDetail> individuals = null;


                switch (ds.IndividualsType)
                {
                    case IndividualsType.Applicants:
                        if (_IFAEntity.Applicants == null)
                            _IFAEntity.Applicants = new List<Oritax.TaxSimp.Common.IdentityCMDetail>();
                        individuals = _IFAEntity.Applicants;
                        break;
                    case IndividualsType.Signatories:
                        if (_IFAEntity.Signatories == null)
                            _IFAEntity.Signatories = new List<Oritax.TaxSimp.Common.IdentityCMDetail>();
                        individuals = _IFAEntity.Signatories;
                        break;
                    case IndividualsType.Directors:
                        if (_IFAEntity.Directors == null)
                            _IFAEntity.Directors = new List<Oritax.TaxSimp.Common.IdentityCMDetail>();
                        individuals = _IFAEntity.Directors;
                        break;

                }
                if (ds.CommandType == DatasetCommandTypes.Delete)
                    RemoveIndividual(ds, individuals, Broker);
                else if (ds.CommandType == DatasetCommandTypes.Update)
                    AssociateIndividuals(ds, individuals);
            }
            else if (data is InstanceCMDS)
            {
                var ds = data as InstanceCMDS;


                switch (ds.UnitPropertyType)
                {
                    case UnitPropertyType.ShareHolders:
                        if (_IFAEntity.ShareHolders == null)
                            _IFAEntity.ShareHolders = new List<Oritax.TaxSimp.Common.IdentityCMDetail>();
                        if (ds.CommandType == DatasetCommandTypes.Delete)
                            RemoveUnits(ds, _IFAEntity.ShareHolders, Broker);
                        else if (ds.CommandType == DatasetCommandTypes.Update)
                            AssociateUnits(ds, _IFAEntity.ShareHolders);
                        break;
                    case UnitPropertyType.Partnerships:
                        if (_IFAEntity.Partners == null)
                            _IFAEntity.Partners = new List<Oritax.TaxSimp.Common.IdentityCMDetail>();

                        if (ds.CommandType == DatasetCommandTypes.Delete)
                            RemoveUnits(ds, _IFAEntity.Partners, Broker);
                        else if (ds.CommandType == DatasetCommandTypes.Update)
                            AssociateUnits(ds, _IFAEntity.Partners);
                        break;
                    case UnitPropertyType.Corporates:
                        if (_IFAEntity.CorporatePrivate == null)
                            _IFAEntity.CorporatePrivate = new List<IdentityCM>();
                        if (_IFAEntity.CorporatePublic == null)
                            _IFAEntity.CorporatePublic = new List<IdentityCM>();

                        if (ds.CommandType == DatasetCommandTypes.Delete)
                        {
                            RemoveUnits(ds, _IFAEntity.CorporatePrivate, Broker);
                            RemoveUnits(ds, _IFAEntity.CorporatePublic, Broker);
                        }
                        else if (ds.CommandType == DatasetCommandTypes.Update)
                        {
                            AssociateUnits(ds, _IFAEntity.CorporatePublic, OrganizationType.ClientCorporationPublic);
                            AssociateUnits(ds, _IFAEntity.CorporatePrivate, OrganizationType.ClientCorporationPrivate);
                        }


                        break;
                }
            }
            #region set default values
            this.Name = _IFAEntity.Name;
            this.OrganizationStatus = _IFAEntity.OrganizationStatus;
            #endregion
            return ModifiedState.MODIFIED;
        }

        public static void AssociateBankAccount(BankAccountDS ds, List<Oritax.TaxSimp.Common.IdentityCMDetail> bankAccounts)
        {
            if (ds.Tables[ds.BankAccountsTable.TABLENAME].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[ds.BankAccountsTable.TABLENAME].Rows)
                {
                    var clid = new Guid(dr[ds.BankAccountsTable.CLID].ToString());
                    var csid = new Guid(dr[ds.BankAccountsTable.CSID].ToString());
                    var identityCmDetail = bankAccounts.FirstOrDefault(ss => ss.Clid == new Guid(dr[ds.BankAccountsTable.CLID].ToString()) && ss.Csid == new Guid(dr[ds.BankAccountsTable.CSID].ToString()));
                    if (identityCmDetail == null)
                    {
                        if (bankAccounts == null)
                        {
                            bankAccounts = new List<Oritax.TaxSimp.Common.IdentityCMDetail>();
                        }
                        bankAccounts.Add(new Oritax.TaxSimp.Common.IdentityCMDetail { Clid = clid, Csid = csid });

                    }
                }
            }
        }

        public static void RemoveBankAccount(BankAccountDS ds, List<Oritax.TaxSimp.Common.IdentityCMDetail> bankAccounts, ICMBroker broker)
        {
            if (ds.Tables[ds.BankAccountsTable.TABLENAME].Rows.Count > 0)
            {
                var clid = (Guid)ds.Tables[ds.BankAccountsTable.TABLENAME].Rows[0][ds.BankAccountsTable.CLID];
                var csid = (Guid)ds.Tables[ds.BankAccountsTable.TABLENAME].Rows[0][ds.BankAccountsTable.CSID];

                var identityCmDetail = bankAccounts.FirstOrDefault(ss => ss.Clid == clid && ss.Csid == csid);
                if (identityCmDetail != null)
                {

                    bankAccounts.Remove(identityCmDetail);
                    ds.ExtendedProperties.Add("Message", "Account has been deleted successfully");
                }
            }
        }

        private static void SetIfaAddresses(AddressDetailsDS ds, Oritax.TaxSimp.Common.DualAddressEntity Address)
        {
            foreach (DataRow dr in ds.ClientDetailsTable.Rows)
            {
                if ((Oritax.TaxSimp.DataSets.AddressType)System.Enum.Parse(typeof(Oritax.TaxSimp.DataSets.AddressType), dr[ds.ClientDetailsTable.ADDRESSTYPE].ToString()) == Oritax.TaxSimp.DataSets.AddressType.BusinessAddress)
                {
                    SetAddress(ds.ClientDetailsTable, Address.BusinessAddress, dr);
                }
                else if (
                    (Oritax.TaxSimp.DataSets.AddressType)System.Enum.Parse(typeof(Oritax.TaxSimp.DataSets.AddressType), dr[ds.ClientDetailsTable.ADDRESSTYPE].ToString()) == Oritax.TaxSimp.DataSets.AddressType.RegisteredAddress)
                {
                    SetAddress(ds.ClientDetailsTable, Address.RegisteredAddress, dr);
                }
                else if (
                    (Oritax.TaxSimp.DataSets.AddressType)
                    System.Enum.Parse(typeof(Oritax.TaxSimp.DataSets.AddressType), dr[ds.ClientDetailsTable.ADDRESSTYPE].ToString()) == Oritax.TaxSimp.DataSets.AddressType.ResidentialAddress)
                {
                    SetAddress(ds.ClientDetailsTable, Address.ResidentialAddress, dr);
                }
                else if (
                    (Oritax.TaxSimp.DataSets.AddressType)
                    System.Enum.Parse(typeof(Oritax.TaxSimp.DataSets.AddressType), dr[ds.ClientDetailsTable.ADDRESSTYPE].ToString()) == Oritax.TaxSimp.DataSets.AddressType.MailingAddress)
                {
                    SetAddress(ds.ClientDetailsTable, Address.MailingAddress, dr);
                }
                else if (
                    (Oritax.TaxSimp.DataSets.AddressType)
                    System.Enum.Parse(typeof(Oritax.TaxSimp.DataSets.AddressType), dr[ds.ClientDetailsTable.ADDRESSTYPE].ToString()) == Oritax.TaxSimp.DataSets.AddressType.DuplicateMailingAddress)
                {
                    SetAddress(ds.ClientDetailsTable, Address.DuplicateMailingAddress, dr);
                }
            }
        }

        private static void SetAddress(AddressDetails table, Oritax.TaxSimp.Common.AddressEntity address, DataRow dr)
        {
            address.Addressline1 = dr[table.ADDRESSLINE1].ToString();
            address.Addressline2 = dr[table.ADDRESSLINE2].ToString();
            address.Suburb = dr[table.SUBURB].ToString();
            address.State = dr[table.STATE].ToString();
            address.Country = dr[table.COUNTRY].ToString();
            address.PostCode = dr[table.POSTCODE].ToString();
        }

        public static void AssociateUnits(InstanceCMDS ds, List<Oritax.TaxSimp.Data.IdentityCM> units, OrganizationType Type)
        {
            if (units != null && ds.Tables[ds.InstanceCMDetailsTable.TABLENAME].Rows.Count > 0)
            {
                var drs = ds.Tables[ds.InstanceCMDetailsTable.TABLENAME].Select(string.Format("{0}='{1}'", ds.InstanceCMDetailsTable.TYPE, Type));
                foreach (DataRow dr in drs)
                {
                    var clid = new Guid(dr[ds.InstanceCMDetailsTable.CLID].ToString());
                    var cid = new Guid(dr[ds.InstanceCMDetailsTable.CID].ToString());
                    var csid = new Guid(dr[ds.InstanceCMDetailsTable.CSID].ToString());

                    var identityCmDetail = units.FirstOrDefault(ss => ss.Clid == new Guid(dr[ds.InstanceCMDetailsTable.CLID].ToString()) && ss.Csid == new Guid(dr[ds.InstanceCMDetailsTable.CSID].ToString()));
                    if (identityCmDetail == null)
                    {
                        if (units == null)
                        {
                            units = new List<Oritax.TaxSimp.Data.IdentityCM>();
                        }
                        units.Add(new Oritax.TaxSimp.Data.IdentityCM { Clid = clid, Csid = csid, Cid = cid });
                    }
                }
            }
        }

        public static void AssociateUnits(InstanceCMDS ds, List<Oritax.TaxSimp.Common.IdentityCMDetail> units)
        {
            if (units != null && ds.Tables[ds.InstanceCMDetailsTable.TABLENAME].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[ds.InstanceCMDetailsTable.TABLENAME].Rows)
                {
                    var clid = new Guid(dr[ds.InstanceCMDetailsTable.CLID].ToString());
                    var cid = new Guid(dr[ds.InstanceCMDetailsTable.CID].ToString());
                    var csid = new Guid(dr[ds.InstanceCMDetailsTable.CSID].ToString());

                    var identityCmDetail = units.FirstOrDefault(ss => ss.Clid == new Guid(dr[ds.InstanceCMDetailsTable.CLID].ToString()) && ss.Csid == new Guid(dr[ds.InstanceCMDetailsTable.CSID].ToString()));
                    if (identityCmDetail == null)
                    {
                        if (units == null)
                        {
                            units = new List<Oritax.TaxSimp.Common.IdentityCMDetail>();
                        }
                        units.Add(new Oritax.TaxSimp.Common.IdentityCMDetail { Clid = clid, Csid = csid, Cid = cid });

                    }
                }
            }
        }

        public static void RemoveUnits(InstanceCMDS ds, List<Oritax.TaxSimp.Common.IdentityCMDetail> units, ICMBroker broker)
        {
            if (units != null && ds.Tables[ds.InstanceCMDetailsTable.TABLENAME].Rows.Count > 0)
            {
                var clid = (Guid)ds.Tables[ds.InstanceCMDetailsTable.TABLENAME].Rows[0][ds.InstanceCMDetailsTable.CLID];
                var csid = (Guid)ds.Tables[ds.InstanceCMDetailsTable.TABLENAME].Rows[0][ds.InstanceCMDetailsTable.CSID];

                var identityCmDetail = units.FirstOrDefault(ss => ss.Clid == clid && ss.Csid == csid);
                if (identityCmDetail != null)
                {

                    units.Remove(identityCmDetail);
                    ds.ExtendedProperties.Add("Message", "Record has been deleted successfully");
                }
            }
        }

        public static void RemoveUnits(InstanceCMDS ds, List<Oritax.TaxSimp.Data.IdentityCM> units, ICMBroker broker)
        {
            if (units != null && ds.Tables[ds.InstanceCMDetailsTable.TABLENAME].Rows.Count > 0)
            {
                var clid = (Guid)ds.Tables[ds.InstanceCMDetailsTable.TABLENAME].Rows[0][ds.InstanceCMDetailsTable.CLID];
                var csid = (Guid)ds.Tables[ds.InstanceCMDetailsTable.TABLENAME].Rows[0][ds.InstanceCMDetailsTable.CSID];

                var identityCmDetail = units.FirstOrDefault(ss => ss.Clid == clid && ss.Csid == csid);
                if (identityCmDetail != null)
                {

                    units.Remove(identityCmDetail);
                    ds.ExtendedProperties.Add("Message", "Record has been deleted successfully");
                }
            }
        }

        public static void AssociateIndividuals(IndividualDS ds, List<Oritax.TaxSimp.Common.IdentityCMDetail> individuals)
        {
            if (individuals != null && ds.Tables[ds.IndividualTable.TABLENAME].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[ds.IndividualTable.TABLENAME].Rows)
                {
                    var clid = new Guid(dr[ds.IndividualTable.CLID].ToString());
                    var cid = new Guid(dr[ds.IndividualTable.CID].ToString());
                    var csid = new Guid(dr[ds.IndividualTable.CSID].ToString());
                    var businessttitle = dr[ds.IndividualTable.BUSINESSTITLE].ToString();
                    var identityCmDetail = individuals.FirstOrDefault(ss => ss.Clid == new Guid(dr[ds.IndividualTable.CLID].ToString()) && ss.Csid == new Guid(dr[ds.IndividualTable.CSID].ToString()));
                    if (identityCmDetail == null)
                    {
                        individuals.Add(new Oritax.TaxSimp.Common.IdentityCMDetail { Clid = clid, Csid = csid, Cid = cid, BusinessTitle = new TitleEntity { Title = businessttitle } });
                    }
                    else
                    {
                        identityCmDetail.BusinessTitle = new TitleEntity { Title = businessttitle };
                    }
                }
            }
        }

        public static void RemoveIndividual(IndividualDS ds, List<Oritax.TaxSimp.Common.IdentityCMDetail> individuals, ICMBroker broker)
        {
            if (individuals != null && ds.Tables[ds.IndividualTable.TABLENAME].Rows.Count > 0)
            {
                var clid = (Guid)ds.Tables[ds.IndividualTable.TABLENAME].Rows[0][ds.IndividualTable.CLID];
                var csid = (Guid)ds.Tables[ds.IndividualTable.TABLENAME].Rows[0][ds.IndividualTable.CSID];

                var identityCmDetail = individuals.FirstOrDefault(ss => ss.Clid == clid && ss.Csid == csid);
                if (identityCmDetail != null)
                {

                    individuals.Remove(identityCmDetail);
                    ds.ExtendedProperties.Add("Message", "Individual has been deleted successfully");
                }
            }
        }
        private void SetEntityDetails(IFAEntity entity, IFATable ifaTable)
        {
            DataRow dr = ifaTable.Rows[0];
            entity.Name = dr[ifaTable.TRADINGNAME].ToString();
            entity.LegalName = dr[ifaTable.LEGALNAME].ToString();
            entity.Email = dr[ifaTable.EMAIL].ToString();
            entity.IFAType = (IFAEntityType)System.Enum.Parse(typeof(IFAEntityType), dr[ifaTable.IFATYPE].ToString());
            entity.OrganizationStatus = dr[ifaTable.STATUS].ToString();
            switch (entity.IFAType)
            {
                case IFAEntityType.IFACompanyControl:
                    {
                        entity.Facsimile = new PhoneNumberEntity();
                        entity.Facsimile.CountryCode = dr[ifaTable.FACSIMILECOUNTRYCODE].ToString();
                        entity.Facsimile.CityCode = dr[ifaTable.FACSIMILECITYCODE].ToString();
                        entity.Facsimile.PhoneNumber = dr[ifaTable.FACSIMILE].ToString();
                        entity.Websiteaddress = dr[ifaTable.WEBSITEADDRESS].ToString();
                        entity.ACN = dr[ifaTable.ACN].ToString();
                        if (dr[ifaTable.LASTAUDITEDDATE] != DBNull.Value)
                            entity.lastAuditedDate = Convert.ToDateTime(dr[ifaTable.LASTAUDITEDDATE].ToString());
                        entity.Turnover = dr[ifaTable.TURNOVER].ToString();
                        entity.PhoneNumber = new PhoneNumberEntity();
                        entity.PhoneNumber.CountryCode = dr[ifaTable.PHONENUMBERCOUNTRYCODE].ToString();
                        entity.PhoneNumber.CityCode = dr[ifaTable.PHONENUMBERCITYCODE].ToString();
                        entity.PhoneNumber.PhoneNumber = dr[ifaTable.PHONENUBER].ToString();
                        entity.SKCode = dr[ifaTable.SKCODE].ToString();
                        entity.ABN = dr[ifaTable.ABN].ToString();
                        entity.TFN = dr[ifaTable.TFN].ToString();
                        entity.PreviousPracticeName = dr[ifaTable.PREVIOUSPRACTICE].ToString();
                        entity.FUM = dr[ifaTable.FUM].ToString();
                    }
                    break;
                case IFAEntityType.IFAIndividualControl:
                    {
                        entity.Facsimile = new PhoneNumberEntity();
                        entity.Facsimile.CountryCode = dr[ifaTable.FACSIMILECOUNTRYCODE].ToString();
                        entity.Facsimile.CityCode = dr[ifaTable.FACSIMILECITYCODE].ToString();
                        entity.Facsimile.PhoneNumber = dr[ifaTable.FACSIMILE].ToString();
                        entity.PhoneNumber = new PhoneNumberEntity();
                        entity.PhoneNumber.CountryCode = dr[ifaTable.PHONENUMBERCOUNTRYCODE].ToString();
                        entity.PhoneNumber.CityCode = dr[ifaTable.PHONENUMBERCITYCODE].ToString();
                        entity.PhoneNumber.PhoneNumber = dr[ifaTable.PHONENUBER].ToString();
                        entity.SKCode = dr[ifaTable.SKCODE].ToString();
                        entity.Websiteaddress = dr[ifaTable.WEBSITEADDRESS].ToString();
                        entity.ABN = dr[ifaTable.ABN].ToString();
                        entity.ACN = dr[ifaTable.ACN].ToString();
                        if (dr[ifaTable.LASTAUDITEDDATE] != DBNull.Value)
                            entity.lastAuditedDate = Convert.ToDateTime(dr[ifaTable.LASTAUDITEDDATE].ToString());
                        entity.PreviousPracticeName = dr[ifaTable.PREVIOUSPRACTICE].ToString();
                        entity.FUM = dr[ifaTable.FUM].ToString();
                        entity.Turnover = dr[ifaTable.TURNOVER].ToString();
                    }
                    break;
                case IFAEntityType.IFAPartnershipControl:
                    {
                        entity.Facsimile = new PhoneNumberEntity();
                        entity.Facsimile.CountryCode = dr[ifaTable.FACSIMILECOUNTRYCODE].ToString();
                        entity.Facsimile.CityCode = dr[ifaTable.FACSIMILECITYCODE].ToString();
                        entity.Facsimile.PhoneNumber = dr[ifaTable.FACSIMILE].ToString();
                        entity.PhoneNumber = new PhoneNumberEntity();
                        entity.PhoneNumber.CountryCode = dr[ifaTable.PHONENUMBERCOUNTRYCODE].ToString();
                        entity.PhoneNumber.CityCode = dr[ifaTable.PHONENUMBERCITYCODE].ToString();
                        entity.PhoneNumber.PhoneNumber = dr[ifaTable.PHONENUBER].ToString();
                        entity.SKCode = dr[ifaTable.SKCODE].ToString();
                        entity.Websiteaddress = dr[ifaTable.WEBSITEADDRESS].ToString();
                        entity.ABN = dr[ifaTable.ABN].ToString();
                        entity.TFN = dr[ifaTable.TFN].ToString();
                        if (dr[ifaTable.LASTAUDITEDDATE] != DBNull.Value)
                            entity.lastAuditedDate = Convert.ToDateTime(dr[ifaTable.LASTAUDITEDDATE].ToString());
                        entity.PreviousPracticeName = dr[ifaTable.PREVIOUSPRACTICE].ToString();
                        entity.FUM = dr[ifaTable.FUM].ToString();
                        entity.Turnover = dr[ifaTable.TURNOVER].ToString();
                    }
                    break;
                case IFAEntityType.IFATrustControl:
                    {
                        entity.Facsimile = new PhoneNumberEntity();
                        entity.Facsimile.CountryCode = dr[ifaTable.FACSIMILECOUNTRYCODE].ToString();
                        entity.Facsimile.CityCode = dr[ifaTable.FACSIMILECITYCODE].ToString();
                        entity.Facsimile.PhoneNumber = dr[ifaTable.FACSIMILE].ToString();
                        entity.Websiteaddress = dr[ifaTable.WEBSITEADDRESS].ToString();
                        entity.ACN = dr[ifaTable.ACN].ToString();
                        if (dr[ifaTable.LASTAUDITEDDATE] != DBNull.Value)
                            entity.lastAuditedDate = Convert.ToDateTime(dr[ifaTable.LASTAUDITEDDATE].ToString());
                        entity.Turnover = dr[ifaTable.TURNOVER].ToString();
                        entity.PhoneNumber = new PhoneNumberEntity();
                        entity.PhoneNumber.CountryCode = dr[ifaTable.PHONENUMBERCOUNTRYCODE].ToString();
                        entity.PhoneNumber.CityCode = dr[ifaTable.PHONENUMBERCITYCODE].ToString();
                        entity.PhoneNumber.PhoneNumber = dr[ifaTable.PHONENUBER].ToString();
                        entity.SKCode = dr[ifaTable.SKCODE].ToString();
                        entity.ABN = dr[ifaTable.ABN].ToString();
                        entity.PreviousPracticeName = dr[ifaTable.PREVIOUSPRACTICE].ToString();
                        entity.FUM = dr[ifaTable.FUM].ToString();
                    }
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }

        }

        public void BMCUpdateComponentParent(IdentityCM child, CmCommand cmd, ICMBroker broker)
        {
            OrganizationUnitCM component = broker.GetCMImplementation(child.Clid, child.Csid) as OrganizationUnitCM;
            if (component == null && child.Cid != null)
                component = broker.GetBMCInstance(child.Cid) as OrganizationUnitCM;
            if (component != null)
            {
                switch (cmd)
                {
                    case CmCommand.Add_Single:

                        component.UpdateDataStream((int)CmCommand.AttachToParent, new IFAtoAdvisor() { Cid = this.CID, Clid = this.Clid, Csid = this.Csid, Address = _IFAEntity.Address, WorkPhoneNumber = _IFAEntity.PhoneNumber, FaxNumber = _IFAEntity.Facsimile }.ToXmlString());
                        break;
                    case CmCommand.Remove_Single:

                        component.UpdateDataStream((int)CmCommand.DeAttachFromParent, new IFAtoAdvisor() { Cid = this.CID, Clid = this.Clid, Csid = this.Csid, Address = new Oritax.TaxSimp.Common.DualAddressEntity(), WorkPhoneNumber = new PhoneNumberEntity(), FaxNumber = new PhoneNumberEntity() }.ToXmlString());
                        break;
                }
                broker.ReleaseBrokerManagedComponent(component);
            }


        }

        public override string OnUpdateDataStream(int type, string data)
        {
            string xml = string.Empty;
            IFAEntity _ifaentity = null;
            IdentityCM child = null;
            List<Oritax.TaxSimp.Common.OrganizationUnit> list = null;
            Oritax.TaxSimp.Common.OrganizationUnit ou = null;
            switch ((CmCommand)type)
            {
                case CmCommand.AttachToParent:
                    if (Parent != IdentityCM.Null)
                    {
                        var component = Broker.GetCMImplementation(Parent.Clid, Parent.Csid) as OrganizationUnitCM;
                        if (component == null)
                            component = Broker.GetBMCInstance(Parent.Cid) as OrganizationUnitCM;
                        if (component != null)
                        {
                            string name = component.GetDataStream((int)CmCommand.FullName, "");
                            Broker.ReleaseBrokerManagedComponent(component);
                            throw new Exception("IFA (" + _IFAEntity.Name + ") is already part of IFA (" + name + ")");
                        }
                    }
                    IdentityCM parent = GetParent(data);
                    Parent = parent;
                    break;
                case CmCommand.DeAttachFromParent:
                    Parent = IdentityCM.Null;
                    break;
                case CmCommand.Add_Single:
                    child = GetChild(data);
                    BMCUpdateComponent(child, CmCommand.Add_Single);
                    _Children.Add(child);
                    break;
                case CmCommand.SPAddLibrary:
                    var _value = data.ToNewOrData<Oritax.TaxSimp.Common.SPLibraryDocument>();
                    if (_value != null)
                    {
                        SPLibrary.AddLibraryToClient(_value.CurrentUser.CurrentUserName, _value.Name);
                    }
                    break;
                case CmCommand.Remove_Single:
                    child = GetChild(data);
                    BMCUpdateComponent(child, CmCommand.Remove_Single);
                    _Children.RemoveAll(c => c == child);//c => c.Clid == child.Clid && c.Csid == child.Csid);                    
                    break;
                case CmCommand.Add_List:
                    list = data.ToNewOrData<List<Oritax.TaxSimp.Common.OrganizationUnit>>();
                    foreach (var each in list)
                    {
                        child = GetChild(each.ToXmlString());
                        BMCUpdateComponent(child, CmCommand.Add_Single);
                        _Children.Add(child);
                    }
                    break;
                case CmCommand.Remove_List:
                    list = data.ToNewOrData<List<Oritax.TaxSimp.Common.OrganizationUnit>>();
                    foreach (var each in list)
                    {
                        child = GetChild(each.ToXmlString());
                        BMCUpdateComponent(child, CmCommand.Remove_Single);
                        _Children.RemoveAll(c => c.Clid == child.Clid && c.Csid == child.Csid);
                    }
                    break;
                case CmCommand.EditOrganizationUnit:
                    _ifaentity = data.ToNewOrData<IFAEntity>();
                    this.Name = _ifaentity.Name;
                    this.OrganizationStatus = _ifaentity.OrganizationStatus;
                    _IFAEntity = _ifaentity;
                    ou = new Oritax.TaxSimp.Common.OrganizationUnit();
                    ou.Clid = this.Clid;
                    ou.Csid = this.Csid;
                    ou.Name = _ifaentity.Name;
                    xml = ou.ToXmlString();
                    break;
                case CmCommand.AddEntitySecurity:
                    xml = AddEntitySecurity(data);
                    break;
                case CmCommand.DeleteEntitySecurity:
                    xml = DeleteEntitySecurity(data);
                    break;
                case CmCommand.GetEntitySecurity:
                    xml = GetEntitySecurity();
                    break;
                case CmCommand.UpdateClientID:// for the purpose of updating client id's
                    break;
                default:
                    _ifaentity = data.ToNewOrData<IFAEntity>();
                    this.Name = _ifaentity.Name;
                    this.OrganizationStatus = _ifaentity.OrganizationStatus;
                    _IFAEntity = _ifaentity;
                    if (ClientId == "" || ClientId == null)
                    {
                        UpDateEventLog.UpdateLog("IFA", _IFAEntity.Name, type, this.Broker);
                    }
                    else
                    {
                        UpDateEventLog.UpdateLog("IFA", _IFAEntity.Name + " (" + ClientId + ")", type, this.Broker);
                    }

                    break;
            }
            return xml;
        }

        private IdentityCM GetChild(string data)
        {
            return data.ToIdentity();
        }

        private IdentityCM GetParent(string data)
        {
            return data.ToIdentity();
        }

        private void BMCUpdateComponent(IdentityCM child, CmCommand cmd)
        {
            IBrokerManagedComponent component = base.Broker.GetCMImplementation(child.Clid, child.Csid) as IBrokerManagedComponent;
            switch (cmd)
            {
                case CmCommand.Add_Single:
                    Broker.SetWriteStart();
                    component.UpdateDataStream((int)CmCommand.AttachToParent, new IFAtoAdvisor() { Clid = this.Clid, Csid = this.Csid, Address = _IFAEntity.Address, WorkPhoneNumber = _IFAEntity.PhoneNumber, FaxNumber = _IFAEntity.Facsimile }.ToXmlString());
                    break;
                case CmCommand.Remove_Single:
                    Broker.SetWriteStart();
                    component.UpdateDataStream((int)CmCommand.DeAttachFromParent, new IFAtoAdvisor() { Clid = this.Clid, Csid = this.Csid, Address = new Oritax.TaxSimp.Common.DualAddressEntity(), WorkPhoneNumber = new PhoneNumberEntity(), FaxNumber = new PhoneNumberEntity() }.ToXmlString());
                    break;
            }
        }

        private List<Oritax.TaxSimp.Common.OrganizationUnit> GetOrganizationUnitNameList(List<IdentityCM> value)
        {
            IBrokerManagedComponent component = null;
            List<Oritax.TaxSimp.Common.OrganizationUnit> entities = new List<Oritax.TaxSimp.Common.OrganizationUnit>();
            foreach (var each in value)
            {
                component = base.Broker.GetCMImplementation(each.Clid, each.Csid) as IBrokerManagedComponent;
                if (component != null)
                {
                    entities.Add(new Oritax.TaxSimp.Common.OrganizationUnit()
                    {
                        Clid = each.Clid,
                        Csid = each.Csid,
                        Name = component.GetDataStream(999, string.Empty) //component.Name 
                    });
                }
            }
            return entities;
        }

        private void UpdateBMC(IdentityCM cm)
        {
            IBrokerManagedComponent component = base.Broker.GetCMImplementation(cm.Clid, cm.Csid) as IBrokerManagedComponent;
            component.UpdateDataStream(-1, this.ToIdentityXml());
        }

        public void Export(XElement root)
        {
            _IFAEntity.OrganizationStatus = this.OrganizationStatus;
            _IFAEntity.ClientId = this.ClientId;
            root.Add(_IFAEntity.ToXElement("IFA"));
            root.Add(Parent.ToXElement("Parent"));
            root.Add(_Children.ToXElement("Children"));
        }

        public void Import(XElement root, IdentityCmMap map)
        {
            _IFAEntity = root.FromXElement<IFAEntity>("IFA");
            map.ReplaceOrRemove(_IFAEntity.BankAccounts);
            map.ReplaceOrRemove(_IFAEntity.Signatories);
            map.ReplaceOrRemove(_IFAEntity.Directors);
            map.ReplaceOrRemove(_IFAEntity.ShareHolders);
            map.ReplaceOrRemove(_IFAEntity.Partners);
            map.ReplaceOrRemove(_IFAEntity.Applicants);
            map.ReplaceOrRemove(_IFAEntity.CorporatePrivate);
            map.ReplaceOrRemove(_IFAEntity.CorporatePublic);

            Name = _IFAEntity.Name;
            OrganizationStatus = _IFAEntity.OrganizationStatus;
            ClientId = _IFAEntity.ClientId;

            Parent = root.FromXElement<IdentityCM>("Parent");
            map.ReplaceOrNull(Parent);

            _Children = root.FromXElement<List<IdentityCM>>("Children");
            map.ReplaceOrRemove(_Children);
        }

        public override void DoTagged(EntityTag tag, Guid modelId)
        {
            string appender = tag.Appender;
            _IFAEntity.DoTaggedExtension<IFAEntity>(tag);

            if (this.Parent != null)
            {
                var orgnizationunitcm = this.Parent.ToOrganizationUnit(Broker);
                if (orgnizationunitcm != null)
                    orgnizationunitcm.DoTagged(tag.SetAppender(appender + ".IFA"), modelId);

            }
            tag.SetAppender(appender);
        }



        #region Security

        private string GetEntitySecurity()
        {
            EntitySecurity entity = new EntitySecurity();

            PartyDS ds = new PartyDS();
            base.OnGetData(ds);
            foreach (DataRow row in ds.Tables[PartyDS.INCLUDEDPARTY_TABLE].Rows)
            {
                entity.IncludedUsers.Add(new UserEntity() { CID = new Guid(row[PartyDS.PARTYCID_FIELD].ToString()), CurrentUserName = row[PartyDS.PARTYNAME_FIELD].ToString() });
            }
            foreach (DataRow row in ds.Tables[PartyDS.AVAILABLEPARTY_TABLE].Rows)
            {
                if (row[PartyDS.PARTYNAME_FIELD].ToString().ToLower() != "Administrator, Administrator".ToLower())
                {
                    entity.AvailableUsers.Add(new UserEntity() { CID = new Guid(row[PartyDS.PARTYCID_FIELD].ToString()), CurrentUserName = row[PartyDS.PARTYNAME_FIELD].ToString() });
                }
            }

            return entity.ToXmlString();
        }

        private string DeleteEntitySecurity(string UserEntity)
        {
            UserEntity entity = UserEntity.ToData<UserEntity>();

            this.Broker.SetWriteStart();

            PartyDS ds = new PartyDS();
            base.OnGetData(ds);
            ds.AcceptChanges();

            DataRow[] drCollection = ds.Tables[PartyDS.INCLUDEDPARTY_TABLE].Select(PartyDS.PARTYCID_FIELD + " = '" + entity.CID + "'", String.Empty);
            drCollection[0].Delete();
            this.SetData(ds);

            return string.Empty;
        }

        #endregion

        #region OrganizationsChart

        public OrganizationChart GetOrganizationChart()
        {
            OrganizationChart chart = new OrganizationChart();

            chart.Clid = this.CLID;
            chart.Csid = this.CSID;
            chart.NodeName = this._IFAEntity.Name;

            return chart;
        }

        public OrganizationChart GetAssociation(IIdentityCM identitycm, OrganizationType organizationtype)
        {
            OrganizationChart OChart = new OrganizationChart();
            switch (organizationtype)
            {
                case OrganizationType.BankAccount:
                    if (this._IFAEntity.BankAccounts.Where(e => e.Clid == identitycm.Clid && e.Csid == identitycm.Csid).ToList().Count > 0)
                    {
                        OChart.Clid = this.Clid;
                        OChart.Csid = this.Csid;
                        OChart.NodeName = this.Name;
                    }
                    break;
                case OrganizationType.Individual:
                    OChart = GetIndividualChart(identitycm);
                    break;
                case OrganizationType.IFA:
                    if (identitycm.Clid == this.Clid && identitycm.Csid == this.Csid)
                    {
                        OChart = new OrganizationChart() { Clid = this.Clid, Csid = this.Csid, NodeName = this.Name, TypeName = OrganizationType.IFA.ToString() };
                        //OChart.Subordinates.Add(this._Children.ToOrganizationChart(this.Broker, "Adviser"));
                        OChart.Subordinates.Add(GetEntityChild());
                        OChart.Subordinates.Add(this._IFAEntity.BankAccounts.ToOrganizationChart(this.Broker, "Bank Account(s)"));
                        OChart.Subordinates.Add(this._IFAEntity.Signatories.ToOrganizationChart(this.Broker, "Signatory(ies)"));
                        OChart.Subordinates.Add(this._IFAEntity.Directors.ToOrganizationChart(this.Broker, "Director(s)"));
                        OChart.Subordinates.Add(this._IFAEntity.ShareHolders.ToOrganizationChart(this.Broker, "ShareHolder(s)"));
                        OChart.Subordinates.Add(this._IFAEntity.Partners.ToOrganizationChart(this.Broker, "Partner(s)"));
                        OChart.Subordinates.Add(this._IFAEntity.Applicants.ToOrganizationChart(this.Broker, "Applicant(s)"));

                        OChart.Subordinates.Add(this._IFAEntity.CorporatePrivate.ToOrganizationChart(this.Broker, "Corporate Private"));
                        OChart.Subordinates.Add(this._IFAEntity.CorporatePublic.ToOrganizationChart(this.Broker, "Corporate Public"));
                        OChart.Subordinates.RemoveAll(e => e == null);
                        OChart = GetEntityParent(OChart);
                    }
                    break;
                default:
                    break;
            }
            return OChart;
        }

        public OrganizationChart GetEntityParent(OrganizationChart organizationchart)
        {
            ICalculationModule module = null;
            OrganizationChart association = new OrganizationChart() { Clid = this.Clid, Csid = this.Csid, NodeName = this.Name, TypeName = OrganizationType.IFA.ToString() };
            if (organizationchart.Clid == this.Clid && organizationchart.Csid == this.Csid)
            {
                association = organizationchart;
            }
            else
            {
                association.Subordinates.Add(organizationchart);
            }
            module = Broker.GetCMImplementation(this.Parent.Clid, this.Parent.Csid);
            if (module != null && module is IHaveAssociation)
            {
                association = (module as IHaveAssociation).GetEntityParent(association);
            }
            return association;
        }

        public OrganizationChart GetEntityChild()
        {
            ICalculationModule module = null;
            OrganizationChart children = new OrganizationChart();
            children.NodeName = "Advisers";
            foreach (var each in this._Children)
            {
                module = Broker.GetCMImplementation(each.Clid, each.Csid);
                if (module != null && module is IHaveAssociation)
                {
                    children.Subordinates.Add((module as IHaveAssociation).GetEntityChildWithEntity());
                }
            }
            return children.Subordinates.Count <= 0 ? null : children;
        }

        public OrganizationChart GetEntityChildWithEntity()
        {
            ICalculationModule module = null;
            OrganizationChart association = new OrganizationChart() { Clid = this.Clid, Csid = this.Csid, NodeName = this.Name, TypeName = OrganizationType.IFA.ToString() };
            OrganizationChart children = new OrganizationChart();
            children.NodeName = "Advisers";
            foreach (var each in this._Children)
            {
                module = Broker.GetCMImplementation(each.Clid, each.Csid);
                if (module != null && module is IHaveAssociation)
                {
                    children.Subordinates.Add((module as IHaveAssociation).GetEntityChildWithEntity());
                }
            }
            association.Subordinates.Add(children.Subordinates.Count <= 0 ? null : children);
            association.Subordinates.Add(this._IFAEntity.BankAccounts.ToOrganizationChart(this.Broker, "Bank Account(s)"));
            association.Subordinates.Add(this._IFAEntity.Signatories.ToOrganizationChart(this.Broker, "Signatory(ies)"));
            association.Subordinates.Add(this._IFAEntity.Directors.ToOrganizationChart(this.Broker, "Director(s)"));
            association.Subordinates.Add(this._IFAEntity.ShareHolders.ToOrganizationChart(this.Broker, "ShareHolder(s)"));
            association.Subordinates.Add(this._IFAEntity.Partners.ToOrganizationChart(this.Broker, "Partner(s)"));
            association.Subordinates.Add(this._IFAEntity.Applicants.ToOrganizationChart(this.Broker, "Applicant(s)"));

            association.Subordinates.Add(this._IFAEntity.CorporatePrivate.ToOrganizationChart(this.Broker, "Corporate Private"));
            association.Subordinates.Add(this._IFAEntity.CorporatePublic.ToOrganizationChart(this.Broker, "Corporate Public"));
            association.Subordinates.RemoveAll(e => e == null);
            return association;
        }

        private OrganizationChart GetIndividualChart(IIdentityCM identitycm)
        {
            OrganizationChart OChart = new OrganizationChart();
            OChart.Clid = this.Clid;
            OChart.Csid = this.Csid;
            OChart.NodeName = this.Name;
            if (this._IFAEntity.Directors.Where(e => e.Clid == identitycm.Clid && e.Csid == identitycm.Csid).ToList().Count > 0)
            {
                OChart.Subordinates.Add(new OrganizationChart() { NodeName = "Director(s)" });
            }
            if (this._IFAEntity.Partners.Where(e => e.Clid == identitycm.Clid && e.Csid == identitycm.Csid).ToList().Count > 0)
            {
                OChart.Subordinates.Add(new OrganizationChart() { NodeName = "Partner(s)" });
            }
            if (this._IFAEntity.Signatories.Where(e => e.Clid == identitycm.Clid && e.Csid == identitycm.Csid).ToList().Count > 0)
            {
                OChart.Subordinates.Add(new OrganizationChart() { NodeName = "Signatory(ies)" });
            }
            if (this._IFAEntity.ShareHolders.Where(e => e.Clid == identitycm.Clid && e.Csid == identitycm.Csid).ToList().Count > 0)
            {
                OChart.Subordinates.Add(new OrganizationChart() { NodeName = "ShareHolder(s)" });
            }
            if (this._IFAEntity.Applicants.Where(e => e.Clid == identitycm.Clid && e.Csid == identitycm.Csid).ToList().Count > 0)
            {
                OChart.Subordinates.Add(new OrganizationChart() { NodeName = "Applicant(s)" });
            }
            OChart.Subordinates.RemoveAll(e => e == null);
            return OChart.Subordinates.Count <= 0 ? null : OChart;
        }

        #endregion

        #region GetXMLData

        private string ExtractXML(string data)
        {
            string xmlco = _IFAEntity.ToXmlString();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmlco);
            XMLExtensions.SetNode(_IFAEntity.Applicants, "Applicants", "Applicant", doc, this.Broker);
            XMLExtensions.SetNode(_IFAEntity.Signatories, "Signatories", "Signatory", doc, this.Broker);
            XMLExtensions.SetNode(_IFAEntity.Directors, "Directors", "Director", doc, this.Broker);
            XMLExtensions.SetNode(_IFAEntity.ShareHolders, "ShareHolders", "ShareHolder", doc, this.Broker);
            XMLExtensions.SetNode(_IFAEntity.Partners, "Partners", "Partner", doc, this.Broker);
            XMLExtensions.SetNode(_IFAEntity.BankAccounts, "BankAccounts", "BankAccount", doc, this.Broker);
            XmlAttribute rootAtt = doc.CreateAttribute("Type");
            rootAtt.Value = this.TypeName;
            doc.DocumentElement.Attributes.Append(rootAtt);
            string xml = doc.OuterXml;
            XDocument document = XDocument.Load(XmlReader.Create(new StringReader(xml)));
            document.Root.Name = data;
            document.Root.RemoveAttributes();
            xml = document.ToString().Trim();
            return xml;
        }

        #endregion
    }

    [Serializable]
    public class IFAEntity
    {
        public String Name { get; set; }
        public String ACN { get; set; }
        public String ABN { get; set; }
        public String TFN { get; set; }

        public Oritax.TaxSimp.Common.DualAddressEntity Address { get; set; }

        public PhoneNumberEntity PhoneNumber { get; set; }
        public PhoneNumberEntity Facsimile { get; set; }
        public String Websiteaddress { get; set; }
        public String PreviousPracticeName { get; set; }
        public String LegalName { get; set; }
        public DateTime? lastAuditedDate { get; set; }
        public String FUM { get; set; }
        public String Turnover { get; set; }
        public String SKCode { get; set; }

        public List<Oritax.TaxSimp.Common.IdentityCMDetail> BankAccounts { get; set; }
        public List<Oritax.TaxSimp.Common.IdentityCMDetail> Signatories { get; set; }
        public List<Oritax.TaxSimp.Common.IdentityCMDetail> Directors { get; set; }
        public List<Oritax.TaxSimp.Common.IdentityCMDetail> ShareHolders { get; set; }
        public List<Oritax.TaxSimp.Common.IdentityCMDetail> Partners { get; set; }
        public List<Oritax.TaxSimp.Common.IdentityCMDetail> Applicants { get; set; }

        public List<IdentityCM> CorporatePrivate { get; set; }
        public List<IdentityCM> CorporatePublic { get; set; }

        public String OrganizationStatus { get; set; }
        public String ClientId { get; set; }
        public IFAEntityType IFAType { get; set; }

        public IdentityCM Parent { get; set; }
        public List<IdentityCM> Children { get; set; }
        public string Email { get; set; }
    }

    public enum IFAEntityType
    {
        IFACompanyControl = 1,
        IFAIndividualControl,
        IFAPartnershipControl,
        IFATrustControl,
    }
}
