using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Security.Permissions;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Enum;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Utilities;
using OTC = Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Common;

namespace Oritax.TaxSimp.CM.Group
{
    [Serializable]
    public sealed class PrincipalPracticeCM : GroupCM, ISerializable, IIdentityCM, ICmHasParent
    {
        public IdentityCM Parent { get; set; }

        private List<IdentityCM> _Children;

        private PrincipalPracticeEntity _PrincipalPracticeEntity;

        public PrincipalPracticeCM()
            : base()
        {
            Parent = IdentityCM.Null;
            _Children = new List<IdentityCM>();
            _PrincipalPracticeEntity = new PrincipalPracticeEntity();
        }

        public PrincipalPracticeCM(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            Parent = info.GetValue<IdentityCM>("Oritax.TaxSimp.CM.Group.PrincipalPracticeCM.Parent");
            _Children = info.GetValue<List<IdentityCM>>("Oritax.TaxSimp.CM.Group.PrincipalPracticeCM.Children");
            _PrincipalPracticeEntity = info.GetValue<PrincipalPracticeEntity>("Oritax.TaxSimp.CM.Group.PrincipalPracticeCM.PrincipalPracticeEntity");
        }

        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValueX("Oritax.TaxSimp.CM.Group.PrincipalPracticeCM.Parent", Parent);
            info.AddValueX("Oritax.TaxSimp.CM.Group.PrincipalPracticeCM.Children", _Children);
            info.AddValueX("Oritax.TaxSimp.CM.Group.PrincipalPracticeCM.PrincipalPracticeEntity", _PrincipalPracticeEntity);
        }

        public override string GetDataStream(int type, string data)
        {
            string xml = string.Empty;
            switch ((CmCommand)type)
            {
                case CmCommand.GetIncludedAdvisorList:
                    xml = GetOrganizationUnitNameList(_Children).ToXmlString();
                    break;
                case CmCommand.EditOrganizationUnit:
                    _PrincipalPracticeEntity.OrganizationStatus = this.OrganizationStatus;
                    _PrincipalPracticeEntity.ClientId = this.ClientId;
                    xml = _PrincipalPracticeEntity.ToXmlString();
                    break;
            }
            return xml;
        }

        public override string OnUpdateDataStream(int type, string data)
        {
            string xml = string.Empty;
            PrincipalPracticeEntity _principalpracticeentity = null;
            IdentityCM child = null;
            List<OTC.OrganizationUnit> list = null;
            OTC.OrganizationUnit ou = null;
            switch ((CmCommand)type)
            {
                case CmCommand.AttachToParent:
                    if (Parent != IdentityCM.Null) throw new Exception("Principal Practice is already part of Dealer Group");
                    IdentityCM parent = GetParent(data);
                    Parent = parent;
                    break;
                case CmCommand.DeAttachFromParent:
                    Parent = IdentityCM.Null;
                    break;
                case CmCommand.Add_Single:
                    child = GetChild(data);
                    BMCUpdateComponent(child, CmCommand.Add_Single);
                    _Children.Add(child);
                    break;
                case CmCommand.Remove_Single:
                    child = GetChild(data);
                    BMCUpdateComponent(child, CmCommand.Remove_Single);
                    _Children.RemoveAll(c => c == child);//c => c.Clid == child.Clid && c.Csid == child.Csid);                    
                    break;
                case CmCommand.Add_List:
                    list = data.ToNewOrData<List<OTC.OrganizationUnit>>();
                    foreach (var each in list)
                    {
                        child = GetChild(each.ToXmlString());
                        BMCUpdateComponent(child, CmCommand.Add_Single);
                        _Children.Add(child);
                    }
                    break;
                case CmCommand.Remove_List:
                    list = data.ToNewOrData<List<OTC.OrganizationUnit>>();
                    foreach (var each in list)
                    {
                        child = GetChild(each.ToXmlString());
                        BMCUpdateComponent(child, CmCommand.Remove_Single);
                        _Children.RemoveAll(c => c.Clid == child.Clid && c.Csid == child.Csid);
                    }
                    break;
                case CmCommand.EditOrganizationUnit:
                    _principalpracticeentity = data.ToNewOrData<PrincipalPracticeEntity>();
                    this.Name = _principalpracticeentity.Name;
                    this.OrganizationStatus = _principalpracticeentity.OrganizationStatus;
                    _PrincipalPracticeEntity = _principalpracticeentity;
                    ou = new OTC.OrganizationUnit();
                    ou.Clid = this.Clid;
                    ou.Csid = this.Csid;
                    ou.Name = _principalpracticeentity.Name;
                    xml = ou.ToXmlString();
                    break;
                default:
                    _principalpracticeentity = data.ToNewOrData<PrincipalPracticeEntity>();
                    this.Name = _principalpracticeentity.Name;
                    this.OrganizationStatus = _principalpracticeentity.OrganizationStatus;
                    _PrincipalPracticeEntity = _principalpracticeentity;
                    if (ClientId == "" || ClientId == null)
                    {
                        UpDateEventLog.UpdateLog("Principal Practice", _PrincipalPracticeEntity.Name, type, this.Broker);
                    }
                    else
                    {
                        UpDateEventLog.UpdateLog("Principal Practice", _PrincipalPracticeEntity.Name + " (" + EclipseClientID + ")", type, this.Broker);
                    }
                    
                    break;
            }
            return xml;
        }

        private IdentityCM GetChild(string data)
        {
            return data.ToIdentity();
        }

        private IdentityCM GetParent(string data)
        {
            return data.ToIdentity();
        }

        private void BMCUpdateComponent(IdentityCM child, CmCommand cmd)
        {
            IBrokerManagedComponent component = base.Broker.GetCMImplementation(child.Clid, child.Csid) as IBrokerManagedComponent;
            switch (cmd)
            {
                case CmCommand.Add_Single:
                    Broker.SetWriteStart();
                    component.UpdateDataStream((int)CmCommand.AttachToParent, this.ToIdentityXml());
                    break;
                case CmCommand.Remove_Single:
                    Broker.SetWriteStart();
                    component.UpdateDataStream((int)CmCommand.DeAttachFromParent, this.ToIdentityXml());
                    break;
            }
        }

        private List<OTC.OrganizationUnit> GetOrganizationUnitNameList(List<IdentityCM> value)
        {
            IBrokerManagedComponent component = null;
            List<OTC.OrganizationUnit> entities = new List<OTC.OrganizationUnit>();
            foreach (var each in value)
            {
                component = base.Broker.GetCMImplementation(each.Clid, each.Csid) as IBrokerManagedComponent;
                entities.Add(new OTC.OrganizationUnit() { Clid = each.Clid, Csid = each.Csid, Name = component.Name });
            }
            return entities;
        }

        private void UpdateBMC(IdentityCM cm)
        {
            IBrokerManagedComponent component = base.Broker.GetCMImplementation(cm.Clid, cm.Csid) as IBrokerManagedComponent;
            component.UpdateDataStream(-1, this.ToIdentityXml());
        }
    }

    [Serializable]
    public class PrincipalPracticeEntity
    {
        public String Name { get; set; }
        public String ACN { get; set; }
        public String ABN { get; set; }
        public Oritax.TaxSimp.Common.AddressEntity ResidentialAddress { get; set; }
        public Oritax.TaxSimp.Common.AddressEntity MailingAddress { get; set; }
        public bool MailingAddressSame { get; set; }
        public PhoneNumberEntity PhoneNumber { get; set; }
        public String Facsimile { get; set; }
        public String Websiteaddress { get; set; }
        public String PreviousPracticeName { get; set; }
        public String TradingName { get; set; }
        public String ShareHolders { get; set; }
        public String DirectorsCompanySecretary { get; set; }
        public DateTime? lastAuditedDate { get; set; }
        public String FUM { get; set; }
        public String Turnover { get; set; }
        public List<Oritax.TaxSimp.Common.IdentityCMDetail> BankAccounts { get; set; }
        public List<Oritax.TaxSimp.Common.IdentityCMDetail> Signatories { get; set; }
        public String OrganizationStatus { get; set; }
        public String ClientId { get; set; }
    }
}
