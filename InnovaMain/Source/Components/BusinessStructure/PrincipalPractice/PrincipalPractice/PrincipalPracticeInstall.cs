namespace Oritax.TaxSimp.CM.Group
{
    public class PrincipalPracticeInstall
	{
		#region INSTALLATION PROPERTIES

		// Assembly Installation Properties
        public const string ASSEMBLY_ID = "62137E6D-E021-4412-8E87-779B32EFEE8E";
        public const string ASSEMBLY_NAME = "PrincipalPractice";
        public const string ASSEMBLY_DISPLAYNAME = "Principal Practice";
		public const string ASSEMBLY_MAJORVERSION="1";
		public const string ASSEMBLY_MINORVERSION="0";
        public const string ASSEMBLY_DATAFORMAT="0"; 
		public const string ASSEMBLY_REVISION="0"; 

		// Component Installation Properties
        public const string COMPONENT_ID = "A6818F74-D0BF-4196-9F3E-964F003BC2EC";
        public const string COMPONENT_NAME = "PrincipalPractice";
        public const string COMPONENT_DISPLAYNAME = "Principal Practice";
		public const string COMPONENT_CATEGORY="BusinessEntity";

		// Component Version Installation Properties
		public const string COMPONENTVERSION_STARTDATE="01/01/1900 00:00:00 AM";
		public const string COMPONENTVERSION_ENDDATE="31/12/2050 12:59:59 AM";
		public const string COMPONENTVERSION_PERSISTERASSEMBLY="CommonPersisters_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";
		public const string COMPONENTVERSION_PERSISTERCLASS="Oritax.TaxSimp.CommonPersistence.GroupPersister";
        public const string COMPONENTVERSION_IMPLEMENTATIONCLASS = "Oritax.TaxSimp.CM.Group.PrincipalPracticeCM";

		// Data Model
		public const string DATAMODEL_MANAGERASSEMBLY="CommonPersisters_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";
		public const string DATAMODEL_MANAGERCLASS="Oritax.TaxSimp.CommonPersistence.GroupPersister";

		#endregion
	}
}
