﻿using System.Reflection;

using Oritax.TaxSimp.InstallInfo;
using Oritax.TaxSimp.CM.Entity;

[assembly: AssemblyTitle("")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("")]
[assembly: AssemblyCopyright("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: AssemblyVersion(ManagedInvestmentSchemesAccountInstall.ASSEMBLY_MAJORVERSION + "." + ManagedInvestmentSchemesAccountInstall.ASSEMBLY_MINORVERSION)]
[assembly: AssemblyInformationalVersion(ManagedInvestmentSchemesAccountInstall.ASSEMBLY_MAJORVERSION + "." + ManagedInvestmentSchemesAccountInstall.ASSEMBLY_MINORVERSION + "." + ManagedInvestmentSchemesAccountInstall.ASSEMBLY_DATAFORMAT + "." + ManagedInvestmentSchemesAccountInstall.ASSEMBLY_REVISION)]
[assembly: AssemblyInstallInfo(ManagedInvestmentSchemesAccountInstall.ASSEMBLY_ID, ManagedInvestmentSchemesAccountInstall.ASSEMBLY_NAME, ManagedInvestmentSchemesAccountInstall.ASSEMBLY_DISPLAYNAME)]
[assembly: ComponentInstallInfo(ManagedInvestmentSchemesAccountInstall.COMPONENT_ID, ManagedInvestmentSchemesAccountInstall.COMPONENT_NAME, ManagedInvestmentSchemesAccountInstall.COMPONENT_DISPLAYNAME, ManagedInvestmentSchemesAccountInstall.COMPONENT_CATEGORY)]
[assembly: ComponentVersionInstallInfo(ManagedInvestmentSchemesAccountInstall.COMPONENTVERSION_STARTDATE, ManagedInvestmentSchemesAccountInstall.COMPONENTVERSION_ENDDATE, ManagedInvestmentSchemesAccountInstall.COMPONENTVERSION_PERSISTERASSEMBLY, ManagedInvestmentSchemesAccountInstall.COMPONENTVERSION_PERSISTERCLASS, ManagedInvestmentSchemesAccountInstall.COMPONENTVERSION_IMPLEMENTATIONCLASS)]

[assembly: AssemblyDelaySign(false)]
