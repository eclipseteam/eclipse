using System;

namespace Oritax.TaxSimp.CM.Entity
{
    public class ManagedInvestmentSchemesAccountInstall
	{
		#region INSTALLATION PROPERTIES
 
		// Assembly Installation Properties
        public const string ASSEMBLY_ID = "CD9D3D21-8199-40E7-A55E-0659A3384B34";
        public const string ASSEMBLY_NAME = "ManagedInvestmentSchemesAccount";
        public const string ASSEMBLY_DISPLAYNAME = "Managed Investment Schemes Account";
		public const string ASSEMBLY_MAJORVERSION="1";
		public const string ASSEMBLY_MINORVERSION="0";
		public const string ASSEMBLY_DATAFORMAT="0";
		public const string ASSEMBLY_REVISION="0";		

		// Component Installation Properties
        public const string COMPONENT_ID = "BF51860C-76B8-4968-A12C-65E8AF16CA3D";
        public const string COMPONENT_NAME = "ManagedInvestmentSchemesAccount";
        public const string COMPONENT_DISPLAYNAME = "Managed Investment Schemes Account";
		public const string COMPONENT_CATEGORY="BusinessEntity";

		// Component Version Installation Properties
		public const string COMPONENTVERSION_STARTDATE="01/01/1990 00:00:00 AM";
		public const string COMPONENTVERSION_ENDDATE="31/12/2050 12:59:59 AM";
		public const string COMPONENTVERSION_PERSISTERASSEMBLY="CommonPersisters_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";
		public const string COMPONENTVERSION_PERSISTERCLASS="Oritax.TaxSimp.CommonPersistence.BlobPersister";
		public const string COMPONENTVERSION_IMPLEMENTATIONCLASS="Oritax.TaxSimp.CM.Entity.ManagedInvestmentSchemesAccountCM";

		#endregion

	}
}
