using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using Oritax.TaxSimp.CM.Organization.Data;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Enum;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.DataSets;
using System.Data;
using IndividualEntity = Oritax.TaxSimp.Common.IndividualEntity;


namespace Oritax.TaxSimp.CM.Entity
{
    [Serializable]
    public partial class ManagedInvestmentSchemesAccountCM : EntityCM, ISerializable, IManagedInvestmentSchemesAccount, IOrganizationChartData, IHaveAssociation, ISupportMigration, IMISEntity
    {
        #region Private Variable
        private ManagedInvestmentSchemesAccountEntity _ManagedInvestmentSchemesAccount;
        #endregion

        #region Public Properties
        public ManagedInvestmentSchemesAccountEntity ManagedInvestmentSchemesAccountEntity
        {
            get { return _ManagedInvestmentSchemesAccount; }
        }
        #endregion

        #region Constructor
        public ManagedInvestmentSchemesAccountCM() : base() { }

        protected ManagedInvestmentSchemesAccountCM(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            _ManagedInvestmentSchemesAccount = info.GetValue<ManagedInvestmentSchemesAccountEntity>("Oritax.TaxSimp.CM.Entity.ManagedInvestmentSchemesAccount");
        }
        #endregion

        #region Override Method
        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValueX("Oritax.TaxSimp.CM.Entity.ManagedInvestmentSchemesAccount", _ManagedInvestmentSchemesAccount);
        }

        public override string OnUpdateDataStream(int type, string data)
        {
            string xml = string.Empty;
            switch ((CmCommand)type)
            {
                case CmCommand.AccountRelationAdd:
                    xml = AddAccountRelation(data);
                    break;
                case CmCommand.AccountRelationAddReference:
                    xml = AddAccountRelationReference(data);
                    break;
                case CmCommand.AccountRelationDelete:
                    DeleteAccountRelation(data);
                    break;
                case CmCommand.AccountRelationDeleteRefrence:
                    DeleteAccountRelationReference(data);
                    break;
                case CmCommand.ResetMISFunds:
                    ResetMISFunds(data);
                    break;
                default:
                    ManagedInvestmentSchemesAccountEntity entity = data.ToNewOrData<ManagedInvestmentSchemesAccountEntity>();
                    this.Name = entity.Name;
                    _ManagedInvestmentSchemesAccount = entity;



                    if (ClientId == "" || ClientId == null)
                    {
                        UpDateEventLog.UpdateLog("Managed Investment Schemes Account", _ManagedInvestmentSchemesAccount.Name, type, this.Broker);
                    }
                    else
                    {
                        UpDateEventLog.UpdateLog("Managed Investment Schemes Account", _ManagedInvestmentSchemesAccount.Name + " (" + ClientId + ")", type, this.Broker);
                    }

                    return data;
            }
            return xml;
        }

        private void ResetMISFunds(string data)
        {

            var lstData = data.ToNewOrData<List<string>>();
            if (ManagedInvestmentSchemesAccountEntity == null || ManagedInvestmentSchemesAccountEntity.FundAccounts == null)
                return;

            var misName = lstData.LastOrDefault();
            var fundCodes = lstData.Take(lstData.Count - 1).ToList();

            var funds = ManagedInvestmentSchemesAccountEntity.FundAccounts.Where(f => !fundCodes.Exists(c => c == f.Code)).ToList();

            funds.ForEach(f => ManagedInvestmentSchemesAccountEntity.FundAccounts.Remove(f));
            // funds.ForEach(f=>f.TotalShares=0);

        }

        public override string GetDataStream(int type, string data)
        {
            string xml = string.Empty;
            switch ((CmCommand)type)
            {
                case CmCommand.AccountRelationGetList:
                    xml = this._ManagedInvestmentSchemesAccount.LinkedAccounts.ToXmlString();
                    break;
                case CmCommand.AccountRelationGetEntity:
                    var entity = new AccountRelation();
                    entity.AccountName = this._ManagedInvestmentSchemesAccount.Name;
                    entity.AccountType = this.TypeName;
                    entity.Clid = this.Clid;
                    entity.Csid = this.Csid;

                    xml = entity.ToXmlString();
                    break;
                case CmCommand.XMLData:
                    xml = ExtractXML(data);
                    break;
                case CmCommand.CSVData:
                    xml = GetCSVObject();
                    break;
                default:
                    //this._ManagedInvestmentSchemesAccount.OrganizationStatus = this.OrganizationStatus;

                    SetAllUnSettledOrders();
                    xml = this._ManagedInvestmentSchemesAccount.ToXmlString();
                    // code changes from here for view log
                    //if (ClientId == "" || ClientId == null)
                    //{
                    //    UpDateEventLog.UpdateLog("Managed Investment Schemes Account", _ManagedInvestmentSchemesAccount.Name, type, this.Broker);
                    //}
                    //else
                    //{
                    //    UpDateEventLog.UpdateLog("Managed Investment Schemes Account", _ManagedInvestmentSchemesAccount.Name + " (" + ClientId + ")", type, this.Broker);
                    //}
                    // code changes till here for view log
                    break;
            }
            return xml;
        }

        public override string GetAllDataStream(int type)
        {
            return this._ManagedInvestmentSchemesAccount.ToXmlString();
        }
        #endregion

        #region GetXMLData

        public override string GetXmlData()
        {
            string xmlco = _ManagedInvestmentSchemesAccount.ToXmlString();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmlco);
            XmlAttribute rootAtt = doc.CreateAttribute("Type");
            rootAtt.Value = this.TypeName;
            doc.DocumentElement.Attributes.Append(rootAtt);
            return doc.OuterXml;
        }

        private string ExtractXML(string data)
        {
            string xml = string.Empty;
            XDocument document = XDocument.Load(XmlReader.Create(new StringReader(this._ManagedInvestmentSchemesAccount.ToXmlString())));
            document.Root.Name = data;
            document.Root.RemoveAttributes();
            xml = document.ToString().Trim();
            return xml;
        }

        #endregion

        #region GetCSVData
        private string GetCSVObject()
        {
            return _ManagedInvestmentSchemesAccount.ToXmlString();
        }
        #endregion

        public override IClientEntity ClientEntity
        {
            get
            {
                return this._ManagedInvestmentSchemesAccount;
            }

            set
            {
                this._ManagedInvestmentSchemesAccount = value as ManagedInvestmentSchemesAccountEntity;
            }
        }


        protected override void OnGetData(DataSet data)
        {
            base.OnGetData(data);

            //SetAllUnSettledOrders();
            if (data is MISTransactionDS)
            {
                MISTransactionDS misTransactionDS = (MISTransactionDS)data;
                GetMISTransactionDS(misTransactionDS);
            }
            if (data is ManagedInvestmentAccountDS)
            {
                var dataset = data as ManagedInvestmentAccountDS;
                var fundCodes = ManagedInvestmentSchemesAccountEntity.FundAccounts.Where(x =>
                    {
                        if (dataset.FundID == Guid.Empty) return true;
                        else return x.ID == dataset.FundID;

                    });
                
                foreach (var fund in fundCodes)
                {
                    DataRow dr = dataset.FundAccountsTable.NewRow();
                    dr[dataset.FundAccountsTable.CLID] = this.Clid;
                    dr[dataset.FundAccountsTable.CSID] = this.Csid;
                    dr[dataset.FundAccountsTable.CID] = this.CID;
                    dr[dataset.FundAccountsTable.FUNDID] = fund.ID;
                    dr[dataset.FundAccountsTable.FUNDCODE] = fund.Code;
                    dr[dataset.FundAccountsTable.FUNDDESCRIPTION] = fund.Description;

                    dataset.FundAccountsTable.Rows.Add(dr);
                }

            }
            if (data is MISBasicDS)
            {
                var dataset = data as MISBasicDS;
                DataRow dr = dataset.MISBasicTable.NewRow();

                dr[dataset.MISBasicTable.CID] = this.CID;
                dr[dataset.MISBasicTable.CLID] = this.Clid;
                dr[dataset.MISBasicTable.CSID] = this.Csid;
                dr[dataset.MISBasicTable.NAME] = ManagedInvestmentSchemesAccountEntity.Name;
                dataset.MISBasicTable.Rows.Add(dr);
            }
            if (data is ClientTotalUnitsDS)
            {
                var dataset = data as ClientTotalUnitsDS;
                var fundEntity = ManagedInvestmentSchemesAccountEntity.FundAccounts.FirstOrDefault(ss=>ss.Code == dataset.code);
                if (fundEntity != null)
                {
                    DataRow dr = dataset.clientTotalUnitsTable.NewRow();
                    var units = Convert.ToInt32(fundEntity.FundTransactions.Where(r => r.ClientID == dataset.clientID).Sum(dd => dd.Shares));
                    dr[dataset.clientTotalUnitsTable.TOTALUNITS] = units.ToString();
                    dataset.clientTotalUnitsTable.Rows.Add(dr);
                }
            }
        }

        public void GetMISTransactionDS(MISTransactionDS misTransactionDS)
        {
            if (!misTransactionDS.GetOnlyReconcileReport)
            {
                foreach (FundAccountEntity misFundAccEntity in this.ManagedInvestmentSchemesAccountEntity.FundAccounts)
                {
                    foreach (MISFundTransactionEntity misFundTransactionEntity in misFundAccEntity.FundTransactions)
                    {
                        DataRow row = misTransactionDS.Tables[MISTransactionDS.MISTRANTABLE].NewRow();

                        row[MISTransactionDS.CODE] = misFundAccEntity.Code;
                        row[MISTransactionDS.CLIENTIDCOL] = misFundTransactionEntity.ClientID;
                        row[MISTransactionDS.MISCID] = this.CID.ToString();
                        row[MISTransactionDS.DESCRIPTION] = misFundAccEntity.Description;
                        row[MISTransactionDS.ID] = misFundTransactionEntity.ID;
                        row[MISTransactionDS.TRADEDATE] = misFundTransactionEntity.TradeDate;
                        row[MISTransactionDS.TRANSTYPE] = misFundTransactionEntity.TransactionType;
                        row[MISTransactionDS.SHARES] = misFundTransactionEntity.Shares;
                        row[MISTransactionDS.UNITPRICE] = Math.Round(misFundTransactionEntity.UnitPrice, 4);
                        row[MISTransactionDS.AMOUNT] = misFundTransactionEntity.Amount;
                        row[MISTransactionDS.STATUS] = misFundTransactionEntity.Status;

                        misTransactionDS.Tables[MISTransactionDS.MISTRANTABLE].Rows.Add(row);
                    }
                }
            }

            var orgFiteredTable = OrgInstanceTable(this.Broker.UserContext.Identity.Name);

            foreach (FundAccountEntity misFundAccEntity in this.ManagedInvestmentSchemesAccountEntity.FundAccounts)
            {
                var groupByClientsMisFundAccEntity = misFundAccEntity.FundTransactions.GroupBy(grp => grp.ClientID);
                foreach (var clientUnit in groupByClientsMisFundAccEntity)
                {
                    decimal totalShares = Math.Round(clientUnit.Sum(c => c.Shares), 4);
                    decimal totalAmount = Math.Round(clientUnit.Sum(c => c.Amount), 2);
                    decimal totalHolding = 0;

                    var totalHoldingEntity = misFundAccEntity.SecurityHoldings.OrderByDescending(c => c.AsofDate).Where(c => c.Account == clientUnit.Key).FirstOrDefault();
                    if (totalHoldingEntity != null)
                        totalHolding = Math.Round(Convert.ToDecimal(totalHoldingEntity.TotalShares), 4);

                    decimal diffTotal = totalHolding - totalShares;

                    DataRow rowHoldingVsTran = misTransactionDS.Tables[MISTransactionDS.MISTRANHOLDINGTABLE].NewRow();

                    rowHoldingVsTran[MISTransactionDS.CODE] = misFundAccEntity.Code;
                    rowHoldingVsTran[MISTransactionDS.CLIENTIDCOL] = clientUnit.Key;
                    rowHoldingVsTran[MISTransactionDS.MISCID] = this.CID.ToString();
                    rowHoldingVsTran[MISTransactionDS.DESCRIPTION] = misFundAccEntity.Description;
                    rowHoldingVsTran[MISTransactionDS.AMOUNT] = totalAmount;
                    rowHoldingVsTran[MISTransactionDS.SHARES] = totalShares;
                    rowHoldingVsTran[MISTransactionDS.HOLDING] = totalHolding;
                    rowHoldingVsTran[MISTransactionDS.DIFF] = Math.Round(diffTotal, 4);

                    if (Math.Round(totalShares, 0) != Math.Round(totalHolding, 0))
                        rowHoldingVsTran[MISTransactionDS.ERROR] = true;
                    else
                        rowHoldingVsTran[MISTransactionDS.ERROR] = false;


                    DataRow selectedRow = orgFiteredTable.Select().Where(c => c["ENTITYECLIPSEID_FIELD"].ToString() == clientUnit.Key).FirstOrDefault();
                    if (selectedRow != null)
                    {
                        rowHoldingVsTran[MISTransactionDS.CLIENTCID] = selectedRow["ENTITYCIID_FIELD"].ToString();
                        rowHoldingVsTran[MISTransactionDS.ACCOUNTTYPE] = selectedRow["ENTITYTYPENAME_FIELD"].ToString();
                        rowHoldingVsTran[MISTransactionDS.ACCOUNTNAME] = selectedRow["ENTITYNAME_FIELD"].ToString();
                    }

                    misTransactionDS.Tables[MISTransactionDS.MISTRANHOLDINGTABLE].Rows.Add(rowHoldingVsTran);

                }
            }
        }

        private DataTable OrgInstanceTable(string userName)
        {
            IBrokerManagedComponent user = this.Broker.GetBMCInstance(userName, "DBUser_1_1");
            var orgnCm = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            if (orgnCm != null)
            {
                var args = new Object[3] {orgnCm.CLID, user.CID, 1};
                var view = new DataView(this.Broker.GetBMCDetailList(OrganizationInstall.ASSEMBLY_NAME, args).Tables[OrganisationListingDS.TableName]) {RowFilter = new OrganisationListingDS().GetRowFilter(), Sort = "ENTITYNAME_FIELD ASC"};
                DataTable orgFiteredTable = view.ToTable();
                return orgFiteredTable;
            }
            return null;
        }



        public void GetInstanceData(DataTable orgFiteredTable,string ClientId,Action<Guid,string,string> callback)
        {
            DataRow selectedRow = orgFiteredTable.Select().Where(c => c["ENTITYECLIPSEID_FIELD"].ToString() == ClientId).FirstOrDefault();
                    if (selectedRow != null&&callback!=null)
                    {
                        callback(Guid.Parse(selectedRow["ENTITYCIID_FIELD"].ToString()), selectedRow["ENTITYTYPENAME_FIELD"].ToString(), selectedRow["ENTITYNAME_FIELD"].ToString());
                       
                    }
        }

        protected override Calculation.BrokerManagedComponent.ModifiedState OnSetData(System.Data.DataSet data)
        {
            base.OnSetData(data);

            if (data is SMAImportProcessDS)
            {

                SMAImportProcessDS smaImportProcessDS = data as SMAImportProcessDS;
                foreach (DataRow assetCodeRow in smaImportProcessDS.Tables["InvestmentProfileAssetViewModel"].Rows)
                {
                    string assetCode = assetCodeRow["AssetLongCode"].ToString();
                    if (assetCode.StartsWith("IV"))
                    {
                        var misfund = ManagedInvestmentSchemesAccountEntity.FundAccounts.FirstOrDefault(fund => fund.Code == assetCode);
                        var oldTransactionsList = misfund.FundTransactions.Where(ee => ee.ClientID == smaImportProcessDS.ClientID).ToList();

                        foreach (var itemToRemove in oldTransactionsList)
                            misfund.FundTransactions.Remove(itemToRemove);

                        foreach (DataTable table in smaImportProcessDS.Tables)
                        {
                            if (table.TableName.StartsWith("AssetTransactionViewModelIV") && table.TableName.Contains(assetCode))
                            {
                                foreach (DataRow row in table.Rows)
                                    ImportTransactionsSMA(row, assetCode, smaImportProcessDS.ClientID);
                            }
                        }

                        var newTransactionList = misfund.FundTransactions.Where(ee => ee.ClientID == smaImportProcessDS.ClientID);

                        foreach (var itemToCheck in oldTransactionsList)
                        {
                            var newTran = newTransactionList.Where(ee => ee.Shares.ToString("N4") == itemToCheck.Shares.ToString("N4") && ee.TradeDate == itemToCheck.TradeDate).FirstOrDefault();
                            
                            if (newTran != null)
                                newTran.ID = itemToCheck.ID;
                            else
                                misfund.DeletedFundTransactions.Add(itemToCheck);
                        }
                    }
                }

                if (smaImportProcessDS.Tables[SMAImportProcessDS.HOLDINGVIEWTABLE] != null)
                {
                    foreach (DataRow holdingRow in smaImportProcessDS.Tables[SMAImportProcessDS.HOLDINGVIEWTABLE].Rows)
                    {
                        string assetCode = holdingRow["LongAssetCode"].ToString();
                        if (assetCode.StartsWith("IV"))
                        {
                            var secEntity = smaImportProcessDS.Securities.Where(sec => sec.AsxCode == assetCode).FirstOrDefault();
                            decimal holding = decimal.Parse(holdingRow["DollarBalance"].ToString());
                            decimal holdingUnits = decimal.Parse(holdingRow["UnitBalance"].ToString());
                            decimal redemptionPrice = decimal.Parse(holdingRow["UnitPrice"].ToString());

                            var secHoldingNew = new MISUnitHolderFile { Account = smaImportProcessDS.ClientID, AsofDate = DateTime.Now.Date, CurrentValue = Convert.ToDouble(Math.Round(holding, 4)), FundCode = assetCode, FundName = secEntity.CompanyName, RedemptionPrice = Math.Round(Convert.ToDouble(redemptionPrice), 6), SubAccount = "IV", TotalShares = Math.Round(Convert.ToDouble(holdingUnits), 4) };
                            var secHoldingOld = _ManagedInvestmentSchemesAccount.FundAccounts.Where(fundAccount => fundAccount.Code == assetCode).FirstOrDefault().SecurityHoldings.Where(er => er.Account == smaImportProcessDS.ClientID && er.FundCode == assetCode).FirstOrDefault();

                            if (secHoldingOld != null)
                                _ManagedInvestmentSchemesAccount.FundAccounts.Where(fundAccount => fundAccount.Code == assetCode).FirstOrDefault().SecurityHoldings.Remove(secHoldingOld);

                            _ManagedInvestmentSchemesAccount.FundAccounts.Where(fundAccount => fundAccount.Code == assetCode).FirstOrDefault().SecurityHoldings.Add(secHoldingNew);
                        }
                    }
                }
            }

            if (data is MISTransactionDetailsDS)
            {
                MISTransactionDetailsDS misTransactionDetailsDS = (MISTransactionDetailsDS)data;


                if (misTransactionDetailsDS.DataSetOperationType == DataSetOperationType.NewSingle)
                {
                    DataRow rowEntity = data.Tables[MISTransactionDetailsDS.MISTRANSDETAILSTABLE].Rows[0];
                    MISFundTransactionEntity misFundTransactionEntity = MISFundTransactionEntity.GetFundAccountEntity(rowEntity);
                    string fundCode = rowEntity[MISTransactionDetailsDS.FUNDCODE].ToString();

                    var linkedMISAccount = this.ManagedInvestmentSchemesAccountEntity.FundAccounts.FirstOrDefault(fa => fa.Code == fundCode);
                    if (linkedMISAccount != null)
                    {
                        var orgFiteredTable = OrgInstanceTable(this.Broker.UserContext.Identity.Name);
                        GetInstanceData(orgFiteredTable, misFundTransactionEntity.ClientID, (clientCid, clientType, clientName) => MatcMISTransactionWithDistribution(misFundTransactionEntity, clientCid, linkedMISAccount.ID, fundCode));
                        linkedMISAccount.FundTransactions.Add(misFundTransactionEntity);
                    }
                }

                else if (misTransactionDetailsDS.DataSetOperationType == DataSetOperationType.UpdateSingle)
                {
                    DataRow rowEntity = data.Tables[MISTransactionDetailsDS.MISTRANSDETAILSTABLE].Rows[0];
                    MISFundTransactionEntity misFundTransactionEntity = MISFundTransactionEntity.GetFundAccountEntity(rowEntity);
                    string fundCode = rowEntity[MISTransactionDetailsDS.FUNDCODEOLD].ToString();
                    string fundCodeNew = rowEntity[MISTransactionDetailsDS.FUNDCODE].ToString();
                    Guid fundID = (Guid)rowEntity[MISTransactionDetailsDS.ID];
                    var linkedMISAccount = this.ManagedInvestmentSchemesAccountEntity.FundAccounts.FirstOrDefault(fa => fa.Code == fundCode);
                    if (linkedMISAccount != null)
                    {
                        if (fundCode != fundCodeNew)
                        {
                            MISFundTransactionEntity misFundTransactionEntityToUpdate = linkedMISAccount.FundTransactions.FirstOrDefault(tran => tran.ID == misFundTransactionEntity.ID);

                            if (misFundTransactionEntityToUpdate != null)
                            {
                                if (linkedMISAccount.DeletedFundTransactions == null)
                                    linkedMISAccount.DeletedFundTransactions = new ObservableCollection<MISFundTransactionEntity>();
                                //add to deleted transactions
                                linkedMISAccount.DeletedFundTransactions.Add(misFundTransactionEntityToUpdate);
                                linkedMISAccount.FundTransactions.Remove(misFundTransactionEntityToUpdate);
                            }

                            misFundTransactionEntity.ID = Guid.NewGuid();
                            linkedMISAccount = this.ManagedInvestmentSchemesAccountEntity.FundAccounts.FirstOrDefault(fa => fa.Code == fundCodeNew);
                            if (linkedMISAccount != null)
                            {
                                linkedMISAccount.FundTransactions.Add(misFundTransactionEntity);
                                var orgFiteredTable = OrgInstanceTable(this.Broker.UserContext.Identity.Name);
                                GetInstanceData(orgFiteredTable, misFundTransactionEntity.ClientID, (clientCid, clientType, clientName) => MatcMISTransactionWithDistribution(misFundTransactionEntity, clientCid, linkedMISAccount.ID, fundCode));
                      
                            }
                        }
                        else
                        {
                            MISFundTransactionEntity misFundTransactionEntityToUpdate = linkedMISAccount.FundTransactions.FirstOrDefault(tran => tran.ID == misFundTransactionEntity.ID);

                            if (misFundTransactionEntityToUpdate != null)
                            {
                                misFundTransactionEntityToUpdate.Shares = misFundTransactionEntity.Shares;
                                misFundTransactionEntityToUpdate.UnitPrice = misFundTransactionEntity.UnitPrice;
                                misFundTransactionEntityToUpdate.TransactionType = misFundTransactionEntity.TransactionType;
                                var orgFiteredTable = OrgInstanceTable(this.Broker.UserContext.Identity.Name);
                                GetInstanceData(orgFiteredTable, misFundTransactionEntityToUpdate.ClientID, (clientCid, clientType, clientName) => MatcMISTransactionWithDistribution(misFundTransactionEntityToUpdate, clientCid, linkedMISAccount.ID, fundCode));
                      
                            }
                        }
                    }
                }
                else if (misTransactionDetailsDS.DataSetOperationType == DataSetOperationType.DeletSingle)
                {
                    DataRow rowEntity = data.Tables[MISTransactionDetailsDS.MISTRANSDETAILSTABLE].Rows[0];
                    string fundCode = rowEntity[MISTransactionDetailsDS.FUNDCODE].ToString();
                    Guid transID = (Guid)rowEntity[MISTransactionDetailsDS.ID];

                    var linkedMISAccount = this.ManagedInvestmentSchemesAccountEntity.FundAccounts.FirstOrDefault(fa => fa.Code == fundCode);
                    if (linkedMISAccount != null)
                    {
                        MISFundTransactionEntity misFundTransactionEntityToUpdate = linkedMISAccount.FundTransactions.FirstOrDefault(tran => tran.ID == transID);
                        if (misFundTransactionEntityToUpdate != null)
                        {
                            if (linkedMISAccount.DeletedFundTransactions == null)
                            {
                                linkedMISAccount.DeletedFundTransactions = new ObservableCollection<MISFundTransactionEntity>();
                            }
                            //add to deleted transactions
                            linkedMISAccount.DeletedFundTransactions.Add(misFundTransactionEntityToUpdate);

                            linkedMISAccount.FundTransactions.Remove(misFundTransactionEntityToUpdate);
                        }
                    }
                }
                else if (misTransactionDetailsDS.DataSetOperationType == DataSetOperationType.DeletBulk)
                {
                    foreach (var misAccount in this.ManagedInvestmentSchemesAccountEntity.FundAccounts)
                    {
                        var toDeleteTransactions = misAccount.FundTransactions.Where(c => c.ClientID == misTransactionDetailsDS.ClientID).ToList();
                        foreach (MISFundTransactionEntity misEntityTran in toDeleteTransactions)
                            misAccount.FundTransactions.Remove(misEntityTran);
                    }
                }
            }
            else if (data is ImportProcessDS)
            {
                var ds = data as ImportProcessDS;
                switch ((WebCommands)ds.Command)
                {
                    case WebCommands.ImportMISFundTransactionsImportFile:
                        DataRow[] drs = ds.Tables[0].Select("HasErrors=false");
                        if (drs.Length > 0)
                        {
                            foreach (var dr in drs)
                            {
                                dr["Message"] = "";
                                dr["HasErrors"] = false;
                                dr["IsMissingItem"] = false;
                                ImportTransactions(dr);
                            }
                        }
                        break;
                    case WebCommands.ImportMISUnitHoldImportFile:
                        ImportTransactionsUnitHolderBalance(ds);
                        break;
                }
            }


            Name = _ManagedInvestmentSchemesAccount.Name;

            return ModifiedState.MODIFIED;
        }
        public void Export(XElement root)
        {
            //_ManagedInvestmentSchemesAccount.OrganizationStatus = this.OrganizationStatus;
            root.Add(_ManagedInvestmentSchemesAccount.ToXElement("ManagedInvestmentSchemesAccount"));
        }

        public void Import(XElement root, IdentityCmMap map)
        {
            _ManagedInvestmentSchemesAccount = root.FromXElement<ManagedInvestmentSchemesAccountEntity>("ManagedInvestmentSchemesAccount");
            //OrganizationStatus = _ManagedInvestmentSchemesAccount.OrganizationStatus;
            Name = _ManagedInvestmentSchemesAccount.Name;
        }

        #region DoTagged
        public override void DoTagged(EntityTag tag, Guid modelId)
        {
            _ManagedInvestmentSchemesAccount.DoTaggedExtension<ManagedInvestmentSchemesAccountEntity>(tag);
        }
        #endregion

        #region OrganizationsChart

        public OrganizationChart GetOrganizationChart()
        {
            OrganizationChart chart = new OrganizationChart();

            chart.Clid = this.CLID;
            chart.Csid = this.CSID;
            chart.NodeName = this._ManagedInvestmentSchemesAccount.Name;

            return chart;
        }

        public OrganizationChart GetAssociation(IIdentityCM identitycm, OrganizationType organizationtype)
        {
            OrganizationChart OChart = new OrganizationChart();
            return OChart;
        }

        public OrganizationChart GetEntityParent(OrganizationChart organizationchart)
        {
            return null;
        }

        public OrganizationChart GetEntityChild()
        {
            return null;
        }

        public OrganizationChart GetEntityChildWithEntity()
        {
            return null;
        }

        #endregion

        #region AccountRelationList

        public string AddAccountRelation(string data)
        {
            AccountRelation newitem = data.ToNewOrData<AccountRelation>();
            if (this._ManagedInvestmentSchemesAccount.LinkedAccounts.Exists(p => p == newitem)) return "false";

            this._ManagedInvestmentSchemesAccount.LinkedAccounts.Add(newitem);
            IBrokerManagedComponent component = Broker.GetCMImplementation(newitem.Clid, newitem.Csid) as IBrokerManagedComponent;
            component.UpdateDataStream((int)CmCommand.AccountRelationAddReference, (new AccountRelation() { Clid = this.Clid, Csid = this.Csid, AccountType = newitem.AccountType, ServiceType = newitem.ServiceType }).ToXmlString());

            return "true";
        }
        public string AddAccountRelationReference(string data)
        {
            AccountRelation newitem = data.ToNewOrData<AccountRelation>();
            if (this._ManagedInvestmentSchemesAccount.LinkedAccounts.Exists(p => p == newitem)) return "false";
            this._ManagedInvestmentSchemesAccount.LinkedAccounts.Add(newitem);
            return "true";
        }

        public string DeleteAccountRelation(string data)
        {
            AccountRelation newitem = data.ToNewOrData<AccountRelation>();

            var list = this._ManagedInvestmentSchemesAccount.LinkedAccounts.Where(e => e.Clid == newitem.Clid && e.Csid == newitem.Csid).ToList();
            foreach (var each in list)
            {
                this._ManagedInvestmentSchemesAccount.LinkedAccounts.Remove(each);
            }

            IBrokerManagedComponent component = Broker.GetCMImplementation(newitem.Clid, newitem.Csid) as IBrokerManagedComponent;
            component.UpdateDataStream((int)CmCommand.AccountRelationDeleteRefrence, (new AccountRelation() { Clid = this.Clid, Csid = this.Csid, AccountType = newitem.AccountType }).ToXmlString());

            return "true";
        }
        public string DeleteAccountRelationReference(string data)
        {
            AccountRelation newitem = data.ToNewOrData<AccountRelation>();
            var list = this._ManagedInvestmentSchemesAccount.LinkedAccounts.Where(e => e.Clid == newitem.Clid && e.Csid == newitem.Csid).ToList();
            foreach (var each in list)
            {
                this._ManagedInvestmentSchemesAccount.LinkedAccounts.Remove(each);
            }
            return "true";
        }

        #endregion


        #region OrderPaidimport

        public override List<ImportMessages> ImportPaidOrders(System.Xml.Linq.XElement root)
        {
            List<ImportMessages> messages = new List<ImportMessages>();

            if (this.ManagedInvestmentSchemesAccountEntity.FundAccounts != null)
            {
                string FundCode = root.XPathSelectElement("FundCode").Value;
                var misfund = this.ManagedInvestmentSchemesAccountEntity.FundAccounts.Where(fund => fund.Code == FundCode).FirstOrDefault();



                MISPaidOrderEntity misPaidOrderEntity = CreateMISOrderPaidMessage(root, messages, (misfund != null), misfund);

                if (misfund != null && misPaidOrderEntity != null)
                {
                    this.ManagedInvestmentSchemesAccountEntity.UploadTime = DateTime.Now;
                    if (misfund.PaidOrders == null)
                    {
                        misfund.PaidOrders = new ObservableCollection<MISPaidOrderEntity>();
                    }




                    //settle if order already exists in  transactions

                    if (misfund.FundTransactions != null)
                    {

                        // first check if any amount with same amount exits
                        var temptrans = misfund.FundTransactions.Where(
                            tran =>
                            tran.TradeDate == misPaidOrderEntity.TradeDate && Math.Floor(tran.Amount) == Math.Floor(misPaidOrderEntity.Amount) &&
                            tran.ClientID == misPaidOrderEntity.UnitholderCode && tran.TransactionType == "100").FirstOrDefault();



                        // if not then check if any share match with same number of shares
                        if (temptrans == null)
                        {
                            temptrans = misfund.FundTransactions.Where(
                               tran => tran.TradeDate == misPaidOrderEntity.TradeDate && tran.Shares == misPaidOrderEntity.NumberofShares &&
                               tran.ClientID == misPaidOrderEntity.UnitholderCode && tran.TransactionType == "690").FirstOrDefault();

                        }

                        if (temptrans != null)
                        {


                            misPaidOrderEntity.IsSettled = true;
                            misPaidOrderEntity.SettelerTransactionID = temptrans.ID.ToString();

                        }
                    }



                    misfund.PaidOrders.Add(misPaidOrderEntity);

                    SetUnsetlledOrders(misPaidOrderEntity.UnitholderCode, misfund);

                }
            }

            return messages;
        }

        private MISPaidOrderEntity CreateMISOrderPaidMessage(XElement asxPrice, List<ImportMessages> messages, bool IsFundCodeAttached, FundAccountEntity misfund)
        {
            MISPaidOrderMessage message = new MISPaidOrderMessage();

            MISPaidOrderEntity fundTrans = new MISPaidOrderEntity();




            DateTimeFormatInfo info = new DateTimeFormatInfo();
            info.ShortDatePattern = "dd/MM/yyyy";

            string UnitholderCode = asxPrice.XPathSelectElement("UnitholderCode").Value;
            string UnitholderSubCode = asxPrice.XPathSelectElement("UnitholderSubCode").Value;
            string FundCode = asxPrice.XPathSelectElement("FundCode").Value;
            string MCHCode = asxPrice.XPathSelectElement("MCHCode").Value;
            string TradeDate = asxPrice.XPathSelectElement("TradeDate").Value;
            string ClearDate = asxPrice.XPathSelectElement("ClearDate").Value;
            string Amount = asxPrice.XPathSelectElement("Amount").Value;
            string TransactionType = asxPrice.XPathSelectElement("TransactionType").Value;
            string CurrencyOfTrade = asxPrice.XPathSelectElement("CurrencyOfTrade").Value;
            string PurchaseSource = asxPrice.XPathSelectElement("PurchaseSource").Value;
            string ProcessAtNAV = asxPrice.XPathSelectElement("ProcessAtNAV").Value;
            string Status = asxPrice.XPathSelectElement("Status").Value;


            string RedemptionOrExchangeOptions = asxPrice.XPathSelectElement("RedemptionOrExchangeOptions").Value;
            string NumberofSharesNotCorrect = asxPrice.XPathSelectElement("NumberofShares").Value;
            string ExchangeToFund = asxPrice.XPathSelectElement("ExchangeToFund").Value;

            //Need to remove this code
            string NumberofShares = string.Empty;

            if (TransactionType == "690")
            {
                NumberofShares = String.Concat(NumberofSharesNotCorrect.Remove(0, 1), ExchangeToFund.Remove(ExchangeToFund.Count() - 1, 1));
                ExchangeToFund = string.Empty;
            }

            string DealerCommissionRate = asxPrice.XPathSelectElement("DealerCommissionRate").Value;

            string ExternalID = asxPrice.XPathSelectElement("ExternalID").Value;
            string RedemptionSource = asxPrice.XPathSelectElement("RedemptionSource").Value;
            string OperatorEnteredBy = asxPrice.XPathSelectElement("OperatorEnteredBy").Value;

            string Generator = asxPrice.XPathSelectElement("Generator").Value;
            string EffectiveDate = asxPrice.XPathSelectElement("EffectiveDate").Value;
            string AUDEquivalentAmount = asxPrice.XPathSelectElement("AUDEquivalentAmount").Value;


            DateTime date;
            decimal utemp;
            try
            {
                fundTrans.ID = Guid.NewGuid();
                message.Status = ImportMessageStatus.Sucess;

                message.MessageType = ImportMessageType.Sucess;

                message.UnitholderCode = UnitholderCode;
                message.UnitholderSubCode = UnitholderSubCode;
                message.FundCode = FundCode;
                message.MCHCode = MCHCode;
                message.TradeDate = TradeDate;
                message.ClearDate = ClearDate;
                message.Amount = Amount;
                message.TransactionType = TransactionType;
                message.CurrencyOfTrade = CurrencyOfTrade;
                message.PurchaseSource = PurchaseSource;
                message.ProcessAtNAV = ProcessAtNAV;


                message.RedemptionOrExchangeOptions = RedemptionOrExchangeOptions;
                message.NumberofShares = NumberofShares;
                message.ExchangeToFund = ExchangeToFund;
                message.DealerCommissionRate = DealerCommissionRate;

                message.ExternalID = ExternalID;
                message.RedemptionSource = RedemptionSource;
                message.OperatorEnteredBy = OperatorEnteredBy;

                message.Generator = Generator;
                message.EffectiveDate = EffectiveDate;
                message.AUDEquivalentAmount = AUDEquivalentAmount;


                fundTrans.UnitholderCode = UnitholderCode;
                fundTrans.UnitholderSubCode = UnitholderSubCode;
                fundTrans.FundCode = FundCode;
                fundTrans.MCHCode = MCHCode;

                fundTrans.TransactionType = TransactionType;
                fundTrans.CurrencyOfTrade = CurrencyOfTrade;
                fundTrans.PurchaseSource = PurchaseSource;
                fundTrans.ProcessAtNAV = ProcessAtNAV;


                fundTrans.RedemptionOrExchangeOptions = RedemptionOrExchangeOptions;

                fundTrans.ExchangeToFund = ExchangeToFund;
                fundTrans.DealerCommissionRate = DealerCommissionRate;

                fundTrans.ExternalID = ExternalID;
                fundTrans.RedemptionSource = RedemptionSource;
                fundTrans.OperatorEnteredBy = OperatorEnteredBy;

                fundTrans.Generator = Generator;

                fundTrans.AUDEquivalentAmount = AUDEquivalentAmount;


                fundTrans.Status = Status;

                if (!IsFundCodeAttached)
                {
                    message.Message += "Fund code (" + FundCode + ") doesn't exists Or Fund code is not attached with any client,";

                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                }

                if (!DateTime.TryParseExact(TradeDate, "yyyyMMdd", info, DateTimeStyles.None, out date))
                {

                    message.Message += "Invalid Price Date,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                }
                else
                {
                    message.TradeDate = date.ToString("dd/MM/yyyy");
                    fundTrans.TradeDate = date;
                }

                if (!string.IsNullOrEmpty(EffectiveDate))
                    if (!DateTime.TryParseExact(EffectiveDate, "yyyyMMdd", info, DateTimeStyles.None, out date))
                    {

                        message.Message += "Invalid Effective Date,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                    }
                    else
                    {
                        message.EffectiveDate = date.ToString("dd/MM/yyyy");
                        fundTrans.EffectiveDate = date;
                    }

                if (!DateTime.TryParseExact(ClearDate, "yyyyMMdd", info, DateTimeStyles.None, out date))
                {

                    message.Message += "Invalid Clear Date,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                }
                else
                {
                    message.ClearDate = date.ToString("dd/MM/yyyy");
                    fundTrans.ClearDate = date;
                }




                if (!string.IsNullOrEmpty(Amount))
                    if (!decimal.TryParse(Amount, NumberStyles.Currency, CultureInfo.CurrentCulture, out utemp))
                    {
                        message.Message += "Invalid Amount,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                    }
                    else
                    {

                        // string format = "$#,##0.00;-$#,##0.00;$0";

                        message.Amount = utemp.ToString("C");
                        fundTrans.Amount = utemp;
                    }

                if (!string.IsNullOrEmpty(NumberofShares))
                    if (!decimal.TryParse(NumberofShares, NumberStyles.Currency, CultureInfo.CurrentCulture, out utemp))
                    {
                        message.Message += "Invalid Number of Shares,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                    }
                    else
                    {

                        fundTrans.NumberofShares = utemp;

                        message.NumberofShares = utemp.ToString("C");
                    }



                if (message.MessageType != ImportMessageType.Error)
                {

                    if (IsFundCodeAttached && misfund.PaidOrders != null)
                    {
                        var oldTransactions = misfund.PaidOrders.Where(ee => Math.Floor(ee.Amount) == Math.Floor(fundTrans.Amount) && ee.UnitholderCode == fundTrans.UnitholderCode && ee.TradeDate == fundTrans.TradeDate && ee.ClearDate == fundTrans.ClearDate);

                        if (TransactionType == "690")
                            oldTransactions = misfund.PaidOrders.Where(ee => ee.NumberofShares == fundTrans.NumberofShares && ee.UnitholderCode == fundTrans.UnitholderCode && ee.TradeDate == fundTrans.TradeDate && ee.ClearDate == fundTrans.ClearDate);

                        if (oldTransactions != null && oldTransactions.Count() > 0)
                        {
                            for (int i = oldTransactions.Count() - 1; i >= 0; i--)
                            {
                                misfund.PaidOrders.Remove(oldTransactions.ElementAt(i));
                            }


                            //message.Message += "Order pad already exists,";
                            //message.Status = ImportMessageStatus.Failed;
                            //message.MessageType = ImportMessageType.Error;
                            //oldTransactions.NumberofShares = fundTrans.NumberofShares;
                            //oldTransactions.Amount = fundTrans.Amount;
                        }
                    }
                }


                if (!string.IsNullOrEmpty(message.Message) && message.Message.Contains(','))
                {
                    message.Message = message.Message.Remove(message.Message.LastIndexOf(','));
                }
                else
                {
                    message.Message = "";
                }

            }
            catch
            {

                message = null;
            }
            messages.Add(message);

            if (message.MessageType == ImportMessageType.Error)
            {
                fundTrans = null;
            }

            return fundTrans;
        }

        #endregion

        public void ImportTransactions(DataRow dataRow)
        {
            if (ManagedInvestmentSchemesAccountEntity.FundAccounts != null)
            {
                string fundCode = dataRow["FundCode"].ToString();
                var misfund = ManagedInvestmentSchemesAccountEntity.FundAccounts.FirstOrDefault(fund => fund.Code == fundCode);
                ManagedInvestmentSchemesAccountEntity.UploadTime = DateTime.Now;
                MISFundTransactionEntity fundTransaction = CreateMessage(dataRow, (misfund != null), misfund);

                if (misfund != null && fundTransaction != null)
                {
                    SettleUnsettledOrders(fundCode, fundTransaction, (dataRow.Table.DataSet as IHasOrganizationUnit).Unit.CurrentUser);
                    MatcMISTransactionWithDistribution(fundTransaction, Guid.Parse(dataRow["ClientCID"].ToString()), misfund.ID, fundCode);
                    AddFundTransaction(misfund, fundTransaction);
                }
            }
        }
        private void MatcMISTransactionWithDistribution(MISFundTransactionEntity fundTransaction, Guid parentCID, Guid fundId,string FundCode)
        {
            if (parentCID != Guid.Empty)//if  the clientId is not empty
            {
                var client = Broker.GetBMCInstance(parentCID) as IHasDistributionsAndDividends;
                if (client != null)
                {
                    client.MatchMISTransactionWithDistribution(fundTransaction, new Common.IdentityCMDetail
                    {
                        Cid = CID,
                        Clid = Clid,
                        Csid = Csid,
                        FundID = fundId
                    }, FundCode);
                }

            }
        }
        private void SettleUnsettledOrders(string transfundCode,MISFundTransactionEntity fundTransaction,UserEntity user)
        {
            var cid = CheckIfSettelledCmExists(fundTransaction.ClientID, Broker,user);

            if (cid != Guid.Empty)
            {
                var ds = new SettledUnsetteledTransactionDS();
                ds.Unit = new Data.OrganizationUnit
                    {
                        Name = fundTransaction.ClientID,
                        ClientId = fundTransaction.ClientID,
                        Type = ((int) OrganizationType.SettledUnsettled).ToString(),
                        CurrentUser = user,
                    };
                ds.CommandType = DatasetCommandTypes.Settle;

                var dt = new DataTable("MisTransactions");
                var dc = new DataColumn("TransactionID", typeof (Guid));
                dc.DefaultValue = fundTransaction.ID;
                var tradedate = new DataColumn("TradeDate", typeof(string));
                tradedate.DefaultValue = fundTransaction.TradeDate.ToString("dd/MM/yyyy");;
                var amount = new DataColumn("Amount", typeof(decimal));
                amount.DefaultValue = fundTransaction.Amount;
                var shares = new DataColumn("Shares", typeof(decimal));
                shares.DefaultValue = fundTransaction.Shares;
                var transactionType = new DataColumn("TransactionType", typeof(string));
                transactionType.DefaultValue = fundTransaction.TransactionType;
                var fundCode = new DataColumn("FundCode", typeof(string));
                fundCode.DefaultValue = transfundCode;
                dt.Columns.Add(dc);
                dt.Columns.Add(tradedate);
                dt.Columns.Add(amount);
                dt.Columns.Add(shares);
                dt.Columns.Add(transactionType);
                dt.Columns.Add(fundCode);
                dt.Rows.Add(dt.NewRow());
                ds.Tables.Add(dt);
                var settledUnsettledCm = Broker.GetBMCInstance(cid);
                settledUnsettledCm.SetData(ds);
                Broker.ReleaseBrokerManagedComponent(settledUnsettledCm);
            }
        }

        public void ImportTransactionsSMA(DataRow dataRow,string assetCode, string clientID)
        {
            if (ManagedInvestmentSchemesAccountEntity.FundAccounts != null)
            {
                var misfund = ManagedInvestmentSchemesAccountEntity.FundAccounts.FirstOrDefault(fund => fund.Code == assetCode);
                ManagedInvestmentSchemesAccountEntity.UploadTime = DateTime.Now;
                MISFundTransactionEntity fundTransaction = CreateMessageSMA(dataRow, (misfund != null), misfund, assetCode, clientID);
                if(fundTransaction != null)     
                    AddFundTransactionSMA(misfund, fundTransaction);
            }
        }

        private static Guid CheckIfSettelledCmExists(string clientID, ICMBroker broker, UserEntity user)
        {
            var ds = new SettledUnsetteledDS
            {
                Unit = new Data.OrganizationUnit
                {
                    Name = clientID,
                    CurrentUser = user,
                    Type = ((int)OrganizationType.SettledUnsettled).ToString(),
                },
                CommandType = DatasetCommandTypes.Check,
                Command = (int)WebCommands.GetOrganizationUnitsByType

            };

            var org = broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            org.GetData(ds);
            broker.ReleaseBrokerManagedComponent(org);

            return ds.Unit.Cid;
        }

        public override List<ImportMessages> ImportTransactions(System.Xml.Linq.XElement root)
        {
            List<ImportMessages> messages = new List<ImportMessages>();

            if (this.ManagedInvestmentSchemesAccountEntity.FundAccounts != null)
            {
                string FundCode = root.XPathSelectElement("FundCode").Value;
                var misfund = this.ManagedInvestmentSchemesAccountEntity.FundAccounts.Where(fund => fund.Code == FundCode).FirstOrDefault();
                this.ManagedInvestmentSchemesAccountEntity.UploadTime = DateTime.Now;
                MISFundTransactionEntity fundTransaction = CreateMessage(root, messages, (misfund != null), misfund);
                if (misfund != null && fundTransaction != null)
                {
                    AddFundTransaction(misfund, fundTransaction);
                }
            }

            return messages;
        }

        private void AddFundTransaction(FundAccountEntity misfund, MISFundTransactionEntity fundTransaction)
        {

            if (misfund.FundTransactions == null)
            {
                misfund.FundTransactions = new ObservableCollection<MISFundTransactionEntity>();
            }

            //add UnsettledOrder Settledment code here

            if (misfund.PaidOrders != null)
            {
                // first check if any amount with same amount exits
                var paidorder = misfund.PaidOrders.Where(
                    order =>
                    order.IsSettled == false && order.TradeDate == fundTransaction.TradeDate &&
                    Math.Floor(order.Amount) == Math.Floor(fundTransaction.Amount) &&
                    order.UnitholderCode == fundTransaction.ClientID && order.TransactionType == "100").FirstOrDefault();


                // if not then check if any share match with same number of shares
                if (paidorder == null)
                {
                    paidorder = misfund.PaidOrders.Where(
                        order =>
                        order.IsSettled == false && order.TradeDate == fundTransaction.TradeDate &&
                        order.NumberofShares == fundTransaction.Shares &&
                        order.UnitholderCode == fundTransaction.ClientID && order.TransactionType == "690").FirstOrDefault();
                }

                if (paidorder != null)
                {
                    paidorder.IsSettled = true;
                    paidorder.SettelerTransactionID = fundTransaction.ID.ToString();
                }
            }

            SetUnsetlledOrders(fundTransaction.ClientID, misfund);
            misfund.FundTransactions.Add(fundTransaction);
        }

        private void AddFundTransactionSMA(FundAccountEntity misfund, MISFundTransactionEntity fundTransaction)
        {

            if (misfund.FundTransactions == null)
                misfund.FundTransactions = new ObservableCollection<MISFundTransactionEntity>();

            misfund.FundTransactions.Add(fundTransaction);
        }

        private void SetAllUnSettledOrders()
        {

            foreach (var misFund in this.ManagedInvestmentSchemesAccountEntity.FundAccounts)
            {

                if (misFund.PaidOrders == null)
                {
                    misFund.UnsettledOrders = new ObservableCollection<MISUnsettledOrderEntity>();
                    continue;
                }

                var orders = from t in misFund.PaidOrders
                             group t by t.UnitholderCode into g
                             select g.FirstOrDefault();

                foreach (var holdingTransactionse in orders)
                {
                    SetUnsetlledOrders(holdingTransactionse.UnitholderCode, misFund);
                }
            }
        }


        private void SetUnsetlledOrders(string clientId, FundAccountEntity misfund)
        {
            if (misfund.PaidOrders == null)
            {
                misfund.UnsettledOrders = new ObservableCollection<MISUnsettledOrderEntity>();
                return;
            }

            if (misfund.UnsettledOrders == null)
            {
                misfund.UnsettledOrders = new ObservableCollection<MISUnsettledOrderEntity>();


            }

            var order = misfund.PaidOrders.Where(temp => temp.IsSettled == false && temp.UnitholderCode == clientId);
            var unseOrder = misfund.UnsettledOrders.Where(sh => sh.ClientID == clientId).FirstOrDefault();
            //reset unsetlled orders
            if (unseOrder != null)
            {
                unseOrder.UnsettledOrders = 0;
                unseOrder.UnsettledUnits = 0;
            }
            foreach (var misPaidOrderEntity in order)
            {

                if (unseOrder == null)
                {
                    misfund.UnsettledOrders.Add(new MISUnsettledOrderEntity() { ID = Guid.NewGuid(), ClientID = misPaidOrderEntity.UnitholderCode, UnsettledOrders = misPaidOrderEntity.Amount, UnsettledUnits = misPaidOrderEntity.NumberofShares });
                }
                else
                {
                    unseOrder.UnsettledOrders += misPaidOrderEntity.Amount;
                    unseOrder.UnsettledUnits += misPaidOrderEntity.NumberofShares;
                }
            }

        }


        private MISFundTransactionEntity CreateMessage(DataRow dataRow, bool isFundCodeAttached, FundAccountEntity misfund)
        {
            MISFundTransactionEntity fundTrans = new MISFundTransactionEntity();
            fundTrans.DistributionStatus = TransectionStatus.Loaded;
            DateTimeFormatInfo info = new DateTimeFormatInfo();
            info.ShortDatePattern = "dd/MM/yyyy";

            string fundCode = dataRow["FundCode"].ToString();
            string tradeDate = dataRow["TradeDate"].ToString();
            string account = dataRow["Account"].ToString();

            string transactionType = dataRow["TransactionType"].ToString();
            string shares = dataRow["Shares"].ToString();
            string unitPrice = dataRow["UnitPrice"].ToString();
            string investorCategory = dataRow["InvestorCategory"].ToString();

            try
            {
                fundTrans.ID = Guid.NewGuid();
                fundTrans.InvestorCategory = investorCategory;
                fundTrans.TransactionType = transactionType;
                fundTrans.ClientID = account;
                fundTrans.Status = "Uploaded";

                if (!isFundCodeAttached)
                {
                    dataRow["Message"] += "Fund Code (" + fundCode + ") does not exist. Or Fund Code is not attached with any client,";
                    dataRow["HasErrors"] = true;
                    dataRow["IsMissingItem"] = true;
                }

                DateTime date;
                if (!DateTime.TryParse(tradeDate, info, DateTimeStyles.None, out date))
                {

                    dataRow["Message"] += "Invalid Price Date,";
                    dataRow["HasErrors"] = true;
                }
                else
                {
                    fundTrans.TradeDate = date;
                }


                decimal utemp;
                if (!string.IsNullOrEmpty(unitPrice))
                    if (!decimal.TryParse(unitPrice, NumberStyles.Any, CultureInfo.CurrentCulture, out utemp))
                    {
                        dataRow["Message"] += "Invalid Unit Price,";
                        dataRow["HasErrors"] = true;
                    }
                    else
                    {
                        dataRow["UnitPrice"] = utemp;
                        fundTrans.UnitPrice = utemp;
                    }


                if (!string.IsNullOrEmpty(shares))
                    if (!decimal.TryParse(shares, NumberStyles.Any, CultureInfo.CurrentCulture, out utemp))
                    {
                        dataRow["Message"] += "Invalid Shares,";
                        dataRow["HasErrors"] = true;
                    }
                    else
                    {
                        fundTrans.Shares = utemp;
                    }

                if (!Convert.ToBoolean(dataRow["HasErrors"].ToString()))
                {
                    if (isFundCodeAttached && misfund.FundTransactions != null)
                    {
                        var oldTransactions = misfund.FundTransactions.Where(ee => ee.Shares.ToString("N4") == fundTrans.Shares.ToString("N4") && ee.ClientID == fundTrans.ClientID && ee.TradeDate == fundTrans.TradeDate);
                        if (oldTransactions != null && oldTransactions.Count() > 0)
                        {
                            fundTrans.ID = oldTransactions.FirstOrDefault().ID;
                            for (int i = oldTransactions.Count() - 1; i >= 0; i--)
                            {
                                misfund.FundTransactions.Remove(oldTransactions.ElementAt(i));
                            }
                            dataRow["Message"] += "Transaction updated,";
                        }
                    }
                }


                if (!string.IsNullOrEmpty(dataRow["Message"].ToString()) && dataRow["Message"].ToString().Contains(','))
                {
                    dataRow["Message"] = dataRow["Message"].ToString().Remove(dataRow["Message"].ToString().LastIndexOf(','));
                }

            }
            catch
            {
                // message = null;
            }

            if (Convert.ToBoolean(dataRow["HasErrors"].ToString()))
            {
                fundTrans = null;
            }

            return fundTrans;
        }

        private MISFundTransactionEntity CreateMessageSMA(DataRow dataRow, bool isFundCodeAttached, FundAccountEntity misfund,string assetCode, string clientID)
        {
            MISFundTransactionEntity fundTrans = null;

            DateTimeFormatInfo info = new DateTimeFormatInfo();
            info.ShortDatePattern = "dd/MM/yyyy";
                   
            string tradeDate = dataRow["TransactionDate"].ToString();
            string shares = dataRow["TransactionUnits"].ToString();
            DateTime date;
            if (!DateTime.TryParse(tradeDate, info, DateTimeStyles.None, out date))
            {

                dataRow["Message"] += "Invalid Price Date,";
                dataRow["HasErrors"] = true;
            }
            decimal sharesHolding = 0;
            if (!string.IsNullOrEmpty(shares) && !decimal.TryParse(shares, NumberStyles.Any, CultureInfo.CurrentCulture, out sharesHolding))
            {
                dataRow["Message"] += "Invalid Shares,";
                dataRow["HasErrors"] = true;
            }
            string unitPriceStr = dataRow["TransactionPrice"].ToString();
            decimal unitPrice = 0;
            if (!string.IsNullOrEmpty(unitPriceStr) && !decimal.TryParse(unitPriceStr, NumberStyles.Any, CultureInfo.CurrentCulture, out unitPrice))
            {
                dataRow["Message"] += "Invalid Unit Price,";
                dataRow["HasErrors"] = true;
            }

            if (isFundCodeAttached && misfund.FundTransactions != null)
            {
                var oldTransactions = misfund.FundTransactions.Where(ee => ee.Shares.ToString("N4") == sharesHolding.ToString("N4") && ee.ClientID == clientID && ee.TradeDate == date);
                if (oldTransactions == null || oldTransactions.Count() == 0)
                {
                    fundTrans = new MISFundTransactionEntity();
                    string fundCode = assetCode;
                    string account = clientID;
                    string transactionType = dataRow["Description"].ToString();
                    decimal amount = 0;
                    bool success = decimal.TryParse(dataRow["TransactionAmount"].ToString(), out amount);

                    if (transactionType.Contains("adjustment") || transactionType == "Dividend/Distribution Income")
                    {
                        if (amount > 0)
                            transactionType = "Adjustment Up";
                        else
                            transactionType = "Adjustment Down";
                    }
                    else
                    {
                        if (amount > 0)
                            transactionType = "Purchase";
                        else
                            transactionType = "Redemption";
                    }


                    string investorCategory = string.Empty;

                    try
                    {
                        fundTrans.ID = Guid.NewGuid();
                        fundTrans.InvestorCategory = investorCategory;
                        fundTrans.TransactionType = transactionType;
                        fundTrans.ClientID = account;
                        fundTrans.Status = "Upload By Super";

                        if (!isFundCodeAttached)
                        {
                            dataRow["Message"] += "Fund Code (" + fundCode + ") does not exist. Or Fund Code is not attached with any client,";
                            dataRow["HasErrors"] = true;
                            dataRow["IsMissingItem"] = true;
                        }

                        fundTrans.UnitPrice = Math.Round(unitPrice, 6);
                        fundTrans.TradeDate = date;
                        fundTrans.Shares = Math.Round(sharesHolding, 4);

                        if (!string.IsNullOrEmpty(dataRow["Message"].ToString()) && dataRow["Message"].ToString().Contains(','))
                            dataRow["Message"] = dataRow["Message"].ToString().Remove(dataRow["Message"].ToString().LastIndexOf(','));
                    }
                    catch
                    {
                        // message = null;
                    }

                    if (Convert.ToBoolean(dataRow["HasErrors"].ToString()))
                        fundTrans = null;
                }
                else
                {
                    dataRow["Message"] += "Transaction updated,";
                }
            }

      
            return fundTrans;
        }

        private MISFundTransactionEntity CreateMessage(XElement asxPrice, List<ImportMessages> messages, bool isFundCodeAttached, FundAccountEntity misfund)
        {
            MISFundTransactionMessage message = new MISFundTransactionMessage();

            MISFundTransactionEntity fundTrans = new MISFundTransactionEntity();




            DateTimeFormatInfo info = new DateTimeFormatInfo();
            info.ShortDatePattern = "dd/MM/yyyy";

            string TradeDate = asxPrice.XPathSelectElement("TradeDate").Value;
            string FundCode = asxPrice.XPathSelectElement("FundCode").Value;
            string FundName = asxPrice.XPathSelectElement("FundName").Value;
            string Account = asxPrice.XPathSelectElement("Account").Value;
            string SubAccount = asxPrice.XPathSelectElement("SubAccount").Value;

            string TransactionType = asxPrice.XPathSelectElement("TransactionType").Value;
            string Shares = asxPrice.XPathSelectElement("Shares").Value;
            string UnitPrice = asxPrice.XPathSelectElement("UnitPrice").Value;
            string Amount = asxPrice.XPathSelectElement("Amount").Value;
            string InvestorCategory = asxPrice.XPathSelectElement("InvestorCategory").Value;






            DateTime date;
            decimal utemp;
            try
            {

                message.Status = ImportMessageStatus.Sucess;
                //trans.TranstactionUniqueID = Guid.NewGuid();
                message.MessageType = ImportMessageType.Sucess;

                message.TradeDate = TradeDate;
                message.FundCode = FundCode;
                message.FundName = FundName;
                message.Account = Account;
                message.SubAccount = SubAccount;
                message.TransactionType = TransactionType;
                message.Shares = Shares;
                message.UnitPrice = UnitPrice;
                message.Amount = Amount;
                message.InvestorCategory = InvestorCategory;



                fundTrans.ID = Guid.NewGuid();
                fundTrans.InvestorCategory = InvestorCategory;
                fundTrans.TransactionType = TransactionType;
                fundTrans.ClientID = Account;
                fundTrans.Status = "Uploaded";

                if (!isFundCodeAttached)
                {
                    message.Message += "Fund code (" + FundCode + ") doesn't exists Or Fund code is not attached with any client,";

                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                }

                if (!DateTime.TryParse(TradeDate, info, DateTimeStyles.None, out date))
                {

                    message.Message += "Invalid Price Date,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                }
                else
                {
                    message.TradeDate = date.ToString("dd/MM/yyyy");
                    fundTrans.TradeDate = date;
                }





                if (!string.IsNullOrEmpty(UnitPrice))
                    if (!decimal.TryParse(UnitPrice, NumberStyles.Any, CultureInfo.CurrentCulture, out utemp))
                    {
                        message.Message += "Invalid Unit Price,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                    }
                    else
                    {

                        // string format = "$#,##0.00;-$#,##0.00;$0";

                        message.UnitPrice = utemp.ToString();
                        fundTrans.UnitPrice = utemp;

                    }




                if (!string.IsNullOrEmpty(Shares))
                    if (!decimal.TryParse(Shares, NumberStyles.Any, CultureInfo.CurrentCulture, out utemp))
                    {
                        message.Message += "Invalid Shares,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                    }
                    else
                    {

                        // string format = "$#,##0.00;-$#,##0.00;$0";

                        message.Shares = utemp.ToString();
                        fundTrans.Shares = utemp;
                    }

                if (message.MessageType != ImportMessageType.Error)
                {

                    if (isFundCodeAttached && misfund.FundTransactions != null)
                    {
                        var oldTransactions = misfund.FundTransactions.Where(ee => ee.Amount.ToString("C") == fundTrans.Amount.ToString("C") && ee.ClientID == fundTrans.ClientID && ee.TradeDate == fundTrans.TradeDate);
                        if (oldTransactions != null && oldTransactions.Count() > 0)
                        {
                            fundTrans.ID = oldTransactions.FirstOrDefault().ID;
                            for (int i = oldTransactions.Count() - 1; i >= 0; i--)
                            {
                               
                                misfund.FundTransactions.Remove(oldTransactions.ElementAt(i));
                            }
                            message.Message += "Transaction updated,";
                            //message.Status = ImportMessageStatus.AlreayExists;
                            //message.MessageType = ImportMessageType.Error;
                        }
                    }
                }


                if (!string.IsNullOrEmpty(message.Message) && message.Message.Contains(','))
                {
                    message.Message = message.Message.Remove(message.Message.LastIndexOf(','));
                }
                else
                {
                    message.Message = "";
                }

            }
            catch
            {

                message = null;
            }
            messages.Add(message);

            if (message.MessageType == ImportMessageType.Error)
            {
                fundTrans = null;
            }

            return fundTrans;
        }

        public override List<ImportMessages> ImportTransactionsUnitHolderBalance(System.Xml.Linq.XElement root)
        {
            List<ImportMessages> messages = new List<ImportMessages>();
            if (this.ManagedInvestmentSchemesAccountEntity.FundAccounts != null)
            {
                string FundCode_ = root.XPathSelectElement("FundCode").Value;
                string TotalShares_ = root.XPathSelectElement("TotalShares").Value;
                string RedemptionPrice_ = root.XPathSelectElement("RedemptionPrice").Value;
                string Account_ = root.XPathSelectElement("Account").Value;
                var misfund = this.ManagedInvestmentSchemesAccountEntity.FundAccounts.Where(fund => fund.Code == FundCode_).FirstOrDefault();

                // code changes from here

                // code changes till here

                MISFundTransactionEntity fundTransaction = CreateMessageUnitHolderBalance(root, messages, (misfund != null));
                if (misfund != null)
                {
                    if (misfund.SecurityHoldings == null)
                    {
                        misfund.SecurityHoldings = new ObservableCollection<MISUnitHolderFile>();
                    }
                    var secHolding = misfund.SecurityHoldings.Where(er => er.Account == Account_ && er.FundCode == FundCode_).FirstOrDefault();
                    if (secHolding == null)
                    {
                        secHolding = new MISUnitHolderFile { Account = Account_/*, AsofDate = Convert.ToDateTime(root.XPathSelectElement("AsofDate").Value)*/, CurrentValue = Math.Round(Convert.ToDouble(TotalShares_),4) * Math.Round(Convert.ToDouble(RedemptionPrice_),6), FundCode = FundCode_, FundName = root.XPathSelectElement("FundName").Value, InvestorCategory = root.XPathSelectElement("InvestorCategory").Value, RedemptionPrice = Math.Round(Convert.ToDouble(RedemptionPrice_),6), SubAccount = root.XPathSelectElement("SubAccount").Value, TotalShares = Math.Round(Convert.ToDouble(root.XPathSelectElement("TotalShares").Value),4) };
                    }
                    else
                    {
                        misfund.SecurityHoldings.Remove(secHolding);
                        secHolding.Account = root.XPathSelectElement("Account").Value;
                        secHolding.FundCode = root.XPathSelectElement("FundCode").Value;

                       
                        secHolding.InvestorCategory = root.XPathSelectElement("InvestorCategory").Value;
                        secHolding.RedemptionPrice = Math.Round(Convert.ToDouble(RedemptionPrice_),6);
                        secHolding.SubAccount = root.XPathSelectElement("SubAccount").Value;
                        secHolding.TotalShares = Math.Round(Convert.ToDouble(root.XPathSelectElement("TotalShares").Value),4);
                        secHolding.CurrentValue = secHolding.TotalShares * secHolding.RedemptionPrice;
                    }
                    DateTime asofDate;
                    if (DateTime.TryParse(root.XPathSelectElement("AsofDate").Value, new CultureInfo("en-AU"), DateTimeStyles.None, out asofDate))
                        secHolding.AsofDate = asofDate;

                    misfund.SecurityHoldings.Add(secHolding);
                }
            }
            return messages;
        }

        private void ImportTransactionsUnitHolderBalance(ImportProcessDS ds)
        {
            if (this.ManagedInvestmentSchemesAccountEntity.FundAccounts != null)
            {

                foreach (DataRow dr in ds.Tables[0].Select("HasErrors='false'"))
                {
                    var misfund = this.ManagedInvestmentSchemesAccountEntity.FundAccounts.Where(fund => fund.Code == dr["FundCode"].ToString().Trim()).FirstOrDefault();

                    if (misfund != null)
                    {
                        if (misfund.SecurityHoldings == null)
                        {
                            misfund.SecurityHoldings = new ObservableCollection<MISUnitHolderFile>();
                        }
                        var secHolding = misfund.SecurityHoldings.Where(er => er.Account == dr["Account"].ToString().Trim() && er.FundCode == dr["FundCode"].ToString().Trim()).FirstOrDefault();
                        if (secHolding == null)
                        {
                            secHolding = new MISUnitHolderFile { Account = dr["Account"].ToString().Trim(), CurrentValue = Math.Round(Convert.ToDouble(dr["TotalShares"].ToString().Trim()), 4) * Math.Round(Convert.ToDouble(dr["RedemptionPrice"].ToString().Trim()), 6), FundCode = dr["FundCode"].ToString().Trim(), FundName = dr["FundName"].ToString().Trim(), InvestorCategory = dr["InvestorCategory"].ToString().Trim(), RedemptionPrice = Math.Round(Convert.ToDouble(dr["RedemptionPrice"].ToString().Trim()), 6), SubAccount = dr["SubAccount"].ToString().Trim(), TotalShares = Math.Round(Convert.ToDouble(dr["TotalShares"].ToString().Trim()), 4) };

                            DateTime asofDate;
                            if (DateTime.TryParse(dr["AsOfDate"].ToString().Trim(), new CultureInfo("en-AU"), DateTimeStyles.None, out asofDate))
                                secHolding.AsofDate = asofDate;

                            misfund.SecurityHoldings.Add(secHolding);
                        }
                        else
                        {
                            DateTime asofDate;
                            if (DateTime.TryParse(dr["AsOfDate"].ToString().Trim(), new CultureInfo("en-AU"), DateTimeStyles.None, out asofDate))
                                secHolding.AsofDate = asofDate;

                            secHolding.Account = dr["Account"].ToString().Trim();
                            secHolding.FundCode = dr["FundCode"].ToString().Trim();
                             secHolding.InvestorCategory = dr["InvestorCategory"].ToString().Trim();
                            secHolding.RedemptionPrice =Math.Round(Convert.ToDouble(dr["RedemptionPrice"].ToString().Trim()),6);
                            secHolding.SubAccount = dr["SubAccount"].ToString().Trim();
                            secHolding.TotalShares = Math.Round(Convert.ToDouble(dr["TotalShares"].ToString().Trim()),4);
                            secHolding.CurrentValue = secHolding.TotalShares * secHolding.RedemptionPrice;
                        }

                    }
                }
            }
        }

        public override void DeleteAllSecurityHolding_ManagedInvestmentSchemes()
        {
            foreach (var count in _ManagedInvestmentSchemesAccount.FundAccounts)
            {
                count.SecurityHoldings = new ObservableCollection<MISUnitHolderFile>();
            }
        }

        public override void DeleteAllFundTransaction_ManagedInvestmentSchemes()
        {
            foreach (var count in _ManagedInvestmentSchemesAccount.FundAccounts)
            {
                count.FundTransactions = new ObservableCollection<MISFundTransactionEntity>();
            }
        }

        private MISFundTransactionEntity CreateMessageUnitHolderBalance(XElement docElement, List<ImportMessages> messages, bool IsFundCodeAttached)
        {
            UnitHolderBalanceTransactionMessage message = new UnitHolderBalanceTransactionMessage();
            MISFundTransactionEntity fundTrans = new MISFundTransactionEntity();
            DateTimeFormatInfo info = new DateTimeFormatInfo();
            info.ShortDatePattern = "dd/MM/yyyy";

            string Account = docElement.XPathSelectElement("Account").Value;
            string FundCode = docElement.XPathSelectElement("FundCode").Value;
            string FundName = docElement.XPathSelectElement("FundName").Value;
            string AsofDate = docElement.XPathSelectElement("AsofDate").Value;
            string SubAccount = docElement.XPathSelectElement("SubAccount").Value;
            string TotalShares = docElement.XPathSelectElement("TotalShares").Value;
            string RedemptionPrice = docElement.XPathSelectElement("RedemptionPrice").Value;
            string CurrentValue = docElement.XPathSelectElement("CurrentValue").Value;
            string InvestorCategory = docElement.XPathSelectElement("InvestorCategory").Value;

            DateTime date;
            decimal utemp;
            try
            {
                message.Status = ImportMessageStatus.Sucess;
                //trans.TranstactionUniqueID = Guid.NewGuid();
                message.MessageType = ImportMessageType.Sucess;
                DateTime asofDate;
                if (DateTime.TryParse(AsofDate, new CultureInfo("en-AU"), DateTimeStyles.None, out asofDate))
                    message.AsofDate = asofDate;
                //message.AsofDate = Convert.ToDateTime(AsofDate);
                message.FundCode = FundCode;
                message.FundName = FundName;
                message.Account = Account;
                message.SubAccount = SubAccount;
                message.TotalShares = TotalShares;
                message.RedemptionPrice = RedemptionPrice;
                message.CurrentValue = CurrentValue;
                message.InvestorCategory = InvestorCategory;
                fundTrans.ID = Guid.NewGuid();
                fundTrans.InvestorCategory = InvestorCategory;
                //fundTrans.TransactionType = TransactionType;
                fundTrans.ClientID = Account;
                fundTrans.Status = "Uploaded";

                if (!IsFundCodeAttached)
                {
                    message.Message += "Fund code (" + FundCode + ") doesn't exists Or Fund code is not attached with any client,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                }

                if (!DateTime.TryParse(AsofDate, info, DateTimeStyles.None, out date))
                {
                    message.Message += "Invalid As of Date,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                }
                else
                {
                    message.AsofDate = date;
                    fundTrans.TradeDate = date;
                }

                if (!string.IsNullOrEmpty(RedemptionPrice))
                    if (!decimal.TryParse(RedemptionPrice, NumberStyles.Any, CultureInfo.CurrentCulture, out utemp))
                    {
                        message.Message += "Invalid Redemption Price,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                    }
                    else
                    {
                        message.RedemptionPrice = utemp.ToString();
                        fundTrans.UnitPrice = utemp;
                    }

                if (!string.IsNullOrEmpty(TotalShares))
                    if (!decimal.TryParse(TotalShares, NumberStyles.Any, CultureInfo.CurrentCulture, out utemp))
                    {
                        message.Message += "Invalid Total Shares,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                    }
                    else
                    {
                        message.TotalShares = utemp.ToString();
                        fundTrans.Shares = utemp;
                    }
                if (!string.IsNullOrEmpty(message.Message) && message.Message.Contains(','))
                {
                    message.Message = message.Message.Remove(message.Message.LastIndexOf(','));
                }
                else
                {
                    message.Message = "";
                }
            }
            catch
            {
                message = null;
            }
            messages.Add(message);

            if (message.MessageType == ImportMessageType.Error)
            {
                fundTrans = null;
            }

            return fundTrans;
        }

        public void SetResetFunds(List<string> fundCodes, string misName)
        {

            if (ManagedInvestmentSchemesAccountEntity == null || ManagedInvestmentSchemesAccountEntity.Name != misName)
                return;
            var funds = ManagedInvestmentSchemesAccountEntity.FundAccounts.Where(f => !fundCodes.Exists(c => c == f.Code)).ToList();

            funds.ForEach(f => f.TotalShares = 0);

        }


        public bool HasFund(string FundCode, Guid FundID)
        {
            return this.ManagedInvestmentSchemesAccountEntity.FundAccounts.Count(exd => exd.Code == FundCode && exd.ID == FundID) > 0;
        }
    }
}
