using System;

namespace Oritax.TaxSimp.CM.Entity
{
    public class BankAccountInstall
	{
		#region INSTALLATION PROPERTIES

		// Assembly Installation Properties
        public const string ASSEMBLY_ID = "D851B5FF-AE5F-4BF6-A4D6-AB48743D1752";
        public const string ASSEMBLY_NAME = "BankAccount";
        public const string ASSEMBLY_DISPLAYNAME = "Bank Account";
		public const string ASSEMBLY_MAJORVERSION="1";
		public const string ASSEMBLY_MINORVERSION="0";
		public const string ASSEMBLY_DATAFORMAT="0";
		public const string ASSEMBLY_REVISION="0";		//2005.1

		// Component Installation Properties
        public const string COMPONENT_ID = "A6DBD5EC-623B-4E6C-A616-F20533B450D0";
        public const string COMPONENT_NAME = "BankAccount";
        public const string COMPONENT_DISPLAYNAME = "Bank Account";
		public const string COMPONENT_CATEGORY="BusinessEntity";

		// Component Version Installation Properties
		public const string COMPONENTVERSION_STARTDATE="01/01/1990 00:00:00 AM";
		public const string COMPONENTVERSION_ENDDATE="31/12/2050 12:59:59 AM";
		public const string COMPONENTVERSION_PERSISTERASSEMBLY="CommonPersisters_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";
		public const string COMPONENTVERSION_PERSISTERCLASS="Oritax.TaxSimp.CommonPersistence.BlobPersister";
		public const string COMPONENTVERSION_IMPLEMENTATIONCLASS="Oritax.TaxSimp.CM.Entity.BankAccountCM";

        #endregion

	}
}
