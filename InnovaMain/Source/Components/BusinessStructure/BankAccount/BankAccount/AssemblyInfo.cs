﻿using System.Reflection;

using Oritax.TaxSimp.InstallInfo;
using Oritax.TaxSimp.CM.Entity;

[assembly: AssemblyTitle("")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("")]
[assembly: AssemblyCopyright("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: AssemblyVersion(BankAccountInstall.ASSEMBLY_MAJORVERSION + "." + BankAccountInstall.ASSEMBLY_MINORVERSION)]
[assembly: AssemblyInformationalVersion(BankAccountInstall.ASSEMBLY_MAJORVERSION + "." + BankAccountInstall.ASSEMBLY_MINORVERSION + "." + BankAccountInstall.ASSEMBLY_DATAFORMAT + "." + BankAccountInstall.ASSEMBLY_REVISION)]
[assembly: AssemblyInstallInfo(BankAccountInstall.ASSEMBLY_ID, BankAccountInstall.ASSEMBLY_NAME, BankAccountInstall.ASSEMBLY_DISPLAYNAME)]
[assembly: ComponentInstallInfo(BankAccountInstall.COMPONENT_ID, BankAccountInstall.COMPONENT_NAME, BankAccountInstall.COMPONENT_DISPLAYNAME, BankAccountInstall.COMPONENT_CATEGORY)]
[assembly: ComponentVersionInstallInfo(BankAccountInstall.COMPONENTVERSION_STARTDATE, BankAccountInstall.COMPONENTVERSION_ENDDATE, BankAccountInstall.COMPONENTVERSION_PERSISTERASSEMBLY, BankAccountInstall.COMPONENTVERSION_PERSISTERCLASS, BankAccountInstall.COMPONENTVERSION_IMPLEMENTATIONCLASS)]

[assembly: AssemblyDelaySign(false)]
