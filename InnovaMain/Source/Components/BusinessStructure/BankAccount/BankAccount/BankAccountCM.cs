using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Globalization;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Enum;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.CalculationInterface;
using System.Linq;
using Oritax.TaxSimp.DataSets;
using BankAccountEntity = Oritax.TaxSimp.Common.BankAccountEntity;
using CashManagementEntity = Oritax.TaxSimp.Common.CashManagementEntity;
using IdentityCMDetail = Oritax.TaxSimp.Common.IdentityCMDetail;
using TermDepositAccountEntity = Oritax.TaxSimp.Common.TermDepositAccountEntity;

namespace Oritax.TaxSimp.CM.Entity
{
    [Serializable]
    public class BankAccountCM : EntityCM, ISerializable, IBankAccount, IOrganizationChartData, IHaveAssociation, ISupportMigration
    {
        #region Private Variable
        private Common.BankAccountEntity _bankAccount;

        #endregion

        #region Public Properties
        public Common.BankAccountEntity BankAccountEntity
        {
            get { return _bankAccount; }
        }
        #endregion

        #region Constructor
        public BankAccountCM()
        { }

        protected BankAccountCM(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            _bankAccount = info.GetValue<Common.BankAccountEntity>("Oritax.TaxSimp.CM.Entity.BankAccount");
        }
        #endregion

        #region Override Method
        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValueX("Oritax.TaxSimp.CM.Entity.BankAccount", _bankAccount);
        }

        public override string OnUpdateDataStream(int type, string data)
        {
            string xml = string.Empty;
            switch ((CmCommand)type)
            {
                case CmCommand.AccountRelationAdd:
                    xml = AddAccountRelation(data);
                    break;
                case CmCommand.AccountRelationAddReference:
                    xml = AddAccountRelationReference(data);
                    break;
                case CmCommand.AccountRelationDelete:
                    DeleteAccountRelation(data);
                    break;
                case CmCommand.AccountRelationDeleteRefrence:
                    DeleteAccountRelationReference(data);
                    break;
                default:
                    Common.BankAccountEntity entity = data.ToNewOrData<Common.BankAccountEntity>();

                    if (entity.BSB == "XXX-XXX")
                    {
                        entity.Holding = 0;
                        foreach (Common.CashManagementEntity cashManagementEntity in entity.CashManagementTransactions)
                        {
                            if (cashManagementEntity.SystemTransactionType == "Redemption")
                                entity.Holding += cashManagementEntity.Amount;
                            else
                                entity.Holding += cashManagementEntity.TotalAmount;
                        }
                    }

                    Name = entity.Name;
                    OrganizationStatus = entity.OrganizationStatus;
                    _bankAccount = entity;
                    if (string.IsNullOrEmpty(ClientId))
                    {
                        UpDateEventLog.UpdateLog("Bank Account", entity.Name, type, Broker);
                    }
                    else
                    {
                        UpDateEventLog.UpdateLog("Bank Account", entity.Name + " (" + ClientId + ")", type, Broker);
                    }

                    return data;
            }
            return xml;
        }

        public override string GetDataStream(int type, string data)
        {
            string xml;
            switch ((CmCommand)type)
            {
                case CmCommand.AccountRelationGetList:
                    xml = _bankAccount.LinkedAccounts.ToXmlString();
                    break;
                case CmCommand.AccountRelationGetEntity:
                    var entity = new AccountRelation();
                    entity.AccountName = _bankAccount.Name + "" + _bankAccount.AccountNumber;
                    entity.AccountType = TypeName;
                    entity.Clid = Clid;
                    entity.Csid = Csid;

                    xml = entity.ToXmlString();
                    break;
                case CmCommand.XMLData:
                    xml = ExtractXML(data);
                    break;
                case CmCommand.CSVData:
                    xml = GetCSVObject();
                    break;
                case CmCommand.BSB:
                    xml = _bankAccount.BSB ?? string.Empty;
                    break;
                case CmCommand.AccountNumber:
                    xml = _bankAccount.AccountNumber ?? string.Empty;
                    break;
                case CmCommand.GetType:
                    xml = string.Empty;
                    var inst = ((IOrganization)(Broker.GetWellKnownCM(WellKnownCM.Organization))).Institution.FirstOrDefault(ss => ss.ID == _bankAccount.InstitutionID);
                    if (inst != null)
                        xml = inst.Name;
                    break;
                case CmCommand.GetAccountType:
                    xml = _bankAccount.AccoutType;
                    break;
                default:
                    _bankAccount.OrganizationStatus = OrganizationStatus;
                    xml = _bankAccount.ToXmlString();
                    break;
            }
            return xml;
        }

        public override string GetAllDataStream(int type)
        {
            return _bankAccount.ToXmlString();
        }
        #endregion

        #region GetXMLData

        public override string GetXmlData()
        {
            string xmlco = _bankAccount.ToXmlString();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmlco);
            XmlAttribute rootAtt = doc.CreateAttribute("Type");
            rootAtt.Value = TypeName;
            doc.DocumentElement.Attributes.Append(rootAtt);
            return doc.OuterXml;
        }

        private string ExtractXML(string data)
        {
            string xml;
            XDocument document = XDocument.Load(XmlReader.Create(new StringReader(_bankAccount.ToXmlString())));
            XmlDocument xmlDocument = new XmlDocument();

            if (this.BankAccountEntity.LinkedAccounts.Count > 0)
            {
                using (var xmlReader = document.CreateReader())
                    xmlDocument.Load(xmlReader);

                XMLExtensions.SetNode(BankAccountEntity.LinkedAccounts, "LinkedAccounts", "LinkedAccount", xmlDocument, Broker);

                using (var nodeReader = new XmlNodeReader(xmlDocument)) { nodeReader.MoveToContent(); document = XDocument.Load(nodeReader); }
            }
            document.Root.Name = data;
            document.Root.RemoveAttributes();
            xml = document.ToString().Trim();
            return xml;
        }

        #endregion

        #region GetCSVData
        private string GetCSVObject()
        {
            return _bankAccount.ToXmlString();
        }
        #endregion

        public override IClientEntity ClientEntity
        {
            get
            {
                return _bankAccount;
            }
        }

        public void Export(XElement root)
        {
            _bankAccount.OrganizationStatus = OrganizationStatus;
            root.Add(_bankAccount.ToXElement("BankAccount"));
        }

        public void Import(XElement root, IdentityCmMap map)
        {
            _bankAccount = root.FromXElement<Common.BankAccountEntity>("BankAccount");
            OrganizationStatus = _bankAccount.OrganizationStatus;
            Name = _bankAccount.Name;
        }

        protected override void OnGetData(DataSet data)
        {
            base.OnGetData(data);
            _bankAccount.OrganizationStatus = OrganizationStatus;
            if (data is BankDetailsDS)
            {
                DataRow bankDetailsRow = data.Tables[BankDetailsDS.BANKDETAILSTABLE].NewRow();
                bankDetailsRow[BankDetailsDS.ID] = this.CID;
                bankDetailsRow[BankDetailsDS.BSB] = this.BankAccountEntity.BSB;
                bankDetailsRow[BankDetailsDS.ACCOUNTNUMBER] = this.BankAccountEntity.AccountNumber;
                bankDetailsRow[BankDetailsDS.ACCNAME] = this.BankAccountEntity.Name;
                bankDetailsRow[BankDetailsDS.ACCOUTTYPE] = this.BankAccountEntity.AccoutType.ToUpper();
                bankDetailsRow[BankDetailsDS.CUSTNO] = this.BankAccountEntity.CustomerNo;

                decimal transactionTotal = 0;
                if (this.BankAccountEntity.CashManagementTransactions != null)
                    transactionTotal = this.BankAccountEntity.CashManagementTransactions.Where(c => !c.Comment.StartsWith("Total Movement in Perod for")).Sum(tran => tran.TotalAmount);

                decimal holdingTotal = this.BankAccountEntity.Holding;
                decimal diff = holdingTotal - transactionTotal;

                bankDetailsRow[BankDetailsDS.TRANSACTION] = transactionTotal;
                bankDetailsRow[BankDetailsDS.HOLDING] = holdingTotal;
                bankDetailsRow[BankDetailsDS.DIFF] = diff;
                bankDetailsRow[BankDetailsDS.CLIENTCID] = BankAccountEntity.ParentCID;

                bankDetailsRow[BankDetailsDS.INSID] = this.BankAccountEntity.InstitutionID;
                bankDetailsRow[BankDetailsDS.ISEXTERNALACCOUNT] = BankAccountEntity.IsExternalAccount;
                data.Tables[BankDetailsDS.BANKDETAILSTABLE].Rows.Add(bankDetailsRow);
            }

            else if (data is BankAccountDS)
            {
                BankAccountDS dataSet = data as BankAccountDS;
                DataRow dr = data.Tables[dataSet.BankAccountsTable.TABLENAME].NewRow();
                dr[dataSet.BankAccountsTable.CID] = CID;
                dr[dataSet.BankAccountsTable.CLID] = CLID;
                dr[dataSet.BankAccountsTable.CSID] = CSID;
                dr[dataSet.BankAccountsTable.ACCOUNTNAME] = BankAccountEntity.Name;
                dr[dataSet.BankAccountsTable.ACCOUNTNO] = BankAccountEntity.AccountNumber;
                dr[dataSet.BankAccountsTable.BSBNO] = BankAccountEntity.BSB;
                dr[dataSet.BankAccountsTable.ACCOUNTTYPE] = BankAccountEntity.AccoutType.ToUpper();
                dr[dataSet.BankAccountsTable.INSTITUTIONID] = BankAccountEntity.InstitutionID;
                dr[dataSet.BankAccountsTable.STATUS] = BankAccountEntity.OrganizationStatus = OrganizationStatus;
                if (BankAccountEntity.BrokerID != null)
                {
                    dr[dataSet.BankAccountsTable.BROKERID] = BankAccountEntity.BrokerID.Clid;
                    dr[dataSet.BankAccountsTable.BROKERCSID] = BankAccountEntity.BrokerID.Csid;
                }
                else
                {
                    dr[dataSet.BankAccountsTable.BROKERID] = Guid.Empty;
                    dr[dataSet.BankAccountsTable.BROKERCSID] = Guid.Empty;
                }

                var inst = ((IOrganization)(Broker.GetWellKnownCM(WellKnownCM.Organization))).Institution.FirstOrDefault(ss => ss.ID == BankAccountEntity.InstitutionID);
                if (inst != null)
                    dr[dataSet.BankAccountsTable.INSTITUTION] = inst.Name;

                dr[dataSet.BankAccountsTable.CUSTOMERNO] = BankAccountEntity.CustomerNo;
                dr[dataSet.BankAccountsTable.TOTALAMOUNT] = BankAccountEntity.Total;
                dr[dataSet.BankAccountsTable.TRANSACTIONHOLDING] = BankAccountEntity.TransactionHolding;
                dr[dataSet.BankAccountsTable.ISEXTERNALACCOUNT] = BankAccountEntity.IsExternalAccount;

                dataSet.Tables[dataSet.BankAccountsTable.TABLENAME].Rows.Add(dr);

                DataRow drMap = data.Tables[BankAccountDS.INCOMETRANMAPTABLE].NewRow();
                drMap[UMABaseDS.BGLCODE] = _bankAccount.BGLCode;
                drMap[UMABaseDS.INVESTMENTTYPE] = "Cash at Bank";
                drMap[UMABaseDS.INVESTMENTCID] = this.CID;
                drMap[UMABaseDS.INVESTMENTCODE] = inst.Name;
                drMap[UMABaseDS.INVESTMENTACCNO] = this._bankAccount.AccountNumber;
                drMap[UMABaseDS.INVESTMENTNAME] = this.BankAccountEntity.Name;
                drMap[UMABaseDS.INVESTMENTPROVIDER] = inst.LegalName;
                drMap[UMABaseDS.BGLDEFAULTCODE] = "250";
                drMap[UMABaseDS.INVESTMENTCOATYPE] = "Interest Income";
                drMap[UMABaseDS.INVESTMENTACCOUNTFILTERTYPE] = this._bankAccount.AccountNumber + " - " + inst.LegalName + " (" + _bankAccount.AccoutType.ToUpper() + ")";
                data.Tables[UMABaseDS.INCOMETRANMAPTABLE].Rows.Add(drMap);

                drMap = data.Tables[UMABaseDS.INCOMETRANMAPTABLE].NewRow();
                drMap[UMABaseDS.BGLCODE] = _bankAccount.BGLCodeCommissionRebate;
                drMap[UMABaseDS.INVESTMENTTYPE] = "Cash at Bank";
                drMap[UMABaseDS.INVESTMENTCID] = this.CID;
                drMap[UMABaseDS.INVESTMENTCODE] = inst.Name;
                drMap[UMABaseDS.INVESTMENTACCNO] = this._bankAccount.AccountNumber;
                drMap[UMABaseDS.INVESTMENTNAME] = this.BankAccountEntity.Name;
                drMap[UMABaseDS.INVESTMENTPROVIDER] = inst.LegalName;
                drMap[UMABaseDS.BGLDEFAULTCODE] = "265";
                drMap[UMABaseDS.INVESTMENTCOATYPE] = "Commission Rebate";
                drMap[UMABaseDS.INVESTMENTACCOUNTFILTERTYPE] = this._bankAccount.AccountNumber + " - " + inst.LegalName + " (" + _bankAccount.AccoutType.ToUpper() + ")";
                data.Tables[UMABaseDS.INCOMETRANMAPTABLE].Rows.Add(drMap);

                drMap = data.Tables[UMABaseDS.INCOMETRANMAPTABLE].NewRow();
                drMap[UMABaseDS.BGLCODE] = _bankAccount.BGLCodeCashAccount;
                drMap[UMABaseDS.INVESTMENTTYPE] = "Cash at Bank";
                drMap[UMABaseDS.INVESTMENTCID] = this.CID;
                drMap[UMABaseDS.INVESTMENTCODE] = inst.Name;
                drMap[UMABaseDS.INVESTMENTACCNO] = this._bankAccount.AccountNumber;
                drMap[UMABaseDS.INVESTMENTNAME] = this.BankAccountEntity.Name;
                drMap[UMABaseDS.INVESTMENTPROVIDER] = inst.LegalName;
                drMap[UMABaseDS.BGLDEFAULTCODE] = "604";
                drMap[UMABaseDS.INVESTMENTCOATYPE] = "Bank Code";
                drMap[UMABaseDS.INVESTMENTACCOUNTFILTERTYPE] = this._bankAccount.AccountNumber + " - " + inst.LegalName + " (" + _bankAccount.AccoutType.ToUpper() + ")";
                data.Tables[UMABaseDS.INCOMETRANMAPTABLE].Rows.Add(drMap);

                drMap = data.Tables[UMABaseDS.INCOMETRANMAPTABLE].NewRow();
                drMap[UMABaseDS.BGLCODE] = _bankAccount.BGLCodeSuspense;
                drMap[UMABaseDS.INVESTMENTTYPE] = "Cash at Bank";
                drMap[UMABaseDS.INVESTMENTCID] = this.CID;
                drMap[UMABaseDS.INVESTMENTCODE] = inst.Name;
                drMap[UMABaseDS.INVESTMENTACCNO] = this._bankAccount.AccountNumber;
                drMap[UMABaseDS.INVESTMENTNAME] = this.BankAccountEntity.Name;
                drMap[UMABaseDS.INVESTMENTPROVIDER] = inst.LegalName;
                drMap[UMABaseDS.BGLDEFAULTCODE] = "999";
                drMap[UMABaseDS.INVESTMENTCOATYPE] = "Suspense Account";
                drMap[UMABaseDS.INVESTMENTACCOUNTFILTERTYPE] = this._bankAccount.AccountNumber + " - " + inst.LegalName + " (" + _bankAccount.AccoutType.ToUpper() + ")";
                data.Tables[UMABaseDS.INCOMETRANMAPTABLE].Rows.Add(drMap);
            }
        }

        #region Maquarie Bank Transactions Import Functions

        private void AddTransactionsMaquarieCMA(DataRow dr)
        {
            if (_bankAccount.CashManagementTransactions == null)
                _bankAccount.CashManagementTransactions = new System.Collections.ObjectModel.ObservableCollection<Common.CashManagementEntity>();

            Common.CashManagementEntity cashManagementEntity = CreateCashManagementEntityMaquarieCMA(dr);


            if (cashManagementEntity != null)
            {

                if (cashManagementEntity.Category == string.Empty)
                {
                    cashManagementEntity.AccountName = "MAQ" + BankAccountEntity.AccountNumber;
                    string comment = cashManagementEntity.Comment;

                    SetTransactioncCat(cashManagementEntity, cashManagementEntity.Comment);
                }
                //add UnsettledOrder Settlement code here

                if (BankAccountEntity.PaidOrders != null)
                {
                    var paidorder = BankAccountEntity.PaidOrders.FirstOrDefault(order => order.IsSettled == false && Math.Floor(order.Amount) == Math.Floor(cashManagementEntity.Amount));

                    if (paidorder != null)
                    {
                        BankAccountEntity.UnsettledOrder -= paidorder.Amount;
                        paidorder.IsSettled = true;
                        paidorder.SettelerTransactionID = cashManagementEntity.ID.ToString();

                    }
                }
                if (dr["ClientID"] != DBNull.Value)
                    SettleOrder(dr["ClientID"].ToString(), cashManagementEntity, (dr.Table.DataSet as IHasOrganizationUnit).Unit.CurrentUser);


                dr["Message"] += "Successful.";
                _bankAccount.CashManagementTransactions.Add(cashManagementEntity);
            }

        }

        private Common.CashManagementEntity CreateCashManagementEntityMaquarieCMA(DataRow dr)
        {
            Common.CashManagementEntity cashManagementEntity = new Common.CashManagementEntity();
            bool hasErrors = false;
            DateTime dateofTrans;
            DateTimeFormatInfo info = new DateTimeFormatInfo();
            info.ShortDatePattern = "dd/MM/yyyy";
            decimal amount;
            //don't add transaction if Date of Transaction and Amount are not in correct format
            try
            {
                cashManagementEntity.ID = Guid.NewGuid();
                cashManagementEntity.ExternalReferenceID = dr[MaquarieCMATransactionsDS.ACCOUNTNUMBER].ToString();
                cashManagementEntity.Comment = dr[MaquarieCMATransactionsDS.NARRATIVE].ToString();
                cashManagementEntity.Status = "Uploaded";

                if (decimal.TryParse(dr[MaquarieCMATransactionsDS.AMOUNT].ToString(), out amount))
                    cashManagementEntity.Amount = amount;
                else
                {
                    dr[MaquarieCMATransactionsDS.MESSAGE] += "Invalid amount,";
                    //dr["HasErrors"] = true;
                    hasErrors = true;
                }

                if (dr[MaquarieCMATransactionsDS.DEBITCREDIT].ToString() == "D")
                {
                    cashManagementEntity.ImportTransactionType = "Withdrawal";
                    cashManagementEntity.Amount = cashManagementEntity.Amount * -1;
                }
                else if (dr[MaquarieCMATransactionsDS.DEBITCREDIT].ToString() == "C")
                    cashManagementEntity.ImportTransactionType = "Deposit";

                if (dr[MaquarieCMATransactionsDS.TRANSACTIONTYPE] != DBNull.Value && !string.IsNullOrEmpty(dr[MaquarieCMATransactionsDS.TRANSACTIONTYPE].ToString()))
                {
                    var transactionDescDetails = MacquarieTransactionDescList.TranDescList.Where(tran => tran.Code == dr[MaquarieCMATransactionsDS.TRANSACTIONTYPE].ToString()).FirstOrDefault();

                    if (transactionDescDetails != null)
                    {
                        cashManagementEntity.SystemTransactionType = transactionDescDetails.SystemTransaction;
                        cashManagementEntity.ImportTransactionTypeDetail = transactionDescDetails.TransactionDescriptionDetail;
                    }
                    else
                        cashManagementEntity.SystemTransactionType = cashManagementEntity.ImportTransactionType;

                }
                else
                {
                    //imported from file
                    cashManagementEntity.SystemTransactionType = dr[MaquarieCMATransactionsDS.SYSTEMTRANSACTIONTYPE].ToString();
                    cashManagementEntity.ImportTransactionTypeDetail = dr[MaquarieCMATransactionsDS.IMPORTTRANSACTIONTYPEDETAIL].ToString();

                }

                cashManagementEntity.InvestmentCode = "CASH";
                cashManagementEntity.Currency = "AUD";
                cashManagementEntity.InvestmentName = "AUD CASH";
                cashManagementEntity.AdministrationSystem = string.Empty;
                //bsbnaccountnumber added
                cashManagementEntity.AccountName = "MAQ-" + this.BankAccountEntity.AccountNumber;

                if (DateTime.TryParse(dr[MaquarieCMATransactionsDS.TRANSACTIONDATE].ToString(), info, DateTimeStyles.None, out dateofTrans))
                    cashManagementEntity.TransactionDate = dateofTrans;
                else
                {
                    dr[MaquarieCMATransactionsDS.MESSAGE] += "Invalid transaction date,";
                    //dr["HasErrors"] = true;
                    hasErrors = true;
                }

                if (!hasErrors)
                {
                    var oldtrans = _bankAccount.CashManagementTransactions.Where(temp => temp.Amount == cashManagementEntity.Amount && temp.TransactionDate.Date == cashManagementEntity.TransactionDate.Date && temp.Comment.ToLower() == cashManagementEntity.Comment.ToLower());
                    if (oldtrans != null && oldtrans.Count() > 0)// transaction already exits
                    {
                        cashManagementEntity.CopyImportantPropertiesFromOldTransaction(oldtrans.FirstOrDefault());
                        for (int i = oldtrans.Count() - 1; i >= 0; i--)
                        {
                            _bankAccount.CashManagementTransactions.Remove(oldtrans.ElementAt(i));
                        }

                        dr[MaquarieCMATransactionsDS.MESSAGE] += "Transaction updated,";
                    }
                }
            }
            catch
            {
                cashManagementEntity = null;
            }

            if (hasErrors)
            {
                cashManagementEntity = null;
            }

            dr[MaquarieCMATransactionsDS.HASERRORS] = hasErrors;

            return cashManagementEntity;
        }

        #endregion

        protected override ModifiedState OnSetData(DataSet data)
        {
            base.OnSetData(data);

            if (data is SMAImportProcessDS)
            {
                SMAImportProcessDS smaImportProcessDS = data as SMAImportProcessDS;

                List<CashManagementEntity> listCashTransactions = this.BankAccountEntity.CashManagementTransactions.ToList();
                decimal totalTranHolding = listCashTransactions.Sum(c => c.TotalAmount);
                string movementComment = "Total Movement in Perod for " + smaImportProcessDS.ClientID;

                decimal holding = 0;

                if (smaImportProcessDS.Tables[SMAImportProcessDS.HOLDINGVIEWTABLE] != null)
                {
                    DataRow holdingRow = smaImportProcessDS.Tables[SMAImportProcessDS.HOLDINGVIEWTABLE].Select().Where(dr => dr["InvestmentClass"].ToString().ToLower() == "cash").FirstOrDefault();

                    if (holdingRow != null)
                    {
                        holding = decimal.Parse(holdingRow["DollarBalance"].ToString());
                    }
                }
                //if holding doesn't match then import all transaction again if they both match then we have latest Transaction.
                if (BankAccountEntity.Holding != holding || totalTranHolding != holding)
                {
                    BankAccountEntity.CashManagementTransactions.Clear();

                    if (smaImportProcessDS.Tables[SMAImportProcessDS.HOLDINGVIEWTABLE] != null)
                    {
                        DataRow holdingRow = smaImportProcessDS.Tables[SMAImportProcessDS.HOLDINGVIEWTABLE].Select().Where(dr => dr["InvestmentClass"].ToString().ToLower() == "cash").FirstOrDefault();

                        if (holdingRow != null)
                        {
                            holding = decimal.Parse(holdingRow["DollarBalance"].ToString());
                            decimal pendingAmount = decimal.Parse(holdingRow["PendingAmount"].ToString());
                            BankAccountEntity.UnsettledOrder = pendingAmount;
                            BankAccountEntity.Holding = holding;

                            if (smaImportProcessDS.Tables[SMAImportProcessDS.CASHTRANSACTIONTABLE] != null)
                            {
                                foreach (DataRow cashRow in smaImportProcessDS.Tables[SMAImportProcessDS.CASHTRANSACTIONTABLE].Rows)
                                {
                                    if (!cashRow["Description"].ToString().ToLower().Contains("movement in protection account"))
                                        AddTransactionsSMACASH(cashRow, smaImportProcessDS.MemberID, smaImportProcessDS.SuperID);
                                }
                            }

                            decimal transactionHolding = BankAccountEntity.CashManagementTransactions.Where(tran => tran.Comment != movementComment).Sum(tran => tran.TotalAmount);

                            var movementTranCurrent = BankAccountEntity.CashManagementTransactions.Where(tran => tran.Comment == movementComment).FirstOrDefault();

                            if (movementTranCurrent == null)
                            {
                                movementTranCurrent = new CashManagementEntity();
                                movementTranCurrent.ID = Guid.NewGuid();
                                movementTranCurrent.Comment = movementComment;
                                BankAccountEntity.CashManagementTransactions.Add(movementTranCurrent);
                            }

                            movementTranCurrent.TransactionDate = DateTime.Now;
                            movementTranCurrent.Amount = BankAccountEntity.Holding - transactionHolding;

                            if (movementTranCurrent.Amount > 0)
                            {
                                movementTranCurrent.ImportTransactionType = "Deposit";
                                movementTranCurrent.SystemTransactionType = "Internal Cash Movement";
                                movementTranCurrent.Category = "Income";
                            }
                            else
                            {
                                movementTranCurrent.ImportTransactionType = "Withdrawal";
                                movementTranCurrent.SystemTransactionType = "Internal Cash Movement";
                                movementTranCurrent.Category = "Expense";
                            }

                            // copy important properties from Old transactions
                            foreach (CashManagementEntity cashManagementEntity in listCashTransactions)
                            {
                                var newTransaction = _bankAccount.CashManagementTransactions.FirstOrDefault(temp => temp.Comment == cashManagementEntity.Comment && temp.TransactionDate.Date == cashManagementEntity.TransactionDate.Date && temp.TotalAmount == cashManagementEntity.TotalAmount);
                                if (newTransaction != null)// transaction already exits
                                {
                                    newTransaction.CopyImportantPropertiesFromOldTransaction(cashManagementEntity);

                                }
                                else
                                    BankAccountEntity.DeletedCashManagementTransactions.Add(cashManagementEntity);
                            }


                            //settled Client Orders
                            if (BankAccountEntity.CashManagementTransactions != null)
                                foreach (var trans in BankAccountEntity.CashManagementTransactions)
                                {
                                    SetSettledUnSettledTransaction(trans);
                                    SettleOrder(smaImportProcessDS.ClientUMAID, trans, smaImportProcessDS.Unit.CurrentUser);
                                }
                        }
                    }
                }
            }

            if (data is BankDetailsDS)
            {
                BankDetailsDS bankDetailsDS = (BankDetailsDS)data;

                if (bankDetailsDS.DataSetOperationType == DataSetOperationType.UpdateSingle)
                {
                    DataRow row = bankDetailsDS.Tables[BankDetailsDS.BANKDETAILSTABLE].Rows[0];
                    this.BankAccountEntity.AccountNumber = (String)row[BankDetailsDS.ACCOUNTNUMBER];
                    this.BankAccountEntity.CustomerNo = (String)row[BankDetailsDS.CUSTNO];
                    this.BankAccountEntity.BSB = (String)row[BankDetailsDS.BSB];
                    this.BankAccountEntity.Name = (String)row[BankDetailsDS.ACCNAME];
                    this.BankAccountEntity.InstitutionID = (Guid)row[BankDetailsDS.INSID];
                    this.BankAccountEntity.AccoutType = (row[BankDetailsDS.ACCOUTTYPE]).ToString().ToLower();
                }
            }

            else if (data is BankAccountDS)
            {
                BankAccountDS bankAccountDS = (BankAccountDS)data;

                DataRow row = bankAccountDS.BankAccountsTable.Rows[0];
                if (BankAccountEntity == null)
                {
                    _bankAccount = new BankAccountEntity();

                }
                BankAccountEntity.Name = row[bankAccountDS.BankAccountsTable.ACCOUNTNAME].ToString();
                BankAccountEntity.AccountNumber = row[bankAccountDS.BankAccountsTable.ACCOUNTNO].ToString();
                BankAccountEntity.BSB = row[bankAccountDS.BankAccountsTable.BSBNO].ToString();
                BankAccountEntity.AccoutType = row[bankAccountDS.BankAccountsTable.ACCOUNTTYPE].ToString().ToLower();
                BankAccountEntity.InstitutionID = Guid.Parse(row[bankAccountDS.BankAccountsTable.INSTITUTIONID].ToString());
                OrganizationStatus = BankAccountEntity.OrganizationStatus = row[bankAccountDS.BankAccountsTable.STATUS].ToString();

                if (BankAccountEntity.BrokerID == null)
                {
                    BankAccountEntity.BrokerID = new TermDepositAccountEntity();
                    BankAccountEntity.BrokerID.BankAccounts = new List<IdentityCMDetail>();

                }
                if (row[bankAccountDS.BankAccountsTable.ACCOUNTTYPE].ToString().ToLower() == "termdeposit")
                {
                    if (row[bankAccountDS.BankAccountsTable.BROKER] != DBNull.Value)
                        BankAccountEntity.BrokerID.Name = row[bankAccountDS.BankAccountsTable.BROKER].ToString();
                    if (row[bankAccountDS.BankAccountsTable.BROKERID] != DBNull.Value)
                        BankAccountEntity.BrokerID.Clid = Guid.Parse(row[bankAccountDS.BankAccountsTable.BROKERID].ToString());
                    if (row[bankAccountDS.BankAccountsTable.BROKERCSID] != DBNull.Value)
                        BankAccountEntity.BrokerID.Csid = Guid.Parse(row[bankAccountDS.BankAccountsTable.BROKERCSID].ToString());
                }

                //Setting Customer No
                BankAccountEntity.CustomerNo = row[bankAccountDS.BankAccountsTable.CUSTOMERNO].ToString();
                BankAccountEntity.IsExternalAccount = (bool)row[bankAccountDS.BankAccountsTable.ISEXTERNALACCOUNT];
            }
            else if (data is BankTransactionDetailsDS)
            {
                BankTransactionDetailsDS bankTransactionDetailsDS = (BankTransactionDetailsDS)data;
                if (bankTransactionDetailsDS.DataSetOperationType == DataSetOperationType.NewSingle)
                {
                    Oritax.TaxSimp.Common.CashManagementEntity cashManagementEntity = new Common.CashManagementEntity();
                    cashManagementEntity.ID = Guid.NewGuid();
                    cashManagementEntity.AccountName = BankAccountEntity.BSB.Split('-')[1] + BankAccountEntity.AccountNumber;
                    cashManagementEntity.AdministrationSystem = BankAccountEntity.BSB.Split('-')[1] + BankAccountEntity.AccountNumber;
                    cashManagementEntity.ImportTransactionType = data.Tables[BankTransactionDetailsDS.BANKTRANSDETAILSTABLE].Rows[0][BankTransactionDetailsDS.IMPORTTRANTYPE].ToString();
                    cashManagementEntity.Category = data.Tables[BankTransactionDetailsDS.BANKTRANSDETAILSTABLE].Rows[0][BankTransactionDetailsDS.CATTRANTYPE].ToString();
                    cashManagementEntity.SystemTransactionType = data.Tables[BankTransactionDetailsDS.BANKTRANSDETAILSTABLE].Rows[0][BankTransactionDetailsDS.SYSTRANTYPE].ToString();
                    cashManagementEntity.Amount = (decimal)data.Tables[BankTransactionDetailsDS.BANKTRANSDETAILSTABLE].Rows[0][BankTransactionDetailsDS.AMOUNT];
                    cashManagementEntity.Adjustment = (decimal)data.Tables[BankTransactionDetailsDS.BANKTRANSDETAILSTABLE].Rows[0][BankTransactionDetailsDS.ADJUSTMENT];
                    cashManagementEntity.Comment = data.Tables[BankTransactionDetailsDS.BANKTRANSDETAILSTABLE].Rows[0][BankTransactionDetailsDS.COMMENT].ToString();
                    cashManagementEntity.TransactionDate = (DateTime)data.Tables[BankTransactionDetailsDS.BANKTRANSDETAILSTABLE].Rows[0][BankTransactionDetailsDS.TRANDATE];

                    this.BankAccountEntity.CashManagementTransactions.Add(cashManagementEntity);
                    MatchBankTransactionWithDividendOrDistribution(cashManagementEntity, _bankAccount.ParentCID);
                }
                else if (bankTransactionDetailsDS.DataSetOperationType == DataSetOperationType.DeletSingle)
                {
                    Guid transactionID = Guid.Empty;

                    if (data.Tables[BankTransactionDetailsDS.BANKTRANSDETAILSTABLE].Rows.Count > 0)
                        transactionID = (Guid)data.Tables[BankTransactionDetailsDS.BANKTRANSDETAILSTABLE].Rows[0][BankTransactionDetailsDS.ID];
                    else
                        transactionID = new Guid(bankTransactionDetailsDS.BankTransactionID);


                    Oritax.TaxSimp.Common.CashManagementEntity cashManagementEntity = this.BankAccountEntity.CashManagementTransactions.Where(cashTran =>
                                                cashTran.ID == transactionID).FirstOrDefault();

                    if (cashManagementEntity != null)
                    {
                        if (this._bankAccount.DeletedCashManagementTransactions == null)
                        {
                            _bankAccount.DeletedCashManagementTransactions = new ObservableCollection<CashManagementEntity>();
                        }
                        _bankAccount.DeletedCashManagementTransactions.Add(cashManagementEntity);
                        this.BankAccountEntity.CashManagementTransactions.Remove(cashManagementEntity);
                    }

                }
                else if (bankTransactionDetailsDS.DataSetOperationType == DataSetOperationType.UpdateSingle)
                {
                    Oritax.TaxSimp.Common.CashManagementEntity cashManagementEntity = this.BankAccountEntity.CashManagementTransactions.Where(cashTran =>
                                                cashTran.ID == (Guid)data.Tables[BankTransactionDetailsDS.BANKTRANSDETAILSTABLE].Rows[0][BankTransactionDetailsDS.ID]).FirstOrDefault();

                    cashManagementEntity.AccountName = BankAccountEntity.BSB.Split('-')[1] + BankAccountEntity.AccountNumber;
                    cashManagementEntity.AdministrationSystem = BankAccountEntity.BSB.Split('-')[1] + BankAccountEntity.AccountNumber;
                    cashManagementEntity.ImportTransactionType = data.Tables[BankTransactionDetailsDS.BANKTRANSDETAILSTABLE].Rows[0][BankTransactionDetailsDS.IMPORTTRANTYPE].ToString();
                    cashManagementEntity.Category = data.Tables[BankTransactionDetailsDS.BANKTRANSDETAILSTABLE].Rows[0][BankTransactionDetailsDS.CATTRANTYPE].ToString();
                    cashManagementEntity.SystemTransactionType = data.Tables[BankTransactionDetailsDS.BANKTRANSDETAILSTABLE].Rows[0][BankTransactionDetailsDS.SYSTRANTYPE].ToString();
                    cashManagementEntity.Amount = (decimal)data.Tables[BankTransactionDetailsDS.BANKTRANSDETAILSTABLE].Rows[0][BankTransactionDetailsDS.AMOUNT];
                    cashManagementEntity.Adjustment = (decimal)data.Tables[BankTransactionDetailsDS.BANKTRANSDETAILSTABLE].Rows[0][BankTransactionDetailsDS.ADJUSTMENT];
                    cashManagementEntity.Comment = data.Tables[BankTransactionDetailsDS.BANKTRANSDETAILSTABLE].Rows[0][BankTransactionDetailsDS.COMMENT].ToString();
                    cashManagementEntity.TransactionDate = (DateTime)data.Tables[BankTransactionDetailsDS.BANKTRANSDETAILSTABLE].Rows[0][BankTransactionDetailsDS.TRANDATE];
                    MatchBankTransactionWithDividendOrDistribution(cashManagementEntity, _bankAccount.ParentCID);
                }
                else if (bankTransactionDetailsDS.DataSetOperationType == DataSetOperationType.DeletSingleTD)
                {
                    Oritax.TaxSimp.Common.CashManagementEntity cashManagementEntity = this.BankAccountEntity.CashManagementTransactions.Where(cashTran =>
                                                cashTran.ID == (Guid)data.Tables[BankTransactionDetailsDS.BANKTRANSDETAILSTABLE].Rows[0][BankTransactionDetailsDS.ID]).FirstOrDefault();

                    if (cashManagementEntity != null)
                    {
                        //remove settledunsettledentity if any
                        var unit = (data as BankTransactionDetailsDS).Unit;
                        RemoveSettleOrders(unit.ClientId, cashManagementEntity, unit.CurrentUser);

                        if (this._bankAccount.DeletedCashManagementTransactions == null)
                        {
                            _bankAccount.DeletedCashManagementTransactions = new ObservableCollection<CashManagementEntity>();
                        }
                        _bankAccount.DeletedCashManagementTransactions.Add(cashManagementEntity);
                        this.BankAccountEntity.CashManagementTransactions.Remove(cashManagementEntity);
                    }
                }
                else if (bankTransactionDetailsDS.DataSetOperationType == DataSetOperationType.NewSingleTD)
                {
                    Oritax.TaxSimp.Common.CashManagementEntity cashManagementEntity = new Common.CashManagementEntity();
                    DataRow dr = data.Tables[BankTransactionDetailsDS.BANKTRANSDETAILSTABLE].Rows[0];
                    cashManagementEntity.ID = Guid.NewGuid();
                    cashManagementEntity.AccountName = BankAccountEntity.BSB.Split('-')[1] + BankAccountEntity.AccountNumber;
                    cashManagementEntity.AdministrationSystem = (string)dr[BankTransactionDetailsDS.ADMINSYS];
                    cashManagementEntity.ImportTransactionType = dr[BankTransactionDetailsDS.IMPORTTRANTYPE].ToString();
                    cashManagementEntity.Category = dr[BankTransactionDetailsDS.CATTRANTYPE].ToString();
                    cashManagementEntity.SystemTransactionType = dr[BankTransactionDetailsDS.SYSTRANTYPE].ToString();
                    cashManagementEntity.Amount = (decimal)dr[BankTransactionDetailsDS.AMOUNT];
                    cashManagementEntity.Adjustment = (decimal)dr[BankTransactionDetailsDS.ADJUSTMENT];
                    cashManagementEntity.Comment = dr[BankTransactionDetailsDS.COMMENT].ToString();

                    cashManagementEntity.InstitutionID = (Guid)dr[BankTransactionDetailsDS.INSTITUTEID];
                    cashManagementEntity.ContractNote = (string)dr[BankTransactionDetailsDS.CONTRACTNOTE];
                    cashManagementEntity.IsConfirmed = Convert.ToBoolean(dr[BankTransactionDetailsDS.ISCONFIRMED]);
                    cashManagementEntity.TransactionDate = (DateTime)dr[BankTransactionDetailsDS.TRANDATE];
                    cashManagementEntity.TransactionType = (string)dr[BankTransactionDetailsDS.TRANTYPE];
                    if (cashManagementEntity.TransactionType == "At Call")
                        cashManagementEntity.MaturityDate = (DateTime)dr[BankTransactionDetailsDS.TRANDATE];
                    else
                        cashManagementEntity.MaturityDate = (DateTime)dr[BankTransactionDetailsDS.MATURITYDATE];

                    cashManagementEntity.Product = (Guid)dr[BankTransactionDetailsDS.PROID];

                    cashManagementEntity.InstitutionID = (Guid)dr[BankTransactionDetailsDS.INSTITUTEID];
                    cashManagementEntity.TransactionType = (string)dr[BankTransactionDetailsDS.TRANTYPE];

                    cashManagementEntity.Currency = "AUD";
                    cashManagementEntity.InvestmentCode = "CASH";
                    cashManagementEntity.Status = "Manually Input";
                    cashManagementEntity.InvestmentName = "AUD CASH";

                    cashManagementEntity.TransactionDate = (DateTime)dr[BankTransactionDetailsDS.TRANDATE];
                    if (dr[BankTransactionDetailsDS.INTERESTRATE] is DBNull)
                        cashManagementEntity.InterestRate = 0;
                    else
                        cashManagementEntity.InterestRate = (decimal)dr[BankTransactionDetailsDS.INTERESTRATE];
                    cashManagementEntity.Product = (Guid)dr[BankTransactionDetailsDS.PROID];
                    if (dr[BankTransactionDetailsDS.ORDERID] != null && !string.IsNullOrEmpty(dr[BankTransactionDetailsDS.ORDERID].ToString()))
                    {
                        cashManagementEntity.OrderID = Guid.Parse(dr[BankTransactionDetailsDS.ORDERID].ToString());
                    }

                    if (dr[BankTransactionDetailsDS.ORDERITEMID] != null && !string.IsNullOrEmpty(dr[BankTransactionDetailsDS.ORDERITEMID].ToString()))
                    {
                        cashManagementEntity.OrderItemID = Guid.Parse(dr[BankTransactionDetailsDS.ORDERITEMID].ToString());
                    }

                    this.BankAccountEntity.CashManagementTransactions.Add(cashManagementEntity);
                    var unit = (data as BankTransactionDetailsDS).Unit;
                    //Contract Note is required for attaching confirmation
                    if (!string.IsNullOrEmpty(cashManagementEntity.ContractNote.Trim()) || cashManagementEntity.IsConfirmed)
                    {
                        SettleOrder(unit.ClientId, cashManagementEntity, unit.CurrentUser);
                    }
                }
                else if (bankTransactionDetailsDS.DataSetOperationType == DataSetOperationType.UpdateSingleTD)
                {
                    Common.CashManagementEntity cashManagementEntity = BankAccountEntity.CashManagementTransactions.FirstOrDefault(cashTran => cashTran.ID == (Guid)data.Tables[BankTransactionDetailsDS.BANKTRANSDETAILSTABLE].Rows[0][BankTransactionDetailsDS.ID]);
                    DataRow dr = data.Tables[BankTransactionDetailsDS.BANKTRANSDETAILSTABLE].Rows[0];

                    cashManagementEntity.AccountName = BankAccountEntity.BSB.Split('-')[1] + BankAccountEntity.AccountNumber;
                    cashManagementEntity.AdministrationSystem = (string)dr[BankTransactionDetailsDS.ADMINSYS];
                    cashManagementEntity.ImportTransactionType = dr[BankTransactionDetailsDS.IMPORTTRANTYPE].ToString();
                    cashManagementEntity.Category = dr[BankTransactionDetailsDS.CATTRANTYPE].ToString();
                    cashManagementEntity.SystemTransactionType = dr[BankTransactionDetailsDS.SYSTRANTYPE].ToString();
                    cashManagementEntity.Amount = (decimal)dr[BankTransactionDetailsDS.AMOUNT];
                    cashManagementEntity.Adjustment = (decimal)dr[BankTransactionDetailsDS.ADJUSTMENT];
                    cashManagementEntity.Comment = dr[BankTransactionDetailsDS.COMMENT].ToString();
                    cashManagementEntity.TransactionType = (string)dr[BankTransactionDetailsDS.TRANTYPE];
                    cashManagementEntity.InstitutionID = (Guid)dr[BankTransactionDetailsDS.INSTITUTEID];
                    cashManagementEntity.ContractNote = (string)dr[BankTransactionDetailsDS.CONTRACTNOTE];
                    cashManagementEntity.IsConfirmed = Convert.ToBoolean(dr[BankTransactionDetailsDS.ISCONFIRMED]);
                    cashManagementEntity.TransactionDate = (DateTime)dr[BankTransactionDetailsDS.TRANDATE];

                    cashManagementEntity.Currency = "AUD";
                    cashManagementEntity.InvestmentCode = "CASH";
                    cashManagementEntity.Status = "Manually Input";
                    cashManagementEntity.InvestmentName = "AUD CASH";

                    if (cashManagementEntity.TransactionType == "At Call")
                        cashManagementEntity.MaturityDate = (DateTime)dr[BankTransactionDetailsDS.TRANDATE];
                    else
                        cashManagementEntity.MaturityDate = (DateTime)dr[BankTransactionDetailsDS.MATURITYDATE];
                    cashManagementEntity.InterestRate = (decimal)dr[BankTransactionDetailsDS.INTERESTRATE];
                    cashManagementEntity.Product = (Guid)dr[BankTransactionDetailsDS.PROID];
                    var unit = (data as BankTransactionDetailsDS).Unit;
                    //Contract Note is required for attaching confirmation
                    if (!string.IsNullOrEmpty(cashManagementEntity.ContractNote) || cashManagementEntity.IsConfirmed)
                    {
                        SettleOrder(unit.ClientId, cashManagementEntity, unit.CurrentUser);
                    }
                    else
                    {
                        //if contract note is not provided remove settledunsettledentity
                        RemoveSettleOrders(unit.ClientId, cashManagementEntity, unit.CurrentUser);
                    }
                }
                else if (bankTransactionDetailsDS.DataSetOperationType == DataSetOperationType.DeletBulkTD)
                {
                    var cashManagementEntity = BankAccountEntity.CashManagementTransactions.Where(cashTran => cashTran.OrderID == (Guid)data.Tables[BankTransactionDetailsDS.BANKTRANSDETAILSTABLE].Rows[0][BankTransactionDetailsDS.ORDERID]).ToList();

                    if (cashManagementEntity.Count > 0)
                    {
                        foreach (var entity in cashManagementEntity.ToList())
                        {
                            if (_bankAccount.DeletedCashManagementTransactions == null)
                            {
                                _bankAccount.DeletedCashManagementTransactions =
                                    new ObservableCollection<CashManagementEntity>();
                            }
                            _bankAccount.DeletedCashManagementTransactions.Add(entity);
                            BankAccountEntity.CashManagementTransactions.Remove(entity);
                        }
                    }

                }
            }

            else if (data is BankTransactionDetailsFixDS)
            {
                if (BankAccountEntity.CashManagementTransactions != null && BankAccountEntity.CashManagementTransactions.Count > 0)
                {
                    foreach (Common.CashManagementEntity cashEntity in BankAccountEntity.CashManagementTransactions)
                    {
                        if (cashEntity.Category == string.Empty)
                        {
                            cashEntity.AccountName = "BWA-100" + BankAccountEntity.AccountNumber;
                            string comment = cashEntity.Comment;

                            if (cashEntity.ImportTransactionType != string.Empty && cashEntity.ImportTransactionType != "Deposit" && cashEntity.ImportTransactionType != "Withdrawal")
                            {
                                comment = cashEntity.ImportTransactionType;
                                cashEntity.Comment = comment;

                                if (cashEntity.TotalAmount > 0)
                                    cashEntity.ImportTransactionType = "Deposit";
                                else
                                    cashEntity.ImportTransactionType = "Withdrawal";

                                SetTransactioncCat(cashEntity, comment);
                            }
                            else
                                SetTransactioncCat(cashEntity, comment);
                        }

                        if (BankAccountEntity.AccoutType.ToLower() == "cma")
                            SetTransactioncCat(cashEntity, cashEntity.Comment);
                        else if (BankAccountEntity.AccoutType.ToLower() == "termdeposit")
                            SetTransactioncCatTD(BankAccountEntity.CashManagementTransactions);
                    }
                }
            }
            else if (data is ImportProcessDS)
            {
                var ds = data as ImportProcessDS;
                switch ((WebCommands)ds.Command)
                {
                    case WebCommands.ImportBankwestDatafeedImportFile:
                        ImporBankTransactionAndHoldings(ds);
                        break;
                    case WebCommands.ImportJPMCashTransactionsAndHoldings:
                        ImporBankTransactionAndHoldingsSMA(ds);
                        break;
                    case WebCommands.ImportAtCallTransactionsImportFiles:
                        ImportAtCallTransactions(ds);
                        break;
                    case WebCommands.MaquarieBankTransactionsImport:
                        this.ImporBankTransactionAndHoldingsMacquarie(ds);
                        break;
                    case WebCommands.ImportAtCallClientTransactionImportFile:
                        ImportAtCallTrans(ds);
                        break;
                    case WebCommands.ImportTDClientTransactionImportFile:
                        ImportTDTransaction(ds);
                        break;
                }
            }

            #region set default field in last
            if (_bankAccount.BSB == "XXX-XXX")
            {
                _bankAccount.Holding = 0;
                foreach (Common.CashManagementEntity cashManagementEntity in _bankAccount.CashManagementTransactions)
                {
                    if (cashManagementEntity.SystemTransactionType == "Redemption")
                        _bankAccount.Holding += cashManagementEntity.Amount;
                    else
                        _bankAccount.Holding += cashManagementEntity.TotalAmount;
                }
            }

            Name = _bankAccount.Name;
            OrganizationStatus = _bankAccount.OrganizationStatus;
            #endregion

            return ModifiedState.MODIFIED;
        }

        private void ImportTDTransaction(ImportProcessDS ds)
        {
            if (ds.Tables.Contains("AtTDTransaction"))
            {
                var dr = ds.Tables["AtTDTransaction"].Select(string.Format("accountNumber='{0}'", _bankAccount.AccountNumber));
                if (dr.Length > 0)
                {
                    foreach (var each in dr)
                        AddTransactionTD(each);

                }
            }
        }
        private void AddTransactionTD(DataRow dr)
        {
            if (_bankAccount.CashManagementTransactions == null)
            {
                _bankAccount.CashManagementTransactions = new ObservableCollection<CashManagementEntity>();
            }
            // Set Values in entity TD
            var cashManagementEntity = CreateTDTransactionEntity(dr);
            if (cashManagementEntity != null)
            {
                
                if (dr["ClientID"] != DBNull.Value)
                    SettleOrder(dr["ClientID"].ToString(), cashManagementEntity,(dr.Table.DataSet as IHasOrganizationUnit).Unit.CurrentUser);


                dr["Message"] += "Successful.";
                _bankAccount.CashManagementTransactions.Add(cashManagementEntity);
            }
        }
        private CashManagementEntity CreateTDTransactionEntity(DataRow dr)
        {
            var cashManagementEntity = new CashManagementEntity();

            DateTime dateofTrans;
            var info = new DateTimeFormatInfo();
            info.ShortDatePattern = "dd/MM/yyyy";
            decimal utemp;
            //don't add transaction if Date of Transaction and Amount are not in correct format
            try
            {
                cashManagementEntity.ID = Guid.NewGuid();
                cashManagementEntity.AccountName = BankAccountEntity.BSB.Split('-')[1] + BankAccountEntity.AccountNumber;
                cashManagementEntity.AdministrationSystem = dr["BrokerName"] + "-TD";
                cashManagementEntity.Status = "Uploaded";
                cashManagementEntity.Comment = string.Empty;
                cashManagementEntity.Currency = "AUD";
                cashManagementEntity.ImportTransactionType = new[] { "Redemption", "Withdrawal" }.Contains(dr["TransactionType"].ToString()) ? "Withdrawal" : "Deposit";
                cashManagementEntity.SystemTransactionType = dr["TransactionType"].ToString() == "Deposit" ? "Application" : dr["TransactionType"].ToString();
                cashManagementEntity.Category = cashManagementEntity.SystemTransactionType == "Interest" ? "Income" : "Investment";
                cashManagementEntity.Adjustment = 0;
                cashManagementEntity.TransactionType = "TD";
                cashManagementEntity.InterestRate = 0;
                cashManagementEntity.InvestmentCode = "CASH";
                cashManagementEntity.InvestmentName = "AUD CASH";
                cashManagementEntity.IsConfirmed = true;
                cashManagementEntity.IsContractNote = true;


                if (decimal.TryParse(dr["TransactionAmount"].ToString(), out utemp))
                    cashManagementEntity.Amount = utemp;
                else
                {
                    dr["Message"] += "Invalid Amount,";
                    dr["HasErrors"] = true;
                }
              
                if (dr["ContractID"].ToString() != string.Empty)
                cashManagementEntity.ContractNote = dr["ContractID"].ToString();
                else
                {
                    dr["Message"] += "Invalid Contract ID,";
                    dr["HasErrors"] = true;
                }


                if (dr["TransactionType"].ToString() == string.Empty)
                {
                    dr["Message"] += "Invalid Transaction Type,";
                    dr["HasErrors"] = true;
                }
                if (dr["InstituteID"] != DBNull.Value)
                cashManagementEntity.InstitutionID = new Guid(dr["InstituteID"].ToString());
                if (dr["ProductID"] != DBNull.Value)
                    cashManagementEntity.Product = new Guid(dr["ProductID"].ToString());
                
                if (dr["ServiceType"] != DBNull.Value)
                cashManagementEntity.ServiceType = (ServiceTypes)System.Enum.Parse(typeof(ServiceTypes), dr["ServiceType"].ToString());

          
                if (DateTime.TryParse(dr["TransactionDate"].ToString(), info, DateTimeStyles.None, out dateofTrans))
                {
                    cashManagementEntity.MaturityDate = dateofTrans;
                    cashManagementEntity.TransactionDate = dateofTrans;
                }
                else
                {
                    dr["Message"] += "Invalid transaction date,";
                    dr["HasErrors"] = true;
                }
                if (!bool.Parse(dr["HasErrors"].ToString()))
                {
                    var oldtrans = _bankAccount.CashManagementTransactions.Where(temp => temp.ContractNote == cashManagementEntity.ContractNote);
                    if (oldtrans != null && oldtrans.Count() > 0)// transaction already exits
                    {
                        cashManagementEntity.CopyImportantPropertiesFromOldTransaction(oldtrans.FirstOrDefault());
                        for (int i = oldtrans.Count() - 1; i >= 0; i--)
                        {
                            _bankAccount.CashManagementTransactions.Remove(oldtrans.ElementAt(i));
                        }
                        dr["Message"] += "Transaction updated,";
                    }
                }
            }
            catch
            {
                cashManagementEntity = null;
            }

            if (bool.Parse(dr["HasErrors"].ToString()))
                cashManagementEntity = null;

            return cashManagementEntity;
        }
        private void ImportAtCallTrans(ImportProcessDS ds)
        {
            if (ds.Tables.Contains("AtCallAmm"))
            {
                var dr = ds.Tables["AtCallAmm"].Select(string.Format("accountNumber='{0}'", _bankAccount.AccountNumber));
                if (dr.Length > 0)
                {
                    foreach (var each in dr)
                    {
                        AddTransactionAtCall(each);
                    }
                }
            }
        }
        private void AddTransactionAtCall(DataRow dr)
        {
            if (_bankAccount.CashManagementTransactions == null)
            {
                _bankAccount.CashManagementTransactions = new ObservableCollection<CashManagementEntity>();
            }
            //Set values in Entity AT Call
            var cashManagementEntity = CreateAtCallTransactionEntity(dr);
            if (cashManagementEntity != null)
            {
                
                if (dr["ClientID"] != DBNull.Value)
                    SettleOrder(dr["ClientID"].ToString(), cashManagementEntity,(dr.Table.DataSet as IHasOrganizationUnit).Unit.CurrentUser);
                

                dr["Message"] += "Successful.";
                _bankAccount.CashManagementTransactions.Add(cashManagementEntity);
            }
        }
        private CashManagementEntity CreateAtCallTransactionEntity(DataRow dr)
        {
            var cashManagementEntity = new CashManagementEntity();

            DateTime dateofTrans;
            var info = new DateTimeFormatInfo();
            info.ShortDatePattern = "dd/MM/yyyy";
            decimal utemp;
            //don't add transaction if Date of Transaction and Amount are not in correct format
            try
            {
                cashManagementEntity.ID = Guid.NewGuid();
                cashManagementEntity.AccountName = BankAccountEntity.BSB.Split('-')[1] + BankAccountEntity.AccountNumber;
                cashManagementEntity.AdministrationSystem = dr["BrokerName"] + "-At Call";
                cashManagementEntity.Comment = string.Empty;
                cashManagementEntity.Currency = "AUD";
                cashManagementEntity.ImportTransactionType = new[] { "Redemption", "Withdrawal" }.Contains(dr["TransactionType"].ToString()) ? "Withdrawal" : "Deposit";
                cashManagementEntity.SystemTransactionType = dr["TransactionType"].ToString() == "Deposit" ? "Application" : dr["TransactionType"].ToString();
                cashManagementEntity.Category = cashManagementEntity.SystemTransactionType == "Interest" ? "Income" : "Investment";
                cashManagementEntity.Adjustment = 0;
                cashManagementEntity.IsConfirmed = true;
                cashManagementEntity.TransactionType = "At Call";
                cashManagementEntity.InvestmentCode = "CASH";
                cashManagementEntity.InvestmentName = "AUD CASH";
                cashManagementEntity.IsConfirmed = true;
                cashManagementEntity.IsContractNote = true;
                cashManagementEntity.Status = "Uploaded";


                if (decimal.TryParse(dr["TransactionAmount"].ToString(), out utemp))
                    cashManagementEntity.Amount = utemp;
                else
                {
                    dr["Message"] += "Invalid Amount,";
                    dr["HasErrors"] = true;
                }
                if (decimal.TryParse(dr["TransactionRate"].ToString(), out utemp))
                    cashManagementEntity.InterestRate = utemp;
                else
                {
                    dr["Message"] += "Invalid Transaction Rate,";
                    dr["HasErrors"] = true;
                }


                if (!string.IsNullOrEmpty(dr["ContractID"].ToString()))
                    cashManagementEntity.ContractNote = dr["ContractID"].ToString();
                else
                {
                    dr["Message"] += "Invalid Contract ID,";
                    dr["HasErrors"] = true;
                }

                if (string.IsNullOrEmpty(dr["TransactionType"].ToString()))
                {
                    dr["Message"] += "Invalid Transaction Type,";
                    dr["HasErrors"] = true;
                }

                if (dr["ProductID"] != DBNull.Value)
                    cashManagementEntity.Product = new Guid(dr["ProductID"].ToString());

                if (dr["ServiceType"] != DBNull.Value)
                cashManagementEntity.ServiceType = (ServiceTypes)System.Enum.Parse(typeof(ServiceTypes), dr["ServiceType"].ToString());

                if (dr["InstituteID"] != DBNull.Value)
                cashManagementEntity.InstitutionID = new Guid(dr["InstituteID"].ToString());
                if (DateTime.TryParse(dr["TransactionDate"].ToString(), info, DateTimeStyles.None, out dateofTrans))
                {
                    cashManagementEntity.MaturityDate = dateofTrans;
                    cashManagementEntity.TransactionDate = dateofTrans;
                }
                else
                {
                    dr["Message"] += "Invalid Transaction Date,";
                    dr["HasErrors"] = true;
                }
                if (!bool.Parse(dr["HasErrors"].ToString()))
                {
                    var oldtrans = _bankAccount.CashManagementTransactions.Where(temp => temp.ContractNote == cashManagementEntity.ContractNote);
                    if (oldtrans != null && oldtrans.Count() > 0)// transaction already exits
                    {
                        cashManagementEntity.CopyImportantPropertiesFromOldTransaction(oldtrans.FirstOrDefault());
                        for (int i = oldtrans.Count() - 1; i >= 0; i--)
                        {
                            _bankAccount.CashManagementTransactions.Remove(oldtrans.ElementAt(i));
                        }
                        dr["Message"] += "Transaction updated,";
                    }
                }
            }
            catch
            {
                cashManagementEntity = null;
            }

            if (bool.Parse(dr["HasErrors"].ToString()))
                cashManagementEntity = null;

            return cashManagementEntity;
        }
        private void MatchBankTransactionWithDividendOrDistribution(CashManagementEntity cashManagementEntity, Guid parentCID)
        {
            if (parentCID != Guid.Empty)//if id f the client is not empty
            {
                var client = Broker.GetBMCInstance(parentCID) as IHasDistributionsAndDividends;
                if (client != null)
                {
                    client.MatchBankTransactionWithDividendOrDistribution
                    (cashManagementEntity, new IdentityCMDetail
                    {
                        Cid = CID,
                        Clid = Clid,
                        Csid = Csid
                    });
                }

            }
        }

        private void ImportAtCallTransactions(ImportProcessDS ds)
        {
            var drs = ds.Tables["AtCallTransactions"].Select(string.Format("CustomerNumber='{0}' and hasErrors='false'", BankAccountEntity.CustomerNo));
            if (drs.Length > 0)
            {
                foreach (DataRow each in drs)
                {
                    ValidationRow(each);
                    if ((!bool.Parse(each["HasErrors"].ToString())) && AddATCallTransaction(each))
                    {
                        var ClientID = ds.ExtendedProperties["ClientID"];
                        var ClientCID = ds.ExtendedProperties["ClientCID"];
                        var ClientName = ds.ExtendedProperties["ClientName"];
                        var holding = Convert.ToDecimal(each["Balance"].ToString());
                        var tranDate = Convert.ToDateTime(each["TranDate"].ToString());
                        var transHodling = _bankAccount.CashManagementTransactions.Where(trans => trans.InstitutionID == new Guid(each["InstituteID"].ToString()) && trans.Product.Value == new Guid(each["ProductID"].ToString()) && trans.TransactionDate <= tranDate).Sum(tr => tr.Amount);
                        var diff = holding - transHodling;
                        var rows = ds.Tables["AtCallHoldings"].Select(string.Format("CustomerNumber='{0}'  and ClientID='{1}'", each["CustomerNumber"], ClientID));

                        if (rows.Length > 0)
                        {
                            foreach (DataRow dr in rows)
                            {


                                dr["ClientID"] = ClientID;
                                dr["ClientCID"] = ClientCID;
                                dr["ClientName"] = ClientName;
                                dr["CID"] = this.CID;
                                dr["CLID"] = this.CLID;
                                dr["CSID"] = this.CSID;
                                dr["CustomerNumber"] = each["CustomerNumber"];
                                dr["Holding"] = holding;
                                dr["TransactionHolding"] = transHodling;
                                dr["Diff"] = diff;
                            }

                        }
                        else
                        {

                            DataRow dr = ds.Tables["AtCallHoldings"].NewRow();
                            dr["ClientID"] = ClientID;
                            dr["ClientCID"] = ClientCID;
                            dr["ClientName"] = ClientName;
                            dr["CID"] = this.CID;
                            dr["CLID"] = this.CLID;
                            dr["CSID"] = this.CSID;
                            dr["CustomerNumber"] = each["CustomerNumber"];
                            dr["Holding"] = holding;
                            dr["TransactionHolding"] = transHodling;
                            dr["Diff"] = diff;
                            ds.Tables["AtCallHoldings"].Rows.Add(dr);
                        }
                    }
                }
            }

        }

        private void ValidationRow(DataRow dr)
        {
            decimal utemp;
            long temp;
            if (string.IsNullOrEmpty(dr["CustomerNumber"].ToString()))
            {
                dr["Message"] += "Invalid Customer Number,";
                dr["HasErrors"] = true;
            }
            if (string.IsNullOrEmpty(dr["Month"].ToString()))
            {
                dr["Message"] += "Invalid Month,";
                dr["HasErrors"] = true;
            }
            if (string.IsNullOrEmpty(dr["Balance"].ToString()))
            {
                dr["Message"] += "Invalid Balance,";
                dr["HasErrors"] = true;
            }
            if (string.IsNullOrEmpty(dr["Amount"].ToString()))
            {
                dr["Message"] += "Invalid amount,";
                dr["HasErrors"] = true;
            }
            if ((!decimal.TryParse(dr["Balance"].ToString(), NumberStyles.Any, CultureInfo.CurrentCulture, out utemp)) && (!string.IsNullOrEmpty(dr["Balance"].ToString())))
            {
                dr["Message"] += "Invalid Balance,";
                dr["HasErrors"] = true;
            }
            if ((!decimal.TryParse(dr["Amount"].ToString(), NumberStyles.Any, CultureInfo.CurrentCulture, out utemp)) && (!string.IsNullOrEmpty(dr["Amount"].ToString())))
            {
                dr["Message"] += "Invalid amount,";
                dr["HasErrors"] = true;
            }
            string CustNumber = string.Empty;
            if (dr["CustomerNumber"].ToString().Contains(" "))
            {
                CustNumber = dr["CustomerNumber"].ToString().Replace(" ", "");
            }
            else
            {
                CustNumber = dr["CustomerNumber"].ToString();
            }
            string AccNumber = string.Empty;
            if (dr["AccNumber"].ToString().Contains(" "))
            {
                AccNumber = dr["AccNumber"].ToString().Replace(" ", "");
            }
            else
            {
                AccNumber = dr["AccNumber"].ToString();
            }

            if ((!long.TryParse(CustNumber.Trim(), NumberStyles.Any, CultureInfo.CurrentCulture, out temp)) && (!string.IsNullOrEmpty(CustNumber.ToString())))
            {
                dr["Message"] += "Invalid Customer Number,";
                dr["HasErrors"] = true;
            }

        }

        private bool AddATCallTransaction(DataRow dr)
        {
            if (BankAccountEntity.CashManagementTransactions == null)
            {

                BankAccountEntity.CashManagementTransactions = new ObservableCollection<CashManagementEntity>();
            }

            CashManagementEntity cashManagementEntity = new CashManagementEntity();
            if (BankAccountEntity.AccountNumber == dr["CustomerNumber"].ToString())
            {
                if (dr["ProductID"] == DBNull.Value || string.IsNullOrEmpty(dr["ProductID"].ToString()))
                {
                    dr["Message"] = "There is no FIIG/AMM account associated to the client. Please contact e-Clipse Head Office if assistance is required";
                    dr["HasErrors"] = true;
                    return false;
                }
                else
                {
                    cashManagementEntity.ID = Guid.NewGuid();
                    cashManagementEntity.AccountName = BankAccountEntity.BSB.Split('-')[1] + BankAccountEntity.AccountNumber;
                    cashManagementEntity.ImportTransactionType = dr["ImportTranType"].ToString();
                    cashManagementEntity.SystemTransactionType = dr["SysTranType"].ToString();
                    cashManagementEntity.Amount = Convert.ToDecimal(dr["Amount"].ToString());
                    cashManagementEntity.InstitutionID = new Guid(dr["InstituteID"].ToString());
                    cashManagementEntity.TransactionDate = Convert.ToDateTime(dr["TranDate"].ToString());
                    cashManagementEntity.TransactionType = "At Call";
                    if (cashManagementEntity.TransactionType == "At Call")
                        cashManagementEntity.MaturityDate = Convert.ToDateTime(dr["TranDate"].ToString());
                    cashManagementEntity.Currency = "AUD";
                    cashManagementEntity.InvestmentCode = "CASH";
                    cashManagementEntity.Status = "";
                    cashManagementEntity.InvestmentName = "AUD CASH";
                    cashManagementEntity.InterestRate = 0;
                    cashManagementEntity.Product = new Guid(dr["ProductID"].ToString());
                    cashManagementEntity.AdministrationSystem = dr["BrokerName"] + "-At Call";
                    cashManagementEntity.Category = "Income";
                    cashManagementEntity.Adjustment = 0;
                    cashManagementEntity.Comment = "";
                    cashManagementEntity.ContractNote = ""; // There is no Contact note available in import file that's why settle will not executed             
                    cashManagementEntity.IsConfirmed = false;
                    this.BankAccountEntity.CashManagementTransactions.Add(cashManagementEntity);
                    dr["Message"] = "Successfully Added";
                }
                return true;
            }
            else
            {
                dr["Message"] = "Customer Number not found";
                dr["HasErrors"] = true;
                return false;
            }

        }

        private void ImporBankTransactionAndHoldings(ImportProcessDS ds)
        {
            string bsb = "";
            if (!string.IsNullOrEmpty(_bankAccount.BSB))
            {
                if (_bankAccount.BSB.Contains('-'))
                {
                    bsb = _bankAccount.BSB.Split('-')[1];
                }
                else if (bsb.Length > 5)//length should be sixe
                {
                    bsb = bsb.Substring(3, 5);
                }
            }
            else
                bsb = "bsbNotFound";

            string BsBNAccountNumber = bsb + _bankAccount.AccountNumber;

            if (ds.Tables.Contains("CashTransctions"))
            {
                var drs = ds.Tables["CashTransctions"].Select(string.Format("BSBNAccountNumber='{0}'", BsBNAccountNumber));
                if (drs.Length > 0)
                {
                    foreach (var each in drs)
                        AddTransactions(each, BsBNAccountNumber);
                }
            }

            if (ds.Tables.Contains("HoldingBalances"))
            {
                var drs = ds.Tables["HoldingBalances"].Select(string.Format("BSBNAccountNumber='{0}'", BsBNAccountNumber));
                if (drs.Length > 0)
                {
                    foreach (var each in drs)
                        SetAccountHoldings(each);
                }
            }

        }

        private void ImporBankTransactionAndHoldingsSMA(ImportProcessDS ds)
        {

            string BsBNAccountNumber = _bankAccount.CustomerNo;

            if (ds.Tables.Contains("CashTransactions"))
            {
                var drs = ds.Tables["CashTransactions"].Select(string.Format("AccountCID='{0}'", this.CID.ToString()));

                if (drs.Length > 0)
                {
                    this.BankAccountEntity.CustomerNo = drs[0]["AccountNumber"].ToString();
                    foreach (var each in drs)
                        this.AddTransactionsSMACASH(each);
                }
            }
        }

        private void ImporBankTransactionAndHoldingsMacquarie(ImportProcessDS ds)
        {
            string bsb = "";
            if (!string.IsNullOrEmpty(_bankAccount.BSB))
            {
                if (_bankAccount.BSB.Contains('-'))
                {
                    bsb = _bankAccount.BSB.Split('-')[1];
                }
                else if (bsb.Length > 5)//length should be sixe
                {
                    bsb = bsb.Substring(3, 5);
                }
            }
            else
                bsb = "bsbNotFound";
            string BsBNAccountNumber = _bankAccount.AccountNumber;
            if (ds.Tables.Contains(MaquarieCMATransactionsDS.MACQUARIEBANKTRANSACTIONSTABLE))
            {
                var drs = ds.Tables[MaquarieCMATransactionsDS.MACQUARIEBANKTRANSACTIONSTABLE].Select(string.Format("{0}='{1}' and {2}='false'", MaquarieCMATransactionsDS.ACCOUNTNUMBER, _bankAccount.AccountNumber, MaquarieCMATransactionsDS.HASERRORS));
                if (drs.Length > 0)
                {
                    foreach (var each in drs)
                        AddTransactionsMaquarieCMA(each);
                }

            }
        }

        private void SetAccountHoldings(DataRow dr)
        {
            DateTimeFormatInfo info = new DateTimeFormatInfo();
            info.ShortDatePattern = "dd/MM/yyyy";
            DateTime dateofHolding = DateTime.Now;
            decimal holding = 0;
            decimal unSettledOrder = 0;


            try
            {
                //don't add transaction if Date of Transaction and Amount are not in correct format
                if (!DateTime.TryParse(dr["DateOfHolding"].ToString(), info, DateTimeStyles.None, out dateofHolding))
                {
                    dr["Message"] += "Invalid Date Of Holding,";
                    dr["HasErrors"] = true;
                }

                if (!decimal.TryParse(dr["HoldingAmount"].ToString(), out holding))
                {
                    dr["Message"] += "Invalid Holding Amount,";
                    dr["HasErrors"] = true;
                }
                if (string.IsNullOrEmpty(dr["UnsettledOrder"].ToString()))
                    if (!decimal.TryParse(dr["UnsettledOrder"].ToString(), out unSettledOrder))
                    {
                        dr["Message"] += "Invalid Unsettled Order,";
                        dr["HasErrors"] = true;
                    }
            }
            catch
            {
                dr["HasErrors"] = true;
            }

            if (!bool.Parse(dr["HasErrors"].ToString()))
            {
                _bankAccount.Holding = holding;
                _bankAccount.UploadTime = dateofHolding;
                _bankAccount.UnsettledOrder = unSettledOrder;
                dr["Message"] += "Succesfully updated.";
            }
        }

        private void AddTransactions(DataRow dr, string BsBNAccountNumber)
        {
            if (_bankAccount.CashManagementTransactions == null)
            {
                _bankAccount.CashManagementTransactions = new System.Collections.ObjectModel.ObservableCollection<Common.CashManagementEntity>();
            }

            Common.CashManagementEntity cashManagementEntity = CreateCashManagementEntity(dr, BsBNAccountNumber);

            if (cashManagementEntity.Category == string.Empty)
            {
                cashManagementEntity.AccountName = "BWA-100" + BankAccountEntity.AccountNumber;
                string comment = cashManagementEntity.Comment;

                if (cashManagementEntity.ImportTransactionType != string.Empty)
                {
                    comment = cashManagementEntity.ImportTransactionType;
                    cashManagementEntity.Comment = comment;

                    if (cashManagementEntity.TotalAmount > 0)
                        cashManagementEntity.ImportTransactionType = "Deposit";
                    else
                        cashManagementEntity.ImportTransactionType = "Withdrawal";

                    SetTransactioncCat(cashManagementEntity, comment);
                }
                else
                    SetTransactioncCat(cashManagementEntity, comment);
            }

            if (cashManagementEntity != null)
            {
                //add UnsettledOrder Settlement code here

                SetSettledUnSettledTransaction(cashManagementEntity);
                if (dr["ClientID"] != DBNull.Value)
                    SettleOrder(dr["ClientID"].ToString(), cashManagementEntity, (dr.Table.DataSet as IHasOrganizationUnit).Unit.CurrentUser);


                dr["Message"] += "Successful.";
                _bankAccount.CashManagementTransactions.Add(cashManagementEntity);
            }

        }

        private void AddTransactionsSMACASH(DataRow dr, string memberID, string clientID)
        {
            if (_bankAccount.CashManagementTransactions == null)
            {
                _bankAccount.CashManagementTransactions = new System.Collections.ObjectModel.ObservableCollection<Common.CashManagementEntity>();
            }

            Common.CashManagementEntity cashManagementEntity = CreateCashManagementEntitySMA(dr, memberID, clientID);

            if (cashManagementEntity.Category == string.Empty)
            {
                cashManagementEntity.AccountName = this.Name;
                string comment = cashManagementEntity.Comment;

                if (cashManagementEntity.ImportTransactionType != string.Empty)
                {
                    comment = cashManagementEntity.ImportTransactionType;
                    cashManagementEntity.Comment = comment;

                    if (cashManagementEntity.TotalAmount > 0)
                        cashManagementEntity.ImportTransactionType = "Deposit";
                    else
                        cashManagementEntity.ImportTransactionType = "Withdrawal";

                    SetTransactioncCat(cashManagementEntity, comment);
                }
                else
                {
                    if (cashManagementEntity.TotalAmount > 0)
                        cashManagementEntity.ImportTransactionType = "Deposit";
                    else
                        cashManagementEntity.ImportTransactionType = "Withdrawal";

                    SetTransactioncCat(cashManagementEntity, comment);
                }
            }

            if (cashManagementEntity != null)
            {
                dr["Message"] += "Successful.";
                _bankAccount.CashManagementTransactions.Add(cashManagementEntity);
            }
        }

        private void AddTransactionsSMACASH(DataRow dr)
        {
            if (_bankAccount.CashManagementTransactions == null)
            {
                _bankAccount.CashManagementTransactions = new System.Collections.ObjectModel.ObservableCollection<Common.CashManagementEntity>();
            }

            Common.CashManagementEntity cashManagementEntity = CreateCashManagementEntitySMA(dr);

            if (cashManagementEntity.Category == string.Empty)
            {
                cashManagementEntity.AccountName = this.Name;
                string comment = cashManagementEntity.Comment;

                if (cashManagementEntity.ImportTransactionType != string.Empty)
                {
                    comment = cashManagementEntity.ImportTransactionType;
                    cashManagementEntity.Comment = comment;

                    if (cashManagementEntity.TotalAmount > 0)
                        cashManagementEntity.ImportTransactionType = "Deposit";
                    else
                        cashManagementEntity.ImportTransactionType = "Withdrawal";

                    SetTransactioncCat(cashManagementEntity, comment);
                }
                else
                {
                    if (cashManagementEntity.TotalAmount > 0)
                        cashManagementEntity.ImportTransactionType = "Deposit";
                    else
                        cashManagementEntity.ImportTransactionType = "Withdrawal";

                    SetTransactioncCat(cashManagementEntity, comment);
                }
            }

            if (cashManagementEntity != null)
            {
                SetSettledUnSettledTransaction(cashManagementEntity);
                if (dr["ClientID"] != DBNull.Value)
                    SettleOrder(dr["ClientID"].ToString(), cashManagementEntity, (dr.Table.DataSet as IHasOrganizationUnit).Unit.CurrentUser);

                dr["Message"] += "Successful.";
                _bankAccount.CashManagementTransactions.Add(cashManagementEntity);
            }
        }

        private void SetSettledUnSettledTransaction(Common.CashManagementEntity cashManagementEntity)
        {

            if (BankAccountEntity.PaidOrders != null)
            {
                var paidorder = BankAccountEntity.PaidOrders.FirstOrDefault(order => order.IsSettled == false && Math.Floor(order.Amount) == Math.Floor(cashManagementEntity.Amount));

                if (paidorder != null)
                {
                    BankAccountEntity.UnsettledOrder -= paidorder.Amount;
                    paidorder.IsSettled = true;
                    paidorder.SettelerTransactionID = cashManagementEntity.ID.ToString();

                }
            }
        }


        /// <summary>
        /// it should be used for bank transactions ,TD and  AtCall
        /// one must know eclipse Client id for order settlement so when importing transactions we need to know if account is attached to a client or not
        /// </summary>
        /// <param name="clientID">ID of the client to Whom Account is attached</param>
        /// <param name="cashManagementEntity">Trasnaction Entity </param>
        /// <param name="currentUser">User Entity</param>
        private void SettleOrder(string clientID, CashManagementEntity cashManagementEntity, UserEntity currentUser)
        {
            if (!string.IsNullOrEmpty(clientID))
            {
                Guid cid = CheckIfSettelledCmExists(clientID, Broker, currentUser);
                if (cid != Guid.Empty)
                {
                    if (_bankAccount.AccoutType.ToLower() != "termdeposit") // for bank accounts
                    {
                        var ds = new SettledUnsetteledTransactionDS
                            {
                                Unit = new Data.OrganizationUnit
                                    {
                                        Name = clientID,
                                        ClientId = clientID,
                                        Type = ((int)OrganizationType.SettledUnsettled).ToString(),
                                        CurrentUser = currentUser,
                                    },
                                CommandType = DatasetCommandTypes.Settle
                            };

                        DataTable dt = new DataTable("CashTransctions");
                        var accNumber = new DataColumn("BSBNAccountNumber", typeof(string));

                        string bsb = string.Empty;

                        if (!string.IsNullOrEmpty(_bankAccount.BSB) && _bankAccount.BSB.Contains('-'))
                            bsb = _bankAccount.BSB.Split('-')[1];
                        string BsBNAccountNumber = bsb + _bankAccount.AccountNumber;
                        accNumber.DefaultValue = BsBNAccountNumber;
                        dt.Columns.Add(accNumber);
                        var amount = new DataColumn("Amount", typeof(decimal));
                        amount.DefaultValue = cashManagementEntity.TotalAmount;
                        dt.Columns.Add(amount);
                        var dc1 = new DataColumn("DateOfTransaction", typeof(string));
                        dc1.DefaultValue = cashManagementEntity.TransactionDate.ToString("dd/MM/yyyy");
                        dt.Columns.Add(dc1);
                        var dc = new DataColumn("TransactionID", typeof(Guid));
                        dc.DefaultValue = cashManagementEntity.ID;
                        dt.Columns.Add(dc);

                        dt.Rows.Add(dt.NewRow());
                        ds.Tables.Add(dt);
                        var settledUnsettledCm = Broker.GetBMCInstance(cid);
                        settledUnsettledCm.SetData(ds);
                        Broker.ReleaseBrokerManagedComponent(settledUnsettledCm);

                    }
                    else if (_bankAccount.AccoutType.ToLower() == "termdeposit") // for Term Deposit and At Call Transactions
                    {

                        var ds = new SettledUnsetteledTransactionDS
                            {
                                Unit = new Data.OrganizationUnit
                                    {
                                        Name = clientID,
                                        ClientId = clientID,
                                        Type = ((int)OrganizationType.SettledUnsettled).ToString(),
                                        CurrentUser = currentUser,
                                    },
                                CommandType = DatasetCommandTypes.Settle
                            };

                        var dt = new DataTable("BankTransactionDetailsTables");
                        var dcTrasnactionID = new DataColumn("TransactionID", typeof(Guid));
                        dcTrasnactionID.DefaultValue = cashManagementEntity.ID;
                        dt.Columns.Add(dcTrasnactionID);

                        var dctype = new DataColumn("TranType", typeof(string));
                        dctype.DefaultValue = cashManagementEntity.TransactionType;
                        dt.Columns.Add(dctype);

                        var dcsystetype = new DataColumn("SysTranType", typeof(string));
                        dcsystetype.DefaultValue = cashManagementEntity.SystemTransactionType;
                        dt.Columns.Add(dcsystetype);

                        var dctradeDate = new DataColumn("TranDate", typeof(DateTime));
                        dctradeDate.DefaultValue = cashManagementEntity.TransactionDate;
                        dt.Columns.Add(dctradeDate);

                        var dcproductID = new DataColumn("ProductID", typeof(string));
                        dcproductID.DefaultValue = cashManagementEntity.Product;
                        dt.Columns.Add(dcproductID);

                        var dcInstituteID = new DataColumn("InstituteID", typeof(string));
                        dcInstituteID.DefaultValue = cashManagementEntity.InstitutionID;
                        dt.Columns.Add(dcInstituteID);

                        var amount = new DataColumn("Amount", typeof(decimal));
                        amount.DefaultValue = cashManagementEntity.TotalAmount;
                        dt.Columns.Add(amount);

                        var rate = new DataColumn("InterestRate", typeof(decimal));
                        rate.DefaultValue = cashManagementEntity.InterestRate;
                        dt.Columns.Add(rate);

                        dt.Rows.Add(dt.NewRow());
                        ds.Tables.Add(dt);
                        var settledUnsettledCm = Broker.GetBMCInstance(cid);
                        settledUnsettledCm.SetData(ds);
                        Broker.ReleaseBrokerManagedComponent(settledUnsettledCm);
                    }
                }
            }
        }


        private void RemoveSettleOrders(string clientId, CashManagementEntity cashManagementEntity, UserEntity currentUser)
        {
            Guid cid = CheckIfSettelledCmExists(clientId, Broker, currentUser);
            if (cid != Guid.Empty)
            {
                var unit = new Data.OrganizationUnit
                {
                    Name = clientId,
                    ClientId = clientId,
                    Type = ((int)OrganizationType.SettledUnsettled).ToString(),
                    CurrentUser = currentUser,
                };

                var ds = new SettledUnsetteledDS { CommandType = DatasetCommandTypes.Delete, Unit = unit };

                if (cashManagementEntity.ID != Guid.Empty)
                {
                    DataRow dr = ds.SettledUnsetteledTable.NewRow();
                    dr[ds.SettledUnsetteledTable.TRANSACTIONID] = cashManagementEntity.ID;
                    ds.SettledUnsetteledTable.Rows.Add(dr);
                    var settledUnsettledCm = Broker.GetBMCInstance(cid);
                    settledUnsettledCm.SetData(ds);
                    Broker.ReleaseBrokerManagedComponent(settledUnsettledCm);
                }
            }
        }

        private static Guid CheckIfSettelledCmExists(string clientID, ICMBroker broker, UserEntity user)
        {
            var ds = new SettledUnsetteledDS
            {
                Unit = new Data.OrganizationUnit
                {
                    Name = clientID,
                    CurrentUser = user,
                    Type = ((int)OrganizationType.SettledUnsettled).ToString(),
                },
                CommandType = DatasetCommandTypes.Check,
                Command = (int)WebCommands.GetOrganizationUnitsByType

            };

            var org = broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            org.GetData(ds);
            broker.ReleaseBrokerManagedComponent(org);

            return ds.Unit.Cid;
        }

        private Common.CashManagementEntity CreateCashManagementEntity(DataRow dr, string BsBNAccountNumber)
        {
            Common.CashManagementEntity cashManagementEntity = new Common.CashManagementEntity();

            DateTime dateofTrans;
            DateTimeFormatInfo info = new DateTimeFormatInfo();
            info.ShortDatePattern = "dd/MM/yyyy";
            decimal amount;
            //don't add transaction if Date of Transaction and Amount are not in correct format
            try
            {
                cashManagementEntity.ID = Guid.NewGuid();
                cashManagementEntity.ExternalReferenceID = dr["BankwestReferenceNumber"].ToString();
                cashManagementEntity.Comment = dr["TransactionDescription"].ToString();
                cashManagementEntity.Status = "Uploaded";// "Bankwest";
                cashManagementEntity.ImportTransactionType = dr["TransactionType"].ToString();
                cashManagementEntity.SystemTransactionType = dr["TransactionType"].ToString();
                cashManagementEntity.InvestmentCode = "CASH";
                cashManagementEntity.Currency = "AUD";
                cashManagementEntity.InvestmentName = "AUD CASH";
                cashManagementEntity.AdministrationSystem = string.Empty;
                //bsbnaccountnumber added
                cashManagementEntity.AccountName = BsBNAccountNumber;

                if (DateTime.TryParse(dr["DateOfTransaction"].ToString(), info, DateTimeStyles.None, out dateofTrans))
                    cashManagementEntity.TransactionDate = dateofTrans;
                else
                {
                    dr["Message"] += "Invalid transaction date,";
                    dr["HasErrors"] = true;
                }

                if (decimal.TryParse(dr["Amount"].ToString(), out amount))
                    cashManagementEntity.Amount = amount;
                else
                {
                    dr["Message"] += "Invalid amount,";
                    dr["HasErrors"] = true;
                }

                if (!bool.Parse(dr["HasErrors"].ToString()))
                {
                    var oldtrans = _bankAccount.CashManagementTransactions.Where(temp => temp.ExternalReferenceID == cashManagementEntity.ExternalReferenceID);
                    if (oldtrans != null && oldtrans.Count() > 0)// transaction already exits
                    {
                        cashManagementEntity.CopyImportantPropertiesFromOldTransaction(oldtrans.FirstOrDefault());
                        for (int i = oldtrans.Count() - 1; i >= 0; i--)
                        {
                            _bankAccount.CashManagementTransactions.Remove(oldtrans.ElementAt(i));
                        }

                        dr["Message"] += "Transaction updated,";
                    }
                }
            }
            catch
            {
                cashManagementEntity = null;
            }

            if (bool.Parse(dr["HasErrors"].ToString()))
                cashManagementEntity = null;

            return cashManagementEntity;
        }

        private Common.CashManagementEntity CreateCashManagementEntitySMA(DataRow dr, string memberID, string clientID)
        {
            Common.CashManagementEntity cashManagementEntity = new Common.CashManagementEntity();

            DateTime dateofTrans;
            DateTimeFormatInfo info = new DateTimeFormatInfo();
            info.ShortDatePattern = "dd/MM/yyyy";

            //don't add transaction if Date of Transaction and Amount are not in correct format
            try
            {
                cashManagementEntity.ID = Guid.NewGuid();
                cashManagementEntity.ExternalReferenceID = string.Empty;
                cashManagementEntity.Comment = dr["Description"].ToString();
                cashManagementEntity.Status = "Upload By Super";
                cashManagementEntity.ImportTransactionType = dr["Description"].ToString();
                cashManagementEntity.SystemTransactionType = dr["Description"].ToString();
                cashManagementEntity.InvestmentCode = "CASH";
                cashManagementEntity.Currency = "AUD";
                cashManagementEntity.InvestmentName = "AUD CASH";
                cashManagementEntity.AdministrationSystem = string.Empty;
                //bsbnaccountnumber added
                cashManagementEntity.AccountName = this.Name;
                dr["HasErrors"] = false;

                if (DateTime.TryParse(dr["EffectiveDate"].ToString(), info, DateTimeStyles.None, out dateofTrans))
                    cashManagementEntity.TransactionDate = dateofTrans;
                else
                {
                    dr["Message"] += "Invalid transaction date,";
                    dr["HasErrors"] = true;
                }


                decimal creditAmount = decimal.Parse(dr["CreditAmount"].ToString());
                decimal debitAmount = decimal.Parse(dr["DebitAmount"].ToString());

                if (creditAmount != 0)
                    cashManagementEntity.Amount = creditAmount;
                else
                    cashManagementEntity.Amount = -1 * debitAmount;

                if (!bool.Parse(dr["HasErrors"].ToString()))
                {
                    var oldtrans = _bankAccount.CashManagementTransactions.Where(temp => temp.Comment == cashManagementEntity.Comment && temp.TransactionDate.Date == cashManagementEntity.TransactionDate.Date && temp.TotalAmount == cashManagementEntity.TotalAmount);
                    if (oldtrans != null && oldtrans.Count() > 0)// a valid transaction already exists with same comment , date and amount.//comment needs to be updated
                    {
                        var count = oldtrans.Count();
                        cashManagementEntity.Comment += " (" + count + " )";

                        //cashManagementEntity.CopyImportantPropertiesFromOldTransaction(oldtrans.FirstOrDefault());
                        //for (int i = oldtrans.Count() - 1; i >= 0; i--)
                        //{
                        //    _bankAccount.CashManagementTransactions.Remove(oldtrans.ElementAt(i));
                        //}
                        //dr["Message"] += "Transaction updated,";
                    }
                }
            }
            catch
            {
                cashManagementEntity = null;
            }

            if (bool.Parse(dr["HasErrors"].ToString()))
                cashManagementEntity = null;

            return cashManagementEntity;
        }

        private Common.CashManagementEntity CreateCashManagementEntitySMA(DataRow dr)
        {
            Common.CashManagementEntity cashManagementEntity = new Common.CashManagementEntity();

            DateTime dateofTrans;
            DateTimeFormatInfo info = new DateTimeFormatInfo();
            info.ShortDatePattern = "dd/MM/yyyy";
            decimal amount;
            //don't add transaction if Date of Transaction and Amount are not in correct format
            try
            {
                cashManagementEntity.ID = Guid.NewGuid();
                cashManagementEntity.ExternalReferenceID = dr["BankwestReferenceNumber"].ToString();
                cashManagementEntity.Comment = dr["TransactionDescription"].ToString();
                cashManagementEntity.Status = "Uploaded";// "Bankwest";
                cashManagementEntity.ImportTransactionType = dr["TransactionType"].ToString();
                cashManagementEntity.SystemTransactionType = dr["TransactionType"].ToString();
                cashManagementEntity.InvestmentCode = "CASH";
                cashManagementEntity.Currency = "AUD";
                cashManagementEntity.InvestmentName = "AUD CASH";
                cashManagementEntity.AdministrationSystem = string.Empty;
                //bsbnaccountnumber added
                cashManagementEntity.AccountName = this.Name;
                dr["HasErrors"] = false;

                if (DateTime.TryParse(dr["DateOfTransaction"].ToString(), info, DateTimeStyles.None, out dateofTrans))
                    cashManagementEntity.TransactionDate = dateofTrans;
                else
                {
                    dr["Message"] += "Invalid transaction date,";
                    dr["HasErrors"] = true;
                }

                if (decimal.TryParse(dr["Amount"].ToString(), out amount))
                    cashManagementEntity.Amount = amount;
                else
                {
                    dr["Message"] += "Invalid amount,";
                    dr["HasErrors"] = true;
                }

                if (!bool.Parse(dr["HasErrors"].ToString()))
                {
                    var oldtrans = _bankAccount.CashManagementTransactions.Where(temp => temp.Comment == cashManagementEntity.Comment && temp.TransactionDate.Date == cashManagementEntity.TransactionDate.Date && temp.TotalAmount == cashManagementEntity.TotalAmount);
                    if (oldtrans != null && oldtrans.Count() > 0)// transaction already exits
                    {
                        cashManagementEntity.CopyImportantPropertiesFromOldTransaction(oldtrans.FirstOrDefault());
                        for (int i = oldtrans.Count() - 1; i >= 0; i--)
                        {
                            _bankAccount.CashManagementTransactions.Remove(oldtrans.ElementAt(i));
                        }

                        dr["Message"] += "Transaction updated,";
                    }
                }
            }
            catch
            {
                cashManagementEntity = null;
            }

            if (bool.Parse(dr["HasErrors"].ToString()))
                cashManagementEntity = null;

            return cashManagementEntity;
        }

        private static void SetTransactioncCat(Common.CashManagementEntity cashEntity, string comment)
        {

            if (comment.ToLower().Contains("premium") || comment.ToLower().Contains("group life") || comment.ToLower().Contains("insurance"))
            {
                cashEntity.Category = "Expense";
                cashEntity.SystemTransactionType = "Insurance Premium";
            }
            else if (comment.ToLower().Contains("e-clipse online fees"))
            {
                cashEntity.Category = "Expense";
                cashEntity.SystemTransactionType = "Administration Fee";
            }

            else if (comment.ToLower().Contains("e-clipse fees"))
            {
                cashEntity.Category = "Expense";
                cashEntity.SystemTransactionType = "Administration Fee";
            }

            else if (comment.ToLower().Contains("expense recovery fee"))
            {
                cashEntity.Category = "Expense";
                cashEntity.SystemTransactionType = "Administration Fee";
            }

            else if (comment.ToLower().Contains("pension"))
            {
                cashEntity.Category = "Benefit Payment";
                cashEntity.SystemTransactionType = "Pension Payment";
            }

            else if (comment.ToLower().Contains("rollover"))
            {
                cashEntity.SystemTransactionType = "Transfer In";
                cashEntity.Category = "Income";
            }

            else if (comment.ToLower().Contains("investment income tax"))
            {
                cashEntity.SystemTransactionType = "Income Tax";
                cashEntity.Category = "Tax";
            }


            else if (comment.ToLower().Contains("contributions tax"))
            {
                cashEntity.SystemTransactionType = "Contributions Tax";
                cashEntity.Category = "Tax";
            }

            else if (comment.ToLower().Contains("capital gains tax"))
            {
                cashEntity.SystemTransactionType = "Capital Gains Tax";
                cashEntity.Category = "Tax";
            }

            else if (comment.ToLower().Contains("sg contribution"))
            {
                cashEntity.SystemTransactionType = "Employer SG";
                cashEntity.Category = "Contribution";
            }

            else if (comment.ToLower().Contains("employer contribution"))
            {
                cashEntity.SystemTransactionType = "Employer SG";
                cashEntity.Category = "Contribution";
            }

            else if (comment.ToLower().Contains("member contribution"))
            {
                cashEntity.SystemTransactionType = "Personal";
                cashEntity.Category = "Contribution";
            }

            else if (comment.ToLower().Contains("non-concessional"))
            {
                cashEntity.SystemTransactionType = "Personal";
                cashEntity.Category = "Contribution";
            }

            else if (comment.ToLower().Contains("self emp taxed cont"))
            {
                cashEntity.SystemTransactionType = "Salary Sacrifice";
                cashEntity.Category = "Contribution";
            }

            else if (comment.ToLower().Contains("payg tax"))
            {
                cashEntity.SystemTransactionType = "PAYG";
                cashEntity.Category = "Tax";
            }

            else if (comment.ToLower().Contains("PAYG Tax"))
            {
                cashEntity.SystemTransactionType = "PAYG";
                cashEntity.Category = "Tax";
            }

            else if (comment.ToLower().Contains("Internal Transfer"))
            {
                if (cashEntity.TotalAmount > 0)
                {
                    cashEntity.SystemTransactionType = "Internal Cash Movement";
                    cashEntity.Category = "Income";
                }
                else if (cashEntity.TotalAmount < 0)
                {
                    cashEntity.SystemTransactionType = "Internal Cash Movement";
                    cashEntity.Category = "Expense";
                }
            }

            else if (comment.ToLower().Contains("benefit"))
            {
                cashEntity.Category = "Expense";
                cashEntity.SystemTransactionType = "Transfer Out";
            }

            else if (comment.ToLower().Contains("rebate"))
            {
                cashEntity.Category = "Income";
                cashEntity.SystemTransactionType = "Commission Rebate";
            }

            else if (comment.ToLower().Contains("rabo"))
            {
                if (cashEntity.TotalAmount > 0)
                {
                    cashEntity.Category = "Investment";
                    cashEntity.SystemTransactionType = "Redemption";
                }
                else if (cashEntity.TotalAmount < 0)
                {
                    cashEntity.Category = "Investment";
                    cashEntity.SystemTransactionType = "Application";
                }
            }

            else if (comment.ToLower().Contains("tpp trust"))
            {
                if (cashEntity.TotalAmount > 0)
                {
                    cashEntity.Category = "Investment";
                    cashEntity.SystemTransactionType = "Redemption";
                }
                else if (cashEntity.TotalAmount < 0)
                {
                    cashEntity.Category = "Investment";
                    cashEntity.SystemTransactionType = "Application";
                }
            }

            else if (comment.ToLower().Contains("rent"))
            {
                if (cashEntity.TotalAmount > 0)
                {
                    cashEntity.Category = "Income";
                    cashEntity.SystemTransactionType = "Rental Income";
                }
                else if (cashEntity.TotalAmount < 0)
                {
                    cashEntity.Category = "Expense";
                    cashEntity.SystemTransactionType = "Property";
                }
            }

            else if (comment.ToLower().Contains("lease"))
            {
                if (cashEntity.TotalAmount > 0)
                {
                    cashEntity.Category = "Income";
                    cashEntity.SystemTransactionType = "Rental Income";
                }
                else if (cashEntity.TotalAmount < 0)
                {
                    cashEntity.Category = "Expense";
                    cashEntity.SystemTransactionType = "Property";
                }
            }

            else if (comment.ToLower().Contains("rates"))
            {
                if (cashEntity.TotalAmount > 0)
                {
                    cashEntity.Category = "Income";
                    cashEntity.SystemTransactionType = "Rental Income";
                }
                else if (cashEntity.TotalAmount < 0)
                {
                    cashEntity.Category = "Expense";
                    cashEntity.SystemTransactionType = "Property";
                }
            }

            else if (comment.ToLower().Contains("adv") && comment.ToLower().Contains("fee"))
            {
                cashEntity.Category = "Expense";
                cashEntity.SystemTransactionType = "Advisory Fee";
            }

            else if (comment.ToLower().Contains("uma") && comment.ToLower().Contains("fee"))
            {
                cashEntity.Category = "Expense";
                cashEntity.SystemTransactionType = "Administration Fee";
            }

            else if (comment.ToLower().Contains("pension"))
            {
                cashEntity.Category = "Expense";
                cashEntity.SystemTransactionType = "Pension Payment";
            }

            else if (comment.ToLower().Contains("cont") && comment.ToLower().Contains("sgc"))
            {
                cashEntity.Category = "Contribution";
                cashEntity.SystemTransactionType = "Employer SG";
            }
            else if (comment.ToLower().Contains("div"))
            {
                cashEntity.Category = "Income";
                cashEntity.SystemTransactionType = "Dividend";
            }
            else if (comment.ToLower().Contains("dist"))
            {
                cashEntity.Category = "Income";
                cashEntity.SystemTransactionType = "Distribution";
            }
            else if (comment.ToLower().Contains("iv0"))
            {
                if (cashEntity.TotalAmount > 0)
                {
                    cashEntity.Category = "Investment";
                    cashEntity.SystemTransactionType = "Redemption";
                }
                else if (cashEntity.TotalAmount < 0)
                {
                    cashEntity.Category = "Investment";
                    cashEntity.SystemTransactionType = "Application";
                }
            }
            else if (comment.ToLower().Contains("interest"))
            {
                cashEntity.Category = "Income";
                cashEntity.SystemTransactionType = "Interest";
            }
            else if (comment.ToLower().Contains("administration fee"))
            {
                cashEntity.Category = "Expense";
                cashEntity.SystemTransactionType = "Administration Fee";
            }
            else if (comment.ToLower().Contains("exp") && comment.ToLower().Contains("fee"))
            {
                cashEntity.Category = "Expense";
                cashEntity.SystemTransactionType = "Investment Fee";
            }
            else if (comment.ToLower().Contains("asset") && comment.ToLower().Contains("fee"))
            {
                cashEntity.Category = "Expense";
                cashEntity.SystemTransactionType = "Investment Fee";
            }
            else if (comment.ToLower().Contains("pension"))
            {
                cashEntity.Category = "Benefit Payment";
                cashEntity.SystemTransactionType = "Pension Payment";
            }
            else if (comment.ToLower().Contains("interest"))
            {
                if (cashEntity.TotalAmount > 0)
                {
                    cashEntity.Category = "Income";
                    cashEntity.SystemTransactionType = "Interest";
                }
                else if (cashEntity.TotalAmount < 0)
                {
                    cashEntity.Category = "Expense";
                    cashEntity.SystemTransactionType = "Interest";
                }
            }
            else if (comment.ToLower().Contains("distribution"))
            {
                if (cashEntity.TotalAmount > 0)
                {
                    cashEntity.Category = "Income";
                    cashEntity.SystemTransactionType = "Distribution";
                }
            }
            else if (comment.ToLower().Contains("purchase"))
            {
                cashEntity.Category = "Investment";
                cashEntity.SystemTransactionType = "Application";
            }
            else if (comment.ToLower().Contains("sale"))
            {
                cashEntity.Category = "Investment";
                cashEntity.SystemTransactionType = "Redemption";
            }
            else
            {
                if (cashEntity.TotalAmount > 0)
                {
                    cashEntity.Category = "Income";
                    cashEntity.SystemTransactionType = "Transfer In";
                }
                else if (cashEntity.TotalAmount < 0)
                {
                    cashEntity.Category = "Expense";
                    cashEntity.SystemTransactionType = "Transfer Out";
                }
            }
        }

        private void SetTransactioncCatTD(ObservableCollection<CashManagementEntity> CashManagementTransactions)
        {
            IOrganization orgCM = this.Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            var cashTransactionsByInsGroup = CashManagementTransactions.GroupBy(tran => tran.InstitutionID);

            foreach (var groupItem in cashTransactionsByInsGroup)
            {
                bool isAtCall = groupItem.FirstOrDefault().AdministrationSystem.Contains("Call");
                var financialIns = orgCM.Institution.Where(ins => ins.ID == groupItem.Key).FirstOrDefault();

                if (isAtCall)
                {
                    bool isING = financialIns.Name.Contains("ing");
                    decimal interestLimit = 2500;
                    if (isING)
                        interestLimit = 4500;

                    foreach (Common.CashManagementEntity cashEntity in groupItem)
                    {
                        if (cashEntity.TotalAmount > 0 && cashEntity.TotalAmount < interestLimit)
                        {
                            cashEntity.ImportTransactionType = "Deposit";
                            cashEntity.Category = "Income";
                            cashEntity.SystemTransactionType = "Interest";
                        }
                        else if (cashEntity.TotalAmount > 0 && cashEntity.TotalAmount > interestLimit)
                        {
                            cashEntity.ImportTransactionType = "Deposit";
                            cashEntity.Category = "Investment";
                            cashEntity.SystemTransactionType = "Application";
                        }
                        else if (cashEntity.TotalAmount < 0)
                        {
                            cashEntity.ImportTransactionType = "Withdrawal";
                            cashEntity.Category = "Investment";
                            cashEntity.SystemTransactionType = "Redemption";
                        }
                    }
                }
                else
                {

                    var getAllApplicationTransaction = groupItem.Where(tran => tran.TransactionDate.Date != tran.MaturityDate.Date && tran.TotalAmount > 0);
                    foreach (Common.CashManagementEntity cashEntity in getAllApplicationTransaction)
                    {
                        cashEntity.ImportTransactionType = "Deposit";
                        cashEntity.Category = "Investment";
                        cashEntity.SystemTransactionType = "Application";

                        var redemptionTran = groupItem.Where(tran => tran.TransactionDate.Date == cashEntity.MaturityDate.Date && tran.TotalAmount < 0).FirstOrDefault();
                        var listOfTranOnMaturityDate = groupItem.Where(tran => tran.TransactionDate.Date == cashEntity.MaturityDate.Date && tran.TotalAmount > 0);
                        if (redemptionTran != null)
                        {
                            redemptionTran.ImportTransactionType = "Withdrawal";
                            redemptionTran.Category = "Investment";
                            redemptionTran.SystemTransactionType = "Redemption";

                            foreach (Common.CashManagementEntity maturityCashTran in listOfTranOnMaturityDate)
                            {
                                decimal initialApplicationAmount = maturityCashTran.TotalAmount + redemptionTran.TotalAmount;
                                if ((cashEntity.TotalAmount + initialApplicationAmount) == 0)
                                {
                                    maturityCashTran.ImportTransactionType = "Deposit";
                                    maturityCashTran.Category = "Income";
                                    maturityCashTran.SystemTransactionType = "Interest";
                                }
                            }
                        }
                    }
                }
            }
        }

        #region DoTagged
        public override void DoTagged(EntityTag tag, Guid modelID)
        {
            string appender = tag.Appender;
            _bankAccount.DoTaggedExtension(tag.SetAppenderExt(appender, ""));

            _bankAccount.DoTaggedExtension(tag.SetAppenderExt(appender, _bankAccount.AccoutType.ToUpper()));
            tag.SetAppender(appender);
        }
        #endregion

        #region OrganizationsChart

        public OrganizationChart GetOrganizationChart()
        {
            OrganizationChart chart = new OrganizationChart();

            chart.Clid = CLID;
            chart.Csid = CSID;
            chart.NodeName = _bankAccount.Name;

            return chart;
        }

        public OrganizationChart GetAssociation(IIdentityCM identitycm, OrganizationType organizationtype)
        {
            OrganizationChart OChart = new OrganizationChart();
            return OChart;
        }

        public OrganizationChart GetEntityParent(OrganizationChart organizationchart)
        {
            return null;
        }

        public OrganizationChart GetEntityChild()
        {
            return null;
        }

        public OrganizationChart GetEntityChildWithEntity()
        {
            return null;
        }

        #endregion

        #region AccountRelationList

        public string AddAccountRelation(string data)
        {
            AccountRelation newitem = data.ToNewOrData<AccountRelation>();
            if (_bankAccount.LinkedAccounts.Exists(p => p == newitem)) return "false";

            _bankAccount.LinkedAccounts.Add(newitem);
            IBrokerManagedComponent component = Broker.GetCMImplementation(newitem.Clid, newitem.Csid);
            component.UpdateDataStream((int)CmCommand.AccountRelationAddReference, (new AccountRelation { Clid = Clid, Csid = Csid, AccountType = newitem.AccountType, ServiceType = newitem.ServiceType }).ToXmlString());

            return "true";
        }
        public string AddAccountRelationReference(string data)
        {
            AccountRelation newitem = data.ToNewOrData<AccountRelation>();
            if (_bankAccount.LinkedAccounts.Exists(p => p == newitem)) return "false";
            _bankAccount.LinkedAccounts.Add(newitem);
            return "true";
        }

        public string DeleteAccountRelation(string data)
        {
            AccountRelation newitem = data.ToNewOrData<AccountRelation>();

            var list = _bankAccount.LinkedAccounts.Where(e => e.Clid == newitem.Clid && e.Csid == newitem.Csid).ToList();
            foreach (var each in list)
            {
                _bankAccount.LinkedAccounts.Remove(each);
            }

            IBrokerManagedComponent component = Broker.GetCMImplementation(newitem.Clid, newitem.Csid);
            component.UpdateDataStream((int)CmCommand.AccountRelationDeleteRefrence, (new AccountRelation { Clid = Clid, Csid = Csid, AccountType = newitem.AccountType }).ToXmlString());

            return "true";
        }
        public string DeleteAccountRelationReference(string data)
        {
            AccountRelation newitem = data.ToNewOrData<AccountRelation>();
            var list = _bankAccount.LinkedAccounts.Where(e => e.Clid == newitem.Clid && e.Csid == newitem.Csid).ToList();
            foreach (var each in list)
            {
                _bankAccount.LinkedAccounts.Remove(each);
            }
            return "true";
        }

        #endregion

        #region importBankWest Accounts Transactions
        public override List<ImportMessages> ImportTransactions(XElement xml)
        {
            List<ImportMessages> messages = new List<ImportMessages>();
            string bsb = "";
            if (!string.IsNullOrEmpty(_bankAccount.BSB))
            {
                if (_bankAccount.BSB.Contains('-'))
                {
                    bsb = _bankAccount.BSB.Split('-')[1];
                }
                else if (bsb.Length > 5)//length should be sixe
                {
                    bsb = bsb.Substring(3, 5);
                }
            }
            else
                bsb = "bsbNotFound";


            string BsBNAccountNumber = bsb + _bankAccount.AccountNumber;
            XElement[] elements = xml.XPathSelectElements("//AccountTransactions[@BSBNAccountNumber = '" + BsBNAccountNumber + "']").ToArray();

            foreach (var each in elements)
                AddTransactions(each, BsBNAccountNumber, messages);

            elements = xml.XPathSelectElements("//AccountHoldings[@BSBNAccountNumber = '" + BsBNAccountNumber + "']").ToArray();

            foreach (var each in elements)
                SetAccountHoldings(each, BsBNAccountNumber, messages);

            return messages;
        }

        private void SetAccountHoldings(XElement holdings, string BSBNAccountNumber, List<ImportMessages> messageses)
        {
            XElement[] elements = holdings.XPathSelectElements("Holding").ToArray();

            foreach (var xElement in elements)
            {

                SetAccountHoldingForDate(xElement, messageses, BSBNAccountNumber);

            }
        }

        private void SetAccountHoldingForDate(XElement xElement, List<ImportMessages> messages, string BSBNAccountNumber)
        {
            BankAccountTransactionMessage message = new BankAccountTransactionMessage();
            DateTimeFormatInfo info = new DateTimeFormatInfo();
            info.ShortDatePattern = "dd/MM/yyyy";
            DateTime dateofHolding = DateTime.Now;
            decimal holding = 0;
            decimal unSettledOrder = 0;


            try
            {

                message.Status = ImportMessageStatus.Sucess;
                message.MessageType = ImportMessageType.Sucess;
                message.BSBNAccountNumber = BSBNAccountNumber;

                message.Type = xElement.XPathSelectElement("Type").Value;
                message.InputMode = "Holding";
                message.Amount = xElement.XPathSelectElement("HoldingAmount").Value;

                message.Date = xElement.XPathSelectElement("DateOfHolding").Value;
                message.UnsettledOrder = xElement.XPathSelectElement("UnsettledOrder").Value;


                //don't add transaction if Date of Transaction and Amount are not in correct format
                if (!DateTime.TryParse(xElement.XPathSelectElement("DateOfHolding").Value, info, DateTimeStyles.None, out dateofHolding))
                {
                    message.Message += "Invalid Date Of Holding,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                }

                if (!decimal.TryParse(xElement.XPathSelectElement("HoldingAmount").Value, out holding))
                {
                    message.Message += "Invalid Holding Amount,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                }



                if (string.IsNullOrEmpty(xElement.XPathSelectElement("UnsettledOrder").Value))
                    if (!decimal.TryParse(xElement.XPathSelectElement("UnsettledOrder").Value, out unSettledOrder))
                    {
                        message.Message += "Invalid Unsettled Order,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                    }

            }
            catch
            {
                message.MessageType = ImportMessageType.Error;


            }



            messages.Add(message);

            if (message.MessageType != ImportMessageType.Error)
            {
                _bankAccount.Holding = holding;
                _bankAccount.UploadTime = dateofHolding;
                _bankAccount.UnsettledOrder = unSettledOrder;
            }

        }

        public string LinkDividendData(DividendEntity dv, int select)
        {
            string ExternalRefID = "";
            if (this._bankAccount.CashManagementTransactions != null)
            {
                Common.CashManagementEntity cmTransaction = _bankAccount.CashManagementTransactions.Where(ee => Math.Abs(ee.TotalAmount - Convert.ToDecimal(dv.CentsPerShare * select)) < 1 && ee.TransactionDate == dv.PaymentDate && ee.SystemTransactionType == "Dividend").FirstOrDefault();
                if (cmTransaction != null)
                {
                    cmTransaction.DividendID = dv.ID;
                    cmTransaction.DividendStatus = TransectionStatus.Matched;
                    ExternalRefID = cmTransaction.ExternalReferenceID;
                }
            }
            return ExternalRefID;
        }

        private void AddTransactions(XElement transactions, string BSBNAccountNumber, List<ImportMessages> messages)
        {
            XElement[] elements = transactions.XPathSelectElements("Transaction").ToArray();

            if (_bankAccount.CashManagementTransactions == null)
            {
                _bankAccount.CashManagementTransactions = new System.Collections.ObjectModel.ObservableCollection<Common.CashManagementEntity>();
            }

            foreach (var xElement in elements)
            {

                Common.CashManagementEntity cashManagementEntity = CreateCashManagementEntity(xElement, messages, BSBNAccountNumber);

                if (cashManagementEntity != null)
                {
                    //add UnsettledOrder Settledment code here

                    if (BankAccountEntity.PaidOrders != null)
                    { //order.RequiredDate == cashManagementEntity.TransactionDate &&
                        var paidorder = BankAccountEntity.PaidOrders.FirstOrDefault(order => order.IsSettled == false && Math.Floor(order.Amount) == Math.Floor(cashManagementEntity.Amount));

                        if (paidorder != null)
                        {
                            BankAccountEntity.UnsettledOrder -= paidorder.Amount;
                            paidorder.IsSettled = true;
                            paidorder.SettelerTransactionID = cashManagementEntity.ID.ToString();

                        }
                    }

                    _bankAccount.CashManagementTransactions.Add(cashManagementEntity);
                }
            }

        }

        public Common.CashManagementEntity CreateCashManagementEntity(XElement xElement, List<ImportMessages> messages, string BSBNAccountNumber)
        {
            Common.CashManagementEntity cashManagementEntity = new Common.CashManagementEntity();
            BankAccountTransactionMessage message = new BankAccountTransactionMessage();

            DateTime dateofTrans;
            DateTimeFormatInfo info = new DateTimeFormatInfo();
            info.ShortDatePattern = "dd/MM/yyyy";
            decimal amount;
            //don't add transaction if Date of Transaction and Amount are not in correct format
            try
            {
                cashManagementEntity.ID = Guid.NewGuid();
                cashManagementEntity.ExternalReferenceID = xElement.XPathSelectElement("BankwestReferenceNumber").Value;
                cashManagementEntity.Comment = xElement.XPathSelectElement("TransactionDescription").Value;
                cashManagementEntity.Status = xElement.XPathSelectElement("Status").Value;// "Bankwest";
                cashManagementEntity.ImportTransactionType = xElement.XPathSelectElement("TransactionType").Value;
                cashManagementEntity.InvestmentCode = xElement.XPathSelectElement("InvestmentCode").Value;
                cashManagementEntity.Currency = xElement.XPathSelectElement("Currency").Value;
                cashManagementEntity.InvestmentName = xElement.XPathSelectElement("InvestmentName").Value;

                cashManagementEntity.AdministrationSystem = xElement.XPathSelectElement("AdministrationSystem").Value;
                cashManagementEntity.AccountName = xElement.XPathSelectElement("AccountName").Value;


                message.Status = ImportMessageStatus.Sucess;
                message.MessageType = ImportMessageType.Sucess;
                message.BSBNAccountNumber = BSBNAccountNumber;
                message.InputMode = "Transaction";
                message.Amount = xElement.XPathSelectElement("Amount").Value;
                message.Date = xElement.XPathSelectElement("DateOfTransaction").Value;
                message.Type = xElement.XPathSelectElement("Type").Value;
                message.BankwestReferenceNumber = xElement.XPathSelectElement("BankwestReferenceNumber").Value;
                message.TransactionDescription = xElement.XPathSelectElement("TransactionDescription").Value;
                message.TransactionType = xElement.XPathSelectElement("TransactionType").Value;


                if (DateTime.TryParse(xElement.XPathSelectElement("DateOfTransaction").Value, info, DateTimeStyles.None, out dateofTrans))
                {
                    cashManagementEntity.TransactionDate = dateofTrans;
                    message.Date = dateofTrans.ToString("dd/MM/yyyy");
                }
                else
                {
                    message.Message += "Invalid transaction date,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                }

                if (decimal.TryParse(xElement.XPathSelectElement("Amount").Value, out amount))
                {
                    cashManagementEntity.Amount = amount;
                }
                else
                {
                    message.Message += "Invalid amount,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                }

                if (message.MessageType != ImportMessageType.Error)
                {
                    var oldtrans = _bankAccount.CashManagementTransactions.Where(temp => temp.ExternalReferenceID == cashManagementEntity.ExternalReferenceID);
                    if (oldtrans != null && oldtrans.Count() > 0)// transaction already exits
                    {
                        cashManagementEntity.CopyImportantPropertiesFromOldTransaction(oldtrans.FirstOrDefault());
                        for (int i = oldtrans.Count() - 1; i >= 0; i--)
                        {
                            _bankAccount.CashManagementTransactions.Remove(oldtrans.ElementAt(i));
                        }

                        message.Message += "Transaction updated,";
                        //message.Status = ImportMessageStatus.AlreayExists;
                        //message.MessageType = ImportMessageType.Error;
                    }
                }

                if (!string.IsNullOrEmpty(message.Message) && message.Message.Contains(','))
                {
                    message.Message = message.Message.Remove(message.Message.LastIndexOf(','));
                }
                else
                {
                    message.Message = "";
                }
            }
            catch
            {
                cashManagementEntity = null;
            }

            messages.Add(message);
            if (message.MessageType == ImportMessageType.Error)
            {
                cashManagementEntity = null;
            }

            return cashManagementEntity;
        }

        #endregion

        #region importBankWest Transactions Dividend Entity
        public override void ImportTransactionsDividendEntity(DividendEntity dv)
        {
            if (_bankAccount.CashManagementTransactions == null)
            {
                _bankAccount.CashManagementTransactions = new System.Collections.ObjectModel.ObservableCollection<Common.CashManagementEntity>();
            }
            else
            {
                foreach (var count in _bankAccount.CashManagementTransactions)
                {
                    if (dv.PaymentDate == count.TransactionDate && Convert.ToDecimal(dv.PaidDividend) == count.Amount)
                    {
                        count.DividendID = dv.ID;
                    }
                }
            }
        }

        public override void DeleteAllCashTransaction_Bank()
        {
            _bankAccount.CashManagementTransactions = new System.Collections.ObjectModel.ObservableCollection<Common.CashManagementEntity>();
            _bankAccount.Holding = 0;
        }

        #endregion

        #region Import Order Pad
        public override List<ImportMessages> ImportPaidOrders(XElement xml)
        {
            List<ImportMessages> messages = new List<ImportMessages>();




            XElement[] elements = xml.XPathSelectElements("//PaidOrder[Account = '" + _bankAccount.BSB + "-" + _bankAccount.AccountNumber + "']").ToArray();

            foreach (var each in elements)
                AddPayOrder(each, messages);



            return messages;
        }

        private void AddPayOrder(XElement order, List<ImportMessages> messages)
        {


            if (_bankAccount.PaidOrders == null)
            {
                _bankAccount.PaidOrders = new List<Common.BankAccountPaidOrderEntity>();
            }



            var payOrder = CreateBankAccountPayOrder(order, messages);

            if (payOrder != null)
            {
                _bankAccount.UnsettledOrder += payOrder.Amount;


                //settle if order already exists in  transactions

                if (BankAccountEntity.CashManagementTransactions != null)
                { //order.RequiredDate == cashManagementEntity.TransactionDate &&
                    var temp = BankAccountEntity.CashManagementTransactions.FirstOrDefault(temporder => Math.Floor(temporder.Amount) == Math.Floor(payOrder.Amount));

                    if (temp != null)
                    {
                        BankAccountEntity.UnsettledOrder -= payOrder.Amount;
                        payOrder.IsSettled = true;
                        payOrder.SettelerTransactionID = temp.ID.ToString();

                    }
                }

                _bankAccount.PaidOrders.Add(payOrder);
            }

        }

        private Common.BankAccountPaidOrderEntity CreateBankAccountPayOrder(XElement xElement, List<ImportMessages> messages)
        {
            Common.BankAccountPaidOrderEntity cashManagementEntity = new Common.BankAccountPaidOrderEntity();
            BankAccountOrdersMessage message = new BankAccountOrdersMessage();

            // public decimal Amount { get; set; }
            //public string Status { get; set; }
            //public Guid ID { get; set; }
            //public DateTime RequiredDate { get; set; }
            //public string ReferenceNarration { get; set; }



            string Account = xElement.XPathSelectElement("Account").Value;
            string Amount = xElement.XPathSelectElement("Amount").Value;
            string ReferenceNarration = xElement.XPathSelectElement("ReferenceNarration").Value;
            string DateRequired = xElement.XPathSelectElement("DateRequired").Value;
            string Status = xElement.XPathSelectElement("Status").Value;

            if (DateRequired.Length == 7)
            {
                DateRequired = "0" + DateRequired;
            }
            DateTime dateofTrans;
            DateTimeFormatInfo info = new DateTimeFormatInfo();
            info.ShortDatePattern = "dd/MM/yyyy";
            decimal amount;
            //don't add transaction if Date of Transaction and Amount are not in correct format
            try
            {
                cashManagementEntity.ID = Guid.NewGuid();

                cashManagementEntity.ReferenceNarration = ReferenceNarration;
                cashManagementEntity.Status = Status;// "Bankwest";


                message.Status = ImportMessageStatus.Sucess;
                message.MessageType = ImportMessageType.Sucess;


                message.Account = Account;
                message.Amount = Amount;

                message.DateRequired = DateRequired;

                message.ReferenceNarration = ReferenceNarration;


                if (DateTime.TryParseExact(DateRequired, "dMMyyyy", info, DateTimeStyles.None, out dateofTrans))
                {
                    cashManagementEntity.RequiredDate = dateofTrans;
                    message.DateRequired = dateofTrans.ToString("dd/MM/yyyy");
                }
                else
                {
                    message.Message += "Invalid transaction date,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                }

                if (decimal.TryParse(xElement.XPathSelectElement("Amount").Value, out amount))
                {
                    cashManagementEntity.Amount = amount;
                }
                else
                {
                    message.Message += "Invalid amount,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                }


                if (message.MessageType != ImportMessageType.Error)
                {
                    var oldtrans = _bankAccount.PaidOrders.Where(temp => temp.ReferenceNarration == cashManagementEntity.ReferenceNarration);
                    if (oldtrans != null & oldtrans.Any())// transaction already exits
                    {
                        //for (int i = oldtrans.Count() - 1; i >= 0; i--)
                        //    {
                        //    _BankAccount.PaidOrders.Remove(oldtrans.ElementAt(i));
                        //    }


                        message.Message += "Order Pad already exists,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                    }
                }

                if (!string.IsNullOrEmpty(message.Message) && message.Message.Contains(','))
                {
                    message.Message = message.Message.Remove(message.Message.LastIndexOf(','));
                }
                else
                {
                    message.Message = "";
                }

            }
            catch
            {

                cashManagementEntity = null;
            }

            messages.Add(message);
            if (message.MessageType == ImportMessageType.Error)
            {
                cashManagementEntity = null;
            }

            return cashManagementEntity;
        }


        #endregion


    }

}
