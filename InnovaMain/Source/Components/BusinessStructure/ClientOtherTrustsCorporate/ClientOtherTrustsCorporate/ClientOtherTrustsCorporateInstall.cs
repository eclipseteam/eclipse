namespace Oritax.TaxSimp.CM.Group
{
    public class ClientOtherTrustsCorporateInstall
	{
		#region INSTALLATION PROPERTIES

		// Assembly Installation Properties
        public const string ASSEMBLY_ID = "815051A3-D82D-41B6-8536-147D8A2055D2";
        public const string ASSEMBLY_NAME = "ClientOtherTrustsCorporate";
        public const string ASSEMBLY_DISPLAYNAME = "Other Trusts Corporate";
		public const string ASSEMBLY_MAJORVERSION="1";
		public const string ASSEMBLY_MINORVERSION="0";
        public const string ASSEMBLY_DATAFORMAT="0"; 
		public const string ASSEMBLY_REVISION="0"; 

		// Component Installation Properties
        public const string COMPONENT_ID = "E705E1E7-8B32-4241-B685-C610C56F7AA7";
        public const string COMPONENT_NAME = "ClientOtherTrustsCorporate";
        public const string COMPONENT_DISPLAYNAME = "Other Trusts Corporate";
		public const string COMPONENT_CATEGORY="BusinessEntity";

		// Component Version Installation Properties
		public const string COMPONENTVERSION_STARTDATE="01/01/1900 00:00:00 AM";
		public const string COMPONENTVERSION_ENDDATE="31/12/2050 12:59:59 AM";
		public const string COMPONENTVERSION_PERSISTERASSEMBLY="CommonPersisters_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";
		public const string COMPONENTVERSION_PERSISTERCLASS="Oritax.TaxSimp.CommonPersistence.GroupPersister";
        public const string COMPONENTVERSION_IMPLEMENTATIONCLASS = "Oritax.TaxSimp.CM.Group.ClientOtherTrustsCorporateCM";
        	
		// Data Model
		public const string DATAMODEL_MANAGERASSEMBLY="CommonPersisters_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";
		public const string DATAMODEL_MANAGERCLASS="Oritax.TaxSimp.CommonPersistence.GroupPersister";

		#endregion
	}
}