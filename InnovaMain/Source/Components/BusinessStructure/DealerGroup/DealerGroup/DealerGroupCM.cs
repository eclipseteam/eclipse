using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Xml.Linq;
using EPI.Data;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Enum;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Utilities;
using OTC = Oritax.TaxSimp.Common;
using System.Xml;
using System.IO;
using IdentityCMDetail = Oritax.TaxSimp.Common.IdentityCMDetail;


namespace Oritax.TaxSimp.CM.Group
{
    [Serializable]
    public sealed class DealerGroupCM : GroupCM, ISerializable, IIdentityCM, IHaveAssociation, ISupportMigration, ICmHasChildren
    {
        #region PRIVATE
        private List<IdentityCM> _Children;
        private DealerGroupEntity _DealerGroupEntity;
        #endregion
        #region PUBLIC
        public List<IdentityCM> Children { get { return _Children; } set { _Children = value; } }
        #endregion

        public DealerGroupCM()
            : base()
        {
            _Children = new List<IdentityCM>();
            _DealerGroupEntity = new DealerGroupEntity();
        }

        public DealerGroupCM(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            _Children = info.GetValue<List<IdentityCM>>("Oritax.TaxSimp.CM.Group.DealerGroupCM.Children");
            _DealerGroupEntity = info.GetValue<DealerGroupEntity>("Oritax.TaxSimp.CM.Group.DealerGroupCM.DealerGroupEntity");
        }

        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValueX("Oritax.TaxSimp.CM.Group.DealerGroupCM.Children", _Children);
            info.AddValueX("Oritax.TaxSimp.CM.Group.DealerGroupCM.DealerGroupEntity", _DealerGroupEntity);
        }

        public override string GetDataStream(int type, string data)
        {
            if (type == -911)
                type = (int)CmCommand.EditOrganizationUnit;

            string xml = string.Empty;
            switch ((CmCommand)type)
            {
                case CmCommand.GetIncludedIFAList:
                    xml = GetOrganizationUnitNameList(_Children).ToXmlString();
                    break;
                case CmCommand.EditOrganizationUnit:
                    _DealerGroupEntity.OrganizationStatus = this.OrganizationStatus;
                    _DealerGroupEntity.ClientId = this.ClientId;
                    xml = _DealerGroupEntity.ToXmlString();
                    break;
                case CmCommand.XMLData:
                    xml = ExtractXML(data);
                    break;
                case CmCommand.GetEntitySecurity:
                    xml = GetEntitySecurity();
                    break;
                case CmCommand.FullName:
                    xml = _DealerGroupEntity.Name;
                    break;
            }
            return xml;
        }
        protected override void OnGetData(DataSet data)
        {
            base.OnGetData(data);
            #region Set Default  fields
            _DealerGroupEntity.OrganizationStatus = this.OrganizationStatus;
            _DealerGroupEntity.ClientId = this.ClientId;
            this.Name = _DealerGroupEntity.Name;
            #endregion

            if (data is DealerGroupDS)
            {


                var ds = data as DealerGroupDS;
                switch (ds.CommandType)
                {
                    case DatasetCommandTypes.Details://details
                        {
                            AddDetailsRow(ds.DealerGroupTable);
                        }
                        break;

                    default:// for listing on gird
                        {
                            DataRow r = ds.DealerGroupTable.NewRow();
                            r[ds.DealerGroupTable.CLIENTID] = ClientId;
                            r[ds.DealerGroupTable.TRADINGNAME] = _DealerGroupEntity.Name;
                            r[ds.DealerGroupTable.DEALERGROUPTYPE] = _DealerGroupEntity.DealerGroupType;
                            r[ds.DealerGroupTable.TYPE] = typeName;
                            r[ds.DealerGroupTable.AFSLNO] = _DealerGroupEntity.AFSLNo;
                            r[ds.DealerGroupTable.STATUS] = OrganizationStatus;
                            r[ds.DealerGroupTable.CID] = CID;
                            r[ds.DealerGroupTable.CLID] = Clid;
                            r[ds.DealerGroupTable.CSID] = Csid;
                            ds.DealerGroupTable.Rows.Add(r);
                        }
                        break;
                }

            }
            else if (data is MembershipDS)
            {
                var ds = data as MembershipDS;
                switch (ds.CommandType)
                {
                    case DatasetCommandTypes.GetChildren:
                        GetChildren(Children, ds, Broker);
                        break;
                }

            }
            else if (data is AddressDetailsDS)
            {
                var ds = data as AddressDetailsDS;
                GetDealorGroupAddressDetails(ds, _DealerGroupEntity);
            } 
            else if (data is BankAccountDS)
            {
                if (_DealerGroupEntity.BankAccounts == null)
                    _DealerGroupEntity.BankAccounts = new List<Oritax.TaxSimp.Common.IdentityCMDetail>();
                GetBankAccountDs(data as BankAccountDS, _DealerGroupEntity.BankAccounts, Broker);
            }
            else if (data is IndividualDS)
            {
                GetIndividualDs(data as IndividualDS, _DealerGroupEntity, Broker);
            }
            else if (data is InstanceCMDS)
            {
                var ds = data as InstanceCMDS;
                GetInstanceDetails(ds,_DealerGroupEntity,Broker);
            }
            else if (data is MDAAlertDS)
            {
                var ds = data as MDAAlertDS;

                var rows = ds.ClientDetailTable.Select(string.Format("{0}='{1}'", ClientDetailTable.DGCID, this.CID));
                foreach (var row in rows)
                {
                    row[ClientDetailTable.DGCODE] = this.ClientId;
                    if (!string.IsNullOrEmpty(_DealerGroupEntity.Name))
                    {
                        row[ClientDetailTable.DGNAME] = _DealerGroupEntity.Name;
                        row[ClientDetailTable.DGACN] = _DealerGroupEntity.ACN;
                    }
                    else
                    {
                        row[ClientDetailTable.DGNAME] = "Fortnum Private Wealth Pty Ltd"; //Default DG Name.
                        row[ClientDetailTable.DGACN] = _DealerGroupEntity.ACN; //Do not have default value.
                    }                  
                }                
            }
        }

        private void GetInstanceDetails(InstanceCMDS ds, DealerGroupEntity dealerGroupEntity, ICMBroker broker)
        {
            List<Common.IdentityCMDetail> units = null;
            if (ds.CommandType == DatasetCommandTypes.GetChildren)
            {
                switch (ds.UnitPropertyType)
                {
                    case UnitPropertyType.ShareHolders:
                        units = dealerGroupEntity.ShareHolders;

                        break;
                    case UnitPropertyType.Partnerships:
                        units = dealerGroupEntity.Partners;
                        break;

                    case UnitPropertyType.Corporates:
                        units = new List<IdentityCMDetail>();
                             if (dealerGroupEntity.CorporatePrivate != null)
                            foreach (var identityCm in dealerGroupEntity.CorporatePrivate)
                            {
                                units.Add( IdentityCMDetail.FromIdetityCM(identityCm));
                            }
                             if (dealerGroupEntity.CorporatePublic != null)
                                 foreach (var identityCm in dealerGroupEntity.CorporatePublic)
                                 {
                                     units.Add(IdentityCMDetail.FromIdetityCM(identityCm));
                                 }

                        break;

                }
                if (units != null)
                {
                    foreach (var identityCM in units)
                    {
                        var bmc = broker.GetCMImplementation(identityCM.Clid, identityCM.Csid);
                        if (bmc != null)
                        {
                            DataRow dr = ds.InstanceCMDetailsTable.NewRow();
                            dr[ds.InstanceCMDetailsTable.CID] = bmc.CID;
                            dr[ds.InstanceCMDetailsTable.CLID] = bmc.CLID;
                            dr[ds.InstanceCMDetailsTable.CSID] = bmc.CSID;
                            dr[ds.InstanceCMDetailsTable.NAME] = bmc.Name;
                            dr[ds.InstanceCMDetailsTable.TYPE] = bmc.TypeName;
                            dr[ds.InstanceCMDetailsTable.STATUS] = (bmc as OrganizationUnitCM).OrganizationStatus;
                            ds.InstanceCMDetailsTable.Rows.Add(dr);
                            broker.ReleaseBrokerManagedComponent(bmc);
                        }
                    }
                }
            }
        }

        private void GetIndividualDs(IndividualDS individualDs, DealerGroupEntity dealerGroupEntity, ICMBroker broker)
        {

            List<IdentityCMDetail> individuals=null;


            switch (individualDs.IndividualsType)
            {
                case IndividualsType.Applicants:
                    individuals = dealerGroupEntity.Applicants;
                    break;
                case IndividualsType.Signatories:
                    individuals = dealerGroupEntity.Signatories;
                    break;
                case IndividualsType.Directors:
                    individuals = dealerGroupEntity.Directors;
                    break;
               }

                if (individuals != null)
                foreach (Oritax.TaxSimp.Common.IdentityCMDetail identityCM in individuals)
                {
                    var indvCM = broker.GetCMImplementation(identityCM.Clid, identityCM.Csid);
                    if (indvCM != null)
                    {
                        indvCM.GetData(individualDs);
                        if (identityCM.BusinessTitle != null)
                            individualDs.IndividualTable.Rows[individualDs.IndividualTable.Rows.Count - 1][individualDs.IndividualTable.BUSINESSTITLE] = identityCM.BusinessTitle.Title;
                        broker.ReleaseBrokerManagedComponent(indvCM);
                    }
                }
        }

        private void GetDealorGroupAddressDetails(AddressDetailsDS ds, DealerGroupEntity dealerGroupEntity)
        {

            if (dealerGroupEntity.Address == null)
            {

                dealerGroupEntity.Address = new Oritax.TaxSimp.Common.DualAddressEntity();

            }
            FillDealorGroupAddresses(ds, dealerGroupEntity.Address);
        }
        private static void FillDealorGroupAddresses(AddressDetailsDS ds, Oritax.TaxSimp.Common.DualAddressEntity address)
        {
            #region Get Business Address
            
            if (address.BusinessAddress != null)
            {
                FillAddress(ds, address.BusinessAddress, Oritax.TaxSimp.DataSets.AddressType.BusinessAddress);
            }
            if (address.MailingAddress != null)
            {
                FillAddress(ds, address.MailingAddress, Oritax.TaxSimp.DataSets.AddressType.MailingAddress);
            }
            if (address.RegisteredAddress != null)
            {
                FillAddress(ds, address.RegisteredAddress, Oritax.TaxSimp.DataSets.AddressType.RegisteredAddress);
            }
            if (address.ResidentialAddress != null)
            {
                FillAddress(ds, address.ResidentialAddress, Oritax.TaxSimp.DataSets.AddressType.ResidentialAddress);
            }
            if (address.DuplicateMailingAddress != null)
            {
                FillAddress(ds, address.DuplicateMailingAddress, Oritax.TaxSimp.DataSets.AddressType.DuplicateMailingAddress);
            }
           
            #endregion


        }
        private static void FillAddress(AddressDetailsDS ds, Oritax.TaxSimp.Common.AddressEntity address, Oritax.TaxSimp.DataSets.AddressType addressType)
        {

            DataRow drBusinessAddress = ds.ClientDetailsTable.NewRow();
            drBusinessAddress[ds.ClientDetailsTable.ADDRESSLINE1] = address.Addressline1;
            drBusinessAddress[ds.ClientDetailsTable.ADDRESSLINE2] = address.Addressline2;
            drBusinessAddress[ds.ClientDetailsTable.SUBURB] = address.Suburb;
            drBusinessAddress[ds.ClientDetailsTable.STATE] = address.State;
            drBusinessAddress[ds.ClientDetailsTable.COUNTRY] = address.Country;
            drBusinessAddress[ds.ClientDetailsTable.POSTCODE] = address.PostCode;
            drBusinessAddress[ds.ClientDetailsTable.ADDRESSTYPE] = addressType;
            ds.ClientDetailsTable.Rows.Add(drBusinessAddress);
        }
        public static void GetBankAccountDs(BankAccountDS dataSet, List<Oritax.TaxSimp.Common.IdentityCMDetail> bankAccounts, ICMBroker broker)
        {
            if (bankAccounts != null && bankAccounts.Count > 0)
            {
                foreach (var bank in bankAccounts)
                {
                    var entity = broker.GetCMImplementation(bank.Clid, bank.Csid);
                    if (entity != null)
                    {
                        entity.GetData(dataSet);
                        broker.ReleaseBrokerManagedComponent(entity);
                    }
                }
            }
        }

        public override string OnUpdateDataStream(int type, string data)
        {
            string xml = string.Empty;
            DealerGroupEntity _dealergroupentity = null;
            IdentityCM child = null;
            List<OTC.OrganizationUnit> list = null;
            OTC.OrganizationUnit ou = null;
            switch ((CmCommand)type)
            {
                case CmCommand.Add_Single:
                    child = GetChild(data);
                    BMCUpdateComponent(child, CmCommand.Add_Single);
                    _Children.Add(child);
                    break;
                case CmCommand.Remove_Single:
                    child = GetChild(data);
                    BMCUpdateComponent(child, CmCommand.Remove_Single);
                    _Children.RemoveAll(c => c.Clid == child.Clid && c.Csid == child.Csid);
                    break;
                case CmCommand.SPAddLibrary:
                    var _value = data.ToNewOrData<Oritax.TaxSimp.Common.SPLibraryDocument>();
                    if (_value != null)
                    {
                        SPLibrary.AddLibraryToClient(_value.CurrentUser.CurrentUserName, _value.Name);
                    }
                    break;
                case CmCommand.Add_List:
                    list = data.ToNewOrData<List<OTC.OrganizationUnit>>();
                    foreach (var each in list)
                    {
                        child = GetChild(each.ToXmlString());
                        BMCUpdateComponent(child, CmCommand.Add_Single);
                        _Children.Add(child);
                    }
                    break;
                case CmCommand.Remove_List:
                    list = data.ToNewOrData<List<OTC.OrganizationUnit>>();
                    foreach (var each in list)
                    {
                        child = GetChild(each.ToXmlString());
                        BMCUpdateComponent(child, CmCommand.Remove_Single);
                        _Children.RemoveAll(c => c.Clid == child.Clid && c.Csid == child.Csid);
                    }
                    break;
                case CmCommand.EditOrganizationUnit:
                    _dealergroupentity = data.ToNewOrData<DealerGroupEntity>();
                    this.Name = _dealergroupentity.Name;
                    this.OrganizationStatus = _dealergroupentity.OrganizationStatus;
                    _DealerGroupEntity = _dealergroupentity;
                    ou = new OTC.OrganizationUnit();
                    ou.Clid = this.Clid;
                    ou.Csid = this.Csid;
                    ou.Name = _dealergroupentity.Name;
                    xml = ou.ToXmlString();
                    break;
                case CmCommand.AddEntitySecurity:
                    xml = AddEntitySecurity(data);
                    break;
                case CmCommand.DeleteEntitySecurity:
                    xml = DeleteEntitySecurity(data);
                    break;
                case CmCommand.GetEntitySecurity:
                    xml = GetEntitySecurity();
                    break;
                case CmCommand.UpdateClientID:// for the purpose of updating client id's
                    break;
                default:
                    _dealergroupentity = data.ToNewOrData<DealerGroupEntity>();
                    this.Name = _dealergroupentity.Name;
                    this.OrganizationStatus = _dealergroupentity.OrganizationStatus;
                    _DealerGroupEntity = _dealergroupentity;
                    if (ClientId == "" || ClientId == null)
                    {
                        UpDateEventLog.UpdateLog("Dealer Group", _DealerGroupEntity.Name, type, this.Broker);
                    }
                    else
                    {
                        UpDateEventLog.UpdateLog("Dealer Group", _DealerGroupEntity.Name + " (" + EclipseClientID + ")", type, this.Broker);
                    }

                    break;
            }
            return xml;
        }
        protected override ModifiedState OnSetData(DataSet data)
        {
            base.OnSetData(data);

            if (data is MembershipDS)
            {
                var ds = data as MembershipDS;
                IdentityCM child;
                switch (ds.CommandType)
                {
                    case DatasetCommandTypes.Add:
                        foreach (DataRow dr in ds.membershipTable.Rows)
                        {
                            child = new IdentityCM();
                            child.Cid = new Guid(dr[ds.membershipTable.CID].ToString());
                            child.Clid = new Guid(dr[ds.membershipTable.CLID].ToString());
                            child.Csid = new Guid(dr[ds.membershipTable.CSID].ToString());
                            if (Children.Count(ss => ss.Clid == child.Clid && ss.Csid == child.Csid) == 0)
                            {

                                try
                                {
                                    BMCUpdateComponentParent(child, new IdentityCM { Clid = this.Clid, Csid = this.CSID, Cid = this.CID }, CmCommand.Add_Single, Broker);
                                }
                                catch (Exception ex)
                                {

                                    dr.RowError = ex.Message;
                                }
                            }
                        }
                        break;
                    case DatasetCommandTypes.Delete:
                        foreach (DataRow dr in ds.membershipTable.Rows)
                        {
                            child = new IdentityCM();
                            child.Cid = new Guid(dr[ds.membershipTable.CID].ToString());
                            child.Clid = new Guid(dr[ds.membershipTable.CLID].ToString());
                            child.Csid = new Guid(dr[ds.membershipTable.CSID].ToString());
                            if (Children.Count(ss => ss.Clid == child.Clid && ss.Csid == child.Csid) > 0)
                            {
                                BMCUpdateComponentParent(child, new IdentityCM { Clid = this.Clid, Csid = this.CSID, Cid = this.CID }, CmCommand.Remove_Single, Broker);
                                Children.Remove(child);
                            }
                        }
                        break;
                }
            }
            else if (data is DealerGroupDS)
            {
                var ds = data as DealerGroupDS;
                switch (ds.CommandType)
                {
                    case DatasetCommandTypes.Add:

                        if (ds.DealerGroupTable.Rows.Count > 0)
                        {
                            var dealorGroup = new DealerGroupEntity();
                            dealorGroup.ClientId = ClientId;
                            SetEntityDetails(dealorGroup, ds.DealerGroupTable);
                            _DealerGroupEntity = dealorGroup;
                        }
                        break;
                    case DatasetCommandTypes.Update:
                        if (ds.DealerGroupTable.Rows.Count > 0)
                        {
                            SetEntityDetails(_DealerGroupEntity, ds.DealerGroupTable);
                        }

                        break;

                }
            }
            else if (data is AddressDetailsDS)
            {
                var ds = data as AddressDetailsDS;
                SetDealorGroupAddresses(ds, _DealerGroupEntity.Address);
            }
            else if (data is BankAccountDS)
            {
                if (_DealerGroupEntity.BankAccounts == null)
                    _DealerGroupEntity.BankAccounts = new List<Oritax.TaxSimp.Common.IdentityCMDetail>();
                var ds = data as BankAccountDS;
                if (ds.CommandType == DatasetCommandTypes.Delete)
                    RemoveBankAccount(ds, _DealerGroupEntity.BankAccounts, Broker);
                else if (ds.CommandType == DatasetCommandTypes.Update)
                    AssociateBankAccount(ds, _DealerGroupEntity.BankAccounts);
            }
            else if (data is IndividualDS)
            {
                var ds = data as IndividualDS;
             
                List<IdentityCMDetail> individuals = null;


                switch (ds.IndividualsType)
                {
                    case IndividualsType.Applicants:
                        if (_DealerGroupEntity.Applicants==null)
                            _DealerGroupEntity.Applicants=new List<IdentityCMDetail>();
                        individuals = _DealerGroupEntity.Applicants;
                        break;
                    case IndividualsType.Signatories:
                        if (_DealerGroupEntity.Signatories == null)
                            _DealerGroupEntity.Signatories = new List<IdentityCMDetail>();
                        individuals = _DealerGroupEntity.Signatories;
                        break;
                    case IndividualsType.Directors:
                        if (_DealerGroupEntity.Directors == null)
                            _DealerGroupEntity.Directors = new List<IdentityCMDetail>();
                        individuals = _DealerGroupEntity.Directors;
                        break;
                        
                }
                if (ds.CommandType == DatasetCommandTypes.Delete)
                    RemoveIndividual(ds, individuals, Broker);
                else if (ds.CommandType == DatasetCommandTypes.Update)
                    AssociateIndividuals(ds, individuals);
            }
            else if (data is InstanceCMDS)
            {
                var ds = data as InstanceCMDS;

              
                   switch (ds.UnitPropertyType)
                    {
                        case UnitPropertyType.ShareHolders:
                            if (_DealerGroupEntity.ShareHolders == null)
                                _DealerGroupEntity.ShareHolders = new List<IdentityCMDetail>();
                            if (ds.CommandType == DatasetCommandTypes.Delete)
                                RemoveUnits(ds, _DealerGroupEntity.ShareHolders, Broker);
                            else if (ds.CommandType == DatasetCommandTypes.Update)
                                AssociateUnits(ds, _DealerGroupEntity.ShareHolders);
                            break;
                        case UnitPropertyType.Partnerships:
                            if (_DealerGroupEntity.Partners == null)
                                _DealerGroupEntity.Partners = new List<IdentityCMDetail>();
                          
                            if (ds.CommandType == DatasetCommandTypes.Delete)
                                RemoveUnits(ds, _DealerGroupEntity.Partners, Broker);
                            else if (ds.CommandType == DatasetCommandTypes.Update)
                                AssociateUnits(ds, _DealerGroupEntity.Partners);
                            break;
                        case UnitPropertyType.Corporates:
                            if (_DealerGroupEntity.CorporatePrivate == null)
                                _DealerGroupEntity.CorporatePrivate = new List<IdentityCM>();
                            if (_DealerGroupEntity.CorporatePublic == null)
                                _DealerGroupEntity.CorporatePublic = new List<IdentityCM>();

                            if (ds.CommandType == DatasetCommandTypes.Delete)
                            {
                                RemoveUnits(ds, _DealerGroupEntity.CorporatePrivate, Broker);
                                RemoveUnits(ds, _DealerGroupEntity.CorporatePublic, Broker);
                            }
                            else if (ds.CommandType == DatasetCommandTypes.Update)
                            {
                                AssociateUnits(ds, _DealerGroupEntity.CorporatePublic,OrganizationType.ClientCorporationPublic);
                                AssociateUnits(ds, _DealerGroupEntity.CorporatePrivate, OrganizationType.ClientCorporationPrivate);
                            }


                            break;
                    }

                    
                
            }

            #region set default fields
            this.Name = _DealerGroupEntity.Name;
            this.OrganizationStatus = _DealerGroupEntity.OrganizationStatus;
            #endregion

            return ModifiedState.MODIFIED;
        }
        public static void RemoveUnits(InstanceCMDS ds, List<Oritax.TaxSimp.Common.IdentityCMDetail> units, ICMBroker broker)
        {
            if (units != null && ds.Tables[ds.InstanceCMDetailsTable.TABLENAME].Rows.Count > 0)
            {
                var clid = (Guid)ds.Tables[ds.InstanceCMDetailsTable.TABLENAME].Rows[0][ds.InstanceCMDetailsTable.CLID];
                var csid = (Guid)ds.Tables[ds.InstanceCMDetailsTable.TABLENAME].Rows[0][ds.InstanceCMDetailsTable.CSID];

                //Remove Bank Account
                var identityCmDetail = units.FirstOrDefault(ss => ss.Clid == clid && ss.Csid == csid);
                if (identityCmDetail != null)
                {

                    units.Remove(identityCmDetail);
                    ds.ExtendedProperties.Add("Message", "Record has been deleted successfully");


                }

            }
        }

        public static void RemoveUnits(InstanceCMDS ds, List<Oritax.TaxSimp.Data.IdentityCM> units, ICMBroker broker)
        {
            if (units != null && ds.Tables[ds.InstanceCMDetailsTable.TABLENAME].Rows.Count > 0)
            {
                var clid = (Guid)ds.Tables[ds.InstanceCMDetailsTable.TABLENAME].Rows[0][ds.InstanceCMDetailsTable.CLID];
                var csid = (Guid)ds.Tables[ds.InstanceCMDetailsTable.TABLENAME].Rows[0][ds.InstanceCMDetailsTable.CSID];

                //Remove Bank Account
                var identityCmDetail = units.FirstOrDefault(ss => ss.Clid == clid && ss.Csid == csid);
                if (identityCmDetail != null)
                {

                    units.Remove(identityCmDetail);
                    ds.ExtendedProperties.Add("Message", "Record has been deleted successfully");


                }

            }
        }

        public static void AssociateUnits(InstanceCMDS ds, List<Oritax.TaxSimp.Data.IdentityCM> units,OrganizationType Type)
        {
            if (units != null && ds.Tables[ds.InstanceCMDetailsTable.TABLENAME].Rows.Count > 0)
            {
             var drs=   ds.Tables[ds.InstanceCMDetailsTable.TABLENAME].Select(string.Format("{0}='{1}'",ds.InstanceCMDetailsTable.TYPE, Type));
             foreach (DataRow dr in drs)
                {
                    var clid = new Guid(dr[ds.InstanceCMDetailsTable.CLID].ToString());
                    var cid = new Guid(dr[ds.InstanceCMDetailsTable.CID].ToString());
                    var csid = new Guid(dr[ds.InstanceCMDetailsTable.CSID].ToString());

                    var identityCmDetail = units.FirstOrDefault(ss => ss.Clid == new Guid(dr[ds.InstanceCMDetailsTable.CLID].ToString()) && ss.Csid == new Guid(dr[ds.InstanceCMDetailsTable.CSID].ToString()));
                    if (identityCmDetail == null)
                    {
                        if (units == null)
                        {
                            units = new List<Oritax.TaxSimp.Data.IdentityCM>();
                        }
                        units.Add(new Oritax.TaxSimp.Data.IdentityCM { Clid = clid, Csid = csid, Cid = cid });

                    }

                }
            }
        }

        public static void RemoveIndividual(IndividualDS ds, List<Oritax.TaxSimp.Common.IdentityCMDetail> individuals, ICMBroker broker)
        {
            if (individuals!=null&&ds.Tables[ds.IndividualTable.TABLENAME].Rows.Count > 0)
            {
                var clid = (Guid)ds.Tables[ds.IndividualTable.TABLENAME].Rows[0][ds.IndividualTable.CLID];
                var csid = (Guid)ds.Tables[ds.IndividualTable.TABLENAME].Rows[0][ds.IndividualTable.CSID];

                //Remove Bank Account
                var identityCmDetail = individuals.FirstOrDefault(ss => ss.Clid == clid && ss.Csid == csid);
                if (identityCmDetail != null)
                {

                    individuals.Remove(identityCmDetail);
                    ds.ExtendedProperties.Add("Message", "Individual has been deleted successfully");


                }

            }
        }

        public static void AssociateUnits(InstanceCMDS ds, List<Oritax.TaxSimp.Common.IdentityCMDetail> units)
        {
            if (units != null && ds.Tables[ds.InstanceCMDetailsTable.TABLENAME].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[ds.InstanceCMDetailsTable.TABLENAME].Rows)
                {
                    var clid = new Guid(dr[ds.InstanceCMDetailsTable.CLID].ToString());
                    var cid = new Guid(dr[ds.InstanceCMDetailsTable.CID].ToString());
                    var csid = new Guid(dr[ds.InstanceCMDetailsTable.CSID].ToString());
                 
                    var identityCmDetail = units.FirstOrDefault(ss => ss.Clid == new Guid(dr[ds.InstanceCMDetailsTable.CLID].ToString()) && ss.Csid == new Guid(dr[ds.InstanceCMDetailsTable.CSID].ToString()));
                    if (identityCmDetail == null)
                    {
                        if (units == null)
                        {
                            units = new List<Oritax.TaxSimp.Common.IdentityCMDetail>();
                        }
                        units.Add(new Oritax.TaxSimp.Common.IdentityCMDetail { Clid = clid, Csid = csid, Cid = cid});

                    }
                  
                }
            }
        }

        public static void AssociateIndividuals(IndividualDS ds, List<Oritax.TaxSimp.Common.IdentityCMDetail> individuals)
        {
            if (individuals != null && ds.Tables[ds.IndividualTable.TABLENAME].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[ds.IndividualTable.TABLENAME].Rows)
                {
                    var clid = new Guid(dr[ds.IndividualTable.CLID].ToString());
                    var cid = new Guid(dr[ds.IndividualTable.CID].ToString());
                    var csid = new Guid(dr[ds.IndividualTable.CSID].ToString());
                     var businessttitle = dr[ds.IndividualTable.BUSINESSTITLE].ToString();
                    var identityCmDetail = individuals.FirstOrDefault(ss => ss.Clid == new Guid(dr[ds.IndividualTable.CLID].ToString()) && ss.Csid == new Guid(dr[ds.IndividualTable.CSID].ToString()));
                    if (identityCmDetail == null)
                    {
                        if (individuals == null)
                        {
                            individuals = new List<Oritax.TaxSimp.Common.IdentityCMDetail>();
                        }
                        individuals.Add(new Oritax.TaxSimp.Common.IdentityCMDetail { Clid = clid, Csid = csid, Cid = cid, BusinessTitle = new TitleEntity { Title = businessttitle } });

                    }
                    else
                    {
                        identityCmDetail.BusinessTitle = new TitleEntity { Title = businessttitle };
                    }
                }
            }
        }
        

        private static void SetDealorGroupAddresses(AddressDetailsDS ds, Oritax.TaxSimp.Common.DualAddressEntity Address)
        {
            foreach (DataRow dr in ds.ClientDetailsTable.Rows)
            {
                if ((Oritax.TaxSimp.DataSets.AddressType)System.Enum.Parse(typeof(Oritax.TaxSimp.DataSets.AddressType), dr[ds.ClientDetailsTable.ADDRESSTYPE].ToString()) == Oritax.TaxSimp.DataSets.AddressType.BusinessAddress)
                {
                    SetAddress(ds.ClientDetailsTable, Address.BusinessAddress, dr);
                }
                else if (
                    (Oritax.TaxSimp.DataSets.AddressType)System.Enum.Parse(typeof(Oritax.TaxSimp.DataSets.AddressType), dr[ds.ClientDetailsTable.ADDRESSTYPE].ToString()) == Oritax.TaxSimp.DataSets.AddressType.RegisteredAddress)
                {
                    SetAddress(ds.ClientDetailsTable, Address.RegisteredAddress, dr);
                }
                else if (
                    (Oritax.TaxSimp.DataSets.AddressType)
                    System.Enum.Parse(typeof(Oritax.TaxSimp.DataSets.AddressType), dr[ds.ClientDetailsTable.ADDRESSTYPE].ToString()) == Oritax.TaxSimp.DataSets.AddressType.ResidentialAddress)
                {
                    SetAddress(ds.ClientDetailsTable, Address.ResidentialAddress, dr);
                }
                else if (
                    (Oritax.TaxSimp.DataSets.AddressType)
                    System.Enum.Parse(typeof(Oritax.TaxSimp.DataSets.AddressType), dr[ds.ClientDetailsTable.ADDRESSTYPE].ToString()) == Oritax.TaxSimp.DataSets.AddressType.MailingAddress)
                {
                    SetAddress(ds.ClientDetailsTable, Address.MailingAddress, dr);
                }
                else if (
                    (Oritax.TaxSimp.DataSets.AddressType)
                    System.Enum.Parse(typeof(Oritax.TaxSimp.DataSets.AddressType), dr[ds.ClientDetailsTable.ADDRESSTYPE].ToString()) == Oritax.TaxSimp.DataSets.AddressType.DuplicateMailingAddress)
                {
                    SetAddress(ds.ClientDetailsTable, Address.DuplicateMailingAddress, dr);
                }
            }
        }
        private static void SetAddress(AddressDetails table, Oritax.TaxSimp.Common.AddressEntity Address, DataRow dr)
        {
            Address.Addressline1 = dr[table.ADDRESSLINE1].ToString();
            Address.Addressline2 = dr[table.ADDRESSLINE2].ToString();
            Address.Suburb = dr[table.SUBURB].ToString();
            Address.State = dr[table.STATE].ToString();
            Address.Country = dr[table.COUNTRY].ToString();
            Address.PostCode = dr[table.POSTCODE].ToString();
        }
        public static void AssociateBankAccount(BankAccountDS ds, List<Oritax.TaxSimp.Common.IdentityCMDetail> bankAccounts)
        {
            if (ds.Tables[ds.BankAccountsTable.TABLENAME].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[ds.BankAccountsTable.TABLENAME].Rows)
                {
                    var clid = new Guid(dr[ds.BankAccountsTable.CLID].ToString());
                    var csid = new Guid(dr[ds.BankAccountsTable.CSID].ToString());
                    var identityCmDetail = bankAccounts.FirstOrDefault(ss => ss.Clid == new Guid(dr[ds.BankAccountsTable.CLID].ToString()) && ss.Csid == new Guid(dr[ds.BankAccountsTable.CSID].ToString()));
                    if (identityCmDetail == null)
                    {
                        if (bankAccounts == null)
                        {
                            bankAccounts = new List<Oritax.TaxSimp.Common.IdentityCMDetail>();
                        }
                        bankAccounts.Add(new Oritax.TaxSimp.Common.IdentityCMDetail { Clid = clid, Csid = csid });

                    }
                }
            }
        }

        public static void RemoveBankAccount(BankAccountDS ds, List<Oritax.TaxSimp.Common.IdentityCMDetail> bankAccounts, ICMBroker broker)
        {
            if (ds.Tables[ds.BankAccountsTable.TABLENAME].Rows.Count > 0)
            {
                var clid = (Guid)ds.Tables[ds.BankAccountsTable.TABLENAME].Rows[0][ds.BankAccountsTable.CLID];
                var csid = (Guid)ds.Tables[ds.BankAccountsTable.TABLENAME].Rows[0][ds.BankAccountsTable.CSID];

                //Remove Bank Account
                var identityCmDetail = bankAccounts.FirstOrDefault(ss => ss.Clid == clid && ss.Csid == csid);
                if (identityCmDetail != null)
                {
                  
                        bankAccounts.Remove(identityCmDetail);
                        ds.ExtendedProperties.Add("Message", "Account has been deleted successfully");
                  
                   
                }
               
            }
        }
        private void AddDetailsRow(DealerGroupTable dealerGroupTable)
        {
            DataRow r = dealerGroupTable.NewRow();
            r[dealerGroupTable.CLIENTID] = ClientId;
            r[dealerGroupTable.TRADINGNAME] = _DealerGroupEntity.Name;
            r[dealerGroupTable.LEGALNAME] = _DealerGroupEntity.LegalName;
            r[dealerGroupTable.DEALERGROUPTYPE] = _DealerGroupEntity.DealerGroupType;
            r[dealerGroupTable.TYPE] = typeName;
            r[dealerGroupTable.STATUS] = OrganizationStatus;
            r[dealerGroupTable.CID] = CID;
            r[dealerGroupTable.CLID] = Clid;
            r[dealerGroupTable.CSID] = Csid;


            switch (_DealerGroupEntity.DealerGroupType)
            {
                case DealerGroupEntityType.DealerGroupCompanyControl:
                    if (_DealerGroupEntity.Facsimile != null)
                    {
                        r[dealerGroupTable.FACSIMLECOUTNRYCODE] = _DealerGroupEntity.Facsimile.CountryCode;
                        r[dealerGroupTable.FACSIMLECITYCODE] = _DealerGroupEntity.Facsimile.CityCode;
                        r[dealerGroupTable.FACSIMLENUMBER] = _DealerGroupEntity.Facsimile.PhoneNumber;
                    }
                    if (_DealerGroupEntity.WorkPhoneNumber != null)
                    {
                        r[dealerGroupTable.WORKPHONECOUTNRYCODE] = _DealerGroupEntity.WorkPhoneNumber.CountryCode;
                        r[dealerGroupTable.WORKPHONECITYCODE] = _DealerGroupEntity.WorkPhoneNumber.CityCode;
                        r[dealerGroupTable.WORKPHONENUMBER] = _DealerGroupEntity.WorkPhoneNumber.PhoneNumber;
                    }
                    r[dealerGroupTable.ADVISORYFIRMTERMINOLOGY] = _DealerGroupEntity.AdvisoryFirmTerminology;
                    r[dealerGroupTable.WEBSITEADDRESS] = _DealerGroupEntity.Websiteaddress;
                    r[dealerGroupTable.TFN] = _DealerGroupEntity.TFN;
                    r[dealerGroupTable.ABN] = _DealerGroupEntity.ABN;
                    r[dealerGroupTable.AFSLNO] = _DealerGroupEntity.AFSLNo;
                    r[dealerGroupTable.ACN] = _DealerGroupEntity.ACN;
                    r[dealerGroupTable.FUM] = _DealerGroupEntity.FUM;
                    r[dealerGroupTable.TURNOVER] = _DealerGroupEntity.Turnover;
                    break;
                case DealerGroupEntityType.DealerGroupIndividualControl:
                case DealerGroupEntityType.DealerGroupPartnershipControl:
                case DealerGroupEntityType.DealerGroupTrustControl:
                  
                    r[dealerGroupTable.TITLE] = _DealerGroupEntity.Title;
                    r[dealerGroupTable.PREFERREDNAME] = _DealerGroupEntity.PreferredName;
                    r[dealerGroupTable.SURNAME] = _DealerGroupEntity.Surname;
                    r[dealerGroupTable.EMAIL] = _DealerGroupEntity.Email;
                    r[dealerGroupTable.GENDER] = _DealerGroupEntity.Gender;
                    if (_DealerGroupEntity.Facsimile != null)
                    {
                        r[dealerGroupTable.FACSIMLECOUTNRYCODE] = _DealerGroupEntity.Facsimile.CountryCode;
                        r[dealerGroupTable.FACSIMLECITYCODE] = _DealerGroupEntity.Facsimile.CityCode;
                        r[dealerGroupTable.FACSIMLENUMBER] = _DealerGroupEntity.Facsimile.PhoneNumber;
                    }
                    r[dealerGroupTable.ABN] = _DealerGroupEntity.ABN;
                    r[dealerGroupTable.TFN] = _DealerGroupEntity.TFN;
                    r[dealerGroupTable.OCCUPATION] = _DealerGroupEntity.Occupation;
                    if (_DealerGroupEntity.WorkPhoneNumber != null)
                    {
                        r[dealerGroupTable.WORKPHONECOUTNRYCODE] = _DealerGroupEntity.WorkPhoneNumber.CountryCode;
                        r[dealerGroupTable.WORKPHONECITYCODE] = _DealerGroupEntity.WorkPhoneNumber.CityCode;
                        r[dealerGroupTable.WORKPHONENUMBER] = _DealerGroupEntity.WorkPhoneNumber.PhoneNumber;
                    }
                    if (_DealerGroupEntity.HomePhoneNumber != null)
                    {
                        r[dealerGroupTable.HOMEPHONECOUTNRYCODE] = _DealerGroupEntity.HomePhoneNumber.CountryCode;
                        r[dealerGroupTable.HOMEPHONECITYCODE] = _DealerGroupEntity.HomePhoneNumber.CityCode;
                        r[dealerGroupTable.HOMEPHONENUMBER] = _DealerGroupEntity.HomePhoneNumber.PhoneNumber;
                    }
                    if (_DealerGroupEntity.MobilePhoneNumber != null)
                    {
                        r[dealerGroupTable.MOBILEPHONECOUTNRYCODE] = _DealerGroupEntity.MobilePhoneNumber.CountryCode;
                        r[dealerGroupTable.MOBILEPHONENUMBER] = _DealerGroupEntity.MobilePhoneNumber.MobileNumber;
                    }


                    break;
               
                default:
                    throw new ArgumentOutOfRangeException();
            }



            dealerGroupTable.Rows.Add(r);
        }
        private void SetEntityDetails(DealerGroupEntity entity, DealerGroupTable DealerGroupTable)
        {
            DataRow dr = DealerGroupTable.Rows[0];
            entity.Name = dr[DealerGroupTable.TRADINGNAME].ToString();
            entity.LegalName = dr[DealerGroupTable.LEGALNAME].ToString();
            entity.DealerGroupType = (DealerGroupEntityType)System.Enum.Parse(typeof(DealerGroupEntityType), dr[DealerGroupTable.DEALERGROUPTYPE].ToString());
            entity.OrganizationStatus = dr[DealerGroupTable.STATUS].ToString();
            switch (entity.DealerGroupType)
            {
                case DealerGroupEntityType.DealerGroupCompanyControl:

                    entity.Facsimile = new PhoneNumberEntity();
                    entity.Facsimile.CountryCode = dr[DealerGroupTable.FACSIMLECOUTNRYCODE].ToString();
                    entity.Facsimile.CityCode = dr[DealerGroupTable.FACSIMLECITYCODE].ToString();
                    entity.Facsimile.PhoneNumber = dr[DealerGroupTable.FACSIMLENUMBER].ToString();
                    entity.WorkPhoneNumber = new PhoneNumberEntity();
                    entity.WorkPhoneNumber.CountryCode = dr[DealerGroupTable.WORKPHONECOUTNRYCODE].ToString();
                    entity.WorkPhoneNumber.CityCode = dr[DealerGroupTable.WORKPHONECITYCODE].ToString();
                    entity.WorkPhoneNumber.PhoneNumber = dr[DealerGroupTable.WORKPHONENUMBER].ToString();
                    entity.AdvisoryFirmTerminology = dr[DealerGroupTable.ADVISORYFIRMTERMINOLOGY].ToString();
                    entity.Websiteaddress = dr[DealerGroupTable.WEBSITEADDRESS].ToString();
                    entity.TFN = dr[DealerGroupTable.TFN].ToString();
                    entity.ABN = dr[DealerGroupTable.ABN].ToString();
                    entity.AFSLNo = dr[DealerGroupTable.AFSLNO].ToString();
                    entity.ACN = dr[DealerGroupTable.ACN].ToString();
                    entity.FUM = dr[DealerGroupTable.FUM].ToString();
                    entity.Turnover = dr[DealerGroupTable.TURNOVER].ToString();



                    break;
                case DealerGroupEntityType.DealerGroupIndividualControl:
                case DealerGroupEntityType.DealerGroupPartnershipControl:
                case DealerGroupEntityType.DealerGroupTrustControl:
                    entity.Title = dr[DealerGroupTable.TITLE].ToString();
                    entity.PreferredName = dr[DealerGroupTable.PREFERREDNAME].ToString();
                    entity.Surname = dr[DealerGroupTable.SURNAME].ToString();
                    entity.Email = dr[DealerGroupTable.EMAIL].ToString();
                    entity.Gender = dr[DealerGroupTable.GENDER].ToString();
                    entity.Facsimile = new PhoneNumberEntity();
                    entity.Facsimile.CountryCode = dr[DealerGroupTable.FACSIMLECOUTNRYCODE].ToString();
                    entity.Facsimile.CityCode = dr[DealerGroupTable.FACSIMLECITYCODE].ToString();
                    entity.Facsimile.PhoneNumber = dr[DealerGroupTable.FACSIMLENUMBER].ToString();
                    entity.ABN = dr[DealerGroupTable.ABN].ToString();
                    entity.TFN = dr[DealerGroupTable.TFN].ToString();
                    entity.Occupation = dr[DealerGroupTable.OCCUPATION].ToString();
                    entity.WorkPhoneNumber = new PhoneNumberEntity();
                    entity.WorkPhoneNumber.CountryCode = dr[DealerGroupTable.WORKPHONECOUTNRYCODE].ToString();
                    entity.WorkPhoneNumber.CityCode = dr[DealerGroupTable.WORKPHONECITYCODE].ToString();
                    entity.WorkPhoneNumber.PhoneNumber = dr[DealerGroupTable.WORKPHONENUMBER].ToString();
                    entity.MobilePhoneNumber = new MobileNumberEntity();
                    entity.MobilePhoneNumber.CountryCode = dr[DealerGroupTable.MOBILEPHONECOUTNRYCODE].ToString();
                    entity.MobilePhoneNumber.MobileNumber = dr[DealerGroupTable.MOBILEPHONENUMBER].ToString();
                    entity.HomePhoneNumber = new PhoneNumberEntity();
                    entity.HomePhoneNumber.CountryCode = dr[DealerGroupTable.HOMEPHONECOUTNRYCODE].ToString();
                    entity.HomePhoneNumber.CityCode = dr[DealerGroupTable.HOMEPHONECITYCODE].ToString();
                    entity.HomePhoneNumber.PhoneNumber = dr[DealerGroupTable.HOMEPHONENUMBER].ToString();
                    break;
             
                default:
                    throw new ArgumentOutOfRangeException();
            }

        }


        private IdentityCM GetChild(string data)
        {
            return data.ToIdentity();
        }

        private void BMCUpdateComponent(IdentityCM child, CmCommand cmd)
        {
            IBrokerManagedComponent component = base.Broker.GetCMImplementation(child.Clid, child.Csid) as IBrokerManagedComponent;
            switch (cmd)
            {
                case CmCommand.Add_Single:
                    Broker.SetWriteStart();
                    component.UpdateDataStream((int)CmCommand.AttachToParent, this.ToIdentityXml());
                    break;
                case CmCommand.Remove_Single:
                    Broker.SetWriteStart();
                    component.UpdateDataStream((int)CmCommand.DeAttachFromParent, this.ToIdentityXml());
                    break;
            }
        }

        private List<OTC.OrganizationUnit> GetOrganizationUnitNameList(List<IdentityCM> value)
        {
            IBrokerManagedComponent component = null;
            List<OTC.OrganizationUnit> entities = new List<OTC.OrganizationUnit>();
            foreach (var each in value)
            {
                component = base.Broker.GetCMImplementation(each.Clid, each.Csid) as IBrokerManagedComponent;
                entities.Add(new OTC.OrganizationUnit() { Clid = each.Clid, Csid = each.Csid, Name = component.Name });
            }
            return entities;
        }

        public void Export(XElement root)
        {
            _DealerGroupEntity.OrganizationStatus = this.OrganizationStatus;
            _DealerGroupEntity.ClientId = this.ClientId;
            root.Add(_DealerGroupEntity.ToXElement("DealerGroup"));
            root.Add(_Children.ToXElement("Children"));
        }

        public void Import(XElement root, IdentityCmMap map)
        {
            _DealerGroupEntity = root.FromXElement<DealerGroupEntity>("DealerGroup");
            map.ReplaceOrRemove(_DealerGroupEntity.BankAccounts);
            map.ReplaceOrRemove(_DealerGroupEntity.Signatories);
            map.ReplaceOrRemove(_DealerGroupEntity.Directors);
            map.ReplaceOrRemove(_DealerGroupEntity.ShareHolders);
            map.ReplaceOrRemove(_DealerGroupEntity.Partners);
            map.ReplaceOrRemove(_DealerGroupEntity.Applicants);
            map.ReplaceOrRemove(_DealerGroupEntity.CorporatePrivate);
            map.ReplaceOrRemove(_DealerGroupEntity.CorporatePublic);
            Name = _DealerGroupEntity.Name;
            OrganizationStatus = _DealerGroupEntity.OrganizationStatus;
            ClientId = _DealerGroupEntity.ClientId;

            _Children = root.FromXElement<List<IdentityCM>>("Children");
            map.ReplaceOrRemove(_Children);
        }

        #region Security

        private string GetEntitySecurity()
        {
            EntitySecurity entity = new EntitySecurity();

            PartyDS ds = new PartyDS();
            base.OnGetData(ds);
            foreach (DataRow row in ds.Tables[PartyDS.INCLUDEDPARTY_TABLE].Rows)
            {
                entity.IncludedUsers.Add(new UserEntity() { CID = new Guid(row[PartyDS.PARTYCID_FIELD].ToString()), CurrentUserName = row[PartyDS.PARTYNAME_FIELD].ToString() });
            }
            foreach (DataRow row in ds.Tables[PartyDS.AVAILABLEPARTY_TABLE].Rows)
            {
                if (row[PartyDS.PARTYNAME_FIELD].ToString().ToLower() != "Administrator, Administrator".ToLower())
                {
                    entity.AvailableUsers.Add(new UserEntity() { CID = new Guid(row[PartyDS.PARTYCID_FIELD].ToString()), CurrentUserName = row[PartyDS.PARTYNAME_FIELD].ToString() });
                }
            }

            return entity.ToXmlString();
        }

        private string DeleteEntitySecurity(string UserEntity)
        {
            UserEntity entity = UserEntity.ToData<UserEntity>();

            this.Broker.SetWriteStart();

            PartyDS ds = new PartyDS();
            base.OnGetData(ds);
            ds.AcceptChanges();

            DataRow[] drCollection = ds.Tables[PartyDS.INCLUDEDPARTY_TABLE].Select(PartyDS.PARTYCID_FIELD + " = '" + entity.CID + "'", String.Empty);
            drCollection[0].Delete();
            this.SetData(ds);

            return string.Empty;
        }

        #endregion

        #region OrganizationsChart

        public OrganizationChart GetOrganizationChart()
        {
            OrganizationChart chart = new OrganizationChart();

            chart.Clid = this.CLID;
            chart.Csid = this.CSID;
            chart.NodeName = this._DealerGroupEntity.Name;

            return chart;
        }

        public OrganizationChart GetAssociation(IIdentityCM identitycm, OrganizationType organizationtype)
        {
            OrganizationChart OChart = new OrganizationChart();
            switch (organizationtype)
            {
                case OrganizationType.BankAccount:
                    if (this._DealerGroupEntity.BankAccounts.Where(e => e.Clid == identitycm.Clid && e.Csid == identitycm.Csid).ToList().Count > 0)
                    {
                        OChart.Clid = this.Clid;
                        OChart.Csid = this.Csid;
                        OChart.NodeName = this.Name;
                    }
                    break;
                case OrganizationType.Individual:
                    OChart = GetIndividualChart(identitycm);
                    break;
                case OrganizationType.DealerGroup:
                    if (identitycm.Clid == this.Clid && identitycm.Csid == this.Csid)
                    {
                        OChart = new OrganizationChart() { Clid = this.Clid, Csid = this.Csid, NodeName = this.Name, TypeName = OrganizationType.DealerGroup.ToString() };
                        //OChart.Subordinates.Add(this._Children.ToOrganizationChart(this.Broker, "IFA"));
                        OChart.Subordinates.Add(GetEntityChild());
                        OChart.Subordinates.Add(this._DealerGroupEntity.BankAccounts.ToOrganizationChart(this.Broker, "Bank Account(s)"));
                        OChart.Subordinates.Add(this._DealerGroupEntity.Signatories.ToOrganizationChart(this.Broker, "Signatory(ies)"));
                        OChart.Subordinates.Add(this._DealerGroupEntity.Directors.ToOrganizationChart(this.Broker, "Director(s)"));
                        OChart.Subordinates.Add(this._DealerGroupEntity.ShareHolders.ToOrganizationChart(this.Broker, "ShareHolder(s)"));
                        OChart.Subordinates.Add(this._DealerGroupEntity.Partners.ToOrganizationChart(this.Broker, "Partner(s)"));
                        OChart.Subordinates.Add(this._DealerGroupEntity.Applicants.ToOrganizationChart(this.Broker, "Applicant(s)"));

                        OChart.Subordinates.Add(this._DealerGroupEntity.CorporatePrivate.ToOrganizationChart(this.Broker, "Corporate Private"));
                        OChart.Subordinates.Add(this._DealerGroupEntity.CorporatePublic.ToOrganizationChart(this.Broker, "Corporate Public"));

                        OChart.Subordinates.RemoveAll(e => e == null);
                    }
                    break;
                default:
                    break;
            }
            return OChart;
        }

        public OrganizationChart GetEntityParent(OrganizationChart organizationchart)
        {
            OrganizationChart association = null;
            association = new OrganizationChart() { Clid = this.Clid, Csid = this.Csid, NodeName = this.Name, TypeName = OrganizationType.DealerGroup.ToString() };
            if (organizationchart.Clid == this.Clid && organizationchart.Csid == this.Csid)
            {
                association = organizationchart;
            }
            else
            {
                association.Subordinates.Add(organizationchart);
            }
            return association;
        }

        public OrganizationChart GetEntityChild()
        {
            ICalculationModule module = null;
            //OrganizationChart association = new OrganizationChart() { Clid = this.Clid, Csid = this.Csid, NodeName = this.Name, TypeName = OrganizationType.DealerGroup.ToString() };
            OrganizationChart children = new OrganizationChart();
            children.NodeName = "IFA(s)";
            foreach (var each in this._Children)
            {
                module = Broker.GetCMImplementation(each.Clid, each.Csid);
                if (module != null && module is IHaveAssociation)
                {
                    children.Subordinates.Add((module as IHaveAssociation).GetEntityChildWithEntity());
                }
            }
            //association.Subordinates.Add(children);
            return children.Subordinates.Count <= 0 ? null : children;
        }

        public OrganizationChart GetEntityChildWithEntity()
        {
            ICalculationModule module = null;
            OrganizationChart association = null;
            association = new OrganizationChart() { Clid = this.Clid, Csid = this.Csid, NodeName = this.Name, TypeName = OrganizationType.DealerGroup.ToString() };
            OrganizationChart children = new OrganizationChart();
            children.NodeName = "IFA(s)";
            foreach (var each in this._Children)
            {
                module = Broker.GetCMImplementation(each.Clid, each.Csid);
                if (module != null && module is IHaveAssociation)
                {
                    children.Subordinates.Add((module as IHaveAssociation).GetEntityChildWithEntity());
                }
            }
            association.Subordinates.Add(children.Subordinates.Count <= 0 ? null : children);
            association.Subordinates.Add(this._DealerGroupEntity.BankAccounts.ToOrganizationChart(this.Broker, "Bank Account(s)"));
            association.Subordinates.Add(this._DealerGroupEntity.Signatories.ToOrganizationChart(this.Broker, "Signatory(ies)"));
            association.Subordinates.Add(this._DealerGroupEntity.Directors.ToOrganizationChart(this.Broker, "Director(s)"));
            association.Subordinates.Add(this._DealerGroupEntity.ShareHolders.ToOrganizationChart(this.Broker, "ShareHolder(s)"));
            association.Subordinates.Add(this._DealerGroupEntity.Partners.ToOrganizationChart(this.Broker, "Partner(s)"));
            association.Subordinates.Add(this._DealerGroupEntity.Applicants.ToOrganizationChart(this.Broker, "Applicant(s)"));

            association.Subordinates.Add(this._DealerGroupEntity.CorporatePrivate.ToOrganizationChart(this.Broker, "Corporate Private"));
            association.Subordinates.Add(this._DealerGroupEntity.CorporatePublic.ToOrganizationChart(this.Broker, "Corporate Public"));

            association.Subordinates.RemoveAll(e => e == null);
            return association;
        }

        private OrganizationChart GetIndividualChart(IIdentityCM identitycm)
        {
            OrganizationChart OChart = new OrganizationChart();
            OChart.Clid = this.Clid;
            OChart.Csid = this.Csid;
            OChart.NodeName = this.Name;
            if (this._DealerGroupEntity.Directors.Where(e => e.Clid == identitycm.Clid && e.Csid == identitycm.Csid).ToList().Count > 0)
            {
                OChart.Subordinates.Add(new OrganizationChart() { NodeName = "Director(s)" });
            }
            if (this._DealerGroupEntity.Partners.Where(e => e.Clid == identitycm.Clid && e.Csid == identitycm.Csid).ToList().Count > 0)
            {
                OChart.Subordinates.Add(new OrganizationChart() { NodeName = "Partner(s)" });
            }
            if (this._DealerGroupEntity.Signatories.Where(e => e.Clid == identitycm.Clid && e.Csid == identitycm.Csid).ToList().Count > 0)
            {
                OChart.Subordinates.Add(new OrganizationChart() { NodeName = "Signatory(ies)" });
            }
            if (this._DealerGroupEntity.ShareHolders.Where(e => e.Clid == identitycm.Clid && e.Csid == identitycm.Csid).ToList().Count > 0)
            {
                OChart.Subordinates.Add(new OrganizationChart() { NodeName = "ShareHolder(s)" });
            }
            if (this._DealerGroupEntity.Applicants.Where(e => e.Clid == identitycm.Clid && e.Csid == identitycm.Csid).ToList().Count > 0)
            {
                OChart.Subordinates.Add(new OrganizationChart() { NodeName = "Applicant(s)" });
            }
            OChart.Subordinates.RemoveAll(e => e == null);
            return OChart.Subordinates.Count <= 0 ? null : OChart;
        }

        #endregion

        public override void DoTagged(EntityTag tag, Guid modelId)
        {
            string appender = tag.Appender;
            _DealerGroupEntity.DoTaggedExtension<DealerGroupEntity>(tag);

            tag.SetAppender(appender);

        }
        public override void FillEntityDetailsInObjects(object target, int type)
        {
            switch (type)
            {
                case 1: // epi data response

                    var dealerGroup = (target as DealerGroup);

                    dealerGroup.AFSL = _DealerGroupEntity.AFSLNo.ReplaceEmptyWithNull();
                    var ContactDetails = new ContactDetails()
                   {

                       Addresses =
                           new ContactDetailsAddressesAddress[]
                                                                                               {
                                                                                                   new ContactDetailsAddressesAddress
                                                                                                       ()
                                                                                                       {
                                                                                                           Addressee =_DealerGroupEntity.Name.ReplaceEmptyWithNull(),
                                                                                                           Country =Country.AU,
                                                                                                         
                                                                                                            Line1 =_DealerGroupEntity.Address.BusinessAddress.Addressline1.ReplaceEmptyWithNull()??"line1",
                                                                                                           Line2 =_DealerGroupEntity.Address.BusinessAddress.Addressline2.ReplaceEmptyWithNull(),
                                                                                                          PostCode =_DealerGroupEntity.Address.BusinessAddress.PostCode.ReplaceEmptyWithNull()??"123",
                                                                                                           Preferred =true,
                                                                                                           State =_DealerGroupEntity.Address.BusinessAddress.State.ReplaceEmptyWithNull()??"NSW",
                                                                                                           Suburb =_DealerGroupEntity.Address.BusinessAddress.Suburb.ReplaceEmptyWithNull()??"subrub",
                                                                                                           Type =EPI.Data.AddressType.Business
                                                                                                       }
                                                                                               },






                       PreferredContactMethod = ContactMethod.Address

                   };



                    if (_DealerGroupEntity.WorkPhoneNumber.PhoneNumber.ReplaceEmptyWithNull() != null)
                    {
                        ContactDetails.PhoneNumbers = new ContactDetailsPhoneNumbersPhoneNumber[]
                                                           {
                                                               new ContactDetailsPhoneNumbersPhoneNumber()
                                                                   {

                                                                       AreaCode =
                                                                           _DealerGroupEntity.WorkPhoneNumber.CityCode.
                                                                           ReplaceEmptyWithNull(),
                                                                       CountryCode =
                                                                           _DealerGroupEntity.WorkPhoneNumber.
                                                                           CountryCode.ReplaceEmptyWithNull(),
                                                                       Number =
                                                                           _DealerGroupEntity.WorkPhoneNumber.
                                                                           PhoneNumber.ReplaceEmptyWithNull(),
                                                                       Preferred = false,
                                                                       Type = PhoneNumberType.Business
                                                                   }
                                                           };
                        ContactDetails.PreferredContactMethod = ContactMethod.Phone;
                    }


                    if (_DealerGroupEntity.Email.ReplaceEmptyWithNull() != null)
                    {
                        ContactDetails.EmailAddresses = new ContactDetailsEmailAddressesEmailAddress[]
                    {
                                new ContactDetailsEmailAddressesEmailAddress
                                    ()
                                    {
                                        Address = _DealerGroupEntity.Email.ReplaceEmptyWithNull(),
                                        Preferred = false,
                                        Type = EmailAddressType.Business
                                    }
                                   
                    };
                        ContactDetails.PreferredContactMethod = ContactMethod.Email;
                    }



                    dealerGroup.ContactDetails = ContactDetails;
                    //dealerGroup.FPSId = "xxxx";
                    dealerGroup.Name = _DealerGroupEntity.Name.ReplaceEmptyWithNull();
                    dealerGroup.ABN = dealerGroup.ABN.ReplaceEmptyWithNull();
                    dealerGroup.ACNorARBN = _DealerGroupEntity.ACN.ReplaceEmptyWithNull();

                    dealerGroup.Id = this.ClientId.ReplaceEmptyWithNull();






                    break;

            }



        }

        #region GetXMLData

        private string ExtractXML(string data)
        {
            string xmlco = _DealerGroupEntity.ToXmlString();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmlco);
            XMLExtensions.SetNode(_DealerGroupEntity.Applicants, "Applicants", "Applicant", doc, this.Broker);
            XMLExtensions.SetNode(_DealerGroupEntity.Signatories, "Signatories", "Signatory", doc, this.Broker);
            XMLExtensions.SetNode(_DealerGroupEntity.Directors, "Directors", "Director", doc, this.Broker);
            XMLExtensions.SetNode(_DealerGroupEntity.ShareHolders, "ShareHolders", "ShareHolder", doc, this.Broker);
            XMLExtensions.SetNode(_DealerGroupEntity.Partners, "Partners", "Partner", doc, this.Broker);
            XMLExtensions.SetNode(_DealerGroupEntity.BankAccounts, "BankAccounts", "BankAccount", doc, this.Broker);
            XmlAttribute rootAtt = doc.CreateAttribute("Type");
            rootAtt.Value = this.TypeName;
            doc.DocumentElement.Attributes.Append(rootAtt);
            string xml = doc.OuterXml;
            XDocument document = XDocument.Load(XmlReader.Create(new StringReader(xml)));
            document.Root.Name = data;
            document.Root.RemoveAttributes();
            xml = document.ToString().Trim();
            return xml;
        }

        #endregion

    }

    [Serializable]
    public class DealerGroupEntity
    {
        public String Title { get; set; }
        public String Name { get; set; }
        public String LegalName { get; set; }
        public String PreferredName { get; set; }
        public String Surname { get; set; }
        public String Gender { get; set; }

        public PhoneNumberEntity HomePhoneNumber { get; set; }
        public PhoneNumberEntity WorkPhoneNumber { get; set; }
        public PhoneNumberEntity Facsimile { get; set; }
        public MobileNumberEntity MobilePhoneNumber { get; set; }
        public Oritax.TaxSimp.Common.DualAddressEntity Address { get; set; }

        public String Email { get; set; }
        public String Occupation { get; set; }
        public String ACN { get; set; }
        public String ABN { get; set; }
        public String TFN { get; set; }
        public String AFSLNo { get; set; }
        public String AdvisoryFirmTerminology { get; set; }
        public String Websiteaddress { get; set; }
        public String FUM { get; set; }
        public String Turnover { get; set; }

        public List<Oritax.TaxSimp.Common.IdentityCMDetail> BankAccounts { get; set; }
        public List<Oritax.TaxSimp.Common.IdentityCMDetail> Signatories { get; set; }
        public List<Oritax.TaxSimp.Common.IdentityCMDetail> Directors { get; set; }
        public List<Oritax.TaxSimp.Common.IdentityCMDetail> ShareHolders { get; set; }
        public List<Oritax.TaxSimp.Common.IdentityCMDetail> Partners { get; set; }
        public List<Oritax.TaxSimp.Common.IdentityCMDetail> Applicants { get; set; }

        public List<IdentityCM> CorporatePrivate { get; set; }
        public List<IdentityCM> CorporatePublic { get; set; }

        public String OrganizationStatus { get; set; }
        public String ClientId { get; set; }
        public DealerGroupEntityType DealerGroupType { get; set; }

        public List<IdentityCM> Children { get; set; }
    }

    public enum DealerGroupEntityType
    {
        DealerGroupCompanyControl = 1,
        DealerGroupIndividualControl,
        DealerGroupPartnershipControl,
        DealerGroupTrustControl,
    }
}
