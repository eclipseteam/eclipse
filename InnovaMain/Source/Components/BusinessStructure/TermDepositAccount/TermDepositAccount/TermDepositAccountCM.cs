using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Xml;
using System.Xml.Linq;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Enum;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Utilities;
using System.Collections.Generic;

namespace Oritax.TaxSimp.CM.Entity
{
    [Serializable]
    public partial class TermDepositAccountCM : EntityCM, ISerializable, ITermDepositAccount, IOrganizationChartData, IHaveAssociation
    {
        #region Private Variable
        private Oritax.TaxSimp.Common.TermDepositAccountEntity _TermDepositAccount;
        #endregion

        #region Public Properties
        public Oritax.TaxSimp.Common.TermDepositAccountEntity TermDepositAccountEntity
        {
            get { return _TermDepositAccount; }
        }
        #endregion

        #region Constructor
        public TermDepositAccountCM() : base() { }

        protected TermDepositAccountCM(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            _TermDepositAccount = info.GetValue<Oritax.TaxSimp.Common.TermDepositAccountEntity>("Oritax.TaxSimp.CM.Entity.TermDepositAccount");
        }
        #endregion

        #region Override Method
        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValueX("Oritax.TaxSimp.CM.Entity.TermDepositAccount", _TermDepositAccount);
        }

        public override string OnUpdateDataStream(int type, string data)
        {
            string xml = string.Empty;
            switch ((CmCommand)type)
            {
                case CmCommand.AccountRelationAdd:
                    xml = AddAccountRelation(data);
                    break;
                case CmCommand.AccountRelationAddReference:
                    xml = AddAccountRelationReference(data);
                    break;
                case CmCommand.AccountRelationDelete:
                    DeleteAccountRelation(data);
                    break;
                case CmCommand.AccountRelationDeleteRefrence:
                    DeleteAccountRelationReference(data);
                    break;
                default:
                    Oritax.TaxSimp.Common.TermDepositAccountEntity entity = data.ToNewOrData<Oritax.TaxSimp.Common.TermDepositAccountEntity>();
                    this.Name = entity.Name;
                    _TermDepositAccount = entity;
                    if (ClientId == "" || ClientId == null)
                    {
                        UpDateEventLog.UpdateLog("Term Deposit Account", _TermDepositAccount.Name, type, this.Broker);
                    }
                    else
                    {
                        UpDateEventLog.UpdateLog("Term Deposit Account", _TermDepositAccount.Name + " (" + ClientId + ")", type, this.Broker);
                    }
                    return data;
            }
            return xml;
        }
        protected override void OnGetData(DataSet data)
        {
            base.OnGetData(data);
            if (data is TermDepositBrokerBasicDS)
            {
                var dataset = data as TermDepositBrokerBasicDS;
                DataRow dr= dataset.Tables[dataset.TermDepositBasicTable.TABLENAME].NewRow();

                dr[dataset.TermDepositBasicTable.STATUS] = this.OrganizationStatus;
                dr[dataset.TermDepositBasicTable.BROKERNAME] = _TermDepositAccount.Name;
                dr[dataset.TermDepositBasicTable.ACCOUNTNUMBER] = _TermDepositAccount.AccountNumber;
                dr[dataset.TermDepositBasicTable.ID] = Clid;
                dr[dataset.TermDepositBasicTable.CID] = CID;
                dr[dataset.TermDepositBasicTable.CLID] = Clid;
                dr[dataset.TermDepositBasicTable.CSID] = Csid;
                dataset.Tables[dataset.TermDepositBasicTable.TABLENAME].Rows.Add(dr);

            }
        }
        protected override ModifiedState OnSetData(DataSet data)
        {
            base.OnSetData(data);



            this.Name = _TermDepositAccount.Name;
            
            return ModifiedState.MODIFIED;
        
        }


        public override string GetDataStream(int type, string data)
        {
            string xml = string.Empty;

            UpDateEventLog.UpdateLog("Term Deposit Account", this._TermDepositAccount.Name, type, this.Broker);
            switch ((CmCommand)type)
            {
                case CmCommand.AccountRelationGetList:
                    xml = this._TermDepositAccount.LinkedAccounts.ToXmlString();
                    break;
                case CmCommand.IncludedBankAccountGetList:
                    xml = GetIncludedBankAccountsXml(_TermDepositAccount.BankAccounts);
                    break;
                case CmCommand.AccountRelationGetEntity:
                    var entity = new AccountRelation();
                    entity.AccountName = this._TermDepositAccount.Name + "-" + this._TermDepositAccount.AccountNumber;
                    entity.AccountType = this.TypeName;
                    entity.Clid = this.Clid;
                    entity.Csid = this.Csid;

                    xml = entity.ToXmlString();
                    break;
                case CmCommand.XMLData:
                    xml = ExtractXML(data);
                    break;
                case CmCommand.CSVData:
                    xml = GetCSVObject();
                    break;
                default:
                    xml = this._TermDepositAccount.ToXmlString();
                    if (ClientId == "" || ClientId == null)
                    {
                        UpDateEventLog.UpdateLog("Term Deposit Account", this._TermDepositAccount.Name, type, this.Broker);
                    }
                    else
                    {
                        UpDateEventLog.UpdateLog("Term Deposit Account", this._TermDepositAccount.Name + " (" + ClientId + ")", type, this.Broker);
                    }
                    break;
            }
            return xml;
        }

        public override string GetAllDataStream(int type)
        {
            return this._TermDepositAccount.ToXmlString();
        }
        #endregion

        #region GetXMLData

        public override string GetXmlData()
        {
            string xmlco = _TermDepositAccount.ToXmlString();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmlco);
            XmlAttribute rootAtt = doc.CreateAttribute("Type");
            rootAtt.Value = this.TypeName;
            doc.DocumentElement.Attributes.Append(rootAtt);
            return doc.OuterXml;
        }

        private string ExtractXML(string data)
        {
            string xml = string.Empty;
            XDocument document = XDocument.Load(XmlReader.Create(new StringReader(this._TermDepositAccount.ToXmlString())));
            document.Root.Name = data;
            document.Root.RemoveAttributes();
            xml = document.ToString().Trim();
            return xml;
        }

        #endregion

        #region GetCSVData
        private string GetCSVObject()
        {
            return _TermDepositAccount.ToXmlString();
        }
        #endregion

        #region ToXElement
        public override XElement ToXElement()
        {
            return _TermDepositAccount.ToXElement("TermDepositAccount");
        }
        #endregion

        #region DoTagged
        public override void DoTagged(EntityTag tag,Guid modelId)
        {
           
            string appender = tag.Appender;
            _TermDepositAccount.DoTaggedExtension<Oritax.TaxSimp.Common.TermDepositAccountEntity>(tag);




            _TermDepositAccount.DoTaggedExtension<Oritax.TaxSimp.Common.TermDepositAccountEntity>(tag.SetAppender(appender + ".TermDepositAccount".ToUpper()));
            tag.SetAppender(appender);


        }
        #endregion

        #region OrganizationsChart

        public OrganizationChart GetOrganizationChart()
        {
            OrganizationChart chart = new OrganizationChart();

            chart.Clid = this.CLID;
            chart.Csid = this.CSID;
            chart.NodeName = this._TermDepositAccount.Name;

            return chart;
        }

        public OrganizationChart GetAssociation(IIdentityCM identitycm, OrganizationType organizationtype)
        {
            OrganizationChart OChart = new OrganizationChart();
            return OChart;
        }

        public OrganizationChart GetEntityParent(OrganizationChart organizationchart)
        {
            return null;
        }

        public OrganizationChart GetEntityChild()
        {
            return null;
        }

        public OrganizationChart GetEntityChildWithEntity()
        {
            return null;
        }

        #endregion

        #region AccountRelationList

        public string AddAccountRelation(string data)
        {
            AccountRelation newitem = data.ToNewOrData<AccountRelation>();
            if (this._TermDepositAccount.LinkedAccounts.Exists(p => p == newitem)) return "false";

            this._TermDepositAccount.LinkedAccounts.Add(newitem);
            IBrokerManagedComponent component = Broker.GetCMImplementation(newitem.Clid, newitem.Csid) as IBrokerManagedComponent;
            component.UpdateDataStream((int)CmCommand.AccountRelationAddReference, (new AccountRelation() { Clid = this.Clid, Csid = this.Csid, AccountType = newitem.AccountType, ServiceType = newitem.ServiceType }).ToXmlString());

            return "true";
        }
        public string AddAccountRelationReference(string data)
        {
            AccountRelation newitem = data.ToNewOrData<AccountRelation>();
            if (this._TermDepositAccount.LinkedAccounts.Exists(p => p == newitem)) return "false";
            this._TermDepositAccount.LinkedAccounts.Add(newitem);
            return "true";
        }

        public string DeleteAccountRelation(string data)
        {
            AccountRelation newitem = data.ToNewOrData<AccountRelation>();

            var list = this._TermDepositAccount.LinkedAccounts.Where(e => e.Clid == newitem.Clid && e.Csid == newitem.Csid).ToList();
            foreach (var each in list)
            {
                this._TermDepositAccount.LinkedAccounts.Remove(each);
            }

            IBrokerManagedComponent component = Broker.GetCMImplementation(newitem.Clid, newitem.Csid) as IBrokerManagedComponent;
            component.UpdateDataStream((int)CmCommand.AccountRelationDeleteRefrence, (new AccountRelation() { Clid = this.Clid, Csid = this.Csid, AccountType = newitem.AccountType }).ToXmlString());

            return "true";
        }
        public string DeleteAccountRelationReference(string data)
        {
            AccountRelation newitem = data.ToNewOrData<AccountRelation>();
            var list = this._TermDepositAccount.LinkedAccounts.Where(e => e.Clid == newitem.Clid && e.Csid == newitem.Csid).ToList();
            foreach (var each in list)
            {
                this._TermDepositAccount.LinkedAccounts.Remove(each);
            }
            return "true";
        }

        #endregion

        #region Included Bank Accounts List

        public string GetIncludedBankAccountsXml(List<Oritax.TaxSimp.Common.IdentityCMDetail> includedBanks)
        {

            SerializableDictionary<Oritax.TaxSimp.Common.IdentityCMDetail, Oritax.TaxSimp.Common.BankAccountEntity> banks = new SerializableDictionary<Oritax.TaxSimp.Common.IdentityCMDetail, Oritax.TaxSimp.Common.BankAccountEntity>();
           foreach (var bank in includedBanks)
            {
                 IBrokerManagedComponent component = Broker.GetCMImplementation(bank.Clid, bank.Csid) as IBrokerManagedComponent;
               var id=new Oritax.TaxSimp.Common.IdentityCMDetail { Clid = bank.Clid, Csid = bank.Csid };
               if(component!=null&& !banks.ContainsKey(id))
                   banks.Add(id, component.GetAllDataStream(0).ToData<Oritax.TaxSimp.Common.BankAccountEntity>());
            }
           return banks.ToXmlString();
        }
       

        #endregion
    }
}
