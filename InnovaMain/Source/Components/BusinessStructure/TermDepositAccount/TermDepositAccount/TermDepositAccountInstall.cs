using System;

namespace Oritax.TaxSimp.CM.Entity
{
    public class TermDepositAccountInstall
	{
		#region INSTALLATION PROPERTIES
        
		// Assembly Installation Properties
        public const string ASSEMBLY_ID = "C55FC882-0480-49EF-B844-71B77E454357";
        public const string ASSEMBLY_NAME = "TermDepositAccount";
        public const string ASSEMBLY_DISPLAYNAME = "Term Deposit Account";
		public const string ASSEMBLY_MAJORVERSION="1";
		public const string ASSEMBLY_MINORVERSION="0";
		public const string ASSEMBLY_DATAFORMAT="0";
		public const string ASSEMBLY_REVISION="0";

        // Component Installation Properties
        public const string COMPONENT_ID = "3368682F-5E47-4263-AB02-0BC67AD61F34";
        public const string COMPONENT_NAME = "TermDepositAccount";
        public const string COMPONENT_DISPLAYNAME = "Term Deposit Account";
		public const string COMPONENT_CATEGORY="BusinessEntity";

		// Component Version Installation Properties
		public const string COMPONENTVERSION_STARTDATE="01/01/1990 00:00:00 AM";
		public const string COMPONENTVERSION_ENDDATE="31/12/2050 12:59:59 AM";
		public const string COMPONENTVERSION_PERSISTERASSEMBLY="CommonPersisters_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";
		public const string COMPONENTVERSION_PERSISTERCLASS="Oritax.TaxSimp.CommonPersistence.BlobPersister";
		public const string COMPONENTVERSION_IMPLEMENTATIONCLASS="Oritax.TaxSimp.CM.Entity.TermDepositAccountCM";
        #endregion

	}
}
