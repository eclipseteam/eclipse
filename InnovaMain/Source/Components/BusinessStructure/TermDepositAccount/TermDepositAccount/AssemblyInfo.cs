﻿using System.Reflection;

using Oritax.TaxSimp.InstallInfo;
using Oritax.TaxSimp.CM.Entity;

[assembly: AssemblyTitle("")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("")]
[assembly: AssemblyCopyright("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: AssemblyVersion(TermDepositAccountInstall.ASSEMBLY_MAJORVERSION + "." + TermDepositAccountInstall.ASSEMBLY_MINORVERSION)]
[assembly: AssemblyInformationalVersion(TermDepositAccountInstall.ASSEMBLY_MAJORVERSION + "." + TermDepositAccountInstall.ASSEMBLY_MINORVERSION + "." + TermDepositAccountInstall.ASSEMBLY_DATAFORMAT + "." + TermDepositAccountInstall.ASSEMBLY_REVISION)]
[assembly: AssemblyInstallInfo(TermDepositAccountInstall.ASSEMBLY_ID, TermDepositAccountInstall.ASSEMBLY_NAME, TermDepositAccountInstall.ASSEMBLY_DISPLAYNAME)]
[assembly: ComponentInstallInfo(TermDepositAccountInstall.COMPONENT_ID, TermDepositAccountInstall.COMPONENT_NAME, TermDepositAccountInstall.COMPONENT_DISPLAYNAME, TermDepositAccountInstall.COMPONENT_CATEGORY)]
[assembly: ComponentVersionInstallInfo(TermDepositAccountInstall.COMPONENTVERSION_STARTDATE, TermDepositAccountInstall.COMPONENTVERSION_ENDDATE, TermDepositAccountInstall.COMPONENTVERSION_PERSISTERASSEMBLY, TermDepositAccountInstall.COMPONENTVERSION_PERSISTERCLASS, TermDepositAccountInstall.COMPONENTVERSION_IMPLEMENTATIONCLASS)]

[assembly: AssemblyDelaySign(false)]
