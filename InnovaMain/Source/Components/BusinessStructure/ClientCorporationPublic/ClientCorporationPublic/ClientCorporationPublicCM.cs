using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Xml.XPath;
using System.Globalization;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using Aspose.Words;
using Aspose.Words.Tables;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.Entity;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Enum;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.IOrganizationUnits;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Commands;
using IdentityCMDetail = Oritax.TaxSimp.Common.IdentityCMDetail;

namespace Oritax.TaxSimp.CM.Group
{
    [Serializable]
    public sealed class ClientCorporationPublicCM : GroupCM, ISerializable, IIdentityCM, ICmHasParent, ICmHasClient, IIdentitySMSFCorporateTrustee, ICsvData, IHaveAssociation, IMigrationHelper<CorporateEntity>, ISupportMigration, IHaveAccountProcess, IDesktopBrokerData
    {
        #region PROPERTIES
        public IdentityCM Parent { get; set; }
        public bool IsClient { get; set; }
        public IdentityCM IdentitySMSFCorporateTrustee { get; set; }
        #endregion
        public override IdentityCM ConfiguredParent
        {
            get
            {
                return this.Parent;
            }
            set
            {
                this.Parent = value;
            }
        }
        #region PRIVATE VARIABLES
        private CorporateEntity _entity;
        #endregion

        #region CONSTRUCTOR
        public ClientCorporationPublicCM()
            : base()
        {
            Aspose.Words.License license = new Aspose.Words.License();
            license.SetLicense("Aspose.Words.lic");
            Parent = IdentityCM.Null;
            IsClient = false;
            IdentitySMSFCorporateTrustee = IdentityCM.Null;
            _entity = new CorporateEntity();
        }

        public ClientCorporationPublicCM(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            Parent = info.GetValue<IdentityCM>("Oritax.TaxSimp.CM.Group.ClientCorporationPublicCM.Parent");
            IsClient = info.GetBoolean("Oritax.TaxSimp.CM.Group.ClientCorporationPublicCM.IsClient");
            IdentitySMSFCorporateTrustee = info.GetValue<IdentityCM>("Oritax.TaxSimp.CM.Group.ClientCorporationPublicCM.IdentitySMSFCorporateTrustee");
            _entity = info.GetValue<CorporateEntity>("Oritax.TaxSimp.CM.Group.ClientCorporationPublicCM.Corporate");
        }

        #endregion

        #region OVERRIDE METHODS
        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValueX("Oritax.TaxSimp.CM.Group.ClientCorporationPublicCM.Parent", Parent);
            info.AddValueX("Oritax.TaxSimp.CM.Group.ClientCorporationPublicCM.Corporate", _entity);
            info.AddValue("Oritax.TaxSimp.CM.Group.ClientCorporationPublicCM.IsClient", IsClient);
            info.AddValueX("Oritax.TaxSimp.CM.Group.ClientCorporationPublicCM.IdentitySMSFCorporateTrustee", IdentitySMSFCorporateTrustee);
        }

        public override string GetDataStream(int type, string data)
        {
            string xml = string.Empty;
            switch ((CmCommand)type)
            {
                case CmCommand.GetAdminAccontRelation:
                    List<AdminSectionEntity> returnvalue = null;//new List<AdminSectionEntity>();
                    returnvalue = _entity.LinkedAccounts;
                    xml = returnvalue.ToXmlString();
                    break;
                case CmCommand.GetEntitySecurity:
                    xml = GetEntitySecurity();
                    break;
                case CmCommand.XMLData:
                    xml = ExtractXML(data);
                    break;
                case CmCommand.AccountProcessFundCodeProductID:
                    xml = ClientUtilities.GetProductFromAccountProcess(Entity.ListAccountProcess, data, Broker);
                    break;
                case CmCommand.AccountProcessInvestmentCodeProductID:
                    {
                        List<string> list = data.ToNewOrData<List<string>>();
                        xml = ClientUtilities.GetProductFromAccountProcessDesktop(Entity.ListAccountProcess, list[0], list[1], Broker);
                    }
                    break;
                case CmCommand.CheckASXInvestmentCode:
                    {
                        List<string> list = data.ToNewOrData<List<string>>();
                        xml = ClientUtilities.GetProductFromAccountProcessDesktopForAllServiceTypes(Entity.ListAccountProcess, list[0], list[1], Broker);
                    }
                    break;
                default:
                    _entity.IsActive = this.IsInvestableClient;
                    _entity.IsClient = _entity.IsActive;
                    _entity.OrganizationStatus = this.OrganizationStatus;
                    _entity.ClientId = this.ClientId;
                    SetServiceTypeFromCM(_entity);
                    SetExemptionCategoryFromCM(_entity);
                    SetOperationMannerFromCM(_entity);
                    SetAccessFacilitiesFromCM(_entity);
                    _entity.BTWRAPCode = _entity.BTWRAPCode = (this.Entity.BTWrap == null) ? string.Empty : this.Entity.BTWrap.AccountDescription;
                    xml = _entity.ToXmlString();
                    break;
            }
            return xml;
        }

        public override string OnUpdateDataStream(int type, string data)
        {
            string xml = string.Empty;

            xml = (this as IOrganizationUnit).OnUpdateClient(type, data);
            if (!string.IsNullOrEmpty(xml)) return xml;

            IsClient = true;
            switch ((CmCommand)type)
            {
                case CmCommand.AttachToParent:
                    ClientUtilities.SetParentChild(data, Parent, this.Broker, CLID, CSID, CID);
                    break;
                case CmCommand.DeAttachFromParent:
                    ClientUtilities.RemoveParentChild(data, Parent, this.Broker, CLID, CSID, CID);
                    break;
                case CmCommand.AddEntitySecurity:
                    xml = AddEntitySecurity(data);
                    break;
                case CmCommand.DeleteEntitySecurity:
                    xml = DeleteEntitySecurity(data);
                    break;
                case CmCommand.UpdateClientID:// for the purpose of updating client id's
                    break;
                case CmCommand.UpdateApplicationID:// for the purpose of updating Application id's
                    break;
                case CmCommand.MarkExported:// for the purpose of marking exported.
                    if (data != null && !string.IsNullOrEmpty(data))
                    {
                        bool tomark = data.ToLower() == "false" ? false : true;
                        if (tomark)
                        {
                            if (!IsExported)
                                this.ApplicationDate = DateTime.Now.ToShortDateString();

                            this.IsExported = tomark;

                            if (string.IsNullOrEmpty(this.ApplicationID))
                                this.ApplicationID = "BW" + UniqueID.Return9UniqueID();

                        }
                        else
                        {
                            this.IsExported = tomark;
                            this.ApplicationDate = "";
                        }
                    }
                    break;
                case CmCommand.MarkExportedSS:// for the purpose of marking exported.
                    if (data != null && !string.IsNullOrEmpty(data))
                    {
                        bool tomark = data.ToLower() == "false" ? false : true;
                        if (tomark)
                        {
                            this.IsExportedSS = tomark;
                        }
                        else
                        {
                            this.IsExportedSS = tomark;
                        }
                    }
                    break;
                case CmCommand.MarkExportedDB:// for the purpose of marking exported.
                    if (data != null && !string.IsNullOrEmpty(data))
                    {
                        bool tomark = data.ToLower() == "false" ? false : true;
                        if (tomark)
                        {
                            this.IsExportedDB = tomark;
                        }
                        else
                        {
                            this.IsExportedDB = tomark;
                        }
                    }
                    break;
                case CmCommand.SPAddLibrary:
                    var _value = data.ToNewOrData<Oritax.TaxSimp.Common.SPLibraryDocument>();
                    if (_value != null)
                    {
                        SPLibrary.AddLibraryToClient(_value.CurrentUser.CurrentUserName, _value.Name);
                    }
                    break;
                default:
                    _entity = data.ToNewOrData<CorporateEntity>();
                    _entity.Clid = CLID;
                    _entity.Csid = CSID;
                    this.IsInvestableClient = _entity.IsActive;
                    _entity.IsClient = _entity.IsActive;
                    this.OrganizationStatus = _entity.OrganizationStatus;
                    this.Name = _entity.Name;
                    SetServiceTypeToCM(_entity);
                    SetExemptionCategoryToCM(_entity);
                    SetOperationMannerToCM(_entity);
                    UpdateModels(_entity);
                    SetAccessFacilitiesToCM(_entity);
                    UpDateEventLog.UpdateLog("Client Corporation Public", _entity.Name + " (" + EclipseClientID + ")", type, this.Broker);
                    return _entity.ToXmlString();
            }
            return xml;
        }

        #region Tagging
        public override void DoTaggedDoc(Aspose.Words.Document document, Guid modelID)
        {

            document.Range.Replace("[ECLIPSE_CLIENT_NAME]", this.Entity.Name, false, false);
            document.Range.Replace("[ECLIPSE_ ADDRESS]", this.Entity.Address.BusinessAddress.GetFullAddress(), false, false);

            List<Oritax.TaxSimp.Common.IndividualEntity> individualEntityList = new CSVInstance(this.Broker).GetConcreteIndividualEntityObject(_entity.Signatories);

            for (int count = 0; count < individualEntityList.Count; count++)
                document.Range.Replace("[ECLIPSE_SIG_" + (count + 1).ToString() + "_NAME]", individualEntityList[count].PersonalTitle.Title.ToUpper() + " " + individualEntityList[count].Fullname, false, false);

            if (individualEntityList.Count > 0)
                document.Range.Replace("[ECLIPSE_CLIENT_EMAIL]", individualEntityList[0].EmailAddress, false, false);

            if (individualEntityList.Count < 2)
                document.Range.Replace("[ECLIPSE_SIG_2_NAME]", "", false, false);

            if (individualEntityList.Count < 1)
                document.Range.Replace("[ECLIPSE_SIG_1_NAME]", "", false, false);

            if (this.Entity.Servicetype.DoItForMe.WholesaleInvestor)
                document.Range.Replace("[ECLIPSE_ISWHOLESALE]", "Wholesale", false, false);

            if (this.Entity.Servicetype.DoItForMe.RetailInvestor)
                document.Range.Replace("[ECLIPSE_ISWHOLESALE]", "Retail", false, false);

            ICalculationModule objOrganisation = Broker.GetWellKnownCM(WellKnownCM.Organization);
            IModelStructure organization = (objOrganisation as IModelStructure);

            foreach (ModelEntity modelEntity in organization.Model)
            {
                if (modelEntity.ID == modelID)
                {
                    document.Range.Replace("[ECLIPSE_PROGRAM]", modelEntity.Name, false, false);
                    document.Range.Replace("[ECLIPSE_PROGRAM_CODE]", "Model#" + modelEntity.ProgramCode, false, false);

                    DocumentBuilder builder = new DocumentBuilder(document);
                    builder.MoveToBookmark("ECLIPSE_MODEL", false, true);

                    builder.StartTable();

                    // Insert a cell
                    builder.InsertCell();
                    builder.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
                    builder.CellFormat.LeftPadding = 0;
                    builder.CellFormat.Orientation = TextOrientation.Horizontal;
                    builder.Bold = true;
                    builder.Write("Asset Class");

                    builder.InsertCell();
                    builder.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
                    builder.CellFormat.LeftPadding = 0;
                    builder.CellFormat.Orientation = TextOrientation.Horizontal;
                    builder.Bold = true;
                    builder.Write("Minimum");

                    builder.InsertCell();
                    builder.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
                    builder.CellFormat.LeftPadding = 0;
                    builder.CellFormat.Orientation = TextOrientation.Horizontal;
                    builder.Bold = true;
                    builder.Write("Target Weight");

                    builder.InsertCell();
                    builder.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
                    builder.CellFormat.LeftPadding = 0;
                    builder.CellFormat.Orientation = TextOrientation.Horizontal;
                    builder.Bold = true;
                    builder.Write("Maximum");

                    builder.EndRow();

                    foreach (AssetEntity assetEntity in modelEntity.Assets)
                    {
                        double totalMinAllocation = 0;
                        double totalMaxAllocation = 0;
                        foreach (ProductEntity productEntity in assetEntity.Products)
                        {
                            totalMinAllocation += productEntity.MinAllocation;
                            totalMaxAllocation += productEntity.MaxAllocation;
                        }
                        // Insert a cell
                        builder.InsertCell();
                        builder.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
                        builder.Bold = true;
                        builder.Write(assetEntity.Description);

                        builder.InsertCell();
                        builder.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
                        builder.Write(totalMinAllocation.ToString() + "%");

                        builder.InsertCell();
                        builder.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
                        builder.Write(assetEntity.SharePercentage.ToString() + "%");

                        builder.InsertCell();
                        builder.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
                        builder.Write(assetEntity.DynamicPercentage.ToString() + "%");

                        builder.InsertCell();
                        builder.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
                        builder.Write(totalMaxAllocation.ToString() + "%");

                        builder.EndRow();
                    }

                    // Insert a cell
                    builder.InsertCell();
                    builder.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
                    builder.Bold = true;

                    builder.InsertCell();
                    builder.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;

                    builder.InsertCell();
                    builder.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
                    builder.Bold = true;
                    builder.Write("100%");

                    builder.EndRow();

                    Table table = builder.EndTable();
                }
            }

            if (Parent != null && Parent.Clid != Guid.Empty && Parent.Csid != Guid.Empty)
            {
                OrganizationUnitCM adviserUnit = Broker.GetCMImplementation(Parent.Clid, Parent.Csid) as OrganizationUnitCM;
                document.Range.Replace("[ECLIPSE_ADVISER_NAME]", adviserUnit.FullName(), false, false);
            }
            else
                document.Range.Replace("[ECLIPSE_ADVISER_NAME]", "", false, false);

            Broker.ReleaseBrokerManagedComponent(organization);
        }

        public override bool HasAdviser
        {
            get
            {
                if (this.IsInvestableClient)
                {
                    if (this.Parent != null)
                        if (this.Parent.Clid != Guid.Empty)
                            return true;
                        else
                            return false;
                    else
                        return false;
                }
                else
                    return false;
            }
        }


        public override void DoTagged(EntityTag tag, Guid modelId)
        {
            string appender = tag.Appender;
            _entity.DoTaggedExtension<CorporateEntity>(tag.SetAppenderExt(appender, ""));
            string applicationIdKey = (string.IsNullOrEmpty(appender)) ?
         "ApplicationID" : (appender + ".ApplicationID");

            tag.AddSingleKeyValueExtension(applicationIdKey, this.ApplicationID);
            if (this.Parent != null & this.Parent.Clid != Guid.Empty & this.Parent.Clid != Guid.Empty)
            {
                var orgnizationunitcm = this.Parent.ToOrganizationUnit(Broker);
                if (orgnizationunitcm != null)
                    orgnizationunitcm.DoTagged(tag.SetAppenderExt(appender, "Adviser"), modelId);

            }
            if (_entity.ListAccountProcess != null && modelId != new Guid())
            {
                foreach (var each in _entity.ListAccountProcess)
                {
                    if (modelId == each.ModelID)
                    {

                        var organizationunitcm =
                            each.LinkedEntity.ToOrganizationUnit(Broker);
                        if (organizationunitcm != null)
                            organizationunitcm.DoTagged(tag.SetAppenderExt(appender, "ServiceType"), modelId);
                    }

                }
            }
            #region Address

            _entity.Address.BusinessAddress.DoTaggedExtension<Oritax.TaxSimp.Common.AddressEntity>(tag.SetAppenderExt(appender, "BusinessAddress"));

            if (!_entity.Address.RegisteredAddressSame)
                _entity.Address.RegisteredAddress.DoTaggedExtension<Oritax.TaxSimp.Common.AddressEntity>(tag.SetAppenderExt(appender, "RegisteredAddress"));

            _entity.Address.ResidentialAddress.DoTaggedExtension<Oritax.TaxSimp.Common.AddressEntity>(tag.SetAppenderExt(appender, "ResidentialAddress"));


            if (!_entity.Address.MailingAddressSame)
                _entity.Address.MailingAddress.DoTaggedExtension<Oritax.TaxSimp.Common.AddressEntity>(tag.SetAppenderExt(appender, "MailingAddress"));

            if (!_entity.Address.DuplicateMailingAddressSame)
                _entity.Address.DuplicateMailingAddress.DoTaggedExtension<Oritax.TaxSimp.Common.AddressEntity>(tag.SetAppenderExt(appender, "DuplicateMailingAddress"));

            _entity.Address.DoTaggedExtension<Oritax.TaxSimp.Common.DualAddressEntity>(tag.SetAppenderExt(appender, ""));

            #endregion
            var contacts = from obj in _entity.Contacts
                           orderby obj.IsPrimary descending
                           select obj;


            contacts.ListTagged(Broker, tag.SetAppenderExt(appender, "Contact"), modelId);
            _entity.Contacts.ListTagged(Broker, tag.SetAppenderExt(appender, "Applicant"), modelId);
            _entity.Signatories.ListTagged(Broker, tag.SetAppenderExt(appender, "Signatory"), modelId);
            _entity.BankAccounts.ListTagged(Broker, tag.SetAppenderExt(appender, "BankAccount"), modelId);

            #region Other Services

            _entity.AccessFacilities.DoTaggedExtension<Oritax.TaxSimp.Common.AccessFacilities>(tag.SetAppenderExt(appender, "AccessFacilities"));
            _entity.ExemptionCategory.DoTaggedExtension<Oritax.TaxSimp.Common.ExemptionCategory>(tag.SetAppenderExt(appender, "ExemptionCategory"));
            _entity.OperationManner.DoTaggedExtension<Oritax.TaxSimp.Common.OperationManner>(tag.SetAppenderExt(appender, "OperationManner"));

            #endregion

            tag.SetAppender(appender);
        }
        public override List<IDictionary<string, string>> DoPartialTag(EntityTag tag)
        {
            List<IDictionary<string, string>> _documents = new List<IDictionary<string, string>>();



            string appender = tag.Appender;
            // _entity.DoTagged<CorporateEntity>(tag);

            #region Advisor



            #endregion
            List<IDictionary<string, string>> listAdvisor = new List<IDictionary<string, string>>();
            if (this.Parent != null)
            {

                listAdvisor = this.Parent.IIdentityCMPartialTags(Broker, tag.SetAppender("Adviser"));

            }

            foreach (var docs in _entity.Signatories.ListPartialTags(Broker, tag.SetAppender("Individual")))
            {
                foreach (var eachdic in listAdvisor)
                {
                    foreach (var dic in eachdic)
                    {
                        docs.Add(dic.Key, dic.Value);
                    }

                }

                _documents.Add(docs);
            }



            #region Other Services



            #endregion

            tag.SetAppender(appender);


            return _documents;
        }
        #endregion

        #endregion

        #region OTHER SERVICES
        #region TO CM
        public void SetServiceTypeToCM(CorporateEntity entity)
        {
            this.ServiceType.DO_IT_FOR_ME = entity.Servicetype.DO_IT_FOR_ME;
            this.ServiceType.DO_IT_WITH_ME = entity.Servicetype.DO_IT_WITH_ME;
            this.ServiceType.DO_IT_YOURSELF = entity.Servicetype.DO_IT_YOURSELF;
        }

        public void SetExemptionCategoryToCM(CorporateEntity entity)
        {
            this.ExemptionCategory.Category1 = entity.ExemptionCategory.Category1;
            this.ExemptionCategory.Category2 = entity.ExemptionCategory.Category2;
            this.ExemptionCategory.Category3 = entity.ExemptionCategory.Category3;
        }

        public void SetOperationMannerToCM(CorporateEntity entity)
        {
            this.OperationManner.ALL_OF_US_TO_SIGN = entity.OperationManner.ALL_OF_US_TO_SIGN;
            this.OperationManner.ANY_ONE_OF_US_TO_SIGN = entity.OperationManner.ANY_ONE_OF_US_TO_SIGN;
            this.OperationManner.ANY_TWO_OF_US_TO_SIGN = entity.OperationManner.ANY_TWO_OF_US_TO_SIGN;
        }

        public void SetAccessFacilitiesToCM(CorporateEntity entity)
        {
            this.AccessFacilities.PHONEACCESS = entity.AccessFacilities.PHONEACCESS;
            this.AccessFacilities.ONLINEACCESS = entity.AccessFacilities.ONLINEACCESS;
            this.AccessFacilities.DEPOSITBOOK = entity.AccessFacilities.DEPOSITBOOK;
            this.AccessFacilities.DEBITCARD = entity.AccessFacilities.DEBITCARD;
            this.AccessFacilities.CHEQUEBOOK = entity.AccessFacilities.CHEQUEBOOK;
        }
        #endregion

        #region FROM CM
        public void SetServiceTypeFromCM(CorporateEntity entity)
        {
            entity.Servicetype.DO_IT_FOR_ME = this.ServiceType.DO_IT_FOR_ME;
            entity.Servicetype.DO_IT_WITH_ME = this.ServiceType.DO_IT_WITH_ME;
            entity.Servicetype.DO_IT_YOURSELF = this.ServiceType.DO_IT_YOURSELF;
        }

        public void SetExemptionCategoryFromCM(CorporateEntity entity)
        {
            entity.ExemptionCategory.Category1 = this.ExemptionCategory.Category1;
            entity.ExemptionCategory.Category2 = this.ExemptionCategory.Category2;
            entity.ExemptionCategory.Category3 = this.ExemptionCategory.Category3;
        }

        public void SetOperationMannerFromCM(CorporateEntity entity)
        {
            entity.OperationManner.ALL_OF_US_TO_SIGN = this.OperationManner.ALL_OF_US_TO_SIGN;
            entity.OperationManner.ANY_ONE_OF_US_TO_SIGN = this.OperationManner.ANY_ONE_OF_US_TO_SIGN;
            entity.OperationManner.ANY_TWO_OF_US_TO_SIGN = this.OperationManner.ANY_TWO_OF_US_TO_SIGN;
        }

        public void SetAccessFacilitiesFromCM(CorporateEntity entity)
        {
            entity.AccessFacilities.PHONEACCESS = this.AccessFacilities.PHONEACCESS;
            entity.AccessFacilities.ONLINEACCESS = this.AccessFacilities.ONLINEACCESS;
            entity.AccessFacilities.DEPOSITBOOK = this.AccessFacilities.DEPOSITBOOK;
            entity.AccessFacilities.DEBITCARD = this.AccessFacilities.DEBITCARD;
            entity.AccessFacilities.CHEQUEBOOK = this.AccessFacilities.CHEQUEBOOK;
        }
        #endregion
        #endregion

        #region PRIVATE METHODS
        private IdentityCM GetParent(string data)
        {
            return data.ToIdentity();
        }

        public bool UpdateModels(CorporateEntity entity)
        {
            var models = new List<string>();
            models.Add(entity.Servicetype.DoItForMe.ProgramCode);
            models.Add(entity.Servicetype.DoItWithMe.ProgramCode);
            models.Add(entity.Servicetype.DoItYourSelf.ProgramCode);
            return this.AssociateModel(models);
        }

        #endregion

        #region GetXMLData

        public override string GetXmlData()
        {
            this.GetDataStream(-100, string.Empty);
            string xmlco = _entity.ToXmlString();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmlco);

            XMLExtensions.SetNode(_entity.Contacts, "Contacts", "Contact", doc, this.Broker, this.ClientId);
            XMLExtensions.SetNode(_entity.Signatories, "Signatories", "Signatory", doc, this.Broker, this.ClientId);
            XMLExtensions.SetNode(_entity.BankAccounts, "BankAccounts", "BankAccount", doc, this.Broker);
            XMLExtensions.SetParentNode(this.Parent, "Adviser", doc, this.Broker);
            this.SetAdviserIDToXMlDATA(doc, this.Parent.Clid, Parent.Csid, this.IsClient);
            IdentityCM BusinessGroupIdentityCM = XMLExtensions.GetParentIDs(this.Parent, this.Broker);
            XMLExtensions.SetParentNode(BusinessGroupIdentityCM, "BusinessGroup", doc, this.Broker);
            IdentityCM InvestorGroupIdentityCM = XMLExtensions.GetParentIDs(BusinessGroupIdentityCM, this.Broker);
            XMLExtensions.SetParentNode(InvestorGroupIdentityCM, "InvestorGroup", doc, this.Broker);

            XMLExtensions.AddNode("InvestorID", this.ClientId, doc);
            XMLExtensions.AddNode("InvestorName", this._entity.Name, doc);
            XMLExtensions.AddNode("AuthorisationType", "TBD", doc);
            XMLExtensions.AddNode("BankwestAcc", this._entity.IsActive.ToString(), doc);
            XMLExtensions.AddNode("BellDirectAcc", this._entity.IsActive.ToString(), doc);
            XMLExtensions.AddNode("TermDepositAcc", this._entity.IsActive.ToString(), doc);
            XMLExtensions.AddNode("AccountStatus", this._entity.IsActive.ToString(), doc);

            if (this._entity.LinkedAccounts != null)
            {
                AddAdminSectionXML(doc);
            }

            XMLExtensions.RemoveNode("Clid", doc);
            XMLExtensions.RemoveNode("Csid", doc);
            XMLExtensions.RemoveNode("CorporatePrivate", doc);
            XMLExtensions.RemoveNode("CorporatePublic", doc);
            XMLExtensions.RemoveNode("IsActive", doc);
            XMLExtensions.RemoveNode("Parent", doc);
            XMLExtensions.RemoveNode("ClientId", doc);

            XMLExtensions.RemoveNode("TFN", doc);
            XMLExtensions.RemoveNode("TIN", doc);

            XMLExtensions.RemoveNodeByTagName("RegisteredAddressSame", doc);
            XMLExtensions.RemoveNodeByTagName("MailingAddressSame", doc);
            XMLExtensions.RemoveNodeByTagName("DuplicateMailingAddressSame", doc);

            XmlAttribute rootAtt = doc.CreateAttribute("Type");
            rootAtt.Value = this.TypeName;
            doc.DocumentElement.Attributes.Append(rootAtt);

            if (doc.DocumentElement.SelectSingleNode("ManagedInvestmentSchemesAccounts") != null)
            {
                //-----MIS CHANGES----//
                doc.DocumentElement.SelectSingleNode("ManagedInvestmentSchemesAccounts").RemoveAll();

                string misXML = string.Empty;
                XmlDocument misDoc = new XmlDocument();

                if (this.Entity != null && this.Entity.ManagedInvestmentSchemesAccounts != null)
                {
                    foreach (var fund in this.Entity.ManagedInvestmentSchemesAccounts)
                    {
                        if (fund.FundID != null)
                        {
                            var allFundsinMIS = Broker.GetCMImplementation(fund.Clid, fund.Csid);

                            if (allFundsinMIS != null)
                            {
                                var mis = allFundsinMIS.GetDataStream(-999, null).ToNewOrData<ManagedInvestmentSchemesAccountEntity>();
                                misXML = mis.ToXmlString();
                                misDoc.LoadXml(misXML);
                                XmlNode misDetailNode = misDoc.SelectSingleNode("ManagedInvestmentSchemesAccountEntity").Clone();
                                XmlNode importNode = doc.DocumentElement.SelectSingleNode("ManagedInvestmentSchemesAccounts").OwnerDocument.ImportNode(misDetailNode, true);
                                doc.DocumentElement.SelectSingleNode("ManagedInvestmentSchemesAccounts").AppendChild(importNode);
                            }

                            Broker.ReleaseBrokerManagedComponent(allFundsinMIS);
                        }
                    }
                }
            }
            //-----END MIS CHANGES----//

            return doc.OuterXml;
        }

        private string ExtractXML(string data)
        {
            this.GetDataStream(-100, string.Empty);
            string xmlco = _entity.ToXmlString();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmlco);
            XMLExtensions.RemoveNode("Clid", doc);
            XMLExtensions.RemoveNode("Csid", doc);
            XMLExtensions.SetNode(_entity.Contacts, "Contacts", "Contact", doc, this.Broker);
            XMLExtensions.SetNode(_entity.Signatories, "Signatories", "Signatory", doc, this.Broker);
            XMLExtensions.SetNode(_entity.BankAccounts, "BankAccounts", "BankAccount", doc, this.Broker);
            XmlAttribute rootAtt = doc.CreateAttribute("Type");
            rootAtt.Value = this.TypeName;
            doc.DocumentElement.Attributes.Append(rootAtt);
            string xml = doc.OuterXml;
            XDocument document = XDocument.Load(XmlReader.Create(new StringReader(xml)));
            document.Root.Name = data;
            document.Root.RemoveAttributes();
            xml = document.ToString().Trim();
            return xml;
        }

        private void AddAdminSectionXML(XmlDocument _doc)
        {
            string rootadmin = "AdminSection";
            string adminrow = "AdminRow";
            XmlElement elementroot = _doc.CreateElement(rootadmin);

            foreach (var _each in this._entity.LinkedAccounts)
            {
                XmlElement rowdata = _doc.CreateElement(adminrow);

                XmlElement account = _doc.CreateElement("Account");
                XmlElement type = _doc.CreateElement("Type");
                type.InnerText = _each.LeftAccount.AccountType;
                account.AppendChild(type);
                XmlElement name = _doc.CreateElement("Name");
                name.InnerText = _each.LeftAccount.AccountName;
                account.AppendChild(name);
                rowdata.AppendChild(account);

                XmlElement linkaccount = _doc.CreateElement("LinkAccount");
                XmlElement linktype = _doc.CreateElement("Type");
                linktype.InnerText = _each.RightAccount.AccountType;
                linkaccount.AppendChild(linktype);
                XmlElement linkname = _doc.CreateElement("Name");
                linkname.InnerText = _each.RightAccount.AccountName;
                linkaccount.AppendChild(linkname);
                rowdata.AppendChild(linkaccount);

                XmlElement serviceType = _doc.CreateElement("ServiceType");
                serviceType.InnerText = _each.ServiceType;
                rowdata.AppendChild(serviceType);

                elementroot.AppendChild(rowdata);
            }
            _doc.DocumentElement.AppendChild(elementroot);
        }

        #endregion


        #region GetCSVData

        public string GetCsvData(CSVType csvtype)
        {
            this.GetDataStream(-999, "");
            string xml = string.Empty;

            switch (csvtype)
            {
                case CSVType.BankWest:
                    xml = GetCsvData_BankWest(ExportUtilities.GetAccountTypes(Entity));

                    //if (!IsExported)
                    //            this.ApplicationDate = DateTime.Now.ToShortDateString();
                    //this.IsExported = true;
                    //if (string.IsNullOrEmpty(this.ApplicationID))
                    //   this.ApplicationID = "BW" + UniqueID.Return9UniqueID();


                    break;
                case CSVType.BankWest2:
                    xml = GetCsvData_BankWest2();
                    //this.IsExportedDB = true;
                    break;
                case CSVType.StateStreet:
                    if (Entity.Servicetype.DO_IT_FOR_ME || Entity.Servicetype.DO_IT_WITH_ME)
                    {
                        xml = GetCsvData_StateStreet();
                        //this.IsExportedSS = true;

                    }
                    break;
                default:
                    break;
            }
            return xml;
        }

        private string GetCsvData_BankWest2()
        {
            StringBuilder csvstring = new StringBuilder();

            CSVInstance csvinstance = new CSVInstance(this.Broker);
            CorporateCSV _entitycsv = csvinstance.GetEntityConcreteInstance(_entity);

            foreach (Oritax.TaxSimp.Common.IndividualEntity individualEntity in _entitycsv.Signatories)
            {

                AccountOpeningBankWestIDCSVColumns obj = new AccountOpeningBankWestIDCSVColumns();

                obj.SetColumnValue(obj.Col_ApplicationID, this.ApplicationID);
                obj.SetColumnValue(obj.Col_ApplicationDate, DateTime.Now.ToString("dd-MM-yy"));
                obj.SetColumnValue(obj.Col_ApplicationStatus, "");
                obj.SetColumnValue(obj.Col_HundredPointApplicationCreatorUserID, "");
                obj.SetColumnValue(obj.Col_HundredPointApplicationReceiptNo, this.ApplicationID);

                obj.SetColumnValue(obj.Col_HundredPointApplicationTitle, individualEntity.PersonalTitle.Title);
                obj.SetColumnValue(obj.Col_HundredPointApplicationTitleOther, "");
                obj.SetColumnValue(obj.Col_HundredPointApplicationSurname, individualEntity.Surname);
                obj.SetColumnValue(obj.Col_HundredPointApplicationGivenName, individualEntity.Name);
                obj.SetColumnValue(obj.Col_HundredPointApplicationDOBDay, individualEntity.DOB.Value.Day.ToString());
                obj.SetColumnValue(obj.Col_HundredPointApplicationDOBMonth, individualEntity.DOB.Value.Month.ToString());
                obj.SetColumnValue(obj.Col_HundredPointApplicationDOBYear, individualEntity.DOB.Value.Year.ToString());
                obj.SetColumnValue(obj.Col_HundredPointApplicationResidentialAddress, individualEntity.ResidentialAddress.Addressline1 + " " + individualEntity.ResidentialAddress.Addressline2 + " " + individualEntity.ResidentialAddress.Suburb + " " + individualEntity.ResidentialAddress.PostCode);
                obj.SetColumnValue(obj.Col_HundredPointApplicationPartnerCode, "CMT1345");
                obj.SetColumnValue(obj.Col_HundredPointApplicationBSB1, "");
                obj.SetColumnValue(obj.Col_HundredPointApplicationBSB2, "");
                obj.SetColumnValue(obj.Col_HundredPointApplicationAccountNumber, "");
                obj.SetColumnValue(obj.Col_HundredPointApplicationOccupation, individualEntity.Occupation);
                obj.SetColumnValue(obj.Col_HundredPointApplicationCountryOfResidence, "Australia");
                obj.SetColumnValue(obj.Col_ResidentialAddressStreet, individualEntity.ResidentialAddress.Addressline1 + " " + individualEntity.ResidentialAddress.Addressline2);
                obj.SetColumnValue(obj.Col_ResidentialAddressSuburb, individualEntity.ResidentialAddress.Suburb);
                obj.SetColumnValue(obj.Col_ResidentialAddressState, individualEntity.ResidentialAddress.State);
                obj.SetColumnValue(obj.Col_ResidentialAddressPostcode, individualEntity.ResidentialAddress.PostCode);
                if (!_entity.Address.MailingAddressSame)
                {
                    obj.SetColumnValue(obj.Col_HundredPointApplicationMailingAddress, "");
                    obj.SetColumnValue(obj.Col_MailingAddressStreet, individualEntity.MailingAddress.Addressline1 + " " + individualEntity.MailingAddress.Addressline2);
                    obj.SetColumnValue(obj.Col_MailingAddressSuburb, individualEntity.MailingAddress.Suburb);
                    obj.SetColumnValue(obj.Col_MailingAddressState, individualEntity.MailingAddress.State);
                    obj.SetColumnValue(obj.Col_MailingAddressPostcode, individualEntity.MailingAddress.PostCode);
                }
                obj.SetColumnValue(obj.Col_HundredPointApplicationHomePh, individualEntity.HomePhoneNumber.PhoneNumber);
                obj.SetColumnValue(obj.Col_HundredPointApplicationBusPh, individualEntity.WorkPhoneNumber.PhoneNumber);
                obj.SetColumnValue(obj.Col_HundredPointApplicationMobilePh, individualEntity.MobilePhoneNumber.MobileNumber);
                obj.SetColumnValue(obj.Col_HundredPointApplicationPeriodKnown, "");
                obj.SetColumnValue(obj.Col_HundredPointApplicationPartiallyComplete, "");

                Oritax.TaxSimp.Common.DocumentTypeEntityList documentTypeEntityList = individualEntity.DocumentTypeEntityList();

                if (documentTypeEntityList.Count > 0)
                {
                    string[] keys = documentTypeEntityList.Keys.ToArray();
                    Oritax.TaxSimp.Common.DocumentTypeEntity documentTypeEntity = null;

                    for (int count = 0; count <= keys.Length - 1; count++)
                    {
                        if (count == 0)
                        {

                            documentTypeEntity = documentTypeEntityList[keys[count]];

                            obj.SetColumnValue(obj.Col_HundredPointApplicationIDDocument, documentTypeEntity.DocNumber);
                            obj.SetColumnValue(obj.Col_IDDocumentDocumentLayout, "");
                            obj.SetColumnValue(obj.Col_IDDocumentTypeOfDoc, documentTypeEntity.Type);
                            obj.SetColumnValue(obj.Col_IDDocumentFullName, documentTypeEntity.FullName);
                            if (documentTypeEntity.DOB != null)
                            {
                                obj.SetColumnValue(obj.Col_IDDocumentDOBDay, documentTypeEntity.DOB.Value.Day.ToString());
                                obj.SetColumnValue(obj.Col_IDDocumentDOBMonth, documentTypeEntity.DOB.Value.Month.ToString());
                                obj.SetColumnValue(obj.Col_IDDocumentDOBYear, documentTypeEntity.DOB.Value.Year.ToString());
                            }
                            obj.SetColumnValue(obj.Col_IDDocumentDocNumber, documentTypeEntity.DocNumber);
                            obj.SetColumnValue(obj.Col_IDDocumentPhotoShown, "Yes");
                            obj.SetColumnValue(obj.Col_IDDocumentAddressMatch, "Yes");
                            if (documentTypeEntity.IssueDate != null)
                            {
                                obj.SetColumnValue(obj.Col_IDDocumentDateOfIssueDay, documentTypeEntity.IssueDate.Value.Day.ToString());
                                obj.SetColumnValue(obj.Col_IDDocumentDateOfIssueMonth, documentTypeEntity.IssueDate.Value.Month.ToString());
                                obj.SetColumnValue(obj.Col_IDDocumentDateOfIssueYear, documentTypeEntity.IssueDate.Value.Year.ToString());
                            }

                            if (documentTypeEntity.ExpiryDate != null)
                            {
                                obj.SetColumnValue(obj.Col_IDDocumentExpDateDay, documentTypeEntity.ExpiryDate.Value.Day.ToString());
                                obj.SetColumnValue(obj.Col_IDDocumentExpDateMonth, documentTypeEntity.ExpiryDate.Value.Month.ToString());
                                obj.SetColumnValue(obj.Col_IDDocumentExpDateYear, documentTypeEntity.ExpiryDate.Value.Year.ToString());
                            }
                            obj.SetColumnValue(obj.Col_IDDocumentIssuePlace, documentTypeEntity.IssuePlace);
                            obj.SetColumnValue(obj.Col_IDDocumentPlaceResidence, documentTypeEntity.PlaceResidence);
                            obj.SetColumnValue(obj.Col_IDDocumentPoints, "");
                            obj.SetColumnValue(obj.Col_IDDocumentReferenceNumber, documentTypeEntity.ReferenceNumber);
                            obj.SetColumnValue(obj.Col_IDDocumentInformationProvidedByOne, documentTypeEntity.InfoProvidedByOne);
                            obj.SetColumnValue(obj.Col_IDDocumentTitleRankOrDesignation, documentTypeEntity.TitleRankDesignation);
                            obj.SetColumnValue(obj.Col_IDDocumentInformationProvidedByTwo, documentTypeEntity.InfoProvidedByTwo);
                            obj.SetColumnValue(obj.Col_IDDocumentNameOfOrganisation, documentTypeEntity.OrganizationName);
                            obj.SetColumnValue(obj.Col_IDDocumentAddressOfOrganisation, "");
                            obj.SetColumnValue(obj.Col_IDDocumentPhoneNumberOfOrganisation, documentTypeEntity.OrganizationPhone.PhoneNumber);
                            obj.SetColumnValue(obj.Col_IDDocumentBillLetterSighted, documentTypeEntity.BillLetterSighted.ToString());
                            if (documentTypeEntity.TelephoneContactDate != null)
                            {
                                obj.SetColumnValue(obj.Col_IDDocumentDateTelephoneContactMadeDay, documentTypeEntity.TelephoneContactDate.Value.Day.ToString());
                                obj.SetColumnValue(obj.Col_IDDocumentDateTelephoneContactMadeMonth, documentTypeEntity.TelephoneContactDate.Value.Month.ToString());
                                obj.SetColumnValue(obj.Col_IDDocumentDateTelephoneContactMadeYear, documentTypeEntity.TelephoneContactDate.Value.Year.ToString());
                            }
                        }

                        else if (count == 1)
                        {
                            documentTypeEntity = documentTypeEntityList[keys[count]];

                            obj.SetColumnValue(obj.Col_HundredPointApplicationIDDocument_2, documentTypeEntity.DocNumber);
                            obj.SetColumnValue(obj.Col_IDDocumentDocumentLayout_2, "");
                            obj.SetColumnValue(obj.Col_IDDocumentTypeOfDoc_2, documentTypeEntity.Type);
                            obj.SetColumnValue(obj.Col_IDDocumentFullName_2, documentTypeEntity.FullName);
                            if (documentTypeEntity.DOB != null)
                            {
                                obj.SetColumnValue(obj.Col_IDDocumentDOBDay_2, documentTypeEntity.DOB.Value.Day.ToString());
                                obj.SetColumnValue(obj.Col_IDDocumentDOBMonth_2, documentTypeEntity.DOB.Value.Month.ToString());
                                obj.SetColumnValue(obj.Col_IDDocumentDOBYear_2, documentTypeEntity.DOB.Value.Year.ToString());
                            }
                            obj.SetColumnValue(obj.Col_IDDocumentDocNumber_2, documentTypeEntity.DocNumber);
                            obj.SetColumnValue(obj.Col_IDDocumentPhotoShown_2, "Yes");
                            obj.SetColumnValue(obj.Col_IDDocumentAddressMatch_2, "Yes");
                            if (documentTypeEntity.IssueDate != null)
                            {
                                obj.SetColumnValue(obj.Col_IDDocumentDateOfIssueDay_2, documentTypeEntity.IssueDate.Value.Day.ToString());
                                obj.SetColumnValue(obj.Col_IDDocumentDateOfIssueMonth_2, documentTypeEntity.IssueDate.Value.Month.ToString());
                                obj.SetColumnValue(obj.Col_IDDocumentDateOfIssueYear_2, documentTypeEntity.IssueDate.Value.Year.ToString());
                            }

                            if (documentTypeEntity.ExpiryDate != null)
                            {
                                obj.SetColumnValue(obj.Col_IDDocumentExpDateDay_2, documentTypeEntity.ExpiryDate.Value.Day.ToString());
                                obj.SetColumnValue(obj.Col_IDDocumentExpDateMonth_2, documentTypeEntity.ExpiryDate.Value.Month.ToString());
                                obj.SetColumnValue(obj.Col_IDDocumentExpDateYear_2, documentTypeEntity.ExpiryDate.Value.Year.ToString());
                            }
                            obj.SetColumnValue(obj.Col_IDDocumentIssuePlace_2, documentTypeEntity.IssuePlace);
                            obj.SetColumnValue(obj.Col_IDDocumentPlaceResidence_2, documentTypeEntity.PlaceResidence);
                            obj.SetColumnValue(obj.Col_IDDocumentPoints_2, "");
                            obj.SetColumnValue(obj.Col_IDDocumentReferenceNumber_2, documentTypeEntity.ReferenceNumber);
                            obj.SetColumnValue(obj.Col_IDDocumentInformationProvidedByOne_2, documentTypeEntity.InfoProvidedByOne);
                            obj.SetColumnValue(obj.Col_IDDocumentTitleRankOrDesignation_2, documentTypeEntity.TitleRankDesignation);
                            obj.SetColumnValue(obj.Col_IDDocumentInformationProvidedByTwo_2, documentTypeEntity.InfoProvidedByTwo);
                            obj.SetColumnValue(obj.Col_IDDocumentNameOfOrganisation_2, documentTypeEntity.OrganizationName);
                            obj.SetColumnValue(obj.Col_IDDocumentAddressOfOrganisation_2, "");
                            obj.SetColumnValue(obj.Col_IDDocumentPhoneNumberOfOrganisation_2, documentTypeEntity.OrganizationPhone.PhoneNumber);
                            obj.SetColumnValue(obj.Col_IDDocumentBillLetterSighted_2, documentTypeEntity.BillLetterSighted.ToString());
                            if (documentTypeEntity.TelephoneContactDate != null)
                            {
                                obj.SetColumnValue(obj.Col_IDDocumentDateTelephoneContactMadeDay_2, documentTypeEntity.TelephoneContactDate.Value.Day.ToString());
                                obj.SetColumnValue(obj.Col_IDDocumentDateTelephoneContactMadeMonth_2, documentTypeEntity.TelephoneContactDate.Value.Month.ToString());
                                obj.SetColumnValue(obj.Col_IDDocumentDateTelephoneContactMadeYear_2, documentTypeEntity.TelephoneContactDate.Value.Year.ToString());
                            }
                        }

                        else if (count == 2)
                        {
                            documentTypeEntity = documentTypeEntityList[keys[count]];


                            obj.SetColumnValue(obj.Col_HundredPointApplicationIDDocument_3, documentTypeEntity.DocNumber);
                            obj.SetColumnValue(obj.Col_IDDocumentDocumentLayout_3, "");
                            obj.SetColumnValue(obj.Col_IDDocumentTypeOfDoc_3, documentTypeEntity.Type);
                            obj.SetColumnValue(obj.Col_IDDocumentFullName_3, documentTypeEntity.FullName);
                            if (documentTypeEntity.DOB != null)
                            {
                                obj.SetColumnValue(obj.Col_IDDocumentDOBDay_3, documentTypeEntity.DOB.Value.Day.ToString());
                                obj.SetColumnValue(obj.Col_IDDocumentDOBMonth_3, documentTypeEntity.DOB.Value.Month.ToString());
                                obj.SetColumnValue(obj.Col_IDDocumentDOBYear_3, documentTypeEntity.DOB.Value.Year.ToString());
                            }
                            obj.SetColumnValue(obj.Col_IDDocumentDocNumber_3, documentTypeEntity.DocNumber);
                            obj.SetColumnValue(obj.Col_IDDocumentPhotoShown_3, "Yes");
                            obj.SetColumnValue(obj.Col_IDDocumentAddressMatch_3, "Yes");
                            if (documentTypeEntity.IssueDate != null)
                            {
                                obj.SetColumnValue(obj.Col_IDDocumentDateOfIssueDay_3, documentTypeEntity.IssueDate.Value.Day.ToString());
                                obj.SetColumnValue(obj.Col_IDDocumentDateOfIssueMonth_3, documentTypeEntity.IssueDate.Value.Month.ToString());
                                obj.SetColumnValue(obj.Col_IDDocumentDateOfIssueYear_3, documentTypeEntity.IssueDate.Value.Year.ToString());
                            }

                            if (documentTypeEntity.ExpiryDate != null)
                            {
                                obj.SetColumnValue(obj.Col_IDDocumentExpDateDay_3, documentTypeEntity.ExpiryDate.Value.Day.ToString());
                                obj.SetColumnValue(obj.Col_IDDocumentExpDateMonth_3, documentTypeEntity.ExpiryDate.Value.Month.ToString());
                                obj.SetColumnValue(obj.Col_IDDocumentExpDateYear_3, documentTypeEntity.ExpiryDate.Value.Year.ToString());
                            }
                            obj.SetColumnValue(obj.Col_IDDocumentIssuePlace_3, documentTypeEntity.IssuePlace);
                            obj.SetColumnValue(obj.Col_IDDocumentPlaceResidence_3, documentTypeEntity.PlaceResidence);
                            obj.SetColumnValue(obj.Col_IDDocumentPoints_3, "");
                            obj.SetColumnValue(obj.Col_IDDocumentReferenceNumber_3, documentTypeEntity.ReferenceNumber);
                            obj.SetColumnValue(obj.Col_IDDocumentInformationProvidedByOne_3, documentTypeEntity.InfoProvidedByOne);
                            obj.SetColumnValue(obj.Col_IDDocumentTitleRankOrDesignation_3, documentTypeEntity.TitleRankDesignation);
                            obj.SetColumnValue(obj.Col_IDDocumentInformationProvidedByTwo_3, documentTypeEntity.InfoProvidedByTwo);
                            obj.SetColumnValue(obj.Col_IDDocumentNameOfOrganisation_3, documentTypeEntity.OrganizationName);
                            obj.SetColumnValue(obj.Col_IDDocumentAddressOfOrganisation_3, "");
                            obj.SetColumnValue(obj.Col_IDDocumentPhoneNumberOfOrganisation_3, documentTypeEntity.OrganizationPhone.PhoneNumber);
                            obj.SetColumnValue(obj.Col_IDDocumentBillLetterSighted_3, documentTypeEntity.BillLetterSighted.ToString());
                            if (documentTypeEntity.TelephoneContactDate != null)
                            {
                                obj.SetColumnValue(obj.Col_IDDocumentDateTelephoneContactMadeDay_3, documentTypeEntity.TelephoneContactDate.Value.Day.ToString());
                                obj.SetColumnValue(obj.Col_IDDocumentDateTelephoneContactMadeMonth_3, documentTypeEntity.TelephoneContactDate.Value.Month.ToString());
                                obj.SetColumnValue(obj.Col_IDDocumentDateTelephoneContactMadeYear_3, documentTypeEntity.TelephoneContactDate.Value.Year.ToString());
                            }
                        }

                        else if (count == 3)
                        {
                            documentTypeEntity = documentTypeEntityList[keys[count]];


                            obj.SetColumnValue(obj.Col_HundredPointApplicationIDDocument_4, documentTypeEntity.DocNumber);
                            obj.SetColumnValue(obj.Col_IDDocumentDocumentLayout_4, "");
                            obj.SetColumnValue(obj.Col_IDDocumentTypeOfDoc_4, documentTypeEntity.Type);
                            obj.SetColumnValue(obj.Col_IDDocumentFullName_4, documentTypeEntity.FullName);
                            if (documentTypeEntity.DOB != null)
                            {
                                obj.SetColumnValue(obj.Col_IDDocumentDOBDay_4, documentTypeEntity.DOB.Value.Day.ToString());
                                obj.SetColumnValue(obj.Col_IDDocumentDOBMonth_4, documentTypeEntity.DOB.Value.Month.ToString());
                                obj.SetColumnValue(obj.Col_IDDocumentDOBYear_4, documentTypeEntity.DOB.Value.Year.ToString());
                            }
                            obj.SetColumnValue(obj.Col_IDDocumentDocNumber_4, documentTypeEntity.DocNumber);
                            obj.SetColumnValue(obj.Col_IDDocumentPhotoShown_4, "Yes");
                            obj.SetColumnValue(obj.Col_IDDocumentAddressMatch_4, "Yes");
                            if (documentTypeEntity.IssueDate != null)
                            {
                                obj.SetColumnValue(obj.Col_IDDocumentDateOfIssueDay_4, documentTypeEntity.IssueDate.Value.Day.ToString());
                                obj.SetColumnValue(obj.Col_IDDocumentDateOfIssueMonth_4, documentTypeEntity.IssueDate.Value.Month.ToString());
                                obj.SetColumnValue(obj.Col_IDDocumentDateOfIssueYear_4, documentTypeEntity.IssueDate.Value.Year.ToString());
                            }
                            if (documentTypeEntity.ExpiryDate != null)
                            {
                                obj.SetColumnValue(obj.Col_IDDocumentExpDateDay_4, documentTypeEntity.ExpiryDate.Value.Day.ToString());
                                obj.SetColumnValue(obj.Col_IDDocumentExpDateMonth_4, documentTypeEntity.ExpiryDate.Value.Month.ToString());
                                obj.SetColumnValue(obj.Col_IDDocumentExpDateYear_4, documentTypeEntity.ExpiryDate.Value.Year.ToString());
                            }
                            obj.SetColumnValue(obj.Col_IDDocumentIssuePlace_4, documentTypeEntity.IssuePlace);
                            obj.SetColumnValue(obj.Col_IDDocumentPlaceResidence_4, documentTypeEntity.PlaceResidence);
                            obj.SetColumnValue(obj.Col_IDDocumentPoints_4, "");
                            obj.SetColumnValue(obj.Col_IDDocumentReferenceNumber_4, documentTypeEntity.ReferenceNumber);
                            obj.SetColumnValue(obj.Col_IDDocumentInformationProvidedByOne_4, documentTypeEntity.InfoProvidedByOne);
                            obj.SetColumnValue(obj.Col_IDDocumentTitleRankOrDesignation_4, documentTypeEntity.TitleRankDesignation);
                            obj.SetColumnValue(obj.Col_IDDocumentInformationProvidedByTwo_4, documentTypeEntity.InfoProvidedByTwo);
                            obj.SetColumnValue(obj.Col_IDDocumentNameOfOrganisation_4, documentTypeEntity.OrganizationName);
                            obj.SetColumnValue(obj.Col_IDDocumentAddressOfOrganisation_4, "");
                            obj.SetColumnValue(obj.Col_IDDocumentPhoneNumberOfOrganisation_4, documentTypeEntity.OrganizationPhone.PhoneNumber);
                            obj.SetColumnValue(obj.Col_IDDocumentBillLetterSighted_4, documentTypeEntity.BillLetterSighted.ToString());
                            if (documentTypeEntity.TelephoneContactDate != null)
                            {
                                obj.SetColumnValue(obj.Col_IDDocumentDateTelephoneContactMadeDay_4, documentTypeEntity.TelephoneContactDate.Value.Day.ToString());
                                obj.SetColumnValue(obj.Col_IDDocumentDateTelephoneContactMadeMonth_4, documentTypeEntity.TelephoneContactDate.Value.Month.ToString());
                                obj.SetColumnValue(obj.Col_IDDocumentDateTelephoneContactMadeYear_4, documentTypeEntity.TelephoneContactDate.Value.Year.ToString());
                            }
                        }
                    }
                }

                csvstring.Append(obj.ToCSV());
            }

            return csvstring.ToString();
        }

        private string GetCsvData_BankWest(List<AccountType> acctypelist)
        {
            StringBuilder csvstring = new StringBuilder();
            if (acctypelist != null && acctypelist.Count > 0)
            {
                foreach (AccountType _each in acctypelist)
                {
                    if (_each == AccountType.CMA)
                    {
                        csvstring.Append(CMAAccount());
                    }
                    else if (_each == AccountType.MMA)
                    {
                        csvstring.Append(MMAAccount());
                    }
                }
            }
            return csvstring.ToString();
        }

        private string CMAAccount()
        {
            AccountOpeningBankWestCSVColumns obj = new AccountOpeningBankWestCSVColumns();
            CSVInstance csvinstance = new CSVInstance(this.Broker);
            CorporateCSV _entitycsv = csvinstance.GetEntityConcreteInstance(_entity);

            obj.SetColumnValue(obj.Col_ApplicationID, this.ApplicationID);
            obj.SetColumnValue(obj.Col_ApplicationDate, DateTime.Now.ToString("dd-MM-yy"));
            obj.SetColumnValue(obj.Col_ApplicationStatus, "New");
            obj.SetColumnValue(obj.Col_CMTOpenDate, "");
            obj.SetColumnValue(obj.Col_OpenAccountApplicationCreatorUserID, "");
            obj.SetColumnValue(obj.Col_OpenAccountApplicationApplicantType, CSVUtilities.GetBankWestType(this.TypeName));
            obj.SetColumnValue(obj.Col_OpenAccountApplicationAccountOption, "Investor Plus");
            obj.SetColumnValue(obj.Col_OpenAccountApplicationProductType, "CMT");
            obj.SetColumnValue(obj.Col_OpenAccountApplicationTrusteesCompany, CSVUtilities.GetTrusteesCompanyType(this.TypeName));
            obj.SetColumnValue(obj.Col_TrusteesCompanyDetailEntityName1, _entity.Name);
            obj.SetColumnValue(obj.Col_TrusteesCompanyDetailEntityName2, _entity.LegalName);
            obj.SetColumnValue(obj.Col_TrusteesCompanyDetailACNABN, _entity.ACN);
            obj.SetColumnValue(obj.Col_TrusteesCompanyDetailABN, _entity.ABN);
            obj.SetColumnValue(obj.Col_TrusteesCompanyDetailACNARBN, "");
            obj.SetColumnValue(obj.Col_TrusteesCompanyDetailContactName, "");
            obj.SetColumnValue(obj.Col_TrusteesCompanyDetailContactEmail, "");
            obj.SetColumnValue(obj.Col_TrusteesCompanyDetailContactPhPrefix, "");
            obj.SetColumnValue(obj.Col_TrusteesCompanyDetailContactPh, "");
            obj.SetColumnValue(obj.Col_TrusteesCompanyDetailCountryOfEstablishment, "Australia");
            if (_entitycsv.Contacts != null && _entitycsv.Contacts.Count > 0)
            {
                obj.SetColumnValue(obj.Col_TrusteesCompanyDetailContactName, _entitycsv.Contacts[0].Name + " " + _entitycsv.Contacts[0].Surname);
                obj.SetColumnValue(obj.Col_TrusteesCompanyDetailContactEmail, _entitycsv.Contacts[0].EmailAddress);
                obj.SetColumnValue(obj.Col_TrusteesCompanyDetailContactPhPrefix, _entitycsv.Contacts[0].WorkPhoneNumber.CityCode);
                obj.SetColumnValue(obj.Col_TrusteesCompanyDetailContactPh, _entitycsv.Contacts[0].WorkPhoneNumber.PhoneNumber);
                obj.SetColumnValue(obj.Col_TrusteesCompanyDetailCountryOfEstablishment, _entitycsv.Country);
            }

            obj.SetColumnValue(obj.Col_TFNDetailTFN, _entitycsv.TFN);
            obj.SetColumnValue(obj.Col_TFNDetailNRC, "");
            obj.SetColumnValue(obj.Col_TFNDetailExemptionCategory, "");
            obj.SetColumnValue(obj.Col_TFNDetailTIN, _entitycsv.TIN);
            obj.SetColumnValue(obj.Col_CorpTrusteeDetailEntityName1, "");
            obj.SetColumnValue(obj.Col_CorpTrusteeDetailEntityName2, "");
            obj.SetColumnValue(obj.Col_CorpTrusteeDetailACNABN, "");
            obj.SetColumnValue(obj.Col_CorpTrusteeDetailABN, "");
            obj.SetColumnValue(obj.Col_CorpTrusteeDetailACNARBN, "");
            obj.SetColumnValue(obj.Col_CorpTrusteeDetailCountryOfEstablishment, "");
            obj.SetColumnValue(obj.Col_OpenAccountApplicationInitialInvestmentAmount, "");
            obj.SetColumnValue(obj.Col_OpenAccountApplicationMannerOfOperation, "Anyone");
            obj.SetColumnValue(obj.Col_OpenAccountApplicationFinancialAdviserAccess, "");
            obj.SetColumnValue(obj.Col_OpenAccountApplicationAdvisorFirmAccess, "General");
            obj.SetColumnValue(obj.Col_OpenAccountApplicationLimitedAccessApproved, "FALSE");
            obj.SetColumnValue(obj.Col_AccessFacilityType, "Telephone Access");
            obj.SetColumnValue(obj.Col_AccessFacilitySelected, _entitycsv.AccessFacilities.PHONEACCESS.ToString());
            obj.SetColumnValue(obj.Col_AccessFacilityType_2, "Online Access");
            obj.SetColumnValue(obj.Col_AccessFacilitySelected_2, _entitycsv.AccessFacilities.ONLINEACCESS.ToString());
            obj.SetColumnValue(obj.Col_AccessFacilityType_3, "Cheque Book");
            obj.SetColumnValue(obj.Col_AccessFacilitySelected_3, _entitycsv.AccessFacilities.CHEQUEBOOK.ToString());
            obj.SetColumnValue(obj.Col_AccessFacilityType_4, "Deposit Book");
            obj.SetColumnValue(obj.Col_AccessFacilitySelected_4, _entitycsv.AccessFacilities.DEPOSITBOOK.ToString());
            obj.SetColumnValue(obj.Col_AccessFacilityType_5, "Debit Card");
            obj.SetColumnValue(obj.Col_AccessFacilitySelected_5, _entitycsv.AccessFacilities.DEBITCARD.ToString());
            obj.SetColumnValue(obj.Col_OpenAccountApplicationDealerName, "RI Advice");
            obj.SetColumnValue(obj.Col_OpenAccountApplicationAdviserName, "FORTNUM PRIVATE WEALTH PTY LTD");
            obj.SetColumnValue(obj.Col_OpenAccountApplicationAdviserCode, "SK15410001");
            obj.SetColumnValue(obj.Col_OpenAccountApplicationCMTaccountNumber, "");
            obj.SetColumnValue(obj.Col_OpenAccountApplicationCustomerAccountNumber, this.ApplicationID);
            obj.SetColumnValue(obj.Col_OpenAccountApplicationCMTaccountBSB1, "");
            obj.SetColumnValue(obj.Col_OpenAccountApplicationCMTaccountBSB2, "");
            obj.SetColumnValue(obj.Col_OpenAccountApplicationClientAccountDesignation, "");
            obj.SetColumnValue(obj.Col_OpenAccountApplicationPartnerCode, "CMT1345");
            obj.SetColumnValue(obj.Col_OpenAccountApplicationReceiptNo, "");

            int i = 1;
            foreach (Oritax.TaxSimp.Common.IndividualEntity each in _entitycsv.Signatories.Take(4))
            {
                if (i == 1)
                {
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailOccupation, each.Occupation);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailEmployer, each.Employer);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailCountryOfResidence, each.ResidentialAddress.Country);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailTitle, each.PersonalTitle.Title);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailTitleOther, "");
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailGivenName, each.Name);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailSurname, each.Surname);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailPhoneWk, each.WorkPhoneNumber.PhoneNumber);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailPhoneHm, each.HomePhoneNumber.PhoneNumber);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailPhoneMb, each.MobilePhoneNumber.MobileNumber);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailDOBDay, each.DOB.Value.Day.ToString());
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailDOBMonth, each.DOB.Value.Month.ToString());
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailDOBYear, each.DOB.Value.Year.ToString());
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailEmail, each.EmailAddress);

                    obj.SetColumnValue(obj.Col_AddressAddress1, each.ResidentialAddress.Addressline1);
                    obj.SetColumnValue(obj.Col_AddressAddress2, each.ResidentialAddress.Addressline2);
                    obj.SetColumnValue(obj.Col_AddressAddress3, "");
                    obj.SetColumnValue(obj.Col_AddressSuburb, each.ResidentialAddress.Suburb);
                    obj.SetColumnValue(obj.Col_AddressState, each.ResidentialAddress.State);
                    obj.SetColumnValue(obj.Col_AddressPostcode, each.ResidentialAddress.PostCode);
                    obj.SetColumnValue(obj.Col_AddressCountry, each.ResidentialAddress.Country);

                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailPhoneHmPrefix, each.HomePhoneNumber.CityCode);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailPhonewkPrefix, each.WorkPhoneNumber.CityCode);

                    obj.SetColumnValue(obj.Col_TFNDetailTFN_2, "");
                    obj.SetColumnValue(obj.Col_TFNDetailNRC_2, "");
                    obj.SetColumnValue(obj.Col_TFNDetailExemptionCategory_2, "");
                    obj.SetColumnValue(obj.Col_TFNDetailTIN_2, "");
                }

                if (i == 2)
                {
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailOccupation_2, each.Occupation);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailEmployer_2, each.Employer);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailCountryOfResidence_2, each.ResidentialAddress.Country);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailTitle_2, each.PersonalTitle.Title);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailTitleOther_2, "");
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailGivenName_2, each.Name);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailSurname_2, each.Surname);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailPhoneWk_2, each.WorkPhoneNumber.PhoneNumber);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailPhoneHm_2, each.HomePhoneNumber.PhoneNumber);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailPhoneMb_2, each.MobilePhoneNumber.MobileNumber);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailDOBDay_2, each.DOB.Value.Day.ToString());
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailDOBMonth_2, each.DOB.Value.Month.ToString());
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailDOBYear_2, each.DOB.Value.Year.ToString());
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailEmail_2, each.EmailAddress);

                    obj.SetColumnValue(obj.Col_AddressAddress1_2, each.ResidentialAddress.Addressline1);
                    obj.SetColumnValue(obj.Col_AddressAddress2_2, each.ResidentialAddress.Addressline2);
                    obj.SetColumnValue(obj.Col_AddressAddress3_2, "");
                    obj.SetColumnValue(obj.Col_AddressSuburb_2, each.ResidentialAddress.Suburb);
                    obj.SetColumnValue(obj.Col_AddressState_2, each.ResidentialAddress.State);
                    obj.SetColumnValue(obj.Col_AddressPostcode_2, each.ResidentialAddress.PostCode);
                    obj.SetColumnValue(obj.Col_AddressCountry_2, each.ResidentialAddress.Country);

                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailPhoneHmPrefix_2, each.HomePhoneNumber.CityCode);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailPhonewkPrefix_2, each.WorkPhoneNumber.CityCode);

                    obj.SetColumnValue(obj.Col_TFNDetailTFN_3, "");
                    obj.SetColumnValue(obj.Col_TFNDetailNRC_3, "");
                    obj.SetColumnValue(obj.Col_TFNDetailExemptionCategory_3, "");
                    obj.SetColumnValue(obj.Col_TFNDetailTIN_3, "");
                }

                if (i == 3)
                {
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailOccupation_3, each.Occupation);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailEmployer_3, each.Employer);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailCountryOfResidence_3, each.ResidentialAddress.Country);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailTitle_3, each.PersonalTitle.Title);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailTitleOther_3, each.PersonalTitle.Title);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailGivenName_3, each.Name);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailSurname_3, each.Surname);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailPhoneWk_3, each.WorkPhoneNumber.PhoneNumber);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailPhoneHm_3, each.HomePhoneNumber.PhoneNumber);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailPhoneMb_3, each.MobilePhoneNumber.MobileNumber);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailDOBDay_3, each.DOB.Value.Day.ToString());
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailDOBMonth_3, each.DOB.Value.Month.ToString());
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailDOBYear_3, each.DOB.Value.Year.ToString());
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailEmail_3, each.EmailAddress);

                    obj.SetColumnValue(obj.Col_AddressAddress1_3, each.ResidentialAddress.Addressline1);
                    obj.SetColumnValue(obj.Col_AddressAddress2_3, each.ResidentialAddress.Addressline2);
                    obj.SetColumnValue(obj.Col_AddressAddress3_3, "");
                    obj.SetColumnValue(obj.Col_AddressSuburb_3, each.ResidentialAddress.Suburb);
                    obj.SetColumnValue(obj.Col_AddressState_3, each.ResidentialAddress.State);
                    obj.SetColumnValue(obj.Col_AddressPostcode_3, each.ResidentialAddress.PostCode);
                    obj.SetColumnValue(obj.Col_AddressCountry_3, each.ResidentialAddress.Country);

                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailPhoneHmPrefix_3, each.HomePhoneNumber.CityCode);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailPhonewkPrefix_3, each.WorkPhoneNumber.CityCode);

                    obj.SetColumnValue(obj.Col_TFNDetailTFN_4, "");
                    obj.SetColumnValue(obj.Col_TFNDetailNRC_4, "");
                    obj.SetColumnValue(obj.Col_TFNDetailExemptionCategory_4, "");
                    obj.SetColumnValue(obj.Col_TFNDetailTIN_4, "");
                }

                if (i == 4)
                {
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailOccupation_4, each.Occupation);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailEmployer_4, each.Employer);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailCountryOfResidence_4, each.ResidentialAddress.Country);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailTitle_4, each.PersonalTitle.Title);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailTitleOther_4, each.PersonalTitle.Title);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailGivenName_4, each.Name);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailSurname_4, each.Surname);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailPhoneWk_4, each.WorkPhoneNumber.PhoneNumber);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailPhoneHm_4, each.HomePhoneNumber.PhoneNumber);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailPhoneMb_4, each.MobilePhoneNumber.MobileNumber);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailDOBDay_4, each.DOB.Value.Day.ToString());
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailDOBMonth_4, each.DOB.Value.Month.ToString());
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailDOBYear_4, each.DOB.Value.Year.ToString());
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailEmail_4, each.EmailAddress);

                    obj.SetColumnValue(obj.Col_AddressAddress1_4, each.ResidentialAddress.Addressline1);
                    obj.SetColumnValue(obj.Col_AddressAddress2_4, each.ResidentialAddress.Addressline2);
                    obj.SetColumnValue(obj.Col_AddressAddress3_4, "");
                    obj.SetColumnValue(obj.Col_AddressSuburb_4, each.ResidentialAddress.Suburb);
                    obj.SetColumnValue(obj.Col_AddressState_4, each.ResidentialAddress.State);
                    obj.SetColumnValue(obj.Col_AddressPostcode_4, each.ResidentialAddress.PostCode);
                    obj.SetColumnValue(obj.Col_AddressCountry_4, each.ResidentialAddress.Country);

                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailPhoneHmPrefix_4, each.HomePhoneNumber.CityCode);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailPhonewkPrefix_4, each.WorkPhoneNumber.CityCode);

                    obj.SetColumnValue(obj.Col_TFNDetailTFN_5, "");
                    obj.SetColumnValue(obj.Col_TFNDetailNRC_5, "");
                    obj.SetColumnValue(obj.Col_TFNDetailExemptionCategory_5, "");
                    obj.SetColumnValue(obj.Col_TFNDetailTIN_5, "");
                }

                i++;
            }
            obj.SetColumnValue(obj.Col_CorrespondenceAddressAddress1, _entitycsv.Address.BusinessAddress.Addressline1);
            obj.SetColumnValue(obj.Col_CorrespondenceAddressAddress2, _entitycsv.Address.BusinessAddress.Addressline2);
            obj.SetColumnValue(obj.Col_CorrespondenceAddressAddress3, "");
            obj.SetColumnValue(obj.Col_CorrespondenceAddressSuburb, _entitycsv.Address.BusinessAddress.Suburb);
            obj.SetColumnValue(obj.Col_CorrespondenceAddressState, _entitycsv.Address.BusinessAddress.State);
            obj.SetColumnValue(obj.Col_CorrespondenceAddressPostcode, _entitycsv.Address.BusinessAddress.PostCode);
            obj.SetColumnValue(obj.Col_CorrespondenceAddressCountry, "Australia");
            obj.SetColumnValue(obj.Col_CorrespondenceAddressCareOf, "");
            if (!_entitycsv.Address.MailingAddressSame)
            {

                obj.SetColumnValue(obj.Col_CorrespondenceAddressAddress1_2, _entitycsv.Address.MailingAddress.Addressline1);
                obj.SetColumnValue(obj.Col_CorrespondenceAddressAddress2_2, _entitycsv.Address.MailingAddress.Addressline2);
                obj.SetColumnValue(obj.Col_CorrespondenceAddressAddress3_2, "");
                obj.SetColumnValue(obj.Col_CorrespondenceAddressSuburb_2, _entitycsv.Address.MailingAddress.Suburb);
                obj.SetColumnValue(obj.Col_CorrespondenceAddressState_2, _entitycsv.Address.MailingAddress.State);
                obj.SetColumnValue(obj.Col_CorrespondenceAddressPostcode_2, _entitycsv.Address.MailingAddress.PostCode);
                obj.SetColumnValue(obj.Col_CorrespondenceAddressCountry_2, "Australia");
                obj.SetColumnValue(obj.Col_CorrespondenceAddressCareOf_2, "");
            }
            if (!_entitycsv.Address.DuplicateMailingAddressSame)
            {
                obj.SetColumnValue(obj.Col_CorrespondenceAddressAddress1_3, _entitycsv.Address.DuplicateMailingAddress.Addressline1);
                obj.SetColumnValue(obj.Col_CorrespondenceAddressAddress2_3, _entitycsv.Address.DuplicateMailingAddress.Addressline2);
                obj.SetColumnValue(obj.Col_CorrespondenceAddressAddress3_3, "");
                obj.SetColumnValue(obj.Col_CorrespondenceAddressSuburb_3, _entitycsv.Address.DuplicateMailingAddress.Suburb);
                obj.SetColumnValue(obj.Col_CorrespondenceAddressState_3, _entitycsv.Address.DuplicateMailingAddress.State);
                obj.SetColumnValue(obj.Col_CorrespondenceAddressPostcode_3, _entitycsv.Address.DuplicateMailingAddress.PostCode);
                obj.SetColumnValue(obj.Col_CorrespondenceAddressCountry_3, "Australia");
                obj.SetColumnValue(obj.Col_CorrespondenceAddressCareOf_3, "");
            }
            obj.SetColumnValue(obj.Col_TrusteesCompanyCIF, "");
            obj.SetColumnValue(obj.Col_CorpTrusteeCIF, "");
            obj.SetColumnValue(obj.Col_SignatoryCIF, "");
            obj.SetColumnValue(obj.Col_SignatoryCIF_2, "");
            obj.SetColumnValue(obj.Col_SignatoryCIF_3, "");
            obj.SetColumnValue(obj.Col_SignatoryCIF_4, "");
            obj.SetColumnValue(obj.Col_PAN, "");
            obj.SetColumnValue(obj.Col_PAN_2, "");
            obj.SetColumnValue(obj.Col_PAN_3, "");
            obj.SetColumnValue(obj.Col_PAN_4, "");
            obj.SetColumnValue(obj.Col_CBSStatus, "");
            obj.SetColumnValue(obj.Col_Allocation, "");
            obj.SetColumnValue(obj.Col_FollowupDate, "");
            obj.SetColumnValue(obj.Col_OperationsStatus, "");
            obj.SetColumnValue(obj.Col_OpenAccountApplicationPartnerCIF, "");
            obj.SetColumnValue(obj.Col_OpenAccountApplicationAdviserCIF, "");
            obj.SetColumnValue(obj.Col_PlatformName, "");
            obj.SetColumnValue(obj.Col_PlatformCode, "");
            obj.SetColumnValue(obj.Col_PlatformReference1, "");
            obj.SetColumnValue(obj.Col_PlatformReference2, "");
            obj.SetColumnValue(obj.Col_PlatformName_2, "");
            obj.SetColumnValue(obj.Col_PlatformCode_2, "");
            obj.SetColumnValue(obj.Col_PlatformReference1_2, "");
            obj.SetColumnValue(obj.Col_PlatformReference2_2, "");
            obj.SetColumnValue(obj.Col_PlatformName_3, "");
            obj.SetColumnValue(obj.Col_PlatformCode_3, "");
            obj.SetColumnValue(obj.Col_PlatformReference1_3, "");
            obj.SetColumnValue(obj.Col_PlatformReference2_3, "");
            obj.SetColumnValue(obj.Col_ThirdPartyCIFKey, "");
            obj.SetColumnValue(obj.Col_ThirdPartyPartnerCode, "");
            obj.SetColumnValue(obj.Col_OpenAccountApplicationFirmName, "");
            obj.SetColumnValue(obj.Col_OpenAccountApplicationAdvisorFirmName, "");
            obj.SetColumnValue(obj.Col_ReportedOn, "");
            obj.SetColumnValue(obj.Col_ProcessingResult, "");
            obj.SetColumnValue(obj.Col_ProcessingIssues, "");
            obj.SetColumnValue(obj.Col_ApplicationReceivedDate, "");
            obj.SetColumnValue(obj.Col_DocumentationOutstandingDate, "");
            obj.SetColumnValue(obj.Col_FinalisedDate, "");
            obj.SetColumnValue(obj.Col_OpenAccountApplicationProductCode, "BWACMA");
            obj.SetColumnValue(obj.Col_OpenAccountApplicationProductCostCenter, "");
            obj.SetColumnValue(obj.Col_TS_AccountDetails, "");


            return obj.ToCSV();
        }

        private string MMAAccount()
        {
            AccountOpeningBankWestCSVColumns obj = new AccountOpeningBankWestCSVColumns();
            CSVInstance csvinstance = new CSVInstance(this.Broker);
            CorporateCSV _entitycsv = csvinstance.GetEntityConcreteInstance(_entity);

            obj.SetColumnValue(obj.Col_ApplicationID, this.ApplicationID);
            obj.SetColumnValue(obj.Col_ApplicationDate, DateTime.Now.ToString("dd-MM-yy"));
            obj.SetColumnValue(obj.Col_ApplicationStatus, "New");
            obj.SetColumnValue(obj.Col_CMTOpenDate, "");
            obj.SetColumnValue(obj.Col_OpenAccountApplicationCreatorUserID, "");
            obj.SetColumnValue(obj.Col_OpenAccountApplicationApplicantType, CSVUtilities.GetBankWestType(this.TypeName));
            obj.SetColumnValue(obj.Col_OpenAccountApplicationAccountOption, "");
            obj.SetColumnValue(obj.Col_OpenAccountApplicationProductType, "MMA");
            obj.SetColumnValue(obj.Col_OpenAccountApplicationTrusteesCompany, CSVUtilities.GetTrusteesCompanyType(this.TypeName));
            obj.SetColumnValue(obj.Col_TrusteesCompanyDetailEntityName1, _entity.Name);
            obj.SetColumnValue(obj.Col_TrusteesCompanyDetailEntityName2, _entity.LegalName);
            obj.SetColumnValue(obj.Col_TrusteesCompanyDetailACNABN, _entity.ACN);
            obj.SetColumnValue(obj.Col_TrusteesCompanyDetailABN, _entity.ABN);
            obj.SetColumnValue(obj.Col_TrusteesCompanyDetailACNARBN, "");
            obj.SetColumnValue(obj.Col_TrusteesCompanyDetailContactName, "");
            obj.SetColumnValue(obj.Col_TrusteesCompanyDetailContactEmail, "");
            obj.SetColumnValue(obj.Col_TrusteesCompanyDetailContactPhPrefix, "");
            obj.SetColumnValue(obj.Col_TrusteesCompanyDetailContactPh, "");
            obj.SetColumnValue(obj.Col_TrusteesCompanyDetailCountryOfEstablishment, "Australia");
            if (_entitycsv.Contacts != null && _entitycsv.Contacts.Count > 0)
            {
                obj.SetColumnValue(obj.Col_TrusteesCompanyDetailContactName, _entitycsv.Contacts[0].Name + " " + _entitycsv.Contacts[0].Surname);
                obj.SetColumnValue(obj.Col_TrusteesCompanyDetailContactEmail, _entitycsv.Contacts[0].EmailAddress);
                obj.SetColumnValue(obj.Col_TrusteesCompanyDetailContactPhPrefix, _entitycsv.Contacts[0].WorkPhoneNumber.CityCode);
                obj.SetColumnValue(obj.Col_TrusteesCompanyDetailContactPh, _entitycsv.Contacts[0].WorkPhoneNumber.PhoneNumber);
                obj.SetColumnValue(obj.Col_TrusteesCompanyDetailCountryOfEstablishment, _entitycsv.Country);
            }


            obj.SetColumnValue(obj.Col_TFNDetailTFN, _entitycsv.TFN);
            obj.SetColumnValue(obj.Col_TFNDetailNRC, "");
            obj.SetColumnValue(obj.Col_TFNDetailExemptionCategory, "");
            obj.SetColumnValue(obj.Col_TFNDetailTIN, _entitycsv.TIN);
            obj.SetColumnValue(obj.Col_CorpTrusteeDetailEntityName1, "");
            obj.SetColumnValue(obj.Col_CorpTrusteeDetailEntityName2, "");
            obj.SetColumnValue(obj.Col_CorpTrusteeDetailACNABN, "");
            obj.SetColumnValue(obj.Col_CorpTrusteeDetailABN, "");
            obj.SetColumnValue(obj.Col_CorpTrusteeDetailACNARBN, "");
            obj.SetColumnValue(obj.Col_CorpTrusteeDetailCountryOfEstablishment, "");
            obj.SetColumnValue(obj.Col_OpenAccountApplicationInitialInvestmentAmount, "");
            obj.SetColumnValue(obj.Col_OpenAccountApplicationMannerOfOperation, "All");
            obj.SetColumnValue(obj.Col_OpenAccountApplicationFinancialAdviserAccess, "");
            obj.SetColumnValue(obj.Col_OpenAccountApplicationAdvisorFirmAccess, "");
            obj.SetColumnValue(obj.Col_OpenAccountApplicationLimitedAccessApproved, "FALSE");
            obj.SetColumnValue(obj.Col_AccessFacilityType, "Telephone Access");
            obj.SetColumnValue(obj.Col_AccessFacilitySelected, "FALSE");
            obj.SetColumnValue(obj.Col_AccessFacilityType_2, "Online Access");
            obj.SetColumnValue(obj.Col_AccessFacilitySelected_2, "FALSE");
            obj.SetColumnValue(obj.Col_AccessFacilityType_3, "Cheque Book");
            obj.SetColumnValue(obj.Col_AccessFacilitySelected_3, "FALSE");
            obj.SetColumnValue(obj.Col_AccessFacilityType_4, "Deposit Book");
            obj.SetColumnValue(obj.Col_AccessFacilitySelected_4, "FALSE");
            obj.SetColumnValue(obj.Col_AccessFacilityType_5, "Debit Card");
            obj.SetColumnValue(obj.Col_AccessFacilitySelected_5, "FALSE");
            obj.SetColumnValue(obj.Col_OpenAccountApplicationDealerName, "Fortnum Private Wealth");
            obj.SetColumnValue(obj.Col_OpenAccountApplicationAdviserName, "FORTNUM PRIVATE WEALTH PTY LTD");
            obj.SetColumnValue(obj.Col_OpenAccountApplicationAdviserCode, "SK15410001");
            obj.SetColumnValue(obj.Col_OpenAccountApplicationCMTaccountNumber, "");
            obj.SetColumnValue(obj.Col_OpenAccountApplicationCustomerAccountNumber, this.ApplicationID);
            obj.SetColumnValue(obj.Col_OpenAccountApplicationCMTaccountBSB1, "");
            obj.SetColumnValue(obj.Col_OpenAccountApplicationCMTaccountBSB2, "");
            obj.SetColumnValue(obj.Col_OpenAccountApplicationClientAccountDesignation, "");
            obj.SetColumnValue(obj.Col_OpenAccountApplicationPartnerCode, "CMT1345");
            obj.SetColumnValue(obj.Col_OpenAccountApplicationReceiptNo, "");

            int i = 1;
            foreach (Oritax.TaxSimp.Common.IndividualEntity each in _entitycsv.Signatories.Take(4))
            {
                if (i == 1)
                {
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailOccupation, each.Occupation);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailEmployer, each.Employer);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailCountryOfResidence, each.ResidentialAddress.Country);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailTitle, each.PersonalTitle.Title);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailTitleOther, "");
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailGivenName, each.Name);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailSurname, each.Surname);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailPhoneWk, each.WorkPhoneNumber.PhoneNumber);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailPhoneHm, each.HomePhoneNumber.PhoneNumber);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailPhoneMb, each.MobilePhoneNumber.MobileNumber);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailDOBDay, each.DOB.Value.Day.ToString());
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailDOBMonth, each.DOB.Value.Month.ToString());
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailDOBYear, each.DOB.Value.Year.ToString());
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailEmail, each.EmailAddress);

                    obj.SetColumnValue(obj.Col_AddressAddress1, each.ResidentialAddress.Addressline1);
                    obj.SetColumnValue(obj.Col_AddressAddress2, each.ResidentialAddress.Addressline2);
                    obj.SetColumnValue(obj.Col_AddressAddress3, "");
                    obj.SetColumnValue(obj.Col_AddressSuburb, each.ResidentialAddress.Suburb);
                    obj.SetColumnValue(obj.Col_AddressState, each.ResidentialAddress.State);
                    obj.SetColumnValue(obj.Col_AddressPostcode, each.ResidentialAddress.PostCode);
                    obj.SetColumnValue(obj.Col_AddressCountry, each.ResidentialAddress.Country);

                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailPhoneHmPrefix, each.HomePhoneNumber.CityCode);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailPhonewkPrefix, each.WorkPhoneNumber.CityCode);

                    obj.SetColumnValue(obj.Col_TFNDetailTFN_2, "");
                    obj.SetColumnValue(obj.Col_TFNDetailNRC_2, "");
                    obj.SetColumnValue(obj.Col_TFNDetailExemptionCategory_2, "");
                    obj.SetColumnValue(obj.Col_TFNDetailTIN_2, "");
                }

                if (i == 2)
                {
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailOccupation_2, each.Occupation);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailEmployer_2, each.Employer);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailCountryOfResidence_2, each.ResidentialAddress.Country);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailTitle_2, each.PersonalTitle.Title);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailTitleOther_2, "");
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailGivenName_2, each.Name);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailSurname_2, each.Surname);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailPhoneWk_2, each.WorkPhoneNumber.PhoneNumber);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailPhoneHm_2, each.HomePhoneNumber.PhoneNumber);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailPhoneMb_2, each.MobilePhoneNumber.MobileNumber);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailDOBDay_2, each.DOB.Value.Day.ToString());
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailDOBMonth_2, each.DOB.Value.Month.ToString());
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailDOBYear_2, each.DOB.Value.Year.ToString());
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailEmail_2, each.EmailAddress);

                    obj.SetColumnValue(obj.Col_AddressAddress1_2, each.ResidentialAddress.Addressline1);
                    obj.SetColumnValue(obj.Col_AddressAddress2_2, each.ResidentialAddress.Addressline2);
                    obj.SetColumnValue(obj.Col_AddressAddress3_2, "");
                    obj.SetColumnValue(obj.Col_AddressSuburb_2, each.ResidentialAddress.Suburb);
                    obj.SetColumnValue(obj.Col_AddressState_2, each.ResidentialAddress.State);
                    obj.SetColumnValue(obj.Col_AddressPostcode_2, each.ResidentialAddress.PostCode);
                    obj.SetColumnValue(obj.Col_AddressCountry_2, each.ResidentialAddress.Country);

                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailPhoneHmPrefix_2, each.HomePhoneNumber.CityCode);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailPhonewkPrefix_2, each.WorkPhoneNumber.CityCode);

                    obj.SetColumnValue(obj.Col_TFNDetailTFN_3, "");
                    obj.SetColumnValue(obj.Col_TFNDetailNRC_3, "");
                    obj.SetColumnValue(obj.Col_TFNDetailExemptionCategory_3, "");
                    obj.SetColumnValue(obj.Col_TFNDetailTIN_3, "");
                }

                if (i == 3)
                {
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailOccupation_3, each.Occupation);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailEmployer_3, each.Employer);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailCountryOfResidence_3, each.ResidentialAddress.Country);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailTitle_3, each.PersonalTitle.Title);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailTitleOther_3, each.PersonalTitle.Title);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailGivenName_3, each.Name);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailSurname_3, each.Surname);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailPhoneWk_3, each.WorkPhoneNumber.PhoneNumber);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailPhoneHm_3, each.HomePhoneNumber.PhoneNumber);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailPhoneMb_3, each.MobilePhoneNumber.MobileNumber);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailDOBDay_3, each.DOB.Value.Day.ToString());
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailDOBMonth_3, each.DOB.Value.Month.ToString());
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailDOBYear_3, each.DOB.Value.Year.ToString());
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailEmail_3, each.EmailAddress);

                    obj.SetColumnValue(obj.Col_AddressAddress1_3, each.ResidentialAddress.Addressline1);
                    obj.SetColumnValue(obj.Col_AddressAddress2_3, each.ResidentialAddress.Addressline2);
                    obj.SetColumnValue(obj.Col_AddressAddress3_3, "");
                    obj.SetColumnValue(obj.Col_AddressSuburb_3, each.ResidentialAddress.Suburb);
                    obj.SetColumnValue(obj.Col_AddressState_3, each.ResidentialAddress.State);
                    obj.SetColumnValue(obj.Col_AddressPostcode_3, each.ResidentialAddress.PostCode);
                    obj.SetColumnValue(obj.Col_AddressCountry_3, each.ResidentialAddress.Country);

                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailPhoneHmPrefix_3, each.HomePhoneNumber.CityCode);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailPhonewkPrefix_3, each.WorkPhoneNumber.CityCode);

                    obj.SetColumnValue(obj.Col_TFNDetailTFN_4, "");
                    obj.SetColumnValue(obj.Col_TFNDetailNRC_4, "");
                    obj.SetColumnValue(obj.Col_TFNDetailExemptionCategory_4, "");
                    obj.SetColumnValue(obj.Col_TFNDetailTIN_4, "");
                }

                if (i == 4)
                {
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailOccupation_4, each.Occupation);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailEmployer_4, each.Employer);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailCountryOfResidence_4, each.ResidentialAddress.Country);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailTitle_4, each.PersonalTitle.Title);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailTitleOther_4, each.PersonalTitle.Title);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailGivenName_4, each.Name);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailSurname_4, each.Surname);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailPhoneWk_4, each.WorkPhoneNumber.PhoneNumber);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailPhoneHm_4, each.HomePhoneNumber.PhoneNumber);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailPhoneMb_4, each.MobilePhoneNumber.MobileNumber);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailDOBDay_4, each.DOB.Value.Day.ToString());
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailDOBMonth_4, each.DOB.Value.Month.ToString());
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailDOBYear_4, each.DOB.Value.Year.ToString());
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailEmail_4, each.EmailAddress);

                    obj.SetColumnValue(obj.Col_AddressAddress1_4, each.ResidentialAddress.Addressline1);
                    obj.SetColumnValue(obj.Col_AddressAddress2_4, each.ResidentialAddress.Addressline2);
                    obj.SetColumnValue(obj.Col_AddressAddress3_4, "");
                    obj.SetColumnValue(obj.Col_AddressSuburb_4, each.ResidentialAddress.Suburb);
                    obj.SetColumnValue(obj.Col_AddressState_4, each.ResidentialAddress.State);
                    obj.SetColumnValue(obj.Col_AddressPostcode_4, each.ResidentialAddress.PostCode);
                    obj.SetColumnValue(obj.Col_AddressCountry_4, each.ResidentialAddress.Country);

                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailPhoneHmPrefix_4, each.HomePhoneNumber.CityCode);
                    obj.SetColumnValue(obj.Col_SignatoryPersonalDetailPhonewkPrefix_4, each.WorkPhoneNumber.CityCode);

                    obj.SetColumnValue(obj.Col_TFNDetailTFN_5, "");
                    obj.SetColumnValue(obj.Col_TFNDetailNRC_5, "");
                    obj.SetColumnValue(obj.Col_TFNDetailExemptionCategory_5, "");
                    obj.SetColumnValue(obj.Col_TFNDetailTIN_5, "");
                }

                i++;
            }
            obj.SetColumnValue(obj.Col_CorrespondenceAddressAddress1, _entitycsv.Address.BusinessAddress.Addressline1);
            obj.SetColumnValue(obj.Col_CorrespondenceAddressAddress2, _entitycsv.Address.BusinessAddress.Addressline2);
            obj.SetColumnValue(obj.Col_CorrespondenceAddressAddress3, "");
            obj.SetColumnValue(obj.Col_CorrespondenceAddressSuburb, _entitycsv.Address.BusinessAddress.Suburb);
            obj.SetColumnValue(obj.Col_CorrespondenceAddressState, _entitycsv.Address.BusinessAddress.State);
            obj.SetColumnValue(obj.Col_CorrespondenceAddressPostcode, _entitycsv.Address.BusinessAddress.PostCode);
            obj.SetColumnValue(obj.Col_CorrespondenceAddressCountry, "Australia");
            obj.SetColumnValue(obj.Col_CorrespondenceAddressCareOf, "");
            if (!_entitycsv.Address.MailingAddressSame)
            {

                obj.SetColumnValue(obj.Col_CorrespondenceAddressAddress1_2, _entitycsv.Address.MailingAddress.Addressline1);
                obj.SetColumnValue(obj.Col_CorrespondenceAddressAddress2_2, _entitycsv.Address.MailingAddress.Addressline2);
                obj.SetColumnValue(obj.Col_CorrespondenceAddressAddress3_2, "");
                obj.SetColumnValue(obj.Col_CorrespondenceAddressSuburb_2, _entitycsv.Address.MailingAddress.Suburb);
                obj.SetColumnValue(obj.Col_CorrespondenceAddressState_2, _entitycsv.Address.MailingAddress.State);
                obj.SetColumnValue(obj.Col_CorrespondenceAddressPostcode_2, _entitycsv.Address.MailingAddress.PostCode);
                obj.SetColumnValue(obj.Col_CorrespondenceAddressCountry_2, "Australia");
                obj.SetColumnValue(obj.Col_CorrespondenceAddressCareOf_2, "");
            }
            if (!_entitycsv.Address.DuplicateMailingAddressSame)
            {
                obj.SetColumnValue(obj.Col_CorrespondenceAddressAddress1_3, _entitycsv.Address.DuplicateMailingAddress.Addressline1);
                obj.SetColumnValue(obj.Col_CorrespondenceAddressAddress2_3, _entitycsv.Address.DuplicateMailingAddress.Addressline2);
                obj.SetColumnValue(obj.Col_CorrespondenceAddressAddress3_3, "");
                obj.SetColumnValue(obj.Col_CorrespondenceAddressSuburb_3, _entitycsv.Address.DuplicateMailingAddress.Suburb);
                obj.SetColumnValue(obj.Col_CorrespondenceAddressState_3, _entitycsv.Address.DuplicateMailingAddress.State);
                obj.SetColumnValue(obj.Col_CorrespondenceAddressPostcode_3, _entitycsv.Address.DuplicateMailingAddress.PostCode);
                obj.SetColumnValue(obj.Col_CorrespondenceAddressCountry_3, "Australia");
                obj.SetColumnValue(obj.Col_CorrespondenceAddressCareOf_3, "");
            }
            obj.SetColumnValue(obj.Col_TrusteesCompanyCIF, "");
            obj.SetColumnValue(obj.Col_CorpTrusteeCIF, "");
            obj.SetColumnValue(obj.Col_SignatoryCIF, "");
            obj.SetColumnValue(obj.Col_SignatoryCIF_2, "");
            obj.SetColumnValue(obj.Col_SignatoryCIF_3, "");
            obj.SetColumnValue(obj.Col_SignatoryCIF_4, "");
            obj.SetColumnValue(obj.Col_PAN, "");
            obj.SetColumnValue(obj.Col_PAN_2, "");
            obj.SetColumnValue(obj.Col_PAN_3, "");
            obj.SetColumnValue(obj.Col_PAN_4, "");
            obj.SetColumnValue(obj.Col_CBSStatus, "");
            obj.SetColumnValue(obj.Col_Allocation, "");
            obj.SetColumnValue(obj.Col_FollowupDate, "");
            obj.SetColumnValue(obj.Col_OperationsStatus, "");
            obj.SetColumnValue(obj.Col_OpenAccountApplicationPartnerCIF, "");
            obj.SetColumnValue(obj.Col_OpenAccountApplicationAdviserCIF, "");
            obj.SetColumnValue(obj.Col_PlatformName, "");
            obj.SetColumnValue(obj.Col_PlatformCode, "");
            obj.SetColumnValue(obj.Col_PlatformReference1, "");
            obj.SetColumnValue(obj.Col_PlatformReference2, "");
            obj.SetColumnValue(obj.Col_PlatformName_2, "");
            obj.SetColumnValue(obj.Col_PlatformCode_2, "");
            obj.SetColumnValue(obj.Col_PlatformReference1_2, "");
            obj.SetColumnValue(obj.Col_PlatformReference2_2, "");
            obj.SetColumnValue(obj.Col_PlatformName_3, "");
            obj.SetColumnValue(obj.Col_PlatformCode_3, "");
            obj.SetColumnValue(obj.Col_PlatformReference1_3, "");
            obj.SetColumnValue(obj.Col_PlatformReference2_3, "");
            obj.SetColumnValue(obj.Col_ThirdPartyCIFKey, "");
            obj.SetColumnValue(obj.Col_ThirdPartyPartnerCode, "");
            obj.SetColumnValue(obj.Col_OpenAccountApplicationFirmName, "");
            obj.SetColumnValue(obj.Col_OpenAccountApplicationAdvisorFirmName, "");
            obj.SetColumnValue(obj.Col_ReportedOn, "");
            obj.SetColumnValue(obj.Col_ProcessingResult, "");
            obj.SetColumnValue(obj.Col_ProcessingIssues, "");
            obj.SetColumnValue(obj.Col_ApplicationReceivedDate, "");
            obj.SetColumnValue(obj.Col_DocumentationOutstandingDate, "");
            obj.SetColumnValue(obj.Col_FinalisedDate, "");
            obj.SetColumnValue(obj.Col_OpenAccountApplicationProductCode, "BFSMMA");
            obj.SetColumnValue(obj.Col_OpenAccountApplicationProductCostCenter, "");
            obj.SetColumnValue(obj.Col_TS_AccountDetails, "");

            return obj.ToCSV();
        }

        public string GetCsvData_StateStreet()
        {
            if (IsExportedSS == true) return "";
            AOIDCSVColumns obj = new AOIDCSVColumns();
            CSVInstance csvinstance = new CSVInstance(this.Broker);
            CorporateCSV _entitycsv = csvinstance.GetEntityConcreteInstance(_entity);

            obj.SetColumnValue(obj.Col_UnitholderCode, _entitycsv.ClientId);

            string UnitholderSubCode = "IV";

            if (string.IsNullOrEmpty(_entitycsv.ClientId) && _entitycsv.ClientId.Length > 2)
            {
                UnitholderSubCode = _entitycsv.ClientId.Substring(0, 2);
            }

            obj.SetColumnValue(obj.Col_UnitholderSubCode, UnitholderSubCode);

            //  obj.SetColumnValue(obj.Col_UnitholderSubCode, _entitycsv.ClientId);
            string temp1 = "";
            string temp2 = "";
            _entitycsv.Name.SplitInTwo(35, "atf", ref temp1, ref temp2);
            obj.SetColumnValue(obj.Col_UnitholderName1, temp1);
            obj.SetColumnValue(obj.Col_UnitholderName2, temp2);

            obj.SetColumnValue(obj.Col_PostalAddress1, "C/- Innova Portfolio Management P/L");
            obj.SetColumnValue(obj.Col_PostalAddress2, "3/36 Bydown Street");
            obj.SetColumnValue(obj.Col_PostalAddress3, "NEUTRAL BAY  NSW");
            obj.SetColumnValue(obj.Col_PostalAddress4, "");
            obj.SetColumnValue(obj.Col_PostCode, "2089");

            //if (!_entitycsv.Address.MailingAddressSame)
            //{
            //    obj.SetColumnValue(obj.Col_PostalAddress1, _entitycsv.Address.MailingAddress.Addressline1);
            //    obj.SetColumnValue(obj.Col_PostalAddress2, _entitycsv.Address.MailingAddress.Addressline2);
            //    obj.SetColumnValue(obj.Col_PostalAddress3, _entitycsv.Address.MailingAddress.Addressline1);
            //    obj.SetColumnValue(obj.Col_PostalAddress4, _entitycsv.Address.MailingAddress.Addressline2);
            //    obj.SetColumnValue(obj.Col_PostCode, _entitycsv.Address.MailingAddress.PostCode);
            //}
            //else
            //{
            //    obj.SetColumnValue(obj.Col_PostalAddress1, _entitycsv.Address.BusinessAddress.Addressline1);
            //    obj.SetColumnValue(obj.Col_PostalAddress2, _entitycsv.Address.BusinessAddress.Addressline2);
            //    obj.SetColumnValue(obj.Col_PostalAddress3, _entitycsv.Address.BusinessAddress.Addressline1);
            //    obj.SetColumnValue(obj.Col_PostalAddress4, _entitycsv.Address.BusinessAddress.Addressline2);
            //    obj.SetColumnValue(obj.Col_PostCode, _entitycsv.Address.MailingAddress.PostCode);
            //}
            obj.SetColumnValue(obj.Col_PhoneNumber1, "");
            obj.SetColumnValue(obj.Col_PhoneNumber2, "");
            obj.SetColumnValue(obj.Col_FaxNumber, "");
            if (_entitycsv.Contacts.Count > 0)
            {
                //obj.SetColumnValue(obj.Col_PhoneNumber1, _entitycsv.Contacts[0].WorkPhoneNumber.PhoneNumber);
                //obj.SetColumnValue(obj.Col_PhoneNumber2, _entitycsv.Contacts[0].HomePhoneNumber.PhoneNumber);
                //obj.SetColumnValue(obj.Col_FaxNumber, _entitycsv.Contacts[0].Facsimile.PhoneNumber);
                obj.SetColumnValue(obj.Col_EmailAddress, _entitycsv.Contacts[0].EmailAddress);
            }

            obj.SetColumnValue(obj.Col_ShortName, "");
            obj.SetColumnValue(obj.Col_DateAccountOpened, DateTime.Now.ToString("yyyyMMdd"));
            //  obj.SetColumnValue(obj.Col_DateAccountOpened, this.Entity.Name);
            obj.SetColumnValue(obj.Col_UnitholderInvestorType, "CO");
            obj.SetColumnValue(obj.Col_DistributionOption, "00");
            obj.SetColumnValue(obj.Col_CategoryCode3, "RETL");
            //obj.SetColumnValue(obj.Col_StatementFrequency, "");
            //obj.SetColumnValue(obj.Col_AnnualReport, "");
            obj.SetColumnValue(obj.Col_Active, "Y");
            obj.SetColumnValue(obj.Col_Units, "0");

            obj.SetColumnValue(obj.Col_InvestorType, "CO");

            //if (_entitycsv.Signatories.Count > 0)
            //{
            //    obj.SetColumnValue(obj.Col_FirstName, _entitycsv.Signatories[0].Name);
            //    obj.SetColumnValue(obj.Col_MiddleName, "");
            //    obj.SetColumnValue(obj.Col_LastName, _entitycsv.Signatories[0].Surname);
            //    obj.SetColumnValue(obj.Col_DateofBirth, _entitycsv.Signatories[0].DOB.ToString());
            //    obj.SetColumnValue(obj.Col_Gender, "U");
            //}
            this._entity.LegalName.SplitInTwo(50, "atf", ref temp1, ref temp2);
            obj.SetColumnValue(obj.Col_Name1, temp1);
            //obj.SetColumnValue(obj.Col_Name2, temp2);
            if (temp2.ToLower().Contains("superannuation") || temp2.ToLower().Contains("super annuation"))
            {
                temp2 = temp2.Replace("Superannuation", "Super");
                temp2 = temp2.Replace("SuperAnnuation", "Super");
                temp2 = temp2.Replace("superannuation", "Super");
                temp2 = temp2.Replace("Super annuation", "Super");
                temp2 = temp2 = temp2.Replace("Super Annuation", "Super");
                temp2 = temp2.Replace("super annuation", "Super");
                obj.SetColumnValue(obj.Col_Name2, temp2);
            }
            else
            {
                obj.SetColumnValue(obj.Col_Name2, temp2);
            }

            obj.SetColumnValue(obj.Col_Name3, "");
            obj.SetColumnValue(obj.Col_Name4, "");



            Oritax.TaxSimp.Common.AddressEntity address = new Oritax.TaxSimp.Common.AddressEntity();

            if (_entitycsv.Address.BusinessAddress != null && !string.IsNullOrEmpty(_entitycsv.Address.BusinessAddress.Addressline1))
                address = _entitycsv.Address.BusinessAddress;
            else if (_entitycsv.Address.MailingAddress != null && !string.IsNullOrEmpty(_entitycsv.Address.MailingAddress.Addressline1))
                address = _entitycsv.Address.MailingAddress;
            else if (_entitycsv.Address.DuplicateMailingAddress != null && !string.IsNullOrEmpty(_entitycsv.Address.DuplicateMailingAddress.Addressline1))
                address = _entitycsv.Address.DuplicateMailingAddress;
            else if (_entitycsv.Address.RegisteredAddress != null && !string.IsNullOrEmpty(_entitycsv.Address.RegisteredAddress.Addressline1))
                address = _entitycsv.Address.RegisteredAddress;


            obj.SetColumnValue(obj.Col_Address1, address.Addressline1);
            obj.SetColumnValue(obj.Col_Address2, address.Addressline2);

            obj.SetColumnValue(obj.Col_City, address.Suburb);
            obj.SetColumnValue(obj.Col_State, address.State);
            obj.SetColumnValue(obj.Col_PostCode2, address.PostCode);
            obj.SetColumnValue(obj.Col_Country, address.Country);
            obj.SetColumnValue(obj.Col_TaxCountry, address.Country);
            //obj.SetColumnValue(obj.Col_Address1, _entitycsv.Address.BusinessAddress.Addressline1);
            //obj.SetColumnValue(obj.Col_Address2, _entitycsv.Address.BusinessAddress.Addressline2);

            //obj.SetColumnValue(obj.Col_City, _entitycsv.Address.BusinessAddress.Suburb);
            //obj.SetColumnValue(obj.Col_State, _entitycsv.Address.BusinessAddress.State);
            //obj.SetColumnValue(obj.Col_PostCode2, _entitycsv.Address.BusinessAddress.PostCode);
            //obj.SetColumnValue(obj.Col_Country, _entitycsv.Address.BusinessAddress.Country);
            //obj.SetColumnValue(obj.Col_TaxCountry, _entitycsv.Address.BusinessAddress.Country);
            obj.SetColumnValue(obj.Col_TFN_ABN, _entitycsv.ABN);

            ICalculationModule objOrganisation = Broker.GetWellKnownCM(WellKnownCM.Organization);
            IModelStructure organization = (objOrganisation as IModelStructure);
            Oritax.TaxSimp.Common.BankAccountEntity bankAccount = null;

            if (_entitycsv.Servicetype.DO_IT_FOR_ME)
            {
                if (organization != null)
                {
                    var model = organization.Model.FirstOrDefault(e => e.ServiceType == ServiceTypes.DoItForMe && e.ProgramCode == _entitycsv.Servicetype.DoItForMe.ProgramCode);

                    if (model != null)
                    {
                        bankAccount = GetCmBankAccount(_entitycsv, csvinstance, model);


                    }
                }
            }

            if (bankAccount == null && _entitycsv.Servicetype.DO_IT_WITH_ME)
            {


                bankAccount = GetCmBankAccount(_entitycsv, csvinstance, _entitycsv.Servicetype.DoItWithMe.Model);



            }
            if (bankAccount == null && _entitycsv.Servicetype.DO_IT_YOURSELF)
            {


                bankAccount = GetCmBankAccount(_entitycsv, csvinstance, _entitycsv.Servicetype.DoItYourSelf.Model);

            }


            if (bankAccount != null)
                SetbankAccountValues(temp1, temp2, obj, bankAccount);
            //else
            //    if (_entitycsv.BankAccounts.Count > 0)
            //        {
            //        SetbankAccountValues(temp1, temp2, obj, _entitycsv.BankAccounts[0]);
            //        }





            return obj.ToCSV();
        }

        private static void SetbankAccountValues(string temp1, string temp2, AOIDCSVColumns obj, Oritax.TaxSimp.Common.BankAccountEntity _entitycsv)
        {
            obj.SetColumnValue(obj.Col_BankAccountType, "ACH02");
            //  obj.SetColumnValue(obj.Col_BankAccountType, _entitycsv.BankAccounts[0].AccoutType);
            obj.SetColumnValue(obj.Col_BSB, _entitycsv.BSB);
            obj.SetColumnValue(obj.Col_BankAccountNumber, _entitycsv.AccountNumber);


            _entitycsv.Name.SplitInTwo(35, "atf", ref temp1, ref temp2);

            obj.SetColumnValue(obj.Col_BankAccountName, temp1);
            //obj.SetColumnValue(obj.Col_BankAccountName2, temp2);
            if (temp2.ToLower().Contains("superannuation") || temp2.ToLower().Contains("super annuation"))
            {
                temp2 = temp2.Replace("Superannuation", "Super");
                temp2 = temp2.Replace("SuperAnnuation", "Super");
                temp2 = temp2.Replace("superannuation", "Super");
                temp2 = temp2.Replace("Super annuation", "Super");
                temp2 = temp2.Replace("Super Annuation", "Super");
                temp2 = temp2.Replace("super annuation", "Super");
                obj.SetColumnValue(obj.Col_BankAccountName2, temp2);
            }
            else
            {
                obj.SetColumnValue(obj.Col_BankAccountName2, temp2);
            }
        }

        private static Oritax.TaxSimp.Common.BankAccountEntity GetCmBankAccount(CorporateCSV _entitycsv, CSVInstance csvinstance, ModelEntity model)
        {
            Oritax.TaxSimp.Common.BankAccountEntity bankAccount = null;
            if (model != null)
                foreach (var asset in model.Assets)
                {
                    foreach (var product in asset.Products)
                    {
                        var task =
                            _entitycsv.ListAccountProcess.Where(
                                e => e.ModelID == model.ID && e.AssetID == asset.ID && e.TaskID == product.ID).FirstOrDefault();
                        // new TaskEntity() { ModelID = model.ID, AssetID = asset.ID, ServiceType = model.ServiceType.GetDescription(), TaskDescription = product.TaskDescription, Status = "", IsCompleted = task.IsCompleted, TaskID = product.ID, LinkedEntity = task.LinkedEntity, LinkedEntityType = product.EntityType, AssetName = asset.Name, ProductName = product.Name, IsExempted = task.IsExempted, OldStatus = "", Type = model.ServiceType }
                        if (!string.IsNullOrEmpty(product.BankAccountType) && product.BankAccountType.Trim().ToLower() == "cma")
                        {
                            bankAccount = csvinstance.GetConcreteBankAccountEntityObject(task.LinkedEntity);
                            break;
                        }
                    }
                }
            return bankAccount;
        }
        #endregion

        #region Security

        private string GetEntitySecurity()
        {
            EntitySecurity entity = new EntitySecurity();

            PartyDS ds = new PartyDS();
            base.OnGetData(ds);
            foreach (DataRow row in ds.Tables[PartyDS.INCLUDEDPARTY_TABLE].Rows)
            {
                entity.IncludedUsers.Add(new UserEntity() { CID = new Guid(row[PartyDS.PARTYCID_FIELD].ToString()), CurrentUserName = row[PartyDS.PARTYNAME_FIELD].ToString() });
            }
            foreach (DataRow row in ds.Tables[PartyDS.AVAILABLEPARTY_TABLE].Rows)
            {
                if (row[PartyDS.PARTYNAME_FIELD].ToString().ToLower() != "Administrator, Administrator".ToLower())
                {
                    entity.AvailableUsers.Add(new UserEntity() { CID = new Guid(row[PartyDS.PARTYCID_FIELD].ToString()), CurrentUserName = row[PartyDS.PARTYNAME_FIELD].ToString() });
                }
            }

            return entity.ToXmlString();
        }

        //private string AddEntitySecurity(string UserEntity)
        //{
        //    UserEntity newentity = UserEntity.ToData<UserEntity>();

        //    this.Broker.SetWriteStart();

        //    PartyDS ds = new PartyDS();
        //    base.OnGetData(ds);

        //    DataRow partyRow = ds.Tables[PartyDS.INCLUDEDPARTY_TABLE].NewRow();
        //    partyRow[PartyDS.PARTYNAME_FIELD] = newentity.CurrentUserName;
        //    partyRow[PartyDS.PARTYCID_FIELD] = newentity.CID;
        //    ds.Tables[PartyDS.INCLUDEDPARTY_TABLE].Rows.Add(partyRow);

        //    this.SetData(ds);

        //    return string.Empty;
        //}

        private string DeleteEntitySecurity(string UserEntity)
        {
            UserEntity entity = UserEntity.ToData<UserEntity>();

            this.Broker.SetWriteStart();

            PartyDS ds = new PartyDS();
            base.OnGetData(ds);
            ds.AcceptChanges();

            DataRow[] drCollection = ds.Tables[PartyDS.INCLUDEDPARTY_TABLE].Select(PartyDS.PARTYCID_FIELD + " = '" + entity.CID + "'", String.Empty);
            drCollection[0].Delete();
            this.SetData(ds);

            return string.Empty;
        }

        #endregion

        protected override ModifiedState OnSetData(DataSet data)
        {
            FinancialDataUtility FinancialDataUtility = new FinancialDataUtility();

            base.OnSetData(data);

            if (data is ManualTransactionDetailsDS)
            {
                FinancialDataUtility.SetManualTransactionDetail((ManualTransactionDetailsDS)data, this.Entity.ClientManualAssetCollection);
            }
            else if (data is DivTransactionDetailsDS)
            {
                DivTransactionDetailsDS divTransactionDetailsDS = data as DivTransactionDetailsDS;
                FinancialDataUtility.SetDividendTransactionDetails(this.Entity, divTransactionDetailsDS, this.Broker);
            }
            else if (data is DeleteMISTransactionDS)
            {
                IOrganization orgCM = this.Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                FinancialDataUtility.ClearIV004Transactions(orgCM, Entity.ListAccountProcess, this.Broker, this.ClientId);
                return ModifiedState.MODIFIED;
            }
            else if (data is ClientAccountProcessDS)
            {
                var ds = data as ClientAccountProcessDS;
                if (ds.Command == DatasetCommandTypes.Delete)
                    ClientUtilities.RemoveAccountProcessAssociation(ds, this.Entity.ListAccountProcess);
                else if (ds.Command == DatasetCommandTypes.Update)
                    ClientUtilities.AssociateAccountProcess(ds, Entity.ListAccountProcess, Entity.BankAccounts, Entity.DesktopBrokerAccounts, Entity.ManagedInvestmentSchemesAccounts);
            }
            else if (data is DesktopBrokerAccountDS)
            {
                var ds = data as DesktopBrokerAccountDS;
                if (ds.CommandType == DatasetCommandTypes.Delete)
                    ClientUtilities.RemoveASXAccount(ds, Entity.DesktopBrokerAccounts, Entity.ListAccountProcess, Broker);
                else if (ds.CommandType == DatasetCommandTypes.Update)
                    ClientUtilities.AssociateASXAccount(ds, Entity.DesktopBrokerAccounts);
            }
            else if (data is BankAccountDS)
            {
                var ds = data as BankAccountDS;
                if (ds.CommandType == DatasetCommandTypes.Delete)
                    ClientUtilities.RemoveBankAccount(ds, Entity.BankAccounts, Entity.ListAccountProcess, Broker);
                else if (ds.CommandType == DatasetCommandTypes.Update)
                    ClientUtilities.AssociateBankAccount(ds, Entity.BankAccounts);
            }
            else if (data is TermDepositDS)
            {
                var ds = data as TermDepositDS;
                if (ds.CommandType == DatasetCommandTypes.Delete)
                    ClientUtilities.RemoveTermDepositAccount(ds, Entity.TermDepositAccounts);
                else if (ds.CommandType == DatasetCommandTypes.Update)
                    ClientUtilities.AssociateTermDepositAccount(ds, Entity.TermDepositAccounts);
            }
            else if (data is ManagedInvestmentAccountDS)
            {
                var ds = data as ManagedInvestmentAccountDS;
                if (ds.CommandType == DatasetCommandTypes.Delete)
                    MISFundUtilities.RemoveAccount(ds, Entity.ManagedInvestmentSchemesAccounts, Entity.ListAccountProcess, Broker);
                else if (ds.CommandType == DatasetCommandTypes.Add)
                {

                    MISFundUtilities.AssociateAccount(ds, Entity.ManagedInvestmentSchemesAccounts);
                }
            }
            else if (data is ClientDistributionsDS)
            {
                var ds = data as ClientDistributionsDS;
                if (ds.CommandType == DatasetCommandTypes.Delete)
                    ClientUtilities.RemoveDistAccount(ds, Entity.DistributionIncomes);

            }
            else if (data is ClientDistributionDetailsDS)
            {
                var clientDistributionDs = data as ClientDistributionDetailsDS;
                clientDistributionDs.ClientID = this.ClientId;
                FinancialDataUtility.UpdateClientDistribution(clientDistributionDs, Entity.DistributionIncomes, Broker, Entity.BankAccounts, Entity.ManagedInvestmentSchemesAccounts, EclipseClientID);; 
            }
            else if (data is ClientModelsDetailsDS)
            {
                ClientModelsDetailsDS modelDs = data as ClientModelsDetailsDS;
                if (Entity.ManagedInvestmentSchemesAccounts == null)
                    Entity.ManagedInvestmentSchemesAccounts = new List<IdentityCMDetail>();
                ClientUtilities.SetClientlModelsInfo(modelDs, Entity.Servicetype, this.ServiceType, Entity.ListAccountProcess, Broker, Entity.ManagedInvestmentSchemesAccounts);
            }
            else if (data is OtherServicesDS)
            {
                OtherServicesDS otherServicesDs = data as OtherServicesDS;
                ClientUtilities.SetOtherServices(otherServicesDs, Entity);
            }
            else if (data is ExportStatusDS)
            {
                var statuses = ExportUtilities.SetExportStatusDetails(data as ExportStatusDS, CID, IsExportedSS, IsExported, IsExportedDB);
                IsExportedSS = statuses.IsExportedSs;
                IsExported = statuses.IsExportedBw;
                IsExportedDB = statuses.IsExportedDb;
            }

            else if (data is UpdateClientModelsDS)
            {
                var ds = data as UpdateClientModelsDS;
                switch (ds.ModelDataSetOperationType)
                {
                    case ModelDataSetOperationType.UpdateModel:
                        ClientUtilities.SetClientModels(ds, Entity);
                        break;

                }

            }
            else if (data is ImportProcessDS)
            {
                var ds = data as ImportProcessDS;
                switch ((WebCommands)ds.Command)
                {
                    case WebCommands.ImportBankwestDatafeedImportFile:
                        BankAccountUtilities.SetDividendsTransactionsID(ds, Entity.BankAccounts, Entity.DividendCollection, this);
                        BankAccountUtilities.SetDistributionsTransactionsID(ds, Entity.BankAccounts, Entity.DistributionIncomes, this);
                        break;
                    case WebCommands.ImportAtCallTransactionsImportFiles:
                        BankAccountUtilities.ImportAtCallTransactions(ClientId, this.Name, CID, ds, Entity.BankAccounts, Entity.ListAccountProcess, Broker);
                        break;
                }
            }
            else if (data is ClientDividendDetailsDS)
            {
                var ds = data as ClientDividendDetailsDS;
                ClientUtilities.SetClientDividendDs(ds, Entity, Broker);
            }

            else if (data is AddressDetailsDS)
            {
                var ds = data as AddressDetailsDS;
                ClientUtilities.SetClientAddressDetail(ds, Entity);
            }

            return ModifiedState.MODIFIED;
        }

        public override string ClientAccountType
        {
            get
            {
                return "Corporation Public";
            }
        }
        public override string ClientApplicabilityType
        {
            get
            {
                return "Company";
            }
        }

        #region OnGetData

        protected override void OnGetData(DataSet data)
        {
            base.OnGetData(data);

            FinancialDataUtility FinancialDataUtility = new FinancialDataUtility();
            HoldingDataReportUtility HoldingDataReportUtility = new HoldingDataReportUtility();
            CapitalFlowReportUtility CapitalFlowReportUtility = new CapitalFlowReportUtility();
            PerformanceFlowReportUtility PerformanceFlowReportUtility = new PerformanceFlowReportUtility();
            SecurityDataReportUtility securityDataReportUtility = new SecurityDataReportUtility();
            GainsAndLossReportUtility gainsAndLossReportUtility = new GainsAndLossReportUtility();
            HoldingStatementReportUtility holdingStatementReportUtility = new HoldingStatementReportUtility();

            if (data is HoldingRptDataSet)
            {
                HoldingRptDataSet holdingRptDataSet = data as HoldingRptDataSet;
                HoldingDataReportUtility.OnGetDataHoldingRptDataSetCorporate(holdingRptDataSet, ClientAccountType, this.ClientId, this.name, this.Parent, this.Broker, this.Entity, ClientApplicabilityType, CID);
            }
            else if (data is CashFlowReportDS)
            {

                CapitalFlowReportUtility.GetClientCashFlowReportPerAccount(data as CashFlowReportDS, this.name, this.ClientId, this.CID, typeName, this.Parent, Entity.ListAccountProcess, Broker);
            }
            else if (data is BankTransactionDetailsFixDS)
            {
                IOrganization orgCM = this.Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                BankTransactionDetailsFixDS bankTransactionDetailsFixDS = data as BankTransactionDetailsFixDS;
                FinancialDataUtility.SetBankAccountDataSetTransactionFix(bankTransactionDetailsFixDS, orgCM, Entity.ListAccountProcess, Broker);
                this.Broker.ReleaseBrokerManagedComponent(orgCM);
            }
            else if (data is PerfReportDS)
            {
                PerfReportDS perfReportDS = data as PerfReportDS;
                PerformanceFlowReportUtility.OnGetDataCapitalFlowDataSetCorporate(perfReportDS, ClientAccountType, this.ClientId, this.name, this.Parent, this.Broker, this.Entity);
            }
            else if (data is SecuritySummaryReportDS)
            {
                SecuritySummaryReportDS securityPerfReportDS = data as SecuritySummaryReportDS;
                securityDataReportUtility.OnGetDataHoldingRptDataSetCorporate(securityPerfReportDS, ClientAccountType, this.ClientId, this.name, this.Parent, this.Broker, this.Entity, this.CID);
            }
            else if (data is HoldingSummaryReportDS)
            {
                HoldingSummaryReportDS securityPerfReportDS = data as HoldingSummaryReportDS;
                holdingStatementReportUtility.OnGetDataHoldingRptDataSetCorporate(securityPerfReportDS, ClientAccountType, this.ClientId, CID, this.name, this.Parent, this.Broker, this.Entity);
            }
            else if (data is LossAndGainsReportDS)
            {
                LossAndGainsReportDS lossAndGainsReportDS = data as LossAndGainsReportDS;
                gainsAndLossReportUtility.OnGetDataLossAndGainsReportDSCorporate(this as IOrganizationUnit, lossAndGainsReportDS, ClientAccountType, this.ClientId, this.name, this.Parent, this.Broker, this.Entity);
            }
            else if (data is CapitalReportDS)
            {
                CapitalReportDS capitalReportDS = data as CapitalReportDS;
                CapitalFlowReportUtility.OnGetDataCapitalFlowDataSetCorporate(capitalReportDS, ClientAccountType, this.ClientId, this.name, this.Parent, this.Broker, this.Entity,CID);
            }
            else if (data is ManualTransactionDS)
            {
                ManualTransactionDS manualTransactionDS = data as ManualTransactionDS;
                manualTransactionDS.CLIENTID = this.ClientId;
                manualTransactionDS.CLIENTNAME = this.Name;
                IOrganization orgCM = this.Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                FinancialDataUtility.GetManualTransactionDS(manualTransactionDS, orgCM, Entity.ClientManualAssetCollection, this.Broker);
                this.Broker.ReleaseBrokerManagedComponent(orgCM);
            }
            else if (data is BankTransactionDS)
            {
                BankTransactionDS bankTransactionDS = data as BankTransactionDS;
                bankTransactionDS.CLIENTIDPara = this.ClientId;
                bankTransactionDS.CLIENTNAMEPara = this.Name;
                IOrganization orgCM = this.Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                FinancialDataUtility.GetBankAccountDataSet(bankTransactionDS, orgCM, ClientAccountType, Parent, this.ClientId, this.name, Entity.ListAccountProcess, this.Broker, this.Entity);
                this.Broker.ReleaseBrokerManagedComponent(orgCM);
            }
            else if (data is TDTransactionDS)
            {
                TDTransactionDS tdTransactionDS = data as TDTransactionDS;
                tdTransactionDS.CLIENTIDPara = this.ClientId;
                tdTransactionDS.CLIENTNAMEPara = this.Name;
                IOrganization orgCM = this.Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                FinancialDataUtility.GetTDAccountDataSet(tdTransactionDS, orgCM, ClientAccountType, Parent, this.ClientId, this.name, Entity.ListAccountProcess, this.Broker, this.Entity);
                this.Broker.ReleaseBrokerManagedComponent(orgCM);
            }
            else if (data is ASXTransactionDS)
            {
                ASXTransactionDS asxTransactionDS = data as ASXTransactionDS;
                asxTransactionDS.CLIENTID = this.ClientId;
                asxTransactionDS.CLIENTNAME = this.Name;
                IOrganization orgCM = this.Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                FinancialDataUtility.GetASXTransactionDS(asxTransactionDS, orgCM, this.Entity.DesktopBrokerAccounts, Entity.DividendCollection, this.Broker, this.ClientId);
                this.Broker.ReleaseBrokerManagedComponent(orgCM);
            }
            else if (data is MISTransactionDS)
            {
                MISTransactionDS misTransactionDS = data as MISTransactionDS;

                misTransactionDS.CLIENTID = this.ClientId;
                misTransactionDS.CLIENTNAME = this.Name;


                switch (misTransactionDS.CommandType)
                {
                    case DatasetCommandTypes.Validate: // for filling CID against ClientID
                        ClientUtilities.FillClientCID(misTransactionDS, ClientId, CID);
                        break;
                    default:
                        IOrganization orgCM = this.Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                        FinancialDataUtility.GetMISTransactionDS(misTransactionDS, orgCM, Entity.ListAccountProcess,
                                                                 this.Broker, this.ClientId);
                        this.Broker.ReleaseBrokerManagedComponent(orgCM);
                        break;
                }
            }
            else if (data is ClientDistributionsDS)
            {
                ClientDistributionsDS misTransactionDS = data as ClientDistributionsDS;
                misTransactionDS.CLIENTIDForm = this.ClientId;
                misTransactionDS.CLIENTNAMEForm = this.Name;
                misTransactionDS.DistributionIncomeEntityList = this._entity.DistributionIncomes;
                ClientDetailsUtilities.OnGetDataDataSetClientCorporate(misTransactionDS, ClientAccountType, this.ClientId, this.name, this.Parent, this.Broker, this.Entity);
                FinancialDataUtility.GetClientDistributionsDS(misTransactionDS, Entity.DistributionIncomes, this.Broker, this.ClientId, this.CID, this.name);
            }
            else if (data is ClientTaxDistributionsDS)
            {
                ClientTaxDistributionsDS clientTaxDistibutionsDS = data as ClientTaxDistributionsDS;
                clientTaxDistibutionsDS.CLIENTIDForm = this.ClientId;
                clientTaxDistibutionsDS.CLIENTNAMEForm = this.Name;
                clientTaxDistibutionsDS.DistributionIncomeEntityList = this._entity.DistributionIncomes;
                ClientDetailsUtilities.OnGetDataDataSetClientCorporate(clientTaxDistibutionsDS, ClientAccountType, this.ClientId, this.name, this.Parent, this.Broker, this.Entity);
                FinancialDataUtility.GetClientDistributionsDS(clientTaxDistibutionsDS, Entity.DistributionIncomes, this.Broker);
            }
            else if (data is ClientDistributionDetailsDS)
            {
                ClientDistributionDetailsDS clientDistributionDs = data as ClientDistributionDetailsDS;
                clientDistributionDs.ClientID = this.ClientId;
                FinancialDataUtility.GetClientDistributionDS(clientDistributionDs, Entity.DistributionIncomes, this.Broker);
            }
            else if (data is ClientDividendDetailsDS)
            {
                ClientDividendDetailsDS clientDistributionDs = data as ClientDividendDetailsDS;
                clientDistributionDs.ClientID = this.ClientId;
                FinancialDataUtility.GetClientDividendDS(clientDistributionDs, Entity.DividendCollection, this.Broker);
            }
            else if (data is DIVTransactionDS)
            {
                DIVTransactionDS divTransactionDS = data as DIVTransactionDS;
                divTransactionDS.CLIENTIDForm = this.ClientId;
                divTransactionDS.CLIENTNAMEForm = this.Name;
                IOrganization orgCM = this.Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                FinancialDataUtility.GetdividendAccountDataSet(divTransactionDS, orgCM, ClientAccountType, Parent, this.ClientId, this.name, this.Broker, this.Entity);
                this.Broker.ReleaseBrokerManagedComponent(orgCM);
            }
            else if (data is ClientAccountProcessDS)
            {
                ClientUtilities.FillAccountProcessDataset(data as ClientAccountProcessDS, this.Entity.ListAccountProcess, this.Broker);
            }
            else if (data is BankAccountDS)
            {
                BankAccountUtilities.GetBankAccountDs(ClientId, data as BankAccountDS, Entity.BankAccounts, Broker, Entity.ListAccountProcess);
            }
            else if (data is DesktopBrokerAccountDS)
            {
                BankAccountUtilities.GetASXAccountDs(data as DesktopBrokerAccountDS, Entity.DesktopBrokerAccounts, this.ClientId, this.Name, CID, Entity.ListAccountProcess, Broker);
            }
            else if (data is TermDepositDS)
            {
                BankAccountUtilities.GetTermDepositAccountDs(data as TermDepositDS, Entity.TermDepositAccounts, Broker);
            }
            else if (data is ClientDetailsDS)
            {
                ClientDetailsUtilities.GetClientDetailsDs(this.ClientId, data as ClientDetailsDS, Entity, Broker, TypeName);
            }
            else if (data is ManagedInvestmentAccountDS)
            {
                ManagedInvestmentAccountDS misDS = data as ManagedInvestmentAccountDS;
                MISFundUtilities.FillDS(misDS, Entity.ManagedInvestmentSchemesAccounts, Broker);

            }
            else if (data is ClientModelsDetailsDS)
            {
                ClientModelsDetailsDS modelDs = data as ClientModelsDetailsDS;
                ClientUtilities.FillModelInfo(modelDs, Entity.Servicetype, Broker,EclipseClientID,CID,Name);
            }
            else if (data is OtherServicesDS)
            {
                OtherServicesDS otherServicesDs = data as OtherServicesDS;
                ClientUtilities.GetOtherServices(otherServicesDs, Entity);
            }
            else if (data is ExportDS)
            {
                var ds = data as ExportDS;
                switch ((WebCommands)ds.Command)
                {
                    case WebCommands.ExportStateStreet:
                        #region Filling Datasets for State Street
                        if (Entity.Servicetype.DO_IT_FOR_ME || Entity.Servicetype.DO_IT_WITH_ME)
                        {
                            if (!IsExportedSS)
                            {
                                ExportUtilities.FillStateStreetDataset(data, _entity, this.Broker, this.ApplicationID, this.TypeName, IsExportedSS, this.ClientId);
                                IsExportedSS = true;
                            }
                        }
                        #endregion
                        break;
                    case WebCommands.ExportStateStreetRecon:
                        #region Filling Datasets for State Street Recon
                        if (Entity.Servicetype.DO_IT_FOR_ME || Entity.Servicetype.DO_IT_WITH_ME)
                        {
                            ExportUtilities.FillStateStreetDatasetRecon(data, _entity, this.Broker, this.ApplicationID, this.TypeName, IsExportedSS, this.ClientId);
                            IsExportedSS = true;
                        }
                        #endregion
                        break;
                    case WebCommands.ExportBankwestAccountOpennings:
                        #region Filling Datasets for Bankwest account opening
                        List<AccountType> accounts = ExportUtilities.GetAccountTypes(Entity);
                        ExportUtilities.FillBankWestAccountOpeningDataset(data, accounts, _entity, this.Broker, this.ApplicationID, this.TypeName, this.CID);
                        #endregion
                        break;
                }
            }
            else if (data is ExportAllClientDetailsDS)
            {
                #region Filling Datasets for Bankwest account opening
                List<AccountType> accounts = ExportUtilities.GetAccountTypes(Entity);
                ExportUtilities.FillBankWestAccountOpeningDataset(data, accounts, _entity, this.Broker, this.ApplicationID, this.TypeName, this.CID);
                ExportUtilities.FillClientsDetails(data as ExportAllClientDetailsDS, this.ClientId, this.CID, this.typeName, _entity, Parent, Broker);
                #endregion
            }
            else if (data is ExportStatusDS)
            {
                ExportUtilities.AddExportStatusDetails(data as ExportStatusDS, CID, Clid, Csid, ClientId, Entity.Name, typeName, OrganizationStatus, ApplicationDate, ApplicationID, IsExportedSS, IsExported, IsExportedDB, Entity.Servicetype.DO_IT_FOR_ME || Entity.Servicetype.DO_IT_WITH_ME);
            }
            else if (data is ClientTotalUnitsDS)
            {
                var ds = data as ClientTotalUnitsDS;
                ClientUtilities.GetClientUnitsForASX(ds, Entity.DesktopBrokerAccounts, Broker);
            }
            else if (data is ImportProcessDS)
            {
                var ds = data as ImportProcessDS;
                switch ((WebCommands)ds.Command)
                {
                    case WebCommands.ImportBankwestDatafeedImportFile:
                        BankAccountUtilities.SetClientIdInAccounts(ds, Entity.BankAccounts, Entity.ListAccountProcess, this.ClientId, Broker);
                        break;
                    case WebCommands.ImportDesktopBrokerDataFeedImportFile:
                        BankAccountUtilities.GetASXAccountDs(ds, Entity.DesktopBrokerAccounts, this.ClientId, CID, Broker);
                        break;

                    case WebCommands.ImportAtCallClientTransactionImportFile:
                        BankAccountUtilities.SetClientIdInAccountAtCall(ds, Entity.BankAccounts, Entity.ListAccountProcess, this.ClientId, Broker);
                        break;

                    case WebCommands.ImportTDClientTransactionImportFile:
                        BankAccountUtilities.SetClientIdInAccountTdTransaction(ds, Entity.BankAccounts, Entity.ListAccountProcess, this.ClientId, Broker);
                        break;
                }
            }
            else if (data is OrderPadDS)
            {
                var ds = data as OrderPadDS;
                ClientUtilities.GetProductFromAccountProcessDesktop(ds, Entity.ListAccountProcess, Broker);
            }
            else if (data is ClientsBankAccountsHoldingsDS)
            {
                var ds = data as ClientsBankAccountsHoldingsDS;
                BankAccountUtilities.GetBankAccountHoldings(CID, ClientId, Entity, ds, Broker);
            }
            else if (data is ClientsASXAccountsHoldingsDS)
            {
                var ds = data as ClientsASXAccountsHoldingsDS;
                BankAccountUtilities.GetAsxHoldings(CID, ClientId, Entity, ds, Broker);
            }
            else if (data is ClientsMISAccountsHoldingsDS)
            {
                var ds = data as ClientsMISAccountsHoldingsDS;
                BankAccountUtilities.GetMISHoldings(CID, ClientId, Entity, ds, Broker);
            }
            else if (data is AddressDetailsDS)
            {
                var ds = data as AddressDetailsDS;
                ClientUtilities.FillClientAddressDetailDs(ds, Entity);
            }
            else if (data is BTWrapDS)
            {
                BTWrapDS ds = data as BTWrapDS;
                ClientUtilities.GetBTWrapData(ds, CID, Entity, Broker);
            }
        }

        #endregion
        public CorporateEntity Entity { get { return _entity; } set { _entity = value; } }


        public override IClientEntity ClientEntity
        {
            get
            {
                return this.Entity;
            }
            set
            {
                this.Entity = value as CorporateEntity;
            }
        }

        public string XName { get { return "ClientCorporationPublic"; } }

        public void Export(XElement root)
        {
            this.ExportHelper(root);
        }

        public void Import(XElement root, IdentityCmMap map)
        {
            this.ImportHelper(root, map);
        }

        //public override DataSet ProcessDataSet(int type, DataSet data)
        //{
        //    return base.ProcessDataSet(type, data);
        //}
        #region OrganizationsChart

        public OrganizationChart GetOrganizationChart()
        {
            OrganizationChart chart = new OrganizationChart();

            chart.Clid = this.CLID;
            chart.Csid = this.CSID;
            chart.NodeName = this._entity.Name;

            return chart;
        }

        public OrganizationChart GetAssociation(IIdentityCM identitycm, OrganizationType organizationtype)
        {
            OrganizationChart OChart = new OrganizationChart();
            switch (organizationtype)
            {
                case OrganizationType.BankAccount:
                    if (this._entity.BankAccounts.Where(e => e.Clid == identitycm.Clid && e.Csid == identitycm.Csid).ToList().Count > 0)
                    {
                        OChart.Clid = this.Clid;
                        OChart.Csid = this.Csid;
                        OChart.NodeName = this.Name;
                    }
                    break;
                case OrganizationType.Individual:
                    OChart = GetIndividualChart(identitycm);
                    break;
                case OrganizationType.ClientCorporationPublic:
                    if (identitycm.Clid == this.Clid && identitycm.Csid == this.Csid)
                    {
                        OChart = new OrganizationChart() { Clid = this.Clid, Csid = this.Csid, NodeName = this.Name, TypeName = OrganizationType.ClientCorporationPublic.ToString() };
                        OChart.Subordinates.Add(this._entity.BankAccounts.ToOrganizationChart(this.Broker, "Bank Account(s)"));
                        OChart.Subordinates.Add(this._entity.Signatories.ToOrganizationChart(this.Broker, "Signatory(ies)"));
                        OChart.Subordinates.Add(this._entity.Contacts.ToOrganizationChart(this.Broker, "Contact(s)"));

                        OChart.Subordinates.Add(this._entity.CorporatePrivate.ToOrganizationChart(this.Broker, "Corporate Private"));
                        OChart.Subordinates.Add(this._entity.CorporatePublic.ToOrganizationChart(this.Broker, "Corporate Public"));

                        OChart.Subordinates.RemoveAll(e => e == null);
                        OChart = GetEntityParent(OChart);
                    }
                    break;
                default:
                    break;
            }
            return OChart;
        }

        public OrganizationChart GetEntityParent(OrganizationChart organizationchart)
        {
            ICalculationModule module = null;
            OrganizationChart association = new OrganizationChart() { Clid = this.Clid, Csid = this.Csid, NodeName = this.Name, TypeName = OrganizationType.ClientCorporationPublic.ToString() };
            if (organizationchart.Clid == this.Clid && organizationchart.Csid == this.Csid)
            {
                association = organizationchart;
            }
            else
            {
                association.Subordinates.Add(organizationchart);
            }
            module = Broker.GetCMImplementation(this.Parent.Clid, this.Parent.Csid);
            if (module != null && module is IHaveAssociation)
            {
                association = (module as IHaveAssociation).GetEntityParent(organizationchart);
            }
            return association;
        }

        public OrganizationChart GetEntityChild()
        {
            //ICalculationModule module = null;
            //OrganizationChart association = new OrganizationChart() { Clid = this.Clid, Csid = this.Csid, NodeName = this.Name, TypeName = OrganizationType.ClientCorporationPublic.ToString() };
            //association.Subordinates.Add(this._entity.BankAccounts.ToOrganizationChart(this.Broker, "Bank Account(s)"));
            //association.Subordinates.Add(this._entity.Signatories.ToOrganizationChart(this.Broker, "Signatory(ies)"));
            //association.Subordinates.Add(this._entity.Contacts.ToOrganizationChart(this.Broker, "Contact(s)"));

            //association.Subordinates.Add(this._entity.CorporatePrivate.ToOrganizationChart(this.Broker, "Corporate Private"));
            //association.Subordinates.Add(this._entity.CorporatePublic.ToOrganizationChart(this.Broker, "Corporate Public"));

            //association.Subordinates.RemoveAll(e => e == null);
            //foreach (var each in this._Children)
            //{
            //    module = Broker.GetCMImplementation(each.Clid, each.Csid);
            //    if (module != null && module is IHaveAssociation)
            //    {
            //        association.Subordinates.Add((module as IHaveAssociation).GetEntityChild(showAllevel));
            //    }
            //}
            return null;
        }

        public OrganizationChart GetEntityChildWithEntity()
        {
            OrganizationChart association = new OrganizationChart() { Clid = this.Clid, Csid = this.Csid, NodeName = this.Name, TypeName = OrganizationType.ClientCorporationPublic.ToString() };
            association.Subordinates.Add(this._entity.BankAccounts.ToOrganizationChart(this.Broker, "Bank Account(s)"));
            association.Subordinates.Add(this._entity.Signatories.ToOrganizationChart(this.Broker, "Signatory(ies)"));
            association.Subordinates.Add(this._entity.Contacts.ToOrganizationChart(this.Broker, "Contact(s)"));

            association.Subordinates.Add(this._entity.CorporatePrivate.ToOrganizationChart(this.Broker, "Corporate Private"));
            association.Subordinates.Add(this._entity.CorporatePublic.ToOrganizationChart(this.Broker, "Corporate Public"));

            association.Subordinates.RemoveAll(e => e == null);
            return association;
        }

        private OrganizationChart GetIndividualChart(IIdentityCM identitycm)
        {
            OrganizationChart OChart = new OrganizationChart();
            OChart.Clid = this.Clid;
            OChart.Csid = this.Csid;
            OChart.NodeName = this.Name;

            if (this._entity.Signatories.Where(e => e.Clid == identitycm.Clid && e.Csid == identitycm.Csid).ToList().Count > 0)
            {
                OChart.Subordinates.Add(new OrganizationChart() { NodeName = "Signatory(ies)" });
            }
            if (this._entity.Contacts.Where(e => e.Clid == identitycm.Clid && e.Csid == identitycm.Csid).ToList().Count > 0)
            {
                OChart.Subordinates.Add(new OrganizationChart() { NodeName = "Contact(s)" });
            }

            OChart.Subordinates.RemoveAll(e => e == null);
            return OChart.Subordinates.Count <= 0 ? null : OChart;
        }

        #endregion

        public string GetAccountProcessStatus()
        {
            return GetAccountProcessStatus(Entity);
        }

        public override Guid AdviserID
        {
            get
            {
                if (Parent != null && Parent.Clid != Guid.Empty && Parent.Csid != Guid.Empty)
                {
                    AdvisorCM adviserUnit = Broker.GetCMImplementation(Parent.Clid, Parent.Csid) as AdvisorCM;
                    Broker.ReleaseBrokerManagedComponent(adviserUnit);

                    if (adviserUnit != null)
                        return adviserUnit.CID;
                }

                return base.AdviserID;
            }
        }

        override public void ImportBankwestData(List<IIdentityCM> accounts)
        {
            foreach (var account in accounts)
            {
                Entity.BankAccounts.Add(new Common.IdentityCMDetail() { Clid = account.Clid, Csid = account.Csid });
            }
        }

        public DesktopBrokerDataResponse GetDesktopBrokerData()
        {
            if (_entity == null || _entity.Signatories == null || _entity.Signatories.Count == 0)
                return null;

            CSVInstance csvinstance = new CSVInstance(this.Broker);
            CorporateCSV _entitycsv = csvinstance.GetEntityConcreteInstance(_entity);

            DesktopBrokerDataResponse response = new DesktopBrokerDataResponse();

            AddClientToDesktopBrokerAccountResponse(response, _entity.Address != null ? _entity.Address.RegisteredAddress : null);

            AddApplicants(response, _entitycsv.Signatories, _entity.Signatories);
            if (!response.Data.ContainsKey(DesktopBrokerExportKeys.Applicant_1.ToString()))
            {
                AddEmptyApplicant(response, DesktopBrokerExportKeys.Applicant_1.ToString());
            }

            if (!response.Data.ContainsKey(DesktopBrokerExportKeys.Applicant_2.ToString()))
            {
                AddEmptyApplicant(response, DesktopBrokerExportKeys.Applicant_2.ToString());
            }

            AddCompnayDetailsToDesktopBrokerAccountResponse(response, this._entity);

            //AddTrustAccountToDesktopBrokerAccountResponse(response, this._entity);

            AddTransferStockToDesktopBrokerAccountResponse(response);

            List<Common.BankAccountEntity> bankAccountEntities = new List<Common.BankAccountEntity>();

            var InvalidBankAccountEntities = new List<Common.IdentityCMDetail>();
            foreach (Common.IdentityCMDetail bankAccount in _entity.BankAccounts)
            {
                BankAccountCM bankAccountCM = Broker.GetCMImplementation(bankAccount.Clid, bankAccount.Csid) as BankAccountCM;
                //bankAccountEntities.Add(bankAccountCM.BankAccountEntity);
                if (bankAccountCM != null)
                {
                    bankAccountEntities.Add(bankAccountCM.BankAccountEntity);
                }
                else
                {
                    InvalidBankAccountEntities.Add(bankAccount);
                }
            }
            int totalBankAccountsToRemove = InvalidBankAccountEntities.Count();
            for (int i = 0; i < totalBankAccountsToRemove; i++)
            {
                // _entity.BankAccounts.Remove(InvalidBankAccountEntities[i]);
            }
            AddBankAccountDetailsToDesktopBrokerAccountResponse(response, bankAccountEntities);

            //       AddCompnayDetailsToDesktopBrokerAccountResponse(response,_entity);

            CheckDoubleRows(response);

            response.Type = OrganizationType.ClientCorporationPublic;
            return response;
        }
        #region Check Mis Is Attached
        public override bool IsMISAttached(Guid clid, Guid csid)
        {
            bool result = false;

            if (this.Entity != null)
            {

                result = ClientUtilities.IsMISAttached(clid, csid, Entity.ManagedInvestmentSchemesAccounts);

            }


            return result;
        }
        public override bool IsMISFundCodeAttached(Guid FundCodeId)
        {
            bool result = false;

            if (this.Entity != null)
            {

                result = ClientUtilities.IsMISFundCodeAttached(FundCodeId, Entity.ManagedInvestmentSchemesAccounts);

            }


            return result;
        }
        public override bool IsMISAttachedWithFund(string FundCode, string MisName)
        {
            bool result = false;

            if (this.Entity != null)
            {
                result = ClientUtilities.IsMISAttachedWithFund(FundCode, MisName, Entity.ManagedInvestmentSchemesAccounts, Broker);
            }


            return result;
        }
        #endregion


        public override void AddClientDividend(System.Xml.Linq.XElement xml, OrganizationUnitCM unit)
        {
            string bsb = "";
            foreach (var bankaccountRec in _entity.BankAccounts)
            {
                var temp = unit.Broker.GetCMImplementation(bankaccountRec.Clid, bankaccountRec.Csid);
                if (temp != null)
                {
                    Common.BankAccountEntity bankAccount = temp.GetDataStream(-999, null).ToNewOrData<Common.BankAccountEntity>();
                    if (bankAccount != null)
                    {


                        if (!string.IsNullOrEmpty(bankAccount.BSB))
                        {
                            if (bankAccount.BSB.Contains('-'))
                            {
                                bsb = bankAccount.BSB.Split('-')[1];
                            }
                            else if (bsb.Length > 5)//length should be sixe
                            {
                                bsb = bsb.Substring(3, 5);
                            }
                        }
                        else
                        {
                            bsb = "bsbNotFound";
                        }
                        string BsBNAccountNumber = bsb + bankAccount.AccountNumber;
                        XElement[] elements = xml.XPathSelectElements("//AccountTransactions[@BSBNAccountNumber = '" + BsBNAccountNumber + "']").ToArray();

                        foreach (var each in elements)
                        {
                            AddTransactions(each, BsBNAccountNumber);
                        }
                    }
                }

            }
        }


        private void AddTransactions(XElement transactions, string BSBNAccountNumber)
        {
            XElement[] elements = transactions.XPathSelectElements("Transaction").ToArray();
            foreach (var xElement in elements)
            {
                Oritax.TaxSimp.Common.CashManagementEntity cashManagementEntity = CreateCashManagementEntity(xElement);
                if (cashManagementEntity != null)
                {
                    var dV = _entity.DividendCollection.Where(ee => ee.PaymentDate == cashManagementEntity.TransactionDate && Math.Abs(Convert.ToDecimal(ee.PaidDividend) - cashManagementEntity.TotalAmount) < 1 && cashManagementEntity.SystemTransactionType == "Dividend").FirstOrDefault();
                    if (dV != null)
                    {
                        this.Broker.SetWriteStart();
                        dV.CashManagementTransactionID = cashManagementEntity.ExternalReferenceID;
                        this.CalculateToken(true);
                    }
                }
            }
        }



        public Oritax.TaxSimp.Common.CashManagementEntity CreateCashManagementEntity(XElement xElement)
        {
            Oritax.TaxSimp.Common.CashManagementEntity cashManagementEntity = new Oritax.TaxSimp.Common.CashManagementEntity();
            BankAccountTransactionMessage message = new BankAccountTransactionMessage();

            DateTime dateofTrans;
            DateTimeFormatInfo info = new DateTimeFormatInfo();
            info.ShortDatePattern = "dd/MM/yyyy";
            decimal amount;
            //don't add transaction if Date of Transaction and Amount are not in correct format
            try
            {
                cashManagementEntity.ID = Guid.NewGuid();
                cashManagementEntity.ExternalReferenceID = xElement.XPathSelectElement("BankwestReferenceNumber").Value;
                cashManagementEntity.Comment = xElement.XPathSelectElement("TransactionDescription").Value;
                cashManagementEntity.Status = xElement.XPathSelectElement("Status").Value;// "Bankwest";
                cashManagementEntity.ImportTransactionType = xElement.XPathSelectElement("TransactionType").Value;
                cashManagementEntity.InvestmentCode = xElement.XPathSelectElement("InvestmentCode").Value;
                cashManagementEntity.Currency = xElement.XPathSelectElement("Currency").Value;
                cashManagementEntity.InvestmentName = xElement.XPathSelectElement("InvestmentName").Value;

                cashManagementEntity.AdministrationSystem = xElement.XPathSelectElement("AdministrationSystem").Value;
                cashManagementEntity.AccountName = xElement.XPathSelectElement("AccountName").Value;


                message.Status = ImportMessageStatus.Sucess;
                message.MessageType = ImportMessageType.Sucess;
                message.BSBNAccountNumber = "";
                message.InputMode = "Transaction";
                message.Amount = xElement.XPathSelectElement("Amount").Value;
                message.Date = xElement.XPathSelectElement("DateOfTransaction").Value;
                message.Type = xElement.XPathSelectElement("Type").Value;
                message.BankwestReferenceNumber = xElement.XPathSelectElement("BankwestReferenceNumber").Value;
                message.TransactionDescription = xElement.XPathSelectElement("TransactionDescription").Value;
                message.TransactionType = xElement.XPathSelectElement("TransactionType").Value;


                if (DateTime.TryParse(xElement.XPathSelectElement("DateOfTransaction").Value, info, DateTimeStyles.None, out dateofTrans))
                {

                    cashManagementEntity.TransactionDate = dateofTrans;
                    message.Date = dateofTrans.ToString("dd/MM/yyyy");

                }
                else
                {

                    message.Message += "Invalid transaction date,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;

                }


                if (decimal.TryParse(xElement.XPathSelectElement("Amount").Value, out amount))
                {
                    cashManagementEntity.Amount = amount;
                }
                else
                {
                    message.Message += "Invalid amount,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                }




            }
            catch
            {

                cashManagementEntity = null;
            }

            if (message.MessageType == ImportMessageType.Error)
            {
                cashManagementEntity = null;
            }






            return cashManagementEntity;
        }

        public override void ProcessDistribution(DistributionEntity dv, List<ProductEntity> ProductCollection, SecuritiesEntity security)
        {
            Entity.ClientId = EclipseClientID; 
            ClientUtilities.ProcessDistribution(dv, Entity, Broker);
        }

        public override void ProcessDividend(DividendEntity dv, List<ProductEntity> ProductCollection, SecuritiesEntity security)
        {
            Entity.Clid = this.Clid;
            Entity.Csid = this.Csid;
            ClientUtilities.ApplyDividend(this.EclipseClientID, this.ClientEntity as IClientUMAData, dv, ProductCollection, security, this.Broker, this as IOrganizationUnit);
        }

        public override void ImportDistributions(DataRow[] rows)
        {
            this.Broker.SetStart();
            ClientUtilities.ImportDistributions(rows, _entity, this.Broker);
            this.CalculateToken(true);
        }


        public override void IncomeTransactionProcess(DividendEntity dv, List<ProductEntity> ProductCollection, SecuritiesEntity security)
        {
            string eventname;
            string InvestmentCode;
            int select = 0;
            if (_entity.ListAccountProcess != null)
            {
                if (_entity.DividendCollection == null)
                {
                    _entity.DividendCollection = new System.Collections.ObjectModel.ObservableCollection<DividendEntity>();
                }

                if (security.InvestmentType == "Managed Fund /Unit Trust")
                {
                    InvestmentCode = security.AsxCode;
                    foreach (var mis in _entity.ManagedInvestmentSchemesAccounts)
                    {
                        //foreach (var count in _entity.ListAccountProcess)
                        //{
                        //foreach (var pr in ProductCollection)
                        //{
                        //if (count.TaskID == pr.ID)
                        //{
                        var temp = this.Broker.GetCMImplementation(mis.Clid, mis.Csid);
                        if (temp != null)
                        {
                            ManagedInvestmentSchemesAccountEntity MIS = temp.GetDataStream(-999, null).ToNewOrData<ManagedInvestmentSchemesAccountEntity>();
                            var misfund = MIS.FundAccounts.Where(fund => fund.ID == mis.FundID && mis.FundID == security.FundID).FirstOrDefault();

                            if (misfund != null && misfund.Code == security.AsxCode)
                            {
                                decimal transactionShares = 0;
                                transactionShares = misfund.FundTransactions.Where(ee => ee.ClientID == this._entity.ClientId && ee.TradeDate <= dv.RecordDate).Sum(dd => dd.Shares);
                                if (transactionShares != 0)
                                {
                                    select = Convert.ToInt32(transactionShares);
                                    //break;
                                    dv.UnitsOnHand = select;
                                    if (select != 0)
                                    {
                                        var divAccountNA = _entity.DividendCollection.Where(d => d.ID == dv.ID && d.Account == null).ToList();
                                        if (divAccountNA != null)
                                        {
                                            foreach (var div in divAccountNA)
                                            {
                                                IdentityCM account = new IdentityCM();
                                                account.Clid = mis.Clid;
                                                account.Csid = mis.Csid;
                                                div.Account = account;
                                            }
                                        }

                                        var divAccountNull = _entity.DividendCollection.Where(d => d.ID == dv.ID && d.Account.Clid == Guid.Empty && d.Account.Csid == Guid.Empty).ToList();
                                        if (divAccountNull != null)
                                        {
                                            foreach (var div in divAccountNull)
                                            {
                                                IdentityCM account = new IdentityCM();
                                                account.Clid = mis.Clid;
                                                account.Csid = mis.Csid;
                                                div.Account = account;
                                            }
                                        }

                                        var selectdividend = _entity.DividendCollection.Where(d => d.ID == dv.ID && d.Account.Clid == mis.Clid && d.Account.Csid == mis.Csid).FirstOrDefault();
                                        if (selectdividend != null)
                                        {
                                            _entity.DividendCollection.Remove(selectdividend);
                                        }


                                        var dvreportNA = dv.ReportCollection.Where(r => r.ID == dv.ID && r.Clid == _entity.Clid && r.Csid == _entity.Csid && r.Account == null).FirstOrDefault();
                                        if (dvreportNA != null)
                                        {
                                            IdentityCM account = new IdentityCM();
                                            account.Clid = mis.Clid;
                                            account.Csid = mis.Csid;
                                            dvreportNA.Account = account;
                                        }

                                        var dvreportNull = dv.ReportCollection.Where(r => r.ID == dv.ID && r.Clid == _entity.Clid && r.Csid == _entity.Csid && r.Account.Clid == Guid.Empty && r.Account.Csid == Guid.Empty).FirstOrDefault();
                                        if (dvreportNull != null)
                                        {
                                            IdentityCM account = new IdentityCM();
                                            account.Clid = mis.Clid;
                                            account.Csid = mis.Csid;
                                            dvreportNull.Account = account;
                                        }

                                        var check = dv.ReportCollection.Where(r => r.ID == dv.ID && r.Clid == _entity.Clid && r.Csid == _entity.Csid && r.Account.Clid == mis.Clid && r.Account.Csid == mis.Csid).FirstOrDefault();
                                        if (check != null)
                                        {
                                            dv.ReportCollection.Remove(check);
                                            //dv.ReportCollection.Add(new DividendReportEntity { CentsPerShare = dv.CentsPerShare, Clid = _entity.Clid, ClientIdentification = _entity.ClientId, Csid = _entity.Csid, ID = dv.ID, Name = _entity.Name, PaymentDate = dv.PaymentDate, RecordDate = dv.RecordDate, UnitsOnHand = select });
                                            eventname = "updated";
                                        }
                                        else
                                        {
                                            //dv.ReportCollection.Add(new DividendReportEntity { CentsPerShare = dv.CentsPerShare, Clid = _entity.Clid, ClientIdentification = _entity.ClientId, Csid = _entity.Csid, ID = dv.ID, Name = _entity.Name, PaymentDate = dv.PaymentDate, RecordDate = dv.RecordDate, UnitsOnHand = select });
                                            eventname = "added";
                                        }

                                        IdentityCM _account = new IdentityCM();
                                        _account.Clid = mis.Clid;
                                        _account.Csid = mis.Csid;

                                        string clientNumber = "";
                                        if (_entity.ClientId == null)
                                        {
                                            clientNumber = this.ClientId;
                                        }
                                        else
                                        {
                                            clientNumber = _entity.ClientId;
                                        }

                                        dv.ReportCollection.Add(new DividendReportEntity { Account = _account, CentsPerShare = dv.CentsPerShare, Clid = _entity.Clid, ClientIdentification = clientNumber, Csid = _entity.Csid, ID = dv.ID, Name = _entity.Name, PaymentDate = dv.PaymentDate, RecordDate = dv.RecordDate, UnitsOnHand = select, PaidDividend = select * dv.CentsPerShare });

                                        string ExternalrefID_ = "";
                                        foreach (var countbankaccount in _entity.BankAccounts)
                                        {
                                            BankAccountCM tempbankaccount = (BankAccountCM)this.Broker.GetCMImplementation(countbankaccount.Clid, countbankaccount.Csid);
                                            tempbankaccount.Broker.SetWriteStart();
                                            string tempexternal = tempbankaccount.LinkDividendData(dv, select);
                                            if (tempexternal != "")
                                            {
                                                ExternalrefID_ = tempexternal;
                                            }
                                            if (ExternalrefID_ != "")
                                            {
                                                dv.CashManagementTransactionID = ExternalrefID_;
                                            }
                                            tempbankaccount.CalculateToken(true);
                                        }

                                        _entity.DividendCollection.Add(new DividendEntity
                                        {
                                            Account = _account,
                                            AdministrationSystem = dv.AdministrationSystem,
                                            Comment = dv.Comment,
                                            ExternalReferenceID = dv.ExternalReferenceID,
                                            CashManagementTransactionID = ExternalrefID_,
                                            DomesticDiscountedCGT = ((dv.DomesticDiscountedCGT) * ((select * dv.CentsPerShare) / 100)),
                                            DomesticGrossAmount = ((dv.DomesticGrossAmount) * ((select * dv.CentsPerShare) / 100)),
                                            DomesticImputationTaxCredits = ((dv.DomesticImputationTaxCredits) * ((select * dv.CentsPerShare) / 100)),
                                            DomesticIndexationCGT = ((dv.DomesticIndexationCGT) * ((select * dv.CentsPerShare) / 100)),
                                            DomesticInterestAmount = ((dv.DomesticInterestAmount) * ((select * dv.CentsPerShare) / 100)),
                                            DomesticOtherCGT = ((dv.DomesticOtherCGT) * ((select * dv.CentsPerShare) / 100)),
                                            DomesticOtherIncome = ((dv.DomesticOtherIncome) * ((select * dv.CentsPerShare) / 100)),
                                            DomesticWithHoldingTax = ((dv.DomesticWithHoldingTax) * ((select * dv.CentsPerShare) / 100)),
                                            ForeignDiscountedCGT = ((dv.ForeignDiscountedCGT) * ((select * dv.CentsPerShare) / 100)),
                                            ForeignImputationTaxCredits = ((dv.ForeignImputationTaxCredits) * ((select * dv.CentsPerShare) / 100)),
                                            ForeignIndexationCGT = ((dv.ForeignIndexationCGT) * ((select * dv.CentsPerShare) / 100)),
                                            ForeignInterestAmount = ((dv.ForeignInterestAmount) * ((select * dv.CentsPerShare) / 100)),
                                            ForeignInvestmentFunds = ((dv.ForeignInvestmentFunds) * ((select * dv.CentsPerShare) / 100)),
                                            ForeignOtherCGT = ((dv.ForeignOtherCGT) * ((select * dv.CentsPerShare) / 100)),
                                            ForeignOtherIncome = ((dv.ForeignOtherIncome) * ((select * dv.CentsPerShare) / 100)),
                                            ForeignWithHoldingTax = ((dv.ForeignWithHoldingTax) * ((select * dv.CentsPerShare) / 100)),
                                            FrankedAmount = ((dv.FrankedAmount) * ((select * dv.CentsPerShare) / 100)),
                                            UnfrankedAmount = ((dv.UnfrankedAmount) * ((select * dv.CentsPerShare) / 100)),
                                            ReturnOfCapital = ((dv.ReturnOfCapital) * ((select * dv.CentsPerShare) / 100)),
                                            TaxDeferred = ((dv.TaxDeferred) * ((select * dv.CentsPerShare) / 100)),
                                            TaxFree = ((dv.TaxFree) * ((select * dv.CentsPerShare) / 100)),
                                            PercentDomesticDiscountedCGT = dv.DomesticDiscountedCGT,
                                            PercentDomesticGrossAmount = dv.DomesticGrossAmount,
                                            PercentDomesticImputationTaxCredits = dv.DomesticImputationTaxCredits,
                                            PercentDomesticIndexationCGT = dv.DomesticIndexationCGT,
                                            PercentDomesticInterestAmount = dv.DomesticInterestAmount,
                                            PercentDomesticOtherCGT = dv.DomesticOtherCGT,
                                            PercentDomesticOtherIncome = dv.DomesticOtherIncome,
                                            PercentDomesticWithHoldingTax = dv.DomesticWithHoldingTax,
                                            PercentForeignDiscountedCGT = dv.ForeignDiscountedCGT,
                                            PercentForeignImputationTaxCredits = dv.ForeignImputationTaxCredits,
                                            PercentForeignIndexationCGT = dv.ForeignIndexationCGT,
                                            PercentForeignInterestAmount = dv.ForeignInterestAmount,
                                            PercentForeignInvestmentFunds = dv.ForeignInvestmentFunds,
                                            PercentForeignOtherCGT = dv.ForeignOtherCGT,
                                            PercentForeignOtherIncome = dv.ForeignOtherIncome,
                                            PercentForeignWithHoldingTax = dv.ForeignWithHoldingTax,
                                            PercentFrankedAmount = dv.FrankedAmount,
                                            PercentUnfrankedAmount = dv.UnfrankedAmount,
                                            PercentReturnOfCapital = dv.ReturnOfCapital,
                                            PercentTaxDeferred = dv.TaxDeferred,
                                            PercentTaxFree = dv.TaxFree,
                                            ID = dv.ID,
                                            InvestmentCode = InvestmentCode,
                                            Investor = dv.Investor,
                                            PaymentDate = dv.PaymentDate,
                                            ProcessFlag = dv.ProcessFlag,
                                            RecordDate = dv.RecordDate,
                                            TransactionType = dv.TransactionType,
                                            CentsPerShare = dv.CentsPerShare,
                                            UnitsOnHand = select,
                                            PaidDividend = select * dv.CentsPerShare
                                        });

                                        this.Broker.LogEvent(EventType.UMAClientUpdated, CID,  string.Format("Income Transaction of {0} {1} in client {2} ({3}), Record Date - {4}, Payment date - {5}", security.AsxCode, eventname, _entity.Name, _entity.ClientId, Convert.ToDateTime(dv.RecordDate.ToString()).ToString("d"), Convert.ToDateTime(dv.PaymentDate.ToString()).ToString("d")));
                                        goto loopmis;
                                    }
                                }
                            }
                        }
                    //}
                    //}
                    //}

                    loopmis:
                        int lp = 1;
                        if (lp == 1)
                        {
                            lp = 0;
                        }
                    }
                }
                else
                {
                    InvestmentCode = security.AsxCode;
                    foreach (var asxbroker in _entity.DesktopBrokerAccounts)
                    {
                        //foreach (var count in _entity.ListAccountProcess)
                        //{
                        //foreach (var pr in ProductCollection)
                        //{
                        //if (count.TaskID == pr.ID)
                        //{
                        var temp = this.Broker.GetCMImplementation(asxbroker.Clid, asxbroker.Csid);
                        if (temp != null)
                        {
                            DesktopBrokerAccountEntity Desktop = temp.GetDataStream(-999, null).ToNewOrData<DesktopBrokerAccountEntity>();
                            var DesktopValue = Desktop.holdingTransactions.Where(fund => fund.InvestmentCode == security.AsxCode).ToList();

                            if (DesktopValue != null)
                            {
                                decimal transactionShares = 0;
                                transactionShares = DesktopValue.Where(ee => ee.TradeDate <= dv.RecordDate).Sum(dd => dd.Units);
                                //var transaction = DesktopValue.OrderBy(dd => dd.TradeDate).LastOrDefault();
                                if (transactionShares != 0)
                                {
                                    select = Convert.ToInt32(transactionShares);
                                    //break;
                                    dv.UnitsOnHand = select;
                                    if (select != 0)
                                    {
                                        var divAccountNA = _entity.DividendCollection.Where(d => d.ID == dv.ID && d.Account == null).ToList();
                                        if (divAccountNA != null)
                                        {
                                            foreach (var div in divAccountNA)
                                            {
                                                IdentityCM account = new IdentityCM();
                                                account.Clid = asxbroker.Clid;
                                                account.Csid = asxbroker.Csid;
                                                div.Account = account;
                                            }
                                        }

                                        var divAccountNull = _entity.DividendCollection.Where(d => d.ID == dv.ID && d.Account.Clid == Guid.Empty && d.Account.Csid == Guid.Empty).ToList();
                                        if (divAccountNull != null)
                                        {
                                            foreach (var div in divAccountNull)
                                            {
                                                IdentityCM account = new IdentityCM();
                                                account.Clid = asxbroker.Clid;
                                                account.Csid = asxbroker.Csid;
                                                div.Account = account;
                                            }
                                        }

                                        var selectdividend = _entity.DividendCollection.Where(d => d.ID == dv.ID && d.Account.Clid == asxbroker.Clid && d.Account.Csid == asxbroker.Csid).FirstOrDefault();
                                        if (selectdividend != null)
                                        {
                                            _entity.DividendCollection.Remove(selectdividend);
                                        }

                                        var dvreportNA = dv.ReportCollection.Where(r => r.ID == dv.ID && r.Clid == _entity.Clid && r.Csid == _entity.Csid && r.Account == null).FirstOrDefault();
                                        if (dvreportNA != null)
                                        {
                                            IdentityCM account = new IdentityCM();
                                            account.Clid = asxbroker.Clid;
                                            account.Csid = asxbroker.Csid;
                                            dvreportNA.Account = account;
                                        }

                                        var dvreportNull = dv.ReportCollection.Where(r => r.ID == dv.ID && r.Clid == _entity.Clid && r.Csid == _entity.Csid && r.Account.Clid == Guid.Empty && r.Account.Csid == Guid.Empty).FirstOrDefault();
                                        if (dvreportNull != null)
                                        {
                                            IdentityCM account = new IdentityCM();
                                            account.Clid = asxbroker.Clid;
                                            account.Csid = asxbroker.Csid;
                                            dvreportNull.Account = account;
                                        }


                                        var check = dv.ReportCollection.Where(r => r.ID == dv.ID && r.Clid == _entity.Clid && r.Csid == _entity.Csid && r.Account.Clid == asxbroker.Clid && r.Account.Csid == asxbroker.Csid).FirstOrDefault();
                                        if (check != null)
                                        {
                                            dv.ReportCollection.Remove(check);
                                            //dv.ReportCollection.Add(new DividendReportEntity { CentsPerShare = dv.CentsPerShare, Clid = _entity.Clid, ClientIdentification = _entity.ClientId, Csid = _entity.Csid, ID = dv.ID, Name = _entity.Name, PaymentDate = dv.PaymentDate, RecordDate = dv.RecordDate, UnitsOnHand = select });
                                            eventname = "updated";
                                        }
                                        else
                                        {
                                            //dv.ReportCollection.Add(new DividendReportEntity { CentsPerShare = dv.CentsPerShare, Clid = _entity.Clid, ClientIdentification = _entity.ClientId, Csid = _entity.Csid, ID = dv.ID, Name = _entity.Name, PaymentDate = dv.PaymentDate, RecordDate = dv.RecordDate, UnitsOnHand = select });
                                            eventname = "added";
                                        }

                                        IdentityCM _account = new IdentityCM();
                                        _account.Clid = asxbroker.Clid;
                                        _account.Csid = asxbroker.Csid;


                                        string clientNumber = "";
                                        if (_entity.ClientId == null)
                                        {
                                            clientNumber = this.ClientId;
                                        }
                                        else
                                        {
                                            clientNumber = _entity.ClientId;
                                        }

                                        DesktopBrokerAccountCM tempdesktopbroker = (DesktopBrokerAccountCM)this.Broker.GetCMImplementation(asxbroker.Clid, asxbroker.Csid);
                                        string dbaccountname = tempdesktopbroker.getAccountName();
                                        string dbaccountnumber = tempdesktopbroker.getAccountNumber();

                                        dv.ReportCollection.Add(new DividendReportEntity { acname = dbaccountname, acnumber = dbaccountnumber, Account = _account, CentsPerShare = dv.CentsPerShare, Clid = _entity.Clid, ClientIdentification = clientNumber, Csid = _entity.Csid, ID = dv.ID, Name = _entity.Name, PaymentDate = dv.PaymentDate, RecordDate = dv.RecordDate, UnitsOnHand = select, PaidDividend = select * dv.CentsPerShare });

                                        string ExternalrefID_ = "";
                                        foreach (var countbankaccount in _entity.BankAccounts)
                                        {
                                            BankAccountCM tempbankaccount = (BankAccountCM)this.Broker.GetCMImplementation(countbankaccount.Clid, countbankaccount.Csid);
                                            tempbankaccount.Broker.SetWriteStart();
                                            string tempexternal = tempbankaccount.LinkDividendData(dv, select);
                                            if (tempexternal != "")
                                            {
                                                ExternalrefID_ = tempexternal;
                                            }
                                            if (ExternalrefID_ != "")
                                            {
                                                dv.CashManagementTransactionID = ExternalrefID_;
                                            }
                                            tempbankaccount.CalculateToken(true);
                                        }

                                        _entity.DividendCollection.Add(new DividendEntity
                                        {
                                            Account = _account,
                                            AdministrationSystem = dv.AdministrationSystem,
                                            Comment = dv.Comment,
                                            ExternalReferenceID = dv.ExternalReferenceID,
                                            CashManagementTransactionID = ExternalrefID_,
                                            DomesticDiscountedCGT = ((dv.DomesticDiscountedCGT) * ((select * dv.CentsPerShare) / 100)),
                                            DomesticGrossAmount = ((dv.DomesticGrossAmount) * ((select * dv.CentsPerShare) / 100)),
                                            DomesticImputationTaxCredits = ((dv.DomesticImputationTaxCredits) * ((select * dv.CentsPerShare) / 100)),
                                            DomesticIndexationCGT = ((dv.DomesticIndexationCGT) * ((select * dv.CentsPerShare) / 100)),
                                            DomesticInterestAmount = ((dv.DomesticInterestAmount) * ((select * dv.CentsPerShare) / 100)),
                                            DomesticOtherCGT = ((dv.DomesticOtherCGT) * ((select * dv.CentsPerShare) / 100)),
                                            DomesticOtherIncome = ((dv.DomesticOtherIncome) * ((select * dv.CentsPerShare) / 100)),
                                            DomesticWithHoldingTax = ((dv.DomesticWithHoldingTax) * ((select * dv.CentsPerShare) / 100)),
                                            ForeignDiscountedCGT = ((dv.ForeignDiscountedCGT) * ((select * dv.CentsPerShare) / 100)),
                                            ForeignImputationTaxCredits = ((dv.ForeignImputationTaxCredits) * ((select * dv.CentsPerShare) / 100)),
                                            ForeignIndexationCGT = ((dv.ForeignIndexationCGT) * ((select * dv.CentsPerShare) / 100)),
                                            ForeignInterestAmount = ((dv.ForeignInterestAmount) * ((select * dv.CentsPerShare) / 100)),
                                            ForeignInvestmentFunds = ((dv.ForeignInvestmentFunds) * ((select * dv.CentsPerShare) / 100)),
                                            ForeignOtherCGT = ((dv.ForeignOtherCGT) * ((select * dv.CentsPerShare) / 100)),
                                            ForeignOtherIncome = ((dv.ForeignOtherIncome) * ((select * dv.CentsPerShare) / 100)),
                                            ForeignWithHoldingTax = ((dv.ForeignWithHoldingTax) * ((select * dv.CentsPerShare) / 100)),
                                            FrankedAmount = ((dv.FrankedAmount) * ((select * dv.CentsPerShare) / 100)),
                                            UnfrankedAmount = ((dv.UnfrankedAmount) * ((select * dv.CentsPerShare) / 100)),
                                            ReturnOfCapital = ((dv.ReturnOfCapital) * ((select * dv.CentsPerShare) / 100)),
                                            TaxDeferred = ((dv.TaxDeferred) * ((select * dv.CentsPerShare) / 100)),
                                            TaxFree = ((dv.TaxFree) * ((select * dv.CentsPerShare) / 100)),
                                            PercentDomesticDiscountedCGT = dv.DomesticDiscountedCGT,
                                            PercentDomesticGrossAmount = dv.DomesticGrossAmount,
                                            PercentDomesticImputationTaxCredits = dv.DomesticImputationTaxCredits,
                                            PercentDomesticIndexationCGT = dv.DomesticIndexationCGT,
                                            PercentDomesticInterestAmount = dv.DomesticInterestAmount,
                                            PercentDomesticOtherCGT = dv.DomesticOtherCGT,
                                            PercentDomesticOtherIncome = dv.DomesticOtherIncome,
                                            PercentDomesticWithHoldingTax = dv.DomesticWithHoldingTax,
                                            PercentForeignDiscountedCGT = dv.ForeignDiscountedCGT,
                                            PercentForeignImputationTaxCredits = dv.ForeignImputationTaxCredits,
                                            PercentForeignIndexationCGT = dv.ForeignIndexationCGT,
                                            PercentForeignInterestAmount = dv.ForeignInterestAmount,
                                            PercentForeignInvestmentFunds = dv.ForeignInvestmentFunds,
                                            PercentForeignOtherCGT = dv.ForeignOtherCGT,
                                            PercentForeignOtherIncome = dv.ForeignOtherIncome,
                                            PercentForeignWithHoldingTax = dv.ForeignWithHoldingTax,
                                            PercentFrankedAmount = dv.FrankedAmount,
                                            PercentUnfrankedAmount = dv.UnfrankedAmount,
                                            PercentReturnOfCapital = dv.ReturnOfCapital,
                                            PercentTaxDeferred = dv.TaxDeferred,
                                            PercentTaxFree = dv.TaxFree,
                                            ID = dv.ID,
                                            InvestmentCode = InvestmentCode,
                                            Investor = dv.Investor,
                                            PaymentDate = dv.PaymentDate,
                                            ProcessFlag = dv.ProcessFlag,
                                            RecordDate = dv.RecordDate,
                                            TransactionType = dv.TransactionType,
                                            CentsPerShare = dv.CentsPerShare,
                                            UnitsOnHand = select,
                                            PaidDividend = select * dv.CentsPerShare,
                                        });

                                        this.Broker.LogEvent(EventType.UMAClientUpdated,this.CID, string.Format("Income Transaction of {0} {1} in client {2} ({3}), Record Date - {4}, Payment date - {5}", security.AsxCode, eventname, _entity.Name, _entity.ClientId, Convert.ToDateTime(dv.RecordDate.ToString()).ToString("d"), Convert.ToDateTime(dv.PaymentDate.ToString()).ToString("d")));
                                        goto loopdesktop;
                                    }
                                }

                            }
                        }
               
                        loopdesktop:
                        int lp = 1;
                        if (lp == 1)
                        {
                            lp = 0;
                        }
                    }

                }


            }


        }

        public override void IncomeTransactionProcessTD(Guid dv)
        {
            if (_entity.DividendCollection == null)
            {
                _entity.DividendCollection = new System.Collections.ObjectModel.ObservableCollection<DividendEntity>();
            }

            foreach (var bankaccount in _entity.BankAccounts)
            {
                var temp = this.Broker.GetCMImplementation(bankaccount.Clid, bankaccount.Csid);
                if (temp != null)
                {
                    Common.BankAccountEntity bankaccountentity = temp.GetDataStream(-999, null).ToNewOrData<Common.BankAccountEntity>();
                    var cashtransactions = bankaccountentity.CashManagementTransactions.Where(ba => ba.ID == dv).FirstOrDefault();
                    if (cashtransactions != null)
                    {
                        DividendEntity dEntity = _entity.DividendCollection.Where(ee => ee.ID == cashtransactions.ID).FirstOrDefault();
                        if (dEntity != null)
                        {
                            _entity.DividendCollection.Remove(dEntity);

                            var check = dEntity.ReportCollection.Where(r => r.ID == dv).FirstOrDefault();
                            if (check != null)
                                dEntity.ReportCollection.Remove(check);
                        }

                        IdentityCM _account = new IdentityCM();
                        _account.Clid = bankaccount.Clid;
                        _account.Csid = bankaccount.Csid;
                        string clientNumber = "";
                        if (_entity.ClientId == null)
                        {
                            clientNumber = this.ClientId;
                        }
                        else
                        {
                            clientNumber = _entity.ClientId;
                        }

                        int daysFactor = 0;
                        double paidDividend;
                        decimal interestPaid;

                        if (cashtransactions.TransactionType == "At Call")
                        {
                            paidDividend = Convert.ToDouble(cashtransactions.Adjustment);
                            interestPaid = cashtransactions.Adjustment;
                            if (cashtransactions.Amount < 0)
                                cashtransactions.Amount = cashtransactions.Amount * -1;
                        }
                        else
                        {
                            daysFactor = InterestIncomeCalculation.DaysFactor(cashtransactions.MaturityDate, cashtransactions.TransactionDate);
                            paidDividend = Convert.ToDouble(InterestIncomeCalculation.InterestIncome(cashtransactions.Amount, cashtransactions.InterestRate, daysFactor));
                            interestPaid = InterestIncomeCalculation.InterestIncome(cashtransactions.Amount, cashtransactions.InterestRate, daysFactor);
                        }

                        _entity.DividendCollection.Add(new DividendEntity
                        {
                            Account = _account,
                            AdministrationSystem = cashtransactions.AdministrationSystem,
                            ID = dv,
                            InvestmentCode = "CASH",
                            PaymentDate = cashtransactions.MaturityDate,
                            ProcessFlag = true,
                            InterestRate = cashtransactions.InterestRate,
                            InvestmentAmount = cashtransactions.Amount,
                            RecordDate = cashtransactions.TransactionDate,
                            TransactionType = "Interest Income Accrual",
                            InterestPaid = interestPaid,
                            CentsPerShare = 0,
                            UnitsOnHand = 0,
                            PaidDividend = paidDividend,
                        });
                        dEntity.ReportCollection.Add(new DividendReportEntity { Account = _account, Clid = _entity.Clid, ClientIdentification = clientNumber, Csid = _entity.Csid, ID = dv, Name = _entity.Name, PaymentDate = cashtransactions.MaturityDate, RecordDate = cashtransactions.TransactionDate, PaidDividend = Convert.ToDouble((cashtransactions.Amount * cashtransactions.InterestRate) / 100), });


                    }
                }

            }

        }

        public override void DeletAllTransaction()
        {
            if (_entity.BankAccounts != null)
            {
                foreach (var countbankaccount in _entity.BankAccounts)
                {
                    BankAccountCM tempbankaccount = (BankAccountCM)this.Broker.GetCMImplementation(countbankaccount.Clid, countbankaccount.Csid);
                    tempbankaccount.Broker.SetWriteStart();
                    tempbankaccount.DeleteAllCashTransaction_Bank();
                    tempbankaccount.CalculateToken(true);
                }
            }

            if (_entity.DesktopBrokerAccounts != null)
            {
                foreach (var countbankaccount in _entity.DesktopBrokerAccounts)
                {
                    DesktopBrokerAccountCM tempbankaccount = (DesktopBrokerAccountCM)this.Broker.GetCMImplementation(countbankaccount.Clid, countbankaccount.Csid);
                    tempbankaccount.Broker.SetWriteStart();
                    tempbankaccount.DeleteAllHoldingBalances_DesktopBroker();
                    tempbankaccount.DeleteAllHoldingTransaction_DesktopBroker();
                    tempbankaccount.CalculateToken(true);
                }
            }

            if (_entity.ManagedInvestmentSchemesAccounts != null)
            {

                foreach (var countbankaccount in _entity.ManagedInvestmentSchemesAccounts)
                {
                    ManagedInvestmentSchemesAccountCM tempbankaccount = (ManagedInvestmentSchemesAccountCM)this.Broker.GetCMImplementation(countbankaccount.Clid, countbankaccount.Csid);
                    tempbankaccount.Broker.SetWriteStart();
                    tempbankaccount.DeleteAllSecurityHolding_ManagedInvestmentSchemes();
                    tempbankaccount.DeleteAllFundTransaction_ManagedInvestmentSchemes();
                    tempbankaccount.CalculateToken(true);
                }
            }
        }
    }
}