using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.Data;
using System.Reflection;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Web.UI.WebControls;
using Oritax.TaxSimp;
using Oritax.TaxSimp.BusinessStructureUtilities;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.Group;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.CM.Period;
using Oritax.TaxSimp.DSCalculation;
using Oritax.TaxSimp.Exceptions;
using Oritax.TaxSimp.SAPBase;
using Oritax.TaxSimp.Security;
using Oritax.TaxSimp.TaxSimpLicence;
using Oritax.TaxSimp.Utilities;
namespace Oritax.TaxSimp.CM.Entity
{
	[Serializable]
	public class EntityCM : OrganizationUnitCM, IEntity , IBusinessStructureModule
	{
		#region INSTALLATION PROPERTIES

		// Assembly Installation Properties
		public const string ASSEMBLY_ID="DE2A4470-2581-4941-96A7-1EC76AB726E4";
		public const string ASSEMBLY_NAME="Entity";
		public const string ASSEMBLY_STRONGNAME="Entity, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";
		public const string ASSEMBLY_MAJORVERSION="1";
		public const string ASSEMBLY_MINORVERSION="1";
		public const string ASSEMBLY_DATAFORMAT="0";
		public const string ASSEMBLY_REVISION="0";

		// Component Installation Properties
		public const string COMPONENT_ID="DE2A4470-2581-4941-96A7-1EC76AB726E4";
		public const string COMPONENT_NAME="Entity";
		public const string COMPONENT_DISPLAYNAME="Entity";
		public const string COMPONENT_CATEGORY="BusinessStructure";

		// Component Version Installation Properties
		public const string COMPONENTVERSION_ID="DE2A4470-2581-4941-96A7-1EC76AB726E4";
		public const string COMPONENTVERSION_VERSIONNAME="Entity";
		public const string COMPONENTVERSION_STARTDATE="8/02/2004 3:01:36 PM";
		public const string COMPONENTVERSION_ENDDATE="8/02/2004 3:01:36 PM";
		public const string COMPONENTVERSION_PERSISTERASSEMBLYSTRONGNAME="CommonPersisters_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";
		public const string COMPONENTVERSION_PERSISTERCLASS="Oritax.TaxSimp.CommonPersistence.BlobPersister";
		public const string COMPONENTVERSION_IMPLEMENTATIONCLASS="Oritax.TaxSimp.CM.Entity.EntityCM";

		#endregion

		#region CONSTRUCTORS
		public EntityCM()
			: base()
		{
			
		}
		protected EntityCM(SerializationInfo si, StreamingContext context)
			: base(si,context)
		{
		}
	
		//Only allow the .NET Serialization core code to call this function
		[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]
		public override void GetObjectData(SerializationInfo si, StreamingContext context)
		{
			base.GetObjectData(si,context);
		}

		#endregion

		#region PROPERTIES
				
		public override ISubstitutedAccountingPeriods SAPs
		{
			get
			{
				return null;
			}
		}

        //public virtual string TaxStatusName
        //{
        //    get
        //    {
        //        return string.Empty;
        //    }
        //}
		#endregion

		#region OVERRIDE MEMBERS FOR CM BEHAVIOR
		public override IEnumerable Subscriptions
		{
			get
			{
				return new string[]{"EntityPoll","StreamControl"};
			}
		}

		public override IsSubscriberType IsSubscriber(IMessage objMessage, SubscriptionsList objSubscriptions, Guid subscriberCLID, Guid subscriberCSID)//, Guid subscribedCSID)
		{
			if ( objMessage == null )
				throw new ArgumentNullException( "objMessage", "Parameter cannot be null: objMessage passed to IsSubscriber method of EntityCM is null" );

			if ( objSubscriptions == null )
				throw new ArgumentNullException( "objSubscriptions", "Parameter cannot be null: objSubscriptions passed to IsSubscriber method of EntityCM is null" );

			if ( objMessage.Datatype == null ||
				objMessage.Datatype == String.Empty )
				throw new ArgumentException( "objMessage", "DataType not specified in message, datatype must be present in IsSubscriber method of EntityCM" );

			Guid subscribedCSID = objMessage.MSID;

			if ( objMessage.Contains( "StreamControl" ) &&
				objMessage.Contains( "StreamControlTargetCLID" ) &&
				objMessage.Contains( "StreamControlTargetCSID" ) &&
				( (StreamControlType) objMessage["StreamControl"] == StreamControlType.PeriodChanging ||
				(StreamControlType) objMessage["StreamControl"] == StreamControlType.PeriodJoining ||
				(StreamControlType) objMessage["StreamControl"] == StreamControlType.PeriodLeaving ) )
			{
				Message objStreamControlMessage = new Message( this, objMessage );
				Guid objTargetCLID = (Guid) objMessage["StreamControlTargetCLID"];
				Guid objTargetCSID = (Guid) objMessage["StreamControlTargetCSID"];

				if ( objTargetCLID == subscriberCLID &&
					objTargetCSID == subscriberCSID )
					return IsSubscriberType.True;
				else
					return IsSubscriberType.False;
			}

			foreach(Subscription subscription in objSubscriptions)
			{
				if(subscription.Consolidation == false) //Handle as per previously
				{
					if((subscription.DataType==objMessage.Datatype) || ( objMessage.Datatype.StartsWith("XP") && subscription.DataType == "XP"))
						if(subscription.SubscriberCLID==subscriberCLID
							&& FilterMessage(objMessage,subscription.Parameters)
							&& (subscription.SubscriberCSID==subscriberCSID || subscription.SubscriberCSID==Guid.Empty || subscriberCSID==Guid.Empty)
							&& (subscription.SubscribedCSID==subscribedCSID || subscription.SubscribedCSID==Guid.Empty || subscribedCSID==Guid.Empty)
							)
							return IsSubscriberType.True;
				}
				else if(objMessage.Contains("Consolidation"))
				{
					bool messageConsolidation = (bool)objMessage["Consolidation"];
					if(subscription.Consolidation == messageConsolidation
						&& subscription.SubscriberCLID==subscriberCLID
						&& objMessage.IsPublisher(subscription.SubscribedCLID)
						&& subscription.SubscriberCSID==subscriberCSID
						&& subscription.SubscribedCSID==objMessage.MSID)//Check that the message has originiated from a CM that the subscriber is subscribed to
						return IsSubscriberType.True;
				}
			}
			return IsSubscriberType.False;
		}

		public override DataSet PrimaryDataSet{get{return new EntityDS();}}
		
		#region OnSet/GetData Methods
		
		protected override void OnGetData(DataSet data )
		{
			base.OnGetData(data);
			if (data is EntityTypesDS)
			{
				this.OnGetData((EntityTypesDS)data);
			}
		}

		protected void OnGetData( EntityTypesDS objData )
		{
			DataRowCollection objSourceEntityTypes = (DataRowCollection) this.Owner.GetCMTypeList( "BusinessEntity" );

			DataTable objEntityTypes = objData.Tables[ EntityTypesDS.ENTITYTYPES_TABLE ];
			objEntityTypes.Rows.Clear( );
			
			foreach( DataRow objSourceEntityType in objSourceEntityTypes )
			{
				Guid typeID = (Guid) objSourceEntityType[ DSCalculationModuleType.CMTYPEID_FIELD ];
				string strTypeName = (string) objSourceEntityType[ DSCalculationModuleType.CMTYPENAME_FIELD ];
				ICalculationModule objCM = (ICalculationModule) this.Owner.CreateTransientComponentInstance(typeID);

				DataRow objEntityType = objEntityTypes.NewRow( );
				objEntityType[ EntityTypesDS.BUSINESSENTITYTYPENAME_FIELD ] = (string) objSourceEntityType[ DSCalculationModuleType.CMTYPEDISPLAYNAME_FIELD ];
				objEntityType[ EntityTypesDS.BUSINESSENTITYTYPE_FIELD ] = strTypeName;
				objEntityType[ EntityTypesDS.BUSINESSENTITYTYPEID_FIELD ] = (Guid) objSourceEntityType[ DSCalculationModuleType.CMTYPEID_FIELD ];
				objEntityType[ EntityTypesDS.BUSINESSENTITYWORKPAPER_FIELD ] = objCM.ConfigureWPURL;
				objEntityTypes.Rows.Add(objEntityType);
			}

			//Add the organizations CLID to the dataset
			ICalculationModule cmOrganization=(ICalculationModule)this.Broker.GetWellKnownCM(WellKnownCM.Organization);
			if(cmOrganization != null)
				objData.OrganizationCLID = cmOrganization.CLID;
			else
				objData.OrganizationCLID = Guid.Empty;
		}
		#endregion
		
		/// <summary>
		/// Called when an externally originating subscription update is to be delivered to the CM instance.
		/// Externally originating subscription updates are those comming from outside the containment
		/// of the CM rather than from CM instances contained within it.
		/// </summary>
		/// <param name="message">The message containing the subscription update.</param>
		protected override ModifiedState OnSetSubscriptionData(IMessage message)
		{
			ModifiedState modState = base.OnSetSubscriptionData(message);
			// External Subscription updates may be acted on by the CM or may be published
			// to internal CMs by calling the base class function "PostMessage"
			String DataType=(String)message["DataType"];

			if ( message["StreamControl"] != null )
			{
				Message onPublishedMessage=new Message(message);	// Down published message
				onPublishedMessage.Scope=0;							// publish only internally to the group's scope
				onPublishedMessage.InterScenario=false;				// publish only internally to the scenario
				onPublishedMessage.MSID=this.CSID;					// identify as this scenario
				if(onPublishedMessage.Contains("TargetReached"))
					onPublishedMessage.Remove("TargetReached");
				DeliverMessage(onPublishedMessage);
			}
			if(modState != ModifiedState.UNCHANGED)
				return ModifiedState.UNKNOWN;
			else
				return ModifiedState.UNCHANGED;
		}

		public override bool DeleteScenario( Guid objScenarioCSID, Guid objParentCSID, ArrayList objLastScenarioConflicts, ArrayList objMemberOfGroupScenario, ArrayList objLockedScenarios )
		{
			if ( this.IsMemberOfOtherGroupScenario( objParentCSID ) )
			{
				objMemberOfGroupScenario.Add( this.Name );
				return false;
			}

			ILogicalModule objThisLM=this.Broker.GetLogicalCM(this.CLID);
			LogicalModuleBase.CMScenarios cmScenarios = (LogicalModuleBase.CMScenarios)objThisLM.Scenarios;

			try
			{
				//Cannot delete the last scenario
				if( cmScenarios.Count > 1 )
				{
					//Get scenario name for use in logging details
					CMScenario cMScenario = objThisLM.GetScenario(objScenarioCSID);

					if(cMScenario.Locked)
					{
						objLockedScenarios.Add(this.Name);
						return false;
					}

					this.Broker.LogEvent(EventType.ScenarioDeleted,Guid.Empty, "Deleted scenario '"+cMScenario.Name+"' from '"+this.Name+"' in '" + this.ConstructBreadcrumb() + "'");
					
					Guid gdCurrentScenarioBeforeDelete = objThisLM.CurrentScenario;
					objThisLM.DeleteScenario(objScenarioCSID);
					Guid gdCurrentScenarioAfterDelete = objThisLM.CurrentScenario;

					if(gdCurrentScenarioBeforeDelete != gdCurrentScenarioAfterDelete)
					{
						cMScenario = objThisLM.GetScenario(gdCurrentScenarioAfterDelete);
                        this.Broker.LogEvent(EventType.SetCurrentScenario, Guid.Empty, "Current scenario for '" + objThisLM.Name + "' changed to '" + cMScenario.Name + "' in '" + this.ConstructBreadcrumb() + "'");
					}
					
					return true;
				}
				else
				{
					objLastScenarioConflicts.Add( this.Name );
					return false;
				}
			}
			finally
			{
				objThisLM.CalculateToken(true);
				this.Broker.ReleaseBrokerManagedComponent(objThisLM);
			}
		}
		#endregion

		#region OTHER MEMBER FUNCTIONS
		protected override bool HasScenario(string scenarioName, bool checkInMembers, ArrayList conflictingOUNames)
		{
			ILogicalModule objThisLogicalModule = this.Broker.GetLogicalCM( this.CLID );

			try
			{
				if (this.ContainsScenario(scenarioName, objThisLogicalModule))
				{
					conflictingOUNames.Add(this.Name);
					return true;
				}
				else
					return false;
			}
			finally
			{
				this.Broker.ReleaseBrokerManagedComponent(objThisLogicalModule);
			}
		}

		private bool ContainsScenario( string strScenarioName, ILogicalModule objLM )
		{
			IDictionaryEnumerator objScenarioEn = (IDictionaryEnumerator) objLM.Scenarios.GetEnumerator( );

			while( objScenarioEn.MoveNext( ) )
			{
				CMScenario objScenario = (CMScenario) objScenarioEn.Value;
				
				if ( objScenario.Name == strScenarioName )
					return true;
			}
			return false;
		}
		void IBusinessStructureModule.PerformBusinessStructreNavigationOperation(IBusinessStructureNavigatorVisitor navigator)
		{
			if(navigator.NavigationDirection == BusinessStructureNavigationDirection.DownAndAcross &&
                navigator.ShouldVisitReportingUnitNode(this.CLID,this.CSID))
			{
				EntityNodeEventArgs entityNodeEventArgs = new EntityNodeEventArgs();
				entityNodeEventArgs.CLID = this.CLID; 
				entityNodeEventArgs.CSID = this.CSID; 
				entityNodeEventArgs.Entity = this;
				entityNodeEventArgs.OrganisationUnit = this;

				navigator.VisitEntityNode(this,entityNodeEventArgs);
				if(navigator.ShouldNavigateToLowerLevels(BusinessStrucutureNavigatorLevel.Entity))
				{
					ILogicalModule lCM = this.Broker.GetLogicalCM(this.CLID);
					if(lCM != null)
					{
						for(int i=0;i<=lCM.ChildCLIDs.Count-1;i++) 
						{
							IBusinessStructureModule period = (IBusinessStructureModule)this.Broker.GetCMImplementation((Guid)lCM.ChildCLIDs[i],this.CSID);

							// period will be null if the period does not exist in
							// the scenario
							if(period != null)
								period.PerformBusinessStructreNavigationOperation(navigator); 

							this.Broker.ReleaseBrokerManagedComponent(period);
						}
						this.Broker.ReleaseBrokerManagedComponent(lCM);
					}
				}
			}
            else if (navigator.NavigationDirection == BusinessStructureNavigationDirection.UpAndAcross)
            {
                ReadOnlyCollection<IGroup> parentReportingUnits = this.GetParentGroups();

                navigator.IncrementGroupLevelsVisited();

                foreach (IGroup group in parentReportingUnits)
                {
                    if (navigator.ShouldVisitReportingUnitNode(group.CLID, group.CSID))
                    {
                        GroupNodeEventArgs eventArgs = new GroupNodeEventArgs();
                        eventArgs.Group = group;
                        navigator.VisitGroupNode(this, eventArgs);

                        ((IBusinessStructureModule)group).PerformBusinessStructreNavigationOperation(navigator);
                    }
                }

                navigator.DecrementGroupLevelsVisited();
            }
		}
	
		#endregion
	}
}
