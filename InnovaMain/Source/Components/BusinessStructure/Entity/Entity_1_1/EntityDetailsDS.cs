#region CHANGE_HISTORY
/*
 $Header: /2002/3. Implementation/Construction/BusinessStructureCM/Entity/Entity-1-0/EntityDetailsDS.cs 3     11/06/03 5:16p Pveitch $
 $History: EntityDetailsDS.cs $
 * 
 * *****************  Version 3  *****************
 * User: Pveitch      Date: 11/06/03   Time: 5:16p
 * Updated in $/2002/3. Implementation/Construction/BusinessStructureCM/Entity/Entity-1-0
 * Updates to the business structure to support the new use cases, as well
 * as updates to WorkpaperBase to handle multi-user and transaction
 * failures, refactored to make it more reliable and consistent
 * 
 * *****************  Version 2  *****************
 * User: Pveitch      Date: 17/04/03   Time: 5:27p
 * Updated in $/2002/3. Implementation/Construction/BusinessStructureCM/Entity/Entity-1-0
 * 
 * *****************  Version 1  *****************
 * User: Sshrimpton   Date: 18/12/02   Time: 4:55p
 * Created in $/2002/3. Implementation/Construction/BusinessStructureCM/Entity/Entity-1-1-1
 * 
 * *****************  Version 4  *****************
 * User: Pveitch      Date: 7/11/02    Time: 3:07p
 * Updated in $/2002/3. Implementation/Elaboration4/CM/Entity
 * Header Information Added
*/
#endregion
using System;
using System.Data;
using Oritax.TaxSimp.CM.OrganizationUnit;

namespace Oritax.TaxSimp.CM.Entity
{
	[Serializable] 
	public class EntityDetailsDS : OrganisationUnitDetailsDS
	{
		public EntityDetailsDS()
		{
		}
	}
}

