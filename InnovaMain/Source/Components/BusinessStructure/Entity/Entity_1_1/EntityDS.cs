using System;
using System.Data;
using System.Data.SqlClient;

using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Calculation;

namespace Oritax.TaxSimp.CM.Entity
{
	[Serializable]
	public class EntityDS : OrganizationUnitDS
	{
		public const String ENTITY_TABLE			= "EntityCM";

		public EntityDS() : base()
		{
			DataTable dt;

			dt = new DataTable( ENTITY_TABLE );
			this.Tables.Add( dt );
			BrokerManagedComponentDS.AddVersionStamp(dt,typeof(EntityDS));
		}
	}
}
