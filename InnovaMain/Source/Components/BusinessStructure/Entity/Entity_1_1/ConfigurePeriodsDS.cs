using System;
using System.Data;

namespace Oritax.TaxSimp.CM.Entity
{
	/// <summary>
	/// PeriodsDS is a DataSet that contains data for listing periods within an entity.
	/// </summary>
	[Serializable] 
	public class ConfigurePeriodsDS : DataSet
	{
		public const String INCLUDEDPERIODS_TABLE	= "INCLUDEDPERIODS_TABLE";
		public const String AVAILABLEPERIODS_TABLE	= "AVAILABLEPERIODS_TABLE";
		public const String PERIODNAME_FIELD		= "PERIODNAME_FIELD";
		public const String PERIODCLID_FIELD		= "PERIODCLID_FIELD";
		public const String PERIODCSID_FIELD		= "PERIODCSID_FIELD";
		public const String PERIODCIID_FIELD		= "PERIODCIID_FIELD";

		private string entityName;
		private string breadCrumb;
		// Entity Information
		string			cSID;
		string			cIID;
		string			cLID;

		public ConfigurePeriodsDS()
		{
			DataTable table;

			table = new DataTable(INCLUDEDPERIODS_TABLE);
			table.Columns.Add(PERIODNAME_FIELD, typeof(System.String));
			table.Columns.Add(PERIODCLID_FIELD, typeof(System.String));
			table.Columns.Add(PERIODCSID_FIELD, typeof(System.String));
			table.Columns.Add(PERIODCIID_FIELD, typeof(System.String));
			this.Tables.Add(table);

			table = new DataTable(AVAILABLEPERIODS_TABLE);
			table.Columns.Add(PERIODNAME_FIELD, typeof(System.String));
			table.Columns.Add(PERIODCLID_FIELD, typeof(System.String));
			table.Columns.Add(PERIODCSID_FIELD, typeof(System.String));
			table.Columns.Add(PERIODCIID_FIELD, typeof(System.String));
			this.Tables.Add(table);

			entityName="";
			breadCrumb="";
		}

		public string EntityName{get{return entityName;}set{entityName=value;}}
		public string BreadCrumb{get{return breadCrumb;}set{breadCrumb=value;}}
		public string CSID {get{return cSID;}set{cSID=value;}}
		public string CIID	{get{return cIID;}set{cIID=value;}}
		public string CLID	{get{return cLID;}set{cLID=value;}}
	}
}

