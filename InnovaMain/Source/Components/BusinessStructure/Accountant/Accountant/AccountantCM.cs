using System;
using System.Collections.Generic;
using System.Data;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Xml.Linq;
using System.Linq;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Enum;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Utilities;
using OTC = Oritax.TaxSimp.Common;
using System.Xml;
using System.IO;
using Oritax.TaxSimp.DataSets;

namespace Oritax.TaxSimp.CM.Group
{
    [Serializable]
    public sealed class AccountantCM : GroupCM, ISerializable, IIdentityCM, ICmHasParent, IHaveAssociation, ISupportMigration
    {
        #region PRIVATE
        private List<IdentityCM> _Children;
        private AccountantEntity _AccountantEntity;

        public override IClientEntity ClientEntity
        {
            get
            {
                return _AccountantEntity;
            }
            set
            {
                _AccountantEntity = value as AccountantEntity;
            }
        }
        #endregion

        #region PUBLIC
        public IdentityCM Parent { get; set; }
        public List<IdentityCM> Children { get { return _Children; } set { _Children = value; } }
        #endregion

        public AccountantCM()
            : base()
        {
            Parent = IdentityCM.Null;
            _Children = new List<IdentityCM>();
            _AccountantEntity = new AccountantEntity();
        }

        public AccountantCM(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            Parent = info.GetValue<IdentityCM>("Oritax.TaxSimp.CM.Group.AccountantCM.Parent");
            _Children = info.GetValue<List<IdentityCM>>("Oritax.TaxSimp.CM.Group.AccountantCM.Children");
            _AccountantEntity = info.GetValue<AccountantEntity>("Oritax.TaxSimp.CM.Group.AccountantCM.AccountantEntity");
        }

        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValueX("Oritax.TaxSimp.CM.Group.AccountantCM.Parent", Parent);
            info.AddValueX("Oritax.TaxSimp.CM.Group.AccountantCM.Children", _Children);
            info.AddValueX("Oritax.TaxSimp.CM.Group.AccountantCM.AccountantEntity", _AccountantEntity);
        }

        public override string GetDataStream(int type, string data)
        {
            if (type == -911)
                type = (int)CmCommand.EditOrganizationUnit;

            string xml = string.Empty;
            switch ((CmCommand)type)
            {
                case CmCommand.GetIncludedAdvisorList:
                    xml = GetOrganizationUnitNameList(_Children).ToXmlString();
                    break;
                case CmCommand.EditOrganizationUnit:
                    _AccountantEntity.OrganizationStatus = this.OrganizationStatus;
                    _AccountantEntity.ClientId = this.ClientId;
                    xml = _AccountantEntity.ToXmlString();
                    break;
                case CmCommand.XMLData:
                    xml = ExtractXML(data);
                    break;
                case CmCommand.GetEntitySecurity:
                    xml = GetEntitySecurity();
                    break;
                default:

                    xml = this._AccountantEntity.ToXmlString();
                    // code changes from here for view log
                    if (ClientId == "" || ClientId == null)
                    {
                        UpDateEventLog.UpdateLog("Accountant", _AccountantEntity.Name, type, this.Broker);
                    }
                    else
                    {
                        UpDateEventLog.UpdateLog("Accountant", _AccountantEntity.Name + " (" + ClientId + ")", type, this.Broker);
                    }
                    // code changes till here for view log
                    break;
            }
            return xml;
        }

        public override string OnUpdateDataStream(int type, string data)
        {
            string xml = string.Empty;
            AccountantEntity _accountantentity = null;
            IdentityCM child = null;
            List<OTC.OrganizationUnit> list = null;
            OTC.OrganizationUnit ou = null;
            switch ((CmCommand)type)
            {
                case CmCommand.AttachToParent:
                    if (Parent != IdentityCM.Null) throw new Exception("Accountant is already part of Accountants");
                    IdentityCM parent = GetParent(data);
                    Parent = parent;
                    break;
                case CmCommand.DeAttachFromParent:
                    Parent = IdentityCM.Null;
                    break;
                case CmCommand.Add_Single:
                    child = GetChild(data);
                    BMCUpdateComponent(child, CmCommand.Add_Single);
                    _Children.Add(child);
                    break;
                case CmCommand.Remove_Single:
                    child = GetChild(data);
                    BMCUpdateComponent(child, CmCommand.Remove_Single);
                    _Children.RemoveAll(c => c == child);//c => c.Clid == child.Clid && c.Csid == child.Csid);                    
                    break;
                case CmCommand.SPAddLibrary:
                    var _value = data.ToNewOrData<Oritax.TaxSimp.Common.SPLibraryDocument>();
                    if (_value != null)
                    {
                        SPLibrary.AddLibraryToClient(_value.CurrentUser.CurrentUserName, _value.Name);
                    }
                    break;
                case CmCommand.Add_List:
                    list = data.ToNewOrData<List<OTC.OrganizationUnit>>();
                    foreach (var each in list)
                    {
                        child = GetChild(each.ToXmlString());
                        BMCUpdateComponent(child, CmCommand.Add_Single);
                        _Children.Add(child);
                    }
                    break;
                case CmCommand.Remove_List:
                    list = data.ToNewOrData<List<OTC.OrganizationUnit>>();
                    foreach (var each in list)
                    {
                        child = GetChild(each.ToXmlString());
                        BMCUpdateComponent(child, CmCommand.Remove_Single);
                        _Children.RemoveAll(c => c.Clid == child.Clid && c.Csid == child.Csid);
                    }
                    break;
                case CmCommand.EditOrganizationUnit:
                    _accountantentity = data.ToNewOrData<AccountantEntity>();
                    this.Name = _accountantentity.Name;
                    this.OrganizationStatus = _accountantentity.OrganizationStatus;
                    _AccountantEntity = _accountantentity;
                    ou = new OTC.OrganizationUnit();
                    ou.Clid = this.Clid;
                    ou.Csid = this.Csid;
                    ou.Name = _accountantentity.Name;
                    xml = ou.ToXmlString();
                    break;
                case CmCommand.AddEntitySecurity:
                    xml = AddEntitySecurity(data);
                    break;
                case CmCommand.DeleteEntitySecurity:
                    xml = DeleteEntitySecurity(data);
                    break;
                case CmCommand.GetEntitySecurity:
                    xml = GetEntitySecurity();
                    break;
                case CmCommand.UpdateClientID:// for the purpose of updating client id's
                    break;
                default:
                    _accountantentity = data.ToNewOrData<AccountantEntity>();
                    this.Name = _accountantentity.Name;
                    this.OrganizationStatus = _accountantentity.OrganizationStatus;
                    _AccountantEntity = _accountantentity;
                    if (ClientId == "" || ClientId == null)
                    {
                        UpDateEventLog.UpdateLog("Accountant", _AccountantEntity.Name, type, this.Broker);
                    }
                    else
                    {
                        UpDateEventLog.UpdateLog("Accountant", _AccountantEntity.Name + " (" + ClientId + ")", type, this.Broker);
                    }

                    break;
            }
            return xml;
        }
        protected override void OnGetData(DataSet data)
        {
            #region Set Default Fields
            base.OnGetData(data);
            _AccountantEntity.OrganizationStatus = this.OrganizationStatus;
            _AccountantEntity.ClientId = this.ClientId;
            this.Name = _AccountantEntity.Name;
            #endregion

            if (data is AccountantDS)
            {
                var ds = data as AccountantDS;
                switch (ds.CommandType)
                {
                    case DatasetCommandTypes.Details:
                        {
                            AddDetailsRow(ds.accountantTable);
                        }
                        break;

                    default:
                        {
                            DataRow r = ds.accountantTable.NewRow();
                            r[ds.accountantTable.CLIENTID] = ClientId;
                            r[ds.accountantTable.TRADINGNAME] = _AccountantEntity.Name;
                            r[ds.accountantTable.LEGALNAME] = _AccountantEntity.LegalName;
                            r[ds.accountantTable.ACCOUNTANTTYPE] = _AccountantEntity.AccountantType;
                            r[ds.accountantTable.TYPE] = typeName;
                            r[ds.accountantTable.STATUS] = OrganizationStatus;
                            r[ds.accountantTable.CID] = CID;
                            r[ds.accountantTable.CLID] = Clid;
                            r[ds.accountantTable.CSID] = Csid;
                            ds.accountantTable.Rows.Add(r);
                        }
                        break;
                }
            }
            else if (data is MembershipDS)
            {
                var ds = data as MembershipDS;
                switch (ds.CommandType)
                {
                    case DatasetCommandTypes.GetChildren:
                        GetChildren(Children, ds, Broker);
                        break;
                }

            }
            else if (data is ClientMappingControlDS)
            {
                var ds = data as ClientMappingControlDS;
                if (_AccountantEntity.Clients == null)
                {
                    _AccountantEntity.Clients = new List<Oritax.TaxSimp.Common.IdentityCMDetail>();
                }
                GetclientMappingDs(data as ClientMappingControlDS, _AccountantEntity.Clients, Broker);

            }
            else if (data is AddressDetailsDS)
            {
                var ds = data as AddressDetailsDS;
                GetAccountantAddressDetails(ds, _AccountantEntity);
            }
            else if (data is BankAccountDS)
            {
                if (_AccountantEntity.BankAccounts == null)
                    _AccountantEntity.BankAccounts = new List<Oritax.TaxSimp.Common.IdentityCMDetail>();
                GetBankAccountDs(data as BankAccountDS, _AccountantEntity.BankAccounts, Broker);
            }
            else if (data is IndividualDS)
            {
                GetIndividualDs(data as IndividualDS, _AccountantEntity, Broker);
            }
            //else if (data is ClientsByAccountantDS)
            //{
            //    foreach (IdentityCM identityCM in this._Children)
            //    {
            //        IBrokerManagedComponent ibmc = Broker.GetCMImplementation(identityCM.Clid, identityCM.Csid) as IBrokerManagedComponent;

            //        if (ibmc != null)
            //        {
            //            ibmc.GetData(data);
            //            Broker.ReleaseBrokerManagedComponent(ibmc);
            //        }
            //    }
            //}
            else if (data is InstanceCMDS)
            {
                var ds = data as InstanceCMDS;
                GetInstanceDetails(ds, _AccountantEntity, Broker);
            }
        }

        public static void GetBankAccountDs(BankAccountDS dataSet, List<Oritax.TaxSimp.Common.IdentityCMDetail> bankAccounts, ICMBroker broker)
        {
            if (bankAccounts != null && bankAccounts.Count > 0)
            {
                foreach (var bank in bankAccounts)
                {
                    var entity = broker.GetCMImplementation(bank.Clid, bank.Csid);
                    if (entity != null)
                    {
                        entity.GetData(dataSet);
                        broker.ReleaseBrokerManagedComponent(entity);
                    }
                }
            }
        }

        private void GetIndividualDs(IndividualDS individualDs, AccountantEntity accountantEntity, ICMBroker broker)
        {
            List<Oritax.TaxSimp.Common.IdentityCMDetail> individuals = null;

            switch (individualDs.IndividualsType)
            {
                case IndividualsType.Applicants:
                    individuals = accountantEntity.Applicants;
                    break;
                case IndividualsType.Signatories:
                    individuals = accountantEntity.Signatories;
                    break;
                case IndividualsType.Directors:
                    individuals = accountantEntity.Directors;
                    break;
            }

            if (individuals != null)
                foreach (Oritax.TaxSimp.Common.IdentityCMDetail identityCM in individuals)
                {
                    var indvCM = broker.GetCMImplementation(identityCM.Clid, identityCM.Csid);
                    if (indvCM != null)
                    {
                        indvCM.GetData(individualDs);
                        if (identityCM.BusinessTitle != null)
                            individualDs.IndividualTable.Rows[individualDs.IndividualTable.Rows.Count - 1][individualDs.IndividualTable.BUSINESSTITLE] = identityCM.BusinessTitle.Title;
                        broker.ReleaseBrokerManagedComponent(indvCM);
                    }
                }
        }

        private void GetInstanceDetails(InstanceCMDS ds, AccountantEntity accountantEntity, ICMBroker broker)
        {
            List<Common.IdentityCMDetail> units = null;
            if (ds.CommandType == DatasetCommandTypes.GetChildren)
            {
                switch (ds.UnitPropertyType)
                {
                    case UnitPropertyType.ShareHolders:
                        units = accountantEntity.ShareHolders;

                        break;
                    case UnitPropertyType.Partnerships:
                        units = accountantEntity.Partners;
                        break;

                    case UnitPropertyType.Corporates:
                        units = new List<Oritax.TaxSimp.Common.IdentityCMDetail>();
                        if (accountantEntity.CorporatePrivate != null)
                            foreach (var identityCm in accountantEntity.CorporatePrivate)
                            {
                                units.Add(Oritax.TaxSimp.Common.IdentityCMDetail.FromIdetityCM(identityCm));
                            }
                        if (accountantEntity.CorporatePublic != null)
                            foreach (var identityCm in accountantEntity.CorporatePublic)
                            {
                                units.Add(Oritax.TaxSimp.Common.IdentityCMDetail.FromIdetityCM(identityCm));
                            }

                        break;

                }
                if (units != null)
                {
                    foreach (var identityCM in units)
                    {
                        var bmc = broker.GetCMImplementation(identityCM.Clid, identityCM.Csid);
                        if (bmc != null)
                        {
                            DataRow dr = ds.InstanceCMDetailsTable.NewRow();
                            dr[ds.InstanceCMDetailsTable.CID] = bmc.CID;
                            dr[ds.InstanceCMDetailsTable.CLID] = bmc.CLID;
                            dr[ds.InstanceCMDetailsTable.CSID] = bmc.CSID;
                            dr[ds.InstanceCMDetailsTable.NAME] = bmc.Name;
                            dr[ds.InstanceCMDetailsTable.TYPE] = bmc.TypeName;
                            dr[ds.InstanceCMDetailsTable.STATUS] = (bmc as OrganizationUnitCM).OrganizationStatus;
                            ds.InstanceCMDetailsTable.Rows.Add(dr);
                            broker.ReleaseBrokerManagedComponent(bmc);
                        }
                    }
                }
            }
        }

        private void GetAccountantAddressDetails(AddressDetailsDS ds, AccountantEntity accountantEntity)
        {

            if (accountantEntity.Address == null)
            {

                accountantEntity.Address = new Common.DualAddressEntity();

            }
            FillAccountantAddresses(ds, accountantEntity.Address);
        }

        private static void FillAccountantAddresses(AddressDetailsDS ds, Common.DualAddressEntity address)
        {
            #region Get Business Address

            if (address.BusinessAddress != null)
            {
                FillAddress(ds, address.BusinessAddress, AddressType.BusinessAddress);
            }
            if (address.MailingAddress != null)
            {
                FillAddress(ds, address.MailingAddress, AddressType.MailingAddress);
            }
            if (address.RegisteredAddress != null)
            {
                FillAddress(ds, address.RegisteredAddress, AddressType.RegisteredAddress);
            }
            if (address.ResidentialAddress != null)
            {
                FillAddress(ds, address.ResidentialAddress, AddressType.ResidentialAddress);
            }
            if (address.DuplicateMailingAddress != null)
            {
                FillAddress(ds, address.DuplicateMailingAddress, AddressType.DuplicateMailingAddress);
            }
            #endregion
        }

        private static void FillAddress(AddressDetailsDS ds, Common.AddressEntity address, AddressType addressType)
        {

            DataRow drBusinessAddress = ds.ClientDetailsTable.NewRow();
            drBusinessAddress[ds.ClientDetailsTable.ADDRESSLINE1] = address.Addressline1;
            drBusinessAddress[ds.ClientDetailsTable.ADDRESSLINE2] = address.Addressline2;
            drBusinessAddress[ds.ClientDetailsTable.SUBURB] = address.Suburb;
            drBusinessAddress[ds.ClientDetailsTable.STATE] = address.State;
            drBusinessAddress[ds.ClientDetailsTable.COUNTRY] = address.Country;
            drBusinessAddress[ds.ClientDetailsTable.POSTCODE] = address.PostCode;
            drBusinessAddress[ds.ClientDetailsTable.ADDRESSTYPE] = addressType;
            ds.ClientDetailsTable.Rows.Add(drBusinessAddress);
        }

        private void AddDetailsRow(AccountantTable accountantTable)
        {
            DataRow r = accountantTable.NewRow();
            r[accountantTable.CLIENTID] = ClientId;
            r[accountantTable.TRADINGNAME] = _AccountantEntity.Name;
            r[accountantTable.LEGALNAME] = _AccountantEntity.LegalName;
            r[accountantTable.ACCOUNTANTTYPE] = _AccountantEntity.AccountantType;
            r[accountantTable.TYPE] = typeName;
            r[accountantTable.STATUS] = OrganizationStatus;
            r[accountantTable.CID] = CID;
            r[accountantTable.CLID] = Clid;
            r[accountantTable.CSID] = Csid;

            switch (_AccountantEntity.AccountantType)
            {
                case AccountantEntityType.AccountantCompanyControl:
                    {
                        if (_AccountantEntity.Facsimile != null)
                        {
                            r[accountantTable.FACSIMILECOUNTRYCODE] = _AccountantEntity.Facsimile.CountryCode;
                            r[accountantTable.FACSIMILECITYCODE] = _AccountantEntity.Facsimile.CityCode;
                            r[accountantTable.FACSIMILE] = _AccountantEntity.Facsimile.PhoneNumber;
                        }

                        if (_AccountantEntity.PhoneNumber != null)
                        {
                            r[accountantTable.PHONENUMBERCOUNTRYCODE] = _AccountantEntity.PhoneNumber.CountryCode;
                            r[accountantTable.PHONENUMBERCITYCODE] = _AccountantEntity.PhoneNumber.CityCode;
                            r[accountantTable.PHONENUBER] = _AccountantEntity.PhoneNumber.PhoneNumber;
                        }
                        r[accountantTable.WEBSITEADDRESS] = _AccountantEntity.Websiteaddress;
                        r[accountantTable.ABN] = _AccountantEntity.ABN;
                        r[accountantTable.ACN] = _AccountantEntity.ACN;
                        r[accountantTable.TFN] = _AccountantEntity.TFN;
                    }
                    break;

                case AccountantEntityType.AccountantIndividualControl:
                    {
                        if (_AccountantEntity.Facsimile != null)
                        {
                            r[accountantTable.FACSIMILECOUNTRYCODE] = _AccountantEntity.Facsimile.CountryCode;
                            r[accountantTable.FACSIMILECITYCODE] = _AccountantEntity.Facsimile.CityCode;
                            r[accountantTable.FACSIMILE] = _AccountantEntity.Facsimile.PhoneNumber;
                        }

                        if (_AccountantEntity.PhoneNumber != null)
                        {
                            r[accountantTable.PHONENUMBERCOUNTRYCODE] = _AccountantEntity.PhoneNumber.CountryCode;
                            r[accountantTable.PHONENUMBERCITYCODE] = _AccountantEntity.PhoneNumber.CityCode;
                            r[accountantTable.PHONENUBER] = _AccountantEntity.PhoneNumber.PhoneNumber;
                        }
                        r[accountantTable.WEBSITEADDRESS] = _AccountantEntity.Websiteaddress;
                        r[accountantTable.ABN] = _AccountantEntity.ABN;
                        r[accountantTable.ACN] = _AccountantEntity.ACN;
                    }
                    break;
                case AccountantEntityType.AccountantPartnershipControl:
                    {

                        if (_AccountantEntity.Facsimile != null)
                        {
                            r[accountantTable.FACSIMILECOUNTRYCODE] = _AccountantEntity.Facsimile.CountryCode;
                            r[accountantTable.FACSIMILECITYCODE] = _AccountantEntity.Facsimile.CityCode;
                            r[accountantTable.FACSIMILE] = _AccountantEntity.Facsimile.PhoneNumber;
                        }

                        if (_AccountantEntity.PhoneNumber != null)
                        {
                            r[accountantTable.PHONENUMBERCOUNTRYCODE] = _AccountantEntity.PhoneNumber.CountryCode;
                            r[accountantTable.PHONENUMBERCITYCODE] = _AccountantEntity.PhoneNumber.CityCode;
                            r[accountantTable.PHONENUBER] = _AccountantEntity.PhoneNumber.PhoneNumber;
                        }
                        r[accountantTable.WEBSITEADDRESS] = _AccountantEntity.Websiteaddress ?? string.Empty;
                        r[accountantTable.ABN] = _AccountantEntity.ABN;
                        r[accountantTable.TFN] = _AccountantEntity.TFN;

                    }
                    break;
                case AccountantEntityType.AccountantTrustControl:
                    {
                        if (_AccountantEntity.Facsimile != null)
                        {
                            r[accountantTable.FACSIMILECOUNTRYCODE] = _AccountantEntity.Facsimile.CountryCode;
                            r[accountantTable.FACSIMILECITYCODE] = _AccountantEntity.Facsimile.CityCode;
                            r[accountantTable.FACSIMILE] = _AccountantEntity.Facsimile.PhoneNumber;
                        }

                        if (_AccountantEntity.PhoneNumber != null)
                        {
                            r[accountantTable.PHONENUMBERCOUNTRYCODE] = _AccountantEntity.PhoneNumber.CountryCode;
                            r[accountantTable.PHONENUMBERCITYCODE] = _AccountantEntity.PhoneNumber.CityCode;
                            r[accountantTable.PHONENUBER] = _AccountantEntity.PhoneNumber.PhoneNumber;
                        }
                        r[accountantTable.WEBSITEADDRESS] = _AccountantEntity.Websiteaddress;
                        r[accountantTable.ABN] = _AccountantEntity.ABN;
                        r[accountantTable.ACN] = _AccountantEntity.ACN;
                    }
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
            accountantTable.Rows.Add(r);
        }

        protected override ModifiedState OnSetData(DataSet data)
        {
            base.OnSetData(data);
            if (data is ClientMappingControlDS)
            {
                var ds = data as ClientMappingControlDS;
                if (_AccountantEntity.Clients == null)
                {
                    _AccountantEntity.Clients = new List<OTC.IdentityCMDetail>();
                }

                switch (ds.CommandType)
                {
                    case DatasetCommandTypes.Add:
                        foreach (DataRow dr in ds.ClientMappingTable.Rows)
                        {
                            var client = new Oritax.TaxSimp.Common.IdentityCMDetail();
                            client.Cid = new Guid(dr[ds.ClientMappingTable.CID].ToString());
                            client.Clid = new Guid(dr[ds.ClientMappingTable.CLID].ToString());
                            client.Csid = new Guid(dr[ds.ClientMappingTable.CSID].ToString());
                            if (_AccountantEntity.Clients.Count(ss => ss.Clid == client.Clid && ss.Csid == client.Csid) == 0)
                            {
                                try
                                {

                                    _AccountantEntity.Clients.Add(client);
                                }
                                catch (Exception ex)
                                {
                                    dr.RowError = ex.Message;
                                }
                            }
                        }
                        break;
                    case DatasetCommandTypes.Delete:
                        foreach (DataRow dr in ds.ClientMappingTable.Rows)
                        {
                            var client = new IdentityCM();
                            client.Cid = new Guid(dr[ds.ClientMappingTable.CID].ToString());
                            client.Clid = new Guid(dr[ds.ClientMappingTable.CLID].ToString());
                            client.Csid = new Guid(dr[ds.ClientMappingTable.CSID].ToString());

                           _AccountantEntity.Clients.RemoveAll(ss => ss.Clid == client.Clid && ss.Csid == client.Csid);
                           
                        }
                        break;
                }
            }

            if (data is MembershipDS)
            {
                var ds = data as MembershipDS;
                IdentityCM child;
                switch (ds.CommandType)
                {
                    case DatasetCommandTypes.Add:
                        foreach (DataRow dr in ds.membershipTable.Rows)
                        {
                            child = new IdentityCM();
                            child.Cid = new Guid(dr[ds.membershipTable.CID].ToString());
                            child.Clid = new Guid(dr[ds.membershipTable.CLID].ToString());
                            child.Csid = new Guid(dr[ds.membershipTable.CSID].ToString());
                            if (Children.Count(ss => ss.Clid == child.Clid && ss.Csid == child.Csid) == 0)
                            {
                                try
                                {
                                    BMCUpdateComponentParent(child, CmCommand.Add_Single, Broker);
                                    Children.Add(child);
                                }
                                catch (Exception ex)
                                {

                                    dr.RowError = ex.Message;
                                }
                            }
                        }
                        break;
                    case DatasetCommandTypes.Delete:
                        foreach (DataRow dr in ds.membershipTable.Rows)
                        {
                            child = new IdentityCM();
                            child.Cid = new Guid(dr[ds.membershipTable.CID].ToString());
                            child.Clid = new Guid(dr[ds.membershipTable.CLID].ToString());
                            child.Csid = new Guid(dr[ds.membershipTable.CSID].ToString());
                            if (Children.Count(ss => ss.Clid == child.Clid && ss.Csid == child.Csid) > 0)
                            {
                                BMCUpdateComponentParent(child, CmCommand.Remove_Single, Broker);
                                Children.Remove(child);
                            }
                        }
                        break;
                }
            }


            else if (data is AccountantDS)
            {
                var ds = data as AccountantDS;
                switch (ds.CommandType)
                {
                    case DatasetCommandTypes.Add:

                        if (ds.accountantTable.Rows.Count > 0)
                        {
                            var accountantEntity = new AccountantEntity();
                            accountantEntity.ClientId = ClientId;
                            SetEntityDetails(accountantEntity, ds.accountantTable);
                            _AccountantEntity = accountantEntity;
                        }
                        break;
                    case DatasetCommandTypes.Update:
                        if (ds.accountantTable.Rows.Count > 0)
                        {
                            SetEntityDetails(_AccountantEntity, ds.accountantTable);
                        }

                        break;
                }
            }
            else if (data is AddressDetailsDS)
            {
                var ds = data as AddressDetailsDS;
                SetAccountantAddresses(ds, _AccountantEntity.Address);
            }
            else if (data is BankAccountDS)
            {
                if (_AccountantEntity.BankAccounts == null)
                    _AccountantEntity.BankAccounts = new List<Oritax.TaxSimp.Common.IdentityCMDetail>();
                var ds = data as BankAccountDS;
                if (ds.CommandType == DatasetCommandTypes.Delete)
                    RemoveBankAccount(ds, _AccountantEntity.BankAccounts, Broker);
                else if (ds.CommandType == DatasetCommandTypes.Update)
                    AssociateBankAccount(ds, _AccountantEntity.BankAccounts);
            }
            else if (data is IndividualDS)
            {
                var ds = data as IndividualDS;

                List<Oritax.TaxSimp.Common.IdentityCMDetail> individuals = null;


                switch (ds.IndividualsType)
                {
                    case IndividualsType.Applicants:
                        if (_AccountantEntity.Applicants == null)
                            _AccountantEntity.Applicants = new List<Oritax.TaxSimp.Common.IdentityCMDetail>();
                        individuals = _AccountantEntity.Applicants;
                        break;
                    case IndividualsType.Signatories:
                        if (_AccountantEntity.Signatories == null)
                            _AccountantEntity.Signatories = new List<Oritax.TaxSimp.Common.IdentityCMDetail>();
                        individuals = _AccountantEntity.Signatories;
                        break;
                    case IndividualsType.Directors:
                        if (_AccountantEntity.Directors == null)
                            _AccountantEntity.Directors = new List<Oritax.TaxSimp.Common.IdentityCMDetail>();
                        individuals = _AccountantEntity.Directors;
                        break;

                }
                if (ds.CommandType == DatasetCommandTypes.Delete)
                    RemoveIndividual(ds, individuals, Broker);
                else if (ds.CommandType == DatasetCommandTypes.Update)
                    AssociateIndividuals(ds, individuals);
            }
            else if (data is InstanceCMDS)
            {
                var ds = data as InstanceCMDS;


                switch (ds.UnitPropertyType)
                {
                    case UnitPropertyType.ShareHolders:
                        if (_AccountantEntity.ShareHolders == null)
                            _AccountantEntity.ShareHolders = new List<Oritax.TaxSimp.Common.IdentityCMDetail>();
                        if (ds.CommandType == DatasetCommandTypes.Delete)
                            RemoveUnits(ds, _AccountantEntity.ShareHolders, Broker);
                        else if (ds.CommandType == DatasetCommandTypes.Update)
                            AssociateUnits(ds, _AccountantEntity.ShareHolders);
                        break;
                    case UnitPropertyType.Partnerships:
                        if (_AccountantEntity.Partners == null)
                            _AccountantEntity.Partners = new List<Oritax.TaxSimp.Common.IdentityCMDetail>();

                        if (ds.CommandType == DatasetCommandTypes.Delete)
                            RemoveUnits(ds, _AccountantEntity.Partners, Broker);
                        else if (ds.CommandType == DatasetCommandTypes.Update)
                            AssociateUnits(ds, _AccountantEntity.Partners);
                        break;
                    case UnitPropertyType.Corporates:
                        if (_AccountantEntity.CorporatePrivate == null)
                            _AccountantEntity.CorporatePrivate = new List<IdentityCM>();
                        if (_AccountantEntity.CorporatePublic == null)
                            _AccountantEntity.CorporatePublic = new List<IdentityCM>();

                        if (ds.CommandType == DatasetCommandTypes.Delete)
                        {
                            RemoveUnits(ds, _AccountantEntity.CorporatePrivate, Broker);
                            RemoveUnits(ds, _AccountantEntity.CorporatePublic, Broker);
                        }
                        else if (ds.CommandType == DatasetCommandTypes.Update)
                        {
                            AssociateUnits(ds, _AccountantEntity.CorporatePublic, OrganizationType.ClientCorporationPublic);
                            AssociateUnits(ds, _AccountantEntity.CorporatePrivate, OrganizationType.ClientCorporationPrivate);
                        }
                        break;
                }
            }
            #region set default values
            this.Name = _AccountantEntity.Name;
            this.OrganizationStatus = _AccountantEntity.OrganizationStatus;
            #endregion
            return ModifiedState.MODIFIED;

        }

        public static void GetclientMappingDs(ClientMappingControlDS dataSet, List<Oritax.TaxSimp.Common.IdentityCMDetail> clientMapping, ICMBroker broker)
        {
            if (clientMapping != null && clientMapping.Count > 0)
            {
                var commandType = dataSet.CommandType;
                dataSet.CommandType = DatasetCommandTypes.Get;
                ;
                foreach (var client in clientMapping)
                {
                    var entity = broker.GetCMImplementation(client.Clid, client.Csid);
                    if (entity != null)
                    {

                        entity.GetData(dataSet);
                        broker.ReleaseBrokerManagedComponent(entity);
                    }
                }
                dataSet.CommandType = commandType;

            }
        }

        public void BMCUpdateComponentParent(IdentityCM child, CmCommand cmd, ICMBroker broker)
        {
            OrganizationUnitCM component = broker.GetCMImplementation(child.Clid, child.Csid) as OrganizationUnitCM;
            if (component == null && child.Cid != null)
                component = broker.GetBMCInstance(child.Cid) as OrganizationUnitCM;
            if (component != null)
            {
                switch (cmd)
                {
                    case CmCommand.Add_Single:

                        component.UpdateDataStream((int)CmCommand.AttachToParent,
                            new AccountanttoAdvisor()
                                {
                                    Clid = this.Clid,
                                    Csid = this.Csid,
                                    Address = _AccountantEntity.Address,
                                    WorkPhoneNumber = _AccountantEntity.PhoneNumber,
                                    FaxNumber = _AccountantEntity.Facsimile
                                }.ToXmlString());
                        break;
                    case CmCommand.Remove_Single:

                        component.UpdateDataStream((int)CmCommand.DeAttachFromParent,
                            new AccountanttoAdvisor()
                                {
                                    Clid = this.Clid,
                                    Csid = this.Csid,
                                    Address = new Oritax.TaxSimp.Common.DualAddressEntity(),
                                    WorkPhoneNumber = new PhoneNumberEntity(),
                                    FaxNumber = new PhoneNumberEntity()
                                }.ToXmlString());
                        break;
                }
                broker.ReleaseBrokerManagedComponent(component);
            }
        }

        private static void SetAccountantAddresses(AddressDetailsDS ds, Oritax.TaxSimp.Common.DualAddressEntity Address)
        {
            foreach (DataRow dr in ds.ClientDetailsTable.Rows)
            {
                if ((Oritax.TaxSimp.DataSets.AddressType)System.Enum.Parse(typeof(Oritax.TaxSimp.DataSets.AddressType), dr[ds.ClientDetailsTable.ADDRESSTYPE].ToString()) == Oritax.TaxSimp.DataSets.AddressType.BusinessAddress)
                {
                    SetAddress(ds.ClientDetailsTable, Address.BusinessAddress, dr);
                }
                else if (
                    (Oritax.TaxSimp.DataSets.AddressType)System.Enum.Parse(typeof(Oritax.TaxSimp.DataSets.AddressType), dr[ds.ClientDetailsTable.ADDRESSTYPE].ToString()) == Oritax.TaxSimp.DataSets.AddressType.RegisteredAddress)
                {
                    SetAddress(ds.ClientDetailsTable, Address.RegisteredAddress, dr);
                }
                else if (
                    (Oritax.TaxSimp.DataSets.AddressType)
                    System.Enum.Parse(typeof(Oritax.TaxSimp.DataSets.AddressType), dr[ds.ClientDetailsTable.ADDRESSTYPE].ToString()) == Oritax.TaxSimp.DataSets.AddressType.ResidentialAddress)
                {
                    SetAddress(ds.ClientDetailsTable, Address.ResidentialAddress, dr);
                }
                else if (
                    (Oritax.TaxSimp.DataSets.AddressType)
                    System.Enum.Parse(typeof(Oritax.TaxSimp.DataSets.AddressType), dr[ds.ClientDetailsTable.ADDRESSTYPE].ToString()) == Oritax.TaxSimp.DataSets.AddressType.MailingAddress)
                {
                    SetAddress(ds.ClientDetailsTable, Address.MailingAddress, dr);
                }
                else if (
                    (Oritax.TaxSimp.DataSets.AddressType)
                    System.Enum.Parse(typeof(Oritax.TaxSimp.DataSets.AddressType), dr[ds.ClientDetailsTable.ADDRESSTYPE].ToString()) == Oritax.TaxSimp.DataSets.AddressType.DuplicateMailingAddress)
                {
                    SetAddress(ds.ClientDetailsTable, Address.DuplicateMailingAddress, dr);
                }
            }
        }

        private static void SetAddress(AddressDetails table, Oritax.TaxSimp.Common.AddressEntity address, DataRow dr)
        {
            address.Addressline1 = dr[table.ADDRESSLINE1].ToString();
            address.Addressline2 = dr[table.ADDRESSLINE2].ToString();
            address.Suburb = dr[table.SUBURB].ToString();
            address.State = dr[table.STATE].ToString();
            address.Country = dr[table.COUNTRY].ToString();
            address.PostCode = dr[table.POSTCODE].ToString();
        }

        private void SetEntityDetails(AccountantEntity entity, AccountantTable accountantTable)
        {
            DataRow dr = accountantTable.Rows[0];
            entity.ClientId = dr[accountantTable.CLIENTID].ToString();
            entity.Name = dr[accountantTable.TRADINGNAME].ToString();
            entity.LegalName = dr[accountantTable.LEGALNAME].ToString();
            entity.AccountantType = (AccountantEntityType)System.Enum.Parse(typeof(AccountantEntityType),
                dr[accountantTable.ACCOUNTANTTYPE].ToString());
            entity.OrganizationStatus = dr[accountantTable.STATUS].ToString();
            switch (entity.AccountantType)
            {
                case AccountantEntityType.AccountantCompanyControl:
                    {
                        entity.Facsimile = new PhoneNumberEntity();
                        entity.Facsimile.CountryCode = dr[accountantTable.FACSIMILECOUNTRYCODE].ToString();
                        entity.Facsimile.CityCode = dr[accountantTable.FACSIMILECITYCODE].ToString();
                        entity.Facsimile.PhoneNumber = dr[accountantTable.FACSIMILE].ToString();

                        entity.PhoneNumber = new PhoneNumberEntity();
                        entity.PhoneNumber.CountryCode = dr[accountantTable.PHONENUMBERCOUNTRYCODE].ToString();
                        entity.PhoneNumber.CityCode = dr[accountantTable.PHONENUMBERCITYCODE].ToString();
                        entity.PhoneNumber.PhoneNumber = dr[accountantTable.PHONENUBER].ToString();

                        entity.Websiteaddress = dr[accountantTable.WEBSITEADDRESS].ToString();
                        entity.ABN = dr[accountantTable.ABN].ToString();
                        entity.ACN = dr[accountantTable.ACN].ToString();
                        entity.TFN = dr[accountantTable.TFN].ToString();
                    }
                    break;
                case AccountantEntityType.AccountantIndividualControl:
                    {
                        entity.Facsimile = new PhoneNumberEntity();
                        entity.Facsimile.CountryCode = dr[accountantTable.FACSIMILECOUNTRYCODE].ToString();
                        entity.Facsimile.CityCode = dr[accountantTable.FACSIMILECITYCODE].ToString();
                        entity.Facsimile.PhoneNumber = dr[accountantTable.FACSIMILE].ToString();

                        entity.PhoneNumber = new PhoneNumberEntity();
                        entity.PhoneNumber.CountryCode = dr[accountantTable.PHONENUMBERCOUNTRYCODE].ToString();
                        entity.PhoneNumber.CityCode = dr[accountantTable.PHONENUMBERCITYCODE].ToString();
                        entity.PhoneNumber.PhoneNumber = dr[accountantTable.PHONENUBER].ToString();

                        entity.Websiteaddress = dr[accountantTable.WEBSITEADDRESS].ToString();
                        entity.ABN = dr[accountantTable.ABN].ToString();
                        entity.ACN = dr[accountantTable.ACN].ToString();
                    }
                    break;
                case AccountantEntityType.AccountantPartnershipControl:
                    {
                        entity.Facsimile = new PhoneNumberEntity();
                        entity.Facsimile.CountryCode = dr[accountantTable.FACSIMILECOUNTRYCODE].ToString();
                        entity.Facsimile.CityCode = dr[accountantTable.FACSIMILECITYCODE].ToString();
                        entity.Facsimile.PhoneNumber = dr[accountantTable.FACSIMILE].ToString();
                        entity.PhoneNumber = new PhoneNumberEntity();
                        entity.PhoneNumber.CountryCode = dr[accountantTable.PHONENUMBERCOUNTRYCODE].ToString();
                        entity.PhoneNumber.CityCode = dr[accountantTable.PHONENUMBERCITYCODE].ToString();
                        entity.PhoneNumber.PhoneNumber = dr[accountantTable.PHONENUBER].ToString();
                        entity.Websiteaddress = dr[accountantTable.WEBSITEADDRESS].ToString();
                        entity.ABN = dr[accountantTable.ABN].ToString();
                        entity.TFN = dr[accountantTable.TFN].ToString();
                    }
                    break;
                case AccountantEntityType.AccountantTrustControl:
                    {
                        entity.Facsimile = new PhoneNumberEntity();
                        entity.Facsimile.CountryCode = dr[accountantTable.FACSIMILECOUNTRYCODE].ToString();
                        entity.Facsimile.CityCode = dr[accountantTable.FACSIMILECITYCODE].ToString();
                        entity.Facsimile.PhoneNumber = dr[accountantTable.FACSIMILE].ToString();

                        entity.PhoneNumber = new PhoneNumberEntity();
                        entity.PhoneNumber.CountryCode = dr[accountantTable.PHONENUMBERCOUNTRYCODE].ToString();
                        entity.PhoneNumber.CityCode = dr[accountantTable.PHONENUMBERCITYCODE].ToString();
                        entity.PhoneNumber.PhoneNumber = dr[accountantTable.PHONENUBER].ToString();

                        entity.Websiteaddress = dr[accountantTable.WEBSITEADDRESS].ToString();
                        entity.ABN = dr[accountantTable.ABN].ToString();
                        entity.ACN = dr[accountantTable.ACN].ToString();
                    }
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public static void AssociateBankAccount(BankAccountDS ds, List<Oritax.TaxSimp.Common.IdentityCMDetail> bankAccounts)
        {
            if (ds.Tables[ds.BankAccountsTable.TABLENAME].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[ds.BankAccountsTable.TABLENAME].Rows)
                {
                    var clid = new Guid(dr[ds.BankAccountsTable.CLID].ToString());
                    var csid = new Guid(dr[ds.BankAccountsTable.CSID].ToString());
                    var identityCmDetail = bankAccounts.FirstOrDefault(ss => ss.Clid == new Guid(dr[ds.BankAccountsTable.CLID].ToString()) && ss.Csid == new Guid(dr[ds.BankAccountsTable.CSID].ToString()));
                    if (identityCmDetail == null)
                    {
                        if (bankAccounts == null)
                        {
                            bankAccounts = new List<Oritax.TaxSimp.Common.IdentityCMDetail>();
                        }
                        bankAccounts.Add(new Oritax.TaxSimp.Common.IdentityCMDetail { Clid = clid, Csid = csid });
                    }
                }
            }
        }

        public static void RemoveBankAccount(BankAccountDS ds, List<Oritax.TaxSimp.Common.IdentityCMDetail> bankAccounts, ICMBroker broker)
        {
            if (ds.Tables[ds.BankAccountsTable.TABLENAME].Rows.Count > 0)
            {
                var clid = (Guid)ds.Tables[ds.BankAccountsTable.TABLENAME].Rows[0][ds.BankAccountsTable.CLID];
                var csid = (Guid)ds.Tables[ds.BankAccountsTable.TABLENAME].Rows[0][ds.BankAccountsTable.CSID];

                var identityCmDetail = bankAccounts.FirstOrDefault(ss => ss.Clid == clid && ss.Csid == csid);
                if (identityCmDetail != null)
                {

                    bankAccounts.Remove(identityCmDetail);
                    ds.ExtendedProperties.Add("Message", "Account has been deleted successfully");
                }
            }
        }

        public static void AssociateIndividuals(IndividualDS ds, List<Oritax.TaxSimp.Common.IdentityCMDetail> individuals)
        {
            if (individuals != null && ds.Tables[ds.IndividualTable.TABLENAME].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[ds.IndividualTable.TABLENAME].Rows)
                {
                    var clid = new Guid(dr[ds.IndividualTable.CLID].ToString());
                    var cid = new Guid(dr[ds.IndividualTable.CID].ToString());
                    var csid = new Guid(dr[ds.IndividualTable.CSID].ToString());
                    var businessttitle = dr[ds.IndividualTable.BUSINESSTITLE].ToString();
                    var identityCmDetail = individuals.FirstOrDefault(ss => ss.Clid == new Guid(dr[ds.IndividualTable.CLID].ToString()) && ss.Csid == new Guid(dr[ds.IndividualTable.CSID].ToString()));
                    if (identityCmDetail == null)
                    {
                        individuals.Add(new Oritax.TaxSimp.Common.IdentityCMDetail { Clid = clid, Csid = csid, Cid = cid, BusinessTitle = new TitleEntity { Title = businessttitle } });
                    }
                    else
                    {
                        identityCmDetail.BusinessTitle = new TitleEntity { Title = businessttitle };
                    }
                }
            }
        }

        public static void RemoveIndividual(IndividualDS ds, List<Oritax.TaxSimp.Common.IdentityCMDetail> individuals, ICMBroker broker)
        {
            if (individuals != null && ds.Tables[ds.IndividualTable.TABLENAME].Rows.Count > 0)
            {
                var clid = (Guid)ds.Tables[ds.IndividualTable.TABLENAME].Rows[0][ds.IndividualTable.CLID];
                var csid = (Guid)ds.Tables[ds.IndividualTable.TABLENAME].Rows[0][ds.IndividualTable.CSID];

                var identityCmDetail = individuals.FirstOrDefault(ss => ss.Clid == clid && ss.Csid == csid);
                if (identityCmDetail != null)
                {

                    individuals.Remove(identityCmDetail);
                    ds.ExtendedProperties.Add("Message", "Individual has been deleted successfully");
                }
            }
        }

        public static void RemoveUnits(InstanceCMDS ds, List<Oritax.TaxSimp.Common.IdentityCMDetail> units, ICMBroker broker)
        {
            if (units != null && ds.Tables[ds.InstanceCMDetailsTable.TABLENAME].Rows.Count > 0)
            {
                var clid = (Guid)ds.Tables[ds.InstanceCMDetailsTable.TABLENAME].Rows[0][ds.InstanceCMDetailsTable.CLID];
                var csid = (Guid)ds.Tables[ds.InstanceCMDetailsTable.TABLENAME].Rows[0][ds.InstanceCMDetailsTable.CSID];

                var identityCmDetail = units.FirstOrDefault(ss => ss.Clid == clid && ss.Csid == csid);
                if (identityCmDetail != null)
                {

                    units.Remove(identityCmDetail);
                    ds.ExtendedProperties.Add("Message", "Record has been deleted successfully");
                }
            }
        }

        public static void RemoveUnits(InstanceCMDS ds, List<Oritax.TaxSimp.Data.IdentityCM> units, ICMBroker broker)
        {
            if (units != null && ds.Tables[ds.InstanceCMDetailsTable.TABLENAME].Rows.Count > 0)
            {
                var clid = (Guid)ds.Tables[ds.InstanceCMDetailsTable.TABLENAME].Rows[0][ds.InstanceCMDetailsTable.CLID];
                var csid = (Guid)ds.Tables[ds.InstanceCMDetailsTable.TABLENAME].Rows[0][ds.InstanceCMDetailsTable.CSID];

                var identityCmDetail = units.FirstOrDefault(ss => ss.Clid == clid && ss.Csid == csid);
                if (identityCmDetail != null)
                {

                    units.Remove(identityCmDetail);
                    ds.ExtendedProperties.Add("Message", "Record has been deleted successfully");
                }
            }
        }


        public static void AssociateUnits(InstanceCMDS ds, List<Oritax.TaxSimp.Data.IdentityCM> units, OrganizationType Type)
        {
            if (units != null && ds.Tables[ds.InstanceCMDetailsTable.TABLENAME].Rows.Count > 0)
            {
                var drs = ds.Tables[ds.InstanceCMDetailsTable.TABLENAME].Select(string.Format("{0}='{1}'", ds.InstanceCMDetailsTable.TYPE, Type));
                foreach (DataRow dr in drs)
                {
                    var clid = new Guid(dr[ds.InstanceCMDetailsTable.CLID].ToString());
                    var cid = new Guid(dr[ds.InstanceCMDetailsTable.CID].ToString());
                    var csid = new Guid(dr[ds.InstanceCMDetailsTable.CSID].ToString());

                    var identityCmDetail = units.FirstOrDefault(ss => ss.Clid == new Guid(dr[ds.InstanceCMDetailsTable.CLID].ToString()) && ss.Csid == new Guid(dr[ds.InstanceCMDetailsTable.CSID].ToString()));
                    if (identityCmDetail == null)
                    {
                        if (units == null)
                        {
                            units = new List<Oritax.TaxSimp.Data.IdentityCM>();
                        }
                        units.Add(new Oritax.TaxSimp.Data.IdentityCM { Clid = clid, Csid = csid, Cid = cid });
                    }
                }
            }
        }

        public static void AssociateUnits(InstanceCMDS ds, List<Oritax.TaxSimp.Common.IdentityCMDetail> units)
        {
            if (units != null && ds.Tables[ds.InstanceCMDetailsTable.TABLENAME].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[ds.InstanceCMDetailsTable.TABLENAME].Rows)
                {
                    var clid = new Guid(dr[ds.InstanceCMDetailsTable.CLID].ToString());
                    var cid = new Guid(dr[ds.InstanceCMDetailsTable.CID].ToString());
                    var csid = new Guid(dr[ds.InstanceCMDetailsTable.CSID].ToString());

                    var identityCmDetail = units.FirstOrDefault(ss => ss.Clid == new Guid(dr[ds.InstanceCMDetailsTable.CLID].ToString()) && ss.Csid == new Guid(dr[ds.InstanceCMDetailsTable.CSID].ToString()));
                    if (identityCmDetail == null)
                    {
                        if (units == null)
                        {
                            units = new List<Oritax.TaxSimp.Common.IdentityCMDetail>();
                        }
                        units.Add(new Oritax.TaxSimp.Common.IdentityCMDetail { Clid = clid, Csid = csid, Cid = cid });

                    }
                }
            }
        }


        private IdentityCM GetChild(string data)
        {
            return data.ToIdentity();
        }

        private IdentityCM GetParent(string data)
        {
            return data.ToIdentity();
        }

        private void BMCUpdateComponent(IdentityCM child, CmCommand cmd)
        {
            IBrokerManagedComponent component = base.Broker.GetCMImplementation(child.Clid, child.Csid) as IBrokerManagedComponent;
            switch (cmd)
            {
                case CmCommand.Add_Single:
                    Broker.SetWriteStart();
                    component.UpdateDataStream((int)CmCommand.AttachToParent, new AccountanttoAdvisor() { Clid = this.Clid, Csid = this.Csid, Address = _AccountantEntity.Address, WorkPhoneNumber = _AccountantEntity.PhoneNumber, FaxNumber = _AccountantEntity.Facsimile }.ToXmlString());
                    break;
                case CmCommand.Remove_Single:
                    Broker.SetWriteStart();
                    component.UpdateDataStream((int)CmCommand.DeAttachFromParent, new AccountanttoAdvisor() { Clid = this.Clid, Csid = this.Csid, Address = new Oritax.TaxSimp.Common.DualAddressEntity(), WorkPhoneNumber = new PhoneNumberEntity(), FaxNumber = new PhoneNumberEntity() }.ToXmlString());
                    break;
            }
        }

        private List<OTC.OrganizationUnit> GetOrganizationUnitNameList(List<IdentityCM> value)
        {
            IBrokerManagedComponent component = null;
            List<OTC.OrganizationUnit> entities = new List<OTC.OrganizationUnit>();
            foreach (var each in value)
            {
                component = base.Broker.GetCMImplementation(each.Clid, each.Csid) as IBrokerManagedComponent;
                entities.Add(new OTC.OrganizationUnit()
                {
                    Clid = each.Clid,
                    Csid = each.Csid,
                    Name = component.GetDataStream(999, string.Empty) //component.Name 
                });
            }
            return entities;
        }

        private void UpdateBMC(IdentityCM cm)
        {
            IBrokerManagedComponent component = base.Broker.GetCMImplementation(cm.Clid, cm.Csid) as IBrokerManagedComponent;
            component.UpdateDataStream(-1, this.ToIdentityXml());
        }

        public void Export(XElement root)
        {
            _AccountantEntity.OrganizationStatus = this.OrganizationStatus;
            _AccountantEntity.ClientId = this.ClientId;
            root.Add(_AccountantEntity.ToXElement("Accountant"));
            root.Add(Parent.ToXElement("Parent"));
            root.Add(_Children.ToXElement("Children"));
        }

        public void Import(XElement root, IdentityCmMap map)
        {
            _AccountantEntity = root.FromXElement<AccountantEntity>("Accountant");
            map.ReplaceOrRemove(_AccountantEntity.BankAccounts);
            map.ReplaceOrRemove(_AccountantEntity.Signatories);
            map.ReplaceOrRemove(_AccountantEntity.Directors);
            map.ReplaceOrRemove(_AccountantEntity.ShareHolders);
            map.ReplaceOrRemove(_AccountantEntity.Partners);
            map.ReplaceOrRemove(_AccountantEntity.Applicants);
            map.ReplaceOrRemove(_AccountantEntity.CorporatePrivate);
            map.ReplaceOrRemove(_AccountantEntity.CorporatePublic);

            Name = _AccountantEntity.Name;
            OrganizationStatus = _AccountantEntity.OrganizationStatus;
            ClientId = _AccountantEntity.ClientId;

            Parent = root.FromXElement<IdentityCM>("Parent");
            map.ReplaceOrNull(Parent);

            _Children = root.FromXElement<List<IdentityCM>>("Children");
            map.ReplaceOrRemove(_Children);
        }

        #region Security

        private string GetEntitySecurity()
        {
            EntitySecurity entity = new EntitySecurity();

            PartyDS ds = new PartyDS();
            base.OnGetData(ds);
            foreach (DataRow row in ds.Tables[PartyDS.INCLUDEDPARTY_TABLE].Rows)
            {
                entity.IncludedUsers.Add(new UserEntity() { CID = new Guid(row[PartyDS.PARTYCID_FIELD].ToString()), CurrentUserName = row[PartyDS.PARTYNAME_FIELD].ToString() });
            }
            foreach (DataRow row in ds.Tables[PartyDS.AVAILABLEPARTY_TABLE].Rows)
            {
                if (row[PartyDS.PARTYNAME_FIELD].ToString().ToLower() != "Administrator, Administrator".ToLower())
                {
                    entity.AvailableUsers.Add(new UserEntity() { CID = new Guid(row[PartyDS.PARTYCID_FIELD].ToString()), CurrentUserName = row[PartyDS.PARTYNAME_FIELD].ToString() });
                }
            }

            return entity.ToXmlString();
        }

        private string DeleteEntitySecurity(string UserEntity)
        {
            UserEntity entity = UserEntity.ToData<UserEntity>();

            this.Broker.SetWriteStart();

            PartyDS ds = new PartyDS();
            base.OnGetData(ds);
            ds.AcceptChanges();

            DataRow[] drCollection = ds.Tables[PartyDS.INCLUDEDPARTY_TABLE].Select(PartyDS.PARTYCID_FIELD + " = '" + entity.CID + "'", String.Empty);
            drCollection[0].Delete();
            this.SetData(ds);

            return string.Empty;
        }

        #endregion

        #region OrganizationsChart

        public OrganizationChart GetOrganizationChart()
        {
            OrganizationChart chart = new OrganizationChart();

            chart.Clid = this.CLID;
            chart.Csid = this.CSID;
            chart.NodeName = this._AccountantEntity.Name;

            return chart;
        }

        public OrganizationChart GetAssociation(IIdentityCM identitycm, OrganizationType organizationtype)
        {
            OrganizationChart OChart = new OrganizationChart();
            switch (organizationtype)
            {
                case OrganizationType.BankAccount:
                    if (this._AccountantEntity.BankAccounts.Where(e => e.Clid == identitycm.Clid && e.Csid == identitycm.Csid).ToList().Count > 0)
                    {
                        OChart.Clid = this.Clid;
                        OChart.Csid = this.Csid;
                        OChart.NodeName = this.Name;
                    }
                    break;
                case OrganizationType.Individual:
                    OChart = GetIndividualChart(identitycm);
                    break;
                case OrganizationType.Accountant:
                    if (identitycm.Clid == this.Clid && identitycm.Csid == this.Csid)
                    {
                        OChart = new OrganizationChart() { Clid = this.Clid, Csid = this.Csid, NodeName = this.Name, TypeName = OrganizationType.Accountant.ToString() };
                        //OChart.Subordinates.Add(this._Children.ToOrganizationChart(this.Broker, "Adviser"));
                        OChart.Subordinates.Add(GetEntityChild());
                        OChart.Subordinates.Add(this._AccountantEntity.BankAccounts.ToOrganizationChart(this.Broker, "Bank Account(s)"));
                        OChart.Subordinates.Add(this._AccountantEntity.Signatories.ToOrganizationChart(this.Broker, "Signatory(ies)"));
                        OChart.Subordinates.Add(this._AccountantEntity.Directors.ToOrganizationChart(this.Broker, "Director(s)"));
                        OChart.Subordinates.Add(this._AccountantEntity.ShareHolders.ToOrganizationChart(this.Broker, "ShareHolder(s)"));
                        OChart.Subordinates.Add(this._AccountantEntity.Partners.ToOrganizationChart(this.Broker, "Partner(s)"));
                        OChart.Subordinates.Add(this._AccountantEntity.Applicants.ToOrganizationChart(this.Broker, "Applicant(s)"));

                        OChart.Subordinates.Add(this._AccountantEntity.CorporatePrivate.ToOrganizationChart(this.Broker, "Corporate Private"));
                        OChart.Subordinates.Add(this._AccountantEntity.CorporatePublic.ToOrganizationChart(this.Broker, "Corporate Public"));
                        OChart.Subordinates.RemoveAll(e => e == null);
                        OChart = GetEntityParent(OChart);
                    }
                    break;
                default:
                    break;
            }
            return OChart;
        }

        public OrganizationChart GetEntityParent(OrganizationChart organizationchart)
        {
            ICalculationModule module = null;
            OrganizationChart association = new OrganizationChart() { Clid = this.Clid, Csid = this.Csid, NodeName = this.Name, TypeName = OrganizationType.Accountant.ToString() };
            if (organizationchart.Clid == this.Clid && organizationchart.Csid == this.Csid)
            {
                association = organizationchart;
            }
            else
            {
                association.Subordinates.Add(organizationchart);
            }
            module = Broker.GetCMImplementation(this.Parent.Clid, this.Parent.Csid);
            if (module != null && module is IHaveAssociation)
            {
                association = (module as IHaveAssociation).GetEntityParent(association);
            }
            return association;
        }

        public OrganizationChart GetEntityChild()
        {
            ICalculationModule module = null;
            //OrganizationChart association = new OrganizationChart() { Clid = this.Clid, Csid = this.Csid, NodeName = this.Name, TypeName = OrganizationType.Accountant.ToString() };
            OrganizationChart children = new OrganizationChart();
            children.NodeName = "Advisers";
            foreach (var each in this._Children)
            {
                module = Broker.GetCMImplementation(each.Clid, each.Csid);
                if (module != null && module is IHaveAssociation)
                {
                    children.Subordinates.Add((module as IHaveAssociation).GetEntityChildWithEntity());
                }
            }
            //association.Subordinates.Add(children);
            return children.Subordinates.Count <= 0 ? null : children;
        }

        public OrganizationChart GetEntityChildWithEntity()
        {
            ICalculationModule module = null;
            OrganizationChart association = new OrganizationChart() { Clid = this.Clid, Csid = this.Csid, NodeName = this.Name, TypeName = OrganizationType.Accountant.ToString() };
            OrganizationChart children = new OrganizationChart();
            children.NodeName = "Advisers";
            foreach (var each in this._Children)
            {
                module = Broker.GetCMImplementation(each.Clid, each.Csid);
                if (module != null && module is IHaveAssociation)
                {
                    children.Subordinates.Add((module as IHaveAssociation).GetEntityChildWithEntity());
                }
            }
            association.Subordinates.Add(children.Subordinates.Count <= 0 ? null : children);
            association.Subordinates.Add(this._AccountantEntity.BankAccounts.ToOrganizationChart(this.Broker, "Bank Account(s)"));
            association.Subordinates.Add(this._AccountantEntity.Signatories.ToOrganizationChart(this.Broker, "Signatory(ies)"));
            association.Subordinates.Add(this._AccountantEntity.Directors.ToOrganizationChart(this.Broker, "Director(s)"));
            association.Subordinates.Add(this._AccountantEntity.ShareHolders.ToOrganizationChart(this.Broker, "ShareHolder(s)"));
            association.Subordinates.Add(this._AccountantEntity.Partners.ToOrganizationChart(this.Broker, "Partner(s)"));
            association.Subordinates.Add(this._AccountantEntity.Applicants.ToOrganizationChart(this.Broker, "Applicant(s)"));

            association.Subordinates.Add(this._AccountantEntity.CorporatePrivate.ToOrganizationChart(this.Broker, "Corporate Private"));
            association.Subordinates.Add(this._AccountantEntity.CorporatePublic.ToOrganizationChart(this.Broker, "Corporate Public"));
            association.Subordinates.RemoveAll(e => e == null);
            return association;
        }

        private OrganizationChart GetIndividualChart(IIdentityCM identitycm)
        {
            OrganizationChart OChart = new OrganizationChart();
            OChart.Clid = this.Clid;
            OChart.Csid = this.Csid;
            OChart.NodeName = this.Name;
            if (this._AccountantEntity.Directors.Where(e => e.Clid == identitycm.Clid && e.Csid == identitycm.Csid).ToList().Count > 0)
            {
                OChart.Subordinates.Add(new OrganizationChart() { NodeName = "Director(s)" });
            }
            if (this._AccountantEntity.Partners.Where(e => e.Clid == identitycm.Clid && e.Csid == identitycm.Csid).ToList().Count > 0)
            {
                OChart.Subordinates.Add(new OrganizationChart() { NodeName = "Partner(s)" });
            }
            if (this._AccountantEntity.Signatories.Where(e => e.Clid == identitycm.Clid && e.Csid == identitycm.Csid).ToList().Count > 0)
            {
                OChart.Subordinates.Add(new OrganizationChart() { NodeName = "Signatory(ies)" });
            }
            if (this._AccountantEntity.ShareHolders.Where(e => e.Clid == identitycm.Clid && e.Csid == identitycm.Csid).ToList().Count > 0)
            {
                OChart.Subordinates.Add(new OrganizationChart() { NodeName = "ShareHolder(s)" });
            }
            if (this._AccountantEntity.Applicants.Where(e => e.Clid == identitycm.Clid && e.Csid == identitycm.Csid).ToList().Count > 0)
            {
                OChart.Subordinates.Add(new OrganizationChart() { NodeName = "Applicant(s)" });
            }
            OChart.Subordinates.RemoveAll(e => e == null);
            return OChart.Subordinates.Count <= 0 ? null : OChart;
        }

        #endregion

        #region GetXMLData

        private string ExtractXML(string data)
        {
            string xmlco = _AccountantEntity.ToXmlString();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmlco);
            XMLExtensions.SetNode(_AccountantEntity.Applicants, "Applicants", "Applicant", doc, this.Broker);
            XMLExtensions.SetNode(_AccountantEntity.Signatories, "Signatories", "Signatory", doc, this.Broker);
            XMLExtensions.SetNode(_AccountantEntity.Directors, "Directors", "Director", doc, this.Broker);
            XMLExtensions.SetNode(_AccountantEntity.ShareHolders, "ShareHolders", "ShareHolder", doc, this.Broker);
            XMLExtensions.SetNode(_AccountantEntity.Partners, "Partners", "Partner", doc, this.Broker);
            XMLExtensions.SetNode(_AccountantEntity.BankAccounts, "BankAccounts", "BankAccount", doc, this.Broker);
            XmlAttribute rootAtt = doc.CreateAttribute("Type");
            rootAtt.Value = this.TypeName;
            doc.DocumentElement.Attributes.Append(rootAtt);
            string xml = doc.OuterXml;
            XDocument document = XDocument.Load(XmlReader.Create(new StringReader(xml)));
            document.Root.Name = data;
            document.Root.RemoveAttributes();
            xml = document.ToString().Trim();
            return xml;
        }

        #endregion
    }

    [Serializable]
    public class AccountantEntity : IClientEntity
    {
        public String Name { get; set; }
        public String ACN { get; set; }
        public String ABN { get; set; }
        public String TFN { get; set; }

        public Oritax.TaxSimp.Common.DualAddressEntity Address { get; set; }

        public PhoneNumberEntity PhoneNumber { get; set; }
        public PhoneNumberEntity Facsimile { get; set; }
        public String Websiteaddress { get; set; }
        public String PreviousPracticeName { get; set; }
        public String LegalName { get; set; }
        public DateTime? lastAuditedDate { get; set; }
        public String FUM { get; set; }
        public String Turnover { get; set; }

        public List<Oritax.TaxSimp.Common.IdentityCMDetail> BankAccounts { get; set; }
        public List<Oritax.TaxSimp.Common.IdentityCMDetail> Signatories { get; set; }
        public List<Oritax.TaxSimp.Common.IdentityCMDetail> Directors { get; set; }
        public List<Oritax.TaxSimp.Common.IdentityCMDetail> ShareHolders { get; set; }
        public List<Oritax.TaxSimp.Common.IdentityCMDetail> Partners { get; set; }
        public List<Oritax.TaxSimp.Common.IdentityCMDetail> Applicants { get; set; }
        public List<Oritax.TaxSimp.Common.IdentityCMDetail> Clients { get; set; }


        public List<IdentityCM> CorporatePrivate { get; set; }
        public List<IdentityCM> CorporatePublic { get; set; }

        public String OrganizationStatus { get; set; }
        public String ClientId { get; set; }
        public AccountantEntityType AccountantType { get; set; }

        public IdentityCM Parent { get; set; }
        public List<IdentityCM> Children { get; set; }
    }

    public enum AccountantEntityType
    {
        AccountantCompanyControl = 1,
        AccountantIndividualControl,
        AccountantPartnershipControl,
        AccountantTrustControl,
    }
}
