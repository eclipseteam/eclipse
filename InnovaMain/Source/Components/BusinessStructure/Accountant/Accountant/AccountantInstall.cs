namespace Oritax.TaxSimp.CM.Group
{
    public class AccountantInstall
	{
		#region INSTALLATION PROPERTIES
		// Assembly Installation Properties
        public const string ASSEMBLY_ID = "FF8E26E5-1A3E-4AAC-8455-4B80707F38E9";
        public const string ASSEMBLY_NAME = "Accountant";
        public const string ASSEMBLY_DISPLAYNAME = "Accountant";
		public const string ASSEMBLY_MAJORVERSION="1";
		public const string ASSEMBLY_MINORVERSION="0";
        public const string ASSEMBLY_DATAFORMAT="0"; 
		public const string ASSEMBLY_REVISION="0"; 

		// Component Installation Properties
        public const string COMPONENT_ID = "77BE3C25-8EDE-427B-A965-88AD43571D2F";
        public const string COMPONENT_NAME = "Accountant";
        public const string COMPONENT_DISPLAYNAME = "Accountant";
		public const string COMPONENT_CATEGORY="BusinessEntity";

		// Component Version Installation Properties
		public const string COMPONENTVERSION_STARTDATE="01/01/1900 00:00:00 AM";
		public const string COMPONENTVERSION_ENDDATE="31/12/2050 12:59:59 AM";
		public const string COMPONENTVERSION_PERSISTERASSEMBLY="CommonPersisters_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";
		public const string COMPONENTVERSION_PERSISTERCLASS="Oritax.TaxSimp.CommonPersistence.GroupPersister";
        public const string COMPONENTVERSION_IMPLEMENTATIONCLASS = "Oritax.TaxSimp.CM.Group.AccountantCM";

		// Data Model
		public const string DATAMODEL_MANAGERASSEMBLY="CommonPersisters_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";
		public const string DATAMODEL_MANAGERCLASS="Oritax.TaxSimp.CommonPersistence.GroupPersister";

		#endregion
	}
}