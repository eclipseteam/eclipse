using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Runtime.Serialization;
using System.Security.Permissions;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Enum;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Utilities;
using System.Linq;

namespace Oritax.TaxSimp.CM.Entity
{
    [Serializable]
    public class SettledUnsettledCM : EntityCM, ISerializable
    {
        #region Private Variable
        private List<OrderSettledUnsettledEntity> _OrderSettledUnsettledEntity;
        private Guid _ParentClientCID;
        private string _ParentClientID;
        #endregion

        #region Public Properties
        public List<OrderSettledUnsettledEntity> OrderSettledUnsettledEntity
        {
            get { return _OrderSettledUnsettledEntity; }
        }


        public Guid ParentClientCID
        {
            get { return _ParentClientCID; }
        }
        public string ParentClientID
        {
            get { return _ParentClientID; }
        }

        #endregion

        #region Constructor
        public SettledUnsettledCM()
        { }

        protected SettledUnsettledCM(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            _OrderSettledUnsettledEntity = info.GetValue<List<OrderSettledUnsettledEntity>>("Oritax.TaxSimp.CM.Entity.SettledUnsettled");
            _ParentClientCID = info.GetGuidX("Oritax.TaxSimp.CM.Entity.ParentClientCID");
            _ParentClientID = info.GetString("Oritax.TaxSimp.CM.Entity.ParentClientID");

        }
        #endregion

        #region Override Method
        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValueX("Oritax.TaxSimp.CM.Entity.SettledUnsettled", _OrderSettledUnsettledEntity);
            info.AddValueX("Oritax.TaxSimp.CM.Entity.ParentClientCID", ParentClientCID);
            info.AddValueX("Oritax.TaxSimp.CM.Entity.ParentClientID", _ParentClientID);
        }

        public override string OnUpdateDataStream(int type, string data)
        {
            string xml = string.Empty;

            return xml;
        }

        public override string GetDataStream(int type, string data)
        {
            string xml = string.Empty;

            return xml;
        }

        public override string GetAllDataStream(int type)
        {
            return string.Empty;
        }
        #endregion

        protected override void OnGetData(DataSet data)
        {
            base.OnGetData(data);
            if (data is SettledUnsetteledDS)
            {
                var ds = data as SettledUnsetteledDS;
                switch (ds.CommandType)
                {
                    case DatasetCommandTypes.Get:
                        if (OrderSettledUnsettledEntity != null)
                        {
                            var orderSettledUnSettled = OrderSettledUnsettledEntity;
                            //Checking OrderNo and Filtering SettledUnsettled by OrderNo
                            if (ds.OrderId != Guid.Empty)
                            {
                                orderSettledUnSettled = orderSettledUnSettled.Where(ss => ss.OrderID == ds.OrderId).ToList();
                            }
                            else if (ds.Unit != null && !string.IsNullOrEmpty(ds.Unit.ClientId))
                            {
                                orderSettledUnSettled = orderSettledUnSettled.Where(ss => ss.ClientID == ds.Unit.ClientId).ToList();
                            }
                            AddSettlledUnsettledRows(ds, orderSettledUnSettled);
                            GetOrderStatus(ds);
                        }
                        break;
                    case DatasetCommandTypes.Check:
                        ds.Unit.Cid = CID;
                        break;
                    case DatasetCommandTypes.Unsettle:
                        if (OrderSettledUnsettledEntity != null)
                        {
                            var orderSettledUnSettled = OrderSettledUnsettledEntity;
                            if (ds.Unit != null && !string.IsNullOrEmpty(ds.Unit.ClientId))
                            {
                                orderSettledUnSettled = orderSettledUnSettled.Where(ss => ss.ClientID == ds.Unit.ClientId && ss.Type < OrderSetlledUnsettledType.OnMarket).ToList();
                            }
                            AddSettlledUnsettledRows(ds, orderSettledUnSettled);
                            GetOrderStatus(ds); 
                        }

                        break;

                }
            }

        }

        private void AddSettlledUnsettledRows(SettledUnsetteledDS ds, List<OrderSettledUnsettledEntity> orderSettledUnSettled)
        {
            foreach (var settledUnSettled in orderSettledUnSettled)
            {
                DataRow dr = ds.SettledUnsetteledTable.NewRow();
                dr[ds.SettledUnsetteledTable.ACCOUNTCID] = settledUnSettled.AccountCID;
                dr[ds.SettledUnsetteledTable.ACCOUNTNUMBER] = settledUnSettled.AccountNo;
                dr[ds.SettledUnsetteledTable.ACCOUNTBSB] = settledUnSettled.AccountBsb;
                if (settledUnSettled.Amount.HasValue)
                    dr[ds.SettledUnsetteledTable.AMOUNT] = settledUnSettled.Amount;
                dr[ds.SettledUnsetteledTable.CLIENTCID] = settledUnSettled.ClientCID;
                dr[ds.SettledUnsetteledTable.CLIENTID] = settledUnSettled.ClientID;
                dr[ds.SettledUnsetteledTable.CREATEDDATE] = settledUnSettled.CreatedDate;
                dr[ds.SettledUnsetteledTable.ID] = settledUnSettled.ID;
                dr[ds.SettledUnsetteledTable.INVESTMENTCODE] = settledUnSettled.InvestmentCode;
                dr[ds.SettledUnsetteledTable.ORDERID] = settledUnSettled.OrderID;
                dr[ds.SettledUnsetteledTable.ORDERITEMID] = settledUnSettled.OrderItemID;
                dr[ds.SettledUnsetteledTable.ORDERNO] = settledUnSettled.OrderNo;
                dr[ds.SettledUnsetteledTable.PRODUCTID] = settledUnSettled.ProductID;
                dr[ds.SettledUnsetteledTable.SETTLEDASSOCIATION] = settledUnSettled.SettledAssociation;
                dr[ds.SettledUnsetteledTable.TRANSACTIONID] = settledUnSettled.TransactionID;
                dr[ds.SettledUnsetteledTable.TYPE] = settledUnSettled.Type.ToString();
                dr[ds.SettledUnsetteledTable.TRADETYPE] = settledUnSettled.TradeType.ToString();
                dr[ds.SettledUnsetteledTable.TRADEDATE] = settledUnSettled.TradeDate;
                dr[ds.SettledUnsetteledTable.ENTRYTYPE] = settledUnSettled.EntryType.ToString();
                dr[ds.SettledUnsetteledTable.TRANSACTIONTYPE] = settledUnSettled.TransactionType.ToString();
                dr[ds.SettledUnsetteledTable.ACCOUNTTYPE] = settledUnSettled.AccountType.ToString();
                dr[ds.SettledUnsetteledTable.ORDERTYPE] = settledUnSettled.OrderType;
                if (settledUnSettled.Units.HasValue)
                {
                    dr[ds.SettledUnsetteledTable.UNITS] = settledUnSettled.Units;
                }
                if (settledUnSettled.UnitPrice.HasValue)
                {
                    dr[ds.SettledUnsetteledTable.UNITPRICE] = settledUnSettled.UnitPrice;
                }
                if (settledUnSettled.UpdatedDate.HasValue)
                {
                    dr[ds.SettledUnsetteledTable.UPDATEDDATE] = settledUnSettled.UpdatedDate;
                }
                if (settledUnSettled.Percentage.HasValue)
                {
                    dr[ds.SettledUnsetteledTable.PERCENTAGE] = settledUnSettled.Percentage.Value;
                }

                dr[ds.SettledUnsetteledTable.BROKERID] = settledUnSettled.BrokerId;
                dr[ds.SettledUnsetteledTable.BANKID] = settledUnSettled.BankId;
                dr[ds.SettledUnsetteledTable.DURATION] = settledUnSettled.Duration;

                if (settledUnSettled.BrokerId != Guid.Empty)
                {
                    dr[ds.SettledUnsetteledTable.BROKERNAME] = OrderPadUtilities.GetInstituteName(Broker, settledUnSettled.BrokerId, true);
                }
                if (settledUnSettled.BankId != Guid.Empty)
                {
                    dr[ds.SettledUnsetteledTable.BANKNAME] = OrderPadUtilities.GetInstituteName(Broker, settledUnSettled.BankId, true);
                }
                dr[ds.SettledUnsetteledTable.CLIENTMANAGEMENTTYPE] = settledUnSettled.ClientManagementType;

                ds.SettledUnsetteledTable.Rows.Add(dr);
            }
        }

        protected override ModifiedState OnSetData(DataSet data)
        {
            base.OnSetData(data);

            if (OrderSettledUnsettledEntity == null)
            {
                _OrderSettledUnsettledEntity = new List<OrderSettledUnsettledEntity>();
            }

            if (ParentClientID == null)
            {
                _ParentClientID = string.Empty;
            }

            if (data is SettledUnsetteledDS)
            {
                var ds = data as SettledUnsetteledDS;

                List<OrderEntity> orders = new List<OrderEntity>();

                if (ds.ExtendedProperties.Contains("Order"))
                    orders.Add(ds.ExtendedProperties["Order"] as OrderEntity);
                else
                    if (ds.ExtendedProperties.Contains("Orders"))
                        orders = ds.ExtendedProperties["Orders"] as List<OrderEntity>;
                if (orders != null && orders.Count > 0)
                {
                    switch (ds.CommandType)
                    {
                        case DatasetCommandTypes.Add:
                            var org = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                            foreach (var orderEntity in orders)
                            {
                                AddUnsetteled(orderEntity, org);
                            }

                            Broker.ReleaseBrokerManagedComponent(org);
                            break;
                        case DatasetCommandTypes.Update:
                            foreach (var orderEntity in orders)
                            {
                                UpdateUnsetteledStatus(orderEntity);
                            }
                            break;
                        case DatasetCommandTypes.Delete:
                            foreach (var orderEntity in orders)
                            {
                                RemoveUnsettled(orderEntity);
                            }
                            break;
                        case DatasetCommandTypes.Complete:
                            foreach (var orderEntity in orders)
                            {
                                AddConfirmationByForce(orderEntity, ds.Unit.CurrentUser);
                            }
                            break;
                        case DatasetCommandTypes.Incomplete:
                            foreach (var orderEntity in orders)
                            {
                                RemoveForcedConfirmation(orderEntity, ds.Unit.CurrentUser);
                            }
                            break;
                    }
                }
                else
                {
                    switch (ds.CommandType)
                    {
                        case DatasetCommandTypes.Delete:
                            if (ds.SettledUnsetteledTable.Rows.Count > 0)
                            {
                                RemoveConfirmation(ds);
                            }
                            break;
                        case DatasetCommandTypes.Settle:
                            if (ds.SettledUnsetteledTable.Rows.Count > 0)
                            {
                                AddConfirmationByForce(ds);
                            }
                            break;
                        case DatasetCommandTypes.Unsettle:
                            if (ds.SettledUnsetteledTable.Rows.Count > 0)
                            {
                                RemoveForcedConfirmation(ds);
                            }
                            break;
                    }
                }
            }
            else if (data is SettledUnsetteledTransactionDS)
            {
                var ds = data as SettledUnsetteledTransactionDS;

                switch (ds.CommandType)
                {
                    case DatasetCommandTypes.Settle:
                        //if Cash Transaction is received
                        if (ds.Tables.Contains("CashTransctions") && ds.Tables["CashTransctions"].Rows.Count > 0)
                        {
                            DateTimeFormatInfo info = new DateTimeFormatInfo();
                            info.ShortDatePattern = "dd/MM/yyyy";
                            foreach (DataRow dr in ds.Tables["CashTransctions"].Rows)
                            {
                                DateTime tradeDate;
                                DateTime.TryParse(dr["DateOfTransaction"].ToString(), info, DateTimeStyles.None, out tradeDate);
                                decimal amount;
                                decimal.TryParse(dr["Amount"].ToString(), NumberStyles.Currency, CultureInfo.CurrentCulture.NumberFormat, out amount);

                                OrderSettledUnsettledEntity unsetteled;

                                if (ds.ExtendedProperties.Count > 0 && ds.ExtendedProperties["IsManual"] != null && ds.ExtendedProperties["IsManual"].ToString().ToLower() == "true")
                                {
                                    unsetteled = OrderSettledUnsettledEntity.FirstOrDefault(ss => ss.ID == Guid.Parse(dr["EntityID"].ToString()));
                                    AddConfirmationTrans(amount, tradeDate, Guid.Parse(dr["TransactionID"].ToString()), ds, unsetteled);
                                    AddExternalConfirmationTrans(unsetteled, ds);
                                }
                                else
                                {
                                    var unsettledOrders = OrderSettledUnsettledEntity.Where(ss =>
                                    {
                                        string bsb = string.Empty;

                                        if (!string.IsNullOrEmpty(ss.AccountBsb) && ss.AccountBsb.Contains('-'))
                                            bsb = ss.AccountBsb.Split('-')[1];

                                        string BsBNAccountNumber = bsb + ss.AccountNo;

                                        string drBSBNAccountNumber = dr["BSBNAccountNumber"].ToString();
                                        if (!string.IsNullOrEmpty(drBSBNAccountNumber) && drBSBNAccountNumber.Contains('-'))
                                        {
                                            drBSBNAccountNumber = drBSBNAccountNumber.Split('-')[1].Trim();
                                        }

                                        int daystoCheck = GetDaystoCheckForBank(ss);
                                        bool isDateMatched = ss.TradeDate.Date.IsMatched(tradeDate.Date, daystoCheck, true);
                                        bool accountMatch = (BsBNAccountNumber == drBSBNAccountNumber);
                                        bool isActual = ss.TransactionType == SetlledunSettledTransactionType.Orignal;
                                        bool isnotSettled = ss.Type == OrderSetlledUnsettledType.Unsettled;
                                        bool isBankTrans = (ss.AccountType == OrderAccountType.BankAccount);
                                        return accountMatch && isDateMatched && isActual && isnotSettled && isBankTrans;
                                    }).GroupBy(ss => ss.OrderType);
                                    bool hasFound = false;
                                    foreach (var unsettledOrder in unsettledOrders)
                                    {
                                        switch (unsettledOrder.Key)
                                        {
                                            case OrderAccountType.ASX:
                                                // put asx logic here
                                                decimal combineAmount = unsettledOrder.Sum(aa => (aa.Amount.HasValue) ? aa.Amount.Value : 0);
                                                decimal brokageAmount = unsettledOrder.Sum(aa => (aa.BrokrageFee.HasValue) ? aa.BrokrageFee.Value : 0);

                                                bool amountMatch = amount.IsMatchedInRange(combineAmount - brokageAmount, (decimal)0.9, (decimal)1.1);//amount.IsAbsMatched(combineAmount - brokageAmount, 1);
                                                if (amountMatch)
                                                {
                                                    foreach (var orderSettledUnsettledEntity in unsettledOrder)
                                                    {
                                                        AddConfirmationTrans(amount, tradeDate, Guid.Parse(dr["TransactionID"].ToString()), ds, orderSettledUnsettledEntity);
                                                        AddExternalConfirmationTrans(orderSettledUnsettledEntity, ds);
                                                    }

                                                    hasFound = true;
                                                }
                                                else
                                                {
                                                    var buytrans = unsettledOrder.Where(ss => ss.TradeType == TradeType.Buy);
                                                    decimal buyAmount = buytrans.Sum(aa => (aa.Amount.HasValue) ? aa.Amount.Value : 0);
                                                    decimal BuybrokageAmount = buytrans.Sum(aa => (aa.BrokrageFee.HasValue) ? aa.BrokrageFee.Value : 0);

                                                    bool isBuyMatch = amount.IsMatchedInRange(buyAmount - BuybrokageAmount, (decimal)0.9, (decimal)1.1); //amount.IsAbsMatched(buyAmount - BuybrokageAmount, 1);

                                                    if (isBuyMatch)
                                                    {
                                                        foreach (var orderSettledUnsettledEntity in buytrans)
                                                        {
                                                            AddConfirmationTrans(amount, tradeDate, Guid.Parse(dr["TransactionID"].ToString()), ds, orderSettledUnsettledEntity);
                                                            AddExternalConfirmationTrans(orderSettledUnsettledEntity, ds);
                                                        }
                                                        hasFound = true;
                                                    }
                                                    else
                                                    {
                                                        var sellTrans = unsettledOrder.Where(ss => ss.TradeType == TradeType.Sell);
                                                        decimal sellAmount = sellTrans.Sum(aa => (aa.Amount.HasValue) ? aa.Amount.Value : 0);
                                                        decimal sellbrokageAmount = sellTrans.Sum(aa => (aa.BrokrageFee.HasValue) ? aa.BrokrageFee.Value : 0);

                                                        bool isSellMatch = amount.IsMatchedInRange(sellAmount - sellbrokageAmount, (decimal)0.9, (decimal)1.1);// amount.IsAbsMatched(sellAmount - sellbrokageAmount, 1);

                                                        if (isSellMatch)
                                                        {
                                                            foreach (var orderSettledUnsettledEntity in sellTrans)
                                                            {
                                                                AddConfirmationTrans(amount, tradeDate, Guid.Parse(dr["TransactionID"].ToString()), ds, orderSettledUnsettledEntity);
                                                                AddExternalConfirmationTrans(orderSettledUnsettledEntity, ds);
                                                            }
                                                            hasFound = true;
                                                        }
                                                        else
                                                        {
                                                            unsetteled = unsettledOrder.FirstOrDefault(ss =>
                                                            {
                                                                return amount.IsMatchedInRange((ss.Amount.HasValue ? ss.Amount.Value : 0) - (ss.BrokrageFee.HasValue ? ss.BrokrageFee.Value : 0), (decimal)0.9, (decimal)1.1);//amount.IsAbsMatched((ss.Amount.HasValue ? ss.Amount.Value : 0), 1);
                                                            });
                                                            if (unsetteled != null)
                                                            {
                                                                AddConfirmationTrans(amount, tradeDate, Guid.Parse(dr["TransactionID"].ToString()), ds, unsetteled);
                                                                AddExternalConfirmationTrans(unsetteled, ds);
                                                                hasFound = true;
                                                            }
                                                        }
                                                    }
                                                }
                                                break;
                                            case OrderAccountType.StateStreet:
                                                // put normal logic here

                                                unsetteled = unsettledOrder.FirstOrDefault(ss =>
                                                    {
                                                        bool amountMatched = ss.TradeType == TradeType.Buy ? amount.IsAbsMatched((ss.Amount.HasValue ? ss.Amount.Value : 0), 1) : amount.IsMatchedInRange((ss.Amount.HasValue ? ss.Amount.Value : 0), (decimal)0.9, (decimal)1.1);
                                                        return amountMatched;

                                                    });

                                                if (unsetteled != null)
                                                {
                                                    AddConfirmationTrans(amount, tradeDate, Guid.Parse(dr["TransactionID"].ToString()), ds, unsetteled);
                                                    AddExternalConfirmationTrans(unsetteled, ds);
                                                    hasFound = true;
                                                }

                                                break;
                                            case OrderAccountType.TermDeposit:
                                            case OrderAccountType.BankAccount:
                                            case OrderAccountType.AtCall:
                                                // put normal logic here
                                                unsetteled = unsettledOrder.FirstOrDefault(ss =>
                                                {
                                                    return amount.IsAbsMatched((ss.Amount.HasValue ? ss.Amount.Value : 0), 1);
                                                });
                                                if (unsetteled != null)
                                                {
                                                    AddConfirmationTrans(amount, tradeDate, Guid.Parse(dr["TransactionID"].ToString()), ds, unsetteled);
                                                    AddExternalConfirmationTrans(unsetteled, ds);
                                                    hasFound = true;
                                                }
                                                break;
                                        }
                                        if (hasFound)
                                            break;
                                    }

                                }

                            }
                        }

                        //if MIS Transaction is received
                        if (ds.Tables.Contains("MisTransactions") && ds.Tables["MisTransactions"].Rows.Count > 0)
                        {
                            DateTimeFormatInfo info = new DateTimeFormatInfo();
                            info.ShortDatePattern = "dd/MM/yyyy";
                            foreach (DataRow dr in ds.Tables["MisTransactions"].Rows)
                            {
                                DateTime tradeDate;
                                DateTime.TryParse(dr["TradeDate"].ToString(), info, DateTimeStyles.None, out tradeDate);
                                decimal amount;
                                decimal.TryParse(dr["Amount"].ToString(), NumberStyles.Currency, CultureInfo.CurrentCulture.NumberFormat, out amount);
                                decimal units = decimal.Parse(dr["Shares"].ToString());
                                TradeType TradeType;

                                switch (dr["TransactionType"].ToString().ToLower())
                                {
                                    case "redemption":
                                        TradeType = TradeType.Sell;
                                        break;
                                    case "application":
                                    case "purchase":
                                        TradeType = TradeType.Buy;
                                        break;
                                    default:
                                        continue;
                                }

                                OrderSettledUnsettledEntity unsetteled;

                                if (ds.ExtendedProperties.Count > 0 && ds.ExtendedProperties["IsManual"] != null && ds.ExtendedProperties["IsManual"].ToString().ToLower() == "true")
                                {
                                    unsetteled = OrderSettledUnsettledEntity.FirstOrDefault(ss => ss.ID == Guid.Parse(dr["EntityID"].ToString()));
                                }
                                else
                                {
                                    /*  
                                     * Section 1.1.1.3.1 MIS units - Buy 
                                     * Settlement check:
                                     * 1.Settlement transaction is within 0-2 days from trade transaction
                                     * 2.ABS(Settlement amount - trade amount) < 1
                                     * Section 1.1.1.3.2 MIS units � Sell
                                     * For MIS on sell we do not issue settlement instructions as State Street pay to the linked bank account.
                                     * Settlement check:
                                     * 1.Settlement transaction is within 2-5 days from trade transaction
                                     * 2.ABS(unsettled transacion amount - Confirmation amount) < 1  or 
                                     * Number of units sold = Number of units requested in the original instruction                                      
                                     * Revision Task-919
                                     * For P2: Increased Settlement transaction to 0-10 days from trade transaction for Both Buy and Sell
                                     */

                                    unsetteled = OrderSettledUnsettledEntity.FirstOrDefault(ss =>
                                        {
                                            bool result;
                                            string fundCode = dr["FundCode"].ToString();
                                            int daystoCheck = GetDaystoCheckForMIS(fundCode, TradeType);
                                            if (TradeType == TradeType.Buy)
                                            {
                                                result =
                                                 ss.TradeType == TradeType
                                                 && ss.TradeDate.Date.IsMatched(tradeDate.Date, daystoCheck, true)
                                                 && amount.IsAbsMatched((ss.Amount.HasValue ? ss.Amount.Value : 0), 1)
                                                 && ss.InvestmentCode == fundCode
                                                 && ss.TransactionType == SetlledunSettledTransactionType.Orignal
                                                 && ss.Type == OrderSetlledUnsettledType.Unsettled
                                                 && ss.AccountType == OrderAccountType.StateStreet
                                                 ;
                                            }
                                            else
                                            {
                                                result =
                                                  ss.TradeType == TradeType
                                                  && ss.TradeDate.Date.IsMatched(tradeDate.Date, daystoCheck, true)

                                                  //&& (amount.IsAbsMatched((ss.Amount.HasValue ? ss.Amount.Value : 0), 1)


                                                  && (amount.IsMatchedInRange((ss.Amount.HasValue ? ss.Amount.Value : 0), (decimal)0.9, (decimal)1.1)
                                                  || units.IsMatched((ss.Units.HasValue ? ss.Units.Value : 0)))
                                                  && ss.InvestmentCode == fundCode
                                                  && ss.TransactionType == SetlledunSettledTransactionType.Orignal
                                                  && ss.Type == OrderSetlledUnsettledType.Unsettled
                                                  && ss.AccountType == OrderAccountType.StateStreet
                                                  ;
                                            }
                                            return result;
                                        });
                                }

                                AddConfirmationTrans(amount, tradeDate, Guid.Parse(dr["TransactionID"].ToString()), ds, unsetteled, units);
                            }
                        }

                        //ASX Settlement
                        if (ds.Tables.Contains("ASXTransactions") && ds.Tables["ASXTransactions"].Rows.Count > 0)
                        {
                            DateTimeFormatInfo info = new DateTimeFormatInfo();
                            info.ShortDatePattern = "dd/MM/yyyy";

                            foreach (DataRow dr in ds.Tables["ASXTransactions"].Rows)
                            {
                                DateTime tradeDate;
                                DateTime.TryParse(dr["TradeDate"].ToString(), info, DateTimeStyles.None, out tradeDate);
                                decimal transAmount;
                                decimal.TryParse(dr["Amount"].ToString(), NumberStyles.Currency, CultureInfo.CurrentCulture.NumberFormat, out transAmount);
                                decimal units = decimal.Parse(dr["Units"].ToString());
                                decimal brokerageAmount;
                                decimal.TryParse(dr["BrokerageAmount"].ToString(), NumberStyles.Currency, CultureInfo.CurrentCulture.NumberFormat, out brokerageAmount);

                                TradeType TradeType;

                                switch (dr["TransactionType"].ToString().ToLower())
                                {
                                    case "sale":
                                        TradeType = TradeType.Sell;
                                        break;
                                    case "buy / purchase":
                                        TradeType = TradeType.Buy;
                                        break;
                                    default:
                                        continue;

                                }
                                OrderSettledUnsettledEntity unsetteled;

                                if (ds.ExtendedProperties.Count > 0 && ds.ExtendedProperties["IsManual"] != null && ds.ExtendedProperties["IsManual"].ToString().ToLower() == "true")
                                {
                                    unsetteled = OrderSettledUnsettledEntity.FirstOrDefault(ss => ss.ID == Guid.Parse(dr["EntityID"].ToString()));
                                }
                                else
                                {
                                    unsetteled = OrderSettledUnsettledEntity.FirstOrDefault(ss =>
                                    {

                                        /* Settlement check: - Buy ASX: 
                                         * 1.Settlement transaction is within 1-5 days from trade transaction
                                         * 2.Settlement amount - trade amount - brokerage fees < 1
                                         * 
                                         * Settlement check: � Sell ASX (This formula allows for 10% fluctuation of unit price)
                                         * 1.Settlement transaction is within 1-5 days from trade transaction
                                         * 2.(Settlement amount� brokerage fees > = expected trade amount * 0.9) and (Settlement amount� brokerage fees < = expected trade amount * 1.1)
                                         * 3.Number of units sold = Number of units requested in the original transaction (for sell transactions only)
                                         */
                                        if (TradeType == TradeType.Buy)
                                        {
                                            bool isTradetypeMatch = ss.TradeType == TradeType;
                                            bool isTradeDateMatch = ss.TradeDate.Date.IsMatched(tradeDate.Date, 3, true);

                                            bool isAmountmatch = (transAmount - brokerageAmount).IsAbsMatched(ss.Amount.HasValue ? ss.Amount.Value : 0, 1);
                                            bool investmentCodeMatch = ss.InvestmentCode == dr["InvestmentCode"].ToString();
                                            bool isTransActual = ss.TransactionType == SetlledunSettledTransactionType.Orignal;
                                            bool isNotsettled = ss.Type == OrderSetlledUnsettledType.Unsettled;
                                            bool isAsxTrans = ss.AccountType == OrderAccountType.ASX;
                                            return isTradetypeMatch && isTradeDateMatch && isAmountmatch && investmentCodeMatch && isTransActual && isNotsettled && isAsxTrans
                                          ;
                                        }
                                        else
                                        {

                                            bool isTradetypeMatch = ss.TradeType == TradeType;
                                            bool isTradeDateMatch = ss.TradeDate.Date.IsMatched(tradeDate.Date, 3, true);
                                            decimal orderamount = Math.Abs((ss.Amount.HasValue ? ss.Amount.Value : 0));
                                            bool isAmountGreaterThenMin = (transAmount - brokerageAmount) >= (orderamount * (decimal)0.9);
                                            bool isAmountlessThenMax = (transAmount - brokerageAmount) <= (orderamount * (decimal)1.1);

                                            bool unitsMatch = units.IsMatched(ss.Units.HasValue ? ss.Units.Value : 0);
                                            bool investmentCodeMatch = ss.InvestmentCode == dr["InvestmentCode"].ToString();
                                            bool isTransActual = ss.TransactionType == SetlledunSettledTransactionType.Orignal;
                                            bool isNotsettled = ss.Type == OrderSetlledUnsettledType.Unsettled;
                                            bool isAsxTrans = ss.AccountType == OrderAccountType.ASX;

                                            return isTradetypeMatch && isTradeDateMatch && (isAmountGreaterThenMin && isAmountlessThenMax) && unitsMatch && investmentCodeMatch && isTransActual && isNotsettled && isAsxTrans;
                                        }
                                    });
                                }

                                AddConfirmationTrans(transAmount, tradeDate, Guid.Parse(dr["TransactionID"].ToString()), ds, unsetteled, units);
                            }
                        }

                        //td settlement
                        if (ds.Tables.Contains("BankTransactionDetailsTables") && ds.Tables["BankTransactionDetailsTables"].Rows.Count > 0)
                        {
                            DateTimeFormatInfo info = new DateTimeFormatInfo();
                            info.ShortDatePattern = "dd/MM/yyyy";

                            foreach (DataRow dr in ds.Tables["BankTransactionDetailsTables"].Rows)
                            {
                                string transactionType = dr["TranType"].ToString();
                                OrderAccountType accountType = OrderAccountType.AtCall;
                                if (transactionType == "TD")
                                {
                                    accountType = OrderAccountType.TermDeposit;
                                }
                                TradeType TradeType;
                                string systransactionType = dr["SysTranType"].ToString();
                                switch (systransactionType)
                                {
                                    case "Application":
                                    case "Transfer In":
                                        TradeType = TradeType.Buy;
                                        break;
                                    case "Redemption":
                                        TradeType = TradeType.Sell;
                                        break;
                                    default: continue;

                                }

                                DateTime tradeDate = (DateTime)dr["TranDate"];

                                Guid productID;
                                Guid.TryParse(dr["ProductID"].ToString(), out productID);
                                Guid bankID;
                                Guid.TryParse(dr["InstituteID"].ToString(), out bankID);
                                decimal amount;
                                decimal.TryParse(dr["Amount"].ToString(), NumberStyles.Currency, CultureInfo.CurrentCulture.NumberFormat, out amount);
                                decimal interestRate;
                                decimal.TryParse(dr["InterestRate"].ToString(), NumberStyles.Currency, CultureInfo.CurrentCulture.NumberFormat, out interestRate);
                                OrderSettledUnsettledEntity unsetteled;

                                if (ds.ExtendedProperties.Count > 0 && ds.ExtendedProperties["IsManual"] != null && ds.ExtendedProperties["IsManual"].ToString().ToLower() == "true")
                                {
                                    unsetteled = OrderSettledUnsettledEntity.FirstOrDefault(ss => ss.ID == Guid.Parse(dr["EntityID"].ToString()));
                                }
                                else
                                {
                                    unsetteled = OrderSettledUnsettledEntity.FirstOrDefault(ss =>
                                    {
                                        /* Section 1.1.1.3.4 TD - buy orders
                                         * Settlement instructions is not issued to FIIG or AMM, as the money is �pulled� by Broker or Bank. 
                                         * Settlement check: � Sell TD: 
                                         * 1.Settlement transaction is within 1-5 days from trade transaction
                                         * 2.Settlement amount= trade amount
                                         * Section 1.1.1.3.5 TD � request money before mature date
                                         * Settlement check:
                                         * 1.Settlement transaction is within 1-5 days from trade transaction
                                         * 2.Settlement amount= trade amount � penalty
                                         * Penalty unknown, therefore users have to set this status manually
                                         * Section 1.1.1.3.6 TD � matures naturally
                                         * NA , as no orders or instruction issued at this point
                                         * * Revision Task-
                                         * 1.Settlement transaction is within 1-10 days from trade transaction
                                         */


                                        if (TradeType == TradeType.Buy)
                                        {
                                            return
                                                ss.TradeDate.Date.IsMatched(tradeDate.Date, 8, true)
                                                && amount.IsAbsMatched((ss.Amount.HasValue ? ss.Amount.Value : 0), 1)
                                                && ss.ProductID == productID
                                                && ss.BankId == bankID
                                                && ((ss.Percentage.HasValue ? ss.Percentage.Value * 100 : interestRate) == interestRate || accountType == OrderAccountType.AtCall)
                                                && ss.TransactionType == SetlledunSettledTransactionType.Orignal
                                                && ss.Type == OrderSetlledUnsettledType.Unsettled
                                                && (ss.AccountType == accountType);
                                        }
                                        else
                                        {
                                            return
                                                ss.TradeDate.Date.IsMatched(tradeDate.Date, 8, true)
                                                && amount.IsAbsMatched((ss.Amount.HasValue ? ss.Amount.Value : 0), 1)
                                                && ss.ProductID == productID
                                                && ss.BankId == bankID
                                                && ((ss.Percentage.HasValue ? ss.Percentage.Value * 100 : interestRate) == interestRate || accountType == OrderAccountType.AtCall)
                                                && ss.TransactionType == SetlledunSettledTransactionType.Orignal
                                                && ss.Type == OrderSetlledUnsettledType.Unsettled
                                                && (ss.AccountType == accountType);
                                        }
                                    });
                                }

                                AddConfirmationTrans(amount, tradeDate, Guid.Parse(dr["TransactionID"].ToString()), ds, unsetteled);
                            }
                        }

                        break;
                }
            }

            return ModifiedState.MODIFIED;
        }

        /// <summary>
        /// Get Days to Check for Settlement transaction For Bank
        /// </summary>
        /// <param name="entity">OrderSettledUnsettledEntity</param>
        /// <returns>int</returns>
        private int GetDaystoCheckForBank(OrderSettledUnsettledEntity entity)
        {
            //default days to check
            int daystoCheck = 3;

            //For State Street (MIS) P2 bank transaction set days to 10
            int count = OrderSettledUnsettledEntity.Count(ss => ss.OrderID == entity.OrderID && ss.OrderItemID == entity.OrderItemID && ss.AccountType == OrderAccountType.StateStreet && ss.TransactionType == SetlledunSettledTransactionType.Orignal && (ss.InvestmentCode.ToLower() == "iv004" || ss.InvestmentCode.ToLower() == "iv014"));
            if (count > 0)
            {
                daystoCheck = 8;
            }

            return daystoCheck;
        }

        /// <summary>
        /// Get Days to Check for Settlement transaction For MIS
        /// </summary>
        /// <param name="fundCode">Fund Code</param>
        /// <param name="tradeType">TradeType</param>
        /// <returns>int</returns>
        private static int GetDaystoCheckForMIS(string fundCode, TradeType tradeType)
        {
            int daystoCheck;
            switch (fundCode.ToLower())
            {
                //For P2 i.e. IV004 and IV014
                case "iv014":
                case "iv004":
                    daystoCheck = 8;
                    break;
                default:
                    daystoCheck = tradeType == TradeType.Buy ? 2 : 3;
                    break;
            }
            return daystoCheck;
        }

        private void UpdateUnsetteledStatus(OrderEntity orderEntity)
        {
            var unsettledTrans = _OrderSettledUnsettledEntity.Where(us => us.OrderID == orderEntity.ID);

            foreach (var orderSettledUnsettledEntity in unsettledTrans)
            {
                orderSettledUnsettledEntity.Type = (orderEntity.Status == OrderStatus.Submitted)
                                                       ? OrderSetlledUnsettledType.Unsettled
                                                       : OrderSetlledUnsettledType.Initialized;
            }
        }

        private void AddUnsetteled(OrderEntity order, IOrganization org)
        {
            foreach (var item in order.Items)
            {
                decimal amount = 0;

                if (item is StateStreetOrderItem)
                {
                    var sitem = item as StateStreetOrderItem;
                    var seunsettled = new OrderSettledUnsettledEntity
                                          {
                                              ID = Guid.NewGuid(),
                                              OrderID = order.ID,
                                              OrderNo = order.OrderID,
                                              ClientCID = order.ClientCID,
                                              ClientID = order.ClientID,
                                              //AccountCID = order.AccountCID,
                                              //AccountNo = order.AccountNumber,
                                              Type = order.Status == OrderStatus.Submitted ? OrderSetlledUnsettledType.Unsettled : OrderSetlledUnsettledType.Initialized,
                                              OrderItemID = sitem.ID,
                                              InvestmentCode = sitem.FundCode,
                                              ProductID = sitem.ProductID,
                                              CreatedDate = DateTime.Now,
                                              SettledAssociation = Guid.Empty,
                                              TransactionID = Guid.Empty,
                                              TradeDate = order.TradeDate,
                                              TradeType =
                                                  order.OrderItemType == OrderItemType.Buy ? TradeType.Buy : TradeType.Sell,
                                              EntryType = SetlledunSettledEntryType.TradeTransactions,
                                              TransactionType = SetlledunSettledTransactionType.Orignal,
                                              AccountType = order.OrderAccountType,
                                              OrderType = order.OrderAccountType,
                                              ClientManagementType = order.ClientManagementType,
                                          };
                    var sec = org.Securities.FirstOrDefault(ss => ss.AsxCode == sitem.FundCode);
                    decimal unitPrice = 0;
                    if (sec != null)
                    {
                        var latestprice = sec.ASXSecurity.OrderByDescending(ss => ss.Date).FirstOrDefault();
                        if (latestprice != null)
                        {
                            unitPrice = (decimal)latestprice.UnitPrice;
                        }
                    }

                    if (order.OrderItemType == OrderItemType.Buy)
                    {
                        seunsettled.Amount = sitem.Amount;
                        item.Units = seunsettled.Units = seunsettled.Amount / unitPrice;
                    }
                    else if (order.OrderItemType == OrderItemType.Sell)
                    {
                        if ((item.Units.HasValue) && (item.Units.Value > 0))
                        {
                            item.Amount =
                                seunsettled.Amount = (sitem.Units.HasValue ? (sitem.Units.Value * unitPrice) : 0) * -1;
                            seunsettled.Units = sitem.Units * -1;
                        }
                        else
                        {
                            seunsettled.Amount = item.Amount = item.Amount * -1;
                            item.Units = (item.Amount.HasValue) ? (item.Amount / unitPrice) * -1 : null;
                            seunsettled.Units = item.Units * -1;
                        }
                    }
                    amount = seunsettled.Amount.Value;
                    seunsettled.UnitPrice = unitPrice;
                    OrderSettledUnsettledEntity.Add(seunsettled);
                }
                else if (item is ASXOrderItem)
                {
                    var asxOrderItem = item as ASXOrderItem;
                    var seunsettled = new OrderSettledUnsettledEntity
                                          {
                                              ID = Guid.NewGuid(),
                                              OrderID = order.ID,
                                              OrderNo = order.OrderID,
                                              ClientCID = order.ClientCID,
                                              ClientID = order.ClientID,
                                              Type = order.Status == OrderStatus.Submitted ? OrderSetlledUnsettledType.Unsettled : OrderSetlledUnsettledType.Initialized,
                                              AccountCID = asxOrderItem.ASXAccountCID,
                                              AccountNo = asxOrderItem.ASXAccountNo,
                                              OrderItemID = asxOrderItem.ID,
                                              InvestmentCode = asxOrderItem.InvestmentCode,
                                              ProductID = asxOrderItem.ProductID,
                                              CreatedDate = DateTime.Now,
                                              SettledAssociation = Guid.Empty,
                                              TransactionID = Guid.Empty,
                                              TradeDate = order.TradeDate,
                                              TradeType =
                                                  order.OrderItemType == OrderItemType.Buy ? TradeType.Buy : TradeType.Sell,
                                              EntryType = SetlledunSettledEntryType.TradeTransactions,
                                              TransactionType = SetlledunSettledTransactionType.Orignal,
                                              AccountType = order.OrderAccountType,
                                              OrderType = order.OrderAccountType,
                                              ClientManagementType = order.ClientManagementType,
                                          };
                    var sec = org.Securities.FirstOrDefault(ss => ss.AsxCode == asxOrderItem.InvestmentCode);
                    decimal unitPrice = 0;
                    if (sec != null)
                    {
                        var latestprice = sec.ASXSecurity.OrderByDescending(ss => ss.Date).FirstOrDefault();
                        if (latestprice != null)
                        {
                            unitPrice = (decimal)latestprice.UnitPrice;
                        }
                        switch (order.OrderType)
                        {
                            case OrderType.Manual:
                                unitPrice = asxOrderItem.UnitPrice.Value;
                                break;
                            case OrderType.FinSimplicity:
                                if (asxOrderItem.UnitPrice.HasValue)
                                    unitPrice = asxOrderItem.UnitPrice.Value;
                                else
                                    asxOrderItem.UnitPrice = unitPrice; // if price not found in order item which is imported from fin simplicity then update with latest unit price.
                                break;
                        }
                    }
                    if (order.OrderItemType == OrderItemType.Buy)
                    {
                        seunsettled.Amount = asxOrderItem.Amount;
                        if (order.OrderType == OrderType.Manual)
                        {
                            item.Units = seunsettled.Units = asxOrderItem.Units;
                        }
                        else
                        {
                            item.Units = seunsettled.Units = seunsettled.Amount / unitPrice;
                        }
                    }
                    else if (order.OrderItemType == OrderItemType.Sell)
                    {
                        if (order.OrderType == OrderType.Manual)
                        {
                            item.Amount = seunsettled.Amount = (asxOrderItem.Units.HasValue ? asxOrderItem.Amount : 0) * -1;
                            seunsettled.Units = asxOrderItem.Units * -1;
                        }
                        else
                        {
                            item.Amount = seunsettled.Amount = (asxOrderItem.Units.HasValue ? (asxOrderItem.Units.Value * unitPrice) : 0) * -1;
                            seunsettled.Units = asxOrderItem.Units * -1;
                        }
                    }
                    amount = seunsettled.Amount.Value;
                    seunsettled.UnitPrice = unitPrice;
                    seunsettled.BrokrageFee = Math.Abs(amount.CalculateBrokrage().GetMax(20));

                    OrderSettledUnsettledEntity.Add(seunsettled);
                }
                else if (item is TermDepositOrderItem)
                {
                    var termDepositOrderItem = item as TermDepositOrderItem;
                    var seunsettled = new OrderSettledUnsettledEntity
                                          {
                                              ID = Guid.NewGuid(),
                                              OrderID = order.ID,
                                              OrderNo = order.OrderID,
                                              ClientCID = order.ClientCID,
                                              ClientID = order.ClientID,
                                              Type = order.Status == OrderStatus.Submitted ? OrderSetlledUnsettledType.Unsettled : OrderSetlledUnsettledType.Initialized,
                                              AccountCID = termDepositOrderItem.TDAccountCID,
                                              AccountNo = termDepositOrderItem.Duration + "-TD",
                                              OrderItemID = termDepositOrderItem.ID,
                                              ProductID = termDepositOrderItem.ProductID,
                                              BrokerId = termDepositOrderItem.BrokerID,
                                              BankId = termDepositOrderItem.BankID,
                                              Duration = termDepositOrderItem.Duration,
                                              Percentage = termDepositOrderItem.Percentage,
                                              CreatedDate = DateTime.Now,
                                              SettledAssociation = Guid.Empty,
                                              TransactionID = Guid.Empty,
                                              Amount =
                                                  order.OrderItemType == OrderItemType.Sell
                                                      ? (termDepositOrderItem.Amount * -1)
                                                      : termDepositOrderItem.Amount,
                                              TradeDate = order.TradeDate,
                                              TradeType =
                                                  order.OrderItemType == OrderItemType.Buy ? TradeType.Buy : TradeType.Sell,
                                              EntryType = SetlledunSettledEntryType.TradeTransactions,
                                              TransactionType = SetlledunSettledTransactionType.Orignal,
                                              AccountType = order.OrderAccountType,
                                              OrderType = order.OrderAccountType,
                                              ClientManagementType = order.ClientManagementType,
                                          };
                    amount = seunsettled.Amount.Value;

                    OrderSettledUnsettledEntity.Add(seunsettled);
                }
                else if (item is AtCallOrderItem)
                {
                    var atCallOrderItem = item as AtCallOrderItem;

                    switch (atCallOrderItem.AtCallType)
                    {
                        case AtCallType.BestRates:
                            {
                                var seunsettled = new OrderSettledUnsettledEntity
                                 {
                                     ID = Guid.NewGuid(),
                                     OrderID = order.ID,
                                     OrderNo = order.OrderID,
                                     ClientCID = order.ClientCID,
                                     ClientID = order.ClientID,
                                     Type = order.Status == OrderStatus.Submitted ? OrderSetlledUnsettledType.Unsettled : OrderSetlledUnsettledType.Initialized,
                                     AccountCID = atCallOrderItem.AtCallAccountCID,
                                     AccountNo = "At Call",
                                     OrderItemID = atCallOrderItem.ID,
                                     ProductID = atCallOrderItem.ProductID,
                                     BrokerId = atCallOrderItem.BrokerID,
                                     BankId = atCallOrderItem.BankID,
                                     Percentage = atCallOrderItem.Rate,
                                     CreatedDate = DateTime.Now,
                                     SettledAssociation = Guid.Empty,
                                     TransactionID = Guid.Empty,
                                     Amount =
                                         order.OrderItemType == OrderItemType.Sell
                                             ? (atCallOrderItem.Amount * -1)
                                             : atCallOrderItem.Amount,
                                     TradeDate = order.TradeDate,
                                     TradeType =
                                         order.OrderItemType == OrderItemType.Buy ? TradeType.Buy : TradeType.Sell,
                                     EntryType = SetlledunSettledEntryType.TradeTransactions,
                                     TransactionType = SetlledunSettledTransactionType.Orignal,
                                     AccountType = order.OrderAccountType,
                                     OrderType = order.OrderAccountType,
                                     ClientManagementType = order.ClientManagementType,
                                 };
                                amount = seunsettled.Amount.Value;

                                OrderSettledUnsettledEntity.Add(seunsettled);

                            }
                            break;
                        case AtCallType.MoneyMovement:
                            {
                                var seunsettled = new OrderSettledUnsettledEntity
                                                      {
                                                          ID = Guid.NewGuid(),
                                                          OrderID = order.ID,
                                                          OrderNo = order.OrderID,
                                                          ClientCID = order.ClientCID,
                                                          ClientID = order.ClientID,
                                                          Type =
                                                              order.Status == OrderStatus.Submitted
                                                                  ? OrderSetlledUnsettledType.Unsettled
                                                                  : OrderSetlledUnsettledType.Initialized,

                                                          AccountCID = atCallOrderItem.TransferAccCID,
                                                          AccountNo = atCallOrderItem.TransferAccNumber,
                                                          AccountBsb = atCallOrderItem.TransferAccBSB,
                                                          OrderItemID = atCallOrderItem.ID,

                                                          CreatedDate = DateTime.Now,
                                                          SettledAssociation = Guid.Empty,
                                                          TransactionID = Guid.Empty,
                                                          Amount = order.OrderItemType == OrderItemType.Sell
                                                                       ? (atCallOrderItem.Amount * -1)
                                                                       : atCallOrderItem.Amount,
                                                          TradeDate = order.TradeDate,
                                                          TradeType =
                                                              order.OrderItemType == OrderItemType.Buy
                                                                  ? TradeType.Buy
                                                                  : TradeType.Sell,
                                                          EntryType = SetlledunSettledEntryType.TradeTransactions,
                                                          TransactionType = SetlledunSettledTransactionType.Orignal,
                                                          AccountType = OrderAccountType.BankAccount,
                                                          OrderType = order.OrderAccountType,
                                                          ClientManagementType = order.ClientManagementType,
                                                      };
                                amount = seunsettled.Amount.Value;

                                if (!atCallOrderItem.IsExternalAccount)
                                    OrderSettledUnsettledEntity.Add(seunsettled);
                            }
                            break;
                        case AtCallType.MoneyMovementBroker:
                            {

                                var seunsettled = new OrderSettledUnsettledEntity
                                  {
                                      ID = Guid.NewGuid(),
                                      OrderID = order.ID,
                                      OrderNo = order.OrderID,
                                      ClientCID = order.ClientCID,
                                      ClientID = order.ClientID,
                                      Type = order.Status == OrderStatus.Submitted ? OrderSetlledUnsettledType.Unsettled : OrderSetlledUnsettledType.Initialized,
                                      AccountCID = atCallOrderItem.AtCallAccountCID,
                                      AccountNo = "At Call",
                                      OrderItemID = atCallOrderItem.ID,
                                      ProductID = atCallOrderItem.ProductID,
                                      BrokerId = atCallOrderItem.BrokerID,
                                      BankId = atCallOrderItem.BankID,
                                      CreatedDate = DateTime.Now,
                                      SettledAssociation = Guid.Empty,
                                      TransactionID = Guid.Empty,
                                      Amount =
                                          order.OrderItemType == OrderItemType.Sell
                                              ? (atCallOrderItem.Amount * -1)
                                              : atCallOrderItem.Amount,
                                      TradeDate = order.TradeDate,
                                      TradeType =
                                          order.OrderItemType == OrderItemType.Buy ? TradeType.Buy : TradeType.Sell,
                                      EntryType = SetlledunSettledEntryType.TradeTransactions,
                                      TransactionType = SetlledunSettledTransactionType.Orignal,
                                      AccountType = order.OrderAccountType,
                                      OrderType = order.OrderAccountType,
                                      ClientManagementType = order.ClientManagementType,
                                  };
                                amount = seunsettled.Amount.Value;

                                OrderSettledUnsettledEntity.Add(seunsettled);
                            }
                            break;

                    }
                }
                decimal? brokrageAmount = order.OrderAccountType == OrderAccountType.ASX ? Math.Abs(amount.CalculateBrokrage().GetMax(20)) : (decimal?)null;
                switch (order.OrderAccountType)
                {
                    case OrderAccountType.StateStreet:
                    case OrderAccountType.ASX:
                    case OrderAccountType.TermDeposit:
                    case OrderAccountType.AtCall:
                        var bankSettled = new OrderSettledUnsettledEntity
                                              {
                                                  ID = Guid.NewGuid(),
                                                  OrderID = order.ID,
                                                  OrderNo = order.OrderID,
                                                  OrderItemID = item.ID,
                                                  //id of item
                                                  ClientCID = order.ClientCID,
                                                  ClientID = order.ClientID,
                                                  AccountCID = order.AccountCID,
                                                  AccountNo = order.AccountNumber,
                                                  AccountBsb = order.AccountBSB,
                                                  Type = order.Status == OrderStatus.Submitted ? OrderSetlledUnsettledType.Unsettled : OrderSetlledUnsettledType.Initialized,
                                                  CreatedDate = DateTime.Now,
                                                  SettledAssociation = Guid.Empty,
                                                  TransactionID = Guid.Empty,
                                                  Amount = amount,
                                                  BrokrageFee = brokrageAmount,
                                                  TradeDate = order.TradeDate,
                                                  TradeType = order.OrderItemType == OrderItemType.Buy ? TradeType.Buy : TradeType.Sell,
                                                  EntryType = SetlledunSettledEntryType.SettlementTransactions,
                                                  TransactionType = SetlledunSettledTransactionType.Orignal,
                                                  AccountType = OrderAccountType.BankAccount,
                                                  OrderType = order.OrderAccountType,
                                                  ClientManagementType = order.ClientManagementType,
                                              };
                        if (order.OrderItemType == OrderItemType.Buy)
                        {
                            if (bankSettled.Amount > 0)
                                bankSettled.Amount *= -1;
                        }
                        else if (order.OrderItemType == OrderItemType.Sell)
                        {
                            if (bankSettled.Amount < 0)
                                bankSettled.Amount *= -1;
                        }
                        OrderSettledUnsettledEntity.Add(bankSettled);
                        break;
                }
            }
        }

        private void RemoveUnsettled(OrderEntity order)
        {
            _OrderSettledUnsettledEntity.RemoveAll(ss => ss.OrderID == order.ID);
        }

        private void AddConfirmationTrans(decimal amount, DateTime tradeDate, Guid TransactionID, SettledUnsetteledTransactionDS ds, OrderSettledUnsettledEntity unsetteled, decimal units = 0)
        {
            /*  �	Issued (on instruction register , instruction file is generated)
                �	On market (Confirmation for one transaction (Trade or Settlement) is received,  but confirmation for its matching transaction is not.  For example shares are sold, but haven�t received money)
                �	Settled (when both confirmation for trade and settlement instructions are received � match by date, but amount or number of units does not match)
                �	Complete (when confirmation reconciled with original transaction, both confirmation for trade and settlement instructions are received and match by date , amount and number of units,  i.e amount moved to a bank account matches to the amount that has appeared on this bank account)
                �	Cancelled (see Order cancelation)
            */
            if (unsetteled != null)
            {
                unsetteled.Type = OrderSetlledUnsettledType.OnMarket;
                unsetteled.UpdatedDate = DateTime.Now;
                AddConfirmationTransaction(unsetteled, TransactionID, tradeDate, amount, units);
                SetTradeTransactionsOnMarket(unsetteled.OrderID, unsetteled.OrderItemID);
                SettleUnSettleOrder(unsetteled.OrderID, unsetteled.ClientID, ds.Unit.CurrentUser, DatasetCommandTypes.Settle);
            }
        }

        private void AddExternalConfirmationTrans(OrderSettledUnsettledEntity unsetteled, SettledUnsetteledTransactionDS ds)
        {
            if (unsetteled != null && unsetteled.Type == OrderSetlledUnsettledType.OnMarket)
            {
                var setunsetDs = new SettledUnsetteledDS { Unit = ds.Unit };
                var dataRow = setunsetDs.SettledUnsetteledTable.NewRow();
                dataRow[setunsetDs.SettledUnsetteledTable.ORDERID] = unsetteled.OrderID;
                setunsetDs.SettledUnsetteledTable.Rows.Add(dataRow);
                GetOrderStatus(setunsetDs);
                if (setunsetDs.SettledUnsetteledTable.Rows[0][setunsetDs.SettledUnsetteledTable.ORDERBANKACCOUNTTYPE].ToString() == OrderBankAccountType.External.ToString())
                {
                    var setunsettled = OrderSettledUnsettledEntity.FirstOrDefault(ss => ss.ID != unsetteled.ID && ss.OrderID == unsetteled.OrderID && ss.OrderItemID == unsetteled.OrderItemID && ss.TransactionType == SetlledunSettledTransactionType.Orignal);
                    if (setunsettled != null)
                    {
                        setunsettled.Type = OrderSetlledUnsettledType.OnMarket;
                        setunsettled.UpdatedDate = DateTime.Now;
                        SetTradeTransactionsOnMarket(setunsettled.OrderID, setunsettled.OrderItemID);
                        SettleUnSettleOrder(setunsettled.OrderID, setunsettled.ClientID, ds.Unit.CurrentUser, DatasetCommandTypes.Settle);
                    }
                }
            }
        }

        private void AddConfirmationByForce(SettledUnsetteledDS ds)
        {
            foreach (DataRow row in ds.SettledUnsetteledTable.Rows)
            {
                var id = Guid.Parse(row[ds.SettledUnsetteledTable.ID].ToString());
                var setunsettled = OrderSettledUnsettledEntity.FirstOrDefault(ss => ss.ID == id && ss.TransactionType == SetlledunSettledTransactionType.Orignal && ss.Type == OrderSetlledUnsettledType.Unsettled);
                if (setunsettled != null)
                {
                    setunsettled.Type = OrderSetlledUnsettledType.OnMarket;
                    setunsettled.UpdatedDate = DateTime.Now;
                    SetTradeTransactionsOnMarket(setunsettled.OrderID, setunsettled.OrderItemID);
                    SettleUnSettleOrder(setunsettled.OrderID, setunsettled.ClientID, ds.Unit.CurrentUser, DatasetCommandTypes.Settle);
                }
            }
        }

        private void AddConfirmationByForce(OrderEntity order, UserEntity user)
        {
            var setunsettled = OrderSettledUnsettledEntity.Where(ss => ss.OrderID == order.ID && ss.TransactionType == SetlledunSettledTransactionType.Orignal && ss.Type == OrderSetlledUnsettledType.Unsettled);
            foreach (var unsettledEntity in setunsettled)
            {
                unsettledEntity.Type = OrderSetlledUnsettledType.OnMarket;
                unsettledEntity.UpdatedDate = DateTime.Now;
                SetTradeTransactionsOnMarket(unsettledEntity.OrderID, unsettledEntity.OrderItemID);
                SettleUnSettleOrder(unsettledEntity.OrderID, unsettledEntity.ClientID, user, DatasetCommandTypes.Settle);
            }
        }

        private void RemoveConfirmation(SettledUnsetteledDS ds)
        {
            if (!string.IsNullOrEmpty(ds.SettledUnsetteledTable.Rows[0][ds.SettledUnsetteledTable.ID].ToString()))
            {
                var id = new Guid(ds.SettledUnsetteledTable.Rows[0][ds.SettledUnsetteledTable.ID].ToString());
                //Get Confirmation item
                var item = _OrderSettledUnsettledEntity.FirstOrDefault(ss => ss.ID == id);
                if (item != null)
                {
                    RemoveExternalConfirmationTrans(item, ds);
                    RemoveItem(item, ds);
                }
            }
            else if (!string.IsNullOrEmpty(ds.SettledUnsetteledTable.Rows[0][ds.SettledUnsetteledTable.TRANSACTIONID].ToString()))
            {
                var transId = new Guid(ds.SettledUnsetteledTable.Rows[0][ds.SettledUnsetteledTable.TRANSACTIONID].ToString());
                //Get Confirmation items
                var items = _OrderSettledUnsettledEntity.Where(ss => ss.TransactionID == transId).ToList();
                for (int i = items.Count - 1; i >= 0; i--)
                {
                    RemoveExternalConfirmationTrans(items[i], ds);
                    RemoveItem(items[i], ds);
                }
            }
        }

        private void RemoveExternalConfirmationTrans(OrderSettledUnsettledEntity unsetteled, SettledUnsetteledDS ds)
        {
            if (unsetteled != null && unsetteled.TransactionType == SetlledunSettledTransactionType.Confirmation)
            {
                var setunsetDs = new SettledUnsetteledDS { Unit = ds.Unit };
                var dataRow = setunsetDs.SettledUnsetteledTable.NewRow();
                dataRow[setunsetDs.SettledUnsetteledTable.ORDERID] = unsetteled.OrderID;
                setunsetDs.SettledUnsetteledTable.Rows.Add(dataRow);
                GetOrderStatus(setunsetDs);
                if (setunsetDs.SettledUnsetteledTable.Rows[0][setunsetDs.SettledUnsetteledTable.ORDERBANKACCOUNTTYPE].ToString() == OrderBankAccountType.External.ToString())
                {
                    var setunsettled = OrderSettledUnsettledEntity.FirstOrDefault(ss => ss.ID != unsetteled.ID && ss.ID != unsetteled.SettledAssociation && ss.OrderID == unsetteled.OrderID && ss.OrderItemID == unsetteled.OrderItemID && ss.TransactionType == SetlledunSettledTransactionType.Orignal);
                    if (setunsettled != null && setunsettled.Type != OrderSetlledUnsettledType.Unsettled)
                    {
                        setunsettled.Type = OrderSetlledUnsettledType.Unsettled;
                        setunsettled.SettledAssociation = Guid.Empty;
                        setunsettled.TransactionID = Guid.Empty;
                    }
                }
            }
        }

        private void RemoveForcedConfirmation(SettledUnsetteledDS ds)
        {
            foreach (DataRow row in ds.SettledUnsetteledTable.Rows)
            {
                var id = Guid.Parse(row[ds.SettledUnsetteledTable.ID].ToString());
                var setunsettled = OrderSettledUnsettledEntity.FirstOrDefault(ss => ss.ID == id && ss.TransactionType == SetlledunSettledTransactionType.Orignal && (ss.Type == OrderSetlledUnsettledType.OnMarket || ss.Type == OrderSetlledUnsettledType.Settled));
                if (setunsettled != null)
                {
                    setunsettled.Type = OrderSetlledUnsettledType.Unsettled;
                    setunsettled.UpdatedDate = DateTime.Now;
                    setunsettled.SettledAssociation = Guid.Empty;
                    setunsettled.TransactionID = Guid.Empty;
                    // find its pair transactions and if it is settled change it to on market
                    var otherSettledItem = _OrderSettledUnsettledEntity.Where(ss => ss.OrderID == setunsettled.OrderID && ss.OrderItemID == setunsettled.OrderItemID && ss.Type == OrderSetlledUnsettledType.Settled);
                    foreach (var orderSettledUnsettledEntity in otherSettledItem)
                    {
                        orderSettledUnsettledEntity.Type = OrderSetlledUnsettledType.OnMarket;
                    }
                    SettleUnSettleOrder(setunsettled.OrderID, setunsettled.ClientID, ds.Unit.CurrentUser, DatasetCommandTypes.Unsettle);
                }
            }
        }

        private void RemoveForcedConfirmation(OrderEntity order, UserEntity user)
        {
            var setunsettled = OrderSettledUnsettledEntity.Where(ss => ss.OrderID == order.ID && ss.TransactionType == SetlledunSettledTransactionType.Orignal && (ss.Type == OrderSetlledUnsettledType.OnMarket || ss.Type == OrderSetlledUnsettledType.Settled));
            foreach (var settledEntity in setunsettled)
            {
                settledEntity.Type = OrderSetlledUnsettledType.Unsettled;
                settledEntity.UpdatedDate = DateTime.Now;
                settledEntity.SettledAssociation = Guid.Empty;
                settledEntity.TransactionID = Guid.Empty;
                // find its pair transactions and if it is settled change it to on market
                OrderSettledUnsettledEntity entity = settledEntity;
                var otherSettledItem = _OrderSettledUnsettledEntity.Where(ss => ss.OrderID == entity.OrderID && ss.OrderItemID == entity.OrderItemID && ss.Type == OrderSetlledUnsettledType.Settled);
                foreach (var orderSettledUnsettledEntity in otherSettledItem)
                {
                    orderSettledUnsettledEntity.Type = OrderSetlledUnsettledType.OnMarket;
                }
                SettleUnSettleOrder(settledEntity.OrderID, settledEntity.ClientID, user, DatasetCommandTypes.Unsettle);
            }
        }

        private void RemoveItem(OrderSettledUnsettledEntity item, SettledUnsetteledDS ds)
        {
            //Get Original Item 
            var actualItem = _OrderSettledUnsettledEntity.FirstOrDefault(ss => ss.ID == item.SettledAssociation);
            if (actualItem != null)
            {
                //remove settled association
                actualItem.SettledAssociation = Guid.Empty;
                //remove transaction id
                actualItem.TransactionID = Guid.Empty;
                actualItem.Type = OrderSetlledUnsettledType.Unsettled;

                // find its pair transaction and if it is settled change it to on market
                var otherSettledItem = _OrderSettledUnsettledEntity.Where(ss => ss.OrderID == actualItem.OrderID && ss.OrderItemID == actualItem.OrderItemID && ss.Type == OrderSetlledUnsettledType.Settled);
                foreach (var orderSettledUnsettledEntity in otherSettledItem)
                {
                    orderSettledUnsettledEntity.Type = OrderSetlledUnsettledType.OnMarket;
                }

                SettleUnSettleOrder(actualItem.OrderID, ds.Unit.ClientId, ds.Unit.CurrentUser, DatasetCommandTypes.Unsettle);
            }
            //Remove Confirmation item
            _OrderSettledUnsettledEntity.Remove(item);
        }

        private void SettleUnSettleOrder(Guid orderID, string clientId, UserEntity user, DatasetCommandTypes command)
        {
            if (orderID == Guid.Empty) return;

            if (command == DatasetCommandTypes.Settle)
            {
                int count = OrderSettledUnsettledEntity.Count(ss => ss.OrderID == orderID && ss.Type < OrderSetlledUnsettledType.Settled);
                if (count == 0) //all items have been settled
                {
                    SetOrderStatus(orderID, clientId, user, command);
                }
            }
            else if (command == DatasetCommandTypes.Unsettle)
            {
                SetOrderStatus(orderID, clientId, user, command);
            }
        }

        private void SetOrderStatus(Guid orderID, string clientId, UserEntity user, DatasetCommandTypes command)
        {
            Guid OrderCID = IsOrderCmExists(user);
            if (OrderCID != Guid.Empty)
            {
                var unit = new Data.OrganizationUnit
                               {
                                   ClientId = clientId,
                                   Type = ((int)OrganizationType.Order).ToString(),
                                   CurrentUser = user
                               };

                var ds = new OrderPadDS { CommandType = command, Unit = unit };
                DataRow dr = ds.OrdersTable.NewRow();
                dr[ds.OrdersTable.ID] = orderID;
                ds.OrdersTable.Rows.Add(dr);

                var ordercm = Broker.GetBMCInstance(OrderCID);
                ordercm.SetData(ds);
                Broker.ReleaseBrokerManagedComponent(ordercm);
            }
        }

        private void GetOrderStatus(SettledUnsetteledDS ds)
        {
            Guid orderCID = IsOrderCmExists(ds.Unit.CurrentUser);
            if (orderCID != Guid.Empty)
            {
                var ordercm = Broker.GetBMCInstance(orderCID);
                ordercm.GetData(ds);
                Broker.ReleaseBrokerManagedComponent(ordercm);
            }
        }

        private Guid IsOrderCmExists(UserEntity user)
        {
            var unit = new Data.OrganizationUnit
            {
                Type = ((int)OrganizationType.Order).ToString(),
                CurrentUser = user
            };

            var ds = new OrderPadDS
            {
                CommandType = DatasetCommandTypes.Check,
                Unit = unit,
                Command = (int)WebCommands.GetOrganizationUnitsByType
            };

            var org = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            org.GetData(ds);
            Broker.ReleaseBrokerManagedComponent(org);

            Guid orderCMCID = Guid.Empty;
            if (ds.OrdersTable.Rows.Count > 0)
            {
                //Getting ORDER CID 
                orderCMCID = new Guid(ds.OrdersTable.Rows[0][ds.OrdersTable.CID].ToString());
            }

            return orderCMCID;
        }

        private void AddConfirmationTransaction(OrderSettledUnsettledEntity entity, Guid transID, DateTime tradeDate, decimal amount, decimal units = 0)
        {
            var seunsettled = entity.GetConfirmationTransaction(transID, tradeDate, amount, units);
            entity.SettledAssociation = seunsettled.ID;
            OrderSettledUnsettledEntity.Add(seunsettled);
        }

        private void SetTradeTransactionsOnMarket(Guid orderID, Guid itemId)
        {
            var unsetteled = OrderSettledUnsettledEntity.Count(ss => ss.OrderID == orderID && ss.OrderItemID == itemId && ss.TransactionType == SetlledunSettledTransactionType.Orignal && ss.Type < OrderSetlledUnsettledType.OnMarket);

            if (unsetteled == 0)// both trade and settlement have been changed to on market (received confirmation) then change them to settled
            {
                var temp = OrderSettledUnsettledEntity.Where(ss => ss.OrderID == orderID && ss.OrderItemID == itemId);
                foreach (var orderSettledUnsettledEntity in temp)
                {
                    orderSettledUnsettledEntity.Type = OrderSetlledUnsettledType.Settled;
                }
            }
        }
    }

}
