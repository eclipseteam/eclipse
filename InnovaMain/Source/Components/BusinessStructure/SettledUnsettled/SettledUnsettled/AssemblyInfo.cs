﻿using System.Reflection;

using Oritax.TaxSimp.InstallInfo;
using Oritax.TaxSimp.CM.Entity;

[assembly: AssemblyTitle("")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("")]
[assembly: AssemblyCopyright("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: AssemblyVersion(SettledUnsettledInstall.ASSEMBLY_MAJORVERSION + "." + SettledUnsettledInstall.ASSEMBLY_MINORVERSION)]
[assembly: AssemblyInformationalVersion(SettledUnsettledInstall.ASSEMBLY_MAJORVERSION + "." + SettledUnsettledInstall.ASSEMBLY_MINORVERSION + "." + SettledUnsettledInstall.ASSEMBLY_DATAFORMAT + "." + SettledUnsettledInstall.ASSEMBLY_REVISION)]
[assembly: AssemblyInstallInfo(SettledUnsettledInstall.ASSEMBLY_ID, SettledUnsettledInstall.ASSEMBLY_NAME, SettledUnsettledInstall.ASSEMBLY_DISPLAYNAME)]
[assembly: ComponentInstallInfo(SettledUnsettledInstall.COMPONENT_ID, SettledUnsettledInstall.COMPONENT_NAME, SettledUnsettledInstall.COMPONENT_DISPLAYNAME, SettledUnsettledInstall.COMPONENT_CATEGORY)]
[assembly: ComponentVersionInstallInfo(SettledUnsettledInstall.COMPONENTVERSION_STARTDATE, SettledUnsettledInstall.COMPONENTVERSION_ENDDATE, SettledUnsettledInstall.COMPONENTVERSION_PERSISTERASSEMBLY, SettledUnsettledInstall.COMPONENTVERSION_PERSISTERCLASS, SettledUnsettledInstall.COMPONENTVERSION_IMPLEMENTATIONCLASS)]

[assembly: AssemblyDelaySign(false)]
