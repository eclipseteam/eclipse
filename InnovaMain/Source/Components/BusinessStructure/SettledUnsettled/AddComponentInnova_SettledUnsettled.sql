
Declare @AssemblyID varchar(100)
Declare @ComponentID varchar(100)

Declare @AssemblyStrongName varchar(100)
Declare @ImplementationCMClass varchar(100)

set @AssemblyID='83F89990-796E-4FA8-BA8F-83658BA0B12D'
set @ComponentID='84099B12-A768-4D49-BBD5-BE9832AE7844'
set @AssemblyStrongName='SettledUnsettled'
set @ImplementationCMClass='Oritax.TaxSimp.CM.Entity.SettledUnsettledCM'

DELETE FROM [dbo].[ASSEMBLY]
      WHERE ID = @AssemblyID



INSERT INTO [dbo].[ASSEMBLY]
           ([ID]
           ,[NAME]
           ,[DISPLAYNAME]
           ,[STRONGNAME]
           ,[MAJORVERSION]
           ,[MINORVERSION]
           ,[DATAFORMAT]
           ,[REVISION])
     VALUES
     (
           @AssemblyID,
           @AssemblyStrongName,
           @AssemblyStrongName, 
           @AssemblyStrongName+', Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215',
           1,
           1,
           0,
           0
           )


DELETE FROM [dbo].[COMPONENT]
      WHERE ID = @ComponentID



INSERT INTO [dbo].[COMPONENT]
           ([ID]
           ,[NAME]
           ,[DISPLAYNAME]
           ,[CATEGORY]
           ,[APPLICABILITY]
           ,[OBSOLETE])
     VALUES (
           @ComponentID,
           @AssemblyStrongName,
           @AssemblyStrongName,
           'BusinessEntity',
           3,
           'False'
           )


DELETE FROM [dbo].[COMPONENTVERSION]
      WHERE ID = @AssemblyID



INSERT INTO [dbo].[COMPONENTVERSION]
           ([ID]
           ,[VERSIONNAME]
           ,[COMPONENTID]
           ,[STARTDATE]
           ,[ENDDATE]
           ,[PERSISTERASSEMBLYSTRONGNAME]
           ,[PERSISTERCLASS]
           ,[IMPLEMENTATIONCLASS]
           ,[OBSOLETE])
     VALUES
     (
           @AssemblyID,
           '',
           @ComponentID,
           '1990-01-01 00:00:00.000',
           '2050-12-31 00:59:59.000',
           'CommonPersisters_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215',
           'Oritax.TaxSimp.CommonPersistence.BlobPersister',
           @ImplementationCMClass,
           'False'
           )
