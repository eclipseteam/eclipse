using System;
using System.Web.UI;
using System.Web.UI.WebControls;
//using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.Calculation;
namespace Oritax.TaxSimp.CM.Entity.AustralianEntity
{
	/// <summary>
	/// Summary description for AustralianEntityWorkpaper.
	/// </summary>
	public abstract class AustralianEntityWorkpaper : BusinessStructureWorkpaper
	{
		protected System.Web.UI.WebControls.TextBox textBox_EntityName;
		
		public AustralianEntityWorkpaper() :base(){	}

		#region Overriden Properties

		protected override BreadCrumbType BreadCrumbDisplayType
		{
			get
			{
				return BreadCrumbType.NoPeriodBreadCrumbWhenInstanceExists;
			}
		}

		protected override ActionType PostSaveActionType
		{
			get
			{
				return ActionType.RemainOnWorkpaper;
			}
		}

		protected override ActionType CancelActionType
		{
			get
			{
				//Set redirect action page so that cancel and save operations will redirect to the Entities
				//list workpaper.
				this.SetRedirectActionPage("Entities.aspx","Organization_1_1");
				return ActionType.RedirectToSpecificWorkpaper;
			}
		}

		protected override ActionType CloseActionType
		{
			get
			{
				//Set redirect action page so that cancel and save operations will redirect to the Entities
				//list workpaper.
				this.SetRedirectActionPage("Entities.aspx","Organization_1_1");
				return ActionType.RedirectToSpecificWorkpaper;
			}
		}

		protected override Javascript[] WorkpaperSpecificJavascript
		{
			get
			{
				return new Javascript[]{new Javascript(JavaScriptType.IncludeFile,"scripts/OrganisationUnitScripts_1_1.js")};
			}
		}

		protected override HeaderType WorkpaperHeaderType
		{
			get
			{
				return HeaderType.BusinessStructureEdit;
			}
		}

		protected override string NameOnCreation
		{
			get
			{
				return StringUtilities.RemoveRecurringSpaces(this.textBox_EntityName.Text.Trim());
			}
		}

		protected override PresentationControls.NableMenuButton[] CustomMenuButtons
		{
			get
			{
				return new PresentationControls.NableMenuButton[]{	new PresentationControls.NableMenuButton("buttonEntityDetails", "Entity Details", "entityDetailsCommand", true ),
																	 new PresentationControls.NableMenuButton("buttonSecurity", "Security", "securityCommand", true ),
				};
			}
		}

		protected override string SelectedMenuButton
		{
			get
			{
				return "buttonEntityDetails";
			}
		}

		protected override MenuEvent[] CustomMenuCommands
		{
			get
			{
				return new MenuEvent[]{  new MenuEvent( "entityDetailsCommand", new CommandEventHandler( entityDetailsCommand_Click ),true ),
										  new MenuEvent( "securityCommand", new CommandEventHandler( securityCommand_Click ),true )
									  };
			}
		}

		protected override bool AdministratorRequired
		{
			get
			{
				return true;
			}
		}

		protected override bool ReadOnly
		{
			get
			{
				return true;
			}
		} 

		protected override string[]  EnabledControls ()
		{
			return new string[] {"buttonEntityDetails"};
		}
		
		#endregion
		
		#region Member methods
		private void entityDetailsCommand_Click( object sender, CommandEventArgs e )
		{
				
		}

		private void securityCommand_Click( object sender, CommandEventArgs e )
		{
			if(	this.Broker.SecuritySetting != 1 )
				SystemAdministratorOnlyActionAccess();

			this.AfterPostbackActionType = ActionType.RedirectToSpecificWorkpaper;
			this.SetRedirectActionPage("EntitySecurityConfiguration.aspx","Entity_1_1",true);
		}
		#endregion
	}
}
