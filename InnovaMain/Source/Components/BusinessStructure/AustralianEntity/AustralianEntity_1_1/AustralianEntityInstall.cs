using System;

namespace Oritax.TaxSimp.CM.Entity.AustralianEntity
{
	/// <summary>
	/// Summary description for AustralianEntityInstall.
	/// </summary>
	public class AustralianEntityInstall
	{
		#region INSTALLATION PROPERTIES

		// Assembly Installation Properties
		public const string ASSEMBLY_ID="50AB0A1F-12DA-4ECD-862F-7472838E3B95";
		public const string ASSEMBLY_NAME="AustralianEntity_1_1";
		public const string ASSEMBLY_DISPLAYNAME="Australian entity V1.1";
		public const string ASSEMBLY_MAJORVERSION="1";
		public const string ASSEMBLY_MINORVERSION="1";
		public const string ASSEMBLY_DATAFORMAT="0";
		//public const string ASSEMBLY_REVISION="0";	//2004.3.1
		//public const string ASSEMBLY_REVISION="1";	//2004.3.2

		public const string ASSEMBLY_REVISION="2";		//2005.1

		// Component Installation Properties
		public const string COMPONENT_ID="0AA37352-3610-4ebb-9C15-856F4E2B7E2A";
		public const string COMPONENT_NAME="AustralianEntity";
		public const string COMPONENT_DISPLAYNAME="Australian entity";
		public const string COMPONENT_CATEGORY="BusinessStructure";

		// Component Version Installation Properties
		public const string COMPONENTVERSION_STARTDATE="01/01/1990 00:00:00 AM";
		public const string COMPONENTVERSION_ENDDATE="31/12/2050 12:59:59 AM";
		public const string COMPONENTVERSION_PERSISTERASSEMBLY="CommonPersisters_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";
		public const string COMPONENTVERSION_PERSISTERCLASS="Oritax.TaxSimp.CommonPersistence.BlobPersister";
		public const string COMPONENTVERSION_IMPLEMENTATIONCLASS="Oritax.TaxSimp.CM.Entity.AustralianEntity.AustralianEntityCM";

		#endregion
	}
}
