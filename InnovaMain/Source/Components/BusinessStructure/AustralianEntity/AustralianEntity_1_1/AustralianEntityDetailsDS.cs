#region CHANGE_HISTORY
/*
 $Header: /2002/3. Implementation/NewConstruction/Components/BusinessStructure/AustralianEntity/AustralianEntity-1-0/AustralianEntityDetails $
 $History: AustralianEntityDetailsDS.cs $
 * 
 * *****************  Version 5  *****************
 * User: Sireland     Date: 1/09/03    Time: 11:00a
 * Updated in $/2002/3. Implementation/NewConstruction/Components/BusinessStructure/AustralianEntity/AustralianEntity-1-0
 * Added TaxStatusType as common functionality
 * 
 * *****************  Version 4  *****************
 * User: Pveitch      Date: 11/06/03   Time: 5:16p
 * Updated in $/2002/3. Implementation/Construction/BusinessStructureCM/AustralianEntity/AustralianEntity-1-0
 * Updates to the business structure to support the new use cases, as well
 * as updates to WorkpaperBase to handle multi-user and transaction
 * failures, refactored to make it more reliable and consistent
 * 
 * *****************  Version 3  *****************
 * User: Sireland     Date: 15/04/03   Time: 11:38a
 * Updated in $/2002/3. Implementation/Construction/BusinessStructureCM/AustralianEntity/AustralianEntity-1-0
 * Remove Warnings & Multiuser
 * 
 * *****************  Version 2  *****************
 * User: Pveitch      Date: 21/02/03   Time: 11:36a
 * Updated in $/2002/3. Implementation/Construction/BusinessStructureCM/AustralianEntity/AustralianEntity-1-0
 * 
 * *****************  Version 1  *****************
 * User: Sshrimpton   Date: 18/12/02   Time: 4:56p
 * Created in $/2002/3. Implementation/Construction/BusinessStructureCM/AustralianEntity/AustralianEntity-1-1-1
 * 
 * *****************  Version 2  *****************
 * User: Secampbell   Date: 7/11/02    Time: 11:59a
 * Updated in $/2002/3. Implementation/Elaboration4/CM/AustralianEntity
 * Added Change History Header
*/
#endregion

using System;
using System.Data;

using Oritax.TaxSimp.AustralianBusinessUtilities;
using Oritax.TaxSimp.BusinessStructureUtilities;


namespace Oritax.TaxSimp.CM.Entity.AustralianEntity
{
	/// <summary>
	/// DSCalculationModule is a DataSet that contains all of the persistent data for a CM instance.
	/// </summary>
	[Serializable] 
	public class AustralianEntityDetailsDS : EntityDetailsDS
	{
		public const String SAP_TABLE = "SAP_TABLE";
		private TaxStatusType m_objTaxStatus;

		public AustralianEntityDetailsDS()
			: base()
		{
			m_objTaxStatus = TaxStatusType.Default;
			DataTable table = new DataTable(SAP_TABLE);
			this.Tables.Add(table);
		}

		public TaxStatusType TaxStatus
		{
			get
			{
				return this.m_objTaxStatus;
			}
			set
			{
				this.m_objTaxStatus = value;
			}
		}
	}
}

