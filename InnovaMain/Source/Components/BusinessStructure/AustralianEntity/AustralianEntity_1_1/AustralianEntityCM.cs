#region CHANGE_HISTORY
/*
 $Header: /2002/3. Implementation/NewConstruction/Components/BusinessStructure/AustralianEntity/AustralianEntity-1-0/AustralianEntityCM.cs 1 $
 $History: AustralianEntityCM.cs $
 * 
 * *****************  Version 15  *****************
 * User: Paubailey    Date: 2/10/03    Time: 11:27a
 * Updated in $/2002/3. Implementation/NewConstruction/Components/BusinessStructure/AustralianEntity/AustralianEntity-1-0
 * See CQ issue 2576
 * 
 * *****************  Version 14  *****************
 * User: Studavies    Date: 3/09/03    Time: 11:28a
 * Updated in $/2002/3. Implementation/NewConstruction/Components/BusinessStructure/AustralianEntity/AustralianEntity-1-0
 * 
 * *****************  Version 13  *****************
 * User: Sireland     Date: 1/09/03    Time: 11:00a
 * Updated in $/2002/3. Implementation/NewConstruction/Components/BusinessStructure/AustralianEntity/AustralianEntity-1-0
 * Added TaxStatusType as common functionality
 * 
 * *****************  Version 12  *****************
 * User: Sireland     Date: 29/08/03   Time: 3:35p
 * Updated in $/2002/3. Implementation/NewConstruction/Components/BusinessStructure/AustralianEntity/AustralianEntity-1-0
 * 
 * *****************  Version 11  *****************
 * User: Sireland     Date: 28/08/03   Time: 1:08p
 * Updated in $/2002/3. Implementation/NewConstruction/Components/BusinessStructure/AustralianEntity/AustralianEntity-1-0
 * Implements Custom Serialization
 * 
 * *****************  Version 10  *****************
 * User: Sireland     Date: 12/08/03   Time: 5:40p
 * Updated in $/2002/3. Implementation/NewConstruction/Components/BusinessStructure/AustralianEntity/AustralianEntity-1-0
 * Added Data Migration Support
 * 
 * *****************  Version 9  *****************
 * User: Pveitch      Date: 19/06/03   Time: 2:10p
 * Updated in $/2002/3. Implementation/NewConstruction/Components/BusinessStructure/AustralianEntity/AustralianEntity-1-0
 * Updates for AustralianEntity use case
 * 
 * *****************  Version 8  *****************
 * User: Pveitch      Date: 11/06/03   Time: 5:16p
 * Updated in $/2002/3. Implementation/Construction/BusinessStructureCM/AustralianEntity/AustralianEntity-1-0
 * Updates to the business structure to support the new use cases, as well
 * as updates to WorkpaperBase to handle multi-user and transaction
 * failures, refactored to make it more reliable and consistent
 * 
 * *****************  Version 7  *****************
 * User: Paubailey    Date: 17/04/03   Time: 2:44p
 * Updated in $/2002/3. Implementation/Construction/BusinessStructureCM/AustralianEntity/AustralianEntity-1-0
 * 
 * *****************  Version 6  *****************
 * User: Sireland     Date: 15/04/03   Time: 11:38a
 * Updated in $/2002/3. Implementation/Construction/BusinessStructureCM/AustralianEntity/AustralianEntity-1-0
 * Remove Warnings & Multiuser
 * 
 * *****************  Version 5  *****************
 * User: Pveitch      Date: 13/03/03   Time: 5:09p
 * Updated in $/2002/3. Implementation/Construction/BusinessStructureCM/AustralianEntity/AustralianEntity-1-0
 * 
 * *****************  Version 4  *****************
 * User: Pveitch      Date: 25/02/03   Time: 9:36a
 * Updated in $/2002/3. Implementation/Construction/BusinessStructureCM/AustralianEntity/AustralianEntity-1-0
 * 
 * *****************  Version 3  *****************
 * User: Pveitch      Date: 21/02/03   Time: 11:36a
 * Updated in $/2002/3. Implementation/Construction/BusinessStructureCM/AustralianEntity/AustralianEntity-1-0
 * 
 * *****************  Version 2  *****************
 * User: Sshrimpton   Date: 29/01/03   Time: 2:11p
 * Updated in $/2002/3. Implementation/Construction/BusinessStructureCM/AustralianEntity/AustralianEntity-1-1-1
 * 
 * *****************  Version 1  *****************
 * User: Sshrimpton   Date: 18/12/02   Time: 4:56p
 * Created in $/2002/3. Implementation/Construction/BusinessStructureCM/AustralianEntity/AustralianEntity-1-1-1
 * 
 * *****************  Version 4  *****************
 * User: Pveitch      Date: 5/12/02    Time: 3:09p
 * Updated in $/2002/3. Implementation/Elaboration4/CM/AustralianEntity
 * Changes for Substituted Accounting Periods
 * 
 * *****************  Version 3  *****************
 * User: Pveitch      Date: 29/11/02   Time: 4:13p
 * Updated in $/2002/3. Implementation/Elaboration4/CM/AustralianEntity
 * 
 * *****************  Version 2  *****************
 * User: Secampbell   Date: 7/11/02    Time: 11:59a
 * Updated in $/2002/3. Implementation/Elaboration4/CM/AustralianEntity
 * Added Change History Header
*/
#endregion

using System;
using System.Data;
using System.Collections;
using System.Reflection;
using System.Runtime.Serialization;
using System.Security.Permissions ;
using System.Web.UI.WebControls;

using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.DSCalculation;
using Oritax.TaxSimp.CM.Entity;
using Oritax.TaxSimp.CM;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.AustralianBusinessUtilities;
using Oritax.TaxSimp.BusinessStructureUtilities;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Exceptions;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.SAPBase;

namespace Oritax.TaxSimp.CM.Entity.AustralianEntity
{
	[Serializable]
	public abstract class AustralianEntityCM : EntityCM//, ISerializable
	{
		#region INSTALLATION PROPERTIES

		// Assembly Installation Properties
		public new const string ASSEMBLY_ID="50AB0A1F-12DA-4ECD-862F-7472838E3B95";
		public new const string ASSEMBLY_NAME="AustralianEntity";
		public new const string ASSEMBLY_STRONGNAME="AustralianEntity, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";
		public new const string ASSEMBLY_MAJORVERSION="1";
		public new const string ASSEMBLY_MINORVERSION="1";
		public new const string ASSEMBLY_DATAFORMAT="0";
		public new const string ASSEMBLY_REVISION="0";

		// Component Installation Properties
		public new const string COMPONENT_ID="50AB0A1F-12DA-4ECD-862F-7472838E3B95";
		public new const string COMPONENT_NAME="AustralianEntity";
		public new const string COMPONENT_DISPLAYNAME="Australian entity";
		public new const string COMPONENT_CATEGORY="BusinessStructure";

		// Component Version Installation Properties
		public new const string COMPONENTVERSION_ID="50AB0A1F-12DA-4ECD-862F-7472838E3B95";
		public new const string COMPONENTVERSION_VERSIONNAME="AustralianEntity";
		public new const string COMPONENTVERSION_STARTDATE="8/02/2004 3:01:36 PM";
		public new const string COMPONENTVERSION_ENDDATE="8/02/2004 3:01:36 PM";
		public new const string COMPONENTVERSION_PERSISTERASSEMBLYSTRONGNAME="CommonPersisters_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";
		public new const string COMPONENTVERSION_PERSISTERCLASS="Oritax.TaxSimp.CommonPersistence.BlobPersister";
		public new const string COMPONENTVERSION_IMPLEMENTATIONCLASS="Oritax.TaxSimp.CM.Entity.AustralianEntity.AustralianEntityCM";

		#endregion

		#region CONSTRUCTORS
		public AustralianEntityCM()
			: base()
		{
			m_objSubstitutedAccountingPeriods = new AustralianSubstitutedAccountingPeriods( );
			this.m_objTaxStatus = TaxStatusType.Default;
		}

		protected AustralianEntityCM(SerializationInfo si, StreamingContext context)
			:base(si,context)
		{
			m_objSubstitutedAccountingPeriods=(AustralianSubstitutedAccountingPeriods)Serialize.GetSerializedValue(si,"ae_m_objSubstitutedAccountingPeriods",typeof(AustralianSubstitutedAccountingPeriods),new AustralianSubstitutedAccountingPeriods());
			//m_objTaxStatus=(TaxStatusType)Serialize.GetSerializedValue(si,"ae_m_objTaxStatus",typeof(TaxStatusType),TaxStatusType.Default);
		}
	
		//Only allow the .NET Serialization core code to call this function
		[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]
		public override void GetObjectData(SerializationInfo si, StreamingContext context)
		{
			base.GetObjectData(si,context);
			Serialize.AddSerializedValue(si,"ae_m_objSubstitutedAccountingPeriods",m_objSubstitutedAccountingPeriods);
			//Serialize.AddSerializedValue(si,"ae_m_objTaxStatus",m_objTaxStatus);
		}

		#endregion
		#region STATEFUL VARIABLES

		//USING CUSTOM SERIALIZATION.
		//Still mark the fields as [NonSerialized] for your own infomation but this is ignored now by .NET
		//You must add your own serialzation code for each instance
		//See GetObjectData & Special Constructor
		AustralianSubstitutedAccountingPeriods m_objSubstitutedAccountingPeriods;
        //TaxStatusType m_objTaxStatus;

		#endregion
		#region PROPERTIES
		public override ISubstitutedAccountingPeriods SAPs
		{
			get
			{
				return (SubstitutedAccountingPeriods) this.m_objSubstitutedAccountingPeriods;
			}
		}

        //public TaxStatusType TaxStatus
        //{
        //    get
        //    {
        //        return this.m_objTaxStatus;
        //    }
        //    set
        //    {
        //        this.m_objTaxStatus = value;
        //    }
        //}

        //public override string TaxStatusName
        //{
        //    get
        //    {
        //        return this.TaxStatus.ToString();
        //    }
        //}


		
		#endregion
		#region OVERRIDE MEMBERS FOR CM BEHAVIOR
		public override void Initialize(String cmName)
		{
			base.Initialize(cmName);
		}

		#region OnGetData Methods
		public override DataSet PrimaryDataSet{get{return new AustralianEntityDS();}}

		protected void OnGetData( AustralianSubstitutedAccountingPeriodDetailsDS objData )
		{
			this.m_objSubstitutedAccountingPeriods.ExtractPresentationData( objData.Tables[ AustralianSubstitutedAccountingPeriodDetailsDS.SAP_TABLE ] );
			objData.BreadCrumb = ConstructBreadcrumb(); 
			objData.ParentIsGroup = false;
		}

		protected void OnGetData( AustralianEntityDetailsDS objData )
		{
			objData.TaxStatus = this.TaxStatus;
		}

		protected override void OnGetData(DataSet data )
		{
			base.OnGetData(data);
			if (data is AustralianEntityDS)
			{
				AustralianEntityDS objData = data as AustralianEntityDS;
				objData.TaxStatus = this.TaxStatus.ToInt();
				this.m_objSubstitutedAccountingPeriods.ExtractPresentationData( objData.Tables[ AustralianSubstitutedAccountingPeriodDetailsDS.SAP_TABLE ] );
			}
			else if (data is AustralianSubstitutedAccountingPeriodDetailsDS)
			{
				this.OnGetData((AustralianSubstitutedAccountingPeriodDetailsDS)data);
			}
			else if (data is AustralianEntityDetailsDS)
			{
				this.OnGetData((AustralianEntityDetailsDS)data);
			}
		}

		#endregion

		#region OnSetData Methods
		protected ModifiedState OnSetData( AustralianSubstitutedAccountingPeriodDetailsDS objData )
		{
			this.m_objSubstitutedAccountingPeriods.DeliverPresentationData( objData);//.Tables[ AustralianSubstitutedAccountingPeriodDetailsDS.SAP_TABLE ] );
			return ModifiedState.UNKNOWN;
		}

		protected ModifiedState OnSetData( AustralianEntityDetailsDS objEntityDetails )
		{
			// check the rule that an entity cannot have a status of 'not a member of a consolidated group'
			// and be a member of a group, if it is, change the tax status back to what it was
			// and throw an exception
			if ( objEntityDetails.TaxStatus == TaxStatusType.NotAMemberOfAConsolidatedGroup &&
				this.IsMemberOfGroup( ) )
			{
				objEntityDetails.TaxStatus = this.TaxStatus;
				throw new DisplayUserMessageException( new MessageBoxDefinition(MessageBoxDefinition.MSG_UPDATE_ENTITY_STATUS_FAILURE),false,MessageDisplayMethod.MainWindowError);
			}

			this.TaxStatus = objEntityDetails.TaxStatus;

			return ModifiedState.UNKNOWN;
		}

		protected override ModifiedState OnSetData(DataSet data)
		{
			base.OnSetData(data);
			if (data is AustralianEntityDS)
			{
				AustralianEntityDS objData = data as AustralianEntityDS;
				this.TaxStatus = objData.TaxStatus;
				this.m_objSubstitutedAccountingPeriods.DeliverPresentationData( objData.Tables[ AustralianSubstitutedAccountingPeriodDetailsDS.SAP_TABLE ]);
		
				return ModifiedState.MODIFIED;
			}
			else if (data is AustralianEntityDetailsDS)
			{
				return this.OnSetData((AustralianEntityDetailsDS)data);
			}
			else if (data is AustralianSubstitutedAccountingPeriodDetailsDS)
			{
				return this.OnSetData((AustralianSubstitutedAccountingPeriodDetailsDS)data);
			}
			return ModifiedState.MODIFIED;
		}

		#endregion
		#endregion
		
		#region Migration
		
		public override DataSet MigrateDataSet(DataSet data)
		{
			versionHistory.Add(AustralianEntityInstall.COMPONENTVERSION_IMPLEMENTATIONCLASS, AustralianEntityDS.GetDataSetVersion(data));

			DataSet baseDataset = base.MigrateDataSet(data);

			DataSet convData = AustralianEntityDS.ConvertV_1_0_3_0toV_1_1_0_0DataSet(data, data);
			//convData = ConvertV_XtoV_YDataSet(data, convData);

			if(convData == null)
				return null;

			AustralianEntityDS ds = new AustralianEntityDS();

			ds.Merge(convData.Tables[AustralianEntityDS.AUSTENT_TABLE]);
			ds.Merge(convData.Tables[AustralianSubstitutedAccountingPeriodDetailsDS.SAP_TABLE]);
			
			//merge in the base dataset
			ds.Merge(baseDataset);

			return ds;
		}

		/// <summary>
		/// notifies to the cm that the migration is completed
		/// </summary>
		public override void MigrationCompleted()
		{
			base.MigrationCompleted();
			versionHistory.Remove(AustralianEntityInstall.COMPONENTVERSION_IMPLEMENTATIONCLASS);
		}

		#endregion

		#region OTHER MEMBER FUNCTIONS
		#endregion
	}
}
