using System;
using System.Data;
using System.Data.SqlClient;

using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.AustralianBusinessUtilities;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.CM.Entity.AustralianEntity
{
	/// <summary>
	/// Summary description for AustralianEntityDS.
	/// </summary>
	[Serializable]
	public class AustralianEntityDS :  EntityDS
	{
		public const String AUSTENT_TABLE			= "AustralianEntityCM";

		public const String AUSTENTTAXSTATUS_FIELD	= "TAXSTATUS";

		public AustralianEntityDS() : base()
		{
			DataTable dt;

			dt = new DataTable( AUSTENT_TABLE );
			dt.Columns.Add( AUSTENTTAXSTATUS_FIELD, typeof( int ) );
			this.Tables.Add( dt );
			BrokerManagedComponentDS.AddVersionStamp(dt,typeof(AustralianEntityDS));

			AustralianSubstitutedAccountingPeriodDetailsDS asapDS = new AustralianSubstitutedAccountingPeriodDetailsDS();
			dt = asapDS.Tables[ AustralianSubstitutedAccountingPeriodDetailsDS.SAP_TABLE ];
			BrokerManagedComponentDS.AddVersionStamp(dt,typeof(AustralianSubstitutedAccountingPeriodDetailsDS));
			this.Merge(dt);
		}

		public int TaxStatus
		{
			set
			{

				if(this.Tables[AustralianEntityDS.AUSTENT_TABLE].Rows.Count>0)
				{
					DataRow row=this.Tables[AustralianEntityDS.AUSTENT_TABLE].Rows[0];
					row[AustralianEntityDS.AUSTENTTAXSTATUS_FIELD]=value;
				}
				else
				{
					DataRow row=this.Tables[AustralianEntityDS.AUSTENT_TABLE].NewRow();
					row[AustralianEntityDS.AUSTENTTAXSTATUS_FIELD]=value;
					this.Tables[AustralianEntityDS.AUSTENT_TABLE].Rows.Add(row);
				}
			}
			get
			{
				if(this.Tables[AustralianEntityDS.AUSTENT_TABLE].Rows.Count>0)
				{
					DataRow row=this.Tables[AustralianEntityDS.AUSTENT_TABLE].Rows[0];
					return (int)row[AustralianEntityDS.AUSTENTTAXSTATUS_FIELD];
				}
				else
					return 0;
			}
		}

		public static new string GetDataSetVersion(DataSet data)
		{
			return GetDataSetVersion(data, "AustralianEntityCM").ToString();
		}

		/// <summary>
		///		Convert a V1.0.3.0 dataset to a V1.1.0.0 dataset
		/// </summary>
		/// <param name="data">The orginal untampered dataset</param>
		/// <param name="convData">A converted dataset passed in from a previous conversion function</param>
		/// <returns>A converted dataset</returns>
		public new static DataSet ConvertV_1_0_3_0toV_1_1_0_0DataSet(DataSet data, DataSet convData)
		{
			if(convData == null)
				return null;

			string ver = GetDataSetVersion(convData);

			//if not the appropriate verion for correction then just return back
			if(ver != "1.0.3.0")
				return convData;
			
			//the conversion is merely changing the full name to the correct version number.
			ver = "1.1.0.0";

			DataSet ds = new DataSet();
			DataTable table;

			table = new DataTable( "AustralianEntityCM" );
			table.Columns.Add( "TAXSTATUS", typeof( int ) );
			ds.Tables.Add( table );

			table = new DataTable( "SAP" );
			table.Columns.Add( "STARTDATE", typeof( DateTime ) );
			table.Columns.Add( "ENDDATE", typeof( DateTime ) );
			table.Columns.Add( "SHARED", typeof( bool ) );
			table.Columns.Add( "KEY", typeof( Guid ) );
			table.Columns.Add( "BALANCETYPE", typeof( AustralianEntityBalancingType ) );
			ds.Tables.Add( table );
			
			ds.EnforceConstraints = false;
			ds.Merge(convData, true, MissingSchemaAction.Ignore);

			//Perform Conversions
			//None
			ds.EnforceConstraints = true;

			ds.Tables["AustralianEntityCM"].ExtendedProperties["FullName"] = ver;
			ds.Tables["SAP"].ExtendedProperties["FullName"] = ver;
			
			return ds;
		}
	}
}
