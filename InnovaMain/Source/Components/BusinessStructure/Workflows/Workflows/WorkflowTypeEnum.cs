﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Oritax.TaxSimp.SM.Workflows.Data
{
    public enum WorkflowTypeEnum
    {
        [Description("None")]
        None = 0,
        [Description("Annual Report")]
        AnnualReport = 1,
        [Description("MDA")]
        MDA = 2,
        [Description("UMA Account: Change of Address")]
        UMAAccountChangeOfAddress = 3,
        [Description("UMA Account: Add New")]
        UMAAccountAddNew = 4,
        [Description("UMA Account: Update")]
        UMAAccountUpdate = 5,
        [Description("UMA Account: Deactivate")]
        UMAAccountDeactivate = 6
    }
}
