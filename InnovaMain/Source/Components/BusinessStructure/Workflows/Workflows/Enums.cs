﻿using System.ComponentModel;

namespace Oritax.TaxSimp.SM.Workflows
{
    public enum ActionStatus
    {
        [Description("Active")]
        Active = 0,
        [Description("Completed")]
        Completed = 1
    }

    public enum ActionType
    {
        [Description("None")]
        None = 0,
        [Description("MDA")]
        MDA = 1
    }

    public enum CreationMode
    {
        [Description("SystemGenerated")]
        SystemGenerated = 0
    }

    public enum MDAActionTypeDetail
    {
        [Description("0")]
        None = 0,
        [Description("5")]
        FirstAlert = 5,
        [Description("10")]
        SecondAlert = 10,
        [Description("15")]
        ThirdAlert = 15,
        [Description("20")]
        FourthAlert = 20,
        [Description("25")]
        Custom = 25
    }

}
