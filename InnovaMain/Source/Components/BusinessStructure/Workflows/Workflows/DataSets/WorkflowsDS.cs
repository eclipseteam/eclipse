﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.DataSets;

namespace Oritax.TaxSimp.SM.Workflows
{
    public class WorkflowsDS : UMABaseDS
    {
        public const string WorkflowsTable = "Workflows";
        public const string WorkflowStatusTable = "WorkflowStatus";
        public const string WorkflowKeyValuesTable = "WorkflowKeyValues";
        public const string NotificationsTable = "Notifications";
        public const string ActionsTable = "Actions";
        
        public Guid WorkflowID = Guid.Empty;
        public Workflow WorkflowToAdd = null;
        public List<Notification> NotificationListToAdd = null;
        public List<Action> ActionListToAdd = null;
        

        public DataRow RecordToUpdate = null;
        public DataTable RecordsToUpdate = null;
        //Use above constant for table name. Better to use Enums
        public string TableToUpdate = null;
    }

    public class ActionsTable
    {
        // The name of columns should be same as the property names in Action Type
        public const string ACTIONID_FIELD = "ActionID";
        public const string ACTIONTYPE_FIELD = "ActionType";
        public const string CREATEDBY_FIELD = "CreatedBy";
        public const string CREATIONDATE_FIELD = "CreationDate";
        public const string CREATIONMODE_FIELD = "CreationMode";
        public const string DUEDATE_FIELD = "DueDate";
        public const string EXPIRYDATE_FIELD = "ExpiryDate";
        public const string ISEXPIRED_FIELD = "IsExpired";
        public const string MESSAGE_FIELD = "Message";
        public const string NOTIFICATIONID_FIELD = "NotificationID";
        public const string STATUS_FIELD = "Status";
        public const string UPDATEDBY_FIELD = "UpdatedBy";
        public const string UPDATEDDATE_FIELD = "UpdatedDate";
        public const string ACTIONTYPEDETAIL_FIELD = "ActionTypeDetail";
        public const string ISVISIBLETOCLIENT_FIELD = "IsVisibleToClient";
        public const string ISVISIBLETOADVISER_FIELD = "IsVisibleToAdviser";
        public const string ISVISIBLETOADMIN_FIELD = "IsVisibleToAdmin";


        private ActionsTable()
        {

        }

    }

    public class NotificationsTable
    {
        public const string NOTIFICATIONID_FIELD = "NotificationID";
        public const string ATTACHMENTID_FIELD = "AttachmentID";
        public const string CLIENTCID_FIELD = "ClientCID";
        public const string CLIENTID_FIELD = "ClientID";
        public const string CLIENTNAME_FIELD = "ClientName";
        public const string CREATEDDATE_FIELD = "CreatedDate";
        public const string FILEDOWNLOADEDURLDOWNLOAD_FIELD = "FileDownLoadURLDownload";
        public const string MODIFIFIEDDATE_FIELD = "ModifiedDate";
        public const string NOTIFICATIONDESCRIPTION_FIELD = "NotificationDescription";
        public const string NOTIFICATIONDETAILS_FIELD = "NotificationDetails";
        public const string NOTIFICATIONNAME_FIELD = "NotificationName";
        public const string NOTIFICATIONTYPE_FIELD = "NotificationType";
        public const string WORKFLOWID_FIELD = "WorkflowID";

        private NotificationsTable()
        {

        }

    }


    public class WorkflowsTable
    {
        public const string WORKFLOWID_FIELD = "WorkflowID";
        public const string WORKFLOWDESC_FIELD = "WorkflowDesc";
        public const string WORKFLOWNAME_FIELD = "WorkflowName";
        public const string CREATEDBY_FIELD = "CreatedBy";
        public const string COMMENTS_FIELD = "Comments";
        public const string WORKFLOWTYPE_FIELD = "WorkflowType";
        public const string CREATEDDATE_FIELD = "CreatedDate";
        public const string LASTRUNDATE_FIELD = "LastRunDate";

        private WorkflowsTable()
        {

        }

    }
  

}
