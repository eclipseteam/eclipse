﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.SM.Workflows.Data;

namespace Oritax.TaxSimp.SM.Workflows
{
    public class ExecuteWorkFlowDS : UMABaseDS
    {
        public Guid WorkflowID = Guid.Empty;
        public WorkflowTypeEnum WorkflowType = WorkflowTypeEnum.None;
        public int ClientCount { get; set; }
        public string ClientIDs { get; set; }
    }
}
