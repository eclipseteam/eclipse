using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;

using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Persistence;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.MapTarget;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.Workflows
{
    public class WorkflowsPersister : BrokerManagedPersister
    {
        public WorkflowsPersister(SqlConnection connection, SqlTransaction transaction) : base(connection, transaction) { }

        public override DataTable ListBMCCollection(CollectionSpecifier bMCCollectionSpecifier)
        {
            WorkflowsAllCollectionSpecifier eventLogAllCollectionSpecifier = (WorkflowsAllCollectionSpecifier)bMCCollectionSpecifier;

            string selectCommand = "SELECT * FROM Worflows order by Worflows.CreatedDate DESC";

            DataTable results = new DataTable();

            SqlDataAdapter sda = new SqlDataAdapter(selectCommand, this.Connection);
            sda.SelectCommand.Transaction = this.Transaction;
            sda.SelectCommand.CommandType = CommandType.Text;
            sda.Fill(results);
            return results;
        }
    }
}
