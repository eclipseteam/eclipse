﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Oritax.TaxSimp.SM.Workflows.Data
{
    public enum NotificationType
    {
        [Description("None")]
        None = 0,
        [Description("Annual Report Generated")]
        AnnualReportGenerated = 1,
        [Description("MDA")]
        MDA = 2
    }
}
