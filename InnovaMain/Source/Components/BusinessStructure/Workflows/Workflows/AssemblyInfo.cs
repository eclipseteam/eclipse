using System.Reflection;
using System.Runtime.CompilerServices;

using Oritax.TaxSimp.InstallInfo;
using Oritax.TaxSimp.EventLog;

//



//
[assembly: AssemblyTitle("")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("")]
[assembly: AssemblyCopyright("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]		

[assembly: AssemblyVersion(WorkflowsInstall.ASSEMBLY_MAJORVERSION+"."+WorkflowsInstall.ASSEMBLY_MINORVERSION)]
[assembly: AssemblyInformationalVersion(WorkflowsInstall.ASSEMBLY_MAJORVERSION+"."+WorkflowsInstall.ASSEMBLY_MINORVERSION+"."+WorkflowsInstall.ASSEMBLY_DATAFORMAT+"."+WorkflowsInstall.ASSEMBLY_REVISION)]
[assembly: AssemblyInstallInfo(WorkflowsInstall.ASSEMBLY_ID,WorkflowsInstall.ASSEMBLY_NAME,WorkflowsInstall.ASSEMBLY_DISPLAYNAME)]
[assembly: ComponentInstallInfo(WorkflowsInstall.COMPONENT_ID,WorkflowsInstall.COMPONENT_NAME,WorkflowsInstall.COMPONENT_DISPLAYNAME,WorkflowsInstall.COMPONENT_CATEGORY)]
[assembly: ComponentVersionInstallInfo(WorkflowsInstall.COMPONENTVERSION_STARTDATE,WorkflowsInstall.COMPONENTVERSION_ENDDATE,WorkflowsInstall.COMPONENTVERSION_PERSISTERASSEMBLY,WorkflowsInstall.COMPONENTVERSION_PERSISTERCLASS,WorkflowsInstall.COMPONENTVERSION_IMPLEMENTATIONCLASS)]
[assembly: DataModelInstallInfo(WorkflowsInstall.ASSEMBLY_PERSISTERASSEMBLY,WorkflowsInstall.ASSEMBLY_PERSISTERCLASS)]

//


//
// Use the attributes below to control which key is used for signing. 
//
// Notes: 
//   (*) If no key is specified, the assembly is not signed.
//   (*) KeyName refers to a key that has been installed in the Crypto Service
//       Provider (CSP) on your machine. KeyFile refers to a file which contains
//       a key.
//   (*) If the KeyFile and the KeyName values are both specified, the 
//       following processing occurs:
//       (1) If the KeyName can be found in the CSP, that key is used.
//       (2) If the KeyName does not exist and the KeyFile does exist, the key 
//           in the KeyFile is installed into the CSP and used.
//   (*) In order to create a KeyFile, you can use the sn.exe (Strong Name) utility.
//        When specifying the KeyFile, the location of the KeyFile should be
//        relative to the "project output directory". The location of the project output
//        directory is dependent on whether you are working with a local or web project.
//        For local projects, the project output directory is defined as
//       <Project Directory>\obj\<Configuration>. For example, if your KeyFile is
//       located in the project directory, you would specify the AssemblyKeyFile 
//       attribute as [assembly: AssemblyKeyFile("..\\..\\mykey.snk")]
//        For web projects, the project output directory is defined as
//       %HOMEPATH%\VSWebCache\<Machine Name>\<Project Directory>\obj\<Configuration>.
//   (*) Delay Signing is an advanced option - see the Microsoft .NET Framework
//       documentation for more information on this.
//
[assembly:AssemblyDelaySignAttribute(false)]

