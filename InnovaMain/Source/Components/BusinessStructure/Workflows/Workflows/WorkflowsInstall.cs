using System;

namespace Oritax.TaxSimp.EventLog
{
	/// <summary>
	/// Summary description for Install.
	/// </summary>
    public class WorkflowsInstall
	{
		#region INSTALLATION PROPERTIES

		// Assembly Installation Properties
        public const string ASSEMBLY_ID = "15DE2644-C1A0-41D5-8293-2794EF31840D";
        public const string ASSEMBLY_NAME = "Workflows_1_1";
        public const string ASSEMBLY_DISPLAYNAME = "Workflows";
		public const string ASSEMBLY_MAJORVERSION="1";
		public const string ASSEMBLY_MINORVERSION="1";
		public const string ASSEMBLY_DATAFORMAT="0";
		public const string ASSEMBLY_REVISION="0";

        public const string ASSEMBLY_PERSISTERASSEMBLY = "Workflows_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";
        public const string ASSEMBLY_PERSISTERCLASS = "Oritax.TaxSimp.EventLog.ActionsPersister";

		// Component Installation Properties
        public const string COMPONENT_ID = "B5B11EF9-15D5-4FD0-92C6-58F7CD7E3BE3";
        public const string COMPONENT_NAME = "Workflows";
        public const string COMPONENT_DISPLAYNAME = "Workflows";
		public const string COMPONENT_CATEGORY="Framework";

		// Component Version Installation Properties
		public const string COMPONENTVERSION_STARTDATE="01/01/1990 00:00:00 AM";
		public const string COMPONENTVERSION_ENDDATE="31/12/2050 12:59:59 AM";
        public const string COMPONENTVERSION_PERSISTERASSEMBLY = "Workflows_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";
        public const string COMPONENTVERSION_PERSISTERCLASS = "Oritax.TaxSimp.Workflows.WorkflowsPersister";
        public const string COMPONENTVERSION_IMPLEMENTATIONCLASS = "Oritax.TaxSimp.Workflows.WorkflowsSM";

		#endregion
	}
}
