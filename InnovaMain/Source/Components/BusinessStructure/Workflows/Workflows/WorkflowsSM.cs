using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Linq;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.MapTarget;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.SM.Workflows;
using Oritax.TaxSimp.SM.Workflows.Data;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.Organization.Data;
using Oritax.TaxSimp.CM.OrganizationUnit;
using C1.C1Preview;
using System.Drawing;
using Oritax.TaxSimp.CM;
using Oritax.TaxSimp.Security;
using Oritax.TaxSimp.Utilities;
using ActionType = Oritax.TaxSimp.SM.Workflows.ActionType;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aspose.Words.Tables;
using Telerik.Web.UI;
using OrganizationUnit = Oritax.TaxSimp.Data.OrganizationUnit;
using System.Web;
using Aspose.Words;
using System.Reflection;

namespace Oritax.TaxSimp.Workflows
{
    [Serializable]
    public partial class WorkflowsSM : CalculationModuleBase
    {
        #region CONSTRUCTOR
        //string workflowConnectionString== "metadata=res://*/Workflows.csdl|res://*/Workflows.ssdl|res://*/Workflows.msl;provider=System.Data.SqlClient;provider connection string='Data Source=111.67.4.135,1433;Initial Catalog=Innova2011;User ID=sa;Password=$Admin#1'";
        string _workflowMetaData = "metadata=res://*/Workflows.csdl|res://*/Workflows.ssdl|res://*/Workflows.msl;";
        private string workflowConnectionString
        {
            get
            {
                System.Data.Common.DbConnectionStringBuilder builder = new System.Data.Common.DbConnectionStringBuilder();

                builder.ConnectionString = Utilities.DBConnection.Connection.ConnectionString;

                return _workflowMetaData + string.Format("provider=System.Data.SqlClient;provider connection string='Data Source={0};Initial Catalog={1};User ID={2};Password={3}'", builder["server"], builder["Database"], builder["uid"], builder["pwd"]);
            }
        }

        public WorkflowsSM()
            : base()
        {

        }

        public WorkflowsSM(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {

        }

        #endregion

        #region OVERRIDE METHODS
        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }

        protected override void OnGetData(DataSet data)
        {
            if (data is WorkflowsDS)
            {
                #region Workflow routine
                using (var workflowsEntities = new WorkflowsEntities(workflowConnectionString))
                {
                    DataTable workflowTable = workflowsEntities.Workflows.ToDataTable();
                    workflowTable.TableName = WorkflowsDS.WorkflowsTable;
                    data.Tables.Add(workflowTable);

                    DataTable workflowStatusTable = workflowsEntities.WorkflowStatus.ToDataTable();
                    workflowStatusTable.TableName = WorkflowsDS.WorkflowStatusTable;
                    data.Tables.Add(workflowStatusTable);

                    DataTable workflowKeyValuesTable = workflowsEntities.WorkflowKeyValues.ToDataTable();
                    workflowKeyValuesTable.TableName = WorkflowsDS.WorkflowKeyValuesTable;
                    data.Tables.Add(workflowKeyValuesTable);

                    DataTable notificationsTable = workflowsEntities.Notifications.ToDataTable();
                    notificationsTable.TableName = WorkflowsDS.NotificationsTable;
                    data.Tables.Add(notificationsTable);

                    DataTable actionTable = workflowsEntities.Actions.ToDataTable();
                    actionTable.TableName = WorkflowsDS.ActionsTable;
                    data.Tables.Add(actionTable);
                }   
                #endregion
            }

            if (data is MDAAlertDS)
            {
                #region MDAAlertDS routine
                using (var workflowsEntities = new WorkflowsEntities(workflowConnectionString))
                {
                    DataTable workflowTable = workflowsEntities.Workflows.ToDataTable();
                    workflowTable.TableName = WorkflowsDS.WorkflowsTable;

                    DataTable notificationsTable = workflowsEntities.Notifications.ToDataTable();
                    notificationsTable.TableName = WorkflowsDS.NotificationsTable;

                    DataTable actionTable = workflowsEntities.Actions.ToDataTable();
                    actionTable.TableName = WorkflowsDS.ActionsTable;
                    
                    var workflows = workflowTable.AsEnumerable().Where
                        (w => (WorkflowTypeEnum)int.Parse(w["WorkflowType"].ToString()) == WorkflowTypeEnum.MDA);
                    if (!workflows.Any())
                        return;

                    string wfID = workflows.First()["WorkflowID"].ToString();
                    var notifications = notificationsTable.AsEnumerable().Where
                        (n => n["WorkflowID"].ToString() == wfID);

                    //var configurations = configurationTable.AsEnumerable();

                    var DSInput = data as MDAAlertDS;
                    DataTable clientDetailTable = null;
                    DataTable attachmentdt = null;
                    if (DSInput.Unit != null)
                    {
                        var user = DSInput.Unit.CurrentUser;
                        //Get All client data that is needed for Alert Functionality (filtered applied)
                        if (!user.IsAdmin)
                        {

                            clientDetailTable = this.GetClientMDAData(user, 0, string.Empty, out attachmentdt);

                        }
                        else
                        {
                            //
                            var adminalerts = from notification in notifications
                                              join action in actionTable.AsEnumerable()
                                              on notification[NotificationsTable.NOTIFICATIONID_FIELD] equals action[ActionsTable.NOTIFICATIONID_FIELD]
                                              select
                                              new
                                              {
                                                  ClientCID = (Guid)notification[NotificationsTable.CLIENTCID_FIELD],
                                                  IsExpired = action[ActionsTable.ISEXPIRED_FIELD],
                                              };
                            adminalerts = adminalerts.Where(a => a.IsExpired.ToString().ToLower() == "false");

                            var adminalertsgroup = adminalerts.GroupBy(g => g.ClientCID);
                            MDAAlertDS ds = new MDAAlertDS();
                            foreach (var r in adminalertsgroup)
                            {

                                IOrganizationUnit clientData = this.Broker.GetBMCInstance(r.Key) as IOrganizationUnit;
                                clientData.GetData(ds);

                            }
                            clientDetailTable = ds.ClientDetailTable.Copy();
                            if (ds.Tables.Contains(AttachmentListDS.ATTACHMENTTABLE))
                                attachmentdt = ds.Tables[AttachmentListDS.ATTACHMENTTABLE].Copy();
                        }
                    }
                    var alerts =
                        from notification in notifications
                        join action in actionTable.AsEnumerable()
                        on notification[NotificationsTable.NOTIFICATIONID_FIELD] equals action[ActionsTable.NOTIFICATIONID_FIELD]
                        join clientDetails in clientDetailTable.AsEnumerable()
                        on notification[NotificationsTable.CLIENTCID_FIELD] equals clientDetails[ClientDetailTable.CLIENTCID]
                        select
                        new
                        {
                            ActionID = action[ActionsTable.ACTIONID_FIELD],
                            ClientID = notification[NotificationsTable.CLIENTID_FIELD],
                            ClientCID = notification[NotificationsTable.CLIENTCID_FIELD],
                            ClientName = notification[NotificationsTable.CLIENTNAME_FIELD],
                            Status = action[ActionsTable.STATUS_FIELD].ToString(),
                            Event = action[ActionsTable.MESSAGE_FIELD].ToString(),
                            AdviserID = clientDetails[ClientDetailTable.ADVISERID],
                            AdviserName = clientDetails[ClientDetailTable.ADVISERNAME],
                            Service = clientDetails[ClientDetailTable.SERVICE],
                            DueDate = clientDetails[ClientDetailTable.MDAEXECUTIONDATE] != DBNull.Value && clientDetails[ClientDetailTable.MDAEXECUTIONDATE].ToString() != string.Empty ? Convert.ToDateTime(clientDetails[ClientDetailTable.MDAEXECUTIONDATE]).AddYears(1) : action[ActionsTable.DUEDATE_FIELD],
                            RaisedDate = action[ActionsTable.CREATIONDATE_FIELD],
                            IsExpired = action[ActionsTable.ISEXPIRED_FIELD],
                            AttachmentID = IsValidAttachmentID((Guid)notification[NotificationsTable.ATTACHMENTID_FIELD], attachmentdt) ? notification[NotificationsTable.ATTACHMENTID_FIELD] : Guid.Empty,
                            NotificationID = notification[NotificationsTable.NOTIFICATIONID_FIELD],
                            FileDownLoadURLDownload = notification[NotificationsTable.FILEDOWNLOADEDURLDOWNLOAD_FIELD],
                            FileName = GetFileName((Guid)notification[NotificationsTable.ATTACHMENTID_FIELD], attachmentdt),
                            ActionTypeDetail = action[ActionsTable.ACTIONTYPEDETAIL_FIELD],
                            Attached = new Guid(notification[NotificationsTable.ATTACHMENTID_FIELD].ToString()) == Guid.Empty ? "Attach" : "Attached",
                            Completed = action[ActionsTable.STATUS_FIELD].ToString() == "Active" ? "Complete" : action[ActionsTable.STATUS_FIELD].ToString(),
                            ADVISERID = clientDetails[ClientDetailTable.ADVISERID].ToString(),
                            IFANAME = clientDetails[ClientDetailTable.IFANAME].ToString(),
                            DGNAME = clientDetails[ClientDetailTable.DGNAME].ToString(),
                            DGACN = clientDetails[ClientDetailTable.DGACN].ToString(),
                            DIFMPROGRAMCODE = clientDetails[ClientDetailTable.DIFMPROGRAMCODE].ToString(),
                            DIFMPROGRAMNAME = clientDetails[ClientDetailTable.DIFMPROGRAMNAME].ToString(),
                            MDAEXECUTIONDATE = clientDetails[ClientDetailTable.MDAEXECUTIONDATE].ToString(),
                            ADVISEREMAIL = clientDetails[ClientDetailTable.ADVISEREMAIL].ToString()

                        };

                    alerts = alerts.OrderBy(a => a.DueDate).ThenBy(a => a.ClientName)
                    .Where(a => a.IsExpired.ToString().ToLower() == "false");

                    DataTable alertTable = alerts.ToDataTable();
                    alertTable.TableName = MDAAlertDS.MDAALERTTABLE;

                    data.Tables.Add(alertTable);
                }
                #endregion
            }
            if (data is AlertConfigurationDS)
            {
                #region Alert Configuration
                using (var workflowsEntities = new WorkflowsEntities(workflowConnectionString))
                {
                    var MDAworkflow= workflowsEntities.Workflows.FirstOrDefault(ss => ss.WorkflowType ==(int) WorkflowTypeEnum.MDA);

                    if (MDAworkflow != null)
                    {
                       var ds=data as AlertConfigurationDS;

                        var wfID = MDAworkflow.WorkflowID;
                        //Configuration Table
                        var configurationTable = workflowsEntities.AlertConfigurations.ToDataTable();
                        
                        //Finding Rows for cid.
                        var configurationRows =
                            configurationTable.AsEnumerable().Where(rows => new Guid(rows[ds.configurationTable.CID].ToString()) == ds.CID);
                        
                        if(!configurationRows.Any())
                        {
                            //Finding Rows for parent.
                            configurationRows =
                                configurationTable.AsEnumerable().Where(rows => new Guid(rows[ds.configurationTable.CID].ToString()) == ds.ParentID);
                        }

                        if (!configurationRows.Any())
                        {
                            //Finding Rows for admin.
                            configurationRows =
                                configurationTable.AsEnumerable().Where(rows => new Guid(rows[ds.configurationTable.CID].ToString()) ==Guid.Empty);
                        }

                        switch (ds.SortOrderBy)
                        {
                          case SortOrder.Descending:
                                             configurationRows=configurationRows.OrderByDescending(rows => rows[ds.configurationTable.EXECUTIONDAYS]);
                                break;
                          default:
                                configurationRows =configurationRows.OrderBy(rows => rows[ds.configurationTable.EXECUTIONDAYS]);
                                break;
                        }


                        if (configurationRows.Any())
                            {
                                foreach (var row in configurationRows)
                                {
                                    ds.configurationTable.ImportRow(row);
                                }
                            }
                            else
                            {
                                ds.configurationTable.AddDefaultConfigurationRows(wfID, ds.CID);
                            }

                        //Filling Presentation Table (In Horizontal order)
                        ds.configurationPresentationTable.FillFromConfigurationTable(ds.configurationTable);
                      
                    }
                }
                #endregion
            }
        }

      
        public string FileName
        {
            get
            {
                return FileName;
            }
            set
            {
                FileName = value;
            }
        }

        private bool IsValidAttachmentID(Guid ID, DataTable attachmentdt)
        {
            bool result = false;

            if (attachmentdt != null)
            {
                var drs = attachmentdt.Select(string.Format("{0}='{1}'", AttachmentListDS.ATTACHMENTID_FIELD, ID));
                if (drs.Length > 0)
                {
                    result = true;
                }
            }

            return result;
        }

        private string GetFileName(Guid id, DataTable attachmentdt)
        {
            string fileName = string.Empty;
            if (attachmentdt != null)
            {
                var drs = attachmentdt.Select(string.Format("{0}='{1}'", AttachmentListDS.ATTACHMENTID_FIELD, id));
                if (drs.Length > 0)
                    fileName = drs[0][AttachmentListDS.ATTACHMENTORIGINPATH_FIELD].ToString();
                else
                    fileName = string.Empty;
            }
            return fileName;
        }

        protected override BrokerManagedComponent.ModifiedState OnSetData(DataSet data)
        {
            base.OnSetData(data);

            if (data is WorkflowsDS)
            {
                WorkflowsDS workflowsDS = (WorkflowsDS)data;
                if (workflowsDS.DataSetOperationType == DataSetOperationType.NewSingle)
                {
                    using (var workflowsEntities = new WorkflowsEntities(workflowConnectionString))
                    {
                        workflowsEntities.Workflows.AddObject(workflowsDS.WorkflowToAdd);
                        workflowsEntities.SaveChanges();
                    }
                }

                #region Delete
                if (workflowsDS.DataSetOperationType == DataSetOperationType.DeletSingle)
                {
                    using (var workflowsEntities = new WorkflowsEntities(workflowConnectionString))
                    {
                        var workFlowToDelete =
                            workflowsEntities.Workflows.Where(item => item.WorkflowID == workflowsDS.WorkflowID).FirstOrDefault();
                        if (workFlowToDelete != null)
                        {
                            var wfStatusList =
                                workflowsEntities.WorkflowStatus.Where(
                                    item => item.WorflowID == workFlowToDelete.WorkflowID).ToList();

                            foreach (var wfStatusItem in wfStatusList)
                                workflowsEntities.WorkflowStatus.DeleteObject(wfStatusItem);

                            var workflowKeyValuesList =
                                workflowsEntities.WorkflowKeyValues.Where(
                                    item => item.WorkflowID == workFlowToDelete.WorkflowID).ToList();

                            foreach (var wfKeyValueItem in workflowKeyValuesList)
                                workflowsEntities.WorkflowKeyValues.DeleteObject(wfKeyValueItem);

                            var notificationsList =
                                workflowsEntities.Notifications.Where(
                                    item => item.WorkflowID == workFlowToDelete.WorkflowID).ToList();

                            foreach (var notificationsListItem in notificationsList)
                            {
                                if (notificationsListItem.AttachmentID != Guid.Empty)
                                {
                                    Broker.SaveOverride = true;
                                    IBrokerManagedComponent organizationUnit =
                                        this.Broker.GetBMCInstance(notificationsListItem.ClientCID);
                                    AttachmentListDS attachmentListDS = new AttachmentListDS();
                                    organizationUnit.GetData(attachmentListDS);
                                    DataTable objAttachmentsTable =
                                        attachmentListDS.Tables[AttachmentListDS.ATTACHMENTTABLE];
                                    objAttachmentsTable.AcceptChanges();

                                    foreach (DataRow objRow in objAttachmentsTable.Rows)
                                    {
                                        if (objRow[AttachmentListDS.ATTACHMENTID_FIELD].ToString() ==
                                            notificationsListItem.AttachmentID.ToString())
                                            objRow.Delete();
                                    }

                                    organizationUnit.SetData(attachmentListDS);
                                }

                                workflowsEntities.Notifications.DeleteObject(notificationsListItem);
                            }

                            workflowsEntities.Workflows.DeleteObject(workFlowToDelete);
                            workflowsEntities.SaveChanges();
                        }
                    }
                }
                #endregion

                #region Actions
                if (workflowsDS.TableToUpdate == WorkflowsDS.ActionsTable)
                {
                    if (workflowsDS.DataSetOperationType == DataSetOperationType.UpdateSingle)
                    {
                        using (var workflowsEntities = new WorkflowsEntities(workflowConnectionString))
                        {
                            Guid actionID =
                                new Guid(workflowsDS.RecordToUpdate[ActionsTable.ACTIONID_FIELD].ToString());

                            Oritax.TaxSimp.SM.Workflows.Action action =
                                workflowsEntities.Actions.FirstOrDefault(a => a.ActionID == actionID);
                            if (action != null)
                            {
                                action.ActionType =
                                    workflowsDS.RecordToUpdate[ActionsTable.ACTIONTYPE_FIELD].ToString();
                                action.CreatedBy =
                                    workflowsDS.RecordToUpdate[ActionsTable.CREATEDBY_FIELD].ToString();
                                action.CreationDate =
                                    Convert.ToDateTime(workflowsDS.RecordToUpdate[ActionsTable.CREATIONDATE_FIELD]);
                                action.CreationMode =
                                    workflowsDS.RecordToUpdate[ActionsTable.CREATIONMODE_FIELD].ToString();
                                action.DueDate =
                                    Convert.ToDateTime(workflowsDS.RecordToUpdate[ActionsTable.DUEDATE_FIELD]);
                                action.ExpiryDate =
                                    Convert.ToDateTime(workflowsDS.RecordToUpdate[ActionsTable.EXPIRYDATE_FIELD]);
                                action.ActionTypeDetail =
                                    Convert.ToInt32(workflowsDS.RecordToUpdate[ActionsTable.ACTIONTYPEDETAIL_FIELD]);
                                action.IsVisibleToAdmin =
                                    Convert.ToBoolean(workflowsDS.RecordToUpdate[ActionsTable.ISVISIBLETOADMIN_FIELD]);
                                action.IsVisibleToAdviser =
                                    Convert.ToBoolean(workflowsDS.RecordToUpdate[ActionsTable.ISVISIBLETOADVISER_FIELD]);
                                action.IsVisibleToClient =
                                    Convert.ToBoolean(workflowsDS.RecordToUpdate[ActionsTable.ISVISIBLETOCLIENT_FIELD]);
                                action.IsExpired =
                                    Convert.ToBoolean(workflowsDS.RecordToUpdate[ActionsTable.ISEXPIRED_FIELD]);
                                action.Message = workflowsDS.RecordToUpdate[ActionsTable.MESSAGE_FIELD].ToString();
                                action.NotificationID =
                                    new Guid(
                                        workflowsDS.RecordToUpdate[ActionsTable.NOTIFICATIONID_FIELD].ToString());
                                action.Status = workflowsDS.RecordToUpdate[ActionsTable.STATUS_FIELD].ToString();
                                action.UpdatedBy =
                                    workflowsDS.RecordToUpdate[ActionsTable.UPDATEDBY_FIELD].ToString();
                                action.UpdatedDate = DateTime.Now;

                                workflowsEntities.SaveChanges();
                            }

                        }
                    }

                    if (workflowsDS.DataSetOperationType == DataSetOperationType.UpdateBulk)
                    {
                        using (var workflowsEntities = new WorkflowsEntities(workflowConnectionString))
                        {
                            foreach (DataRow row in workflowsDS.RecordsToUpdate.Rows)
                            {
                                Guid actionID =
                               new Guid(row[ActionsTable.ACTIONID_FIELD].ToString());

                                Oritax.TaxSimp.SM.Workflows.Action action =
                                    workflowsEntities.Actions.FirstOrDefault(a => a.ActionID == actionID);
                                if (action != null)
                                {
                                    action.ActionType =
                                        row[ActionsTable.ACTIONTYPE_FIELD].ToString();
                                    action.CreatedBy =
                                        row[ActionsTable.CREATEDBY_FIELD].ToString();
                                    action.CreationDate =
                                        Convert.ToDateTime(row[ActionsTable.CREATIONDATE_FIELD]);
                                    action.CreationMode =
                                        row[ActionsTable.CREATIONMODE_FIELD].ToString();
                                    action.DueDate =
                                        Convert.ToDateTime(row[ActionsTable.DUEDATE_FIELD]);
                                    action.ExpiryDate =
                                        Convert.ToDateTime(row[ActionsTable.EXPIRYDATE_FIELD]);
                                    action.ActionTypeDetail =
                                    Convert.ToInt32(row[ActionsTable.ACTIONTYPEDETAIL_FIELD]);
                                    action.IsVisibleToAdmin =
                                        Convert.ToBoolean(row[ActionsTable.ISVISIBLETOADMIN_FIELD]);
                                    action.IsVisibleToAdviser =
                                        Convert.ToBoolean(row[ActionsTable.ISVISIBLETOADVISER_FIELD]);
                                    action.IsVisibleToClient =
                                        Convert.ToBoolean(row[ActionsTable.ISVISIBLETOCLIENT_FIELD]);
                                    action.IsExpired =
                                        Convert.ToBoolean(row[ActionsTable.ISEXPIRED_FIELD]);
                                    action.Message = row[ActionsTable.MESSAGE_FIELD].ToString();
                                    action.NotificationID =
                                        new Guid(
                                            row[ActionsTable.NOTIFICATIONID_FIELD].ToString());
                                    action.Status = row[ActionsTable.STATUS_FIELD].ToString();
                                    action.UpdatedBy =
                                        row[ActionsTable.UPDATEDBY_FIELD].ToString();
                                    action.UpdatedDate = DateTime.Now;
                                }

                                workflowsEntities.SaveChanges();

                            }

                        }

                    }

                    if (workflowsDS.DataSetOperationType == DataSetOperationType.NewBulk)
                    {
                        using (var workflowsEntities = new WorkflowsEntities(workflowConnectionString))
                        {
                            foreach (var action in workflowsDS.ActionListToAdd)
                            {
                                workflowsEntities.Actions.AddObject(action);
                            }
                            workflowsEntities.SaveChanges();
                        }
                    }
                }
                #endregion

                #region Notifications
                if (workflowsDS.TableToUpdate == WorkflowsDS.NotificationsTable)
                {
                    if (workflowsDS.DataSetOperationType == DataSetOperationType.UpdateSingle)
                    {
                        using (var workflowsEntities = new WorkflowsEntities(workflowConnectionString))
                        {
                            Guid notificationID =
                                new Guid(
                                    workflowsDS.RecordToUpdate[NotificationsTable.NOTIFICATIONID_FIELD].ToString());
                            Notification notification =
                                workflowsEntities.Notifications.FirstOrDefault(
                                    a => a.NotificationID == notificationID);
                            if (notification != null)
                            {
                                notification.AttachmentID =
                                    new Guid(
                                        workflowsDS.RecordToUpdate[NotificationsTable.ATTACHMENTID_FIELD].ToString());
                                notification.ClientCID =
                                    new Guid(
                                        workflowsDS.RecordToUpdate[NotificationsTable.CLIENTCID_FIELD].ToString());
                                notification.ClientID =
                                    workflowsDS.RecordToUpdate[NotificationsTable.CLIENTID_FIELD].ToString();
                                notification.ClientName =
                                    workflowsDS.RecordToUpdate[NotificationsTable.CLIENTNAME_FIELD].ToString();
                                notification.CreatedDate =
                                    Convert.ToDateTime(
                                        workflowsDS.RecordToUpdate[NotificationsTable.CREATEDDATE_FIELD]);
                                notification.FileDownLoadURLDownload =
                                    workflowsDS.RecordToUpdate[NotificationsTable.FILEDOWNLOADEDURLDOWNLOAD_FIELD]
                                        .ToString();
                                notification.ModifiedDate =
                                    Convert.ToDateTime(
                                        workflowsDS.RecordToUpdate[NotificationsTable.MODIFIFIEDDATE_FIELD]);
                                notification.NotificationDescription =
                                    workflowsDS.RecordToUpdate[NotificationsTable.NOTIFICATIONDESCRIPTION_FIELD]
                                        .ToString();
                                notification.NotificationDetails =
                                    workflowsDS.RecordToUpdate[NotificationsTable.NOTIFICATIONDETAILS_FIELD]
                                        .ToString();
                                notification.NotificationName =
                                    workflowsDS.RecordToUpdate[NotificationsTable.NOTIFICATIONNAME_FIELD].ToString();
                                notification.NotificationType =
                                    Convert.ToInt32(
                                        workflowsDS.RecordToUpdate[NotificationsTable.NOTIFICATIONTYPE_FIELD]);
                                notification.WorkflowID =
                                    new Guid(
                                        workflowsDS.RecordToUpdate[NotificationsTable.WORKFLOWID_FIELD].ToString());
                                workflowsEntities.SaveChanges();
                            }

                        }
                    }

                    if (workflowsDS.DataSetOperationType == DataSetOperationType.NewBulk)
                    {
                        using (var workflowsEntities = new WorkflowsEntities(workflowConnectionString))
                        {
                            foreach (var notification in workflowsDS.NotificationListToAdd)
                            {
                                workflowsEntities.Notifications.AddObject(notification);
                            }
                            workflowsEntities.SaveChanges();
                        }
                    }
                }
                #endregion

               
            }
            if (data is AlertConfigurationDS)
            {
                //Alert Configuration Saving
                #region Alert Configuration
                var alertConfigurationDS = (AlertConfigurationDS)data;

                switch (alertConfigurationDS.CommandType)
                {
                        case DatasetCommandTypes.Update:
                        using (var workflowsEntities = new WorkflowsEntities(workflowConnectionString))
                        {


                            foreach (DataRow row in alertConfigurationDS.configurationTable.Rows)
                            {
                                var configurationID = new Guid(row[alertConfigurationDS.configurationTable.CONFIGURATIONID].ToString());
                                var CID = new Guid(row[alertConfigurationDS.configurationTable.CID].ToString());

                                var configuration =
                                    workflowsEntities.AlertConfigurations.FirstOrDefault(a => a.ConfigurationID == configurationID && a.CID == CID);
                                if (configuration != null)
                                {
                                    configuration.WorkflowID = new Guid(row[alertConfigurationDS.configurationTable.WORKFLOWID].ToString());
                                    configuration.CID = new Guid(row[alertConfigurationDS.configurationTable.CID].ToString());
                                    configuration.ReminderDuration = Convert.ToInt16(row[alertConfigurationDS.configurationTable.REMINDERDURATION].ToString());
                                    configuration.DurationType = Convert.ToInt16(row[alertConfigurationDS.configurationTable.DURATIONTYPE].ToString());
                                    configuration.ExecutionMode = Convert.ToInt16(row[alertConfigurationDS.configurationTable.EXECUTIONMODE].ToString());
                                    configuration.ViewOnline = Convert.ToBoolean(row[alertConfigurationDS.configurationTable.VIEWONLINE].ToString());
                                    configuration.SendEmail = Convert.ToBoolean(row[alertConfigurationDS.configurationTable.SENDEMAIL].ToString());
                                    configuration.ModifiedOn = Convert.ToDateTime(row[alertConfigurationDS.configurationTable.MODIFIEDON].ToString());
                                    configuration.UpdateBy = row[alertConfigurationDS.configurationTable.UPDATEBY].ToString();
                                    configuration.UserType = Convert.ToInt16(row[alertConfigurationDS.configurationTable.USERTYPE].ToString());
                                    configuration.ExecutionDays = Convert.ToInt16(row[alertConfigurationDS.configurationTable.EXECUTIONDAYS].ToString());
                                    workflowsEntities.SaveChanges();
                                }
                                else
                                {
                                    //Add New
                                    var newConfiguration = new AlertConfiguration
                                        {
                                            ConfigurationID =Guid.NewGuid(),
                                            WorkflowID =new Guid(row[alertConfigurationDS.configurationTable.WORKFLOWID].ToString()),
                                            CID = new Guid(row[alertConfigurationDS.configurationTable.CID].ToString()),
                                            ReminderDuration =Convert.ToInt16(row[alertConfigurationDS.configurationTable.REMINDERDURATION].ToString()),
                                            DurationType =Convert.ToInt16(row[alertConfigurationDS.configurationTable.DURATIONTYPE].ToString()),
                                            ExecutionMode =Convert.ToInt16(row[alertConfigurationDS.configurationTable.EXECUTIONMODE].ToString()),
                                            ViewOnline =Convert.ToBoolean(row[alertConfigurationDS.configurationTable.VIEWONLINE].ToString()),
                                            SendEmail =Convert.ToBoolean(row[alertConfigurationDS.configurationTable.SENDEMAIL].ToString()),
                                            CreatedOn =Convert.ToDateTime(row[alertConfigurationDS.configurationTable.CREATEDON].ToString()),
                                            UpdateBy = row[alertConfigurationDS.configurationTable.UPDATEBY].ToString(),
                                            UserType =Convert.ToInt16(row[alertConfigurationDS.configurationTable.USERTYPE].ToString()),
                                            ExecutionDays =Convert.ToInt16(row[alertConfigurationDS.configurationTable.EXECUTIONDAYS].ToString())
                                        };

                                    workflowsEntities.AlertConfigurations.AddObject(newConfiguration);
                                    workflowsEntities.SaveChanges();
                                }
                            }
                            

                        }
                        break;
                        case DatasetCommandTypes.Delete:
                        break;
                        
                }
                        
                #endregion
            }
            

            #region Execute WorkFlow
            if (data is ExecuteWorkFlowDS)
            {
                ExecuteWorkFlowDS executeWorkFlowDS = (ExecuteWorkFlowDS)data;
                using (var workflowsEntities = new WorkflowsEntities(workflowConnectionString))
                {
                    var workflowObject = workflowsEntities.Workflows.Where(item => item.WorkflowID == executeWorkFlowDS.WorkflowID).FirstOrDefault();
                    if (workflowObject != null)
                    {
                        //var workflowKeyValues = workflowsEntities.WorkflowKeyValues.Where(item => item.WorkflowID == workflowObject.WorkflowID);
                        //if (workflowKeyValues != null && workflowKeyValues.Count() > 0)
                        //{
                        switch (executeWorkFlowDS.WorkflowType)
                        {
                            case WorkflowTypeEnum.AnnualReport:
                                {
                                    WorkflowTypeEnumAnnualReportExecute(workflowsEntities, executeWorkFlowDS, workflowObject);
                                    break;
                                }
                            case WorkflowTypeEnum.MDA:
                                {
                                    WorkflowTypeEnumMDAExecute(workflowsEntities, executeWorkFlowDS, workflowObject);
                                    break;
                                }
                            default:
                                break;
                        }
                        //}
                    }
                }
            }
            #endregion

            return ModifiedState.MODIFIED;
        }
        #endregion

        #region WorkflowTypeEnum.AnnualReport

        private void WorkflowTypeEnumAnnualReportExecute(WorkflowsEntities workflowsEntities, ExecuteWorkFlowDS executeWorkFlowDS, Workflow workflow)
        {
            IOrganization orgCM = Broker.GetWellKnownBMC(CalculationInterface.WellKnownCM.Organization) as IOrganization;
            DBUser user = this.Broker.GetBMCInstance("Administrator", "DBUser_1_1") as DBUser;
            object[] args = new Object[3] { orgCM.CLID, user.CID, EnableSecurity.SecuritySetting };
            DataView view = new DataView(this.Broker.GetBMCDetailList(OrganizationInstall.ASSEMBLY_NAME, args).Tables[OrganisationListingDS.TableName]);
            OrganisationListingDS organisationListingDS = new OrganisationListingDS();
            view.RowFilter = organisationListingDS.GetRowFilterAllClientsExceptEclipse();

            DataTable orgFiteredTable = view.ToTable();
            int count = 0;
            foreach (DataRow row in orgFiteredTable.Rows)
            {
                Broker.SaveOverride = true;
                Guid cid = new Guid(row["ENTITYCIID_FIELD"].ToString()); //new Guid("77a2ff84-7058-47b7-a9ab-9b2d73beb4ed");//
                IOrganizationUnit organizationUnit = this.Broker.GetBMCInstance(cid) as IOrganizationUnit;
                string fileName = string.Empty;
                string notificationDetails = "System failed to generate report";
                string notificationDesc = "System failed to generate report";

                if (organizationUnit.IsInvestableClient)
                {

                    CapitalReportDS capitalReportDS = new CapitalReportDS();
                    capitalReportDS.PrintAddress = false;
                    AccountingFinancialYear accountingFinancialYear = new AccountingFinancialYear(DateTime.Now);
                    capitalReportDS.StartDate = accountingFinancialYear.FinYearStartDate;
                    capitalReportDS.EndDate = accountingFinancialYear.FinYearEndDate;

                    var existingNotification = workflowsEntities.Notifications.Where(item => item.ClientCID == organizationUnit.CID && item.WorkflowID == workflow.WorkflowID).FirstOrDefault();
                    string fileDownloadURL = string.Empty;
                    Guid attachmentGuid = Guid.Empty;

                    try
                    {
                        organizationUnit.GetData(capitalReportDS);
                        MemoryStream attachmentStream = MakeDocCapital(capitalReportDS, ref fileName);
                        byte[] byteArray = attachmentStream.ToArray();
                        //Clean up the memory stream 
                        attachmentStream.Flush();
                        attachmentStream.Close();
                        Stream stream = new MemoryStream(byteArray);

                        AttachmentListDS attachmentListDS = new AttachmentListDS();
                        organizationUnit.GetData(attachmentListDS);
                        DataTable objAttachmentsTable = attachmentListDS.Tables[AttachmentListDS.ATTACHMENTTABLE];
                        objAttachmentsTable.AcceptChanges();

                        if (existingNotification != null)
                        {
                            foreach (DataRow objRow in objAttachmentsTable.Rows)
                            {
                                if (objRow[AttachmentListDS.ATTACHMENTID_FIELD].ToString() == existingNotification.AttachmentID.ToString())
                                    objRow.Delete();
                            }
                        }

                        attachmentGuid = Guid.NewGuid();
                        DataRow objDataRow = objAttachmentsTable.NewRow();
                        objDataRow[AttachmentListDS.ATTACHMENTID_FIELD] = attachmentGuid;
                        objDataRow[AttachmentListDS.ATTACHMENTDESCRIPTION_FIELD] = "ANNUAL REPORT [" + capitalReportDS.EndDate.ToString("yyyy") + "]";
                        objDataRow[AttachmentListDS.ATTACHMENTDATEIMPORTED_FIELD] = DateTime.Now;
                        objDataRow[AttachmentListDS.ATTACHMENTORIGINPATH_FIELD] = organizationUnit.EclipseClientID + " - " + organizationUnit.Name + " - " + workflow.WorkflowDesc + ".pdf";
                        objDataRow[AttachmentListDS.ATTACHMENTIMPORTEDBY_FIELD] = "Administrator";
                        objDataRow[AttachmentListDS.ATTACHMENTFILE_FIELD] = stream;
                        objDataRow[AttachmentListDS.ATTACHMENTTYPE_FIELD] = AttachmentType.AnnualReport;
                        objDataRow[AttachmentListDS.ATTACHMENTLINKED_FIELD] = false;
                        objAttachmentsTable.Rows.Add(objDataRow);

                        notificationDesc = objDataRow[AttachmentListDS.ATTACHMENTDESCRIPTION_FIELD].ToString();
                        organizationUnit.SetData(attachmentListDS);
                        fileDownloadURL = attachmentListDS.FileDownloadURL;
                        notificationDetails = "Report is generated successfully";
                        Broker.SetComplete();
                        Broker.SetStart();
                    }
                    catch
                    {

                    }

                    if (existingNotification == null)
                    {
                        existingNotification = new Notification();
                        existingNotification.NotificationID = Guid.NewGuid();
                        existingNotification.WorkflowID = workflow.WorkflowID;
                        existingNotification.CreatedDate = DateTime.Now;
                        workflowsEntities.Notifications.AddObject(existingNotification);
                    }
                    existingNotification.FileDownLoadURLDownload = fileDownloadURL;
                    existingNotification.AttachmentID = attachmentGuid;
                    existingNotification.ClientCID = organizationUnit.CID;
                    existingNotification.ClientID = organizationUnit.EclipseClientID;
                    existingNotification.ClientName = organizationUnit.Name;
                    existingNotification.NotificationDescription = notificationDesc;
                    existingNotification.NotificationDetails = notificationDetails;
                    existingNotification.NotificationName = "ANNUAL REPORT";
                    existingNotification.ModifiedDate = DateTime.Now;
                    existingNotification.NotificationType = (int)NotificationType.AnnualReportGenerated;
                }

                count++;
            }

            workflowsEntities.SaveChanges();
        }

        public MemoryStream MakeDocCapital(CapitalReportDS PresentationData, ref string FileName)
        {
            C1PrintDocument doc = new C1PrintDocument();
            DataRow clientSummaryRow = PresentationData.Tables[CapitalReportDS.CLIENTSUMMARYTABLE].Rows[0];
            ReportUtilities reportUtilities = new CM.ReportUtilities();
            ReportUtilities.AddClientHeaderToReport(doc, clientSummaryRow, PresentationData.DateInfo(), "ANNUAL REPORT");

            DataTable capitalSummaryTable = PresentationData.Tables[CapitalReportDS.CAPITALFLOWSUMMARY];
            DataTable capitalSummaryCatTable = PresentationData.Tables[CapitalReportDS.CAPITALFLOWSUMMARYCAT];
            DataTable bankTranTable = PresentationData.Tables[CapitalReportDS.BANKTRANSACTIONSTABLE];
            DataTable asxTranTable = PresentationData.Tables[CapitalReportDS.ASXTRANSTABLE];

            var capitalSummaryCatRows = capitalSummaryCatTable.Select().OrderByDescending(rows => rows[CapitalReportDS.MONTH]);
            var bankTranTableRows = bankTranTable.Select().OrderByDescending(rows => rows[CapitalReportDS.BANKTRANSDATE]).OrderBy(rows => rows[CapitalReportDS.CAPITALMOVEMENTCAT]).OrderBy(rows => rows[CapitalReportDS.BANKTRANSDATE]);
            var asxTranTableRows = asxTranTable.Select().OrderByDescending(rows => rows[CapitalReportDS.TRADEDATE]);

            var difmRows = capitalSummaryTable.Select("ServiceType = 'Do It For Me'").OrderBy(rows => rows[CapitalReportDS.ASSETNAME]);
            var diyRows = capitalSummaryTable.Select("ServiceType = 'Do It Yourself'").OrderBy(rows => rows[CapitalReportDS.ASSETNAME]);
            var diwmRows = capitalSummaryTable.Select("ServiceType = 'Do It With Me'").OrderBy(rows => rows[CapitalReportDS.ASSETNAME]);
            var manRows = capitalSummaryTable.Select("ServiceType = 'Manual Asset'").OrderBy(rows => rows[CapitalReportDS.ASSETNAME]);

            reportUtilities.SetCapitalCatSummaryGrid(doc, capitalSummaryCatRows, "CAPITAL MOVEMENT SUMMARY");

            reportUtilities.SetCapitalGrid(doc, difmRows, "CAPITAL MOVEMENT SUMMARY - DO IT FOR ME", (PresentationData).DateInfo());
            reportUtilities.SetCapitalGrid(doc, diyRows, "CAPITAL MOVEMENT SUMMARY - DO IT YOURSELF", (PresentationData).DateInfo());
            reportUtilities.SetCapitalGrid(doc, diwmRows, "CAPITAL MOVEMENT SUMMARY - DO IT WITH ME", (PresentationData).DateInfo());
            reportUtilities.SetCapitalGrid(doc, manRows, "CAPITAL MOVEMENT SUMMARY - MANUAL ASSETS", (PresentationData).DateInfo());

            var groupedRows = PresentationData.Tables[CapitalReportDS.TRANBREAKDOWNTABLE].Select().OrderByDescending(row => (DateTime)row[CapitalReportDS.TRADEDATE]).GroupBy(row => row[CapitalReportDS.MAJORCATEGORY]);

            foreach (var tranCategory in groupedRows)
                reportUtilities.SetTransBreakDownGrid(doc, tranCategory, tranCategory.Key.ToString().ToUpper());

            reportUtilities.AddFooterCapMvtStatement(doc);

            MemoryStream memoryStrem = new MemoryStream();
            doc.Export(memoryStrem, doc.ExportProviders["pdf"]);
            return memoryStrem;
        }

        private void WorkflowTypeEnumMDAExecute(WorkflowsEntities workflowsEntities, ExecuteWorkFlowDS executeWorkFlowDS,
                                                Workflow workflow)
        {


            UserEntity userEntity = this.GetUser("Administrator");

            //Get All client data that is needed for Alert Functionality (filtered applied)
            var attachmentdt = new DataTable();
            DataTable clientDetailTable = this.GetClientMDAData(userEntity,executeWorkFlowDS.ClientCount,executeWorkFlowDS.ClientIDs, out attachmentdt);

            // Get alert(action) and notification Data
            WorkflowsDS workflowsDS = new WorkflowsDS();
            this.GetData(workflowsDS);
            DataTable notificationTable = workflowsDS.Tables[WorkflowsDS.NotificationsTable];
            DataTable actionTable = workflowsDS.Tables[WorkflowsDS.ActionsTable];

            // Create collectiond for notificationsToAdd & actionsToAdd
            List<Notification> notificationsToAdd = new List<Notification>();
            List<Oritax.TaxSimp.SM.Workflows.Action> actionsToAdd = new List<Oritax.TaxSimp.SM.Workflows.Action>();

            IOrganization org = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            MDAUtil mdautil = new MDAUtil();

            //Iterate through each record in clientDetailTable (client Data)
            foreach (DataRow row in clientDetailTable.Rows)
            {
                // Check if a notification exist with same (WFID, ClientCID, and notificationtype=2)
                // returns notification id if notification exists otherwise Guid.Empty
                Guid notificationID = this.GetMDANotificationID(notificationTable, (Guid)row[ClientDetailTable.CLIENTCID], workflow.WorkflowID.ToString());

                //calculate due date and current date
                DateTime mdaExecutionDate = DateTime.Parse(row[ClientDetailTable.MDAEXECUTIONDATE].ToString());
                //todo:shall be decided after client conseltation.
                DateTime dueDate = mdaExecutionDate.AddYears(1);//MDA due Date should be after one year
                DateTime currentDate = DateTime.Now;

                MDAActionTypeDetail actionTypeDetail = MDAActionTypeDetail.Custom;
                bool isVisibleToAdmin = true, isVisibleToAdviser = false, isVisibleToClient = false;
                //bool generateAlert = false;

                var cid = new Guid(row[ClientDetailTable.CLIENTCID].ToString());
                var parentID = new Guid(row[ClientDetailTable.PARENTID].ToString());
                var alertConfigurationDS = GetConfigurationTable(cid, parentID);
                
                if (alertConfigurationDS.configurationTable.Rows.Count > 0)
                {
                    //Processing alert generation as per alert configuration.
                    foreach (DataRow r in alertConfigurationDS.configurationTable.Rows)
                    {
                        int executionDays = Convert.ToInt16(r[alertConfigurationDS.configurationTable.EXECUTIONDAYS]);
                        int reminderDuration =Convert.ToInt16(r[alertConfigurationDS.configurationTable.REMINDERDURATION]);
                        bool viewOnlin = Convert.ToBoolean(r[alertConfigurationDS.configurationTable.VIEWONLINE]);
                        bool sendEmail=Convert.ToBoolean(r[alertConfigurationDS.configurationTable.SENDEMAIL]);

                        var durationType = (AlertDurationType)Enum.Parse(typeof(AlertDurationType), r[alertConfigurationDS.configurationTable.DURATIONTYPE].ToString());
                        var executionMode = (AlertExecutionMode)Enum.Parse(typeof(AlertExecutionMode), r[alertConfigurationDS.configurationTable.EXECUTIONMODE].ToString());
                        
                        //Set alert date.
                        DateTime alertDate = dueDate.AddDays(executionDays);
                        // If an alert is not complete 4 weeks after the due date the system raises an alert to administrators to take the appropriate action
                        if (currentDate.Date >= alertDate.Date)
                        {
                            string alertDescription = 
                                string.Format("MDA Reminder {0}{1} {2}",executionMode == AlertExecutionMode.OnDueDate? string.Empty: reminderDuration.ToString()
                                    ,executionMode == AlertExecutionMode.OnDueDate? string.Empty: durationType.ToString().Substring(0, 1),executionMode.ToString());
                            
                            isVisibleToAdviser = true;
                            isVisibleToClient = viewOnlin;

                            //if (!this.ActionExistsAndCompleted(actionTable, notificationID, ActionType.MDA, dueDate.ToShortDateString()))
                            {
                                if (!this.ActionExists(actionTable, notificationID, ActionType.MDA, actionTypeDetail, dueDate.ToShortDateString()))
                                {
                                    Guid newNotificationID = notificationID;
                                    if (notificationID == Guid.Empty)
                                    {
                                        newNotificationID = this.AddNotification(notificationsToAdd, row, workflow.WorkflowID);
                                    }
                                    else
                                    {
                                        //todo: please don't expire ro remove fourth alter (after 4 week) it shall be expired after action
                                        //Fourth Alert should also be expired when a new first alert is added. This means that execution date for that alert has already been updated and the due date of new contract is about to come
                                        //...mark all actions expired (who has same notificationID, and ActionType = 'MDA' 
                                        this.SetActionsAsExpired(notificationID, ActionType.MDA);
                                    }
                                    this.AddAction(actionsToAdd, row, newNotificationID,
                                                   ActionType.MDA, actionTypeDetail,
                                                   isVisibleToClient, isVisibleToAdmin, isVisibleToAdviser,alertDescription);

                                    if (sendEmail)
                                    {
                                        var isFirstWarning = executionMode == AlertExecutionMode.Before ? true : false;
                                        mdautil.GeneratMDADocumentAndSendToAdviser(row, org.Model, isFirstWarning, (message) =>
                                            {
                                                message = alertDescription + message;
                                                Broker.LogEvent(EventType.EmailSent, message, userEntity.CurrentUserName,
                                                                Guid.Empty);
                                            });
                                    }
                                }
                            }
                            break;
                        }
                    }
                }
                //Mannual alert generation
                #region Mannual alert generation
                //// If an alert is not complete 4 weeks after the due date the system raises an alert to administrators to take the appropriate action
                //if (currentDate.Date >= dueDate.AddDays(7 * 4).Date)
                //{
                //    if (!this.ActionExistsAndCompleted(actionTable, notificationID, ActionType.MDA, dueDate.ToShortDateString()))
                //    {
                //        actionTypeDetail = MDAActionTypeDetail.FourthAlert;
                //        isVisibleToAdmin = true;
                //        isVisibleToAdviser = true;
                //        generateAlert = true;
                //    }

                //}
                //    /*
                // * Email work
                // * If an alert is not complete on the due date the system sends warning emails to the client and the adviser
                // * If due has occured. Now send an Email.
                // * Generating alert too, because it may be a first alert for existing client. therefore, It will be added and shown to Adviser                
                //*/
                //else if (currentDate.Date >= dueDate.Date)
                //{
                //    actionTypeDetail = MDAActionTypeDetail.ThirdAlert;

                //    //Sending email, if not sent before.
                //    if (
                //        !this.ActionExists(actionTable, notificationID, ActionType.MDA, actionTypeDetail,
                //                           dueDate.ToShortDateString()))
                //    {
                //        mdautil.GeneratMDADocumentAndSendToAdviser(row, org.Model, false, (message) =>
                //            {
                //                message = "MDA Renewal (Third Alert) - " + message;
                //                Broker.LogEvent(EventType.EmailSent, message, userEntity.CurrentUserName, Guid.Empty);
                //            });
                //    }

                //    //Generting Alert.

                //    isVisibleToAdmin = true;
                //    isVisibleToAdviser = true;
                //    generateAlert = true;

                //}
                //    // If an alert is not complete 2 weeks prior the due date the alert becomes red
                //else if (currentDate.Date >= dueDate.AddDays(-7*2).Date)
                //{
                //    actionTypeDetail = MDAActionTypeDetail.SecondAlert;
                //    isVisibleToAdmin = true;
                //    isVisibleToAdviser = true;
                //    generateAlert = true;
                //}
                //    // 8 weeks before a due date the system raises an alert for an adviser.    
                //else if (currentDate.Date >= dueDate.AddDays(-7*8).Date)
                //{
                //    actionTypeDetail = MDAActionTypeDetail.FirstAlert;
                //    //Sending email, if not sent before.
                //    if (
                //        !this.ActionExists(actionTable, notificationID, ActionType.MDA, actionTypeDetail,
                //                           dueDate.ToShortDateString()))
                //    {
                //        mdautil.GeneratMDADocumentAndSendToAdviser(row, org.Model, true, (message) =>
                //            {
                //                message = "MDA Renewal (First Alert) - " + message;
                //                Broker.LogEvent(EventType.EmailSent, message, userEntity.CurrentUserName, Guid.Empty);
                //            });
                //    }
                //    isVisibleToAdmin = true;
                //    isVisibleToAdviser = true;
                //    generateAlert = true;
                //}

                ////If generate alert comes true from the top then an alert will be generated and saved in DB.
                //if (generateAlert)
                //{
                //    //if notification is not empty && if alert exists 
                //    //(with same notificationID,ActionType='MDA' and raises date = duedate + 4weeks(subtype = '4')
                //    // ... do nothing
                //    // else
                //    if (!this.ActionExists(actionTable, notificationID, ActionType.MDA, actionTypeDetail, dueDate.ToShortDateString()))
                //    {
                //        Guid newNotificationID = notificationID;
                //        if (notificationID == Guid.Empty)
                //        {
                //            newNotificationID = this.AddNotification(notificationsToAdd, row, workflow.WorkflowID);
                //        }
                //        else
                //        {
                //            //todo: please don't expire ro remove fourth alter (after 4 week) it shall be expired after action
                //            //Fourth Alert should also be expired when a new first alert is added. This means that execution date for that alert has already been updated and the due date of new contract is about to come
                //            //...mark all actions expired (who has same notificationID, and ActionType = 'MDA' 
                //            this.SetActionsAsExpired(notificationID, ActionType.MDA);
                //        }
                //        this.AddAction(actionsToAdd, row, newNotificationID,
                //                       ActionType.MDA, actionTypeDetail,
                //                       isVisibleToClient, isVisibleToAdmin, isVisibleToAdviser);
                //    }
                //}
                #endregion
            }
            Broker.ReleaseBrokerManagedComponent(org);
            // todo: need to refactor when get time (don't call from DS, call/use entities directly if you are already in workflow Sm)
            workflowsDS.DataSetOperationType = DataSetOperationType.NewBulk;
            workflowsDS.TableToUpdate = WorkflowsDS.NotificationsTable;
            workflowsDS.NotificationListToAdd = notificationsToAdd;
            this.SetData(workflowsDS);

            workflowsDS.DataSetOperationType = DataSetOperationType.NewBulk;
            workflowsDS.TableToUpdate = WorkflowsDS.ActionsTable;
            workflowsDS.ActionListToAdd = actionsToAdd;
            this.SetData(workflowsDS);

        }

        private AlertConfigurationDS GetConfigurationTable(Guid cid, Guid parentID)
        {
            var alertConfigurationDS = new AlertConfigurationDS();
            alertConfigurationDS.CID = cid;
            alertConfigurationDS.ParentID = parentID;
            alertConfigurationDS.SortOrderBy = SortOrder.Descending;
            OnGetData(alertConfigurationDS);
            return alertConfigurationDS;
        }

        private void SetActionsAsExpired(Guid notificationID, Oritax.TaxSimp.SM.Workflows.ActionType actionType)
        {
            WorkflowsDS workflowsDS = new WorkflowsDS();
            this.GetData(workflowsDS);
            DataTable actionTable = workflowsDS.Tables[WorkflowsDS.ActionsTable];

            DataTable recordsToUpdate = actionTable.Clone();

            string nID = notificationID.ToString();
            string type = actionType.ToString();
            foreach (DataRow row in actionTable.Rows)
            {
                if (string.Compare(row[ActionsTable.NOTIFICATIONID_FIELD].ToString(), nID) == 0
                    && string.Compare(row[ActionsTable.ACTIONTYPE_FIELD].ToString(), type) == 0)
                //&& string.Compare(row[ActionsTable.ACTIONTYPEDETAIL_FIELD].ToString(), MDAActionTypeDetail.FourthAlert.ToString()) != 0)
                {

                    row[ActionsTable.ISEXPIRED_FIELD] = 1;
                    recordsToUpdate.ImportRow(row);
                }
            }

            if (recordsToUpdate.Rows.Count > 0)
            {
                workflowsDS.DataSetOperationType = DataSetOperationType.UpdateBulk;
                workflowsDS.TableToUpdate = WorkflowsDS.ActionsTable;
                workflowsDS.RecordsToUpdate = recordsToUpdate;
                this.SetData(workflowsDS);
            }
        }

        private Guid AddAction(List<Oritax.TaxSimp.SM.Workflows.Action> actions, DataRow row, Guid notificationID,
            Oritax.TaxSimp.SM.Workflows.ActionType actionType,
            MDAActionTypeDetail actionTypeDetail, bool isVisibleToClient = false,
            bool isVisibleToAdmin = false, bool isVisibleToAdviser = false,string alertDescription="MDA Renewal")
        {
            Oritax.TaxSimp.SM.Workflows.Action a = new Oritax.TaxSimp.SM.Workflows.Action();
            a.ActionID = Guid.NewGuid();
            a.ActionType = Oritax.TaxSimp.SM.Workflows.ActionType.MDA.ToString();
            a.CreatedBy = "";
            a.CreationDate = DateTime.Now;
            a.CreationMode = CreationMode.SystemGenerated.ToString();
            a.DueDate = Convert.ToDateTime(row[ClientDetailTable.MDAEXECUTIONDATE].ToString()).AddYears(1);
            a.ExpiryDate = a.DueDate;
            a.ActionTypeDetail = Convert.ToInt32(actionTypeDetail);
            a.IsVisibleToAdmin = isVisibleToAdmin;
            a.IsVisibleToAdviser = isVisibleToAdviser;
            a.IsVisibleToClient = isVisibleToClient;
            a.IsExpired = false;
            a.Message = alertDescription;
            a.NotificationID = notificationID;
            a.Status = ActionStatus.Active.ToString();
            a.UpdatedBy = "";
            a.UpdatedDate = DateTime.Now;

            actions.Add(a);
            

            return a.ActionID;
        }

        private Guid AddNotification(List<Notification> notifications, DataRow row, Guid workflowID)
        {
            Notification n = new Notification
            {
                AttachmentID = Guid.Empty,
                ClientID = row[ClientDetailTable.CLIENTID].ToString(),
                ClientCID = new Guid(row[ClientDetailTable.CLIENTCID].ToString()),
                ClientName = row[ClientDetailTable.CLIENTNAME].ToString(),
                CreatedDate = DateTime.Now,
                FileDownLoadURLDownload = string.Empty,
                ModifiedDate = DateTime.Now,
                NotificationDescription = "MDA Alert",
                NotificationDetails = "MDA ALert",
                NotificationID = Guid.NewGuid(),
                NotificationName = "MDA Alert",
                NotificationType = (int)NotificationType.MDA,
                WorkflowID = workflowID
            };

            notifications.Add(n);

            return n.NotificationID;
        }

        private DataTable GetClientMDAData(UserEntity userEntity,int clientCount,string clientIDs, out DataTable attachement)
        {
            attachement = null;
            IOrganization organization = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            MDAAlertDS ds = new MDAAlertDS()
            {
                Command = (int)WebCommands.GetAllClientDetails,
                CommandType = DatasetCommandTypes.Get,
                Unit = new Oritax.TaxSimp.Data.OrganizationUnit { CurrentUser = userEntity },
                ClientIDs = clientIDs??string.Empty,
                Count = clientCount,
                
            };
            
            if (organization != null)
            {
                organization.GetData(ds);
                Broker.ReleaseBrokerManagedComponent(organization);
            }
            if (ds.Tables[AttachmentListDS.ATTACHMENTTABLE] != null)
                attachement = ds.Tables[AttachmentListDS.ATTACHMENTTABLE].Copy();
            //Filter data: 1.DIFM clients only
            //2.clients have mdaexecution date
            //Update:Innova-1413 enable it for DIWM too 
            string filter = String.Format("({0} = '{1}' OR  {2} = '{3}') AND NOT Isnull({4},'Null Column')= 'Null Column'",
               ClientDetailTable.HASDIFM, true, ClientDetailTable.HASDIWM, true,
               ClientDetailTable.MDAEXECUTIONDATE);

            DataView clientDetailView = new DataView(ds.Tables[MDAAlertDS.CLIENTDETAILTABLE]) { RowFilter = filter };
            DataTable clientDetailTable = clientDetailView.ToTable();

            return clientDetailTable;

        }

       
        /// <summary>
        /// Check if notification exist with same (WFID, ClientCID, and notificationtype=2)
        /// 
        /// </summary>
        /// <param name="notificationTable"></param>
        /// <param name="notificationRow"></param>
        /// <returns> notification id if notification exists otherwise Guid.Empty</returns>
        private Guid GetMDANotificationID(DataTable notificationTable, Guid clientCID, string workflowID)
        {
            //Check if notification exist with same (WFID, ClientCID, and notificationtype=2)
            //string newClientCID = notificationRow[ClientDetailTable.CLIENTCID].ToString();
            string filter = string.Format("{0}='{1}' and {2}='{3}' and {4}='{5}'", NotificationsTable.WORKFLOWID_FIELD, workflowID, NotificationsTable.CLIENTCID_FIELD, clientCID, NotificationsTable.NOTIFICATIONTYPE_FIELD, (int)NotificationType.MDA);
            var rows = notificationTable.Select(filter);
            Guid id = Guid.Empty;
            if (rows.Length > 0)
            {
                id = new Guid(rows[0][NotificationsTable.NOTIFICATIONID_FIELD].ToString());
            }
            return id;
        }

        private bool ActionExists(DataTable actionTable, Guid notificationID,
            ActionType actionType, MDAActionTypeDetail actionTypeDetail, string dueDate)
        {
            string nID = notificationID.ToString();
            string type = actionType.ToString();
            int typeDetail = Convert.ToInt32(actionTypeDetail);
            string filter = string.Format("{0}='{1}' and {2}='{3}' and {4}='{5}' and {6}='{7}'", ActionsTable.NOTIFICATIONID_FIELD, nID, ActionsTable.ACTIONTYPE_FIELD, type, ActionsTable.ACTIONTYPEDETAIL_FIELD, typeDetail, ActionsTable.DUEDATE_FIELD, dueDate);
            var rows = actionTable.Select(filter);
            bool actionExist = rows.Length > 0;
            return actionExist;
        }
        private bool ActionExistsAndCompleted(DataTable actionTable, Guid notificationID,
            ActionType actionType, string dueDate)
        {
            string nID = notificationID.ToString();
            string type = actionType.ToString();
            string filter = string.Format("{0}='{1}' and {2}='{3}' and {4}='{5}' and {6}='{7}'", ActionsTable.NOTIFICATIONID_FIELD, nID, ActionsTable.ACTIONTYPE_FIELD, type, ActionsTable.STATUS_FIELD, ActionStatus.Completed, ActionsTable.DUEDATE_FIELD, dueDate);
            var rows = actionTable.Select(filter);
            bool actionExist = rows.Length > 0;
            return actionExist;
        }

        private UserEntity GetUser(string userName)
        {
            DBUser objUser = this.Broker.GetBMCInstance("Administrator", "DBUser_1_1") as DBUser;
            UserEntity userEntity = new UserEntity();
            userEntity.CID = objUser.CID;
            userEntity.CurrentUserName = objUser.Name;
            userEntity.IsAdmin = objUser.Administrator;
            userEntity.IsWebUser = true;
            userEntity.UserType = (int)objUser.UserType;
            this.Broker.ReleaseBrokerManagedComponent(objUser);
            return userEntity;
        }


        #endregion


    }
}
