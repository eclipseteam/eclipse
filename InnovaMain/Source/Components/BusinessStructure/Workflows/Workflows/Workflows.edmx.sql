
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, and Azure
-- --------------------------------------------------
-- Date Created: 11/08/2013 15:51:38
-- Generated from EDMX file: D:\Innova\InnovaMain\Source\Components\BusinessStructure\Workflows\Workflows\Workflows.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [TestInnova2011];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Notifications]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Notifications];
GO
IF OBJECT_ID(N'[dbo].[WorkflowStatus]', 'U') IS NOT NULL
    DROP TABLE [dbo].[WorkflowStatus];
GO
IF OBJECT_ID(N'[dbo].[Workflows]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Workflows];
GO
IF OBJECT_ID(N'[dbo].[WorkflowKeyValues]', 'U') IS NOT NULL
    DROP TABLE [dbo].[WorkflowKeyValues];
GO
IF OBJECT_ID(N'[dbo].[Actions]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Actions];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Notifications'
CREATE TABLE [dbo].[Notifications] (
    [NotificationID] uniqueidentifier  NOT NULL,
    [WorkflowID] uniqueidentifier  NOT NULL,
    [NotificationType] int  NOT NULL,
    [NotificationName] varchar(200)  NOT NULL,
    [NotificationDescription] varchar(400)  NOT NULL,
    [NotificationDetails] nvarchar(max)  NULL,
    [AttachmentID] uniqueidentifier  NULL,
    [ClientCID] uniqueidentifier  NOT NULL,
    [ClientID] nvarchar(max)  NOT NULL,
    [ClientName] nvarchar(max)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedDate] datetime  NOT NULL,
    [FileDownLoadURLDownload] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'WorkflowStatus'
CREATE TABLE [dbo].[WorkflowStatus] (
    [StatusID] uniqueidentifier  NOT NULL,
    [WorflowID] uniqueidentifier  NOT NULL,
    [BeforeStatus] int  NULL,
    [AfterStatus] int  NULL,
    [UserCID] uniqueidentifier  NULL,
    [Username] varchar(max)  NULL,
    [Comments] varchar(max)  NULL,
    [StatusDate] datetime  NULL
);
GO

-- Creating table 'Workflows'
CREATE TABLE [dbo].[Workflows] (
    [WorkflowID] uniqueidentifier  NOT NULL,
    [WorkflowDesc] varchar(max)  NOT NULL,
    [WorkflowName] varchar(max)  NOT NULL,
    [CreatedBy] varchar(max)  NOT NULL,
    [Comments] varchar(max)  NULL,
    [WorkflowType] int  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [LastRunDate] datetime  NULL
);
GO

-- Creating table 'WorkflowKeyValues'
CREATE TABLE [dbo].[WorkflowKeyValues] (
    [WorkflowKeyValueID] uniqueidentifier  NOT NULL,
    [Key] nvarchar(max)  NOT NULL,
    [Value] nvarchar(max)  NOT NULL,
    [WorkflowID] uniqueidentifier  NOT NULL
);
GO

-- Creating table 'Actions'
CREATE TABLE [dbo].[Actions] (
    [ActionID] uniqueidentifier  NOT NULL,
    [NotificationID] uniqueidentifier  NULL,
    [Message] varchar(max)  NULL,
    [IsExpired] bit  NOT NULL,
    [ExpiryDate] datetime  NOT NULL,
    [DueDate] datetime  NOT NULL,
    [ActionType] varchar(255)  NULL,
    [Status] varchar(255)  NULL,
    [CreationDate] datetime  NOT NULL,
    [CreatedBy] varchar(255)  NULL,
    [UpdatedDate] datetime  NOT NULL,
    [UpdatedBy] varchar(255)  NULL,
    [CreationMode] varchar(255)  NULL,
    [ActionTypeDetail] int  NOT NULL,
    [IsVisibleToClient] bit  NOT NULL,
    [IsVisibleToAdmin] bit  NOT NULL,
    [IsVisibleToAdviser] bit  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [NotificationID] in table 'Notifications'
ALTER TABLE [dbo].[Notifications]
ADD CONSTRAINT [PK_Notifications]
    PRIMARY KEY CLUSTERED ([NotificationID] ASC);
GO

-- Creating primary key on [StatusID] in table 'WorkflowStatus'
ALTER TABLE [dbo].[WorkflowStatus]
ADD CONSTRAINT [PK_WorkflowStatus]
    PRIMARY KEY CLUSTERED ([StatusID] ASC);
GO

-- Creating primary key on [WorkflowID], [WorkflowDesc], [WorkflowName], [CreatedBy], [WorkflowType], [CreatedDate] in table 'Workflows'
ALTER TABLE [dbo].[Workflows]
ADD CONSTRAINT [PK_Workflows]
    PRIMARY KEY CLUSTERED ([WorkflowID], [WorkflowDesc], [WorkflowName], [CreatedBy], [WorkflowType], [CreatedDate] ASC);
GO

-- Creating primary key on [WorkflowKeyValueID] in table 'WorkflowKeyValues'
ALTER TABLE [dbo].[WorkflowKeyValues]
ADD CONSTRAINT [PK_WorkflowKeyValues]
    PRIMARY KEY CLUSTERED ([WorkflowKeyValueID] ASC);
GO

-- Creating primary key on [ActionID] in table 'Actions'
ALTER TABLE [dbo].[Actions]
ADD CONSTRAINT [PK_Actions]
    PRIMARY KEY CLUSTERED ([ActionID] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------