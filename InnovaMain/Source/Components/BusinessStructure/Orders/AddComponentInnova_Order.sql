Declare @AssemblyID varchar(100)
Declare @ComponentID varchar(100)

Declare @AssemblyStrongName varchar(100)
Declare @ImplementationCMClass varchar(100)

set @AssemblyID='6F78B54B-C4D4-4C4B-8711-26FDDBD53072'
set @ComponentID='1504CBBC-4C93-43E9-8146-C97F5C6A661D'
set @AssemblyStrongName='Orders'
set @ImplementationCMClass='Oritax.TaxSimp.CM.Entity.OrdersCM'

DELETE FROM [dbo].[ASSEMBLY]
      WHERE ID = @AssemblyID



INSERT INTO [dbo].[ASSEMBLY]
           ([ID]
           ,[NAME]
           ,[DISPLAYNAME]
           ,[STRONGNAME]
           ,[MAJORVERSION]
           ,[MINORVERSION]
           ,[DATAFORMAT]
           ,[REVISION])
     VALUES
     (
           @AssemblyID,
           @AssemblyStrongName,
           @AssemblyStrongName, 
           @AssemblyStrongName+', Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215',
           1,
           1,
           0,
           0
           )


DELETE FROM [dbo].[COMPONENT]
      WHERE ID = @ComponentID



INSERT INTO [dbo].[COMPONENT]
           ([ID]
           ,[NAME]
           ,[DISPLAYNAME]
           ,[CATEGORY]
           ,[APPLICABILITY]
           ,[OBSOLETE])
     VALUES (
           @ComponentID,
           @AssemblyStrongName,
           @AssemblyStrongName,
           'BusinessEntity',
           3,
           'False'
           )


DELETE FROM [dbo].[COMPONENTVERSION]
      WHERE ID = @AssemblyID



INSERT INTO [dbo].[COMPONENTVERSION]
           ([ID]
           ,[VERSIONNAME]
           ,[COMPONENTID]
           ,[STARTDATE]
           ,[ENDDATE]
           ,[PERSISTERASSEMBLYSTRONGNAME]
           ,[PERSISTERCLASS]
           ,[IMPLEMENTATIONCLASS]
           ,[OBSOLETE])
     VALUES
     (
           @AssemblyID,
           '',
           @ComponentID,
           '1990-01-01 00:00:00.000',
           '2050-12-31 00:59:59.000',
           'CommonPersisters_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215',
           'Oritax.TaxSimp.CommonPersistence.BlobPersister',
           @ImplementationCMClass,
           'False'
           )
