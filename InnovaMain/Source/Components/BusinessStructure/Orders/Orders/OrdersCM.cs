using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Linq;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Enum;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Extensions;
using System.Text;
using TradeClient.DAL;

namespace Oritax.TaxSimp.CM.Entity
{
    [Serializable]
    public class OrdersCM : EntityCM, ISerializable
    {
        #region Private Variable
        private List<OrderEntity> _orders;
        private List<BulkOrderEntity> _bulkOrders;

        #endregion

        #region Public Properties
        public List<OrderEntity> Orders
        {
            get { return _orders; }
            set { _orders = value; }
        }
        public List<BulkOrderEntity> BulkOrders
        {
            get { return _bulkOrders; }
            set { _bulkOrders = value; }
        }
        #endregion

        #region Constructor
        public OrdersCM() { }

        protected OrdersCM(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            _orders = info.GetValue<List<OrderEntity>>("Oritax.TaxSimp.CM.Entity.Orders");
            _bulkOrders = info.GetValue<List<BulkOrderEntity>>("Oritax.TaxSimp.CM.Entity.BulkOrders");
        }
        #endregion

        #region Override Method
        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValueX("Oritax.TaxSimp.CM.Entity.Orders", _orders);
            info.AddValueX("Oritax.TaxSimp.CM.Entity.BulkOrders", _bulkOrders);

        }

        public override string OnUpdateDataStream(int type, string data)
        {
            string xml = String.Empty;

            return xml;
        }

        public override string GetDataStream(int type, string data)
        {
            string xml = String.Empty;
            switch ((CmCommand)type)
            {
                case CmCommand.MatchFileName:
                    xml = (Orders.Count(ss => ss.FileName == data) > 0).ToString();
                    break;
            }
            return xml;
        }

        public override string GetAllDataStream(int type)
        {
            return String.Empty;
        }
        #endregion

        protected override void OnGetData(DataSet data)
        {
            base.OnGetData(data);
            if (data is OrderPadDS)
            {
                var ds = data as OrderPadDS;

                switch (ds.CommandType)
                {
                    case DatasetCommandTypes.Get:
                        if (Orders != null)
                        {
                            var orders = Orders;
                            if (ds.Unit != null && !String.IsNullOrEmpty(ds.Unit.ClientId))
                            {
                                orders = orders.Where(ss => ss.ClientID == ds.Unit.ClientId).ToList();
                            }
                            if (ds.OrderId != Guid.Empty)
                            {
                                orders = orders.Where(ss => ss.ID == ds.OrderId).ToList();
                            }
                            foreach (var orderusers in orders.GroupBy(ss => ss.CreatedByCID))
                            {
                                string createdBy = string.Empty;

                                if (orderusers.Key != Guid.Empty)
                                {
                                    var user = Broker.GetBMCInstance(orderusers.Key);
                                    if (user != null)
                                        createdBy = user.GetDataStream(-999, null);
                                }

                                foreach (var order in orderusers)
                                {
                                    DataRow orderDr = ds.OrdersTable.NewRow();
                                    orderDr[ds.OrdersTable.ID] = order.ID;
                                    orderDr[ds.OrdersTable.ORDERID] = order.OrderID;
                                    orderDr[ds.OrdersTable.CID] = CID;
                                    orderDr[ds.OrdersTable.CSID] = CSID;
                                    orderDr[ds.OrdersTable.CLID] = Clid;
                                    orderDr[ds.OrdersTable.CLIENTCID] = order.ClientCID;
                                    orderDr[ds.OrdersTable.CLIENTID] = order.ClientID;
                                    orderDr[ds.OrdersTable.STATUS] = order.Status.ToString();
                                    orderDr[ds.OrdersTable.ORDERTYPE] = order.OrderType.ToString();
                                    orderDr[ds.OrdersTable.ORDERACCOUNTTYPE] = order.OrderAccountType.ToString();
                                    orderDr[ds.OrdersTable.FILENAME] = order.FileName;
                                    orderDr[ds.OrdersTable.ACCOUNTCID] = order.AccountCID;
                                    orderDr[ds.OrdersTable.ACCOUNTNUMBER] = order.AccountNumber;
                                    orderDr[ds.OrdersTable.ACCOUNTBSB] = order.AccountBSB;
                                    orderDr[ds.OrdersTable.ORDERITEMTYPE] = order.OrderItemType.ToString();
                                    orderDr[ds.OrdersTable.VALIDATIONMSG] = order.ValidationMessage;
                                    orderDr[ds.OrdersTable.COMMENTS] = order.Comments;
                                    orderDr[ds.OrdersTable.VALUE] = order.Items.Sum(ss => ss.Amount);

                                    if (order.Attachment != null)
                                    {
                                        orderDr[ds.OrdersTable.ATTACHMENT] = order.Attachment;
                                    }
                                    orderDr[ds.OrdersTable.CREATEDDATE] = order.CreatedDate;
                                    orderDr[ds.OrdersTable.CLIENTMANAGEMENTTYPE] = order.ClientManagementType;

                                    orderDr[ds.OrdersTable.TRADEDATE] = order.TradeDate;
                                    if (order.UpdatedDate.HasValue)
                                    {
                                        orderDr[ds.OrdersTable.UPDATEDDATE] = order.UpdatedDate;
                                    }
                                    if (order.ApprovedDate.HasValue)
                                    {
                                        orderDr[ds.OrdersTable.APPROVEDDATE] = order.ApprovedDate;
                                    }
                                    if (order.BatchID.HasValue)
                                    {
                                        orderDr[ds.OrdersTable.BATCHID] = order.BatchID;
                                    }

                                    orderDr[ds.OrdersTable.UPDATEDBY] = order.UpdatedBy;
                                    orderDr[ds.OrdersTable.UPDATEDBYCID] = order.UpdatedByCID;
                                    orderDr[ds.OrdersTable.ORDERBULKSTATUS] = order.OrderBulkStatus;
                                    orderDr[ds.OrdersTable.CREATEDBY] = createdBy;
                                    orderDr[ds.OrdersTable.CREATEDBYCID] = order.CreatedByCID;
                                    orderDr[ds.OrdersTable.ORDERPREFEREDBY] = order.OrderPreferedBy;
                                    orderDr[ds.OrdersTable.SMAREQUESTID] = order.SMARequestID;
                                    orderDr[ds.OrdersTable.SMACALLSTATUS] = order.SMACallStatus;
                                    ds.OrdersTable.Rows.Add(orderDr);

                                    foreach (var item in order.Items)
                                    {
                                        switch (order.OrderAccountType)
                                        {
                                            case OrderAccountType.StateStreet:
                                                {
                                                    var stateStreetItem = item as StateStreetOrderItem;

                                                    if (stateStreetItem != null)
                                                    {
                                                        DataRow itemDr = ds.StateStreetTable.NewRow();
                                                        itemDr[ds.StateStreetTable.ID] = stateStreetItem.ID;
                                                        itemDr[ds.StateStreetTable.ORDERID] = stateStreetItem.OrderID;

                                                        itemDr[ds.StateStreetTable.VALIDATIONMSG] = stateStreetItem.ValidationMessage;
                                                        if (stateStreetItem.Units.HasValue)
                                                        {
                                                            itemDr[ds.StateStreetTable.UNITS] = stateStreetItem.Units;
                                                        }
                                                        if (stateStreetItem.Amount.HasValue)
                                                        {
                                                            itemDr[ds.StateStreetTable.AMOUNT] = stateStreetItem.Amount;
                                                        }

                                                        itemDr[ds.StateStreetTable.CREATEDDATE] = stateStreetItem.CreatedDate;
                                                        if (stateStreetItem.UpdatedDate.HasValue)
                                                        {
                                                            itemDr[ds.StateStreetTable.UPDATEDDATE] = stateStreetItem.UpdatedDate;
                                                        }
                                                        itemDr[ds.StateStreetTable.PRODUCTID] = stateStreetItem.ProductID;
                                                        itemDr[ds.StateStreetTable.FUNDCODE] = stateStreetItem.FundCode;
                                                        itemDr[ds.StateStreetTable.FUNDNAME] = GetFundName(stateStreetItem.FundCode);
                                                        itemDr[ds.StateStreetTable.SMATRANSACTIONID] = item.SMATransactionID;
                                                        ds.StateStreetTable.Rows.Add(itemDr);
                                                    }
                                                }
                                                break;
                                            case OrderAccountType.TermDeposit:
                                                {
                                                    var termDepositItem = item as TermDepositOrderItem;

                                                    if (termDepositItem != null)
                                                    {
                                                        DataRow itemDr = ds.TermDepositTable.NewRow();
                                                        itemDr[ds.TermDepositTable.ID] = termDepositItem.ID;
                                                        itemDr[ds.TermDepositTable.ORDERID] = termDepositItem.OrderID;

                                                        itemDr[ds.TermDepositTable.VALIDATIONMSG] = termDepositItem.ValidationMessage;
                                                        if (termDepositItem.Units.HasValue)
                                                        {
                                                            itemDr[ds.TermDepositTable.UNITS] = termDepositItem.Units;
                                                        }
                                                        if (termDepositItem.Amount.HasValue)
                                                        {
                                                            itemDr[ds.TermDepositTable.AMOUNT] = termDepositItem.Amount;
                                                        }

                                                        itemDr[ds.TermDepositTable.CREATEDDATE] = termDepositItem.CreatedDate;
                                                        if (termDepositItem.UpdatedDate.HasValue)
                                                        {
                                                            itemDr[ds.TermDepositTable.UPDATEDDATE] = termDepositItem.UpdatedDate;
                                                        }
                                                        itemDr[ds.TermDepositTable.BROKERID] = termDepositItem.BrokerID;
                                                        itemDr[ds.TermDepositTable.BROKERNAME] = OrderPadUtilities.GetInstituteName(Broker, termDepositItem.BrokerID, true);
                                                        itemDr[ds.TermDepositTable.BANKID] = termDepositItem.BankID;
                                                        itemDr[ds.TermDepositTable.BANKNAME] = OrderPadUtilities.GetInstituteName(Broker, termDepositItem.BankID, true);
                                                        if (termDepositItem.Min.HasValue)
                                                            itemDr[ds.TermDepositTable.MIN] = termDepositItem.Min;
                                                        if (termDepositItem.Max.HasValue)
                                                            itemDr[ds.TermDepositTable.MAX] = termDepositItem.Max;
                                                        itemDr[ds.TermDepositTable.RATING] = termDepositItem.Rating;
                                                        itemDr[ds.TermDepositTable.DURATION] = termDepositItem.Duration;
                                                        if (termDepositItem.Percentage.HasValue)
                                                            itemDr[ds.TermDepositTable.PERCENTAGE] = termDepositItem.Percentage;
                                                        itemDr[ds.TermDepositTable.PRODUCTID] = termDepositItem.ProductID;
                                                        itemDr[ds.TermDepositTable.TDACCOUNTCID] = termDepositItem.TDAccountCID;
                                                        itemDr[ds.TermDepositTable.CONTRACTNOTE] = termDepositItem.ContractNote;
                                                        itemDr[ds.TermDepositTable.SMATRANSACTIONID] = item.SMATransactionID;
                                                        ds.TermDepositTable.Rows.Add(itemDr);
                                                    }
                                                }
                                                break;
                                            case OrderAccountType.ASX:
                                                {
                                                    var asxItem = item as ASXOrderItem;

                                                    if (asxItem != null)
                                                    {
                                                        DataRow itemDr = ds.ASXTable.NewRow();
                                                        itemDr[ds.ASXTable.ID] = asxItem.ID;
                                                        itemDr[ds.ASXTable.ORDERID] = asxItem.OrderID;

                                                        itemDr[ds.ASXTable.VALIDATIONMSG] = asxItem.ValidationMessage;
                                                        if (asxItem.Units.HasValue)
                                                        {
                                                            itemDr[ds.ASXTable.UNITS] = asxItem.Units;
                                                        }
                                                        if (asxItem.Amount.HasValue)
                                                        {
                                                            itemDr[ds.ASXTable.AMOUNT] = asxItem.Amount;
                                                        }

                                                        itemDr[ds.ASXTable.CREATEDDATE] = asxItem.CreatedDate;
                                                        if (asxItem.UpdatedDate.HasValue)
                                                        {
                                                            itemDr[ds.ASXTable.UPDATEDDATE] = asxItem.UpdatedDate;
                                                        }
                                                        itemDr[ds.ASXTable.INVESTMENTCODE] = asxItem.InvestmentCode;
                                                        itemDr[ds.ASXTable.PRODUCTID] = asxItem.ProductID;
                                                        itemDr[ds.ASXTable.ASXACCOUNTNO] = asxItem.ASXAccountNo;
                                                        itemDr[ds.ASXTable.ASXACCOUNTCID] = asxItem.ASXAccountCID;
                                                        itemDr[ds.ASXTable.INVESTMENTNAME] = GetFundName(asxItem.InvestmentCode);
                                                        itemDr[ds.ASXTable.SMATRANSACTIONID] = item.SMATransactionID;
                                                        if (asxItem.UnitPrice.HasValue)
                                                            itemDr[ds.ASXTable.ASXUNITPRICE] = asxItem.UnitPrice;
                                                        ds.ASXTable.Rows.Add(itemDr);
                                                    }
                                                }
                                                break;
                                            case OrderAccountType.AtCall:
                                                {
                                                    var atCallItem = item as AtCallOrderItem;

                                                    if (atCallItem != null)
                                                    {
                                                        DataRow itemDr = ds.AtCallTable.NewRow();
                                                        itemDr[ds.AtCallTable.ID] = atCallItem.ID;
                                                        itemDr[ds.AtCallTable.ORDERID] = atCallItem.OrderID;

                                                        itemDr[ds.AtCallTable.VALIDATIONMSG] = atCallItem.ValidationMessage;

                                                        if (atCallItem.Amount.HasValue)
                                                        {
                                                            itemDr[ds.AtCallTable.AMOUNT] = atCallItem.Amount;
                                                        }

                                                        itemDr[ds.AtCallTable.CREATEDDATE] = atCallItem.CreatedDate;
                                                        if (atCallItem.UpdatedDate.HasValue)
                                                        {
                                                            itemDr[ds.AtCallTable.UPDATEDDATE] = atCallItem.UpdatedDate;
                                                        }
                                                        itemDr[ds.AtCallTable.TRANSFERTO] = atCallItem.TransferTo;
                                                        itemDr[ds.AtCallTable.TRANSFERACCCID] = atCallItem.TransferAccCID;
                                                        itemDr[ds.AtCallTable.ISEXTERNALACCOUNT] = atCallItem.IsExternalAccount;
                                                        itemDr[ds.AtCallTable.TRANSFERACCNUMBER] = atCallItem.TransferAccNumber;
                                                        itemDr[ds.AtCallTable.TRANSFERACCBSB] = atCallItem.TransferAccBSB;
                                                        itemDr[ds.AtCallTable.TRANSFERNARRATION] = atCallItem.TransferNarration;
                                                        itemDr[ds.AtCallTable.FROMNARRATION] = atCallItem.FromNarration;

                                                        itemDr[ds.AtCallTable.BROKERID] = atCallItem.BrokerID;
                                                        itemDr[ds.AtCallTable.BROKERNAME] = OrderPadUtilities.GetInstituteName(Broker, atCallItem.BrokerID, true);
                                                        itemDr[ds.AtCallTable.BANKID] = atCallItem.BankID;
                                                        itemDr[ds.AtCallTable.BANKNAME] = OrderPadUtilities.GetInstituteName(Broker, atCallItem.BankID, true);
                                                        itemDr[ds.AtCallTable.ACCOUNTTIER] = atCallItem.AccountTier;
                                                        itemDr[ds.AtCallTable.SMATRANSACTIONID] = item.SMATransactionID;
                                                        if (atCallItem.Min.HasValue)
                                                            itemDr[ds.AtCallTable.MIN] = atCallItem.Min;
                                                        if (atCallItem.Max.HasValue)
                                                            itemDr[ds.AtCallTable.MAX] = atCallItem.Max;
                                                        if (atCallItem.Rate.HasValue)
                                                            itemDr[ds.AtCallTable.RATE] = atCallItem.Rate;
                                                        else
                                                            itemDr[ds.AtCallTable.RATE] = atCallItem.TransferNarration;
                                                        itemDr[ds.AtCallTable.PRODUCTID] = atCallItem.ProductID;
                                                        itemDr[ds.AtCallTable.ATCALLACCOUNTCID] = atCallItem.AtCallAccountCID;

                                                        itemDr[ds.AtCallTable.ATCALLTYPE] = atCallItem.AtCallType;

                                                        ds.AtCallTable.Rows.Add(itemDr);
                                                    }
                                                }
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                        break;
                    case DatasetCommandTypes.Check:
                        DataRow orderCMDr = ds.OrdersTable.NewRow();
                        orderCMDr[ds.OrdersTable.CID] = CID;
                        orderCMDr[ds.OrdersTable.CSID] = CSID;
                        orderCMDr[ds.OrdersTable.CLID] = Clid;
                        ds.OrdersTable.Rows.Add(orderCMDr);
                        break;
                }
            }
            else if (data is SettledUnsetteledDS)
            {
                var ds = data as SettledUnsetteledDS;

                switch (ds.CommandType)
                {
                    case DatasetCommandTypes.Get:
                        if (Orders != null)
                        {
                            foreach (DataRow row in ds.SettledUnsetteledTable.Rows)
                            {
                                var order = Orders.FirstOrDefault(ss => ss.ID == Guid.Parse(row[ds.SettledUnsetteledTable.ORDERID].ToString()));
                                if (order != null)
                                {
                                    row[ds.SettledUnsetteledTable.ORDERSTATUS] = order.Status;
                                    row[ds.SettledUnsetteledTable.ORDERCREATEDBY] = order.CreatedBy;
                                    row[ds.SettledUnsetteledTable.ORDERPREFEREDBY] = order.OrderPreferedBy;
                                    row[ds.SettledUnsetteledTable.ORDERBANKACCOUNTTYPE] = order.OrderBankAccountType;
                                }
                            }
                        }
                        break;
                    case DatasetCommandTypes.Unsettle:
                        if (Orders != null)
                        {
                            foreach (DataRow row in ds.SettledUnsetteledTable.Rows)
                            {
                                var order = Orders.FirstOrDefault(ss => ss.ID == Guid.Parse(row[ds.SettledUnsetteledTable.ORDERID].ToString()));
                                if (order != null)
                                {
                                    row[ds.SettledUnsetteledTable.ORDERPREFEREDBY] = order.OrderPreferedBy;
                                }
                            }
                        }
                        break;
                }
            }
            else if (data is BulkOrderDS)
            {
                var ds = data as BulkOrderDS;

                switch (ds.CommandType)
                {
                    case DatasetCommandTypes.Get:
                        if (BulkOrders != null)
                        {
                            var bulkOrders = BulkOrders;

                            foreach (var bulkOrder in bulkOrders)
                            {
                                DataRow orderDr = ds.BulkOrdersTable.NewRow();

                                orderDr[ds.BulkOrdersTable.ID] = bulkOrder.ID;
                                orderDr[ds.BulkOrdersTable.BULKORDERID] = bulkOrder.BulkOrderID;
                                orderDr[ds.BulkOrdersTable.STATUS] = bulkOrder.Status.ToString();
                                orderDr[ds.BulkOrdersTable.ORDERTYPE] = bulkOrder.OrderType.ToString();
                                orderDr[ds.BulkOrdersTable.ORDERACCOUNTTYPE] = bulkOrder.OrderAccountType.ToString();
                                orderDr[ds.BulkOrdersTable.ORDERITEMTYPE] = bulkOrder.OrderItemType.ToString();
                                orderDr[ds.BulkOrdersTable.CREATEDDATE] = bulkOrder.CreatedDate;
                                if (bulkOrder.UpdatedDate.HasValue)
                                {
                                    orderDr[ds.BulkOrdersTable.UPDATEDDATE] = bulkOrder.UpdatedDate;
                                }
                                orderDr[ds.BulkOrdersTable.UPDATEDBY] = bulkOrder.UpdatedBy;
                                orderDr[ds.BulkOrdersTable.UPDATEDBYCID] = bulkOrder.UpdatedByCID;
                                orderDr[ds.BulkOrdersTable.ISEXPORTED] = bulkOrder.IsExported;
                                if (bulkOrder.TradeDate.HasValue)
                                {
                                    orderDr[ds.BulkOrdersTable.TRADEDATE] = bulkOrder.TradeDate;
                                }
                                orderDr[ds.BulkOrdersTable.CLIENTMANAGEMENTTYPE] = bulkOrder.ClientManagementType;
                                orderDr[ds.BulkOrdersTable.ORDERPREFEREDBY] = bulkOrder.OrderPreferedBy;
                                orderDr[ds.BulkOrdersTable.SMAREQUESTID] = bulkOrder.SMARequestID;
                                orderDr[ds.BulkOrdersTable.SMACALLSTATUS] = bulkOrder.SMACallStatus;
                                ds.BulkOrdersTable.Rows.Add(orderDr);

                                //ASX Bulk Items
                                if (bulkOrder.BulkOrderItems != null)
                                {
                                    foreach (var item in bulkOrder.BulkOrderItems)
                                    {
                                        DataRow orderItemDr = ds.BulkOrderItemsTable.NewRow();

                                        orderItemDr[ds.BulkOrderItemsTable.ID] = item.ID;
                                        orderItemDr[ds.BulkOrderItemsTable.BULKORDERID] = item.BulkOrderID;

                                        foreach (var orderItem in item.OrderItems)
                                        {
                                            orderItemDr[ds.BulkOrderItemsTable.ORDERITEMIDS] += string.Format("{0},", orderItem.OrderItemID);

                                            DataRow orderItemDetailDr = ds.BulkOrderItemDetailsList.NewRow();

                                            orderItemDetailDr[ds.BulkOrderItemDetailsList.BULKORDERID] = item.BulkOrderID;
                                            orderItemDetailDr[ds.BulkOrderItemDetailsList.BULKORDERITEMID] = item.ID;
                                            orderItemDetailDr[ds.BulkOrderItemDetailsList.ORDERID] = orderItem.OrderID;
                                            orderItemDetailDr[ds.BulkOrderItemDetailsList.ORDERITEMID] = orderItem.OrderItemID;
                                            orderItemDetailDr[ds.BulkOrderItemDetailsList.CODE] = item.Code;
                                            orderItemDetailDr[ds.BulkOrderItemDetailsList.CURRENCY] = GetCurrencyCode(item.Code);

                                            var order = Orders.FirstOrDefault(ss => ss.ID == orderItem.OrderID);
                                            if (order != null)
                                            {
                                                orderItemDetailDr[ds.BulkOrderItemDetailsList.ORDERNO] = order.OrderID;
                                                orderItemDetailDr[ds.BulkOrderItemDetailsList.ORDERSTATUS] = order.Status;
                                                orderItemDetailDr[ds.BulkOrderItemDetailsList.CLIENTID] = order.ClientID;
                                                orderItemDetailDr[ds.BulkOrderItemDetailsList.CLIENTCID] = order.ClientCID;
                                                orderItemDetailDr[ds.BulkOrderItemDetailsList.ORDERITEMTYPE] = order.OrderItemType;

                                                var itemOrder = order.Items.FirstOrDefault(ss => ss.ID == orderItem.OrderItemID);
                                                var asxItem = itemOrder as ASXOrderItem;
                                                if (asxItem != null)
                                                {
                                                    orderItemDetailDr[ds.BulkOrderItemDetailsList.UNITPRICE] = asxItem.SuggestedUnitPrice;
                                                    orderItemDetailDr[ds.BulkOrderItemDetailsList.UNITS] = asxItem.SuggestedUnits;
                                                    orderItemDetailDr[ds.BulkOrderItemDetailsList.AMOUNT] = asxItem.SuggestedAmount;
                                                    orderItemDetailDr[ds.BulkOrderItemDetailsList.ACTUALUNITPRICE] = asxItem.UnitPrice;
                                                    orderItemDetailDr[ds.BulkOrderItemDetailsList.ACTUALUNITS] = asxItem.Units;
                                                    orderItemDetailDr[ds.BulkOrderItemDetailsList.ACTUALAMOUNT] = asxItem.Amount;
                                                }
                                            }
                                            ds.BulkOrderItemDetailsList.Rows.Add(orderItemDetailDr);
                                        }

                                        orderItemDr[ds.BulkOrderItemsTable.CODE] = item.Code;
                                        orderItemDr[ds.BulkOrderItemsTable.EXPECTEDUNITPRICE] = item.ExpectedUnitPrice;
                                        orderItemDr[ds.BulkOrderItemsTable.EXPECTEDUNITS] = item.ExpectedUnits;
                                        orderItemDr[ds.BulkOrderItemsTable.EXPECTEDAMOUNT] = item.ExpectedAmount;
                                        if (item.ActualUnitPrice.HasValue)
                                        {
                                            orderItemDr[ds.BulkOrderItemsTable.ACTUALUNITPRICE] = item.ActualUnitPrice;
                                        }
                                        if (item.ActualUnits.HasValue)
                                        {
                                            orderItemDr[ds.BulkOrderItemsTable.ACTUALUNITS] = item.ActualUnits;
                                        }
                                        if (item.ActualAmount.HasValue)
                                        {
                                            orderItemDr[ds.BulkOrderItemsTable.ACTUALAMOUNT] = item.ActualAmount;
                                        }
                                        orderItemDr[ds.BulkOrderItemsTable.CREATEDDATE] = item.CreatedDate;
                                        if (item.UpdatedDate.HasValue)
                                        {
                                            orderItemDr[ds.BulkOrderItemsTable.UPDATEDDATE] = item.UpdatedDate;
                                        }
                                        orderItemDr[ds.BulkOrderItemsTable.CONTRACTNOTE] = item.ContractNote;
                                        orderItemDr[ds.BulkOrderItemsTable.ISAPPORTIONED] = item.IsApportioned;
                                        if (item.Brokerage.HasValue)
                                        {
                                            orderItemDr[ds.BulkOrderItemsTable.BROKERAGE] = item.Brokerage;
                                        }
                                        if (item.Charges.HasValue)
                                        {
                                            orderItemDr[ds.BulkOrderItemsTable.CHARGES] = item.Charges;
                                        }
                                        if (item.Tax.HasValue)
                                        {
                                            orderItemDr[ds.BulkOrderItemsTable.TAX] = item.Tax;
                                        }
                                        if (item.GrossValue.HasValue)
                                        {
                                            orderItemDr[ds.BulkOrderItemsTable.GROSSVALUE] = item.GrossValue;
                                        }

                                        ds.BulkOrderItemsTable.Rows.Add(orderItemDr);
                                    }
                                }
                                //TD Bulk Items for SMA
                                else if (bulkOrder.TDBulkOrderItems != null)
                                {
                                    foreach (var item in bulkOrder.TDBulkOrderItems)
                                    {
                                        DataRow orderItemDr = ds.BulkOrderItemsTable.NewRow();

                                        orderItemDr[ds.BulkOrderItemsTable.ID] = item.ID;
                                        orderItemDr[ds.BulkOrderItemsTable.BULKORDERID] = item.BulkOrderID;

                                        foreach (var orderItem in item.OrderItems)
                                        {
                                            orderItemDr[ds.BulkOrderItemsTable.ORDERITEMIDS] += string.Format("{0},", orderItem.OrderItemID);

                                            DataRow orderItemDetailDr = ds.BulkOrderItemDetailsList.NewRow();

                                            orderItemDetailDr[ds.BulkOrderItemDetailsList.BULKORDERID] = item.BulkOrderID;
                                            orderItemDetailDr[ds.BulkOrderItemDetailsList.BULKORDERITEMID] = item.ID;
                                            orderItemDetailDr[ds.BulkOrderItemDetailsList.ORDERID] = orderItem.OrderID;
                                            orderItemDetailDr[ds.BulkOrderItemDetailsList.ORDERITEMID] = orderItem.OrderItemID;

                                            var order = Orders.FirstOrDefault(ss => ss.ID == orderItem.OrderID);
                                            if (order != null)
                                            {
                                                orderItemDetailDr[ds.BulkOrderItemDetailsList.ORDERNO] = order.OrderID;
                                                orderItemDetailDr[ds.BulkOrderItemDetailsList.ORDERSTATUS] = order.Status;
                                                orderItemDetailDr[ds.BulkOrderItemDetailsList.CLIENTID] = order.ClientID;
                                                orderItemDetailDr[ds.BulkOrderItemDetailsList.CLIENTCID] = order.ClientCID;
                                                orderItemDetailDr[ds.BulkOrderItemDetailsList.ORDERITEMTYPE] = order.OrderItemType;

                                                var itemOrder = order.Items.FirstOrDefault(ss => ss.ID == orderItem.OrderItemID);
                                                var tdItem = itemOrder as TermDepositOrderItem;
                                                if (tdItem != null)
                                                {
                                                    orderItemDetailDr[ds.BulkOrderItemDetailsList.BROKERID] = tdItem.BrokerID;
                                                    orderItemDetailDr[ds.BulkOrderItemDetailsList.BROKERNAME] = OrderPadUtilities.GetInstituteName(Broker, tdItem.BrokerID, true);
                                                    orderItemDetailDr[ds.BulkOrderItemDetailsList.INSTITUTEID] = tdItem.BankID;
                                                    orderItemDetailDr[ds.BulkOrderItemDetailsList.INSTITUTENAME] = OrderPadUtilities.GetInstituteName(Broker, tdItem.BankID, true);
                                                    orderItemDetailDr[ds.BulkOrderItemDetailsList.TERM] = tdItem.Duration;
                                                    orderItemDetailDr[ds.BulkOrderItemDetailsList.RATE] = tdItem.Percentage;
                                                    orderItemDetailDr[ds.BulkOrderItemDetailsList.AMOUNT] = tdItem.Amount;
                                                }
                                            }
                                            ds.BulkOrderItemDetailsList.Rows.Add(orderItemDetailDr);
                                        }

                                        orderItemDr[ds.BulkOrderItemsTable.CREATEDDATE] = item.CreatedDate;
                                        if (item.UpdatedDate.HasValue)
                                        {
                                            orderItemDr[ds.BulkOrderItemsTable.UPDATEDDATE] = item.UpdatedDate;
                                        }
                                        orderItemDr[ds.BulkOrderItemsTable.CONTRACTNOTE] = item.ContractNote;
                                        orderItemDr[ds.BulkOrderItemsTable.ISAPPORTIONED] = item.IsApportioned;

                                        orderItemDr[ds.BulkOrderItemsTable.BROKERID] = item.BrokerId;
                                        orderItemDr[ds.BulkOrderItemsTable.BROKERNAME] = OrderPadUtilities.GetInstituteName(Broker, item.BrokerId, true);
                                        orderItemDr[ds.BulkOrderItemsTable.INSTITUTEID] = item.InstituteID;
                                        orderItemDr[ds.BulkOrderItemsTable.INSTITUTENAME] = OrderPadUtilities.GetInstituteName(Broker, item.InstituteID, true);
                                        orderItemDr[ds.BulkOrderItemsTable.TERM] = item.Term;
                                        orderItemDr[ds.BulkOrderItemsTable.RATE] = item.Rate;
                                        orderItemDr[ds.BulkOrderItemsTable.AMOUNT] = item.Amount;


                                        ds.BulkOrderItemsTable.Rows.Add(orderItemDr);
                                    }
                                }
                            }
                        }
                        break;
                }
            }
        }

        protected override ModifiedState OnSetData(DataSet data)
        {
            base.OnSetData(data);
            if (_orders == null)
            {
                _orders = new List<OrderEntity>();
            }
            if (_bulkOrders == null)
            {
                _bulkOrders = new List<BulkOrderEntity>();
            }

            if (data is OrderPadDS)
            {
                var ds = data as OrderPadDS;

                switch (ds.CommandType)
                {
                    case DatasetCommandTypes.Add:// adding new Order CM
                        if (ds.OrdersTable.Rows.Count > 0)
                        {
                            var message = new StringBuilder();

                            foreach (DataRow dr in ds.OrdersTable.Rows)
                            {
                                //Setting Order ID/No to start from 1111
                                long newOrderID = 1111;
                                if (Orders.Count() > 0)
                                {
                                    newOrderID = Orders.Max(ss => ss.OrderID) + 1;
                                }
                                var order = new OrderEntity
                                                {
                                                    ID = dr[ds.OrdersTable.ID] != DBNull.Value ? Guid.Parse(dr[ds.OrdersTable.ID].ToString()) : Guid.NewGuid(),
                                                    OrderID = newOrderID,
                                                    CreatedBy = ds.Unit.CurrentUser.CurrentUserName,
                                                    CreatedByCID = ds.Unit.CurrentUser.CID,
                                                    CreatedDate = dr[ds.OrdersTable.CREATEDDATE] != DBNull.Value ? DateTime.Parse(dr[ds.OrdersTable.CREATEDDATE].ToString()) : DateTime.Now,
                                                    TradeDate = dr[ds.OrdersTable.TRADEDATE] != DBNull.Value ? DateTime.Parse(dr[ds.OrdersTable.TRADEDATE].ToString()) : DateTime.Now,
                                                    ClientCID = Guid.Parse(dr[ds.OrdersTable.CLIENTCID].ToString()),
                                                    ClientID = dr[ds.OrdersTable.CLIENTID].ToString(),
                                                    FileName = dr[ds.OrdersTable.FILENAME] != DBNull.Value ? dr[ds.OrdersTable.FILENAME].ToString() : String.Empty,
                                                    OrderAccountType = (OrderAccountType)System.Enum.Parse(typeof(OrderAccountType), dr[ds.OrdersTable.ORDERACCOUNTTYPE].ToString(), true),
                                                    OrderType = (OrderType)System.Enum.Parse(typeof(OrderType), dr[ds.OrdersTable.ORDERTYPE].ToString(), true),
                                                    Status = (OrderStatus)System.Enum.Parse(typeof(OrderStatus), dr[ds.OrdersTable.STATUS].ToString(), true),
                                                    Attachment = dr[ds.OrdersTable.ATTACHMENT] != DBNull.Value ? Convert.FromBase64String(dr[ds.OrdersTable.ATTACHMENT].ToString()) : null,
                                                    AccountCID = dr[ds.OrdersTable.ACCOUNTCID] != DBNull.Value ? Guid.Parse(dr[ds.OrdersTable.ACCOUNTCID].ToString()) : Guid.Empty,
                                                    AccountNumber = dr[ds.OrdersTable.ACCOUNTNUMBER].ToString(),
                                                    AccountBSB = dr[ds.OrdersTable.ACCOUNTBSB].ToString(),
                                                    OrderItemType = (OrderItemType)System.Enum.Parse(typeof(OrderItemType), dr[ds.OrdersTable.ORDERITEMTYPE].ToString(), true),
                                                    Items = new List<OrderItemEntity>(),
                                                    Instructions = new List<OrderInstructionEntity>(),
                                                    OrderBulkStatus = OrderBulkStatus.None,
                                                    ClientManagementType = (ClientManagementType)System.Enum.Parse(typeof(ClientManagementType), dr[ds.OrdersTable.CLIENTMANAGEMENTTYPE].ToString(), true),
                                                    OrderPreferedBy = dr[ds.OrdersTable.ORDERPREFEREDBY] != DBNull.Value ? (OrderPreferedBy)System.Enum.Parse(typeof(OrderPreferedBy), dr[ds.OrdersTable.ORDERPREFEREDBY].ToString()) : OrderPreferedBy.Amount,
                                                    OrderBankAccountType = dr[ds.OrdersTable.ORDERBANKACCOUNTTYPE] != DBNull.Value ? (OrderBankAccountType)System.Enum.Parse(typeof(OrderBankAccountType), dr[ds.OrdersTable.ORDERBANKACCOUNTTYPE].ToString()) : OrderBankAccountType.Internal,
                                                };

                                switch (order.OrderAccountType)
                                {
                                    case OrderAccountType.StateStreet:
                                        {
                                            if (ds.StateStreetTable.Rows.Count > 0)
                                            {
                                                DataRow[] drs = ds.StateStreetTable.Select(String.Format("{0}='{1}'", ds.StateStreetTable.ORDERID, order.ID));

                                                foreach (var row in drs)
                                                {
                                                    var orderitem = new StateStreetOrderItem
                                                    {

                                                        Amount = row[ds.StateStreetTable.AMOUNT] != DBNull.Value ? Decimal.Parse(row[ds.StateStreetTable.AMOUNT].ToString()) : (decimal?)null,
                                                        Units = row[ds.StateStreetTable.UNITS] != DBNull.Value ? Decimal.Parse(row[ds.StateStreetTable.UNITS].ToString()) : (decimal?)null,
                                                        CreatedDate = row[ds.StateStreetTable.CREATEDDATE] != DBNull.Value ? DateTime.Parse(row[ds.StateStreetTable.CREATEDDATE].ToString()) : DateTime.Now,
                                                        ID = row[ds.StateStreetTable.ID] != DBNull.Value ? Guid.Parse(row[ds.StateStreetTable.ID].ToString()) : Guid.NewGuid(),
                                                        OrderID = Guid.Parse(row[ds.StateStreetTable.ORDERID].ToString()),
                                                        ProductID = Guid.Parse(row[ds.StateStreetTable.PRODUCTID].ToString()),
                                                        FundCode = row[ds.StateStreetTable.FUNDCODE].ToString(),
                                                    };
                                                    order.Items.Add(orderitem);
                                                }
                                            }
                                        }
                                        break;
                                    case OrderAccountType.TermDeposit:
                                        {
                                            if (ds.TermDepositTable.Rows.Count > 0)
                                            {
                                                DataRow[] drs = ds.TermDepositTable.Select(String.Format("{0}='{1}'", ds.TermDepositTable.ORDERID, order.ID));

                                                foreach (var row in drs)
                                                {
                                                    var orderitem = new TermDepositOrderItem
                                                    {
                                                        Amount = row[ds.TermDepositTable.AMOUNT] != DBNull.Value ? Decimal.Parse(row[ds.TermDepositTable.AMOUNT].ToString()) : (decimal?)null,
                                                        Units = row[ds.TermDepositTable.UNITS] != DBNull.Value ? Decimal.Parse(row[ds.TermDepositTable.UNITS].ToString()) : (decimal?)null,
                                                        CreatedDate = row[ds.TermDepositTable.CREATEDDATE] != DBNull.Value ? DateTime.Parse(row[ds.TermDepositTable.CREATEDDATE].ToString()) : DateTime.Now,
                                                        ID = row[ds.TermDepositTable.ID] != DBNull.Value ? Guid.Parse(row[ds.TermDepositTable.ID].ToString()) : Guid.NewGuid(),
                                                        OrderID = Guid.Parse(row[ds.TermDepositTable.ORDERID].ToString()),
                                                        BrokerID = new Guid(row[ds.TermDepositTable.BROKERID].ToString()),
                                                        BankID = new Guid(row[ds.TermDepositTable.BANKID].ToString()),
                                                        Min = row[ds.TermDepositTable.MIN] != DBNull.Value ? Convert.ToDecimal(row[ds.TermDepositTable.MIN].ToString()) : (decimal?)null,
                                                        Max = row[ds.TermDepositTable.MAX] != DBNull.Value ? Convert.ToDecimal(row[ds.TermDepositTable.MAX].ToString()) : (decimal?)null,
                                                        Rating = row[ds.TermDepositTable.RATING].ToString(),
                                                        Duration = row[ds.TermDepositTable.DURATION].ToString(),
                                                        Percentage = row[ds.TermDepositTable.PERCENTAGE] != DBNull.Value ? Convert.ToDecimal(row[ds.TermDepositTable.PERCENTAGE].ToString()) : (decimal?)null,
                                                        ProductID = row[ds.TermDepositTable.PRODUCTID] != DBNull.Value ? Guid.Parse(row[ds.TermDepositTable.PRODUCTID].ToString()) : Guid.Empty,
                                                        TDAccountCID = row[ds.TermDepositTable.TDACCOUNTCID] != DBNull.Value ? Guid.Parse(row[ds.TermDepositTable.TDACCOUNTCID].ToString()) : Guid.Empty,
                                                        ContractNote = row[ds.TermDepositTable.CONTRACTNOTE].ToString(),
                                                    };
                                                    order.Items.Add(orderitem);
                                                }
                                            }
                                        }
                                        break;
                                    case OrderAccountType.ASX:
                                        {
                                            if (ds.ASXTable.Rows.Count > 0)
                                            {
                                                DataRow[] drs = ds.ASXTable.Select(String.Format("{0}='{1}'", ds.ASXTable.ORDERID, order.ID));

                                                foreach (var row in drs)
                                                {
                                                    var orderitem = new ASXOrderItem
                                                    {
                                                        Amount = row[ds.ASXTable.AMOUNT] != DBNull.Value ? Decimal.Parse(row[ds.ASXTable.AMOUNT].ToString()) : (decimal?)null,
                                                        Units = row[ds.ASXTable.UNITS] != DBNull.Value ? Decimal.Parse(row[ds.ASXTable.UNITS].ToString()) : (decimal?)null,
                                                        CreatedDate = row[ds.ASXTable.CREATEDDATE] != DBNull.Value ? DateTime.Parse(row[ds.ASXTable.CREATEDDATE].ToString()) : DateTime.Now,
                                                        ID = row[ds.ASXTable.ID] != DBNull.Value ? Guid.Parse(row[ds.ASXTable.ID].ToString()) : Guid.NewGuid(),
                                                        OrderID = Guid.Parse(row[ds.ASXTable.ORDERID].ToString()),
                                                        InvestmentCode = row[ds.ASXTable.INVESTMENTCODE].ToString(),
                                                        ProductID = row[ds.ASXTable.PRODUCTID] != DBNull.Value ? Guid.Parse(row[ds.ASXTable.PRODUCTID].ToString()) : Guid.Empty,
                                                        ASXAccountNo = row[ds.ASXTable.ASXACCOUNTNO].ToString(),
                                                        ParentAcc = "142639", // Account number of innova in desktop broker.
                                                        ASXAccountCID = row[ds.ASXTable.ASXACCOUNTCID] != DBNull.Value ? Guid.Parse(row[ds.ASXTable.ASXACCOUNTCID].ToString()) : Guid.Empty,
                                                        UnitPrice = row[ds.ASXTable.ASXUNITPRICE] != DBNull.Value ? Decimal.Parse(row[ds.ASXTable.ASXUNITPRICE].ToString()) : (decimal?)null,
                                                    };
                                                    if (order.OrderType == OrderType.Manual)
                                                    {
                                                        orderitem.SuggestedUnitPrice = orderitem.UnitPrice;
                                                        orderitem.SuggestedUnits = orderitem.Units;
                                                        orderitem.SuggestedAmount = order.OrderItemType == OrderItemType.Sell ? orderitem.Amount * -1 : orderitem.Amount;
                                                    }

                                                    order.Items.Add(orderitem);
                                                }
                                            }
                                        }
                                        break;
                                    case OrderAccountType.AtCall:
                                        {
                                            if (ds.AtCallTable.Rows.Count > 0)
                                            {
                                                DataRow[] drs = ds.AtCallTable.Select(String.Format("{0}='{1}'", ds.AtCallTable.ORDERID, order.ID));

                                                foreach (var row in drs)
                                                {
                                                    var orderitem = new AtCallOrderItem
                                                    {
                                                        Amount = row[ds.AtCallTable.AMOUNT] != DBNull.Value ? Decimal.Parse(row[ds.AtCallTable.AMOUNT].ToString()) : (decimal?)null,
                                                        CreatedDate = row[ds.AtCallTable.CREATEDDATE] != DBNull.Value ? DateTime.Parse(row[ds.AtCallTable.CREATEDDATE].ToString()) : DateTime.Now,
                                                        ID = row[ds.AtCallTable.ID] != DBNull.Value ? Guid.Parse(row[ds.AtCallTable.ID].ToString()) : Guid.NewGuid(),
                                                        OrderID = Guid.Parse(row[ds.AtCallTable.ORDERID].ToString()),
                                                    };

                                                    if (row[ds.AtCallTable.ATCALLTYPE].ToString() == AtCallType.MoneyMovement.ToString())
                                                    {
                                                        orderitem.TransferTo = (AtCallAccountType)System.Enum.Parse(typeof(AtCallAccountType), row[ds.AtCallTable.TRANSFERTO].ToString(), true);
                                                        orderitem.TransferAccCID = row[ds.AtCallTable.TRANSFERACCCID] != DBNull.Value ? Guid.Parse(row[ds.AtCallTable.TRANSFERACCCID].ToString()) : Guid.Empty;
                                                        orderitem.IsExternalAccount = row[ds.AtCallTable.ISEXTERNALACCOUNT] != DBNull.Value ? bool.Parse(row[ds.AtCallTable.ISEXTERNALACCOUNT].ToString()) : false;
                                                        orderitem.TransferAccNumber = row[ds.AtCallTable.TRANSFERACCNUMBER].ToString();
                                                        orderitem.TransferAccBSB = row[ds.AtCallTable.TRANSFERACCBSB].ToString();
                                                        orderitem.AtCallType = AtCallType.MoneyMovement;
                                                        orderitem.FromNarration = row[ds.AtCallTable.FROMNARRATION].ToString();
                                                        orderitem.TransferNarration = row[ds.AtCallTable.TRANSFERNARRATION].ToString();
                                                    }
                                                    else if (row[ds.AtCallTable.ATCALLTYPE].ToString() == AtCallType.BestRates.ToString() || row[ds.AtCallTable.ATCALLTYPE].ToString() == AtCallType.MoneyMovementBroker.ToString())
                                                    {
                                                        orderitem.BrokerID = row[ds.AtCallTable.BROKERID] != DBNull.Value ? new Guid(row[ds.AtCallTable.BROKERID].ToString()) : Guid.Empty;
                                                        orderitem.BankID = row[ds.AtCallTable.BANKID] != DBNull.Value ? new Guid(row[ds.AtCallTable.BANKID].ToString()) : Guid.Empty;
                                                        orderitem.Min = row[ds.AtCallTable.MIN] != DBNull.Value ? Convert.ToDecimal(row[ds.AtCallTable.MIN].ToString()) : (decimal?)null;
                                                        orderitem.Max = row[ds.AtCallTable.MAX] != DBNull.Value ? Convert.ToDecimal(row[ds.AtCallTable.MAX].ToString()) : (decimal?)null;
                                                        orderitem.Rate = (decimal?)null;
                                                        orderitem.ProductID = row[ds.AtCallTable.PRODUCTID] != DBNull.Value ? Guid.Parse(row[ds.AtCallTable.PRODUCTID].ToString()) : Guid.Empty;
                                                        orderitem.AtCallAccountCID = row[ds.AtCallTable.ATCALLACCOUNTCID] != DBNull.Value ? Guid.Parse(row[ds.AtCallTable.ATCALLACCOUNTCID].ToString()) : Guid.Empty;
                                                        orderitem.AccountTier = row[ds.AtCallTable.ACCOUNTTIER] != DBNull.Value ? row[ds.AtCallTable.ACCOUNTTIER].ToString() : string.Empty;

                                                        if (row[ds.AtCallTable.ATCALLTYPE].ToString() == AtCallType.BestRates.ToString())
                                                        {
                                                            orderitem.AtCallType = AtCallType.BestRates;
                                                            orderitem.TransferNarration = row[ds.AtCallTable.RATE].ToString();
                                                        }
                                                        else if (row[ds.AtCallTable.ATCALLTYPE].ToString() == AtCallType.MoneyMovementBroker.ToString())
                                                        {
                                                            orderitem.AtCallType = AtCallType.MoneyMovementBroker;
                                                        }
                                                    }

                                                    order.Items.Add(orderitem);
                                                }
                                            }
                                        }
                                        break;
                                }

                                Orders.Add(order);


                                //Validating Orders upon creation as requested in Task 806
                                var orders = Orders.Where(ss => ss.Status < OrderStatus.Validated && ss.ID == order.ID);
                                if (orders.Count() > 0)
                                {
                                    var smaResults = OrderPadUtilities.ValidateOrders(orders.ToList(), Broker, ds.Unit.CurrentUser);
                                    if (smaResults != null && smaResults.Count > 0)
                                    {
                                        if (!ds.ExtendedProperties.Contains("SMAMessages"))
                                        {
                                            ds.ExtendedProperties.Add("SMAMessages", smaResults);
                                        }
                                        else
                                        {
                                            var results = ds.ExtendedProperties["SMAMessages"] as Dictionary<long, string>;
                                            if (results != null)
                                            {
                                                results.Merge(smaResults);
                                            }
                                            ds.ExtendedProperties["SMAMessages"] = results;

                                        }
                                    }
                                }
                                switch (order.Status)
                                {
                                    case OrderStatus.Validated:
                                        message.AppendLine(String.Format("Order {0} is saved and validated successfully.", newOrderID));
                                        break;
                                    case OrderStatus.ValidationFailed:
                                        message.AppendLine(String.Format(IsProductIdEmpty(order) ? "Order {0} is saved successfully." : "Order {0} is saved but validation is failed.", newOrderID));
                                        break;
                                }

                                //ASX Bulk Order for Manual Buy/Sell; TD Bulk Orders for SMA
                                var bulkOrders = Orders.Where(o => o.ID == order.ID && (o.OrderAccountType == OrderAccountType.ASX || (o.OrderAccountType == OrderAccountType.TermDeposit && o.ClientManagementType == ClientManagementType.SMA)) && o.OrderType == OrderType.Manual && o.Status == OrderStatus.Validated && o.OrderBulkStatus < OrderBulkStatus.Batched);
                                CreateBulkOrder(BulkOrders, bulkOrders.ToList());
                            }
                            ds.ExtendedProperties.Add("Result", OperationResults.Successfull);
                            ds.ExtendedProperties.Add("Message", message.ToString());

                        }
                        break;
                    case DatasetCommandTypes.Validate://Validate Orders
                        if (Orders != null)
                        {
                            var finSimpOrders = Orders.Where(ss => ss.ClientManagementType == ds.ClientManagementType && (ss.OrderAccountType == OrderAccountType.ASX || ss.OrderAccountType == OrderAccountType.StateStreet) && ss.OrderType == OrderType.FinSimplicity && (ss.Status == OrderStatus.Active || ss.Status == OrderStatus.Validated || ss.Status == OrderStatus.ValidationFailed)).GroupBy(ss => ss.ClientID);
                            foreach (var orderEntity in finSimpOrders)
                            {
                                foreach (var entity in orderEntity)
                                {
                                    entity.Status = OrderStatus.Approved;
                                }
                                OrderPadUtilities.RemoveNCreateSettledUnsettledForClientOrders(orderEntity.Key, orderEntity.ToList(), Broker, ds.Unit.CurrentUser, true);
                            }

                            var orders = Orders.Where(ss => ss.ClientManagementType == ds.ClientManagementType && ss.Status < OrderStatus.Validated);
                            if (ds.Unit != null && !String.IsNullOrEmpty(ds.Unit.ClientId))
                            {
                                orders = orders.Where(ss => ss.ClientID == ds.Unit.ClientId).ToList();
                            }

                            var smaResults = OrderPadUtilities.ValidateOrders(orders.ToList(), Broker, ds.Unit.CurrentUser);
                            if (smaResults != null && smaResults.Count > 0)
                            {
                                ds.ExtendedProperties.Add("SMAMessages", smaResults);
                            }
                            //ASX Bulk Order for Manual Buy/Sell; TD Bulk Orders for SMA
                            var bulkOrders = Orders.Where(o => o.ClientManagementType == ds.ClientManagementType && (o.OrderAccountType == OrderAccountType.ASX || (o.OrderAccountType == OrderAccountType.TermDeposit && o.ClientManagementType == ClientManagementType.SMA)) && o.OrderType == OrderType.Manual && o.Status == OrderStatus.Validated && o.OrderBulkStatus < OrderBulkStatus.Batched);
                            CreateBulkOrder(BulkOrders, bulkOrders.ToList());
                        }
                        break;
                    case DatasetCommandTypes.Approve://Approve Orders
                        if (Orders != null)
                        {
                            if (ds.OrdersTable.Rows.Count > 0)
                            {
                                DataRow dr = ds.OrdersTable.Rows[0];
                                var id = Guid.Parse(dr[ds.OrdersTable.ID].ToString());

                                if (ds.Unit != null && id != Guid.Empty)
                                {
                                    var order = Orders.FirstOrDefault(ss => ss.ID == id && ss.Status == OrderStatus.Validated);
                                    if (order != null)
                                    {
                                        OrderPadUtilities.ApproveOrder(order, Broker, ds.Unit.CurrentUser);
                                    }
                                }
                            }
                        }
                        break;
                    case DatasetCommandTypes.Cancel://Cancel Order
                        if (Orders != null)
                        {
                            if (ds.OrdersTable.Rows.Count > 0)
                            {
                                DataRow dr = ds.OrdersTable.Rows[0];
                                var id = Guid.Parse(dr[ds.OrdersTable.ID].ToString());
                                var comments = dr[ds.OrdersTable.COMMENTS].ToString();

                                if (ds.Unit != null && id != Guid.Empty)
                                {
                                    var order = Orders.FirstOrDefault(ss => ss.ID == id && ss.Status < OrderStatus.Complete);
                                    if (order != null)
                                    {
                                        //checking for order status before updating it
                                        if (order.Status == OrderStatus.Submitted && (order.OrderAccountType == OrderAccountType.AtCall || order.OrderAccountType == OrderAccountType.TermDeposit))
                                        {
                                            //Removing entries from Transactions -> Term Deposit 
                                            OrderPadUtilities.RemoveTransaction(order, Broker, ds.Unit.CurrentUser);
                                        }
                                        order.Status = OrderStatus.Cancelled;
                                        order.UpdatedDate = DateTime.Now;
                                        order.UpdatedBy = ds.Unit.CurrentUser.CurrentUserName;
                                        order.UpdatedByCID = ds.Unit.CurrentUser.CID;
                                        order.Comments = comments;
                                        //Removing entries from settled unsettled
                                        OrderPadUtilities.RemoveNCreateSettledUnsettledEntities(order, Broker, ds.Unit.CurrentUser);
                                        //Removing entries from Bulk Orders
                                        RemoveOrderFromBulkOrders(order);
                                    }
                                }
                            }
                        }
                        break;
                    case DatasetCommandTypes.Settle://Complete Order
                        if (Orders != null)
                        {
                            if (ds.OrdersTable.Rows.Count > 0)
                            {
                                DataRow dr = ds.OrdersTable.Rows[0];
                                var id = Guid.Parse(dr[ds.OrdersTable.ID].ToString());

                                if (ds.Unit != null && id != Guid.Empty)
                                {
                                    var order = Orders.FirstOrDefault(ss => ss.ID == id && ss.Status == OrderStatus.Submitted);
                                    if (order != null)
                                    {
                                        order.Status = OrderStatus.Complete;
                                        order.UpdatedDate = DateTime.Now;
                                        order.UpdatedBy = ds.Unit.CurrentUser.CurrentUserName;
                                        order.UpdatedByCID = ds.Unit.CurrentUser.CID;
                                    }
                                }
                            }
                        }
                        break;
                    case DatasetCommandTypes.Delete://Delete Order
                        if (Orders != null)
                        {
                            if (ds.OrdersTable.Rows.Count > 0)
                            {
                                DataRow dr = ds.OrdersTable.Rows[0];
                                var id = Guid.Parse(dr[ds.OrdersTable.ID].ToString());
                                if (ds.Unit != null && id != Guid.Empty)
                                {
                                    var order = Orders.FirstOrDefault(ss => ss.ID == id && ss.Status == OrderStatus.Cancelled);
                                    if (order != null)
                                    {
                                        //Removing entries from settled unsettled if any
                                        OrderPadUtilities.RemoveNCreateSettledUnsettledEntities(order, Broker, ds.Unit.CurrentUser);
                                        Orders.Remove(order);
                                    }
                                }
                            }
                        }
                        break;
                    case DatasetCommandTypes.Update://Update Order (Manual Status Update)
                        if (Orders != null)
                        {
                            if (ds.OrdersTable.Rows.Count > 0)
                            {
                                DataRow dr = ds.OrdersTable.Rows[0];
                                var id = Guid.Parse(dr[ds.OrdersTable.ID].ToString());
                                var oStatus = dr[ds.OrdersTable.STATUS].ToString();
                                OrderStatus orderStatus;
                                bool isValidOrderStatus = System.Enum.TryParse(oStatus, out orderStatus);
                                if (isValidOrderStatus)
                                {
                                    if (ds.Unit != null && id != Guid.Empty)
                                    {
                                        var order = Orders.FirstOrDefault(ss => ss.ID == id);
                                        if (order != null && (order.Status == OrderStatus.Cancelled || (order.Status <= OrderStatus.Submitted && order.Status > orderStatus)))
                                        {
                                            //checking for order status before updating it
                                            if (order.Status == OrderStatus.Submitted && (order.OrderAccountType == OrderAccountType.AtCall || order.OrderAccountType == OrderAccountType.TermDeposit))
                                            {
                                                //Removing entries from Transactions -> Term Deposit 
                                                OrderPadUtilities.RemoveTransaction(order, Broker, ds.Unit.CurrentUser);
                                            }

                                            //updating order
                                            order.Status = orderStatus;
                                            order.UpdatedDate = DateTime.Now;
                                            order.UpdatedBy = ds.Unit.CurrentUser.CurrentUserName;
                                            order.UpdatedByCID = ds.Unit.CurrentUser.CID;
                                            //clear message if active
                                            switch (order.Status)
                                            {
                                                case OrderStatus.Active:
                                                    OrderPadUtilities.ClearValidationMessages(order);
                                                    RemoveOrderFromBulkOrders(order);
                                                    break;
                                                case OrderStatus.ValidationFailed:
                                                    order.ValidationMessage = "Status is changed to Validation Failed manually";
                                                    RemoveOrderFromBulkOrders(order);
                                                    break;
                                            }
                                            //Removing entries from settled unsettled
                                            OrderPadUtilities.RemoveNCreateSettledUnsettledEntities(order, Broker, ds.Unit.CurrentUser);
                                        }
                                    }
                                }
                            }
                        }
                        break;
                    case DatasetCommandTypes.Unsettle://Complete to Submitted order
                        if (Orders != null)
                        {
                            if (ds.OrdersTable.Rows.Count > 0)
                            {
                                DataRow dr = ds.OrdersTable.Rows[0];
                                var id = Guid.Parse(dr[ds.OrdersTable.ID].ToString());

                                if (ds.Unit != null && id != Guid.Empty)
                                {
                                    var order = Orders.FirstOrDefault(ss => ss.ID == id && ss.Status == OrderStatus.Complete);
                                    if (order != null)
                                    {
                                        order.Status = OrderStatus.Submitted;
                                        order.UpdatedDate = DateTime.Now;
                                        order.UpdatedBy = ds.Unit.CurrentUser.CurrentUserName;
                                        order.UpdatedByCID = ds.Unit.CurrentUser.CID;
                                    }
                                }
                            }
                        }
                        break;
                    case DatasetCommandTypes.SendOrderToSMA:
                        if (Orders != null)
                        {
                            var order = Orders.FirstOrDefault(ss => ss.ID == ds.OrderId);
                            if (order != null)
                            {
                                var smaResults = OrderPadUtilities.SendOrderToSMA(order, Broker, ds.Unit.CurrentUser);
                                if (!string.IsNullOrEmpty(smaResults))
                                {
                                    var result = new Dictionary<long, string> { { order.OrderID, smaResults } };
                                    if (!ds.ExtendedProperties.Contains("SMAMessages"))
                                    {
                                        ds.ExtendedProperties.Add("SMAMessages", result);
                                    }
                                    else
                                    {
                                        var results = ds.ExtendedProperties["SMAMessages"] as Dictionary<long, string>;
                                        if (results != null)
                                        {
                                            results.Merge(result);
                                        }
                                        ds.ExtendedProperties["SMAMessages"] = results;

                                    }
                                }
                            }
                        }
                        break;
                    case DatasetCommandTypes.Complete://Complete Order
                        if (Orders != null)
                        {
                            if (ds.OrdersTable.Rows.Count > 0)
                            {
                                DataRow dr = ds.OrdersTable.Rows[0];
                                var id = Guid.Parse(dr[ds.OrdersTable.ID].ToString());

                                if (ds.Unit != null && id != Guid.Empty)
                                {
                                    var order = Orders.FirstOrDefault(ss => ss.ID == id && ss.Status == OrderStatus.Submitted);
                                    if (order != null)
                                    {
                                        OrderPadUtilities.CompleteInCompleteOrder(order.ClientID, order, DatasetCommandTypes.Complete, Broker, ds.Unit.CurrentUser);
                                    }
                                }
                            }
                        }
                        break;
                    case DatasetCommandTypes.Incomplete://Incomplete order
                        if (Orders != null)
                        {
                            if (ds.OrdersTable.Rows.Count > 0)
                            {
                                DataRow dr = ds.OrdersTable.Rows[0];
                                var id = Guid.Parse(dr[ds.OrdersTable.ID].ToString());

                                if (ds.Unit != null && id != Guid.Empty)
                                {
                                    var order = Orders.FirstOrDefault(ss => ss.ID == id && ss.Status == OrderStatus.Complete);
                                    if (order != null)
                                    {
                                        OrderPadUtilities.CompleteInCompleteOrder(order.ClientID, order, DatasetCommandTypes.Incomplete, Broker, ds.Unit.CurrentUser);
                                    }
                                }
                            }
                        }
                        break;
                }
            }
            else if (data is ImportProcessDS)
            {
                var value = data as ImportProcessDS;
                long batchId = 2222;
                if (Orders.Count() > 0)
                {
                    var maxBatchID = Orders.Max(ss => ss.BatchID);
                    if (maxBatchID.HasValue)
                        batchId = maxBatchID.Value + 1;
                }
                switch ((WebCommands)value.Command)
                {
                    case WebCommands.ImportFinSimplcityOrders:
                        #region State Street
                        if (value.ExtendedProperties["GetSquanceNumber"] != null)
                        {
                            long maxSequance = Orders.Max(ss => ss.FileNameSequanceNumber);
                            value.ExtendedProperties["PreviouseSequanceNumber"] = maxSequance + 1;
                        }
                        else
                        {
                            DataRow[] drStateStreet = value.Tables["State Street"].Select("HasErrors=false");
                            long sequanceNumber;
                            if (value.ExtendedProperties["SequanceNumber"] != null)
                            {
                                sequanceNumber = Int64.Parse(value.ExtendedProperties["SequanceNumber"].ToString());
                            }
                            else
                            {
                                long maxSequanceOrder = Orders.Max(ss => ss.FileNameSequanceNumber);
                                sequanceNumber = maxSequanceOrder + 1;
                            }
                            var result_SS = drStateStreet.AsEnumerable().GroupBy(row => row.Field<string>("Oritax Id"));
                            foreach (var clientrows in result_SS)
                            {
                                var clientOrders = new List<OrderEntity>();
                                var buyorsell = clientrows.GroupBy(row => row.Field<string>("Order Type"));

                                foreach (var buyorsellItems in buyorsell)
                                {
                                    long newOrderID = 1111;
                                    if (Orders.Count() > 0)
                                    {
                                        newOrderID = Orders.Max(ss => ss.OrderID) + 1;
                                    }
                                    OrderEntity order = null;
                                    foreach (var row in buyorsellItems)
                                    {
                                        if (order == null)
                                        {
                                            order = new OrderEntity
                                            {
                                                ID = Guid.NewGuid(),
                                                OrderID = newOrderID,
                                                BatchID = batchId,
                                                CreatedDate = DateTime.Now,
                                                TradeDate = DateTime.Now,
                                                ClientCID = Guid.Parse(row["ClientCID"].ToString()),
                                                ClientID = row["Oritax Id"].ToString(),
                                                FileName = value.FileName,
                                                FileNameSequanceNumber = sequanceNumber,
                                                OrderAccountType = OrderAccountType.StateStreet,
                                                OrderType = OrderType.FinSimplicity,
                                                Status = OrderStatus.Approved,
                                                Attachment = null,
                                                AccountCID = Guid.Parse(row["AccountCID"].ToString()),
                                                AccountNumber = row["AccountNo"].ToString(),
                                                AccountBSB = row["AccountBSB"].ToString(),
                                                OrderItemType = (OrderItemType)System.Enum.Parse(typeof(OrderItemType), row["Order Type"].ToString(), true),
                                                Items = new List<OrderItemEntity>(),
                                                Instructions = new List<OrderInstructionEntity>(),
                                                ClientManagementType = ClientManagementType.UMA,
                                            };
                                        }
                                        var orderitem = new StateStreetOrderItem
                                        {
                                            Amount = row["Order Amount"] != DBNull.Value ? decimal.Parse(row["Order Amount"].ToString()) : (decimal?)null,
                                            Units = row["Est Order Units"] != DBNull.Value ? decimal.Parse(row["Est Order Units"].ToString()) : (decimal?)null,
                                            CreatedDate = DateTime.Now,
                                            ID = Guid.NewGuid(),
                                            OrderID = order.ID,
                                            ProductID = Guid.Parse(row["ProductID"].ToString()),
                                            FundCode = row["Investment Code"].ToString(),
                                        };
                                        order.Items.Add(orderitem);
                                    }

                                    if (order != null)
                                    {
                                        Orders.Add(order);
                                        clientOrders.Add(order);
                                    }
                                }
                                OrderPadUtilities.RemoveNCreateSettledUnsettledForClientOrders(clientrows.Key, clientOrders, Broker, value.Unit.CurrentUser, true);
                            }
                        }
                        #endregion

                        break;
                    case WebCommands.ImportFinSimplcityOrdersForDesktopBroker:
                        #region Desktop Broker
                        DataRow[] drdesktop = value.Tables["Desktop Broker Rebookings"].Select("HasErrors=false");

                        var results = drdesktop.AsEnumerable().GroupBy(row => row.Field<string>("BOOKED A/C"));
                        // var result = from row in value.Tables["Desktop Broker Rebookings"].AsEnumerable() group row by row.Field<string>("BOOKED A/C") into grp select new { ClientIds = grp.Key, Members = grp };

                        foreach (var clientrows in results)
                        {
                            var clientOrders = new List<OrderEntity>();
                            var buyorsell = clientrows.GroupBy(row => row.Field<string>("B/S"));

                            foreach (var buyorsellItems in buyorsell)
                            {
                                long newOrderID = 1111;
                                if (Orders.Count() > 0)
                                {
                                    newOrderID = Orders.Max(ss => ss.OrderID) + 1;
                                }
                                OrderEntity order = null;
                                foreach (var row in buyorsellItems)
                                {
                                    if (order == null)
                                    {
                                        order = new OrderEntity
                                        {
                                            ID = Guid.NewGuid(),
                                            OrderID = newOrderID,
                                            BatchID = batchId,
                                            CreatedDate = DateTime.Now,
                                            TradeDate = DateTime.Parse(row["Trade As At Date"].ToString(), new DateTimeFormatInfo { ShortDatePattern = "dd/MM/yyyy" }),
                                            ClientCID = Guid.Parse(row["ClientCID"].ToString()),
                                            ClientID = row["BOOKED A/C"].ToString(),
                                            FileName = value.FileName,
                                            OrderAccountType = OrderAccountType.ASX,
                                            OrderType = OrderType.FinSimplicity,
                                            Status = OrderStatus.Approved,
                                            Attachment = null,
                                            AccountCID = Guid.Parse(row["AccountCID"].ToString()),
                                            AccountNumber = row["AccountNo"].ToString(),
                                            AccountBSB = row["AccountBSB"].ToString(),
                                            OrderItemType = (OrderItemType)System.Enum.Parse(typeof(OrderItemType), row["B/S"].ToString(), true),
                                            Items = new List<OrderItemEntity>(),
                                            Instructions = new List<OrderInstructionEntity>(),
                                            ClientManagementType = ClientManagementType.UMA,
                                        };
                                    }
                                    var orderitem = new ASXOrderItem
                                    {

                                        Amount = row["PRICE/AVG"] != DBNull.Value && row["QTY"] != DBNull.Value ? Decimal.Parse(row["PRICE/AVG"].ToString()) * Decimal.Parse(row["QTY"].ToString()) : (decimal?)null,
                                        Units = row["QTY"] != DBNull.Value ? Decimal.Parse(row["QTY"].ToString()) : (decimal?)null,
                                        CreatedDate = DateTime.Now,
                                        ID = Guid.NewGuid(),
                                        OrderID = order.ID,
                                        ProductID = Guid.Parse(row["ProductID"].ToString()),
                                        InvestmentCode = row["STOCK"].ToString(),
                                        ASXAccountName = row["Client Account Name"].ToString(),
                                        ASXAccountNo = row["ASXAccountNo"].ToString(),
                                        ParentAcc = row["PARENT A/C"].ToString(),
                                        ASXAccountCID = Guid.Parse(row["ASXCID"].ToString()),
                                        UnitPrice = row["PRICE/AVG"] != DBNull.Value ? Decimal.Parse(row["PRICE/AVG"].ToString()) : (decimal?)null
                                    };
                                    order.Items.Add(orderitem);
                                }

                                if (order != null)
                                {
                                    Orders.Add(order);
                                    clientOrders.Add(order);
                                }
                            }

                            OrderPadUtilities.RemoveNCreateSettledUnsettledForClientOrders(clientrows.Key, clientOrders, Broker, value.Unit.CurrentUser, true);
                        }

                        #endregion
                        break;
                }
            }
            else if (data is ExportDS)
            {
                var ds = data as ExportDS;
                if (ds.ExtendedProperties["IsBulk"] != null && ds.ExtendedProperties["IsBulk"].ToString().ToLower() == "true")
                {
                    if (Orders != null && BulkOrders != null)
                    {
                        if (ds.ExtendedProperties["Orders"] is List<OrderEntity> && ds.ExtendedProperties["BulkOrders"] is List<BulkOrderEntity>)
                        {
                            var ordersToUpdate = ds.ExtendedProperties["Orders"] as List<OrderEntity>;
                            var bulkOrdersToUpdate = ds.ExtendedProperties["BulkOrders"] as List<BulkOrderEntity>;
                            OrderPadUtilities.UpdateBulkOrderStatus(bulkOrdersToUpdate, ordersToUpdate, ds.Unit.CurrentUser);

                        }
                        else
                        {
                            var bulkOrderIDs = (List<Guid>)ds.ExtendedProperties["BulkOrdersFilter"];
                            var bulkOrders = bulkOrderIDs == null ? BulkOrders.Where(ss => ss.ClientManagementType == ds.ClientManagementType && ss.IsExported == false && ss.Status == BulkOrderStatus.Active) : BulkOrders.Where(ss => bulkOrderIDs.Contains(ss.ID));
                            var orders = Orders.Where(ss => ss.ClientManagementType == ds.ClientManagementType && ss.OrderBulkStatus == OrderBulkStatus.Batched);
                            OrderPadUtilities.ExportInstructions(ds, orders.ToList(), bulkOrders.ToList(), Broker);
                        }
                    }
                }
                else if (Orders != null)
                {
                    if (ds.ExtendedProperties["Orders"] is List<OrderEntity>)
                    {
                        var ordersToUpdate = ds.ExtendedProperties["Orders"] as List<OrderEntity>;
                        var smaMessages = OrderPadUtilities.UpdateOrderStatus(ordersToUpdate, Broker, ds.Unit.CurrentUser);
                        if (smaMessages != null && smaMessages.Count > 0)
                        {
                            if (!ds.ExtendedProperties.Contains("SMAMessages"))
                            {
                                ds.ExtendedProperties.Add("SMAMessages", smaMessages);
                            }
                            else
                            {
                                var results = ds.ExtendedProperties["SMAMessages"] as Dictionary<long, string>;
                                if (results != null)
                                {
                                    results.Merge(smaMessages);
                                }
                                ds.ExtendedProperties["SMAMessages"] = results;

                            }
                        }
                    }
                    else
                    {
                        long maxSequanceNumber = Orders.Max(ss => ss.FileNameSequanceNumber);
                        List<Guid> orderIDs = (List<Guid>)ds.ExtendedProperties["OrdersFilter"];
                        var orders = (orderIDs == null ? Orders.Where(ss => ss.ClientManagementType == ds.ClientManagementType && ss.Status == OrderStatus.Approved || (ss.OrderType == OrderType.FinSimplicity && (ss.Status == OrderStatus.Validated))) : Orders.Where(ss => orderIDs.Contains(ss.ID)));
                        OrderPadUtilities.ExportInstructions(ds, orders.ToList(), Broker, maxSequanceNumber);
                    }
                }
            }
            else if (data is BulkOrderDS)
            {
                var ds = data as BulkOrderDS;

                switch (ds.CommandType)
                {
                    case DatasetCommandTypes.SubmitToDesktopBroker:
                        foreach (Guid bulkOrderID in ds.BulkOrderIDs)
                        {
                            var bulkOrderEntity = BulkOrders.FirstOrDefault(ss => ss.ID == bulkOrderID);
                            if (bulkOrderEntity == null)
                                continue;

                            foreach (BulkOrderItemEntity bulkOrderItem in bulkOrderEntity.BulkOrderItems)
                            {
                                FIXTradeRequest orderRequest = new FIXTradeRequest();

                                // todo
                                // Add BulkOrderID into request table
                                orderRequest.BulkOrderID = Convert.ToInt32(bulkOrderEntity.BulkOrderID);
                                orderRequest.SecurityName = bulkOrderItem.Code;
                                orderRequest.BuySell = bulkOrderEntity.OrderItemType.ToString().ToUpper();
                                orderRequest.OrderType = "MARKET";
                                orderRequest.HandleInstruction = "AUTOMATED_EXECUTION_ORDER_PUBLIC";

                                if (bulkOrderEntity.OrderPreferedBy == OrderPreferedBy.Amount)
                                    orderRequest.CashOrderQty = bulkOrderItem.ExpectedAmount;
                                else if (bulkOrderEntity.OrderPreferedBy == OrderPreferedBy.Units)
                                    orderRequest.Quantity = bulkOrderItem.ExpectedUnits.HasValue ? Convert.ToInt32(bulkOrderItem.ExpectedUnits.Value) : 0;

                                orderRequest.ValidDuration = "GOOD_TILL_CANCEL";

                                //todo
                                // TPPClientID should be individual UMA account 
                                orderRequest.TPPClientID = "116647"; // Eclipse Super is "116647"
                                orderRequest.TPPPersonID = "25398";

                                orderRequest.CreateTime = DateTime.Now;


                                FIXTradeRequest_DAL.Insert(orderRequest);
                            }

                            bulkOrderEntity.IsSubmitted = true;
                        }
                        break;
                    case DatasetCommandTypes.Update://Update Bulk Order Item
                        if (Orders != null && BulkOrders != null)
                        {
                            ApportionBulkOrders(ds);
                        }
                        break;
                    case DatasetCommandTypes.SendOrderToSMA:

                        if (Orders != null && BulkOrders != null)
                        {
                            foreach (Guid bulkOrderID in ds.BulkOrderIDs)
                            {
                                var bulkOrderEntity = BulkOrders.FirstOrDefault(ss => ss.ID == bulkOrderID);
                                if (bulkOrderEntity != null)
                                {
                                    var orderEntities = new List<OrderEntity>();
                                    if (bulkOrderEntity.BulkOrderItems != null)
                                        foreach (var bulkOrderItem in bulkOrderEntity.BulkOrderItems)
                                        {
                                            foreach (var item in bulkOrderItem.OrderItems)
                                            {
                                                var order = Orders.FirstOrDefault(ss => ss.ID == item.OrderID);
                                                if (order != null)
                                                {
                                                    if (orderEntities.Contains(order))
                                                    {
                                                        orderEntities.Remove(order);
                                                    }
                                                    orderEntities.Add(order);
                                                }
                                            }
                                        }

                                    if (bulkOrderEntity.TDBulkOrderItems != null)
                                        foreach (var bulkOrderItem in bulkOrderEntity.TDBulkOrderItems)
                                        {
                                            foreach (var item in bulkOrderItem.OrderItems)
                                            {
                                                var order = Orders.FirstOrDefault(ss => ss.ID == item.OrderID);
                                                if (order != null)
                                                {
                                                    if (orderEntities.Contains(order))
                                                    {
                                                        orderEntities.Remove(order);
                                                    }
                                                    orderEntities.Add(order);
                                                }
                                            }
                                        }
                                    var smaResults = OrderPadUtilities.SendBulkToSMA(bulkOrderEntity, orderEntities, ds.Unit.CurrentUser, Broker);
                                    if (!string.IsNullOrEmpty(smaResults))
                                    {
                                        var result = new Dictionary<long, string> { { bulkOrderEntity.BulkOrderID, smaResults } };
                                        if (!ds.ExtendedProperties.Contains("SMAMessages"))
                                        {
                                            ds.ExtendedProperties.Add("SMAMessages", result);
                                        }
                                        else
                                        {
                                            var results = ds.ExtendedProperties["SMAMessages"] as Dictionary<long, string>;
                                            if (results != null)
                                            {
                                                results.Merge(result);
                                            }
                                            ds.ExtendedProperties["SMAMessages"] = results;

                                        }
                                    }
                                }
                            }

                        }
                        break;
                }
            }
            return ModifiedState.MODIFIED;
        }

        /// <summary>
        /// Checking Product ID is empty or not 
        /// </summary>
        /// <param name="order">OrderEntity</param>
        /// <returns>bool</returns>
        private static bool IsProductIdEmpty(OrderEntity order)
        {
            bool isEmpty = false;
            switch (order.OrderAccountType)
            {
                case OrderAccountType.TermDeposit:
                    if (order.Items.Cast<TermDepositOrderItem>().Any(item => item.ProductID == Guid.Empty))
                    {
                        isEmpty = true;
                    }
                    break;
                case OrderAccountType.StateStreet:
                    if (order.Items.Cast<StateStreetOrderItem>().Any(item => item.ProductID == Guid.Empty))
                    {
                        isEmpty = true;
                    }
                    break;
                case OrderAccountType.ASX:
                    foreach (ASXOrderItem item in order.Items)
                    {
                        isEmpty = item.ProductID == Guid.Empty;
                    }
                    if (order.Items.Cast<ASXOrderItem>().Any(item => item.ProductID == Guid.Empty))
                    {
                        isEmpty = true;
                    }
                    break;
                case OrderAccountType.AtCall:
                    if (order.Items.Cast<AtCallOrderItem>().Any(item => item.ProductID == Guid.Empty))
                    {
                        isEmpty = true;
                    }
                    break;
            }
            return isEmpty;
        }

        private void ApportionBulkOrders(BulkOrderDS ds)
        {
            if (ds.BulkOrderItemsTable.Rows.Count > 0)
            {
                DataRow dr = ds.BulkOrderItemsTable.Rows[0];
                var bulkId = Guid.Parse(dr[ds.BulkOrderItemsTable.BULKORDERID].ToString());
                var id = Guid.Parse(dr[ds.BulkOrderItemsTable.ID].ToString());
                var contractNote = dr[ds.BulkOrderItemsTable.CONTRACTNOTE].ToString();
                var actualAmount = (dr[ds.BulkOrderItemsTable.ACTUALAMOUNT] != DBNull.Value ? dr[ds.BulkOrderItemsTable.ACTUALAMOUNT] : null) as decimal?;
                var actualUnitPrice = (dr[ds.BulkOrderItemsTable.ACTUALUNITPRICE] != DBNull.Value ? dr[ds.BulkOrderItemsTable.ACTUALUNITPRICE] : null) as decimal?;
                var actualUnits = (dr[ds.BulkOrderItemsTable.ACTUALUNITS] != DBNull.Value ? dr[ds.BulkOrderItemsTable.ACTUALUNITS] : null) as decimal?;
                var brokerage = (dr[ds.BulkOrderItemsTable.BROKERAGE] != DBNull.Value ? dr[ds.BulkOrderItemsTable.BROKERAGE] : null) as decimal?;
                var charges = (dr[ds.BulkOrderItemsTable.CHARGES] != DBNull.Value ? dr[ds.BulkOrderItemsTable.CHARGES] : null) as decimal?;
                var tax = (dr[ds.BulkOrderItemsTable.TAX] != DBNull.Value ? dr[ds.BulkOrderItemsTable.TAX] : null) as decimal?;
                var grossValue = (dr[ds.BulkOrderItemsTable.GROSSVALUE] != DBNull.Value ? dr[ds.BulkOrderItemsTable.GROSSVALUE] : null) as decimal?;

                var bulkOrder = BulkOrders.FirstOrDefault(ss => ss.ID == bulkId);
                if (bulkOrder != null)
                {
                    switch (bulkOrder.OrderAccountType)
                    {
                        case OrderAccountType.ASX:
                            {
                                var bulkOrderItem = bulkOrder.BulkOrderItems.FirstOrDefault(ss => ss.ID == id);
                                if (bulkOrderItem != null)
                                {
                                    //Creating anonymous class
                                    var o = new { Percent = (decimal?)0, OrderId = Guid.Empty, OrderItemId = Guid.Empty, Code = "", Units = (decimal?)0 };
                                    var list = new[] { o }.ToList();
                                    //Removing first dummy item
                                    list.RemoveAt(0);

                                    foreach (var item in bulkOrderItem.OrderItems)
                                    {
                                        var order = Orders.FirstOrDefault(ss => ss.ID == item.OrderID);
                                        if (order != null)
                                        {
                                            var orderItem = order.Items.FirstOrDefault(ss => ss.ID == item.OrderItemID);
                                            if (orderItem != null)
                                            {
                                                if (orderItem is ASXOrderItem)
                                                {
                                                    var asxOrderItem = orderItem as ASXOrderItem;
                                                    if (asxOrderItem.InvestmentCode == bulkOrderItem.Code)
                                                    {
                                                        //Calculating percent according to Buy/Sell
                                                        decimal? percent = 0;
                                                        switch (order.OrderItemType)
                                                        {
                                                            case OrderItemType.Buy:
                                                                percent = asxOrderItem.Amount.CalculatePercentage(bulkOrderItem.ExpectedAmount);
                                                                break;
                                                            case OrderItemType.Sell:
                                                                percent = asxOrderItem.Units.CalculatePercentage(bulkOrderItem.ExpectedUnits);
                                                                break;
                                                        }

                                                        var calcUnitPrice = actualUnitPrice;
                                                        //Calculating actual values according to percent
                                                        var calcUnits = actualUnits.CalculateValueFromPercentage(percent);
                                                        //Allocate units using floor method
                                                        calcUnits = decimal.Floor(Convert.ToDecimal(calcUnits));
                                                        var calcAmount = calcUnits * calcUnitPrice;

                                                        //Adding percent, orderid and orderitemid to list
                                                        list.Add(new { Percent = percent, OrderId = asxOrderItem.OrderID, OrderItemId = asxOrderItem.ID, Code = asxOrderItem.InvestmentCode, Units = calcUnits });

                                                        //Update Order Items
                                                        asxOrderItem.Amount = calcAmount;
                                                        asxOrderItem.Units = calcUnits;
                                                        asxOrderItem.UnitPrice = calcUnitPrice;
                                                        asxOrderItem.UpdatedDate = DateTime.Now;

                                                        //Update Bulk Order Item
                                                        bulkOrderItem.ActualAmount = order.OrderItemType == OrderItemType.Sell ? Math.Abs(Convert.ToDecimal(actualAmount)) * -1 : actualAmount;
                                                        bulkOrderItem.ActualUnits = actualUnits;
                                                        bulkOrderItem.ActualUnitPrice = actualUnitPrice;
                                                        bulkOrderItem.ContractNote = contractNote;
                                                        bulkOrderItem.IsApportioned = true;
                                                        bulkOrderItem.Brokerage = brokerage;
                                                        bulkOrderItem.Charges = charges;
                                                        bulkOrderItem.Tax = tax;
                                                        bulkOrderItem.GrossValue = grossValue;
                                                        bulkOrderItem.UpdatedDate = DateTime.Now;
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    if (list.Count > 0)
                                    {
                                        //order items having max percentage 
                                        list = list.OrderByDescending(ss => ss.Percent).ToList();
                                        //Total Units Apportioned
                                        var totalAportionedUnits = list.Sum(ss => ss.Units);
                                        //Units Remaining to be Apportioned
                                        var unitsRemaining = actualUnits - totalAportionedUnits;
                                        if (unitsRemaining > 0)
                                        {
                                            while (unitsRemaining > 0)
                                            {
                                                foreach (var item in list)
                                                {
                                                    var order = Orders.FirstOrDefault(ss => ss.ID == item.OrderId);
                                                    if (order != null)
                                                    {
                                                        var orderItem = order.Items.FirstOrDefault(ss => ss.ID == item.OrderItemId);
                                                        if (orderItem != null)
                                                        {
                                                            if (orderItem is ASXOrderItem)
                                                            {
                                                                var asxOrderItem = orderItem as ASXOrderItem;
                                                                if (asxOrderItem.InvestmentCode == item.Code)
                                                                {
                                                                    //Allocating one extra unit
                                                                    var calcUnits = asxOrderItem.Units + 1;
                                                                    var calcAmount = calcUnits * asxOrderItem.UnitPrice;
                                                                    //Update Order Items
                                                                    asxOrderItem.Amount = calcAmount;
                                                                    asxOrderItem.Units = calcUnits;
                                                                    asxOrderItem.UpdatedDate = DateTime.Now;
                                                                    //subtracting one allocated unit from remaining pool
                                                                    unitsRemaining = unitsRemaining - 1;
                                                                    if (unitsRemaining == 0)
                                                                        break;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                }
                            }
                            break;
                        case OrderAccountType.TermDeposit:
                            {
                                var tdBulkOrderItem = bulkOrder.TDBulkOrderItems.FirstOrDefault(ss => ss.ID == id);
                                if (tdBulkOrderItem != null)
                                {
                                    foreach (var item in tdBulkOrderItem.OrderItems)
                                    {
                                        var order = Orders.FirstOrDefault(ss => ss.ID == item.OrderID);
                                        if (order != null)
                                        {
                                            var orderItem = order.Items.FirstOrDefault(ss => ss.ID == item.OrderItemID);
                                            if (orderItem != null)
                                            {
                                                if (orderItem is TermDepositOrderItem)
                                                {
                                                    var tdOrderItem = orderItem as TermDepositOrderItem;
                                                    if (tdOrderItem.BrokerID == tdBulkOrderItem.BrokerId && tdOrderItem.BankID == tdBulkOrderItem.InstituteID && tdOrderItem.Duration == tdBulkOrderItem.Term && tdOrderItem.Percentage == tdBulkOrderItem.Rate)
                                                    {
                                                        //Update Bulk Order Item
                                                        tdBulkOrderItem.ContractNote = contractNote;
                                                        tdBulkOrderItem.IsApportioned = true;
                                                        tdBulkOrderItem.UpdatedDate = DateTime.Now;
                                                    }
                                                }
                                            }
                                        }
                                    }

                                }
                            }
                            break;
                    }
                    string message = UpdateBulkOrderStatus(bulkOrder, ds.Unit.CurrentUser);


                    if (!string.IsNullOrEmpty(message))
                    {
                        var result = new Dictionary<long, string> { { bulkOrder.BulkOrderID, message } };
                        if (!ds.ExtendedProperties.Contains("SMAMessages"))
                        {

                            ds.ExtendedProperties.Add("SMAMessages", result);
                        }
                        else
                        {
                            var results = ds.ExtendedProperties["SMAMessages"] as Dictionary<long, string>;
                            if (results != null)
                            {
                                results.Merge(result);
                            }
                            ds.ExtendedProperties["SMAMessages"] = results;

                        }
                    }
                }
            }
        }

        private string UpdateBulkOrderStatus(BulkOrderEntity bulkOrder, UserEntity user)
        {
            var orderEntities = new List<OrderEntity>();
            switch (bulkOrder.OrderAccountType)
            {
                case OrderAccountType.TermDeposit:
                    {
                        var bulk = bulkOrder.TDBulkOrderItems.FirstOrDefault(ss => ss.IsApportioned == false);
                        if (bulk == null)
                        {
                            bulkOrder.Status = BulkOrderStatus.Complete;
                            bulkOrder.UpdatedDate = DateTime.Now;
                            bulkOrder.UpdatedBy = user.CurrentUserName;
                            bulkOrder.UpdatedByCID = user.CID;

                            foreach (var tdbulkOrderItem in bulkOrder.TDBulkOrderItems)
                            {
                                foreach (var item in tdbulkOrderItem.OrderItems)
                                {
                                    var order = Orders.FirstOrDefault(ss => ss.ID == item.OrderID);
                                    if (order != null)
                                    {
                                        if (orderEntities.Contains(order))
                                        {
                                            orderEntities.Remove(order);

                                        }

                                        orderEntities.Add(order);
                                        order.Status = bulkOrder.ClientManagementType == ClientManagementType.SMA ? OrderStatus.Submitted : OrderStatus.Approved;// changing status of order to Submitted for SMA Orders
                                        order.OrderBulkStatus = OrderBulkStatus.Complete;
                                        order.UpdatedDate = DateTime.Now;
                                        order.UpdatedBy = user.CurrentUserName;
                                        order.UpdatedByCID = user.CID;
                                        //Removing entries from settled unsettled
                                        OrderPadUtilities.RemoveNCreateSettledUnsettledEntities(order, Broker, user);
                                    }
                                }
                            }
                        }
                    }
                    break;
                case OrderAccountType.ASX:
                    {
                        var bulk = bulkOrder.BulkOrderItems.FirstOrDefault(ss => ss.IsApportioned == false);
                        if (bulk == null)
                        {
                            bulkOrder.Status = BulkOrderStatus.Complete;
                            bulkOrder.UpdatedDate = DateTime.Now;
                            bulkOrder.UpdatedBy = user.CurrentUserName;
                            bulkOrder.UpdatedByCID = user.CID;

                            foreach (var bulkOrderItem in bulkOrder.BulkOrderItems)
                            {
                                foreach (var item in bulkOrderItem.OrderItems)
                                {
                                    var order = Orders.FirstOrDefault(ss => ss.ID == item.OrderID);
                                    if (order != null)
                                    {
                                        if (orderEntities.Contains(order))
                                        {
                                            orderEntities.Remove(order);

                                        }
                                        orderEntities.Add(order);
                                        order.Status = bulkOrder.ClientManagementType == ClientManagementType.SMA ? OrderStatus.Submitted : OrderStatus.Approved;// changing status of order to Submitted for SMA Orders
                                        order.OrderBulkStatus = OrderBulkStatus.Complete;
                                        order.UpdatedDate = DateTime.Now;
                                        order.UpdatedBy = user.CurrentUserName;
                                        order.UpdatedByCID = user.CID;
                                        //Removing entries from settled unsettled
                                        OrderPadUtilities.RemoveNCreateSettledUnsettledEntities(order, Broker, user);


                                    }
                                }
                            }



                        }
                    }
                    break;
            }

            return OrderPadUtilities.SendBulkToSMA(bulkOrder, orderEntities, user, this.Broker);
        }

        private string GetFundName(string fundCode)
        {
            string fundName = String.Empty;
            var org = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            var security = org.Securities.Where(ss => ss.AsxCode == fundCode).FirstOrDefault();
            if (security != null)
            {
                fundName = security.CompanyName;
            }
            Broker.ReleaseBrokerManagedComponent(org);
            return fundName;
        }

        private string GetCurrencyCode(string fundCode)
        {
            string currencyCode = String.Empty;
            var org = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            var security = org.Securities.Where(ss => ss.AsxCode == fundCode).FirstOrDefault();
            if (security != null)
            {
                if (security.ASXSecurity.Count > 0)
                {
                    var asxSecurity = security.ASXSecurity.OrderByDescending(sec => sec.Date).FirstOrDefault();
                    if (asxSecurity != null)
                    {
                        currencyCode = asxSecurity.Currency;
                    }
                }
            }
            Broker.ReleaseBrokerManagedComponent(org);
            return currencyCode;
        }

        private static void CreateBulkOrder(List<BulkOrderEntity> bulkOrders, List<OrderEntity> orders)
        {
            foreach (var order in orders)
            {
                bool isNewBulkOrder = false;
                var bulkOrder = bulkOrders.FirstOrDefault(ss => ss.ClientManagementType == order.ClientManagementType && ss.IsExported == false && ss.OrderItemType == order.OrderItemType && ss.OrderPreferedBy == order.OrderPreferedBy && ss.OrderAccountType == order.OrderAccountType);

                if (bulkOrder == null)
                {
                    //Setting Bulk Order ID/No to start from 1111
                    long newBulkOrderID = 1111;
                    if (bulkOrders.Count() > 0)
                    {
                        newBulkOrderID = bulkOrders.Max(ss => ss.BulkOrderID) + 1;
                    }

                    var newBulkOrder = new BulkOrderEntity
                    {
                        ID = Guid.NewGuid(),
                        BulkOrderID = newBulkOrderID,
                        CreatedDate = DateTime.Now,
                        OrderAccountType = order.OrderAccountType,
                        OrderType = order.OrderType,
                        OrderItemType = order.OrderItemType,
                        Status = BulkOrderStatus.Active,
                        IsExported = false,
                        Instructions = new List<BulkOrderInstructionEntity>(),
                        ClientManagementType = order.ClientManagementType,
                        OrderPreferedBy = order.OrderPreferedBy,
                    };
                    switch (order.OrderAccountType)
                    {
                        case OrderAccountType.TermDeposit:
                            newBulkOrder.TDBulkOrderItems = new List<TDBulkOrderItemEntity>();
                            break;
                        case OrderAccountType.ASX:
                            newBulkOrder.BulkOrderItems = new List<BulkOrderItemEntity>();
                            break;
                    }

                    bulkOrder = newBulkOrder;
                    isNewBulkOrder = true;
                }

                foreach (var item in order.Items)
                {
                    bool isNewBulkOrderItem = false;
                    if (item is ASXOrderItem)
                    {
                        var asxItem = item as ASXOrderItem;

                        var bulkOrderItem =
                            bulkOrder.BulkOrderItems.FirstOrDefault(
                                ss => ss.BulkOrderID == bulkOrder.ID && ss.Code == asxItem.InvestmentCode);
                        if (bulkOrderItem == null)
                        {
                            var newBulkOrderItem = new BulkOrderItemEntity
                                {
                                    ID = Guid.NewGuid(),
                                    BulkOrderID = bulkOrder.ID,
                                    OrderItems = new List<OrderItemPair>(),
                                    Code = asxItem.InvestmentCode,
                                    ExpectedAmount = asxItem.Amount,
                                    ExpectedUnitPrice = asxItem.UnitPrice,
                                    ExpectedUnits = asxItem.Units,
                                    CreatedDate = DateTime.Now,
                                };
                            bulkOrderItem = newBulkOrderItem;
                            isNewBulkOrderItem = true;
                        }
                        else
                        {
                            bulkOrderItem.ExpectedAmount += asxItem.Amount;
                            bulkOrderItem.ExpectedUnitPrice = (bulkOrderItem.ExpectedUnitPrice + asxItem.UnitPrice) /
                                                              2;
                            bulkOrderItem.ExpectedUnits += asxItem.Units;
                            bulkOrderItem.UpdatedDate = DateTime.Now;
                        }

                        var orderItems = new OrderItemPair
                            {
                                OrderID = order.ID,
                                OrderItemID = asxItem.ID
                            };

                        bulkOrderItem.OrderItems.Add(orderItems);

                        if (isNewBulkOrderItem)
                        {
                            bulkOrder.BulkOrderItems.Add(bulkOrderItem);
                        }
                    }
                    else if (item is TermDepositOrderItem)
                    {
                        var tdItem = item as TermDepositOrderItem;

                        var bulkOrderItem = bulkOrder.TDBulkOrderItems.FirstOrDefault(ss => ss.BulkOrderID == bulkOrder.ID && ss.BrokerId == tdItem.BrokerID && ss.InstituteID == tdItem.BankID && ss.Term == tdItem.Duration && ss.Rate == tdItem.Percentage);
                        if (bulkOrderItem == null)
                        {
                            var newBulkOrderItem = new TDBulkOrderItemEntity
                                {
                                    ID = Guid.NewGuid(),
                                    BulkOrderID = bulkOrder.ID,
                                    OrderItems = new List<OrderItemPair>(),
                                    BrokerId = tdItem.BrokerID,
                                    InstituteID = tdItem.BankID,
                                    Term = tdItem.Duration,
                                    Rate = tdItem.Percentage,
                                    CreatedDate = DateTime.Now,
                                    Amount = tdItem.Amount,
                                };
                            bulkOrderItem = newBulkOrderItem;
                            isNewBulkOrderItem = true;
                        }
                        else
                        {
                            bulkOrderItem.Amount += tdItem.Amount;
                            bulkOrderItem.UpdatedDate = DateTime.Now;
                        }

                        var orderItems = new OrderItemPair
                            {
                                OrderID = order.ID,
                                OrderItemID = tdItem.ID
                            };

                        bulkOrderItem.OrderItems.Add(orderItems);

                        if (isNewBulkOrderItem)
                        {
                            bulkOrder.TDBulkOrderItems.Add(bulkOrderItem);
                        }
                    }
                }

                order.OrderBulkStatus = OrderBulkStatus.Batched;
                if (isNewBulkOrder)
                {
                    bulkOrders.Add(bulkOrder);
                }
            }
        }

        private void RemoveOrderFromBulkOrders(OrderEntity order)
        {
            if ((order.OrderAccountType == OrderAccountType.ASX || (order.OrderAccountType == OrderAccountType.TermDeposit && order.ClientManagementType == ClientManagementType.SMA)) && order.OrderType == OrderType.Manual)
            {
                if (BulkOrders != null)
                {
                    var bulkOrders = BulkOrders.Where(ss => ss.Status == BulkOrderStatus.Active && ss.OrderItemType == order.OrderItemType && ss.ClientManagementType == order.ClientManagementType && ss.OrderPreferedBy == order.OrderPreferedBy && ss.OrderAccountType == order.OrderAccountType).ToList();

                    for (int i = bulkOrders.Count - 1; i >= 0; i--)
                    {
                        var bulkOrder = bulkOrders[i];
                        //ASX Bulk Orders
                        var bulkOrderItems = bulkOrder.BulkOrderItems;
                        if (bulkOrderItems != null)
                        {
                            for (int j = bulkOrderItems.Count - 1; j >= 0; j--)
                            {
                                var bulkOrderItem = bulkOrderItems[j];
                                foreach (ASXOrderItem orderItem in order.Items)
                                {
                                    var item =
                                        bulkOrderItem.OrderItems.FirstOrDefault(
                                            ss => ss.OrderID == orderItem.OrderID && ss.OrderItemID == orderItem.ID);
                                    if (item != null)
                                    {
                                        if (bulkOrderItem.OrderItems.Count == 1)
                                        {
                                            bulkOrder.BulkOrderItems.Remove(bulkOrderItem);
                                            if (bulkOrder.BulkOrderItems.Count == 0)
                                            {
                                                BulkOrders.Remove(bulkOrder);
                                            }
                                        }
                                        else if (bulkOrderItem.OrderItems.Count > 1)
                                        {
                                            bulkOrderItem.ExpectedAmount = bulkOrderItem.ExpectedAmount -
                                                                           orderItem.SuggestedAmount;
                                            bulkOrderItem.ExpectedUnits = bulkOrderItem.ExpectedUnits -
                                                                          orderItem.SuggestedUnits;
                                            bulkOrderItem.ExpectedUnitPrice = (bulkOrderItem.ExpectedUnitPrice * 2) -
                                                                              orderItem.SuggestedUnitPrice;
                                            bulkOrderItem.OrderItems.Remove(item);
                                        }
                                    }
                                }
                            }
                        }

                        //TD Bulk Orders
                        var tdBulkOrderItems = bulkOrder.TDBulkOrderItems;
                        if (tdBulkOrderItems != null)
                        {
                            for (int j = tdBulkOrderItems.Count - 1; j >= 0; j--)
                            {
                                var tdBulkOrderItem = tdBulkOrderItems[j];
                                foreach (TermDepositOrderItem orderItem in order.Items)
                                {
                                    var item = tdBulkOrderItem.OrderItems.FirstOrDefault(ss => ss.OrderID == orderItem.OrderID && ss.OrderItemID == orderItem.ID);
                                    if (item != null)
                                    {
                                        if (tdBulkOrderItem.OrderItems.Count == 1)
                                        {
                                            bulkOrder.TDBulkOrderItems.Remove(tdBulkOrderItem);
                                            if (bulkOrder.TDBulkOrderItems.Count == 0)
                                            {
                                                BulkOrders.Remove(bulkOrder);
                                            }
                                        }
                                        else if (tdBulkOrderItem.OrderItems.Count > 1)
                                        {
                                            tdBulkOrderItem.Amount = tdBulkOrderItem.Amount - orderItem.Amount;
                                            tdBulkOrderItem.OrderItems.Remove(item);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                order.OrderBulkStatus = OrderBulkStatus.None;
            }
        }
    }
}
