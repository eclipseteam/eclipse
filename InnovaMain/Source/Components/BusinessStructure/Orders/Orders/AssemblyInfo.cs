﻿using System.Reflection;

using Oritax.TaxSimp.InstallInfo;
using Oritax.TaxSimp.CM.Entity;

[assembly: AssemblyTitle("")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("")]
[assembly: AssemblyCopyright("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: AssemblyVersion(OrdersInstall.ASSEMBLY_MAJORVERSION + "." + OrdersInstall.ASSEMBLY_MINORVERSION)]
[assembly: AssemblyInformationalVersion(OrdersInstall.ASSEMBLY_MAJORVERSION + "." + OrdersInstall.ASSEMBLY_MINORVERSION + "." + OrdersInstall.ASSEMBLY_DATAFORMAT + "." + OrdersInstall.ASSEMBLY_REVISION)]
[assembly: AssemblyInstallInfo(OrdersInstall.ASSEMBLY_ID, OrdersInstall.ASSEMBLY_NAME, OrdersInstall.ASSEMBLY_DISPLAYNAME)]
[assembly: ComponentInstallInfo(OrdersInstall.COMPONENT_ID, OrdersInstall.COMPONENT_NAME, OrdersInstall.COMPONENT_DISPLAYNAME, OrdersInstall.COMPONENT_CATEGORY)]
[assembly: ComponentVersionInstallInfo(OrdersInstall.COMPONENTVERSION_STARTDATE, OrdersInstall.COMPONENTVERSION_ENDDATE, OrdersInstall.COMPONENTVERSION_PERSISTERASSEMBLY, OrdersInstall.COMPONENTVERSION_PERSISTERCLASS, OrdersInstall.COMPONENTVERSION_IMPLEMENTATIONCLASS)]

[assembly: AssemblyDelaySign(false)]
