using System;

namespace Oritax.TaxSimp.CM.Entity
{
    public class OrdersInstall
	{
		#region INSTALLATION PROPERTIES

		// Assembly Installation Properties
        public const string ASSEMBLY_ID = "6F78B54B-C4D4-4C4B-8711-26FDDBD53072";
        public const string ASSEMBLY_NAME = "Orders";
        public const string ASSEMBLY_DISPLAYNAME = "Orders";
		public const string ASSEMBLY_MAJORVERSION="1";
		public const string ASSEMBLY_MINORVERSION="1";
		public const string ASSEMBLY_DATAFORMAT="0";
		public const string ASSEMBLY_REVISION="0";		

		// Component Installation Properties
        public const string COMPONENT_ID = "1504CBBC-4C93-43E9-8146-C97F5C6A661D";
        public const string COMPONENT_NAME = "Orders";
        public const string COMPONENT_DISPLAYNAME = "Orders";
		public const string COMPONENT_CATEGORY="BusinessEntity";

		// Component Version Installation Properties
		public const string COMPONENTVERSION_STARTDATE="01/01/1990 00:00:00 AM";
		public const string COMPONENTVERSION_ENDDATE="31/12/2050 12:59:59 AM";
		public const string COMPONENTVERSION_PERSISTERASSEMBLY="CommonPersisters_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";
		public const string COMPONENTVERSION_PERSISTERCLASS="Oritax.TaxSimp.CommonPersistence.BlobPersister";
		public const string COMPONENTVERSION_IMPLEMENTATIONCLASS="Oritax.TaxSimp.CM.Entity.OrdersCM";
        
		#endregion
	}
}
