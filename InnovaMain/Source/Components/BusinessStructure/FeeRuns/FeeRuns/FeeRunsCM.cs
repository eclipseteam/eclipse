using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Enum;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.CalculationInterface;
using System.Linq;
using Oritax.TaxSimp.DataSets;
using BankAccountEntity = Oritax.TaxSimp.Common.BankAccountEntity;
using IdentityCMDetail = Oritax.TaxSimp.Common.IdentityCMDetail;
using TermDepositAccountEntity = Oritax.TaxSimp.Common.TermDepositAccountEntity;
using Oritax.TaxSimp.Common.UMAFee;
using Oritax.TaxSimp.Common.Data;
using Oritax.TaxSimp.CM.Organization.Data;
using Oritax.TaxSimp.CM.OrganizationUnit;

namespace Oritax.TaxSimp.CM.Entity
{
    [Serializable]
    public class FeeRunsCM : EntityCM, ISerializable, IOrganizationChartData, IHaveAssociation, IFeeRun
    {
        #region Private Variable

        string shortName = string.Empty;
        string description = string.Empty;
        FeeRunType feeRunType = FeeRunType.Global;
        List<FeeTransaction> feeTransactionLists = new List<FeeTransaction>();
        int month = 1;
        int year = 2000;
        DateTime createdDate = DateTime.Now;
        DateTime lastModifiedDate = DateTime.Now;
        bool runLocalFee = false;

        #endregion

        #region Public Properties

        public bool RunLocalFee
        {
            get { return runLocalFee; }
            set { runLocalFee = value; }
        }

        public DateTime LastModifiedDate
        {
            get { return lastModifiedDate; }
            set { lastModifiedDate = value; }
        }

        public DateTime CreatedDate
        {
            get { return createdDate; }
            set { createdDate = value; }
        }

        public FeeRunType FeeRunType
        {
            get { return feeRunType; }
            set { feeRunType = value; }
        }

        public string ShortName
        {
            get { return shortName; }
            set { shortName = value; }
        }

        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        public int Month
        {
            get { return month; }
            set { month = value; }
        }

        public int Year
        {
            get { return year; }
            set { year = value; }
        } 

        public List<FeeTransaction> FeeTransactionLists
        {
            get { return feeTransactionLists; }
            set { feeTransactionLists = value; }
        } 

        #endregion

        #region Constructor
        public FeeRunsCM()
        { }

        protected FeeRunsCM(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            month = Serialize.GetSerializedInt32(info, "FeeRunsCM.Month");
            year = Serialize.GetSerializedInt32(info, "FeeRunsCM.Year");

            feeTransactionLists = Serialize.GetValue<List<FeeTransaction>>(info, "FeeRunsCM.FeeTransactionLists");
            feeRunType = (FeeRunType)Serialize.GetSerializedValue(info, "FeeRunsCM.FeeRunType",typeof(FeeRunType),FeeRunType.Global);

            shortName = Serialize.GetSerializedString(info, "FeeRunsCM.ShortName");
            description = Serialize.GetSerializedString(info, "FeeRunsCM.Description");

            createdDate = Serialize.GetSerializedDateTime(info, "FeeRunsCM.CreatedDate");
            lastModifiedDate = Serialize.GetSerializedDateTime(info, "FeeRunsCM.LastModifiedDate");

            runLocalFee = Serialize.GetSerializedBool(info, "FeeRunsCM.RunLocalFee");
        }
        #endregion

        #region Override Method
        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);

            Serialize.AddSerializedValue(info, "FeeRunsCM.Month", month);
            Serialize.AddSerializedValue(info, "FeeRunsCM.Year", year);
            Serialize.AddSerializedValue(info, "FeeRunsCM.FeeTransactionLists", feeTransactionLists);
            Serialize.AddSerializedValue(info, "FeeRunsCM.FeeRunType", feeRunType);
            Serialize.AddSerializedValue(info, "FeeRunsCM.ShortName", shortName);
            Serialize.AddSerializedValue(info, "FeeRunsCM.Description", description);
            Serialize.AddSerializedValue(info, "FeeRunsCM.CreatedDate", createdDate);
            Serialize.AddSerializedValue(info, "FeeRunsCM.LastModifiedDate", lastModifiedDate);
            Serialize.AddSerializedValue(info, "FeeRunsCM.RunLocalFee", runLocalFee);
        }
        #endregion

        protected override void OnGetData(DataSet data)
        {
            base.OnGetData(data);

            if (data is GroupFeeBankwestExportDS)
            {
                GroupFeeBankwestExportDS groupFeeBankwestExportDS = (GroupFeeBankwestExportDS)data;
                IOrganization org = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                groupFeeBankwestExportDS.FeeEnttyList = org.FeeEnttyList;
                groupFeeBankwestExportDS.Assets = org.Assets;
                groupFeeBankwestExportDS.Models = org.Model;
                groupFeeBankwestExportDS.Narration = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Month) + " " + Year.ToString() + " UMA Fees";

                foreach (FeeTransaction feeTransaction in FeeTransactions)
                {
                    IBrokerManagedComponent iBMC = Broker.GetBMCInstance(feeTransaction.ClientCID) as IBrokerManagedComponent;
                    if (iBMC != null)
                    {
                        groupFeeBankwestExportDS.FeeRunID = this.CID;
                        iBMC.GetData(groupFeeBankwestExportDS);
                    }
                }

                Broker.ReleaseBrokerManagedComponent(org);
            }

            if (data is FeeRunDetailsDS)
            { 
                FeeRunDetailsDS feeRunDetailsDS = (FeeRunDetailsDS)data;

                DataTable feeRunTable = feeRunDetailsDS.Tables[FeeRunDetailsDS.FEERUNSDETAILSTABLE];
                DataRow row = feeRunTable.NewRow();
                row[FeeRunDetailsDS.ID] = this.CID;
                row[FeeRunDetailsDS.SHORTNAME] = this.shortName;
                row[FeeRunDetailsDS.DESCRIPTION] = this.description;
                row[FeeRunDetailsDS.RUNTYPE] = Enumeration.GetDescription(this.FeeRunType);
                row[FeeRunDetailsDS.RUNTYPEENUM] = this.FeeRunType;
                row[FeeRunDetailsDS.MONTH] = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Month); 
                row[FeeRunDetailsDS.YEAR] = this.year;
                row[FeeRunDetailsDS.FEERUNDATE] = this.createdDate;
                row[FeeRunDetailsDS.TOTALFEES] = this.FeeTransactions.Sum(tran => tran.Total);
                feeRunTable.Rows.Add(row); 
            }

            if (data is FeeRunTransactionsDetailsDS)
            {
                FeeRunTransactionsDetailsDS FeeRunTransactionsDetailsDS = (FeeRunTransactionsDetailsDS)data;
                if (FeeRunTransactionsDetailsDS.FeeRunTransactionsOperation == FeeRunTransactionsOperation.GetOnlyConfiguredTemplate)
                {
                    FeeRunTransactionsDetailsDS.ConfiguredFeesTemplateAtRun = this.ConfiguredFeesList;
                    FeeRunTransactionsDetailsDS.FeeRunDetails = this.name + ", FEE TYPE: " + this.shortName;
                    FeeRunTransactionsDetailsDS.UseLocalFee = this.RunLocalFee;
                }
                else
                {
                    DataTable feeRunTable = FeeRunTransactionsDetailsDS.Tables[FeeRunTransactionsDetailsDS.FEERUNSDETAILSTRANSACTIONTABLE];
                    IOrganization iOrgCM = this.Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;

                    foreach (FeeTransaction transaction in FeeTransactions)
                    {
                        DataRow row = feeRunTable.NewRow();

                        row[FeeRunTransactionsDetailsDS.ID] = transaction.ID;
                        row[FeeRunTransactionsDetailsDS.FEERUNID] = this.CID;
                        row[FeeRunTransactionsDetailsDS.SHORTNAME] = this.shortName;
                        row[FeeRunTransactionsDetailsDS.DESCRIPTION] = this.description;
                        row[FeeRunTransactionsDetailsDS.MONTH] = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Month);
                        row[FeeRunTransactionsDetailsDS.YEAR] = this.year;
                        row[FeeRunTransactionsDetailsDS.FEERUNDATE] = this.createdDate;
                        row[FeeRunTransactionsDetailsDS.TOTALFEES] = transaction.Total;
                        row[FeeRunTransactionsDetailsDS.CLIENTCID] = transaction.ClientCID;
                        row[FeeRunTransactionsDetailsDS.CLIENTID] = transaction.ClientID;
                        row[FeeRunTransactionsDetailsDS.CLIENTNAME] = transaction.ClientName;
                        row[FeeRunTransactionsDetailsDS.ADVISERCID] = transaction.AdviserCID;
                        row[FeeRunTransactionsDetailsDS.ADVISERNAME] = transaction.AdviserName;
                        row[FeeRunTransactionsDetailsDS.ADVISERID] = transaction.AdviserID;
                        row[FeeRunTransactionsDetailsDS.CLENTTYPE] = transaction.ClientType;
                        feeRunTable.Rows.Add(row);
                    }

                    DataTable feeTemplateList = FeeRunTransactionsDetailsDS.Tables[FeeRunTransactionsDetailsDS.TEMPLATELISTTABLE];

                    foreach (Guid feeTemplateGuid in ConfiguredFeesList)
                    {
                        DataRow row = feeTemplateList.NewRow();
                        var feeTemplate = iOrgCM.FeeEnttyList.Where(feeTemp => feeTemp.ID == feeTemplateGuid).FirstOrDefault();

                        row[FeeRunTransactionsDetailsDS.ID] = feeTemplate.ID;
                        row[FeeRunTransactionsDetailsDS.DESCRIPTION] = feeTemplate.Description;
                        feeTemplateList.Rows.Add(row);

                        if (!FeeRunTransactionsDetailsDS.FeeType.Contains(feeTemplate.FeeType))
                            FeeRunTransactionsDetailsDS.FeeType.Add(feeTemplate.FeeType);
                    }

                    FeeRunTransactionsDetailsDS.ConfiguredFeesTemplateAtRun = this.ConfiguredFeesList;
                    FeeRunTransactionsDetailsDS.FeeRunDetails = this.name + ", FEE TYPE: " + this.shortName;
                    FeeRunTransactionsDetailsDS.UseLocalFee = this.RunLocalFee;
                    this.Broker.ReleaseBrokerManagedComponent(iOrgCM);
                }
            }

            if (data is FeeRunTransactionsDetailsByFeeBreakDownDS)
            {
                FeeRunTransactionsDetailsByFeeBreakDownDS FeeRunTransactionsDetailsByFeeBreakDownDS = (FeeRunTransactionsDetailsByFeeBreakDownDS)data;
                DataTable feeRunTable = FeeRunTransactionsDetailsByFeeBreakDownDS.Tables[FeeRunTransactionsDetailsByFeeBreakDownDS.FEERUNSDETAILSTRANSACTIONTABLE];

                IOrganization iOrgCM = this.Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;


                foreach (FeeTransaction transaction in FeeTransactions)
                {
                   IOrganizationUnit clientData = this.Broker.GetBMCInstance(transaction.ClientCID) as IOrganizationUnit;
                   FeeTransactionDetailsDS feeTransactionDetailsDS = new FeeTransactionDetailsDS();
                   feeTransactionDetailsDS.FeeTransactionID = transaction.ID;
                   if (clientData != null)
                   {
                       clientData.GetData(feeTransactionDetailsDS);
                       DataView View = new DataView(feeTransactionDetailsDS.Tables[FeeTransactionDetailsDS.CALCULATEDFEESSUMMARYBYFEETYPE]);
                       View.Sort = FeeTransactionDetailsDS.FEETYPE + " ASC";
                       View.RowFilter = FeeTransactionDetailsDS.DESCRIPTION + " LIKE '%Total%'";
                       DataTable sortedFeeTable = View.ToTable();

                       foreach (DataRow rowDetail in sortedFeeTable.Rows)
                       {
                           DataRow row = feeRunTable.NewRow();

                           row[FeeRunTransactionsDetailsByFeeBreakDownDS.ID] = transaction.ID;
                           row[FeeRunTransactionsDetailsByFeeBreakDownDS.FEERUNID] = this.CID;
                           row[FeeRunTransactionsDetailsByFeeBreakDownDS.SHORTNAME] = this.shortName;
                           row[FeeRunTransactionsDetailsByFeeBreakDownDS.DESCRIPTION] = this.Name;
                           row[FeeRunTransactionsDetailsByFeeBreakDownDS.MONTH] = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Month);
                           row[FeeRunTransactionsDetailsByFeeBreakDownDS.YEAR] = this.year;
                           row[FeeRunTransactionsDetailsByFeeBreakDownDS.FEERUNDATE] = this.createdDate;
                           row[FeeRunTransactionsDetailsByFeeBreakDownDS.TOTALFEES] = rowDetail[FeeTransactionDetailsDS.CALCULATEDFEES];
                           row[FeeRunTransactionsDetailsByFeeBreakDownDS.CLIENTCID] = transaction.ClientCID;
                           row[FeeRunTransactionsDetailsByFeeBreakDownDS.CLIENTID] = transaction.ClientID;
                           row[FeeRunTransactionsDetailsByFeeBreakDownDS.CLIENTNAME] = transaction.ClientName;
                           row[FeeRunTransactionsDetailsByFeeBreakDownDS.ADVISERCID] = transaction.AdviserCID;

                           IOrganizationUnit dealerGroup = null;
                           IOrganizationUnit ifaUnit = null;

                           if (transaction.AdviserCID != null && transaction.AdviserCID != Guid.Empty)
                           {
                               IOrganizationUnit advUnit = Broker.GetBMCInstance(transaction.AdviserCID) as IOrganizationUnit;
                               ICmHasParent icmHasParent = advUnit as ICmHasParent;
                               ifaUnit = Broker.GetCMImplementation(icmHasParent.Parent.Clid, icmHasParent.Parent.Csid) as IOrganizationUnit;
                               if (ifaUnit == null)
                               {
                                   ifaUnit = Broker.GetBMCInstance(icmHasParent.Parent.Cid) as IOrganizationUnit;
                                   if (ifaUnit != null)
                                       row[FeeRunTransactionsDetailsByFeeBreakDownDS.IFANAME] = ifaUnit.Name;
                               }
                               else
                                   row[FeeRunTransactionsDetailsByFeeBreakDownDS.IFANAME] = ifaUnit.Name;
                           }

                           if (ifaUnit != null)
                           {
                               ICmHasParent icmHasParent = ifaUnit as ICmHasParent;
                               dealerGroup = Broker.GetCMImplementation(icmHasParent.Parent.Clid, icmHasParent.Parent.Csid) as IOrganizationUnit;
                               if (dealerGroup == null)
                               {
                                   dealerGroup = Broker.GetBMCInstance(icmHasParent.Parent.Cid) as IOrganizationUnit;
                                   if (dealerGroup != null)
                                       row[FeeRunTransactionsDetailsByFeeBreakDownDS.DEEALERGROUPNAME] = dealerGroup.Name;
                               }
                               else
                                   row[FeeRunTransactionsDetailsByFeeBreakDownDS.DEEALERGROUPNAME] = dealerGroup.Name;
                           }

                           row[FeeRunTransactionsDetailsByFeeBreakDownDS.ADVISERNAME] = transaction.AdviserName;
                           row[FeeRunTransactionsDetailsByFeeBreakDownDS.ADVISERID] = transaction.AdviserID;
                           row[FeeRunTransactionsDetailsByFeeBreakDownDS.CLENTTYPE] = transaction.ClientType;
                           row[FeeRunTransactionsDetailsByFeeBreakDownDS.FEETYPEDESCRIPTION] = rowDetail[FeeTransactionDetailsDS.FEETYPE];
                           row[FeeRunTransactionsDetailsByFeeBreakDownDS.FEETYPE] = Enumeration.GetDescription((FeeType)rowDetail[FeeTransactionDetailsDS.FEETYPEENUM]);

                           feeRunTable.Rows.Add(row);
                       }
                   }
                }

                this.Broker.ReleaseBrokerManagedComponent(iOrgCM);
            }

            if (data is FeeRunTransactionsDetailsByHoldingsDS)
            {
                FeeRunTransactionsDetailsByHoldingsDS FeeRunTransactionsDetailsByHoldingsDS = (FeeRunTransactionsDetailsByHoldingsDS)data;
                DataTable feeRunTable = FeeRunTransactionsDetailsByHoldingsDS.Tables[FeeRunTransactionsDetailsByHoldingsDS.FEEHOLDINGTABLE];

                IOrganization iOrgCM = this.Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
              
                var withoutDupesList = FeeTransactions.Distinct();

                foreach (FeeTransaction transaction in withoutDupesList)
                {
                    foreach (FeeHoldingEntity holdingEntity in transaction.FeeHoldingEntityList)
                    {
                        DataRow row = feeRunTable.NewRow();
                        string proName = string.Empty;
                        var product = iOrgCM.Products.Where(pro => pro.ID == holdingEntity.ProductID).FirstOrDefault();
                        
                        if (product != null)
                            proName = product.Name; 

                        row[FeeRunTransactionsDetailsByHoldingsDS.ID] = transaction.ID;
                        row[FeeRunTransactionsDetailsByHoldingsDS.FEERUNID] = this.CID;
                        row[FeeRunTransactionsDetailsByHoldingsDS.SHORTNAME] = this.shortName;
                        row[FeeRunTransactionsDetailsByHoldingsDS.DESCRIPTION] = this.Name;
                        row[FeeRunTransactionsDetailsByHoldingsDS.MONTH] = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Month);
                        row[FeeRunTransactionsDetailsByHoldingsDS.YEAR] = this.year;
                        row[FeeRunTransactionsDetailsByHoldingsDS.FEERUNDATE] = this.createdDate;
                        row[FeeRunTransactionsDetailsByHoldingsDS.TOTALFEES] = holdingEntity.HoldingValue;
                        row[FeeRunTransactionsDetailsByHoldingsDS.CLIENTCID] = transaction.ClientCID;
                        row[FeeRunTransactionsDetailsByHoldingsDS.CLIENTID] = transaction.ClientID;
                        row[FeeRunTransactionsDetailsByHoldingsDS.CLIENTNAME] = transaction.ClientName;
                        row[FeeRunTransactionsDetailsByHoldingsDS.ADVISERCID] = transaction.AdviserCID;
                        row[FeeRunTransactionsDetailsByHoldingsDS.DEEALERGROUPNAME] = String.Empty;
                       
                        IOrganizationUnit dealerGroup = null;
                        IOrganizationUnit ifaUnit = null;

                        if (transaction.AdviserCID != null && transaction.AdviserCID != Guid.Empty)
                        {
                            IOrganizationUnit advUnit = Broker.GetBMCInstance(transaction.AdviserCID) as IOrganizationUnit;
                            ICmHasParent icmHasParent = advUnit as ICmHasParent;
                            ifaUnit = Broker.GetCMImplementation(icmHasParent.Parent.Clid, icmHasParent.Parent.Csid) as IOrganizationUnit;
                            if (ifaUnit == null)
                            {
                                ifaUnit = Broker.GetBMCInstance(icmHasParent.Parent.Cid) as IOrganizationUnit;
                                if (ifaUnit != null)
                                    row[FeeRunTransactionsDetailsByHoldingsDS.IFANAME] = ifaUnit.Name;
                            }
                            else
                                row[FeeRunTransactionsDetailsByHoldingsDS.IFANAME] = ifaUnit.Name;
                        }

                        if (ifaUnit != null)
                        {
                            ICmHasParent icmHasParent = ifaUnit as ICmHasParent;
                            dealerGroup = Broker.GetCMImplementation(icmHasParent.Parent.Clid, icmHasParent.Parent.Csid) as IOrganizationUnit;
                            if (dealerGroup == null)
                            {
                                dealerGroup = Broker.GetBMCInstance(icmHasParent.Parent.Cid) as IOrganizationUnit;
                                if (dealerGroup != null)
                                    row[FeeRunTransactionsDetailsByHoldingsDS.DEEALERGROUPNAME] = dealerGroup.Name;
                            }
                            else
                                row[FeeRunTransactionsDetailsByHoldingsDS.DEEALERGROUPNAME] = dealerGroup.Name;
                        }

                        row[FeeRunTransactionsDetailsByHoldingsDS.ADVISERNAME] = transaction.AdviserName;
                        row[FeeRunTransactionsDetailsByHoldingsDS.ADVISERID] = transaction.AdviserID;
                        row[FeeRunTransactionsDetailsByHoldingsDS.CLENTTYPE] = transaction.ClientType;
                        row[FeeRunTransactionsDetailsByHoldingsDS.TRANSACTIONDATE] = holdingEntity.TransactionDate;
                        row[FeeRunTransactionsDetailsByHoldingsDS.PRODUCTID] = holdingEntity.ProductID;
                        row[FeeRunTransactionsDetailsByHoldingsDS.PRODUCTNAME] = proName;
                        row[FeeRunTransactionsDetailsByHoldingsDS.TOTALFUM] = holdingEntity.TotalHoldingValue;
                        row[FeeRunTransactionsDetailsByHoldingsDS.SERVICESTYPE] = Enumeration.GetDescription(holdingEntity.ServiceTypes);

                        feeRunTable.Rows.Add(row);
                    }
                }

                this.Broker.ReleaseBrokerManagedComponent(iOrgCM);
            }
        }

        protected override ModifiedState OnSetData(DataSet data)
        {
            if (data is ConsolidationFeeDS)
            {
                ConsolidationFeeDS consolidationFeeDS = data as ConsolidationFeeDS;
                var feeGroup = consolidationFeeDS.Tables[ConsolidationFeeDS.CONSOLIDATIONFEETABLE].Select().Where(c => c[ConsolidationFeeDS.CLIENTCID].ToString() == this.CID.ToString());

                foreach (var fee in feeGroup)
                {
                    Guid feeTranID = (Guid)fee[ConsolidationFeeDS.FEETRANID];
                    Guid feeTypeID = (Guid)fee[ConsolidationFeeDS.FEETYPEID];
                    decimal feeManAdjust = (decimal)fee[ConsolidationFeeDS.ADJUST];

                    var selectedTransactions = this.FeeTransactionLists.Where(t => t.FeeRunID == consolidationFeeDS.FeeRunID).Where(t => t.ID == feeTranID).FirstOrDefault();
                    if (selectedTransactions.ConfiguredFeesListWithManualAdjustment.ContainsKey(feeTypeID))
                        selectedTransactions.ConfiguredFeesListWithManualAdjustment[feeTypeID] = feeManAdjust;
                    else
                        selectedTransactions.ConfiguredFeesListWithManualAdjustment.Add(feeTypeID, feeManAdjust);
                }

                var feeRun = this.Broker.GetBMCInstance(consolidationFeeDS.FeeRunID);
            }
            if (data is FeeRunDetailsDS)
            {
                FeeRunDetailsDS feeRunDetailsDS = (FeeRunDetailsDS)data;

                DataTable feeRunTable = feeRunDetailsDS.Tables[FeeRunDetailsDS.FEERUNSDETAILSTABLE];

                if (feeRunTable.Rows.Count > 0)
                {
                    this.shortName = (String)feeRunTable.Rows[0][FeeRunDetailsDS.SHORTNAME];
                    this.month = (int)feeRunTable.Rows[0][FeeRunDetailsDS.MONTHINT];
                    this.year = (int)feeRunTable.Rows[0][FeeRunDetailsDS.YEAR];
                    this.FeeRunType = (FeeRunType)feeRunTable.Rows[0][FeeRunDetailsDS.RUNTYPEENUM];
                }
            }
            if (data is GroupFeeRunDS)
            {
                GroupFeeRunDS groupFeeRunDS = (GroupFeeRunDS)data;

                if (groupFeeRunDS.DataSetOperationType == DataSetOperationType.DeletSingle)
                {
                    foreach (FeeTransaction transaction in FeeTransactions)
                    {
                        IOrganizationUnit orgUnit = Broker.GetBMCInstance(transaction.ClientCID) as IOrganizationUnit;
                        if(orgUnit != null)
                            orgUnit.SetData(groupFeeRunDS);
                    }
                }
            }

            if (data is FeeTransactionDetailsDS)
            {
                FeeTransactionDetailsDS feeTransactionDetailsDS = (FeeTransactionDetailsDS)data;

                if (feeTransactionDetailsDS.DataSetOperationType == DataSetOperationType.DeletSingle)
                {
                    var feeTransaction = this.FeeTransactions.Where(fee => fee.ClientCID == feeTransactionDetailsDS.ClientID).FirstOrDefault();
                    if (feeTransaction != null)
                        FeeTransactions.Remove(feeTransaction);
                }

                else if (feeTransactionDetailsDS.DataSetOperationType == DataSetOperationType.UpdateSingle)
                {
                    if (feeTransactionDetailsDS.UseConsolidationRatios)
                    {
                        var feeTransactions = this.FeeTransactions.Where(fee => fee.ClientCID == feeTransactionDetailsDS.ClientID).ToList();
                        foreach(var feeTran in feeTransactions)
                            FeeTransactions.Remove(feeTran);

                        FeeTransactions.Add(feeTransactionDetailsDS.FeeTransaction); 
                    }
                    else
                    {
                        var feeTransaction = this.FeeTransactions.Where(fee => fee.ID == feeTransactionDetailsDS.FeeTransactionID).FirstOrDefault();
                        if (feeTransaction != null)
                        {
                            feeTransaction.AdviserCID = feeTransactionDetailsDS.AdviserCID;
                            feeTransaction.AdviserID = feeTransactionDetailsDS.AdviserID;
                            feeTransaction.AdviserName = feeTransactionDetailsDS.AdviserName;
                        }
                    }
                }
                else if (feeTransactionDetailsDS.DataSetOperationType == DataSetOperationType.UpdateBulk)
                {
                    var feeTransaction = this.FeeTransactions.Where(fee => fee.ID == feeTransactionDetailsDS.FeeTransactionID).FirstOrDefault();
                    if (feeTransaction != null)
                        feeTransaction.ManAdjustment = feeTransactionDetailsDS.ManAdjustment;
                }
            }

            if (data is FeeRunTransactionsDetailsDS)
            {
                FeeRunTransactionsDetailsDS feeRunTransactionsDetailsDS = (FeeRunTransactionsDetailsDS)data;

                if (feeRunTransactionsDetailsDS.FeeRunTransactionsOperation == FeeRunTransactionsOperation.AddFeeTemplate)
                {
                    DataTable templateListTable = feeRunTransactionsDetailsDS.Tables[FeeRunTransactionsDetailsDS.TEMPLATELISTTABLE];
                    foreach (DataRow row in templateListTable.Rows)
                    {
                        Guid feeTemplateID = (Guid)row[FeeRunTransactionsDetailsDS.ID];
                        string description = (string)row[FeeRunTransactionsDetailsDS.DESCRIPTION];

                        var feeRunTemplate = this.ConfiguredFeesList.Where(feeTempID => feeTempID == feeTemplateID).FirstOrDefault();
                        if (feeRunTemplate == Guid.Empty)
                            ConfiguredFeesList.Add(feeTemplateID);
                    }
                }

                if (feeRunTransactionsDetailsDS.FeeRunTransactionsOperation == FeeRunTransactionsOperation.DeleteFeeTemplate)
                {
                    var feeRunTemplate = this.ConfiguredFeesList.Where(fee => fee == feeRunTransactionsDetailsDS.FeeTemplateID).FirstOrDefault();
                    if (feeRunTemplate != null)
                        ConfiguredFeesList.Remove(feeRunTemplate);
                }
            }

            if (data is GroupFeeTransactionDetailsDS)
            {
                GroupFeeTransactionDetailsDS feeTransactionDetailsDS = (GroupFeeTransactionDetailsDS)data;

                if (feeTransactionDetailsDS.FeeOperationType == FeeOperationType.UseLocalFee)
                {
                    this.RunLocalFee = feeTransactionDetailsDS.UseLocalFee;
                }

                if (feeTransactionDetailsDS.DataSetOperationType == DataSetOperationType.DeletSingle)
                {
                    var feeTransaction = this.FeeTransactions.Where(fee => fee.ClientCID == feeTransactionDetailsDS.ClientID).FirstOrDefault();
                    if (feeTransaction != null)
                    {
                        FeeTransactions.Remove(feeTransaction);
                        feeTransactionDetailsDS.FeeRunID = this.CID;
                        feeTransactionDetailsDS.ClientID = feeTransaction.ClientCID;
                        IBrokerManagedComponent bmc = Broker.GetBMCInstance(feeTransaction.ClientCID) as IBrokerManagedComponent;
                        bmc.SetData(data);
                        Broker.ReleaseBrokerManagedComponent(bmc);
                    }
                }
            }

            if (data is ExecuteFeeRunDS)
            {
                IOrganization org = this.Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                OrganisationListingDS organisationListingDS = new OrganisationListingDS();
                organisationListingDS.OrganisationListingOperationType = OrganisationListingOperationType.ClientsWithFUM;
                IBrokerManagedComponent user = this.Broker.GetBMCInstance(this.Broker.UserContext.Identity.Name, "DBUser_1_1");
                object[] args = new Object[3] { org.CLID, user.CID, EnableSecurity.SecuritySetting };
                DataView view = new DataView(this.Broker.GetBMCDetailList(OrganizationInstall.ASSEMBLY_NAME, args).Tables["Entities_Table"]);
                view.RowFilter = organisationListingDS.GetRowFilterAllClientsExceptEclipse();
                view.Sort = "ENTITYNAME_FIELD ASC";
                DataTable orgFiteredTable = view.ToTable();

                this.FeeTransactions.Clear();

                List<FeeType> feeTypeList = new List<FeeType>();

                foreach (Guid feeTemplateGuid in ConfiguredFeesList)
                {
                    var feeTemplate = org.FeeEnttyList.Where(feeTemp => feeTemp.ID == feeTemplateGuid).FirstOrDefault();

                    if (!feeTypeList.Contains(feeTemplate.FeeType))
                        feeTypeList.Add(feeTemplate.FeeType);
                }

                foreach (DataRow row in orgFiteredTable.Rows)
                {
                    Guid ciid = new Guid(row["ENTITYCIID_FIELD"].ToString());
                    IOrganizationUnit organizationUnit = this.Broker.GetBMCInstance(ciid) as IOrganizationUnit;

                    if (organizationUnit.IsInvestableClient && (organizationUnit.ConfiguredFeesList.Count > 0 || RunLocalFee))
                    {
                        Broker.SaveOverride = true;
                        UMAEntityFeesDS umaEntityFeesDS = new UMAEntityFeesDS();
                        umaEntityFeesDS.UseLocalFee = RunLocalFee;
                        umaEntityFeesDS.DataSetOperationType = DataSetOperationType.NewSingle;
                        umaEntityFeesDS.Month = this.month;
                        umaEntityFeesDS.Year = this.year;
                        umaEntityFeesDS.FeeRunID = this.CID;
                        umaEntityFeesDS.ConfiguredFeesTemplateAtRun = this.ConfiguredFeesList;
                        umaEntityFeesDS.FeeType = feeTypeList;
                        organizationUnit.GetData(umaEntityFeesDS);
                        umaEntityFeesDS.FeeTransaction.UseLocalFee = RunLocalFee;
                        if (umaEntityFeesDS.FeeTransaction.Total != 0)
                        {
                            organizationUnit.SetData(umaEntityFeesDS);
                            umaEntityFeesDS.FeeRunType = FeeRunType.Global;

                            if (this.FeeTransactions.Count > 0)
                            {
                                var feeTransaction = this.FeeTransactions.Where(fee => fee.ClientCID == umaEntityFeesDS.FeeTransaction.ClientCID).FirstOrDefault();

                                if (feeTransaction == null)
                                    this.FeeTransactions.Add(umaEntityFeesDS.FeeTransaction);
                            }
                            else
                                this.FeeTransactions.Add(umaEntityFeesDS.FeeTransaction);
                        }
                    }
                }

                this.Broker.ReleaseBrokerManagedComponent(org);
            }

            return ModifiedState.MODIFIED;
        }

        #region OrganizationsChart

        public OrganizationChart GetOrganizationChart()
        {
            OrganizationChart chart = new OrganizationChart();

            chart.Clid = CLID;
            chart.Csid = CSID;
            chart.NodeName = this.name;

            return chart;
        }

        public OrganizationChart GetAssociation(IIdentityCM identitycm, OrganizationType organizationtype)
        {
            OrganizationChart OChart = new OrganizationChart();
            return OChart;
        }

        public OrganizationChart GetEntityParent(OrganizationChart organizationchart)
        {
            return null;
        }

        public OrganizationChart GetEntityChild()
        {
            return null;
        }

        public OrganizationChart GetEntityChildWithEntity()
        {
            return null;
        }

        #endregion
    }
}
