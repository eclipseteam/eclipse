using System;

namespace Oritax.TaxSimp.CM.Entity
{
    public class FeeRunsInstall
	{
		#region INSTALLATION PROPERTIES

		// Assembly Installation Properties
        public const string ASSEMBLY_ID = "AE8DCE8C-0343-461C-8152-7FA4F4E22427";
        public const string ASSEMBLY_NAME = "FeeRuns";
        public const string ASSEMBLY_DISPLAYNAME = "FeeRuns";
		public const string ASSEMBLY_MAJORVERSION="1";
		public const string ASSEMBLY_MINORVERSION="1";
		public const string ASSEMBLY_DATAFORMAT="0";
		public const string ASSEMBLY_REVISION="0";		

		// Component Installation Properties
        public const string COMPONENT_ID = "2CBFA60B-8E1D-47BF-838E-BF0031A38669";
        public const string COMPONENT_NAME = "FeeRuns";
        public const string COMPONENT_DISPLAYNAME = "FeeRuns";
		public const string COMPONENT_CATEGORY="BusinessEntity";

		// Component Version Installation Properties
		public const string COMPONENTVERSION_STARTDATE="01/01/1990 00:00:00 AM";
		public const string COMPONENTVERSION_ENDDATE="31/12/2050 12:59:59 AM";
		public const string COMPONENTVERSION_PERSISTERASSEMBLY="CommonPersisters_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";
		public const string COMPONENTVERSION_PERSISTERCLASS="Oritax.TaxSimp.CommonPersistence.BlobPersister";
        public const string COMPONENTVERSION_IMPLEMENTATIONCLASS = "Oritax.TaxSimp.CM.Entity.FeeRunsCM";

		#endregion

	}
}
