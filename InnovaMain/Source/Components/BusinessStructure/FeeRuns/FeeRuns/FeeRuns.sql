--Fee Runs Assembly Update

DELETE FROM [Innova2011].[dbo].[ASSEMBLY]
      WHERE ID = 'AE8DCE8C-0343-461C-8152-7FA4F4E22427'
GO


INSERT INTO [Innova2011].[dbo].[ASSEMBLY]
           ([ID]
           ,[NAME]
           ,[DISPLAYNAME]
           ,[STRONGNAME]
           ,[MAJORVERSION]
           ,[MINORVERSION]
           ,[DATAFORMAT]
           ,[REVISION])
     VALUES
     (
           'AE8DCE8C-0343-461C-8152-7FA4F4E22427',
           'FeeRuns',
           'FeeRuns', 
           'FeeRuns, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215',
           1,
           1,
           0,
           0
           )
GO

DELETE FROM [Innova2011].[dbo].[COMPONENT]
      WHERE ID = '2CBFA60B-8E1D-47BF-838E-BF0031A38669'
GO


INSERT INTO [Innova2011].[dbo].[COMPONENT]
           ([ID]
           ,[NAME]
           ,[DISPLAYNAME]
           ,[CATEGORY]
           ,[APPLICABILITY]
           ,[OBSOLETE])
     VALUES (
           '2CBFA60B-8E1D-47BF-838E-BF0031A38669',
           'FeeRuns',
           'FeeRuns',
           'BusinessEntity',
           3,
           'False'
           )
GO

DELETE FROM [Innova2011].[dbo].[COMPONENTVERSION]
      WHERE ID = 'AE8DCE8C-0343-461C-8152-7FA4F4E22427'
GO


INSERT INTO [Innova2011].[dbo].[COMPONENTVERSION]
           ([ID]
           ,[VERSIONNAME]
           ,[COMPONENTID]
           ,[STARTDATE]
           ,[ENDDATE]
           ,[PERSISTERASSEMBLYSTRONGNAME]
           ,[PERSISTERCLASS]
           ,[IMPLEMENTATIONCLASS]
           ,[OBSOLETE])
     VALUES
     (
           'AE8DCE8C-0343-461C-8152-7FA4F4E22427',
           '',
           '2CBFA60B-8E1D-47BF-838E-BF0031A38669',
           '1990-01-01 00:00:00.000',
           '2050-12-31 00:59:59.000',
           'CommonPersisters_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215',
           'Oritax.TaxSimp.CommonPersistence.BlobPersister',
           'Oritax.TaxSimp.CM.Entity.FeeRunsCM',
           'False'
           )
GO