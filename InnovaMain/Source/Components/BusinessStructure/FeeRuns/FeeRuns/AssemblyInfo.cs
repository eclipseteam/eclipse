﻿using System.Reflection;

using Oritax.TaxSimp.InstallInfo;
using Oritax.TaxSimp.CM.Entity;

[assembly: AssemblyTitle("")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("")]
[assembly: AssemblyCopyright("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: AssemblyVersion(FeeRunsInstall.ASSEMBLY_MAJORVERSION + "." + FeeRunsInstall.ASSEMBLY_MINORVERSION)]
[assembly: AssemblyInformationalVersion(FeeRunsInstall.ASSEMBLY_MAJORVERSION + "." + FeeRunsInstall.ASSEMBLY_MINORVERSION + "." + FeeRunsInstall.ASSEMBLY_DATAFORMAT + "." + FeeRunsInstall.ASSEMBLY_REVISION)]
[assembly: AssemblyInstallInfo(FeeRunsInstall.ASSEMBLY_ID, FeeRunsInstall.ASSEMBLY_NAME, FeeRunsInstall.ASSEMBLY_DISPLAYNAME)]
[assembly: ComponentInstallInfo(FeeRunsInstall.COMPONENT_ID, FeeRunsInstall.COMPONENT_NAME, FeeRunsInstall.COMPONENT_DISPLAYNAME, FeeRunsInstall.COMPONENT_CATEGORY)]
[assembly: ComponentVersionInstallInfo(FeeRunsInstall.COMPONENTVERSION_STARTDATE, FeeRunsInstall.COMPONENTVERSION_ENDDATE, FeeRunsInstall.COMPONENTVERSION_PERSISTERASSEMBLY, FeeRunsInstall.COMPONENTVERSION_PERSISTERCLASS, FeeRunsInstall.COMPONENTVERSION_IMPLEMENTATIONCLASS)]

[assembly: AssemblyDelaySign(false)]
