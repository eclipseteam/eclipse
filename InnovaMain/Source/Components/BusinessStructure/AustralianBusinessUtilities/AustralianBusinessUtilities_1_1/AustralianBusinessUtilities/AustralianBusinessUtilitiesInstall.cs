using System;

namespace Oritax.TaxSimp.AustralianBusinessUtilities
{
	/// <summary>
	/// Summary description for AustralianBusinessUtilitiesInstall.
	/// </summary>
	public class AustralianBusinessUtilitiesInstall
	{
		#region INSTALLATION PROPERTIES

		// Assembly Installation Properties
		public const string ASSEMBLY_ID="E2587706-C409-4067-A88D-23C53508C6CF";
		public const string ASSEMBLY_NAME="AustralianBusinessUtilities_1_1";
		public const string ASSEMBLY_DISPLAYNAME="AustralianBusinessUtilities V1.1";
		public const string ASSEMBLY_MAJORVERSION="1";
		public const string ASSEMBLY_MINORVERSION="1";
		public const string ASSEMBLY_DATAFORMAT="0";
		public const string ASSEMBLY_REVISION="1"; 

        #endregion
	}
}
