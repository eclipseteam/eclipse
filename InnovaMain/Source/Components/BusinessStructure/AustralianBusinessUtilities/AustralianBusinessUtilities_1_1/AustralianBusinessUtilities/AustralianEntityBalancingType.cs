using System;

namespace Oritax.TaxSimp.AustralianBusinessUtilities
{
	/// <summary>
	/// The entity's balancing type, e.g. Normal, Early Balancer, Late Balancer
	/// </summary>
	public enum AustralianEntityBalancingType
	{
		/// <summary>
		/// Normal balancing company
		/// </summary>
		Normal,
		/// <summary>
		/// The entity is an early balancer
		/// </summary>
		EarlyBalancer,
		/// <summary>
		/// The entity is a late balancer
		/// </summary>
		LateBalancer
	}
}
