#region Using
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Xml.Linq;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.Group;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CM.Entity;
using Oritax.TaxSimp.Extensions;
using System.Collections.ObjectModel;
using Oritax.TaxSimp.Utilities;
using CashManagementEntity = Oritax.TaxSimp.Common.CashManagementEntity;
using IdentityCMDetail = Oritax.TaxSimp.Common.IdentityCMDetail;

#endregion

namespace Oritax.TaxSimp.CM
{
    public class CapitalFlowReportUtility
    {
        public DateTime LastDayOfMonthFromDateTime(DateTime dateTime)
        {
            DateTime firstDayOfTheMonth = new DateTime(dateTime.Year, dateTime.Month, 1);
            return firstDayOfTheMonth.AddMonths(1).AddDays(-1);
        }

        public DateTime FirstDayOfMonthFromDateTime(DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, 1);
        }

        public void OnGetDataCapitalFlowDataSetCorporateInd(CapitalReportDS data, string clientType, string clientID, string clientName, IdentityCM Parent, ICMBroker Broker, ClientIndividualEntity Entity, object clientCID)
        {

            DataTable clientSummaryTable = data.Tables[CapitalReportDS.CLIENTSUMMARYTABLE];
            DataTable contactTable = data.Tables[CapitalReportDS.CONTACTTABLE];

            DataRow clientSummaryRow = clientSummaryTable.NewRow();
            DataRow contactRow = contactTable.NewRow();
            clientSummaryRow[CapitalReportDS.CLIENTCID] = clientCID;
            clientSummaryRow[CapitalReportDS.CLIENTID] = clientID;
            clientSummaryRow[CapitalReportDS.CLIENTNAME] = clientName;
            clientSummaryRow[CapitalReportDS.CIENTABN] = Entity.ABN;
            clientSummaryRow[CapitalReportDS.CLIENTTYPE] = clientType;

            if (Parent != null && Parent.Clid != Guid.Empty && Parent.Csid != Guid.Empty)
            {
                AdvisorCM adviserUnit = Broker.GetCMImplementation(Parent.Clid, Parent.Csid) as AdvisorCM;
                if (adviserUnit != null)
                {
                    clientSummaryRow[HoldingRptDataSet.ADVISERFULLNAME] = adviserUnit.AdvisorEntity.FullName;
                    clientSummaryRow[HoldingRptDataSet.ADVISERCID] = adviserUnit.CID;
                    clientSummaryRow[HoldingRptDataSet.ADVISERID] = adviserUnit.EclipseClientID;
                    Broker.ReleaseBrokerManagedComponent(adviserUnit);
                }
            }

            CSVInstance csvinstance = new CSVInstance(Broker);
            ClientIndividualEntityCSV _entitycsv = csvinstance.GetEntityConcreteInstance(Entity);

            if (data.PrintAddress)
            {

                if (Entity.Address.MailingAddress.Addressline1 != string.Empty)
                {
                    clientSummaryRow[HoldingRptDataSet.ADDRESSLINE1] = Entity.Address.MailingAddress.Addressline1;
                    clientSummaryRow[HoldingRptDataSet.ADDRESSLINE2] = Entity.Address.MailingAddress.Addressline2;
                    clientSummaryRow[HoldingRptDataSet.SUBURB] = Entity.Address.MailingAddress.Suburb;
                    clientSummaryRow[HoldingRptDataSet.STATE] = Entity.Address.MailingAddress.State;
                    clientSummaryRow[HoldingRptDataSet.POSTCODE] = Entity.Address.MailingAddress.PostCode;
                    clientSummaryRow[HoldingRptDataSet.COUNTRY] = Entity.Address.MailingAddress.Country;
                }
                else if (Entity.Address.ResidentialAddress.Addressline1 != string.Empty)
                {
                    clientSummaryRow[HoldingRptDataSet.ADDRESSLINE1] = Entity.Address.ResidentialAddress.Addressline1;
                    clientSummaryRow[HoldingRptDataSet.ADDRESSLINE2] = Entity.Address.ResidentialAddress.Addressline2;
                    clientSummaryRow[HoldingRptDataSet.SUBURB] = Entity.Address.ResidentialAddress.Suburb;
                    clientSummaryRow[HoldingRptDataSet.STATE] = Entity.Address.ResidentialAddress.State;
                    clientSummaryRow[HoldingRptDataSet.POSTCODE] = Entity.Address.ResidentialAddress.PostCode;
                    clientSummaryRow[HoldingRptDataSet.COUNTRY] = Entity.Address.ResidentialAddress.Country;
                }
                else if (Entity.Address.RegisteredAddress.Addressline1 != string.Empty)
                {
                    clientSummaryRow[HoldingRptDataSet.ADDRESSLINE1] = Entity.Address.RegisteredAddress.Addressline1;
                    clientSummaryRow[HoldingRptDataSet.ADDRESSLINE2] = Entity.Address.RegisteredAddress.Addressline2;
                    clientSummaryRow[HoldingRptDataSet.SUBURB] = Entity.Address.RegisteredAddress.Suburb;
                    clientSummaryRow[HoldingRptDataSet.STATE] = Entity.Address.RegisteredAddress.State;
                    clientSummaryRow[HoldingRptDataSet.POSTCODE] = Entity.Address.RegisteredAddress.PostCode;
                    clientSummaryRow[HoldingRptDataSet.COUNTRY] = Entity.Address.RegisteredAddress.Country;
                }
                else if (Entity.Address.BusinessAddress.Addressline1 != string.Empty)
                {
                    clientSummaryRow[HoldingRptDataSet.ADDRESSLINE1] = Entity.Address.BusinessAddress.Addressline1;
                    clientSummaryRow[HoldingRptDataSet.ADDRESSLINE2] = Entity.Address.BusinessAddress.Addressline2;
                    clientSummaryRow[HoldingRptDataSet.SUBURB] = Entity.Address.BusinessAddress.Suburb;
                    clientSummaryRow[HoldingRptDataSet.STATE] = Entity.Address.BusinessAddress.State;
                    clientSummaryRow[HoldingRptDataSet.POSTCODE] = Entity.Address.BusinessAddress.PostCode;
                    clientSummaryRow[HoldingRptDataSet.COUNTRY] = Entity.Address.BusinessAddress.Country;
                }

            }

            foreach (var contact in _entitycsv.Applicants)
            {
                contactRow[CapitalReportDS.CONTACT_TITLE] = contact.PersonalTitle;
                contactRow[CapitalReportDS.CONTACT_NAME] = contact.Fullname;
                contactRow[CapitalReportDS.CONTACT_BIZNO] = contact.WorkPhoneNumber;
                contactRow[CapitalReportDS.CONTACT_MOBNO] = contact.MobilePhoneNumber;
                contactRow[CapitalReportDS.CONTACT_HNO] = contact.HomePhoneNumber;
                contactRow[CapitalReportDS.CONTACT_FAXNO] = contact.Facsimile;
                contactRow[CapitalReportDS.CONTACT_EMAIL] = contact.EmailAddress;
            }

            clientSummaryTable.Rows.Add(clientSummaryRow);
            contactTable.Rows.Add(contactRow);

            IOrganization orgCM = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            CapitalReportDS capitalReportDS = data as CapitalReportDS;

            DataTable CapitalSummaryTable = capitalReportDS.Tables[CapitalReportDS.CAPITALFLOWSUMMARY];
            DataTable productBreakDownTable = capitalReportDS.Tables[CapitalReportDS.PRODUCTBREAKDOWNTABLE];
            DataTable TDBreakDownTable = capitalReportDS.Tables[CapitalReportDS.TDBREAKDOWNTABLE];

            DataTable CapitalFlowSummaryReport = ExtractCapitalSummaryTableByCategoryInd(Broker, Entity, capitalReportDS, clientID, orgCM);

            if (Entity.ClientManualAssetCollection != null)
                OnGetDataManualTransactions(clientID, Entity.ClientManualAssetCollection, orgCM, capitalReportDS);

            OnGetDateTransactionDetailsFromAccountProcess(clientID, Broker, Entity.ListAccountProcess, orgCM, capitalReportDS, TDBreakDownTable);
            Broker.ReleaseBrokerManagedComponent(orgCM);
        }

        private DataTable ExtractCapitalSummaryTableByCategoryInd(ICMBroker Broker, ClientIndividualEntity Entity, CapitalReportDS capitalReportDS, string ClientID, IOrganization orgCM)
        {
            DataTable CapitalFlowSummaryReport = capitalReportDS.Tables[CapitalReportDS.CAPITALFLOWSUMMARYCAT];
            DateTimeSpan dateTimeSpan = DateTimeSpan.CompareDates(capitalReportDS.StartDate, capitalReportDS.EndDate);

            for (int monthCounter = 0; monthCounter <= dateTimeSpan.Months; monthCounter++)
            {
                CapitalMovementByCategoryTotal capitalMovementByCategoryTotal = new CapitalMovementByCategoryTotal();

                DateTime startdate = FirstDayOfMonthFromDateTime(capitalReportDS.StartDate.AddMonths(monthCounter));
                DateTime endDate = LastDayOfMonthFromDateTime(capitalReportDS.StartDate.AddMonths(monthCounter));

                ManualTransactions(ClientID, Entity.ClientManualAssetCollection, orgCM, capitalReportDS, capitalMovementByCategoryTotal, startdate, endDate);
                GetTransactionsInfoFromLinkProcess(ClientID, Broker, Entity.ListAccountProcess, orgCM, capitalReportDS, capitalMovementByCategoryTotal, startdate, endDate);
                ManageInvestmentSchemes(ClientID, Broker, orgCM, capitalReportDS, capitalMovementByCategoryTotal, startdate, endDate);
                SetDesktopBrokerAccount(Broker, Entity.DesktopBrokerAccounts, capitalReportDS, orgCM, capitalMovementByCategoryTotal, startdate, endDate, ClientID, Entity.ListAccountProcess);

                DataRow row = CapitalFlowSummaryReport.NewRow();

                row[CapitalReportDS.MONTH] = startdate;
                row[CapitalReportDS.OPENINGBAL] = capitalMovementByCategoryTotal.OpeningBalTotal;
                row[CapitalReportDS.TRANSFEROUT] = capitalMovementByCategoryTotal.TransferInOutTotal;
                row[CapitalReportDS.INCOME] = capitalMovementByCategoryTotal.IncomeTotal;
                row[CapitalReportDS.APPLICATIONREDEMPTION] = capitalMovementByCategoryTotal.ApplicationRedTotal;
                row[CapitalReportDS.TAXINOUT] = capitalMovementByCategoryTotal.TaxInOutTotal;
                row[CapitalReportDS.EXPENSE] = capitalMovementByCategoryTotal.ExpenseTotal;
                row[CapitalReportDS.INTERNALCASHMOVEMENT] = capitalMovementByCategoryTotal.InternalCashMovementTotal;
                row[CapitalReportDS.CLOSINGBALANCE] = capitalMovementByCategoryTotal.ClosingBalanceTotal;
                row[CapitalReportDS.CHANGEININVESTMENTVALUE] = capitalMovementByCategoryTotal.ChangeInInvestTotal;
                CapitalFlowSummaryReport.Rows.Add(row);

            }

            return CapitalFlowSummaryReport;
        }

        private static void SetDesktopBrokerAccount(ICMBroker Broker, List<IdentityCMDetail> DesktopBrokerAccounts, CapitalReportDS capitalReportDS, IOrganization orgCM, CapitalMovementByCategoryTotal capitalMovementByCategoryTotal, DateTime startdate, DateTime endDate, string clientId, List<AccountProcessTaskEntity> accountProcessTaskEntities)
        {
            foreach (Oritax.TaxSimp.Common.IdentityCMDetail desktopBrokerAccounts in DesktopBrokerAccounts)
            {
                DesktopBrokerAccountCM desktopBrokerAccountCM = Broker.GetCMImplementation(desktopBrokerAccounts.Clid,
                                                                    desktopBrokerAccounts.Csid) as DesktopBrokerAccountCM;
                if (desktopBrokerAccountCM != null)
                {
                    var filteredTransactionsByMonth = desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Where(tran => tran.TradeDate <= endDate && tran.TradeDate >= startdate);

                    var task = accountProcessTaskEntities.FirstOrDefault(ss => ss.LinkedEntityType == OrganizationType.DesktopBrokerAccount && ss.LinkedEntity != null && (ss.LinkedEntity.Cid == desktopBrokerAccounts.Cid || (ss.LinkedEntity.Clid == desktopBrokerAccounts.Clid && ss.LinkedEntity.Csid == desktopBrokerAccounts.Csid)));
                    string serviceType = string.Empty;

                    if (task != null)
                    {
                        var modelasx = orgCM.Model.FirstOrDefault(ss => ss.ID == task.ModelID);
                        serviceType = modelasx == null ? string.Empty : modelasx.ServiceType.ToString();
                    }


                    SetDesktopBrokerTransactionDS(capitalReportDS, startdate.ToString("MMM-yy"), filteredTransactionsByMonth, desktopBrokerAccountCM.DesktopBrokerAccountEntity, clientId, serviceType);

                    decimal totalOpeiningBalance = 0;
                    decimal totalClosingBalance = 0;
                    CalculateDesktopBrokerWithoutProductSecurity(orgCM, startdate, endDate, desktopBrokerAccountCM, ref totalOpeiningBalance, ref totalClosingBalance);

                    capitalMovementByCategoryTotal.OpeningBalTotal += totalOpeiningBalance;
                    capitalMovementByCategoryTotal.ClosingBalanceTotal += totalClosingBalance;

                    foreach (HoldingTransactions holdingTransactions in filteredTransactionsByMonth)
                    {
                        if (holdingTransactions.Type == "RJ" || holdingTransactions.Type == "AJ")
                        {
                            decimal unitprice = 0;

                            var unitPriceObj = orgCM.Securities.Where(sec => sec.AsxCode == holdingTransactions.InvestmentCode).FirstOrDefault().ASXSecurity.Where(his => his.Date <= holdingTransactions.TradeDate).OrderByDescending(his => his.Date).FirstOrDefault();

                            if (unitPriceObj != null)
                                unitprice = Convert.ToDecimal(unitPriceObj.UnitPrice);

                            holdingTransactions.NetValue = holdingTransactions.Units * unitprice;

                            if (holdingTransactions.Type == "AJ")
                            {
                                if (holdingTransactions.NetValue > 0)
                                    holdingTransactions.NetValue = holdingTransactions.NetValue * -1;
                            }

                            capitalReportDS.AddTranBreakdownRow(holdingTransactions.TranstactionUniqueID, "Securities", desktopBrokerAccountCM.DesktopBrokerAccountEntity.AccountNumber, holdingTransactions.InvestmentCode, holdingTransactions.TransactionType, "Transfer In/Out", holdingTransactions.TradeDate, holdingTransactions.NetValue);

                        }
                    }

                    foreach (HoldingTransactions holdingTransactions in filteredTransactionsByMonth)
                    {
                        if (holdingTransactions.Type == "AT" || holdingTransactions.Type == "RT")
                        {
                            decimal unitprice = 0;

                            var unitPriceObj = orgCM.Securities.Where(sec => sec.AsxCode == holdingTransactions.InvestmentCode).FirstOrDefault().ASXSecurity.Where(his => his.Date <= holdingTransactions.TradeDate).OrderByDescending(his => his.Date).FirstOrDefault();

                            if (unitPriceObj != null)
                                unitprice = Convert.ToDecimal(unitPriceObj.UnitPrice);

                            holdingTransactions.NetValue = holdingTransactions.Units * unitprice;

                            if (holdingTransactions.Type == "RT")
                            {
                                if (holdingTransactions.NetValue > 0)
                                    holdingTransactions.NetValue = holdingTransactions.NetValue * -1;
                            }
                            capitalReportDS.AddTranBreakdownRow(holdingTransactions.TranstactionUniqueID, "Securities", desktopBrokerAccountCM.DesktopBrokerAccountEntity.AccountNumber, holdingTransactions.InvestmentCode, holdingTransactions.TransactionType, "Transfer In/Out", holdingTransactions.TradeDate, holdingTransactions.NetValue);
                        }
                    }

                    decimal transferInASX = filteredTransactionsByMonth.Where(tran => tran.Type == "AT").Sum(tran => tran.NetValue);
                    decimal transferOutASX = filteredTransactionsByMonth.Where(tran => tran.Type == "RT").Sum(tran => tran.NetValue);
                    decimal adjustmentUp = filteredTransactionsByMonth.Where(tran => tran.Type == "RJ").Sum(tran => tran.NetValue);
                    decimal adjustmentDown = filteredTransactionsByMonth.Where(tran => tran.Type == "AJ").Sum(tran => tran.NetValue);

                    var sellTrans = filteredTransactionsByMonth.Where(tran => tran.Type == "RA");
                    foreach (HoldingTransactions holdingTransactions in sellTrans)
                        capitalReportDS.AddTranBreakdownRow(holdingTransactions.TranstactionUniqueID, "Securities", desktopBrokerAccountCM.DesktopBrokerAccountEntity.AccountNumber, holdingTransactions.InvestmentCode, holdingTransactions.TransactionType, "Investment", holdingTransactions.TradeDate, holdingTransactions.NetValue * -1);
                    decimal sell = filteredTransactionsByMonth.Where(tran => tran.Type == "RA").Sum(tran => tran.NetValue);

                    var buyPurchaseTrans = filteredTransactionsByMonth.Where(tran => tran.Type == "AN");
                    foreach (HoldingTransactions holdingTransactions in buyPurchaseTrans)
                        capitalReportDS.AddTranBreakdownRow(holdingTransactions.TranstactionUniqueID, "Securities", desktopBrokerAccountCM.DesktopBrokerAccountEntity.AccountNumber, holdingTransactions.InvestmentCode, holdingTransactions.TransactionType, "Investment", holdingTransactions.TradeDate, holdingTransactions.NetValue);
                    decimal buyPurchase = filteredTransactionsByMonth.Where(tran => tran.Type == "AN").Sum(tran => tran.NetValue);

                    var reversalBuyPurchaseTrans = filteredTransactionsByMonth.Where(tran => tran.Type == "VB");
                    foreach (HoldingTransactions holdingTransactions in reversalBuyPurchaseTrans)
                        capitalReportDS.AddTranBreakdownRow(holdingTransactions.TranstactionUniqueID, "Securities", desktopBrokerAccountCM.DesktopBrokerAccountEntity.AccountNumber, holdingTransactions.InvestmentCode, holdingTransactions.TransactionType, "Investment", holdingTransactions.TradeDate, holdingTransactions.NetValue);
                    decimal reversalBuyPurchase = filteredTransactionsByMonth.Where(tran => tran.Type == "VB").Sum(tran => tran.NetValue);

                    var reversalRedemptionPurchaseTrans = filteredTransactionsByMonth.Where(tran => tran.Type == "VA");
                    foreach (HoldingTransactions holdingTransactions in reversalRedemptionPurchaseTrans)
                        capitalReportDS.AddTranBreakdownRow(holdingTransactions.TranstactionUniqueID, "Securities", desktopBrokerAccountCM.DesktopBrokerAccountEntity.AccountNumber, holdingTransactions.InvestmentCode, holdingTransactions.TransactionType, "Investment", holdingTransactions.TradeDate, (holdingTransactions.NetValue * -1));
                    decimal reversalRedemptionPurchase = filteredTransactionsByMonth.Where(tran => tran.Type == "VA").Sum(tran => tran.NetValue);

                    capitalMovementByCategoryTotal.TransferInOutTotal += transferInASX;
                    capitalMovementByCategoryTotal.TransferInOutTotal += transferOutASX;
                    capitalMovementByCategoryTotal.TransferInOutTotal += adjustmentUp;
                    capitalMovementByCategoryTotal.TransferInOutTotal += adjustmentDown;
                    capitalMovementByCategoryTotal.Application += buyPurchase;
                    capitalMovementByCategoryTotal.Redemption += (sell * -1);
                    capitalMovementByCategoryTotal.Application += reversalBuyPurchase;
                    capitalMovementByCategoryTotal.Redemption += (reversalRedemptionPurchase * -1);
                }
            }
        }

        public void OnGetDataCapitalFlowDataSetCorporate(CapitalReportDS data, string clientType, string clientID, string clientName, IdentityCM Parent, ICMBroker Broker, CorporateEntity Entity, object clientCID)
        {

            DataTable clientSummaryTable = data.Tables[CapitalReportDS.CLIENTSUMMARYTABLE];
            DataTable contactTable = data.Tables[CapitalReportDS.CONTACTTABLE];

            DataRow clientSummaryRow = clientSummaryTable.NewRow();
            DataRow contactRow = contactTable.NewRow();
            clientSummaryRow[CapitalReportDS.CLIENTCID] = clientCID;
            clientSummaryRow[CapitalReportDS.CLIENTID] = clientID;
            clientSummaryRow[CapitalReportDS.CLIENTNAME] = clientName;
            clientSummaryRow[CapitalReportDS.CIENTABN] = Entity.ABN;
            clientSummaryRow[CapitalReportDS.CLIENTACN] = Entity.ACN;
            clientSummaryRow[CapitalReportDS.CLIENTTYPE] = clientType;

            if (Parent != null && Parent.Clid != Guid.Empty && Parent.Csid != Guid.Empty)
            {
                AdvisorCM adviserUnit = Broker.GetCMImplementation(Parent.Clid, Parent.Csid) as AdvisorCM;

                if (adviserUnit != null)
                {
                    clientSummaryRow[HoldingRptDataSet.ADVISERFULLNAME] = adviserUnit.AdvisorEntity.FullName;
                    clientSummaryRow[HoldingRptDataSet.ADVISERCID] = adviserUnit.CID;
                    clientSummaryRow[HoldingRptDataSet.ADVISERID] = adviserUnit.EclipseClientID;

                    Broker.ReleaseBrokerManagedComponent(adviserUnit);
                }
            }

            if (data.PrintAddress)
            {
                if (Entity.Address.RegisteredAddress.Addressline1 != string.Empty)
                {
                    clientSummaryRow[HoldingRptDataSet.ADDRESSLINE1] = Entity.Address.MailingAddress.Addressline1;
                    clientSummaryRow[HoldingRptDataSet.ADDRESSLINE2] = Entity.Address.MailingAddress.Addressline2;
                    clientSummaryRow[HoldingRptDataSet.SUBURB] = Entity.Address.MailingAddress.Suburb;
                    clientSummaryRow[HoldingRptDataSet.STATE] = Entity.Address.MailingAddress.State;
                    clientSummaryRow[HoldingRptDataSet.POSTCODE] = Entity.Address.MailingAddress.PostCode;
                    clientSummaryRow[HoldingRptDataSet.COUNTRY] = Entity.Address.MailingAddress.Country;
                }
                else if (Entity.Address.RegisteredAddress.Addressline1 != string.Empty)
                {
                    clientSummaryRow[HoldingRptDataSet.ADDRESSLINE1] = Entity.Address.ResidentialAddress.Addressline1;
                    clientSummaryRow[HoldingRptDataSet.ADDRESSLINE2] = Entity.Address.ResidentialAddress.Addressline2;
                    clientSummaryRow[HoldingRptDataSet.SUBURB] = Entity.Address.ResidentialAddress.Suburb;
                    clientSummaryRow[HoldingRptDataSet.STATE] = Entity.Address.ResidentialAddress.State;
                    clientSummaryRow[HoldingRptDataSet.POSTCODE] = Entity.Address.ResidentialAddress.PostCode;
                    clientSummaryRow[HoldingRptDataSet.COUNTRY] = Entity.Address.ResidentialAddress.Country;
                }
                else if (Entity.Address.RegisteredAddress.Addressline1 != string.Empty)
                {
                    clientSummaryRow[HoldingRptDataSet.ADDRESSLINE1] = Entity.Address.RegisteredAddress.Addressline1;
                    clientSummaryRow[HoldingRptDataSet.ADDRESSLINE2] = Entity.Address.RegisteredAddress.Addressline2;
                    clientSummaryRow[HoldingRptDataSet.SUBURB] = Entity.Address.RegisteredAddress.Suburb;
                    clientSummaryRow[HoldingRptDataSet.STATE] = Entity.Address.RegisteredAddress.State;
                    clientSummaryRow[HoldingRptDataSet.POSTCODE] = Entity.Address.RegisteredAddress.PostCode;
                    clientSummaryRow[HoldingRptDataSet.COUNTRY] = Entity.Address.RegisteredAddress.Country;
                }
                else if (Entity.Address.BusinessAddress.Addressline1 != string.Empty)
                {
                    clientSummaryRow[HoldingRptDataSet.ADDRESSLINE1] = Entity.Address.BusinessAddress.Addressline1;
                    clientSummaryRow[HoldingRptDataSet.ADDRESSLINE2] = Entity.Address.BusinessAddress.Addressline2;
                    clientSummaryRow[HoldingRptDataSet.SUBURB] = Entity.Address.BusinessAddress.Suburb;
                    clientSummaryRow[HoldingRptDataSet.STATE] = Entity.Address.BusinessAddress.State;
                    clientSummaryRow[HoldingRptDataSet.POSTCODE] = Entity.Address.BusinessAddress.PostCode;
                    clientSummaryRow[HoldingRptDataSet.COUNTRY] = Entity.Address.BusinessAddress.Country;
                }
            }

            CSVInstance csvinstance = new CSVInstance(Broker);
            CorporateCSV _entitycsv = csvinstance.GetEntityConcreteInstance(Entity);
            foreach (var contact in _entitycsv.Contacts)
            {
                contactRow[CapitalReportDS.CONTACT_TITLE] = contact.PersonalTitle;
                contactRow[CapitalReportDS.CONTACT_NAME] = contact.Fullname;
                contactRow[CapitalReportDS.CONTACT_BIZNO] = contact.WorkPhoneNumber;
                contactRow[CapitalReportDS.CONTACT_MOBNO] = contact.MobilePhoneNumber;
                contactRow[CapitalReportDS.CONTACT_HNO] = contact.HomePhoneNumber;
                contactRow[CapitalReportDS.CONTACT_FAXNO] = contact.Facsimile;
                contactRow[CapitalReportDS.CONTACT_EMAIL] = contact.EmailAddress;
            }

            clientSummaryTable.Rows.Add(clientSummaryRow);
            contactTable.Rows.Add(contactRow);

            IOrganization orgCM = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            CapitalReportDS capitalReportDS = data as CapitalReportDS;

            DataTable CapitalSummaryTable = capitalReportDS.Tables[CapitalReportDS.CAPITALFLOWSUMMARY];
            DataTable productBreakDownTable = capitalReportDS.Tables[CapitalReportDS.PRODUCTBREAKDOWNTABLE];
            DataTable TDBreakDownTable = capitalReportDS.Tables[CapitalReportDS.TDBREAKDOWNTABLE];

            DataTable CapitalFlowSummaryReport = ExtractCapitalSummaryTableByCategory(Broker, Entity, capitalReportDS, clientID, orgCM);

            if (Entity.ClientManualAssetCollection != null)
                OnGetDataManualTransactions(clientID, Entity.ClientManualAssetCollection, orgCM, capitalReportDS);

            OnGetDateTransactionDetailsFromAccountProcess(clientID, Broker, Entity.ListAccountProcess, orgCM, capitalReportDS, TDBreakDownTable);
            Broker.ReleaseBrokerManagedComponent(orgCM);
        }

        private static void OnGetDataManualTransactions(string clientID, ObservableCollection<ClientManualAssetEntity> ClientManualAssetCollection, IOrganization orgCM, CapitalReportDS capitalReportDS)
        {
            var groupedManualTransactions = ClientManualAssetCollection.GroupBy(man => man.InvestmentCode);

            foreach (var groupedManualTransaction in groupedManualTransactions)
            {
                ManualAssetEntity systemManualAsset = orgCM.ManualAsset.Where(manList => manList.ID == groupedManualTransaction.Key).FirstOrDefault();

                decimal openingBalance = 0;
                decimal closingBalance = 0;
                decimal transferInOut = 0;
                decimal appRedemption = 0;
                if (systemManualAsset.ASXSecurity.Count > 0)
                {

                    double unitPriceLatest = systemManualAsset.ASXSecurity.OrderByDescending(manualHis => manualHis.Date).FirstOrDefault().UnitPrice;
                    double unitPriceStartDate = systemManualAsset.ASXSecurity.OrderByDescending(manualHis => manualHis.Date).FirstOrDefault().UnitPrice;

                    decimal unitholdingStartDate = groupedManualTransaction.Where(manTran => manTran.TransactionDate <= capitalReportDS.StartDate).Sum(manTran => manTran.UnitsOnHand);
                    decimal unitholdingEndDate = groupedManualTransaction.Where(manTran => manTran.TransactionDate <= capitalReportDS.EndDate).Sum(manTran => manTran.UnitsOnHand);

                    closingBalance = Convert.ToDecimal(unitPriceLatest) * unitholdingEndDate;
                    openingBalance = Convert.ToDecimal(unitPriceStartDate) * unitholdingStartDate;

                    //Calculate Holding based on unit price for each transactiondate
                    foreach (var clientManualAssetEntity in groupedManualTransaction)
                    {
                        decimal netValue = 0;
                        systemManualAsset = orgCM.ManualAsset.Where(manList => manList.ID == groupedManualTransaction.Key).FirstOrDefault();

                        var systemManualAssetPriceHistory = systemManualAsset.ASXSecurity.Where(manualHIS => manualHIS.Date <= clientManualAssetEntity.TransactionDate).OrderByDescending(manualHis => manualHis.Date).FirstOrDefault();

                        if (systemManualAssetPriceHistory != null)
                        {
                            if (clientManualAssetEntity.TransactionType == "Transfer In")
                            {
                                if (systemManualAssetPriceHistory.PurValue != 0)
                                    netValue = clientManualAssetEntity.UnitsOnHand * Convert.ToDecimal(systemManualAssetPriceHistory.PurValue);
                                else
                                    netValue = clientManualAssetEntity.UnitsOnHand * Convert.ToDecimal(systemManualAssetPriceHistory.UnitPrice);

                                transferInOut += netValue;
                            }

                            if (clientManualAssetEntity.TransactionType == "Transfer Out")
                            {
                                netValue = -1 * (clientManualAssetEntity.UnitsOnHand * Convert.ToDecimal(systemManualAssetPriceHistory.UnitPrice));
                                transferInOut += netValue;
                            }

                            if (clientManualAssetEntity.TransactionType == "Application")
                            {
                                if (systemManualAssetPriceHistory.PurValue != 0)
                                    netValue = clientManualAssetEntity.UnitsOnHand * Convert.ToDecimal(systemManualAssetPriceHistory.PurValue);
                                else
                                    netValue = clientManualAssetEntity.UnitsOnHand * Convert.ToDecimal(systemManualAssetPriceHistory.UnitPrice);

                                appRedemption += netValue;
                            }

                            if (clientManualAssetEntity.TransactionType == "Redemption")
                            {
                                netValue = -1 * (clientManualAssetEntity.UnitsOnHand * Convert.ToDecimal(systemManualAssetPriceHistory.UnitPrice));
                                appRedemption += netValue;
                            }
                        }
                    }

                    capitalReportDS.AddCapitalRow("ManualAsset", "ManualAsset", systemManualAsset.AsxCode, systemManualAsset.CompanyName, clientID, openingBalance, closingBalance, transferInOut, 0, appRedemption, 0, 0, 0, 0, string.Empty, string.Empty);
                }
            }
        }

        private static void OnGetDateTransactionDetailsFromAccountProcess(string clientID, ICMBroker Broker, List<AccountProcessTaskEntity> ListAccountProcess, IOrganization orgCM, CapitalReportDS capitalReportDS, DataTable TDBreakDownTable)
        {
            GetUnlistedSecurity(clientID, Broker, ListAccountProcess, orgCM, capitalReportDS);
            foreach (var accountProcessTaskEntity in ListAccountProcess)
            {
                if (accountProcessTaskEntity.LinkedEntity != null &&
                    accountProcessTaskEntity.LinkedEntity.Clid != Guid.Empty)
                {
                    var model = orgCM.Model.Where(mod => mod.ID == accountProcessTaskEntity.ModelID).FirstOrDefault();
                    if (model != null)
                    {
                        var asset = model.Assets.Where(ast => ast.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                        if (asset != null)
                        {
                            var product = asset.Products.Where(pro => pro.ID == accountProcessTaskEntity.TaskID).FirstOrDefault();
                            if (product != null)
                            {
                                if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.BankAccount)
                                    GetCapitalMovementBankTransactions(Broker, capitalReportDS, accountProcessTaskEntity, asset, model, product, clientID);
                                else if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.TermDepositAccount)
                                    GetCapitalMovementTDTransactions(Broker, orgCM, capitalReportDS, TDBreakDownTable, accountProcessTaskEntity, asset, model, product, clientID);
                                else if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.DesktopBrokerAccount)
                                    GetCapitalMovementDesktopBroker(Broker, orgCM, capitalReportDS, accountProcessTaskEntity, asset, model, product);
                                else if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.ManagedInvestmentSchemesAccount)
                                    GetCapitalMovementMIS(clientID, Broker, orgCM, capitalReportDS, accountProcessTaskEntity, asset, model, product);
                            }
                        }
                    }

                }
            }
        }

        private static void GetUnlistedSecurity(string clientID, ICMBroker Broker, List<AccountProcessTaskEntity> ListAccountProcess, IOrganization orgCM, CapitalReportDS capitalReportDS)
        {
            var groupedAccountProcessByModel = ListAccountProcess.Where(accProcess => accProcess.LinkedEntityType == OrganizationType.DesktopBrokerAccount).GroupBy(accProcess => accProcess.ModelID);

            foreach (var groupedModel in groupedAccountProcessByModel)
            {
                bool processUnlistedAccounts = false;
                var model = orgCM.Model.Where(mod => mod.ID == groupedModel.Key).FirstOrDefault();
                ObservableCollection<ProductSecurityDetail> ProductDetails = new ObservableCollection<ProductSecurityDetail>();

                foreach (var accountProcessEntity in groupedModel)
                {
                    if (accountProcessEntity.LinkedEntity.Clid != Guid.Empty)
                    {
                        var asset = orgCM.Assets.Where(var => var.ID == accountProcessEntity.AssetID).FirstOrDefault();
                        var linkedAsset = model.Assets.Where(ast => ast.ID == accountProcessEntity.AssetID).FirstOrDefault();
                        if (linkedAsset != null)
                        {
                            var product = linkedAsset.Products.Where(pro => pro.ID == accountProcessEntity.TaskID).FirstOrDefault();
                            if (product != null)
                            {
                                if (product.ProductSecuritiesId != Guid.Empty)
                                {
                                    processUnlistedAccounts = true;
                                    var productSecurity = orgCM.ProductSecurities.Where(proseclist => proseclist.ID == product.ProductSecuritiesId).FirstOrDefault();

                                    foreach (var productSecurityDetail in productSecurity.Details)
                                    {
                                        ProductDetails.Add(productSecurityDetail);
                                    }
                                }
                            }
                        }
                    }
                }

                if (processUnlistedAccounts)
                {

                    Guid productID = Guid.NewGuid();
                    DesktopBrokerAccountCM desktopBrokerAccountCM = Broker.GetCMImplementation(groupedModel.FirstOrDefault().LinkedEntity.Clid,
                                                                                groupedModel.FirstOrDefault().LinkedEntity.Csid) as DesktopBrokerAccountCM;

                    var filteredHoldingTransactionsByStartDate = desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Where(holdTrans =>
                                                                                                  holdTrans.TradeDate.Date <= capitalReportDS.StartDate.Date.AddDays(-1));

                    var filteredHoldingTransactionsByEndDate = desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Where(holdTrans =>
                                                                          holdTrans.TradeDate.Date <= capitalReportDS.EndDate.Date);


                    var groupedUnlistedSecurities = filteredHoldingTransactionsByEndDate.GroupBy(holdTran => holdTran.InvestmentCode);

                    DateTime priceAtDate = DateTime.Now;

                    decimal presentHoldingTotal = 0;
                    decimal startHoldingTotal = 0;
                    decimal endHoldingValueProductSec = 0;
                    decimal adjustmentUpTotal = 0;
                    decimal adjustmentDownTotal = 0;
                    decimal sellTotal = 0;
                    decimal buyPurchaseTotal = 0;
                    decimal transferOutASXTotal = 0;
                    decimal transferInASXTotal = 0;

                    foreach (var security in groupedUnlistedSecurities)
                    {

                        var asxBroker = orgCM.Securities.Where(sec => sec.AsxCode == security.Key).FirstOrDefault();
                        var prosecdetail = ProductDetails.Where(prosec => prosec.SecurityCodeId == asxBroker.ID).FirstOrDefault();

                        if (prosecdetail == null)
                        {

                            var filteredHoldingTransactions = security.Where(holdTrans =>
                                                                             holdTrans.TradeDate.Date.Date <= capitalReportDS.EndDate.Date.Date && holdTrans.TradeDate.Date.Date >= capitalReportDS.StartDate.Date.Date);


                            decimal totalSecHoldingUnits = security.Sum(sec => sec.Units);


                            var hisSecurityStart = asxBroker.ASXSecurity.Where(his => his.Date.Date <= capitalReportDS.StartDate.Date.AddDays(-1)).OrderByDescending(secpri => secpri.Date).FirstOrDefault();
                            decimal startHoldingUnits = filteredHoldingTransactionsByStartDate.Where(filteredHolding => filteredHolding.InvestmentCode == security.Key).Sum(filteredHolding => filteredHolding.Units);

                            decimal startUnitPrice = 0;
                            decimal endUnitPrice = 0;

                            if (hisSecurityStart != null)
                                startUnitPrice = Convert.ToDecimal(hisSecurityStart.UnitPrice);

                            decimal startHoldingValue = startHoldingUnits * startUnitPrice;

                            var hisSecurityLatest = asxBroker.ASXSecurity.Where(his => his.Date.Date <= capitalReportDS.EndDate.Date).OrderByDescending(secpri => secpri.Date).FirstOrDefault();

                            if (hisSecurityLatest != null)
                                endUnitPrice = Convert.ToDecimal(hisSecurityLatest.UnitPrice);

                            decimal endHoldingUnits = filteredHoldingTransactionsByEndDate.Where(tran => tran.InvestmentCode == security.Key).Sum(filteredHolding => filteredHolding.Units);
                            decimal latestHoldingValue = endHoldingUnits * endUnitPrice;

                            var transferInASXList = filteredHoldingTransactions.Where(tran => tran.Type == "AT");
                            decimal transferInASX = 0;

                            foreach (var transferInTran in transferInASXList)
                            {
                                decimal netValue = 0;
                                var secEntityTransferIn = asxBroker.ASXSecurity.Where(his => his.Date.Date <= transferInTran.TradeDate.Date.Date).OrderByDescending(secpri => secpri.Date).FirstOrDefault();
                                if (secEntityTransferIn != null)
                                    netValue = transferInTran.Units * Convert.ToDecimal(secEntityTransferIn.UnitPrice);

                                transferInASX += netValue;
                            }

                            var transferOutASXList = filteredHoldingTransactions.Where(tran => tran.Type == "RT");
                            decimal transferOutASX = 0;

                            foreach (var transferOutTran in transferOutASXList)
                            {
                                decimal netValue = 0;
                                var secEntityTransferIn = asxBroker.ASXSecurity.Where(his => his.Date.Date <= transferOutTran.TradeDate.Date.Date).OrderByDescending(secpri => secpri.Date).FirstOrDefault();
                                if (secEntityTransferIn != null)
                                    netValue = transferOutTran.Units * Convert.ToDecimal(secEntityTransferIn.UnitPrice);

                                transferOutASX += netValue;
                            }

                            decimal adjustmentUp = filteredHoldingTransactions.Where(tran => tran.Type == "RJ").Sum(tran => tran.NetValue);
                            decimal adjustmentDown = filteredHoldingTransactions.Where(tran => tran.Type == "AJ").Sum(tran => tran.NetValue);
                            decimal sell = filteredHoldingTransactions.Where(tran => tran.Type == "RA" || tran.Type == "VA").Sum(tran => tran.NetValue);
                            decimal buyPurchase = filteredHoldingTransactions.Where(tran => tran.Type == "AN" || tran.Type == "VB").Sum(tran => tran.NetValue);

                            decimal endHoldingValue = filteredHoldingTransactionsByEndDate.Where(tran => tran.InvestmentCode == security.Key).Where(tran => tran.Type != "AT" || tran.Type != "RT" || tran.Type != "RA" || tran.Type != "VA").Sum(filteredHolding => filteredHolding.NetValue);
                            var endHoldingValueTransferInTrans = filteredHoldingTransactionsByEndDate.Where(tran => tran.InvestmentCode == security.Key).Where(tran => tran.Type == "AT");

                            foreach (var transferInTran in endHoldingValueTransferInTrans)
                            {
                                decimal netValue = 0;
                                var secEntityTransferIn = asxBroker.ASXSecurity.Where(his => his.Date.Date <= transferInTran.TradeDate.Date.Date).OrderByDescending(secpri => secpri.Date).FirstOrDefault();
                                if (secEntityTransferIn != null)
                                    netValue = transferInTran.Units * Convert.ToDecimal(secEntityTransferIn.UnitPrice);

                                endHoldingValue += netValue;
                            }

                            presentHoldingTotal += latestHoldingValue;
                            endHoldingValueProductSec += endHoldingValue;
                            startHoldingTotal += startHoldingValue;

                            adjustmentUpTotal += adjustmentUp;
                            adjustmentDownTotal += adjustmentDown;
                            sellTotal += (sell * -1);
                            buyPurchaseTotal += buyPurchase;
                            transferOutASXTotal += transferOutASX;
                            transferInASXTotal += transferInASX;
                        }
                    }
                    capitalReportDS.AddCapitalRow("AEQ", model.ServiceType.ToString(), "ASX Desktop Broker", "ASX Desktop Broker", desktopBrokerAccountCM.DesktopBrokerAccountEntity.AccountNumber
                                                                                    , startHoldingTotal, presentHoldingTotal, transferOutASXTotal + transferInASXTotal, 0, sellTotal + buyPurchaseTotal, 0, 0, 0, 0, string.Empty, string.Empty);
                }
            }
        }

        private static void GetCapitalMovementMIS(string clientID, ICMBroker Broker, IOrganization orgCM, CapitalReportDS capitalReportDS, AccountProcessTaskEntity accountProcessTaskEntity, AssetEntity asset, ModelEntity model, ProductEntity product)
        {
            ManagedInvestmentSchemesAccountCM managedInvestmentSchemesAccountCM = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as ManagedInvestmentSchemesAccountCM;
            var linkedMISAccount = managedInvestmentSchemesAccountCM.ManagedInvestmentSchemesAccountEntity.FundAccounts.Where(fa => product.TaskDescription.Contains(fa.Code)).FirstOrDefault();

            var filteredHoldingTransactionsByStartDate = linkedMISAccount.FundTransactions.Where(cliFundTrans => cliFundTrans.TradeDate <= capitalReportDS.StartDate);
            var filteredHoldingTransactionsByEndDate = linkedMISAccount.FundTransactions.Where(cliFundTrans => cliFundTrans.TradeDate <= capitalReportDS.EndDate);

            var clientFundTransactionsStart = filteredHoldingTransactionsByStartDate.Where(cliFundTrans => cliFundTrans.ClientID == clientID);
            decimal totalSharesStart = clientFundTransactionsStart.Sum(ft => ft.Shares);

            var clientFundTransactionsEnd = filteredHoldingTransactionsByEndDate.Where(cliFundTrans => cliFundTrans.ClientID == clientID);
            decimal totalSharesEnd = clientFundTransactionsEnd.Sum(ft => ft.Shares);

            var linkedmissec = orgCM.Securities.Where(missec => missec.AsxCode == linkedMISAccount.Code).FirstOrDefault();

            decimal currentValue = 0;
            decimal startValue = 0;
            decimal endValue = 0;
            decimal transferInOut = 0;
            decimal applicationRedemption = 0;

            if (linkedmissec != null)
            {
                decimal startUnitPrice = 0;
                decimal endUnitPrice = 0;

                var latestMisPriceObjectStart = linkedmissec.ASXSecurity.Where(mishis => mishis.Date <= capitalReportDS.StartDate.Date.AddDays(-1)).OrderByDescending(mishis => mishis.Date).FirstOrDefault();
                var latestMisPriceObjectEnd = linkedmissec.ASXSecurity.Where(mishis => mishis.Date <= capitalReportDS.EndDate).OrderByDescending(mishis => mishis.Date).FirstOrDefault();

                if (latestMisPriceObjectStart != null)
                    startUnitPrice = Convert.ToDecimal(latestMisPriceObjectStart.UnitPrice);

                if (latestMisPriceObjectEnd != null)
                    endUnitPrice = Convert.ToDecimal(latestMisPriceObjectEnd.UnitPrice);

                startValue = totalSharesStart * startUnitPrice;
                currentValue = totalSharesEnd * endUnitPrice;

                var filteredHoldingTransactions = linkedMISAccount.FundTransactions.Where(cliFundTrans => cliFundTrans.TradeDate >= capitalReportDS.StartDate.Date && cliFundTrans.TradeDate <= capitalReportDS.EndDate.Date && cliFundTrans.ClientID == clientID);

                decimal buyPurchase = filteredHoldingTransactions.Where(tran => tran.TransactionType == "Purchase").Sum(tran => tran.Amount);
                buyPurchase += filteredHoldingTransactions.Where(tran => tran.TransactionType == "Application").Sum(tran => tran.Amount);

                decimal sell = filteredHoldingTransactions.Where(tran => tran.TransactionType == "Redemption").Sum(tran => tran.Amount);

                applicationRedemption += buyPurchase;
                applicationRedemption += sell;

                capitalReportDS.AddProductDetailRow(product, endUnitPrice, linkedmissec.AsxCode, totalSharesEnd, totalSharesEnd, totalSharesStart,
                                                                                    currentValue, startValue, endValue, model.ServiceType.ToString());
            }

            capitalReportDS.AddCapitalRow(asset.Name, model.ServiceType.ToString(), product.Name, product.TaskDescription, clientID, startValue, currentValue, transferInOut, 0, applicationRedemption, 0, 0, 0, 0, string.Empty, string.Empty);
        }

        private static void GetCapitalMovementDesktopBroker(ICMBroker Broker, IOrganization orgCM, CapitalReportDS capitalReportDS, AccountProcessTaskEntity accountProcessTaskEntity, AssetEntity asset, ModelEntity model, ProductEntity product)
        {
            DesktopBrokerAccountCM desktopBrokerAccountCM = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid,
                                                                        accountProcessTaskEntity.LinkedEntity.Csid) as DesktopBrokerAccountCM;
            decimal presentHoldingTotal = 0;
            decimal startHoldingTotal = 0;
            decimal endHoldingValueProductSec = 0;
            decimal adjustmentUpTotal = 0;
            decimal adjustmentDownTotal = 0;
            decimal sellTotal = 0;
            decimal buyPurchaseTotal = 0;
            decimal transferOutASXTotal = 0;
            decimal transferInASXTotal = 0;

            //If it is linked with product securities for split type calculation
            if (product.ProductSecuritiesId != Guid.Empty)
            {
                var productSecurity = orgCM.ProductSecurities.Where(proseclist => proseclist.ID == product.ProductSecuritiesId).FirstOrDefault();
                var linkedAssets = model.Assets.Where(ast => ast.ID == accountProcessTaskEntity.AssetID && accountProcessTaskEntity.ModelID == model.ID);
                double totalPercentage = HoldingRptDataSet.CalculateTotalPercentageForSplit(linkedAssets);

                foreach (var productSecurityDetail in productSecurity.Details)
                {
                    var asxBroker = orgCM.Securities.Where(sec => sec.ID == productSecurityDetail.SecurityCodeId).FirstOrDefault();
                    var filteredHoldingTransactionsByStartDate = desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Where(holdTrans =>
                                                                                                  holdTrans.TradeDate.Date <= capitalReportDS.StartDate.Date.AddDays(-1));
                    var filteredHoldingTransactionsStartDate = filteredHoldingTransactionsByStartDate.Where(holdTrans =>
                                                                                                  holdTrans.InvestmentCode == asxBroker.AsxCode);

                    var filteredHoldingTransactionsByEndDate = desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Where(holdTrans =>
                                                                                                  holdTrans.TradeDate.Date <= capitalReportDS.EndDate.Date);
                    var filteredHoldingTransactionsEndDate = filteredHoldingTransactionsByEndDate.Where(holdTrans =>
                                                                                                  holdTrans.InvestmentCode == asxBroker.AsxCode);

                    var count = filteredHoldingTransactionsByEndDate.Count();

                    if (count > 0)
                    {
                        totalPercentage = product.SharePercentage;

                        foreach (var linkedasst in linkedAssets)
                        {
                            var otherProducts = linkedasst.Products.Where(linkedAsstPro => linkedAsstPro.ProductSecuritiesId != productSecurity.ID);
                            foreach (var OtherproductSecurity in otherProducts)
                            {
                                var OtherproductSecurityDetails = orgCM.ProductSecurities.Where(otherpro => otherpro.ID == OtherproductSecurity.ProductSecuritiesId).FirstOrDefault();

                                if (OtherproductSecurityDetails != null)
                                {
                                    var otherAsxBroker = OtherproductSecurityDetails.Details.Where(otherProDetail => otherProDetail.SecurityCodeId == asxBroker.ID);

                                    if (otherAsxBroker.Count() > 0)
                                        totalPercentage += OtherproductSecurity.SharePercentage;
                                }
                            }
                        }

                        decimal unitPriceStart = 0;
                        decimal unitPriceEnd = 0;

                        var filteredHoldingTransactions = filteredHoldingTransactionsEndDate.Where(holdTrans =>
                                                                   holdTrans.TradeDate.Date.Date <= capitalReportDS.EndDate.Date.Date && holdTrans.TradeDate.Date.Date >= capitalReportDS.StartDate.Date.Date);

                        var hisSecurityStart = asxBroker.ASXSecurity.Where(his => his.Date.Date <= capitalReportDS.StartDate.Date.AddDays(-1)).OrderByDescending(secpri => secpri.Date).FirstOrDefault();
                        decimal startHoldingUnits = filteredHoldingTransactionsStartDate.Where(filteredHolding => filteredHolding.InvestmentCode == asxBroker.AsxCode).Sum(filteredHolding => filteredHolding.Units);

                        if (hisSecurityStart != null)
                            unitPriceStart = Convert.ToDecimal(hisSecurityStart.UnitPrice);

                        decimal startHoldingValue = startHoldingUnits * unitPriceStart;

                        var hisSecurityLatest = asxBroker.ASXSecurity.Where(his => his.Date.Date <= capitalReportDS.EndDate.Date).OrderByDescending(secpri => secpri.Date).FirstOrDefault();
                        decimal endHoldingUnits = filteredHoldingTransactionsEndDate.Where(filteredHolding => filteredHolding.InvestmentCode == asxBroker.AsxCode).Sum(filteredHolding => filteredHolding.Units);

                        if (hisSecurityLatest != null)
                            unitPriceEnd = Convert.ToDecimal(hisSecurityLatest.UnitPrice);

                        decimal latestHoldingValue = endHoldingUnits * unitPriceEnd;
                        var transferInASXList = filteredHoldingTransactions.Where(tran => tran.Type == "AT");
                        decimal transferInASX = 0;

                        foreach (var transferInTran in transferInASXList)
                        {
                            decimal netValue = 0;
                            var secEntityTransferIn = asxBroker.ASXSecurity.Where(his => his.Date.Date <= transferInTran.TradeDate.Date.Date).OrderByDescending(secpri => secpri.Date).FirstOrDefault();
                            if (secEntityTransferIn != null)
                                netValue = transferInTran.Units * Convert.ToDecimal(secEntityTransferIn.UnitPrice);

                            transferInASX += netValue;
                        }

                        var transferOutASXList = filteredHoldingTransactions.Where(tran => tran.Type == "RT");
                        decimal transferOutASX = 0;

                        foreach (var transferOutTran in transferOutASXList)
                        {
                            decimal netValue = 0;
                            var secEntityTransferIn = asxBroker.ASXSecurity.Where(his => his.Date.Date <= transferOutTran.TradeDate.Date.Date).OrderByDescending(secpri => secpri.Date).FirstOrDefault();
                            if (secEntityTransferIn != null)
                                netValue = transferOutTran.Units * Convert.ToDecimal(secEntityTransferIn.UnitPrice);

                            transferOutASX += netValue;
                        }

                        decimal adjustmentUp = filteredHoldingTransactions.Where(tran => tran.Type == "RJ").Sum(tran => tran.NetValue);
                        decimal adjustmentDown = filteredHoldingTransactions.Where(tran => tran.Type == "AJ").Sum(tran => tran.NetValue);
                        decimal sell = filteredHoldingTransactions.Where(tran => tran.Type == "RA" || tran.Type == "VA").Sum(tran => tran.NetValue);
                        decimal buyPurchase = filteredHoldingTransactions.Where(tran => tran.Type == "AN" || tran.Type == "VB").Sum(tran => tran.NetValue);

                        if (totalPercentage != 0)
                        {
                            startHoldingValue = startHoldingValue * Convert.ToDecimal((product.SharePercentage / totalPercentage));
                            latestHoldingValue = latestHoldingValue * Convert.ToDecimal((product.SharePercentage / totalPercentage));

                            adjustmentUp = adjustmentUp * Convert.ToDecimal((product.SharePercentage / totalPercentage));
                            adjustmentDown = adjustmentDown * Convert.ToDecimal((product.SharePercentage / totalPercentage));
                            sell = sell * Convert.ToDecimal((product.SharePercentage / totalPercentage));
                            buyPurchase = buyPurchase * Convert.ToDecimal((product.SharePercentage / totalPercentage));
                            transferOutASX = transferOutASX * Convert.ToDecimal((product.SharePercentage / totalPercentage));
                            transferInASX = transferInASX * Convert.ToDecimal((product.SharePercentage / totalPercentage));
                        }


                        capitalReportDS.AddProductDetailRow(product, unitPriceEnd, asxBroker.AsxCode, endHoldingUnits, endHoldingUnits,
                                                               startHoldingUnits, latestHoldingValue, startHoldingValue, latestHoldingValue, model.ServiceType.ToString());

                        presentHoldingTotal += latestHoldingValue;
                        startHoldingTotal += startHoldingValue;
                        adjustmentUpTotal += adjustmentUp;
                        adjustmentDownTotal += adjustmentDown;
                        sellTotal += (sell * -1);
                        buyPurchaseTotal += buyPurchase;
                        transferOutASXTotal += transferOutASX;
                        transferInASXTotal += transferInASX;
                    }
                }
            }
            else
            {
                var filteredHoldingTransactionsByStartDate = desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Where(holdTrans =>
                                                                                                  holdTrans.TradeDate.Date <= capitalReportDS.StartDate.Date.AddDays(-1));

                var filteredHoldingTransactionsByEndDate = desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Where(holdTrans =>
                                                                      holdTrans.TradeDate.Date <= capitalReportDS.EndDate.Date);

                var groupedSecurities = filteredHoldingTransactionsByEndDate.GroupBy(holdTran => holdTran.InvestmentCode);

                foreach (var security in groupedSecurities)
                {

                    var filteredHoldingTransactions = security.Where(holdTrans =>
                                                                     holdTrans.TradeDate.Date.Date <= capitalReportDS.EndDate.Date.Date && holdTrans.TradeDate.Date.Date >= capitalReportDS.StartDate.Date.Date);


                    decimal totalSecHoldingUnits = security.Sum(sec => sec.Units);
                    var asxBroker = orgCM.Securities.Where(sec => sec.AsxCode == security.Key).FirstOrDefault();


                    var hisSecurityStart = asxBroker.ASXSecurity.Where(his => his.Date.Date <= capitalReportDS.StartDate.Date.AddDays(-1)).OrderByDescending(secpri => secpri.Date).FirstOrDefault();
                    decimal startHoldingUnits = filteredHoldingTransactionsByStartDate.Where(filteredHolding => filteredHolding.InvestmentCode == security.Key).Sum(filteredHolding => filteredHolding.Units);

                    decimal startUnitPrice = 0;
                    decimal endUnitPrice = 0;

                    if (hisSecurityStart != null)
                        startUnitPrice = Convert.ToDecimal(hisSecurityStart.UnitPrice);

                    decimal startHoldingValue = startHoldingUnits * startUnitPrice;

                    var hisSecurityLatest = asxBroker.ASXSecurity.Where(his => his.Date.Date <= capitalReportDS.EndDate.Date).OrderByDescending(secpri => secpri.Date).FirstOrDefault();

                    if (hisSecurityLatest != null)
                        endUnitPrice = Convert.ToDecimal(hisSecurityLatest.UnitPrice);

                    decimal endHoldingUnits = filteredHoldingTransactionsByEndDate.Where(tran => tran.InvestmentCode == security.Key).Sum(filteredHolding => filteredHolding.Units);
                    decimal latestHoldingValue = endHoldingUnits * endUnitPrice;

                    var transferInASXList = filteredHoldingTransactions.Where(tran => tran.Type == "AT");
                    decimal transferInASX = 0;

                    foreach (var transferInTran in transferInASXList)
                    {
                        decimal netValue = 0;
                        var secEntityTransferIn = asxBroker.ASXSecurity.Where(his => his.Date.Date <= transferInTran.TradeDate.Date.Date).OrderByDescending(secpri => secpri.Date).FirstOrDefault();
                        if (secEntityTransferIn != null)
                            netValue = transferInTran.Units * Convert.ToDecimal(secEntityTransferIn.UnitPrice);

                        transferInASX += netValue;
                    }

                    var transferOutASXList = filteredHoldingTransactions.Where(tran => tran.Type == "RT");
                    decimal transferOutASX = 0;

                    foreach (var transferOutTran in transferOutASXList)
                    {
                        decimal netValue = 0;
                        var secEntityTransferIn = asxBroker.ASXSecurity.Where(his => his.Date.Date <= transferOutTran.TradeDate.Date.Date).OrderByDescending(secpri => secpri.Date).FirstOrDefault();
                        if (secEntityTransferIn != null)
                            netValue = transferOutTran.Units * Convert.ToDecimal(secEntityTransferIn.UnitPrice);

                        transferOutASX += netValue;
                    }

                    decimal adjustmentUp = filteredHoldingTransactions.Where(tran => tran.Type == "RJ").Sum(tran => tran.NetValue);
                    decimal adjustmentDown = filteredHoldingTransactions.Where(tran => tran.Type == "AJ").Sum(tran => tran.NetValue);
                    decimal sell = filteredHoldingTransactions.Where(tran => tran.Type == "RA" || tran.Type == "VA").Sum(tran => tran.NetValue);
                    decimal buyPurchase = filteredHoldingTransactions.Where(tran => tran.Type == "AN" || tran.Type == "VB").Sum(tran => tran.NetValue);

                    decimal endHoldingValue = filteredHoldingTransactionsByEndDate.Where(tran => tran.InvestmentCode == security.Key).Where(tran => tran.Type != "AT" || tran.Type != "RT" || tran.Type != "RA" || tran.Type != "VA").Sum(filteredHolding => filteredHolding.NetValue);
                    var endHoldingValueTransferInTrans = filteredHoldingTransactionsByEndDate.Where(tran => tran.InvestmentCode == security.Key).Where(tran => tran.Type == "AT");

                    foreach (var transferInTran in endHoldingValueTransferInTrans)
                    {
                        decimal netValue = 0;
                        var secEntityTransferIn = asxBroker.ASXSecurity.Where(his => his.Date.Date <= transferInTran.TradeDate.Date.Date).OrderByDescending(secpri => secpri.Date).FirstOrDefault();
                        if (secEntityTransferIn != null)
                            netValue = transferInTran.Units * Convert.ToDecimal(secEntityTransferIn.UnitPrice);

                        endHoldingValue += netValue;
                    }

                    capitalReportDS.AddProductDetailRow(product, endUnitPrice, asxBroker.AsxCode, endHoldingUnits, endHoldingUnits,
                                                           startHoldingUnits, latestHoldingValue, startHoldingValue, endHoldingValue, model.ServiceType.ToString());

                    presentHoldingTotal += latestHoldingValue;
                    endHoldingValueProductSec += endHoldingValue;
                    startHoldingTotal += startHoldingValue;

                    adjustmentUpTotal += adjustmentUp;
                    adjustmentDownTotal += adjustmentDown;
                    sellTotal += (sell * -1);
                    buyPurchaseTotal += buyPurchase;
                    transferOutASXTotal += transferOutASX;
                    transferInASXTotal += transferInASX;

                }
            }

            capitalReportDS.AddCapitalRow(asset.Name, model.ServiceType.ToString(), product.Name, product.TaskDescription, desktopBrokerAccountCM.DesktopBrokerAccountEntity.AccountNumber
                                                                                    , startHoldingTotal, presentHoldingTotal, transferOutASXTotal + transferInASXTotal, 0, sellTotal + buyPurchaseTotal, 0, 0, 0, 0, product.ID.ToString(), string.Empty);
        }

        private static void GetCapitalMovementTDTransactions(ICMBroker Broker, IOrganization orgCM, CapitalReportDS capitalReportDS, DataTable TDBreakDownTable, AccountProcessTaskEntity accountProcessTaskEntity, AssetEntity asset, ModelEntity model, ProductEntity product, string clientID)
        {
            BankAccountCM bankAccount = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as BankAccountCM;
            if (bankAccount != null)
            {
                var totalMonthTransactions = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate.Date.Date <= capitalReportDS.EndDate.Date && trans.TransactionDate.Date >= capitalReportDS.StartDate.Date);

                SetBankTransactionDS(capitalReportDS, capitalReportDS.StartDate.Date.ToString("MMM-yy"), totalMonthTransactions, bankAccount.BankAccountEntity, clientID, model.ServiceType);

                decimal contributions = totalMonthTransactions.Where(tran => tran.Category == "Contribution").Sum(tran => tran.TotalAmount);
                decimal income = totalMonthTransactions.Where(tran => tran.Category == "Income").Sum(tran => tran.TotalAmount);
                decimal transferin = totalMonthTransactions.Where(tran => tran.Category == "Income" && tran.SystemTransactionType == "Transfer In").Sum(tran => tran.TotalAmount);
                decimal internalCashMovementIncome = totalMonthTransactions.Where(tran => tran.Category == "Income" && tran.SystemTransactionType == "Internal Cash Movement").Sum(tran => tran.TotalAmount);
                decimal transferInOutOther = totalMonthTransactions.Where(tran => tran.SystemTransactionType == string.Empty).Sum(tran => tran.TotalAmount);

                income = income - transferin - internalCashMovementIncome;

                decimal taxinout = totalMonthTransactions.Where(tran => tran.Category == "TAX").Sum(tran => tran.TotalAmount);
                decimal benefitPayment = totalMonthTransactions.Where(tran => tran.Category == "Benefit Payment").Sum(tran => tran.TotalAmount);
                decimal investment = totalMonthTransactions.Where(tran => tran.Category == "Investment").Sum(tran => tran.TotalAmount);
                decimal expense = totalMonthTransactions.Where(tran => tran.Category == "Expense").Sum(tran => tran.TotalAmount);
                decimal transferOut = totalMonthTransactions.Where(tran => tran.Category == "Expense" && tran.SystemTransactionType == "Transfer Out").Sum(tran => tran.TotalAmount);
                decimal internalCashMovementExpense = totalMonthTransactions.Where(tran => tran.Category == "Expense" && tran.SystemTransactionType == "Internal Cash Movement").Sum(tran => tran.TotalAmount);

                expense = expense - transferOut - internalCashMovementExpense;

                decimal transfer = 0;
                transfer += contributions;
                transfer += benefitPayment;
                transfer += transferOut;
                transfer += transferin;
                transfer += transferInOutOther;

                decimal internalCashMove = 0;
                internalCashMove += internalCashMovementIncome;
                internalCashMove += internalCashMovementExpense;

                if (bankAccount != null)
                {
                    decimal holdingTotalStart = 0;
                    decimal holdingTotalEnd = 0;

                    var filteredTransactions = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate <= capitalReportDS.StartDate.Date.AddDays(-1));
                    var tdGroupsType = filteredTransactions.GroupBy(cashMan => cashMan.TransactionType);

                    foreach (var tdType in tdGroupsType)
                    {
                        var groupByIns = tdType.GroupBy(tdtypetran => tdtypetran.InstitutionID);

                        foreach (var groupins in groupByIns)
                        {
                            var transaction = groupins.FirstOrDefault();
                            decimal holding = groupins.Sum(inssum => inssum.Amount);

                            DataRow tDBreakDownTableRow = TDBreakDownTable.NewRow();
                            capitalReportDS.TDBreakDownRow(orgCM.Institution.Where(ins => ins.ID == groupins.Key).FirstOrDefault().Name, model.ServiceType.ToString(), tdType.Key, transaction.AdministrationSystem, holding, bankAccount.BankAccountEntity.InstitutionID.ToString(), tDBreakDownTableRow);
                            TDBreakDownTable.Rows.Add(tDBreakDownTableRow);

                            holdingTotalStart += holding;
                        }
                    }

                    filteredTransactions = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate <= capitalReportDS.EndDate);
                    tdGroupsType = filteredTransactions.GroupBy(cashMan => cashMan.TransactionType);

                    foreach (var tdType in tdGroupsType)
                    {
                        var groupByIns = tdType.GroupBy(tdtypetran => tdtypetran.InstitutionID);

                        foreach (var groupins in groupByIns)
                        {
                            var transaction = groupins.FirstOrDefault();
                            decimal holding = groupins.Sum(inssum => inssum.Amount);

                            DataRow tDBreakDownTableRow = TDBreakDownTable.NewRow();

                            string name = string.Empty;
                            var instNameObj = orgCM.Institution.Where(ins => ins.ID == groupins.Key).FirstOrDefault();

                            if (instNameObj != null)
                                name = instNameObj.Name;

                            capitalReportDS.TDBreakDownRow(name, model.ServiceType.ToString(), tdType.Key, transaction.AdministrationSystem, holding, bankAccount.BankAccountEntity.InstitutionID.ToString(), tDBreakDownTableRow);
                            TDBreakDownTable.Rows.Add(tDBreakDownTableRow);

                            holdingTotalEnd += holding;
                        }
                    }

                    capitalReportDS.AddCapitalRow(asset.Name, model.ServiceType.ToString(), product.Name,
                                                        product.TaskDescription, bankAccount.BankAccountEntity.AccountNumber, holdingTotalStart, holdingTotalEnd, transfer, income, investment, taxinout, expense, internalCashMove, 0, string.Empty, bankAccount.BankAccountEntity.InstitutionID.ToString());
                }
            }
        }

        private static void GetCapitalMovementBankTransactions(ICMBroker Broker, CapitalReportDS capitalReportDS, AccountProcessTaskEntity accountProcessTaskEntity, AssetEntity asset, ModelEntity model, ProductEntity product, string clientId)
        {
            BankAccountCM bankAccount = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as BankAccountCM;
            var filteredTransactionsStartDate = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate.Date <= capitalReportDS.StartDate.Date.AddDays(-1));
            var filteredTransactionsEndDat = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate.Date <= capitalReportDS.EndDate.Date);
            decimal openingBalance = filteredTransactionsStartDate.Sum(calc => calc.TotalAmount);
            decimal closingBalance = filteredTransactionsEndDat.Sum(calc => calc.TotalAmount);

            var totalMonthTransactions = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate.Date.Date <= capitalReportDS.EndDate.Date && trans.TransactionDate.Date >= capitalReportDS.StartDate.Date);

            SetBankTransactionDS(capitalReportDS, capitalReportDS.StartDate.Date.ToString("MMM-yy"), totalMonthTransactions, bankAccount.BankAccountEntity, clientId, model.ServiceType);

            decimal contributions = totalMonthTransactions.Where(tran => tran.Category == "Contribution").Sum(tran => tran.TotalAmount);
            decimal income = totalMonthTransactions.Where(tran => tran.Category == "Income").Sum(tran => tran.TotalAmount);
            decimal transferin = totalMonthTransactions.Where(tran => tran.Category == "Income" && tran.SystemTransactionType == "Transfer In").Sum(tran => tran.TotalAmount);
            decimal internalCashMovementIncome = totalMonthTransactions.Where(tran => tran.Category == "Income" && tran.SystemTransactionType == "Internal Cash Movement").Sum(tran => tran.TotalAmount);
            decimal transferInOutOther = totalMonthTransactions.Where(tran => tran.SystemTransactionType == string.Empty).Sum(tran => tran.TotalAmount);

            income = income - transferin - internalCashMovementIncome;

            decimal taxinout = totalMonthTransactions.Where(tran => tran.Category == "TAX").Sum(tran => tran.TotalAmount);
            decimal benefitPayment = totalMonthTransactions.Where(tran => tran.Category == "Benefit Payment").Sum(tran => tran.TotalAmount);
            decimal investment = totalMonthTransactions.Where(tran => tran.Category == "Investment").Sum(tran => tran.TotalAmount);
            decimal expense = totalMonthTransactions.Where(tran => tran.Category == "Expense").Sum(tran => tran.TotalAmount);
            decimal transferOut = totalMonthTransactions.Where(tran => tran.Category == "Expense" && tran.SystemTransactionType == "Transfer Out").Sum(tran => tran.TotalAmount);
            decimal internalCashMovementExpense = totalMonthTransactions.Where(tran => tran.Category == "Expense" && tran.SystemTransactionType == "Internal Cash Movement").Sum(tran => tran.TotalAmount);

            expense = expense - transferOut - internalCashMovementExpense;

            decimal transfer = 0;
            transfer += contributions;
            transfer += benefitPayment;
            transfer += transferOut;
            transfer += transferin;
            transfer += transferInOutOther;

            decimal internalCashMove = 0;
            internalCashMove += internalCashMovementIncome;
            internalCashMove += internalCashMovementExpense;


            capitalReportDS.AddCapitalRow(asset.Name, model.ServiceType.ToString(), product.Name,
                                                product.TaskDescription, bankAccount.BankAccountEntity.AccountNumber, openingBalance, closingBalance, transfer, income, investment, taxinout, expense, internalCashMove, 0, string.Empty, string.Empty);
        }

        public static void SetDesktopBrokerTransactionDS(CapitalReportDS data, string month, IEnumerable<HoldingTransactions>
                                                        filteredTransactionsByMonth, DesktopBrokerAccountEntity desktopBrokerAccountEntity, string clientId, string serviceType)
        {


            DataTable dt = data.Tables[CapitalReportDS.ASXTRANSTABLE];

            foreach (HoldingTransactions holdingTransactions in filteredTransactionsByMonth)
            {
                string category = GetDesktopBrokerTransactionCategory(holdingTransactions);

                if (category != string.Empty)
                {
                    DataRow row = dt.NewRow();
                    row[CapitalReportDS.CLIENTID] = clientId;
                    row[CapitalReportDS.SERVICETYPE] = serviceType;
                    row[CapitalReportDS.NAME] = desktopBrokerAccountEntity.Name;
                    row[CapitalReportDS.ACCOUNTNO] = desktopBrokerAccountEntity.AccountNumber;
                    row[CapitalReportDS.FPSID] = holdingTransactions.FPSInstructionID;
                    row[CapitalReportDS.INVESMENTCODE] = holdingTransactions.InvestmentCode;
                    row[CapitalReportDS.TRANSACTIONTYPE] = holdingTransactions.TransactionType;
                    row[CapitalReportDS.TRADEDATE] = holdingTransactions.TradeDate;

                    if (holdingTransactions.SettlementDate != null)
                        row[CapitalReportDS.SETTLEDATE] = holdingTransactions.SettlementDate;
                    else
                        row[CapitalReportDS.SETTLEDATE] = holdingTransactions.TradeDate;
                    row[CapitalReportDS.MONTH] = month;
                    row[CapitalReportDS.CAPITALMOVEMENTCAT] = category;
                    row[CapitalReportDS.UNITS] = holdingTransactions.Units;
                    row[CapitalReportDS.GROSSVALUE] = holdingTransactions.GrossValue;
                    row[CapitalReportDS.BROKERAGEAMOUNT] = holdingTransactions.BrokerageAmount;
                    row[CapitalReportDS.CHARGES] = holdingTransactions.Charges;
                    row[CapitalReportDS.BROKERGST] = holdingTransactions.BrokerageGST;
                    row[CapitalReportDS.BROKERAGEAMOUNT] = holdingTransactions.BrokerageAmount;
                    row[CapitalReportDS.NETVALUE] = holdingTransactions.NetValue;
                    row[CapitalReportDS.NARRATIVE] = holdingTransactions.Narrative;

                    dt.Rows.Add(row);
                }
            }

        }

        public static void SetBankTransactionDS(CapitalReportDS data, string month, IEnumerable<CashManagementEntity> totalMonthTransactions, BankAccountEntity bankAccountEntity, string clientId, ServiceTypes serviceType)
        {
            DataTable dt = data.Tables[CapitalReportDS.BANKTRANSACTIONSTABLE];

            CapitalMovementByCategoryTotal capitalMovementByCategoryTotal = new CapitalMovementByCategoryTotal();

            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashManagementEntity in totalMonthTransactions)
            {
                string category = GetBankTransactionCategory(cashManagementEntity);

                if (category != string.Empty)
                {
                    DataRow row = dt.NewRow();

                    row[CapitalReportDS.SERVICETYPE] = serviceType;
                    row[CapitalReportDS.CLIENTID] = clientId;
                    row[CapitalReportDS.BSB] = bankAccountEntity.BSB;
                    row[CapitalReportDS.ACCOUNTTYPE] = bankAccountEntity.AccoutType.ToUpper();
                    row[CapitalReportDS.ACCOUNTNO] = bankAccountEntity.AccountNumber;
                    row[CapitalReportDS.ACCOUNTNAME] = bankAccountEntity.Name;
                    row[CapitalReportDS.MONTH] = month;
                    row[CapitalReportDS.CAPITALMOVEMENTCAT] = category;
                    row[CapitalReportDS.AMOUNTTOTAL] = cashManagementEntity.TotalAmount;
                    row[CapitalReportDS.BANKAMOUNT] = cashManagementEntity.Amount;
                    row[CapitalReportDS.BANKADJUSTMENT] = cashManagementEntity.Adjustment;
                    row[CapitalReportDS.BANKTRANSDATE] = cashManagementEntity.TransactionDate;
                    row[CapitalReportDS.IMPTRANTYPE] = cashManagementEntity.ImportTransactionType;
                    row[CapitalReportDS.SYSTRANTYPE] = cashManagementEntity.SystemTransactionType;
                    row[CapitalReportDS.CATEGORY] = cashManagementEntity.Category;
                    row[CapitalReportDS.COMMENT] = cashManagementEntity.Comment;

                    dt.Rows.Add(row);
                }
            }
        }

        private static string GetBankTransactionCategory(Oritax.TaxSimp.Common.CashManagementEntity cashManagementEntity)
        {
            string category = string.Empty;

            if (cashManagementEntity.Category == "Contribution")
                category = "Transfer In/Out";
            else if (cashManagementEntity.Category == "Income" && (cashManagementEntity.SystemTransactionType != "Transfer In" || cashManagementEntity.SystemTransactionType != "Internal Cash Movement"))
                category = "Income";
            else if (cashManagementEntity.Category == "Income" && cashManagementEntity.SystemTransactionType == "Transfer In")
                category = "Transfer In/Out";
            else if (cashManagementEntity.Category == "Income" && cashManagementEntity.SystemTransactionType == "Internal Cash Movement")
                category = "Internal Cash Movement";
            else if (cashManagementEntity.Category == "Benefit Payment")
                category = "Transfer In/Out";
            else if (cashManagementEntity.Category == "TAX")
                category = "Tax & Expenses";
            else if (cashManagementEntity.Category == "Investment")
                category = "Investments";
            else if (cashManagementEntity.Category == "Expense" && cashManagementEntity.SystemTransactionType == "Transfer Out")
                category = "Transfer In/Out";
            else if (cashManagementEntity.Category == "Expense" && (cashManagementEntity.SystemTransactionType != "Transfer Out" || cashManagementEntity.SystemTransactionType != "Internal Cash Movement"))
                category = "Tax & Expenses";
            else if (cashManagementEntity.Category == "Expense" && cashManagementEntity.SystemTransactionType == "Internal Cash Movement")
                category = "Internal Cash Movement";
            return category;
        }

        private static string GetDesktopBrokerTransactionCategory(HoldingTransactions holdingTransaction)
        {
            string category = string.Empty;

            if (holdingTransaction.Type == "AT")
                category = "Transfer In/Out";
            else if (holdingTransaction.Type == "RT")
                category = "Transfer In/Out";
            else if (holdingTransaction.Type == "RJ")
                category = "Transfer In/Out";
            else if (holdingTransaction.Type == "AJ")
                category = "Transfer In/Out";
            else if (holdingTransaction.Type == "RA")
                category = "Investments";
            else if (holdingTransaction.Type == "AN")
                category = "Investments";

            return category;
        }

        private DataTable ExtractCapitalSummaryTableByCategory(ICMBroker Broker, CorporateEntity Entity, CapitalReportDS capitalReportDS, string ClientID, IOrganization orgCM)
        {
            DataTable CapitalFlowSummaryReport = capitalReportDS.Tables[CapitalReportDS.CAPITALFLOWSUMMARYCAT];
            DateTimeSpan dateTimeSpan = DateTimeSpan.CompareDates(capitalReportDS.StartDate, capitalReportDS.EndDate);
            for (int monthCounter = 0; monthCounter <= dateTimeSpan.TotalMonths(); monthCounter++)
            {
                CapitalMovementByCategoryTotal capitalMovementByCategoryTotal = new CapitalMovementByCategoryTotal();

                DateTime startdate = FirstDayOfMonthFromDateTime(capitalReportDS.StartDate.AddMonths(monthCounter));
                DateTime endDate = LastDayOfMonthFromDateTime(capitalReportDS.StartDate.AddMonths(monthCounter));

                ManualTransactions(ClientID, Entity.ClientManualAssetCollection, orgCM, capitalReportDS, capitalMovementByCategoryTotal, startdate, endDate);
                GetTransactionsInfoFromLinkProcess(ClientID, Broker, Entity.ListAccountProcess, orgCM, capitalReportDS, capitalMovementByCategoryTotal, startdate, endDate);
                ManageInvestmentSchemes(ClientID, Broker, orgCM, capitalReportDS, capitalMovementByCategoryTotal, startdate, endDate);
                SetDesktopBrokerAccount(Broker, Entity.DesktopBrokerAccounts, capitalReportDS, orgCM, capitalMovementByCategoryTotal, startdate, endDate, ClientID, Entity.ListAccountProcess);

                DataRow row = CapitalFlowSummaryReport.NewRow();

                row[CapitalReportDS.MONTH] = startdate;
                row[CapitalReportDS.OPENINGBAL] = capitalMovementByCategoryTotal.OpeningBalTotal;
                row[CapitalReportDS.TRANSFEROUT] = capitalMovementByCategoryTotal.TransferInOutTotal;
                row[CapitalReportDS.INCOME] = capitalMovementByCategoryTotal.IncomeTotal;
                row[CapitalReportDS.APPLICATIONREDEMPTION] = capitalMovementByCategoryTotal.ApplicationRedTotal;
                row[CapitalReportDS.TAXINOUT] = capitalMovementByCategoryTotal.TaxInOutTotal;
                row[CapitalReportDS.EXPENSE] = capitalMovementByCategoryTotal.ExpenseTotal;
                row[CapitalReportDS.INTERNALCASHMOVEMENT] = capitalMovementByCategoryTotal.InternalCashMovementTotal;
                row[CapitalReportDS.CLOSINGBALANCE] = capitalMovementByCategoryTotal.ClosingBalanceTotal;
                row[CapitalReportDS.CHANGEININVESTMENTVALUE] = capitalMovementByCategoryTotal.ChangeInInvestTotal;
                CapitalFlowSummaryReport.Rows.Add(row);

            }

            return CapitalFlowSummaryReport;
        }

        private static void GetTransactionsInfoFromLinkProcess(string clientID, ICMBroker Broker, List<AccountProcessTaskEntity> ListAccountProcess, IOrganization orgCM, CapitalReportDS capitalReportDS, CapitalMovementByCategoryTotal capitalMovementByCategoryTotal, DateTime startDate, DateTime endDate)
        {
            foreach (var accountProcessTaskEntity in ListAccountProcess)
            {
                if (accountProcessTaskEntity.LinkedEntity != null &&
                    accountProcessTaskEntity.LinkedEntity.Clid != Guid.Empty)
                {
                    var model = orgCM.Model.Where(mod => mod.ID == accountProcessTaskEntity.ModelID).FirstOrDefault();
                    if (model != null)
                    {
                        var asset = model.Assets.Where(var => var.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                        if (asset != null)
                        {
                            var product = asset.Products.Where(pro => pro.ID == accountProcessTaskEntity.TaskID).FirstOrDefault();
                            if (product != null)
                            {

                                if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.BankAccount)
                                    BankAccountTransctions(Broker, capitalReportDS, accountProcessTaskEntity, asset, model, product, capitalMovementByCategoryTotal, startDate, endDate, clientID);
                                else if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.TermDepositAccount)
                                    TDTransactions(Broker, orgCM, capitalReportDS, accountProcessTaskEntity,capitalMovementByCategoryTotal, startDate, endDate);
                            }
                        }
                    }
                }
            }
        }

        private static void ManageInvestmentSchemes(string clientID, ICMBroker Broker, IOrganization orgCM, CapitalReportDS capitalReportDS, CapitalMovementByCategoryTotal capitalMovementByCategoryTotal, DateTime startDate, DateTime endDate)
        {
            ManagedInvestmentSchemesAccountCM managedInvestmentSchemesAccountCM = Broker.GetBMCInstance(new Guid("b354e928-a3c2-43d7-b61e-feb215cf1bd3")) as ManagedInvestmentSchemesAccountCM;

            var linkedMISAccountGroup = managedInvestmentSchemesAccountCM.ManagedInvestmentSchemesAccountEntity.FundAccounts.GroupBy(fa => fa.Code);
            foreach (var linkedMISAccountItem in linkedMISAccountGroup)
            {
                var linkedMISAccount = linkedMISAccountItem.FirstOrDefault();
                var filteredHoldingTransactionsForClient = linkedMISAccount.FundTransactions.Where(cliFundTrans => cliFundTrans.ClientID == clientID);
                var filteredHoldingTransactionsRange = filteredHoldingTransactionsForClient.Where(cliFundTrans => cliFundTrans.TradeDate >= startDate && cliFundTrans.TradeDate <= endDate);

                if (filteredHoldingTransactionsForClient.Count() > 0)
                {
                    var filteredHoldingTransactionsByOpeningDate = filteredHoldingTransactionsForClient.Where(cliFundTrans => cliFundTrans.TradeDate <= startDate.AddDays(-1));
                    decimal totalSharesOpening = filteredHoldingTransactionsByOpeningDate.Sum(ft => ft.Shares);

                    var linkedmissec = orgCM.Securities.Where(missec => missec.AsxCode == linkedMISAccount.Code).FirstOrDefault();

                    decimal openingBalance = 0;

                    if (linkedmissec != null)
                    {
                        var latestMisPriceObject = linkedmissec.ASXSecurity.Where(mishis => mishis.Date <= startDate.AddDays(-1)).OrderByDescending(mishis => mishis.Date).FirstOrDefault();
                        decimal unitPrice = 0;
                        if (latestMisPriceObject != null)
                            unitPrice = Convert.ToDecimal(latestMisPriceObject.UnitPrice);
                        openingBalance = totalSharesOpening * unitPrice;
                    }

                    var filteredHoldingTransactionsByClosingDate = filteredHoldingTransactionsForClient.Where(cliFundTrans => cliFundTrans.TradeDate <= endDate);
                    decimal totalSharesClosing = filteredHoldingTransactionsByClosingDate.Sum(ft => ft.Shares);
                    decimal closingBalance = 0;

        
                    if (linkedmissec != null)
                    {
                        var latestMisPriceObject = linkedmissec.ASXSecurity.Where(mishis => mishis.Date <= endDate).OrderByDescending(mishis => mishis.Date).FirstOrDefault();
                        decimal unitPrice = 0;
                        if (latestMisPriceObject != null)
                            unitPrice = Convert.ToDecimal(latestMisPriceObject.UnitPrice);
                        closingBalance = totalSharesClosing * unitPrice;
                    }
                    capitalMovementByCategoryTotal.OpeningBalTotal += openingBalance;
                    capitalMovementByCategoryTotal.ClosingBalanceTotal += closingBalance;
                }

                if (filteredHoldingTransactionsRange.Count() > 0)
                {
                    var buyPurchaseTrans = filteredHoldingTransactionsRange.Where(tran => tran.TransactionType == "Purchase" || tran.TransactionType == "Application");
                    foreach (MISFundTransactionEntity entity in buyPurchaseTrans)
                        capitalReportDS.AddTranBreakdownRow(entity.ID, "Fund", linkedMISAccount.Code, "", "Application", "Investment", entity.TradeDate, entity.Amount);

                    decimal buyPurchase = filteredHoldingTransactionsRange.Where(tran => tran.TransactionType == "Purchase").Sum(tran => tran.Amount);
                    buyPurchase += filteredHoldingTransactionsRange.Where(tran => tran.TransactionType == "Application").Sum(tran => tran.Amount);

                    var sellTrans = filteredHoldingTransactionsRange.Where(tran => tran.TransactionType == "Redemption");
                    foreach (MISFundTransactionEntity entity in sellTrans)
                        capitalReportDS.AddTranBreakdownRow(entity.ID, "Fund", linkedMISAccount.Code, "", entity.TransactionType, "Investment", entity.TradeDate, entity.Amount);
                    decimal sell = filteredHoldingTransactionsRange.Where(tran => tran.TransactionType == "Redemption").Sum(tran => tran.Amount);

                    capitalMovementByCategoryTotal.Application += buyPurchase;
                    capitalMovementByCategoryTotal.Redemption += sell;
                }
            }
        }

        private static void DesktopBrokerTransactions(ICMBroker Broker, IOrganization orgCM, CapitalReportDS capitalReportDS, AccountProcessTaskEntity accountProcessTaskEntity, AssetEntity asset, ModelEntity model, ProductEntity product, CapitalMovementByCategoryTotal capitalMovementByCategoryTotal, DateTime startDate, DateTime endDate)
        {
            DesktopBrokerAccountCM desktopBrokerAccountCM = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid,
                                                                        accountProcessTaskEntity.LinkedEntity.Csid) as DesktopBrokerAccountCM;
            decimal totalOpeiningBalance = 0;
            decimal totalClosingBalance = 0;

            if (product.ProductSecuritiesId != Guid.Empty)
            {
                var productSecurity = orgCM.ProductSecurities.Where(proseclist => proseclist.ID == product.ProductSecuritiesId).FirstOrDefault();
                var linkedAssets = model.Assets.Where(ast => ast.ID == accountProcessTaskEntity.AssetID && accountProcessTaskEntity.ModelID == model.ID);
                double totalPercentage = HoldingRptDataSet.CalculateTotalPercentageForSplit(linkedAssets);

                foreach (var productSecurityDetail in productSecurity.Details)
                {
                    var asxBroker = orgCM.Securities.Where(sec => sec.ID == productSecurityDetail.SecurityCodeId).FirstOrDefault();

                    var filteredHoldingTransactionsByOpening = desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Where(holdTrans =>
                                                                                              holdTrans.TradeDate <= startDate.AddDays(-1));

                    var filteredHoldingTransactionsByClosing = desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Where(holdTrans =>
                                                                                  holdTrans.TradeDate <= endDate);

                    var filteredHoldingTransactionsOpening = filteredHoldingTransactionsByOpening.Where(holdTrans =>
                                                                                                  holdTrans.InvestmentCode == asxBroker.AsxCode);

                    var filteredHoldingTransactionsClosing = filteredHoldingTransactionsByClosing.Where(holdTrans =>
                                                                              holdTrans.InvestmentCode == asxBroker.AsxCode);

                    SetProductSecurity(orgCM, product, startDate.AddDays(-1), productSecurity, linkedAssets, ref totalPercentage, asxBroker, filteredHoldingTransactionsOpening, ref totalOpeiningBalance);

                    SetProductSecurity(orgCM, product, endDate, productSecurity, linkedAssets, ref totalPercentage, asxBroker, filteredHoldingTransactionsClosing, ref totalClosingBalance);
                }
            }

            else
                CalculateDesktopBrokerWithoutProductSecurity(orgCM, startDate, endDate, desktopBrokerAccountCM, ref totalOpeiningBalance, ref totalClosingBalance);

            capitalMovementByCategoryTotal.OpeningBalTotal += totalOpeiningBalance;
            capitalMovementByCategoryTotal.ClosingBalanceTotal += totalClosingBalance;
        }

        private static void SetProductSecurity(IOrganization orgCM, ProductEntity product, DateTime datetime, ProductSecuritiesEntity productSecurity, IEnumerable<AssetEntity> linkedAssets, ref double totalPercentage, SecuritiesEntity asxBroker, IEnumerable<HoldingTransactions> filteredHoldingTransactions, ref decimal totalHolding)
        {
            if (filteredHoldingTransactions.Count() > 0)
            {
                totalPercentage = product.SharePercentage;

                foreach (var linkedasst in linkedAssets)
                {
                    var otherProducts = linkedasst.Products.Where(linkedAsstPro => linkedAsstPro.ProductSecuritiesId != productSecurity.ID);
                    foreach (var OtherproductSecurity in otherProducts)
                    {
                        var OtherproductSecurityDetails = orgCM.ProductSecurities.Where(otherpro => otherpro.ID == OtherproductSecurity.ProductSecuritiesId).FirstOrDefault();

                        if (OtherproductSecurityDetails != null)
                        {
                            var otherAsxBroker = OtherproductSecurityDetails.Details.Where(otherProDetail => otherProDetail.SecurityCodeId == asxBroker.ID);

                            if (otherAsxBroker.Count() > 0)
                                totalPercentage += OtherproductSecurity.SharePercentage;
                        }
                    }
                }

                var hisSecurity = asxBroker.ASXSecurity.Where(secpri => secpri.Date <= datetime).OrderByDescending(secpri => secpri.Date).FirstOrDefault();
                decimal holding = filteredHoldingTransactions.Sum(filteredHolding => filteredHolding.Units);
                decimal unitPrice = 0;
                if (hisSecurity != null)
                    unitPrice = Convert.ToDecimal(hisSecurity.UnitPrice);

                decimal currentValue = holding * unitPrice;
                currentValue = currentValue * Convert.ToDecimal((product.SharePercentage / totalPercentage));
                totalHolding += currentValue;
            }
        }

        private static void CalculateDesktopBrokerWithoutProductSecurity(IOrganization orgCM, DateTime startDate, DateTime endDate, DesktopBrokerAccountCM desktopBrokerAccountCM, ref decimal totalOpeiningBalance, ref decimal totalClosingBalance)
        {
            var filteredHoldingTransactionsByOpening = desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Where(holdTrans =>
                                                                                              holdTrans.TradeDate <= startDate.AddDays(-1));

            var filteredHoldingTransactionsByClosing = desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Where(holdTrans =>
                                                                                  holdTrans.TradeDate <= endDate);


            var groupedSecuritiesOpening = filteredHoldingTransactionsByOpening.GroupBy(holdTran => holdTran.InvestmentCode);

            foreach (var security in groupedSecuritiesOpening)
            {
                decimal totalSecHoldingUnits = security.Sum(sec => sec.Units);
                var asxBroker = orgCM.Securities.Where(sec => sec.AsxCode == security.Key).FirstOrDefault();
                var hisSecurity = asxBroker.ASXSecurity.Where(secpri => secpri.Date <= startDate.AddDays(-1)).OrderByDescending(secpri => secpri.Date).FirstOrDefault();
                decimal unitPrice = 0;

                if (hisSecurity != null)
                    unitPrice = Convert.ToDecimal(hisSecurity.UnitPrice);

                decimal currentValue = totalSecHoldingUnits * unitPrice;
                totalOpeiningBalance += currentValue;
            }

            var groupedSecuritiesClosing = filteredHoldingTransactionsByClosing.GroupBy(holdTran => holdTran.InvestmentCode);

            foreach (var security in groupedSecuritiesClosing)
            {
                decimal totalSecHoldingUnits = security.Sum(sec => sec.Units);
                var asxBroker = orgCM.Securities.Where(sec => sec.AsxCode == security.Key).FirstOrDefault();
                var hisSecurity = asxBroker.ASXSecurity.Where(secpri => secpri.Date <= endDate).OrderByDescending(secpri => secpri.Date).FirstOrDefault();
                decimal unitPrice = 0;

                if (hisSecurity != null)
                    unitPrice = Convert.ToDecimal(hisSecurity.UnitPrice);

                decimal currentValue = totalSecHoldingUnits * unitPrice;
                totalClosingBalance += currentValue;
            }
        }

        private static void TDTransactions(ICMBroker Broker, IOrganization orgCM, CapitalReportDS capitalReportDS, AccountProcessTaskEntity accountProcessTaskEntity, CapitalMovementByCategoryTotal capitalMovementByCategoryTotal, DateTime startDate, DateTime endDate)
        {
            BankAccountCM bankAccount = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as BankAccountCM;

            if (bankAccount != null)
            {
                decimal openingBalance = 0;
                decimal closingBalance = 0;

                var filteredTransactionsOpening = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate.Date <= startDate.AddDays(-1));
                var filteredTransactionsClosing = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate.Date <= endDate);

                var tdGroupsOpeningType = filteredTransactionsOpening.GroupBy(cashMan => cashMan.TransactionType);

                foreach (var tdType in tdGroupsOpeningType)
                {
                    var groupByIns = tdType.GroupBy(tdtypetran => tdtypetran.InstitutionID);

                    foreach (var groupins in groupByIns)
                    {
                        var transaction = groupins.FirstOrDefault();
                        decimal holding = groupins.Sum(inssum => inssum.Amount);
                        openingBalance += holding;
                    }
                }

                var tdGroupsClosingType = filteredTransactionsClosing.GroupBy(cashMan => cashMan.TransactionType);

                foreach (var tdType in tdGroupsClosingType)
                {
                    var groupByIns = tdType.GroupBy(tdtypetran => tdtypetran.InstitutionID);

                    foreach (var groupins in groupByIns)
                    {
                        var transaction = groupins.FirstOrDefault();
                        decimal holding = groupins.Sum(inssum => inssum.Amount);
                        closingBalance += holding;
                    }
                }

                var filteredTransactions = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate.Date <= endDate && trans.TransactionDate.Date >= startDate);

                var applicationTrans = filteredTransactions.Where(tran => tran.SystemTransactionType == "Application");
                foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in applicationTrans)
                    capitalReportDS.AddTranBreakdownRow(cashEntity.ID, "TDs", bankAccount.BankAccountEntity.AccountNumber, cashEntity.Comment, cashEntity.SystemTransactionType, "Investment", cashEntity.TransactionDate, cashEntity.TotalAmount);
                decimal application = filteredTransactions.Where(tran => tran.SystemTransactionType == "Application").Sum(tran => tran.TotalAmount);

                var redemptionTrans = filteredTransactions.Where(tran => tran.SystemTransactionType == "Redemption");
                foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in redemptionTrans)
                    capitalReportDS.AddTranBreakdownRow(cashEntity.ID, "TDs", bankAccount.BankAccountEntity.AccountNumber, cashEntity.Comment, cashEntity.SystemTransactionType, "Investment", cashEntity.TransactionDate, cashEntity.TotalAmount);
                decimal redemption = filteredTransactions.Where(tran => tran.SystemTransactionType == "Redemption").Sum(tran => tran.TotalAmount);

                var interestTrans = filteredTransactions.Where(tran => tran.SystemTransactionType == "Interest");
                foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in interestTrans)
                    capitalReportDS.AddTranBreakdownRow(cashEntity.ID, "TDs", bankAccount.BankAccountEntity.AccountNumber, cashEntity.Comment, cashEntity.SystemTransactionType, "Income", cashEntity.TransactionDate, cashEntity.TotalAmount);
                decimal interest = filteredTransactions.Where(tran => tran.SystemTransactionType == "Interest").Sum(tran => tran.TotalAmount);

                capitalMovementByCategoryTotal.OpeningBalTotal += openingBalance;
                capitalMovementByCategoryTotal.ClosingBalanceTotal += closingBalance;
                capitalMovementByCategoryTotal.Application += application;
                capitalMovementByCategoryTotal.Redemption += redemption;
                capitalMovementByCategoryTotal.IncomeTotal += interest;
            }
        }

        private static void BankAccountTransctions(ICMBroker Broker, CapitalReportDS capitalReportDS, AccountProcessTaskEntity accountProcessTaskEntity, AssetEntity asset, ModelEntity model, ProductEntity product,
            CapitalMovementByCategoryTotal capitalMovementByCategoryTotal, DateTime startDate, DateTime endDate, string clientId)
        {
            BankAccountCM bankAccount = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as BankAccountCM;
            var filteredTransactionsStartDate = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate.Date <= startDate.Date.AddDays(-1));
            var filteredTransactionsEndDat = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate.Date <= endDate.Date);
            decimal openingBalance = filteredTransactionsStartDate.Sum(calc => calc.TotalAmount);
            decimal closingBalance = filteredTransactionsEndDat.Sum(calc => calc.TotalAmount);

            var totalMonthTransactions = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate.Date.Date <= endDate && trans.TransactionDate.Date >= startDate.Date);

            SetBankTransactionDS(capitalReportDS, startDate.ToString("MMM-yy"), totalMonthTransactions, bankAccount.BankAccountEntity, clientId, model.ServiceType);

            decimal contributions = totalMonthTransactions.Where(tran => tran.Category == "Contribution").Sum(tran => tran.TotalAmount);

            var incomeTransactions = totalMonthTransactions.Where(tran => tran.Category == "Income" && tran.SystemTransactionType != "Internal Cash Movement" && tran.SystemTransactionType != "Transfer In");
            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in incomeTransactions)
                capitalReportDS.AddTranBreakdownRow(cashEntity.ID, "CASH", bankAccount.BankAccountEntity.AccountNumber, cashEntity.Comment, cashEntity.SystemTransactionType, "Income", cashEntity.TransactionDate, cashEntity.TotalAmount);
            decimal income = totalMonthTransactions.Where(tran => tran.Category == "Income").Sum(tran => tran.TotalAmount);

            var transferinTransactions = totalMonthTransactions.Where(tran => tran.Category == "Income" && tran.SystemTransactionType == "Transfer In");
            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in transferinTransactions)
                capitalReportDS.AddTranBreakdownRow(cashEntity.ID, "CASH", bankAccount.BankAccountEntity.AccountNumber, cashEntity.Comment, "Transfer In", "Transfer In/Out", cashEntity.TransactionDate, cashEntity.TotalAmount);
            decimal transferin = totalMonthTransactions.Where(tran => tran.Category == "Income" && tran.SystemTransactionType == "Transfer In").Sum(tran => tran.TotalAmount);

            var internalCashMovementIncomeTrans = totalMonthTransactions.Where(tran => tran.Category == "Income" && tran.SystemTransactionType == "Internal Cash Movement");
            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in internalCashMovementIncomeTrans)
                capitalReportDS.AddTranBreakdownRow(cashEntity.ID, "CASH", bankAccount.BankAccountEntity.AccountNumber, cashEntity.Comment, "Internal Cash Mvt", "Internal Cash Mvt", cashEntity.TransactionDate, cashEntity.TotalAmount);
            decimal internalCashMovementIncome = totalMonthTransactions.Where(tran => tran.Category == "Income" && tran.SystemTransactionType == "Internal Cash Movement").Sum(tran => tran.TotalAmount);

            var transferInOutOtherTrans = totalMonthTransactions.Where(tran => tran.SystemTransactionType == string.Empty);
            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in transferInOutOtherTrans)
                capitalReportDS.AddTranBreakdownRow(cashEntity.ID, "CASH", bankAccount.BankAccountEntity.AccountNumber, cashEntity.Comment, "Transfer In/Out Other", "Transfer In/Out", cashEntity.TransactionDate, cashEntity.TotalAmount);
            decimal transferInOutOther = totalMonthTransactions.Where(tran => tran.SystemTransactionType == string.Empty).Sum(tran => tran.TotalAmount);

            income = income - transferin - internalCashMovementIncome;

            var taxinoutTrans = totalMonthTransactions.Where(tran => tran.Category == "TAX");
            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in taxinoutTrans)
                capitalReportDS.AddTranBreakdownRow(cashEntity.ID, "CASH", bankAccount.BankAccountEntity.AccountNumber, cashEntity.Comment, "Tax", "Tax & Expenses", cashEntity.TransactionDate, cashEntity.TotalAmount);
            decimal taxinout = totalMonthTransactions.Where(tran => tran.Category == "TAX").Sum(tran => tran.TotalAmount);

            var benefitPaymentTrans = totalMonthTransactions.Where(tran => tran.Category == "Benefit Payment");
            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in benefitPaymentTrans)
                capitalReportDS.AddTranBreakdownRow(cashEntity.ID, "CASH", bankAccount.BankAccountEntity.AccountNumber, cashEntity.Comment, "Benefit Payment", "Transfer In/Out", cashEntity.TransactionDate, cashEntity.TotalAmount);
            decimal benefitPayment = totalMonthTransactions.Where(tran => tran.Category == "Benefit Payment").Sum(tran => tran.TotalAmount);

            var investmentTrans = totalMonthTransactions.Where(tran => tran.Category == "Investment");
            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in investmentTrans)
                capitalReportDS.AddTranBreakdownRow(cashEntity.ID, "CASH", bankAccount.BankAccountEntity.AccountNumber, cashEntity.Comment, cashEntity.SystemTransactionType, cashEntity.Category, cashEntity.TransactionDate, cashEntity.TotalAmount);
            decimal investment = totalMonthTransactions.Where(tran => tran.Category == "Investment").Sum(tran => tran.TotalAmount);

            var expenseTrans = totalMonthTransactions.Where(tran => tran.Category == "Expense" && tran.SystemTransactionType != "Transfer Out" && tran.SystemTransactionType != "Internal Cash Movement");
            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in expenseTrans)
                capitalReportDS.AddTranBreakdownRow(cashEntity.ID, "CASH", bankAccount.BankAccountEntity.AccountNumber, cashEntity.Comment, cashEntity.SystemTransactionType, "Tax & Expenses", cashEntity.TransactionDate, cashEntity.TotalAmount);
            decimal expense = totalMonthTransactions.Where(tran => tran.Category == "Expense").Sum(tran => tran.TotalAmount);

            var transferOutTrans = totalMonthTransactions.Where(tran => tran.Category == "Expense" && tran.SystemTransactionType == "Transfer Out");
            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in transferOutTrans)
                capitalReportDS.AddTranBreakdownRow(cashEntity.ID, "CASH", bankAccount.BankAccountEntity.AccountNumber, cashEntity.Comment, cashEntity.SystemTransactionType, "Transfer In/Out", cashEntity.TransactionDate, cashEntity.TotalAmount);
            decimal transferOut = totalMonthTransactions.Where(tran => tran.Category == "Expense" && tran.SystemTransactionType == "Transfer Out").Sum(tran => tran.TotalAmount);

            var internalCashMovementExpenseTrans = totalMonthTransactions.Where(tran => tran.Category == "Expense" && tran.SystemTransactionType == "Internal Cash Movement");
            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in internalCashMovementExpenseTrans)
                capitalReportDS.AddTranBreakdownRow(cashEntity.ID, "CASH", bankAccount.BankAccountEntity.AccountNumber, cashEntity.Comment, "Internal Cash Mvt", "Internal Cash Mvt", cashEntity.TransactionDate, cashEntity.TotalAmount);
            decimal internalCashMovementExpense = totalMonthTransactions.Where(tran => tran.Category == "Expense" && tran.SystemTransactionType == "Internal Cash Movement").Sum(tran => tran.TotalAmount);
            expense = expense - transferOut - internalCashMovementExpense;


            capitalMovementByCategoryTotal.TransferInOutTotal += contributions;
            capitalMovementByCategoryTotal.TransferInOutTotal += benefitPayment;
            capitalMovementByCategoryTotal.TransferInOutTotal += transferOut;
            capitalMovementByCategoryTotal.TransferInOutTotal += transferin;
            capitalMovementByCategoryTotal.TransferInOutTotal += transferInOutOther;

            capitalMovementByCategoryTotal.InternalCashMovementTotal += internalCashMovementIncome;
            capitalMovementByCategoryTotal.InternalCashMovementTotal += internalCashMovementExpense;

            capitalMovementByCategoryTotal.Application += investment;

            capitalMovementByCategoryTotal.ExpenseTotal += expense;
            capitalMovementByCategoryTotal.ExpenseTotal += taxinout;

            capitalMovementByCategoryTotal.IncomeTotal += income;

            capitalMovementByCategoryTotal.OpeningBalTotal += openingBalance;
            capitalMovementByCategoryTotal.ClosingBalanceTotal += closingBalance;
        }

        private static void ManualTransactions(string clientID, ObservableCollection<ClientManualAssetEntity> ClientManualAssetCollection, IOrganization orgCM, CapitalReportDS capitalReportDS, CapitalMovementByCategoryTotal capitalMovementByCategoryTotal, DateTime startDate, DateTime endDate)
        {
            if (ClientManualAssetCollection != null)
            {
                var filteredManualAssetsStartDate = ClientManualAssetCollection.Where(man => man.TransactionDate.Date <= startDate.Date.AddDays(-1));
                var filteredManualAssetsEndDate = ClientManualAssetCollection.Where(man => man.TransactionDate.Date <= endDate.Date);

                decimal openingBalance = 0;

                foreach (var manualAsset in filteredManualAssetsStartDate)
                {
                    var systemManualAsset = orgCM.ManualAsset.Where(manList => manList.ID == manualAsset.InvestmentCode).FirstOrDefault();
                    var systemManualAssetPriceHistory = systemManualAsset.ASXSecurity.Where(manualHIS => manualHIS.Date <= startDate.AddDays(-1)).OrderByDescending(manualHis => manualHis.Date).FirstOrDefault();
                    if (systemManualAssetPriceHistory != null)
                        openingBalance += manualAsset.UnitsOnHand * Convert.ToDecimal(systemManualAssetPriceHistory.UnitPrice);
                }

                decimal closingBalance = 0;

                foreach (var manualAsset in filteredManualAssetsEndDate)
                {
                    var systemManualAsset = orgCM.ManualAsset.Where(manList => manList.ID == manualAsset.InvestmentCode).FirstOrDefault();
                    var systemManualAssetPriceHistory = systemManualAsset.ASXSecurity.Where(manualHIS => manualHIS.Date.Date <= endDate.Date).OrderByDescending(manualHis => manualHis.Date).FirstOrDefault();
                    if (systemManualAssetPriceHistory != null)
                        closingBalance += manualAsset.UnitsOnHand * Convert.ToDecimal(systemManualAssetPriceHistory.UnitPrice);
                }

                capitalMovementByCategoryTotal.OpeningBalTotal += openingBalance;
                capitalMovementByCategoryTotal.ClosingBalanceTotal += closingBalance;

                var filteredTransactionsByMonth = ClientManualAssetCollection.Where(man => man.TransactionDate.Date >= startDate.Date && man.TransactionDate <= endDate.Date);


                foreach (ClientManualAssetEntity clientManualAssetEntity in filteredTransactionsByMonth)
                {
                    var systemManualAsset = orgCM.ManualAsset.Where(manList => manList.ID == clientManualAssetEntity.InvestmentCode).FirstOrDefault();
                    var systemManualAssetPriceHistory = systemManualAsset.ASXSecurity.Where(manualHIS => manualHIS.Date.Date <= clientManualAssetEntity.TransactionDate.Date).OrderByDescending(manualHis => manualHis.Date).FirstOrDefault();
                    if (systemManualAssetPriceHistory != null)
                    {
                        if (clientManualAssetEntity.TransactionType == "Transfer In")
                        {
                            capitalMovementByCategoryTotal.TransferInOutTotal += clientManualAssetEntity.UnitsOnHand * Convert.ToDecimal(systemManualAssetPriceHistory.UnitPrice);
                            capitalReportDS.AddTranBreakdownRow(clientManualAssetEntity.ID, "MANUAL ASSETS", systemManualAsset.AsxCode, "", clientManualAssetEntity.TransactionType, "Transfer In/Out", clientManualAssetEntity.TransactionDate, clientManualAssetEntity.UnitsOnHand * Convert.ToDecimal(systemManualAssetPriceHistory.UnitPrice));
                        }

                        if (clientManualAssetEntity.TransactionType == "Transfer Out")
                        {
                            capitalMovementByCategoryTotal.TransferInOutTotal += -1 * (clientManualAssetEntity.UnitsOnHand * Convert.ToDecimal(systemManualAssetPriceHistory.UnitPrice));
                            capitalReportDS.AddTranBreakdownRow(clientManualAssetEntity.ID, "MANUAL ASSETS", systemManualAsset.AsxCode, "", clientManualAssetEntity.TransactionType, "Transfer In/Out", clientManualAssetEntity.TransactionDate, clientManualAssetEntity.UnitsOnHand * Convert.ToDecimal(systemManualAssetPriceHistory.UnitPrice));
                        }

                        if (clientManualAssetEntity.TransactionType == "Application")
                        {
                            capitalMovementByCategoryTotal.Application += (clientManualAssetEntity.UnitsOnHand * Convert.ToDecimal(systemManualAssetPriceHistory.UnitPrice));
                            capitalReportDS.AddTranBreakdownRow(clientManualAssetEntity.ID, "MANUAL ASSETS", systemManualAsset.AsxCode, "", clientManualAssetEntity.TransactionType, "Investment", clientManualAssetEntity.TransactionDate, clientManualAssetEntity.UnitsOnHand * Convert.ToDecimal(systemManualAssetPriceHistory.UnitPrice));
                        }

                        if (clientManualAssetEntity.TransactionType == "Redemption")
                        {
                            capitalMovementByCategoryTotal.Redemption += -1 * (clientManualAssetEntity.UnitsOnHand * Convert.ToDecimal(systemManualAssetPriceHistory.UnitPrice));
                            capitalReportDS.AddTranBreakdownRow(clientManualAssetEntity.ID, "MANUAL ASSETS", systemManualAsset.AsxCode, "", clientManualAssetEntity.TransactionType, "Investment", clientManualAssetEntity.TransactionDate, clientManualAssetEntity.UnitsOnHand * Convert.ToDecimal(systemManualAssetPriceHistory.UnitPrice));
                        }
                    }
                }
            }
        }


        static public void GetClientCashFlowReportPerAccount(CashFlowReportDS ds, string clientName, string clientID, Guid clientCid,string clientType, IdentityCM parent, List<AccountProcessTaskEntity> accountProcess, ICMBroker broker)
        {
            var org = broker.GetWellKnownCM(WellKnownCM.Organization) as IOrganization;
            if (org != null)
            {
                Guid advisorCid=Guid.Empty;
                string advisorName=string.Empty;
                string advisorID=string.Empty;
                if (parent != null)
                {

                    advisorCid = parent.Cid;
                    var logical = broker.GetCMImplementation(parent.Clid,parent.Csid) as IOrganizationUnit;
                    if (logical != null)
                    {
                        advisorName = logical.Name;
                        advisorID = logical.ClientId;
                        broker.ReleaseBrokerManagedComponent(logical);
                    }
                }

                foreach (var taskEntity in accountProcess)
                {

                    var model = org.Model.FirstOrDefault(ss => ss.ID == taskEntity.ModelID);
                    string serviceType = string.Empty;
                    if (model != null)
                    {
                        serviceType = model.ServiceType.ToString().ToUpper();
                    }
                    switch (taskEntity.LinkedEntityType)
                    {
                        case OrganizationType.BankAccount:
                            var bankcm = broker.GetCMImplementation(taskEntity.LinkedEntity.Clid, taskEntity.LinkedEntity.Csid) as BankAccountCM;
                            if (bankcm != null)
                            {
                                AddBankAccountRecodInReport(ds,bankcm.BankAccountEntity, clientCid, clientID, clientName, clientType, advisorCid, advisorID, advisorName, serviceType);

                                broker.ReleaseBrokerManagedComponent(bankcm);
                            }
                            break;

                        case OrganizationType.DesktopBrokerAccount:
                            var asxcm = broker.GetCMImplementation(taskEntity.LinkedEntity.Clid, taskEntity.LinkedEntity.Csid) as DesktopBrokerAccountCM;
                            if (asxcm != null)
                            {
                                AddASXAccountRecodInReport(ds, asxcm.DesktopBrokerAccountEntity, clientCid, clientID, clientName, clientType, advisorCid, advisorID, advisorName, serviceType);
                                broker.ReleaseBrokerManagedComponent(asxcm);
                            }

                            break;

                    }
                }

                broker.ReleaseBrokerManagedComponent(org);
            }
        }

        private static void AddASXAccountRecodInReport(CashFlowReportDS ds, DesktopBrokerAccountEntity desktopBrokerAccountEntity, Guid clientCid, string clientID, string clientName, string clientType, Guid advisorCid, string advisorID, string advisorName, string serviceType)
        {
            if (desktopBrokerAccountEntity != null && desktopBrokerAccountEntity.holdingTransactions != null)
            {

                var periodTransactions = desktopBrokerAccountEntity.holdingTransactions.Where(ss => ss.TradeDate.Date >= ds.StartDate.Date && ss.TradeDate.Date <= ds.EndDate.Date);
                var monthlyTransGroups = periodTransactions.GroupBy(ss => new { sTradeDate = new DateTime(ss.TradeDate.Year, ss.TradeDate.Month, 1), eTradeDate = new DateTime(ss.TradeDate.Year, ss.TradeDate.Month, 1).AddMonths(1).AddDays(-1), MonthYear = ss.TradeDate.ToString("MMM-yyyy") });


                foreach (var transGroup in monthlyTransGroups)
                {
                    string reinvestOrDistributeOption = string.Empty;
                    decimal openingBalance = desktopBrokerAccountEntity.holdingTransactions.Where(ss => ss.TradeDate.Date < transGroup.Key.sTradeDate.Date).Sum(ss => ss.GrossValue);
                    decimal transferIn = transGroup.Where(ss => ss.TransactionType == "Transfer In").Sum(ss => ss.GrossValue);
                    decimal transferOut = transGroup.Where(ss => ss.TransactionType == "Transfer Out").Sum(ss => ss.GrossValue);
                    decimal income = 0;
                    decimal investments = transGroup.Where(ss => new[] { "Application", "Buy / Purchase", "Purchase Reinvestment", "Rights Issue", "Sale" }.Contains(ss.TransactionType)).Sum(ss => ss.GrossValue);
                    decimal feesTaxes = 0;
                    decimal internalCashMovement = 0;
                    decimal closingBalance = desktopBrokerAccountEntity.holdingTransactions.Where(ss => ss.TradeDate.Date <= transGroup.Key.eTradeDate.Date).Sum(ss => ss.GrossValue);
                    ds.CashFlowReportTable.AddRow(transGroup.Key.sTradeDate, transGroup.Key.MonthYear, clientName, clientID, clientCid, clientType, advisorID, advisorName, advisorCid, serviceType, reinvestOrDistributeOption, "ASX Account", desktopBrokerAccountEntity.Name, openingBalance, transferIn, transferOut, income, investments, feesTaxes, internalCashMovement,  closingBalance);
                }
            }
        }

        private static void AddBankAccountRecodInReport(CashFlowReportDS ds, BankAccountEntity bankAccountEntity, Guid clientCid, string clientID, string clientName,string clientType, Guid advisorCid, string advisorID, string advisorName, string serviceType)
        {

            if (bankAccountEntity != null && bankAccountEntity.CashManagementTransactions != null)
            {
                var periodTransactions=bankAccountEntity.CashManagementTransactions.Where(ss => ss.TransactionDate.Date >= ds.StartDate.Date && ss.TransactionDate.Date <= ds.EndDate.Date);
                var monthlyTransGroups=periodTransactions.GroupBy(ss => new { sTradeDate = new DateTime(ss.TransactionDate.Year, ss.TransactionDate.Month, 1),eTradeDate = new DateTime(ss.TransactionDate.Year, ss.TransactionDate.Month, 1).AddMonths(1).AddDays(-1), MonthYear = ss.TransactionDate.ToString("MMM-yyyy") });


                foreach (var transGroup in monthlyTransGroups)
                {

                    string reinvestOrDistributeOption=string.Empty;
                    decimal openingBalance = bankAccountEntity.CashManagementTransactions.Where(ss => ss.TransactionDate.Date < transGroup.Key.sTradeDate.Date).Sum(ss => ss.TotalAmount);
                    decimal income = transGroup.Where(ss => ss.Category == "Income" && new[] {"Commission Rebate","Distribution","Dividend","Interest","Rental Income"}.Contains(ss.SystemTransactionType)).Sum(ss => ss.TotalAmount);
                    decimal investments = transGroup.Where(ss => ss.Category == "Investment" && new[] { "Application", "Redemption" }.Contains(ss.SystemTransactionType)).Sum(ss => ss.TotalAmount);
                    decimal feesTaxes = transGroup.Where(ss => 
                                    (ss.Category == "TAX" && new[] { "Business Activity Statement","Income Tax","Instalment Activity Statement","PAYG","Tax Refund", }.Contains(ss.SystemTransactionType)) ||
                                    (ss.Category == "Expense" && new[] { "Accounting Expense","Administration Fee","Advisory Fee","Insurance Premium","Investment Fee","Legal Expense","Regulatory Fee" }.Contains(ss.SystemTransactionType))
                                    ).Sum(ss => ss.TotalAmount);
                    decimal internalCashMovement = transGroup.Where(ss => 
                                                    (ss.Category == "Income" && ss.SystemTransactionType == "Internal Cash Movement")||
                                                    (ss.Category == "Expense" && ss.SystemTransactionType == "Internal Cash Movement")
                                                     ).Sum(ss => ss.TotalAmount);
                    decimal transferIn = transGroup.Where(ss => 
                                         (ss.Category == "Income" && ss.SystemTransactionType == "Transfer In" )||
                                         (ss.Category == "Contribution" && new[] { "Employer Additional", "Employer SG", "Personal", "Salary Sacrifice", "Spouse" }.Contains(ss.SystemTransactionType))
                        ).Sum(ss => ss.TotalAmount);
                    decimal transferOut = transGroup.Where(ss => (ss.Category == "Expense" && new[] { "Pension Payment", "Transfer Out", "Withdrawal" }.Contains(ss.SystemTransactionType))).Sum(ss => ss.TotalAmount);
                    decimal closingBalance = bankAccountEntity.CashManagementTransactions.Where(ss => ss.TransactionDate.Date <= transGroup.Key.eTradeDate.Date).Sum(ss => ss.TotalAmount);
                    ds.CashFlowReportTable.AddRow(transGroup.Key.sTradeDate, transGroup.Key.MonthYear, clientName, clientID, clientCid, clientType, advisorID, advisorName, advisorCid, serviceType, reinvestOrDistributeOption, "Bank Account", bankAccountEntity.Name, openingBalance, transferIn, transferOut, income, investments, feesTaxes, internalCashMovement,  closingBalance);
                }
            }
        }
    }
}