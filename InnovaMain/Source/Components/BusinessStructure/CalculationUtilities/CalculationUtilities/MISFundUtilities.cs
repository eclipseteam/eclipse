﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.CM.Entity;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Utilities;
using IdentityCMDetail = Oritax.TaxSimp.Common.IdentityCMDetail;
using Oritax.TaxSimp.DataSets;

namespace Oritax.TaxSimp.CM
{
    public class MISFundUtilities
    {
        public static void FillDS(ManagedInvestmentAccountDS ds, List<IdentityCMDetail> MISAccounts, ICMBroker Broker)
        {

            if (MISAccounts != null && MISAccounts.Count > 0)
            {

                foreach (var fund in MISAccounts)
                {
                    if (fund.Csid != Guid.Empty)
                    {
                        var entity = Broker.GetCMImplementation(fund.Clid, fund.Csid);
                        ds.FundID = fund.FundID;
                        entity.GetData(ds);
                        var rows=ds.FundAccountsTable.Select(string.Format("{0}='{1}'",ds.FundAccountsTable.FUNDID,fund.FundID));
                        foreach (DataRow row in rows)
                        {
                            row[ds.FundAccountsTable.REINVESTMENTOPTION] = fund.ReinvestmentOption;
                        }
                    }
                }

            }

        }


        public static void AssociateAccount(ManagedInvestmentAccountDS ds, List<IdentityCMDetail> misAccounts)
        {
            if (ds.Tables[ds.FundAccountsTable.TABLENAME].Rows.Count > 0)
            {
                var clid = (Guid)ds.Tables[ds.FundAccountsTable.TABLENAME].Rows[0][ds.FundAccountsTable.CLID];
                var csid = (Guid)ds.Tables[ds.FundAccountsTable.TABLENAME].Rows[0][ds.FundAccountsTable.CSID];
                var fundid= (Guid)ds.Tables[ds.FundAccountsTable.TABLENAME].Rows[0][ds.FundAccountsTable.FUNDID];
                var reinvestOption = ds.Tables[ds.FundAccountsTable.TABLENAME].Rows[0][ds.FundAccountsTable.REINVESTMENTOPTION].ToString();
                var misAccount = misAccounts.FirstOrDefault(ss => ss.Clid == (Guid)ds.Tables[ds.FundAccountsTable.TABLENAME].Rows[0][ds.FundAccountsTable.CLID] 
                    && ss.Csid == (Guid)ds.Tables[ds.FundAccountsTable.TABLENAME].Rows[0][ds.FundAccountsTable.CSID]
                    && ss.FundID == (Guid)ds.Tables[ds.FundAccountsTable.TABLENAME].Rows[0][ds.FundAccountsTable.FUNDID]);
                ReinvestmentOption reinvest = ReinvestmentOption.Reinvest;
                System.Enum.TryParse(reinvestOption, out reinvest);
                if (misAccount == null)
                {
                    if (misAccounts == null)
                    {
                        misAccounts = new List<IdentityCMDetail>();
                    }
                   

                    misAccounts.Add(new IdentityCMDetail { Clid = clid, Csid = csid, FundID = fundid, ReinvestmentOption = reinvest });

                }
                else
                {
                    misAccount.ReinvestmentOption = reinvest;
                }
            }
        }

        public static void RemoveAccount(ManagedInvestmentAccountDS ds, List<IdentityCMDetail> misAccounts, List<AccountProcessTaskEntity> accountProcess, ICMBroker broker)
        {
            if (ds.Tables[ds.FundAccountsTable.TABLENAME].Rows.Count > 0)
            {
                var misAccount = misAccounts.FirstOrDefault(ss => ss.Clid == (Guid)ds.Tables[ds.FundAccountsTable.TABLENAME].Rows[0][ds.FundAccountsTable.CLID]
                    && ss.Csid == (Guid)ds.Tables[ds.FundAccountsTable.TABLENAME].Rows[0][ds.FundAccountsTable.CSID]
                    && ss.FundID == (Guid)ds.Tables[ds.FundAccountsTable.TABLENAME].Rows[0][ds.FundAccountsTable.FUNDID]);

                if (misAccount != null)
                {
                    if (!ClientUtilities.IsAttachToAccountProcess(misAccount, accountProcess, OrganizationType.ManagedInvestmentSchemesAccount, broker))
                    {
                        misAccounts.Remove(misAccount);
                        ds.ExtendedProperties.Add("Message", "Account has been deleted successfully");
                    }
                    else
                    {
                        ds.ExtendedProperties.Add("Message", "This account is associated with the current client. First remove the association from Account Process then try again.");
                    }
                }
            }
        }

    }


}
