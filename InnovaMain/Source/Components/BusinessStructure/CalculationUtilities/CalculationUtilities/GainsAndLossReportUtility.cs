﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.DataSets;
using System.Data;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.CM.Group;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CM.Entity;
using System.Collections.ObjectModel;
using Oritax.TaxSimp.CM.OrganizationUnit;

namespace Oritax.TaxSimp.CM
{
    public class GainsAndLossReportUtility
    {
        public void OnGetDataLossAndGainsReportDSCorporate(IOrganizationUnit orgUnit, LossAndGainsReportDS data, string clientType, string clientID, string clientName, IdentityCM Parent, ICMBroker Broker, CorporateEntity Entity)
        {
            DataTable clientSummaryTable = data.Tables[SecuritySummaryReportDS.CLIENTSUMMARYTABLE];
            DataTable contactTable = data.Tables[SecuritySummaryReportDS.CONTACTTABLE];

            DataRow clientSummaryRow = clientSummaryTable.NewRow();
            DataRow contactRow = contactTable.NewRow();

            BasicClientDetails(clientType, clientID, clientName, Parent, Broker, Entity.ABN, Entity.ACN, Entity.Address, clientSummaryRow);

            CSVInstance csvinstance = new CSVInstance(Broker);
            CorporateCSV _entitycsv = csvinstance.GetEntityConcreteInstance(Entity);
            foreach (var contact in _entitycsv.Contacts)
            {
                contactRow[SecuritySummaryReportDS.CONTACT_TITLE] = contact.PersonalTitle;
                contactRow[SecuritySummaryReportDS.CONTACT_NAME] = contact.Fullname;
                contactRow[SecuritySummaryReportDS.CONTACT_BIZNO] = contact.WorkPhoneNumber;
                contactRow[SecuritySummaryReportDS.CONTACT_MOBNO] = contact.MobilePhoneNumber;
                contactRow[SecuritySummaryReportDS.CONTACT_HNO] = contact.HomePhoneNumber;
                contactRow[SecuritySummaryReportDS.CONTACT_FAXNO] = contact.Facsimile;
                contactRow[SecuritySummaryReportDS.CONTACT_EMAIL] = contact.EmailAddress;
            }

            clientSummaryTable.Rows.Add(clientSummaryRow);
            contactTable.Rows.Add(contactRow);

            IOrganization orgCM = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            LossAndGainsReportDS securitySummaryReportDS = data as LossAndGainsReportDS;

            ManualTransactions(clientID, Entity.ClientManualAssetCollection, orgCM, securitySummaryReportDS);
            DistributionIncomes(Entity.DistributionIncomes, securitySummaryReportDS, Entity.ListAccountProcess, Broker, orgCM);
            IEnumerable<DividendEntity> accrualDivEntities = DividendIncomes(Entity.DividendCollection, securitySummaryReportDS, orgCM, Entity.ListAccountProcess, Broker);
            GetTransactionsInfoFromLinkProcess(orgUnit, accrualDivEntities, clientID, Broker, Entity.ListAccountProcess, orgCM, securitySummaryReportDS, Entity.DesktopBrokerAccounts);
            SetBankTransactionDetails(data, Entity.DistributionIncomes, "Income", "Distribution");
            Broker.ReleaseBrokerManagedComponent(orgCM);
        }

        private static void DistributionIncomes(ObservableCollection<DistributionIncomeEntity> DistributionIncomes, LossAndGainsReportDS lossAndGainsReportDS, List<AccountProcessTaskEntity> ListAccountProcess, ICMBroker Broker, IOrganization orgCM)
        {
            DataTable incomeBreakDownTable = lossAndGainsReportDS.Tables[LossAndGainsReportDS.INCOMEBREAKDOWN];

            IEnumerable<DistributionIncomeEntity> filteredDistribution = null;

            if (lossAndGainsReportDS.AcountingMethod == AcountingMethodType.Cash)
                filteredDistribution = DistributionIncomes.Where(cliFundTrans => cliFundTrans.PaymentDate.Value.Date >= lossAndGainsReportDS.StartDate.Date && cliFundTrans.PaymentDate.Value.Date <= lossAndGainsReportDS.EndDate.Date);
            else
                filteredDistribution = DistributionIncomes.Where(cliFundTrans => cliFundTrans.RecordDate.Value.Date >= lossAndGainsReportDS.StartDate.Date && cliFundTrans.RecordDate.Value.Date <= lossAndGainsReportDS.EndDate.Date);

            foreach (DistributionIncomeEntity distributionIncomeEntity in filteredDistribution)
            {
                if (distributionIncomeEntity.NetCashDistribution != 0)
                {
                    DataRow incomeRow = incomeBreakDownTable.NewRow();
                    incomeRow[LossAndGainsReportDS.DESCRIPTION] = distributionIncomeEntity.FundName;
                    incomeRow[LossAndGainsReportDS.SECNAME] = distributionIncomeEntity.FundCode;
                    incomeRow[LossAndGainsReportDS.SECTYPE] = "FUND";
                    incomeRow[LossAndGainsReportDS.SOURCE] = "FUND";
                    incomeRow[LossAndGainsReportDS.INCOMETYPE] = "DISTRIBUTION";
                    incomeRow[LossAndGainsReportDS.NETDPU] = distributionIncomeEntity.NetDistributionDPU;
                    incomeRow[LossAndGainsReportDS.RECORDDATE] = distributionIncomeEntity.RecordDate;
                    incomeRow[LossAndGainsReportDS.PAYMENTDATE] = distributionIncomeEntity.PaymentDate;
                    incomeRow[LossAndGainsReportDS.INCPAID] = distributionIncomeEntity.NetCashDistribution;
                    incomeRow[LossAndGainsReportDS.UNITSHELD] = distributionIncomeEntity.RecodDate_Shares;
                    incomeRow[LossAndGainsReportDS.FRANKINGCREDIT] = distributionIncomeEntity.Components.Where(comp => comp.Category == ComponentCategory.Australian_Income).Sum(comp => comp.Autax_Credit);
                    incomeBreakDownTable.Rows.Add(incomeRow);
                }
            }
        }

        private static IEnumerable<DividendEntity> DividendIncomes(ObservableCollection<DividendEntity> DividendCollection, LossAndGainsReportDS lossAndGainsReportDS, IOrganization orgCM, List<AccountProcessTaskEntity> ListAccountProcess, ICMBroker Broker)
        {
            IEnumerable<DividendEntity> DividendCollectionAccrualCheck = DividendCollection.Where(divCol => divCol.RecordDate.Value < lossAndGainsReportDS.StartDate.Date || divCol.RecordDate.Value > lossAndGainsReportDS.EndDate.Date
                                                                            && (divCol.PaymentDate.Value >= lossAndGainsReportDS.StartDate.Date && divCol.PaymentDate.Value <= lossAndGainsReportDS.EndDate.Date));

            DataTable incomeBreakDownTable = lossAndGainsReportDS.Tables[LossAndGainsReportDS.INCOMEBREAKDOWN];

            IEnumerable<DividendEntity> filteredDividends = null;

            if (lossAndGainsReportDS.AcountingMethod == AcountingMethodType.Cash || lossAndGainsReportDS.AcountingMethod == AcountingMethodType.Mixed)
                filteredDividends = DividendCollection.Where(d => d.InvestmentCode.ToLower() != "cash").Where(cliFundTrans => cliFundTrans.PaymentDate.Value.Date >= lossAndGainsReportDS.StartDate && cliFundTrans.PaymentDate.Value.Date <= lossAndGainsReportDS.EndDate.Date);
            else
                filteredDividends = DividendCollection.Where(d => d.InvestmentCode.ToLower() != "cash").Where(cliFundTrans => cliFundTrans.RecordDate.Value.Date >= lossAndGainsReportDS.StartDate && cliFundTrans.RecordDate.Value.Date <= lossAndGainsReportDS.EndDate.Date);

            foreach (DividendEntity dividendEntity in filteredDividends)
            {
                string investmentName = dividendEntity.InvestmentCode;
                var investment = orgCM.Securities.Where(sec => sec.AsxCode == dividendEntity.InvestmentCode).FirstOrDefault();

                if (investment != null)
                    investmentName = investment.CompanyName;

                DataRow incomeRow = incomeBreakDownTable.NewRow();
                incomeRow[LossAndGainsReportDS.DESCRIPTION] = investmentName;
                incomeRow[LossAndGainsReportDS.SECNAME] = dividendEntity.InvestmentCode;
                incomeRow[LossAndGainsReportDS.SECTYPE] = "SEC";
                incomeRow[LossAndGainsReportDS.SOURCE] = "SEC";
                incomeRow[LossAndGainsReportDS.INCOMETYPE] = "DIVIDEND";
                incomeRow[LossAndGainsReportDS.DIVTYPE] = Enumeration.GetDescription(dividendEntity.Dividendtype).ToUpper();
                incomeRow[LossAndGainsReportDS.RECORDDATE] = dividendEntity.RecordDate;
                incomeRow[LossAndGainsReportDS.PAYMENTDATE] = dividendEntity.PaymentDate;
                incomeRow[LossAndGainsReportDS.INCPAID] = dividendEntity.PaidDividend;
                incomeRow[LossAndGainsReportDS.UNITSHELD] = dividendEntity.UnitsOnHand;
                incomeRow[LossAndGainsReportDS.DIVAMOUNTPERSHARE] = dividendEntity.CentsPerShare;
                incomeRow[LossAndGainsReportDS.FRANKEDAMOUNT] = dividendEntity.FrankedAmount;
                incomeRow[LossAndGainsReportDS.UNFRANKEDAMOUNT] = dividendEntity.UnfrankedAmount;
                incomeRow[LossAndGainsReportDS.FRANKINGCREDIT] = dividendEntity.TotalFrankingCredit;

                incomeBreakDownTable.Rows.Add(incomeRow);
            }

            return DividendCollectionAccrualCheck;
        }

        private static void BasicClientDetails(string clientType, string clientID, string clientName, IdentityCM Parent, ICMBroker Broker, string ABN, string ACN, Common.DualAddressEntity Address, DataRow clientSummaryRow)
        {
            clientSummaryRow[HoldingRptDataSet.CLIENTID] = clientID;
            clientSummaryRow[HoldingRptDataSet.CLIENTNAME] = clientName;
            clientSummaryRow[HoldingRptDataSet.CIENTABN] = ABN;
            clientSummaryRow[HoldingRptDataSet.CLIENTACN] = ACN;
            clientSummaryRow[HoldingRptDataSet.CLIENTTYPE] = clientType;

            if (Parent != null && Parent.Clid != Guid.Empty && Parent.Csid != Guid.Empty)
            {
                AdvisorCM adviserUnit = Broker.GetCMImplementation(Parent.Clid, Parent.Csid) as AdvisorCM;
                if (adviserUnit != null)
                    clientSummaryRow[HoldingRptDataSet.ADVISERFULLNAME] = adviserUnit.AdvisorEntity.FullName;
                Broker.ReleaseBrokerManagedComponent(adviserUnit);
            }

            if (Address.RegisteredAddress.Addressline1 != string.Empty)
            {
                clientSummaryRow[HoldingRptDataSet.ADDRESSLINE1] = Address.RegisteredAddress.Addressline1;
                clientSummaryRow[HoldingRptDataSet.ADDRESSLINE2] = Address.RegisteredAddress.Addressline2;
                clientSummaryRow[HoldingRptDataSet.SUBURB] = Address.RegisteredAddress.Suburb;
                clientSummaryRow[HoldingRptDataSet.STATE] = Address.RegisteredAddress.State;
                clientSummaryRow[HoldingRptDataSet.POSTCODE] = Address.RegisteredAddress.PostCode;
                clientSummaryRow[HoldingRptDataSet.COUNTRY] = Address.RegisteredAddress.Country;
            }
            else if (Address.BusinessAddress.Addressline1 != string.Empty)
            {
                clientSummaryRow[HoldingRptDataSet.ADDRESSLINE1] = Address.BusinessAddress.Addressline1;
                clientSummaryRow[HoldingRptDataSet.ADDRESSLINE2] = Address.BusinessAddress.Addressline2;
                clientSummaryRow[HoldingRptDataSet.SUBURB] = Address.BusinessAddress.Suburb;
                clientSummaryRow[HoldingRptDataSet.STATE] = Address.BusinessAddress.State;
                clientSummaryRow[HoldingRptDataSet.POSTCODE] = Address.BusinessAddress.PostCode;
                clientSummaryRow[HoldingRptDataSet.COUNTRY] = Address.BusinessAddress.Country;
            }
            else if (Address.MailingAddress.Addressline1 != string.Empty)
            {
                clientSummaryRow[HoldingRptDataSet.ADDRESSLINE1] = Address.MailingAddress.Addressline1;
                clientSummaryRow[HoldingRptDataSet.ADDRESSLINE2] = Address.MailingAddress.Addressline2;
                clientSummaryRow[HoldingRptDataSet.SUBURB] = Address.MailingAddress.Suburb;
                clientSummaryRow[HoldingRptDataSet.STATE] = Address.MailingAddress.State;
                clientSummaryRow[HoldingRptDataSet.POSTCODE] = Address.MailingAddress.PostCode;
                clientSummaryRow[HoldingRptDataSet.COUNTRY] = Address.MailingAddress.Country;
            }
            else if (Address.ResidentialAddress.Addressline1 != string.Empty)
            {
                clientSummaryRow[HoldingRptDataSet.ADDRESSLINE1] = Address.ResidentialAddress.Addressline1;
                clientSummaryRow[HoldingRptDataSet.ADDRESSLINE2] = Address.ResidentialAddress.Addressline2;
                clientSummaryRow[HoldingRptDataSet.SUBURB] = Address.ResidentialAddress.Suburb;
                clientSummaryRow[HoldingRptDataSet.STATE] = Address.ResidentialAddress.State;
                clientSummaryRow[HoldingRptDataSet.POSTCODE] = Address.ResidentialAddress.PostCode;
                clientSummaryRow[HoldingRptDataSet.COUNTRY] = Address.ResidentialAddress.Country;
            }

        }

        private static void GetTransactionsInfoFromLinkProcess(IOrganizationUnit orgUnit, IEnumerable<DividendEntity> accrualDivEntities, string clientID, ICMBroker Broker, List<AccountProcessTaskEntity> ListAccountProcess, IOrganization orgCM, LossAndGainsReportDS lossAndGainsReportDS, List<Oritax.TaxSimp.Common.IdentityCMDetail> DesktopBrokerAccounts)
        {
            if (ListAccountProcess != null)
            {
                IEnumerable<HoldingTransactions> TotalHoldingTransactions = new ObservableCollection<HoldingTransactions>();
                foreach (Oritax.TaxSimp.Common.IdentityCMDetail desktopBrokerAccounts in DesktopBrokerAccounts)
                {
                    DesktopBrokerAccountCM desktopBrokerAccountCM = Broker.GetCMImplementation(desktopBrokerAccounts.Clid,
                                                                        desktopBrokerAccounts.Csid) as DesktopBrokerAccountCM;
                    if (desktopBrokerAccountCM != null)
                        DesktopBrokerTransactions(orgUnit, desktopBrokerAccountCM.CID, orgCM, lossAndGainsReportDS, desktopBrokerAccountCM.DesktopBrokerAccountEntity);
                }

                foreach (var accountProcessTaskEntity in ListAccountProcess)
                {
                    if (accountProcessTaskEntity.LinkedEntity != null)
                    {
                        var asset = orgCM.Assets.Where(var => var.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                        var model = orgCM.Model.Where(mod => mod.ID == accountProcessTaskEntity.ModelID).FirstOrDefault();
                        var linkedAsset = model.Assets.Where(ast => ast.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                        if (linkedAsset != null)
                        {
                            var product = linkedAsset.Products.Where(pro => pro.ID == accountProcessTaskEntity.TaskID).FirstOrDefault();

                            if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.ManagedInvestmentSchemesAccount)
                                ManageInvestmentSchemes(clientID, Broker, orgCM, lossAndGainsReportDS, accountProcessTaskEntity, asset, model, product);
                            else if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.BankAccount)
                                BankAccountTransctions((orgUnit.ClientEntity as IClientUMAData).DistributionIncomes, accrualDivEntities, Broker, accountProcessTaskEntity, lossAndGainsReportDS, orgCM);
                            else if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.TermDepositAccount)
                                TDTransactions(Broker, orgCM, lossAndGainsReportDS, accountProcessTaskEntity);

                        }
                    }
                }
            }
        }

        private static void ManageInvestmentSchemes(string clientID, ICMBroker Broker, IOrganization orgCM, LossAndGainsReportDS lossAndGainsReportDS, AccountProcessTaskEntity accountProcessTaskEntity, AssetEntity asset, ModelEntity model, ProductEntity product)
        {
            decimal movement = 0;
            decimal totalRealised = 0;
            decimal totalUnrealised = 0;
            decimal totalClosing = 0;
            decimal totalOpening = 0;

            ManagedInvestmentSchemesAccountCM managedInvestmentSchemesAccountCM = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as ManagedInvestmentSchemesAccountCM;

            if (managedInvestmentSchemesAccountCM != null && product != null)
            {

                var linkedMISAccount = managedInvestmentSchemesAccountCM.ManagedInvestmentSchemesAccountEntity.FundAccounts.Where(fa => product.TaskDescription.Contains(fa.Code)).FirstOrDefault();

                var filteredHoldingTransactionsRange = linkedMISAccount.FundTransactions.Where(cliFundTrans => cliFundTrans.TradeDate >= lossAndGainsReportDS.StartDate && cliFundTrans.TradeDate <= lossAndGainsReportDS.EndDate);
                var clientFundTransactionsRange = filteredHoldingTransactionsRange.Where(cliFundTrans => cliFundTrans.ClientID == clientID);
                var clientFundTransactionRangeWithoutDate = linkedMISAccount.FundTransactions.Where(cliFundTrans => cliFundTrans.ClientID == clientID);

                decimal adjustmentUp = clientFundTransactionsRange.Where(ft => ft.TransactionType == "Adjustment Up").Sum(ft => ft.Amount);
                decimal adjustmentDown = clientFundTransactionsRange.Where(ft => ft.TransactionType == "Adjustment Down").Sum(ft => ft.Amount);

                decimal adjustmentUpUnits = clientFundTransactionsRange.Where(ft => ft.TransactionType == "Adjustment Up").Sum(ft => ft.Shares);
                decimal adjustmentDownUnits = clientFundTransactionsRange.Where(ft => ft.TransactionType == "Adjustment Down").Sum(ft => ft.Shares);

                var filteredHoldingTransactionsByClosing = linkedMISAccount.FundTransactions.Where(cliFundTrans => cliFundTrans.TradeDate <= lossAndGainsReportDS.EndDate);
                var clientFundTransactionsClosing = filteredHoldingTransactionsByClosing.Where(cliFundTrans => cliFundTrans.ClientID == clientID);
                decimal totalSharesClosing = clientFundTransactionsClosing.Sum(ft => ft.Shares);
                decimal adjustmentUpDownClosing = clientFundTransactionsClosing.Where(ft => ft.TransactionType == "Adjustment Up" || ft.TransactionType == "Adjustment Down").Sum(ft => ft.Shares);

                var filteredHoldingTransactionsByOpening = linkedMISAccount.FundTransactions.Where(cliFundTrans => cliFundTrans.TradeDate <= lossAndGainsReportDS.StartDate.AddDays(-1));
                var clientFundTransactionsOpening = filteredHoldingTransactionsByOpening.Where(cliFundTrans => cliFundTrans.ClientID == clientID);
                decimal totalSharesOpening = clientFundTransactionsOpening.Sum(ft => ft.Shares);
                decimal adjustmentUpDownOpening = clientFundTransactionsOpening.Where(ft => ft.TransactionType == "Adjustment Up" || ft.TransactionType == "Adjustment Down").Sum(ft => ft.Shares);

                totalOpening += clientFundTransactionsOpening.Sum(ft => ft.Amount);

                decimal soldUnits = clientFundTransactionsRange.Where(ft => ft.TransactionType == "Redemption").Sum(ft => ft.Shares) * -1;
                decimal soldUnitsTotal = clientFundTransactionRangeWithoutDate.Where(ft => ft.TransactionType == "Redemption").Sum(ft => ft.Shares) * -1;
                decimal saleProceeds = clientFundTransactionsRange.Where(ft => ft.TransactionType == "Redemption").Sum(ft => ft.Amount) * -1;

                decimal boughtUnits = clientFundTransactionsRange.Where(ft => ft.TransactionType == "Purchase").Sum(ft => ft.Shares);
                decimal boughtUnitsTotal = clientFundTransactionRangeWithoutDate.Where(ft => ft.TransactionType == "Purchase").Sum(ft => ft.Shares);
                decimal purchaseValue = clientFundTransactionsRange.Where(ft => ft.TransactionType == "Purchase").Sum(ft => ft.Amount);

                decimal totalSharesMovement = totalSharesClosing - totalSharesOpening;

                decimal buyPurchase = clientFundTransactionsRange.Where(tran => tran.TransactionType == "Purchase").Sum(tran => tran.Amount);
                buyPurchase += clientFundTransactionsRange.Where(tran => tran.TransactionType == "Application").Sum(tran => tran.Amount);
                decimal sell = clientFundTransactionsRange.Where(tran => tran.TransactionType == "Redemption").Sum(tran => tran.Amount);

                decimal unrealisedcostValue = 0;
                decimal unrealisedcostPrice = 0;
                decimal unrealisedProfitLoss = 0;

                var linkedmissec = orgCM.Securities.Where(missec => missec.AsxCode == linkedMISAccount.Code).FirstOrDefault();
                var buyPurchaseTrans = clientFundTransactionsRange.Where(tran => tran.TransactionType == "Purchase" || tran.TransactionType == "Application");
                var sellTrans = clientFundTransactionsRange.Where(tran => tran.TransactionType == "Redemption");
                SetMISTransactionDetails(lossAndGainsReportDS, linkedmissec, buyPurchaseTrans, "Purchase", "Investment");
                SetMISTransactionDetails(lossAndGainsReportDS, linkedmissec, sellTrans, "Redemption", "Investment");

                decimal currentValue = 0;
                decimal unitPrice = 0;
                DateTime priceDate = lossAndGainsReportDS.EndDate;
                decimal unRealisedfee = 0;
                if (linkedmissec != null)
                {
                    var latestMisPriceObject = linkedmissec.ASXSecurity.Where(mishis => mishis.Date <= lossAndGainsReportDS.EndDate).OrderByDescending(mishis => mishis.Date).FirstOrDefault();
                    if (latestMisPriceObject != null)
                    {
                        unitPrice = Convert.ToDecimal(latestMisPriceObject.UnitPrice);
                        priceDate = latestMisPriceObject.Date;
                    }
                    currentValue = totalSharesClosing * unitPrice;

                    currentValue += adjustmentUp;
                    currentValue += adjustmentDown;

                    decimal unsettledOrders = linkedMISAccount.UnsettledOrders.Where(fundTrans => fundTrans.ClientID == clientID).Sum(fundTrans => fundTrans.UnsettledOrders);
                    decimal total = currentValue - unsettledOrders;
                    if (currentValue != 0)
                    {
                        if (boughtUnits > soldUnits && soldUnits != 0)
                        {
                            unrealisedcostPrice = SetFifoLifoAvgCostPriceMIS_PR(orgCM, linkedMISAccount.Code, ref unRealisedfee, clientFundTransactionsClosing, totalSharesClosing, unrealisedcostValue, lossAndGainsReportDS.CostTypes);

                        }
                        else
                            unrealisedcostPrice = SetFifoLifoAvgCostPriceMIS(orgCM, linkedMISAccount.Code, ref unRealisedfee, clientFundTransactionsClosing, totalSharesClosing, unrealisedcostValue, lossAndGainsReportDS.CostTypes);
                        unrealisedcostValue = totalSharesClosing * unrealisedcostPrice;
                        unrealisedProfitLoss = CalculateUnrealisedProfitLoss(currentValue, unrealisedcostValue);
                    }

                    if (currentValue != 0)
                    {
                        //IV004 & IV014 have diff logic.
                        if (linkedMISAccount.Code == "IV004" || linkedMISAccount.Code == "IV014")
                        {
                            unrealisedcostValue = (totalSharesClosing - adjustmentUpUnits - adjustmentDownUnits) * unrealisedcostPrice;
                            currentValue = (totalSharesClosing) * unrealisedcostPrice;
                            unrealisedProfitLoss = CalculateUnrealisedProfitLoss(currentValue, unrealisedcostValue);
                            lossAndGainsReportDS.AddSecuritySummaryRowRealised(linkedMISAccount.Code, 0, unrealisedcostValue, linkedMISAccount.Description, totalSharesClosing - adjustmentUpDownClosing, unitPrice, currentValue, unrealisedcostPrice, unrealisedcostValue, unrealisedProfitLoss, linkedMISAccount.Code);
                            totalRealised += unrealisedProfitLoss;
                        }
                        else
                        {
                            lossAndGainsReportDS.AddSecuritySummaryRow(linkedMISAccount.Code, 0, unrealisedcostValue, linkedMISAccount.Description, totalSharesOpening, totalSharesMovement, totalSharesClosing, unitPrice, currentValue, unrealisedcostPrice, unrealisedcostValue, unrealisedProfitLoss, linkedMISAccount.Code);
                            totalUnrealised += unrealisedProfitLoss;
                        }
                    }

                    totalClosing += currentValue;

                    decimal realisedcostValue = 0;
                    decimal realisedcostPrice = 0;
                    decimal realisedProfitLoss = 0;
                    decimal realisedFee = 0;
                    var clientFundTransactionsClosingPUrchases = clientFundTransactionsClosing.Where(t => t.TransactionType == "Purchase").ToList();
                    realisedcostPrice = SetFifoLifoAvgCostPriceMIS(orgCM, linkedMISAccount.Code, ref realisedFee, clientFundTransactionsClosingPUrchases, soldUnits, realisedcostValue, lossAndGainsReportDS.CostTypes);
                    realisedcostValue = soldUnits * realisedcostPrice;
                    realisedProfitLoss = saleProceeds - realisedcostValue;

                    movement += saleProceeds;

                    if (saleProceeds != 0)
                    {
                        if (linkedMISAccount.Code != "IV004" && linkedMISAccount.Code != "IV014")
                        {
                            lossAndGainsReportDS.AddSecuritySummaryRowRealised(linkedMISAccount.Code, 0, realisedcostValue, linkedMISAccount.Description, soldUnits, unitPrice, saleProceeds, realisedcostPrice, realisedcostValue, realisedProfitLoss, linkedMISAccount.Code);
                            totalRealised += realisedProfitLoss;
                        }
                    }
                }

                if (linkedmissec != null)
                {
                    lossAndGainsReportDS.AddSecurityMainSummaryRow(linkedmissec.CompanyName, linkedmissec.AsxCode, totalRealised, totalUnrealised, "FUND");
                    lossAndGainsReportDS.AddTaxSummaryRow(linkedmissec.ID, "FUND", linkedmissec.CompanyName, linkedmissec.AsxCode, totalOpening, 0, 0, 0, 0, 0, 0, 0, 0, 0, totalRealised, totalUnrealised, totalClosing, (buyPurchase + sell), totalClosing, 0);
                }
            }
        }

        private static void DesktopBrokerTransactions(IOrganizationUnit orgUnit, Guid accountID, IOrganization orgCM, LossAndGainsReportDS lossAndGainsReportDS, DesktopBrokerAccountEntity desktopBrokerAccountEntity)
        {
            decimal totalRealised = 0;
            decimal totalUnrealised = 0;
            decimal totalOpening = 0;
            decimal totalClosing = 0;
            decimal movement = 0;
            decimal totalTransferIn = 0;
            decimal totalTransferOut = 0;


            var groupedSecurities = desktopBrokerAccountEntity.holdingTransactions.GroupBy(holdTran => holdTran.InvestmentCode);
            var overallTransactions = desktopBrokerAccountEntity.holdingTransactions.Where(holdTrans => holdTrans.TradeDate >= lossAndGainsReportDS.StartDate && holdTrans.TradeDate <= lossAndGainsReportDS.EndDate);
            if (overallTransactions.Count() > 0)
            {
                LossAndGainsReportDS prevLossGainsDS = new LossAndGainsReportDS();
                prevLossGainsDS.StartDate = lossAndGainsReportDS.StartDate.AddYears(-1);
                prevLossGainsDS.EndDate = lossAndGainsReportDS.EndDate.AddYears(-1);
                //orgUnit.GetData(prevLossGainsDS);

                foreach (var security in groupedSecurities)
                {
                    var holdingTrasactionRange = security.Where(holdTrans => holdTrans.TradeDate >= lossAndGainsReportDS.StartDate && holdTrans.TradeDate <= lossAndGainsReportDS.EndDate);
                    var holdingTrasactionRangeList = security.Where(holdTrans => holdTrans.TradeDate >= lossAndGainsReportDS.StartDate && holdTrans.TradeDate <= lossAndGainsReportDS.EndDate).ToList();
                    decimal totalSecHoldingUnitsClosing = security.Where(holdTrans => holdTrans.TradeDate <= lossAndGainsReportDS.EndDate).Sum(sec => sec.Units);
                    var totalSecHoldingUnitsOpeningList = security.Where(holdTrans => holdTrans.TradeDate <= lossAndGainsReportDS.StartDate.AddDays(-1));
                    var totalSecHoldingUnitsOpeningListCostBase = security.Where(holdTrans => holdTrans.TradeDate <= lossAndGainsReportDS.StartDate.AddDays(-1) && holdTrans.TradeDate >= lossAndGainsReportDS.StartDate.AddYears(-1)).ToList();
                    var asxBroker = orgCM.Securities.Where(sec => sec.AsxCode == security.Key).FirstOrDefault();
                    decimal totalSecHoldingUnitsOpening = security.Where(holdTrans => holdTrans.TradeDate <= lossAndGainsReportDS.StartDate.AddDays(-1)).Sum(sec => sec.Units);
                    decimal soldUnitsLastYear = security.Where(holdTrans => holdTrans.TradeDate <= lossAndGainsReportDS.StartDate.AddDays(-1) && holdTrans.TradeDate >= lossAndGainsReportDS.StartDate.AddYears(-1)).Where(holdTrans => holdTrans.TransactionType == "Sale").Sum(sec => sec.Units);

                    decimal costBaseCarriedForward = 0;

                    var costBaseForAT = totalSecHoldingUnitsOpeningListCostBase.Where(t => t.Type == "AT");
                    if (costBaseForAT != null && costBaseForAT.Count() > 0)
                    {
                        foreach (var tran in costBaseForAT)
                        {
                            decimal adjustCostBase = 0;

                            if (tran.AdjustmentTransactions != null && tran.AdjustmentTransactions.Count > 0)
                            {
                                adjustCostBase -= tran.AdjustmentTransactions.Where(t => t.TransactionType == "Buy / Purchase").Sum(t => t.NetValue);
                                adjustCostBase += tran.AdjustmentTransactions.Where(t => t.TransactionType == "Sale").Sum(t => t.NetValue);
                            }
                            else
                            {
                                var secPrice = asxBroker.ASXSecurity.Where(sec => sec.Date.Date <= tran.TradeDate.Date).OrderByDescending(t => t.Date.Date).FirstOrDefault();
                                if (secPrice != null)
                                    adjustCostBase = Convert.ToDecimal(secPrice.UnitPrice) * tran.Units;
                            }

                            costBaseCarriedForward += adjustCostBase;
                        }
                    }

                    foreach (var item in totalSecHoldingUnitsOpeningListCostBase)
                        item.AdjustedUnits = item.Units;

                    decimal realisedFeeLastYr = 0;
                    decimal realisedcostPriceLastYr = 0;
                    decimal realisedTradeLastYr = 0;
                    decimal realisedcostValueLastYr = 0;

                    realisedcostPriceLastYr = SetFifoLifoAvgCostPrice(security.Key, orgCM, ref realisedFeeLastYr, ref totalSecHoldingUnitsOpeningListCostBase, Math.Abs(soldUnitsLastYear), realisedcostValueLastYr, lossAndGainsReportDS.CostTypes);
                    realisedTradeLastYr = Math.Abs(soldUnitsLastYear) * realisedcostPriceLastYr;
                    realisedcostValueLastYr = realisedTradeLastYr + realisedFeeLastYr;

                    decimal UnrealisedFeeLastYr = 0;
                    decimal UnrealisedcostPriceLastYr = 0;
                    decimal UnrealisedTradeLastYr = 0;
                    decimal UnrealisedcostValueLastYr = 0;
                    decimal UnrealisedProfitLossYr = 0;

                    var hisSecurityLastYear = asxBroker.ASXSecurity.Where(secpri => secpri.Date <= lossAndGainsReportDS.StartDate.AddDays(-1)).OrderByDescending(secpri => secpri.Date).FirstOrDefault();
                    decimal unitPriceLastYear = 0;

                    if (hisSecurityLastYear != null)
                        unitPriceLastYear = Convert.ToDecimal(hisSecurityLastYear.UnitPrice);

                    decimal currentValueLastYear = totalSecHoldingUnitsClosing * unitPriceLastYear;

                    decimal soldUnits = 0;
                    decimal saleProceeds = 0;

                    foreach (HoldingTransactions transaction in holdingTrasactionRange)
                    {
                        if (transaction.Type == "RA")
                        {
                            decimal units = 0;

                            if (transaction.Units < 0)
                                units = transaction.Units * -1;
                            else
                                units = transaction.Units;

                            soldUnits += units;
                        }
                        else if (transaction.Type == "VA")
                            soldUnits -= transaction.Units;
                        else if (transaction.Type == "AJ")
                            soldUnits += Math.Abs(transaction.Units);
                        else if (transaction.Type == "AT" && transaction.AdjustmentTransactions != null && transaction.AdjustmentTransactions.Count > 0)
                        {
                            foreach (AdjustmentTransaction adjAransaction in transaction.AdjustmentTransactions)
                            {
                                if (adjAransaction.Type == "RA")
                                {
                                    decimal units = 0;

                                    if (adjAransaction.Units < 0)
                                        units = adjAransaction.Units * -1;
                                    else
                                        units = adjAransaction.Units;

                                    soldUnits += units;
                                    saleProceeds += adjAransaction.NetValue;
                                }
                            }
                        }
                    }

                    decimal reaminingUnitsPrevYear = totalSecHoldingUnitsOpening;

                    if (soldUnits == 0)
                        reaminingUnitsPrevYear = totalSecHoldingUnitsOpening;
                    else if (reaminingUnitsPrevYear > soldUnits)
                        reaminingUnitsPrevYear = soldUnits;


                    UnrealisedcostPriceLastYr = SetFifoLifoAvgCostPrice(security.Key, orgCM, ref UnrealisedFeeLastYr, ref totalSecHoldingUnitsOpeningListCostBase, reaminingUnitsPrevYear, UnrealisedcostValueLastYr, lossAndGainsReportDS.CostTypes);
                    UnrealisedTradeLastYr = UnrealisedcostPriceLastYr * reaminingUnitsPrevYear;
                    UnrealisedcostValueLastYr = UnrealisedTradeLastYr + UnrealisedFeeLastYr;
                    UnrealisedProfitLossYr = CalculateUnrealisedProfitLoss(currentValueLastYear, UnrealisedcostValueLastYr);

                    decimal prevYearCostBaseValueWTATRA = totalSecHoldingUnitsOpeningListCostBase.Where(t => t.TransactionType != "Sale" && t.Type != "AT").Sum(t => t.NetValue);

                    var prevYearSecCostBase = prevLossGainsDS.Tables[LossAndGainsReportDS.SECURITYSUMMARYTABLEREALISED].Select().Where(r => r[LossAndGainsReportDS.SECNAME].ToString() == security.Key).FirstOrDefault();
                    if (prevYearSecCostBase != null)
                        costBaseCarriedForward += (decimal)prevYearSecCostBase[LossAndGainsReportDS.REALISEDCOSTVALUE];

                    costBaseCarriedForward = UnrealisedcostValueLastYr;
                    if (totalSecHoldingUnitsOpening == 0)
                        costBaseCarriedForward = 0;

                    saleProceeds += holdingTrasactionRange.Where(tran => tran.Type == "RA").Sum(tran => tran.NetValue);
                    saleProceeds += holdingTrasactionRange.Where(tran => tran.Type == "VA").Sum(tran => tran.NetValue);
                    saleProceeds += Math.Abs(holdingTrasactionRange.Where(tran => tran.Type == "AJ").Sum(tran => tran.NetValue));

                    decimal totalSecHoldingUnitsOpeningValue = 0;

                    var totalSecHoldingUnitsOpeningTransactions = holdingTrasactionRange.Where(holdTrans => holdTrans.TradeDate <= lossAndGainsReportDS.StartDate.AddDays(-1));

                    decimal totalUnits = totalSecHoldingUnitsOpeningTransactions.Sum(trans => trans.Units);
                    var openingSecurityPriceObj = asxBroker.ASXSecurity.Where(secpri => secpri.Date <= lossAndGainsReportDS.StartDate.AddDays(-1)).OrderByDescending(secpri => secpri.Date).FirstOrDefault();

                    if (openingSecurityPriceObj != null)
                        totalSecHoldingUnitsOpeningValue = totalUnits * Convert.ToDecimal(openingSecurityPriceObj.UnitPrice);

                    totalOpening += totalSecHoldingUnitsOpeningValue;

                    decimal totalSecHoldingUnitsMovement = totalSecHoldingUnitsClosing - totalSecHoldingUnitsOpening;
                    var hisSecurity = asxBroker.ASXSecurity.Where(secpri => secpri.Date <= lossAndGainsReportDS.EndDate).OrderByDescending(secpri => secpri.Date).FirstOrDefault();
                    decimal unitPrice = 0;

                    if (hisSecurity != null)
                        unitPrice = Convert.ToDecimal(hisSecurity.UnitPrice);

                    decimal currentValue = totalSecHoldingUnitsClosing * unitPrice;
                    decimal unrealisedcostValue = 0;
                    decimal unrealisedcostPrice = 0;
                    decimal unrealisedProfitLoss = 0;
                    decimal unRealisedfee = 0;
                    decimal unRealisedTrade = 0;

                    decimal realisedcostValue = 0;
                    decimal realisedcostPrice = 0;
                    decimal realisedProfitLoss = 0;
                    decimal realisedFee = 0;
                    decimal realisedTrade = 0;

                    foreach (var item in holdingTrasactionRangeList)
                        item.AdjustedUnits = item.Units;

                    decimal remainingUnits = soldUnits;

                    if (totalSecHoldingUnitsOpening != 0)
                        remainingUnits -= reaminingUnitsPrevYear;

                    if (soldUnitsLastYear == 0)
                    {
                        realisedFeeLastYr = 0;
                        realisedcostPriceLastYr = SetFifoLifoAvgCostPrice(security.Key, orgCM, ref realisedFeeLastYr, ref totalSecHoldingUnitsOpeningListCostBase, Math.Abs(reaminingUnitsPrevYear), realisedcostValueLastYr, lossAndGainsReportDS.CostTypes);
                        realisedTradeLastYr = Math.Abs(soldUnitsLastYear) * realisedcostPriceLastYr;
                        realisedcostValueLastYr = realisedTradeLastYr + realisedFeeLastYr;
                    }

                    realisedcostPrice = SetFifoLifoAvgCostPrice(security.Key, orgCM, ref realisedFee, ref holdingTrasactionRangeList, remainingUnits, realisedcostValue, lossAndGainsReportDS.CostTypes);

                    bool isRelusedCostPriceWasZero = false;

                    if (realisedcostPrice == 0 && soldUnits != 0 && totalSecHoldingUnitsClosing != 0 && totalSecHoldingUnitsOpening != 0)
                    {
                        isRelusedCostPriceWasZero = true;

                        List<HoldingTransactions> filteredListLastYr = totalSecHoldingUnitsOpeningListCostBase.Where(t => t.TransactionType != "Sale" && t.Type != "AT").ToList();

                        foreach (var item in filteredListLastYr)
                            item.AdjustedUnits = item.Units;

                        SetSalesHistory(lossAndGainsReportDS, Math.Abs(soldUnitsLastYear), filteredListLastYr);

                        realisedFeeLastYr = 0;
                        realisedcostPriceLastYr = SetFifoLifoAvgCostPrice(security.Key, orgCM, ref realisedFeeLastYr, ref filteredListLastYr, Math.Abs(reaminingUnitsPrevYear), realisedcostValueLastYr, lossAndGainsReportDS.CostTypes);
                        realisedTradeLastYr = Math.Abs(reaminingUnitsPrevYear) * realisedcostPriceLastYr;
                        realisedcostValueLastYr = realisedTradeLastYr + realisedFeeLastYr;

                        if (realisedcostPriceLastYr != 0)
                        {
                            costBaseCarriedForward = 0;
                            realisedcostPrice = realisedcostPriceLastYr;
                        }
                        else
                            realisedFeeLastYr = 0;
                    }

                    realisedTrade = (remainingUnits * realisedcostPrice) + realisedTradeLastYr;
                    realisedcostValue = realisedTrade + realisedFee + realisedFeeLastYr;

                    decimal presentationRealisedCost = (realisedcostValue - (realisedTradeLastYr + realisedFeeLastYr)) + UnrealisedcostValueLastYr;

                    if (isRelusedCostPriceWasZero && soldUnits != 0 && totalSecHoldingUnitsClosing != 0 && totalSecHoldingUnitsOpening != 0)
                    {
                        realisedProfitLoss = saleProceeds - realisedcostValueLastYr - costBaseCarriedForward;
                        presentationRealisedCost = realisedcostValueLastYr;
                    }
                    else
                        realisedProfitLoss = saleProceeds - (realisedcostValue - (realisedTradeLastYr + realisedFeeLastYr)) - costBaseCarriedForward;

                    if (realisedcostPrice == 0)
                        realisedcostPrice = UnrealisedcostPriceLastYr;

                    if (saleProceeds != 0)
                    {
                        lossAndGainsReportDS.AddSecuritySummaryRowRealised(asxBroker.AsxCode, realisedFee, realisedTrade, asxBroker.CompanyName, soldUnits, unitPrice, saleProceeds, realisedcostPrice, presentationRealisedCost, realisedProfitLoss, desktopBrokerAccountEntity.AccountNumber);
                        totalRealised += realisedProfitLoss;
                    }

                    if (unitPrice != 0 && currentValue != 0)
                    {
                        decimal unitsInCurrentYear = holdingTrasactionRange.Where(t => t.TradeDate.Date >= lossAndGainsReportDS.StartDate.Date && t.TradeDate.Date <= lossAndGainsReportDS.EndDate.Date).Where(t => t.TransactionType != "Sale").Where(t => t.Type != "VA").Sum(t => t.Units);

                        var trans = security.Where(t => t.TradeDate.Date <= lossAndGainsReportDS.EndDate.Date);
                        decimal soldUnitsForRecon = Math.Abs(security.Where(t => t.TradeDate.Date <= lossAndGainsReportDS.EndDate.Date).Where(t => t.Type == "RA" || t.Type == "VA").Sum(t => t.Units));

                        List<HoldingTransactions> transactionsForUnRealised = new List<HoldingTransactions>();

                        if (unitsInCurrentYear > totalSecHoldingUnitsClosing)
                            transactionsForUnRealised = holdingTrasactionRangeList.Where(t => t.TradeDate.Date >= lossAndGainsReportDS.StartDate.Date && t.TradeDate.Date <= lossAndGainsReportDS.EndDate.Date).Where(t => t.TransactionType != "Sale").Where(t => t.Type != "VA").ToList();
                        else
                            transactionsForUnRealised = holdingTrasactionRangeList.Where(t => t.TradeDate.Date <= lossAndGainsReportDS.EndDate.Date).Where(t => t.TransactionType != "Sale").Where(t => t.Type != "VA").ToList();


                        if (totalSecHoldingUnitsClosing > totalSecHoldingUnitsOpening && ((soldUnits - totalSecHoldingUnitsOpening) != 0 && (soldUnits - totalSecHoldingUnitsOpening) <= 0))
                        {
                            transactionsForUnRealised = security.Where(t => t.TradeDate.Date <= lossAndGainsReportDS.EndDate.Date).Where(t => t.TransactionType != "Sale" && t.Type != "VA").ToList();

                            foreach (var item in transactionsForUnRealised)
                                item.AdjustedUnits = item.Units;

                            List<HoldingTransactions> holdingTransactionsWithoutAT = transactionsForUnRealised.Where(t => t.Type != "AT").ToList();
                            List<HoldingTransactions> holdingTransactionsWithAT = transactionsForUnRealised.Where(t => t.Type == "AT").ToList();

                            foreach (HoldingTransactions holdingTransaction in holdingTransactionsWithAT)
                            {
                                if (holdingTransaction.AdjustmentTransactions != null && holdingTransaction.AdjustmentTransactions.Count > 0)
                                {
                                    foreach (var item in holdingTransaction.AdjustmentTransactions)
                                    {
                                        var holdTran = item.GetHoldingTransactionObjFromAdjustmentTransaction();
                                        holdTran.AdjustedUnits = holdTran.Units;
                                        holdTran.InvestmentCode = security.Key;
                                        holdingTransactionsWithoutAT.Add(holdTran);
                                    }
                                }
                            }

                            transactionsForUnRealised = holdingTransactionsWithoutAT.ToList();

                            soldUnitsForRecon = SetSalesHistory(lossAndGainsReportDS, soldUnitsForRecon, transactionsForUnRealised);

                            unrealisedcostPrice = SetFifoLifoAvgCostPriceUnRealised(security.Key, orgCM, ref unRealisedfee, ref transactionsForUnRealised, (totalSecHoldingUnitsClosing), unrealisedcostValue, lossAndGainsReportDS.CostTypes);
                        }
                        else
                            unrealisedcostPrice = SetFifoLifoAvgCostPriceUnRealised(security.Key, orgCM, ref unRealisedfee, ref transactionsForUnRealised, (totalSecHoldingUnitsClosing), unrealisedcostValue, lossAndGainsReportDS.CostTypes);

                        if (unrealisedcostPrice == 0)
                        {
                            transactionsForUnRealised = security.Where(t => t.TradeDate.Date <= lossAndGainsReportDS.EndDate.Date).Where(t => t.TransactionType != "Sale" && t.Type != "VA" && t.Type != "AT").ToList();

                            List<HoldingTransactions> transactionsForUnRealisedAT = security.Where(t => t.TradeDate.Date <= lossAndGainsReportDS.EndDate.Date).Where(t => t.Type == "AT").ToList();

                            foreach (var itemAT in transactionsForUnRealisedAT)
                            {
                                if (itemAT.AdjustmentTransactions != null && itemAT.AdjustmentTransactions.Count > 0)
                                {
                                    foreach (var adjItem in itemAT.AdjustmentTransactions)
                                        transactionsForUnRealised.Add(adjItem.GetHoldingTransactionObjFromAdjustmentTransaction());
                                }
                            }

                            foreach (var item in transactionsForUnRealised)
                                item.AdjustedUnits = item.Units;

                            soldUnitsForRecon = SetSalesHistory(lossAndGainsReportDS, soldUnitsForRecon, transactionsForUnRealised);

                            unrealisedcostPrice = SetFifoLifoAvgCostPriceUnRealised(security.Key, orgCM, ref unRealisedfee, ref transactionsForUnRealised, (totalSecHoldingUnitsClosing), unrealisedcostValue, lossAndGainsReportDS.CostTypes);
                        }

                        if (unitsInCurrentYear > totalSecHoldingUnitsClosing)
                            unRealisedTrade = unrealisedcostPrice * (totalSecHoldingUnitsClosing);
                        else
                            unRealisedTrade = unrealisedcostPrice * (totalSecHoldingUnitsClosing);

                        unrealisedcostValue = unRealisedTrade + unRealisedfee;

                        if (unrealisedcostPrice == 0)
                            unrealisedcostPrice = UnrealisedcostPriceLastYr;

                        unrealisedProfitLoss = CalculateUnrealisedProfitLoss(currentValue, unrealisedcostValue);
                    }

                    if (currentValue != 0)
                    {
                        lossAndGainsReportDS.AddSecuritySummaryRow(asxBroker.AsxCode, unRealisedfee, unRealisedTrade, asxBroker.CompanyName, totalSecHoldingUnitsOpening, totalSecHoldingUnitsMovement, totalSecHoldingUnitsClosing, unitPrice, currentValue, unrealisedcostPrice, unrealisedcostValue, unrealisedProfitLoss, desktopBrokerAccountEntity.AccountNumber);
                        totalUnrealised += unrealisedProfitLoss;
                    }

                    foreach (HoldingTransactions holdingTransactions in holdingTrasactionRange)
                    {

                        if (holdingTransactions.Type == "RJ" || holdingTransactions.Type == "AJ")
                        {
                            decimal unitprice = 0;

                            var unitPriceObj = orgCM.Securities.Where(sec => sec.AsxCode == holdingTransactions.InvestmentCode).FirstOrDefault().ASXSecurity.Where(his => his.Date <= holdingTransactions.TradeDate).OrderByDescending(his => his.Date).FirstOrDefault();

                            if (unitPriceObj != null)
                                unitprice = Convert.ToDecimal(unitPriceObj.UnitPrice);

                            holdingTransactions.NetValue = holdingTransactions.Units * unitprice;

                            if (holdingTransactions.Type == "AJ")
                            {
                                if (holdingTransactions.NetValue > 0)
                                    holdingTransactions.NetValue = holdingTransactions.NetValue * -1;
                            }
                        }
                    }

                    foreach (HoldingTransactions holdingTransactions in holdingTrasactionRange)
                    {
                        if (holdingTransactions.Type == "AT" || holdingTransactions.Type == "RT")
                        {
                            decimal unitprice = 0;

                            var unitPriceObj = orgCM.Securities.Where(sec => sec.AsxCode == holdingTransactions.InvestmentCode).FirstOrDefault().ASXSecurity.Where(his => his.Date <= holdingTransactions.TradeDate).OrderByDescending(his => his.Date).FirstOrDefault();

                            if (unitPriceObj != null)
                                unitprice = Convert.ToDecimal(unitPriceObj.UnitPrice);

                            holdingTransactions.NetValue = holdingTransactions.Units * unitprice;

                            if (holdingTransactions.Type == "RT")
                            {
                                if (holdingTransactions.NetValue > 0)
                                    holdingTransactions.NetValue = holdingTransactions.NetValue * -1;
                            }
                        }
                    }

                    decimal transferInASX = holdingTrasactionRange.Where(tran => tran.Type == "AT").Where(tran => tran.AdjustmentTransactions != null && tran.AdjustmentTransactions.Count == 0).Sum(tran => tran.NetValue);
                    decimal transferOutASX = holdingTrasactionRange.Where(tran => tran.Type == "RT").Sum(tran => tran.NetValue);
                    decimal adjustmentUp = holdingTrasactionRange.Where(tran => tran.Type == "RJ").Sum(tran => tran.NetValue);
                    decimal adjustmentDown = holdingTrasactionRange.Where(tran => tran.Type == "AJ").Sum(tran => tran.NetValue);
                    decimal sell = holdingTrasactionRange.Where(tran => tran.Type == "RA").Sum(tran => tran.NetValue);
                    decimal buyPurchase = holdingTrasactionRange.Where(tran => tran.Type == "AN").Sum(tran => tran.NetValue);
                    List<HoldingTransactions> sellTrans = holdingTrasactionRange.Where(tran => tran.Type == "RA").ToList();
                    List<HoldingTransactions> buyPurchaseTrans = holdingTrasactionRange.Where(tran => tran.Type == "AN").ToList();
                    var transferInWithHistory1 = holdingTrasactionRange.Where(tran => tran.Type == "AT");
                    var transferInWithHistory = holdingTrasactionRange.Where(tran => tran.Type == "AT").Where(tran => tran.AdjustmentTransactions != null && tran.AdjustmentTransactions.Count != 0);
                    foreach (var transferInWithHistoryItem in transferInWithHistory)
                    {
                        sell += transferInWithHistoryItem.AdjustmentTransactions.Where(tran => tran.Type == "RA").Sum(t => t.NetValue);
                        buyPurchase += transferInWithHistoryItem.AdjustmentTransactions.Where(tran => tran.Type == "AN").Sum(t => t.NetValue);

                        foreach (var item in transferInWithHistoryItem.AdjustmentTransactions)
                        {
                            HoldingTransactions holdingTranItem = item.GetHoldingTransactionObjFromAdjustmentTransaction();
                            if (holdingTranItem.Type == "RA")
                                sellTrans.Add(holdingTranItem);
                            if (holdingTranItem.Type == "AN")
                                buyPurchaseTrans.Add(holdingTranItem);
                        }
                    }

                    decimal reversalBuyPurchase = holdingTrasactionRange.Where(tran => tran.Type == "VB").Sum(tran => tran.NetValue);
                    decimal reversalRedemptionPurchase = holdingTrasactionRange.Where(tran => tran.Type == "VA").Sum(tran => tran.NetValue);
                    var reversalBuyPurchaseTrans = holdingTrasactionRange.Where(tran => tran.Type == "VB");
                    var reversalRedemptionPurchaseTrans = holdingTrasactionRange.Where(tran => tran.Type == "VA");
                    SetASXTransactionDetails(lossAndGainsReportDS, accountID, desktopBrokerAccountEntity, sellTrans, "Sale", "Investment");
                    SetASXTransactionDetails(lossAndGainsReportDS, accountID, desktopBrokerAccountEntity, buyPurchaseTrans, "Buy / Purchase", "Investment");
                    SetASXTransactionDetails(lossAndGainsReportDS, accountID, desktopBrokerAccountEntity, reversalBuyPurchaseTrans, "Reversal - Buy / Purchase / Deposit", "Investment");
                    SetASXTransactionDetails(lossAndGainsReportDS, accountID, desktopBrokerAccountEntity, reversalRedemptionPurchaseTrans, "Reversal - Redemption / Withdrawal / Fee / Tax", "Investment");

                    movement += (sell * -1);
                    movement += buyPurchase;
                    movement += reversalBuyPurchase;
                    movement += reversalRedemptionPurchase;

                    totalTransferIn += transferInASX;
                    totalTransferIn += transferOutASX;
                    totalTransferIn += adjustmentUp;
                    totalTransferIn += adjustmentDown;

                    totalClosing += currentValue;
                }
            }

            lossAndGainsReportDS.AddSecurityMainSummaryRow(desktopBrokerAccountEntity.Name, desktopBrokerAccountEntity.AccountNumber, totalRealised, totalUnrealised, "ASX");
            lossAndGainsReportDS.AddTaxSummaryRow(accountID, "ASX", desktopBrokerAccountEntity.Name, desktopBrokerAccountEntity.AccountNumber, totalOpening, 0, 0, 0, 0, 0, 0, totalTransferIn, totalTransferOut, 0, totalRealised, totalUnrealised, totalClosing, movement, totalClosing, 0);
        }

        private static decimal SetSalesHistory(LossAndGainsReportDS lossAndGainsReportDS, decimal soldUnitsForRecon, List<HoldingTransactions> transactionsForUnRealised)
        {
            List<HoldingTransactions> sortedList = new List<HoldingTransactions>();

            decimal calculatedUnits = 0;

            if (lossAndGainsReportDS.CostTypes == CostTypes.FIFO)
                sortedList = transactionsForUnRealised.OrderBy(t => t.TradeDate).ToList();
            else if (lossAndGainsReportDS.CostTypes == CostTypes.LIFO)
                sortedList = transactionsForUnRealised.OrderByDescending(t => t.TradeDate).ToList();
            else
                sortedList = transactionsForUnRealised.OrderBy(t => t.TradeDate).ToList();

            foreach (var itemTran in sortedList)
            {
                if (itemTran.AdjustedUnits >= soldUnitsForRecon)
                {
                    itemTran.AdjustedUnits -= soldUnitsForRecon;
                    break;
                }
                else
                {
                    calculatedUnits += itemTran.AdjustedUnits;
                    soldUnitsForRecon -= itemTran.AdjustedUnits;
                    itemTran.AdjustedUnits = 0;
                }
            }

            return soldUnitsForRecon;
        }

        private static string GetBankTransactionCategory(Oritax.TaxSimp.Common.CashManagementEntity cashManagementEntity)
        {
            string category = string.Empty;

            if (cashManagementEntity.Category == "Contribution")
                category = "Transfer In/Out";
            else if (cashManagementEntity.Category == "Income" && (cashManagementEntity.SystemTransactionType != "Transfer In" || cashManagementEntity.SystemTransactionType != "Internal Cash Movement"))
                category = "Income";
            else if (cashManagementEntity.Category == "Income" && cashManagementEntity.SystemTransactionType == "Transfer In")
                category = "Transfer In/Out";
            else if (cashManagementEntity.Category == "Income" && cashManagementEntity.SystemTransactionType == "Internal Cash Movement")
                category = "Internal Cash Movement";
            else if (cashManagementEntity.Category == "Benefit Payment")
                category = "Transfer In/Out";
            else if (cashManagementEntity.Category == "TAX")
                category = "Tax & Expenses";
            else if (cashManagementEntity.Category == "Investment")
                category = "Investments";
            else if (cashManagementEntity.Category == "Expense" && cashManagementEntity.SystemTransactionType == "Transfer Out")
                category = "Transfer In/Out";
            else if (cashManagementEntity.Category == "Expense" && (cashManagementEntity.SystemTransactionType != "Transfer Out" || cashManagementEntity.SystemTransactionType != "Internal Cash Movement"))
                category = "Tax & Expenses";
            else if (cashManagementEntity.Category == "Expense" && cashManagementEntity.SystemTransactionType == "Internal Cash Movement")
                category = "Internal Cash Movement";
            return category;
        }

        private static decimal SetFifoLifoAvgCostPrice(string key, IOrganization orgCM, ref decimal fee, ref List<HoldingTransactions> security, decimal totalSecHoldingUnitsClosing, decimal unrealisedcostValue, CostTypes costTypes)
        {
            List<HoldingTransactions> holdingTransactionsWithoutAT = security.Where(t => t.Type != "AT").ToList();
            List<HoldingTransactions> holdingTransactionsWithAT = security.Where(t => t.Type == "AT").ToList();

            foreach (HoldingTransactions holdingTransaction in holdingTransactionsWithAT)
            {
                if (holdingTransaction.AdjustmentTransactions != null && holdingTransaction.AdjustmentTransactions.Count > 0)
                {
                    foreach (var item in holdingTransaction.AdjustmentTransactions)
                    {
                        var holdTran = item.GetHoldingTransactionObjFromAdjustmentTransaction();
                        holdTran.AdjustedUnits = holdTran.Units;
                        holdTran.InvestmentCode = key;
                        holdingTransactionsWithoutAT.Add(holdTran);
                    }
                }
            }

            security = holdingTransactionsWithoutAT;

            IEnumerable<IGrouping<DateTime, HoldingTransactions>> holdingTransactions = null;

            if (costTypes == CostTypes.FIFO)
                holdingTransactions = holdingTransactionsWithoutAT.OrderBy(tran => tran.TradeDate).GroupBy(t => t.TradeDate);
            else if (costTypes == CostTypes.LIFO)
                holdingTransactions = holdingTransactionsWithoutAT.OrderByDescending(tran => tran.TradeDate).GroupBy(t => t.TradeDate);
            else
                holdingTransactions = holdingTransactionsWithoutAT.OrderBy(tran => tran.TradeDate).GroupBy(t => t.TradeDate);

            decimal calculatedUnits = 0;
            decimal remainingUnits = 0;
            remainingUnits = totalSecHoldingUnitsClosing;
            decimal costValue = 0;
            bool isFinal = false;

            foreach (var holdingTransactionGroup in holdingTransactions)
            {
                if (!isFinal)
                {
                    var sortedTransactions = holdingTransactionGroup.OrderByDescending(t => t.Units);
                    foreach (HoldingTransactions holdingTransaction in sortedTransactions)
                    {
                        decimal unitPriceChk = 0;

                        if (Math.Abs(holdingTransaction.AdjustedUnits) >= remainingUnits)
                        {
                            if (holdingTransaction.UnitPrice != 0)
                                unitPriceChk = Math.Abs(holdingTransaction.UnitPrice);
                            else if (holdingTransaction.Type == "AT" && holdingTransaction.AdjustmentTransactions != null && holdingTransaction.AdjustmentTransactions.Count > 0)
                            {
                                decimal totalUnits = holdingTransaction.AdjustmentTransactions.Sum(adj => adj.Units);
                                decimal grossValue = holdingTransaction.AdjustmentTransactions.Sum(adj => (adj.Units * adj.UnitPrice));
                                unitPriceChk = grossValue / totalUnits;
                            }
                            else
                            {
                                var unitPriceObj = orgCM.Securities.Where(sec => sec.AsxCode == holdingTransaction.InvestmentCode).FirstOrDefault().ASXSecurity.Where(his => his.Date <= holdingTransaction.TradeDate).OrderByDescending(his => his.Date).FirstOrDefault();

                                if (unitPriceObj != null)
                                    unitPriceChk = Convert.ToDecimal(unitPriceObj.UnitPrice);
                                else
                                    unitPriceChk = Math.Abs(holdingTransaction.UnitPrice);
                            }

                            costValue += unitPriceChk * Math.Abs(remainingUnits);

                            fee += Math.Abs((holdingTransaction.BrokerageAmount / holdingTransaction.Units) * remainingUnits);

                            isFinal = true;

                            var holdTran = security.Where(t => t.TranstactionUniqueID == holdingTransaction.TranstactionUniqueID).FirstOrDefault();

                            if (holdTran != null)
                            {
                                if (holdTran.AdjustedUnits < 0)
                                    holdTran.AdjustedUnits += Math.Abs(remainingUnits);
                                else
                                    holdTran.AdjustedUnits -= Math.Abs(remainingUnits);
                            }

                            break;
                        }
                        else
                        {
                            if (holdingTransaction.UnitPrice != 0)
                                unitPriceChk = Math.Abs(holdingTransaction.UnitPrice);
                            else if (holdingTransaction.Type == "AT" && holdingTransaction.AdjustmentTransactions != null && holdingTransaction.AdjustmentTransactions.Count > 0)
                            {
                                decimal totalUnits = holdingTransaction.AdjustmentTransactions.Sum(adj => adj.Units);
                                decimal grossValue = holdingTransaction.AdjustmentTransactions.Sum(adj => (adj.Units * adj.UnitPrice));
                                unitPriceChk = grossValue / totalUnits;
                            }
                            else
                            {
                                var unitPriceObj = orgCM.Securities.Where(sec => sec.AsxCode == holdingTransaction.InvestmentCode).FirstOrDefault().ASXSecurity.Where(his => his.Date <= holdingTransaction.TradeDate).OrderByDescending(his => his.Date).FirstOrDefault();

                                if (unitPriceObj != null)
                                    unitPriceChk = Convert.ToDecimal(unitPriceObj.UnitPrice);
                                else
                                    unitPriceChk = Math.Abs(holdingTransaction.UnitPrice);
                            }

                            costValue += unitPriceChk * Math.Abs(holdingTransaction.AdjustedUnits);

                            remainingUnits = totalSecHoldingUnitsClosing - Math.Abs(holdingTransaction.AdjustedUnits) - calculatedUnits;

                            calculatedUnits += Math.Abs(holdingTransaction.AdjustedUnits);
                            fee += holdingTransaction.BrokerageAmount;

                            var holdTran = security.Where(t => t.TranstactionUniqueID == holdingTransaction.TranstactionUniqueID).FirstOrDefault();

                            if (holdTran != null)
                                holdTran.AdjustedUnits = 0;
                        }
                    }
                }
            }

            //Set Adjustment Down / Adjustment Up Impact
            decimal adjustment = 0;

            var adjustments = holdingTransactionsWithoutAT.Where(t => t.TransactionType == "Adjustment Up");

            foreach (var item in adjustments)
            {
                if (item.AdjustedUnits != 0)
                    adjustment += item.NetValue;
            }

            costValue += adjustment;
            if (totalSecHoldingUnitsClosing != 0)
                return costValue / Math.Abs(totalSecHoldingUnitsClosing);

            return 0;
        }

        private static decimal SetFifoLifoAvgCostPriceUnRealised(string key, IOrganization orgCM, ref decimal fee, ref List<HoldingTransactions> security, decimal totalSecHoldingUnitsClosing, decimal unrealisedcostValue, CostTypes costTypes)
        {
            List<HoldingTransactions> holdingTransactionsWithoutAT = security.Where(t => t.Type != "AT").ToList();
            List<HoldingTransactions> holdingTransactionsWithAT = security.Where(t => t.Type == "AT").ToList();

            foreach (HoldingTransactions holdingTransaction in holdingTransactionsWithAT)
            {
                if (holdingTransaction.AdjustmentTransactions != null && holdingTransaction.AdjustmentTransactions.Count > 0)
                {
                    foreach (var item in holdingTransaction.AdjustmentTransactions)
                    {
                        var holdTran = item.GetHoldingTransactionObjFromAdjustmentTransaction();
                        holdTran.AdjustedUnits = holdTran.Units;
                        holdTran.InvestmentCode = key;
                        holdingTransactionsWithoutAT.Add(holdTran);
                    }
                }
            }

            security = holdingTransactionsWithoutAT;

            IEnumerable<IGrouping<DateTime, HoldingTransactions>> holdingTransactions = null;

            if (costTypes == CostTypes.FIFO)
                holdingTransactions = holdingTransactionsWithoutAT.OrderBy(tran => tran.TradeDate).GroupBy(t => t.TradeDate);
            else if (costTypes == CostTypes.LIFO)
                holdingTransactions = holdingTransactionsWithoutAT.OrderByDescending(tran => tran.TradeDate).GroupBy(t => t.TradeDate);
            else
                holdingTransactions = holdingTransactionsWithoutAT.OrderBy(tran => tran.TradeDate).GroupBy(t => t.TradeDate);

            decimal calculatedUnits = 0;
            decimal remainingUnits = 0;
            remainingUnits = totalSecHoldingUnitsClosing;
            decimal costValue = 0;
            bool isFinal = false;

            foreach (var holdingTransactionGroup in holdingTransactions)
            {
                if (!isFinal)
                {
                    var sortedTransactions = holdingTransactionGroup.OrderByDescending(t => t.Units);
                    foreach (HoldingTransactions holdingTransaction in sortedTransactions)
                    {
                        decimal unitPriceChk = 0;

                        if (Math.Abs(holdingTransaction.AdjustedUnits) >= remainingUnits)
                        {
                            if (holdingTransaction.UnitPrice != 0)
                                unitPriceChk = Math.Abs(holdingTransaction.UnitPrice);
                            else if (holdingTransaction.Type == "AT" && holdingTransaction.AdjustmentTransactions != null && holdingTransaction.AdjustmentTransactions.Count > 0)
                            {
                                decimal totalUnits = holdingTransaction.AdjustmentTransactions.Sum(adj => adj.Units);
                                decimal grossValue = holdingTransaction.AdjustmentTransactions.Sum(adj => (adj.Units * adj.UnitPrice));
                                unitPriceChk = grossValue / totalUnits;
                            }
                            else
                            {
                                var unitPriceObj = orgCM.Securities.Where(sec => sec.AsxCode == holdingTransaction.InvestmentCode).FirstOrDefault().ASXSecurity.Where(his => his.Date <= holdingTransaction.TradeDate).OrderByDescending(his => his.Date).FirstOrDefault();

                                if (unitPriceObj != null)
                                    unitPriceChk = Convert.ToDecimal(unitPriceObj.UnitPrice);
                                else
                                    unitPriceChk = Math.Abs(holdingTransaction.UnitPrice);
                            }

                            costValue += unitPriceChk * Math.Abs(remainingUnits);

                            fee += Math.Abs((holdingTransaction.BrokerageAmount / holdingTransaction.Units) * remainingUnits);

                            isFinal = true;

                            var holdTran = security.Where(t => t.TranstactionUniqueID == holdingTransaction.TranstactionUniqueID).FirstOrDefault();

                            if (holdTran != null)
                            {
                                if (holdTran.AdjustedUnits < 0)
                                    holdTran.AdjustedUnits += Math.Abs(remainingUnits);
                                else
                                    holdTran.AdjustedUnits -= Math.Abs(remainingUnits);
                            }

                            break;
                        }
                        else
                        {
                            if (holdingTransaction.UnitPrice != 0)
                                unitPriceChk = Math.Abs(holdingTransaction.UnitPrice);
                            else if (holdingTransaction.Type == "AT" && holdingTransaction.AdjustmentTransactions != null && holdingTransaction.AdjustmentTransactions.Count > 0)
                            {
                                decimal totalUnits = holdingTransaction.AdjustmentTransactions.Sum(adj => adj.Units);
                                decimal grossValue = holdingTransaction.AdjustmentTransactions.Sum(adj => (adj.Units * adj.UnitPrice));
                                unitPriceChk = grossValue / totalUnits;
                            }
                            else
                            {
                                var unitPriceObj = orgCM.Securities.Where(sec => sec.AsxCode == holdingTransaction.InvestmentCode).FirstOrDefault().ASXSecurity.Where(his => his.Date <= holdingTransaction.TradeDate).OrderByDescending(his => his.Date).FirstOrDefault();

                                if (unitPriceObj != null)
                                    unitPriceChk = Convert.ToDecimal(unitPriceObj.UnitPrice);
                                else
                                    unitPriceChk = Math.Abs(holdingTransaction.UnitPrice);
                            }

                            costValue += unitPriceChk * Math.Abs(holdingTransaction.AdjustedUnits);

                            remainingUnits = totalSecHoldingUnitsClosing - Math.Abs(holdingTransaction.AdjustedUnits) - calculatedUnits;

                            calculatedUnits += Math.Abs(holdingTransaction.AdjustedUnits);
                            fee += holdingTransaction.BrokerageAmount;

                            var holdTran = security.Where(t => t.TranstactionUniqueID == holdingTransaction.TranstactionUniqueID).FirstOrDefault();

                            if (holdTran != null)
                                holdTran.AdjustedUnits = 0;
                        }
                    }
                }
            }


            //foreach (var holdingTransactionGroup in holdingTransactions)
            //{
            //    var sortedTransactions = holdingTransactionGroup.OrderByDescending(t => t.Units);
            //    foreach (HoldingTransactions holdingTransaction in sortedTransactions)
            //    {
            //        decimal unitPriceChk = 0;

            //        if (holdingTransaction.UnitPrice != 0)
            //            unitPriceChk = Math.Abs(holdingTransaction.UnitPrice);
            //        else if (holdingTransaction.Type == "AT" && holdingTransaction.AdjustmentTransactions != null && holdingTransaction.AdjustmentTransactions.Count > 0)
            //        {
            //            decimal totalUnits = holdingTransaction.AdjustmentTransactions.Sum(adj => adj.Units);
            //            decimal grossValue = holdingTransaction.AdjustmentTransactions.Sum(adj => (adj.Units * adj.UnitPrice));
            //            unitPriceChk = grossValue / totalUnits;
            //        }
            //        else
            //        {
            //            var unitPriceObj = orgCM.Securities.Where(sec => sec.AsxCode == holdingTransaction.InvestmentCode).FirstOrDefault().ASXSecurity.Where(his => his.Date <= holdingTransaction.TradeDate).OrderByDescending(his => his.Date).FirstOrDefault();

            //            if (unitPriceObj != null)
            //                unitPriceChk = Convert.ToDecimal(unitPriceObj.UnitPrice);
            //            else
            //                unitPriceChk = Math.Abs(holdingTransaction.UnitPrice);
            //        }

            //        costValue += unitPriceChk * Math.Abs(totalSecHoldingUnitsClosing);

            //        fee += Math.Abs(holdingTransaction.BrokerageAmount);
            //    }
            //}

            //Set Adjustment Down / Adjustment Up Impact
            decimal adjustment = 0;

            var adjustments = holdingTransactionsWithoutAT.Where(t => t.TransactionType == "Adjustment Up" || t.TransactionType == "Adjustment Down");
            if (adjustments != null && adjustments.Count() > 0)
                adjustment = adjustments.Sum(t => t.NetValue);
            costValue += adjustment;
            if (totalSecHoldingUnitsClosing != 0)
                return costValue / Math.Abs(totalSecHoldingUnitsClosing);

            return 0;
        }


        private static decimal SetFifoLifoAvgCostPriceMIS(IOrganization orgCM, string investmentCode, ref decimal fee, IEnumerable<MISFundTransactionEntity> security, decimal totalSecHoldingUnitsClosing, decimal unrealisedcostValue, CostTypes costTypes)
        {
            IEnumerable<MISFundTransactionEntity> holdingTransactions = null;

            if (costTypes == CostTypes.FIFO)
                holdingTransactions = security.OrderBy(tran => tran.TradeDate);
            else if (costTypes == CostTypes.LIFO)
                holdingTransactions = security.OrderByDescending(tran => tran.TradeDate);
            else
                holdingTransactions = security.OrderBy(tran => tran.TradeDate);

            decimal calculatedUnits = 0;
            decimal remainingUnits = 0;
            remainingUnits = totalSecHoldingUnitsClosing;
            decimal costValue = 0;
            foreach (MISFundTransactionEntity holdingTransaction in holdingTransactions)
            {
                decimal unitPriceChk = 0;

                if (holdingTransaction.Shares >= remainingUnits)
                {
                    if (holdingTransaction.UnitPrice != 0)
                        unitPriceChk = Math.Abs(holdingTransaction.UnitPrice);
                    else
                    {
                        var unitPriceObj = orgCM.Securities.Where(sec => sec.AsxCode == investmentCode).FirstOrDefault().ASXSecurity.Where(his => his.Date <= holdingTransaction.TradeDate).OrderByDescending(his => his.Date).FirstOrDefault();

                        if (unitPriceObj != null)
                            unitPriceChk = Convert.ToDecimal(unitPriceObj.UnitPrice);
                        else
                            unitPriceChk = Math.Abs(holdingTransaction.UnitPrice);
                    }

                    costValue += unitPriceChk * remainingUnits;
                    break;
                }
                else
                {
                    if (holdingTransaction.UnitPrice != 0)
                        unitPriceChk = Math.Abs(holdingTransaction.UnitPrice);
                    else
                    {
                        var unitPriceObj = orgCM.Securities.Where(sec => sec.AsxCode == investmentCode).FirstOrDefault().ASXSecurity.Where(his => his.Date <= holdingTransaction.TradeDate).OrderByDescending(his => his.Date).FirstOrDefault();

                        if (unitPriceObj != null)
                            unitPriceChk = Convert.ToDecimal(unitPriceObj.UnitPrice);
                        else
                            unitPriceChk = Math.Abs(holdingTransaction.UnitPrice);
                    }

                    costValue += unitPriceChk * Math.Abs(holdingTransaction.Shares);

                    remainingUnits = totalSecHoldingUnitsClosing - Math.Abs(holdingTransaction.Shares) - calculatedUnits;

                    calculatedUnits += Math.Abs(holdingTransaction.Shares);
                }
            }

            if (totalSecHoldingUnitsClosing != 0)
                return costValue / totalSecHoldingUnitsClosing;

            return 0;
        }

        private static decimal SetFifoLifoAvgCostPriceMIS_PR(IOrganization orgCM, string investmentCode, ref decimal fee, IEnumerable<MISFundTransactionEntity> security, decimal totalSecHoldingUnitsClosing, decimal unrealisedcostValue, CostTypes costTypes)
        {
            List<MISFundTransactionEntity> holdingTransactions = new List<MISFundTransactionEntity>();

            foreach (MISFundTransactionEntity mistran in security)
            {
                MISFundTransactionEntity tran = new MISFundTransactionEntity();
                tran.Shares = mistran.Shares;
                tran.UnitPrice = mistran.UnitPrice;
                tran.ID = mistran.ID;
                tran.ClientID = tran.ClientID;
                tran.InvestorCategory = tran.InvestorCategory;
                tran.TransactionType = mistran.TransactionType;
                tran.TradeDate = mistran.TradeDate;

                holdingTransactions.Add(tran);
            }

            if (costTypes == CostTypes.FIFO)
                holdingTransactions.OrderBy(tran => tran.TradeDate);
            else if (costTypes == CostTypes.LIFO)
                holdingTransactions.OrderByDescending(tran => tran.TradeDate);
            else
                holdingTransactions.OrderBy(tran => tran.TradeDate);

            var soldUnitsTrans = holdingTransactions.Where(ft => ft.TransactionType == "Redemption");
            var boughtUnitsTrans = holdingTransactions.Where(ft => ft.TransactionType == "Purchase");

            IEnumerable<MISFundTransactionEntity> holdingTransactionsSold = null;
            IEnumerable<MISFundTransactionEntity> holdingTransactionsPUR = null;

            if (costTypes == CostTypes.FIFO)
            {
                holdingTransactionsPUR = boughtUnitsTrans.OrderBy(tran => tran.TradeDate);
                holdingTransactionsSold = soldUnitsTrans.OrderBy(tran => tran.TradeDate);
            }
            else if (costTypes == CostTypes.LIFO)
            {
                holdingTransactionsPUR = boughtUnitsTrans.OrderByDescending(tran => tran.TradeDate);
                holdingTransactionsSold = soldUnitsTrans.OrderByDescending(tran => tran.TradeDate);
            }
            else
            {
                holdingTransactionsPUR = boughtUnitsTrans.OrderBy(tran => tran.TradeDate);
                holdingTransactionsSold = soldUnitsTrans.OrderBy(tran => tran.TradeDate);
            }

            foreach (MISFundTransactionEntity purTrans in holdingTransactionsPUR)
            {
                foreach (MISFundTransactionEntity soldTrans in holdingTransactionsSold)
                {
                    if (!soldTrans.ProcessedTransactionForCostPrice)
                    {
                        if (Math.Abs(soldTrans.Shares) < purTrans.Shares)
                        {
                            purTrans.Shares += soldTrans.Shares;
                            soldTrans.ProcessedTransactionForCostPrice = true;
                        }
                        else
                        {
                            soldTrans.Shares += purTrans.Shares;
                            purTrans.Shares = 0;
                        }
                    }
                }
            }

            decimal calculatedUnits = 0;
            decimal remainingUnits = 0;
            remainingUnits = totalSecHoldingUnitsClosing;
            decimal costValue = 0;
            foreach (MISFundTransactionEntity holdingTransaction in holdingTransactionsPUR)
            {
                decimal unitPriceChk = 0;

                if (holdingTransaction.Shares >= remainingUnits)
                {
                    if (holdingTransaction.UnitPrice != 0)
                        unitPriceChk = Math.Abs(holdingTransaction.UnitPrice);
                    else
                    {
                        var unitPriceObj = orgCM.Securities.Where(sec => sec.AsxCode == investmentCode).FirstOrDefault().ASXSecurity.Where(his => his.Date <= holdingTransaction.TradeDate).OrderByDescending(his => his.Date).FirstOrDefault();

                        if (unitPriceObj != null)
                            unitPriceChk = Convert.ToDecimal(unitPriceObj.UnitPrice);
                        else
                            unitPriceChk = Math.Abs(holdingTransaction.UnitPrice);
                    }

                    costValue += unitPriceChk * remainingUnits;
                    break;
                }
                else
                {
                    if (holdingTransaction.UnitPrice != 0)
                        unitPriceChk = Math.Abs(holdingTransaction.UnitPrice);
                    else
                    {
                        var unitPriceObj = orgCM.Securities.Where(sec => sec.AsxCode == investmentCode).FirstOrDefault().ASXSecurity.Where(his => his.Date <= holdingTransaction.TradeDate).OrderByDescending(his => his.Date).FirstOrDefault();

                        if (unitPriceObj != null)
                            unitPriceChk = Convert.ToDecimal(unitPriceObj.UnitPrice);
                        else
                            unitPriceChk = Math.Abs(holdingTransaction.UnitPrice);
                    }

                    costValue += unitPriceChk * holdingTransaction.Shares;

                    remainingUnits = totalSecHoldingUnitsClosing - holdingTransaction.Shares - calculatedUnits;

                    calculatedUnits += holdingTransaction.Shares;
                }
            }

            if (totalSecHoldingUnitsClosing != 0)
                return costValue / totalSecHoldingUnitsClosing;

            return 0;
        }

        private static decimal SetFifoLifoAvgCostPriceManual(IOrganization orgCM, ref decimal fee, IGrouping<Guid, ClientManualAssetEntity> security, decimal totalSecHoldingUnitsClosing, decimal unrealisedcostValue, CostTypes costTypes)
        {
            IEnumerable<ClientManualAssetEntity> holdingTransactions = null;

            if (costTypes == CostTypes.FIFO)
                holdingTransactions = security.OrderBy(tran => tran.TransactionDate);
            else if (costTypes == CostTypes.LIFO)
                holdingTransactions = security.OrderByDescending(tran => tran.TransactionDate);
            else
                holdingTransactions = security.OrderBy(tran => tran.TransactionDate);

            decimal calculatedUnits = 0;
            decimal remainingUnits = 0;
            remainingUnits = totalSecHoldingUnitsClosing;
            decimal costValue = 0;
            foreach (ClientManualAssetEntity holdingTransaction in holdingTransactions)
            {
                decimal unitPriceChk = 0;

                if (holdingTransaction.UnitsOnHand >= remainingUnits)
                {
                    var unitPriceObj = orgCM.ManualAsset.Where(sec => sec.ID == holdingTransaction.InvestmentCode).FirstOrDefault().ASXSecurity.Where(his => his.Date <= holdingTransaction.TransactionDate).OrderByDescending(his => his.Date).FirstOrDefault();

                    if (unitPriceObj != null)
                        unitPriceChk = Convert.ToDecimal(unitPriceObj.UnitPrice);
                    else
                    {
                        var nextBestPrice = orgCM.ManualAsset.Where(sec => sec.ID == holdingTransaction.InvestmentCode).FirstOrDefault().ASXSecurity.Where(his => his.Date >= holdingTransaction.TransactionDate).OrderBy(his => his.Date).FirstOrDefault();
                        if (nextBestPrice != null)
                            unitPriceChk = Convert.ToDecimal(nextBestPrice.UnitPrice);
                    }

                    costValue += unitPriceChk * holdingTransaction.UnitsOnHand;
                    break;
                }
                else
                {
                    var unitPriceObj = orgCM.ManualAsset.Where(sec => sec.ID == holdingTransaction.InvestmentCode).FirstOrDefault().ASXSecurity.Where(his => his.Date <= holdingTransaction.TransactionDate).OrderByDescending(his => his.Date).FirstOrDefault();

                    if (unitPriceObj != null)
                        unitPriceChk = Convert.ToDecimal(unitPriceObj.UnitPrice);
                    else
                    {
                        var nextBestPrice = orgCM.ManualAsset.Where(sec => sec.ID == holdingTransaction.InvestmentCode).FirstOrDefault().ASXSecurity.Where(his => his.Date >= holdingTransaction.TransactionDate).OrderBy(his => his.Date).FirstOrDefault();
                        if (nextBestPrice != null)
                            unitPriceChk = Convert.ToDecimal(nextBestPrice.UnitPrice);
                    }

                    costValue += unitPriceChk * holdingTransaction.UnitsOnHand;

                    remainingUnits = totalSecHoldingUnitsClosing - holdingTransaction.UnitsOnHand - calculatedUnits;

                    calculatedUnits += holdingTransaction.UnitsOnHand;
                }
            }

            if (totalSecHoldingUnitsClosing != 0)
                return costValue / totalSecHoldingUnitsClosing;

            return 0;
        }

        public static decimal CalculateUnrealisedProfitLoss(decimal currentValue, decimal unrealisedcostValue)
        {
            return currentValue - unrealisedcostValue;
        }

        public static decimal CalculateCostPriceMIS(IEnumerable<MISFundTransactionEntity> security, CostTypes costType, IOrganization orgCM)
        {
            decimal costPrice = 0;

            if (costType == CostTypes.AVG)
            {
                int transactionCounts = 0;
                decimal aggregatedPrice = 0;

                SetAverageCostPrice(security, orgCM, ref transactionCounts, ref aggregatedPrice);
                if (transactionCounts != 0)
                    costPrice = aggregatedPrice / transactionCounts;
            }
            else if (costType == CostTypes.FIFO)
            {
                decimal aggregatedPrice = 0;
                var holdingTransaction = security.OrderBy(tran => tran.TradeDate).FirstOrDefault();

                if (holdingTransaction != null)
                    aggregatedPrice = SetLIFOandFIFOpriceCost(orgCM, aggregatedPrice, holdingTransaction);

                costPrice = aggregatedPrice;
            }
            else if (costType == CostTypes.LIFO)
            {
                decimal aggregatedPrice = 0;
                var holdingTransaction = security.OrderByDescending(tran => tran.TradeDate).FirstOrDefault();

                if (holdingTransaction != null)
                    aggregatedPrice = SetLIFOandFIFOpriceCost(orgCM, aggregatedPrice, holdingTransaction);

                costPrice = aggregatedPrice;
            }
            else if (costType == CostTypes.None)
                throw new Exception("Cost type is not defiend. Need to defined cost type: LIFO, FIFO or AVG");

            return costPrice;
        }

        private static void SetAverageCostPrice(IEnumerable<MISFundTransactionEntity> security, IOrganization orgCM, ref int transactionCounts, ref decimal aggregatedPrice)
        {
            foreach (MISFundTransactionEntity trans in security)
            {
                transactionCounts++;
                aggregatedPrice += SetUnitPrice(trans.UnitPrice);
            }
        }

        private static decimal SetUnitPrice(decimal unitprice)
        {
            if (unitprice < 0)
                unitprice = unitprice * -1;
            return unitprice;
        }

        private static decimal SetLIFOandFIFOpriceCost(IOrganization orgCM, decimal aggregatedPrice, MISFundTransactionEntity MISFundTransactionEntity)
        {
            aggregatedPrice += SetUnitPrice(MISFundTransactionEntity.UnitPrice);
            return aggregatedPrice;
        }

        public static decimal CalculateUnrealisedCostPrice(decimal currentValue, decimal costValue)
        {
            if (currentValue != 0)
                return costValue / currentValue;
            else
                return 0;
        }

        private static void ManualTransactions(string clientID, ObservableCollection<ClientManualAssetEntity> ClientManualAssetCollection, IOrganization orgCM, LossAndGainsReportDS lossAndGainsReportDS)
        {
            if (ClientManualAssetCollection != null)
            {
                var groupedManTransactions = ClientManualAssetCollection.GroupBy(cman => cman.InvestmentCode);

                foreach (var manualAsset in groupedManTransactions)
                {
                    var systemManualAsset = orgCM.ManualAsset.Where(manList => manList.ID == manualAsset.Key).FirstOrDefault();

                    decimal totalSecHoldingUnitsClosing = manualAsset.Where(holdTrans => holdTrans.TransactionDate <= lossAndGainsReportDS.EndDate).Sum(sec => sec.UnitsOnHand);
                    decimal totalSecHoldingUnitsOpening = manualAsset.Where(holdTrans => holdTrans.TransactionDate <= lossAndGainsReportDS.StartDate.AddDays(-1)).Sum(sec => sec.UnitsOnHand);
                    var range = manualAsset.Where(holdTrans => holdTrans.TransactionDate >= lossAndGainsReportDS.StartDate && holdTrans.TransactionDate <= lossAndGainsReportDS.EndDate);

                    decimal totalSecHoldingUnitsOpeningValue = 0;

                    var manualTransactionsOpening = manualAsset.Where(holdTrans => holdTrans.TransactionDate <= lossAndGainsReportDS.StartDate.AddDays(-1));

                    foreach (ClientManualAssetEntity manAssetEntityOp in manualTransactionsOpening)
                    {
                        var transactionPriceDate = systemManualAsset.ASXSecurity.Where(secpri => secpri.Date <= manAssetEntityOp.TransactionDate).OrderByDescending(secpri => secpri.Date).FirstOrDefault();

                        if (transactionPriceDate != null)
                            totalSecHoldingUnitsOpeningValue += manAssetEntityOp.UnitsOnHand * Convert.ToDecimal(transactionPriceDate.UnitPrice);
                    }

                    decimal totalSecHoldingUnitsMovement = totalSecHoldingUnitsClosing - totalSecHoldingUnitsOpening;
                    var filteredManualAssetTransactions = manualAsset.Where(man => man.TransactionDate >= lossAndGainsReportDS.StartDate && man.TransactionDate <= lossAndGainsReportDS.EndDate);

                    var hisSecurity = systemManualAsset.ASXSecurity.Where(secpri => secpri.Date <= lossAndGainsReportDS.EndDate).OrderByDescending(secpri => secpri.Date).FirstOrDefault();
                    decimal unitPrice = 0;
                    decimal soldUnits = 0;
                    decimal saleProceeds = 0;
                    if (hisSecurity != null)
                        unitPrice = Convert.ToDecimal(hisSecurity.UnitPrice);

                    foreach (ClientManualAssetEntity manAssetEntity in range)
                    {
                        if (manAssetEntity.TransactionType == "Sale")
                        {
                            decimal units = 0;

                            if (manAssetEntity.UnitsOnHand < 0)
                                units = manAssetEntity.UnitsOnHand * -1;
                            else
                                units = manAssetEntity.UnitsOnHand;

                            soldUnits += units;

                            decimal unitPriceCostPerTransaction = 0;

                            var transactionPriceDate = systemManualAsset.ASXSecurity.Where(secpri => secpri.Date <= manAssetEntity.TransactionDate).OrderByDescending(secpri => secpri.Date).FirstOrDefault();

                            if (transactionPriceDate != null)
                                unitPriceCostPerTransaction = units * Convert.ToDecimal(transactionPriceDate.UnitPrice);

                            saleProceeds += (unitPriceCostPerTransaction);
                        }
                    }

                    decimal transferInOutValues = 0;

                    foreach (ClientManualAssetEntity manAssetEntity in manualAsset)
                    {
                        if (manAssetEntity.TransactionType == "Transfer In" || manAssetEntity.TransactionType == "Transfer Out")
                        {
                            decimal units = 0;

                            if (manAssetEntity.UnitsOnHand < 0)
                                units = manAssetEntity.UnitsOnHand * -1;
                            else
                                units = manAssetEntity.UnitsOnHand;

                            decimal unitPriceCostPerTransaction = 0;

                            var transactionPriceDate = systemManualAsset.ASXSecurity.Where(secpri => secpri.Date <= manAssetEntity.TransactionDate).OrderByDescending(secpri => secpri.Date).FirstOrDefault();

                            if (transactionPriceDate != null)
                                unitPriceCostPerTransaction = units * Convert.ToDecimal(transactionPriceDate.UnitPrice);

                            transferInOutValues += (unitPriceCostPerTransaction * units);
                        }
                    }


                    decimal currentValue = totalSecHoldingUnitsClosing * unitPrice;
                    decimal unrealisedcostValue = 0;
                    decimal unrealisedcostPrice = 0;
                    decimal unrealisedProfitLoss = 0;
                    decimal unRealisedfee = 0;
                    decimal unRealisedTrade = 0;

                    if (unitPrice != 0 && currentValue != 0)
                    {
                        unrealisedcostPrice = SetFifoLifoAvgCostPriceManual(orgCM, ref unRealisedfee, manualAsset, totalSecHoldingUnitsClosing, unrealisedcostValue, lossAndGainsReportDS.CostTypes);
                        unRealisedTrade = unrealisedcostPrice * totalSecHoldingUnitsClosing;
                        unrealisedcostValue = unRealisedTrade + unRealisedfee;
                        unrealisedProfitLoss = CalculateUnrealisedProfitLoss(currentValue, unrealisedcostValue);
                    }

                    if (currentValue != 0)
                        lossAndGainsReportDS.AddSecuritySummaryRow(systemManualAsset.AsxCode, unRealisedfee, unRealisedTrade, systemManualAsset.CompanyName, totalSecHoldingUnitsOpening, totalSecHoldingUnitsMovement, totalSecHoldingUnitsClosing, unitPrice, currentValue, unrealisedcostPrice, unrealisedcostValue, unrealisedProfitLoss, systemManualAsset.AsxCode);

                    decimal realisedcostValue = 0;
                    decimal realisedcostPrice = 0;
                    decimal realisedProfitLoss = 0;
                    decimal realisedFee = 0;
                    decimal realisedTrade = 0;

                    realisedcostPrice = SetFifoLifoAvgCostPriceManual(orgCM, ref realisedFee, manualAsset, soldUnits, realisedcostValue, lossAndGainsReportDS.CostTypes);
                    realisedTrade = soldUnits * realisedcostPrice;
                    realisedcostValue = realisedTrade + realisedFee;
                    realisedProfitLoss = saleProceeds - realisedcostValue;

                    if (saleProceeds != 0)
                        lossAndGainsReportDS.AddSecuritySummaryRowRealised(systemManualAsset.AsxCode, realisedFee, realisedTrade, systemManualAsset.CompanyName, soldUnits, unitPrice, saleProceeds, realisedcostPrice, realisedcostValue, realisedProfitLoss, systemManualAsset.AsxCode);

                    lossAndGainsReportDS.AddSecurityMainSummaryRow(systemManualAsset.CompanyName, systemManualAsset.AsxCode, realisedProfitLoss, unrealisedProfitLoss, "MA");
                    lossAndGainsReportDS.AddTaxSummaryRow(systemManualAsset.ID, "MA", systemManualAsset.CompanyName, systemManualAsset.AsxCode, totalSecHoldingUnitsOpeningValue, 0, 0, 0, 0, 0, 0, 0, 0, 0, realisedProfitLoss, unrealisedProfitLoss, currentValue, saleProceeds, currentValue, 0);
                }
            }
        }

        public void OnGetDataLossAndGainsReportDSIndividual(IOrganizationUnit orgUnit, LossAndGainsReportDS data, string clientType, string clientID, string clientName, IdentityCM Parent, ICMBroker Broker, ClientIndividualEntity Entity)
        {
            DataTable clientSummaryTable = data.Tables[SecuritySummaryReportDS.CLIENTSUMMARYTABLE];
            DataTable contactTable = data.Tables[SecuritySummaryReportDS.CONTACTTABLE];

            DataRow clientSummaryRow = clientSummaryTable.NewRow();
            DataRow contactRow = contactTable.NewRow();

            BasicClientDetails(clientType, clientID, clientName, Parent, Broker, Entity.ABN, string.Empty, Entity.Address, clientSummaryRow);

            CSVInstance csvinstance = new CSVInstance(Broker);
            ClientIndividualEntityCSV _entitycsv = csvinstance.GetEntityConcreteInstance(Entity);
            foreach (var contact in _entitycsv.Applicants)
            {
                contactRow[SecuritySummaryReportDS.CONTACT_TITLE] = contact.PersonalTitle;
                contactRow[SecuritySummaryReportDS.CONTACT_NAME] = contact.Fullname;
                contactRow[SecuritySummaryReportDS.CONTACT_BIZNO] = contact.WorkPhoneNumber;
                contactRow[SecuritySummaryReportDS.CONTACT_MOBNO] = contact.MobilePhoneNumber;
                contactRow[SecuritySummaryReportDS.CONTACT_HNO] = contact.HomePhoneNumber;
                contactRow[SecuritySummaryReportDS.CONTACT_FAXNO] = contact.Facsimile;
                contactRow[SecuritySummaryReportDS.CONTACT_EMAIL] = contact.EmailAddress;
            }

            clientSummaryTable.Rows.Add(clientSummaryRow);
            contactTable.Rows.Add(contactRow);

            IOrganization orgCM = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            LossAndGainsReportDS securitySummaryReportDS = data as LossAndGainsReportDS;

            ManualTransactions(clientID, Entity.ClientManualAssetCollection, orgCM, securitySummaryReportDS);
            DistributionIncomes(Entity.DistributionIncomes, securitySummaryReportDS, Entity.ListAccountProcess, Broker, orgCM);
            IEnumerable<DividendEntity> accrualDivEntities = DividendIncomes(Entity.DividendCollection, securitySummaryReportDS, orgCM, Entity.ListAccountProcess, Broker);
            GetTransactionsInfoFromLinkProcess(orgUnit, accrualDivEntities, clientID, Broker, Entity.ListAccountProcess, orgCM, securitySummaryReportDS, Entity.DesktopBrokerAccounts);
            SetBankTransactionDetails(data, Entity.DistributionIncomes, "Income", "Distribution");
            Broker.ReleaseBrokerManagedComponent(orgCM);
        }

        private static void TDTransactions(ICMBroker Broker, IOrganization orgCM, LossAndGainsReportDS lossAndGainsReportDS, AccountProcessTaskEntity accountProcessTaskEntity)
        {
            BankAccountCM bankAccount = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as BankAccountCM;
            decimal openingBalance = 0;
            decimal closingBalance = 0;
            if (bankAccount != null)
            {
                var filteredTransactionsOpening = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate.Date <= lossAndGainsReportDS.StartDate.AddDays(-1));
                var filteredTransactionsClosing = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate.Date <= lossAndGainsReportDS.EndDate);

                var tdGroupsOpeningType = filteredTransactionsOpening.GroupBy(cashMan => cashMan.TransactionType);

                foreach (var tdType in tdGroupsOpeningType)
                {
                    var groupByIns = tdType.GroupBy(tdtypetran => tdtypetran.InstitutionID);

                    foreach (var groupins in groupByIns)
                    {
                        var transaction = groupins.FirstOrDefault();
                        decimal holding = groupins.Sum(inssum => inssum.Amount);
                        openingBalance += holding;
                    }
                }


                var tdGroupsClosingType = filteredTransactionsClosing.GroupBy(cashMan => cashMan.TransactionType);

                foreach (var tdType in tdGroupsClosingType)
                {
                    var groupByIns = tdType.GroupBy(tdtypetran => tdtypetran.InstitutionID);

                    foreach (var groupins in groupByIns)
                    {
                        var transaction = groupins.FirstOrDefault();
                        decimal holding = groupins.Sum(inssum => inssum.Amount);
                        closingBalance += holding;
                    }
                }

                var filteredTransactions = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate.Date <= lossAndGainsReportDS.EndDate && trans.TransactionDate.Date >= lossAndGainsReportDS.StartDate);

                decimal application = filteredTransactions.Where(tran => tran.SystemTransactionType == "Application").Sum(tran => tran.TotalAmount);
                decimal redemption = filteredTransactions.Where(tran => tran.SystemTransactionType == "Redemption").Sum(tran => tran.TotalAmount);

                var applicationTrans = filteredTransactions.Where(tran => tran.SystemTransactionType == "Application");
                var redemptionTrans = filteredTransactions.Where(tran => tran.SystemTransactionType == "Redemption");
                SetBankTransactionDetails(lossAndGainsReportDS, bankAccount, redemptionTrans, "Redemption", "Investment");
                SetBankTransactionDetails(lossAndGainsReportDS, bankAccount, applicationTrans, "Application", "Investment");

                string tranCategory = "Income";
                var interestIncomeTrans = filteredTransactions.Where(tran => tran.SystemTransactionType == "Interest");
                SetBankTransactionDetails(lossAndGainsReportDS, bankAccount, interestIncomeTrans, tranCategory, "Interest");
                decimal interestIncome = interestIncomeTrans.Sum(tran => tran.TotalAmount);

                DataTable incomeBreakDownTable = lossAndGainsReportDS.Tables[LossAndGainsReportDS.INCOMEBREAKDOWN];

                foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in interestIncomeTrans)
                {
                    string description = bankAccount.BankAccountEntity.AccoutType.ToUpper();
                    var institute = orgCM.Institution.Where(ins => ins.ID == cashEntity.InstitutionID).FirstOrDefault();

                    if (institute != null)
                        description = institute.Name;

                    DataRow incomeRow = incomeBreakDownTable.NewRow();
                    incomeRow[LossAndGainsReportDS.DESCRIPTION] = description + " " + bankAccount.BankAccountEntity.CustomerNo;
                    incomeRow[LossAndGainsReportDS.SECNAME] = cashEntity.AdministrationSystem.ToUpper();
                    incomeRow[LossAndGainsReportDS.SECTYPE] = cashEntity.TransactionType.ToUpper();
                    incomeRow[LossAndGainsReportDS.INCOMETYPE] = "INTEREST";
                    incomeRow[LossAndGainsReportDS.SOURCE] = cashEntity.TransactionType.ToUpper();
                    incomeRow[LossAndGainsReportDS.PAYMENTDATE] = cashEntity.TransactionDate;
                    incomeRow[LossAndGainsReportDS.INCPAID] = cashEntity.TotalAmount;
                    incomeBreakDownTable.Rows.Add(incomeRow);
                }

                lossAndGainsReportDS.AddTaxSummaryRow(bankAccount.CID, "TDs", bankAccount.BankAccountEntity.Name, bankAccount.BankAccountEntity.BSB + "-" + bankAccount.BankAccountEntity.AccountNumber,
                         openingBalance, 0, interestIncome, 0, 0, 0, 0, 0, 0, 0, 0, 0, closingBalance, (application + redemption), closingBalance, 0);
            }
        }

        private static void BankAccountTransctions(ObservableCollection<DistributionIncomeEntity> DistributionIncomes, IEnumerable<DividendEntity> accrualDivEntities, ICMBroker Broker, AccountProcessTaskEntity accountProcessTaskEntity, LossAndGainsReportDS lossAndGainsReportDS, IOrganization orgCM)
        {
            BankAccountCM bankAccount = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as BankAccountCM;
            if (bankAccount != null)
            {
                var filteredTransactionsStartDate = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate <= lossAndGainsReportDS.StartDate.AddDays(-1));
                var filteredTransactionsEndDat = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate <= lossAndGainsReportDS.EndDate);
                decimal openingBalance = filteredTransactionsStartDate.Sum(calc => calc.TotalAmount);
                decimal closingBalance = filteredTransactionsEndDat.Sum(calc => calc.TotalAmount);

                var totalRangeTransactions = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate.Date <= lossAndGainsReportDS.EndDate.Date && trans.TransactionDate.Date >= lossAndGainsReportDS.StartDate.Date);
                var totalRangeTransactionsPlusSixMonths = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate.Date <= lossAndGainsReportDS.EndDate.Date.AddMonths(6) && trans.TransactionDate.Date >= lossAndGainsReportDS.EndDate.Date);
                decimal contributions = totalRangeTransactions.Where(tran => tran.Category == "Contribution").Sum(tran => tran.TotalAmount);
                decimal income = totalRangeTransactions.Where(tran => tran.Category == "Income").Sum(tran => tran.TotalAmount);

                var interestIncomeTrans = totalRangeTransactions.Where(tran => tran.Category == "Income" && tran.SystemTransactionType == "Interest");
                List<Oritax.TaxSimp.Common.CashManagementEntity> distributionIncomeTrans = totalRangeTransactions.Where(tran => tran.Category == "Income" && tran.SystemTransactionType == "Distribution").ToList();
                var dividendInvoceTrans = totalRangeTransactions.Where(tran => tran.Category == "Income" && tran.SystemTransactionType == "Dividend");
                var rentalIncomeTrans = totalRangeTransactions.Where(tran => tran.Category == "Income" && tran.SystemTransactionType == "Rental Income");
                var commissionRebateTrans = totalRangeTransactions.Where(tran => tran.Category == "Income" && tran.SystemTransactionType == "Commission Rebate");
                var dividendInvoceTransPlus = totalRangeTransactionsPlusSixMonths.Where(tran => tran.Category == "Income" && tran.SystemTransactionType == "Dividend");

                var espenses = totalRangeTransactions.Where(tran => tran.Category == "Expense" && tran.SystemTransactionType.ToLower() != "transfer out");

                DataTable expenseBreakDownTable = lossAndGainsReportDS.Tables[LossAndGainsReportDS.EXPENSEBREAKDOWN];

                foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntityExpense in espenses)
                {
                    DataRow expRow = expenseBreakDownTable.NewRow();
                    expRow[LossAndGainsReportDS.BSB] = bankAccount.BankAccountEntity.BSB;
                    expRow[LossAndGainsReportDS.CASH_BANKACC] = bankAccount.BankAccountEntity.AccountNumber;
                    expRow[LossAndGainsReportDS.SECTYPE] = "CASH";
                    expRow[LossAndGainsReportDS.SOURCE] = "CASH";
                    expRow[LossAndGainsReportDS.TRANSACTIONTYPE] = cashEntityExpense.ImportTransactionType;
                    expRow[LossAndGainsReportDS.SYSTEMTRANSACTIONTYPE] = cashEntityExpense.SystemTransactionType;
                    expRow[LossAndGainsReportDS.TRANCATEGORY] = cashEntityExpense.Category;
                    expRow[LossAndGainsReportDS.PAYMENTDATE] = cashEntityExpense.TransactionDate;
                    decimal originalAmount = cashEntityExpense.TotalAmount / new Decimal(1.1);
                    decimal gst = cashEntityExpense.TotalAmount - originalAmount;
                    expRow[LossAndGainsReportDS.EXPENSE] = originalAmount;
                    expRow[LossAndGainsReportDS.GST] = gst;
                    expRow[LossAndGainsReportDS.TOTAL] = cashEntityExpense.TotalAmount;
                    expRow[LossAndGainsReportDS.COMMENT] = cashEntityExpense.Comment;
                    expenseBreakDownTable.Rows.Add(expRow);
                }

                string tranCategory = "Income";

                DataTable incomeBreakDownTable = lossAndGainsReportDS.Tables[LossAndGainsReportDS.INCOMEBREAKDOWN];
                foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in rentalIncomeTrans)
                {
                    DataRow incomeRow = incomeBreakDownTable.NewRow();
                    incomeRow[LossAndGainsReportDS.DESCRIPTION] = bankAccount.BankAccountEntity.AccountNumber;
                    incomeRow[LossAndGainsReportDS.SECNAME] = bankAccount.BankAccountEntity.AccountNumber;
                    incomeRow[LossAndGainsReportDS.SECTYPE] = "CASH";
                    incomeRow[LossAndGainsReportDS.SOURCE] = "CASH";
                    incomeRow[LossAndGainsReportDS.INCOMETYPE] = "RENTAL";
                    incomeRow[LossAndGainsReportDS.PAYMENTDATE] = cashEntity.TransactionDate;
                    incomeRow[LossAndGainsReportDS.INCPAID] = cashEntity.TotalAmount;
                    incomeBreakDownTable.Rows.Add(incomeRow);
                }

                foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in interestIncomeTrans)
                {
                    DataRow incomeRow = incomeBreakDownTable.NewRow();
                    incomeRow[LossAndGainsReportDS.DESCRIPTION] = bankAccount.BankAccountEntity.AccountNumber;
                    incomeRow[LossAndGainsReportDS.SECNAME] = bankAccount.BankAccountEntity.AccountNumber;
                    incomeRow[LossAndGainsReportDS.SECTYPE] = "CASH";
                    incomeRow[LossAndGainsReportDS.SOURCE] = "CASH";
                    incomeRow[LossAndGainsReportDS.INCOMETYPE] = "INTEREST";
                    incomeRow[LossAndGainsReportDS.PAYMENTDATE] = cashEntity.TransactionDate;
                    incomeRow[LossAndGainsReportDS.INCPAID] = cashEntity.TotalAmount;
                    incomeBreakDownTable.Rows.Add(incomeRow);
                }

                foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in commissionRebateTrans)
                {
                    DataRow incomeRow = incomeBreakDownTable.NewRow();
                    incomeRow[LossAndGainsReportDS.DESCRIPTION] = bankAccount.BankAccountEntity.AccountNumber;
                    incomeRow[LossAndGainsReportDS.SECNAME] = bankAccount.BankAccountEntity.AccountNumber;
                    incomeRow[LossAndGainsReportDS.SECTYPE] = "CASH";
                    incomeRow[LossAndGainsReportDS.SOURCE] = "CASH";
                    incomeRow[LossAndGainsReportDS.INCOMETYPE] = "INTEREST";
                    incomeRow[LossAndGainsReportDS.PAYMENTDATE] = cashEntity.TransactionDate;
                    incomeRow[LossAndGainsReportDS.INCPAID] = cashEntity.TotalAmount;
                    incomeBreakDownTable.Rows.Add(incomeRow);
                }

                foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in dividendInvoceTrans)
                {
                    DataRow matchedRow = incomeBreakDownTable.Select().Where(row => Math.Floor((decimal)row[LossAndGainsReportDS.INCPAID]) == Math.Floor(cashEntity.TotalAmount) &&
                                                         ((DateTime)row[LossAndGainsReportDS.PAYMENTDATE]).Date == cashEntity.TransactionDate.Date && row[LossAndGainsReportDS.INCOMETYPE].ToString() == "DIVIDEND").FirstOrDefault();

                    if (matchedRow != null)
                    {
                        matchedRow[LossAndGainsReportDS.CASH_BANKACC] = bankAccount.BankAccountEntity.AccountNumber;
                        matchedRow[LossAndGainsReportDS.CASH_INCOMETYPE] = "DIVIDEND";
                        matchedRow[LossAndGainsReportDS.SOURCE] = "SEC";
                        matchedRow[LossAndGainsReportDS.CASH_PAYMENTDATE] = cashEntity.TransactionDate;
                        matchedRow[LossAndGainsReportDS.CASH_AMOUNT] = cashEntity.TotalAmount;
                    }
                    else
                    {
                        var matchedWithAccrualItem = accrualDivEntities.Where(tran => Math.Floor(Convert.ToDecimal(tran.PaidDividend)) == Math.Floor(cashEntity.TotalAmount) &&
                                                         tran.PaymentDate.Value.Date.Date == cashEntity.TransactionDate.Date).FirstOrDefault();

                        if (matchedWithAccrualItem == null || lossAndGainsReportDS.AcountingMethod == AcountingMethodType.Cash)
                        {
                            string description = cashEntity.Comment;

                            var secDetails = orgCM.Securities.Where(sec => cashEntity.Comment.Contains(sec.AsxCode)).FirstOrDefault();
                            if (secDetails != null)
                                description = secDetails.CompanyName;

                            DataRow incomeRow = incomeBreakDownTable.NewRow();
                            incomeRow[LossAndGainsReportDS.DESCRIPTION] = description;
                            incomeRow[LossAndGainsReportDS.SECNAME] = bankAccount.BankAccountEntity.AccountNumber;
                            incomeRow[LossAndGainsReportDS.SECTYPE] = "DIVIDEND";
                            incomeRow[LossAndGainsReportDS.SOURCE] = "CASH";
                            incomeRow[LossAndGainsReportDS.INCOMETYPE] = "DIVIDEND";
                            incomeRow[LossAndGainsReportDS.PAYMENTDATE] = cashEntity.TransactionDate;
                            incomeRow[LossAndGainsReportDS.INCPAID] = cashEntity.TotalAmount;
                            incomeBreakDownTable.Rows.Add(incomeRow);
                        }
                    }
                }

                foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in dividendInvoceTransPlus)
                {
                    DataRow matchedRow = incomeBreakDownTable.Select().Where(row => Math.Floor((decimal)row[LossAndGainsReportDS.INCPAID]) == Math.Floor(cashEntity.TotalAmount) &&
                                                         ((DateTime)row[LossAndGainsReportDS.PAYMENTDATE]).Date == cashEntity.TransactionDate.Date && row[LossAndGainsReportDS.INCOMETYPE].ToString() == "DIVIDEND").FirstOrDefault();

                    if (matchedRow != null)
                    {
                        matchedRow[LossAndGainsReportDS.CASH_BANKACC] = bankAccount.BankAccountEntity.AccountNumber;
                        matchedRow[LossAndGainsReportDS.CASH_INCOMETYPE] = "DIVIDEND";
                        matchedRow[LossAndGainsReportDS.SOURCE] = "SEC";
                        matchedRow[LossAndGainsReportDS.CASH_PAYMENTDATE] = cashEntity.TransactionDate;
                        matchedRow[LossAndGainsReportDS.CASH_AMOUNT] = cashEntity.TotalAmount;
                    }
                }


                foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in distributionIncomeTrans)
                {
                    DataRow matchedRow = incomeBreakDownTable.Select().Where(row => Math.Round((decimal)row[LossAndGainsReportDS.INCPAID], 0) == Math.Round(cashEntity.TotalAmount, 0) &&
                                                         ((DateTime)row[LossAndGainsReportDS.PAYMENTDATE]).Date == cashEntity.TransactionDate.Date && row[LossAndGainsReportDS.INCOMETYPE].ToString() == "DISTRIBUTION").FirstOrDefault();

                    if (matchedRow != null)
                    {
                        matchedRow[LossAndGainsReportDS.CASH_BANKACC] = bankAccount.BankAccountEntity.AccountNumber;
                        matchedRow[LossAndGainsReportDS.CASH_INCOMETYPE] = "DISTRIBUTION";
                        matchedRow[LossAndGainsReportDS.SOURCE] = "FUND";
                        matchedRow[LossAndGainsReportDS.CASH_PAYMENTDATE] = cashEntity.TransactionDate;
                        matchedRow[LossAndGainsReportDS.CASH_AMOUNT] = cashEntity.TotalAmount;
                    }
                    //else
                    //{
                    //    string description = cashEntity.Comment;

                    //    var secDetails = orgCM.Securities.Where(sec => sec.InvestmentType != "Share").Where(sec => cashEntity.Comment.Contains(sec.AsxCode)).FirstOrDefault();
                    //    if (secDetails != null && secDetails.CompanyName != string.Empty)
                    //        description = secDetails.CompanyName;

                    //    DataRow incomeRow = incomeBreakDownTable.NewRow();
                    //    incomeRow[LossAndGainsReportDS.DESCRIPTION] = description;
                    //    incomeRow[LossAndGainsReportDS.SECNAME] = bankAccount.BankAccountEntity.AccountNumber;
                    //    incomeRow[LossAndGainsReportDS.SECTYPE] = "DISTRIBUTION";
                    //    incomeRow[LossAndGainsReportDS.SOURCE] = "CASH";
                    //    incomeRow[LossAndGainsReportDS.INCOMETYPE] = "DISTRIBUTION";
                    //    incomeRow[LossAndGainsReportDS.PAYMENTDATE] = cashEntity.TransactionDate;
                    //    incomeRow[LossAndGainsReportDS.INCPAID] = cashEntity.TotalAmount;
                    //    incomeBreakDownTable.Rows.Add(incomeRow);
                    //}
                }


                IEnumerable<DistributionIncomeEntity> filteredDistribution = null;

                if (lossAndGainsReportDS.AcountingMethod == AcountingMethodType.Cash)
                    filteredDistribution = DistributionIncomes.Where(cliFundTrans => cliFundTrans.PaymentDate.Value.Date >= lossAndGainsReportDS.StartDate.Date && cliFundTrans.PaymentDate.Value.Date <= lossAndGainsReportDS.EndDate.Date);
                else
                    filteredDistribution = DistributionIncomes.Where(cliFundTrans => cliFundTrans.RecordDate.Value.Date >= lossAndGainsReportDS.StartDate.Date && cliFundTrans.RecordDate.Value.Date <= lossAndGainsReportDS.EndDate.Date);

                decimal closingBalanceAdjustment = 0;
                decimal distributionIncome = distributionIncomeTrans.Sum(tran => tran.TotalAmount);

                foreach (var tranDisCashItem in distributionIncomeTrans.ToList())
                {
                    var existingDistribution = filteredDistribution.Where(dis => Math.Floor(dis.NetCashDistribution.Value) == Math.Floor((double)tranDisCashItem.TotalAmount)).FirstOrDefault();
                    if (existingDistribution == null)
                    {
                        distributionIncomeTrans.Remove(tranDisCashItem);
                        closingBalanceAdjustment += tranDisCashItem.TotalAmount;
                    }
                }

                SetBankTransactionDetails(lossAndGainsReportDS, bankAccount, interestIncomeTrans, tranCategory, "Interest");
                SetBankTransactionDetails(lossAndGainsReportDS, bankAccount, distributionIncomeTrans, tranCategory, "Distribution");
                SetBankTransactionDetails(lossAndGainsReportDS, bankAccount, dividendInvoceTrans, tranCategory, "Dividend");
                SetBankTransactionDetails(lossAndGainsReportDS, bankAccount, rentalIncomeTrans, tranCategory, "Rental Income");
                SetBankTransactionDetails(lossAndGainsReportDS, bankAccount, commissionRebateTrans, tranCategory, "Interest");

                decimal interestIncome = interestIncomeTrans.Sum(tran => tran.TotalAmount);
                decimal distributionIncomeAdj = distributionIncomeTrans.Sum(tran => tran.TotalAmount);
                decimal dividendInvoce = dividendInvoceTrans.Sum(tran => tran.TotalAmount);
                decimal rentalIncome = rentalIncomeTrans.Sum(tran => tran.TotalAmount);
                decimal commissionRebate = commissionRebateTrans.Sum(tran => tran.TotalAmount);
                interestIncome += commissionRebate;

                decimal transferin = totalRangeTransactions.Where(tran => tran.Category == "Income" && tran.SystemTransactionType == "Transfer In").Sum(tran => tran.TotalAmount);

                var transferinTrans = totalRangeTransactions.Where(tran => tran.Category == "Income" && tran.SystemTransactionType == "Transfer In");
                SetBankTransactionDetails(lossAndGainsReportDS, bankAccount, transferinTrans, "Transfer In", "Transfer In");

                decimal internalCashMovementIncome = totalRangeTransactions.Where(tran => tran.Category == "Income" && tran.SystemTransactionType == "Internal Cash Movement").Sum(tran => tran.TotalAmount);
                var internalCashMovementIncomeTrans = totalRangeTransactions.Where(tran => tran.Category == "Income" && tran.SystemTransactionType == "Internal Cash Movement");
                SetBankTransactionDetails(lossAndGainsReportDS, bankAccount, internalCashMovementIncomeTrans, "Movement", "Investment");

                decimal transferInOutOther = totalRangeTransactions.Where(tran => tran.SystemTransactionType == string.Empty).Sum(tran => tran.TotalAmount);
                income = income - transferin - internalCashMovementIncome - interestIncome - distributionIncome - dividendInvoce - rentalIncome;

                decimal taxinout = totalRangeTransactions.Where(tran => tran.Category == "TAX").Sum(tran => tran.TotalAmount); ;
                decimal benefitPayment = totalRangeTransactions.Where(tran => tran.Category == "Benefit Payment").Sum(tran => tran.TotalAmount);
                decimal investment = totalRangeTransactions.Where(tran => tran.Category == "Investment").Sum(tran => tran.TotalAmount);

                var taxTrans = totalRangeTransactions.Where(tran => tran.Category == "TAX");
                var expenseTrans = totalRangeTransactions.Where(tran => tran.Category == "Expense");
                var investmentTrans = totalRangeTransactions.Where(tran => tran.Category == "Investment");

                SetBankTransactionDetails(lossAndGainsReportDS, bankAccount, taxTrans, "Expenses", "TAX");
                SetBankTransactionDetails(lossAndGainsReportDS, bankAccount, expenseTrans, "Expenses", "Expense");
                SetBankTransactionDetails(lossAndGainsReportDS, bankAccount, investmentTrans, "Investment", "Investment");

                decimal expense = totalRangeTransactions.Where(tran => tran.Category == "Expense").Sum(tran => tran.TotalAmount);
                decimal transferOut = totalRangeTransactions.Where(tran => tran.Category == "Expense" && tran.SystemTransactionType == "Transfer Out").Sum(tran => tran.TotalAmount);

                var transferOutTrans = totalRangeTransactions.Where(tran => tran.Category == "Expense" && tran.SystemTransactionType == "Transfer Out");
                SetBankTransactionDetails(lossAndGainsReportDS, bankAccount, transferOutTrans, "Transfer Out", "Transfer Out");

                decimal internalCashMovementExpense = totalRangeTransactions.Where(tran => tran.Category == "Expense" && tran.SystemTransactionType == "Internal Cash Movement").Sum(tran => tran.TotalAmount);
                var internalCashMovementExpenseTrans = totalRangeTransactions.Where(tran => tran.Category == "Expense" && tran.SystemTransactionType == "Internal Cash Movement");
                SetBankTransactionDetails(lossAndGainsReportDS, bankAccount, internalCashMovementExpenseTrans, "Movement", "Investment");

                expense = expense - transferOut - internalCashMovementExpense;

                lossAndGainsReportDS.AddTaxSummaryRow(bankAccount.CID, "CASH", bankAccount.BankAccountEntity.Name, bankAccount.BankAccountEntity.BSB + "-" + bankAccount.BankAccountEntity.AccountNumber,
                                openingBalance, investment, interestIncome, rentalIncome, distributionIncome,
                                dividendInvoce, income, (transferin + transferInOutOther + contributions), transferOut, (expense + taxinout), 0, 0, closingBalance, internalCashMovementIncome + internalCashMovementExpense, closingBalance - closingBalanceAdjustment, distributionIncomeAdj);
            }
        }

        private static void SetBankTransactionDetails(LossAndGainsReportDS lossAndGainsReportDS, BankAccountCM bankAccount, IEnumerable<Common.CashManagementEntity> transactions, string tranCategory, string tranSysCategory)
        {
            foreach (Oritax.TaxSimp.Common.CashManagementEntity transaction in transactions)
            {
                DataRow row = lossAndGainsReportDS.Tables[LossAndGainsReportDS.BANKSTRANSACTIONDETAILS].NewRow();
                row[LossAndGainsReportDS.ID] = bankAccount.CID;
                row[LossAndGainsReportDS.SYSTRANSACIONTYPE] = tranSysCategory;
                row[LossAndGainsReportDS.TRANSACTIONCATEGORY] = tranCategory;
                row[LossAndGainsReportDS.TRANSACTIONDATE] = transaction.TransactionDate;
                row[LossAndGainsReportDS.ACCOUNTTYPE] = "CASH";
                row[LossAndGainsReportDS.ACCOUNTNO] = bankAccount.BankAccountEntity.BSB + "-" + bankAccount.BankAccountEntity.AccountNumber;
                row[LossAndGainsReportDS.ACCOUNTNAME] = bankAccount.BankAccountEntity.Name;
                row[LossAndGainsReportDS.TOTAL] = transaction.TotalAmount;
                row[LossAndGainsReportDS.DESCRIPTION] = transaction.Comment;

                lossAndGainsReportDS.Tables[LossAndGainsReportDS.BANKSTRANSACTIONDETAILS].Rows.Add(row);
            }
        }

        private static void SetBankTransactionDetails(LossAndGainsReportDS lossAndGainsReportDS, ObservableCollection<DistributionIncomeEntity> DistributionIncomes, string tranCategory, string tranSysCategory)
        {
            IEnumerable<DistributionIncomeEntity> filteredDistribution = null;

            if (lossAndGainsReportDS.AcountingMethod == AcountingMethodType.Cash)
                filteredDistribution = DistributionIncomes.Where(cliFundTrans => cliFundTrans.PaymentDate.Value.Date >= lossAndGainsReportDS.StartDate.Date && cliFundTrans.PaymentDate.Value.Date <= lossAndGainsReportDS.EndDate.Date);
            else
                filteredDistribution = DistributionIncomes.Where(cliFundTrans => cliFundTrans.RecordDate.Value.Date >= lossAndGainsReportDS.StartDate.Date && cliFundTrans.RecordDate.Value.Date <= lossAndGainsReportDS.EndDate.Date);

            foreach (DistributionIncomeEntity distributionIncomeEntity in filteredDistribution)
            {
                var existingDistribution = lossAndGainsReportDS.Tables[LossAndGainsReportDS.BANKSTRANSACTIONDETAILS].Select().Where(row => Math.Floor(double.Parse(row[LossAndGainsReportDS.TOTAL].ToString())) == Math.Floor(distributionIncomeEntity.NetCashDistribution.Value)).FirstOrDefault();

                if (existingDistribution == null)
                {
                    DataRow row = lossAndGainsReportDS.Tables[LossAndGainsReportDS.BANKSTRANSACTIONDETAILS].NewRow();
                    row[LossAndGainsReportDS.ID] = Guid.Empty;
                    row[LossAndGainsReportDS.SYSTRANSACIONTYPE] = tranSysCategory;
                    row[LossAndGainsReportDS.TRANSACTIONCATEGORY] = tranCategory;
                    row[LossAndGainsReportDS.TRANSACTIONDATE] = distributionIncomeEntity.RecordDate;
                    row[LossAndGainsReportDS.ACCOUNTTYPE] = "CASH";
                    row[LossAndGainsReportDS.ACCOUNTNO] = "Not Matched";
                    row[LossAndGainsReportDS.ACCOUNTNAME] = "Not Matched";
                    row[LossAndGainsReportDS.TOTAL] = distributionIncomeEntity.NetCashDistribution;
                    row[LossAndGainsReportDS.DESCRIPTION] = distributionIncomeEntity.FundCode + " " + distributionIncomeEntity.FundName;
                    lossAndGainsReportDS.Tables[LossAndGainsReportDS.BANKSTRANSACTIONDETAILS].Rows.Add(row);
                    lossAndGainsReportDS.AddTaxSummaryRowMissingDistributions(distributionIncomeEntity.ID, "CASH NR", "PAYMENT NOT RECV. IN PERIOD FOR (" + distributionIncomeEntity.FundCode + ")", "PAYMENT NOT RECV. IN PERIOD FOR (" + distributionIncomeEntity.FundCode + ")", (decimal)distributionIncomeEntity.NetCashDistribution.Value);
                }
            }
        }

        private static void SetMISTransactionDetails(LossAndGainsReportDS lossAndGainsReportDS, SecuritiesEntity securitiesEntity, IEnumerable<MISFundTransactionEntity> transactions, string tranCategory, string tranSysCategory)
        {
            foreach (MISFundTransactionEntity transaction in transactions)
            {
                DataRow row = lossAndGainsReportDS.Tables[LossAndGainsReportDS.BANKSTRANSACTIONDETAILS].NewRow();
                row[LossAndGainsReportDS.ID] = securitiesEntity.ID;
                row[LossAndGainsReportDS.SYSTRANSACIONTYPE] = tranSysCategory;
                row[LossAndGainsReportDS.TRANSACTIONCATEGORY] = tranCategory;
                row[LossAndGainsReportDS.TRANSACTIONDATE] = transaction.TradeDate;
                row[LossAndGainsReportDS.ACCOUNTTYPE] = "FUND";
                row[LossAndGainsReportDS.ACCOUNTNO] = securitiesEntity.AsxCode;
                row[LossAndGainsReportDS.ACCOUNTNAME] = securitiesEntity.CompanyName;
                row[LossAndGainsReportDS.TOTAL] = transaction.Amount;
                row[LossAndGainsReportDS.DESCRIPTION] = transaction.ClientID;

                lossAndGainsReportDS.Tables[LossAndGainsReportDS.BANKSTRANSACTIONDETAILS].Rows.Add(row);
            }
        }

        private static void SetASXTransactionDetails(LossAndGainsReportDS lossAndGainsReportDS, Guid ID, DesktopBrokerAccountEntity desktopBrokerAccountEntity, IEnumerable<HoldingTransactions> transactions, string tranCategory, string tranSysCategory)
        {
            foreach (HoldingTransactions transaction in transactions)
            {
                DataRow row = lossAndGainsReportDS.Tables[LossAndGainsReportDS.BANKSTRANSACTIONDETAILS].NewRow();
                row[LossAndGainsReportDS.ID] = ID;
                row[LossAndGainsReportDS.SYSTRANSACIONTYPE] = tranSysCategory;
                row[LossAndGainsReportDS.TRANSACTIONCATEGORY] = tranCategory;
                row[LossAndGainsReportDS.TRANSACTIONDATE] = transaction.TradeDate;
                row[LossAndGainsReportDS.ACCOUNTTYPE] = "ASX";
                row[LossAndGainsReportDS.ACCOUNTNO] = desktopBrokerAccountEntity.HINNumber;
                row[LossAndGainsReportDS.ACCOUNTNAME] = desktopBrokerAccountEntity.Name;

                if (transaction.Type == "RA")
                    row[LossAndGainsReportDS.TOTAL] = transaction.NetValue * -1;
                else
                    row[LossAndGainsReportDS.TOTAL] = transaction.NetValue;

                row[LossAndGainsReportDS.DESCRIPTION] = transaction.TransactionType;

                lossAndGainsReportDS.Tables[LossAndGainsReportDS.BANKSTRANSACTIONDETAILS].Rows.Add(row);
            }
        }

    }
}
