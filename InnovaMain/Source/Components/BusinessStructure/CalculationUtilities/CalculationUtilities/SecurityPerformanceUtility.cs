﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Data;
using System.Collections.ObjectModel;
using System.Data;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.CM
{
    public class SecurityPerformanceUtility
    {
        public SecurityPerformanceUtility()
        { }

        public void SecurityPerformance(IOrganization orgCM, PerfReportDS perfReportDS, ProductEntity product)
        {
            SecuritiesEntity security = null;
            SecuritiesEntity benchmark = null;
            Dictionary<string, ObservableCollection<ModifiedDietz>> securitiesPriceCollection = new Dictionary<string, ObservableCollection<ModifiedDietz>>();
            Dictionary<string, ObservableCollection<ModifiedDietz>> securitiesGrowthCollection = new Dictionary<string, ObservableCollection<ModifiedDietz>>();

            if (product != null && product.FundAccounts != null)
            {

                Guid fundId = product.FundAccounts.FirstOrDefault();
                security = orgCM.Securities.Where(details => details.FundID == fundId).FirstOrDefault();


                SecurityPerformanceSingle(orgCM, perfReportDS, security, securitiesPriceCollection, securitiesGrowthCollection);
                InvestmentManagementEntity investment = orgCM.InvestmentManagementDetails.Where(r => r.ID == product.ID).FirstOrDefault();
                if (investment != null)
                {
                    benchmark = orgCM.Securities.Where(details => details.ID == investment.Benchmark).FirstOrDefault();
                    SecurityPerformanceSingle(orgCM, perfReportDS, benchmark, securitiesPriceCollection, securitiesGrowthCollection);
                }
            }

            foreach (var dietzSec in securitiesPriceCollection)
            {
                ModifiedDietz.SetChainLinksOpeningBalance(dietzSec.Value);
                ModifiedDietz.CalculateChainLinksForModifiedDietzCollection(dietzSec.Value);
                PerformanceFlowReportUtility.SetPerformanceSummaryData(perfReportDS.Tables[PerfReportDS.PERFSUMMARYTABLE], dietzSec.Value, dietzSec.Key, perfReportDS.EndDate);
            }
            if (benchmark != null)
            {
                SetPerformanceSummaryDataAsDiference(perfReportDS.Tables[PerfReportDS.PERFSUMMARYTABLE], security.AsxCode, benchmark.AsxCode, "Excess");
            }

            foreach (var dietzSec in securitiesGrowthCollection)
            {
                ModifiedDietz.SetChainLinksOpeningBalance(dietzSec.Value);
                ModifiedDietz.CalculateChainLinksForModifiedDietzCollection(dietzSec.Value);

                foreach (ModifiedDietz periodPerformance in dietzSec.Value)
                {
                    DateTime startDate = AccountingFinancialYear.FirstDayOfMonthFromDateTime(periodPerformance.Month);
                    DateTime endDate = AccountingFinancialYear.LastDayOfMonthFromDateTime(periodPerformance.Month);
                    ExtractData_PERFSUMMARYINCOMEGROWTHTABLE(perfReportDS, startDate, endDate, dietzSec.Key, dietzSec.Key, periodPerformance);
                }
            }
        }


        private void SetPerformanceSummaryDataAsDiference(DataTable performanceSummaryTable, string code1, string code2, string desc)
        {

            DataRow mainRow = performanceSummaryTable.Select(string.Format("{0} = '{1}'", PerfReportDS.DESCRIPTION, code1)).FirstOrDefault();
            DataRow benchRow = performanceSummaryTable.Select(string.Format("{0} = '{1}'", PerfReportDS.DESCRIPTION, code2)).FirstOrDefault();

            if (mainRow == null || benchRow == null)
                return;


            DataRow performRow = performanceSummaryTable.NewRow();
            performRow[PerfReportDS.DESCRIPTION] = desc;
            performanceSummaryTable.Rows.Add(performRow);

            SetColumnDifference(mainRow, benchRow, performRow, PerfReportDS.ONEMONTH);
            SetColumnDifference(mainRow, benchRow, performRow, PerfReportDS.THREEMONTH);
            SetColumnDifference(mainRow, benchRow, performRow, PerfReportDS.SIXMONTH);
            SetColumnDifference(mainRow, benchRow, performRow, PerfReportDS.TWELVEMONTH);
            SetColumnDifference(mainRow, benchRow, performRow, PerfReportDS.ONEYEAR);
            SetColumnDifference(mainRow, benchRow, performRow, PerfReportDS.TWOYEAR);
            SetColumnDifference(mainRow, benchRow, performRow, PerfReportDS.THREEYEAR);
            SetColumnDifference(mainRow, benchRow, performRow, PerfReportDS.FIVEYEAR);
        }

        private static void SetColumnDifference(DataRow mainRow, DataRow benchRow, DataRow performRow, string colName)
        {
            if (((string)benchRow[colName]) == "-")
                performRow[colName] = mainRow[colName];
            else if (((string)mainRow[colName]) == "-")
                performRow[colName] = "-" + benchRow[colName];//reverse sign on benchrow
            else
            {
                decimal d1 = Decimal.Parse(mainRow[colName].ToString().Replace("%", "").ReplaceEmptyWithNull());
                decimal d2 = Decimal.Parse(benchRow[colName].ToString().Replace("%", "").ReplaceEmptyWithNull());
                performRow[colName] = ((d1 - d2) / 100).ToString("P");
            }
        }


        private void SecurityPerformanceSingle(IOrganization orgCM, PerfReportDS perfReportDS, SecuritiesEntity security, Dictionary<string, ObservableCollection<ModifiedDietz>> securitiesPriceCollection, Dictionary<string, ObservableCollection<ModifiedDietz>> securitiesGrowthCollection)
        {
            decimal openingGrowthBalance = 10000;
            if (perfReportDS.IsRange)
            {
                DateTime startdate = perfReportDS.StartDate;
                DateTime endDate = perfReportDS.EndDate;

                PopulateSecuritiesMonthlyClx(orgCM, perfReportDS, startdate, endDate, security, securitiesPriceCollection, securitiesGrowthCollection, openingGrowthBalance);
            }

            else
            {
                DateTimeSpan timeSpan = DateTimeSpan.CompareDates(perfReportDS.StartDate, perfReportDS.EndDate);

                int nMonthDiff_FirstToFirst = perfReportDS.EndDate.Month - perfReportDS.StartDate.Month + ((perfReportDS.EndDate.Year - perfReportDS.StartDate.Year) * 12);
                double dMonthDiff = timeSpan.TotalMonths();// (double)nMonthDiff_FirstToFirst + (perfReportDS.EndDate - perfReportDS.StartDate.AddMonths(nMonthDiff_FirstToFirst)).TotalDays / (double)DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month);

                for (int monthCounter = 0; monthCounter <= dMonthDiff; monthCounter++)
                {
                    DateTime startDate = AccountingFinancialYear.FirstDayOfMonthFromDateTime(perfReportDS.StartDate.AddMonths(monthCounter));
                    DateTime endDate = AccountingFinancialYear.LastDayOfMonthFromDateTime(perfReportDS.StartDate.AddMonths(monthCounter));

                    openingGrowthBalance = PopulateSecuritiesMonthlyClx(orgCM, perfReportDS, startDate, endDate, security, securitiesPriceCollection, securitiesGrowthCollection, openingGrowthBalance);
                }
            }

        }

        private decimal PopulateSecuritiesMonthlyClx(IOrganization orgCM, PerfReportDS perfReportDS, DateTime startDate, DateTime endDate, SecuritiesEntity security, Dictionary<string, ObservableCollection<ModifiedDietz>> securitiesPriceCollection, Dictionary<string, ObservableCollection<ModifiedDietz>> securitiesGrowthCollection, decimal openingGrowthBalance)
        {
            if (security != null && security.ASXSecurity != null && security.ASXSecurity.Count > 0)
            {
                var transactions = security.ASXSecurity.Where(h => h.Date >= startDate && h.Date <= endDate).OrderBy(h => h.Date);
                if (transactions.Count() > 0)
                {
                    ModifiedDietz dPrice = GetSecurityPriceDietz(orgCM, transactions, startDate, perfReportDS, security);
                    ModifiedDietz dGrowth = GetSecurityGrowthDietz(orgCM, transactions, startDate, perfReportDS, security, openingGrowthBalance);

                    AddToObservableClx(securitiesPriceCollection, security.AsxCode, dPrice);
                    AddToObservableClx(securitiesGrowthCollection, security.AsxCode, dGrowth);

                    return dGrowth.ClosingBalanceTotal;
                }
            }
            return openingGrowthBalance;
        }

        private static DataRow AddCapitalFlowSummaryRow(DataTable table, DateTime startdate, ModifiedDietz md)
        {
            DataRow row = table.NewRow();

            row[PerfReportDS.MONTH] = startdate;
            row[PerfReportDS.OPENINGBAL] = md.CapitalMovementByCategoryTotal.OpeningBalTotal;
            row[PerfReportDS.TRANSFEROUT] = md.CapitalMovementByCategoryTotal.TransferInOutTotal;
            row[PerfReportDS.INCOME] = md.CapitalMovementByCategoryTotal.IncomeTotal;
            row[PerfReportDS.APPLICATIONREDEMPTION] = md.CapitalMovementByCategoryTotal.ApplicationRedTotal;
            row[PerfReportDS.TAXINOUT] = md.CapitalMovementByCategoryTotal.TaxInOutTotal;
            row[PerfReportDS.EXPENSE] = md.CapitalMovementByCategoryTotal.ExpenseTotal;
            row[PerfReportDS.INTERNALCASHMOVEMENT] = md.CapitalMovementByCategoryTotal.InternalCashMovementTotal;
            row[PerfReportDS.CLOSINGBALANCE] = md.CapitalMovementByCategoryTotal.ClosingBalanceTotal;
            row[PerfReportDS.CHANGEININVESTMENTVALUE] = md.CapitalMovementByCategoryTotal.ChangeInInvestTotal;
            row[PerfReportDS.MODDIETZ] = md.ModDietz;
            row[PerfReportDS.SUMCF] = md.CfTotal;
            row[PerfReportDS.SUMWEIGHTCF] = md.WeightedCfTotal;
            row[PerfReportDS.GROWTHRETURN] = md.ChangeInMktValueReturn;
            row[PerfReportDS.INCOMERETURN] = md.IncomeReturn;
            row[PerfReportDS.OVERALLRETURN] = md.OverallReturn;
            table.Rows.Add(row);
            return row;
        }
        private ModifiedDietz GetSecurityPriceDietz(IOrganization orgCM, IEnumerable<ASXSecurityEntity> transactions, DateTime startDate, PerfReportDS perfReportDS, SecuritiesEntity security)
        {
            decimal totalSharesOpening = 1;
            decimal openingBalance = 0;
            decimal totalSharesClosing = 1;
            decimal closingBalance = 0;

            ModifiedDietz dPrice = new ModifiedDietz();
            dPrice.Type = "PRICE";

            if (transactions.Count() > 0)
            {
                var startTransaction = transactions.FirstOrDefault();
                var endTransaction = transactions.LastOrDefault();

                decimal unitPrice = 0;
                if (startTransaction != null)
                {
                    unitPrice = Convert.ToDecimal(startTransaction.UnitPrice);
                    openingBalance = totalSharesOpening * unitPrice;
                }

                if (endTransaction != null)
                {
                    unitPrice = Convert.ToDecimal(endTransaction.UnitPrice);
                    closingBalance = totalSharesClosing * unitPrice;
                }


                //historical prices
                foreach (ASXSecurityEntity asxSecurityEntity in transactions)
                {
                    decimal price = new Decimal(asxSecurityEntity.UnitPrice);
                    dPrice.AddEntity(price, asxSecurityEntity.Date, "PRICE");
                }
                dPrice.Month = startDate;
                dPrice.OpeningBalTotal = dPrice.CapitalMovementByCategoryTotal.OpeningBalTotal = openingBalance;
                dPrice.ClosingBalanceTotal = dPrice.CapitalMovementByCategoryTotal.ClosingBalanceTotal = closingBalance;
                dPrice.ChangeInMktValue = dPrice.CapitalMovementByCategoryTotal.ChangeInInvestTotal = dPrice.ClosingBalanceTotal - dPrice.OpeningBalTotal;
            }
            return dPrice;
        }

        private ModifiedDietz GetSecurityGrowthDietz(IOrganization orgCM, IEnumerable<ASXSecurityEntity> transactions, DateTime startDate, PerfReportDS perfReportDS, SecuritiesEntity security, decimal openingBalance)
        {
            ModifiedDietz dGrowth = new ModifiedDietz();
            dGrowth.Type = "GROWTH";

            if (transactions.Count() > 0)
            {
                var startTransaction = transactions.FirstOrDefault();
                var endTransaction = transactions.LastOrDefault();

                decimal oldBalance = openingBalance;
                if (startTransaction != null)
                {
                    dGrowth.OpeningBalTotal = dGrowth.CapitalMovementByCategoryTotal.OpeningBalTotal = openingBalance;
                }


                double oldPrice = 0;
                double? distributionValue = 0;
                //historical prices
                foreach (ASXSecurityEntity asxSecurityEntity in transactions)
                {

                    if (security.DistributionCollection != null)
                    {
                        DistributionEntity distribution = security.DistributionCollection.Where(dis => dis.PaymentDate == asxSecurityEntity.Date).FirstOrDefault();
                        if (distribution != null)
                            distributionValue = distribution.Components.Sum(d => d.Unit_Amount);
                    }
                    else if (security.DividendCollection != null)
                    {
                        DividendEntity distribution = security.DividendCollection.Where(dis => dis.PaymentDate == asxSecurityEntity.Date).FirstOrDefault();

                        if (distribution != null)
                            distributionValue = distribution.PaidDividend;
                    }
                    double ratio = (double)(oldPrice == 0 ? 1 : (asxSecurityEntity.UnitPrice + distributionValue / 100) / oldPrice);
                    decimal newBalance = oldBalance * Convert.ToDecimal(ratio);
                    oldPrice = asxSecurityEntity.UnitPrice;
                    dGrowth.AddEntity(newBalance, asxSecurityEntity.Date, "GROWTH");
                    oldBalance = newBalance;

                }
                dGrowth.Month = startDate;
                dGrowth.ClosingBalanceTotal = dGrowth.CapitalMovementByCategoryTotal.ClosingBalanceTotal = oldBalance;
                dGrowth.ChangeInMktValue = dGrowth.CapitalMovementByCategoryTotal.ChangeInInvestTotal = dGrowth.ClosingBalanceTotal - dGrowth.OpeningBalTotal;

            }
            return dGrowth;
        }

        private static void AddToObservableClx(Dictionary<string, ObservableCollection<ModifiedDietz>> securitiesCollection, string key, ModifiedDietz md)
        {
            ObservableCollection<ModifiedDietz> secCol = new ObservableCollection<ModifiedDietz>();

            if (securitiesCollection.ContainsKey(key))
                secCol = securitiesCollection[key];
            else
                securitiesCollection.Add(key, secCol);

            secCol.Add(md);
        }

        private static void ExtractData_PERFSUMMARYINCOMEGROWTHTABLE(PerfReportDS perfReportDS, DateTime startDateGI, DateTime endDateGI, string desc, string code, ModifiedDietz periodPerformance)
        {
            DataRow row = perfReportDS.Tables[PerfReportDS.PERFSUMMARYINCOMEGROWTHTABLE].NewRow();

            row[PerfReportDS.DESCRIPTION] = desc;
            row[PerfReportDS.INVESMENTCODE] = code;
            row[PerfReportDS.STARTDATE] = startDateGI;
            row[PerfReportDS.ENDDATE] = endDateGI;
            row[PerfReportDS.OPENINGBAL] = periodPerformance.OpeningBalTotal;
            row[PerfReportDS.INCOME] = periodPerformance.Income;
            row[PerfReportDS.MOVEMENTEXCLINCOME] = periodPerformance.Movement;
            row[PerfReportDS.CHANGEININVESTMENTVALUE] = periodPerformance.ChangeInMktValue;
            row[PerfReportDS.CLOSINGBALANCE] = periodPerformance.ClosingBalanceTotal;
            row[PerfReportDS.SUMCF] = periodPerformance.CfTotalYear;
            row[PerfReportDS.INCOMERETURN] = periodPerformance.IncomeReturn;
            row[PerfReportDS.GROWTHRETURN] = periodPerformance.ChangeInMktValueReturn;
            row[PerfReportDS.OVERALLRETURN] = periodPerformance.OverallReturn;
            perfReportDS.Tables[PerfReportDS.PERFSUMMARYINCOMEGROWTHTABLE].Rows.Add(row);
        }

    }
}
