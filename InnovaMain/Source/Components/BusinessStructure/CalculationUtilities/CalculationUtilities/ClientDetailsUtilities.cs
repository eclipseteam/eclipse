﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using IdentityCMDetail = Oritax.TaxSimp.Common.IdentityCMDetail;
using Oritax.TaxSimp.CM.Group;

namespace Oritax.TaxSimp.CM
{
    public class ClientDetailsUtilities
    {
        public static void BasicClientDetails(string clientType, string clientID, string clientName, IdentityCM Parent, ICMBroker Broker, string ABN, string ACN, Common.DualAddressEntity Address, DataRow clientSummaryRow)
        {
            clientSummaryRow[HoldingRptDataSet.CLIENTID] = clientID;
            clientSummaryRow[HoldingRptDataSet.CLIENTNAME] = clientName;
            clientSummaryRow[HoldingRptDataSet.CIENTABN] = ABN;
            clientSummaryRow[HoldingRptDataSet.CLIENTACN] = ACN;
            clientSummaryRow[HoldingRptDataSet.CLIENTTYPE] = clientType;

            if (Parent != null && Parent.Clid != Guid.Empty && Parent.Csid != Guid.Empty)
            {
                AdvisorCM adviserUnit = Broker.GetCMImplementation(Parent.Clid, Parent.Csid) as AdvisorCM;
                if (adviserUnit != null)
                    clientSummaryRow[HoldingRptDataSet.ADVISERFULLNAME] = adviserUnit.AdvisorEntity.FullName;
                Broker.ReleaseBrokerManagedComponent(adviserUnit);
            }
            switch (clientType.ToLower())
            {
                case "clientsmsfindividualtrustee":
                case "clientindividual":
                case "clientsmsfcorporatetrustee":
                case "trust - individual trustee":
                case "trust - corporate trustee":
                    clientSummaryRow[HoldingRptDataSet.ADDRESSLINE1] = Address.BusinessAddress.Addressline1;
                    clientSummaryRow[HoldingRptDataSet.ADDRESSLINE2] = Address.BusinessAddress.Addressline2;
                    break;
                case "individual":
                case "joint":
                    clientSummaryRow[HoldingRptDataSet.ADDRESSLINE1] = Address.ResidentialAddress.Addressline1;
                    clientSummaryRow[HoldingRptDataSet.ADDRESSLINE2] = Address.ResidentialAddress.Addressline2;
                    break;

            }

        }

        public static void OnGetDataSetClientIndividual(DataSet data, string clientType, string clientID, string clientName, IdentityCM Parent, ICMBroker Broker, ClientIndividualEntity Entity)
        {
            DataTable clientSummaryTable = data.Tables[HoldingRptDataSet.CLIENTSUMMARYTABLE];
            DataTable contactTable = data.Tables[HoldingRptDataSet.CONTACTTABLE];

            DataRow clientSummaryRow = clientSummaryTable.NewRow();
            DataRow contactRow = contactTable.NewRow();

            BasicClientDetails(clientType, clientID, clientName, Parent, Broker, Entity.ABN, string.Empty, Entity.Address, clientSummaryRow);

            CSVInstance csvinstance = new CSVInstance(Broker);
            ClientIndividualEntityCSV _entitycsv = csvinstance.GetEntityConcreteInstance(Entity);
            foreach (var contact in _entitycsv.Applicants)
            {
                contactRow[HoldingRptDataSet.CONTACT_TITLE] = contact.PersonalTitle;
                contactRow[HoldingRptDataSet.CONTACT_NAME] = contact.Fullname;
                contactRow[HoldingRptDataSet.CONTACT_BIZNO] = contact.WorkPhoneNumber;
                contactRow[HoldingRptDataSet.CONTACT_MOBNO] = contact.MobilePhoneNumber;
                contactRow[HoldingRptDataSet.CONTACT_HNO] = contact.HomePhoneNumber;
                contactRow[HoldingRptDataSet.CONTACT_FAXNO] = contact.Facsimile;
                contactRow[HoldingRptDataSet.CONTACT_EMAIL] = contact.EmailAddress;
            }

            clientSummaryTable.Rows.Add(clientSummaryRow);
            contactTable.Rows.Add(contactRow);
        }

        public static void OnGetDataDataSetClientCorporate(DataSet data, string clientType, string clientID, string clientName, IdentityCM Parent, ICMBroker Broker, CorporateEntity Entity)
        {
            DataTable clientSummaryTable = data.Tables[HoldingRptDataSet.CLIENTSUMMARYTABLE];
            DataTable contactTable = data.Tables[HoldingRptDataSet.CONTACTTABLE];

            DataRow clientSummaryRow = clientSummaryTable.NewRow();
            DataRow contactRow = contactTable.NewRow();

            BasicClientDetails(clientType, clientID, clientName, Parent, Broker, Entity.ABN, Entity.ACN, Entity.Address, clientSummaryRow);

            CSVInstance csvinstance = new CSVInstance(Broker);
            CorporateCSV _entitycsv = csvinstance.GetEntityConcreteInstance(Entity);
            foreach (var contact in _entitycsv.Contacts)
            {
                contactRow[HoldingRptDataSet.CONTACT_TITLE] = contact.PersonalTitle;
                contactRow[HoldingRptDataSet.CONTACT_NAME] = contact.Fullname;
                contactRow[HoldingRptDataSet.CONTACT_BIZNO] = contact.WorkPhoneNumber;
                contactRow[HoldingRptDataSet.CONTACT_MOBNO] = contact.MobilePhoneNumber;
                contactRow[HoldingRptDataSet.CONTACT_HNO] = contact.HomePhoneNumber;
                contactRow[HoldingRptDataSet.CONTACT_FAXNO] = contact.Facsimile;
                contactRow[HoldingRptDataSet.CONTACT_EMAIL] = contact.EmailAddress;
            }

            clientSummaryTable.Rows.Add(clientSummaryRow);
            contactTable.Rows.Add(contactRow);
        }

        public static void GetClientDetailsDs(string ClientID, ClientDetailsDS dataSet, ClientIndividualEntity clientDetails, ICMBroker broker, string type)
        {
            if (clientDetails != null)
            {
                DataRow dr = dataSet.Tables[dataSet.ClientDetailsTable.TABLENAME].NewRow();
                dr[dataSet.ClientDetailsTable.CLIENTID] = ClientID;
                dr[dataSet.ClientDetailsTable.CLIENTNAME] = clientDetails.Name;
                dr[dataSet.ClientDetailsTable.DOITFORME] = clientDetails.Servicetype.DO_IT_FOR_ME;
                dr[dataSet.ClientDetailsTable.DOITWITHME] = clientDetails.Servicetype.DO_IT_WITH_ME;
                dr[dataSet.ClientDetailsTable.DOITYOURSELF] = clientDetails.Servicetype.DO_IT_YOURSELF;
                dr[dataSet.ClientDetailsTable.INVESTORTYPE] = type;
                dr[dataSet.ClientDetailsTable.ISCLIENT] = clientDetails.IsActive;
                dr[dataSet.ClientDetailsTable.STATUSFLAG] = clientDetails.OrganizationStatus;

                dataSet.Tables[dataSet.ClientDetailsTable.TABLENAME].Rows.Add(dr);
            }
        }

        public static void GetClientDetailsDs(string ClientID, ClientDetailsDS dataSet, ConsoleClient clientDetails, ICMBroker broker, string type)
        {
            if (clientDetails != null)
            {
                DataRow dr = dataSet.Tables[dataSet.ClientDetailsTable.TABLENAME].NewRow();
                dr[dataSet.ClientDetailsTable.CLIENTID] = ClientID;
                dr[dataSet.ClientDetailsTable.CLIENTNAME] = clientDetails.Name;
                dr[dataSet.ClientDetailsTable.INVESTORTYPE] = type;
                dr[dataSet.ClientDetailsTable.ISCLIENT] = clientDetails.IsActive;
                dr[dataSet.ClientDetailsTable.STATUSFLAG] = clientDetails.OrganizationStatus;

                dataSet.Tables[dataSet.ClientDetailsTable.TABLENAME].Rows.Add(dr);
            }
        }

        public static void GetClientDetailsDs(string ClientID, ClientDetailsDS dataSet, CorporateEntity clientDetails, ICMBroker broker, string type)
        {
            if (clientDetails != null)
            {
                DataRow dr = dataSet.Tables[dataSet.ClientDetailsTable.TABLENAME].NewRow();
                dr[dataSet.ClientDetailsTable.CLIENTID] = ClientID;
                dr[dataSet.ClientDetailsTable.CLIENTNAME] = clientDetails.Name;
                dr[dataSet.ClientDetailsTable.DOITFORME] = clientDetails.Servicetype.DO_IT_FOR_ME;
                dr[dataSet.ClientDetailsTable.DOITWITHME] = clientDetails.Servicetype.DO_IT_WITH_ME;
                dr[dataSet.ClientDetailsTable.DOITYOURSELF] = clientDetails.Servicetype.DO_IT_YOURSELF;
                dr[dataSet.ClientDetailsTable.INVESTORTYPE] = type;
                dr[dataSet.ClientDetailsTable.ISCLIENT] = clientDetails.IsActive;
                dr[dataSet.ClientDetailsTable.STATUSFLAG] = clientDetails.OrganizationStatus;

                dataSet.Tables[dataSet.ClientDetailsTable.TABLENAME].Rows.Add(dr);

            }
        }
    }
}
