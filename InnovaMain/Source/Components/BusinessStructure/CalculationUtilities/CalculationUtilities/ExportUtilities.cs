﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.IOrganizationUnits;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.CM.Organization;
using System.Collections.ObjectModel;
using System.Xml;
using Oritax.TaxSimp.CM.Entity;
using System.IO;
using Oritax.TaxSimp.CM.Organization.UMAForm;
using Oritax.TaxSimp.CM.OrganizationUnit;

namespace Oritax.TaxSimp.CM
{
    public class Statuses
    {
        public bool IsExportedSs { get; set; }
        public bool IsExportedBw { get; set; }
        public bool IsExportedDb { get; set; }
    }

    public class ExportUtilities
    {
        #region Get Account Types

        public static List<AccountType> GetAccountTypes(CorporateEntity _entity)
        {

            List<AccountType> list = new List<AccountType>();


            if (_entity.Servicetype.DO_IT_FOR_ME)
                list.Add(AccountType.CMADIFM);

            if (_entity.Servicetype.DO_IT_YOURSELF)
                list.Add(AccountType.CMADIY);

            if (_entity.Servicetype.DO_IT_WITH_ME)
                list.Add(AccountType.CMADIWM);


            return list;
        }

        public static List<AccountType> GetAccountTypes(ClientIndividualEntity _entity)
        {

            List<AccountType> list = new List<AccountType>();


            if (_entity.Servicetype.DO_IT_FOR_ME)
                list.Add(AccountType.CMADIFM);

            if (_entity.Servicetype.DO_IT_YOURSELF)
                list.Add(AccountType.CMADIY);

            if (_entity.Servicetype.DO_IT_WITH_ME)
                list.Add(AccountType.CMADIWM);


            return list;
        }

        #endregion

        #region Export Banwest AccountOpeings

        public static void FillBankWestAccountOpeningDataset(DataSet value, List<AccountType> accounts, CorporateEntity _entity, ICMBroker Broker, string ApplicationID, string TypeName, Guid orgUnitCID)
        {
            #region Filling Dataset for first file

            if (accounts != null && accounts.Count > 0)
            {
                foreach (AccountType _each in accounts)
                {
                    if (_each == AccountType.CMADIFM || _each == AccountType.CMADIWM || _each == AccountType.CMADIY)
                    {
                        #region CMA
                        AccountOpeningBankWestCSVColumns obj = new AccountOpeningBankWestCSVColumns();
                        DataRow dr = value.Tables[obj.Table.TableName].NewRow();

                        CSVInstance csvinstance = new CSVInstance(Broker);
                        CorporateCSV _entitycsv = csvinstance.GetEntityConcreteInstance(_entity);
                        dr[obj.Col_ApplicationID] = ApplicationID;
                        dr[obj.Col_ApplicationDate] = DateTime.Now.ToString("dd-MM-yy");
                        dr[obj.Col_ApplicationStatus] = "New";
                        dr[obj.Col_CMTOpenDate] = "";
                        dr[obj.Col_OpenAccountApplicationCreatorUserID] = "";
                        dr[obj.Col_OpenAccountApplicationApplicantType] = CSVUtilities.GetBankWestType(TypeName);
                        dr[obj.Col_OpenAccountApplicationAccountOption] = "Investor Plus";
                        dr[obj.Col_OpenAccountApplicationProductType] = "CMT";
                        dr[obj.Col_OpenAccountApplicationTrusteesCompany] = CSVUtilities.GetTrusteesCompanyType(TypeName);
                        dr[obj.Col_TrusteesCompanyDetailEntityName1] = _entity.Name;
                        dr[obj.Col_TrusteesCompanyDetailEntityName2] = String.Empty;

                        if (_entity.Name.ToCharArray().Count() > 40)
                        {
                            string entityName1 = _entity.Name.Substring(0, 40);
                            string entityName2 = _entity.Name.Substring(41);

                            dr[obj.Col_TrusteesCompanyDetailEntityName1] = entityName1;
                            dr[obj.Col_TrusteesCompanyDetailEntityName2] = entityName2;
                        }

                        dr[obj.Col_TrusteesCompanyDetailACNABN] = _entity.ACN;
                        if (_entity.ACN.RemoveWhiteSpacesStartEndInBetween() == string.Empty)
                            dr[obj.Col_TrusteesCompanyDetailACNABN] = _entity.ABN;
                        dr[obj.Col_TrusteesCompanyDetailABN] = _entity.ABN;
                        dr[obj.Col_TrusteesCompanyDetailACNARBN] = "";
                        dr[obj.Col_TrusteesCompanyDetailContactName] = "";
                        dr[obj.Col_TrusteesCompanyDetailContactEmail] = "";
                        dr[obj.Col_TrusteesCompanyDetailCountryOfEstablishment] = "Australia";
                        if (_entitycsv.Signatories != null && _entitycsv.Signatories.Count > 0)
                        {
                            dr[obj.Col_TrusteesCompanyDetailContactName] = _entitycsv.Signatories[0].Name + " " + _entitycsv.Signatories[0].Surname;
                            dr[obj.Col_TrusteesCompanyDetailContactEmail] = _entitycsv.Signatories[0].EmailAddress;

                            PhoneNumberEntity.FixPhoneNumbersAndMobile(_entitycsv.Signatories[0].HomePhoneNumber, _entitycsv.Signatories[0].MobilePhoneNumber);
                            PhoneNumberEntity.FixPhoneNumbersAndMobile(_entitycsv.Signatories[0].WorkPhoneNumber, _entitycsv.Signatories[0].MobilePhoneNumber);

                            if (!String.IsNullOrEmpty(_entitycsv.Signatories[0].HomePhoneNumber.PhoneNumber))
                            {
                                dr[obj.Col_TrusteesCompanyDetailContactPhPrefix] = _entitycsv.Signatories[0].HomePhoneNumber.CityCode;
                                dr[obj.Col_TrusteesCompanyDetailContactPh] = _entitycsv.Signatories[0].HomePhoneNumber.PhoneNumber;
                            }
                            else if (!String.IsNullOrEmpty(_entitycsv.Signatories[0].WorkPhoneNumber.PhoneNumber))
                            {
                                dr[obj.Col_TrusteesCompanyDetailContactPhPrefix] = _entitycsv.Signatories[0].WorkPhoneNumber.CityCode;
                                dr[obj.Col_TrusteesCompanyDetailContactPh] = _entitycsv.Signatories[0].WorkPhoneNumber.PhoneNumber;
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(_entitycsv.Signatories[0].MobilePhoneNumber.MobileNumber))
                                {
                                    dr[obj.Col_TrusteesCompanyDetailContactPhPrefix] = _entitycsv.Signatories[0].MobilePhoneNumber.MobileNumber.Substring(0, 2);
                                    dr[obj.Col_TrusteesCompanyDetailContactPh] = _entitycsv.Signatories[0].MobilePhoneNumber.MobileNumber.Substring(2);
                                }
                            }
                            dr[obj.Col_TrusteesCompanyDetailCountryOfEstablishment] = _entitycsv.Country;
                        }

                        dr[obj.Col_TrusteesCompanyDetailTrustType] = CSVUtilities.GetTrustType(TypeName);
                        if (_entitycsv.TFN.Length == 8)
                            _entitycsv.TFN = "0" + _entitycsv.TFN;
                        dr[obj.Col_TFNDetailTFN] = _entitycsv.TFN;
                        dr[obj.Col_TFNDetailNRC] = "";
                        dr[obj.Col_TFNDetailExemptionCategory] = "";
                        dr[obj.Col_TFNDetailTIN] = _entitycsv.TIN;
                        string temp1 = string.Empty;
                        string temp2 = string.Empty;
                        _entitycsv.LegalName.SplitInTwo(40, "atf", ref temp1, ref temp2);
                        dr[obj.Col_CorpTrusteeDetailEntityName1] = temp1;
                        dr[obj.Col_CorpTrusteeDetailEntityName2] = temp2;
                        dr[obj.Col_CorpTrusteeDetailACNABN] = "";
                        dr[obj.Col_CorpTrusteeDetailABN] = "";
                        dr[obj.Col_CorpTrusteeDetailACNARBN] = "";
                        dr[obj.Col_CorpTrusteeDetailCountryOfEstablishment] = "";
                        dr[obj.Col_OpenAccountApplicationInitialInvestmentAmount] = "";
                        dr[obj.Col_OpenAccountApplicationMannerOfOperation] = "Anyone";
                        dr[obj.Col_OpenAccountApplicationFinancialAdviserAccess] = "";
                        dr[obj.Col_OpenAccountApplicationAdvisorFirmAccess] = "General";
                        dr[obj.Col_OpenAccountApplicationLimitedAccessApproved] = "FALSE";

                        Common.AddressEntity applicatantsAddress = SetApplicantsAndSignatories(obj, dr, _entitycsv.Signatories);
                        SetAddresses(obj, dr, _entitycsv.Address, applicatantsAddress);
                        SetBankwestCommonData(Broker, ApplicationID, obj, dr, _entitycsv.AccessFacilities, orgUnitCID, _each);
                        value.Tables["tblAccountOpeningBankWest"].Rows.Add(dr);
                        #endregion

                        #region Filling Dataset for Second file

                        CSVInstance csvinstance2 = new CSVInstance(Broker);
                        CorporateCSV _entitycsv2 = csvinstance2.GetEntityConcreteInstance(_entity);
                        SetSignatoriesApplicants(value, _entity.Address, ApplicationID, _entitycsv2.Signatories);

                        #endregion
                    }
                }
            }
            #endregion
        }

        private static string GetCorrectStateByPostCode(string postCodeByWorflow, string StateByWorkflow)
        {
            if (postCodeByWorflow != string.Empty)
            {
                string state = StateByWorkflow;
                int postCode = 9999;

                bool postCodeParsed = int.TryParse(postCodeByWorflow, out postCode);

                if (!postCodeParsed)
                    return state;

                if ((postCode >= 1000 && postCode <= 1999) || (postCode >= 2000 && postCode <= 2599) || (postCode >= 2619 && postCode <= 2898) || (postCode >= 2921 && postCode <= 2999))
                    return "NSW";

                else if ((postCode >= 200 && postCode <= 299) || (postCode >= 2600 && postCode <= 2618) || (postCode >= 2900 && postCode <= 2920))
                    return "ACT";

                else if ((postCode >= 3000 && postCode <= 3999) || (postCode >= 8000 && postCode <= 8999))
                    return "VIC";

                else if ((postCode >= 5000 && postCode <= 5799) || (postCode >= 5800 && postCode <= 5999))
                    return "SA";

                else if ((postCode >= 4000 && postCode <= 4999) || (postCode >= 9000 && postCode <= 9999))
                    return "QLD";

                else if ((postCode >= 6000 && postCode <= 6797) || (postCode >= 6800 && postCode <= 6999))
                    return "WA";

                else if ((postCode >= 7000 && postCode <= 7799) || (postCode >= 7800 && postCode <= 7999))
                    return "TAS";

                else if ((postCode >= 800 && postCode <= 899) || (postCode >= 900 && postCode <= 999))
                    return "NT";
                else
                    return state;
            }
            else
                return
                    StateByWorkflow;
        }

        private static void SetBankwestCommonData(ICMBroker Broker, string ApplicationID, AccountOpeningBankWestCSVColumns obj, DataRow dr, Oritax.TaxSimp.Common.AccessFacilities AccessFacilities, Guid orgUnitCID, AccountType accountType)
        {
            OrganizationCM orgCM = Broker.GetWellKnownBMC(WellKnownCM.Organization) as OrganizationCM;
            dr[obj.Col_AccessFacilityType] = "Telephone Access";
            dr[obj.Col_AccessFacilitySelected] = AccessFacilities.PHONEACCESS.ToString();
            dr[obj.Col_AccessFacilityType_2] = "Online Access";
            dr[obj.Col_AccessFacilitySelected_2] = AccessFacilities.ONLINEACCESS.ToString();
            dr[obj.Col_AccessFacilityType_3] = "Cheque Book";
            dr[obj.Col_AccessFacilitySelected_3] = AccessFacilities.CHEQUEBOOK.ToString();
            dr[obj.Col_AccessFacilityType_4] = "Deposit Book";
            dr[obj.Col_AccessFacilitySelected_4] = AccessFacilities.DEPOSITBOOK.ToString();
            dr[obj.Col_AccessFacilityType_5] = "Debit Card";
            dr[obj.Col_AccessFacilitySelected_5] = AccessFacilities.DEBITCARD.ToString();

            UMAFormEntity umaFormEntity = orgCM.UMAFormEntityList.FirstOrDefault(umaForm => umaForm.ClientCID == orgUnitCID);

            if (umaFormEntity != null)
            {
                if (umaFormEntity.IFACID != Guid.Empty)
                {
                    IOrganizationUnit IFA = orgCM.Broker.GetBMCInstance(umaFormEntity.IFACID) as IOrganizationUnit;
                    IFADS ds = new IFADS();
                    IFA.GetData(ds);
                    dr[obj.Col_OpenAccountApplicationDealerName] = "FORTNUM PRIVATE WEALTH PTY LTD";
                    dr[obj.Col_OpenAccountApplicationAdviserName] = ds.ifaTable.Rows[0][ds.ifaTable.TRADINGNAME].ToString();
                    dr[obj.Col_OpenAccountApplicationAdviserCode] = ds.ifaTable.Rows[0][ds.ifaTable.SKCODE].ToString();
                }
            }
            else
            {
                dr[obj.Col_OpenAccountApplicationDealerName] = "FORTNUM PRIVATE WEALTH PTY LTD";
                dr[obj.Col_OpenAccountApplicationAdviserName] = "";
                dr[obj.Col_OpenAccountApplicationAdviserCode] = "SK15410001";
            }

            dr[obj.Col_OpenAccountApplicationCMTaccountNumber] = "";
            dr[obj.Col_OpenAccountApplicationCustomerAccountNumber] = "";
            dr[obj.Col_OpenAccountApplicationCMTaccountBSB1] = "";
            dr[obj.Col_OpenAccountApplicationCMTaccountBSB2] = "";
            dr[obj.Col_OpenAccountApplicationClientAccountDesignation] = "";
            dr[obj.Col_OpenAccountApplicationPartnerCode] = "CMT1345";
            dr[obj.Col_OpenAccountApplicationReceiptNo] = "";
            dr[obj.Col_TrusteesCompanyCIF] = "";
            dr[obj.Col_CorpTrusteeCIF] = "";
            dr[obj.Col_SignatoryCIF] = "";
            dr[obj.Col_SignatoryCIF_2] = "";
            dr[obj.Col_SignatoryCIF_3] = "";
            dr[obj.Col_SignatoryCIF_4] = "";
            dr[obj.Col_PAN] = "";
            dr[obj.Col_PAN_2] = "";
            dr[obj.Col_PAN_3] = "";
            dr[obj.Col_PAN_4] = "";
            dr[obj.Col_CBSStatus] = "";
            dr[obj.Col_Allocation] = "";
            dr[obj.Col_FollowupDate] = "";
            dr[obj.Col_OperationsStatus] = "";
            dr[obj.Col_OpenAccountApplicationPartnerCIF] = "";
            dr[obj.Col_OpenAccountApplicationAdviserCIF] = "";
            dr[obj.Col_PlatformName] = "";
            dr[obj.Col_PlatformCode] = "";
            dr[obj.Col_PlatformReference1] = "";
            dr[obj.Col_PlatformReference2] = "";
            dr[obj.Col_PlatformName_2] = "";
            dr[obj.Col_PlatformCode_2] = "";
            dr[obj.Col_PlatformReference1_2] = "";
            dr[obj.Col_PlatformReference2_2] = "";
            dr[obj.Col_PlatformName_3] = "";
            dr[obj.Col_PlatformCode_3] = "";
            dr[obj.Col_PlatformReference1_3] = "";
            dr[obj.Col_PlatformReference2_3] = "";
            dr[obj.Col_ThirdPartyCIFKey] = "";
            dr[obj.Col_ThirdPartyPartnerCode] = "";
            dr[obj.Col_OpenAccountApplicationFirmName] = "";
            dr[obj.Col_OpenAccountApplicationAdvisorFirmName] = "";
            dr[obj.Col_ReportedOn] = "";
            dr[obj.Col_ProcessingResult] = "";
            dr[obj.Col_ProcessingIssues] = "";
            dr[obj.Col_ApplicationReceivedDate] = "";
            dr[obj.Col_DocumentationOutstandingDate] = "";
            dr[obj.Col_TS_AccountDetails] = "";
            dr[obj.Col_OpenAccountApplicationProductCode] = "BWACMA";
            dr[obj.Col_OpenAccountApplicationProductCostCenter] = "";
            dr[obj.Col_FinalisedDate] = "";
        }

        private static void SetSignatoriesApplicants(DataSet value, Oritax.TaxSimp.Common.DualAddressEntity dualAddressEntity, string ApplicationID, List<Oritax.TaxSimp.Common.IndividualEntity> SignatoriesApplicants)
        {
            foreach (Oritax.TaxSimp.Common.IndividualEntity individualEntity in SignatoriesApplicants)
            {
                PhoneNumberEntity.FixPhoneNumbersAndMobile(individualEntity.HomePhoneNumber, individualEntity.MobilePhoneNumber);
                PhoneNumberEntity.FixPhoneNumbersAndMobile(individualEntity.WorkPhoneNumber, individualEntity.MobilePhoneNumber);

                AccountOpeningBankWestIDCSVColumns obj1 = new AccountOpeningBankWestIDCSVColumns();
                DataRow dr1 = value.Tables[obj1.Table.TableName].NewRow();
                dr1[obj1.Col_ApplicationID] = ApplicationID;
                dr1[obj1.Col_ApplicationDate] = DateTime.Now.ToString("dd-MM-yy");
                dr1[obj1.Col_ApplicationStatus] = "";
                dr1[obj1.Col_HundredPointApplicationCreatorUserID] = "";
                dr1[obj1.Col_HundredPointApplicationReceiptNo] = ApplicationID;

                dr1[obj1.Col_HundredPointApplicationTitle] = individualEntity.PersonalTitle.Title;
                dr1[obj1.Col_HundredPointApplicationTitleOther] = "";
                dr1[obj1.Col_HundredPointApplicationSurname] = individualEntity.Surname;
                dr1[obj1.Col_HundredPointApplicationGivenName] = individualEntity.Name;
                dr1[obj1.Col_HundredPointApplicationDOBDay] = individualEntity.DOB.Value.Day.ToString("00");
                dr1[obj1.Col_HundredPointApplicationDOBMonth] = individualEntity.DOB.Value.Month.ToString("00");
                dr1[obj1.Col_HundredPointApplicationDOBYear] = individualEntity.DOB.Value.Year.ToString("0000");
                dr1[obj1.Col_HundredPointApplicationResidentialAddress] = individualEntity.ResidentialAddress.Addressline1 + " " + individualEntity.ResidentialAddress.Addressline2 + " " + individualEntity.ResidentialAddress.Suburb + " " + individualEntity.ResidentialAddress.PostCode;
                dr1[obj1.Col_HundredPointApplicationPartnerCode] = "CMT1345";
                dr1[obj1.Col_HundredPointApplicationBSB1] = "";
                dr1[obj1.Col_HundredPointApplicationBSB2] = "";
                dr1[obj1.Col_HundredPointApplicationAccountNumber] = "";
                if (individualEntity.Occupation == null || individualEntity.Occupation == String.Empty)
                    dr1[obj1.Col_HundredPointApplicationOccupation] = "NA";
                else
                    dr1[obj1.Col_HundredPointApplicationOccupation] = individualEntity.Occupation;
                dr1[obj1.Col_HundredPointApplicationCountryOfResidence] = "Australia";
                dr1[obj1.Col_ResidentialAddressStreet] = individualEntity.ResidentialAddress.Addressline1 + " " + individualEntity.ResidentialAddress.Addressline2;
                dr1[obj1.Col_ResidentialAddressSuburb] = individualEntity.ResidentialAddress.Suburb;
                dr1[obj1.Col_ResidentialAddressState] = GetCorrectStateByPostCode(individualEntity.ResidentialAddress.PostCode, AusStates.ReturnShortStateCodeFromLong(individualEntity.ResidentialAddress.State));
                dr1[obj1.Col_ResidentialAddressPostcode] = individualEntity.ResidentialAddress.PostCode;
                if (!dualAddressEntity.MailingAddressSame)
                {
                    dr1[obj1.Col_HundredPointApplicationMailingAddress] = "";
                    dr1[obj1.Col_MailingAddressStreet] = individualEntity.MailingAddress.Addressline1 + " " + individualEntity.MailingAddress.Addressline2;
                    dr1[obj1.Col_MailingAddressSuburb] = individualEntity.MailingAddress.Suburb;
                    dr1[obj1.Col_MailingAddressState] = GetCorrectStateByPostCode(individualEntity.MailingAddress.PostCode, AusStates.ReturnShortStateCodeFromLong(individualEntity.MailingAddress.State));
                    dr1[obj1.Col_MailingAddressPostcode] = individualEntity.MailingAddress.PostCode;
                }
                dr1[obj1.Col_HundredPointApplicationHomePh] = individualEntity.HomePhoneNumber.CityCode + individualEntity.HomePhoneNumber.PhoneNumber;
                dr1[obj1.Col_HundredPointApplicationBusPh] = individualEntity.WorkPhoneNumber.CityCode + individualEntity.WorkPhoneNumber.PhoneNumber;
                dr1[obj1.Col_HundredPointApplicationMobilePh] = individualEntity.MobilePhoneNumber.MobileNumber;
                dr1[obj1.Col_HundredPointApplicationPeriodKnown] = "";
                dr1[obj1.Col_HundredPointApplicationPartiallyComplete] = "";

                Oritax.TaxSimp.Common.DocumentTypeEntityList documentTypeEntityList = individualEntity.DocumentTypeEntityList();
                if (documentTypeEntityList.Count > 0)
                {
                    string[] keys = documentTypeEntityList.Keys.ToArray();
                    Oritax.TaxSimp.Common.DocumentTypeEntity documentTypeEntity = null;
                    for (int count = 0; count <= keys.Length - 1; count++)
                    {
                        if (count == 0)
                        {
                            documentTypeEntity = documentTypeEntityList[keys[count]];

                            dr1[obj1.Col_HundredPointApplicationIDDocument] = documentTypeEntity.DocNumber;
                            dr1[obj1.Col_IDDocumentDocumentLayout] = "";
                            dr1[obj1.Col_IDDocumentTypeOfDoc] = keys[count];
                            dr1[obj1.Col_IDDocumentFullName] = documentTypeEntity.FullName;

                            if (documentTypeEntity.DOB != null)
                            {
                                dr1[obj1.Col_IDDocumentDOBDay] = documentTypeEntity.DOB.Value.Day.ToString("00");
                                dr1[obj1.Col_IDDocumentDOBMonth] = documentTypeEntity.DOB.Value.Month.ToString("00");
                                dr1[obj1.Col_IDDocumentDOBYear] = documentTypeEntity.DOB.Value.Year.ToString("0000");
                            }
                            dr1[obj1.Col_IDDocumentDocNumber] = documentTypeEntity.DocNumber;
                            dr1[obj1.Col_IDDocumentPhotoShown] = "Yes";
                            dr1[obj1.Col_IDDocumentAddressMatch] = "Yes";
                            if (documentTypeEntity.IssueDate != null)
                            {
                                dr1[obj1.Col_IDDocumentDateOfIssueDay] = documentTypeEntity.IssueDate.Value.Day.ToString("00");
                                dr1[obj1.Col_IDDocumentDateOfIssueMonth] = documentTypeEntity.IssueDate.Value.Month.ToString("00");
                                dr1[obj1.Col_IDDocumentDateOfIssueYear] = documentTypeEntity.IssueDate.Value.Year.ToString("0000");
                            }

                            if (documentTypeEntity.ExpiryDate != null)
                            {
                                dr1[obj1.Col_IDDocumentExpDateDay] = documentTypeEntity.ExpiryDate.Value.Day.ToString("00");
                                dr1[obj1.Col_IDDocumentExpDateMonth] = documentTypeEntity.ExpiryDate.Value.Month.ToString("00");
                                dr1[obj1.Col_IDDocumentExpDateYear] = documentTypeEntity.ExpiryDate.Value.Year.ToString("0000");
                            }

                            dr1[obj1.Col_IDDocumentIssuePlace] = documentTypeEntity.IssuePlace;
                            dr1[obj1.Col_IDDocumentPlaceResidence] = documentTypeEntity.PlaceResidence;
                            dr1[obj1.Col_IDDocumentPoints] = "";
                            dr1[obj1.Col_IDDocumentReferenceNumber] = documentTypeEntity.ReferenceNumber;
                            dr1[obj1.Col_IDDocumentInformationProvidedByOne] = documentTypeEntity.InfoProvidedByOne;
                            dr1[obj1.Col_IDDocumentTitleRankOrDesignation] = documentTypeEntity.TitleRankDesignation;
                            dr1[obj1.Col_IDDocumentInformationProvidedByTwo] = documentTypeEntity.InfoProvidedByTwo;
                            dr1[obj1.Col_IDDocumentNameOfOrganisation] = documentTypeEntity.OrganizationName;
                            dr1[obj1.Col_IDDocumentAddressOfOrganisation] = "";
                            if (documentTypeEntity.OrganizationPhone != null && documentTypeEntity.OrganizationPhone.PhoneNumber != null)
                                dr1[obj1.Col_IDDocumentPhoneNumberOfOrganisation] = documentTypeEntity.OrganizationPhone.PhoneNumber;
                            dr1[obj1.Col_IDDocumentBillLetterSighted] = documentTypeEntity.BillLetterSighted.ToString();

                            if (documentTypeEntity.TelephoneContactDate != null)
                            {
                                dr1[obj1.Col_IDDocumentDateTelephoneContactMadeDay] = documentTypeEntity.TelephoneContactDate.Value.Day.ToString();
                                dr1[obj1.Col_IDDocumentDateTelephoneContactMadeMonth] = documentTypeEntity.TelephoneContactDate.Value.Month.ToString();
                                dr1[obj1.Col_IDDocumentDateTelephoneContactMadeYear] = documentTypeEntity.TelephoneContactDate.Value.Year.ToString();
                            }
                        }
                        else if (count == 1)
                        {
                            documentTypeEntity = documentTypeEntityList[keys[count]];

                            dr1[obj1.Col_HundredPointApplicationIDDocument_2] = documentTypeEntity.DocNumber;
                            dr1[obj1.Col_IDDocumentDocumentLayout_2] = "";
                            dr1[obj1.Col_IDDocumentTypeOfDoc_2] = keys[count];
                            dr1[obj1.Col_IDDocumentFullName_2] = documentTypeEntity.FullName;
                            if (documentTypeEntity.DOB != null)
                            {
                                dr1[obj1.Col_IDDocumentDOBDay_2] = documentTypeEntity.DOB.Value.Day.ToString("00");
                                dr1[obj1.Col_IDDocumentDOBMonth_2] = documentTypeEntity.DOB.Value.Month.ToString("00");
                                dr1[obj1.Col_IDDocumentDOBYear_2] = documentTypeEntity.DOB.Value.Year.ToString("0000");
                            }

                            dr1[obj1.Col_IDDocumentDocNumber_2] = documentTypeEntity.DocNumber;
                            dr1[obj1.Col_IDDocumentPhotoShown_2] = "Yes";
                            dr1[obj1.Col_IDDocumentAddressMatch_2] = "Yes";

                            if (documentTypeEntity.IssueDate != null)
                            {
                                dr1[obj1.Col_IDDocumentDateOfIssueDay_2] = documentTypeEntity.IssueDate.Value.Day.ToString("00");
                                dr1[obj1.Col_IDDocumentDateOfIssueMonth_2] = documentTypeEntity.IssueDate.Value.Month.ToString("00");
                                dr1[obj1.Col_IDDocumentDateOfIssueYear_2] = documentTypeEntity.IssueDate.Value.Year.ToString("0000");
                            }

                            if (documentTypeEntity.ExpiryDate != null)
                            {

                                dr1[obj1.Col_IDDocumentExpDateDay_2] = documentTypeEntity.ExpiryDate.Value.Day.ToString("00");
                                dr1[obj1.Col_IDDocumentExpDateMonth_2] = documentTypeEntity.ExpiryDate.Value.Month.ToString("00");
                                dr1[obj1.Col_IDDocumentExpDateYear_2] = documentTypeEntity.ExpiryDate.Value.Year.ToString("0000");
                            }
                            dr1[obj1.Col_IDDocumentIssuePlace_2] = documentTypeEntity.IssuePlace;
                            dr1[obj1.Col_IDDocumentPlaceResidence_2] = documentTypeEntity.PlaceResidence;
                            dr1[obj1.Col_IDDocumentPoints_2] = "";
                            dr1[obj1.Col_IDDocumentReferenceNumber_2] = documentTypeEntity.ReferenceNumber;
                            dr1[obj1.Col_IDDocumentInformationProvidedByOne_2] = documentTypeEntity.InfoProvidedByOne;
                            dr1[obj1.Col_IDDocumentTitleRankOrDesignation_2] = documentTypeEntity.TitleRankDesignation;
                            dr1[obj1.Col_IDDocumentInformationProvidedByTwo_2] = documentTypeEntity.InfoProvidedByTwo;
                            dr1[obj1.Col_IDDocumentNameOfOrganisation_2] = documentTypeEntity.OrganizationName;
                            dr1[obj1.Col_IDDocumentAddressOfOrganisation_2] = "";
                            if (documentTypeEntity.OrganizationPhone != null && documentTypeEntity.OrganizationPhone.PhoneNumber != null)
                                dr1[obj1.Col_IDDocumentPhoneNumberOfOrganisation_2] = documentTypeEntity.OrganizationPhone.PhoneNumber;
                            dr1[obj1.Col_IDDocumentBillLetterSighted_2] = documentTypeEntity.BillLetterSighted.ToString();

                            if (documentTypeEntity.TelephoneContactDate != null)
                            {
                                dr1[obj1.Col_IDDocumentDateTelephoneContactMadeDay_2] = documentTypeEntity.TelephoneContactDate.Value.Day.ToString();
                                dr1[obj1.Col_IDDocumentDateTelephoneContactMadeMonth_2] = documentTypeEntity.TelephoneContactDate.Value.Month.ToString();
                                dr1[obj1.Col_IDDocumentDateTelephoneContactMadeYear_2] = documentTypeEntity.TelephoneContactDate.Value.Year.ToString();
                            }
                        }
                        else if (count == 2)
                        {
                            documentTypeEntity = documentTypeEntityList[keys[count]];

                            dr1[obj1.Col_HundredPointApplicationIDDocument_3] = documentTypeEntity.DocNumber;
                            dr1[obj1.Col_IDDocumentDocumentLayout_3] = "";
                            dr1[obj1.Col_IDDocumentTypeOfDoc_3] = keys[count];
                            dr1[obj1.Col_IDDocumentFullName_3] = documentTypeEntity.FullName;

                            if (documentTypeEntity.DOB != null)
                            {
                                dr1[obj1.Col_IDDocumentDOBDay_3] = documentTypeEntity.DOB.Value.Day.ToString("00");
                                dr1[obj1.Col_IDDocumentDOBMonth_3] = documentTypeEntity.DOB.Value.Month.ToString("00");
                                dr1[obj1.Col_IDDocumentDOBYear_3] = documentTypeEntity.DOB.Value.Year.ToString("0000");
                            }
                            dr1[obj1.Col_IDDocumentDocNumber_3] = documentTypeEntity.DocNumber;
                            dr1[obj1.Col_IDDocumentPhotoShown_3] = "Yes";
                            dr1[obj1.Col_IDDocumentAddressMatch_3] = "Yes";
                            if (documentTypeEntity.IssueDate != null)
                            {
                                dr1[obj1.Col_IDDocumentDateOfIssueDay_3] = documentTypeEntity.IssueDate.Value.Day.ToString("00");
                                dr1[obj1.Col_IDDocumentDateOfIssueMonth_3] = documentTypeEntity.IssueDate.Value.Month.ToString("00");
                                dr1[obj1.Col_IDDocumentDateOfIssueYear_3] = documentTypeEntity.IssueDate.Value.Year.ToString("000");
                            }

                            if (documentTypeEntity.ExpiryDate != null)
                            {
                                dr1[obj1.Col_IDDocumentExpDateDay_3] = documentTypeEntity.ExpiryDate.Value.Day.ToString("00");
                                dr1[obj1.Col_IDDocumentExpDateMonth_3] = documentTypeEntity.ExpiryDate.Value.Month.ToString("00");
                                dr1[obj1.Col_IDDocumentExpDateYear_3] = documentTypeEntity.ExpiryDate.Value.Year.ToString("0000");
                            }
                            dr1[obj1.Col_IDDocumentIssuePlace_3] = documentTypeEntity.IssuePlace;
                            dr1[obj1.Col_IDDocumentPlaceResidence_3] = documentTypeEntity.PlaceResidence;
                            dr1[obj1.Col_IDDocumentPoints_3] = "";
                            dr1[obj1.Col_IDDocumentReferenceNumber_3] = documentTypeEntity.ReferenceNumber;
                            dr1[obj1.Col_IDDocumentInformationProvidedByOne_3] = documentTypeEntity.InfoProvidedByOne;
                            dr1[obj1.Col_IDDocumentTitleRankOrDesignation_3] = documentTypeEntity.TitleRankDesignation;
                            dr1[obj1.Col_IDDocumentInformationProvidedByTwo_3] = documentTypeEntity.InfoProvidedByTwo;
                            dr1[obj1.Col_IDDocumentNameOfOrganisation_3] = documentTypeEntity.OrganizationName;
                            dr1[obj1.Col_IDDocumentAddressOfOrganisation_3] = "";
                            if (documentTypeEntity.OrganizationPhone != null && documentTypeEntity.OrganizationPhone.PhoneNumber != null)
                                dr1[obj1.Col_IDDocumentPhoneNumberOfOrganisation_3] = documentTypeEntity.OrganizationPhone.PhoneNumber;
                            dr1[obj1.Col_IDDocumentBillLetterSighted_3] = documentTypeEntity.BillLetterSighted.ToString();
                            if (documentTypeEntity.TelephoneContactDate != null)
                            {
                                dr1[obj1.Col_IDDocumentDateTelephoneContactMadeDay_3] = documentTypeEntity.TelephoneContactDate.Value.Day.ToString();
                                dr1[obj1.Col_IDDocumentDateTelephoneContactMadeMonth_3] = documentTypeEntity.TelephoneContactDate.Value.Month.ToString();
                                dr1[obj1.Col_IDDocumentDateTelephoneContactMadeYear_3] = documentTypeEntity.TelephoneContactDate.Value.Year.ToString();
                            }
                        }
                        else if (count == 3)
                        {
                            documentTypeEntity = documentTypeEntityList[keys[count]];
                            dr1[obj1.Col_HundredPointApplicationIDDocument_4] = documentTypeEntity.DocNumber;
                            dr1[obj1.Col_IDDocumentDocumentLayout_4] = "";
                            dr1[obj1.Col_IDDocumentTypeOfDoc_4] = keys[count];
                            dr1[obj1.Col_IDDocumentFullName_4] = documentTypeEntity.FullName;
                            if (documentTypeEntity.DOB != null)
                            {
                                dr1[obj1.Col_IDDocumentDOBDay_4] = documentTypeEntity.DOB.Value.Day.ToString("00");
                                dr1[obj1.Col_IDDocumentDOBMonth_4] = documentTypeEntity.DOB.Value.Month.ToString("00");
                                dr1[obj1.Col_IDDocumentDOBYear_4] = documentTypeEntity.DOB.Value.Year.ToString("0000");
                            }
                            dr1[obj1.Col_IDDocumentDocNumber_4] = documentTypeEntity.DocNumber;
                            dr1[obj1.Col_IDDocumentPhotoShown_4] = "Yes";
                            dr1[obj1.Col_IDDocumentAddressMatch_4] = "Yes";
                            if (documentTypeEntity.IssueDate != null)
                            {
                                dr1[obj1.Col_IDDocumentDateOfIssueDay_4] = documentTypeEntity.IssueDate.Value.Day.ToString("00");
                                dr1[obj1.Col_IDDocumentDateOfIssueMonth_4] = documentTypeEntity.IssueDate.Value.Month.ToString("00");
                                dr1[obj1.Col_IDDocumentDateOfIssueYear_4] = documentTypeEntity.IssueDate.Value.Year.ToString("0000");
                            }
                            if (documentTypeEntity.ExpiryDate != null)
                            {
                                dr1[obj1.Col_IDDocumentExpDateDay_4] = documentTypeEntity.ExpiryDate.Value.Day.ToString("00");
                                dr1[obj1.Col_IDDocumentExpDateMonth_4] = documentTypeEntity.ExpiryDate.Value.Month.ToString("00");
                                dr1[obj1.Col_IDDocumentExpDateYear_4] = documentTypeEntity.ExpiryDate.Value.Year.ToString("0000");
                            }
                            dr1[obj1.Col_IDDocumentIssuePlace_4] = documentTypeEntity.IssuePlace;
                            dr1[obj1.Col_IDDocumentPlaceResidence_4] = documentTypeEntity.PlaceResidence;
                            dr1[obj1.Col_IDDocumentPoints_4] = "";
                            dr1[obj1.Col_IDDocumentReferenceNumber_4] = documentTypeEntity.ReferenceNumber;
                            dr1[obj1.Col_IDDocumentInformationProvidedByOne_4] = documentTypeEntity.InfoProvidedByOne;
                            dr1[obj1.Col_IDDocumentTitleRankOrDesignation_4] = documentTypeEntity.TitleRankDesignation;
                            dr1[obj1.Col_IDDocumentInformationProvidedByTwo_4] = documentTypeEntity.InfoProvidedByTwo;
                            dr1[obj1.Col_IDDocumentNameOfOrganisation_4] = documentTypeEntity.OrganizationName;
                            dr1[obj1.Col_IDDocumentAddressOfOrganisation_4] = "";
                            if (documentTypeEntity.OrganizationPhone != null && documentTypeEntity.OrganizationPhone.PhoneNumber != null)
                                dr1[obj1.Col_IDDocumentPhoneNumberOfOrganisation_4] = documentTypeEntity.OrganizationPhone.PhoneNumber;
                            dr1[obj1.Col_IDDocumentBillLetterSighted_4] = documentTypeEntity.BillLetterSighted.ToString();

                            if (documentTypeEntity.TelephoneContactDate != null)
                            {
                                dr1[obj1.Col_IDDocumentDateTelephoneContactMadeDay_4] = documentTypeEntity.TelephoneContactDate.Value.Day.ToString();
                                dr1[obj1.Col_IDDocumentDateTelephoneContactMadeMonth_4] = documentTypeEntity.TelephoneContactDate.Value.Month.ToString();
                                dr1[obj1.Col_IDDocumentDateTelephoneContactMadeYear_4] = documentTypeEntity.TelephoneContactDate.Value.Year.ToString();
                            }
                        }
                    }
                }
                value.Tables["tblAccountOpeningBankWestID"].Rows.Add(dr1);
            }
        }

        private static void SetAddresses(AccountOpeningBankWestCSVColumns obj, DataRow dr, Oritax.TaxSimp.Common.DualAddressEntity dualAddressEntity, Oritax.TaxSimp.Common.AddressEntity applicatantsAddress)
        {
            if (applicatantsAddress != null && applicatantsAddress.Addressline1 != string.Empty)
            {
                dr[obj.Col_CorrespondenceAddressAddress1] = applicatantsAddress.Addressline1;
                dr[obj.Col_CorrespondenceAddressAddress2] = applicatantsAddress.Addressline2;
                dr[obj.Col_CorrespondenceAddressAddress3] = "";
                dr[obj.Col_CorrespondenceAddressSuburb] = applicatantsAddress.Suburb;
                dr[obj.Col_CorrespondenceAddressState] = GetCorrectStateByPostCode(applicatantsAddress.PostCode, AusStates.ReturnShortStateCodeFromLong(applicatantsAddress.State));
                dr[obj.Col_CorrespondenceAddressPostcode] = applicatantsAddress.PostCode;
                dr[obj.Col_CorrespondenceAddressCountry] = "Australia";
                dr[obj.Col_CorrespondenceAddressCareOf] = "";
            }
            else
            {
                dr[obj.Col_CorrespondenceAddressAddress1] = dualAddressEntity.BusinessAddress.Addressline1;
                dr[obj.Col_CorrespondenceAddressAddress2] = dualAddressEntity.BusinessAddress.Addressline2;
                dr[obj.Col_CorrespondenceAddressAddress3] = "";
                dr[obj.Col_CorrespondenceAddressSuburb] = dualAddressEntity.BusinessAddress.Suburb;
                dr[obj.Col_CorrespondenceAddressState] = GetCorrectStateByPostCode(dualAddressEntity.BusinessAddress.PostCode, AusStates.ReturnShortStateCodeFromLong(dualAddressEntity.BusinessAddress.State));
                dr[obj.Col_CorrespondenceAddressPostcode] = dualAddressEntity.BusinessAddress.PostCode;
                dr[obj.Col_CorrespondenceAddressCountry] = "Australia";
                dr[obj.Col_CorrespondenceAddressCareOf] = "";
            }

            if (!dualAddressEntity.MailingAddressSame && dualAddressEntity.MailingAddress.Addressline1 != String.Empty)
            {
                dr[obj.Col_CorrespondenceAddressAddress1_2] = dualAddressEntity.MailingAddress.Addressline1;
                dr[obj.Col_CorrespondenceAddressAddress2_2] = dualAddressEntity.MailingAddress.Addressline2;
                dr[obj.Col_CorrespondenceAddressAddress3_2] = "";
                dr[obj.Col_CorrespondenceAddressSuburb_2] = dualAddressEntity.MailingAddress.Suburb;
                dr[obj.Col_CorrespondenceAddressState_2] = GetCorrectStateByPostCode(dualAddressEntity.MailingAddress.PostCode, AusStates.ReturnShortStateCodeFromLong(dualAddressEntity.MailingAddress.State));
                dr[obj.Col_CorrespondenceAddressPostcode_2] = dualAddressEntity.MailingAddress.PostCode;
                dr[obj.Col_CorrespondenceAddressCountry_2] = "Australia";
                dr[obj.Col_CorrespondenceAddressCareOf_2] = "";
            }

            else
            {
                dr[obj.Col_CorrespondenceAddressAddress1_2] = dualAddressEntity.BusinessAddress.Addressline1;
                dr[obj.Col_CorrespondenceAddressAddress2_2] = dualAddressEntity.BusinessAddress.Addressline2;
                dr[obj.Col_CorrespondenceAddressAddress3_2] = "";
                dr[obj.Col_CorrespondenceAddressSuburb_2] = dualAddressEntity.BusinessAddress.Suburb;
                dr[obj.Col_CorrespondenceAddressState_2] = GetCorrectStateByPostCode(dualAddressEntity.BusinessAddress.PostCode, AusStates.ReturnShortStateCodeFromLong(dualAddressEntity.BusinessAddress.State));
                dr[obj.Col_CorrespondenceAddressPostcode_2] = dualAddressEntity.BusinessAddress.PostCode;
                dr[obj.Col_CorrespondenceAddressCountry_2] = "Australia";
                dr[obj.Col_CorrespondenceAddressCareOf_2] = "";
            }

            if (!dualAddressEntity.DuplicateMailingAddressSame && dualAddressEntity.DuplicateMailingAddress.Addressline1 != String.Empty)
            {
                dr[obj.Col_CorrespondenceAddressAddress1_3] = dualAddressEntity.DuplicateMailingAddress.Addressline1;
                dr[obj.Col_CorrespondenceAddressAddress2_3] = dualAddressEntity.DuplicateMailingAddress.Addressline2;
                dr[obj.Col_CorrespondenceAddressAddress3_3] = "";
                dr[obj.Col_CorrespondenceAddressSuburb_3] = dualAddressEntity.DuplicateMailingAddress.Suburb;
                dr[obj.Col_CorrespondenceAddressState_3] = GetCorrectStateByPostCode(dualAddressEntity.DuplicateMailingAddress.PostCode, AusStates.ReturnShortStateCodeFromLong(dualAddressEntity.DuplicateMailingAddress.State));
                dr[obj.Col_CorrespondenceAddressPostcode_3] = dualAddressEntity.DuplicateMailingAddress.PostCode;
                dr[obj.Col_CorrespondenceAddressCountry_3] = "Australia";
                dr[obj.Col_CorrespondenceAddressCareOf_3] = "";
            }
            else
            {
                dr[obj.Col_CorrespondenceAddressAddress1_3] = dualAddressEntity.BusinessAddress.Addressline1;
                dr[obj.Col_CorrespondenceAddressAddress2_3] = dualAddressEntity.BusinessAddress.Addressline2;
                dr[obj.Col_CorrespondenceAddressAddress3_3] = "";
                dr[obj.Col_CorrespondenceAddressSuburb_3] = dualAddressEntity.BusinessAddress.Suburb;
                dr[obj.Col_CorrespondenceAddressState_3] = GetCorrectStateByPostCode(dualAddressEntity.BusinessAddress.PostCode, AusStates.ReturnShortStateCodeFromLong(dualAddressEntity.BusinessAddress.State));
                dr[obj.Col_CorrespondenceAddressPostcode_3] = dualAddressEntity.BusinessAddress.PostCode;
                dr[obj.Col_CorrespondenceAddressCountry_3] = "Australia";
                dr[obj.Col_CorrespondenceAddressCareOf_3] = "";
            }
        }

        private static Oritax.TaxSimp.Common.AddressEntity SetApplicantsAndSignatories(AccountOpeningBankWestCSVColumns obj, DataRow dr, List<Oritax.TaxSimp.Common.IndividualEntity> signatoriesApplicants)
        {
            int i = 1;
            Oritax.TaxSimp.Common.AddressEntity applicantAddress = null;

            foreach (Oritax.TaxSimp.Common.IndividualEntity each in signatoriesApplicants.Take(4))
            {
                if (each.Occupation == null || each.Occupation == String.Empty)
                    each.Occupation = "NA";

                PhoneNumberEntity.FixPhoneNumbersAndMobile(each.HomePhoneNumber, each.MobilePhoneNumber);
                PhoneNumberEntity.FixPhoneNumbersAndMobile(each.WorkPhoneNumber, each.MobilePhoneNumber);

                if (i == 1 && each.Name != string.Empty)
                {
                    applicantAddress = each.ResidentialAddress;
                    dr[obj.Col_SignatoryPersonalDetailOccupation] = each.Occupation;
                    dr[obj.Col_SignatoryPersonalDetailEmployer] = each.Employer;
                    dr[obj.Col_SignatoryPersonalDetailCountryOfResidence] = each.ResidentialAddress.Country;
                    dr[obj.Col_SignatoryPersonalDetailTitle] = each.PersonalTitle.Title;
                    dr[obj.Col_SignatoryPersonalDetailTitleOther] = "";
                    dr[obj.Col_SignatoryPersonalDetailGivenName] = each.Name;
                    dr[obj.Col_SignatoryPersonalDetailSurname] = each.Surname;
                    dr[obj.Col_SignatoryPersonalDetailPhoneWk] = each.WorkPhoneNumber.PhoneNumber;
                    dr[obj.Col_SignatoryPersonalDetailPhoneHm] = each.HomePhoneNumber.PhoneNumber;
                    dr[obj.Col_SignatoryPersonalDetailPhoneMb] = each.MobilePhoneNumber.MobileNumber;
                    dr[obj.Col_SignatoryPersonalDetailDOBDay] = each.DOB.Value.Day.ToString("00");
                    dr[obj.Col_SignatoryPersonalDetailDOBMonth] = each.DOB.Value.Month.ToString("00");
                    dr[obj.Col_SignatoryPersonalDetailDOBYear] = each.DOB.Value.Year.ToString("0000");
                    dr[obj.Col_SignatoryPersonalDetailEmail] = each.EmailAddress;
                    dr[obj.Col_AddressAddress1] = each.ResidentialAddress.Addressline1;
                    dr[obj.Col_AddressAddress2] = each.ResidentialAddress.Addressline2;
                    dr[obj.Col_AddressAddress3] = "";
                    dr[obj.Col_AddressSuburb] = each.ResidentialAddress.Suburb;
                    dr[obj.Col_AddressState] = GetCorrectStateByPostCode(each.ResidentialAddress.PostCode, AusStates.ReturnShortStateCodeFromLong(each.ResidentialAddress.State));
                    dr[obj.Col_AddressPostcode] = each.ResidentialAddress.PostCode;
                    dr[obj.Col_AddressCountry] = each.ResidentialAddress.Country;
                    dr[obj.Col_SignatoryPersonalDetailPhoneHmPrefix] = each.HomePhoneNumber.CityCode;
                    dr[obj.Col_SignatoryPersonalDetailPhonewkPrefix] = each.WorkPhoneNumber.CityCode;
                    dr[obj.Col_TFNDetailTFN_2] = "";
                    dr[obj.Col_TFNDetailNRC_2] = "";
                    dr[obj.Col_TFNDetailExemptionCategory_2] = "";
                    dr[obj.Col_TFNDetailTIN_2] = "";
                }
                if (i == 2 && each.Name != string.Empty)
                {
                    if (applicantAddress == null)
                        applicantAddress = each.ResidentialAddress;

                    dr[obj.Col_SignatoryPersonalDetailOccupation_2] = each.Occupation;
                    dr[obj.Col_SignatoryPersonalDetailEmployer_2] = each.Employer;
                    dr[obj.Col_SignatoryPersonalDetailCountryOfResidence_2] = each.ResidentialAddress.Country;
                    dr[obj.Col_SignatoryPersonalDetailTitle_2] = each.PersonalTitle.Title;
                    dr[obj.Col_SignatoryPersonalDetailTitleOther_2] = "";
                    dr[obj.Col_SignatoryPersonalDetailGivenName_2] = each.Name;
                    dr[obj.Col_SignatoryPersonalDetailSurname_2] = each.Surname;
                    dr[obj.Col_SignatoryPersonalDetailPhoneWk_2] = each.WorkPhoneNumber.PhoneNumber;
                    dr[obj.Col_SignatoryPersonalDetailPhoneHm_2] = each.HomePhoneNumber.PhoneNumber;
                    dr[obj.Col_SignatoryPersonalDetailPhoneMb_2] = each.MobilePhoneNumber.MobileNumber;
                    dr[obj.Col_SignatoryPersonalDetailDOBDay_2] = each.DOB.Value.Day.ToString("00");
                    dr[obj.Col_SignatoryPersonalDetailDOBMonth_2] = each.DOB.Value.Month.ToString("00");
                    dr[obj.Col_SignatoryPersonalDetailDOBYear_2] = each.DOB.Value.Year.ToString("0000");
                    dr[obj.Col_SignatoryPersonalDetailEmail_2] = each.EmailAddress;
                    dr[obj.Col_AddressAddress1_2] = each.ResidentialAddress.Addressline1;
                    dr[obj.Col_AddressAddress2_2] = each.ResidentialAddress.Addressline2;
                    dr[obj.Col_AddressAddress3_2] = "";
                    dr[obj.Col_AddressSuburb_2] = each.ResidentialAddress.Suburb;
                    dr[obj.Col_AddressState_2] = GetCorrectStateByPostCode(each.ResidentialAddress.PostCode, AusStates.ReturnShortStateCodeFromLong(each.ResidentialAddress.State));
                    dr[obj.Col_AddressPostcode_2] = each.ResidentialAddress.PostCode;
                    dr[obj.Col_AddressCountry_2] = each.ResidentialAddress.Country;
                    dr[obj.Col_SignatoryPersonalDetailPhoneHmPrefix_2] = each.HomePhoneNumber.CityCode;
                    dr[obj.Col_SignatoryPersonalDetailPhonewkPrefix_2] = each.WorkPhoneNumber.CityCode;
                    dr[obj.Col_TFNDetailTFN_3] = "";
                    dr[obj.Col_TFNDetailNRC_3] = "";
                    dr[obj.Col_TFNDetailExemptionCategory_3] = "";
                    dr[obj.Col_TFNDetailTIN_3] = "";
                }
                if (i == 3 && each.Name != string.Empty)
                {
                    if (applicantAddress == null)
                        applicantAddress = each.ResidentialAddress;

                    dr[obj.Col_SignatoryPersonalDetailOccupation_3] = each.Occupation;
                    dr[obj.Col_SignatoryPersonalDetailEmployer_3] = each.Employer;
                    dr[obj.Col_SignatoryPersonalDetailCountryOfResidence_3] = each.ResidentialAddress.Country;
                    dr[obj.Col_SignatoryPersonalDetailTitle_3] = each.PersonalTitle.Title;
                    dr[obj.Col_SignatoryPersonalDetailTitleOther_3] = each.PersonalTitle.Title;
                    dr[obj.Col_SignatoryPersonalDetailGivenName_3] = each.Name;
                    dr[obj.Col_SignatoryPersonalDetailSurname_3] = each.Surname;
                    dr[obj.Col_SignatoryPersonalDetailPhoneWk_3] = each.WorkPhoneNumber.PhoneNumber;
                    dr[obj.Col_SignatoryPersonalDetailPhoneHm_3] = each.HomePhoneNumber.PhoneNumber;
                    dr[obj.Col_SignatoryPersonalDetailPhoneMb_3] = each.MobilePhoneNumber.MobileNumber;
                    dr[obj.Col_SignatoryPersonalDetailDOBDay_3] = each.DOB.Value.Day.ToString("00");
                    dr[obj.Col_SignatoryPersonalDetailDOBMonth_3] = each.DOB.Value.Month.ToString("00");
                    dr[obj.Col_SignatoryPersonalDetailDOBYear_3] = each.DOB.Value.Year.ToString("0000");
                    dr[obj.Col_SignatoryPersonalDetailEmail_3] = each.EmailAddress;
                    dr[obj.Col_AddressAddress1_3] = each.ResidentialAddress.Addressline1;
                    dr[obj.Col_AddressAddress2_3] = each.ResidentialAddress.Addressline2;
                    dr[obj.Col_AddressAddress3_3] = "";
                    dr[obj.Col_AddressSuburb_3] = each.ResidentialAddress.Suburb;
                    dr[obj.Col_AddressState_3] = GetCorrectStateByPostCode(each.ResidentialAddress.PostCode, AusStates.ReturnShortStateCodeFromLong(each.ResidentialAddress.State));
                    dr[obj.Col_AddressPostcode_3] = each.ResidentialAddress.PostCode;
                    dr[obj.Col_AddressCountry_3] = each.ResidentialAddress.Country;
                    dr[obj.Col_SignatoryPersonalDetailPhoneHmPrefix_3] = each.HomePhoneNumber.CityCode;
                    dr[obj.Col_SignatoryPersonalDetailPhonewkPrefix_3] = each.WorkPhoneNumber.CityCode;
                    dr[obj.Col_TFNDetailTFN_4] = "";
                    dr[obj.Col_TFNDetailNRC_4] = "";
                    dr[obj.Col_TFNDetailExemptionCategory_4] = "";
                    dr[obj.Col_TFNDetailTIN_4] = "";
                }
                if (i == 4 && each.Name != string.Empty)
                {
                    if (applicantAddress == null)
                        applicantAddress = each.ResidentialAddress;

                    dr[obj.Col_SignatoryPersonalDetailOccupation_4] = each.Occupation;
                    dr[obj.Col_SignatoryPersonalDetailEmployer_4] = each.Employer;
                    dr[obj.Col_SignatoryPersonalDetailCountryOfResidence_4] = each.ResidentialAddress.Country;
                    dr[obj.Col_SignatoryPersonalDetailTitle_4] = each.PersonalTitle.Title;
                    dr[obj.Col_SignatoryPersonalDetailTitleOther_4] = each.PersonalTitle.Title;
                    dr[obj.Col_SignatoryPersonalDetailGivenName_4] = each.Name;
                    dr[obj.Col_SignatoryPersonalDetailSurname_4] = each.Surname;
                    dr[obj.Col_SignatoryPersonalDetailPhoneWk_4] = each.WorkPhoneNumber.PhoneNumber;
                    dr[obj.Col_SignatoryPersonalDetailPhoneHm_4] = each.HomePhoneNumber.PhoneNumber;
                    dr[obj.Col_SignatoryPersonalDetailPhoneMb_4] = each.MobilePhoneNumber.MobileNumber;
                    dr[obj.Col_SignatoryPersonalDetailDOBDay_4] = each.DOB.Value.Day.ToString("00");
                    dr[obj.Col_SignatoryPersonalDetailDOBMonth_4] = each.DOB.Value.Month.ToString("00");
                    dr[obj.Col_SignatoryPersonalDetailDOBYear_4] = each.DOB.Value.Year.ToString("0000");
                    dr[obj.Col_SignatoryPersonalDetailEmail_4] = each.EmailAddress;

                    dr[obj.Col_AddressAddress1_4] = each.ResidentialAddress.Addressline1;
                    dr[obj.Col_AddressAddress2_4] = each.ResidentialAddress.Addressline2;
                    dr[obj.Col_AddressAddress3_4] = "";
                    dr[obj.Col_AddressSuburb_4] = each.ResidentialAddress.Suburb;
                    dr[obj.Col_AddressState_4] = GetCorrectStateByPostCode(each.ResidentialAddress.PostCode, AusStates.ReturnShortStateCodeFromLong(each.ResidentialAddress.State));
                    dr[obj.Col_AddressPostcode_4] = each.ResidentialAddress.PostCode;
                    dr[obj.Col_AddressCountry_4] = each.ResidentialAddress.Country;
                    dr[obj.Col_SignatoryPersonalDetailPhoneHmPrefix_4] = each.HomePhoneNumber.CityCode;
                    dr[obj.Col_SignatoryPersonalDetailPhonewkPrefix_4] = each.WorkPhoneNumber.CityCode;
                    dr[obj.Col_TFNDetailTFN_5] = "";
                    dr[obj.Col_TFNDetailNRC_5] = "";
                    dr[obj.Col_TFNDetailExemptionCategory_5] = "";
                    dr[obj.Col_TFNDetailTIN_5] = "";
                }
                i++;
            }

            return applicantAddress;
        }

        public static void FillBankWestAccountOpeningDataset(DataSet value, List<AccountType> accounts, ClientIndividualEntity _entity, ICMBroker Broker, string ApplicationID, string TypeName, Guid orgUnitCID)
        {
            #region Filling Dataset for first file
            if (accounts != null && accounts.Count > 0)
            {
                foreach (AccountType _each in accounts)
                {
                    if (_each == AccountType.CMADIFM || _each == AccountType.CMADIWM || _each == AccountType.CMADIY)
                    {
                        #region CMA
                        AccountOpeningBankWestCSVColumns obj = new AccountOpeningBankWestCSVColumns();
                        CSVInstance csvinstance = new CSVInstance(Broker);
                        ClientIndividualEntityCSV _entitycsv = csvinstance.GetEntityConcreteInstance(_entity);
                        DataRow dr = value.Tables[obj.Table.TableName].NewRow();
                        dr[obj.Col_ApplicationID] = ApplicationID;
                        dr[obj.Col_ApplicationDate] = DateTime.Now.ToString("dd-MM-yy");
                        dr[obj.Col_ApplicationStatus] = "New";
                        dr[obj.Col_CMTOpenDate] = "";
                        dr[obj.Col_OpenAccountApplicationCreatorUserID] = "";
                        dr[obj.Col_OpenAccountApplicationApplicantType] = CSVUtilities.GetBankWestType(TypeName);
                        dr[obj.Col_OpenAccountApplicationAccountOption] = "Investor Plus";
                        dr[obj.Col_OpenAccountApplicationProductType] = "CMT";
                        dr[obj.Col_OpenAccountApplicationTrusteesCompany] = String.Empty;
                        dr[obj.Col_TrusteesCompanyDetailEntityName1] = String.Empty;
                        dr[obj.Col_TrusteesCompanyDetailEntityName2] = String.Empty;
                        dr[obj.Col_TrusteesCompanyDetailACNABN] = String.Empty;
                        dr[obj.Col_TrusteesCompanyDetailABN] = String.Empty;
                        if (_entity.ABN.RemoveWhiteSpacesStartEndInBetween() == string.Empty)
                            dr[obj.Col_TrusteesCompanyDetailABN] = String.Empty;
                        dr[obj.Col_TrusteesCompanyDetailACNARBN] = "";
                        dr[obj.Col_TrusteesCompanyDetailContactName] = "";
                        dr[obj.Col_TrusteesCompanyDetailContactEmail] = "";
                        dr[obj.Col_TrusteesCompanyDetailContactPhPrefix] = "";
                        dr[obj.Col_TrusteesCompanyDetailContactPh] = "";
                        dr[obj.Col_TrusteesCompanyDetailTrustType] = CSVUtilities.GetTrustType(TypeName);
                        if (_entitycsv.TFN_ABN_ARBN.Length == 8)
                            _entitycsv.TFN_ABN_ARBN = "0" + _entitycsv.TFN_ABN_ARBN;
                        dr[obj.Col_TFNDetailTFN] = _entitycsv.TFN_ABN_ARBN;
                        dr[obj.Col_TFNDetailNRC] = "";
                        dr[obj.Col_TFNDetailExemptionCategory] = "";
                        dr[obj.Col_TFNDetailTIN] = _entitycsv.TIN;
                        string temp1 = string.Empty;
                        string temp2 = string.Empty;
                        _entitycsv.LegalName.SplitInTwo(40, "atf", ref temp1, ref temp2);
                        dr[obj.Col_CorpTrusteeDetailEntityName1] = String.Empty; ;
                        dr[obj.Col_CorpTrusteeDetailEntityName2] = String.Empty; ;
                        dr[obj.Col_CorpTrusteeDetailACNABN] = "";
                        dr[obj.Col_CorpTrusteeDetailABN] = "";
                        dr[obj.Col_CorpTrusteeDetailACNARBN] = "";
                        dr[obj.Col_CorpTrusteeDetailCountryOfEstablishment] = "";
                        dr[obj.Col_OpenAccountApplicationInitialInvestmentAmount] = "";
                        dr[obj.Col_OpenAccountApplicationMannerOfOperation] = "Anyone";
                        dr[obj.Col_OpenAccountApplicationFinancialAdviserAccess] = "";
                        dr[obj.Col_OpenAccountApplicationAdvisorFirmAccess] = "General";
                        dr[obj.Col_OpenAccountApplicationLimitedAccessApproved] = "FALSE";
                        dr[obj.Col_AccessFacilityType] = "Telephone Access";

                        Oritax.TaxSimp.Common.AddressEntity applicatantsAddress = SetApplicantsAndSignatories(obj, dr, _entitycsv.Applicants); ;
                        SetAddresses(obj, dr, _entitycsv.Address, applicatantsAddress);
                        SetBankwestCommonData(Broker, ApplicationID, obj, dr, _entitycsv.AccessFacilities, orgUnitCID, _each);

                        value.Tables["tblAccountOpeningBankWest"].Rows.Add(dr);
                        #endregion

                        #region Filling Dataset for Second file

                        CSVInstance csvinstance2 = new CSVInstance(Broker);
                        ClientIndividualEntityCSV _entitycsv2 = csvinstance2.GetEntityConcreteInstance(_entity);
                        SetSignatoriesApplicants(value, _entity.Address, ApplicationID, _entitycsv2.Applicants);

                        #endregion
                    }
                }
            }
            #endregion

        }

        public static void FillClientsDetails(ExportAllClientDetailsDS value, string clientID, Guid cid, string typeName, ClientIndividualEntity entity, IdentityCM Parent, ICMBroker Broker)
        {
            AddClientDetails(value, clientID,Parent,entity.Name,entity.TFN??string.Empty,entity.ABN??string.Empty,entity.ChessTransferofSecurity,entity.Address,entity.AuthoritytoAct, Broker);
            AddSignatoryDetails(value, clientID, entity.Applicants, Broker);
            AddSignatoryModelDetails(value, clientID, entity.Servicetype);

        }
        public static void FillClientsDetails(ExportAllClientDetailsDS value, string clientID, Guid cid, string typeName, CorporateEntity entity, IdentityCM Parent, ICMBroker Broker)
        {
            AddClientDetails(value, clientID, Parent, entity.Name, entity.TFN ?? string.Empty, entity.ABN ?? string.Empty, entity.ChessTransferofSecurity, entity.Address, entity.AuthoritytoAct, Broker);
            AddSignatoryDetails(value, clientID, entity.Signatories, Broker);
            AddSignatoryModelDetails(value, clientID, entity.Servicetype);

        }
        private static void AddSignatoryModelDetails(ExportAllClientDetailsDS value, string clientID, Common.ServiceType serviceType)
        {
            if (serviceType != null)
            {
                if (serviceType.DO_IT_FOR_ME && serviceType.DoItForMe != null)
                {
                    value.ClientModelTable.AddRow(clientID,serviceType.DoItForMe.ProgramCode,serviceType.DoItForMe.ProgramName);
                }
                if (serviceType.DO_IT_WITH_ME && serviceType.DoItWithMe != null)
                {
                    value.ClientModelTable.AddRow(clientID, serviceType.DoItWithMe.ProgramCode, serviceType.DoItWithMe.ProgramName);
                }
                if (serviceType.DO_IT_YOURSELF && serviceType.DoItYourSelf != null)
                {
                    value.ClientModelTable.AddRow(clientID, serviceType.DoItYourSelf.ProgramCode, serviceType.DoItYourSelf.ProgramName);
                }
            }
        }
        private static void AddSignatoryDetails(ExportAllClientDetailsDS value, string clientID, List<Common.IdentityCMDetail> individuals, ICMBroker Broker)
        {
            foreach (var identityCmDetail in individuals)
            {
                var indi=Broker.GetCMImplementation(identityCmDetail.Clid, identityCmDetail.Csid) as OrganizationUnitCM;
                if(indi!=null)
                {
                    var entity = indi.ClientEntity as IndividualEntity;
                    Dictionary<AddressPart, string> RAddress = null;
                    Dictionary<AddressPart, string> MAddress = null;
                  
                    if (entity.ResidentialAddress != null)
                    {
                       RAddress = entity.ResidentialAddress.GetAddressDictionary();
                    }
                    if (entity.MailingAddress != null)
                    {
                        MAddress = entity.MailingAddress.GetAddressDictionary();
                    }

                    value.ClientSignatoryDetailsTable.AddRow(clientID, entity.Fullname, entity.TFN, entity.EmailAddress, entity.HomePhoneNumber.PhoneNumberToString(), entity.WorkPhoneNumber.PhoneNumberToString(), entity.MobilePhoneNumber.MobileNumberToString(),RAddress,MAddress);

                     Broker.ReleaseBrokerManagedComponent(indi);
                }


            }
            
        }
        private static void AddClientDetails(ExportAllClientDetailsDS value, string clientID, IdentityCM Parent, string Name, string TFN, string ABN, ClientChessTransferofSecurityEntity ChessTransferofSecurity, Common.DualAddressEntity address, ClientAuthoritytoActEntity AuthoritytoAct, ICMBroker Broker)
        {
            string advisorName = string.Empty;

            if (Parent != null)
            {
                var bmc = Broker.GetCMImplementation(Parent.Clid, Parent.Csid);
                if (bmc != null)
                {
                    advisorName = bmc.Name;
                    Broker.ReleaseBrokerManagedComponent(bmc);
                }
            }

            string investorStatus = string.Empty;
            string limitpowerTo = string.Empty;
            if (AuthoritytoAct != null)
            {
                if (AuthoritytoAct.InvestorStatusWholesaleInvestor)
                {
                    investorStatus = "Wholesale Investor";
                }
                else if (AuthoritytoAct.InvestorStatusProfessionalInvestor)
                {
                    investorStatus = "Professional Investor";
                }
                else if (AuthoritytoAct.InvestorStatusRetailInvestor)
                {
                    investorStatus = "Retail Investor";
                }
                else if (AuthoritytoAct.InvestorStatusSophisticatedInvestor)
                {
                    investorStatus = "Sophisticated Investor";
                }

                if (AuthoritytoAct.EclipseOnline)
                    limitpowerTo = "Eclipse Online,";

                if (AuthoritytoAct.AustralianMoneyMarket)
                    limitpowerTo += "Australian Money Market,";
            }
            Dictionary<AddressPart, string> BAddress = null;
            Dictionary<AddressPart, string> RAddress = null;
            Dictionary<AddressPart, string> RIAddress = null;
            Dictionary<AddressPart, string> MAddress = null;
            Dictionary<AddressPart, string> DMAddress = null;
           
            if (address != null)
            {
                BAddress = address.BusinessAddress.GetAddressDictionary();
                RAddress = address.ResidentialAddress.GetAddressDictionary();
                RIAddress = address.RegisteredAddress.GetAddressDictionary();
                MAddress = address.MailingAddress.GetAddressDictionary();
                DMAddress = address.DuplicateMailingAddress.GetAddressDictionary();
            }
            Dictionary<AddressPart, string> CRAddress = null;
            if (ChessTransferofSecurity != null)
            {
                CRAddress = ChessTransferofSecurity.GetAddressDictionary();
            }
            value.ClientDetailsTable.AddRow(clientID, Name, TFN, ABN, advisorName, investorStatus, limitpowerTo, BAddress, RAddress, RIAddress, MAddress, DMAddress, CRAddress);
        }

       
        #endregion

        #region Export State Street

        public static void FillStateStreetDataset(DataSet value, CorporateEntity _entity, ICMBroker Broker, string ApplicationID, string TypeName, bool IsExportedSS, string clientID)
        {
            AOIDCSVColumns obj = new AOIDCSVColumns();
            DataRow dr = value.Tables[obj.Table.TableName].NewRow();
            CSVInstance csvinstance = new CSVInstance(Broker);
            CorporateCSV _entitycsv = csvinstance.GetEntityConcreteInstance(_entity);

            dr[obj.Col_UnitholderCode] = clientID;
            string UnitholderSubCode = "IV";

            if ((!string.IsNullOrEmpty(_entitycsv.ClientId)) && _entitycsv.ClientId.Length > 2)
            {
                UnitholderSubCode = _entitycsv.ClientId.Substring(0, 2);
            }
            dr[obj.Col_UnitholderSubCode] = UnitholderSubCode;

            string temp1 = "";
            string temp2 = "";
            _entitycsv.LegalName.SplitInTwo(35, "atf", ref temp1, ref temp2);

            dr[obj.Col_UnitholderName1] = temp1;
            dr[obj.Col_UnitholderName2] = temp2;

            dr[obj.Col_PostalAddress1] = "C/- Innova Portfolio Management P/L";
            dr[obj.Col_PostalAddress2] = "3/36 Bydown Street";
            dr[obj.Col_PostalAddress3] = "NEUTRAL BAY  NSW";
            dr[obj.Col_PostalAddress4] = "";
            dr[obj.Col_PostCode] = "2089";

            dr[obj.Col_PhoneNumber1] = "";
            dr[obj.Col_PhoneNumber2] = "";
            dr[obj.Col_FaxNumber] = "";
            if (_entitycsv.Contacts.Count > 0)
            {
                dr[obj.Col_EmailAddress] = _entitycsv.Contacts[0].EmailAddress;
            }

            dr[obj.Col_ShortName] = "";
            dr[obj.Col_DateAccountOpened] = DateTime.Now.ToString("yyyyMMdd");
            OrganizationType type;
            if (System.Enum.TryParse(TypeName, out type))
            {
                switch (type)
                {
                    case OrganizationType.ClientOtherTrustsCorporate:
                        dr[obj.Col_UnitholderInvestorType] = "TR";
                        dr[obj.Col_InvestorType] = "TR";
                        break;
                    case OrganizationType.ClientSMSFCorporateTrustee:
                        dr[obj.Col_UnitholderInvestorType] = "SF";
                        dr[obj.Col_InvestorType] = "SF";
                        break;
                    case OrganizationType.ClientSoleTrader:
                        dr[obj.Col_UnitholderInvestorType] = "CO";
                        dr[obj.Col_InvestorType] = "CO";
                        if (_entitycsv.Signatories.Count > 0)
                        {
                            dr[obj.Col_FirstName] = _entitycsv.Signatories[0].Name;
                            dr[obj.Col_MiddleName] = "";
                            dr[obj.Col_LastName] = _entitycsv.Signatories[0].Surname;
                            dr[obj.Col_DateofBirth] = "00000000";
                            dr[obj.Col_Gender] = "U";
                        }
                        break;
                    default:
                        dr[obj.Col_UnitholderInvestorType] = "CO";
                        dr[obj.Col_InvestorType] = "CO";
                        break;
                }
            }
            dr[obj.Col_DistributionOption] = "ACH01";
            dr[obj.Col_CategoryCode3] = "RETL";
            dr[obj.Col_Active] = "Y";
            dr[obj.Col_Units] = "0";
            if (_entitycsv.Signatories.Count > 0)
            {
                dr[obj.Col_FirstName] = _entitycsv.Signatories[0].Name;
                dr[obj.Col_MiddleName] = "";
                dr[obj.Col_LastName] = _entitycsv.Signatories[0].Surname;
                dr[obj.Col_DateofBirth] = "00000000";
                dr[obj.Col_Gender] = "U";
            }
            if (type == OrganizationType.ClientSoleTrader)
            {
                dr[obj.Col_Name1] = "";
            }
            else
            {
                dr[obj.Col_Name1] = _entity.Name;
            }

            if (temp2.ToLower().Contains("superannuation") || temp2.ToLower().Contains("super annuation"))
            {
                temp2 = temp2.Replace("Superannuation", "Super");
                temp2 = temp2.Replace("SuperAnnuation", "Super");
                temp2 = temp2.Replace("superannuation", "Super");
                temp2 = temp2.Replace("Super annuation", "Super");
                temp2 = temp2.Replace("Super Annuation", "Super");
                temp2 = temp2.Replace("super annuation", "Super");
                dr[obj.Col_Name2] = temp2;
            }
            else
            {
                dr[obj.Col_Name2] = temp2;
            }
            dr[obj.Col_Name3] = "";
            dr[obj.Col_Name4] = "";
            Oritax.TaxSimp.Common.AddressEntity address = new Oritax.TaxSimp.Common.AddressEntity();
            if (_entitycsv.Address.BusinessAddress != null && !string.IsNullOrEmpty(_entitycsv.Address.BusinessAddress.Addressline1))
                address = _entitycsv.Address.BusinessAddress;
            else if (_entitycsv.Address.MailingAddress != null && !string.IsNullOrEmpty(_entitycsv.Address.MailingAddress.Addressline1))
                address = _entitycsv.Address.MailingAddress;
            else if (_entitycsv.Address.DuplicateMailingAddress != null && !string.IsNullOrEmpty(_entitycsv.Address.DuplicateMailingAddress.Addressline1))
                address = _entitycsv.Address.DuplicateMailingAddress;
            else if (_entitycsv.Address.RegisteredAddress != null && !string.IsNullOrEmpty(_entitycsv.Address.RegisteredAddress.Addressline1))
                address = _entitycsv.Address.RegisteredAddress;
            dr[obj.Col_Address1] = address.Addressline1;
            dr[obj.Col_Address2] = address.Addressline2;
            dr[obj.Col_City] = address.Suburb;
            dr[obj.Col_State] = GetCorrectStateByPostCode(address.PostCode, AusStates.ReturnShortStateCodeFromLong(address.State));
            dr[obj.Col_PostCode2] = address.PostCode;
            dr[obj.Col_Country] = address.Country;
            dr[obj.Col_TaxCountry] = address.Country;
            dr[obj.Col_TFN_ABN] = _entitycsv.ABN;
            ICalculationModule objOrganisation = Broker.GetWellKnownCM(WellKnownCM.Organization);
            IModelStructure organization = (objOrganisation as IModelStructure);
            Oritax.TaxSimp.Common.BankAccountEntity bankAccount = null;
            if (_entitycsv.Servicetype.DO_IT_FOR_ME)
            {
                if (organization != null)
                {
                    var model = organization.Model.FirstOrDefault(e => e.ServiceType == ServiceTypes.DoItForMe && e.ProgramCode == _entitycsv.Servicetype.DoItForMe.ProgramCode);

                    if (model != null)
                    {
                        bankAccount = GetCmBankAccount(_entitycsv, csvinstance, model);

                    }
                }
            }
            if (bankAccount == null && _entitycsv.Servicetype.DO_IT_WITH_ME)
            {
                bankAccount = GetCmBankAccount(_entitycsv, csvinstance, _entitycsv.Servicetype.DoItWithMe.Model);
            }
            if (bankAccount == null && _entitycsv.Servicetype.DO_IT_YOURSELF)
            {
                bankAccount = GetCmBankAccount(_entitycsv, csvinstance, _entitycsv.Servicetype.DoItYourSelf.Model);
            }
            if (bankAccount != null)
            {
                SetbankAccountValues(dr, temp1, temp2, obj, bankAccount);
            }
            value.Tables[obj.Table.TableName].Rows.Add(dr);
        }

        public static void FillStateStreetDataset(DataSet value, ClientIndividualEntity _entity, ICMBroker Broker, string ApplicationID, string TypeName, bool IsExportedSS, string clientID)
        {
            AOIDCSVColumns obj = new AOIDCSVColumns();
            DataRow dr = value.Tables[obj.Table.TableName].NewRow();
            CSVInstance csvinstance = new CSVInstance(Broker);
            ClientIndividualEntityCSV _entitycsv = csvinstance.GetEntityConcreteInstance(_entity);

            dr[obj.Col_UnitholderCode] = clientID;
            string UnitholderSubCode = "IV";
            if ((!string.IsNullOrEmpty(_entitycsv.ClientId)) && _entitycsv.ClientId.Length > 2)
            {
                UnitholderSubCode = _entitycsv.ClientId.Substring(0, 2);
            }
            dr[obj.Col_UnitholderSubCode] = UnitholderSubCode;

            string temp1 = "";
            string temp2 = "";
            _entitycsv.LegalName.SplitInTwo(35, "atf", ref temp1, ref temp2);
            dr[obj.Col_UnitholderName1] = temp1;
            dr[obj.Col_UnitholderName2] = temp2;

            dr[obj.Col_PostalAddress1] = "C/- Innova Portfolio Management P/L";
            dr[obj.Col_PostalAddress2] = "3/36 Bydown Street";
            dr[obj.Col_PostalAddress3] = "NEUTRAL BAY  NSW";
            dr[obj.Col_PostalAddress4] = "";
            dr[obj.Col_PostCode] = "2089";

            dr[obj.Col_PhoneNumber1] = "";
            dr[obj.Col_PhoneNumber2] = "";
            dr[obj.Col_FaxNumber] = "";
            dr[obj.Col_ShortName] = "";
            dr[obj.Col_DateAccountOpened] = DateTime.Now.ToString("yyyyMMdd");

            OrganizationType type;
            if (System.Enum.TryParse(TypeName, out type))
            {
                switch (type)
                {
                    case OrganizationType.ClientOtherTrustsIndividual:
                        dr[obj.Col_UnitholderInvestorType] = "TR";
                        dr[obj.Col_InvestorType] = "TR";
                        break;
                    case OrganizationType.ClientSMSFIndividualTrustee:
                        dr[obj.Col_UnitholderInvestorType] = "SF";
                        dr[obj.Col_InvestorType] = "SF";
                        break;
                    default:
                        dr[obj.Col_UnitholderInvestorType] = "IN";
                        dr[obj.Col_InvestorType] = "IN";
                        break;
                }
            }
            dr[obj.Col_DistributionOption] = "ACH01";
            dr[obj.Col_CategoryCode3] = "RETL";
            dr[obj.Col_Active] = "Y";
            dr[obj.Col_Units] = "0";

            if (_entitycsv.Applicants.Count > 0)
            {
                dr[obj.Col_FirstName] = _entitycsv.Applicants[0].Name;
                dr[obj.Col_MiddleName] = "";
                dr[obj.Col_LastName] = _entitycsv.Applicants[0].Surname;
                dr[obj.Col_DateofBirth] = "00000000";
                dr[obj.Col_Gender] = "U";
                dr[obj.Col_EmailAddress] = _entitycsv.Applicants[0].EmailAddress;
            }

            if (type == OrganizationType.ClientSoleTrader)
            {
                dr[obj.Col_Name1] = "";
            }
            else
            {
                dr[obj.Col_Name1] = _entity.Name;
            }

            Oritax.TaxSimp.Common.AddressEntity address = new Oritax.TaxSimp.Common.AddressEntity();

            if (_entitycsv.Address.BusinessAddress != null && !string.IsNullOrEmpty(_entitycsv.Address.BusinessAddress.Addressline1))
                address = _entitycsv.Address.BusinessAddress;
            else if (_entitycsv.Address.MailingAddress != null && !string.IsNullOrEmpty(_entitycsv.Address.MailingAddress.Addressline1))
                address = _entitycsv.Address.MailingAddress;
            else if (_entitycsv.Address.DuplicateMailingAddress != null && !string.IsNullOrEmpty(_entitycsv.Address.DuplicateMailingAddress.Addressline1))
                address = _entitycsv.Address.DuplicateMailingAddress;
            else if (_entitycsv.Address.RegisteredAddress != null && !string.IsNullOrEmpty(_entitycsv.Address.RegisteredAddress.Addressline1))
                address = _entitycsv.Address.RegisteredAddress;

            dr[obj.Col_Address1] = address.Addressline1;
            dr[obj.Col_Address2] = address.Addressline2;

            dr[obj.Col_City] = address.Suburb;
            dr[obj.Col_State] = GetCorrectStateByPostCode(address.PostCode, AusStates.ReturnShortStateCodeFromLong(address.State));
            dr[obj.Col_PostCode2] = address.PostCode;
            dr[obj.Col_Country] = address.Country;
            dr[obj.Col_TaxCountry] = address.Country;
            dr[obj.Col_TFN_ABN] = _entitycsv.TFN_ABN_ARBN;

            ICalculationModule objOrganisation = Broker.GetWellKnownCM(WellKnownCM.Organization);
            IModelStructure organization = (objOrganisation as IModelStructure);
            Oritax.TaxSimp.Common.BankAccountEntity bankAccount = null;

            if (_entitycsv.Servicetype.DO_IT_FOR_ME)
            {
                if (organization != null)
                {
                    var model = organization.Model.FirstOrDefault(e => e.ServiceType == ServiceTypes.DoItForMe && e.ProgramCode == _entitycsv.Servicetype.DoItForMe.ProgramCode);

                    if (model != null)
                    {
                        bankAccount = GetCmBankAccount(_entitycsv, csvinstance, model);


                    }
                }
            }

            if (bankAccount == null && _entitycsv.Servicetype.DO_IT_WITH_ME)
            {


                bankAccount = GetCmBankAccount(_entitycsv, csvinstance, _entitycsv.Servicetype.DoItWithMe.Model);



            }
            if (bankAccount == null && _entitycsv.Servicetype.DO_IT_YOURSELF)
            {


                bankAccount = GetCmBankAccount(_entitycsv, csvinstance, _entitycsv.Servicetype.DoItYourSelf.Model);

            }


            if (bankAccount != null)
                SetbankAccountValues(dr, temp1, temp2, obj, bankAccount);

            value.Tables[obj.Table.TableName].Rows.Add(dr);

        }

        public static void FillStateStreetDatasetRecon(DataSet value, CorporateEntity _entity, ICMBroker Broker, string ApplicationID, string TypeName, bool IsExportedSS, string clientID)
        {
            AOIDCSVColumnsRecon obj = new AOIDCSVColumnsRecon();
            DataRow dr = value.Tables[obj.Table.TableName].NewRow();
            CSVInstance csvinstance = new CSVInstance(Broker);
            CorporateCSV _entitycsv = csvinstance.GetEntityConcreteInstance(_entity);

            dr[obj.Col_UnitholderCode] = clientID;
            string UnitholderSubCode = "IV";

            if ((!string.IsNullOrEmpty(_entitycsv.ClientId)) && _entitycsv.ClientId.Length > 2)
            {
                UnitholderSubCode = _entitycsv.ClientId.Substring(0, 2);
            }
            dr[obj.Col_UnitholderSubCode] = UnitholderSubCode;

            string temp1 = "";
            string temp2 = "";
            _entitycsv.LegalName.SplitInTwo(35, "atf", ref temp1, ref temp2);

            dr[obj.Col_UnitholderName1] = temp1;
            dr[obj.Col_UnitholderName2] = temp2;

            dr[obj.Col_PostalAddress1] = "C/- Innova Portfolio Management P/L";
            dr[obj.Col_PostalAddress2] = "3/36 Bydown Street";
            dr[obj.Col_PostalAddress3] = "NEUTRAL BAY  NSW";
            dr[obj.Col_PostalAddress4] = "";
            dr[obj.Col_PostCode] = "2089";

            dr[obj.Col_PhoneNumber1] = "";
            dr[obj.Col_PhoneNumber2] = "";
            dr[obj.Col_FaxNumber] = "";
            if (_entitycsv.Contacts.Count > 0)
            {
                dr[obj.Col_EmailAddress] = _entitycsv.Contacts[0].EmailAddress;
            }

            dr[obj.Col_ShortName] = "";
            dr[obj.Col_DateAccountOpened] = DateTime.Now.ToString("yyyyMMdd");
            OrganizationType type;
            if (System.Enum.TryParse(TypeName, out type))
            {
                switch (type)
                {
                    case OrganizationType.ClientOtherTrustsCorporate:
                        dr[obj.Col_UnitholderInvestorType] = "TR";
                        dr[obj.Col_InvestorType] = "TR";
                        break;
                    case OrganizationType.ClientSMSFCorporateTrustee:
                        dr[obj.Col_UnitholderInvestorType] = "SF";
                        dr[obj.Col_InvestorType] = "SF";
                        break;
                    case OrganizationType.ClientSoleTrader:
                        dr[obj.Col_UnitholderInvestorType] = "CO";
                        dr[obj.Col_InvestorType] = "CO";
                        if (_entitycsv.Signatories.Count > 0)
                        {
                            dr[obj.Col_FirstName] = _entitycsv.Signatories[0].Name;
                            dr[obj.Col_MiddleName] = "";
                            dr[obj.Col_LastName] = _entitycsv.Signatories[0].Surname;
                            dr[obj.Col_DateofBirth] = "00000000";
                            dr[obj.Col_Gender] = "U";
                        }
                        break;
                    default:
                        dr[obj.Col_UnitholderInvestorType] = "CO";
                        dr[obj.Col_InvestorType] = "CO";
                        break;
                }
            }
            dr[obj.Col_DistributionOption] = "ACH01";
            dr[obj.Col_CategoryCode3] = "RETL";
            dr[obj.Col_Active] = "Y";
            dr[obj.Col_Units] = "0";
            if (_entitycsv.Signatories.Count > 0)
            {
                dr[obj.Col_FirstName] = _entitycsv.Signatories[0].Name;
                dr[obj.Col_MiddleName] = "";
                dr[obj.Col_LastName] = _entitycsv.Signatories[0].Surname;
                dr[obj.Col_DateofBirth] = "00000000";
                dr[obj.Col_Gender] = "U";
            }
            if (type == OrganizationType.ClientSoleTrader)
            {
                dr[obj.Col_Name1] = "";
            }
            else
            {
                dr[obj.Col_Name1] = _entity.Name;
            }

            if (temp2.ToLower().Contains("superannuation") || temp2.ToLower().Contains("super annuation"))
            {
                temp2 = temp2.Replace("Superannuation", "Super");
                temp2 = temp2.Replace("SuperAnnuation", "Super");
                temp2 = temp2.Replace("superannuation", "Super");
                temp2 = temp2.Replace("Super annuation", "Super");
                temp2 = temp2.Replace("Super Annuation", "Super");
                temp2 = temp2.Replace("super annuation", "Super");
                dr[obj.Col_Name2] = temp2;
            }
            else
            {
                dr[obj.Col_Name2] = temp2;
            }
            dr[obj.Col_Name3] = "";
            dr[obj.Col_Name4] = "";
            Oritax.TaxSimp.Common.AddressEntity address = new Oritax.TaxSimp.Common.AddressEntity();
            if (_entitycsv.Address.BusinessAddress != null && !string.IsNullOrEmpty(_entitycsv.Address.BusinessAddress.Addressline1))
                address = _entitycsv.Address.BusinessAddress;
            else if (_entitycsv.Address.MailingAddress != null && !string.IsNullOrEmpty(_entitycsv.Address.MailingAddress.Addressline1))
                address = _entitycsv.Address.MailingAddress;
            else if (_entitycsv.Address.DuplicateMailingAddress != null && !string.IsNullOrEmpty(_entitycsv.Address.DuplicateMailingAddress.Addressline1))
                address = _entitycsv.Address.DuplicateMailingAddress;
            else if (_entitycsv.Address.RegisteredAddress != null && !string.IsNullOrEmpty(_entitycsv.Address.RegisteredAddress.Addressline1))
                address = _entitycsv.Address.RegisteredAddress;
            dr[obj.Col_Address1] = address.Addressline1;
            dr[obj.Col_Address2] = address.Addressline2;
            dr[obj.Col_City] = address.Suburb;
            dr[obj.Col_State] = GetCorrectStateByPostCode(address.PostCode, AusStates.ReturnShortStateCodeFromLong(address.State));
            dr[obj.Col_PostCode2] = address.PostCode;
            dr[obj.Col_Country] = address.Country;
            dr[obj.Col_TaxCountry] = address.Country;
            dr[obj.Col_TFN_ABN] = _entitycsv.ABN;
            ICalculationModule objOrganisation = Broker.GetWellKnownCM(WellKnownCM.Organization);
            IModelStructure organization = (objOrganisation as IModelStructure);
            Oritax.TaxSimp.Common.BankAccountEntity bankAccount = null;



            if (_entitycsv.Servicetype.DO_IT_FOR_ME)
            {
                if (organization != null)
                {
                    var model = organization.Model.FirstOrDefault(e => e.ServiceType == ServiceTypes.DoItForMe && e.ProgramCode == _entitycsv.Servicetype.DoItForMe.ProgramCode);

                    if (model != null)
                    {
                        bankAccount = GetCmBankAccount(_entitycsv, csvinstance, model);

                    }
                }
            }
            if (bankAccount == null && _entitycsv.Servicetype.DO_IT_WITH_ME)
            {
                bankAccount = GetCmBankAccount(_entitycsv, csvinstance, _entitycsv.Servicetype.DoItWithMe.Model);
            }
            if (bankAccount == null && _entitycsv.Servicetype.DO_IT_YOURSELF)
            {
                bankAccount = GetCmBankAccount(_entitycsv, csvinstance, _entitycsv.Servicetype.DoItYourSelf.Model);
            }
            if (bankAccount != null)
            {
                SetbankAccountValues(dr, temp1, temp2, obj, bankAccount);
            }

            ReconcileSSBankAccounts(_entity.Servicetype, _entity.ListAccountProcess, Broker, obj, dr, _entitycsv.ABN, _entitycsv.TFN, organization);
            value.Tables[obj.Table.TableName].Rows.Add(dr);
        }

        private static void ReconcileSSBankAccounts(Oritax.TaxSimp.Common.ServiceType Servicetype, List<AccountProcessTaskEntity> ListAccountProcess, ICMBroker Broker, AOIDCSVColumnsRecon obj, DataRow dr, string abn, string tfn, IModelStructure organization)
        {
            string stateStreetBankAccoutBSB = dr[obj.Col_BankAccountNumber].ToString();
            string stateStreetBankAccoutNo = dr[obj.Col_BankAccountNumber].ToString();

            dr[obj.Col_CLIENTABN] = abn;
            dr[obj.Col_CLIENTTFN] = tfn;

            foreach (var accountProcessTaskEntity in ListAccountProcess)
            {
                if (accountProcessTaskEntity.LinkedEntity != null && accountProcessTaskEntity.LinkedEntity.Clid != Guid.Empty)
                {
                    var model = organization.Model.Where(mod => mod.ID == accountProcessTaskEntity.ModelID).FirstOrDefault();

                    if (model != null)
                    {
                        if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.BankAccount && accountProcessTaskEntity.LinkedEntity.Clid != Guid.Empty)
                        {
                            if (model.ServiceType == ServiceTypes.DoItForMe && Servicetype.DO_IT_FOR_ME)
                            {
                                IOrganizationUnit bankUnit = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as IOrganizationUnit;
                                BankAccountEntity bankEntity = bankUnit.ClientEntity as BankAccountEntity;
                                dr[obj.Col_CLIENTDIFMBANKACCOUNTBSB] = bankEntity.BSB;
                                dr[obj.Col_CLIENTDIFMBANKACCOUNTNO] = bankEntity.AccountNumber;

                                //Match It
                                if (dr[obj.Col_CLIENTDIFMBANKACCOUNTNO].ToString() == stateStreetBankAccoutNo)
                                    dr[obj.Col_CLIENTDIFMBANKACCOUNTMATCH] = "Yes";
                                else
                                    dr[obj.Col_CLIENTDIFMBANKACCOUNTMATCH] = "No";

                            }
                            else if (model.ServiceType == ServiceTypes.DoItWithMe && Servicetype.DO_IT_WITH_ME)
                            {
                                IOrganizationUnit bankUnit = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as IOrganizationUnit;
                                BankAccountEntity bankEntity = bankUnit.ClientEntity as BankAccountEntity;
                                dr[obj.Col_CLIENTDIWMBANKACCOUNTBSB] = bankEntity.BSB;
                                dr[obj.Col_CLIENTDIWMBANKACCOUNTNO] = bankEntity.AccountNumber;

                                //Match It
                                if (dr[obj.Col_CLIENTDIWMBANKACCOUNTNO].ToString() == stateStreetBankAccoutNo)
                                    dr[obj.Col_CLIENTDIWMBANKACCOUNTMATCH] = "Yes";
                                else
                                    dr[obj.Col_CLIENTDIWMBANKACCOUNTMATCH] = "No";
                            }
                        }
                    }
                }
            }
        }

        public static void FillStateStreetDatasetRecon(DataSet value, ClientIndividualEntity _entity, ICMBroker Broker, string ApplicationID, string TypeName, bool IsExportedSS, string clientID)
        {
            AOIDCSVColumnsRecon obj = new AOIDCSVColumnsRecon();
            DataRow dr = value.Tables[obj.Table.TableName].NewRow();
            CSVInstance csvinstance = new CSVInstance(Broker);
            ClientIndividualEntityCSV _entitycsv = csvinstance.GetEntityConcreteInstance(_entity);

            dr[obj.Col_UnitholderCode] = clientID;
            string UnitholderSubCode = "IV";
            if ((!string.IsNullOrEmpty(_entitycsv.ClientId)) && _entitycsv.ClientId.Length > 2)
            {
                UnitholderSubCode = _entitycsv.ClientId.Substring(0, 2);
            }
            dr[obj.Col_UnitholderSubCode] = UnitholderSubCode;

            string temp1 = "";
            string temp2 = "";
            _entitycsv.LegalName.SplitInTwo(35, "atf", ref temp1, ref temp2);
            dr[obj.Col_UnitholderName1] = temp1;
            dr[obj.Col_UnitholderName2] = temp2;

            dr[obj.Col_PostalAddress1] = "C/- Innova Portfolio Management P/L";
            dr[obj.Col_PostalAddress2] = "3/36 Bydown Street";
            dr[obj.Col_PostalAddress3] = "NEUTRAL BAY  NSW";
            dr[obj.Col_PostalAddress4] = "";
            dr[obj.Col_PostCode] = "2089";

            dr[obj.Col_PhoneNumber1] = "";
            dr[obj.Col_PhoneNumber2] = "";
            dr[obj.Col_FaxNumber] = "";
            dr[obj.Col_ShortName] = "";
            dr[obj.Col_DateAccountOpened] = DateTime.Now.ToString("yyyyMMdd");

            OrganizationType type;
            if (System.Enum.TryParse(TypeName, out type))
            {
                switch (type)
                {
                    case OrganizationType.ClientOtherTrustsIndividual:
                        dr[obj.Col_UnitholderInvestorType] = "TR";
                        dr[obj.Col_InvestorType] = "TR";
                        break;
                    case OrganizationType.ClientSMSFIndividualTrustee:
                        dr[obj.Col_UnitholderInvestorType] = "SF";
                        dr[obj.Col_InvestorType] = "SF";
                        break;
                    default:
                        dr[obj.Col_UnitholderInvestorType] = "IN";
                        dr[obj.Col_InvestorType] = "IN";
                        break;
                }
            }
            dr[obj.Col_DistributionOption] = "ACH01";
            dr[obj.Col_CategoryCode3] = "RETL";
            dr[obj.Col_Active] = "Y";
            dr[obj.Col_Units] = "0";

            if (_entitycsv.Applicants.Count > 0)
            {
                dr[obj.Col_FirstName] = _entitycsv.Applicants[0].Name;
                dr[obj.Col_MiddleName] = "";
                dr[obj.Col_LastName] = _entitycsv.Applicants[0].Surname;
                dr[obj.Col_DateofBirth] = "00000000";
                dr[obj.Col_Gender] = "U";
                dr[obj.Col_EmailAddress] = _entitycsv.Applicants[0].EmailAddress;
            }

            if (type == OrganizationType.ClientSoleTrader)
            {
                dr[obj.Col_Name1] = "";
            }
            else
            {
                dr[obj.Col_Name1] = _entity.Name;
            }

            Oritax.TaxSimp.Common.AddressEntity address = new Oritax.TaxSimp.Common.AddressEntity();

            if (_entitycsv.Address.BusinessAddress != null && !string.IsNullOrEmpty(_entitycsv.Address.BusinessAddress.Addressline1))
                address = _entitycsv.Address.BusinessAddress;
            else if (_entitycsv.Address.MailingAddress != null && !string.IsNullOrEmpty(_entitycsv.Address.MailingAddress.Addressline1))
                address = _entitycsv.Address.MailingAddress;
            else if (_entitycsv.Address.DuplicateMailingAddress != null && !string.IsNullOrEmpty(_entitycsv.Address.DuplicateMailingAddress.Addressline1))
                address = _entitycsv.Address.DuplicateMailingAddress;
            else if (_entitycsv.Address.RegisteredAddress != null && !string.IsNullOrEmpty(_entitycsv.Address.RegisteredAddress.Addressline1))
                address = _entitycsv.Address.RegisteredAddress;

            dr[obj.Col_Address1] = address.Addressline1;
            dr[obj.Col_Address2] = address.Addressline2;

            dr[obj.Col_City] = address.Suburb;
            dr[obj.Col_State] = GetCorrectStateByPostCode(address.PostCode, AusStates.ReturnShortStateCodeFromLong(address.State));
            dr[obj.Col_PostCode2] = address.PostCode;
            dr[obj.Col_Country] = address.Country;
            dr[obj.Col_TaxCountry] = address.Country;
            dr[obj.Col_TFN_ABN] = _entitycsv.TFN_ABN_ARBN;

            ICalculationModule objOrganisation = Broker.GetWellKnownCM(WellKnownCM.Organization);
            IModelStructure organization = (objOrganisation as IModelStructure);
            Oritax.TaxSimp.Common.BankAccountEntity bankAccount = null;

            if (_entitycsv.Servicetype.DO_IT_FOR_ME)
            {
                if (organization != null)
                {
                    var model = organization.Model.FirstOrDefault(e => e.ServiceType == ServiceTypes.DoItForMe && e.ProgramCode == _entitycsv.Servicetype.DoItForMe.ProgramCode);

                    if (model != null)
                    {
                        bankAccount = GetCmBankAccount(_entitycsv, csvinstance, model);
                    }
                }
            }

            if (bankAccount == null && _entitycsv.Servicetype.DO_IT_WITH_ME)
            {


                bankAccount = GetCmBankAccount(_entitycsv, csvinstance, _entitycsv.Servicetype.DoItWithMe.Model);



            }
            if (bankAccount == null && _entitycsv.Servicetype.DO_IT_YOURSELF)
            {


                bankAccount = GetCmBankAccount(_entitycsv, csvinstance, _entitycsv.Servicetype.DoItYourSelf.Model);

            }


            if (bankAccount != null)
                SetbankAccountValues(dr, temp1, temp2, obj, bankAccount);

            ReconcileSSBankAccounts(_entity.Servicetype, _entity.ListAccountProcess, Broker, obj, dr, _entitycsv.TFN_ABN_ARBN, _entitycsv.TFN_ABN_ARBN, organization);

            value.Tables[obj.Table.TableName].Rows.Add(dr);
        }


        private static Oritax.TaxSimp.Common.BankAccountEntity GetCmBankAccount(CorporateCSV _entitycsv, CSVInstance csvinstance, ModelEntity model)
        {
            Oritax.TaxSimp.Common.BankAccountEntity bankAccount = null;
            if (model != null)
                foreach (var asset in model.Assets)
                {
                    foreach (var product in asset.Products)
                    {
                        var task =
                            _entitycsv.ListAccountProcess.Where(
                                e => e.ModelID == model.ID && e.AssetID == asset.ID && e.TaskID == product.ID).FirstOrDefault();
                        // new TaskEntity() { ModelID = model.ID, AssetID = asset.ID, ServiceType = model.ServiceType.GetDescription(), TaskDescription = product.TaskDescription, Status = "", IsCompleted = task.IsCompleted, TaskID = product.ID, LinkedEntity = task.LinkedEntity, LinkedEntityType = product.EntityType, AssetName = asset.Name, ProductName = product.Name, IsExempted = task.IsExempted, OldStatus = "", Type = model.ServiceType }
                        if (task != null)
                            if (!string.IsNullOrEmpty(product.BankAccountType) && product.BankAccountType.Trim().ToLower() == "cma")
                            {
                                bankAccount = csvinstance.GetConcreteBankAccountEntityObject(task.LinkedEntity);
                                break;
                            }

                    }
                }
            return bankAccount;
        }

        private static Oritax.TaxSimp.Common.BankAccountEntity GetCmBankAccount(ClientIndividualEntityCSV _entitycsv, CSVInstance csvinstance, ModelEntity model)
        {
            Oritax.TaxSimp.Common.BankAccountEntity bankAccount = null;
            if (model != null)
                foreach (var asset in model.Assets)
                {
                    foreach (var product in asset.Products)
                    {
                        var task =
                            _entitycsv.ListAccountProcess.Where(
                                e => e.ModelID == model.ID && e.AssetID == asset.ID && e.TaskID == product.ID).FirstOrDefault();
                        if (task != null)
                            if (!string.IsNullOrEmpty(product.BankAccountType) && product.BankAccountType.Trim().ToLower() == "cma")
                            {
                                bankAccount = csvinstance.GetConcreteBankAccountEntityObject(task.LinkedEntity);
                                break;
                            }
                    }
                }
            return bankAccount;
        }

        private static void SetbankAccountValues(DataRow dr, string temp1, string temp2, AOIDCSVColumns obj, Oritax.TaxSimp.Common.BankAccountEntity _entitycsv)
        {
            dr[obj.Col_BankAccountType] = "ACH02";
            dr[obj.Col_BSB] = _entitycsv.BSB;
            dr[obj.Col_BankAccountNumber] = _entitycsv.AccountNumber;
            _entitycsv.Name.SplitInTwo(35, "atf", ref temp1, ref temp2);
            dr[obj.Col_BankAccountName] = temp1;
            if (temp2.ToLower().Contains("superannuation") || temp2.ToLower().Contains("super annuation"))
            {
                temp2 = temp2.Replace("Superannuation", "Super");
                temp2 = temp2.Replace("SuperAnnuation", "Super");
                temp2 = temp2.Replace("superannuation", "Super");
                temp2 = temp2.Replace("Super annuation", "Super");
                temp2 = temp2.Replace("Super Annuation", "Super");
                temp2 = temp2.Replace("super annuation", "Super");
                dr[obj.Col_BankAccountName2] = temp2;
            }
            else
            {
                dr[obj.Col_BankAccountName2] = temp2;
            }
        }

        private static void SetbankAccountValues(DataRow dr, string temp1, string temp2, AOIDCSVColumnsRecon obj, Oritax.TaxSimp.Common.BankAccountEntity _entitycsv)
        {
            dr[obj.Col_BankAccountType] = "ACH02";
            dr[obj.Col_BSB] = _entitycsv.BSB;
            dr[obj.Col_BankAccountNumber] = _entitycsv.AccountNumber;
            _entitycsv.Name.SplitInTwo(35, "atf", ref temp1, ref temp2);
            dr[obj.Col_BankAccountName] = temp1;
            if (temp2.ToLower().Contains("superannuation") || temp2.ToLower().Contains("super annuation"))
            {
                temp2 = temp2.Replace("Superannuation", "Super");
                temp2 = temp2.Replace("SuperAnnuation", "Super");
                temp2 = temp2.Replace("superannuation", "Super");
                temp2 = temp2.Replace("Super annuation", "Super");
                temp2 = temp2.Replace("Super Annuation", "Super");
                temp2 = temp2.Replace("super annuation", "Super");
                dr[obj.Col_BankAccountName2] = temp2;
            }
            else
            {
                dr[obj.Col_BankAccountName2] = temp2;
            }
        }

        #endregion

        public static void AddExportStatusDetails(ExportStatusDS ds, Guid cid, Guid clid, Guid csid, string clientId, string tradingname, string typeName, string organizationStatus, string applicationDate, string applicationId, bool isExportedSs, bool isExported, bool isExportedDb, bool hasDoItWithMeDoItForMe)
        {
            DataRow dr = ds.ExportStatusTable.NewRow();
            dr[ds.ExportStatusTable.CID] = cid;
            dr[ds.ExportStatusTable.CLID] = clid;
            dr[ds.ExportStatusTable.CSID] = csid;
            dr[ds.ExportStatusTable.CLIENTID] = clientId;
            dr[ds.ExportStatusTable.TRADINGNAME] = tradingname;
            dr[ds.ExportStatusTable.TYPE] = typeName;
            dr[ds.ExportStatusTable.STATUS] = organizationStatus;
            dr[ds.ExportStatusTable.APPLICATIONDATE] = applicationDate;
            dr[ds.ExportStatusTable.APPLICATIONID] = applicationId;
            dr[ds.ExportStatusTable.SSEXPORT] = isExportedSs;
            dr[ds.ExportStatusTable.BWEXPORT] = isExported;
            dr[ds.ExportStatusTable.DBEXPORT] = isExportedDb;

            switch (ds.ExportMode)
            {
                case ExportMode.BankWest:
                case ExportMode.DesktopBroker:
                    ds.ExportStatusTable.Rows.Add(dr);
                    break;
                case ExportMode.StateStreet:
                    if (hasDoItWithMeDoItForMe)
                        ds.ExportStatusTable.Rows.Add(dr);
                    break;
            }
        }

        public static Statuses SetExportStatusDetails(ExportStatusDS ds, Guid cid, bool isExportedSs, bool isExported, bool isExportedDb)
        {
            DataRow[] drs = ds.ExportStatusTable.Select(string.Format("{0}='{1}'", ds.ExportStatusTable.CID, cid));
            Statuses statuses = new Statuses { IsExportedSs = isExportedSs, IsExportedBw = isExported, IsExportedDb = isExportedDb };
            if (drs.Length > 0)
            {
                // Broker.SetStart();
                foreach (var dr in drs)
                {
                    switch (ds.ExportMode)
                    {
                        case ExportMode.StateStreet:
                            statuses.IsExportedSs = bool.Parse(dr[ds.ExportStatusTable.SSEXPORT].ToString());
                            break;
                        case ExportMode.BankWest:
                            statuses.IsExportedBw = bool.Parse(dr[ds.ExportStatusTable.BWEXPORT].ToString());
                            break;
                        case ExportMode.DesktopBroker:
                            statuses.IsExportedDb = bool.Parse(dr[ds.ExportStatusTable.DBEXPORT].ToString());
                            break;
                    }
                }
            }
            return statuses;
        }
    }
}

