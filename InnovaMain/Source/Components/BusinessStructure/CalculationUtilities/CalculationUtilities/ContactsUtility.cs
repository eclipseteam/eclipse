#region Using
using System;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.CalculationInterface;
using System.Collections.ObjectModel;

#endregion

namespace Oritax.TaxSimp.CM
{
    [Serializable]
    public class ContactsUtility
    {
        public static void FillInstitutionContactsDataset(InstitutionContactsDS ds, ObservableCollection<IndividualEntityCsidClidDetail> Contacts, ICMBroker Broker)
        {
            if (Contacts != null && Contacts.Count > 0)
            {
                foreach (var Contact in Contacts)
                {
                    var componenet = Broker.GetCMImplementation(Contact.Clid, Contact.Csid);
                    if (componenet != null)
                    {
                        componenet.GetData(ds);
                        Broker.ReleaseBrokerManagedComponent(componenet);
                    }
                }
            }

        }
    }
}