﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;

namespace Oritax.TaxSimp.CM
{
    public class SettledUnsettledUtilities
    {
        private static Dictionary<string, Guid> UnsettledCmList = new Dictionary<string, Guid>();
        public static SettledUnsetteledDS GetAllUnsetteledForClient(string clientID, ICMBroker broker)
        {
            string userName = "Administrator";

            if (broker.UserContext != null)
                userName = broker.UserContext.Identity.Name;
            var user = new UserEntity() { IsWebUser = true, CurrentUserName = userName };

            Guid Cid = CheckIfSettelledCmExists(clientID, broker, user, true);

            var ds = new SettledUnsetteledDS
            {
                Unit = new Data.OrganizationUnit
                {
                    ClientId = clientID,
                    CurrentUser = user,
                },
                CommandType = DatasetCommandTypes.Unsettle,
            };
            if (Cid != Guid.Empty)
            {
                var settledUnsettled = broker.GetBMCInstance(Cid);
                if (settledUnsettled != null)
                {
                    settledUnsettled.GetData(ds);
                    broker.ReleaseBrokerManagedComponent(settledUnsettled);
                }
            }

            return ds;
        }

        public static Guid CheckIfSettelledCmExists(string clientID, ICMBroker broker, UserEntity user, bool cached)
        {
            //Added this code for services since, services will not be using User Context. 

            string username = "Administrator";
            if (broker.UserContext != null)
                username = broker.UserContext.Identity.Name;

            if (cached && UnsettledCmList.ContainsKey(clientID))
            {
                return UnsettledCmList[clientID];
            }

            var ds = new SettledUnsetteledDS
            {
                Unit = new Data.OrganizationUnit
                {
                    Name = clientID,
                    CurrentUser = new UserEntity() { IsWebUser = true, CurrentUserName = username },
                    Type = ((int)OrganizationType.SettledUnsettled).ToString(),
                },
                CommandType = DatasetCommandTypes.Check,
                Command = (int)WebCommands.GetOrganizationUnitsByType

            };

            var org = broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            org.GetData(ds);
            broker.ReleaseBrokerManagedComponent(org);

            AddUpdateCMCuid(clientID, ds.Unit.Cid);
            return ds.Unit.Cid;
        }

        private static void AddUpdateCMCuid(string clientID, Guid Cid)
        {
            if (UnsettledCmList.ContainsKey(clientID))
            {
                var oldCid = UnsettledCmList[clientID];

                if (oldCid == Guid.Empty && Cid != Guid.Empty)
                    UnsettledCmList[clientID] = Cid;
            }
            else
            {
                UnsettledCmList.Add(clientID, Cid);
            }
        }

        public static void GetASXUnsettledAmountNUnits(SettledUnsetteledDS ds, Guid Accountcid, string accountNumber, out decimal unsettledBuy, out decimal unsettledSell, out decimal unsettledBuyunits, out decimal unsettledSellunits)
        {
            unsettledBuy = 0;
            unsettledSell = 0;
            unsettledBuyunits = 0;
            unsettledSellunits = 0;
            if (ds.SettledUnsetteledTable.Rows.Count == 0)
                return;

            var buyQuery = String.Format("{0}='{1}' and {2}='{3}'",
                                                               ds.SettledUnsetteledTable.ACCOUNTCID, Accountcid,
                                                               ds.SettledUnsetteledTable.TRADETYPE, TradeType.Buy);
            var sellQuery = String.Format("{0}='{1}' and {2}='{3}'",
                                                                 ds.SettledUnsetteledTable.ACCOUNTCID, Accountcid,
                                                                   ds.SettledUnsetteledTable.TRADETYPE, TradeType.Sell);
            unsettledBuy = GetSumOfColumn(ds.SettledUnsetteledTable, ds.SettledUnsetteledTable.AMOUNT, buyQuery);
            unsettledSell = GetSumOfColumn(ds.SettledUnsetteledTable, ds.SettledUnsetteledTable.AMOUNT, sellQuery);
            unsettledBuyunits = GetSumOfColumn(ds.SettledUnsetteledTable, ds.SettledUnsetteledTable.UNITS, buyQuery);
            unsettledSellunits = GetSumOfColumn(ds.SettledUnsetteledTable, ds.SettledUnsetteledTable.UNITS, sellQuery);

        }
        public static void GetASXUnsettledAmountNUnitsForInvementCode(SettledUnsetteledDS ds, string investmentCode, Guid productID, Guid Accountcid, string accountNumber, out decimal unsettledBuy, out decimal unsettledSell, out decimal unsettledBuyunits, out decimal unsettledSellunits, out decimal unsettledSellByUnits, out decimal unsettledSellByAmount)
        {
            unsettledBuy = 0;
            unsettledSell = 0;
            unsettledBuyunits = 0;
            unsettledSellunits = 0;
            unsettledSellByUnits = 0;
            unsettledSellByAmount = 0;

            if (ds.SettledUnsetteledTable.Rows.Count == 0)
                return;

            var buyQuery = String.Format("{0}='{1}' and {2}='{3}' and {4}='{5}'", ds.SettledUnsetteledTable.PRODUCTID, productID, ds.SettledUnsetteledTable.TRADETYPE, TradeType.Buy, ds.SettledUnsetteledTable.INVESTMENTCODE, investmentCode);
            var sellQuery = String.Format("{0}='{1}' and {2}='{3}' and {4}='{5}'", ds.SettledUnsetteledTable.PRODUCTID, productID, ds.SettledUnsetteledTable.TRADETYPE, TradeType.Sell, ds.SettledUnsetteledTable.INVESTMENTCODE, investmentCode);

            unsettledBuy = GetSumOfColumn(ds.SettledUnsetteledTable, ds.SettledUnsetteledTable.AMOUNT, buyQuery);
            unsettledSell = GetSumOfColumn(ds.SettledUnsetteledTable, ds.SettledUnsetteledTable.AMOUNT, sellQuery);
            unsettledBuyunits = GetSumOfColumn(ds.SettledUnsetteledTable, ds.SettledUnsetteledTable.UNITS, buyQuery);
            unsettledSellunits = GetSumOfColumn(ds.SettledUnsetteledTable, ds.SettledUnsetteledTable.UNITS, sellQuery);

            unsettledSellByUnits = GetASXSellByOrderPreferedBy(ds, investmentCode, productID, OrderPreferedBy.Units.ToString());
            unsettledSellByAmount = GetASXSellByOrderPreferedBy(ds, investmentCode, productID, OrderPreferedBy.Amount.ToString());
        }

        private static decimal GetASXSellByOrderPreferedBy(SettledUnsetteledDS ds, string investmentCode, Guid productID, string orderPreferedBy)
        {
            decimal sum = 0;
            foreach (DataRow row in ds.SettledUnsetteledTable.Rows)
            {
                var amount = (decimal)row[ds.SettledUnsetteledTable.AMOUNT];
                if (row[ds.SettledUnsetteledTable.PRODUCTID].ToString() == productID.ToString() &&
                    row[ds.SettledUnsetteledTable.TRADETYPE].ToString() == TradeType.Sell.ToString() &&
                    row[ds.SettledUnsetteledTable.INVESTMENTCODE].ToString() == investmentCode &&
                    row[ds.SettledUnsetteledTable.ORDERPREFEREDBY].ToString() == orderPreferedBy)
                {
                    if (orderPreferedBy == OrderPreferedBy.Amount.ToString())
                        sum += (Math.Abs(amount) * (decimal)0.0011) < 20 ? (Math.Abs(amount) - 20) : (Math.Abs(amount) - (Math.Abs(amount) * (decimal)0.0011));
                    else if (orderPreferedBy == OrderPreferedBy.Units.ToString())
                        sum += (Math.Abs(amount) * (decimal)0.0011) < 20 ? (Math.Abs(amount) * (decimal)0.9 - 20) : (Math.Abs(amount) * (decimal)0.9 - (Math.Abs(amount) * (decimal)0.0011));
                }
            }
            return sum;
        }

        private static decimal GetASXSellByOrderPreferedBy(SettledUnsetteledDS ds, Guid productID, string orderPreferedBy)
        {
            decimal sum = 0;
            foreach (DataRow row in ds.SettledUnsetteledTable.Rows)
            {
                var amount = (decimal)row[ds.SettledUnsetteledTable.AMOUNT];
                if (row[ds.SettledUnsetteledTable.PRODUCTID].ToString() == productID.ToString() &&
                    row[ds.SettledUnsetteledTable.TRADETYPE].ToString() == TradeType.Sell.ToString() &&
                    row[ds.SettledUnsetteledTable.ORDERPREFEREDBY].ToString() == orderPreferedBy)
                {
                    if (orderPreferedBy == OrderPreferedBy.Amount.ToString())
                        sum += (Math.Abs(amount) * (decimal)0.0011) < 20 ? (Math.Abs(amount) - 20) : (Math.Abs(amount) - (Math.Abs(amount) * (decimal)0.0011));
                    else if (orderPreferedBy == OrderPreferedBy.Units.ToString())
                        sum += (Math.Abs(amount) * (decimal)0.0011) < 20 ? (Math.Abs(amount) * (decimal)0.9 - 20) : (Math.Abs(amount) * (decimal)0.9 - (Math.Abs(amount) * (decimal)0.0011));
                }
            }
            return sum;
        }

        private static decimal GetMISSellByOrderPreferedBy(SettledUnsetteledDS ds, Guid productID, string orderPreferedBy)
        {
            decimal sum = 0;
            foreach (DataRow row in ds.SettledUnsetteledTable.Rows)
            {
                var amount = (decimal)row[ds.SettledUnsetteledTable.AMOUNT];
                if (row[ds.SettledUnsetteledTable.PRODUCTID].ToString() == productID.ToString() &&
                    row[ds.SettledUnsetteledTable.TRADETYPE].ToString() == TradeType.Sell.ToString() &&
                    row[ds.SettledUnsetteledTable.ORDERPREFEREDBY].ToString() == orderPreferedBy)
                {
                    if (orderPreferedBy == OrderPreferedBy.Amount.ToString())
                        sum += Math.Abs(amount);
                    else if (orderPreferedBy == OrderPreferedBy.Units.ToString())
                        sum += Math.Abs(amount)*(decimal) 0.9;
                }
            }
            return sum;
        }

        public static void GetASXUnsettledAmountNUnits(SettledUnsetteledDS ds, Guid productID, Guid Accountcid, string accountNumber, out decimal unsettledBuy, out decimal unsettledSell, out decimal unsettledBuyunits, out decimal unsettledSellunits, out decimal unsettledSellByUnits, out decimal unsettledSellByAmount)
        {
            unsettledBuy = 0;
            unsettledSell = 0;
            unsettledBuyunits = 0;
            unsettledSellunits = 0;
            unsettledSellByUnits = 0;
            unsettledSellByAmount = 0;

            if (ds.SettledUnsetteledTable.Rows.Count == 0)
                return;

            var buyQuery = String.Format("{0}='{1}' and {2}='{3}'", ds.SettledUnsetteledTable.PRODUCTID, productID, ds.SettledUnsetteledTable.TRADETYPE, TradeType.Buy);
            var sellQuery = String.Format("{0}='{1}' and {2}='{3}'", ds.SettledUnsetteledTable.PRODUCTID, productID, ds.SettledUnsetteledTable.TRADETYPE, TradeType.Sell);
            

            unsettledBuy = GetSumOfColumn(ds.SettledUnsetteledTable, ds.SettledUnsetteledTable.AMOUNT, buyQuery);
            unsettledSell = GetSumOfColumn(ds.SettledUnsetteledTable, ds.SettledUnsetteledTable.AMOUNT, sellQuery);
            unsettledBuyunits = GetSumOfColumn(ds.SettledUnsetteledTable, ds.SettledUnsetteledTable.UNITS, buyQuery);
            unsettledSellunits = GetSumOfColumn(ds.SettledUnsetteledTable, ds.SettledUnsetteledTable.UNITS, sellQuery);

            unsettledSellByUnits = GetASXSellByOrderPreferedBy(ds, productID, OrderPreferedBy.Units.ToString());
            unsettledSellByAmount = GetASXSellByOrderPreferedBy(ds, productID, OrderPreferedBy.Amount.ToString());
        }

        public static void GetMISUnsettledAmountNUnits(SettledUnsetteledDS ds, Guid productID, string code, out decimal unsettledBuy, out decimal unsettledSell, out decimal unsettledBuyunits, out decimal unsettledSellunits, out decimal unsettledSellByUnits, out decimal unsettledSellByAmount)
        {
            unsettledBuy = 0;
            unsettledSell = 0;
            unsettledBuyunits = 0;
            unsettledSellunits = 0;
            unsettledSellByUnits = 0;
            unsettledSellByAmount = 0;

            if (ds.SettledUnsetteledTable.Rows.Count == 0)
                return;

            var buyQuery = String.Format("{0}='{1}' and {2}='{3}'", ds.SettledUnsetteledTable.PRODUCTID, productID, ds.SettledUnsetteledTable.TRADETYPE, TradeType.Buy);
            var sellQuery = String.Format("{0}='{1}' and {2}='{3}'", ds.SettledUnsetteledTable.PRODUCTID, productID, ds.SettledUnsetteledTable.TRADETYPE, TradeType.Sell);
            

            unsettledBuy = GetSumOfColumn(ds.SettledUnsetteledTable, ds.SettledUnsetteledTable.AMOUNT, buyQuery);
            unsettledSell = GetSumOfColumn(ds.SettledUnsetteledTable, ds.SettledUnsetteledTable.AMOUNT, sellQuery);
            unsettledBuyunits = GetSumOfColumn(ds.SettledUnsetteledTable, ds.SettledUnsetteledTable.UNITS, buyQuery);
            unsettledSellunits = GetSumOfColumn(ds.SettledUnsetteledTable, ds.SettledUnsetteledTable.UNITS, sellQuery);

            unsettledSellByUnits = GetMISSellByOrderPreferedBy(ds, productID, OrderPreferedBy.Units.ToString());
            unsettledSellByAmount = GetMISSellByOrderPreferedBy(ds, productID, OrderPreferedBy.Amount.ToString());
        }

        public static void GetBankUnsettledAmount(SettledUnsetteledDS ds, Guid Accountcid, string AccountNumber, string BSB, out decimal unsettledBuy, out decimal unsettledSell)
        {
            unsettledBuy = 0;
            unsettledSell = 0;
            if (ds.SettledUnsetteledTable.Rows.Count == 0)
                return;

            var buyQuery = String.Format("{0}='{1}' and {2}='{3}'",
                                         ds.SettledUnsetteledTable.ACCOUNTCID, Accountcid,
                                         ds.SettledUnsetteledTable.TRADETYPE, TradeType.Buy);
            var sellQuery = String.Format("{0}='{1}' and {2}='{3}'",
                                          ds.SettledUnsetteledTable.ACCOUNTCID, Accountcid,
                                          ds.SettledUnsetteledTable.TRADETYPE, TradeType.Sell);
            unsettledBuy = GetSumOfColumn(ds.SettledUnsetteledTable, ds.SettledUnsetteledTable.AMOUNT, buyQuery);
            unsettledSell = GetSumOfColumn(ds.SettledUnsetteledTable, ds.SettledUnsetteledTable.AMOUNT, sellQuery);


        }

        public static void GetTDUnsettledAmount(string OrderTDAccountType, SettledUnsetteledDS ds, Guid productid, Guid BrokerID, Guid Accountcid, string AccountNumber, string BSB, out decimal unsettledBuy, out decimal unsettledSell)
        {
            unsettledBuy = 0;
            unsettledSell = 0;
            if (ds.SettledUnsetteledTable.Rows.Count == 0)
                return;


            var buyQuery = String.Format("{0}='{1}' and {2}='{3}'  and {4}='{5}' and {6}='{7}'",
                                         ds.SettledUnsetteledTable.PRODUCTID, productid,
                                         ds.SettledUnsetteledTable.TRADETYPE, TradeType.Buy, ds.SettledUnsetteledTable.BANKID, BrokerID, ds.SettledUnsetteledTable.ACCOUNTTYPE, OrderTDAccountType);

            var sellQuery = String.Format("{0}='{1}' and {2}='{3}' and {4}='{5}' and {6}='{7}'",
                          ds.SettledUnsetteledTable.PRODUCTID, productid,
                          ds.SettledUnsetteledTable.TRADETYPE, TradeType.Sell, ds.SettledUnsetteledTable.BANKID, BrokerID, ds.SettledUnsetteledTable.ACCOUNTTYPE, OrderTDAccountType);

            unsettledBuy = GetSumOfColumn(ds.SettledUnsetteledTable, ds.SettledUnsetteledTable.AMOUNT, buyQuery);
            unsettledSell = GetSumOfColumn(ds.SettledUnsetteledTable, ds.SettledUnsetteledTable.AMOUNT, sellQuery);

        }

        public static void GetTDUnsettledAmountFinSummary(string OrderTDAccountType, SettledUnsetteledDS ds, Guid productid, Guid BrokerID, Guid Accountcid, string AccountNumber, string BSB, out decimal unsettledBuy, out decimal unsettledSell, out decimal validatedunsettledBuy, out decimal validatedunsettledSell)
        {
            unsettledBuy = 0;
            unsettledSell = 0;
            validatedunsettledBuy = 0;
            validatedunsettledSell = 0;
            var buyQuery = String.Format("{0}='{1}' and {2}='{3}'  and {4}='{5}' and {6}='{7}' and {8}='{9}'",
                                           ds.SettledUnsetteledTable.PRODUCTID, productid,
                                           ds.SettledUnsetteledTable.TRADETYPE, TradeType.Buy,
                                           ds.SettledUnsetteledTable.BANKID, BrokerID,
                                           ds.SettledUnsetteledTable.ACCOUNTTYPE, OrderTDAccountType,
                                           ds.SettledUnsetteledTable.TYPE, OrderSetlledUnsettledType.Unsettled);
            var sellQuery = String.Format("{0}='{1}' and {2}='{3}' and {4}='{5}' and {6}='{7}' and {8}='{9}'",
                                            ds.SettledUnsetteledTable.PRODUCTID, productid,
                                            ds.SettledUnsetteledTable.TRADETYPE, TradeType.Sell,
                                            ds.SettledUnsetteledTable.BANKID, BrokerID,
                                            ds.SettledUnsetteledTable.ACCOUNTTYPE, OrderTDAccountType,
                                            ds.SettledUnsetteledTable.TYPE, OrderSetlledUnsettledType.Unsettled);

            var buyQueryValidatedOnly = String.Format("{0}='{1}' and {2}='{3}'  and {4}='{5}' and {6}='{7}' and {8}='{9}'",
                                          ds.SettledUnsetteledTable.PRODUCTID, productid,
                                          ds.SettledUnsetteledTable.TRADETYPE, TradeType.Buy,
                                          ds.SettledUnsetteledTable.BANKID, BrokerID,
                                          ds.SettledUnsetteledTable.ACCOUNTTYPE, OrderTDAccountType,
                                          ds.SettledUnsetteledTable.TYPE, OrderSetlledUnsettledType.Initialized);
            var sellQueryValidatedOnly = String.Format("{0}='{1}' and {2}='{3}' and {4}='{5}' and {6}='{7}' and {8}='{9}'",
                                            ds.SettledUnsetteledTable.PRODUCTID, productid,
                                            ds.SettledUnsetteledTable.TRADETYPE, TradeType.Sell,
                                            ds.SettledUnsetteledTable.BANKID, BrokerID,
                                            ds.SettledUnsetteledTable.ACCOUNTTYPE, OrderTDAccountType,
                                            ds.SettledUnsetteledTable.TYPE, OrderSetlledUnsettledType.Initialized);
            if (ds.SettledUnsetteledTable.Rows.Count == 0)
                return;

            unsettledBuy = GetSumOfColumn(ds.SettledUnsetteledTable, ds.SettledUnsetteledTable.AMOUNT, buyQuery);
            unsettledSell = GetSumOfColumn(ds.SettledUnsetteledTable, ds.SettledUnsetteledTable.AMOUNT, sellQuery);

            validatedunsettledBuy = GetSumOfColumn(ds.SettledUnsetteledTable, ds.SettledUnsetteledTable.AMOUNT, buyQueryValidatedOnly);
            validatedunsettledSell = GetSumOfColumn(ds.SettledUnsetteledTable, ds.SettledUnsetteledTable.AMOUNT, sellQueryValidatedOnly);
        }

        private static decimal GetSumOfColumn(DataTable dt, string ColName, string Query)
        {
            decimal unsettledValue = 0;
            string colToCompute = string.Format("sum({0})", ColName);
            var sum = dt.Compute(colToCompute, Query);
            if (sum != DBNull.Value)
                unsettledValue = (decimal)sum;
            return unsettledValue;
        }


        public static Guid CreateNewUnsettledEntityForSubmiited(string ClientID, List<OrderEntity> orders, ICMBroker broker, UserEntity user)
        {
            var ds = new SettledUnsetteledDS
                         {
                             Unit = new Data.OrganizationUnit
                                        {
                                            Name = ClientID + "_SetteledUnsetteled" + DateTime.Now.ToString("dd-MM-yyyy"),
                                            CurrentUser = user,
                                            Type = ((int)OrganizationType.SettledUnsettled).ToString(),
                                        },
                             CommandType = DatasetCommandTypes.Add,
                             Command = (int)WebCommands.AddNewOrganizationUnit

                         };
            ds.ExtendedProperties.Add("Orders", orders);

            var org = broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            org.SetData(ds);
            broker.ReleaseBrokerManagedComponent(org);
            AddUpdateCMCuid(ClientID, ds.Unit.Cid);
            return ds.Unit.Cid;
        }

        public static Guid CreateNewUnsettledEntity(OrderEntity order, ICMBroker broker, UserEntity user)
        {
            var ds = new SettledUnsetteledDS
                         {
                             Unit = new Data.OrganizationUnit
                                        {
                                            Name = order.ClientID + "_SetteledUnsetteled" + DateTime.Now.ToString("dd-MM-yyyy"),
                                            CurrentUser = user,
                                            Type = ((int)OrganizationType.SettledUnsettled).ToString(),
                                        },
                             CommandType = DatasetCommandTypes.Add,
                             Command = (int)WebCommands.AddNewOrganizationUnit

                         };
            ds.ExtendedProperties.Add("Order", order);

            var org = broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            org.SetData(ds);
            broker.ReleaseBrokerManagedComponent(org);
            AddUpdateCMCuid(order.ClientID, ds.Unit.Cid);
            return ds.Unit.Cid;
        }
    }
}
