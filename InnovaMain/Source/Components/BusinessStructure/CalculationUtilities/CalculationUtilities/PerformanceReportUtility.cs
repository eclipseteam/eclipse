#region Using
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Xml.Linq;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.Group;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CM.Entity;
using Oritax.TaxSimp.Extensions;
using System.Collections.ObjectModel;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.CM.OrganizationUnit;
#endregion

namespace Oritax.TaxSimp.CM
{
    public class PerformanceFlowReportUtility
    {

        public PerformanceFlowReportUtility()
        { }

        public void OnGetDataCapitalFlowDataSetCorporateInd(PerfReportDS data, string clientType, string clientID, string clientName, IdentityCM Parent, ICMBroker Broker, ClientIndividualEntity Entity)
        {
            SetDatasetServiceTypeConfigured(data, Entity.Servicetype);

            DataTable clientSummaryTable = data.Tables[PerfReportDS.CLIENTSUMMARYTABLE];
            DataTable contactTable = data.Tables[PerfReportDS.CONTACTTABLE];

            DataRow clientSummaryRow = clientSummaryTable.NewRow();
            DataRow contactRow = contactTable.NewRow();

            clientSummaryRow[PerfReportDS.CLIENTID] = clientID;
            clientSummaryRow[PerfReportDS.CLIENTNAME] = clientName;
            clientSummaryRow[PerfReportDS.CIENTABN] = Entity.ABN;
            clientSummaryRow[PerfReportDS.CLIENTACN] = "";
            clientSummaryRow[PerfReportDS.CLIENTTYPE] = clientType;

            if (Parent != null && Parent.Clid != Guid.Empty && Parent.Csid != Guid.Empty)
            {
                AdvisorCM adviserUnit = Broker.GetCMImplementation(Parent.Clid, Parent.Csid) as AdvisorCM;
                clientSummaryRow[HoldingRptDataSet.ADVISERFULLNAME] = adviserUnit.AdvisorEntity.FullName;
                Broker.ReleaseBrokerManagedComponent(adviserUnit);
            }

            SetAddress(clientType, Entity.Address, clientSummaryRow);

            CSVInstance csvinstance = new CSVInstance(Broker);
            ClientIndividualEntityCSV _entitycsv = csvinstance.GetEntityConcreteInstance(Entity);
            foreach (var contact in _entitycsv.Applicants)
            {
                contactRow[PerfReportDS.CONTACT_TITLE] = contact.PersonalTitle;
                contactRow[PerfReportDS.CONTACT_NAME] = contact.Fullname;
                contactRow[PerfReportDS.CONTACT_BIZNO] = contact.WorkPhoneNumber;
                contactRow[PerfReportDS.CONTACT_MOBNO] = contact.MobilePhoneNumber;
                contactRow[PerfReportDS.CONTACT_HNO] = contact.HomePhoneNumber;
                contactRow[PerfReportDS.CONTACT_FAXNO] = contact.Facsimile;
                contactRow[PerfReportDS.CONTACT_EMAIL] = contact.EmailAddress;
            }

            clientSummaryTable.Rows.Add(clientSummaryRow);
            contactTable.Rows.Add(contactRow);

            IOrganization orgCM = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            PerfReportDS perfReportDS = data as PerfReportDS;

            DataTable CapitalSummaryTable = perfReportDS.Tables[PerfReportDS.CAPITALFLOWSUMMARY];
            DataTable productBreakDownTable = perfReportDS.Tables[PerfReportDS.PRODUCTBREAKDOWNTABLE];
            DataTable TDBreakDownTable = perfReportDS.Tables[PerfReportDS.TDBREAKDOWNTABLE];

            DataTable CapitalFlowSummaryReport = ExtractCapitalSummaryTableByCategory(clientType, Broker, Entity as IClientUMAData, perfReportDS, clientID, orgCM);

            GetBankAccountDataSet(orgCM, Entity.ListAccountProcess, Broker, data);
            GetASXTransactionDS(orgCM, Entity.ListAccountProcess, Broker, data);
            Broker.ReleaseBrokerManagedComponent(orgCM);
        }

        private static void SetAddress(string clientType, Common.DualAddressEntity address, DataRow clientSummaryRow)
        {
            switch (clientType.ToLower())
            {
                case "corporate smsf trustee":
                case "corporation private":
                case "corporation public":
                case "e-clipse super":
                case "other corporate trustee":
                case "other individual trustee":
                case "individual smsf trustee":
                case "clientsmsfindividualtrustee":
                case "clientindividual":
                case "clientsmsfcorporatetrustee":
                case "trust - individual trustee":
                case "trust - corporate trustee":
                    clientSummaryRow[HoldingRptDataSet.ADDRESSLINE1] = address.BusinessAddress.Addressline1;
                    clientSummaryRow[HoldingRptDataSet.ADDRESSLINE2] = address.BusinessAddress.Addressline2;
                    clientSummaryRow[HoldingRptDataSet.COUNTRY] = address.BusinessAddress.Country;
                    clientSummaryRow[HoldingRptDataSet.SUBURB] = address.BusinessAddress.Suburb;
                    clientSummaryRow[HoldingRptDataSet.POSTCODE] = address.BusinessAddress.PostCode;
                    break;
                case "individual":
                case "client individual":
                case "joint":
                    clientSummaryRow[HoldingRptDataSet.ADDRESSLINE1] = address.ResidentialAddress.Addressline1;
                    clientSummaryRow[HoldingRptDataSet.ADDRESSLINE2] = address.ResidentialAddress.Addressline2;
                    clientSummaryRow[HoldingRptDataSet.COUNTRY] = address.ResidentialAddress.Country;
                    clientSummaryRow[HoldingRptDataSet.SUBURB] = address.ResidentialAddress.Suburb;
                    clientSummaryRow[HoldingRptDataSet.POSTCODE] = address.ResidentialAddress.PostCode;
                    break;

            }
        }

        public void OnGetDataCapitalFlowDataSetCorporate(PerfReportDS data, string clientType, string clientID, string clientName, IdentityCM Parent, ICMBroker Broker, CorporateEntity Entity)
        {
            SetDatasetServiceTypeConfigured(data, Entity.Servicetype);

            DataTable clientSummaryTable = data.Tables[PerfReportDS.CLIENTSUMMARYTABLE];
            DataTable contactTable = data.Tables[PerfReportDS.CONTACTTABLE];

            DataRow clientSummaryRow = clientSummaryTable.NewRow();
            DataRow contactRow = contactTable.NewRow();

            clientSummaryRow[PerfReportDS.CLIENTID] = clientID;
            clientSummaryRow[PerfReportDS.CLIENTNAME] = clientName;
            clientSummaryRow[PerfReportDS.CIENTABN] = Entity.ABN;
            clientSummaryRow[PerfReportDS.CLIENTACN] = Entity.ACN;
            clientSummaryRow[PerfReportDS.CLIENTTYPE] = clientType;

            if (Parent != null && Parent.Clid != Guid.Empty && Parent.Csid != Guid.Empty)
            {
                AdvisorCM adviserUnit = Broker.GetCMImplementation(Parent.Clid, Parent.Csid) as AdvisorCM;
                clientSummaryRow[HoldingRptDataSet.ADVISERFULLNAME] = adviserUnit.AdvisorEntity.FullName;
                Broker.ReleaseBrokerManagedComponent(adviserUnit);
            }

            SetAddress(clientType, Entity.Address, clientSummaryRow);
            CSVInstance csvinstance = new CSVInstance(Broker);
            CorporateCSV _entitycsv = csvinstance.GetEntityConcreteInstance(Entity);
            foreach (var contact in _entitycsv.Contacts)
            {
                contactRow[PerfReportDS.CONTACT_TITLE] = contact.PersonalTitle;
                contactRow[PerfReportDS.CONTACT_NAME] = contact.Fullname;
                contactRow[PerfReportDS.CONTACT_BIZNO] = contact.WorkPhoneNumber;
                contactRow[PerfReportDS.CONTACT_MOBNO] = contact.MobilePhoneNumber;
                contactRow[PerfReportDS.CONTACT_HNO] = contact.HomePhoneNumber;
                contactRow[PerfReportDS.CONTACT_FAXNO] = contact.Facsimile;
                contactRow[PerfReportDS.CONTACT_EMAIL] = contact.EmailAddress;
            }

            clientSummaryTable.Rows.Add(clientSummaryRow);
            contactTable.Rows.Add(contactRow);

            IOrganization orgCM = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            PerfReportDS perfReportDS = data as PerfReportDS;

            DataTable CapitalSummaryTable = perfReportDS.Tables[PerfReportDS.CAPITALFLOWSUMMARY];
            DataTable productBreakDownTable = perfReportDS.Tables[PerfReportDS.PRODUCTBREAKDOWNTABLE];
            DataTable TDBreakDownTable = perfReportDS.Tables[PerfReportDS.TDBREAKDOWNTABLE];

            DataTable CapitalFlowSummaryReport = ExtractCapitalSummaryTableByCategory(clientType, Broker, Entity as IClientUMAData, perfReportDS, clientID, orgCM);

            GetBankAccountDataSet(orgCM, Entity.ListAccountProcess, Broker, data);
            GetASXTransactionDS(orgCM, Entity.ListAccountProcess, Broker, data);
            Broker.ReleaseBrokerManagedComponent(orgCM);
        }

        private static void SetDatasetServiceTypeConfigured(PerfReportDS data, Oritax.TaxSimp.Common.ServiceType ServiceType)
        {
            data.HasDIFM = ServiceType.DO_IT_FOR_ME;
            data.HasDIWM = ServiceType.DO_IT_WITH_ME;
            data.HasDIY = ServiceType.DO_IT_YOURSELF;
        }

        public void GetBankAccountDataSet(IOrganization orgCM, List<AccountProcessTaskEntity> ListAccountProcess, ICMBroker Broker, PerfReportDS data)
        {
            foreach (var accountProcessTaskEntity in ListAccountProcess)
            {
                if (accountProcessTaskEntity.LinkedEntity != null && accountProcessTaskEntity.LinkedEntity.Clid != Guid.Empty)
                {
                    if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.BankAccount)
                    {
                        BankAccountCM bankAccount = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as BankAccountCM;

                        if (bankAccount != null && bankAccount.BankAccountEntity.CashManagementTransactions.Count > 0)
                        {

                            var filteredTransactions = bankAccount.BankAccountEntity.CashManagementTransactions.Where(tran => tran.TransactionDate <= data.EndDate && tran.TransactionDate >= data.StartDate);

                            DataTable dt = data.Tables[PerfReportDS.BANKTRANSACTIONSTABLE];

                            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashManagementEntity in filteredTransactions)
                            {
                                DataRow row = dt.NewRow();

                                row[PerfReportDS.BSB] = bankAccount.BankAccountEntity.BSB;
                                row[PerfReportDS.ACCOUNTTYPE] = bankAccount.BankAccountEntity.AccoutType.ToUpper();
                                row[PerfReportDS.ACCOUNTNO] = bankAccount.BankAccountEntity.AccountNumber;
                                row[PerfReportDS.ACCOUNTNAME] = bankAccount.BankAccountEntity.Name;
                                row[PerfReportDS.AMOUNTTOTAL] = cashManagementEntity.TotalAmount;
                                row[PerfReportDS.BANKAMOUNT] = cashManagementEntity.Amount;
                                row[PerfReportDS.BANKADJUSTMENT] = cashManagementEntity.Adjustment;
                                row[PerfReportDS.BANKTRANSDATE] = cashManagementEntity.TransactionDate;

                                row[PerfReportDS.IMPTRANTYPE] = cashManagementEntity.ImportTransactionType;
                                row[PerfReportDS.SYSTRANTYPE] = cashManagementEntity.SystemTransactionType;
                                row[PerfReportDS.CATEGORY] = cashManagementEntity.Category;
                                row[PerfReportDS.COMMENT] = cashManagementEntity.Comment;

                                dt.Rows.Add(row);
                            }

                            data.Merge(dt, false, MissingSchemaAction.Add);
                        }
                    }
                }
            }
        }

        public void GetASXTransactionDS(IOrganization orgCM, List<AccountProcessTaskEntity> ListAccountProcess, ICMBroker Broker, PerfReportDS data)
        {
            foreach (var accountProcessTaskEntity in ListAccountProcess)
            {
                if (accountProcessTaskEntity.LinkedEntity != null && accountProcessTaskEntity.LinkedEntity.Clid != Guid.Empty)
                {
                    if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.DesktopBrokerAccount)
                    {
                        DesktopBrokerAccountCM desktopBrokerAccountCM = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as DesktopBrokerAccountCM;

                        if (desktopBrokerAccountCM != null && desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions != null && desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Count > 0)
                        {

                            DataTable dt = data.Tables[PerfReportDS.ASXTRANSTABLE];

                            var filteredTransactions = desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Where(tran => tran.TradeDate <= data.EndDate && tran.TradeDate >= data.StartDate);

                            foreach (HoldingTransactions holdingTransactions in desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions)
                            {
                                DataRow row = dt.NewRow();

                                row[PerfReportDS.NAME] = desktopBrokerAccountCM.DesktopBrokerAccountEntity.Name;
                                row[PerfReportDS.ACCOUNTNO] = desktopBrokerAccountCM.DesktopBrokerAccountEntity.AccountNumber;
                                row[PerfReportDS.FPSID] = holdingTransactions.FPSInstructionID;
                                row[PerfReportDS.INVESMENTCODE] = holdingTransactions.InvestmentCode;
                                row[PerfReportDS.TRANSACTIONTYPE] = holdingTransactions.TransactionType;
                                row[PerfReportDS.TRADEDATE] = holdingTransactions.TradeDate;

                                if (holdingTransactions.SettlementDate != null)
                                    row[PerfReportDS.SETTLEDATE] = holdingTransactions.SettlementDate;
                                else
                                    row[PerfReportDS.SETTLEDATE] = holdingTransactions.TradeDate;

                                row[PerfReportDS.UNITS] = holdingTransactions.Units;
                                row[PerfReportDS.GROSSVALUE] = holdingTransactions.GrossValue;
                                row[PerfReportDS.BROKERAGEAMOUNT] = holdingTransactions.BrokerageAmount;
                                row[PerfReportDS.CHARGES] = holdingTransactions.Charges;
                                row[PerfReportDS.BROKERGST] = holdingTransactions.BrokerageGST;
                                row[PerfReportDS.BROKERAGEAMOUNT] = holdingTransactions.BrokerageAmount;
                                row[PerfReportDS.NETVALUE] = holdingTransactions.NetValue;
                                row[PerfReportDS.NARRATIVE] = holdingTransactions.Narrative;

                                dt.Rows.Add(row);
                            }

                            data.Merge(dt, false, MissingSchemaAction.Add);
                        }
                    }
                }
            }
        }

        private DataTable ExtractCapitalSummaryTableByCategory(string clientType, ICMBroker Broker, IClientUMAData Entity, PerfReportDS perfReportDS, string ClientID, IOrganization orgCM)
        {
            DataTable CapitalFlowSummaryReport = perfReportDS.Tables[PerfReportDS.CAPITALFLOWSUMMARYCAT];
            DataTable CapitalFlowSummaryReportBySec = perfReportDS.Tables[PerfReportDS.CAPITALFLOWSUMMARYCATBYSEC];

            ObservableCollection<ModifiedDietz> modifiedDietzCol = new ObservableCollection<ModifiedDietz>();
            ObservableCollection<ModifiedDietz> modifiedDietzDIYCol = new ObservableCollection<ModifiedDietz>();
            ObservableCollection<ModifiedDietz> modifiedDietzDIWMCol = new ObservableCollection<ModifiedDietz>();
            ObservableCollection<ModifiedDietz> modifiedDietzDIFMCol = new ObservableCollection<ModifiedDietz>();
            ObservableCollection<ModifiedDietz> modifiedDietzMANCol = new ObservableCollection<ModifiedDietz>();

            Dictionary<string, ObservableCollection<ModifiedDietz>> securitiesCollection = new Dictionary<string, ObservableCollection<ModifiedDietz>>();

            if (perfReportDS.IsRange)
            {
                CapitalMovementByCategoryTotal capitalMovementByCategoryTotal = new CapitalMovementByCategoryTotal();
                ModifiedDietz modifiedDietz = new CM.ModifiedDietz();
                ModifiedDietz modifiedDietzDIY = new CM.ModifiedDietz();
                ModifiedDietz modifiedDietzDIWM = new CM.ModifiedDietz();
                ModifiedDietz modifiedDietzDIFM = new CM.ModifiedDietz();
                ModifiedDietz modifiedDietzMAN = new CM.ModifiedDietz();
                Dictionary<string, ModifiedDietz> securitiesCollectionMonthly = new Dictionary<string, ModifiedDietz>();

                DateTime startdate = perfReportDS.StartDate;
                DateTime endDate = AccountingFinancialYear.LastDayOfMonthFromDateTime(perfReportDS.EndDate);
                perfReportDS.EndDate = endDate;

                ExtractPerformance(clientType, Entity.DistributionIncomes, Entity.DividendCollection, Entity.DesktopBrokerAccounts, Entity.ListAccountProcess, Entity.ClientManualAssetCollection, Broker, perfReportDS, ClientID, orgCM, CapitalFlowSummaryReport, CapitalFlowSummaryReportBySec, modifiedDietzCol, modifiedDietzDIYCol, modifiedDietzDIWMCol, modifiedDietzDIFMCol, modifiedDietzMANCol, securitiesCollection, capitalMovementByCategoryTotal, modifiedDietz, modifiedDietzDIY, modifiedDietzDIWM, modifiedDietzDIFM, modifiedDietzMAN, securitiesCollectionMonthly, startdate, endDate, Entity.Servicetype);
            }
            else
            {
                DateTimeSpan timeSpan = DateTimeSpan.CompareDates(perfReportDS.StartDate, perfReportDS.EndDate);

                int nMonthDiff_FirstToFirst = perfReportDS.EndDate.Month - perfReportDS.StartDate.Month + ((perfReportDS.EndDate.Year - perfReportDS.StartDate.Year) * 12);
                double dMonthDiff = timeSpan.TotalMonths();

                for (int monthCounter = 1; monthCounter <= dMonthDiff + 1; monthCounter++)
                {
                    CapitalMovementByCategoryTotal capitalMovementByCategoryTotal = new CapitalMovementByCategoryTotal();
                    ModifiedDietz modifiedDietz = new CM.ModifiedDietz();
                    ModifiedDietz modifiedDietzDIY = new CM.ModifiedDietz();
                    ModifiedDietz modifiedDietzDIWM = new CM.ModifiedDietz();
                    ModifiedDietz modifiedDietzDIFM = new CM.ModifiedDietz();
                    ModifiedDietz modifiedDietzMAN = new CM.ModifiedDietz();
                    Dictionary<string, ModifiedDietz> securitiesCollectionMonthly = new Dictionary<string, ModifiedDietz>();

                    DateTime startdate = AccountingFinancialYear.FirstDayOfMonthFromDateTime(perfReportDS.StartDate.AddMonths(monthCounter - 1));
                    DateTime endDate = AccountingFinancialYear.LastDayOfMonthFromDateTime(perfReportDS.StartDate.AddMonths(monthCounter - 1));
                    modifiedDietz.Month = startdate;
                    modifiedDietzDIY.Month = startdate;
                    modifiedDietzDIWM.Month = startdate;
                    modifiedDietzDIFM.Month = startdate;
                    modifiedDietzMAN.Month = startdate;

                    ExtractPerformance(clientType, Entity.DistributionIncomes, Entity.DividendCollection, Entity.DesktopBrokerAccounts, Entity.ListAccountProcess, Entity.ClientManualAssetCollection, Broker, perfReportDS, ClientID, orgCM, CapitalFlowSummaryReport, CapitalFlowSummaryReportBySec, modifiedDietzCol, modifiedDietzDIYCol, modifiedDietzDIWMCol, modifiedDietzDIFMCol, modifiedDietzMANCol, securitiesCollection, capitalMovementByCategoryTotal, modifiedDietz, modifiedDietzDIY, modifiedDietzDIWM, modifiedDietzDIFM, modifiedDietzMAN, securitiesCollectionMonthly, startdate, endDate, Entity.Servicetype);
                }
            }

            SetupPerformanceDataset(Entity.ClientManualAssetCollection, Entity.Servicetype, perfReportDS, modifiedDietzCol, modifiedDietzDIYCol, modifiedDietzDIWMCol, modifiedDietzDIFMCol, modifiedDietzMANCol, securitiesCollection, orgCM);

            return CapitalFlowSummaryReport;
        }

        private void SetupPerformanceDataset(ObservableCollection<ClientManualAssetEntity> ClientManualAssetCollection, Common.ServiceType serviceType, PerfReportDS perfReportDS, ObservableCollection<ModifiedDietz> modifiedDietzCol, ObservableCollection<ModifiedDietz> modifiedDietzDIYCol, ObservableCollection<ModifiedDietz> modifiedDietzDIWMCol, ObservableCollection<ModifiedDietz> modifiedDietzDIFMCol, ObservableCollection<ModifiedDietz> modifiedDietzMANCol, Dictionary<string, ObservableCollection<ModifiedDietz>> securitiesCollection, IOrganization orgCM)
        {
            foreach (var dietzCol in securitiesCollection)
            {
                ModifiedDietz.CalculateChainLinksForModifiedDietzCollection(dietzCol.Value);
                string companyName = string.Empty;
                string desc = dietzCol.Key;

                var selectedSecurity = orgCM.Securities.Where(t => t.AsxCode == dietzCol.Key).FirstOrDefault();

                if (selectedSecurity != null)
                {
                    companyName = selectedSecurity.CompanyName;
                    desc = companyName + " (" + dietzCol.Key + ")";
                }

                SetModifiedDietzData(perfReportDS.Tables[PerfReportDS.OVERALLPERFTABLEBYSEC], dietzCol.Value, dietzCol.Key, companyName);
                SetPerformanceSummaryDataBySec(perfReportDS.IsRange, perfReportDS.Tables[PerfReportDS.PERFSUMMARYTABLEBYSEC], dietzCol.Value, desc, perfReportDS.EndDate, perfReportDS.StartDate);

                ModifiedDietz oneYearPerfCubeBySec = new ModifiedDietz();
                ModifiedDietz twoYearPerfCubeBySec = new ModifiedDietz();
                ModifiedDietz threeYearPerfCubeBySec = new ModifiedDietz();
                ModifiedDietz fourYearPerfCubeBySec = new ModifiedDietz();
                ModifiedDietz fiveYearPerfCubeBySec = new ModifiedDietz();

                SetYearlyIncomeGrowthReturnsBysec(perfReportDS, dietzCol.Value, oneYearPerfCubeBySec, twoYearPerfCubeBySec, threeYearPerfCubeBySec, fourYearPerfCubeBySec, fiveYearPerfCubeBySec, dietzCol.Key);
            }

            ModifiedDietz.CalculateChainLinksForModifiedDietzCollection(modifiedDietzCol);
            ModifiedDietz.CalculateChainLinksForModifiedDietzCollection(modifiedDietzDIYCol);
            ModifiedDietz.CalculateChainLinksForModifiedDietzCollection(modifiedDietzDIWMCol);
            ModifiedDietz.CalculateChainLinksForModifiedDietzCollection(modifiedDietzDIFMCol);
            ModifiedDietz.CalculateChainLinksForModifiedDietzCollection(modifiedDietzMANCol);

            SetModifiedDietzData(perfReportDS.Tables[PerfReportDS.OVERALLPERFTABLE], modifiedDietzCol, "OVERALL", string.Empty);
            if (perfReportDS.HasDIY)
                SetModifiedDietzData(perfReportDS.Tables[PerfReportDS.DIYPERFTABLE], modifiedDietzDIYCol, Enumeration.GetDescription(ServiceTypes.DoItYourSelf), string.Empty);
            if (perfReportDS.HasDIWM)
                SetModifiedDietzData(perfReportDS.Tables[PerfReportDS.DIWMPERFTABLE], modifiedDietzDIWMCol, Enumeration.GetDescription(ServiceTypes.DoItWithMe), string.Empty);
            if (perfReportDS.HasDIFM)
                SetModifiedDietzData(perfReportDS.Tables[PerfReportDS.DIFMPERFTABLE], modifiedDietzDIFMCol, Enumeration.GetDescription(ServiceTypes.DoItForMe), string.Empty);
            SetModifiedDietzData(perfReportDS.Tables[PerfReportDS.MANPERFTABLE], modifiedDietzMANCol, Enumeration.GetDescription(ServiceTypes.ManualAsset), string.Empty);

            SetPerformanceSummaryData(perfReportDS.Tables[PerfReportDS.PERFSUMMARYTABLE], modifiedDietzCol, "Overall", perfReportDS.EndDate);
            if (serviceType.DO_IT_YOURSELF)
                SetPerformanceSummaryData(perfReportDS.Tables[PerfReportDS.PERFSUMMARYTABLE], modifiedDietzDIYCol, "DO IT YOURSELF", perfReportDS.EndDate);
            if (serviceType.DO_IT_WITH_ME)
                SetPerformanceSummaryData(perfReportDS.Tables[PerfReportDS.PERFSUMMARYTABLE], modifiedDietzDIWMCol, "DO IT WITH ME", perfReportDS.EndDate);
            if (serviceType.DO_IT_FOR_ME)
                SetPerformanceSummaryData(perfReportDS.Tables[PerfReportDS.PERFSUMMARYTABLE], modifiedDietzDIFMCol, "DO IT FOR ME", perfReportDS.EndDate);
            if (ClientManualAssetCollection != null && ClientManualAssetCollection.Count > 0)
                SetPerformanceSummaryData(perfReportDS.Tables[PerfReportDS.PERFSUMMARYTABLE], modifiedDietzMANCol, "MANUAL ASSETS", perfReportDS.EndDate);


            ModifiedDietz oneYearPerfCube = new ModifiedDietz();
            ModifiedDietz twoYearPerfCube = new ModifiedDietz();
            ModifiedDietz threeYearPerfCube = new ModifiedDietz();
            ModifiedDietz fourYearPerfCube = new ModifiedDietz();
            ModifiedDietz fiveYearPerfCube = new ModifiedDietz();

            SetYearlyIncomeGrowthReturns(perfReportDS, modifiedDietzCol, oneYearPerfCube, twoYearPerfCube, threeYearPerfCube, fourYearPerfCube, fiveYearPerfCube, "OVERALL");
        }

        private static void ExtractPerformance(string clientType, ObservableCollection<DistributionIncomeEntity> DistributionIncomes, ObservableCollection<DividendEntity> DividendCollection, List<Common.IdentityCMDetail> DesktopBrokerAccounts, List<AccountProcessTaskEntity> ListAccountProcess, ObservableCollection<ClientManualAssetEntity> ClientManualAssetCollection, ICMBroker Broker, PerfReportDS perfReportDS, string ClientID, IOrganization orgCM, DataTable CapitalFlowSummaryReport, DataTable CapitalFlowSummaryReportBySec, ObservableCollection<ModifiedDietz> modifiedDietzCol, ObservableCollection<ModifiedDietz> modifiedDietzDIYCol, ObservableCollection<ModifiedDietz> modifiedDietzDIWMCol, ObservableCollection<ModifiedDietz> modifiedDietzDIFMCol, ObservableCollection<ModifiedDietz> modifiedDietzMANCol, Dictionary<string, ObservableCollection<ModifiedDietz>> securitiesCollection, CapitalMovementByCategoryTotal capitalMovementByCategoryTotal, ModifiedDietz modifiedDietz, ModifiedDietz modifiedDietzDIY, ModifiedDietz modifiedDietzDIWM, ModifiedDietz modifiedDietzDIFM, ModifiedDietz modifiedDietzMAN, Dictionary<string, ModifiedDietz> securitiesCollectionMonthly, DateTime startdate, DateTime endDate, Common.ServiceType serviceType)
        {
            ManualTransactions(ClientID, ClientManualAssetCollection, orgCM, perfReportDS, capitalMovementByCategoryTotal, startdate, endDate, modifiedDietz, modifiedDietzMAN, securitiesCollectionMonthly);
            GetTransactionsInfoFromLinkProcess(clientType, ClientID, Broker, ListAccountProcess, orgCM, perfReportDS, capitalMovementByCategoryTotal, startdate, endDate, modifiedDietz, modifiedDietzDIY, modifiedDietzDIWM, modifiedDietzDIFM, securitiesCollectionMonthly, serviceType);

            foreach (Oritax.TaxSimp.Common.IdentityCMDetail desktopBrokerAccounts in DesktopBrokerAccounts)
            {
                DesktopBrokerAccountCM desktopBrokerAccountCM = Broker.GetCMImplementation(desktopBrokerAccounts.Clid,
                                                                    desktopBrokerAccounts.Csid) as DesktopBrokerAccountCM;
                if (desktopBrokerAccountCM != null)
                {
                    var filteredTransactionsByMonth = desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Where(tran => tran.TradeDate <= endDate && tran.TradeDate >= startdate);

                    decimal totalOpeiningBalance = 0;
                    decimal totalClosingBalance = 0;
                    CalculateDesktopBrokerWithoutProductSecurity(orgCM, startdate, endDate, desktopBrokerAccountCM, ref totalOpeiningBalance, ref totalClosingBalance, securitiesCollectionMonthly);

                    var accountTask = ListAccountProcess.Where(listacc => listacc.LinkedEntityType == OrganizationType.DesktopBrokerAccount && listacc.LinkedEntity.Clid == desktopBrokerAccountCM.CLID && listacc.LinkedEntity.Csid == desktopBrokerAccountCM.CSID).FirstOrDefault();

                    if (accountTask != null)
                    {
                        var model = orgCM.Model.Where(mod => mod.ID == accountTask.ModelID).FirstOrDefault();
                        if (model.ServiceType == ServiceTypes.DoItForMe && serviceType.DO_IT_FOR_ME)
                        {
                            SetOpeningBalanceForModifiedDietz(modifiedDietzDIFM.CapitalMovementByCategoryTotal, modifiedDietzDIFM, totalOpeiningBalance, totalClosingBalance);
                            SetOpeningBalanceForModifiedDietz(capitalMovementByCategoryTotal, modifiedDietz, totalOpeiningBalance, totalClosingBalance);
                        }
                        if (model.ServiceType == ServiceTypes.DoItYourSelf && serviceType.DO_IT_YOURSELF)
                        {
                            SetOpeningBalanceForModifiedDietz(modifiedDietzDIY.CapitalMovementByCategoryTotal, modifiedDietzDIY, totalOpeiningBalance, totalClosingBalance);
                            SetOpeningBalanceForModifiedDietz(capitalMovementByCategoryTotal, modifiedDietz, totalOpeiningBalance, totalClosingBalance);
                        }

                        if (model.ServiceType == ServiceTypes.DoItWithMe && serviceType.DO_IT_WITH_ME)
                        {
                            SetOpeningBalanceForModifiedDietz(modifiedDietzDIWM.CapitalMovementByCategoryTotal, modifiedDietzDIWM, totalOpeiningBalance, totalClosingBalance);

                            SetOpeningBalanceForModifiedDietz(capitalMovementByCategoryTotal, modifiedDietz, totalOpeiningBalance, totalClosingBalance);
                        }
                    }
                }
            }

            SetUpPerformanceTables(clientType, DistributionIncomes, DividendCollection, Broker, DesktopBrokerAccounts, orgCM, CapitalFlowSummaryReportBySec, CapitalFlowSummaryReport, modifiedDietzCol, modifiedDietzDIYCol, modifiedDietzDIWMCol, modifiedDietzDIFMCol, modifiedDietzMANCol, capitalMovementByCategoryTotal, modifiedDietz, modifiedDietzDIY, modifiedDietzDIWM, modifiedDietzDIFM, modifiedDietzMAN, startdate, endDate, securitiesCollectionMonthly, securitiesCollection, perfReportDS);
        }

        private static void SetOpeningBalanceForModifiedDietz(CapitalMovementByCategoryTotal capitalMovementByCategoryTotal, ModifiedDietz modifiedDietz, decimal totalOpeiningBalance, decimal totalClosingBalance)
        {
            capitalMovementByCategoryTotal.OpeningBalTotal += totalOpeiningBalance;
            capitalMovementByCategoryTotal.ClosingBalanceTotal += totalClosingBalance;
            modifiedDietz.OpeningBalTotal += totalOpeiningBalance;
            modifiedDietz.ClosingBalanceTotal += totalClosingBalance;
        }

        private static void SetYearlyIncomeGrowthReturns(PerfReportDS perfReportDS, ObservableCollection<ModifiedDietz> modifiedDietzCol, ModifiedDietz oneYearPerfCube, ModifiedDietz twoYearPerfCube, ModifiedDietz threeYearPerfCube, ModifiedDietz fourYearPerfCube, ModifiedDietz fiveYearPerfCube, string code)
        {
            string desc = " Year";

            DateTime endDateGI = perfReportDS.EndDate.AddYears(-1);
            DateTime startDateGI = perfReportDS.EndDate;
            SetYearIncomeGrowthReturnObject(modifiedDietzCol, oneYearPerfCube, startDateGI, endDateGI, "1" + desc, perfReportDS, code);

            endDateGI = perfReportDS.EndDate.AddYears(-2);
            startDateGI = endDateGI.AddYears(1);
            SetYearIncomeGrowthReturnObject(modifiedDietzCol, twoYearPerfCube, startDateGI, endDateGI, "2" + desc, perfReportDS, code);

            endDateGI = perfReportDS.EndDate.AddYears(-3);
            startDateGI = endDateGI.AddYears(1);
            SetYearIncomeGrowthReturnObject(modifiedDietzCol, threeYearPerfCube, startDateGI, endDateGI, "3" + desc, perfReportDS, code);

            endDateGI = perfReportDS.EndDate.AddYears(-4);
            startDateGI = endDateGI.AddYears(1);
            SetYearIncomeGrowthReturnObject(modifiedDietzCol, fourYearPerfCube, startDateGI, endDateGI, "4" + desc, perfReportDS, code);

            endDateGI = perfReportDS.EndDate.AddYears(-5);
            startDateGI = endDateGI.AddYears(1);
            SetYearIncomeGrowthReturnObject(modifiedDietzCol, fiveYearPerfCube, startDateGI, endDateGI, "5" + desc, perfReportDS, code);
        }

        private static void SetYearlyIncomeGrowthReturnsBysec(PerfReportDS perfReportDS, ObservableCollection<ModifiedDietz> modifiedDietzCol, ModifiedDietz oneYearPerfCube, ModifiedDietz twoYearPerfCube, ModifiedDietz threeYearPerfCube, ModifiedDietz fourYearPerfCube, ModifiedDietz fiveYearPerfCube, string code)
        {
            string desc = " Year";

            DateTime endDateGI = perfReportDS.EndDate.AddYears(-1);
            DateTime startDateGI = perfReportDS.EndDate;
            SetYearIncomeGrowthReturnObjectBySec(modifiedDietzCol, oneYearPerfCube, startDateGI, endDateGI, "1" + desc, perfReportDS, code);

            endDateGI = perfReportDS.EndDate.AddYears(-2);
            startDateGI = endDateGI.AddYears(1);
            SetYearIncomeGrowthReturnObjectBySec(modifiedDietzCol, twoYearPerfCube, startDateGI, endDateGI, "2" + desc, perfReportDS, code);

            endDateGI = perfReportDS.EndDate.AddYears(-3);
            startDateGI = endDateGI.AddYears(1);
            SetYearIncomeGrowthReturnObjectBySec(modifiedDietzCol, threeYearPerfCube, startDateGI, endDateGI, "3" + desc, perfReportDS, code);

            endDateGI = perfReportDS.EndDate.AddYears(-4);
            startDateGI = endDateGI.AddYears(1);
            SetYearIncomeGrowthReturnObjectBySec(modifiedDietzCol, fourYearPerfCube, startDateGI, endDateGI, "4" + desc, perfReportDS, code);

            endDateGI = perfReportDS.EndDate.AddYears(-5);
            startDateGI = endDateGI.AddYears(1);
            SetYearIncomeGrowthReturnObjectBySec(modifiedDietzCol, fiveYearPerfCube, startDateGI, endDateGI, "5" + desc, perfReportDS, code);
        }

        private static void SetYearIncomeGrowthReturnObject(ObservableCollection<ModifiedDietz> modifiedDietzCol, ModifiedDietz yearPerfCube, DateTime startDateGI, DateTime endDateGI, string desc, PerfReportDS perfReportDS, string code)
        {
            var modifiedDietzObjectCB = modifiedDietzCol.OrderByDescending(modcol => modcol.Month).Where(modcol => modcol.Month >= endDateGI & modcol.Month <= startDateGI).OrderByDescending(modcol => modcol.Month).FirstOrDefault();

            if (modifiedDietzObjectCB != null)
                yearPerfCube.ClosingBalanceTotal = modifiedDietzObjectCB.ClosingBalanceTotal;

            var collectiontest = modifiedDietzCol.OrderByDescending(modcol => modcol.Month).Where(modcol => modcol.Month >= endDateGI & modcol.Month <= startDateGI);

            //var filteredModDietz = modifiedDietzCol.OrderByDescending(modcol => modcol.Month).Where(modcol => modcol.Month.Year >= endDateGI.Year & modcol.Month.Year <= startDateGI.Year).Where(modcol => modcol.Month.Month >= endDateGI.Month & modcol.Month.Month <= startDateGI.Month);
            var filteredModDietz = modifiedDietzCol.OrderByDescending(modcol => modcol.Month).Where(modcol => modcol.Month.Date >= endDateGI.Date && modcol.Month.Date <= startDateGI.Date);

            yearPerfCube.Income = filteredModDietz.Sum(modcol => modcol.Income);
            yearPerfCube.Movement = filteredModDietz.Sum(modcol => modcol.Movement);
            yearPerfCube.ChangeInMktValue = filteredModDietz.Sum(modcol => modcol.ChangeInMktValue);

            foreach (var modEntity in filteredModDietz)
            {
                ObservableCollection<ModifiedDietzTransactionEntity> modifiedDietzTransactionEntity = new ObservableCollection<ModifiedDietzTransactionEntity>
                    (yearPerfCube.ModifiedDietzTransactionEntityCol.Union(modEntity.ModifiedDietzTransactionEntityCol).ToList());
                yearPerfCube.ModifiedDietzTransactionEntityCol = modifiedDietzTransactionEntity;
            }

            yearPerfCube.CfTotalYear = filteredModDietz.Sum(modcol => modcol.CfTotal);
            var modifiedDietzObjectOB = filteredModDietz.OrderBy(modcol => modcol.Month).FirstOrDefault();

            if (modifiedDietzObjectOB != null)
                yearPerfCube.OpeningBalTotal = modifiedDietzObjectOB.OpeningBalTotal;

            yearPerfCube.CaculateIncomeGrowthReturnsYear();

            DataRow row = perfReportDS.Tables[PerfReportDS.PERFSUMMARYINCOMEGROWTHTABLE].NewRow();

            row[PerfReportDS.DESCRIPTION] = desc;
            row[PerfReportDS.INVESMENTCODE] = code;
            row[PerfReportDS.STARTDATE] = endDateGI.AddMonths(1);
            row[PerfReportDS.ENDDATE] = startDateGI;
            row[PerfReportDS.OPENINGBAL] = yearPerfCube.OpeningBalTotal;
            row[PerfReportDS.INCOME] = yearPerfCube.Income;
            row[PerfReportDS.MOVEMENTEXCLINCOME] = yearPerfCube.Movement;
            row[PerfReportDS.CHANGEININVESTMENTVALUE] = yearPerfCube.ChangeInMktValue;
            row[PerfReportDS.CLOSINGBALANCE] = yearPerfCube.ClosingBalanceTotal;
            row[PerfReportDS.SUMCF] = yearPerfCube.CfTotalYear;
            row[PerfReportDS.INCOMERETURN] = yearPerfCube.IncomeReturn;
            row[PerfReportDS.GROWTHRETURN] = yearPerfCube.ChangeInMktValueReturn;
            row[PerfReportDS.OVERALLRETURN] = yearPerfCube.OverallReturn;
            perfReportDS.Tables[PerfReportDS.PERFSUMMARYINCOMEGROWTHTABLE].Rows.Add(row);
        }

        private static void SetYearIncomeGrowthReturnObjectBySec(ObservableCollection<ModifiedDietz> modifiedDietzCol, ModifiedDietz yearPerfCube, DateTime startDateGI, DateTime endDateGI, string desc, PerfReportDS perfReportDS, string code)
        {
            var modifiedDietzObjectCB = modifiedDietzCol.OrderByDescending(modcol => modcol.Month).Where(modcol => modcol.Month >= endDateGI & modcol.Month <= startDateGI).OrderByDescending(modcol => modcol.Month).FirstOrDefault();

            if (modifiedDietzObjectCB != null)
                yearPerfCube.ClosingBalanceTotal = modifiedDietzObjectCB.ClosingBalanceTotal;

            var collectiontest = modifiedDietzCol.OrderByDescending(modcol => modcol.Month).Where(modcol => modcol.Month >= endDateGI & modcol.Month <= startDateGI);

            yearPerfCube.Income = modifiedDietzCol.OrderByDescending(modcol => modcol.Month).Where(modcol => modcol.Month >= endDateGI & modcol.Month <= startDateGI).Sum(modcol => modcol.Income);
            yearPerfCube.Movement = modifiedDietzCol.OrderByDescending(modcol => modcol.Month).Where(modcol => modcol.Month >= endDateGI & modcol.Month <= startDateGI).Sum(modcol => modcol.Movement);
            yearPerfCube.ChangeInMktValue = modifiedDietzCol.OrderByDescending(modcol => modcol.Month).Where(modcol => modcol.Month >= endDateGI & modcol.Month <= startDateGI).Sum(modcol => modcol.ChangeInMktValue);
            yearPerfCube.CfTotalYear = modifiedDietzCol.OrderByDescending(modcol => modcol.Month).Where(modcol => modcol.Month >= endDateGI & modcol.Month <= startDateGI).Sum(modcol => modcol.CfTotal);
            var modifiedDietzObjectOB = modifiedDietzCol.OrderByDescending(modcol => modcol.Month).Where(modcol => modcol.Month >= endDateGI & modcol.Month <= startDateGI).OrderBy(modcol => modcol.Month).FirstOrDefault();

            if (modifiedDietzObjectOB != null)
                yearPerfCube.OpeningBalTotal = modifiedDietzObjectOB.OpeningBalTotal;

            yearPerfCube.CaculateIncomeGrowthReturnsYear();

            DataRow row = perfReportDS.Tables[PerfReportDS.PERFSUMMARYINCOMEGROWTHTABLEBYSEC].NewRow();

            row[PerfReportDS.DESCRIPTION] = desc;
            row[PerfReportDS.INVESMENTCODE] = code;
            row[PerfReportDS.STARTDATE] = endDateGI.AddMonths(1);
            row[PerfReportDS.ENDDATE] = startDateGI;
            row[PerfReportDS.OPENINGBAL] = yearPerfCube.OpeningBalTotal;
            row[PerfReportDS.INCOME] = yearPerfCube.Income;
            row[PerfReportDS.MOVEMENTEXCLINCOME] = yearPerfCube.Movement;
            row[PerfReportDS.CHANGEININVESTMENTVALUE] = yearPerfCube.ChangeInMktValue;
            row[PerfReportDS.CLOSINGBALANCE] = yearPerfCube.ClosingBalanceTotal;
            row[PerfReportDS.SUMCF] = yearPerfCube.CfTotalYear;
            row[PerfReportDS.INCOMERETURN] = yearPerfCube.IncomeReturn;
            row[PerfReportDS.GROWTHRETURN] = yearPerfCube.ChangeInMktValueReturn;
            row[PerfReportDS.OVERALLRETURN] = yearPerfCube.OverallReturn;
            perfReportDS.Tables[PerfReportDS.PERFSUMMARYINCOMEGROWTHTABLEBYSEC].Rows.Add(row);
        }

        private static void SetUpPerformanceTables(string clientType, ObservableCollection<DistributionIncomeEntity> distributionIncomes, ObservableCollection<DividendEntity> dividendCollection, ICMBroker Broker, List<Oritax.TaxSimp.Common.IdentityCMDetail> DesktopBrokerAccounts, IOrganization orgCM, DataTable CapitalFlowSummaryReportBysec, DataTable CapitalFlowSummaryReport, ObservableCollection<ModifiedDietz> modifiedDietzCol, ObservableCollection<ModifiedDietz> modifiedDietzDIYCol, ObservableCollection<ModifiedDietz> modifiedDietzDIWMCol, ObservableCollection<ModifiedDietz> modifiedDietzDIFMCol, ObservableCollection<ModifiedDietz> modifiedDietzMANCol, CapitalMovementByCategoryTotal capitalMovementByCategoryTotal,
            ModifiedDietz modifiedDietz, ModifiedDietz modifiedDietzDIY, ModifiedDietz modifiedDietzDIWM, ModifiedDietz modifiedDietzDIFM, ModifiedDietz modifiedDietzMAN, DateTime startdate, DateTime endDate, Dictionary<string, ModifiedDietz> securitiesCollectionMonthly, Dictionary<string, ObservableCollection<ModifiedDietz>> securitiesCollection, PerfReportDS perfReportDS)
        {
            foreach (Oritax.TaxSimp.Common.IdentityCMDetail desktopBrokerAccounts in DesktopBrokerAccounts)
            {
                DesktopBrokerAccountCM desktopBrokerAccountCM = Broker.GetCMImplementation(desktopBrokerAccounts.Clid,
                                                               desktopBrokerAccounts.Csid) as DesktopBrokerAccountCM;
                if (desktopBrokerAccountCM != null)
                    OverallDesktopBrokerTransactionByGroup(Broker, orgCM, startdate, endDate, securitiesCollectionMonthly, desktopBrokerAccountCM, perfReportDS);
            }

            foreach (var modifiedDietzEntry in securitiesCollectionMonthly)
            {
                double income = 0;
                //Calculate Income Accural 
                if (modifiedDietzEntry.Value.Type == "FUND")
                {
                    income = distributionIncomes.Where(dis => dis.FundCode == modifiedDietzEntry.Key).Where(dis => dis.PaymentDate <= endDate && dis.PaymentDate.Value.Date >= startdate).Sum(dis => dis.NetCashDistribution.HasValue ? dis.NetCashDistribution.Value : 0);
                    //if (clientType.ToLower() == "e-clipse super")
                    //{
                    //    modifiedDietzEntry.Value.IncomeAccrual = Convert.ToDecimal(income);
                    //}
                    //else
                    modifiedDietzEntry.Value.IncomeAccrual = Convert.ToDecimal(income);
                }
                else if (modifiedDietzEntry.Value.Type == "SEC")
                {
                    income = dividendCollection.Where(div => div.InvestmentCode == modifiedDietzEntry.Key).Where(div => div.PaymentDate <= endDate && div.PaymentDate >= startdate).Sum(div => div.PaidDividend);
                    modifiedDietzEntry.Value.IncomeAccrual = Convert.ToDecimal(income);
                }
                modifiedDietzEntry.Value.Key = modifiedDietzEntry.Key;
                modifiedDietzEntry.Value.Month = startdate;
                modifiedDietzEntry.Value.Income = modifiedDietzEntry.Value.CapitalMovementByCategoryTotal.IncomeTotal;
                modifiedDietzEntry.Value.Movement = modifiedDietzEntry.Value.CapitalMovementByCategoryTotal.TransferInOutTotal + modifiedDietzEntry.Value.CapitalMovementByCategoryTotal.ApplicationRedTotal + modifiedDietzEntry.Value.CapitalMovementByCategoryTotal.ExpenseTotal + modifiedDietzEntry.Value.CapitalMovementByCategoryTotal.InternalCashMovementTotal;
                modifiedDietzEntry.Value.ChangeInMktValue = modifiedDietzEntry.Value.CapitalMovementByCategoryTotal.ChangeInInvestTotal;
                modifiedDietzEntry.Value.CalculatedDietz();
                modifiedDietzEntry.Value.CaculateIncomeGrowthReturns();

                ObservableCollection<ModifiedDietz> secCol = new ObservableCollection<ModifiedDietz>();

                if (securitiesCollection.ContainsKey(modifiedDietzEntry.Key))
                    secCol = securitiesCollection[modifiedDietzEntry.Key];
                else
                    securitiesCollection.Add(modifiedDietzEntry.Key, secCol);

                secCol.Add(modifiedDietzEntry.Value);

                DataRow rowBySec = CapitalFlowSummaryReportBysec.NewRow();

                var selectedSecurity = orgCM.Securities.Where(t => t.AsxCode == modifiedDietzEntry.Key).FirstOrDefault();

                if (selectedSecurity != null)
                    rowBySec[PerfReportDS.DESCRIPTION] = selectedSecurity.CompanyName + " (" + selectedSecurity.AsxCode + ")";
                else
                    rowBySec[PerfReportDS.DESCRIPTION] = modifiedDietzEntry.Key;

                rowBySec[PerfReportDS.MONTH] = startdate;
                rowBySec[PerfReportDS.OPENINGBAL] = modifiedDietzEntry.Value.CapitalMovementByCategoryTotal.OpeningBalTotal;
                rowBySec[PerfReportDS.TRANSFEROUT] = modifiedDietzEntry.Value.CapitalMovementByCategoryTotal.TransferInOutTotal;
                rowBySec[PerfReportDS.INCOME] = modifiedDietzEntry.Value.CapitalMovementByCategoryTotal.IncomeTotal;
                rowBySec[PerfReportDS.APPLICATIONREDEMPTION] = modifiedDietzEntry.Value.CapitalMovementByCategoryTotal.ApplicationRedTotal;
                rowBySec[PerfReportDS.TAXINOUT] = modifiedDietzEntry.Value.CapitalMovementByCategoryTotal.TaxInOutTotal;
                rowBySec[PerfReportDS.EXPENSE] = modifiedDietzEntry.Value.CapitalMovementByCategoryTotal.ExpenseTotal;
                rowBySec[PerfReportDS.INTERNALCASHMOVEMENT] = modifiedDietzEntry.Value.CapitalMovementByCategoryTotal.InternalCashMovementTotal;
                rowBySec[PerfReportDS.CLOSINGBALANCE] = modifiedDietzEntry.Value.CapitalMovementByCategoryTotal.ClosingBalanceTotal;
                rowBySec[PerfReportDS.CHANGEININVESTMENTVALUE] = modifiedDietzEntry.Value.CapitalMovementByCategoryTotal.ChangeInInvestTotal;
                rowBySec[PerfReportDS.MODDIETZ] = modifiedDietzEntry.Value.ModDietz;
                rowBySec[PerfReportDS.SUMCF] = modifiedDietzEntry.Value.CfTotal;
                rowBySec[PerfReportDS.SUMWEIGHTCF] = modifiedDietzEntry.Value.WeightedCfTotal;
                rowBySec[PerfReportDS.GROWTHRETURN] = modifiedDietzEntry.Value.ChangeInMktValueReturn;
                rowBySec[PerfReportDS.INCOMERETURN] = modifiedDietzEntry.Value.IncomeReturn;
                rowBySec[PerfReportDS.OVERALLRETURN] = modifiedDietzEntry.Value.OverallReturn;
                CapitalFlowSummaryReportBysec.Rows.Add(rowBySec);
            }

            modifiedDietz.CapitalMovementByCategoryTotal = capitalMovementByCategoryTotal;
            modifiedDietz.ChangeInMktValue = modifiedDietz.CapitalMovementByCategoryTotal.ChangeInInvestTotal;

            SetIncomeGrowth(capitalMovementByCategoryTotal, modifiedDietz);
            SetIncomeGrowth(modifiedDietzDIY.CapitalMovementByCategoryTotal, modifiedDietzDIY);
            SetIncomeGrowth(modifiedDietzDIWM.CapitalMovementByCategoryTotal, modifiedDietzDIWM);
            SetIncomeGrowth(modifiedDietzDIFM.CapitalMovementByCategoryTotal, modifiedDietzDIFM);
            SetIncomeGrowth(modifiedDietzMAN.CapitalMovementByCategoryTotal, modifiedDietzMAN);

            modifiedDietz.CalculatedDietz();
            modifiedDietzDIY.CalculatedDietz();
            modifiedDietzDIWM.CalculatedDietz();
            modifiedDietzDIFM.CalculatedDietz();
            modifiedDietzMAN.CalculatedDietz();

            modifiedDietz.Month = startdate;
            modifiedDietzDIY.Month = startdate;
            modifiedDietzDIWM.Month = startdate;
            modifiedDietzDIFM.Month = startdate;
            modifiedDietzMAN.Month = startdate;


            DataRow row = CapitalFlowSummaryReport.NewRow();

            row[PerfReportDS.MONTH] = startdate;
            row[PerfReportDS.OPENINGBAL] = capitalMovementByCategoryTotal.OpeningBalTotal;
            row[PerfReportDS.TRANSFEROUT] = capitalMovementByCategoryTotal.TransferInOutTotal;
            row[PerfReportDS.INCOME] = modifiedDietz.IncomeAccrual + capitalMovementByCategoryTotal.IncomeTotal;
            row[PerfReportDS.APPLICATIONREDEMPTION] = capitalMovementByCategoryTotal.ApplicationRedTotal;
            row[PerfReportDS.TAXINOUT] = capitalMovementByCategoryTotal.TaxInOutTotal;
            row[PerfReportDS.EXPENSE] = capitalMovementByCategoryTotal.ExpenseTotal;
            row[PerfReportDS.INTERNALCASHMOVEMENT] = capitalMovementByCategoryTotal.InternalCashMovementTotal;
            row[PerfReportDS.CLOSINGBALANCE] = capitalMovementByCategoryTotal.ClosingBalanceTotal;
            row[PerfReportDS.CHANGEININVESTMENTVALUE] = capitalMovementByCategoryTotal.ChangeInInvestTotal - modifiedDietz.IncomeAccrual;
            row[PerfReportDS.MODDIETZ] = modifiedDietz.ModDietz;
            row[PerfReportDS.SUMCF] = modifiedDietz.CfTotal;
            row[PerfReportDS.SUMWEIGHTCF] = modifiedDietz.WeightedCfTotal;
            row[PerfReportDS.GROWTHRETURN] = modifiedDietz.ChangeInMktValueReturn;
            row[PerfReportDS.INCOMERETURN] = modifiedDietz.IncomeReturn;
            row[PerfReportDS.OVERALLRETURN] = modifiedDietz.OverallReturn;
            CapitalFlowSummaryReport.Rows.Add(row);

            modifiedDietzCol.Add(modifiedDietz);
            modifiedDietzDIYCol.Add(modifiedDietzDIY);
            modifiedDietzDIWMCol.Add(modifiedDietzDIWM);
            modifiedDietzDIFMCol.Add(modifiedDietzDIFM);
            modifiedDietzMANCol.Add(modifiedDietzMAN);
        }

        private static void SetIncomeGrowth(CapitalMovementByCategoryTotal capitalMovementByCategoryTotal, ModifiedDietz modifiedDietz)
        {
            modifiedDietz.Income = modifiedDietz.TotalIncome + capitalMovementByCategoryTotal.IncomeTotal;
            modifiedDietz.Movement = capitalMovementByCategoryTotal.MovementExcludingInvestment + capitalMovementByCategoryTotal.ApplicationRedTotal - capitalMovementByCategoryTotal.IncomeTotal;
            modifiedDietz.ChangeInMktValue = capitalMovementByCategoryTotal.ChangeInInvestTotal - modifiedDietz.IncomeAccrual;
            modifiedDietz.CaculateIncomeGrowthReturns();
        }

        private static void OverallDesktopBrokerTransactionByGroup(ICMBroker Broker, IOrganization orgCM, DateTime startdate, DateTime endDate, Dictionary<string, ModifiedDietz> securitiesCollectionMonthly, DesktopBrokerAccountCM desktopBrokerAccountCM, PerfReportDS perfReportDS)
        {
            var filteredTransactionsByMonthGroup = desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Where(tran => tran.TradeDate <= endDate && tran.TradeDate >= startdate).GroupBy(asx => asx.InvestmentCode);

            foreach (var asxGroup in filteredTransactionsByMonthGroup)
            {

                ModifiedDietz secDietz = new ModifiedDietz();
                secDietz.Month = startdate;
                secDietz.Type = "SEC";
                if (securitiesCollectionMonthly.ContainsKey(asxGroup.Key))
                    secDietz = securitiesCollectionMonthly[asxGroup.Key];
                else
                    securitiesCollectionMonthly.Add(asxGroup.Key, secDietz);

                HoldingTransactions asxTranStart = asxGroup.OrderBy(sec => sec.TradeDate).FirstOrDefault();
                if (asxTranStart != null)
                    secDietz.StartDateIfOpeningZero = asxTranStart.TradeDate;
                else
                    secDietz.StartDateIfOpeningZero = perfReportDS.StartDate;

                HoldingTransactions asxTranEnd = asxGroup.OrderByDescending(sec => sec.TradeDate).FirstOrDefault();
                if (asxTranEnd != null)
                    secDietz.EndDateIfClosingZero = asxTranEnd.TradeDate;
                else
                    secDietz.EndDateIfClosingZero = perfReportDS.EndDate;

                foreach (HoldingTransactions holdingTransactions in asxGroup)
                {
                    if (holdingTransactions.Type == "RA" || holdingTransactions.Type == "VA")
                    {
                        secDietz.AddModDietzEntity(holdingTransactions.NetValue * -1, holdingTransactions.TradeDate, "SEC", perfReportDS);
                    }
                    else if (holdingTransactions.Type == "AN" || holdingTransactions.Type == "VB")
                    {
                        secDietz.AddModDietzEntity(holdingTransactions.NetValue, holdingTransactions.TradeDate, "SEC", perfReportDS);
                    }
                    if (holdingTransactions.Type == "RJ" || holdingTransactions.Type == "AJ")
                    {
                        decimal unitprice = 0;

                        var unitPriceObj = orgCM.Securities.Where(sec => sec.AsxCode == holdingTransactions.InvestmentCode).FirstOrDefault().ASXSecurity.Where(his => his.Date <= holdingTransactions.TradeDate).OrderByDescending(his => his.Date).FirstOrDefault();

                        if (unitPriceObj != null)
                            unitprice = Convert.ToDecimal(unitPriceObj.UnitPrice);

                        holdingTransactions.NetValue = holdingTransactions.Units * unitprice;

                        if (holdingTransactions.Type == "AJ")
                        {
                            if (holdingTransactions.NetValue > 0)
                                holdingTransactions.NetValue = holdingTransactions.NetValue * -1;
                        }

                        secDietz.AddModDietzEntity(holdingTransactions.NetValue, holdingTransactions.TradeDate, "SEC", perfReportDS);
                    }

                    if (holdingTransactions.Type == "AT" || holdingTransactions.Type == "RT")
                    {
                        decimal unitprice = 0;

                        var unitPriceObj = orgCM.Securities.Where(sec => sec.AsxCode == holdingTransactions.InvestmentCode).FirstOrDefault().ASXSecurity.Where(his => his.Date <= holdingTransactions.TradeDate).OrderByDescending(his => his.Date).FirstOrDefault();

                        if (unitPriceObj != null)
                            unitprice = Convert.ToDecimal(unitPriceObj.UnitPrice);

                        holdingTransactions.NetValue = holdingTransactions.Units * unitprice;

                        if (holdingTransactions.Type == "RT")
                        {
                            if (holdingTransactions.NetValue > 0)
                                holdingTransactions.NetValue = holdingTransactions.NetValue * -1;
                        }

                        secDietz.AddModDietzEntity(holdingTransactions.NetValue, holdingTransactions.TradeDate, "SEC", perfReportDS);
                    }
                }

                decimal transferInASX = asxGroup.Where(tran => tran.Type == "AT").Sum(tran => tran.NetValue);
                decimal transferOutASX = asxGroup.Where(tran => tran.Type == "RT").Sum(tran => tran.NetValue);
                decimal adjustmentUp = asxGroup.Where(tran => tran.Type == "RJ").Sum(tran => tran.NetValue);
                decimal adjustmentDown = asxGroup.Where(tran => tran.Type == "AJ").Sum(tran => tran.NetValue);
                decimal sell = asxGroup.Where(tran => tran.Type == "RA" || tran.Type == "VA").Sum(tran => tran.NetValue);
                decimal buyPurchase = asxGroup.Where(tran => tran.Type == "AN" || tran.Type == "VB").Sum(tran => tran.NetValue);

                secDietz.CapitalMovementByCategoryTotal.TransferInOutTotal += transferInASX;
                secDietz.CapitalMovementByCategoryTotal.TransferInOutTotal += transferOutASX;
                secDietz.CapitalMovementByCategoryTotal.TransferInOutTotal += adjustmentUp;
                secDietz.CapitalMovementByCategoryTotal.TransferInOutTotal += adjustmentDown;
                secDietz.CapitalMovementByCategoryTotal.Redemption += (sell * -1);
                secDietz.CapitalMovementByCategoryTotal.Application += buyPurchase;
            }
        }
        private static void OverallTransactionsForDesktopBroker(ICMBroker Broker, IOrganization orgCM, CapitalMovementByCategoryTotal capitalMovementByCategoryTotal, ModifiedDietz modifiedDietz, DateTime startdate, DateTime endDate, DesktopBrokerAccountCM desktopBrokerAccountCM, PerfReportDS perfReportDS)
        {
            var filteredTransactionsByMonth = desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Where(tran => tran.TradeDate <= endDate && tran.TradeDate >= startdate);

            DateTime startTransactionDate = startdate;
            DateTime endTransactionDate = endDate;

            var startTransactionVar = filteredTransactionsByMonth.OrderBy(tran => tran.TradeDate).FirstOrDefault();
            var endTransactionVar = filteredTransactionsByMonth.OrderByDescending(tran => tran.TradeDate).FirstOrDefault();

            if (startTransactionVar != null)
                startTransactionDate = startTransactionVar.TradeDate;

            if (endTransactionVar != null)
                endTransactionDate = endTransactionVar.TradeDate;

            foreach (HoldingTransactions holdingTransactions in filteredTransactionsByMonth)
            {
                if (holdingTransactions.Type == "RA" || holdingTransactions.Type == "VA")
                {
                    modifiedDietz.AddModDietzEntity(holdingTransactions.NetValue * -1, holdingTransactions.TradeDate, "SEC", perfReportDS, startTransactionDate, endTransactionDate);
                }
                else if (holdingTransactions.Type == "AN" || holdingTransactions.Type == "VB")
                {
                    modifiedDietz.AddModDietzEntity(holdingTransactions.NetValue, holdingTransactions.TradeDate, "SEC", perfReportDS, startTransactionDate, endTransactionDate);
                }
                if (holdingTransactions.Type == "RJ" || holdingTransactions.Type == "AJ")
                {
                    decimal unitprice = 0;

                    var unitPriceObj = orgCM.Securities.Where(sec => sec.AsxCode == holdingTransactions.InvestmentCode).FirstOrDefault().ASXSecurity.Where(his => his.Date <= holdingTransactions.TradeDate).OrderByDescending(his => his.Date).FirstOrDefault();

                    if (unitPriceObj != null)
                        unitprice = Convert.ToDecimal(unitPriceObj.UnitPrice);

                    holdingTransactions.NetValue = holdingTransactions.Units * unitprice;

                    if (holdingTransactions.Type == "AJ")
                    {
                        if (holdingTransactions.NetValue > 0)
                            holdingTransactions.NetValue = holdingTransactions.NetValue * -1;
                    }

                    modifiedDietz.AddModDietzEntity(holdingTransactions.NetValue, holdingTransactions.TradeDate, "SEC", perfReportDS, startTransactionDate, endTransactionDate);
                }

                if (holdingTransactions.Type == "AT" || holdingTransactions.Type == "RT")
                {
                    decimal unitprice = 0;

                    var unitPriceObj = orgCM.Securities.Where(sec => sec.AsxCode == holdingTransactions.InvestmentCode).FirstOrDefault().ASXSecurity.Where(his => his.Date <= holdingTransactions.TradeDate).OrderByDescending(his => his.Date).FirstOrDefault();

                    if (unitPriceObj != null)
                        unitprice = Convert.ToDecimal(unitPriceObj.UnitPrice);

                    holdingTransactions.NetValue = holdingTransactions.Units * unitprice;

                    if (holdingTransactions.Type == "RT")
                    {
                        if (holdingTransactions.NetValue > 0)
                            holdingTransactions.NetValue = holdingTransactions.NetValue * -1;
                    }

                    modifiedDietz.AddModDietzEntity(holdingTransactions.NetValue, holdingTransactions.TradeDate, "SEC", perfReportDS, startTransactionDate, endTransactionDate);
                }
            }

            decimal transferInASX = filteredTransactionsByMonth.Where(tran => tran.Type == "AT").Sum(tran => tran.NetValue);
            decimal transferOutASX = filteredTransactionsByMonth.Where(tran => tran.Type == "RT").Sum(tran => tran.NetValue);
            decimal adjustmentUp = filteredTransactionsByMonth.Where(tran => tran.Type == "RJ").Sum(tran => tran.NetValue);
            decimal adjustmentDown = filteredTransactionsByMonth.Where(tran => tran.Type == "AJ").Sum(tran => tran.NetValue);
            decimal sell = filteredTransactionsByMonth.Where(tran => tran.Type == "RA" || tran.Type == "VA").Sum(tran => tran.NetValue);
            decimal buyPurchase = filteredTransactionsByMonth.Where(tran => tran.Type == "AN" || tran.Type == "VB").Sum(tran => tran.NetValue);

            capitalMovementByCategoryTotal.TransferInOutTotal += transferInASX;
            capitalMovementByCategoryTotal.TransferInOutTotal += transferOutASX;
            capitalMovementByCategoryTotal.TransferInOutTotal += adjustmentUp;
            capitalMovementByCategoryTotal.TransferInOutTotal += adjustmentDown;
            capitalMovementByCategoryTotal.Redemption += (sell * -1);
            capitalMovementByCategoryTotal.Application += buyPurchase;
        }

        private void SetModifiedDietzData(DataTable PerformanceTable, ObservableCollection<ModifiedDietz> modifiedDietzCol, string key, string companyFullName)
        {
            foreach (ModifiedDietz modifiedDietz in modifiedDietzCol)
            {
                string type = modifiedDietz.Type;

                DataRow performRow = PerformanceTable.NewRow();

                if (type == "CASH" || type == "TDs")
                    performRow[PerfReportDS.OTHER] = modifiedDietz.CapitalMovementByCategoryTotal.DividendDistribution + modifiedDietz.IncomeAccrual;
                else if (type == "SEC")
                    performRow[PerfReportDS.OTHER] = modifiedDietz.CapitalMovementByCategoryTotal.ChangeInInvestTotal;
                else if (type == "FUND")
                    performRow[PerfReportDS.OTHER] = modifiedDietz.CapitalMovementByCategoryTotal.ChangeInInvestTotal;
                else
                    performRow[PerfReportDS.OTHER] = 0;

                if (companyFullName != string.Empty)
                    performRow[PerfReportDS.INVESMENTCODE] = companyFullName + " (" + key + ")";
                else
                    performRow[PerfReportDS.INVESMENTCODE] = key;

                performRow[PerfReportDS.MONTH] = modifiedDietz.Month;
                CapitalMovementByCategoryTotal capitalMovementByCategoryTotal = modifiedDietz.CapitalMovementByCategoryTotal;
                performRow[PerfReportDS.OPENINGBAL] = capitalMovementByCategoryTotal.OpeningBalTotal;
                performRow[PerfReportDS.APPLICATION] = capitalMovementByCategoryTotal.Application;
                performRow[PerfReportDS.REDEMPTION] = capitalMovementByCategoryTotal.Redemption;
                performRow[PerfReportDS.INCOME] = capitalMovementByCategoryTotal.IncomeTotal + modifiedDietz.IncomeAccrual;
                performRow[PerfReportDS.MOVEMENTEXCLINCOME] = capitalMovementByCategoryTotal.InternalCashMovementTotal + capitalMovementByCategoryTotal.TaxInOutTotal + capitalMovementByCategoryTotal.ExpenseTotal + capitalMovementByCategoryTotal.TransferInOutTotal;
                performRow[PerfReportDS.EXPENSE] = capitalMovementByCategoryTotal.TaxInOutTotal + capitalMovementByCategoryTotal.ExpenseTotal;
                performRow[PerfReportDS.INTERNALCASHMOVEMENT] = capitalMovementByCategoryTotal.InternalCashMovementTotal;
                performRow[PerfReportDS.TRANSFEROUT] = capitalMovementByCategoryTotal.TransferInOutTotal;
                performRow[PerfReportDS.OTHER] = capitalMovementByCategoryTotal.OtherIncome + capitalMovementByCategoryTotal.IncomeTotal + modifiedDietz.IncomeAccrual;
                performRow[PerfReportDS.CHANGEININVESTMENTVALUE] = capitalMovementByCategoryTotal.ChangeInInvestTotal;
                performRow[PerfReportDS.CLOSINGBALANCE] = capitalMovementByCategoryTotal.ClosingBalanceTotal;
                performRow[PerfReportDS.SUMWEIGHTCF] = modifiedDietz.WeightedCfTotal;
                performRow[PerfReportDS.SUMCF] = modifiedDietz.CfTotal;
                performRow[PerfReportDS.MODDIETZ] = modifiedDietz.ModDietz;
                performRow[PerfReportDS.CHAINLINKDATA] = modifiedDietz.ChainLinkDataIncome;
                performRow[PerfReportDS.INCOMERETURN] = modifiedDietz.IncomeReturn;
                performRow[PerfReportDS.OVERALLGROWTH] = modifiedDietz.IncomeAccrual + modifiedDietz.Income + capitalMovementByCategoryTotal.ChangeInInvestTotal;
                performRow[PerfReportDS.OVERALLRETURN] = modifiedDietz.IncomeReturn + modifiedDietz.ModDietz;

                PerformanceTable.Rows.Add(performRow);
            }
        }

        public static void SetPerformanceSummaryData(DataTable PerformanceSummaryTable, ObservableCollection<ModifiedDietz> modifiedDietzCol, string desc, DateTime endDate)
        {
            string oneMonth = "-";
            string threeMonth = "-";
            string sixMonth = "-";
            string oneYear = "-";
            string twoYear = "-";
            string threeYear = "-";
            string fiveYear = "-";
            DateTime lastDayOfTheMonth = AccountingFinancialYear.LastDayOfMonthFromDateTime(endDate);

            var oneMonthData = modifiedDietzCol.Where(mod => mod.Month <= lastDayOfTheMonth).OrderByDescending(mod => mod.Month).FirstOrDefault();

            if (oneMonthData != null)
            {
                oneMonth = oneMonthData.GetChainLinkValue();


                var threeMonthData = modifiedDietzCol.Where(mod => mod.Month <= lastDayOfTheMonth).Where(mod => mod.Month == oneMonthData.Month.AddMonths(-2)).FirstOrDefault();
                if (threeMonthData != null)
                    threeMonth = threeMonthData.GetChainLinkValue();

                var sixMonthData = modifiedDietzCol.Where(mod => mod.Month <= lastDayOfTheMonth).Where(mod => mod.Month == oneMonthData.Month.AddMonths(-5)).FirstOrDefault();
                if (sixMonthData != null)
                    sixMonth = sixMonthData.GetChainLinkValue();

                var oneYearData = modifiedDietzCol.Where(mod => mod.Month <= lastDayOfTheMonth).Where(mod => mod.Month == oneMonthData.Month.AddMonths(-11)).FirstOrDefault();
                if (oneYearData != null)
                    oneYear = oneYearData.GetChainLinkValue();

                var twoYearData = modifiedDietzCol.Where(mod => mod.Month <= lastDayOfTheMonth).Where(mod => mod.Month == oneMonthData.Month.AddYears(-2)).FirstOrDefault();
                if (twoYearData != null)
                    twoYear = twoYearData.GetChainLinkValue();

                var threeYearData = modifiedDietzCol.Where(mod => mod.Month <= lastDayOfTheMonth).Where(mod => mod.Month == oneMonthData.Month.AddYears(-3)).FirstOrDefault();
                if (threeYearData != null)
                    threeYear = threeYearData.GetChainLinkValue();

                var fiveYearData = modifiedDietzCol.Where(mod => mod.Month <= lastDayOfTheMonth).Where(mod => mod.Month == oneMonthData.Month.AddYears(-5)).FirstOrDefault();
                if (fiveYearData != null)
                    fiveYear = fiveYearData.GetChainLinkValue();
            }

            DataRow performRow = PerformanceSummaryTable.NewRow();
            performRow[PerfReportDS.DESCRIPTION] = desc;
            performRow[PerfReportDS.ONEMONTH] = oneMonth;
            performRow[PerfReportDS.THREEMONTH] = threeMonth;
            performRow[PerfReportDS.SIXMONTH] = sixMonth;
            performRow[PerfReportDS.TWELVEMONTH] = oneYear;
            performRow[PerfReportDS.ONEYEAR] = oneYear;
            performRow[PerfReportDS.TWOYEAR] = twoYear;
            performRow[PerfReportDS.THREEYEAR] = threeYear;
            performRow[PerfReportDS.FIVEYEAR] = fiveYear;
            PerformanceSummaryTable.Rows.Add(performRow);
        }

        private void SetPerformanceSummaryDataBySec(bool isRange, DataTable PerformanceSummaryTableBySec, ObservableCollection<ModifiedDietz> modifiedDietzCol, string desc, DateTime endDate, DateTime startDate)
        {
            string perPeriod = "-";
            string oneMonth = "-";
            string threeMonth = "-";
            string sixMonth = "-";
            string oneYear = "-";
            string twoYear = "-";
            string threeYear = "-";
            string fiveYear = "-";

            string perPeriodDiff = "-";
            string oneMonthDiff = "-";
            string threeMonthDiff = "-";
            string sixMonthDiff = "-";
            string oneYearDiff = "-";
            string twoYearDiff = "-";
            string threeYearDiff = "-";
            string fiveYearDiff = "-";

            string perPeriodIncome = "-";
            string oneMonthIncome = "-";
            string threeMonthIncome = "-";
            string sixMonthIncome = "-";
            string oneYearIncome = "-";
            string twoYearIncome = "-";
            string threeYearIncome = "-";
            string fiveYearIncome = "-";

            decimal openingBal = 0;
            decimal closingBal = 0;
            decimal application = 0;
            decimal redemption = 0;
            decimal changeInInvestmentVal = 0;
            decimal income = 0;
            decimal otherIncome = 0;
            decimal transferInOut = 0;
            decimal internalCashMovement = 0;
            decimal expense = 0;
            decimal movementExcludngInvestment = 0;
            decimal incomeCash = 0;
            decimal distributionDividend = 0;

            DateTime lastDayOfTheMonth = AccountingFinancialYear.LastDayOfMonthFromDateTime(endDate);
            DateTime lastDayOfTheMonthRange = AccountingFinancialYear.LastDayOfMonthFromDateTime(startDate);
            var oneMonthData = modifiedDietzCol.Where(mod => mod.Month <= lastDayOfTheMonth).OrderByDescending(mod => mod.Month).FirstOrDefault();

            if (oneMonthData != null)
            {
                oneMonth = oneMonthData.ChainLinkData.ToString("P");
                oneMonthIncome = oneMonthData.ChainLinkDataIncome.ToString("P");
                oneMonthDiff = (oneMonthData.ChainLinkDataIncome - oneMonthData.ChainLinkData).ToString("P");
                closingBal = oneMonthData.CapitalMovementByCategoryTotal.ClosingBalanceTotal;

                var threeMonthData = modifiedDietzCol.Where(mod => mod.Month <= lastDayOfTheMonth).Where(mod => mod.Month == oneMonthData.Month.AddMonths(-2)).FirstOrDefault();
                if (threeMonthData != null)
                {
                    threeMonthIncome = threeMonthData.GetChainLinkValueIncone();
                    threeMonth = threeMonthData.GetChainLinkValue();
                    threeMonthDiff = threeMonthData.GetChainLinkValueDiff();
                }
                var sixMonthData = modifiedDietzCol.Where(mod => mod.Month <= lastDayOfTheMonth).Where(mod => mod.Month == oneMonthData.Month.AddMonths(-5)).FirstOrDefault();
                if (sixMonthData != null)
                {
                    sixMonthIncome = sixMonthData.GetChainLinkValueIncone();
                    sixMonth = sixMonthData.GetChainLinkValue();
                    sixMonthDiff = sixMonthData.GetChainLinkValueDiff();
                }
                var oneYearData = modifiedDietzCol.Where(mod => mod.Month <= lastDayOfTheMonth).Where(mod => mod.Month == oneMonthData.Month.AddMonths(-11)).FirstOrDefault();
                if (oneYearData != null)
                {
                    oneYearIncome = oneYearData.GetChainLinkValueIncone();
                    oneYear = oneYearData.GetChainLinkValue();
                    oneYearDiff = oneYearData.GetChainLinkValueDiff();
                }
                var twoYearData = modifiedDietzCol.Where(mod => mod.Month <= lastDayOfTheMonth).Where(mod => mod.Month == oneMonthData.Month.AddYears(-2)).FirstOrDefault();
                if (twoYearData != null)
                {
                    twoYearIncome = twoYearData.GetChainLinkValueIncone();
                    twoYear = twoYearData.GetChainLinkValue();
                    twoYearDiff = twoYearData.GetChainLinkValueDiff();
                }
                var threeYearData = modifiedDietzCol.Where(mod => mod.Month <= lastDayOfTheMonth).Where(mod => mod.Month == oneMonthData.Month.AddYears(-3)).FirstOrDefault();
                if (threeYearData != null)
                {
                    threeYearIncome = threeYearData.GetChainLinkValueIncone();
                    threeYear = threeYearData.GetChainLinkValue();
                    threeYearDiff = threeYearData.GetChainLinkValueDiff();
                }

                var modifiedDietzColPerPeriod = modifiedDietzCol;
                if (modifiedDietzColPerPeriod.Count() > 0)
                {
                    DateTime firstModifiedDietzSD = modifiedDietzColPerPeriod.OrderByDescending(mod => mod.Month).FirstOrDefault().Month;
                    DateTime firstModifiedDietzED = modifiedDietzColPerPeriod.OrderBy(mod => mod.Month).FirstOrDefault().Month;

                    var dateSpan = DateTimeSpan.CompareDates(startDate, endDate);
                    int monthCount = dateSpan.Years * 12 + dateSpan.Months;

                    var perPeriodData = modifiedDietzColPerPeriod.Where(mod => mod.Month <= lastDayOfTheMonth).Where(mod => mod.Month == oneMonthData.Month.AddMonths(-monthCount)).FirstOrDefault();

                    if (perPeriodData == null)
                        perPeriodData = modifiedDietzColPerPeriod.Where(mod => mod.Month <= lastDayOfTheMonth).OrderBy(mod => mod.Month).FirstOrDefault();

                    if (perPeriodData != null)
                    {
                        if (!isRange & monthCount > 12)
                        {
                            double ChainLinkData = CalculateAnnualReturn(monthCount, perPeriodData.ChainLinkData);
                            double ChainLinkDataIncome = CalculateAnnualReturn(monthCount, perPeriodData.ChainLinkDataIncome);
                            perPeriod = ChainLinkData.ToString("P");
                            perPeriodIncome = ChainLinkDataIncome.ToString("P");
                            perPeriodDiff = (ChainLinkDataIncome - ChainLinkData).ToString("P");
                        }
                        else
                        {
                            perPeriod = perPeriodData.ChainLinkData.ToString("P");
                            perPeriodIncome = perPeriodData.ChainLinkDataIncome.ToString("P");
                            perPeriodDiff = (perPeriodData.ChainLinkDataIncome - perPeriodData.ChainLinkData).ToString("P");
                        }
                        openingBal = perPeriodData.CapitalMovementByCategoryTotal.OpeningBalTotal;
                    }
                }
                var fiveYearData = modifiedDietzCol.Where(mod => mod.Month <= lastDayOfTheMonth).Where(mod => mod.Month == oneMonthData.Month.AddYears(-5)).FirstOrDefault();
                if (fiveYearData != null)
                {
                    fiveYearIncome = fiveYearData.GetChainLinkValueIncone();
                    fiveYear = fiveYearData.GetChainLinkValue();
                    fiveYearDiff = fiveYearData.GetChainLinkValueDiff();
                }

            }

            distributionDividend = modifiedDietzCol.Where(mod => mod.Month <= endDate && mod.Month >= startDate).Sum(mod => mod.CapitalMovementByCategoryTotal.DividendDistribution);
            transferInOut = modifiedDietzCol.Where(mod => mod.Month <= endDate && mod.Month >= startDate).Sum(mod => mod.CapitalMovementByCategoryTotal.TransferInOutTotal);
            internalCashMovement = modifiedDietzCol.Where(mod => mod.Month <= endDate && mod.Month >= startDate).Sum(mod => mod.CapitalMovementByCategoryTotal.InternalCashMovementTotal);
            expense = modifiedDietzCol.Where(mod => mod.Month <= endDate && mod.Month >= startDate).Sum(mod => mod.CapitalMovementByCategoryTotal.ExpenseTotal);
            movementExcludngInvestment = modifiedDietzCol.Where(mod => mod.Month <= endDate && mod.Month >= startDate).Sum(mod => mod.CapitalMovementByCategoryTotal.MovementExcludingInvestment);
            application = modifiedDietzCol.Where(mod => mod.Month <= endDate && mod.Month >= startDate).Sum(mod => mod.CapitalMovementByCategoryTotal.Application);
            redemption = modifiedDietzCol.Where(mod => mod.Month <= endDate && mod.Month >= startDate).Sum(mod => mod.CapitalMovementByCategoryTotal.Redemption);
            changeInInvestmentVal = modifiedDietzCol.Where(mod => mod.Month <= endDate && mod.Month >= startDate).Sum(mod => mod.CapitalMovementByCategoryTotal.ChangeInInvestTotal);
            income = modifiedDietzCol.Where(mod => mod.Month <= endDate && mod.Month >= startDate).Sum(mod => mod.IncomeAccrual);
            otherIncome = modifiedDietzCol.Where(mod => mod.Month <= endDate && mod.Month >= startDate).Sum(mod => mod.CapitalMovementByCategoryTotal.OtherIncome);
            incomeCash = modifiedDietzCol.Where(mod => mod.Month <= endDate && mod.Month >= startDate).Sum(mod => mod.CapitalMovementByCategoryTotal.IncomeTotal);
            string type = modifiedDietzCol.FirstOrDefault().Type;

            DataRow performRow = PerformanceSummaryTableBySec.NewRow();
            performRow[PerfReportDS.DESCRIPTION] = desc;

            performRow[PerfReportDS.OPENINGBAL] = openingBal;

            if (type == "CASH" || type == "TDs")
                performRow[PerfReportDS.OTHER] = distributionDividend + income;
            else if (type == "SEC")
                performRow[PerfReportDS.OTHER] = changeInInvestmentVal;
            else if (type == "FUND")
                performRow[PerfReportDS.OTHER] = changeInInvestmentVal;
            else
                performRow[PerfReportDS.OTHER] = 0;

            performRow[PerfReportDS.APPLICATION] = application;
            performRow[PerfReportDS.TYPE] = type;
            performRow[PerfReportDS.REDEMPTION] = redemption;
            performRow[PerfReportDS.INCOME] = income;
            performRow[PerfReportDS.CHANGEININVESTMENTVALUE] = changeInInvestmentVal;
            performRow[PerfReportDS.TRANSFEROUT] = transferInOut;
            performRow[PerfReportDS.INTERNALCASHMOVEMENT] = internalCashMovement;
            performRow[PerfReportDS.EXPENSE] = expense;
            performRow[PerfReportDS.OTHER] = otherIncome;
            performRow[PerfReportDS.MOVEMENTEXCINVEST] = movementExcludngInvestment;
            performRow[PerfReportDS.PERPERIOD] = perPeriod;
            performRow[PerfReportDS.PERPERIODINCOME] = perPeriodIncome;
            performRow[PerfReportDS.PERPERIODDIFF] = perPeriodDiff;
            performRow[PerfReportDS.CLOSINGBALANCE] = closingBal;
            performRow[PerfReportDS.ONEMONTH] = oneMonth;
            performRow[PerfReportDS.THREEMONTH] = threeMonth;
            performRow[PerfReportDS.SIXMONTH] = sixMonth;
            performRow[PerfReportDS.TWELVEMONTH] = oneYear;
            performRow[PerfReportDS.ONEYEAR] = oneYear;
            performRow[PerfReportDS.TWOYEAR] = twoYear;
            performRow[PerfReportDS.THREEYEAR] = threeYear;
            performRow[PerfReportDS.FIVEYEAR] = fiveYear;

            performRow[PerfReportDS.ONEMONTHINCOME] = oneMonthIncome;
            performRow[PerfReportDS.THREEMONTHINCOME] = threeMonthIncome;
            performRow[PerfReportDS.SIXMONTHINCOME] = sixMonthIncome;
            performRow[PerfReportDS.TWELVEMONTHINCOME] = oneYearIncome;
            performRow[PerfReportDS.ONEYEARINCOME] = oneYearIncome;
            performRow[PerfReportDS.TWOYEARINCOME] = twoYear;
            performRow[PerfReportDS.THREEYEARINCOME] = threeYearIncome;
            performRow[PerfReportDS.FIVEYEARINCOME] = fiveYearIncome;

            performRow[PerfReportDS.ONEMONTHDIFF] = oneMonthDiff;
            performRow[PerfReportDS.THREEMONTHDIFF] = threeMonthDiff;
            performRow[PerfReportDS.SIXMONTHDIFF] = sixMonthDiff;
            performRow[PerfReportDS.TWELVEMONTHDIFF] = oneYearDiff;
            performRow[PerfReportDS.ONEYEARDIFF] = oneYearDiff;
            performRow[PerfReportDS.TWOYEARDIFF] = twoYearDiff;
            performRow[PerfReportDS.THREEYEARDIFF] = threeYearDiff;
            performRow[PerfReportDS.FIVEYEARDIFF] = fiveYearDiff;

            PerformanceSummaryTableBySec.Rows.Add(performRow);
        }

        private static double CalculateAnnualReturn(int monthCount, decimal chainLinkData)
        {
            double value1ForPOW = Convert.ToDouble(1 + chainLinkData);
            double annualPeriodDividedByMountCount = (monthCount + 1 + 0.0) / 12;
            double value2ForPOW = (1 + 0.0) / annualPeriodDividedByMountCount;
            double result = Math.Pow(value1ForPOW, value2ForPOW);

            return result - 1;
        }

        private static void GetTransactionsInfoFromLinkProcess(string clientType, string clientID, ICMBroker Broker, List<AccountProcessTaskEntity> ListAccountProcess, IOrganization orgCM, PerfReportDS perfReportDS, CapitalMovementByCategoryTotal capitalMovementByCategoryTotal, DateTime startdate, DateTime endDate, ModifiedDietz modifiedDietz, ModifiedDietz modifiedDietzDIY, ModifiedDietz modifiedDietzDIWM, ModifiedDietz modifiedDietzDIFM, Common.ServiceType serviceType)
        {
            GetTransactionsInfoFromLinkProcess(clientType, clientID, Broker, ListAccountProcess, orgCM, perfReportDS, capitalMovementByCategoryTotal, startdate, endDate, modifiedDietz, modifiedDietzDIY, modifiedDietzDIWM, modifiedDietzDIFM, null, serviceType);
        }

        private static void GetTransactionsInfoFromLinkProcess(string clientType, string clientID, ICMBroker Broker, List<AccountProcessTaskEntity> ListAccountProcess, IOrganization orgCM, PerfReportDS perfReportDS, CapitalMovementByCategoryTotal capitalMovementByCategoryTotal, DateTime startDate, DateTime endDate, ModifiedDietz modifiedDietz, ModifiedDietz modifiedDietzDIY, ModifiedDietz modifiedDietzDIWM, ModifiedDietz modifiedDietzDIFM, Dictionary<string, ModifiedDietz> securitiesCollectionMonthly, Common.ServiceType serviceType)
        {
            List<Guid> desktopBrokerCMList = new List<Guid>();

            foreach (var accountProcessTaskEntity in ListAccountProcess)
            {
                if (accountProcessTaskEntity.LinkedEntity != null && accountProcessTaskEntity.LinkedEntity.Clid != Guid.Empty)
                {
                    var asset = orgCM.Assets.Where(var => var.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                    var model = orgCM.Model.Where(mod => mod.ID == accountProcessTaskEntity.ModelID).FirstOrDefault();
                    if (model != null)
                    {
                        var linkedAsset = model.Assets.Where(ast => ast.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                        if (linkedAsset != null)
                        {
                            var product = linkedAsset.Products.Where(pro => pro.ID == accountProcessTaskEntity.TaskID).FirstOrDefault();
                            if (product != null)
                            {
                                if ((model.ServiceType == ServiceTypes.DoItForMe && serviceType.DO_IT_FOR_ME) ||
                                    (model.ServiceType == ServiceTypes.DoItWithMe && serviceType.DO_IT_WITH_ME) ||
                                    (model.ServiceType == ServiceTypes.DoItYourSelf && serviceType.DO_IT_YOURSELF))
                                {

                                    if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.BankAccount)
                                        BankAccountTransctions(clientType, Broker, perfReportDS, accountProcessTaskEntity, asset, model, product, capitalMovementByCategoryTotal, startDate, endDate, modifiedDietz, modifiedDietzDIY, modifiedDietzDIWM, modifiedDietzDIFM, orgCM, securitiesCollectionMonthly);
                                    else if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.DesktopBrokerAccount)
                                    {
                                        DesktopBrokerAccountCM desktopBrokerAccountCM = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid,
                                                                                                                   accountProcessTaskEntity.LinkedEntity.Csid) as DesktopBrokerAccountCM;
                                        if (desktopBrokerAccountCM != null)
                                        {
                                            if (!desktopBrokerCMList.Contains(desktopBrokerAccountCM.CID))
                                            {
                                                if (model.ServiceType == ServiceTypes.DoItWithMe)
                                                {
                                                    OverallTransactionsForDesktopBroker(Broker, orgCM, modifiedDietzDIWM.CapitalMovementByCategoryTotal, modifiedDietzDIWM, startDate, endDate, desktopBrokerAccountCM, perfReportDS);
                                                    OverallTransactionsForDesktopBroker(Broker, orgCM, capitalMovementByCategoryTotal, modifiedDietz, startDate, endDate, desktopBrokerAccountCM, perfReportDS);
                                                }
                                                if (model.ServiceType == ServiceTypes.DoItForMe)
                                                {
                                                    OverallTransactionsForDesktopBroker(Broker, orgCM, modifiedDietzDIFM.CapitalMovementByCategoryTotal, modifiedDietzDIFM, startDate, endDate, desktopBrokerAccountCM, perfReportDS);
                                                    OverallTransactionsForDesktopBroker(Broker, orgCM, capitalMovementByCategoryTotal, modifiedDietz, startDate, endDate, desktopBrokerAccountCM, perfReportDS);
                                                }
                                                if (model.ServiceType == ServiceTypes.DoItYourSelf)
                                                {
                                                    OverallTransactionsForDesktopBroker(Broker, orgCM, modifiedDietzDIY.CapitalMovementByCategoryTotal, modifiedDietzDIY, startDate, endDate, desktopBrokerAccountCM, perfReportDS);
                                                    OverallTransactionsForDesktopBroker(Broker, orgCM, capitalMovementByCategoryTotal, modifiedDietz, startDate, endDate, desktopBrokerAccountCM, perfReportDS);
                                                }
                                            }

                                            desktopBrokerCMList.Add(desktopBrokerAccountCM.CID);
                                        }
                                    }
                                    else if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.TermDepositAccount)
                                        TDTransactions(Broker, orgCM, perfReportDS, accountProcessTaskEntity, asset, model, product, capitalMovementByCategoryTotal, startDate, endDate, modifiedDietz, modifiedDietzDIY, modifiedDietzDIWM, modifiedDietzDIFM, securitiesCollectionMonthly);
                                    else if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.ManagedInvestmentSchemesAccount)
                                        ManageInvestmentSchemes(clientID, Broker, orgCM, perfReportDS, accountProcessTaskEntity, asset, model, product, capitalMovementByCategoryTotal, startDate, endDate, modifiedDietz, modifiedDietzDIY, modifiedDietzDIWM, modifiedDietzDIFM, securitiesCollectionMonthly);
                                }
                            }
                        }
                    }
                }
            }
        }


        private static void ManageInvestmentSchemes(string clientID, ICMBroker Broker, IOrganization orgCM, PerfReportDS perfReportDS, AccountProcessTaskEntity accountProcessTaskEntity, AssetEntity asset, ModelEntity model, ProductEntity product, CapitalMovementByCategoryTotal capitalMovementByCategoryTotal, DateTime startDate, DateTime endDate, ModifiedDietz modifiedDietz, ModifiedDietz modifiedDietzDIY, ModifiedDietz modifiedDietzDIWM, ModifiedDietz modifiedDietzDIFM, Dictionary<string, ModifiedDietz> securitiesCollectionMonthly)
        {
            ManagedInvestmentSchemesAccountCM managedInvestmentSchemesAccountCM = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as ManagedInvestmentSchemesAccountCM;
            if (product != null)
            {
                var linkedMISAccount = managedInvestmentSchemesAccountCM.ManagedInvestmentSchemesAccountEntity.FundAccounts.Where(fa => product.TaskDescription.Contains(fa.Code)).FirstOrDefault();
                ModifiedDietz dietz = new ModifiedDietz();
                dietz.Type = "FUND";
                dietz.Month = startDate;

                if (securitiesCollectionMonthly.ContainsKey(linkedMISAccount.Code))
                    dietz = securitiesCollectionMonthly[linkedMISAccount.Code];
                else
                    securitiesCollectionMonthly.Add(linkedMISAccount.Code, dietz);
                var filteredHoldingTransactions = linkedMISAccount.FundTransactions.Where(cliFundTrans => cliFundTrans.TradeDate >= startDate && cliFundTrans.TradeDate <= endDate && cliFundTrans.ClientID == clientID);

                decimal buyPurchase = filteredHoldingTransactions.Where(tran => tran.TransactionType == "Purchase").Sum(tran => tran.Amount);
                buyPurchase += filteredHoldingTransactions.Where(tran => tran.TransactionType == "Application").Sum(tran => tran.Amount);
                decimal sell = filteredHoldingTransactions.Where(tran => tran.TransactionType == "Redemption").Sum(tran => tran.Amount);

                var filteredHoldingTransactionsByOpeningDate = linkedMISAccount.FundTransactions.Where(cliFundTrans => cliFundTrans.TradeDate <= startDate.AddDays(-1));
                var clientFundTransactionsOpening = filteredHoldingTransactionsByOpeningDate.Where(cliFundTrans => cliFundTrans.ClientID == clientID);
                decimal totalSharesOpening = clientFundTransactionsOpening.Sum(ft => ft.Shares);
                var linkedmissec = orgCM.Securities.Where(missec => missec.AsxCode == linkedMISAccount.Code).FirstOrDefault();

                decimal openingBalance = 0;

                if (linkedmissec != null)
                {
                    var latestMisPriceObject = linkedmissec.ASXSecurity.Where(mishis => mishis.Date <= startDate.AddDays(-1)).OrderByDescending(mishis => mishis.Date).FirstOrDefault();
                    decimal unitPrice = 0;
                    if (latestMisPriceObject != null)
                        unitPrice = Convert.ToDecimal(latestMisPriceObject.UnitPrice);
                    openingBalance = totalSharesOpening * unitPrice;
                }

                var filteredHoldingTransactionsByClosingDate = linkedMISAccount.FundTransactions.Where(cliFundTrans => cliFundTrans.TradeDate <= endDate);
                var clientFundTransactionsCLosing = filteredHoldingTransactionsByClosingDate.Where(cliFundTrans => cliFundTrans.ClientID == clientID);
                decimal totalSharesClosing = clientFundTransactionsCLosing.Sum(ft => ft.Shares);

                decimal closingBalance = 0;

                if (linkedmissec != null)
                {
                    var latestMisPriceObject = linkedmissec.ASXSecurity.Where(mishis => mishis.Date <= endDate).OrderByDescending(mishis => mishis.Date).FirstOrDefault();
                    decimal unitPrice = 0;
                    if (latestMisPriceObject != null)
                        unitPrice = Convert.ToDecimal(latestMisPriceObject.UnitPrice);
                    closingBalance = totalSharesClosing * unitPrice;
                }


                DateTime startDateIfHoldingZero = startDate;
                DateTime endDateIfHoldingZero = endDate;

                var startDatetran = filteredHoldingTransactions.OrderBy(mis => mis.TradeDate).FirstOrDefault();
                var endDatetran = filteredHoldingTransactions.OrderByDescending(mis => mis.TradeDate).FirstOrDefault();

                if (startDatetran != null)
                    startDateIfHoldingZero = startDatetran.TradeDate;


                if (endDatetran != null)
                    endDateIfHoldingZero = endDatetran.TradeDate;

                modifiedDietz.StartDateIfOpeningZero = startDateIfHoldingZero;
                modifiedDietz.EndDateIfClosingZero = endDateIfHoldingZero;

                dietz.StartDateIfOpeningZero = startDateIfHoldingZero;
                dietz.EndDateIfClosingZero = endDateIfHoldingZero;

                SetDietzDataMIS(capitalMovementByCategoryTotal, modifiedDietz, buyPurchase, sell, openingBalance, closingBalance);
                SetDietzDataMIS(dietz.CapitalMovementByCategoryTotal, dietz, buyPurchase, sell, openingBalance, closingBalance);

                if (model.ServiceType == ServiceTypes.DoItForMe)
                {
                    modifiedDietzDIFM.StartDateIfOpeningZero = startDateIfHoldingZero;
                    modifiedDietzDIFM.EndDateIfClosingZero = endDateIfHoldingZero;
                    SetDietzDataMIS(modifiedDietzDIFM.CapitalMovementByCategoryTotal, modifiedDietzDIFM, buyPurchase, sell, openingBalance, closingBalance);

                    foreach (MISFundTransactionEntity misFundTransactionEntity in filteredHoldingTransactions)
                    {
                        switch (misFundTransactionEntity.TransactionType)
                        {
                            case "Adjustment Up":
                            case "Adjustment Down":
                                {
                                    break;
                                }
                            default:
                                {
                                    modifiedDietzDIFM.AddModDietzEntity(misFundTransactionEntity.Amount, misFundTransactionEntity.TradeDate, "MIS", perfReportDS);
                                    break;
                                }
                        }
                    }
                }
                else if (model.ServiceType == ServiceTypes.DoItWithMe)
                {
                    modifiedDietzDIWM.StartDateIfOpeningZero = startDateIfHoldingZero;
                    modifiedDietzDIWM.EndDateIfClosingZero = endDateIfHoldingZero;
                    SetDietzDataMIS(modifiedDietzDIWM.CapitalMovementByCategoryTotal, modifiedDietzDIWM, buyPurchase, sell, openingBalance, closingBalance);

                    foreach (MISFundTransactionEntity misFundTransactionEntity in filteredHoldingTransactions)
                    {
                        switch (misFundTransactionEntity.TransactionType)
                        {
                            case "Adjustment Up":
                            case "Adjustment Down":
                                {
                                    break;
                                }
                            default:
                                {
                                    modifiedDietzDIWM.AddModDietzEntity(misFundTransactionEntity.Amount, misFundTransactionEntity.TradeDate, "MIS", perfReportDS);
                                    break;
                                }
                        }
                    }
                }
                else if (model.ServiceType == ServiceTypes.DoItYourSelf)
                {
                    modifiedDietzDIY.StartDateIfOpeningZero = startDateIfHoldingZero;
                    modifiedDietzDIY.EndDateIfClosingZero = endDateIfHoldingZero;
                    SetDietzDataMIS(modifiedDietzDIY.CapitalMovementByCategoryTotal, modifiedDietzDIY, buyPurchase, sell, openingBalance, closingBalance);

                    foreach (MISFundTransactionEntity misFundTransactionEntity in filteredHoldingTransactions)
                    {
                        switch (misFundTransactionEntity.TransactionType)
                        {
                            case "Adjustment Up":
                            case "Adjustment Down":
                                {
                                    break;
                                }
                            default:
                                {
                                    modifiedDietzDIY.AddModDietzEntity(misFundTransactionEntity.Amount, misFundTransactionEntity.TradeDate, "MIS", perfReportDS);
                                    break;
                                }
                        }
                    }
                }

                foreach (MISFundTransactionEntity misFundTransactionEntity in filteredHoldingTransactions)
                {
                    switch (misFundTransactionEntity.TransactionType)
                    {
                        case "Adjustment Up":
                        case "Adjustment Down":
                            {
                                break;
                            }
                        default:
                            {
                                dietz.AddModDietzEntity(misFundTransactionEntity.Amount, misFundTransactionEntity.TradeDate, "MIS", perfReportDS);
                                modifiedDietz.AddModDietzEntity(misFundTransactionEntity.Amount, misFundTransactionEntity.TradeDate, "MIS", perfReportDS);
                                break;
                            }
                    }
                }
            }
        }

        private static void SetDietzDataMIS(CapitalMovementByCategoryTotal capitalMovementByCategoryTotal, ModifiedDietz dietz, decimal buyPurchase, decimal sell, decimal openingBalance, decimal closingBalance)
        {
            dietz.OpeningBalTotal += openingBalance;
            dietz.ClosingBalanceTotal += closingBalance;

            capitalMovementByCategoryTotal.OpeningBalTotal += openingBalance;
            capitalMovementByCategoryTotal.ClosingBalanceTotal += closingBalance;

            capitalMovementByCategoryTotal.Application += buyPurchase;
            capitalMovementByCategoryTotal.Redemption += sell;
        }

        private static void SetDietzDesktopBuyAndSell(ModifiedDietz modifiedDietz, decimal totalOpeiningBalance, decimal totalClosingBalance, PerfReportDS perfReportDS, IEnumerable<HoldingTransactions> filteredTransactionsByMonth, decimal transferInASX, decimal transferOutASX, decimal adjustmentUp, decimal adjustmentDown, decimal sell, decimal buyPurchase, IOrganization orgCM)
        {
            foreach (HoldingTransactions holdingTransactions in filteredTransactionsByMonth)
            {
                if (holdingTransactions.Type == "RA" || holdingTransactions.Type == "VA")
                {
                    modifiedDietz.AddModDietzEntity(holdingTransactions.NetValue * -1, holdingTransactions.TradeDate, "SEC", perfReportDS);
                }
                else if (holdingTransactions.Type == "AN" || holdingTransactions.Type == "VB")
                {
                    modifiedDietz.AddModDietzEntity(holdingTransactions.NetValue, holdingTransactions.TradeDate, "SEC", perfReportDS);
                }
                if (holdingTransactions.Type == "RJ" || holdingTransactions.Type == "AJ")
                {
                    decimal unitprice = 0;

                    var unitPriceObj = orgCM.Securities.Where(sec => sec.AsxCode == holdingTransactions.InvestmentCode).FirstOrDefault().ASXSecurity.Where(his => his.Date <= holdingTransactions.TradeDate).OrderByDescending(his => his.Date).FirstOrDefault();

                    if (unitPriceObj != null)
                        unitprice = Convert.ToDecimal(unitPriceObj.UnitPrice);

                    holdingTransactions.NetValue = holdingTransactions.Units * unitprice;

                    if (holdingTransactions.Type == "AJ")
                    {
                        if (holdingTransactions.NetValue > 0)
                            holdingTransactions.NetValue = holdingTransactions.NetValue * -1;
                    }

                    modifiedDietz.AddModDietzEntity(holdingTransactions.NetValue, holdingTransactions.TradeDate, "SEC", perfReportDS);
                }

                if (holdingTransactions.Type == "AT" || holdingTransactions.Type == "RT")
                {
                    decimal unitprice = 0;

                    var unitPriceObj = orgCM.Securities.Where(sec => sec.AsxCode == holdingTransactions.InvestmentCode).FirstOrDefault().ASXSecurity.Where(his => his.Date <= holdingTransactions.TradeDate).OrderByDescending(his => his.Date).FirstOrDefault();

                    if (unitPriceObj != null)
                        unitprice = Convert.ToDecimal(unitPriceObj.UnitPrice);

                    holdingTransactions.NetValue = holdingTransactions.Units * unitprice;

                    if (holdingTransactions.Type == "RT")
                    {
                        if (holdingTransactions.NetValue > 0)
                            holdingTransactions.NetValue = holdingTransactions.NetValue * -1;
                    }

                    modifiedDietz.AddModDietzEntity(holdingTransactions.NetValue, holdingTransactions.TradeDate, "SEC", perfReportDS);
                }
            }

            SetServiceTypeModifiedDietz(modifiedDietz, totalOpeiningBalance, totalClosingBalance);

            modifiedDietz.CapitalMovementByCategoryTotal.TransferInOutTotal += transferInASX;
            modifiedDietz.CapitalMovementByCategoryTotal.TransferInOutTotal += transferOutASX;
            modifiedDietz.CapitalMovementByCategoryTotal.TransferInOutTotal += adjustmentUp;
            modifiedDietz.CapitalMovementByCategoryTotal.TransferInOutTotal += adjustmentDown;
            modifiedDietz.CapitalMovementByCategoryTotal.Redemption += (sell * -1);
            modifiedDietz.CapitalMovementByCategoryTotal.Application += buyPurchase;
        }

        private static decimal CalculateTransferInOutNetValue(IOrganization orgCM, HoldingTransactions holdingTransactions)
        {
            double unitPrice = 0;

            var hisPriceObj = orgCM.Securities.Where(sec => sec.AsxCode == holdingTransactions.InvestmentCode).FirstOrDefault().ASXSecurity.Where(
                                    his => his.Date <= holdingTransactions.TradeDate).OrderByDescending(his => his.Date).FirstOrDefault();

            if (hisPriceObj != null)
                unitPrice = hisPriceObj.UnitPrice;

            return holdingTransactions.Units * Convert.ToDecimal(unitPrice);
        }

        private static void SetProductSecurity(IOrganization orgCM, ProductEntity product, DateTime datetime, ProductSecuritiesEntity productSecurity, IEnumerable<AssetEntity> linkedAssets, ref double totalPercentage, SecuritiesEntity asxBroker, IEnumerable<HoldingTransactions> filteredHoldingTransactions, ref decimal totalHolding)
        {
            if (filteredHoldingTransactions.Count() > 0)
            {
                totalPercentage = product.SharePercentage;

                foreach (var linkedasst in linkedAssets)
                {
                    var otherProducts = linkedasst.Products.Where(linkedAsstPro => linkedAsstPro.ProductSecuritiesId != productSecurity.ID);
                    foreach (var OtherproductSecurity in otherProducts)
                    {
                        var OtherproductSecurityDetails = orgCM.ProductSecurities.Where(otherpro => otherpro.ID == OtherproductSecurity.ProductSecuritiesId).FirstOrDefault();

                        if (OtherproductSecurityDetails != null)
                        {
                            var otherAsxBroker = OtherproductSecurityDetails.Details.Where(otherProDetail => otherProDetail.SecurityCodeId == asxBroker.ID);

                            if (otherAsxBroker.Count() > 0)
                                totalPercentage += OtherproductSecurity.SharePercentage;
                        }
                    }
                }

                var hisSecurity = asxBroker.ASXSecurity.Where(secpri => secpri.Date <= datetime).OrderByDescending(secpri => secpri.Date).FirstOrDefault();
                decimal holding = filteredHoldingTransactions.Sum(filteredHolding => filteredHolding.Units);
                decimal unitPrice = 0;
                if (hisSecurity != null)
                    unitPrice = Convert.ToDecimal(hisSecurity.UnitPrice);

                decimal currentValue = holding * unitPrice;
                currentValue = currentValue * Convert.ToDecimal((product.SharePercentage / totalPercentage));
                totalHolding += currentValue;
            }
        }

        private static void CalculateDesktopBrokerWithoutProductSecurity(IOrganization orgCM, DateTime startDate, DateTime endDate, DesktopBrokerAccountCM desktopBrokerAccountCM, ref decimal totalOpeiningBalance, ref decimal totalClosingBalance, Dictionary<string, ModifiedDietz> securitiesCollectionMonthly)
        {
            var filteredHoldingTransactionsByOpening = desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Where(holdTrans =>
                                                                                              holdTrans.TradeDate <= startDate.AddDays(-1));

            var filteredHoldingTransactionsByClosing = desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Where(holdTrans =>
                                                                                  holdTrans.TradeDate <= endDate);

            var filteredHoldingTransactionsByRange = desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Where(holdTrans =>
                                                                                  holdTrans.TradeDate <= endDate && holdTrans.TradeDate >= startDate);

            var groupedSecuritiesOpening = filteredHoldingTransactionsByOpening.GroupBy(holdTran => holdTran.InvestmentCode);
            var groupedSecuritiesRange = filteredHoldingTransactionsByRange.GroupBy(holdTran => holdTran.InvestmentCode);

            foreach (var security in groupedSecuritiesOpening)
            {
                decimal totalSecHoldingUnits = security.Sum(sec => sec.Units);
                var asxBroker = orgCM.Securities.Where(sec => sec.AsxCode == security.Key).FirstOrDefault();
                var hisSecurity = asxBroker.ASXSecurity.Where(secpri => secpri.Date <= startDate.AddDays(-1)).OrderByDescending(secpri => secpri.Date).FirstOrDefault();
                decimal unitPrice = 0;

                if (hisSecurity != null)
                    unitPrice = Convert.ToDecimal(hisSecurity.UnitPrice);

                decimal currentValue = totalSecHoldingUnits * unitPrice;
                totalOpeiningBalance += currentValue;

                ModifiedDietz secDietz = new ModifiedDietz();
                secDietz.Month = startDate;
                secDietz.Type = "SEC";
                if (securitiesCollectionMonthly.ContainsKey(asxBroker.AsxCode))
                    secDietz = securitiesCollectionMonthly[asxBroker.AsxCode];
                else
                    securitiesCollectionMonthly.Add(asxBroker.AsxCode, secDietz);

                secDietz.CapitalMovementByCategoryTotal.OpeningBalTotal += currentValue;
                secDietz.OpeningBalTotal += currentValue;
            }

            var groupedSecuritiesClosing = filteredHoldingTransactionsByClosing.GroupBy(holdTran => holdTran.InvestmentCode);

            foreach (var security in groupedSecuritiesClosing)
            {
                decimal totalSecHoldingUnits = security.Sum(sec => sec.Units);
                var asxBroker = orgCM.Securities.Where(sec => sec.AsxCode == security.Key).FirstOrDefault();
                var hisSecurity = asxBroker.ASXSecurity.Where(secpri => secpri.Date <= endDate).OrderByDescending(secpri => secpri.Date).FirstOrDefault();
                decimal unitPrice = 0;

                if (hisSecurity != null)
                    unitPrice = Convert.ToDecimal(hisSecurity.UnitPrice);

                decimal currentValue = totalSecHoldingUnits * unitPrice;
                totalClosingBalance += currentValue;

                ModifiedDietz secDietz = new ModifiedDietz();
                secDietz.Month = startDate;
                secDietz.Type = "SEC";
                if (securitiesCollectionMonthly.ContainsKey(asxBroker.AsxCode))
                    secDietz = securitiesCollectionMonthly[asxBroker.AsxCode];
                else
                    securitiesCollectionMonthly.Add(asxBroker.AsxCode, secDietz);

                secDietz.ClosingBalanceTotal += currentValue;
                secDietz.CapitalMovementByCategoryTotal.ClosingBalanceTotal += currentValue;
            }
        }

        private static void TDTransactions(ICMBroker Broker, IOrganization orgCM, PerfReportDS perfReportDS, AccountProcessTaskEntity accountProcessTaskEntity, AssetEntity asset, ModelEntity model, ProductEntity product, CapitalMovementByCategoryTotal capitalMovementByCategoryTotal, DateTime startDate, DateTime endDate, ModifiedDietz modifiedDietz, ModifiedDietz modifiedDietzDIY, ModifiedDietz modifiedDietzDIWM, ModifiedDietz modifiedDietzDIFM, Dictionary<string, ModifiedDietz> securitiesCollectionMonthly)
        {
            if (accountProcessTaskEntity.LinkedEntity.Clid == Guid.Empty)
            {
                accountProcessTaskEntity.LinkedEntity.Clid = product.EntityId.Clid;
                accountProcessTaskEntity.LinkedEntity.Csid = product.EntityId.Csid;
            }

            IOrganizationUnit bankAccountUnit = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as IOrganizationUnit;

            if (bankAccountUnit != null)
            {
                BankAccountEntity BankAccountEntity = bankAccountUnit.ClientEntity as BankAccountEntity;
                var filteredByGroup = BankAccountEntity.CashManagementTransactions.GroupBy(tdtypetran => tdtypetran.InstitutionID);
                var filteredTransactionsOpening = BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate.Date <= startDate.AddDays(-1));
                var filteredTransactionsClosing = BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate.Date <= endDate);

                decimal openingBalance = 0;
                decimal closingBalance = 0;
                var tdGroupsOpeningType = filteredTransactionsOpening.GroupBy(cashMan => cashMan.TransactionType);

                foreach (var tdType in tdGroupsOpeningType)
                {
                    var groupByIns = tdType.GroupBy(tdtypetran => tdtypetran.InstitutionID);

                    foreach (var groupins in groupByIns)
                    {
                        var transaction = groupins.FirstOrDefault();
                        decimal holding = groupins.Sum(inssum => inssum.Amount);
                        openingBalance += holding;
                    }
                }

                var tdGroupsClosingType = filteredTransactionsClosing.GroupBy(cashMan => cashMan.TransactionType);

                foreach (var tdType in tdGroupsClosingType)
                {
                    var groupByIns = tdType.GroupBy(tdtypetran => tdtypetran.InstitutionID);

                    foreach (var groupins in groupByIns)
                    {
                        var transaction = groupins.FirstOrDefault();
                        decimal holding = groupins.Sum(inssum => inssum.Amount);
                        closingBalance += holding;
                    }
                }

                var filteredTransactions = BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate.Date <= endDate && trans.TransactionDate.Date >= startDate);
                var tdGroupsType = filteredTransactions.GroupBy(cashMan => cashMan.TransactionType);
                var tdGroupsTypeOpening = filteredTransactionsOpening.GroupBy(cashMan => cashMan.TransactionType);
                var tdGroupsTypeClosing = filteredTransactionsClosing.GroupBy(cashMan => cashMan.TransactionType);

                foreach (var groupins in filteredByGroup)
                {
                    var filteredByGroupBySubType = groupins.GroupBy(tran => tran.TransactionType);

                    var filteredByGroupBySubType1 = groupins.GroupBy(tran => tran.TransactionType);


                    foreach (var groupinsBySubType in filteredByGroupBySubType)
                    {
                        if (groupinsBySubType.Key != null)
                        {
                            var filteredTransactionsOpeningByGroup = groupinsBySubType.Where(trans => trans.TransactionDate.Date <= startDate.AddDays(-1));
                            var filteredTransactionsClosingByGroup = groupinsBySubType.Where(trans => trans.TransactionDate.Date <= endDate);
                            var filteredTransactionsByGroup = groupinsBySubType.Where(trans => trans.TransactionDate.Date <= endDate && trans.TransactionDate.Date >= startDate);

                            decimal openingSubType = filteredTransactionsOpeningByGroup.Sum(inssum => inssum.Amount);
                            decimal clodingSubType = filteredTransactionsClosingByGroup.Sum(inssum => inssum.Amount);

                            decimal applicationByGroup = filteredTransactionsByGroup.Where(tran => tran.SystemTransactionType == "Application").Sum(tran => tran.TotalAmount);
                            decimal redemptionByGroup = filteredTransactionsByGroup.Where(tran => tran.SystemTransactionType == "Redemption").Sum(tran => tran.TotalAmount);
                            decimal openingBalanceByGroup = filteredTransactionsOpeningByGroup.Sum(inssum => inssum.Amount);
                            decimal closingBalanceByGroup = filteredTransactionsClosingByGroup.Sum(inssum => inssum.Amount);
                            decimal incomeInterest = filteredTransactionsByGroup.Where(tran => tran.Category == "Income" && tran.SystemTransactionType == "Interest").Sum(tran => tran.TotalAmount);
                            decimal incomeCommissionRebate = filteredTransactionsByGroup.Where(tran => tran.Category == "Income" && tran.SystemTransactionType == "Commission Rebate").Sum(tran => tran.TotalAmount);

                            var inst = orgCM.Institution.Where(ins => ins.ID == groupins.Key).FirstOrDefault();
                            Common.CashManagementEntity cashManageEntity = filteredTransactionsByGroup.FirstOrDefault();
                            string subType = String.Empty;

                            if (groupinsBySubType.Key.ToLower().Contains("td"))
                                subType = "-TD";

                            if (groupinsBySubType.Key.ToLower().Contains("call"))
                                subType = "-At Call";

                            string instName = string.Empty;
                            if (inst != null)
                                instName = inst.Name;

                            ModifiedDietz dietz = new ModifiedDietz();
                            dietz.Month = startDate;
                            dietz.Type = "TDs";
                            dietz.CapitalMovementByCategoryTotal.Type = "TDs";
                            string key = instName + subType;
                            dietz.OpeningBalTotal += openingSubType;
                            dietz.ClosingBalanceTotal += clodingSubType;
                            dietz.IncomeAccrual += incomeInterest + incomeCommissionRebate;
                            dietz.CapitalMovementByCategoryTotal.Application += applicationByGroup;
                            dietz.CapitalMovementByCategoryTotal.Redemption += redemptionByGroup;
                            dietz.CapitalMovementByCategoryTotal.OpeningBalTotal += openingBalanceByGroup;
                            dietz.CapitalMovementByCategoryTotal.ClosingBalanceTotal += closingBalanceByGroup;

                            DateTime startDateIfOpeningZero = startDate;
                            DateTime endDateIfClosingZero = endDate;

                            var firstTransaction = groupinsBySubType.OrderBy(tran => tran.TransactionDate).FirstOrDefault();
                            var lastTransaction = groupinsBySubType.OrderByDescending(tran => tran.TransactionDate).FirstOrDefault();

                            if (firstTransaction != null)
                                startDateIfOpeningZero = firstTransaction.TransactionDate;

                            if (lastTransaction != null)
                                endDateIfClosingZero = lastTransaction.TransactionDate;

                            dietz.StartDateIfOpeningZero = startDateIfOpeningZero;
                            dietz.EndDateIfClosingZero = endDateIfClosingZero;

                            if (securitiesCollectionMonthly.ContainsKey(key))
                                dietz = securitiesCollectionMonthly[key];
                            else
                                securitiesCollectionMonthly.Add(key, dietz);
                            decimal changeinmkt = dietz.CapitalMovementByCategoryTotal.ChangeInInvestTotal;

                            var totalMonthTransactionsNoIncomeByGroup = groupinsBySubType.Where(tran => tran.SystemTransactionType != "Dividend" && tran.SystemTransactionType != "Distribution");
                            SetServiceTypeCashModifiedDietzOnlyMovement(dietz, openingBalanceByGroup, closingBalanceByGroup, totalMonthTransactionsNoIncomeByGroup, perfReportDS);
                        }
                    }
                }


                decimal application = filteredTransactions.Where(tran => tran.SystemTransactionType == "Application").Sum(tran => tran.TotalAmount);
                decimal redemption = filteredTransactions.Where(tran => tran.SystemTransactionType == "Redemption").Sum(tran => tran.TotalAmount);
                decimal incomeInterestOverall = filteredTransactions.Where(tran => tran.Category == "Income" && tran.SystemTransactionType == "Interest").Sum(tran => tran.TotalAmount);
                decimal incomeCommissionRebateOverall = filteredTransactions.Where(tran => tran.Category == "Income" && tran.SystemTransactionType == "Commission Rebate").Sum(tran => tran.TotalAmount);

                SetDietzTDData(capitalMovementByCategoryTotal, modifiedDietz, openingBalance, closingBalance, application, redemption, incomeInterestOverall, incomeCommissionRebateOverall);

                var totalMonthTransactionsNoIncomePartial = filteredTransactions.Where(tran => tran.SystemTransactionType != "Interest" && tran.SystemTransactionType != "Commission Rebate" && tran.SystemTransactionType != "Dividend" && tran.SystemTransactionType != "Distribution");

                if (model.ServiceType == ServiceTypes.DoItForMe)
                {
                    SetDietzTDData(modifiedDietzDIFM.CapitalMovementByCategoryTotal, modifiedDietzDIFM, openingBalance, closingBalance, application, redemption, incomeInterestOverall, incomeCommissionRebateOverall);
                    SetServiceTypeCashModifiedDietzOnlyMovement(modifiedDietzDIFM, openingBalance, closingBalance, totalMonthTransactionsNoIncomePartial, perfReportDS);

                }
                else if (model.ServiceType == ServiceTypes.DoItWithMe)
                {
                    SetDietzTDData(modifiedDietzDIWM.CapitalMovementByCategoryTotal, modifiedDietzDIWM, openingBalance, closingBalance, application, redemption, incomeInterestOverall, incomeCommissionRebateOverall);
                    SetServiceTypeCashModifiedDietzOnlyMovement(modifiedDietzDIWM, openingBalance, closingBalance, totalMonthTransactionsNoIncomePartial, perfReportDS);

                }
                else if (model.ServiceType == ServiceTypes.DoItYourSelf)
                {
                    SetDietzTDData(modifiedDietzDIY.CapitalMovementByCategoryTotal, modifiedDietzDIY, openingBalance, closingBalance, application, redemption, incomeInterestOverall, incomeCommissionRebateOverall);
                    SetServiceTypeCashModifiedDietzOnlyMovement(modifiedDietzDIY, openingBalance, closingBalance, totalMonthTransactionsNoIncomePartial, perfReportDS);

                }

                SetServiceTypeCashModifiedDietzOnlyMovement(modifiedDietz, openingBalance, closingBalance, totalMonthTransactionsNoIncomePartial, perfReportDS);
            }
        }

        private static void SetDietzTDData(CapitalMovementByCategoryTotal capitalMovementByCategoryTotal, ModifiedDietz modifiedDietz, decimal openingBalance, decimal closingBalance, decimal application, decimal redemption, decimal incomeInterestOverall, decimal incomeCommissionRebateOverall)
        {
            modifiedDietz.OpeningBalTotal += openingBalance;
            modifiedDietz.ClosingBalanceTotal += closingBalance;
            modifiedDietz.IncomeAccrual += incomeInterestOverall + incomeCommissionRebateOverall;
            capitalMovementByCategoryTotal.OpeningBalTotal += openingBalance;
            capitalMovementByCategoryTotal.ClosingBalanceTotal += closingBalance;
            capitalMovementByCategoryTotal.Application += application;
            capitalMovementByCategoryTotal.Redemption += redemption;
        }

        private static void BankAccountTransctions(string clientType, ICMBroker Broker, PerfReportDS perfReportDS, AccountProcessTaskEntity accountProcessTaskEntity, AssetEntity asset, ModelEntity model, ProductEntity product,
            CapitalMovementByCategoryTotal capitalMovementByCategoryTotal, DateTime startDate, DateTime endDate, ModifiedDietz modifiedDietz, ModifiedDietz modifiedDietzDIY, ModifiedDietz modifiedDietzDIWM, ModifiedDietz modifiedDietzDIFM, IOrganization orgCM, Dictionary<string, ModifiedDietz> securitiesCollectionMonthly)
        {
            if (accountProcessTaskEntity.LinkedEntity.Clid == Guid.Empty)
            {
                accountProcessTaskEntity.LinkedEntity.Clid = product.EntityId.Clid;
                accountProcessTaskEntity.LinkedEntity.Csid = product.EntityId.Csid;
            }

            BankAccountCM bankAccount = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as BankAccountCM;
            var filteredTransactionsStartDate = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate <= startDate.AddDays(-1));
            var filteredTransactionsEndDat = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate <= endDate);
            decimal openingBalance = filteredTransactionsStartDate.Sum(calc => calc.TotalAmount);
            decimal closingBalance = filteredTransactionsEndDat.Sum(calc => calc.TotalAmount);

            var totalMonthTransactions = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate.Date <= endDate.Date && trans.TransactionDate.Date >= startDate.Date);

            DateTime startDateIfOpeningZero = perfReportDS.StartDate;
            DateTime endDateIfClosingZero = perfReportDS.EndDate;

            var firstTransaction = totalMonthTransactions.OrderBy(tran => tran.TransactionDate).FirstOrDefault();
            var lastTransaction = totalMonthTransactions.OrderByDescending(tran => tran.TransactionDate).FirstOrDefault();

            if (firstTransaction != null)
                startDateIfOpeningZero = firstTransaction.TransactionDate;

            if (lastTransaction != null)
                endDateIfClosingZero = lastTransaction.TransactionDate;

            decimal contributions = totalMonthTransactions.Where(tran => tran.Category == "Contribution").Sum(tran => tran.TotalAmount);
            decimal income = totalMonthTransactions.Where(tran => tran.Category == "Income").Sum(tran => tran.TotalAmount);
            decimal distribution = totalMonthTransactions.Where(tran => tran.SystemTransactionType == "Distribution").Sum(tran => tran.TotalAmount);
            decimal dividend = totalMonthTransactions.Where(tran => tran.SystemTransactionType == "Dividend").Sum(tran => tran.TotalAmount);
            decimal transferin = totalMonthTransactions.Where(tran => tran.Category == "Income" && tran.SystemTransactionType == "Transfer In").Sum(tran => tran.TotalAmount);
            decimal internalCashMovementIncome = totalMonthTransactions.Where(tran => tran.Category == "Income" && tran.SystemTransactionType == "Internal Cash Movement").Sum(tran => tran.TotalAmount);
            decimal transferInOutOther = totalMonthTransactions.Where(tran => tran.SystemTransactionType == string.Empty).Sum(tran => tran.TotalAmount);
            income = income - transferin - internalCashMovementIncome;

            decimal incomeInterest = totalMonthTransactions.Where(tran => tran.Category == "Income" && tran.SystemTransactionType == "Interest").Sum(tran => tran.TotalAmount);
            decimal incomeCommissionRebate = totalMonthTransactions.Where(tran => tran.Category == "Income" && tran.SystemTransactionType == "Commission Rebate").Sum(tran => tran.TotalAmount);

            decimal taxinout = totalMonthTransactions.Where(tran => tran.Category.ToLower() == "tax").Sum(tran => tran.TotalAmount); ;
            decimal benefitPayment = totalMonthTransactions.Where(tran => tran.Category == "Benefit Payment").Sum(tran => tran.TotalAmount);
            decimal investmentApplication = totalMonthTransactions.Where(tran => tran.Category == "Investment" && tran.SystemTransactionType == "Application").Sum(tran => tran.TotalAmount);
            decimal investmentRedemption = totalMonthTransactions.Where(tran => tran.Category == "Investment" && tran.SystemTransactionType == "Redemption").Sum(tran => tran.TotalAmount);

            decimal expense = totalMonthTransactions.Where(tran => tran.Category == "Expense").Sum(tran => tran.TotalAmount);
            decimal transferOut = totalMonthTransactions.Where(tran => tran.Category == "Expense" && tran.SystemTransactionType == "Transfer Out").Sum(tran => tran.TotalAmount);
            decimal internalCashMovementExpense = totalMonthTransactions.Where(tran => tran.Category == "Expense" && tran.SystemTransactionType == "Internal Cash Movement").Sum(tran => tran.TotalAmount);
            expense = expense - transferOut - internalCashMovementExpense;

            SetModifiedDietzCash(capitalMovementByCategoryTotal, modifiedDietz, openingBalance, closingBalance, contributions, income, transferin, internalCashMovementIncome, transferInOutOther, taxinout, benefitPayment, investmentApplication, investmentRedemption, expense, transferOut, internalCashMovementExpense);
            decimal changeInvestment = capitalMovementByCategoryTotal.ChangeInInvestTotal;

            ModifiedDietz dietz = new ModifiedDietz();
            dietz.Type = "CASH";
            dietz.CapitalMovementByCategoryTotal.Type = "CASH";
            dietz.StartDateIfOpeningZero = startDateIfOpeningZero;
            dietz.EndDateIfClosingZero = endDateIfClosingZero;
            SetBankModDietzData(income - incomeCommissionRebate - incomeInterest, startDate, openingBalance, closingBalance, startDateIfOpeningZero, endDateIfClosingZero, contributions, 0, transferin, internalCashMovementIncome, transferInOutOther, incomeInterest, incomeCommissionRebate, taxinout, benefitPayment, investmentApplication, investmentRedemption, expense, transferOut, internalCashMovementExpense, dietz);

            if (clientType.ToLower() == "e-clipse super")
                dietz.Movement += income - income - incomeCommissionRebate - incomeInterest;
            else
            {
                dietz.CapitalMovementByCategoryTotal.DividendDistribution = distribution + dividend;
                dietz.Movement += income - income - distribution - dividend - incomeCommissionRebate - incomeInterest;
            }

            string key = "CASH - " + bankAccount.BankAccountEntity.AccountNumber;

            if (securitiesCollectionMonthly.ContainsKey(key))
                dietz = securitiesCollectionMonthly[key];
            else
                securitiesCollectionMonthly.Add(key, dietz);

            var totalMonthTransactionsNoIncome = totalMonthTransactions.Where(tran => tran.SystemTransactionType != "Dividend" && tran.SystemTransactionType != "Distribution" && tran.SystemTransactionType != "Rental Income" && tran.SystemTransactionType != "Commission Rebate" && tran.SystemTransactionType != "Interest");
            var totalMonthTransactionsNoIncomePartial = totalMonthTransactions.Where(tran => tran.SystemTransactionType != "Dividend" && tran.SystemTransactionType != "Distribution");

            if (model.ServiceType == ServiceTypes.DoItForMe)
            {
                modifiedDietzDIFM.StartDateIfOpeningZero = startDateIfOpeningZero;
                modifiedDietzDIFM.EndDateIfClosingZero = endDateIfClosingZero;
                SetModifiedDietzCash(modifiedDietzDIFM.CapitalMovementByCategoryTotal, modifiedDietzDIFM, openingBalance, closingBalance, contributions, income, transferin, internalCashMovementIncome, transferInOutOther, taxinout, benefitPayment, investmentApplication, investmentRedemption, expense, transferOut, internalCashMovementExpense);
                SetServiceTypeCashModifiedDietzOnlyMovement(modifiedDietzDIFM, openingBalance, closingBalance, totalMonthTransactionsNoIncome, perfReportDS);
            }
            else if (model.ServiceType == ServiceTypes.DoItWithMe)
            {
                modifiedDietzDIWM.StartDateIfOpeningZero = startDateIfOpeningZero;
                modifiedDietzDIWM.EndDateIfClosingZero = endDateIfClosingZero;
                SetModifiedDietzCash(modifiedDietzDIWM.CapitalMovementByCategoryTotal, modifiedDietzDIWM, openingBalance, closingBalance, contributions, income, transferin, internalCashMovementIncome, transferInOutOther, taxinout, benefitPayment, investmentApplication, investmentRedemption, expense, transferOut, internalCashMovementExpense);
                SetServiceTypeCashModifiedDietzOnlyMovement(modifiedDietzDIWM, openingBalance, closingBalance, totalMonthTransactionsNoIncome, perfReportDS);
            }
            else if (model.ServiceType == ServiceTypes.DoItYourSelf)
            {
                modifiedDietzDIY.StartDateIfOpeningZero = startDateIfOpeningZero;
                modifiedDietzDIY.EndDateIfClosingZero = endDateIfClosingZero;
                SetModifiedDietzCash(modifiedDietzDIY.CapitalMovementByCategoryTotal, modifiedDietzDIY, openingBalance, closingBalance, contributions, income, transferin, internalCashMovementIncome, transferInOutOther, taxinout, benefitPayment, investmentApplication, investmentRedemption, expense, transferOut, internalCashMovementExpense);
                SetServiceTypeCashModifiedDietzOnlyMovement(modifiedDietzDIY, openingBalance, closingBalance, totalMonthTransactionsNoIncome, perfReportDS);
            }

            if (clientType.ToLower() == "e-clipse super")
            {
                decimal incomeAdjustment = modifiedDietzDIWM.CapitalMovementByCategoryTotal.IncomeTotal;
                modifiedDietzDIWM.CapitalMovementByCategoryTotal.IncomeTotal -= distribution;
                modifiedDietzDIWM.CapitalMovementByCategoryTotal.ClosingBalanceTotal -= distribution;
                modifiedDietzDIWM.ClosingBalanceTotal -= distribution;

                modifiedDietzDIFM.CapitalMovementByCategoryTotal.IncomeTotal += distribution;
                modifiedDietzDIFM.CapitalMovementByCategoryTotal.ClosingBalanceTotal += distribution;
                modifiedDietzDIFM.ClosingBalanceTotal += distribution;
            }

            modifiedDietz.StartDateIfOpeningZero = startDateIfOpeningZero;
            modifiedDietz.EndDateIfClosingZero = endDateIfClosingZero;
            SetServiceTypeCashModifiedDietzOnlyMovement(modifiedDietz, openingBalance, closingBalance, totalMonthTransactionsNoIncome, perfReportDS);
            SetServiceTypeCashModifiedDietzOnlyMovement(dietz, openingBalance, closingBalance, totalMonthTransactionsNoIncome, perfReportDS);
        }

        private static void SetModifiedDietzCash(CapitalMovementByCategoryTotal capitalMovementByCategoryTotal, ModifiedDietz modifiedDietz, decimal openingBalance, decimal closingBalance, decimal contributions, decimal income, decimal transferin, decimal internalCashMovementIncome, decimal transferInOutOther, decimal taxinout, decimal benefitPayment, decimal investmentApplication, decimal investmentRedemption, decimal expense, decimal transferOut, decimal internalCashMovementExpense)
        {
            capitalMovementByCategoryTotal.TransferInOutTotal += contributions;
            capitalMovementByCategoryTotal.TransferInOutTotal += benefitPayment;
            capitalMovementByCategoryTotal.TransferInOutTotal += transferOut;
            capitalMovementByCategoryTotal.TransferInOutTotal += transferin;
            capitalMovementByCategoryTotal.TransferInOutTotal += transferInOutOther;

            capitalMovementByCategoryTotal.InternalCashMovementTotal += internalCashMovementIncome;
            capitalMovementByCategoryTotal.InternalCashMovementTotal += internalCashMovementExpense;

            capitalMovementByCategoryTotal.Application += investmentApplication;
            capitalMovementByCategoryTotal.Redemption += investmentRedemption;

            capitalMovementByCategoryTotal.ExpenseTotal += expense;
            capitalMovementByCategoryTotal.ExpenseTotal += taxinout;

            capitalMovementByCategoryTotal.IncomeTotal += income;

            capitalMovementByCategoryTotal.OpeningBalTotal += openingBalance;
            capitalMovementByCategoryTotal.ClosingBalanceTotal += closingBalance;

            modifiedDietz.OpeningBalTotal += openingBalance;
            modifiedDietz.ClosingBalanceTotal += closingBalance;
        }

        private static void SetBankModDietzData(decimal otherIncome, DateTime startDate, decimal openingBalance, decimal closingBalance, DateTime startDateIfOpeningZero, DateTime endDateIfClosingZero, decimal contributions, decimal income, decimal transferin, decimal internalCashMovementIncome, decimal transferInOutOther, decimal incomeInterest, decimal incomeCommissionRebate, decimal taxinout, decimal benefitPayment, decimal investmentApplication, decimal investmentRedemption, decimal expense, decimal transferOut, decimal internalCashMovementExpense, ModifiedDietz dietz)
        {
            dietz.Type = "CASH";
            dietz.CapitalMovementByCategoryTotal.Type = "CASH";
            dietz.Month = startDate;
            dietz.OpeningBalTotal += openingBalance;
            dietz.ClosingBalanceTotal += closingBalance;
            dietz.IncomeAccrual += incomeInterest + incomeCommissionRebate;
            dietz.StartDateIfOpeningZero = startDateIfOpeningZero;
            dietz.EndDateIfClosingZero = endDateIfClosingZero;
            dietz.CapitalMovementByCategoryTotal.TransferInOutTotal += contributions;
            dietz.CapitalMovementByCategoryTotal.TransferInOutTotal += benefitPayment;
            dietz.CapitalMovementByCategoryTotal.TransferInOutTotal += transferOut;
            dietz.CapitalMovementByCategoryTotal.TransferInOutTotal += transferin;
            dietz.CapitalMovementByCategoryTotal.TransferInOutTotal += transferInOutOther;

            dietz.CapitalMovementByCategoryTotal.InternalCashMovementTotal += internalCashMovementIncome;
            dietz.CapitalMovementByCategoryTotal.InternalCashMovementTotal += internalCashMovementExpense;

            dietz.CapitalMovementByCategoryTotal.Application += investmentApplication;
            dietz.CapitalMovementByCategoryTotal.Redemption += investmentRedemption;

            dietz.CapitalMovementByCategoryTotal.ExpenseTotal += expense;
            dietz.CapitalMovementByCategoryTotal.ExpenseTotal += taxinout;

            dietz.CapitalMovementByCategoryTotal.IncomeTotal += income;
            dietz.CapitalMovementByCategoryTotal.OtherIncome += otherIncome;
            dietz.CapitalMovementByCategoryTotal.OpeningBalTotal += openingBalance;
            dietz.CapitalMovementByCategoryTotal.ClosingBalanceTotal += closingBalance;
        }

        private static void SetServiceTypeCashModifiedDietz(ModifiedDietz modifiedDietz, decimal openingBalance, decimal closingBalance, IEnumerable<Common.CashManagementEntity> totalMonthTransactions, PerfReportDS perfReportDS)
        {
            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashManagementEntity in totalMonthTransactions)
                modifiedDietz.AddModDietzEntity(cashManagementEntity.TotalAmount, cashManagementEntity.TransactionDate, "CASH", perfReportDS);
        }

        private static void SetServiceTypeCashModifiedDietzOnlyMovement(ModifiedDietz modifiedDietz, decimal openingBalance, decimal closingBalance, IEnumerable<Common.CashManagementEntity> totalMonthTransactions, PerfReportDS perfReportDS)
        {
            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashManagementEntity in totalMonthTransactions)
                modifiedDietz.AddModDietzEntity(cashManagementEntity.TotalAmount, cashManagementEntity.TransactionDate, "CASH", perfReportDS);
        }

        private static void SetServiceTypeModifiedDietz(ModifiedDietz modifiedDietz, decimal openingBalance, decimal closingBalance)
        {
            modifiedDietz.OpeningBalTotal += openingBalance;
            modifiedDietz.ClosingBalanceTotal += closingBalance;
        }

        private static void ManualTransactions(string clientID, ObservableCollection<ClientManualAssetEntity> ClientManualAssetCollection, IOrganization orgCM, PerfReportDS perfReportDS, CapitalMovementByCategoryTotal capitalMovementByCategoryTotal, DateTime startDate, DateTime endDate, ModifiedDietz modifiedDietz, ModifiedDietz modifiedDietzMAN, Dictionary<string, ModifiedDietz> securitiesCollectionMonthly)
        {
            if (ClientManualAssetCollection != null)
            {

                var filteredManualAssetsByGroup = ClientManualAssetCollection.GroupBy(man => man.InvestmentCode);

                foreach (var manualAssetGroup in filteredManualAssetsByGroup)
                {
                    string companyName = string.Empty;
                    string companyCode = string.Empty;

                    var systemManualAsset = orgCM.ManualAsset.Where(manList => manList.ID == manualAssetGroup.Key).FirstOrDefault();
                    companyName = systemManualAsset.CompanyName;
                    companyCode = systemManualAsset.AsxCode;

                    ModifiedDietz dietzMANPerSecurity = new ModifiedDietz();
                    dietzMANPerSecurity.Month = startDate;
                    dietzMANPerSecurity.Type = "MAN";
                    if (securitiesCollectionMonthly.ContainsKey(companyCode))
                        dietzMANPerSecurity = securitiesCollectionMonthly[companyCode];
                    else
                        securitiesCollectionMonthly.Add(companyCode, dietzMANPerSecurity);


                    var filteredManualAssetsStartDate = manualAssetGroup.Where(man => man.TransactionDate <= startDate);
                    var filteredManualAssetsEndDate = manualAssetGroup.Where(man => man.TransactionDate <= endDate);

                    decimal openingBalance = 0;

                    foreach (var manualAsset in filteredManualAssetsStartDate)
                    {
                        var systemManualAssetPriceHistory = systemManualAsset.ASXSecurity.Where(manualHIS => manualHIS.Date <= startDate.AddDays(-1)).OrderByDescending(manualHis => manualHis.Date).FirstOrDefault();
                        if (systemManualAssetPriceHistory != null)
                            openingBalance += manualAsset.UnitsOnHand * Convert.ToDecimal(systemManualAssetPriceHistory.UnitPrice);
                    }

                    decimal closingBalance = 0;

                    foreach (var manualAsset in filteredManualAssetsEndDate)
                    {
                        var systemManualAssetPriceHistory = systemManualAsset.ASXSecurity.Where(manualHIS => manualHIS.Date <= endDate).OrderByDescending(manualHis => manualHis.Date).FirstOrDefault();
                        if (systemManualAssetPriceHistory != null)
                            closingBalance += manualAsset.UnitsOnHand * Convert.ToDecimal(systemManualAssetPriceHistory.UnitPrice);
                    }

                    dietzMANPerSecurity.CapitalMovementByCategoryTotal.OpeningBalTotal += openingBalance;
                    dietzMANPerSecurity.CapitalMovementByCategoryTotal.ClosingBalanceTotal += closingBalance;

                    var filteredTransactionsByMonth = manualAssetGroup.Where(man => man.TransactionDate >= startDate && man.TransactionDate <= endDate);

                    dietzMANPerSecurity.OpeningBalTotal += openingBalance;
                    dietzMANPerSecurity.ClosingBalanceTotal += closingBalance;

                    DateTime startTransactionDate = startDate;
                    DateTime endTransactionDate = endDate;

                    var startTransactionVar = filteredTransactionsByMonth.OrderBy(tran => tran.TransactionDate).FirstOrDefault();
                    var endTransactionVar = filteredTransactionsByMonth.OrderByDescending(tran => tran.TransactionDate).FirstOrDefault();

                    if (startTransactionVar != null)
                        startTransactionDate = startTransactionVar.TransactionDate;

                    if (endTransactionVar != null)
                        endTransactionDate = endTransactionVar.TransactionDate;


                    foreach (ClientManualAssetEntity clientManualAssetEntity in filteredTransactionsByMonth)
                    {
                        decimal netValue = 0;
                        var systemManualAssetPriceHistory = systemManualAsset.ASXSecurity.Where(manualHIS => manualHIS.Date <= clientManualAssetEntity.TransactionDate).OrderByDescending(manualHis => manualHis.Date).FirstOrDefault();
                        if (systemManualAssetPriceHistory != null)
                        {
                            if (clientManualAssetEntity.TransactionType == "Transfer In")
                            {
                                if (systemManualAssetPriceHistory.PurValue != 0)
                                    netValue = clientManualAssetEntity.UnitsOnHand * Convert.ToDecimal(systemManualAssetPriceHistory.PurValue);
                                else
                                    netValue = clientManualAssetEntity.UnitsOnHand * Convert.ToDecimal(systemManualAssetPriceHistory.UnitPrice);

                                dietzMANPerSecurity.CapitalMovementByCategoryTotal.TransferInOutTotal += netValue;

                                dietzMANPerSecurity.AddModDietzEntity(netValue, clientManualAssetEntity.TransactionDate, "Manual", perfReportDS, startTransactionDate, endTransactionDate);
                            }

                            if (clientManualAssetEntity.TransactionType == "Transfer Out")
                            {
                                netValue = -1 * (clientManualAssetEntity.UnitsOnHand * Convert.ToDecimal(systemManualAssetPriceHistory.UnitPrice));
                                dietzMANPerSecurity.CapitalMovementByCategoryTotal.TransferInOutTotal += netValue;

                                dietzMANPerSecurity.AddModDietzEntity(netValue, clientManualAssetEntity.TransactionDate, "Manual", perfReportDS, startTransactionDate, endTransactionDate);
                            }

                            if (clientManualAssetEntity.TransactionType == "Application")
                            {
                                if (systemManualAssetPriceHistory.PurValue != 0)
                                    netValue = clientManualAssetEntity.UnitsOnHand * Convert.ToDecimal(systemManualAssetPriceHistory.PurValue);
                                else
                                    netValue = clientManualAssetEntity.UnitsOnHand * Convert.ToDecimal(systemManualAssetPriceHistory.UnitPrice);

                                dietzMANPerSecurity.CapitalMovementByCategoryTotal.Application += netValue;

                                dietzMANPerSecurity.AddModDietzEntity(netValue, clientManualAssetEntity.TransactionDate, "Manual", perfReportDS, startTransactionDate, endTransactionDate);
                            }

                            if (clientManualAssetEntity.TransactionType == "Redemption")
                            {
                                netValue = -1 * (clientManualAssetEntity.UnitsOnHand * Convert.ToDecimal(systemManualAssetPriceHistory.UnitPrice));
                                dietzMANPerSecurity.CapitalMovementByCategoryTotal.Redemption += netValue;

                                dietzMANPerSecurity.AddModDietzEntity(netValue, clientManualAssetEntity.TransactionDate, "Manual", perfReportDS, startTransactionDate, endTransactionDate);
                            }

                        }
                    }

                }
            }

            if (ClientManualAssetCollection != null)
            {
                var filteredManualAssetsStartDate = ClientManualAssetCollection.Where(man => man.TransactionDate <= startDate);
                var filteredManualAssetsEndDate = ClientManualAssetCollection.Where(man => man.TransactionDate <= endDate);

                decimal openingBalance = 0;

                foreach (var manualAsset in filteredManualAssetsStartDate)
                {
                    var systemManualAsset = orgCM.ManualAsset.Where(manList => manList.ID == manualAsset.InvestmentCode).FirstOrDefault();
                    var systemManualAssetPriceHistory = systemManualAsset.ASXSecurity.Where(manualHIS => manualHIS.Date <= startDate.AddDays(-1)).OrderByDescending(manualHis => manualHis.Date).FirstOrDefault();
                    if (systemManualAssetPriceHistory != null)
                        openingBalance += manualAsset.UnitsOnHand * Convert.ToDecimal(systemManualAssetPriceHistory.UnitPrice);
                }

                decimal closingBalance = 0;

                foreach (var manualAsset in filteredManualAssetsEndDate)
                {
                    var systemManualAsset = orgCM.ManualAsset.Where(manList => manList.ID == manualAsset.InvestmentCode).FirstOrDefault();
                    var systemManualAssetPriceHistory = systemManualAsset.ASXSecurity.Where(manualHIS => manualHIS.Date <= endDate).OrderByDescending(manualHis => manualHis.Date).FirstOrDefault();
                    if (systemManualAssetPriceHistory != null)
                        closingBalance += manualAsset.UnitsOnHand * Convert.ToDecimal(systemManualAssetPriceHistory.UnitPrice);
                }

                capitalMovementByCategoryTotal.OpeningBalTotal += openingBalance;
                capitalMovementByCategoryTotal.ClosingBalanceTotal += closingBalance;

                var filteredTransactionsByMonth = ClientManualAssetCollection.Where(man => man.TransactionDate >= startDate && man.TransactionDate <= endDate);

                modifiedDietz.OpeningBalTotal += openingBalance;
                modifiedDietz.ClosingBalanceTotal += closingBalance;

                modifiedDietzMAN.OpeningBalTotal += openingBalance;
                modifiedDietzMAN.ClosingBalanceTotal += closingBalance;
                modifiedDietzMAN.CapitalMovementByCategoryTotal.OpeningBalTotal += openingBalance;
                modifiedDietzMAN.CapitalMovementByCategoryTotal.ClosingBalanceTotal += closingBalance;

                DateTime startTransactionDate = startDate;
                DateTime endTransactionDate = endDate;

                var startTransactionVar = filteredTransactionsByMonth.OrderBy(tran => tran.TransactionDate).FirstOrDefault();
                var endTransactionVar = filteredTransactionsByMonth.OrderByDescending(tran => tran.TransactionDate).FirstOrDefault();

                if (startTransactionVar != null)
                    startTransactionDate = startTransactionVar.TransactionDate;

                if (endTransactionVar != null)
                    endTransactionDate = endTransactionVar.TransactionDate;


                foreach (ClientManualAssetEntity clientManualAssetEntity in filteredTransactionsByMonth)
                {
                    decimal netValue = 0;

                    var systemManualAsset = orgCM.ManualAsset.Where(manList => manList.ID == clientManualAssetEntity.InvestmentCode).FirstOrDefault();
                    var systemManualAssetPriceHistory = systemManualAsset.ASXSecurity.Where(manualHIS => manualHIS.Date <= clientManualAssetEntity.TransactionDate).OrderByDescending(manualHis => manualHis.Date).FirstOrDefault();
                    if (systemManualAssetPriceHistory != null)
                    {
                        if (clientManualAssetEntity.TransactionType == "Transfer In")
                        {
                            if (systemManualAssetPriceHistory.PurValue != 0)
                                netValue = clientManualAssetEntity.UnitsOnHand * Convert.ToDecimal(systemManualAssetPriceHistory.PurValue);
                            else
                                netValue = clientManualAssetEntity.UnitsOnHand * Convert.ToDecimal(systemManualAssetPriceHistory.UnitPrice);

                            capitalMovementByCategoryTotal.TransferInOutTotal += netValue;
                            modifiedDietzMAN.CapitalMovementByCategoryTotal.TransferInOutTotal += netValue;

                            modifiedDietz.AddModDietzEntity(netValue, clientManualAssetEntity.TransactionDate, "Manual", perfReportDS, startTransactionDate, endTransactionDate);
                            modifiedDietzMAN.AddModDietzEntity(netValue, clientManualAssetEntity.TransactionDate, "Manual", perfReportDS, startTransactionDate, endTransactionDate);
                        }

                        if (clientManualAssetEntity.TransactionType == "Transfer Out")
                        {
                            netValue = -1 * (clientManualAssetEntity.UnitsOnHand * Convert.ToDecimal(systemManualAssetPriceHistory.UnitPrice));
                            capitalMovementByCategoryTotal.TransferInOutTotal += netValue;
                            modifiedDietzMAN.CapitalMovementByCategoryTotal.TransferInOutTotal += netValue;

                            modifiedDietz.AddModDietzEntity(netValue, clientManualAssetEntity.TransactionDate, "Manual", perfReportDS, startTransactionDate, endTransactionDate);
                            modifiedDietzMAN.AddModDietzEntity(netValue, clientManualAssetEntity.TransactionDate, "Manual", perfReportDS, startTransactionDate, endTransactionDate);
                        }

                        if (clientManualAssetEntity.TransactionType == "Application")
                        {
                            if (systemManualAssetPriceHistory.PurValue != 0)
                                netValue = clientManualAssetEntity.UnitsOnHand * Convert.ToDecimal(systemManualAssetPriceHistory.PurValue);
                            else
                                netValue = clientManualAssetEntity.UnitsOnHand * Convert.ToDecimal(systemManualAssetPriceHistory.UnitPrice);

                            capitalMovementByCategoryTotal.Application += netValue;
                            modifiedDietzMAN.CapitalMovementByCategoryTotal.Application += netValue;

                            modifiedDietz.AddModDietzEntity(netValue, clientManualAssetEntity.TransactionDate, "Manual", perfReportDS, startTransactionDate, endTransactionDate);
                            modifiedDietzMAN.AddModDietzEntity(netValue, clientManualAssetEntity.TransactionDate, "Manual", perfReportDS, startTransactionDate, endTransactionDate);
                        }

                        if (clientManualAssetEntity.TransactionType == "Redemption")
                        {
                            netValue = -1 * (clientManualAssetEntity.UnitsOnHand * Convert.ToDecimal(systemManualAssetPriceHistory.UnitPrice));
                            capitalMovementByCategoryTotal.Redemption += netValue;
                            modifiedDietzMAN.CapitalMovementByCategoryTotal.Redemption += netValue;

                            modifiedDietz.AddModDietzEntity(netValue, clientManualAssetEntity.TransactionDate, "Manual", perfReportDS, startTransactionDate, endTransactionDate);
                            modifiedDietzMAN.AddModDietzEntity(netValue, clientManualAssetEntity.TransactionDate, "Manual", perfReportDS, startTransactionDate, endTransactionDate);
                        }


                    }
                }
            }
        }
    }

    public class CapitalMovementByCategoryTotal
    {
        public string Type = String.Empty;
        public decimal OpeningBalTotal = 0;
        public decimal IncomeTotal = 0;
        public decimal OtherIncome = 0;
        public decimal Application = 0;
        public decimal Redemption = 0;
        public decimal DividendDistribution = 0;
        public decimal ApplicationRedTotal
        {
            get
            {
                return Application + Redemption;
            }
        }
        public decimal TransferInOutTotal = 0;
        public decimal ExpenseTotal = 0;
        public decimal TaxInOutTotal = 0;
        public decimal ClosingBalanceTotal = 0;
        public decimal InternalCashMovementTotal = 0;
        private decimal changeInInvestTotal = 0;

        public decimal MovementExcludingInvestment
        {
            get
            {

                return TransferInOutTotal + ExpenseTotal + TaxInOutTotal + InternalCashMovementTotal + IncomeTotal;

            }
        }

        public decimal ChangeInInvestTotal
        {
            get
            {
                if (this.Type != "CASH" && this.Type != "TDs")
                    changeInInvestTotal = ClosingBalanceTotal - (OpeningBalTotal + IncomeTotal + ApplicationRedTotal + TransferInOutTotal + ExpenseTotal + TaxInOutTotal + InternalCashMovementTotal);

                return changeInInvestTotal;
            }
            set
            {
                changeInInvestTotal = value;
            }
        }
    }

    public class ModifiedDietz
    {
        CapitalMovementByCategoryTotal capitalMovementByCategoryTotal = new CapitalMovementByCategoryTotal();
        public DateTime EndDateIfClosingZero = DateTime.Now;
        public DateTime StartDateIfOpeningZero = DateTime.Now;

        public CapitalMovementByCategoryTotal CapitalMovementByCategoryTotal
        {
            get { return capitalMovementByCategoryTotal; }
            set { capitalMovementByCategoryTotal = value; }
        }

        DateTime month;
        ObservableCollection<ModifiedDietzTransactionEntity> modifiedDietzTransactionEntityCol = new ObservableCollection<ModifiedDietzTransactionEntity>();
        public decimal OpeningBalTotal;
        public decimal ClosingBalanceTotal;
        public decimal IncomeAccrual;
        public decimal Income;
        public decimal Movement;
        public decimal ChangeInMktValue;
        public decimal IncomeReturn;
        public decimal ChangeInMktValueReturn;
        public decimal OverallReturn;
        decimal cfTotal;
        public decimal CfTotalYear;
        public decimal ModDietz;
        public decimal ModDietzIncludingIncome;
        decimal chainLinkData;
        decimal chainLinkDataIncome;
        public string Key = string.Empty;
        public string Type = String.Empty;

        public decimal ChainLinkData
        {
            get
            {
                return chainLinkData;
            }
            set
            {
                chainLinkData = value;
            }
        }

        public decimal ChainLinkDataIncome
        {
            get
            {
                return chainLinkDataIncome;
            }
            set
            {
                chainLinkDataIncome = value;
            }
        }

        public decimal TotalIncome
        {
            get
            {
                return Income + IncomeAccrual;
            }
        }

        public ModifiedDietzTransactionEntity AddEntity(decimal netValue, DateTime transactionDate, string type)
        {
            ModifiedDietzTransactionEntity entity = new ModifiedDietzTransactionEntity();
            entity.NetValue = netValue;
            entity.TransactionDate = transactionDate;
            entity.Type = type;
            this.ModifiedDietzTransactionEntityCol.Add(entity);
            return entity;
        }

        public void AddModDietzEntity(decimal netValue, DateTime transactionDate, string type, PerfReportDS perfReportDS)
        {
            DateTime startDate = perfReportDS.StartDate;
            DateTime endDate = perfReportDS.EndDate;

            if (!perfReportDS.IsRange)
            {

                startDate = AccountingFinancialYear.FirstDayOfMonthFromDateTime(Month);
                endDate = AccountingFinancialYear.LastDayOfMonthFromDateTime(Month);
            }
            if (this.OpeningBalTotal == 0)
                startDate = StartDateIfOpeningZero;
            if (this.ClosingBalanceTotal == 0)
                endDate = EndDateIfClosingZero;

            ModifiedDietzTransactionEntity entity = AddEntity(netValue, transactionDate, type);
            entity.CalculatedModifiedDietzTransactionEntityRange(startDate, endDate);
        }

        public void AddModDietzEntity(decimal netValue, DateTime transactionDate, string type, PerfReportDS perfReportDS, DateTime startDateIfOpeningZero, DateTime endDateIfOpeningZero)
        {
            DateTime startDate = perfReportDS.StartDate;
            DateTime endDate = perfReportDS.EndDate;
            if (!perfReportDS.IsRange)
            {
                startDate = AccountingFinancialYear.FirstDayOfMonthFromDateTime(Month);
                endDate = AccountingFinancialYear.LastDayOfMonthFromDateTime(Month);
            }
            if (this.OpeningBalTotal == 0)
                startDate = startDateIfOpeningZero;
            if (this.ClosingBalanceTotal == 0)
                endDate = endDateIfOpeningZero;

            ModifiedDietzTransactionEntity entity = AddEntity(netValue, transactionDate, type);
            entity.CalculatedModifiedDietzTransactionEntityRange(startDate, endDate);
        }

        public DateTime Month
        {
            get
            {
                return month;
            }
            set
            {
                this.month = value;
            }
        }

        public decimal WeightedCfTotal
        {
            get
            {
                return modifiedDietzTransactionEntityCol.Sum(dietz => dietz.WeightedCf);
            }
        }

        public decimal CfTotal
        {
            get
            {
                this.cfTotal = modifiedDietzTransactionEntityCol.Sum(dietz => dietz.NetValue);
                return cfTotal;
            }
            set
            {
                this.cfTotal = value;
            }
        }

        public decimal CfTotalByRange(DateTime startDate, DateTime endDate)
        {
            decimal cfTotalValue = modifiedDietzTransactionEntityCol.Where(mod => mod.TransactionDate <= startDate && mod.TransactionDate >= endDate).Sum(dietz => dietz.NetValue);
            return cfTotalValue;
        }

        public ObservableCollection<ModifiedDietzTransactionEntity> ModifiedDietzTransactionEntityCol
        {
            get
            {
                return modifiedDietzTransactionEntityCol;
            }
            set
            {
                modifiedDietzTransactionEntityCol = value;
            }
        }

        public void CaculateIncomeGrowthReturns()
        {
            this.CalculateChangeInMktValueReturn();
            this.CalculateOverallReturn();
            this.CalculatedIncomeReturn();
        }

        public void CaculateIncomeGrowthReturnsYear()
        {
            this.CalculateChangeInMktValueReturnYear();
            this.CalculateOverallReturnYear();
            this.CalculatedIncomeReturnYear();
        }

        public void CalculatedDietz()
        {
            try
            {
                decimal value = (this.OpeningBalTotal + this.WeightedCfTotal);

                if (value == 0)
                    this.ModDietz = 0;
                else
                {
                    if (this.Type == "PRICE" || this.Type == "GROWTH")
                        this.ModDietz = this.ChangeInMktValue / value;
                    else if (this.Type != "CASH" && this.Type != "TDs")
                        this.ModDietz = (this.ClosingBalanceTotal - OpeningBalTotal - CfTotal) / (this.OpeningBalTotal + this.WeightedCfTotal);

                    this.ModDietzIncludingIncome = (this.ChangeInMktValue + TotalIncome) / value;
                }

            }
            catch { this.ModDietz = 0; this.ModDietzIncludingIncome = 0; }
            finally { }
        }

        public void CalculateOverallReturn()
        {
            try
            {
                this.OverallReturn = (this.TotalIncome + this.ChangeInMktValue) / (this.OpeningBalTotal + this.WeightedCfTotal);
            }
            catch { this.ChangeInMktValueReturn = 0; }
            finally { }
        }

        public void CalculateOverallReturnYear()
        {
            try
            {
                this.OverallReturn = (this.TotalIncome + this.ChangeInMktValue) / (this.OpeningBalTotal + this.WeightedCfTotal);
            }
            catch { this.ChangeInMktValueReturn = 0; }
            finally { }
        }

        public void CalculatedIncomeReturn()
        {
            try
            {
                this.IncomeReturn = this.TotalIncome / (this.OpeningBalTotal + this.WeightedCfTotal);
            }
            catch { this.IncomeReturn = 0; }
            finally { }
        }

        public void CalculatedIncomeReturnYear()
        {
            try
            {
                this.IncomeReturn = this.TotalIncome / (this.OpeningBalTotal + this.WeightedCfTotal);
            }
            catch { this.IncomeReturn = 0; }
            finally { }
        }

        public void CalculateChangeInMktValueReturn()
        {
            try
            {
                this.ChangeInMktValueReturn = this.ChangeInMktValue / (this.OpeningBalTotal + (this.CfTotal - this.TotalIncome));
            }
            catch { this.ChangeInMktValueReturn = 0; }
            finally { }
        }

        public void CalculateChangeInMktValueReturnYear()
        {
            try
            {
                decimal value = (this.OpeningBalTotal + (this.CfTotalYear - this.TotalIncome));
                if (value == 0)
                    ChangeInMktValueReturn = 0;
                else
                    this.ChangeInMktValueReturn = this.ChangeInMktValue / value;
            }
            catch { this.ChangeInMktValueReturn = 0; }
            finally { }
        }

        public void CalculatedChainLink(decimal previousMontModDietz)
        {
            try
            {
                this.chainLinkData = (previousMontModDietz) * (1 + (this.ModDietz)) - 1;
            }
            catch { this.chainLinkData = 0; }
            finally { }
        }

        public void CalculatedChainLinkIncIncome(decimal previousMontModDietzIncIncome)
        {
            try
            {
                this.chainLinkDataIncome = (previousMontModDietzIncIncome) * (1 + (this.ModDietzIncludingIncome)) - 1;
            }
            catch { this.chainLinkData = 0; }
            finally { }
        }

        public string GetChainLinkValue()
        {
            if (OpeningBalTotal == 0 && ClosingBalanceTotal == 0)
                return "-";
            else
                return this.chainLinkData.ToString("P");
        }

        public string GetChainLinkValueIncone()
        {
            if (OpeningBalTotal == 0 && ClosingBalanceTotal == 0)
                return "-";
            else
                return this.chainLinkDataIncome.ToString("P");
        }

        public string GetChainLinkValueDiff()
        {
            if (OpeningBalTotal == 0 && ClosingBalanceTotal == 0)
                return "-";
            else
                return (this.chainLinkDataIncome - this.chainLinkData).ToString("P");
        }

        public static void CalculateChainLinksForModifiedDietzCollection(ObservableCollection<ModifiedDietz> modifiedDietzCol)
        {
            var sortedmodifiedDietzCol = modifiedDietzCol.OrderByDescending(dietz => dietz.Month);

            foreach (ModifiedDietz modifiedDietz in sortedmodifiedDietzCol)
            {
                DateTime previousMonth = modifiedDietz.Month.AddMonths(1);
                var previousMonthModifiedDietz = sortedmodifiedDietzCol.Where(dietz => dietz.Month == previousMonth).FirstOrDefault();

                if (previousMonthModifiedDietz == null)
                    modifiedDietz.ChainLinkData = modifiedDietz.ModDietz;
                else
                    modifiedDietz.CalculatedChainLink(1 + (previousMonthModifiedDietz.ChainLinkData));
            }

            var sortedmodifiedDietzColByIncome = modifiedDietzCol.OrderByDescending(dietz => dietz.Month);

            foreach (ModifiedDietz modifiedDietz in sortedmodifiedDietzColByIncome)
            {
                DateTime previousMonth = modifiedDietz.Month.AddMonths(1);
                var previousMonthModifiedDietz = sortedmodifiedDietzCol.Where(dietz => dietz.Month == previousMonth).FirstOrDefault();

                if (previousMonthModifiedDietz == null)
                    modifiedDietz.ChainLinkDataIncome = modifiedDietz.ModDietzIncludingIncome;
                else
                    modifiedDietz.CalculatedChainLinkIncIncome(1 + (previousMonthModifiedDietz.ChainLinkDataIncome));
            }
        }

        /// <summary>
        /// Set opening balance as closing balance of the previous month
        /// If no previous month dietz, set opening balance as value of the first transaction
        /// </summary>
        /// <param name="clx"></param>
        public static void SetChainLinksOpeningBalance(ObservableCollection<ModifiedDietz> clx)
        {
            var sortedClx = clx.OrderByDescending(dietz => dietz.Month);

            foreach (ModifiedDietz md in sortedClx)
            {
                DateTime previousMonth = md.Month.AddMonths(-1);
                var previousMonthModifiedDietz = sortedClx.Where(dietz => dietz.Month == previousMonth).FirstOrDefault();

                if (previousMonthModifiedDietz == null)
                {
                    var firtTranscation = md.modifiedDietzTransactionEntityCol.OrderBy(dietz => dietz.TransactionDate).FirstOrDefault();
                    if (firtTranscation != null)
                        md.OpeningBalTotal = firtTranscation.NetValue;
                    else
                        md.OpeningBalTotal = 0;
                }
                else
                    md.OpeningBalTotal = previousMonthModifiedDietz.ClosingBalanceTotal;
                md.CapitalMovementByCategoryTotal.OpeningBalTotal = md.OpeningBalTotal;
                md.CapitalMovementByCategoryTotal.ClosingBalanceTotal = md.ClosingBalanceTotal;
                md.ChangeInMktValue = md.CapitalMovementByCategoryTotal.ChangeInInvestTotal = md.ClosingBalanceTotal - md.OpeningBalTotal;
                md.CalculatedDietz();
                md.CaculateIncomeGrowthReturns();
            }
        }

    }

    public class ModifiedDietzTransactionEntity
    {
        DateTime transactionDate;
        decimal weightedCf;
        decimal netValue;

        public DateTime TransactionDate
        {
            get
            {
                return transactionDate;
            }
            set
            {
                this.transactionDate = value;
            }

        }

        public decimal NetValue
        {
            get
            {
                return netValue;
            }
            set
            {
                this.netValue = value;
            }
        }


        public string Type;

        public decimal WeightedCf
        {
            get
            {
                return weightedCf;
            }
        }

        public void CalculatedModifiedDietzTransactionEntity()
        {
            int totalDaysInMonth = DateTime.DaysInMonth(transactionDate.Year, transactionDate.Month);
            int daysRemaining = totalDaysInMonth - transactionDate.Day;
            this.weightedCf = (Convert.ToDecimal(daysRemaining) / (Convert.ToDecimal(totalDaysInMonth)) * netValue);
        }

        public void CalculatedModifiedDietzTransactionEntityRange(DateTime startDate, DateTime endDate)
        {
            TimeSpan timeSpan = endDate.Date - startDate.Date;

            double totalDaysInRange = 0;

            if (startDate.Date.Day != 1)
                totalDaysInRange = timeSpan.TotalDays;
            else
                totalDaysInRange = timeSpan.TotalDays + 1;

            TimeSpan timeSpanDaysRemaining = endDate.Date - transactionDate.Date;
            double daysRemaining = timeSpanDaysRemaining.TotalDays;

            if (totalDaysInRange != 0)
                this.weightedCf = (Convert.ToDecimal(daysRemaining) / (Convert.ToDecimal(totalDaysInRange)) * netValue);
            else
                weightedCf = 0;
        }
    }

}