﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.CM.Entity;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Enum;
using Oritax.TaxSimp.Extensions;
using IdentityCMDetail = Oritax.TaxSimp.Common.IdentityCMDetail;


namespace Oritax.TaxSimp.CM
{
    public class BankAccountUtilities
    {
        public static void GetBankAccountDs(string clientId, BankAccountDS dataSet, List<IdentityCMDetail> bankAccounts, ICMBroker broker, List<AccountProcessTaskEntity> listAccountProcess)
        {
            if (bankAccounts != null && bankAccounts.Count > 0)
            {
                var models = GetModels(broker);

                foreach (var bank in bankAccounts)
                {
                    var entity = broker.GetCMImplementation(bank.Clid, bank.Csid);
                    if (entity != null)
                        entity.GetData(dataSet);
                }
                var ds = SettledUnsettledUtilities.GetAllUnsetteledForClient(clientId, broker);
                foreach (DataRow row in dataSet.BankAccountsTable.Rows)
                {
                    decimal unsettledBuy;
                    decimal unsettledSell;
                    SettledUnsettledUtilities.GetBankUnsettledAmount(ds, Guid.Parse(row[dataSet.BankAccountsTable.CID].ToString()), row[dataSet.BankAccountsTable.ACCOUNTNO].ToString(), row[dataSet.BankAccountsTable.BSBNO].ToString(), out unsettledBuy, out unsettledSell);
                    row[dataSet.BankAccountsTable.UNSETTLEDBUY] = unsettledBuy;
                    row[dataSet.BankAccountsTable.UNSETTLEDSELL] = unsettledSell;
                    //Getting Service Types of Account
                    var bankAcc = new IdentityCMDetail { Clid = (Guid)row[dataSet.BankAccountsTable.CLID], Csid = (Guid)row[dataSet.BankAccountsTable.CSID] };
                    var serviceTypes = GetAccountServiceTypes(listAccountProcess, bankAcc, models);
                    row[dataSet.BankAccountsTable.SERVICETYPES] = serviceTypes;
                }
            }
        }

        public static void GetASXAccountDs(DesktopBrokerAccountDS dataSet, List<IdentityCMDetail> asxAccounts, string clientId,string clientName,Guid clientCID,List<AccountProcessTaskEntity> accountProcess , ICMBroker broker)
        {
            if (asxAccounts != null && asxAccounts.Count > 0)
            {
                var org=broker.GetWellKnownCM(WellKnownCM.Organization) as IOrganization;
                var models = org.Model;
                broker.ReleaseBrokerManagedComponent(org);
                foreach (var cmDetail in asxAccounts)
                {
                    IBrokerManagedComponent bmc = null;
                    
                    if (cmDetail.Cid != Guid.Empty)
                    {
                         bmc = broker.GetBMCInstance(cmDetail.Cid);
                       
                    }
                    else if (cmDetail.Clid != Guid.Empty && cmDetail.Csid != Guid.Empty)
                    {
                         bmc = broker.GetCMImplementation(cmDetail.Clid, cmDetail.Csid);
                    }
                    if (bmc != null)
                    {
                    bmc.GetData(dataSet);
                    var serviceTypes = GetAccountServiceTypes(accountProcess, cmDetail, models);
                    var row=dataSet.DesktopBrokerAccountsTable.Rows[dataSet.DesktopBrokerAccountsTable.Rows.Count-1];
                    row[dataSet.DesktopBrokerAccountsTable.SERVICETYPE] = serviceTypes;
                    row[dataSet.DesktopBrokerAccountsTable.CLIENTCID] = clientCID;
                    row[dataSet.DesktopBrokerAccountsTable.CLIENTCODE] = clientId;
                    row[dataSet.DesktopBrokerAccountsTable.CLIENTNAME] = clientName;
                    broker.ReleaseBrokerManagedComponent(bmc);
                    }
                }
            }

        }

        public static void GetASXAccountDs(ImportProcessDS dataSet, List<IdentityCMDetail> asxAccounts, string clientId, Guid clientCID, ICMBroker broker)
        {

            if (asxAccounts != null && asxAccounts.Count > 0)
            {
                foreach (var asx in asxAccounts)
                {
                    if (asx.Clid != Guid.Empty || asx.Csid != Guid.Empty)
                    {
                        var entity = broker.GetCMImplementation(asx.Clid, asx.Csid);
                        if (entity != null)
                        {
                            string accountNumber = entity.GetDataStream((int)CmCommand.AccountNumber, "");
                            DataRow dr = dataSet.Tables["ClientASXAccounts"].NewRow();
                            dr["ClientID"] = clientId;
                            dr["AccountNumber"] = accountNumber;
                            dr["ASXClid"] = asx.Clid;
                            dr["ASXCsid"] = asx.Csid;
                            dr["ClientCID"] = clientCID;


                            dataSet.Tables["ClientAsxAccounts"].Rows.Add(dr);



                        }
                    }
                }
            }

        }


        public static void GetTermDepositAccountDs(TermDepositDS dataSet, List<Common.IdentityCMDetail> TermDepositAccounts,
                                                   ICMBroker broker)
        {
            if (TermDepositAccounts != null && TermDepositAccounts.Count > 0)
            {
                foreach (var each in TermDepositAccounts)
                {
                    var entity = broker.GetCMImplementation(each.Clid, each.Csid);
                    entity.GetData(dataSet);
                }

            }
        }

        public static void ImportAtCallTransactions(string clientID, string clientName, Guid Cid, DataSet ds, List<IdentityCMDetail> BankAccountList, List<AccountProcessTaskEntity> AccountProcess, ICMBroker Broker)
        {
            var orgCM = Broker.GetWellKnownCM(WellKnownCM.Organization) as IOrganization;
            if (BankAccountList != null)
            {
                foreach (var bank in BankAccountList)
                {
                    var entity = Broker.GetCMImplementation(bank.Clid, bank.Csid) as BankAccountCM;
                    if (entity != null && entity.BankAccountEntity.AccoutType.ToLower() == "termdeposit")
                    {
                        DataRow[] drs = ds.Tables["AtCallTransactions"].Select("CustomerNumber = '" + entity.BankAccountEntity.AccountNumber + "'");

                        if (drs.Length > 0)
                        {
                            foreach (DataRow dr in drs)
                            {
                                dr["Message"] = "";
                                dr["HasErrors"] = false;
                                var Product = AccountProcess.FirstOrDefault(ss => ss.LinkedEntityType == OrganizationType.TermDepositAccount && ss.LinkedEntity.Clid == bank.Clid && ss.LinkedEntity.Csid == bank.Csid);
                                if (Product != null)
                                {
                                    dr["ProductID"] = Product.TaskID;
                                    if (entity.BankAccountEntity.BrokerID != null)
                                    {
                                        var inst = orgCM.Institution.Where(ss => ss.Name.ToLower() == entity.BankAccountEntity.BrokerID.Name.ToLower()).FirstOrDefault();
                                        if (inst != null)
                                        {
                                            dr["BrokerName"] = inst.Name;
                                        }
                                    }
                                }
                            }

                            if (!ds.ExtendedProperties.Contains("ClientID"))
                            {
                                ds.ExtendedProperties.Add("ClientID", clientID);
                            }
                            else
                            {
                                ds.ExtendedProperties["ClientID"] = clientID;
                            }

                            if (!ds.ExtendedProperties.Contains("ClientCID"))
                            {
                                ds.ExtendedProperties.Add("ClientCID", Cid);
                            }
                            else
                            {
                                ds.ExtendedProperties["ClientCID"] = Cid;
                            }
                            if (!ds.ExtendedProperties.Contains("ClientName"))
                            {
                                ds.ExtendedProperties.Add("ClientName", clientName);
                            }
                            else
                            {
                                ds.ExtendedProperties["ClientName"] = clientName;
                            }

                            entity.SetData(ds);
                        }
                    }
                }
            }
            Broker.ReleaseBrokerManagedComponent(orgCM);

        }

        public static void SetDividendsTransactionsID(ImportProcessDS ds, List<Common.IdentityCMDetail> bankAccounts,
                                                      ObservableCollection<DividendEntity> dividendCollection,
                                                      IBrokerManagedComponent unit)
        {
            if (bankAccounts != null && dividendCollection != null)
                foreach (var bankacc in bankAccounts)
                {
                    var temp = (BankAccountCM)unit.Broker.GetCMImplementation(bankacc.Clid, bankacc.Csid);
                    if (temp != null)
                    {
                        string accountNumber = temp.GetDataStream((int)CmCommand.AccountNumber, String.Empty);
                        string bsb = temp.GetDataStream((int)CmCommand.BSB, String.Empty);
                        if (!string.IsNullOrEmpty(bsb))
                        {
                            if (bsb.Contains('-'))
                            {
                                bsb = bsb.Split('-')[1];
                            }
                            else if (bsb.Length > 5) //length should be sixe
                            {
                                bsb = bsb.Substring(3, 5);
                            }
                        }
                        else
                        {
                            bsb = "bsbNotFound";
                        }
                        if (ds.Tables["CashTransctions"] != null)
                        {
                            string BsBNAccountNumber = bsb + accountNumber;
                            var drs =
                                ds.Tables["CashTransctions"].Select(String.Format("BSBNAccountNumber='{0}'",
                                                                                  BsBNAccountNumber));
                            if (drs.Length > 0)
                            {
                                DateTimeFormatInfo info = new DateTimeFormatInfo();
                                info.ShortDatePattern = "dd/MM/yyyy";
                                foreach (DataRow dr in drs)
                                {
                                    if (!Boolean.Parse(dr["HasErrors"].ToString()))
                                    {
                                        bool modify = true;
                                        DateTime dateofTrans;
                                        if (
                                            !DateTime.TryParse(dr["DateOfTransaction"].ToString(), info,
                                                               DateTimeStyles.None, out dateofTrans))
                                        {
                                            modify = false;
                                        }

                                        double amount;
                                        if (!double.TryParse(dr["Amount"].ToString(), out amount))
                                        {
                                            modify = false;
                                        }
                                        if (modify)
                                        {
                                            var tranEntity =
                                                temp.BankAccountEntity.CashManagementTransactions.FirstOrDefault(
                                                    ss =>
                                                    ss.ExternalReferenceID == dr["BankwestReferenceNumber"].ToString() &&
                                                    ss.SystemTransactionType == "Dividend");
                                            if (tranEntity != null)
                                            {
                                                var oldDV =
                                                    dividendCollection.FirstOrDefault(
                                                        ss =>
                                                        ss.CashManagementTransactionID == tranEntity.ExternalReferenceID);
                                                if (oldDV != null)
                                                {
                                                    oldDV.Status = DividendStatus.Accrued;
                                                    oldDV.CashManagementTransactionID = string.Empty;
                                                }
                                                var dV =
                                                    dividendCollection.FirstOrDefault(
                                                        ee =>
                                                        ee.PaymentDate == tranEntity.TransactionDate &&
                                                        Math.Abs(tranEntity.TotalAmount - (decimal)ee.PaidDividend) < 1 &&
                                                        tranEntity.SystemTransactionType == "Dividend");
                                                if (dV != null)
                                                {
                                                    tranEntity.DividendID = dV.ID;
                                                    tranEntity.DividendStatus = Common.TransectionStatus.Matched;
                                                    dV.Status = DividendStatus.Matched;
                                                    dV.CashManagementTransactionID = tranEntity.ExternalReferenceID;
                                                }

                                            }

                                        }
                                    }
                                }
                            }

                        }


                    }

                    //unit.Broker.ReleaseBrokerManagedComponent(temp);
                }
        }

        public static void SetClientIdInAccountAtCall(ImportProcessDS ds, List<Common.IdentityCMDetail> bankAccounts,
                                                      List<AccountProcessTaskEntity> accprocess,
                                                      string clientId, ICMBroker broker)
        {
            var borkerId = new Guid();
            var brokerName = string.Empty;
            var org = broker.GetWellKnownCM(WellKnownCM.Organization) as IOrganization;
            if (bankAccounts != null && accprocess != null)

                foreach (var bankacc in bankAccounts)
                {
                    var temp = (BankAccountCM)broker.GetCMImplementation(bankacc.Clid, bankacc.Csid);
                    //Get the BrokerId which is Institute ID

                    if (temp != null && org != null)
                    {
                        var instituteId = temp.BankAccountEntity.BrokerID;
                        if (instituteId != null)
                        {
                            var inst =
                                org.Institution.FirstOrDefault(ss => ss.Name.ToLower() == instituteId.Name.ToLower());
                            if (inst != null) borkerId = (inst.ID);
                            if (inst != null) brokerName = inst.Name;
                        }
                        string accountNumber = temp.GetDataStream((int)CmCommand.AccountNumber, string.Empty);

                        if (ds.Tables["AtCallAmm"] != null)
                        {

                            var drs = ds.Tables["AtCallAmm"].Select(string.Format("AccountNumber='{0}'", accountNumber));
                            var task = accprocess.FirstOrDefault(
                                    ss =>
                                    ss.LinkedEntity.Clid == bankacc.Clid && ss.LinkedEntity.Csid == bankacc.Csid &&
                                    ss.LinkedEntityType == OrganizationType.TermDepositAccount);
                            if (drs.Length > 0 && task != null)
                            {
                                foreach (var dataRow in drs)
                                {
                                    dataRow["ClientID"] = clientId;
                                    dataRow["InstituteID"] = borkerId;
                                    dataRow["ProductID"] = task.TaskID;
                                    var model = org.Model.FirstOrDefault(ss => ss.ID == task.ModelID);
                                    if (model != null) dataRow["ServiceType"] = model.ServiceType;
                                    dataRow["BrokerName"] = brokerName;
                                }
                            }
                        }
                    }
                }
        }


        public static void SetClientIdInAccountTdTransaction(ImportProcessDS ds, List<Common.IdentityCMDetail> bankAccounts, List<AccountProcessTaskEntity> accprocess, string clientId, ICMBroker broker)
        {
            var borkerId = new Guid();
            var brokerName = string.Empty;
            var org = broker.GetWellKnownCM(WellKnownCM.Organization) as IOrganization;
            if (bankAccounts != null && accprocess != null)
                foreach (var bankacc in bankAccounts)
                {
                    var temp = (BankAccountCM)broker.GetCMImplementation(bankacc.Clid, bankacc.Csid);
                    if (temp != null && org != null)
                    {
                        string accountNumber = temp.GetDataStream((int)CmCommand.AccountNumber, string.Empty);
                        var instituteId = temp.BankAccountEntity.BrokerID;
                        if(instituteId != null)
                        {
                            var inst = org.Institution.FirstOrDefault(ss => ss.Name.ToLower() == instituteId.Name.ToLower());
                            if (inst != null) borkerId = (inst.ID);
                            if (inst != null) brokerName = inst.Name;
                        }
                       
                        if (ds.Tables["AtTDTransaction"] != null)
                        {

                            var drs = ds.Tables["AtTDTransaction"].Select(string.Format("AccountNumber='{0}'", accountNumber));
                            var task =
                                accprocess.FirstOrDefault(
                                    ss =>
                                    ss.LinkedEntity.Clid == bankacc.Clid && ss.LinkedEntity.Csid == bankacc.Csid &&
                                    ss.LinkedEntityType == OrganizationType.TermDepositAccount);
                            if (drs.Length > 0 && task != null)
                            {
                                foreach (var dataRow in drs)
                                {
                                    dataRow["ClientID"] = clientId;
                                    dataRow["InstituteID"] = borkerId;
                                    dataRow["ProductID"] = task.TaskID;
                                    var model = org.Model.FirstOrDefault(ss => ss.ID == task.ModelID);
                                    if (model != null) dataRow["ServiceType"] = model.ServiceType;
                                    dataRow["BrokerName"] = brokerName;
                                }
                            }
                        }
                    }
                }
        }

        public static void SetClientIdInAccounts(ImportProcessDS ds, List<Common.IdentityCMDetail> bankAccounts, List<AccountProcessTaskEntity> accprocess,
                                                 string clientId, ICMBroker broker)
        {
            if (bankAccounts != null && accprocess != null)
                foreach (var bankacc in bankAccounts)
                {
                    var temp = (BankAccountCM)broker.GetCMImplementation(bankacc.Clid, bankacc.Csid);
                    if (temp != null)
                    {
                        string accountNumber = temp.GetDataStream((int)CmCommand.AccountNumber, String.Empty);
                        string bsb = temp.GetDataStream((int)CmCommand.BSB, String.Empty);
                        if (!string.IsNullOrEmpty(bsb))
                        {
                            if (bsb.Contains('-'))
                            {
                                bsb = bsb.Split('-')[1];
                            }
                            else if (bsb.Length > 5) //length should be sixe
                            {
                                bsb = bsb.Substring(3, 5);
                            }
                        }
                        else
                        {
                            bsb = "bsbNotFound";
                        }
                        if (ds.Tables["CashTransctions"] != null)
                        {
                            string BsBNAccountNumber = bsb + accountNumber;
                            var drs = ds.Tables["CashTransctions"].Select(String.Format("BSBNAccountNumber='{0}'", BsBNAccountNumber));

                            if (drs.Length > 0 && accprocess.Count(ss => ss.LinkedEntity.Clid == bankacc.Clid && ss.LinkedEntity.Csid == bankacc.Csid && ss.LinkedEntityType == OrganizationType.BankAccount) > 0)
                            {
                                foreach (var dataRow in drs)
                                {
                                    dataRow["ClientID"] = clientId;
                                }
                            }
                        }
                    }
                }
        }

        public static void GetBankAccountHoldings(Guid cid, string clientId, ClientIndividualEntity entity, ClientsBankAccountsHoldingsDS dsHolding, ICMBroker broker)
        {
            var models = GetModels(broker);
            GetClientHolding(cid, clientId, entity.ClientId, entity.Name, GetServiceTypes(entity.Servicetype), entity.ListAccountProcess, entity.BankAccounts, dsHolding, models, broker);
        }
        public static void GetBankAccountHoldings(Guid cid, string clientId, CorporateEntity entity, ClientsBankAccountsHoldingsDS dsHolding, ICMBroker broker)
        {
            var models = GetModels(broker);
            GetClientHolding(cid, clientId, entity.ClientId, entity.Name, GetServiceTypes(entity.Servicetype), entity.ListAccountProcess, entity.BankAccounts, dsHolding, models, broker);
        }

        private static List<ModelEntity> GetModels(ICMBroker broker)
        {
            var org = broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            var models = org.Model;
            broker.ReleaseBrokerManagedComponent(org);
            return models;

        }

        private static void GetClientHolding(Guid cid, string clientId, string entityClientId, string name, string serviceTypes, List<AccountProcessTaskEntity> listAccountProcess, IEnumerable<IdentityCMDetail> bankAccounts, ClientsBankAccountsHoldingsDS dsHolding, List<ModelEntity> models, ICMBroker broker)
        {

            foreach (var acc in bankAccounts)
            {
                var bankCm = broker.GetCMImplementation(acc.Clid, acc.Csid) as BankAccountCM;
                if (bankCm != null)
                {
                    decimal holding = bankCm.BankAccountEntity.Holding;
                    decimal transHolding = 0;
                    if (bankCm.BankAccountEntity.CashManagementTransactions != null)
                        transHolding = bankCm.BankAccountEntity.CashManagementTransactions.Sum(c => c.TotalAmount);

                    var ds = SettledUnsettledUtilities.GetAllUnsetteledForClient(clientId, broker);
                    decimal unsettledBuy;
                    decimal unsettledSell;
                    SettledUnsettledUtilities.GetBankUnsettledAmount(ds, bankCm.CID, bankCm.BankAccountEntity.AccountNumber,
                                                                   bankCm.BankAccountEntity.BSB, out unsettledBuy,
                                                                   out unsettledSell);

                    decimal unsetteledOrder = (unsettledBuy + unsettledSell);

                    dsHolding.BankAccountHoldingTable.AddNewRow(cid, clientId, entityClientId, name, serviceTypes, bankCm.BankAccountEntity.Name, bankCm.BankAccountEntity.AccoutType.ToUpper(), GetAccountServiceTypes(listAccountProcess, acc, models), bankCm.BankAccountEntity.AccountNumber, bankCm.BankAccountEntity.BSB, holding, transHolding, unsetteledOrder);
                    broker.ReleaseBrokerManagedComponent(bankCm);
                }
            }


        }
        private static string GetAccountServiceTypes(List<AccountProcessTaskEntity> listAccountProcess, IdentityCMDetail acc, List<ModelEntity> models)
        {
            string result = string.Empty;

            var accountprocessTasks = listAccountProcess.Where(accpr => accpr.LinkedEntity != null && accpr.LinkedEntity.Clid == acc.Clid && accpr.LinkedEntity.Csid == acc.Csid);
            foreach (var accountProcessTaskEntity in accountprocessTasks)
            {
                var model = models.FirstOrDefault(mod => mod.ID == accountProcessTaskEntity.ModelID);
                if (model != null)
                {
                    var servtype = GetAbberviationOfServiceType(model.ServiceType);
                    result += result.Contains(servtype) ? string.Empty : servtype + ",";
                }
            }
            return result.RemoveLast(",");
        }
        private static string GetAbberviationOfServiceType(ServiceTypes serviceTypes)
        {
            string result = "";

            switch (serviceTypes)
            {
                case ServiceTypes.DoItYourSelf:
                    result = "DIY";
                    break;
                case ServiceTypes.DoItForMe:
                    result = "DIFM";
                    break;
                case ServiceTypes.DoItWithMe:
                    result = "DIWM";
                    break;

            }

            return result;
        }
        public static void GetAsxHoldings(Guid cid, string clientId, ClientIndividualEntity entity, ClientsASXAccountsHoldingsDS dsHolding, ICMBroker broker)
        {
            var models = GetModels(broker);
            GetClientAsxHolding(cid, clientId, entity.ClientId, entity.Name, GetServiceTypes(entity.Servicetype), entity.DesktopBrokerAccounts, dsHolding, broker, entity.ListAccountProcess, models);
        }
        public static void GetAsxHoldings(Guid cid, string clientId, CorporateEntity entity, ClientsASXAccountsHoldingsDS dsHolding, ICMBroker broker)
        {
            var models = GetModels(broker);
            GetClientAsxHolding(cid, clientId, entity.ClientId, entity.Name, GetServiceTypes(entity.Servicetype), entity.DesktopBrokerAccounts, dsHolding, broker, entity.ListAccountProcess, models);
        }
        private static void GetClientAsxHolding(Guid cid, string clientId, string entityClientId, string name, string serviceTypes, IEnumerable<IdentityCMDetail> ASXAccounts, ClientsASXAccountsHoldingsDS dsHolding, ICMBroker broker, List<AccountProcessTaskEntity> listAccountProcess, List<ModelEntity> models)
        {

            foreach (var acc in ASXAccounts)
            {
                var accountCm = broker.GetCMImplementation(acc.Clid, acc.Csid) as DesktopBrokerAccountCM;
                if (accountCm != null)
                {
                    decimal unitHolding = 0;
                    decimal transHolding = 0;

                    if (accountCm.DesktopBrokerAccountEntity.HoldingBalances != null)
                        unitHolding += accountCm.DesktopBrokerAccountEntity.HoldingBalances.Sum(ss => ss.Actual);
                    if (accountCm.DesktopBrokerAccountEntity.holdingTransactions != null)
                        transHolding += accountCm.DesktopBrokerAccountEntity.holdingTransactions.Sum(c => c.Units);
                    var ds = SettledUnsettledUtilities.GetAllUnsetteledForClient(clientId, broker);
                    decimal unsettledBuy = 0;
                    decimal unsettledSell = 0;
                    decimal unsettledBuyunits = 0;
                    decimal unsettledSellunits = 0;
                    SettledUnsettledUtilities.GetASXUnsettledAmountNUnits(ds, accountCm.CID, accountCm.DesktopBrokerAccountEntity.AccountNumber, out unsettledBuy, out unsettledSell, out unsettledBuyunits, out unsettledSellunits);

                    decimal unsetteledOrder = (unsettledBuyunits + unsettledSellunits);

                    dsHolding.ASXHoldingTable.AddNewRow(cid, clientId, entityClientId, name, GetAccountServiceTypes(listAccountProcess, acc, models), accountCm.DesktopBrokerAccountEntity.AccountNumber, unitHolding, transHolding, unsetteledOrder);
                }
            }



        }
        private static string GetServiceTypes(Common.ServiceType servicetype)
        {
            string serviceTypes = string.Empty;
            if (servicetype != null)
            {

                if (servicetype.DO_IT_FOR_ME)
                {
                    serviceTypes += "DIFM,";

                }

                if (servicetype.DO_IT_WITH_ME)
                {
                    serviceTypes += "DIWM,";
                }
                if (servicetype.DO_IT_YOURSELF)
                {
                    serviceTypes += "DIY,";
                }
            }


            return serviceTypes.RemoveLast(",");
        }
        public static void GetMISHoldings(Guid cid, string clientId, ClientIndividualEntity entity, ClientsMISAccountsHoldingsDS dsHolding, ICMBroker broker)
        {
            var models = GetModels(broker);
            GetClientMISHolding(cid, clientId, entity.ClientId, entity.Name, GetServiceTypes(entity.Servicetype), entity.ManagedInvestmentSchemesAccounts, dsHolding, broker, entity.ListAccountProcess, models);
        }
        public static void GetMISHoldings(Guid cid, string clientId, CorporateEntity entity, ClientsMISAccountsHoldingsDS dsHolding, ICMBroker broker)
        {
            var models = GetModels(broker);
            GetClientMISHolding(cid, clientId, entity.ClientId, entity.Name, GetServiceTypes(entity.Servicetype), entity.ManagedInvestmentSchemesAccounts, dsHolding, broker, entity.ListAccountProcess, models);
        }


        private static void GetClientMISHolding(Guid cid, string clientId, string entityClientId, string name, string serviceTypes, IEnumerable<IdentityCMDetail> MISAccounts, ClientsMISAccountsHoldingsDS dsHolding, ICMBroker broker, List<AccountProcessTaskEntity> listAccountProcess, List<ModelEntity> models)
        {

            foreach (var acc in MISAccounts)
            {
                var accountCm = broker.GetCMImplementation(acc.Clid, acc.Csid) as ManagedInvestmentSchemesAccountCM;
                if (accountCm != null)
                {
                    var entity = accountCm.ManagedInvestmentSchemesAccountEntity;
                    var fundEntity = entity.FundAccounts.FirstOrDefault(fund => fund.ID == acc.FundID);

                    if (fundEntity != null)
                    {

                        decimal unitHolding = 0;
                        decimal transHolding = 0;

                        if (accountCm.ManagedInvestmentSchemesAccountEntity.FundAccounts != null)
                            unitHolding = (decimal)fundEntity.SecurityHoldings.Where(ss => ss.Account == clientId).Sum(ss => ss.TotalShares);
                        if (fundEntity.FundTransactions != null)
                            transHolding += fundEntity.FundTransactions.Where(ss => ss.ClientID == clientId).Sum(c => c.Shares);
                        var ds = SettledUnsettledUtilities.GetAllUnsetteledForClient(clientId, broker);
                        decimal unsettledBuy;
                        decimal unsettledSell;
                        decimal unsettledBuyunits;
                        decimal unsettledSellunits;
                        decimal unsettledSellByUnits;
                        decimal unsettledSellByAmount;

                        var tasks = listAccountProcess.Where(task => task.LinkedEntity.Clid == acc.Clid && task.LinkedEntity.Csid == acc.Csid);
                        Guid productID = Guid.Empty;
                        string serviceType = string.Empty;
                        foreach (var accountProcessTaskEntity in tasks)
                        {
                            var model = models.FirstOrDefault(modeltemp => modeltemp.ID == accountProcessTaskEntity.ModelID);
                            if (model != null)
                                foreach (var asset in model.Assets)
                                {
                                    foreach (var product in asset.Products)
                                    {
                                        if (product.FundAccounts.FirstOrDefault() == acc.FundID)
                                        {
                                            productID = product.ID;
                                            var servtype = GetAbberviationOfServiceType(model.ServiceType);
                                            serviceType += serviceType.Contains(servtype) ? string.Empty : servtype + ",";
                                        }
                                    }
                                }
                        }
                        SettledUnsettledUtilities.GetMISUnsettledAmountNUnits(ds, productID, fundEntity.Code, out unsettledBuy, out unsettledSell, out unsettledBuyunits, out unsettledSellunits, out unsettledSellByUnits, out unsettledSellByAmount);
                        decimal unsetteledOrder = (unsettledBuyunits + unsettledSellunits);
                        dsHolding.MisHoldingTable.AddNewRow(cid, clientId, entityClientId, name, serviceType.RemoveLast(","), fundEntity.Code, unitHolding, transHolding, unsetteledOrder);
                    }
                }
            }
        }

        public static void SetDistributionsTransactionsID(ImportProcessDS ds, List<IdentityCMDetail> bankAccounts, ObservableCollection<DistributionIncomeEntity> distributionCollection, IBrokerManagedComponent unit)
        {
            if (bankAccounts != null && distributionCollection != null)
                foreach (var bankacc in bankAccounts)
                {
                    var temp = (BankAccountCM)unit.Broker.GetCMImplementation(bankacc.Clid, bankacc.Csid);
                    if (temp != null)
                    {
                        string accountNumber = temp.GetDataStream((int)CmCommand.AccountNumber, String.Empty);
                        string bsb = temp.GetDataStream((int)CmCommand.BSB, String.Empty);
                        if (!string.IsNullOrEmpty(bsb))
                        {
                            if (bsb.Contains('-'))
                            {
                                bsb = bsb.Split('-')[1];
                            }
                            else if (bsb.Length > 5) //length should be sixe
                            {
                                bsb = bsb.Substring(3, 5);
                            }
                        }
                        else
                        {
                            bsb = "bsbNotFound";
                        }
                        if (ds.Tables["CashTransctions"] != null)
                        {
                            string BsBNAccountNumber = bsb + accountNumber;
                            var drs =
                                ds.Tables["CashTransctions"].Select(String.Format("BSBNAccountNumber='{0}'", BsBNAccountNumber));
                            if (drs.Length > 0)
                            {
                                var info = new DateTimeFormatInfo { ShortDatePattern = "dd/MM/yyyy" };
                                foreach (DataRow dr in drs)
                                {
                                    if (!Boolean.Parse(dr["HasErrors"].ToString()))
                                    {
                                        bool modify = true;
                                        DateTime dateofTrans;
                                        if (
                                            !DateTime.TryParse(dr["DateOfTransaction"].ToString(), info,
                                                               DateTimeStyles.None, out dateofTrans))
                                        {
                                            modify = false;
                                        }

                                        double amount;
                                        if (!double.TryParse(dr["Amount"].ToString(), out amount))
                                        {
                                            modify = false;
                                        }
                                        if (modify)
                                        {
                                            var tranEntity = temp.BankAccountEntity.CashManagementTransactions.FirstOrDefault(ss => ss.ExternalReferenceID == dr["BankwestReferenceNumber"].ToString() && ss.SystemTransactionType == "Distribution");
                                            if (tranEntity != null)
                                            {
                                                var oldDb = distributionCollection.FirstOrDefault(ss => ss.CashManagementTransactionID == tranEntity.ExternalReferenceID);
                                                if (oldDb != null)
                                                {
                                                    oldDb.Status = DistributionIncomeStatus.Accrued;
                                                    oldDb.CashManagementTransactionID = string.Empty;
                                                }
                                                var dB = distributionCollection.FirstOrDefault(ee => ee.PaymentDate == tranEntity.TransactionDate && Math.Abs(tranEntity.TotalAmount - (decimal)ee.NetCashDistribution) < (decimal)0.5 && tranEntity.SystemTransactionType == "Distribution");
                                                if (dB != null)
                                                {
                                                    tranEntity.DividendID = dB.ID;
                                                    tranEntity.DividendStatus = TransectionStatus.Matched;
                                                    dB.Status = DistributionIncomeStatus.Matched;
                                                    dB.CashManagementTransactionID = tranEntity.ExternalReferenceID;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    //unit.Broker.ReleaseBrokerManagedComponent(temp);
                }
        }

        public static void SetDividendsDistributionsTransactionsID(ImportProcessDS ds, List<IdentityCMDetail> bankAccounts, ObservableCollection<DividendEntity> dividendCollection, ObservableCollection<DistributionIncomeEntity> distributionCollection, IBrokerManagedComponent unit)
        {
            if (bankAccounts != null && (dividendCollection != null || distributionCollection != null))
                foreach (var bankacc in bankAccounts)
                {
                    var temp = (BankAccountCM)unit.Broker.GetCMImplementation(bankacc.Clid, bankacc.Csid);
                    if (temp != null)
                    {
                        string accountNumber = temp.GetDataStream((int)CmCommand.AccountNumber, String.Empty);
                        string bsb = temp.GetDataStream((int)CmCommand.BSB, String.Empty);
                        if (!string.IsNullOrEmpty(bsb))
                        {
                            if (bsb.Contains('-'))
                            {
                                bsb = bsb.Split('-')[1];
                            }
                            else if (bsb.Length > 5) //length should be sixe
                            {
                                bsb = bsb.Substring(3, 5);
                            }
                        }
                        else
                        {
                            bsb = "bsbNotFound";
                        }
                        if (ds.Tables["CashTransctions"] != null)
                        {
                            string BsBNAccountNumber = bsb + accountNumber;
                            var drs =
                                ds.Tables["CashTransctions"].Select(String.Format("BSBNAccountNumber='{0}'", BsBNAccountNumber));
                            if (drs.Length > 0)
                            {
                                var info = new DateTimeFormatInfo { ShortDatePattern = "dd/MM/yyyy" };
                                foreach (DataRow dr in drs)
                                {
                                    if (!Boolean.Parse(dr["HasErrors"].ToString()))
                                    {
                                        bool modify = true;
                                        DateTime dateofTrans;
                                        if (
                                            !DateTime.TryParse(dr["DateOfTransaction"].ToString(), info,
                                                               DateTimeStyles.None, out dateofTrans))
                                        {
                                            modify = false;
                                        }

                                        double amount;
                                        if (!double.TryParse(dr["Amount"].ToString(), out amount))
                                        {
                                            modify = false;
                                        }
                                        if (modify)
                                        {
                                            if (dividendCollection != null)
                                            {
                                                var tranEntity =
                                                temp.BankAccountEntity.CashManagementTransactions.FirstOrDefault(
                                                    ss =>
                                                    ss.ExternalReferenceID == dr["BankwestReferenceNumber"].ToString() &&
                                                    ss.SystemTransactionType == "Dividend");
                                                if (tranEntity != null)
                                                {
                                                    var oldDV =
                                                        dividendCollection.FirstOrDefault(
                                                            ss =>
                                                            ss.CashManagementTransactionID == tranEntity.ExternalReferenceID);
                                                    if (oldDV != null)
                                                    {
                                                        oldDV.Status = DividendStatus.Accrued;
                                                        oldDV.CashManagementTransactionID = string.Empty;
                                                    }
                                                    var dV =
                                                        dividendCollection.FirstOrDefault(
                                                            ee =>
                                                            ee.PaymentDate == tranEntity.TransactionDate &&
                                                            Math.Abs(tranEntity.TotalAmount - (decimal)ee.PaidDividend) < 1 &&
                                                            tranEntity.SystemTransactionType == "Dividend");
                                                    if (dV != null)
                                                    {
                                                        tranEntity.DividendID = dV.ID;
                                                        tranEntity.DividendStatus = TransectionStatus.Matched;
                                                        dV.Status = DividendStatus.Matched;
                                                        dV.CashManagementTransactionID = tranEntity.ExternalReferenceID;
                                                    }

                                                }
                                            }
                                            if (distributionCollection != null)
                                            {
                                                var tranEntity =
                                                    temp.BankAccountEntity.CashManagementTransactions.FirstOrDefault(
                                                        ss =>
                                                        ss.ExternalReferenceID ==
                                                        dr["BankwestReferenceNumber"].ToString() &&
                                                        ss.SystemTransactionType == "Distribution");
                                                if (tranEntity != null)
                                                {
                                                    var oldDb =
                                                        distributionCollection.FirstOrDefault(
                                                            ss =>
                                                            ss.CashManagementTransactionID ==
                                                            tranEntity.ExternalReferenceID);
                                                    if (oldDb != null)
                                                    {
                                                        oldDb.Status = DistributionIncomeStatus.Accrued;
                                                        oldDb.CashManagementTransactionID = string.Empty;
                                                    }
                                                    var dB =
                                                        distributionCollection.FirstOrDefault(
                                                            ee =>
                                                            ee.PaymentDate == tranEntity.TransactionDate &&
                                                            Math.Abs(tranEntity.TotalAmount -
                                                                     (decimal)ee.NetCashDistribution) < (decimal)0.5 &&
                                                            tranEntity.SystemTransactionType == "Distribution");
                                                    if (dB != null)
                                                    {
                                                        tranEntity.DividendID = dB.ID;
                                                        tranEntity.DividendStatus = TransectionStatus.Matched;
                                                        dB.Status = DistributionIncomeStatus.Matched;
                                                        dB.CashManagementTransactionID = tranEntity.ExternalReferenceID;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    //unit.Broker.ReleaseBrokerManagedComponent(temp);
                }
        }

    }
}
