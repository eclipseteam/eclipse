﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.Data;
using System.Data;
using Oritax.TaxSimp.DataSets;

namespace Oritax.TaxSimp.CM
{
    public class EODWebserviceUtilities
    {
        string token = string.Empty;

        public void GetPriceHistory(ImportProcessDS ds, string exchange, string marketCode, string subCode, SecuritiesEntity secEntity)
        {
            GetQuote(ds, exchange, marketCode, subCode, secEntity); 
        }

        public void Login()
        {
            System.Diagnostics.Trace.WriteLine("Login()", "frmMain");
            var wsEOD = new com.eoddata.ws.Data();
            var response = wsEOD.Login("eclipseonline", "$Admin#1");

            if (response != null)
            {
                // Failed logins will not return a token
                if (!string.IsNullOrEmpty(response.Token))
                {
                    this.token = response.Token;
                }
                // Check response message for errors
                else
                {
                    throw new Exception("Token not recieved");
                }
            }
            //Clean up
            response = null;
            wsEOD = null;
        }

        private void GetQuote(ImportProcessDS ds, string exchange, string marketCode, string subCode, SecuritiesEntity secEntity)
        {
            DataRow row = ds.Tables[0].NewRow();
            row["ASX Code"] = secEntity.AsxCode;
            row["Company Name"] = secEntity.CompanyName;

            System.Diagnostics.Trace.WriteLine("GetQuote()", "frmMain");
            var wsEOD = new com.eoddata.ws.Data();
            var rsQuote = wsEOD.QuoteGet(this.token, exchange, marketCode + subCode);

            if (rsQuote != null)
            {
                if (rsQuote.QUOTE != null)
                {
                    // Set date parameters
                    var endDate = rsQuote.QUOTE.DateTime.AddDays(1);
                    var startDate = DateTime.Now.AddMonths(-1);
                    secEntity.CompanyName = rsQuote.QUOTE.Name; 
                          
                    // Get history for date range
                    var rsHistory = wsEOD.SymbolHistoryPeriodByDateRange(token, exchange, marketCode + subCode, startDate.ToString("yyyyMMdd"), endDate.ToString("yyyyMMdd"), "d");
                    if (rsHistory != null)
                    {
                        if (rsHistory.QUOTES != null)
                        {
                            foreach (var quote in rsHistory.QUOTES)
                            {
                                var secHisEntity = secEntity.ASXSecurity.Where(secHis => secHis.Date.Date == quote.DateTime.Date).FirstOrDefault();

                                if (secHisEntity == null)
                                {
                                    ASXSecurityEntity securityEntity = new ASXSecurityEntity();
                                    securityEntity.Date = quote.DateTime.Date;
                                    securityEntity.UnitPrice = Math.Round(quote.Close, 6);
                                    secEntity.ASXSecurity.Add(securityEntity);
                                }
                            }
                        }
                    }
                    else
                    {
                        var rsHistory2 = wsEOD.SymbolHistoryPeriodByDateRange(token, exchange, marketCode + subCode, endDate.AddDays(-5).ToString("yyyyMMdd"), endDate.ToString("yyyyMMdd"), "d");
                        if (rsHistory2 != null)
                        {
                            foreach (var quote in rsHistory2.QUOTES)
                            {
                                var secHisEntity = secEntity.ASXSecurity.Where(secHis => secHis.Date.Date == quote.DateTime.Date).FirstOrDefault();

                                if (secHisEntity == null)
                                {
                                    ASXSecurityEntity securityEntity = new ASXSecurityEntity();
                                    securityEntity.Date = quote.DateTime.Date;
                                    securityEntity.UnitPrice = Math.Round(quote.Close, 6);
                                    secEntity.ASXSecurity.Add(securityEntity);
                                }
                            }
                        }
                    }
                    row["Status"] = "Account found and updated successfully";
                }
                else
                    row["Status"] = "Account not found at EOD Finance";

                ds.Tables[0].Rows.Add(row);
            }

            //Clean up
            rsQuote = null;
            wsEOD = null;
        }
    }
}
