﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.DataSets;
using System.Data;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.CM.Group;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CM.Entity;
using System.Collections.ObjectModel;

namespace Oritax.TaxSimp.CM
{
    public class HoldingStatementReportUtility
    {
        public void OnGetDataHoldingRptDataSetCorporate(HoldingSummaryReportDS data, string clientType, string clientID, Guid clientCid, string clientName, IdentityCM Parent, ICMBroker Broker, CorporateEntity Entity)
        {
            DataTable clientSummaryTable = data.Tables[HoldingSummaryReportDS.CLIENTSUMMARYTABLE];
            DataTable contactTable = data.Tables[HoldingSummaryReportDS.CONTACTTABLE];

            DataRow clientSummaryRow = clientSummaryTable.NewRow();
            DataRow contactRow = contactTable.NewRow();

            CSVInstance csvinstance = new CSVInstance(Broker);
            CorporateCSV _entitycsv = csvinstance.GetEntityConcreteInstance(Entity);

            Common.AddressEntity addressEntity = new Common.AddressEntity();

            Common.AddressEntity mailAddressEntity = new Common.AddressEntity();

            if (_entitycsv.Signatories != null && _entitycsv.Signatories.Count > 0)
            {
                if (_entitycsv.Signatories.FirstOrDefault().ResidentialAddress != null)
                    addressEntity = _entitycsv.Signatories.FirstOrDefault().ResidentialAddress;

                if (_entitycsv.Signatories.FirstOrDefault().MailingAddress != null)
                    mailAddressEntity = _entitycsv.Signatories.FirstOrDefault().MailingAddress;
            }

            BasicClientDetails(clientType, clientID, clientName, Parent, Broker, Entity.ABN, Entity.ACN, Entity.Address, clientSummaryRow, addressEntity, mailAddressEntity);

            foreach (var contact in _entitycsv.Contacts)
            {
                contactRow[HoldingSummaryReportDS.CONTACT_TITLE] = contact.PersonalTitle;
                contactRow[HoldingSummaryReportDS.CONTACT_NAME] = contact.Fullname;
                contactRow[HoldingSummaryReportDS.CONTACT_BIZNO] = contact.WorkPhoneNumber;
                contactRow[HoldingSummaryReportDS.CONTACT_MOBNO] = contact.MobilePhoneNumber;
                contactRow[HoldingSummaryReportDS.CONTACT_HNO] = contact.HomePhoneNumber;
                contactRow[HoldingSummaryReportDS.CONTACT_FAXNO] = contact.Facsimile;
                contactRow[HoldingSummaryReportDS.CONTACT_EMAIL] = contact.EmailAddress;
            }

            clientSummaryTable.Rows.Add(clientSummaryRow);
            contactTable.Rows.Add(contactRow);

            IOrganization orgCM = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            HoldingSummaryReportDS securitySummaryReportDS = data as HoldingSummaryReportDS;

            ManualTransactions(clientName, clientID, clientCid, Entity.ClientManualAssetCollection, orgCM, securitySummaryReportDS);

            GetTransactionsInfoFromLinkProcess(clientName, Entity as IClientUMAData, clientID, clientCid, Broker, Entity.ListAccountProcess, orgCM, securitySummaryReportDS, Entity.DesktopBrokerAccounts);
        }

        private static void BasicClientDetails(string clientType, string clientID, string clientName, IdentityCM Parent, ICMBroker Broker, string ABN, string ACN, Common.DualAddressEntity Address, DataRow clientSummaryRow, Oritax.TaxSimp.Common.AddressEntity clientResi, Oritax.TaxSimp.Common.AddressEntity clientMailing)
        {
            clientSummaryRow[HoldingRptDataSet.CLIENTID] = clientID;
            clientSummaryRow[HoldingRptDataSet.CLIENTNAME] = clientName;
            clientSummaryRow[HoldingRptDataSet.CIENTABN] = ABN;
            clientSummaryRow[HoldingRptDataSet.CLIENTACN] = ACN;
            clientSummaryRow[HoldingRptDataSet.CLIENTTYPE] = clientType;

            if (Parent != null && Parent.Clid != Guid.Empty && Parent.Csid != Guid.Empty)
            {
                AdvisorCM adviserUnit = Broker.GetCMImplementation(Parent.Clid, Parent.Csid) as AdvisorCM;
                if (adviserUnit != null)
                {
                    clientSummaryRow[HoldingRptDataSet.ADVISERFULLNAME] = adviserUnit.AdvisorEntity.FullName;
                    clientSummaryRow[HoldingRptDataSet.ADVISERID] = adviserUnit.ClientId;
                    clientSummaryRow[HoldingRptDataSet.ADVISERCID] = adviserUnit.CID;
                }
                else
                {
                    clientSummaryRow[HoldingRptDataSet.ADVISERFULLNAME] = string.Empty;
                    clientSummaryRow[HoldingRptDataSet.ADVISERID] = string.Empty;
                    clientSummaryRow[HoldingRptDataSet.ADVISERCID] = string.Empty;
                }
                Broker.ReleaseBrokerManagedComponent(adviserUnit);
            }
            else
            {
                clientSummaryRow[HoldingRptDataSet.ADVISERFULLNAME] = string.Empty;
                clientSummaryRow[HoldingRptDataSet.ADVISERID] = string.Empty;
                clientSummaryRow[HoldingRptDataSet.ADVISERCID] = string.Empty;
            }

            if (Address.RegisteredAddress.Addressline1 != null && Address.RegisteredAddress.Addressline1 != string.Empty)
            {
                clientSummaryRow[HoldingRptDataSet.ADDRESSLINE1] = Address.RegisteredAddress.Addressline1;
                clientSummaryRow[HoldingRptDataSet.ADDRESSLINE2] = Address.RegisteredAddress.Addressline2;
                clientSummaryRow[HoldingRptDataSet.SUBURB] = Address.RegisteredAddress.Suburb;
                clientSummaryRow[HoldingRptDataSet.STATE] = Address.RegisteredAddress.State;
                clientSummaryRow[HoldingRptDataSet.POSTCODE] = Address.RegisteredAddress.PostCode;
                clientSummaryRow[HoldingRptDataSet.COUNTRY] = Address.RegisteredAddress.Country;
            }
            else if (Address.BusinessAddress.Addressline1 != null && Address.BusinessAddress.Addressline1 != string.Empty)
            {
                clientSummaryRow[HoldingRptDataSet.ADDRESSLINE1] = Address.BusinessAddress.Addressline1;
                clientSummaryRow[HoldingRptDataSet.ADDRESSLINE2] = Address.BusinessAddress.Addressline2;
                clientSummaryRow[HoldingRptDataSet.SUBURB] = Address.BusinessAddress.Suburb;
                clientSummaryRow[HoldingRptDataSet.STATE] = Address.BusinessAddress.State;
                clientSummaryRow[HoldingRptDataSet.POSTCODE] = Address.BusinessAddress.PostCode;
                clientSummaryRow[HoldingRptDataSet.COUNTRY] = Address.BusinessAddress.Country;
            }
            else if (Address.MailingAddress.Addressline1 != null && Address.MailingAddress.Addressline1 != string.Empty)
            {
                clientSummaryRow[HoldingRptDataSet.ADDRESSLINE1] = Address.MailingAddress.Addressline1;
                clientSummaryRow[HoldingRptDataSet.ADDRESSLINE2] = Address.MailingAddress.Addressline2;
                clientSummaryRow[HoldingRptDataSet.SUBURB] = Address.MailingAddress.Suburb;
                clientSummaryRow[HoldingRptDataSet.STATE] = Address.MailingAddress.State;
                clientSummaryRow[HoldingRptDataSet.POSTCODE] = Address.MailingAddress.PostCode;
                clientSummaryRow[HoldingRptDataSet.COUNTRY] = Address.MailingAddress.Country;
            }
            else if (Address.ResidentialAddress.Addressline1 != null && Address.ResidentialAddress.Addressline1 != string.Empty)
            {
                clientSummaryRow[HoldingRptDataSet.ADDRESSLINE1] = Address.ResidentialAddress.Addressline1;
                clientSummaryRow[HoldingRptDataSet.ADDRESSLINE2] = Address.ResidentialAddress.Addressline2;
                clientSummaryRow[HoldingRptDataSet.SUBURB] = Address.ResidentialAddress.Suburb;
                clientSummaryRow[HoldingRptDataSet.STATE] = Address.ResidentialAddress.State;
                clientSummaryRow[HoldingRptDataSet.POSTCODE] = Address.ResidentialAddress.PostCode;
                clientSummaryRow[HoldingRptDataSet.COUNTRY] = Address.ResidentialAddress.Country;
            }

            else if (clientResi != null && clientResi.Addressline1 != string.Empty)
            {
                clientSummaryRow[HoldingRptDataSet.ADDRESSLINE1] = clientResi.Addressline1;
                clientSummaryRow[HoldingRptDataSet.ADDRESSLINE2] = clientResi.Addressline2;
                clientSummaryRow[HoldingRptDataSet.SUBURB] = clientResi.Suburb;
                clientSummaryRow[HoldingRptDataSet.STATE] = clientResi.State;
                clientSummaryRow[HoldingRptDataSet.POSTCODE] = clientResi.PostCode;
                clientSummaryRow[HoldingRptDataSet.COUNTRY] = clientResi.Country;
            }

            else if (clientMailing != null && clientMailing.Addressline1 != string.Empty)
            {
                clientSummaryRow[HoldingRptDataSet.ADDRESSLINE1] = clientMailing.Addressline1;
                clientSummaryRow[HoldingRptDataSet.ADDRESSLINE2] = clientMailing.Addressline2;
                clientSummaryRow[HoldingRptDataSet.SUBURB] = clientMailing.Suburb;
                clientSummaryRow[HoldingRptDataSet.STATE] = clientMailing.State;
                clientSummaryRow[HoldingRptDataSet.POSTCODE] = clientMailing.PostCode;
                clientSummaryRow[HoldingRptDataSet.COUNTRY] = clientMailing.Country;
            }

        }

        private static void GetTransactionsInfoFromLinkProcess(string clientName,IClientUMAData clientEntityData, string clientID, Guid cliendCid, ICMBroker Broker, List<AccountProcessTaskEntity> ListAccountProcess, IOrganization orgCM, HoldingSummaryReportDS securitySummaryReportDS, List<Oritax.TaxSimp.Common.IdentityCMDetail> DesktopBrokerAccounts)
        {
            if (ListAccountProcess != null)
            {
                GetUnlistedSecurityMIS(clientName,clientID, cliendCid, Broker, ListAccountProcess, orgCM, securitySummaryReportDS);

                IEnumerable<HoldingTransactions> TotalHoldingTransactions = new ObservableCollection<HoldingTransactions>();
                foreach (Oritax.TaxSimp.Common.IdentityCMDetail desktopBrokerAccounts in DesktopBrokerAccounts)
                {
                    var task=ListAccountProcess.FirstOrDefault(ss => ss.LinkedEntityType == OrganizationType.DesktopBrokerAccount && ss.LinkedEntity != null && (ss.LinkedEntity.Cid == desktopBrokerAccounts.Cid || (ss.LinkedEntity.Clid == desktopBrokerAccounts.Clid && ss.LinkedEntity.Csid == desktopBrokerAccounts.Csid)));
                    string serviceType = string.Empty;
                   
                    if(task!=null)
                    {
                        var modelasx=orgCM.Model.FirstOrDefault(ss => ss.ID == task.ModelID);
                        serviceType = modelasx == null ? string.Empty : modelasx.ServiceType.ToString();
                    }


                    var desktopBrokerAccountCM = Broker.GetCMImplementation(desktopBrokerAccounts.Clid,
                                                                        desktopBrokerAccounts.Csid) as DesktopBrokerAccountCM;
                    if (desktopBrokerAccountCM != null)
                    {
                        TotalHoldingTransactions = TotalHoldingTransactions.Union(desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions);

                        DesktopBrokerTransactionsCapital(clientName,clientID, cliendCid, serviceType, orgCM, securitySummaryReportDS.EndDate, desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions, securitySummaryReportDS, desktopBrokerAccountCM);

                    }
                }

                DesktopBrokerTransactions(orgCM, securitySummaryReportDS.EndDate, TotalHoldingTransactions, securitySummaryReportDS);

                foreach (var accountProcessTaskEntity in ListAccountProcess)
                {
                    if (accountProcessTaskEntity.LinkedEntity != null)
                    {
                        var asset = orgCM.Assets.Where(var => var.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                        var model = orgCM.Model.Where(mod => mod.ID == accountProcessTaskEntity.ModelID).FirstOrDefault();
                        if (model != null)
                        {
                            var linkedAsset = model.Assets.Where(ast => ast.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                            if (linkedAsset != null)
                            {
                                var product = linkedAsset.Products.Where(pro => pro.ID == accountProcessTaskEntity.TaskID).FirstOrDefault();

                                if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.BankAccount && accountProcessTaskEntity.LinkedEntity.Clid != Guid.Empty)
                                    BankAccountTransctions(clientName,clientID, cliendCid, model.ServiceType.ToString(), clientEntityData.DistributionIncomes, Broker, securitySummaryReportDS, accountProcessTaskEntity, asset, model, product, orgCM);
                                else if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.TermDepositAccount && accountProcessTaskEntity.LinkedEntity.Clid != Guid.Empty)
                                    TDTransactions(clientName,clientID, cliendCid, model.ServiceType.ToString(), Broker, orgCM, securitySummaryReportDS, accountProcessTaskEntity, asset, model, product);
                                else if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.ManagedInvestmentSchemesAccount)
                                    ManageInvestmentSchemes(clientName,clientID, cliendCid, model.ServiceType.ToString(), Broker, orgCM, securitySummaryReportDS, accountProcessTaskEntity, asset, model, product);
                            }
                        }
                    }
                }
            }
        }

        private static void GetUnlistedSecurityMIS(string clientName, string clientID, Guid clientCid, ICMBroker Broker, List<AccountProcessTaskEntity> ListAccountProcess, IOrganization orgCM, HoldingSummaryReportDS holdingSummaryReportDS)
        {
            decimal closingBal = 0;
            decimal closingPrice = 0;
            decimal closingUnits = 0;

            decimal openingBal = 0;
            decimal openingPrice = 0;
            decimal openingUnits = 0;

            ManagedInvestmentSchemesAccountCM misData = Broker.GetCMImplementation(new Guid("7986b8ee-4509-44a5-ac8c-7ce59b59592d"), new Guid("b7ae4bb1-2143-4e56-b9ac-10cf24f7eae5")) as ManagedInvestmentSchemesAccountCM;
            foreach (FundAccountEntity entity in misData.ManagedInvestmentSchemesAccountEntity.FundAccounts)
            {
                var transactions = entity.FundTransactions.Where(tran => tran.ClientID == clientID);
                if (transactions.Count() > 0)
                {
                    bool accountLinked = false;

                    foreach (var accountProcessTaskEntity in ListAccountProcess)
                    {
                        var model = orgCM.Model.Where(mod => mod.ID == accountProcessTaskEntity.ModelID).FirstOrDefault();
                        var linkedAsset = model.Assets.Where(ast => ast.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                        if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.ManagedInvestmentSchemesAccount)
                        {
                            if (linkedAsset != null)
                            {
                                var product = linkedAsset.Products.Where(pro => pro.ID == accountProcessTaskEntity.TaskID).FirstOrDefault();

                                if (product != null && product.TaskDescription != null && product.TaskDescription.Contains(entity.Code))
                                    accountLinked = true;
                            }
                        }
                    }

                    DateTime priceDate = holdingSummaryReportDS.EndDate;

                    if (!accountLinked)
                    {
                        Guid productID = Guid.NewGuid();

                        var filteredHoldingTransactionsByRange = entity.FundTransactions.Where(cliFundTrans => cliFundTrans.TradeDate.Date >= holdingSummaryReportDS.StartDate.Date && cliFundTrans.TradeDate.Date <= holdingSummaryReportDS.EndDate.Date);
                        var clientFundTransactionsRange = filteredHoldingTransactionsByRange.Where(cliFundTrans => cliFundTrans.ClientID == clientID);

                        var filteredHoldingTransactionsByCLS = entity.FundTransactions.Where(cliFundTrans => cliFundTrans.TradeDate <= holdingSummaryReportDS.EndDate);
                        var clientFundTransactionsCLS = filteredHoldingTransactionsByCLS.Where(cliFundTrans => cliFundTrans.ClientID == clientID);
                        closingUnits = clientFundTransactionsCLS.Sum(ft => ft.Shares);

                        var filteredHoldingTransactionsByOB = entity.FundTransactions.Where(cliFundTrans => cliFundTrans.TradeDate <= holdingSummaryReportDS.StartDate);
                        var clientFundTransactionsOB = filteredHoldingTransactionsByOB.Where(cliFundTrans => cliFundTrans.ClientID == clientID);
                        openingUnits = clientFundTransactionsOB.Sum(ft => ft.Shares);

                        var filteredHoldingTransactionsByValuationDate = entity.FundTransactions.Where(cliFundTrans => cliFundTrans.TradeDate <= holdingSummaryReportDS.EndDate);
                        var clientFundTransactions = filteredHoldingTransactionsByValuationDate.Where(cliFundTrans => cliFundTrans.ClientID == clientID);
                        decimal totalShares = clientFundTransactions.Sum(ft => ft.Shares);
                        var linkedmissec = orgCM.Securities.Where(missec => missec.AsxCode == entity.Code).FirstOrDefault();

                        var latestMisPriceObjectCLS = linkedmissec.ASXSecurity.Where(mishis => mishis.Date <= holdingSummaryReportDS.EndDate).OrderByDescending(mishis => mishis.Date).FirstOrDefault();
                        if (latestMisPriceObjectCLS != null)
                        {
                            closingPrice = Convert.ToDecimal(latestMisPriceObjectCLS.UnitPrice);
                            priceDate = latestMisPriceObjectCLS.Date;
                        }
                        closingBal = closingUnits * closingPrice;

                        var latestMisPriceObjectOB = linkedmissec.ASXSecurity.Where(mishis => mishis.Date <= holdingSummaryReportDS.StartDate).OrderByDescending(mishis => mishis.Date).FirstOrDefault();
                        if (latestMisPriceObjectOB != null)
                            openingPrice = Convert.ToDecimal(latestMisPriceObjectOB.UnitPrice);

                        openingBal = openingUnits * openingPrice;

                        decimal startUnits = openingUnits;
                        decimal startUnitsAmt = openingBal;

                        holdingSummaryReportDS.AddMISTransDetailsRow(linkedmissec.CompanyName, entity.Code, holdingSummaryReportDS.StartDate.AddDays(-1), 0, startUnitsAmt, 0, 0, startUnits, "Opening Balance", "");
                        var sorted = clientFundTransactionsRange.OrderBy(mis => mis.TradeDate);
                        foreach (MISFundTransactionEntity misTran in sorted)
                        {
                            decimal unitsBalance = startUnits + misTran.Shares;
                            decimal unitsBalanceAmt = startUnitsAmt + misTran.Amount;

                            holdingSummaryReportDS.AddMISTransDetailsRow(linkedmissec.CompanyName, entity.Code, misTran.TradeDate, misTran.Amount, unitsBalanceAmt, misTran.UnitPrice, misTran.Shares, unitsBalance, misTran.TransactionType, "");

                            startUnits = unitsBalance;
                            startUnitsAmt = unitsBalanceAmt;
                        }

                        decimal applicationTotal = clientFundTransactionsRange.Where(t => t.TransactionType == "Application").Sum(t => t.Amount);
                        decimal redemptionTotal = clientFundTransactionsRange.Where(t => t.TransactionType == "Redemption").Sum(t => t.Amount);
                        decimal adjustmentUpTotal = clientFundTransactionsRange.Where(t => t.TransactionType == "Adjustment Up").Sum(t => t.Amount);
                        decimal adjustmentDownTotal = clientFundTransactionsRange.Where(t => t.TransactionType == "Adjustment Down").Sum(t => t.Amount);
                        decimal movement = closingBal - openingBal - applicationTotal - redemptionTotal - adjustmentUpTotal - adjustmentDownTotal;

                        var closingMisPriceObjectOB = linkedmissec.ASXSecurity.Where(mishis => mishis.Date <= holdingSummaryReportDS.EndDate).OrderByDescending(mishis => mishis.Date).FirstOrDefault();

                        decimal closingBalancePrice = 0;

                        if (closingMisPriceObjectOB != null)
                            closingBalancePrice = Convert.ToDecimal(closingMisPriceObjectOB.UnitPrice);

                        decimal closingBalance = closingBalancePrice * closingUnits;

                        holdingSummaryReportDS.AddMISTransDetailsRow(linkedmissec.CompanyName, entity.Code, holdingSummaryReportDS.EndDate, 0, closingBalance, 0, 0, closingUnits, "Closing Balance", "");

                        holdingSummaryReportDS.AddSecuritySummaryRow("MIS", "", entity.Code, entity.Description, priceDate, openingUnits, openingPrice, openingBal, applicationTotal, redemptionTotal, adjustmentUpTotal, adjustmentDownTotal, movement, closingUnits, closingPrice, closingBal, closingBal, movement);

                        //Add Capital Transactions
                        foreach (MISFundTransactionEntity tranEntity in clientFundTransactionsRange)
                        {
                            if (tranEntity.TransactionType == "Redemption" || tranEntity.TransactionType == "Purchase" || tranEntity.TransactionType == "Adjustment Up" || tranEntity.TransactionType == "Adjustment Down")
                                holdingSummaryReportDS.AddCapitalTranRow(clientName,clientID,clientCid,"Unlisted", tranEntity.ID, misData.CID, entity.Code, "FUNDS", entity.Description, tranEntity.TransactionType,
                                    clientID, tranEntity.TradeDate, tranEntity.TradeDate, tranEntity.Shares, tranEntity.UnitPrice, tranEntity.Amount, 0, tranEntity.Amount);
                        }
                    }

                }
            }
        }


        private static void ManageInvestmentSchemes(string clientName, string clientID, Guid clientCid, string serviceType, ICMBroker Broker, IOrganization orgCM, HoldingSummaryReportDS securitySummaryReportDS, AccountProcessTaskEntity accountProcessTaskEntity, AssetEntity asset, ModelEntity model, ProductEntity product)
        {
            decimal closingBal = 0;
            decimal closingPrice = 0;
            decimal closingUnits = 0;

            decimal openingBal = 0;
            decimal openingPrice = 0;
            decimal openingUnits = 0;

            ManagedInvestmentSchemesAccountCM managedInvestmentSchemesAccountCM = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as ManagedInvestmentSchemesAccountCM;

            if (managedInvestmentSchemesAccountCM != null && product != null)
            {

                var linkedMISAccount = managedInvestmentSchemesAccountCM.ManagedInvestmentSchemesAccountEntity.FundAccounts.Where(fa => product.TaskDescription.Contains(fa.Code)).FirstOrDefault();

                var filteredHoldingTransactionsByRange = linkedMISAccount.FundTransactions.Where(cliFundTrans => cliFundTrans.TradeDate >= securitySummaryReportDS.StartDate && cliFundTrans.TradeDate <= securitySummaryReportDS.EndDate);
                var clientFundTransactionsRange = filteredHoldingTransactionsByRange.Where(cliFundTrans => cliFundTrans.ClientID == clientID);

                var filteredHoldingTransactionsByCLS = linkedMISAccount.FundTransactions.Where(cliFundTrans => cliFundTrans.TradeDate <= securitySummaryReportDS.EndDate);
                var clientFundTransactionsCLS = filteredHoldingTransactionsByCLS.Where(cliFundTrans => cliFundTrans.ClientID == clientID);
                closingUnits = clientFundTransactionsCLS.Sum(ft => ft.Shares);

                var filteredHoldingTransactionsByOB = linkedMISAccount.FundTransactions.Where(cliFundTrans => cliFundTrans.TradeDate <= securitySummaryReportDS.StartDate);
                var clientFundTransactionsOB = filteredHoldingTransactionsByOB.Where(cliFundTrans => cliFundTrans.ClientID == clientID);
                openingUnits = clientFundTransactionsOB.Sum(ft => ft.Shares);

                var linkedmissec = orgCM.Securities.Where(missec => missec.AsxCode == linkedMISAccount.Code).FirstOrDefault();

                DateTime priceDate = securitySummaryReportDS.EndDate;

                if (linkedmissec != null)
                {
                    var latestMisPriceObjectCLS = linkedmissec.ASXSecurity.Where(mishis => mishis.Date <= securitySummaryReportDS.EndDate).OrderByDescending(mishis => mishis.Date).FirstOrDefault();
                    if (latestMisPriceObjectCLS != null)
                    {
                        closingPrice = Convert.ToDecimal(latestMisPriceObjectCLS.UnitPrice);
                        priceDate = latestMisPriceObjectCLS.Date;
                    }
                    closingBal = closingUnits * closingPrice;

                    var latestMisPriceObjectOB = linkedmissec.ASXSecurity.Where(mishis => mishis.Date <= securitySummaryReportDS.StartDate).OrderByDescending(mishis => mishis.Date).FirstOrDefault();
                    if (latestMisPriceObjectOB != null)
                        openingPrice = Convert.ToDecimal(latestMisPriceObjectOB.UnitPrice);

                    openingBal = openingUnits * openingPrice;

                    decimal startUnits = openingUnits;
                    decimal startUnitsAmt = openingBal;


                    decimal startUnitPrice = 0;
                    if (startUnits != 0)
                        startUnitPrice = startUnitsAmt / startUnits;

                    securitySummaryReportDS.AddMISTransDetailsRow(linkedmissec.CompanyName, linkedMISAccount.Code, securitySummaryReportDS.StartDate.AddDays(-1), startUnitsAmt, startUnitsAmt, startUnitPrice, startUnits, startUnits, "Opening Balance", "");
                    var sorted = clientFundTransactionsRange.OrderBy(mis => mis.TradeDate);
                    foreach (MISFundTransactionEntity misTran in sorted)
                    {
                        decimal unitsBalance = startUnits + misTran.Shares;
                        decimal unitBalPrice = 0;
                        var unitPriceEntity = linkedmissec.ASXSecurity.Where(mishis => mishis.Date <= misTran.TradeDate.Date).OrderByDescending(mishis => mishis.Date.Date).FirstOrDefault();
                        if (unitPriceEntity != null)
                            unitBalPrice = Convert.ToDecimal(unitPriceEntity.UnitPrice);

                        decimal unitsBalanceAmt = unitsBalance * +unitBalPrice;

                        securitySummaryReportDS.AddMISTransDetailsRow(linkedmissec.CompanyName, linkedMISAccount.Code, misTran.TradeDate, misTran.Amount, unitsBalanceAmt, misTran.UnitPrice, misTran.Shares, unitsBalance, misTran.TransactionType, "");

                        startUnits = unitsBalance;
                    }

                    var closingMisPriceObjectOB = linkedmissec.ASXSecurity.Where(mishis => mishis.Date <= securitySummaryReportDS.EndDate).OrderByDescending(mishis => mishis.Date).FirstOrDefault();

                    decimal closingBalancePrice = 0;

                    if (closingMisPriceObjectOB != null)
                        closingBalancePrice = Convert.ToDecimal(closingMisPriceObjectOB.UnitPrice);

                    decimal closingBalance = closingBalancePrice * closingUnits;

                    decimal clsUnitPrice = 0;
                    if (closingUnits != 0)
                        clsUnitPrice = closingBalance / closingUnits;

                    securitySummaryReportDS.AddMISTransDetailsRow(linkedmissec.CompanyName, linkedMISAccount.Code, securitySummaryReportDS.EndDate, closingBalance, closingBalance, clsUnitPrice, closingUnits, closingUnits, "Closing Balance", "");

                    decimal applicationTotal = clientFundTransactionsRange.Where(t => t.TransactionType == "Application").Sum(t => t.Amount);
                    decimal redemptionTotal = clientFundTransactionsRange.Where(t => t.TransactionType == "Redemption").Sum(t => t.Amount);
                    decimal adjustmentUpTotal = clientFundTransactionsRange.Where(t => t.TransactionType == "Adjustment Up").Sum(t => t.Amount);
                    decimal adjustmentDownTotal = clientFundTransactionsRange.Where(t => t.TransactionType == "Adjustment Down").Sum(t => t.Amount);
                    decimal movement = closingBal - openingBal - applicationTotal - redemptionTotal - adjustmentUpTotal - adjustmentDownTotal;

                    securitySummaryReportDS.AddSecuritySummaryRow("FUND", "", linkedMISAccount.Code, linkedMISAccount.Description, priceDate, openingUnits, openingPrice, openingBal, applicationTotal, redemptionTotal, adjustmentUpTotal, adjustmentDownTotal, movement, closingUnits, closingPrice, closingBal, closingBal, movement);

                    //Add Capital Transactions
                    foreach (MISFundTransactionEntity tranEntity in clientFundTransactionsRange)
                    {
                        if (tranEntity.TransactionType == "Redemption" || tranEntity.TransactionType == "Purchase" || tranEntity.TransactionType == "Adjustment Up" || tranEntity.TransactionType == "Adjustment Down")
                            securitySummaryReportDS.AddCapitalTranRow(clientName, clientID, clientCid, serviceType, tranEntity.ID, managedInvestmentSchemesAccountCM.CID, linkedMISAccount.Code, "FUNDS", linkedMISAccount.Description, tranEntity.TransactionType,
                                clientID, tranEntity.TradeDate, tranEntity.TradeDate, tranEntity.Shares, tranEntity.UnitPrice, tranEntity.Amount, 0, tranEntity.Amount);
                    }
                }
            }
        }

        private static void DesktopBrokerTransactions(IOrganization orgCM, DateTime valuationDate, IEnumerable<HoldingTransactions> holdingTransactions, HoldingSummaryReportDS securitySummaryReportDS)
        {
            var groupedSecurities = holdingTransactions.GroupBy(holdTran => holdTran.InvestmentCode);

            foreach (var security in groupedSecurities)
            {
                decimal closingUnits = 0;
                decimal closingPrice = 0;
                decimal closingBal = 0;

                decimal openingUnits = 0;
                decimal openingPrice = 0;
                decimal openingBal = 0;

                closingUnits = security.Where(sec => sec.TradeDate <= securitySummaryReportDS.EndDate).Sum(sec => sec.Units);
                openingUnits = security.Where(sec => sec.TradeDate <= securitySummaryReportDS.StartDate.AddDays(-1)).Sum(sec => sec.Units);

                var asxBroker = orgCM.Securities.Where(sec => sec.AsxCode == security.Key).FirstOrDefault();
                var hisSecurityCls = asxBroker.ASXSecurity.Where(secpri => secpri.Date <= securitySummaryReportDS.EndDate).OrderByDescending(secpri => secpri.Date).FirstOrDefault();
                DateTime priceDate = securitySummaryReportDS.EndDate;

                if (hisSecurityCls != null)
                {
                    closingPrice = Convert.ToDecimal(hisSecurityCls.UnitPrice);
                    priceDate = hisSecurityCls.Date;
                }

                var hisSecurityOB = asxBroker.ASXSecurity.Where(secpri => secpri.Date <= securitySummaryReportDS.StartDate).OrderByDescending(secpri => secpri.Date).FirstOrDefault();
                if (hisSecurityOB != null)
                    openingPrice = Convert.ToDecimal(hisSecurityOB.UnitPrice);

                closingBal = closingUnits * closingPrice;
                openingBal = openingUnits * openingPrice;

                var orderedList = security.Where(sec => sec.TradeDate.Date >= securitySummaryReportDS.StartDate.Date && sec.TradeDate.Date <= securitySummaryReportDS.EndDate.Date).OrderBy(sec => sec.TradeDate.Date);

                decimal startUnits = openingUnits;

                securitySummaryReportDS.AddTransDetailsRow(security.Key, securitySummaryReportDS.StartDate.AddDays(-1), openingUnits, openingUnits, "Opening Balance", "");

                foreach (HoldingTransactions transaction in orderedList)
                {
                    decimal unitsBalance = startUnits + transaction.Units;
                    securitySummaryReportDS.AddTransDetailsRow(security.Key, transaction.TradeDate, transaction.Units, unitsBalance, transaction.TransactionType, transaction.FPSInstructionID);

                    startUnits = unitsBalance;
                }

                securitySummaryReportDS.AddTransDetailsRow(security.Key, securitySummaryReportDS.EndDate, 0, closingUnits, "Closing Balance", "");

                decimal applicationTotal = orderedList.Where(t => t.Type == "AP" || t.Type == "AN").Sum(t => t.NetValue);
                decimal redemptionTotal = orderedList.Where(t => t.Type == "RA" || t.Type == "RR").Sum(t => t.NetValue);
                decimal adjUpTotal = orderedList.Where(t => t.Type == "RJ").Sum(t => t.NetValue);
                decimal adjDownTotal = orderedList.Where(t => t.Type == "AJ").Sum(t => t.NetValue);
                decimal movement = closingBal - openingBal - applicationTotal - redemptionTotal - adjUpTotal - adjDownTotal;

                securitySummaryReportDS.AddSecuritySummaryRow("SEC", "", asxBroker.AsxCode, asxBroker.CompanyName, priceDate, openingUnits,
                                                    openingPrice, openingBal, applicationTotal, redemptionTotal, adjUpTotal, adjDownTotal, movement, closingUnits, closingPrice, closingBal, closingBal, movement);



            }
        }

        private static void DesktopBrokerTransactionsCapital(string clientName, string clientId, Guid clientCid, string serviceType, IOrganization orgCM, DateTime valuationDate, IEnumerable<HoldingTransactions> holdingTransactions, HoldingSummaryReportDS securitySummaryReportDS, DesktopBrokerAccountCM desktopBrokerAccountCM)
        {
            var groupedSecurities = holdingTransactions.GroupBy(holdTran => holdTran.InvestmentCode);

            foreach (var security in groupedSecurities)
            {
                decimal openingUnits = 0;

                openingUnits = security.Where(sec => sec.TradeDate <= securitySummaryReportDS.StartDate.AddDays(-1)).Sum(sec => sec.Units);

                var asxBroker = orgCM.Securities.Where(sec => sec.AsxCode == security.Key).FirstOrDefault();
                var hisSecurityCls = asxBroker.ASXSecurity.Where(secpri => secpri.Date <= securitySummaryReportDS.EndDate).OrderByDescending(secpri => secpri.Date).FirstOrDefault();
                DateTime priceDate = securitySummaryReportDS.EndDate;

                var orderedList = security.Where(sec => sec.TradeDate >= securitySummaryReportDS.StartDate && sec.TradeDate <= securitySummaryReportDS.EndDate).OrderBy(sec => sec.TradeDate);

                decimal startUnits = openingUnits;

                foreach (HoldingTransactions transaction in orderedList)
                {
                    decimal unitsBalance = startUnits + transaction.Units;
                    securitySummaryReportDS.AddTransDetailsRow(security.Key, transaction.TradeDate, transaction.Units, unitsBalance, transaction.TransactionType, transaction.FPSInstructionID);

                    startUnits = unitsBalance;
                }

                //Add Capital transactions
                foreach (HoldingTransactions holding in orderedList)
                {
                    if (holding.Type == "RA" || holding.Type == "AN")
                    {
                        decimal presentationUnitPrice = holding.UnitPrice < 0 ? holding.UnitPrice * -1 : holding.UnitPrice;
                        decimal presentationGrossValue = presentationUnitPrice * holding.Units;
                        decimal presentationNetValue = presentationGrossValue + holding.BrokerageAmount;

                        securitySummaryReportDS.AddCapitalTranRow(clientName, clientId, clientCid, serviceType, holding.TranstactionUniqueID, desktopBrokerAccountCM.CID, asxBroker.AsxCode, "SEC", asxBroker.CompanyName, holding.TransactionType, desktopBrokerAccountCM.DesktopBrokerAccountEntity.AccountNumber, holding.TradeDate,
                            holding.SettlementDate.HasValue ? holding.SettlementDate.Value : holding.TradeDate, holding.Units, presentationUnitPrice, presentationGrossValue, holding.BrokerageAmount, presentationNetValue);
                    }

                }

            }
        }



        private static void TDTransactions(string clientName, string clientId, Guid clientCid, string servicetype, ICMBroker Broker, IOrganization orgCM, HoldingSummaryReportDS securitySummaryReportDS, AccountProcessTaskEntity accountProcessTaskEntity, AssetEntity asset, ModelEntity model, ProductEntity product)
        {
            BankAccountCM bankAccount = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as BankAccountCM;
            decimal holdingTotalClosing = 0;
            decimal holdingTotalOpening = 0;
            decimal applicationTotal = 0;
            decimal redemptionTotal = 0;

            if (bankAccount != null)
            {

                var filteredTransactionsRange = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate.Date <= securitySummaryReportDS.EndDate.Date && trans.TransactionDate.Date.Date >= securitySummaryReportDS.StartDate.Date);
                var filteredTransactionsRangeGroups = filteredTransactionsRange.GroupBy(cashMan => cashMan.TransactionType);

                foreach (var tdType in filteredTransactionsRangeGroups)
                {
                    var groupByIns = tdType.GroupBy(tdtypetran => tdtypetran.InstitutionID);

                    foreach (var groupins in groupByIns)
                    {
                        var inst = orgCM.Institution.Where(ins => ins.ID == groupins.Key).FirstOrDefault();
                        string instName = string.Empty;
                        if (inst != null)
                            instName = inst.Name;

                        foreach (Oritax.TaxSimp.Common.CashManagementEntity cashManagementEntity in groupins)
                        {
                            if (cashManagementEntity.SystemTransactionType == "Application" || cashManagementEntity.SystemTransactionType == "Redemption" || cashManagementEntity.SystemTransactionType == "Sale")
                            {
                                if (cashManagementEntity.SystemTransactionType == "Application")
                                    applicationTotal += cashManagementEntity.TotalAmount;
                                else if (cashManagementEntity.SystemTransactionType == "Redemption" || cashManagementEntity.SystemTransactionType == "Sale")
                                    redemptionTotal += cashManagementEntity.TotalAmount;

                                securitySummaryReportDS.AddCapitalTranRow(clientName, clientId, clientCid, servicetype, cashManagementEntity.ID, bankAccount.CID, cashManagementEntity.AdministrationSystem, "TDs", instName, cashManagementEntity.SystemTransactionType, bankAccount.BankAccountEntity.AccountNumber, cashManagementEntity.TransactionDate,
                                   cashManagementEntity.TransactionDate, 0, 0, cashManagementEntity.TotalAmount, 0, cashManagementEntity.TotalAmount);
                            }

                        }
                    }
                }

                var filteredTransactionsClosing = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate <= securitySummaryReportDS.EndDate);

                var tdGroupsTypeClosing = filteredTransactionsClosing.GroupBy(cashMan => cashMan.TransactionType);

                foreach (var tdType in tdGroupsTypeClosing)
                {
                    var groupByIns = tdType.GroupBy(tdtypetran => tdtypetran.InstitutionID);

                    foreach (var groupins in groupByIns)
                    {
                        var transaction = groupins.FirstOrDefault();
                        decimal holding = groupins.Sum(inssum => inssum.Amount);

                        var inst = orgCM.Institution.Where(ins => ins.ID == groupins.Key).FirstOrDefault();

                        string instName = string.Empty;
                        if (inst != null)
                            instName = inst.Name;

                        holdingTotalClosing += holding;
                    }
                }

                var filteredTransactionsOpening = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate.Date <= securitySummaryReportDS.StartDate.AddDays(-1));

                var tdGroupsTypeOpening = filteredTransactionsOpening.GroupBy(cashMan => cashMan.TransactionType);

                foreach (var tdType in tdGroupsTypeOpening)
                {
                    var groupByIns = tdType.GroupBy(tdtypetran => tdtypetran.InstitutionID);

                    foreach (var groupins in groupByIns)
                    {
                        var transaction = groupins.FirstOrDefault();
                        decimal holding = groupins.Sum(inssum => inssum.TotalAmount);

                        var inst = orgCM.Institution.Where(ins => ins.ID == groupins.Key).FirstOrDefault();

                        string instName = string.Empty;
                        if (inst != null)
                            instName = inst.Name;

                        holdingTotalOpening += holding;
                    }
                }

                string productName = string.Empty;

                if (bankAccount.BankAccountEntity.AccoutType == "termdeposit")
                    productName = product.Name + "- TD";
                else
                    productName = product.Name + "- AT CALL";

                decimal movement = holdingTotalClosing - holdingTotalOpening - applicationTotal - redemptionTotal;

                securitySummaryReportDS.AddSecuritySummaryRowBank("TDs", asset.Name, productName, securitySummaryReportDS.EndDate, holdingTotalOpening, applicationTotal, redemptionTotal, 0, 0, movement, holdingTotalClosing, string.Empty, string.Empty, bankAccount.BankAccountEntity.CustomerNo, holdingTotalClosing, movement);
            }
        }

        private static void BankAccountTransctions(string clientName, string clientId, Guid clientCid, string serviceType, ObservableCollection<DistributionIncomeEntity> DistributionIncomes, ICMBroker Broker, HoldingSummaryReportDS securitySummaryReportDS, AccountProcessTaskEntity accountProcessTaskEntity, AssetEntity asset, ModelEntity model, ProductEntity product, IOrganization orgCM)
        {
            if (accountProcessTaskEntity.LinkedEntity.Clid == Guid.Empty)
            {
                accountProcessTaskEntity.LinkedEntity.Clid = product.EntityId.Clid;
                accountProcessTaskEntity.LinkedEntity.Csid = product.EntityId.Csid;
            }

            BankAccountCM bankAccount = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as BankAccountCM;
            if (bankAccount != null)
            {
                var filteredTransactionsClosing = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate <= securitySummaryReportDS.EndDate);
                decimal holdingTotalClosing = filteredTransactionsClosing.Sum(calc => calc.TotalAmount);
                var rangeTransactions = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate.Date <= securitySummaryReportDS.EndDate.Date && trans.TransactionDate.Date >= securitySummaryReportDS.StartDate.Date);

                var inst = orgCM.Institution.Where(ins => ins.ID == bankAccount.BankAccountEntity.InstitutionID).FirstOrDefault(); ;

                var filteredTransactionsOpening = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate <= securitySummaryReportDS.StartDate.AddDays(-1));
                decimal holdingTotalOpening = filteredTransactionsOpening.Sum(calc => calc.TotalAmount);

                List<Oritax.TaxSimp.Common.CashManagementEntity> distributionIncomeTrans = rangeTransactions.Where(tran => tran.Category == "Income" && tran.SystemTransactionType == "Distribution").ToList();

                if (securitySummaryReportDS.AddBanktransInInvestment)// if report requires investment bank transactions too
                {
                    var investmentTrans = rangeTransactions.Where(tran => tran.Category == "Investment" && (tran.SystemTransactionType == "Application"||tran.SystemTransactionType == "Redemption"));
                
                    string instName = string.Empty;
                    if (inst != null)
                        instName = inst.Name;
                    foreach (var cashManagementEntity in investmentTrans)
                    {
                        securitySummaryReportDS.AddCapitalTranRow(clientName, clientId, clientCid, serviceType, cashManagementEntity.ID, bankAccount.CID, cashManagementEntity.AdministrationSystem, "CASH", instName, cashManagementEntity.SystemTransactionType, bankAccount.BankAccountEntity.AccountNumber, cashManagementEntity.TransactionDate,
                                                                  cashManagementEntity.TransactionDate, 0, 0, cashManagementEntity.TotalAmount, 0, cashManagementEntity.TotalAmount);
                    }
            
                }



                decimal investmentApplication = rangeTransactions.Where(tran => tran.Category == "Investment" && tran.SystemTransactionType == "Application").Sum(tran => tran.TotalAmount);
                decimal investmentRedemption = rangeTransactions.Where(tran => tran.Category == "Investment" && tran.SystemTransactionType == "Redemption").Sum(tran => tran.TotalAmount);
                decimal distributionIncome = distributionIncomeTrans.Sum(tran => tran.TotalAmount);
                decimal movement = holdingTotalClosing - holdingTotalOpening - investmentApplication - investmentRedemption - distributionIncome;
                string productName = bankAccount.BankAccountEntity.AccoutType.ToUpper();

                if (inst != null)
                    productName = inst.Name + " - " + productName;

                if (product != null)
                    productName = product.Name;

                IEnumerable<DistributionIncomeEntity> filteredDistribution = null;
                filteredDistribution = DistributionIncomes.Where(cliFundTrans => cliFundTrans.RecordDate.Value.Date >= securitySummaryReportDS.StartDate.Date && cliFundTrans.RecordDate.Value.Date <= securitySummaryReportDS.EndDate.Date);

                decimal closingBalanceAdjustment = 0;
                foreach (var tranDisCashItem in distributionIncomeTrans.ToList())
                {
                    var existingDistribution = filteredDistribution.Where(dis => Math.Floor(dis.NetCashDistribution.Value) == Math.Floor((double)tranDisCashItem.TotalAmount)).FirstOrDefault();
                    if (existingDistribution == null)
                    {
                        distributionIncomeTrans.Remove(tranDisCashItem);
                        closingBalanceAdjustment += tranDisCashItem.TotalAmount;
                    }
                }
                decimal distributionIncomeAdj = distributionIncomeTrans.Sum(tran => tran.TotalAmount);

                securitySummaryReportDS.AddSecuritySummaryRowBank("CASH", asset.Name, productName, securitySummaryReportDS.EndDate, holdingTotalOpening, investmentApplication, investmentRedemption, 0, 0, movement + distributionIncome,
                                                                  holdingTotalClosing, bankAccount.BankAccountEntity.AccountNumber, bankAccount.BankAccountEntity.BSB, string.Empty, holdingTotalClosing - closingBalanceAdjustment, movement + distributionIncomeAdj);
            }
        }

        private static void ManualTransactions(string clientName, string clientID, Guid clientCid, ObservableCollection<ClientManualAssetEntity> ClientManualAssetCollection, IOrganization orgCM, HoldingSummaryReportDS securitySummaryReportDS)
        {
            if (ClientManualAssetCollection != null)
            {
                var filteredManualAssetsGroup = ClientManualAssetCollection.GroupBy(man => man.InvestmentCode);

                foreach (var manualAssetGroup in filteredManualAssetsGroup)
                {
                    decimal closingUnits = 0;
                    decimal openingUnits = 0;
                    decimal closingBal = 0;
                    decimal unitPriceClosing = 0;
                    decimal openingBal = 0;
                    decimal openingBalUnitPrice = 0;

                    DateTime priceDate = securitySummaryReportDS.EndDate;

                    var systemManualAsset = orgCM.ManualAsset.Where(manList => manList.ID == manualAssetGroup.Key).FirstOrDefault();
                    var systemManualAssetPriceHistoryCls = systemManualAsset.ASXSecurity.Where(manualHIS => manualHIS.Date <= securitySummaryReportDS.EndDate).OrderByDescending(manualHis => manualHis.Date).FirstOrDefault();
                    if (systemManualAssetPriceHistoryCls != null)
                    {
                        unitPriceClosing = Convert.ToDecimal(systemManualAssetPriceHistoryCls.UnitPrice);
                        priceDate = systemManualAssetPriceHistoryCls.Date;
                    }

                    var systemManualAssetPriceHistoryOB = systemManualAsset.ASXSecurity.Where(manualHIS => manualHIS.Date <= securitySummaryReportDS.StartDate).OrderByDescending(manualHis => manualHis.Date).FirstOrDefault();
                    if (systemManualAssetPriceHistoryOB != null)
                        openingBalUnitPrice = Convert.ToDecimal(systemManualAssetPriceHistoryOB.UnitPrice);

                    closingUnits = manualAssetGroup.Where(man => man.TransactionDate <= securitySummaryReportDS.EndDate).Sum(man => man.UnitsOnHand);
                    closingBal = closingUnits * unitPriceClosing;

                    openingUnits = manualAssetGroup.Where(man => man.TransactionDate <= securitySummaryReportDS.StartDate).Sum(man => man.UnitsOnHand);
                    openingBal = openingUnits * openingBalUnitPrice;

                    var rangeUnits = manualAssetGroup.Where(man => man.TransactionDate.Date <= securitySummaryReportDS.EndDate.Date && man.TransactionDate.Date >= securitySummaryReportDS.StartDate.Date);

                    decimal applicationTotal = 0;
                    decimal redemptionTotal = 0;

                    foreach (ClientManualAssetEntity clientManualAssetEntity in rangeUnits)
                    {
                        if (clientManualAssetEntity.TransactionType == "Application" || clientManualAssetEntity.TransactionType == "Redemption" || clientManualAssetEntity.TransactionType == "Sale")
                        {
                            var systemManualAssetPriceHistory = systemManualAsset.ASXSecurity.Where(manualHIS => manualHIS.Date <= clientManualAssetEntity.TransactionDate).OrderByDescending(manualHis => manualHis.Date).FirstOrDefault();

                            decimal unitPrice = 0;
                            if (systemManualAssetPriceHistory != null)
                                unitPrice = Convert.ToDecimal(systemManualAssetPriceHistory.UnitPrice);

                            decimal netValue = unitPrice * clientManualAssetEntity.UnitsOnHand;

                            securitySummaryReportDS.AddCapitalTranRow(clientName, clientID, clientCid, "Manual Asset", clientManualAssetEntity.ID, Guid.Empty, systemManualAsset.AsxCode, "MA", systemManualAsset.CompanyName, clientManualAssetEntity.TransactionType, clientID, clientManualAssetEntity.TransactionDate,
                                clientManualAssetEntity.SettlementDate, clientManualAssetEntity.UnitsOnHand, unitPrice, netValue, 0, netValue);

                            if (clientManualAssetEntity.TransactionType == "Application")
                                applicationTotal += netValue;
                            else if (clientManualAssetEntity.TransactionType == "Redemption" || clientManualAssetEntity.TransactionType == "Sale")
                                applicationTotal += netValue;
                        }

                    }

                    decimal movement = closingBal - openingBal - applicationTotal - redemptionTotal;

                    securitySummaryReportDS.AddSecuritySummaryRow("MA", "", systemManualAsset.AsxCode, systemManualAsset.CompanyName, priceDate, openingUnits, openingBalUnitPrice, openingBal, applicationTotal, redemptionTotal, 0, 0, movement, closingUnits, unitPriceClosing, closingBal, closingBal, movement);
                }
            }
        }

        public void OnGetDataHoldingRptDataSetIndividual(HoldingSummaryReportDS data, string clientType, string clientID,Guid clientCid, string clientName, IdentityCM Parent, ICMBroker Broker, ClientIndividualEntity Entity)
        {
            DataTable clientSummaryTable = data.Tables[HoldingSummaryReportDS.CLIENTSUMMARYTABLE];
            DataTable contactTable = data.Tables[HoldingSummaryReportDS.CONTACTTABLE];

            DataRow clientSummaryRow = clientSummaryTable.NewRow();
            DataRow contactRow = contactTable.NewRow();

            CSVInstance csvinstance = new CSVInstance(Broker);
            ClientIndividualEntityCSV _entitycsv = csvinstance.GetEntityConcreteInstance(Entity);

            Common.AddressEntity addressEntity = new Common.AddressEntity();

            Common.AddressEntity mailAddressEntity = new Common.AddressEntity();

            if (_entitycsv.Applicants != null && _entitycsv.Applicants.Count > 0)
            {
                if (_entitycsv.Applicants.FirstOrDefault().ResidentialAddress != null)
                    addressEntity = _entitycsv.Applicants.FirstOrDefault().ResidentialAddress;

                if (_entitycsv.Applicants.FirstOrDefault().MailingAddress != null)
                    mailAddressEntity = _entitycsv.Applicants.FirstOrDefault().MailingAddress;
            }


            BasicClientDetails(clientType, clientID, clientName, Parent, Broker, Entity.ABN, string.Empty, Entity.Address, clientSummaryRow, addressEntity, mailAddressEntity);


            foreach (var contact in _entitycsv.Applicants)
            {
                contactRow[HoldingSummaryReportDS.CONTACT_TITLE] = contact.PersonalTitle;
                contactRow[HoldingSummaryReportDS.CONTACT_NAME] = contact.Fullname;
                contactRow[HoldingSummaryReportDS.CONTACT_BIZNO] = contact.WorkPhoneNumber;
                contactRow[HoldingSummaryReportDS.CONTACT_MOBNO] = contact.MobilePhoneNumber;
                contactRow[HoldingSummaryReportDS.CONTACT_HNO] = contact.HomePhoneNumber;
                contactRow[HoldingSummaryReportDS.CONTACT_FAXNO] = contact.Facsimile;
                contactRow[HoldingSummaryReportDS.CONTACT_EMAIL] = contact.EmailAddress;
            }

            clientSummaryTable.Rows.Add(clientSummaryRow);
            contactTable.Rows.Add(contactRow);

            IOrganization orgCM = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            HoldingSummaryReportDS securitySummaryReportDS = data as HoldingSummaryReportDS;

            ManualTransactions(clientName,clientID, clientCid, Entity.ClientManualAssetCollection, orgCM, securitySummaryReportDS);

            GetTransactionsInfoFromLinkProcess(clientName, Entity as IClientUMAData, clientID, clientCid, Broker, Entity.ListAccountProcess, orgCM, securitySummaryReportDS, Entity.DesktopBrokerAccounts);
        }
    }
}
