﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.DataSets;
using System.Data;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.CM.Group;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CM.Entity;
using System.Collections.ObjectModel;

namespace Oritax.TaxSimp.CM
{
    public class SecurityDataReportUtility
    {
        public void OnGetDataHoldingRptDataSetCorporate(SecuritySummaryReportDS data, string clientType, string clientID, string clientName, IdentityCM Parent, ICMBroker Broker, CorporateEntity Entity, Guid clientCID)
        {
            DataTable clientSummaryTable = data.Tables[SecuritySummaryReportDS.CLIENTSUMMARYTABLE];
            DataTable contactTable = data.Tables[SecuritySummaryReportDS.CONTACTTABLE];

            DataRow clientSummaryRow = clientSummaryTable.NewRow();
            DataRow contactRow = contactTable.NewRow();

            BasicClientDetails(clientType, clientID, clientName, Parent, Broker, Entity.ABN, Entity.ACN, Entity.Address, clientSummaryRow);

            CSVInstance csvinstance = new CSVInstance(Broker);
            CorporateCSV _entitycsv = csvinstance.GetEntityConcreteInstance(Entity);
            foreach (var contact in _entitycsv.Contacts)
            {
                contactRow[SecuritySummaryReportDS.CONTACT_TITLE] = contact.PersonalTitle;
                contactRow[SecuritySummaryReportDS.CONTACT_NAME] = contact.Fullname;
                contactRow[SecuritySummaryReportDS.CONTACT_BIZNO] = contact.WorkPhoneNumber;
                contactRow[SecuritySummaryReportDS.CONTACT_MOBNO] = contact.MobilePhoneNumber;
                contactRow[SecuritySummaryReportDS.CONTACT_HNO] = contact.HomePhoneNumber;
                contactRow[SecuritySummaryReportDS.CONTACT_FAXNO] = contact.Facsimile;
                contactRow[SecuritySummaryReportDS.CONTACT_EMAIL] = contact.EmailAddress;
            }

            clientSummaryTable.Rows.Add(clientSummaryRow);
            contactTable.Rows.Add(contactRow);

            IOrganization orgCM = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            SecuritySummaryReportDS securitySummaryReportDS = data as SecuritySummaryReportDS;

            ManualTransactions(clientName, clientCID, clientID, Entity.ClientManualAssetCollection, orgCM, securitySummaryReportDS);

            GetTransactionsInfoFromLinkProcess(clientName, clientCID, clientID, Broker, Entity.ListAccountProcess, orgCM, securitySummaryReportDS, Entity.DesktopBrokerAccounts);
        }

        private static void BasicClientDetails(string clientType, string clientID, string clientName, IdentityCM Parent, ICMBroker Broker, string ABN, string ACN, Common.DualAddressEntity Address, DataRow clientSummaryRow)
        {
            clientSummaryRow[HoldingRptDataSet.CLIENTID] = clientID;
            clientSummaryRow[HoldingRptDataSet.CLIENTNAME] = clientName;
            clientSummaryRow[HoldingRptDataSet.CIENTABN] = ABN;
            clientSummaryRow[HoldingRptDataSet.CLIENTACN] = ACN;
            clientSummaryRow[HoldingRptDataSet.CLIENTTYPE] = clientType;

            if (Parent != null && Parent.Clid != Guid.Empty && Parent.Csid != Guid.Empty)
            {
                AdvisorCM adviserUnit = Broker.GetCMImplementation(Parent.Clid, Parent.Csid) as AdvisorCM;
                if (adviserUnit != null)
                    clientSummaryRow[HoldingRptDataSet.ADVISERFULLNAME] = adviserUnit.AdvisorEntity.FullName;
                Broker.ReleaseBrokerManagedComponent(adviserUnit);
            }

            if (Address.RegisteredAddress.Addressline1 != string.Empty)
            {
                clientSummaryRow[HoldingRptDataSet.ADDRESSLINE1] = Address.RegisteredAddress.Addressline1;
                clientSummaryRow[HoldingRptDataSet.ADDRESSLINE2] = Address.RegisteredAddress.Addressline2;
                clientSummaryRow[HoldingRptDataSet.SUBURB] = Address.RegisteredAddress.Suburb;
                clientSummaryRow[HoldingRptDataSet.STATE] = Address.RegisteredAddress.State;
                clientSummaryRow[HoldingRptDataSet.POSTCODE] = Address.RegisteredAddress.PostCode;
                clientSummaryRow[HoldingRptDataSet.COUNTRY] = Address.RegisteredAddress.Country;
            }
            else if (Address.BusinessAddress.Addressline1 != string.Empty)
            {
                clientSummaryRow[HoldingRptDataSet.ADDRESSLINE1] = Address.BusinessAddress.Addressline1;
                clientSummaryRow[HoldingRptDataSet.ADDRESSLINE2] = Address.BusinessAddress.Addressline2;
                clientSummaryRow[HoldingRptDataSet.SUBURB] = Address.BusinessAddress.Suburb;
                clientSummaryRow[HoldingRptDataSet.STATE] = Address.BusinessAddress.State;
                clientSummaryRow[HoldingRptDataSet.POSTCODE] = Address.BusinessAddress.PostCode;
                clientSummaryRow[HoldingRptDataSet.COUNTRY] = Address.BusinessAddress.Country;
            }
            else if (Address.MailingAddress.Addressline1 != string.Empty)
            {
                clientSummaryRow[HoldingRptDataSet.ADDRESSLINE1] = Address.MailingAddress.Addressline1;
                clientSummaryRow[HoldingRptDataSet.ADDRESSLINE2] = Address.MailingAddress.Addressline2;
                clientSummaryRow[HoldingRptDataSet.SUBURB] = Address.MailingAddress.Suburb;
                clientSummaryRow[HoldingRptDataSet.STATE] = Address.MailingAddress.State;
                clientSummaryRow[HoldingRptDataSet.POSTCODE] = Address.MailingAddress.PostCode;
                clientSummaryRow[HoldingRptDataSet.COUNTRY] = Address.MailingAddress.Country;
            }
            else if (Address.ResidentialAddress.Addressline1 != string.Empty)
            {
                clientSummaryRow[HoldingRptDataSet.ADDRESSLINE1] = Address.ResidentialAddress.Addressline1;
                clientSummaryRow[HoldingRptDataSet.ADDRESSLINE2] = Address.ResidentialAddress.Addressline2;
                clientSummaryRow[HoldingRptDataSet.SUBURB] = Address.ResidentialAddress.Suburb;
                clientSummaryRow[HoldingRptDataSet.STATE] = Address.ResidentialAddress.State;
                clientSummaryRow[HoldingRptDataSet.POSTCODE] = Address.ResidentialAddress.PostCode;
                clientSummaryRow[HoldingRptDataSet.COUNTRY] = Address.ResidentialAddress.Country;
            }

        }

        private static void GetTransactionsInfoFromLinkProcess(string clientName, Guid clientCID, string clientID, ICMBroker Broker, List<AccountProcessTaskEntity> ListAccountProcess, IOrganization orgCM, SecuritySummaryReportDS securitySummaryReportDS, List<Oritax.TaxSimp.Common.IdentityCMDetail> DesktopBrokerAccounts)
        {
            if (ListAccountProcess != null)
            {
                IEnumerable<HoldingTransactions> TotalHoldingTransactions = new ObservableCollection<HoldingTransactions>();
                if (securitySummaryReportDS.ServiceTypes == ServiceTypes.All)
                {
                    foreach (Oritax.TaxSimp.Common.IdentityCMDetail desktopBrokerAccounts in DesktopBrokerAccounts)
                    {
                        DesktopBrokerAccountCM desktopBrokerAccountCM = Broker.GetCMImplementation(desktopBrokerAccounts.Clid,
                                                                            desktopBrokerAccounts.Csid) as DesktopBrokerAccountCM;
                        if (desktopBrokerAccountCM != null)
                            TotalHoldingTransactions = TotalHoldingTransactions.Union(desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions);
                    }


                    DesktopBrokerTransactions(clientName, clientCID, clientID, orgCM, securitySummaryReportDS.ValuationDate, TotalHoldingTransactions, securitySummaryReportDS);
                }

                foreach (var accountProcessTaskEntity in ListAccountProcess)
                {
                    if (accountProcessTaskEntity.LinkedEntity != null)
                    {
                        var asset = orgCM.Assets.Where(var => var.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                        var model = orgCM.Model.Where(mod => mod.ID == accountProcessTaskEntity.ModelID).FirstOrDefault();
                        if (model != null)
                        {
                            var linkedAsset = model.Assets.Where(ast => ast.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                            if (linkedAsset != null && securitySummaryReportDS.ServiceTypes == ServiceTypes.All)
                            {
                                var product = linkedAsset.Products.Where(pro => pro.ID == accountProcessTaskEntity.TaskID).FirstOrDefault();

                                if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.BankAccount && accountProcessTaskEntity.LinkedEntity.Clid != Guid.Empty)
                                    BankAccountTransctions(clientName, clientCID, clientID, Broker, securitySummaryReportDS, accountProcessTaskEntity, asset, model, product);
                                else if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.TermDepositAccount && accountProcessTaskEntity.LinkedEntity.Clid != Guid.Empty)
                                    TDTransactions(clientName, clientCID, clientID, Broker, orgCM, securitySummaryReportDS, accountProcessTaskEntity, asset, model, product);
                                else if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.ManagedInvestmentSchemesAccount)
                                    ManageInvestmentSchemes(clientName, clientCID, clientID, Broker, orgCM, securitySummaryReportDS, accountProcessTaskEntity, asset, model, product);

                            }
                            else if (linkedAsset != null && model.ServiceType == securitySummaryReportDS.ServiceTypes)
                            {
                                var product = linkedAsset.Products.Where(pro => pro.ID == accountProcessTaskEntity.TaskID).FirstOrDefault();

                                if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.BankAccount && accountProcessTaskEntity.LinkedEntity.Clid != Guid.Empty)
                                    BankAccountTransctions(clientName, clientCID, clientID, Broker, securitySummaryReportDS, accountProcessTaskEntity, asset, model, product);
                                else if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.TermDepositAccount && accountProcessTaskEntity.LinkedEntity.Clid != Guid.Empty)
                                    TDTransactions(clientName, clientCID, clientID, Broker, orgCM, securitySummaryReportDS, accountProcessTaskEntity, asset, model, product);
                                else if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.ManagedInvestmentSchemesAccount)
                                    ManageInvestmentSchemes(clientName, clientCID, clientID, Broker, orgCM, securitySummaryReportDS, accountProcessTaskEntity, asset, model, product);
                                else if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.DesktopBrokerAccount)
                                {
                                    DesktopBrokerAccountCM desktopBrokerAccountCM = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid,
                                                                                        accountProcessTaskEntity.LinkedEntity.Csid) as DesktopBrokerAccountCM;
                                    if (desktopBrokerAccountCM != null)
                                    {
                                        TotalHoldingTransactions = TotalHoldingTransactions.Union(desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions);
                                        DesktopBrokerTransactions(clientName, clientCID, clientID, orgCM, securitySummaryReportDS.ValuationDate, TotalHoldingTransactions, securitySummaryReportDS);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private static void ManageInvestmentSchemes(string clientName, Guid clientCID, string clientID, ICMBroker Broker, IOrganization orgCM, SecuritySummaryReportDS securitySummaryReportDS, AccountProcessTaskEntity accountProcessTaskEntity, AssetEntity asset, ModelEntity model, ProductEntity product)
        {
            ManagedInvestmentSchemesAccountCM managedInvestmentSchemesAccountCM = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as ManagedInvestmentSchemesAccountCM;
            if (managedInvestmentSchemesAccountCM != null)
            {
                var linkedMISAccount = managedInvestmentSchemesAccountCM.ManagedInvestmentSchemesAccountEntity.FundAccounts.Where(fa => product.TaskDescription.Contains(fa.Code)).FirstOrDefault();
                var filteredHoldingTransactionsByValuationDate = linkedMISAccount.FundTransactions.Where(cliFundTrans => cliFundTrans.TradeDate <= securitySummaryReportDS.ValuationDate);
                var clientFundTransactions = filteredHoldingTransactionsByValuationDate.Where(cliFundTrans => cliFundTrans.ClientID == clientID);
                decimal totalShares = clientFundTransactions.Sum(ft => ft.Shares);

                var linkedmissec = orgCM.Securities.Where(missec => missec.AsxCode == linkedMISAccount.Code).FirstOrDefault();

                decimal currentValue = 0;
                decimal unitPrice = 0;
                DateTime priceDate = securitySummaryReportDS.ValuationDate;

                if (linkedmissec != null)
                {
                    var latestMisPriceObject = linkedmissec.ASXSecurity.Where(mishis => mishis.Date <= securitySummaryReportDS.ValuationDate).OrderByDescending(mishis => mishis.Date).FirstOrDefault();
                    if (latestMisPriceObject != null)
                    {
                        unitPrice = Convert.ToDecimal(latestMisPriceObject.UnitPrice);
                        priceDate = latestMisPriceObject.Date;
                    }
                    currentValue = totalShares * unitPrice;

                    decimal unsettledOrders = linkedMISAccount.UnsettledOrders.Where(fundTrans => fundTrans.ClientID == clientID).Sum(fundTrans => fundTrans.UnsettledOrders);
                    decimal total = currentValue - unsettledOrders;

                    securitySummaryReportDS.AddSecuritySummaryRow(Enumeration.GetDescription(SecurityType.Funds), linkedMISAccount.Code, linkedMISAccount.Description, totalShares, unitPrice, priceDate, currentValue, clientCID, clientID, clientName);
                }
            }
        }

        private static void DesktopBrokerTransactions(string clientName, Guid clientCID, string clientID, IOrganization orgCM, DateTime valuationDate, IEnumerable<HoldingTransactions> holdingTransactions, SecuritySummaryReportDS securitySummaryReportDS)
        {
            var filteredHoldingTransactionsByOpening = holdingTransactions.Where(holdTrans =>
                                                                                              holdTrans.TradeDate <= valuationDate);


            var groupedSecurities = filteredHoldingTransactionsByOpening.GroupBy(holdTran => holdTran.InvestmentCode);

            foreach (var security in groupedSecurities)
            {
                decimal totalSecHoldingUnits = security.Sum(sec => sec.Units);
                var asxBroker = orgCM.Securities.Where(sec => sec.AsxCode == security.Key).FirstOrDefault();
                var hisSecurity = asxBroker.ASXSecurity.Where(secpri => secpri.Date <= valuationDate).OrderByDescending(secpri => secpri.Date).FirstOrDefault();
                decimal unitPrice = 0;
                DateTime priceDate = securitySummaryReportDS.ValuationDate;

                if (hisSecurity != null)
                {
                    unitPrice = Convert.ToDecimal(hisSecurity.UnitPrice);
                    priceDate = hisSecurity.Date;
                }

                decimal currentValue = totalSecHoldingUnits * unitPrice;
                securitySummaryReportDS.AddSecuritySummaryRow(Enumeration.GetDescription(SecurityType.Sec), asxBroker.AsxCode, asxBroker.CompanyName, totalSecHoldingUnits, unitPrice, priceDate, currentValue, clientCID, clientID, clientName);
            }
        }

        private static void TDTransactions(string clientName, Guid clientCID, string clientID, ICMBroker Broker, IOrganization orgCM, SecuritySummaryReportDS securitySummaryReportDS, AccountProcessTaskEntity accountProcessTaskEntity, AssetEntity asset, ModelEntity model, ProductEntity product)
        {
            BankAccountCM bankAccount = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as BankAccountCM;

            if (bankAccount != null)
            {

                decimal holdingTotal = 0;

                var filteredTransactions = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate <= securitySummaryReportDS.ValuationDate);
                var tdGroupsType = filteredTransactions.GroupBy(cashMan => cashMan.TransactionType);

                foreach (var tdType in tdGroupsType)
                {
                    var groupByIns = tdType.GroupBy(tdtypetran => tdtypetran.InstitutionID);

                    foreach (var groupins in groupByIns)
                    {
                        var transaction = groupins.FirstOrDefault();
                        decimal holding = groupins.Sum(inssum => inssum.Amount);

                        var inst = orgCM.Institution.Where(ins => ins.ID == groupins.Key).FirstOrDefault();

                        string instName = string.Empty;
                        if (inst != null)
                            instName = inst.Name;

                        holdingTotal += holding;
                    }
                }

                string productName = "TD Product not found";

                if (product != null)
                    productName = product.Name;

                securitySummaryReportDS.AddSecuritySummaryRowBankTransaction(Enumeration.GetDescription(SecurityType.TD), asset.Name, productName, 0, 0, securitySummaryReportDS.ValuationDate, holdingTotal, clientCID, clientID, clientName);
            }
        }

        private static void BankAccountTransctions(string clientName, Guid clientCID, string clientID, ICMBroker Broker, SecuritySummaryReportDS securitySummaryReportDS, AccountProcessTaskEntity accountProcessTaskEntity, AssetEntity asset, ModelEntity model, ProductEntity product)
        {
            if (accountProcessTaskEntity.LinkedEntity.Clid == Guid.Empty)
            {
                accountProcessTaskEntity.LinkedEntity.Clid = product.EntityId.Clid;
                accountProcessTaskEntity.LinkedEntity.Csid = product.EntityId.Csid;
            }

            BankAccountCM bankAccount = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as BankAccountCM;
            var filteredTransactions = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate <= securitySummaryReportDS.ValuationDate);
            decimal holdingTotal = filteredTransactions.Sum(calc => calc.TotalAmount);

            securitySummaryReportDS.AddSecuritySummaryRowBankTransactionWithBGLCode(bankAccount.BankAccountEntity.BGLCodeCashAccount, Enumeration.GetDescription(SecurityType.Cash), asset.Name, product.Name + " - " + bankAccount.BankAccountEntity.AccountNumber, 0, 0, securitySummaryReportDS.ValuationDate, holdingTotal, clientCID, clientID, clientName);

        }

        private static void ManualTransactions(string clientName, Guid clientCID, string clientID, ObservableCollection<ClientManualAssetEntity> ClientManualAssetCollection, IOrganization orgCM, SecuritySummaryReportDS securitySummaryReportDS)
        {
            if (ClientManualAssetCollection != null)
            {
                var filteredManualAssets = ClientManualAssetCollection.Where(man => man.TransactionDate <= securitySummaryReportDS.ValuationDate);
                var filteredManualAssetsGroup = filteredManualAssets.GroupBy(sec => sec.InvestmentCode);

                foreach (var manualAssetGroupItem in filteredManualAssetsGroup)
                {
                    decimal currentValue = 0;
                    decimal unitPrice = 0;
                    DateTime priceDate = securitySummaryReportDS.ValuationDate;

                    var systemManualAsset = orgCM.ManualAsset.Where(manList => manList.ID == manualAssetGroupItem.Key).FirstOrDefault();
                    var systemManualAssetPriceHistory = systemManualAsset.ASXSecurity.Where(manualHIS => manualHIS.Date <= securitySummaryReportDS.ValuationDate).OrderByDescending(manualHis => manualHis.Date).FirstOrDefault();
                    if (systemManualAssetPriceHistory != null)
                    {
                        unitPrice = Convert.ToDecimal(systemManualAssetPriceHistory.UnitPrice);
                        priceDate = systemManualAssetPriceHistory.Date;
                    }
					 decimal unitsInHand = manualAssetGroupItem.Sum(t => t.UnitsOnHand);
   currentValue = unitsInHand * unitPrice;
                   securitySummaryReportDS.AddSecuritySummaryRow(Enumeration.GetDescription(SecurityType.ManualAsset), "Manual Asset", systemManualAsset.AsxCode + "- " + systemManualAsset.CompanyName, unitsInHand, unitPrice, priceDate, currentValue, clientCID, clientID, clientName); 
                    
                }
            }
        }

        public void OnGetDataHoldingRptDataSetIndividual(SecuritySummaryReportDS data, string clientType, string clientID, string clientName, IdentityCM Parent, ICMBroker Broker, ClientIndividualEntity Entity, Guid clientCID)
        {
            DataTable clientSummaryTable = data.Tables[SecuritySummaryReportDS.CLIENTSUMMARYTABLE];
            DataTable contactTable = data.Tables[SecuritySummaryReportDS.CONTACTTABLE];

            DataRow clientSummaryRow = clientSummaryTable.NewRow();
            DataRow contactRow = contactTable.NewRow();

            BasicClientDetails(clientType, clientID, clientName, Parent, Broker, Entity.ABN, string.Empty, Entity.Address, clientSummaryRow);

            CSVInstance csvinstance = new CSVInstance(Broker);
            ClientIndividualEntityCSV _entitycsv = csvinstance.GetEntityConcreteInstance(Entity);
            foreach (var contact in _entitycsv.Applicants)
            {
                contactRow[SecuritySummaryReportDS.CONTACT_TITLE] = contact.PersonalTitle;
                contactRow[SecuritySummaryReportDS.CONTACT_NAME] = contact.Fullname;
                contactRow[SecuritySummaryReportDS.CONTACT_BIZNO] = contact.WorkPhoneNumber;
                contactRow[SecuritySummaryReportDS.CONTACT_MOBNO] = contact.MobilePhoneNumber;
                contactRow[SecuritySummaryReportDS.CONTACT_HNO] = contact.HomePhoneNumber;
                contactRow[SecuritySummaryReportDS.CONTACT_FAXNO] = contact.Facsimile;
                contactRow[SecuritySummaryReportDS.CONTACT_EMAIL] = contact.EmailAddress;
            }

            clientSummaryTable.Rows.Add(clientSummaryRow);
            contactTable.Rows.Add(contactRow);

            IOrganization orgCM = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            SecuritySummaryReportDS securitySummaryReportDS = data as SecuritySummaryReportDS;

            ManualTransactions(clientName, clientCID, clientID, Entity.ClientManualAssetCollection, orgCM, securitySummaryReportDS);

            GetTransactionsInfoFromLinkProcess(clientName, clientCID, clientID, Broker, Entity.ListAccountProcess, orgCM, securitySummaryReportDS, Entity.DesktopBrokerAccounts);
        }
    }
}
