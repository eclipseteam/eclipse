﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.DataSets;
using System.Data;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.CM.Group;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CM.Entity;
using System.Collections.ObjectModel;
using Oritax.TaxSimp.CM.OrganizationUnit;
using DualAddressEntity = Oritax.TaxSimp.Common.DualAddressEntity;

namespace Oritax.TaxSimp.CM
{
    public class HoldingDataReportUtility
    {
        public void OnGetDataHoldingRptDataSetCorporate(HoldingRptDataSet data, string clientType, string clientID, string clientName, IdentityCM Parent, ICMBroker Broker, CorporateEntity Entity, string clientApplicabilityType, Guid clientCID)
        {
            DataTable clientSummaryTable = data.Tables[HoldingRptDataSet.CLIENTSUMMARYTABLE];
            DataTable contactTable = data.Tables[HoldingRptDataSet.CONTACTTABLE];

            DataRow clientSummaryRow = clientSummaryTable.NewRow();
            DataRow contactRow = contactTable.NewRow();

            BasicClientDetails(clientType, clientID, clientName, Parent, Broker, Entity.ABN, Entity.ACN, Entity.Address, clientApplicabilityType, clientSummaryRow,clientCID);

            foreach (var contactIdentity in Entity.Contacts)
            {
                IOrganizationUnit iOrgUnit = Broker.GetCMImplementation(contactIdentity.Clid, contactIdentity.Csid) as IOrganizationUnit;
                Oritax.TaxSimp.Common.IndividualEntity contact = iOrgUnit.ClientEntity as Oritax.TaxSimp.Common.IndividualEntity;

                contactRow[HoldingRptDataSet.CONTACT_TITLE] = contact.PersonalTitle;
                contactRow[HoldingRptDataSet.CONTACT_NAME] = contact.Fullname;
                contactRow[HoldingRptDataSet.CONTACT_BIZNO] = contact.WorkPhoneNumber;
                contactRow[HoldingRptDataSet.CONTACT_MOBNO] = contact.MobilePhoneNumber;
                contactRow[HoldingRptDataSet.CONTACT_HNO] = contact.HomePhoneNumber;
                contactRow[HoldingRptDataSet.CONTACT_FAXNO] = contact.Facsimile;
                contactRow[HoldingRptDataSet.CONTACT_EMAIL] = contact.EmailAddress;
            }

            clientSummaryTable.Rows.Add(clientSummaryRow);
            contactTable.Rows.Add(contactRow);

            IOrganization orgCM = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            HoldingRptDataSet holdingReportDataSet = data as HoldingRptDataSet;

            if (holdingReportDataSet.ServiceTypes == ServiceTypes.None)
                ManualTransactions(clientName, clientID, Entity.ClientManualAssetCollection, orgCM, holdingReportDataSet);

            string currUserName = "Administrator";
            if (Broker.UserContext != null)
                currUserName = Broker.UserContext.Identity.Name;
            var user = new UserEntity() { IsWebUser = true, CurrentUserName = currUserName };
            SettledUnsetteledDS ds = new SettledUnsetteledDS
            {
                Unit = new Data.OrganizationUnit
                {
                    ClientId = clientID,
                    CurrentUser = user,
                },
                CommandType = DatasetCommandTypes.Unsettle,
            };

            if (holdingReportDataSet.CheckForUnsettledCM)
                ds = SettledUnsettledUtilities.GetAllUnsetteledForClient(clientID, Broker);
            
            GetTransactionsInfoFromLinkProcess(clientName, clientID, Broker, Entity.ListAccountProcess, orgCM, holdingReportDataSet, holdingReportDataSet.TDBreakDownTable, ds);
        }

        public static void BasicClientDetails(string clientType, string clientID, string clientName, IdentityCM Parent, ICMBroker Broker, string ABN, string ACN, DualAddressEntity Address, string clientApplicabilityType, DataRow clientSummaryRow, Guid clientCID)
        {
            clientSummaryRow[HoldingRptDataSet.CLIENTCID] = clientCID;
            clientSummaryRow[HoldingRptDataSet.CLIENTID] = clientID;
            clientSummaryRow[HoldingRptDataSet.CLIENTNAME] = clientName;
            clientSummaryRow[HoldingRptDataSet.CIENTABN] = ABN;
            clientSummaryRow[HoldingRptDataSet.CLIENTACN] = ACN;
            clientSummaryRow[HoldingRptDataSet.CLIENTTYPE] = clientType;
            clientSummaryRow[HoldingRptDataSet.CLIENTAPPLICABILITYTYPE] = clientApplicabilityType;

            if (Parent != null && Parent.Clid != Guid.Empty && Parent.Csid != Guid.Empty)
            {
                AdvisorCM adviserUnit = Broker.GetCMImplementation(Parent.Clid, Parent.Csid) as AdvisorCM;
                if (adviserUnit != null)
                {
                    clientSummaryRow[HoldingRptDataSet.ADVISERFULLNAME] = adviserUnit.AdvisorEntity.FullName;
                    clientSummaryRow[HoldingRptDataSet.ADVISERID] = adviserUnit.ClientId;
                    clientSummaryRow[HoldingRptDataSet.ADVISERCID] = adviserUnit.CID;
                }
                else
                {
                    clientSummaryRow[HoldingRptDataSet.ADVISERFULLNAME] = string.Empty;
                    clientSummaryRow[HoldingRptDataSet.ADVISERID] = string.Empty;
                    clientSummaryRow[HoldingRptDataSet.ADVISERCID] = string.Empty;
                }
                Broker.ReleaseBrokerManagedComponent(adviserUnit);
            }
            else
            {
                clientSummaryRow[HoldingRptDataSet.ADVISERFULLNAME] = string.Empty;
                clientSummaryRow[HoldingRptDataSet.ADVISERID] = string.Empty;
                clientSummaryRow[HoldingRptDataSet.ADVISERCID] = string.Empty;
            }
            switch (clientType.ToLower())
            {
                case "corporate smsf trustee":
                case "corporation private":
                case "corporation public":
                case "e-clipse super":
                case "other corporate trustee":
                case "other individual trustee":
                case "individual smsf trustee":
                case "clientsmsfindividualtrustee":
                case "clientindividual":
                case "clientsmsfcorporatetrustee":
                case "trust - individual trustee":
                case "trust - corporate trustee":
                    clientSummaryRow[HoldingRptDataSet.ADDRESSLINE1] = Address.BusinessAddress.Addressline1;
                    clientSummaryRow[HoldingRptDataSet.ADDRESSLINE2] = Address.BusinessAddress.Addressline2;
                    clientSummaryRow[HoldingRptDataSet.COUNTRY] = Address.BusinessAddress.Country;
                    clientSummaryRow[HoldingRptDataSet.SUBURB] = Address.BusinessAddress.Suburb;
                    clientSummaryRow[HoldingRptDataSet.POSTCODE] = Address.BusinessAddress.PostCode;
                    break;
                case "individual":
                case "client individual":
                case "joint":
                    clientSummaryRow[HoldingRptDataSet.ADDRESSLINE1] = Address.ResidentialAddress.Addressline1;
                    clientSummaryRow[HoldingRptDataSet.ADDRESSLINE2] = Address.ResidentialAddress.Addressline2;
                    clientSummaryRow[HoldingRptDataSet.COUNTRY] = Address.ResidentialAddress.Country;
                    clientSummaryRow[HoldingRptDataSet.SUBURB] = Address.ResidentialAddress.Suburb;
                    clientSummaryRow[HoldingRptDataSet.POSTCODE] = Address.ResidentialAddress.PostCode;
                    break;

            }

        }

        private static void GetTransactionsInfoFromLinkProcess(string clientName, string clientID, ICMBroker Broker, List<AccountProcessTaskEntity> ListAccountProcess, IOrganization orgCM, HoldingRptDataSet holdingReportDataSet, DataTable TDBreakDownTable, SettledUnsetteledDS ds)
        {
            GetUnlistedSecurity(clientName, clientID, Broker, ListAccountProcess, orgCM, holdingReportDataSet, ds);
            GetUnlistedSecurityMIS(clientName, clientID, Broker, ListAccountProcess, orgCM, holdingReportDataSet, ds);
            if (holdingReportDataSet.ServiceTypes == ServiceTypes.DoItForMe)
            {

                if (ListAccountProcess != null && holdingReportDataSet.ServiceTypes == ServiceTypes.DoItForMe)
                {
                    foreach (var accountProcessTaskEntity in ListAccountProcess)
                    {
                        if (accountProcessTaskEntity.LinkedEntity != null)
                        {
                            var asset = orgCM.Assets.Where(var => var.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                            var model = orgCM.Model.Where(mod => mod.ID == accountProcessTaskEntity.ModelID).FirstOrDefault();
                            if (model != null && model.ServiceType == ServiceTypes.DoItForMe)
                                SetAccountProcessTaskEntity(clientName, clientID, Broker, orgCM, holdingReportDataSet, TDBreakDownTable, accountProcessTaskEntity, asset, model, ds);
                        }
                    }
                }
            }
            else if (holdingReportDataSet.ServiceTypes == ServiceTypes.DoItWithMe)
            {

                if (ListAccountProcess != null && holdingReportDataSet.ServiceTypes == ServiceTypes.DoItWithMe)
                {
                    foreach (var accountProcessTaskEntity in ListAccountProcess)
                    {
                        if (accountProcessTaskEntity.LinkedEntity != null)
                        {
                            var asset = orgCM.Assets.Where(var => var.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                            var model = orgCM.Model.Where(mod => mod.ID == accountProcessTaskEntity.ModelID).FirstOrDefault();
                            if (model != null && model.ServiceType == ServiceTypes.DoItWithMe)
                                SetAccountProcessTaskEntity(clientName, clientID, Broker, orgCM, holdingReportDataSet, TDBreakDownTable, accountProcessTaskEntity, asset, model, ds);
                        }
                    }
                }
            }
            else if (holdingReportDataSet.ServiceTypes == ServiceTypes.DoItYourSelf)
            {

                if (ListAccountProcess != null && holdingReportDataSet.ServiceTypes == ServiceTypes.DoItYourSelf)
                {
                    foreach (var accountProcessTaskEntity in ListAccountProcess)
                    {
                        if (accountProcessTaskEntity.LinkedEntity != null)
                        {
                            var asset = orgCM.Assets.Where(var => var.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                            var model = orgCM.Model.Where(mod => mod.ID == accountProcessTaskEntity.ModelID).FirstOrDefault();
                            if (model != null && model.ServiceType == ServiceTypes.DoItYourSelf)
                                SetAccountProcessTaskEntity(clientName, clientID, Broker, orgCM, holdingReportDataSet, TDBreakDownTable, accountProcessTaskEntity, asset, model, ds);
                        }
                    }
                }
            }
            else
            {
                foreach (var accountProcessTaskEntity in ListAccountProcess)
                {
                    if (accountProcessTaskEntity.LinkedEntity != null)
                    {
                        var asset = orgCM.Assets.Where(var => var.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                        var model = orgCM.Model.Where(mod => mod.ID == accountProcessTaskEntity.ModelID).FirstOrDefault();
                        if (model != null)
                            SetAccountProcessTaskEntity(clientName, clientID, Broker, orgCM, holdingReportDataSet, TDBreakDownTable, accountProcessTaskEntity, asset, model, ds);
                    }
                }
            }
        }

        private static void SetAccountProcessTaskEntity(string clientName, string clientID, ICMBroker Broker, IOrganization orgCM, HoldingRptDataSet holdingReportDataSet, DataTable TDBreakDownTable, AccountProcessTaskEntity accountProcessTaskEntity, AssetEntity asset, ModelEntity model, SettledUnsetteledDS ds)
        {
            if (model != null)
            {
                if (accountProcessTaskEntity.LinkedEntity == null || accountProcessTaskEntity.LinkedEntity.Clid == Guid.Empty)
                    return;
                var linkedAsset = model.Assets.Where(ast => ast.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                if (linkedAsset != null)
                {
                    var product = linkedAsset.Products.Where(pro => pro.ID == accountProcessTaskEntity.TaskID).FirstOrDefault();

                    if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.BankAccount)
                        BankAccountTransctions(clientName, clientID, Broker, holdingReportDataSet, accountProcessTaskEntity, asset, model, product, ds);
                    else if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.TermDepositAccount)
                        TDTransactions(clientName, clientID, Broker, orgCM, holdingReportDataSet, TDBreakDownTable, accountProcessTaskEntity, asset, model, product, ds);
                    else if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.DesktopBrokerAccount)
                        DesktopBrokerTransactions(clientName, clientID, Broker, orgCM, holdingReportDataSet, accountProcessTaskEntity, asset, model, product, ds);
                    else if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.ManagedInvestmentSchemesAccount)
                        ManageInvestmentSchemes(clientName, clientID, Broker, orgCM, holdingReportDataSet, accountProcessTaskEntity, asset, model, product, ds);
                }
            }
        }

        private static void GetUnlistedSecurity(string clientName, string clientID, ICMBroker Broker, List<AccountProcessTaskEntity> ListAccountProcess, IOrganization orgCM, HoldingRptDataSet holdingReportDataSet, SettledUnsetteledDS settledUnsetteledDs)
        {
            var groupedAccountProcessByModel = ListAccountProcess.Where(accProcess => accProcess.LinkedEntityType == OrganizationType.DesktopBrokerAccount).GroupBy(accProcess => accProcess.ModelID);

            foreach (var groupedModel in groupedAccountProcessByModel)
            {
                bool processUnlistedAccounts = false;
                var model = orgCM.Model.Where(mod => mod.ID == groupedModel.Key).FirstOrDefault();
                ObservableCollection<ProductSecurityDetail> ProductDetails = new ObservableCollection<ProductSecurityDetail>();

                foreach (var accountProcessEntity in groupedModel)
                {
                    if (accountProcessEntity.LinkedEntity.Clid != Guid.Empty)
                    {
                        var asset = orgCM.Assets.Where(var => var.ID == accountProcessEntity.AssetID).FirstOrDefault();
                        var linkedAsset = model.Assets.Where(ast => ast.ID == accountProcessEntity.AssetID).FirstOrDefault();
                        if (linkedAsset != null)
                        {
                            var product = linkedAsset.Products.Where(pro => pro.ID == accountProcessEntity.TaskID).FirstOrDefault();
                            if (product != null)
                            {
                                if (product.ProductSecuritiesId != Guid.Empty)
                                {
                                    processUnlistedAccounts = true;
                                    var productSecurity = orgCM.ProductSecurities.Where(proseclist => proseclist.ID == product.ProductSecuritiesId).FirstOrDefault();

                                    foreach (var productSecurityDetail in productSecurity.Details)
                                    {
                                        ProductDetails.Add(productSecurityDetail);
                                    }
                                }
                            }
                        }
                    }
                }

                if (processUnlistedAccounts)
                {
                    decimal totalHolding = 0;
                    Guid productID = Guid.NewGuid();
                    DesktopBrokerAccountCM desktopBrokerAccountCM = Broker.GetCMImplementation(groupedModel.FirstOrDefault().LinkedEntity.Clid,
                                                                                groupedModel.FirstOrDefault().LinkedEntity.Csid) as DesktopBrokerAccountCM;

                    var filteredHoldingTransactionsUnlistedByValuationDate = desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Where(holdTrans =>
                                                                                                        holdTrans.TradeDate <= holdingReportDataSet.ValuationDate);
                    var groupedUnlistedSecurities = filteredHoldingTransactionsUnlistedByValuationDate.GroupBy(holdTran => holdTran.InvestmentCode);

                    DateTime priceAtDate = DateTime.Now;

                    foreach (var security in groupedUnlistedSecurities)
                    {
                        var asxBroker = orgCM.Securities.Where(sec => sec.AsxCode == security.Key).FirstOrDefault();
                        ProductSecurityDetail prosecdetail = null;
                        if (asxBroker != null)
                        {
                            prosecdetail = ProductDetails.Where(prosec => prosec.SecurityCodeId == asxBroker.ID).FirstOrDefault();

                            if (prosecdetail == null)
                            {
                                decimal totalSecHoldingUnits = 0;

                                foreach (HoldingTransactions holdingTran in security)
                                {
                                    if (holdingTran != null)
                                        totalSecHoldingUnits += holdingTran.Units;
                                }

                                ASXSecurityEntity hisSecurity = null;
                                hisSecurity = asxBroker.ASXSecurity.Where(secpri => secpri.Date <= holdingReportDataSet.ValuationDate).OrderByDescending(secpri => secpri.Date).FirstOrDefault();
                                decimal unitPrice = 0;

                                if (hisSecurity != null)
                                {
                                    priceAtDate = hisSecurity.Date;
                                    unitPrice = Convert.ToDecimal(hisSecurity.UnitPrice);
                                }

                                decimal unsettledBuyForCode;
                                decimal unsettledSellForCode;
                                decimal unsettledBuyunitsForCode;
                                decimal unsettledSellunitsForCode;
                                decimal unsettledSellByUnitsForCode;
                                decimal unsettledSellByAmountForCode;
                                SettledUnsettledUtilities.GetASXUnsettledAmountNUnitsForInvementCode(settledUnsetteledDs, asxBroker.AsxCode, productID, desktopBrokerAccountCM.CID, desktopBrokerAccountCM.DesktopBrokerAccountEntity.AccountNumber, out unsettledBuyForCode, out unsettledSellForCode, out unsettledBuyunitsForCode, out unsettledSellunitsForCode, out unsettledSellByUnitsForCode, out unsettledSellByAmountForCode);

                                decimal currentValue = totalSecHoldingUnits * unitPrice;

                                holdingReportDataSet.AddProductDetailRow(productID, OrganizationType.DesktopBrokerAccount.ToString(), unitPrice, asxBroker.AsxCode, asxBroker.CompanyName, totalSecHoldingUnits, currentValue, model.ServiceType.ToString(), unsettledBuyunitsForCode, unsettledSellunitsForCode, asxBroker.IsSMAApproved, asxBroker.SMAHoldingLimit);
                                totalHolding += currentValue;
                            }
                        }
                    }
                    decimal unsettledBuy;
                    decimal unsettledSell;
                    decimal unsettledBuyunits;
                    decimal unsettledSellunits;
                    decimal unsettledSellByUnits;
                    decimal unsettledSellByAmount;
                    SettledUnsettledUtilities.GetASXUnsettledAmountNUnits(settledUnsetteledDs, productID, desktopBrokerAccountCM.CID, desktopBrokerAccountCM.DesktopBrokerAccountEntity.AccountNumber, out unsettledBuy, out unsettledSell, out unsettledBuyunits, out unsettledSellunits, out unsettledSellByUnits, out unsettledSellByAmount);

                    foreach (DataRow row in settledUnsetteledDs.SettledUnsetteledTable.Rows)
                    {
                        if (row[settledUnsetteledDs.SettledUnsetteledTable.TYPE].ToString() == "Unsettled")
                        {
                            Guid productIDToCheckProSec = new Guid(row[settledUnsetteledDs.SettledUnsetteledTable.PRODUCTID].ToString());
                            string investmentCodeForCheck = row[settledUnsetteledDs.SettledUnsetteledTable.INVESTMENTCODE].ToString();
                            string tradeType = row[settledUnsetteledDs.SettledUnsetteledTable.TRADETYPE].ToString();
                            var product = orgCM.Products.Where(c => c.ID == productIDToCheckProSec).FirstOrDefault();

                            if (product != null && investmentCodeForCheck != string.Empty && product.ProductSecuritiesId != Guid.Empty)
                            {
                                var secEntity = orgCM.Securities.Where(sc => sc.AsxCode == investmentCodeForCheck).FirstOrDefault();
                                if (secEntity != null)
                                {
                                    var productSecurityDetails = orgCM.ProductSecurities.Where(p => p.ID == product.ProductSecuritiesId).FirstOrDefault();
                                    if (productSecurityDetails != null && (productSecurityDetails.Details.Where(ps => ps.SecurityCodeId == secEntity.ID).FirstOrDefault() == null))
                                    {
                                        if (tradeType == TradeType.Buy.ToString())
                                        {
                                            if (row[settledUnsetteledDs.SettledUnsetteledTable.AMOUNT] != DBNull.Value)
                                                unsettledBuy += Decimal.Parse(row[settledUnsetteledDs.SettledUnsetteledTable.AMOUNT].ToString());
                                            if (row[settledUnsetteledDs.SettledUnsetteledTable.UNITS] != DBNull.Value)
                                                unsettledBuyunits += Decimal.Parse(row[settledUnsetteledDs.SettledUnsetteledTable.UNITS].ToString());
                                        }
                                        else
                                        {
                                            if (row[settledUnsetteledDs.SettledUnsetteledTable.AMOUNT] != DBNull.Value)
                                                unsettledSell += Decimal.Parse(row[settledUnsetteledDs.SettledUnsetteledTable.AMOUNT].ToString());
                                            if (row[settledUnsetteledDs.SettledUnsetteledTable.ORDERPREFEREDBY].ToString() == OrderPreferedBy.Units.ToString())
                                            {
                                                unsettledSellByUnits += Decimal.Parse(row[settledUnsetteledDs.SettledUnsetteledTable.AMOUNT].ToString());
                                            }
                                            else if (row[settledUnsetteledDs.SettledUnsetteledTable.ORDERPREFEREDBY].ToString() == OrderPreferedBy.Amount.ToString())
                                            {
                                                unsettledSellByAmount += Decimal.Parse(row[settledUnsetteledDs.SettledUnsetteledTable.AMOUNT].ToString());
                                            }

                                            if (row[settledUnsetteledDs.SettledUnsetteledTable.UNITS] != DBNull.Value)
                                                unsettledSellunits += Decimal.Parse(row[settledUnsetteledDs.SettledUnsetteledTable.UNITS].ToString());
                                        }
                                    }

                                    var hisSecurity = secEntity.ASXSecurity.Where(secpri => secpri.Date <= holdingReportDataSet.ValuationDate).OrderByDescending(secpri => secpri.Date).FirstOrDefault();
                                    decimal unitPrice = 0;

                                    if (hisSecurity != null)
                                    {
                                        priceAtDate = hisSecurity.Date;
                                        unitPrice = Convert.ToDecimal(hisSecurity.UnitPrice);
                                    }

                                    holdingReportDataSet.AddProductDetailRow(productID, OrganizationType.DesktopBrokerAccount.ToString(), unitPrice, secEntity.AsxCode, secEntity.CompanyName, 0, 0, model.ServiceType.ToString(), unsettledBuyunits, unsettledSellunits, secEntity.IsSMAApproved, secEntity.SMAHoldingLimit);
                                }
                            }
                        }
                    }
                    holdingReportDataSet.AddHoldingRow(clientName, clientID, true, priceAtDate, model.ProgramCode + " - " + model.Name, "AEQ", model.ServiceType, "ASX Desktop Broker", "ASX Desktop Broker", desktopBrokerAccountCM.CID, desktopBrokerAccountCM.DesktopBrokerAccountEntity.AccountNumber, productID.ToString(), string.Empty, OrganizationType.DesktopBrokerAccount.ToString(), string.Empty, totalHolding, unsettledBuy, unsettledSell, unsettledBuyunits, unsettledSellunits, Guid.Empty, Guid.Empty, unsettledSellByUnits, unsettledSellByAmount);
                }
            }
        }

        private static void GetUnlistedSecurityMIS(string clientName, string clientID, ICMBroker Broker, List<AccountProcessTaskEntity> ListAccountProcess, IOrganization orgCM, HoldingRptDataSet holdingReportDataSet, SettledUnsetteledDS ds)
        {

            ManagedInvestmentSchemesAccountCM misData = Broker.GetCMImplementation(new Guid("7986b8ee-4509-44a5-ac8c-7ce59b59592d"), new Guid("b7ae4bb1-2143-4e56-b9ac-10cf24f7eae5")) as ManagedInvestmentSchemesAccountCM;
            ModelEntity model = null;
            foreach (FundAccountEntity entity in misData.ManagedInvestmentSchemesAccountEntity.FundAccounts)
            {
                var transactions = entity.FundTransactions.Where(tran => tran.ClientID == clientID).ToList();
                if (transactions.Count() > 0)
                {
                    bool accountLinked = false;

                    foreach (var accountProcessTaskEntity in ListAccountProcess)
                    {
                        model = orgCM.Model.Where(mod => mod.ID == accountProcessTaskEntity.ModelID).FirstOrDefault();
                        if (model != null)
                        {
                            var linkedAsset = model.Assets.Where(ast => ast.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                            if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.ManagedInvestmentSchemesAccount)
                            {
                                if (linkedAsset != null)
                                {
                                    var product = linkedAsset.Products.Where(pro => pro.ID == accountProcessTaskEntity.TaskID).FirstOrDefault();

                                    if (product != null && product.TaskDescription != null && product.TaskDescription.Contains(entity.Code))
                                        accountLinked = true;
                                }
                            }
                        }
                    }

                    if (!accountLinked)
                    {
                        Guid productID = Guid.NewGuid();

                        var filteredHoldingTransactionsByValuationDate = entity.FundTransactions.Where(cliFundTrans => cliFundTrans.TradeDate <= holdingReportDataSet.ValuationDate);
                        var clientFundTransactions = filteredHoldingTransactionsByValuationDate.Where(cliFundTrans => cliFundTrans.ClientID == clientID);
                        decimal totalShares = clientFundTransactions.Sum(ft => ft.Shares);
                        var linkedmissec = orgCM.Securities.Where(missec => missec.AsxCode == entity.Code).FirstOrDefault();

                        decimal currentValue = 0;
                        DateTime priceAtDate = DateTime.Now;

                        decimal unsettledBuy;
                        decimal unsettledSell;
                        decimal unsettledBuyunits;
                        decimal unsettledSellunits;
                        decimal unsettledSellByUnits;
                        decimal unsettledSellByAmount;
                        SettledUnsettledUtilities.GetMISUnsettledAmountNUnits(ds, productID, entity.Code, out unsettledBuy, out unsettledSell, out unsettledBuyunits, out unsettledSellunits, out unsettledSellByUnits, out unsettledSellByAmount);

                        if (linkedmissec != null)
                        {
                            var latestMisPriceObject = linkedmissec.ASXSecurity.Where(mishis => mishis.Date <= holdingReportDataSet.ValuationDate).OrderByDescending(mishis => mishis.Date).FirstOrDefault();
                            decimal unitPrice = 0;
                            if (latestMisPriceObject != null)
                            {
                                priceAtDate = latestMisPriceObject.Date;
                                unitPrice = Convert.ToDecimal(latestMisPriceObject.UnitPrice);
                            }
                            currentValue = totalShares * unitPrice;
                            holdingReportDataSet.AddProductDetailRow(productID, OrganizationType.ManagedInvestmentSchemesAccount.ToString(), unitPrice, linkedmissec.AsxCode, linkedmissec.CompanyName, totalShares, currentValue, ServiceTypes.DoItForMe.ToString(), unsettledBuyunits, unsettledSellunits, linkedmissec.IsSMAApproved, linkedmissec.SMAHoldingLimit);
                        }

                        decimal unsettledOrders = entity.UnsettledOrders.Where(fundTrans => fundTrans.ClientID == clientID).Sum(fundTrans => fundTrans.UnsettledOrders);

                        if (model != null)
                            holdingReportDataSet.AddHoldingRow(clientName, clientID, true, priceAtDate, model.ProgramCode + " - " + model.Name, "MIS", model.ServiceType, linkedmissec.CompanyName, linkedmissec.AsxCode + " - " + linkedmissec.CompanyName, Guid.Empty, clientID, productID.ToString(), string.Empty, OrganizationType.ManagedInvestmentSchemesAccount.ToString(), string.Empty, currentValue, unsettledBuy, unsettledSell, unsettledBuyunits, unsettledSellunits, Guid.Empty, Guid.Empty, unsettledSellByUnits, unsettledSellByAmount);
                    }
                }
            }
        }

        private static void ManageInvestmentSchemes(string clientName, string clientID, ICMBroker Broker, IOrganization orgCM, HoldingRptDataSet holdingReportDataSet, AccountProcessTaskEntity accountProcessTaskEntity, AssetEntity asset, ModelEntity model, ProductEntity product, SettledUnsetteledDS settledUnsetteledDs)
        {
            ManagedInvestmentSchemesAccountCM managedInvestmentSchemesAccountCM = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as ManagedInvestmentSchemesAccountCM;

            if (managedInvestmentSchemesAccountCM != null && product != null && product.TaskDescription != null)
            {
                var linkedMISAccount = managedInvestmentSchemesAccountCM.ManagedInvestmentSchemesAccountEntity.FundAccounts.ToList().Where(fa => product.TaskDescription.Contains(fa.Code)).FirstOrDefault();
                var filteredHoldingTransactionsByValuationDate = linkedMISAccount.FundTransactions.Where(cliFundTrans => cliFundTrans.TradeDate <= holdingReportDataSet.ValuationDate);
                var clientFundTransactions = filteredHoldingTransactionsByValuationDate.Where(cliFundTrans => cliFundTrans.ClientID == clientID).ToList();
                decimal totalShares = clientFundTransactions.Sum(ft => ft.Shares);
                var linkedmissec = orgCM.Securities.Where(missec => missec.AsxCode == linkedMISAccount.Code).FirstOrDefault();

                decimal currentValue = 0;
                DateTime priceAtDate = DateTime.Now;
                decimal unsettledBuy;
                decimal unsettledSell;
                decimal unsettledBuyunits;
                decimal unsettledSellunits;
                decimal unsettledSellByUnits;
                decimal unsettledSellByAmount;
                SettledUnsettledUtilities.GetMISUnsettledAmountNUnits(settledUnsetteledDs, product.ID, linkedMISAccount.Code, out unsettledBuy, out unsettledSell, out unsettledBuyunits, out unsettledSellunits, out unsettledSellByUnits, out unsettledSellByAmount);

                if (linkedmissec != null)
                {
                    var latestMisPriceObject = linkedmissec.ASXSecurity.Where(mishis => mishis.Date <= holdingReportDataSet.ValuationDate).OrderByDescending(mishis => mishis.Date).FirstOrDefault();
                    decimal unitPrice = 0;
                    if (latestMisPriceObject != null)
                    {
                        priceAtDate = latestMisPriceObject.Date;
                        unitPrice = Convert.ToDecimal(latestMisPriceObject.UnitPrice);
                    }
                    currentValue = totalShares * unitPrice;
                    holdingReportDataSet.AddProductDetailRow(product, unitPrice, linkedmissec.AsxCode, linkedmissec.CompanyName, totalShares, currentValue, model.ServiceType.ToString(), unsettledBuyunits, unsettledSellunits, linkedmissec.IsSMAApproved, linkedmissec.SMAHoldingLimit);
                }

                holdingReportDataSet.AddHoldingRow(clientName, clientID, true, priceAtDate, model.ProgramCode + " - " + model.Name, asset.Name, model.ServiceType, product.Name, linkedMISAccount.Description, Guid.Empty, linkedMISAccount.Code, product.ID.ToString(), string.Empty, product.EntityType.ToString(), string.Empty, currentValue, unsettledBuy, unsettledSell, unsettledBuyunits, unsettledSellunits, Guid.Empty, Guid.Empty, unsettledSellByUnits, unsettledSellByAmount);
            }
        }

        private static void DesktopBrokerTransactions(string clientName, string clientID, ICMBroker Broker, IOrganization orgCM, HoldingRptDataSet holdingReportDataSet, AccountProcessTaskEntity accountProcessTaskEntity, AssetEntity asset, ModelEntity model, ProductEntity product, SettledUnsetteledDS settledUnsetteledDs)
        {
            if (accountProcessTaskEntity.LinkedEntity.Clid == Guid.Empty)
            {
                accountProcessTaskEntity.LinkedEntity.Clid = product.EntityId.Clid;
                accountProcessTaskEntity.LinkedEntity.Csid = product.EntityId.Csid;
            }

            decimal unsettledBuy = 0;
            decimal unsettledSell = 0;
            decimal unsettledBuyunits = 0;
            decimal unsettledSellunits = 0;
            decimal unsettledSellByUnits = 0;
            decimal unsettledSellByAmount = 0;

            DesktopBrokerAccountCM desktopBrokerAccountCM = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid,
                                                                        accountProcessTaskEntity.LinkedEntity.Csid) as DesktopBrokerAccountCM;
            decimal totalHolding = 0;
            DateTime priceAtDate = DateTime.Now;

            if (product != null && desktopBrokerAccountCM != null)
            {
                //If it is linked with product securities for split type calculation
                if (product.ProductSecuritiesId != Guid.Empty)
                {
                    var productSecurity = orgCM.ProductSecurities.Where(proseclist => proseclist.ID == product.ProductSecuritiesId).FirstOrDefault();
                    var linkedAssets = model.Assets.Where(ast => ast.ID == accountProcessTaskEntity.AssetID && accountProcessTaskEntity.ModelID == model.ID);
                    double totalPercentage = HoldingRptDataSet.CalculateTotalPercentageForSplit(linkedAssets);

                    foreach (var productSecurityDetail in productSecurity.Details)
                    {
                        var asxBroker = orgCM.Securities.Where(sec => sec.ID == productSecurityDetail.SecurityCodeId).FirstOrDefault();
                        var filteredHoldingTransactionsByValuationDate = desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Where(holdTrans =>
                                                                                                      holdTrans.TradeDate <= holdingReportDataSet.ValuationDate);
                        var filteredHoldingTransactions = filteredHoldingTransactionsByValuationDate.Where(holdTrans =>
                                                                                                      holdTrans.InvestmentCode == asxBroker.AsxCode);
                        var count = filteredHoldingTransactions.Count();

                        decimal unsettledBuyForCode;
                        decimal unsettledSellForCode;
                        decimal unsettledBuyunitsForCode;
                        decimal unsettledSellunitsForCode;
                        decimal unsettledSellByUnitsForCode;
                        decimal unsettledSellByAmountForCode;
                        var orgPro = orgCM.Products.Where(p => p.ID == product.ID).First();
                        var orgPro2 = orgCM.Products.Where(p => p.ID == new Guid("950e2631-2312-42c0-827d-d7938f6d6215")).First();

                        SettledUnsettledUtilities.GetASXUnsettledAmountNUnitsForInvementCode(settledUnsetteledDs, asxBroker.AsxCode, product.ID, desktopBrokerAccountCM.CID, desktopBrokerAccountCM.DesktopBrokerAccountEntity.AccountNumber, out unsettledBuyForCode, out unsettledSellForCode, out unsettledBuyunitsForCode, out unsettledSellunitsForCode, out unsettledSellByUnitsForCode, out unsettledSellByAmountForCode);
                        unsettledBuy += unsettledBuyForCode;
                        unsettledSell += unsettledSellForCode;
                        unsettledBuyunits += unsettledBuyunitsForCode;
                        unsettledSellunits += unsettledSellunitsForCode;
                        unsettledSellByUnits += unsettledSellByUnitsForCode;
                        unsettledSellByAmount += unsettledSellByAmountForCode;

                        if (count > 0)
                        {
                            totalPercentage = product.SharePercentage;

                            if (product.SharePercentage > 0)
                            {

                                foreach (var linkedasst in linkedAssets)
                                {
                                    var otherProducts = linkedasst.Products.Where(linkedAsstPro => linkedAsstPro.ProductSecuritiesId != productSecurity.ID);
                                    foreach (var OtherproductSecurity in otherProducts)
                                    {
                                        var OtherproductSecurityDetails = orgCM.ProductSecurities.Where(otherpro => otherpro.ID == OtherproductSecurity.ProductSecuritiesId).FirstOrDefault();

                                        if (OtherproductSecurityDetails != null)
                                        {
                                            var otherAsxBroker = OtherproductSecurityDetails.Details.Where(otherProDetail => otherProDetail.SecurityCodeId == asxBroker.ID);

                                            if (otherAsxBroker.Count() > 0)
                                                totalPercentage += OtherproductSecurity.SharePercentage;
                                        }
                                    }
                                }

                                var hisSecurity = asxBroker.ASXSecurity.Where(secpri => secpri.Date <= holdingReportDataSet.ValuationDate).OrderByDescending(secpri => secpri.Date).FirstOrDefault();
                                decimal holding = filteredHoldingTransactions.Sum(filteredHolding => filteredHolding.Units);
                                decimal unitPrice = 0;

                                if (hisSecurity != null)
                                {
                                    priceAtDate = hisSecurity.Date;
                                    unitPrice = Convert.ToDecimal(hisSecurity.UnitPrice);
                                }

                                decimal currentValue = holding * unitPrice;
                                currentValue = currentValue * Convert.ToDecimal((product.SharePercentage / totalPercentage));
                                holdingReportDataSet.AddProductDetailRow(product, unitPrice, asxBroker.AsxCode, asxBroker.CompanyName, holding, currentValue, model.ServiceType.ToString(), unsettledBuyunitsForCode, unsettledSellunitsForCode, asxBroker.IsSMAApproved, asxBroker.SMAHoldingLimit);
                                totalHolding += currentValue;
                            }
                        }
                    }
                }
                else
                {
                    var filteredHoldingTransactionsByValuationDate = desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Where(holdTrans =>
                                                                                                      holdTrans.TradeDate <= holdingReportDataSet.ValuationDate);

                    var groupedSecurities = filteredHoldingTransactionsByValuationDate.GroupBy(holdTran => holdTran.InvestmentCode);

                    if (groupedSecurities.Count() == 0)
                        priceAtDate = orgCM.Securities.FirstOrDefault().ASXSecurity.FirstOrDefault().Date;

                    foreach (var security in groupedSecurities)
                    {
                        decimal totalSecHoldingUnits = security.Sum(sec => sec.Units);
                        var securitiesEntity = orgCM.Securities.Where(sec => sec.AsxCode == security.Key).FirstOrDefault();
                        if (securitiesEntity != null)
                        {
                            var hisSecurity = securitiesEntity.ASXSecurity.Where(secpri => secpri.Date <= holdingReportDataSet.ValuationDate).OrderByDescending(secpri => secpri.Date).FirstOrDefault();
                            decimal unitPrice = 0;

                            if (hisSecurity != null)
                            {
                                priceAtDate = hisSecurity.Date;
                                unitPrice = Convert.ToDecimal(hisSecurity.UnitPrice);
                            }

                            decimal currentValue = totalSecHoldingUnits * unitPrice;

                            decimal unsettledBuyForCode;
                            decimal unsettledSellForCode;
                            decimal unsettledBuyunitsForCode;
                            decimal unsettledSellunitsForCode;
                            decimal unsettledSellByUnitsForCode;
                            decimal unsettledSellByAmountForCode;
                            SettledUnsettledUtilities.GetASXUnsettledAmountNUnitsForInvementCode(settledUnsetteledDs, securitiesEntity.AsxCode, product.ID, desktopBrokerAccountCM.CID, desktopBrokerAccountCM.DesktopBrokerAccountEntity.AccountNumber, out unsettledBuyForCode, out unsettledSellForCode, out unsettledBuyunitsForCode, out unsettledSellunitsForCode, out unsettledSellByUnitsForCode, out unsettledSellByAmountForCode);

                            holdingReportDataSet.AddProductDetailRow(product, unitPrice, securitiesEntity.AsxCode, securitiesEntity.CompanyName, totalSecHoldingUnits, currentValue, model.ServiceType.ToString(), unsettledBuyunitsForCode, unsettledSellunitsForCode, securitiesEntity.IsSMAApproved, securitiesEntity.SMAHoldingLimit);
                            totalHolding += currentValue;
                            unsettledBuy += unsettledBuyForCode;
                            unsettledSell += unsettledSellForCode;
                            unsettledBuyunits += unsettledBuyunitsForCode;
                            unsettledSellunits += unsettledSellunitsForCode;
                            unsettledSellByUnits += unsettledSellByUnitsForCode;
                            unsettledSellByAmount += unsettledSellByAmountForCode;
                        }
                    }
                }

                //Getting missing security from settled unsettled 
                {
                    var datarows = settledUnsetteledDs.SettledUnsetteledTable.Select(string.Format("{0}='{1}'", settledUnsetteledDs.SettledUnsetteledTable.PRODUCTID, product.ID));
                    foreach (DataRow row in datarows)
                    {
                        string investmentCode = row[settledUnsetteledDs.SettledUnsetteledTable.INVESTMENTCODE].ToString();
                        var seletedRow = holdingReportDataSet.ProductBreakDownTable.Select(string.Format("{0} = '{1}'", holdingReportDataSet.ProductBreakDownTable.INVESMENTCODE, investmentCode));
                        if (seletedRow.Length == 0)
                        {
                            decimal unsettledBuyForCode;
                            decimal unsettledSellForCode;
                            decimal unsettledBuyunitsForCode;
                            decimal unsettledSellunitsForCode;
                            decimal unsettledSellByUnitsForCode;
                            decimal unsettledSellByAmountForCode;
                            SettledUnsettledUtilities.GetASXUnsettledAmountNUnitsForInvementCode(settledUnsetteledDs, investmentCode, product.ID, desktopBrokerAccountCM.CID, desktopBrokerAccountCM.DesktopBrokerAccountEntity.AccountNumber, out unsettledBuyForCode, out unsettledSellForCode, out unsettledBuyunitsForCode, out unsettledSellunitsForCode, out unsettledSellByUnitsForCode, out unsettledSellByAmountForCode);
                            var securitiesEntity = orgCM.Securities.FirstOrDefault(sec => sec.AsxCode == investmentCode);

                            if (securitiesEntity != null)
                            {
                                string securityDesc = securitiesEntity.CompanyName;
                                var price = securitiesEntity.ASXSecurity.OrderByDescending(pr => pr.Date).FirstOrDefault();

                                decimal unitPrice = 0;
                                if (price != null)
                                {
                                    unitPrice = (decimal)price.UnitPrice;
                                }
                                bool isSMAApproved = securitiesEntity.IsSMAApproved;
                                double smaHoldingLimit = securitiesEntity.SMAHoldingLimit;
                                holdingReportDataSet.AddProductDetailRow(product, unitPrice, investmentCode, securityDesc, 0, 0, model.ServiceType.ToString(), unsettledBuyunitsForCode, unsettledSellunitsForCode, isSMAApproved, smaHoldingLimit);
                            }
                        }
                    }
                }

                SettledUnsettledUtilities.GetASXUnsettledAmountNUnits(settledUnsetteledDs, product.ID, desktopBrokerAccountCM.CID, desktopBrokerAccountCM.DesktopBrokerAccountEntity.AccountNumber, out unsettledBuy, out unsettledSell, out unsettledBuyunits, out unsettledSellunits, out unsettledSellByUnits, out unsettledSellByAmount);
                holdingReportDataSet.AddHoldingRow(clientName, clientID, true, priceAtDate, model.ProgramCode + " - " + model.Name, asset.Name, model.ServiceType, product.Name, product.Description, desktopBrokerAccountCM.CID, desktopBrokerAccountCM.DesktopBrokerAccountEntity.AccountNumber, product.ID.ToString(), string.Empty, product.EntityType.ToString(), string.Empty, totalHolding, unsettledBuy, unsettledSell, unsettledBuyunits, unsettledSellunits, Guid.Empty, Guid.Empty, unsettledSellByUnits, unsettledSellByAmount);
            }
        }

        private static void TDTransactions(string clientName, string clientID, ICMBroker Broker, IOrganization orgCM, HoldingRptDataSet holdingReportDataSet, DataTable TDBreakDownTable, AccountProcessTaskEntity accountProcessTaskEntity, AssetEntity asset, ModelEntity model, ProductEntity product, SettledUnsetteledDS unsetteledDs)
        {
            BankAccountCM bankAccount = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as BankAccountCM;
            decimal holdingTotal = 0;
            if (bankAccount != null)
            {

                var filteredTransactions = bankAccount.BankAccountEntity.CashManagementTransactions.Where(trans => trans.TransactionDate.Date <= holdingReportDataSet.ValuationDate.Date).ToList();

                var tdGroupsType = filteredTransactions.GroupBy(cashMan => cashMan.TransactionType);

                foreach (var tdType in tdGroupsType)
                {
                    if (tdType.Key != null)
                    {
                        var groupByIns = tdType.GroupBy(tdtypetran => tdtypetran.InstitutionID);

                        foreach (var groupins in groupByIns)
                        {
                            var transaction = groupins.FirstOrDefault();

                            decimal holding = groupins.Sum(inssum => inssum.Amount);

                            var inst = orgCM.Institution.Where(ins => ins.ID == groupins.Key).FirstOrDefault();

                            string instName = string.Empty;
                            Guid brokerId = Guid.Empty;
                            if (inst != null)
                            {
                                instName = inst.Name;
                                brokerId = inst.ID;
                            }

                            Guid productID = Guid.Empty;
                            string productEntity = string.Empty;

                            if (product != null)
                            {
                                productID = product.ID;
                                productEntity = product.EntityType.ToString();
                            }
                            Guid brodkerId = bankAccount.BankAccountEntity.InstitutionID;
                            Guid insid = Guid.Empty;
                            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashTransaction in groupins)
                            {
                                DataRow tDBreakDownTableRow = TDBreakDownTable.NewRow();
                                insid = cashTransaction.InstitutionID;

                                holdingReportDataSet.TDBreakDownRow(instName, model.ServiceType.ToString(), cashTransaction.SystemTransactionType, transaction.AdministrationSystem, cashTransaction.TotalAmount, bankAccount.BankAccountEntity.InstitutionID.ToString().ToLower() + tdType.Key.ToLower() + instName.ToLower(), tDBreakDownTableRow, cashTransaction.TransactionDate, bankAccount.BankAccountEntity.InstitutionID, cashTransaction.InstitutionID, productID, bankAccount.CID, cashTransaction.ContractNote);
                                TDBreakDownTable.Rows.Add(tDBreakDownTableRow);
                            }

                            DateTime priceAtDate = DateTime.Now;
                            priceAtDate = groupins.OrderByDescending(tran => tran.TransactionDate).FirstOrDefault().TransactionDate;


                            decimal unsettledBuy = 0;
                            decimal unsettledSell = 0;
                            decimal validatedunsettledBuy = 0;
                            decimal validatedunsettledSell = 0;

                            string assetName = asset.Name;
                            if (assetName.ToLower() == "tds")
                                assetName = "FI";

                            if (product != null)
                            {
                                string OrderTDType = string.Empty;
                                if (transaction.AdministrationSystem.ToLower().Contains("td"))
                                {
                                    OrderTDType = OrderAccountType.TermDeposit.ToString();
                                }
                                else if (transaction.AdministrationSystem.ToLower().Contains("at call"))
                                {
                                    OrderTDType = OrderAccountType.AtCall.ToString();
                                }


                                //Filter out TD/At Call transaction amount which are added during export orders  - 933 - Ozair
                                //Also filtered out amount when order is validated - 933                            
                                SettledUnsettledUtilities.GetTDUnsettledAmountFinSummary(OrderTDType, unsetteledDs, product.ID, brokerId,
                                                                               bankAccount.CID,
                                                                               bankAccount.BankAccountEntity.AccountNumber,
                                                                               bankAccount.BankAccountEntity.BSB,
                                                                               out unsettledBuy, out unsettledSell, out validatedunsettledBuy, out validatedunsettledSell);

                            }

                            //Correction added for case when orders is in Validated State and for Buy/Sale order - 933 - Ozair

                            holding = holding - unsettledBuy - unsettledSell;
                            unsettledBuy += validatedunsettledBuy;
                            unsettledSell += validatedunsettledSell;

                            holdingReportDataSet.AddHoldingRow(clientName, clientID, true, priceAtDate, model.ProgramCode + " - " + model.Name, assetName, model.ServiceType, transaction.AdministrationSystem, instName, bankAccount.CID, bankAccount.BankAccountEntity.CustomerNo, productID.ToString(), bankAccount.BankAccountEntity.InstitutionID.ToString().ToLower() + tdType.Key.ToLower() + instName.ToLower(), productEntity, string.Empty, holding, unsettledBuy, unsettledSell, 0, 0, brodkerId, insid, 0, 0);
                            holdingTotal += holding;
                        }
                    }
                }

                if (product != null)
                {
                    var datarows = unsetteledDs.SettledUnsetteledTable.Select(string.Format("{0}='{1}'", unsetteledDs.SettledUnsetteledTable.PRODUCTID, product.ID));
                    foreach (DataRow row in datarows)
                    {
                        var bank = orgCM.Institution.Where(ins => ins.ID == Guid.Parse(row[unsetteledDs.SettledUnsetteledTable.BANKID].ToString())).FirstOrDefault();
                        var brokerInst = orgCM.Institution.Where(ins => ins.ID == Guid.Parse(row[unsetteledDs.SettledUnsetteledTable.BROKERID].ToString())).FirstOrDefault();
                        if (bank != null)
                        {
                            string prductDesc = brokerInst.Name + "-";


                            if (row[unsetteledDs.SettledUnsetteledTable.ACCOUNTTYPE].ToString().ToLower() == "termdeposit")
                            {
                                prductDesc += "TD";
                            }
                            else if (row[unsetteledDs.SettledUnsetteledTable.ACCOUNTTYPE].ToString().ToLower().Contains("atcall"))
                            {
                                prductDesc += "At Call";

                            }

                            var seletedRow =
                                holdingReportDataSet.HoldingSummaryTable.Select(string.Format("{0} = '{1}' and {2} = '{3}'", holdingReportDataSet.HoldingSummaryTable.DESCRIPTION, bank.Name, holdingReportDataSet.HoldingSummaryTable.PRODUCTNAME, prductDesc));
                            if (seletedRow.Length == 0)
                            {
                                decimal unsettledBuy = 0;
                                decimal unsettledSell = 0;
                                SettledUnsettledUtilities.GetTDUnsettledAmount(row[unsetteledDs.SettledUnsetteledTable.ACCOUNTTYPE].ToString(), unsetteledDs, product.ID, bank.ID, bankAccount.CID, bankAccount.BankAccountEntity.AccountNumber, bankAccount.BankAccountEntity.BSB, out unsettledBuy, out unsettledSell);
                                holdingReportDataSet.AddHoldingRow(clientName, clientID, false, DateTime.Now, model.ProgramCode + " - " + model.Name, asset.Name, model.ServiceType, prductDesc, bank.Name, bankAccount.CID, bankAccount.BankAccountEntity.CustomerNo, row[unsetteledDs.SettledUnsetteledTable.PRODUCTID].ToString(), bankAccount.BankAccountEntity.InstitutionID.ToString().ToLower() + bank.Name.ToLower(), product.EntityType.ToString(), string.Empty, 0, unsettledBuy, unsettledSell, 0, 0, Guid.Empty, Guid.Empty, 0, 0);
                            }
                        }
                    }
                }
            }
        }

        private static void BankAccountTransctions(string clientName, string clientID, ICMBroker Broker, HoldingRptDataSet holdingReportDataSet, AccountProcessTaskEntity accountProcessTaskEntity, AssetEntity asset, ModelEntity model, ProductEntity product, SettledUnsetteledDS ds)
        {
            if (accountProcessTaskEntity.LinkedEntity.Clid == Guid.Empty)
            {
                accountProcessTaskEntity.LinkedEntity.Clid = product.EntityId.Clid;
                accountProcessTaskEntity.LinkedEntity.Csid = product.EntityId.Csid;
            }

            BankAccountCM bankAccount =
                Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid,
                                           accountProcessTaskEntity.LinkedEntity.Csid) as BankAccountCM;
            if (bankAccount != null)
            {
                var filteredTransactions =
                    bankAccount.BankAccountEntity.CashManagementTransactions.Where(
                        trans => trans.TransactionDate.Date <= holdingReportDataSet.ValuationDate.Date);
                decimal holdingTotal = filteredTransactions.Sum(calc => calc.TotalAmount);


                decimal unsettledBuy = 0;
                decimal unsettledSell = 0;

                SettledUnsettledUtilities.GetBankUnsettledAmount(ds, bankAccount.CID,
                                                              bankAccount.BankAccountEntity.AccountNumber,
                                                              bankAccount.BankAccountEntity.BSB, out unsettledBuy,
                                                              out unsettledSell);

                if (product != null)
                {
                    holdingReportDataSet.AddHoldingRow(clientName, clientID, true, DateTime.Now.AddDays(-1), model.ProgramCode + " - " + model.Name, asset.Name, model.ServiceType, product.Name,
                                                       product.Name, bankAccount.CID,
                                                       bankAccount.BankAccountEntity.AccountNumber,
                                                       product.ID.ToString(), string.Empty,
                                                       product.EntityType.ToString(), bankAccount.BankAccountEntity.BSB,
                                                       holdingTotal, unsettledBuy, unsettledSell, 0, 0, Guid.Empty, Guid.Empty, 0, 0);
                }

                else
                {
                    holdingReportDataSet.AddHoldingRow(clientName, clientID, true, DateTime.Now.AddDays(-1), model.ProgramCode + " - " + model.Name, asset.Name, model.ServiceType, "Bankwest - CMA",
                                                       "CMA ACCOUNT", bankAccount.CID,
                                                       bankAccount.BankAccountEntity.AccountNumber,
                                                       string.Empty, string.Empty,
                                                       OrganizationType.BankAccount.ToString(),
                                                       bankAccount.BankAccountEntity.BSB, holdingTotal,
                                                       unsettledBuy, unsettledSell, 0, 0, Guid.Empty, Guid.Empty, 0, 0);
                }
            }
        }

        private static void ManualTransactions(string clientName, string clientID, ObservableCollection<ClientManualAssetEntity> ClientManualAssetCollection, IOrganization orgCM, HoldingRptDataSet holdingReportDataSet)
        {
            if (ClientManualAssetCollection != null)
            {
                var filteredManualAssets = ClientManualAssetCollection.Where(man => man.TransactionDate.Date <= holdingReportDataSet.ValuationDate.Date).GroupBy(man => man.InvestmentCode);

                foreach (var manualAsset in filteredManualAssets)
                {
                    decimal currentValue = 0;
                    string companyName = string.Empty;
                    string companyCode = string.Empty;

                    foreach (ClientManualAssetEntity clientManualAssetEntity in manualAsset)
                    {
                        var systemManualAsset = orgCM.ManualAsset.Where(manList => manList.ID == clientManualAssetEntity.InvestmentCode).FirstOrDefault();
                        companyName = systemManualAsset.CompanyName;
                        companyCode = systemManualAsset.AsxCode;
                        var systemManualAssetPriceHistory = systemManualAsset.ASXSecurity.Where(manualHIS => manualHIS.Date <= holdingReportDataSet.ValuationDate).OrderByDescending(manualHis => manualHis.Date).FirstOrDefault();
                        if (systemManualAssetPriceHistory != null)
                            currentValue += clientManualAssetEntity.UnitsOnHand * Convert.ToDecimal(systemManualAssetPriceHistory.UnitPrice);
                    }

                    holdingReportDataSet.AddHoldingRow(clientName, clientID, false, DateTime.Now, "ManualAsset", "ManualAsset", ServiceTypes.ManualAsset, companyCode, companyName, Guid.Empty, clientID, string.Empty, string.Empty, string.Empty, string.Empty, currentValue, 0, 0, 0, 0, Guid.Empty, Guid.Empty, 0, 0);
                }
            }
        }

        public void OnGetDataHoldingRptDataSetIndividual(HoldingRptDataSet data, string clientType, string clientID, string clientName, IdentityCM Parent, ICMBroker Broker, ClientIndividualEntity Entity, string clientApplicabilityType, Guid clientCid)
        {
            DataTable clientSummaryTable = data.Tables[HoldingRptDataSet.CLIENTSUMMARYTABLE];
            DataTable contactTable = data.Tables[HoldingRptDataSet.CONTACTTABLE];

            DataRow clientSummaryRow = clientSummaryTable.NewRow();
            DataRow contactRow = contactTable.NewRow();
            

            BasicClientDetails(clientType, clientID, clientName, Parent, Broker, Entity.ABN, string.Empty, Entity.Address, clientApplicabilityType, clientSummaryRow,clientCid);

            foreach (var contactIdentity in Entity.Applicants)
            {
                IOrganizationUnit iOrgUnit = Broker.GetCMImplementation(contactIdentity.Clid, contactIdentity.Csid) as IOrganizationUnit;
                if (iOrgUnit != null)
                {
                    Oritax.TaxSimp.Common.IndividualEntity contact = iOrgUnit.ClientEntity as Oritax.TaxSimp.Common.IndividualEntity;

                    contactRow[HoldingRptDataSet.CONTACT_TITLE] = contact.PersonalTitle;
                    contactRow[HoldingRptDataSet.CONTACT_NAME] = contact.Fullname;
                    contactRow[HoldingRptDataSet.CONTACT_BIZNO] = contact.WorkPhoneNumber;
                    contactRow[HoldingRptDataSet.CONTACT_MOBNO] = contact.MobilePhoneNumber;
                    contactRow[HoldingRptDataSet.CONTACT_HNO] = contact.HomePhoneNumber;
                    contactRow[HoldingRptDataSet.CONTACT_FAXNO] = contact.Facsimile;
                    contactRow[HoldingRptDataSet.CONTACT_EMAIL] = contact.EmailAddress;
                }
            }

            clientSummaryTable.Rows.Add(clientSummaryRow);
            contactTable.Rows.Add(contactRow);

            IOrganization orgCM = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            HoldingRptDataSet holdingReportDataSet = data as HoldingRptDataSet;


            if (holdingReportDataSet.ServiceTypes == ServiceTypes.None)
                ManualTransactions(clientName, clientID, Entity.ClientManualAssetCollection, orgCM, holdingReportDataSet);
            var ds = SettledUnsettledUtilities.GetAllUnsetteledForClient(clientID, Broker);
            GetTransactionsInfoFromLinkProcess(clientName, clientID, Broker, Entity.ListAccountProcess, orgCM, holdingReportDataSet, holdingReportDataSet.TDBreakDownTable, ds);
        }
    }
}
