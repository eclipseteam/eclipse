﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using C1.C1Preview;
using System.Data;
using System.Drawing;
using Oritax.TaxSimp.DataSets;
using System.Reflection;
using System.IO;

namespace Oritax.TaxSimp.CM
{
    public class ReportUtilities
    {
        #region Common Report Methods

        public static void AddClientHeaderToReport(C1PrintDocument doc, DataRow clientSummaryRow, string dateInfo, string reportname)
        {
            doc.PageLayout.PageSettings.Landscape = true;
            doc.PageLayout.PageSettings.TopMargin = .20;
            doc.PageLayout.PageSettings.LeftMargin = .60;
            doc.PageLayout.PageSettings.RightMargin = .60;

            RenderImage logo = new RenderImage();
            logo.Image = System.Drawing.Image.FromFile(@"C:\Projects\INNOVA\InnovaMain\Source\eclipseonlineweb\eclipseonlineweb\images\E-Clipse-logo-nowhite.png", true);

            RenderText cellText = new RenderText("Page [PageNo] of [PageCount]");
            cellText.Style.FontItalic = true;
            RenderTable theader = new RenderTable(doc);
            theader.Cells[0, 0].SpanRows = 3;
            theader.Cells[0, 0].RenderObject = logo;
            theader.Cells[0, 1].RenderObject = cellText;

            RenderText celltext = new RenderText(doc);
            celltext.Text = "e-Clipse Online\n";
            celltext.Style.TextAlignHorz = AlignHorzEnum.Right;
            celltext.Style.FontSize = 12;
            celltext.Style.FontBold = true;
            theader.Cells[1, 1].RenderObject = celltext;

            celltext = new RenderText(doc);
            celltext.Text = reportname + "\n";
            celltext.Style.TextColor = Color.FromArgb(46, 44, 83);
            celltext.Style.TextAlignHorz = AlignHorzEnum.Right;
            celltext.Style.FontSize = 14;
            celltext.Style.FontBold = true;
            theader.Cells[2, 1].RenderObject = celltext;

            celltext = new RenderText(doc);
            celltext.Text = dateInfo;
            celltext.Style.TextAlignHorz = AlignHorzEnum.Right;
            celltext.Style.FontSize = 12;
            celltext.Style.FontBold = true;
            theader.Cells[3, 1].RenderObject = celltext;

            celltext = new RenderText(doc);
            celltext.Text = "\n\n\n\n\n\n";
            theader.Cells[4, 1].RenderObject = celltext;

            doc.PageLayout.PageHeader = theader;
            doc.PageLayout.PageHeader.Style.TextAlignHorz = AlignHorzEnum.Right;
            doc.PageLayout.PageHeader.Style.TextAlignVert = AlignVertEnum.Center;
            doc.PageLayout.PageHeader.Height = "3cm";

            RenderTable secondHeader = new RenderTable(doc);
            secondHeader.Cells[2, 0].Text = "\n\n" + clientSummaryRow[HoldingRptDataSet.CLIENTNAME];
            secondHeader.Cells[2, 0].Style.TextAlignHorz = AlignHorzEnum.Left;

            string addressLine1And2 = string.Empty;
            if (clientSummaryRow[HoldingRptDataSet.ADDRESSLINE2].ToString() == string.Empty)
                addressLine1And2 = clientSummaryRow[HoldingRptDataSet.ADDRESSLINE1].ToString();
            else
                addressLine1And2 = clientSummaryRow[HoldingRptDataSet.ADDRESSLINE1].ToString() + ", " + clientSummaryRow[HoldingRptDataSet.ADDRESSLINE2].ToString();
            secondHeader.Cells[3, 0].Text = addressLine1And2;
            secondHeader.Cells[3, 0].Style.TextAlignHorz = AlignHorzEnum.Left;
            secondHeader.Cells[4, 0].Text = clientSummaryRow[HoldingRptDataSet.SUBURB].ToString() +
                                            " " + clientSummaryRow[HoldingRptDataSet.STATE].ToString() + " " + clientSummaryRow[HoldingRptDataSet.POSTCODE].ToString();
            secondHeader.Cells[4, 0].Style.TextAlignHorz = AlignHorzEnum.Left;
            secondHeader.Cells[5, 0].Text = clientSummaryRow[HoldingRptDataSet.COUNTRY].ToString();

            secondHeader.Cells[5, 0].Style.TextAlignHorz = AlignHorzEnum.Left;

            RenderText thcelltext = new RenderText(doc);
            thcelltext.Text = "\n\nInvestor Name";
            thcelltext.Style.FontBold = true;
            secondHeader.Cells[2, 1].RenderObject = thcelltext;
            secondHeader.Cells[2, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

            secondHeader.Cells[3, 1].Text = clientSummaryRow[HoldingRptDataSet.CLIENTNAME].ToString();
            secondHeader.Cells[3, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

            thcelltext = new RenderText(doc);
            thcelltext.Text = "Client ID";
            thcelltext.Style.FontBold = true;
            secondHeader.Cells[4, 1].RenderObject = thcelltext;
            secondHeader.Cells[4, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

            secondHeader.Cells[5, 1].Text = clientSummaryRow[HoldingRptDataSet.CLIENTID].ToString();
            secondHeader.Cells[5, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

            thcelltext = new RenderText(doc);
            thcelltext.Text = "Adviser";
            thcelltext.Style.FontBold = true;
            secondHeader.Cells[6, 1].RenderObject = thcelltext;
            secondHeader.Cells[6, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

            secondHeader.Cells[7, 1].Text = clientSummaryRow[HoldingRptDataSet.ADVISERFULLNAME].ToString() + "\n\n";
            secondHeader.Cells[7, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

            string accountType = clientSummaryRow[HoldingRptDataSet.CLIENTTYPE].ToString();

            thcelltext = new RenderText(doc);
            thcelltext.Text = "Acount Type";
            thcelltext.Style.FontBold = true;
            secondHeader.Cells[6, 1].RenderObject = thcelltext;
            secondHeader.Cells[6, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

            secondHeader.Cells[7, 1].Text = accountType;
            secondHeader.Cells[7, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

            doc.Body.Children.Add(secondHeader);
        }

        #endregion 

        #region CapitalMovementReport

        public void SetTransBreakDownGrid(C1PrintDocument doc, IGrouping<object, DataRow> rows, string serviceName)
        {
            int index = 1;
            double total = 0;
            if (rows.Count() > 0)
            {
                RenderTable capitalTable = new RenderTable();
                capitalTable.RowGroups[0, 1].PageHeader = true;
                capitalTable.Style.FontSize = 6;

                capitalTable.Rows[0].Height = .2;
                capitalTable.Rows[0].Style.FontBold = true;
                capitalTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[0].Style.FontBold = true;
                capitalTable.Rows[0].Style.FontSize = 6;
                capitalTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

                capitalTable.Cells[0, 0].Text = CapitalReportDS.TRANSACTIONTYPE;
                capitalTable.Cells[0, 1].Text = CapitalReportDS.ACCOUNTNO;
                capitalTable.Cells[0, 2].Text = CapitalReportDS.CATEGORY;
                capitalTable.Cells[0, 3].Text = CapitalReportDS.MAJORCATEGORY;
                capitalTable.Cells[0, 4].Text = CapitalReportDS.COMMENT;
                capitalTable.Cells[0, 5].Text = CapitalReportDS.ACCOUNTNO;
                capitalTable.Cells[0, 6].Text = CapitalReportDS.TRADEDATE;
                capitalTable.Cells[0, 7].Text = CapitalReportDS.AMOUNTTOTAL;
                capitalTable.Cells[0, 7].Style.TextAlignHorz = AlignHorzEnum.Right;

                capitalTable.Cols[0].Width = 3;
                capitalTable.Cols[1].Width = 3;
                capitalTable.Cols[2].Width = 3;
                capitalTable.Cols[3].Width = 3;
                capitalTable.Cols[4].Width = 8;
                capitalTable.Cols[5].Width = 3;
                capitalTable.Cols[6].Width = 3;
                capitalTable.Cols[7].Width = 3;

                foreach (DataRow row in rows)
                {
                    capitalTable.Rows[index].Style.TextAlignVert = AlignVertEnum.Center;
                    capitalTable.Rows[index].Height = .3;
                    capitalTable.Cells[index, 0].Text = row[CapitalReportDS.TRANSACTIONTYPE].ToString();
                    capitalTable.Cells[index, 1].Text = row[CapitalReportDS.ACCOUNTNO].ToString();
                    capitalTable.Cells[index, 2].Text = row[CapitalReportDS.CATEGORY].ToString();
                    capitalTable.Cells[index, 3].Text = row[CapitalReportDS.MAJORCATEGORY].ToString();
                    capitalTable.Cells[index, 4].Text = row[CapitalReportDS.COMMENT].ToString();
                    capitalTable.Cells[index, 5].Text = row[CapitalReportDS.ACCOUNTNO].ToString();
                    capitalTable.Cells[index, 6].Text = ((DateTime)row[CapitalReportDS.TRADEDATE]).ToString("dd/MM/yyyy");
                    double amount = Convert.ToDouble(row[CapitalReportDS.AMOUNTTOTAL]);
                    total += amount;
                    capitalTable.Cells[index, 7].Text = amount.ToString("C");
                    capitalTable.Cells[index, 7].Style.TextAlignHorz = AlignHorzEnum.Right;
                    index++;
                }

                capitalTable.Rows[index].Height = .2;
                capitalTable.Rows[index].Style.FontBold = true;
                capitalTable.Rows[index].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[index].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[index].Style.FontBold = true;
                capitalTable.Rows[index].Style.TextAlignVert = AlignVertEnum.Center;
                capitalTable.Cells[index, 7].Text = total.ToString("C");
                capitalTable.Cells[index, 7].Style.TextAlignHorz = AlignHorzEnum.Right;
                RenderText gridHeader = new RenderText();
                gridHeader.Text = serviceName + "\n\n";
                gridHeader.Style.TextColor = Color.FromArgb(46, 44, 83);
                gridHeader.Style.FontSize = 11;
                gridHeader.Style.FontBold = true;

                RenderText lineBreak = new RenderText();
                lineBreak.Text = "\n\n\n";

                doc.Body.Children.Add(gridHeader);
                doc.Body.Children.Add(capitalTable);
                doc.Body.Children.Add(lineBreak);
            }
        }

        public void AddFooterCapMvtStatement(C1PrintDocument doc)
        {
            doc.PageLayouts.PrintFooterOnLastPage = true;
            RenderTable footer = new RenderTable();
            footer.Rows[0].Height = .30;
            footer.Rows[0].Style.FontBold = true;
            footer.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[0].Style.FontBold = true;
            footer.Rows[0].Style.Borders.Left = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[0].Style.Borders.Right = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;
            footer.Rows[1].Height = 2.5;
            footer.Rows[1].Style.FontBold = true;
            footer.Rows[1].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[1].Style.FontBold = true;
            footer.Rows[1].Style.Borders.Left = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[1].Style.Borders.Right = new LineDef(".8pt", Color.DarkGray);
            footer.Rows[1].Style.TextAlignVert = AlignVertEnum.Center;

            footer.Cells[0, 0].Text = "FOR YOUR INFORMATION";
            footer.Cells[0, 0].Style.TextAlignHorz = AlignHorzEnum.Center;
            footer.Cells[0, 0].Style.FontSize = 12;

            footer.Cells[1, 0].Text = "\n1. The value of any unitised securities held in your portfolio (e.g. shares & managed funds) is determined based on the latest available exit price of each respective security at the date your portfolio is being valued." +
                                        "\n\n2. Capital movements are based on cash flows in and out of your portfolio or between accounts in your portfolio.  This report also provides an indication of the growth of your portfolio (whether it be positive or negative) as a result of market movement." +
                                        "\n\n3. This report is provided by e-Clipse Online Pty Limited ABN 70 145 358 630, AFSL 357 306 (“e-Clipse”) and is based on information provided to e-Clipse by third parties.  Whilst every reasonable effort has been made by e-Clipse to ensure its accuracy, neither e-Clipse nor any of its related entities guarantee its accuracy nor accept any liability for any errors or omissions." +
                                        "\n\ne-Clipse Online Pty Ltd (ABN 70 145 358 630)\n3/36 Bydown Street, Neutral Bay, NSW 2089\nhttp://www.e-clipse.com.au\nP: +61 2 9346 4686";
            footer.Cells[1, 0].Style.TextAlignHorz = AlignHorzEnum.Left;
            footer.Cells[1, 0].Style.TextAlignVert = AlignVertEnum.Top;
            footer.Cells[1, 0].Style.FontSize = 9;
            footer.Cells[1, 0].Style.FontBold = false;

            doc.PageLayouts.LastPage = new PageLayout();
            doc.PageLayouts.LastPage.PageFooter = footer;
        }

        public void SetCapitalGrid(C1PrintDocument doc, IOrderedEnumerable<DataRow> difmRows, string serviceName, string dateinfo)
        {
            int index = 1;

            if (difmRows.Count() > 0)
            {
                RenderTable capitalTable = new RenderTable();
                capitalTable.RowGroups[0, 1].PageHeader = true;

                decimal openingBalanceTotal = 0;
                decimal transferInOutTotal = 0;
                decimal incomeTotal = 0;
                decimal investmentsTotal = 0;
                decimal taxExpenseTotal = 0;
                decimal intCashMovTotal = 0;
                decimal mktValCahangeTotal = 0;
                decimal closingBalanceTotal = 0;

                capitalTable.Style.FontSize = 7;

                capitalTable.Rows[0].Height = .2;
                capitalTable.Rows[0].Style.FontBold = true;
                capitalTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[0].Style.FontBold = true;
                capitalTable.Rows[0].Style.FontSize = 7;
                capitalTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

                capitalTable.Cells[0, 0].Text = "Asset Name";
                capitalTable.Cells[0, 1].Text = "Product Name";
                capitalTable.Cells[0, 2].Text = "Account No";
                capitalTable.Cells[0, 2].Style.TextAlignHorz = AlignHorzEnum.Center;
                capitalTable.Cells[0, 3].Text = "Opn. Bal.";
                capitalTable.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 4].Text = "Trans. In/Out";
                capitalTable.Cells[0, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 5].Text = "Income";
                capitalTable.Cells[0, 5].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 6].Text = "Investments";
                capitalTable.Cells[0, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 7].Text = "Tax & Exp.";
                capitalTable.Cells[0, 7].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 8].Text = "Int. Cash Mvt.";
                capitalTable.Cells[0, 8].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 9].Text = "Mkt Val. Chg.";
                capitalTable.Cells[0, 9].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 10].Text = "Cls. Bal.";
                capitalTable.Cells[0, 10].Style.TextAlignHorz = AlignHorzEnum.Right;

                capitalTable.Cols[0].Width = .8;
                capitalTable.Cols[1].Width = .8;
                capitalTable.Cols[2].Width = .8;
                capitalTable.Cols[3].Width = 1;
                capitalTable.Cols[4].Width = 1;
                capitalTable.Cols[5].Width = 1;
                capitalTable.Cols[6].Width = 1;
                capitalTable.Cols[7].Width = 1;
                capitalTable.Cols[8].Width = 1;
                capitalTable.Cols[9].Width = 1;
                capitalTable.Cols[10].Width = 1;

                foreach (DataRow row in difmRows)
                {
                    capitalTable.Rows[index].Style.TextAlignVert = AlignVertEnum.Center;
                    capitalTable.Rows[index].Height = .3;
                    capitalTable.Cells[index, 0].Text = row[CapitalReportDS.ASSETNAME].ToString();
                    capitalTable.Cells[index, 1].Text = row[CapitalReportDS.PRODUCTNAME].ToString();
                    capitalTable.Cells[index, 2].Text = row[CapitalReportDS.ACCOUNTNO].ToString();
                    capitalTable.Cells[index, 2].Style.TextAlignHorz = AlignHorzEnum.Center;

                    decimal openingBalance = 0;
                    decimal transferInOut = 0;
                    decimal income = 0;
                    decimal investments = 0;
                    decimal taxExpense = 0;
                    decimal intCashMov = 0;
                    decimal mktValCahange = 0;
                    decimal closingBalance = 0;

                    openingBalance += (decimal)row[CapitalReportDS.OPENINGBAL];
                    transferInOut += (decimal)row[CapitalReportDS.TRANSFEROUT];
                    income += (decimal)row[CapitalReportDS.INCOME];
                    investments += (decimal)row[CapitalReportDS.APPLICATIONREDEMPTION];
                    taxExpense += (decimal)row[CapitalReportDS.TAXINOUT];
                    taxExpense += (decimal)row[CapitalReportDS.EXPENSE];
                    intCashMov += (decimal)row[CapitalReportDS.INTERNALCASHMOVEMENT];
                    mktValCahange += (decimal)row[CapitalReportDS.CHANGEININVESTMENTVALUE];
                    closingBalance += (decimal)row[CapitalReportDS.CLOSINGBALANCE];

                    openingBalanceTotal += openingBalance;
                    transferInOutTotal += transferInOut;
                    incomeTotal += income;
                    investmentsTotal += investments;
                    taxExpenseTotal += taxExpense;
                    intCashMovTotal += intCashMov;
                    mktValCahangeTotal += mktValCahange;
                    closingBalanceTotal += closingBalance;

                    capitalTable.Cells[index, 3].Text = openingBalance.ToString("C");
                    capitalTable.Cells[index, 3].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 4].Text = transferInOut.ToString("C");
                    capitalTable.Cells[index, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 5].Text = income.ToString("C");
                    capitalTable.Cells[index, 5].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 6].Text = investments.ToString("C");
                    capitalTable.Cells[index, 6].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 7].Text = taxExpense.ToString("C");
                    capitalTable.Cells[index, 7].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 8].Text = intCashMov.ToString("C");
                    capitalTable.Cells[index, 8].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 9].Text = mktValCahange.ToString("C");
                    capitalTable.Cells[index, 9].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 10].Text = closingBalance.ToString("C");
                    capitalTable.Cells[index, 10].Style.TextAlignHorz = AlignHorzEnum.Right;
                    index++;
                }

                capitalTable.Rows[index].Height = .2;
                capitalTable.Rows[index].Style.FontBold = true;
                capitalTable.Rows[index].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[index].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[index].Style.FontBold = true;
                capitalTable.Rows[index].Style.TextAlignVert = AlignVertEnum.Center;

                capitalTable.Cells[index, 0].Text = "TOTAL";
                capitalTable.Cells[index, 3].Text = openingBalanceTotal.ToString("C");
                capitalTable.Cells[index, 3].Style.TextAlignHorz = AlignHorzEnum.Right;

                capitalTable.Cells[index, 4].Text = transferInOutTotal.ToString("C");
                capitalTable.Cells[index, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

                capitalTable.Cells[index, 5].Text = incomeTotal.ToString("C");
                capitalTable.Cells[index, 5].Style.TextAlignHorz = AlignHorzEnum.Right;

                capitalTable.Cells[index, 6].Text = investmentsTotal.ToString("C");
                capitalTable.Cells[index, 6].Style.TextAlignHorz = AlignHorzEnum.Right;

                capitalTable.Cells[index, 7].Text = taxExpenseTotal.ToString("C");
                capitalTable.Cells[index, 7].Style.TextAlignHorz = AlignHorzEnum.Right;

                capitalTable.Cells[index, 8].Text = intCashMovTotal.ToString("C");
                capitalTable.Cells[index, 8].Style.TextAlignHorz = AlignHorzEnum.Right;

                capitalTable.Cells[index, 9].Text = mktValCahangeTotal.ToString("C");
                capitalTable.Cells[index, 9].Style.TextAlignHorz = AlignHorzEnum.Right;

                capitalTable.Cells[index, 10].Text = closingBalanceTotal.ToString("C");
                capitalTable.Cells[index, 10].Style.TextAlignHorz = AlignHorzEnum.Right;

                RenderText gridHeader = new RenderText();
                gridHeader.Text = serviceName + " - " + dateinfo + "\n\n";
                gridHeader.Style.TextColor = Color.FromArgb(46, 44, 83);
                gridHeader.Style.FontSize = 11;
                gridHeader.Style.FontBold = true;

                RenderText lineBreak = new RenderText();
                lineBreak.Text = "\n\n\n";

                doc.Body.Children.Add(gridHeader);
                doc.Body.Children.Add(capitalTable);
                doc.Body.Children.Add(lineBreak);
            }

        }

        public void SetCapitalCatSummaryGrid(C1PrintDocument doc, IOrderedEnumerable<DataRow> rows, string serviceName)
        {
            int index = 1;

            if (rows.Count() > 0)
            {
                RenderTable capitalTable = new RenderTable();
                capitalTable.RowGroups[0, 1].PageHeader = true;

                double OpeningBalance = 0;
                double TransferInOut = 0;
                double Income = 0;
                double AppicationRedemption = 0;
                double TaxInOut = 0;
                double Expense = 0;
                double InternalCashMovement = 0;
                double ClosingBalance = 0;
                double ChangeInInvestmentValue = 0;

                capitalTable.Style.FontSize = 8;

                capitalTable.Rows[0].Height = .2;
                capitalTable.Rows[0].Style.FontBold = true;
                capitalTable.Rows[0].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[0].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[0].Style.FontBold = true;
                capitalTable.Rows[0].Style.FontSize = 7;
                capitalTable.Rows[0].Style.TextAlignVert = AlignVertEnum.Center;

                capitalTable.Cells[0, 0].Text = "Month";
                capitalTable.Cells[0, 1].Text = "Opening Balance";
                capitalTable.Cells[0, 1].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 2].Text = "Transfer In/Out";
                capitalTable.Cells[0, 2].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 3].Text = "Income";
                capitalTable.Cells[0, 3].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 4].Text = "Investments";
                capitalTable.Cells[0, 4].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 5].Text = "Tax & Expenses";
                capitalTable.Cells[0, 5].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 6].Text = "Internal Cash Mvt";
                capitalTable.Cells[0, 6].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 7].Text = "Change in Mkt. Val.";
                capitalTable.Cells[0, 7].Style.TextAlignHorz = AlignHorzEnum.Right;
                capitalTable.Cells[0, 8].Text = "Closing Balance";
                capitalTable.Cells[0, 8].Style.TextAlignHorz = AlignHorzEnum.Right;

                capitalTable.Cols[0].Width = .5;
                capitalTable.Cols[1].Width = 1.2;
                capitalTable.Cols[2].Width = 1.2;
                capitalTable.Cols[3].Width = 1.2;
                capitalTable.Cols[4].Width = 1.2;
                capitalTable.Cols[5].Width = 1.2;
                capitalTable.Cols[6].Width = 1.2;
                capitalTable.Cols[7].Width = 1.2;
                capitalTable.Cols[8].Width = 1.2;

                foreach (DataRow row in rows)
                {
                    capitalTable.Rows[index].Style.TextAlignVert = AlignVertEnum.Center;
                    capitalTable.Rows[index].Height = .3;
                    capitalTable.Cells[index, 0].Text = ((DateTime)row[CapitalReportDS.MONTH]).ToString("MMM-yy").ToUpper();

                    OpeningBalance += Convert.ToDouble(row[CapitalReportDS.OPENINGBAL]);
                    TransferInOut += Convert.ToDouble(row[CapitalReportDS.TRANSFEROUT]);
                    Income += Convert.ToDouble(row[CapitalReportDS.INCOME]);
                    AppicationRedemption += Convert.ToDouble(row[CapitalReportDS.APPLICATIONREDEMPTION]);
                    TaxInOut += Convert.ToDouble(row[CapitalReportDS.TAXINOUT]);
                    Expense += Convert.ToDouble(row[CapitalReportDS.EXPENSE]);
                    InternalCashMovement += Convert.ToDouble(row[CapitalReportDS.INTERNALCASHMOVEMENT]);
                    ClosingBalance += Convert.ToDouble(row[CapitalReportDS.CLOSINGBALANCE]);
                    ChangeInInvestmentValue += Convert.ToDouble(row[CapitalReportDS.CHANGEININVESTMENTVALUE]);

                    capitalTable.Cells[index, 1].Text = Convert.ToDouble(row[CapitalReportDS.OPENINGBAL]).ToString("C");
                    capitalTable.Cells[index, 1].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 2].Text = Convert.ToDouble(row[CapitalReportDS.TRANSFEROUT]).ToString("C");
                    capitalTable.Cells[index, 2].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 3].Text = Convert.ToDouble(row[CapitalReportDS.INCOME]).ToString("C");
                    capitalTable.Cells[index, 3].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 4].Text = Convert.ToDouble(row[CapitalReportDS.APPLICATIONREDEMPTION]).ToString("C");
                    capitalTable.Cells[index, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 5].Text = Convert.ToDouble(row[CapitalReportDS.EXPENSE]).ToString("C");
                    capitalTable.Cells[index, 5].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 6].Text = Convert.ToDouble(row[CapitalReportDS.INTERNALCASHMOVEMENT]).ToString("C");
                    capitalTable.Cells[index, 6].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 7].Text = Convert.ToDouble(row[CapitalReportDS.CHANGEININVESTMENTVALUE]).ToString("C");
                    capitalTable.Cells[index, 7].Style.TextAlignHorz = AlignHorzEnum.Right;

                    capitalTable.Cells[index, 8].Text = Convert.ToDouble(row[CapitalReportDS.CLOSINGBALANCE]).ToString("C");
                    capitalTable.Cells[index, 8].Style.TextAlignHorz = AlignHorzEnum.Right;

                    index++;
                }

                capitalTable.Rows[index].Height = .2;
                capitalTable.Rows[index].Style.FontBold = true;
                capitalTable.Rows[index].Style.Borders.Top = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[index].Style.Borders.Bottom = new LineDef(".8pt", Color.DarkGray);
                capitalTable.Rows[index].Style.FontBold = true;
                capitalTable.Rows[index].Style.TextAlignVert = AlignVertEnum.Center;

                capitalTable.Cells[index, 0].Text = "TOTAL";

                capitalTable.Cells[index, 2].Text = TransferInOut.ToString("C");
                capitalTable.Cells[index, 2].Style.TextAlignHorz = AlignHorzEnum.Right;

                capitalTable.Cells[index, 3].Text = Income.ToString("C");
                capitalTable.Cells[index, 3].Style.TextAlignHorz = AlignHorzEnum.Right;

                capitalTable.Cells[index, 4].Text = AppicationRedemption.ToString("C");
                capitalTable.Cells[index, 4].Style.TextAlignHorz = AlignHorzEnum.Right;

                capitalTable.Cells[index, 5].Text = Expense.ToString("C");
                capitalTable.Cells[index, 5].Style.TextAlignHorz = AlignHorzEnum.Right;

                capitalTable.Cells[index, 6].Text = InternalCashMovement.ToString("C");
                capitalTable.Cells[index, 6].Style.TextAlignHorz = AlignHorzEnum.Right;

                capitalTable.Cells[index, 7].Text = ChangeInInvestmentValue.ToString("C");
                capitalTable.Cells[index, 7].Style.TextAlignHorz = AlignHorzEnum.Right;

                RenderText gridHeader = new RenderText();
                gridHeader.Text = serviceName + "\n\n";
                gridHeader.Style.TextColor = Color.FromArgb(46, 44, 83);
                gridHeader.Style.FontSize = 11;
                gridHeader.Style.FontBold = true;

                RenderText lineBreak = new RenderText();
                lineBreak.Text = "\n\n\n";

                doc.Body.Children.Add(gridHeader);
                doc.Body.Children.Add(capitalTable);
                doc.Body.Children.Add(lineBreak);
            }

        }

        #endregion 
    }
}
