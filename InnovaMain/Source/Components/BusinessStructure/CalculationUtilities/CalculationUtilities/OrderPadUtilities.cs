﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Enum;

namespace Oritax.TaxSimp.CM
{
    public class OrderPadUtilities
    {
        public static Dictionary<long, string> ValidateOrders(List<OrderEntity> orders, ICMBroker broker, UserEntity currentUser)
        {
            var smaMessages = new Dictionary<long, string>();
            var orderHoldings = new List<ClientCurrentholdingContainer>();
            foreach (var order in orders)
            {
                FillHoldingIfNotExists(orderHoldings, order, broker);

                string tempmessage;
                var result = ValidateOrder(order, broker, currentUser, out tempmessage, orderHoldings);
                if (!string.IsNullOrEmpty(tempmessage)) smaMessages.Add(order.OrderID, tempmessage);

                if (result)
                {
                    UpdateAvailableHoldingBalance(order, orderHoldings);
                }

            }
            var clientorders = orders.Where(o => o.Status == OrderStatus.Validated);
            CreateUnsettledForOrders(broker, currentUser, clientorders.ToList(), true);
            return smaMessages;
        }

        private static void UpdateAvailableHoldingBalance(OrderEntity order, List<ClientCurrentholdingContainer> orderHoldings)
        {
            if (order.ClientManagementType == ClientManagementType.SMA && orderHoldings.Count > 0)
            {
                foreach (var item in order.Items)
                {
                    if (item is TermDepositOrderItem)
                    {
                        var tdItem = item as TermDepositOrderItem;

                        var curHolding = orderHoldings.FirstOrDefault(ss => ss.ClientID == order.ClientID && ss.BrokerID == tdItem.BrokerID && ss.BankID == tdItem.BankID);
                        if (curHolding != null)
                        {
                            curHolding.AvailableBalance -= Convert.ToDecimal(tdItem.Amount);
                        }
                    }
                    else if (item is ASXOrderItem)
                    {
                        var asxItem = item as ASXOrderItem;
                        var curHolding = orderHoldings.FirstOrDefault(ss => ss.ClientID == order.ClientID && ss.SecuirtyCode == asxItem.InvestmentCode);
                        if (curHolding != null)
                        {
                            curHolding.AvailableBalance -= Convert.ToDecimal(asxItem.Amount);
                        }
                    }
                }
            }
        }

        private static void FillHoldingIfNotExists(List<ClientCurrentholdingContainer> orderHoldings, OrderEntity order, ICMBroker broker)
        {
            if (order.ClientManagementType == ClientManagementType.SMA)
            {
                foreach (var item in order.Items)
                {
                    if (item is TermDepositOrderItem)
                    {
                        var tdItem = item as TermDepositOrderItem;

                        if (!orderHoldings.Any(ss => ss.ClientID == order.ClientID && ss.BrokerID == tdItem.BrokerID && ss.BankID == tdItem.BankID))
                        {
                            var brokerName = GetInstituteName(broker, tdItem.BrokerID, true);
                            var bankName = GetInstituteName(broker, tdItem.BankID, true);

                            //default td holding limit 50%
                            decimal holdingLimit = 50;

                            IBrokerManagedComponent iBMC = broker.GetWellKnownBMC(WellKnownCM.Organization);
                            var ratesDs = new OrderPadRatesDS { ClientManagementType = order.ClientManagementType };
                            iBMC.GetData(ratesDs);
                            broker.ReleaseBrokerManagedComponent(iBMC);
                            var dataView = new DataView(ratesDs.Tables[OrderPadRatesDS.ORDERPADRATESTABLE])
                                {
                                    RowFilter = string.Format("{0}='{1}'", OrderPadRatesDS.CLIENTMANAGEMENTTYPE, order.ClientManagementType),
                                    Sort = string.Format("{0} ASC, {1} DESC", OrderPadRatesDS.INSTITUTENAME, OrderPadRatesDS.DATE)
                                };

                            var rows = dataView.ToTable().Select(string.Format("{0}='{1}' and {2}='{3}'", OrderPadRatesDS.INSTITUTEID, tdItem.BrokerID, OrderPadRatesDS.PRICEINSTITUTEID, tdItem.BankID));
                            foreach (DataRow row in rows)
                            {
                                holdingLimit = Convert.ToDecimal(row[OrderPadRatesDS.SMAHOLDINGLIMIT].ToString());
                                break;
                            }

                            var holdingDs = GetHoldingRptDataSet(order, broker);

                            var availableBalance = GetTDAvaiableFundsForSMA(holdingDs, brokerName, bankName, holdingLimit);

                            var curHolding = new ClientCurrentholdingContainer
                                {
                                    ClientID = order.ClientID,
                                    BankID = tdItem.BankID,
                                    BrokerID = tdItem.BrokerID,
                                    AvailableBalance = availableBalance
                                };
                            orderHoldings.Add(curHolding);
                        }
                    }
                    else if (item is ASXOrderItem)
                    {
                        var asxItem = item as ASXOrderItem;

                        if (!orderHoldings.Any(ss => ss.ClientID == order.ClientID && ss.SecuirtyCode == asxItem.InvestmentCode))
                        {
                            var org = broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                            if (org != null)
                            {
                                var sec = org.Securities.FirstOrDefault(ss => ss.AsxCode == asxItem.InvestmentCode);
                                if (sec != null)
                                {
                                    decimal holdingLimit = (decimal)sec.SMAHoldingLimit;

                                    var holdingDs = GetHoldingRptDataSet(order, broker);

                                    var availableBalance = GetSecurityAvaiableFundsForSMA(holdingDs, asxItem.InvestmentCode, holdingLimit, OrganizationType.DesktopBrokerAccount);
                                    var curHolding = new ClientCurrentholdingContainer
                                        {
                                            ClientID = order.ClientID,
                                            SecuirtyCode = asxItem.InvestmentCode,
                                            AvailableBalance = availableBalance
                                        };
                                    orderHoldings.Add(curHolding);
                                }
                            }
                        }
                    }
                }
            }
        }

        private static HoldingRptDataSet GetHoldingRptDataSet(OrderEntity order, ICMBroker broker)
        {
            var client = broker.GetBMCInstance(order.ClientCID);
            var holdingDs = new HoldingRptDataSet();
            client.GetData(holdingDs);
            broker.ReleaseBrokerManagedComponent(client);
            return holdingDs;
        }

        private static void CreateUnsettledForOrders(ICMBroker broker, UserEntity currentUser, List<OrderEntity> orders, bool createNew)
        {
            var clientOrders = orders.GroupBy(or => or.ClientID);
            foreach (var clientorder in clientOrders)
            {
                RemoveNCreateSettledUnsettledForClientOrders(clientorder.Key, clientorder.ToList(), broker, currentUser, createNew);
            }
        }

        private static bool ValidateOrder(OrderEntity order, ICMBroker broker, UserEntity user, out string smaMessage, List<ClientCurrentholdingContainer> orderHoldings)
        {
            bool result = true;
            order.ValidationMessage = "";
            if (!System.Enum.IsDefined(typeof(OrderStatus), order.Status))
            {
                order.ValidationMessage += "Invalid Order Status,";
                result = false;
            }
            if (!System.Enum.IsDefined(typeof(OrderType), order.OrderType))
            {
                order.ValidationMessage += "Invalid Order Entry Type,";
                result = false;
            }
            if (!System.Enum.IsDefined(typeof(OrderAccountType), order.OrderAccountType))
            {
                order.ValidationMessage += "Invalid Order Account Type,";
                result = false;
            }
            if (!System.Enum.IsDefined(typeof(OrderItemType), order.OrderItemType))
            {
                order.ValidationMessage += "Invalid Order Type,";
                result = false;
            }
            if (string.IsNullOrEmpty(order.AccountNumber))
            {
                order.ValidationMessage += "Invalid AccountNumber,";
                result = false;
            }
            if (string.IsNullOrEmpty(order.AccountBSB))
            {
                order.ValidationMessage += "Invalid AccountBSB,";
                result = false;
            }
            if (Guid.Empty == order.AccountCID)
            {
                order.ValidationMessage += "Invalid Account ReferenceID,";
                result = false;
            }
            if (string.IsNullOrEmpty(order.ClientID))
            {
                order.ValidationMessage += "Invalid ClientID,";
                result = false;
            }

            var ds = new ClientAccountProcessDS();
            var clientAccounts = new BankAccountDS();
            if (Guid.Empty == order.ClientCID)
            {
                order.ValidationMessage += "Invalid Client ReferenceID,";
                result = false;
            }
            else
            {
                GetASXProductId(order, broker);
                FillDataset(order.ClientCID, broker, ds, clientAccounts);
            }

            //Checking Wholesale Investor and Sophisticated Investor for Buy MIS(StateStreet) orders
            if (order.OrderAccountType == OrderAccountType.StateStreet && order.OrderItemType == OrderItemType.Buy)
            {
                //var comp = broker.GetBMCInstance(order.ClientCID) as OrganizationUnitCM;
                //if (comp != null)
                //{
                //    if (comp.IsCorporateType())
                //    {
                //        var entity = comp.ClientEntity as CorporateEntity;
                //        if (entity != null && entity.AuthoritytoAct != null)
                //            isValidType = entity.AuthoritytoAct.InvestorStatusWholesaleInvestor ||
                //                          entity.AuthoritytoAct.InvestorStatusSophisticatedInvestor;
                //    }
                //    else
                //    {
                //        var entity = comp.ClientEntity as ClientIndividualEntity;
                //        if (entity != null && entity.AuthoritytoAct != null)
                //            isValidType = entity.AuthoritytoAct.InvestorStatusWholesaleInvestor ||
                //                          entity.AuthoritytoAct.InvestorStatusSophisticatedInvestor;
                //    }
                //}
                //broker.ReleaseBrokerManagedComponent(comp);

                //if (!isValidType)
                //{
                //    order.ValidationMessage += "Innova Investments are only available for Wholesale and Sophisticated Investors,";
                //    result = false;
                //}

            }
            if (order.Items != null)
            {
                // validate Items
                foreach (var item in order.Items)
                {
                    result = result & ValidateItem(ds, clientAccounts, item, order.OrderItemType);
                }
            }

            //Amount/unit validation .
            var temp = ValidateUnitsNAmount(order, broker, false, orderHoldings);
            //order.ValidationMessage += temp ? string.Empty : "Insufficient amount or units";

            result = result & temp;

            order.Status = result ? OrderStatus.Validated : OrderStatus.ValidationFailed;
            order.UpdatedDate = DateTime.Now;
            order.UpdatedBy = user.CurrentUserName;
            order.UpdatedByCID = user.CID;

            smaMessage = SendOrderToSMA(order, broker, user);

            return result;
        }

        private static void GetASXProductId(OrderEntity order, ICMBroker broker)
        {
            if (order.OrderAccountType == OrderAccountType.ASX && order.OrderType == OrderType.Manual &&
                order.OrderItemType == OrderItemType.Buy)
            {
                foreach (ASXOrderItem item in order.Items)
                {
                    if (item.ProductID == Guid.Empty)
                    {
                        //Getting ASXCID and ASXAccountNumber and ProductId
                        var orderPadDs = new OrderPadDS();
                        DataRow asxItemsRow = orderPadDs.ASXTable.NewRow();
                        asxItemsRow[orderPadDs.ASXTable.INVESTMENTCODE] = item.InvestmentCode;
                        orderPadDs.ASXTable.Rows.Add(asxItemsRow);
                        IBrokerManagedComponent clientData = broker.GetBMCInstance(order.ClientCID);
                        if (orderPadDs.ExtendedProperties.Count >= 0 && orderPadDs.ExtendedProperties["IsBuy"] == null)
                        {
                            orderPadDs.ExtendedProperties.Add("IsBuy", "true");
                        }
                        clientData.GetData(orderPadDs);
                        broker.ReleaseBrokerManagedComponent(clientData);

                        var asxAccCid = orderPadDs.ASXTable.Rows[0][orderPadDs.ASXTable.ASXACCOUNTCID].ToString();
                        item.ASXAccountCID = string.IsNullOrEmpty(asxAccCid) ? Guid.Empty : Guid.Parse(asxAccCid);
                        var productID = orderPadDs.ASXTable.Rows[0][orderPadDs.ASXTable.PRODUCTID].ToString();
                        item.ProductID = string.IsNullOrEmpty(productID) ? Guid.Empty : Guid.Parse(productID);
                        item.ASXAccountNo = orderPadDs.ASXTable.Rows[0][orderPadDs.ASXTable.ASXACCOUNTNO].ToString();
                    }
                }
            }
        }

        public static string SendOrderToSMA(OrderEntity order, ICMBroker broker, UserEntity user)
        {
            string message = string.Empty;
            if (order.ClientManagementType == ClientManagementType.SMA && order.Status >= OrderStatus.Validated)
            {
                var org = broker.GetWellKnownBMC(WellKnownCM.Organization);
                var ds = new SMAOrderExportDS() { Unit = new Data.OrganizationUnit { CurrentUser = user, }, Data = order, Command = (int)WebCommands.ExportToSMA, Operation = SMAOperationType.SingleOrder };
                org.SetData(ds);

                broker.ReleaseBrokerManagedComponent(org);

                if (ds.ExtendedProperties.Contains("SMAExportMessage"))
                {
                    message = ds.ExtendedProperties["SMAExportMessage"].ToString();
                }
            }
            return message;
        }
        public static string SendBulkToSMA(BulkOrderEntity bulkOrderEntity, List<OrderEntity> ordersToUpdate, UserEntity userEntity, ICMBroker broker)
        {
            string message = string.Empty;
            if (bulkOrderEntity.ClientManagementType == ClientManagementType.SMA && bulkOrderEntity.Status == BulkOrderStatus.Complete)
            {
                var org = broker.GetWellKnownBMC(WellKnownCM.Organization);
                var ds = new SMAOrderExportDS() { Unit = new Data.OrganizationUnit { CurrentUser = userEntity, }, Command = (int)WebCommands.ExportToSMA, Operation = SMAOperationType.BulkOrders };
                // var bulkOrders = (from bulkOrderItem in bulkOrderEntity.BulkOrderItems from orderItem in bulkOrderItem.OrderItems select ordersToUpdate.FirstOrDefault(ss => ss.ID == orderItem.OrderID && ss.OrderBulkStatus == OrderBulkStatus.BulkTrade) into order where order != null select order).ToList();
                var keyValue = new KeyValuePair<BulkOrderEntity, List<OrderEntity>>(bulkOrderEntity, ordersToUpdate);
                ds.Data = keyValue;
                org.SetData(ds);
                broker.ReleaseBrokerManagedComponent(org);
                if (ds.ExtendedProperties.Contains("SMAExportMessage"))
                {
                    message = ds.ExtendedProperties["SMAExportMessage"].ToString();
                }
            }
            return message;
        }
        private static void FillDataset(Guid clientCId, ICMBroker broker, params DataSet[] argds)
        {

            if (Guid.Empty != clientCId)
            {
                IBrokerManagedComponent clientData = broker.GetBMCInstance(clientCId);
                foreach (var dataSet in argds)
                {
                    clientData.GetData(dataSet);
                }

                broker.ReleaseBrokerManagedComponent(clientData);
            }


        }

        public static bool ValidateItem(ClientAccountProcessDS ds, BankAccountDS clietnAccounts, OrderItemEntity item, OrderItemType type)
        {
            bool result = true;

            item.ValidationMessage = string.Empty;

            switch (type)
            {
                case OrderItemType.Buy:
                    if (item.Amount.HasValue && item.Amount.Value <= 0)
                    {
                        item.ValidationMessage += "Invalid Amount,";
                        result = false;
                    }
                    if ((item.Units.HasValue && item.Units.Value <= 0))
                    {
                        item.ValidationMessage += "Invalid Units,";
                        result = false;
                    }

                    break;
                case OrderItemType.Sell:
                    if ((item.Units.HasValue && item.Units.Value <= 0) && (item.Amount.HasValue && item.Amount.Value == 0))
                    {
                        item.ValidationMessage += "Invalid Units,";
                        result = false;
                    }
                    if (item.Amount.HasValue && item.Amount.Value <= 0 && (item.Units.HasValue && item.Units.Value == 0))
                    {
                        item.ValidationMessage += "Invalid Amount,";
                        result = false;
                    }
                    break;
                default:
                    item.ValidationMessage += "Invalid Order Status,";
                    result = false;
                    break;
            }

            if (item is StateStreetOrderItem)
            {
                result = result & ValidateStateStreetItem(item as StateStreetOrderItem, ds);
            }
            else if (item is ASXOrderItem)
            {
                result = result & ValidateAsxItem(item as ASXOrderItem, ds);
            }
            else if (item is TermDepositOrderItem)
            {
                result = result & ValidateTDItem(item as TermDepositOrderItem, type, ds);
            }

            else if (item is AtCallOrderItem)
            {
                result = result & ValidateAtCallItem(item as AtCallOrderItem, ds, clietnAccounts, type);
            }

            return result;
        }

        public static bool ValidateStateStreetItem(StateStreetOrderItem item, ClientAccountProcessDS ds)
        {
            bool result = true;
            if (string.IsNullOrEmpty(item.FundCode))
            {
                item.ValidationMessage += "Invalid FundCode,";
                result = false;
            }

            Guid productId = Guid.Empty;
            var dataview = new DataView(ds.Tables[ClientAccountProcessDS.ACCOUNTPROCESS_TABLE]) { RowFilter = string.Format("{0} = '{1}' and {2} = '{3}'", ClientAccountProcessDS.LINKEDENTITYTYPE, OrganizationType.ManagedInvestmentSchemesAccount, ClientAccountProcessDS.LINKENTITYFUNDCODE, item.FundCode) };
            if (dataview.Count > 0)
            {
                DataRow dr = dataview.ToTable().Rows[0];
                if (dr[ClientAccountProcessDS.TASKID] != DBNull.Value)
                {
                    productId = Guid.Parse(dr[ClientAccountProcessDS.TASKID].ToString());
                }

            }

            if (item.ProductID == Guid.Empty)
            {
                if (productId != Guid.Empty)
                {
                    item.ProductID = productId;
                }
                else
                {
                    item.ValidationMessage += "There is no product associated with this security,";
                    result = false;
                }
            }
            return result;
        }

        public static bool ValidateAsxItem(ASXOrderItem item, ClientAccountProcessDS ds)
        {
            bool result = true;
            if (string.IsNullOrEmpty(item.InvestmentCode))
            {
                item.ValidationMessage += "Invalid InvestmentCode,";
                result = false;
            }

            if (item.ProductID == Guid.Empty)
            {
                item.ValidationMessage += "There is no product associated with this security,";
                result = false;
            }

            if (item.ASXAccountCID == Guid.Empty)
            {
                item.ValidationMessage += "Invalid ASX Account CID,";
                result = false;
            }
            if (string.IsNullOrEmpty(item.ASXAccountNo))
            {
                item.ValidationMessage += "Invalid ASX Account Number,";
                result = false;
            }

            return result;
        }

        public static bool ValidateTDItem(TermDepositOrderItem item, OrderItemType type, ClientAccountProcessDS ds)
        {
            bool result = true;
            if (item.BrokerID == Guid.Empty)
            {
                item.ValidationMessage += "Invalid BrokerID,";
                result = false;
            }
            if (item.BankID == Guid.Empty)
            {
                item.ValidationMessage += "Invalid BankID,";
                result = false;
            }
            if (item.Min.HasValue && item.Min.Value < 0)
            {
                item.ValidationMessage += "Invalid Min Value,";
                result = false;
            }

            if (item.Max.HasValue && item.Max.Value < 0)
            {
                item.ValidationMessage += "Invalid Max Value,";
                result = false;
            }

            if (item.Percentage.HasValue && item.Percentage.Value < 0)
            {
                item.ValidationMessage += "Invalid Percentage Value,";
                result = false;
            }
            var dataview = new DataView(ds.Tables[ClientAccountProcessDS.ACCOUNTPROCESS_TABLE]) { RowFilter = string.Format("{0} = '{1}'", ClientAccountProcessDS.LINKEDENTITYTYPE, OrganizationType.TermDepositAccount) };
            dataview = dataview.ToTable(true, ClientAccountProcessDS.INSTITUTEID, ClientAccountProcessDS.BROKERID, ClientAccountProcessDS.TASKID, ClientAccountProcessDS.CID, ClientAccountProcessDS.SERVICETYPE, ClientAccountProcessDS.STATUS).DefaultView;

            Guid tdCid = Guid.Empty;
            Guid productID = Guid.Empty;
            string tdAccStatus = string.Empty;

            if (dataview.Count > 0)
            {
                DataRow[] drs = dataview.ToTable().Select(ClientAccountProcessDS.BROKERID + "='" + item.BrokerID + "'");
                if (drs.Length > 0)
                {
                    if (drs[0][ClientAccountProcessDS.TASKID] != DBNull.Value)
                        productID = Guid.Parse(drs[0][ClientAccountProcessDS.TASKID].ToString());
                    if (drs[0][ClientAccountProcessDS.CID] != DBNull.Value)
                        tdCid = Guid.Parse(drs[0][ClientAccountProcessDS.CID].ToString());
                    if (drs[0][ClientAccountProcessDS.STATUS] != DBNull.Value)
                        tdAccStatus = drs[0][ClientAccountProcessDS.STATUS].ToString();
                }
            }

            if (item.TDAccountCID == Guid.Empty)
            {
                if (result && tdCid != Guid.Empty)//if every thing gone right till this stage then go and fetch account info.
                {
                    item.TDAccountCID = tdCid;
                }
                else
                {
                    item.ValidationMessage += "TD Account Not Found in Account Process,";
                    result = false;
                }
            }

            if (item.ProductID == Guid.Empty)
            {
                if (result && productID != Guid.Empty)//if every thing gone right till this stage then go and fetch product info.
                {
                    item.ProductID = productID;
                }
                else
                {
                    item.ValidationMessage += "Product in Account Process Not Found,";
                    result = false;
                }
            }

            if (item.ProductID != Guid.Empty && tdAccStatus.ToLower() != "active")
            {
                item.ValidationMessage += "Product in Account Process is Not Active,";
                result = false;
            }

            if (type == OrderItemType.Buy)
            {

                if (string.IsNullOrEmpty(item.Rating))
                {
                    item.ValidationMessage += "Invalid Rating Value,";
                    result = false;
                }
                if (string.IsNullOrEmpty(item.Duration))
                {
                    item.ValidationMessage += "Invalid Duration Value,";
                    result = false;
                }
            }
            return result;
        }

        public static bool ValidateAtCallItem(AtCallOrderItem item, ClientAccountProcessDS ds, BankAccountDS clietnAccounts, OrderItemType type)
        {
            bool result = true;

            if (item.AtCallType == AtCallType.MoneyMovement)
            {
                if (item.TransferAccCID == Guid.Empty)
                {
                    item.ValidationMessage += "Invalid Transfer To Account CID,";
                    result = false;
                }
                else
                {
                    DataRow[] rows = clietnAccounts.BankAccountsTable.Select(string.Format("{0}='{1}'", clietnAccounts.BankAccountsTable.CID, item.TransferAccCID));

                    if (rows.Length > 0)
                    {
                        item.IsExternalAccount = bool.Parse(rows[0][clietnAccounts.BankAccountsTable.ISEXTERNALACCOUNT].ToString());
                    }
                }

                if (string.IsNullOrEmpty(item.TransferAccBSB))
                {
                    item.ValidationMessage += "Invalid Transfer To Account BSB,";
                    result = false;
                }
                if (string.IsNullOrEmpty(item.TransferAccNumber))
                {
                    item.ValidationMessage += "Invalid Transfer To Account Number,";
                    result = false;
                }
            }
            else if (item.AtCallType == AtCallType.BestRates || item.AtCallType == AtCallType.MoneyMovementBroker)
            {
                if (item.BrokerID == Guid.Empty)
                {
                    item.ValidationMessage += "Invalid BrokerID,";
                    result = false;
                }
                if (item.BankID == Guid.Empty)
                {
                    item.ValidationMessage += "Invalid BankID,";
                    result = false;
                }
                if (item.Min.HasValue && item.Min.Value < 0)
                {
                    item.ValidationMessage += "Invalid Min Value,";
                    result = false;
                }

                if (item.Max.HasValue && item.Max.Value < 0)
                {
                    item.ValidationMessage += "Invalid Max Value,";
                    result = false;
                }

                //if (item.Rate.HasValue && item.Rate.Value < 0)
                //{
                //    item.ValidationMessage += "Invalid Percentage Value,";
                //    result = false;
                //}

                if (type == OrderItemType.Buy && string.IsNullOrEmpty(item.AccountTier))
                {
                    item.ValidationMessage += "Invalid Accounts/Description,";
                    result = false;
                }
                var dataview = new DataView(ds.Tables[ClientAccountProcessDS.ACCOUNTPROCESS_TABLE]) { RowFilter = string.Format("{0} = '{1}'", ClientAccountProcessDS.LINKEDENTITYTYPE, OrganizationType.TermDepositAccount) };
                dataview = dataview.ToTable(true, ClientAccountProcessDS.BROKERID, ClientAccountProcessDS.TASKID, ClientAccountProcessDS.CID, ClientAccountProcessDS.SERVICETYPE, ClientAccountProcessDS.STATUS).DefaultView;

                Guid atCallCid = Guid.Empty;
                Guid productID = Guid.Empty;
                string atCallAccStatus = string.Empty;

                if (dataview.Count > 0)
                {
                    DataRow[] drs = dataview.ToTable().Select(ClientAccountProcessDS.BROKERID + "='" + item.BrokerID + "'");
                    if (drs.Length > 0)
                    {
                        if (drs[0][ClientAccountProcessDS.TASKID] != DBNull.Value)
                            productID = Guid.Parse(drs[0][ClientAccountProcessDS.TASKID].ToString());
                        if (drs[0][ClientAccountProcessDS.CID] != DBNull.Value)
                            atCallCid = Guid.Parse(drs[0][ClientAccountProcessDS.CID].ToString());
                        if (drs[0][ClientAccountProcessDS.STATUS] != DBNull.Value)
                            atCallAccStatus = drs[0][ClientAccountProcessDS.STATUS].ToString();
                    }
                }

                if (item.AtCallAccountCID == Guid.Empty)
                {
                    if (result && atCallCid != Guid.Empty)//if every thing gone right till this stage then go and fetch account info.
                    {
                        item.AtCallAccountCID = atCallCid;
                    }
                    else
                    {
                        item.ValidationMessage += "At Call Account Not Found in Account Process,";
                        result = false;
                    }
                }

                if (item.ProductID == Guid.Empty)
                {
                    if (result && productID != Guid.Empty)//if every thing gone right till this stage then go and fetch product info.
                    {
                        item.ProductID = productID;
                    }
                    else
                    {
                        item.ValidationMessage += "Product in Account Process Not Found,";
                        result = false;
                    }
                }

                if (item.ProductID != Guid.Empty && atCallAccStatus.ToLower() != "active")
                {
                    item.ValidationMessage += "Product in Account Process is Not Active,";
                    result = false;
                }
            }

            return result;
        }

        public static void ApproveOrder(OrderEntity order, ICMBroker broker, UserEntity user)
        {
            var orderHoldings = new List<ClientCurrentholdingContainer>();
            bool result = ValidateUnitsNAmount(order, broker, true, orderHoldings);
            order.Status = result ? OrderStatus.Approved : OrderStatus.ValidationFailed;
            // order.ValidationMessage = result ? string.Empty : "Insufficient amount or units";
            order.UpdatedDate = DateTime.Now;
            order.UpdatedBy = user.CurrentUserName;
            order.UpdatedByCID = user.CID;
            if (order.Status == OrderStatus.ValidationFailed)//remove unsettled if order is failed in validation
                RemoveNCreateSettledUnsettledEntities(order, broker, user);
        }

        private static bool ValidateUnitsNAmount(OrderEntity order, ICMBroker broker, bool deductOrderAmount, List<ClientCurrentholdingContainer> orderHoldings)
        {
            bool result = false;
            var client = broker.GetBMCInstance(order.ClientCID);
            var org = broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;

            var ds = new HoldingRptDataSet();
            client.GetData(ds);

            var bankDs = new BankAccountDS();
            client.GetData(bankDs);

            if (order.Items != null && order.Items.Count > 0)
            {
                switch (order.OrderItemType)
                {
                    case OrderItemType.Buy:
                        {
                            var pamount = order.Items.Sum(ss => ss.Amount);
                            if (pamount > 0)
                            {
                                result = CheckAmount(order, pamount.Value, ds, deductOrderAmount, bankDs, broker, orderHoldings);
                            }
                        }
                        break;
                    case OrderItemType.Sell:
                        {
                            bool tempresult = true;
                            foreach (var item in order.Items)
                            {
                                switch (order.OrderAccountType)
                                {
                                    case OrderAccountType.TermDeposit:
                                        if (item.Amount.HasValue)
                                        {
                                            //if (item.Units.HasValue)
                                            decimal tdAmount;
                                            tempresult = tempresult &
                                                         CheckTDAmount(item.Amount.Value, ds,
                                                                   (item as TermDepositOrderItem).BankID, (item as TermDepositOrderItem).BrokerID,
                                                                   (item as TermDepositOrderItem).TDAccountCID,
                                                                   OrganizationType.TermDepositAccount, out tdAmount, deductOrderAmount);
                                            item.ValidationMessage += tempresult ? string.Empty : "Insufficient amount (Available amount is " + tdAmount.ToString("N2") + ")";
                                        }
                                        break;
                                    case OrderAccountType.StateStreet:
                                        if (!item.Units.HasValue && item.Amount.HasValue)
                                        {
                                            var sec = org.Securities.FirstOrDefault(ss => ss.AsxCode == (item as StateStreetOrderItem).FundCode);
                                            decimal unitPrice = 0;
                                            if (sec != null)
                                            {
                                                var latestprice = sec.ASXSecurity.OrderByDescending(ss => ss.Date).FirstOrDefault();
                                                if (latestprice != null)
                                                {
                                                    unitPrice = (decimal)latestprice.UnitPrice;
                                                }
                                                if (unitPrice != 0)
                                                    item.Units = item.Amount.Value / unitPrice;
                                            }
                                        }
                                        if (item.Units.HasValue)
                                        {
                                            decimal ssUnits;
                                            tempresult = tempresult &
                                                         CheckUnit(item.Units.Value, ds,
                                                                   (item as StateStreetOrderItem).ProductID,
                                                                   (item as StateStreetOrderItem).FundCode,
                                                                   OrganizationType.ManagedInvestmentSchemesAccount, out ssUnits, deductOrderAmount);
                                            item.ValidationMessage += tempresult ? string.Empty : "Insufficient units (" + (item as StateStreetOrderItem).FundCode + ") (Available units are " + ssUnits.ToString("N4") + ")";
                                        }
                                        break;
                                    case OrderAccountType.ASX:
                                        decimal asxUnits;
                                        if (item.Units.HasValue)
                                        {
                                            tempresult = tempresult &
                                                         CheckUnit(item.Units.Value, ds, (item as ASXOrderItem).ProductID,
                                                                   (item as ASXOrderItem).InvestmentCode,
                                                                   OrganizationType.DesktopBrokerAccount, out asxUnits, deductOrderAmount);
                                            item.ValidationMessage += tempresult ? string.Empty : "Insufficient units (" + (item as ASXOrderItem).InvestmentCode + ") (Available units are " + asxUnits.ToString("N4") + ")";
                                        }
                                        break;
                                    case OrderAccountType.AtCall:
                                        tempresult = item.Amount.HasValue && (item.Amount > 0);
                                        break;
                                }
                            }
                            if (order.Items != null && order.Items.Count > 0)
                            {
                                result = tempresult;
                            }
                        }
                        break;
                }
            }
            broker.ReleaseBrokerManagedComponent(org);
            broker.ReleaseBrokerManagedComponent(client);
            return result;
        }

        private static bool CheckTDAmount(decimal value, HoldingRptDataSet ds, Guid bankID, Guid brokerID, Guid tdAccountCID, OrganizationType termDepositAccount, out decimal tdAmount, bool deductOrderAmount)
        {
            var drs = ds.TDBreakDownTable.Select(string.Format("{0}='{1}' and {2}='{3}' and {4}='{5}' and {6}='{7}'and {8}='{9}'", ds.TDBreakDownTable.BROKERID, brokerID, ds.TDBreakDownTable.TDACCOUNTCID, tdAccountCID, ds.TDBreakDownTable.TRANSTYPE, "Application", ds.TDBreakDownTable.INSID, bankID, ds.TDBreakDownTable.HOLDING, value));
            decimal amount = 0;
            //TD Sell Has been Closed for now so work on Considering Sell Unsettled  has been  stopped.
            foreach (DataRow dataRow in drs)
            {
                // todo: consider unsettled amount
                amount += decimal.Parse(dataRow[ds.TDBreakDownTable.HOLDING].ToString());
            }

            bool result = false;
            if (amount > 0 && value == amount)
            {
                result = true;
            }
            tdAmount = amount;
            return result;
        }

        public static void RemoveNCreateSettledUnsettledEntities(OrderEntity order, ICMBroker broker, UserEntity user)
        {
            var settledUnsetteldCID = SettledUnsettledUtilities.CheckIfSettelledCmExists(order.ClientID, broker, user, false);

            if (settledUnsetteldCID != Guid.Empty)
            {
                var ds = new SettledUnsetteledDS
                {
                    Unit = new Data.OrganizationUnit
                    {
                        ClientId = order.ClientID,
                        CurrentUser = user,
                        Type = OrganizationType.SettledUnsettled.ToString(),
                    },
                    CommandType = DatasetCommandTypes.Delete,
                };
                ds.ExtendedProperties.Add("Order", order);

                var settledCM = broker.GetBMCInstance(settledUnsetteldCID);
                settledCM.SetData(ds);

                if (order.Status >= OrderStatus.Validated && order.Status != OrderStatus.Cancelled)
                {
                    ds.CommandType = DatasetCommandTypes.Add;
                    settledCM.SetData(ds);
                }
                broker.ReleaseBrokerManagedComponent(settledCM);
            }
            else
            {
                if (order.Status >= OrderStatus.Validated && order.Status != OrderStatus.Cancelled)
                {
                    SettledUnsettledUtilities.CreateNewUnsettledEntity(order, broker, user);
                }
            }
        }

        public static void RemoveNCreateSettledUnsettledForClientOrders(string clientID, List<OrderEntity> orders, ICMBroker broker, UserEntity user, bool createNew)
        {
            var settledUnsetteldCID = SettledUnsettledUtilities.CheckIfSettelledCmExists(clientID, broker, user, false);

            if (settledUnsetteldCID != Guid.Empty)
            {
                var ds = new SettledUnsetteledDS
                {
                    Unit = new Data.OrganizationUnit
                    {
                        ClientId = clientID,
                        CurrentUser = user,
                        Type = OrganizationType.SettledUnsettled.ToString(),
                    },

                };
                ds.ExtendedProperties.Add("Orders", orders);


                var settledCM = broker.GetBMCInstance(settledUnsetteldCID);
                if (createNew)
                {
                    ds.CommandType = DatasetCommandTypes.Delete;
                    settledCM.SetData(ds);

                    ds.CommandType = DatasetCommandTypes.Add;
                    settledCM.SetData(ds);
                }
                else
                {
                    ds.CommandType = DatasetCommandTypes.Update;
                    settledCM.SetData(ds);
                }

                broker.ReleaseBrokerManagedComponent(settledCM);
            }
            else
            {
                SettledUnsettledUtilities.CreateNewUnsettledEntityForSubmiited(clientID, orders, broker, user);
            }
        }

        private static bool CheckUnit(decimal units, HoldingRptDataSet ds, Guid productID, string investmentCode, OrganizationType accountType, out decimal clientunits, bool deductOrderAmount)
        {
            bool result = false;

            string filter = string.Format("{0}='{1}' and {2}='{3}' and {4}='{5}'", ds.ProductBreakDownTable.PRODUCTID, productID, ds.ProductBreakDownTable.LINKEDENTITYTYPE, accountType, ds.ProductBreakDownTable.INVESMENTCODE, investmentCode);

            DataRow[] drs = ds.ProductBreakDownTable.Select(filter);
            decimal accunits = 0;
            if (drs.Length > 0)
            {
                //In case of Sell: Client Units= Original units - UnsettledSellUnits
                accunits = decimal.Parse(drs[0][ds.ProductBreakDownTable.HOLDING].ToString()) + ((deductOrderAmount) ? units : 0) + decimal.Parse(drs[0][ds.ProductBreakDownTable.UNSETTLEDSELL].ToString());
                if (units <= accunits)
                {
                    result = true;
                }
            }
            clientunits = accunits;
            return result;
        }

        private static bool CheckAmount(OrderEntity order, decimal amount, HoldingRptDataSet ds, bool deductOrderAmount, BankAccountDS bankDs, ICMBroker broker, List<ClientCurrentholdingContainer> orderHoldings)
        {
            bool result = false;
            //Getting Total Holdings
            decimal totalHolding = (decimal)ds.HoldingSummaryTable.Compute(string.Format("Sum({0})", ds.HoldingSummaryTable.TOTAL), "");
            string filter = string.Format("{0}='{1}'", ds.HoldingSummaryTable.ACCOUNTCID, order.AccountCID);

            DataRow[] cashAccounts = ds.HoldingSummaryTable.Select(filter);
            decimal accAmount = 0;
            if (cashAccounts.Length > 0)
            {
                //In case of buy: Client Amount= original amount -unsetteledBuyAmount
                accAmount = decimal.Parse(cashAccounts[0][ds.HoldingSummaryTable.HOLDING].ToString()) + ((deductOrderAmount) ? amount : 0) + decimal.Parse(cashAccounts[0][ds.HoldingSummaryTable.UNSETTLEDBUY].ToString());
            }
            else
            {
                filter = string.Format("{0}='{1}'", bankDs.BankAccountsTable.CID, order.AccountCID);
                cashAccounts = bankDs.BankAccountsTable.Select(filter);
                if (cashAccounts.Length > 0)
                {
                    //In case of buy: Client Amount= original amount -unsetteledBuyAmount
                    accAmount = decimal.Parse(cashAccounts[0][bankDs.BankAccountsTable.TRANSACTIONHOLDING].ToString()) + ((deductOrderAmount) ? amount : 0) + decimal.Parse(cashAccounts[0][bankDs.BankAccountsTable.UNSETTLEDBUY].ToString());
                }
            }
            //Calculating MinCashValue For SMA and Setting Available Funds.
            if (order.ClientManagementType == ClientManagementType.SMA)
            {
                decimal minValue;
                decimal maxValue;
                var percentage = CalculateSMAPercentage(ds, broker, out minValue, out maxValue);
                decimal minCashValue = totalHolding * percentage;
                if (minCashValue < minValue)
                    minCashValue = minValue;
                else if (minCashValue > maxValue)
                    minCashValue = maxValue;
                decimal smaAvailableFund = accAmount - minCashValue;
                accAmount = smaAvailableFund;
            }
            if (amount <= accAmount)
            {
                result = true;
            }
            order.ValidationMessage += result ? string.Empty : "Insufficient amount in account ( " + order.AccountNumber + ") (Available amount is " + accAmount.ToString("N2") + ")";


            result = CheckSMAAvailableHoldingLimit(order, orderHoldings, result);

            return result;
        }

        private static bool CheckSMAAvailableHoldingLimit(OrderEntity order, List<ClientCurrentholdingContainer> orderHoldings, bool result)
        {
            if (order.ClientManagementType == ClientManagementType.SMA && orderHoldings.Count > 0)
            {
                foreach (var item in order.Items)
                {
                    if (item is TermDepositOrderItem)
                    {
                        var tdItem = item as TermDepositOrderItem;

                        var curHolding = orderHoldings.FirstOrDefault(ss => ss.ClientID == order.ClientID && ss.BrokerID == tdItem.BrokerID && ss.BankID == tdItem.BankID);
                        if (curHolding != null)
                        {
                            decimal availableBalance = curHolding.AvailableBalance;
                            if (tdItem.Amount > availableBalance)
                            {
                                tdItem.ValidationMessage += "Amount exceeds available holding limit,";
                                result = false;
                            }
                        }
                    }
                    else if (item is ASXOrderItem)
                    {
                        var asxItem = item as ASXOrderItem;
                        var curHolding = orderHoldings.FirstOrDefault(ss => ss.ClientID == order.ClientID && ss.SecuirtyCode == asxItem.InvestmentCode);
                        if (curHolding != null)
                        {
                            decimal availableBalance = curHolding.AvailableBalance;
                            if (asxItem.Amount > availableBalance)
                            {
                                asxItem.ValidationMessage += "Amount exceeds available holding limit,";
                                result = false;
                            }
                        }
                    }
                }
            }
            return result;
        }

        private static decimal GetSecurityAvaiableFundsForSMA(HoldingRptDataSet holdingDs, string investmentCode, decimal securityHoldingLimit, OrganizationType organizationType)
        {
            //Getting Total Holdings
            decimal totalHolding = (decimal)holdingDs.HoldingSummaryTable.Compute(string.Format("Sum({0})", holdingDs.HoldingSummaryTable.TOTAL), "");

            //Getting Current Holding of Security
            var prodDrs = holdingDs.ProductBreakDownTable.Select(string.Format("{0}='{1}' and {2}='{3}'", holdingDs.ProductBreakDownTable.INVESMENTCODE, investmentCode, holdingDs.ProductBreakDownTable.LINKEDENTITYTYPE, organizationType));
            decimal currentHolding = prodDrs.Sum(dr => Convert.ToDecimal(dr[holdingDs.ProductBreakDownTable.TOTAL]));

            //Calculating Current Holding
            decimal secCurrentHolding = currentHolding;

            //Calculating Holding Limit 
            decimal secHoldingLimit = (totalHolding * securityHoldingLimit) / 100;

            //Calculating Available Funds
            decimal secAvailableFund = secHoldingLimit - secCurrentHolding;

            return secAvailableFund;
        }

        private static decimal GetTDAvaiableFundsForSMA(HoldingRptDataSet holdingDs, string broker, string bank, decimal holdingLimit)
        {
            //Getting Total Holdings
            decimal totalHolding = (decimal)holdingDs.HoldingSummaryTable.Compute(string.Format("Sum({0})", holdingDs.HoldingSummaryTable.TOTAL), "");

            //Getting Current Holding of td by broker and bank/institute
            var tdDrs = holdingDs.HoldingSummaryTable.Select(string.Format("{0}='{1}-TD' and {2}='{3}' and {4}='{5}'", holdingDs.HoldingSummaryTable.PRODUCTNAME, broker, holdingDs.HoldingSummaryTable.DESCRIPTION, bank, holdingDs.HoldingSummaryTable.LINKEDENTITYTYPE, OrganizationType.TermDepositAccount));
            decimal currentHolding = tdDrs.Sum(dr => Convert.ToDecimal(dr[holdingDs.HoldingSummaryTable.TOTAL]));

            //Calculating Current Holding
            decimal tdCurrentHolding = currentHolding;

            //Calculating Holding Limit 
            decimal tdHoldingLimit = (totalHolding * holdingLimit) / 100;

            //Calculating Available Funds
            decimal tdAvailableFund = tdHoldingLimit - tdCurrentHolding;

            return tdAvailableFund;
        }


        public static decimal CalculateSMAPercentage(HoldingRptDataSet ds, ICMBroker broker, out decimal minValue, out decimal maxValue)
        {
            decimal percentage = 0;
            minValue = 0;
            maxValue = 0;
            var clientName = ds.Tables[BaseClientDetails.CLIENTSUMMARYTABLE].Rows[0][BaseClientDetails.CLIENTNAME].ToString().ToLower();

            IBrokerManagedComponent iBMC = broker.GetWellKnownBMC(WellKnownCM.Organization);
            var cashBalanceds = new CashBalanceDS { CommandType = DatasetCommandTypes.Get };
            iBMC.GetData(cashBalanceds);
            broker.ReleaseBrokerManagedComponent(iBMC);

            foreach (DataRow row in cashBalanceds.CashBalanceTable.Rows)
            {
                string accType = row[cashBalanceds.CashBalanceTable.SUPERACCOUNTTYPE].ToString();
                if (clientName.Contains("pension)") && accType != SuperAccountType.Pension.ToString())
                {
                    continue;
                }
                percentage = Convert.ToDecimal(row[cashBalanceds.CashBalanceTable.PERCENTAGE].ToString());
                minValue = Convert.ToDecimal(row[cashBalanceds.CashBalanceTable.MINVALUE].ToString());
                maxValue = Convert.ToDecimal(row[cashBalanceds.CashBalanceTable.MAXVALUE].ToString());
                break;
            }
            return percentage;
        }

        private static void CreateTransaction(Guid orderID, Guid orderItemID, string transType, string sysTransType, decimal? amount, decimal? rate, string product, Guid prodId, Guid bankId, Guid accCid, string clientId, UserEntity currentUser, ICMBroker broker, string duration = default(string))
        {
            var bankTransactionDetailsDS = new BankTransactionDetailsDS
                                               {
                                                   Unit = new Data.OrganizationUnit { ClientId = clientId, CurrentUser = currentUser },
                                                   DataSetOperationType = DataSetOperationType.NewSingleTD
                                               };

            DataRow row = bankTransactionDetailsDS.Tables[BankTransactionDetailsDS.BANKTRANSDETAILSTABLE].NewRow();
            row[BankTransactionDetailsDS.ORDERID] = orderID;
            row[BankTransactionDetailsDS.ORDERITEMID] = orderItemID;
            row[BankTransactionDetailsDS.ID] = Guid.NewGuid();
            row[BankTransactionDetailsDS.IMPORTTRANTYPE] = (sysTransType == "Redemption" ? "Withdrawal" : "Deposit");
            row[BankTransactionDetailsDS.CATTRANTYPE] = "Investment";
            row[BankTransactionDetailsDS.SYSTRANTYPE] = sysTransType;
            row[BankTransactionDetailsDS.AMOUNT] = (sysTransType == "Redemption" ? (-1 * amount) : amount);
            row[BankTransactionDetailsDS.ADJUSTMENT] = 0;
            row[BankTransactionDetailsDS.COMMENT] = string.Empty;
            row[BankTransactionDetailsDS.TRANTYPE] = transType;
            row[BankTransactionDetailsDS.TRANDATE] = DateTime.Now;
            if (rate.HasValue)
                row[BankTransactionDetailsDS.INTERESTRATE] = rate;

            row[BankTransactionDetailsDS.CONTRACTNOTE] = string.Empty;
            row[BankTransactionDetailsDS.ISCONFIRMED] = false;
            row[BankTransactionDetailsDS.PROID] = prodId;
            row[BankTransactionDetailsDS.INSTITUTEID] = bankId;
            row[BankTransactionDetailsDS.ADMINSYS] = product + "-" + transType;

            if (transType == "TD")
            {
                if (!string.IsNullOrEmpty(duration))
                {
                    var dur = duration.Split(' ');
                    int days;
                    int.TryParse(dur[0], out days);
                    row[BankTransactionDetailsDS.MATURITYDATE] = DateTime.Now.AddDays(days);
                }
            }

            bankTransactionDetailsDS.Tables[BankTransactionDetailsDS.BANKTRANSDETAILSTABLE].Rows.Add(row);

            IBrokerManagedComponent clientData = broker.GetBMCInstance(accCid);
            if (clientData != null)
            {
                clientData.SetData(bankTransactionDetailsDS);
                broker.ReleaseBrokerManagedComponent(clientData);
            }
        }

        public static void RemoveTransaction(OrderEntity order, ICMBroker broker, UserEntity user)
        {
            var ds = new BankTransactionDetailsDS
            {
                Unit = new Data.OrganizationUnit { ClientId = order.ClientID, CurrentUser = user },
                DataSetOperationType = DataSetOperationType.DeletBulkTD
            };

            DataRow row = ds.Tables[BankTransactionDetailsDS.BANKTRANSDETAILSTABLE].NewRow();
            row[BankTransactionDetailsDS.ORDERID] = order.ID;
            ds.Tables[BankTransactionDetailsDS.BANKTRANSDETAILSTABLE].Rows.Add(row);

            foreach (var item in order.Items)
            {
                Guid accCid = Guid.Empty;

                if (item is TermDepositOrderItem)
                {
                    var tdItem = item as TermDepositOrderItem;
                    accCid = tdItem.TDAccountCID;

                }
                else if (item is AtCallOrderItem)
                {
                    var atCallItem = item as AtCallOrderItem;
                    accCid = atCallItem.AtCallAccountCID;
                }
                if (accCid != Guid.Empty)
                {
                    IBrokerManagedComponent clientData = broker.GetBMCInstance(accCid);
                    if (clientData != null)
                    {
                        clientData.SetData(ds);
                        broker.ReleaseBrokerManagedComponent(clientData);
                    }
                }
            }
        }

        #region Export Instructions
        public static void ExportInstructions(ExportDS ds, List<OrderEntity> orders, ICMBroker broker, long FileNameSequanceNumber)
        {
            AddTablesIntoDataSetForExport(ds);
            string TradeDate = string.Empty;
            if (ds.ExtendedProperties["TradeDate"] != null)
                TradeDate = ds.ExtendedProperties["TradeDate"].ToString();

            //For Orders
            foreach (var order in orders)
            {
                if (!string.IsNullOrEmpty(TradeDate))
                    order.TradeDate = DateTime.Parse(TradeDate);

                switch (order.OrderAccountType)
                {
                    case OrderAccountType.StateStreet:
                        CreateInstructionsForStateStreet(order, broker, FileNameSequanceNumber);
                        ExportStateStreetInstructions(order, ds);

                        break;
                    case OrderAccountType.ASX:
                        CreateInstructionsForASX(broker, order);
                        ExportASXInstructions(order, ds);

                        break;
                    case OrderAccountType.TermDeposit:
                        CreateInstructionsForTD(order, broker);
                        ExportTDInstructions(order, ds, broker);

                        break;
                    case OrderAccountType.AtCall:
                        CreateInstructionsForAtCall(order, broker);
                        ExportAtCallInstructions(order, ds, broker);
                        break;
                }
            }
            //adding orders list in ds to update order status after successful export
            ds.ExtendedProperties.Add("Orders", orders);
        }

        public static void ExportInstructions(ExportDS ds, List<OrderEntity> orders, List<BulkOrderEntity> bulkOrders, ICMBroker broker)
        {
            AddTablesIntoDataSetForExport(ds);

            //For Bulk Orders
            foreach (var bulkOrder in bulkOrders)
            {
                switch (bulkOrder.OrderAccountType)
                {
                    case OrderAccountType.ASX:
                        CreateInstructionsForASXBulk(broker, bulkOrder);
                        ExportASXBulkInstructions(bulkOrder, ds);
                        break;
                    case OrderAccountType.TermDeposit:
                        CreateInstructionsForTDBulk(bulkOrder);
                        ExportTDBulkInstructions(bulkOrder, ds, broker);
                        break;
                }
            }
            //adding orders and bulk orders list in ds to update order status after successful export
            ds.ExtendedProperties.Add("Orders", orders);
            ds.ExtendedProperties.Add("BulkOrders", bulkOrders);
        }

        #region Export State Street Instructions
        private static void ExportStateStreetInstructions(OrderEntity orderEntity, ExportDS ds)
        {
            var stateStreetEntity = orderEntity.Instructions.Where(ss => ss.Type == InstructionType.StateStreet).ToList();
            if (!ds.ExtendedProperties.ContainsKey("SequanceNumber"))
                ds.ExtendedProperties.Add("SequanceNumber", string.Empty);
            foreach (OrderPadStateStreetInstructionEntity entity in stateStreetEntity)
            {

                if (entity.FundCode.ToLower() == "iv004" || entity.FundCode.ToLower() == "iv014")
                {
                    DataRow dr = ds.Tables["StateStreetForP2Export"].NewRow();
                    dr["Unitholder Code"] = entity.UnitholderCode;
                    dr["Unitholder Sub Code"] = entity.UnitHolderSubCode;
                    dr["Fund Code"] = entity.FundCode;
                    dr["MCH Code"] = entity.MCHCode;
                    dr["Trade Date"] = entity.TradeDate.ToString("yyyyMMdd");
                    dr["Clear Date"] = entity.ClearDate.ToString("yyyyMMdd");
                    if (entity.Amount.HasValue)
                        dr["Amount"] = entity.Amount.Value.ToString("N2");
                    dr["Redemption or Exchange Options"] = entity.RedumptionOrExchangeOption;
                    dr["Transaction Type"] = entity.Transactiontype;
                    dr["Number of Shares"] = entity.NumberOfShares;
                    dr["Exchange to Fund"] = entity.ExchangeToFund;
                    dr["Dealer Commission Rate"] = entity.DealerCommissionRate;
                    dr["Currency of Trade"] = entity.CurrencyOfTrade;
                    dr["External ID"] = entity.ExternalID;
                    dr["Purchase Source"] = entity.PurchaseSource;
                    dr["Redemption Source"] = entity.RedumptionSource;
                    dr["Operator Entered By"] = entity.OperatorEnteredBy;
                    dr["Process at NAV"] = entity.ProcessAtNAV;
                    dr["Generator"] = entity.Generator;
                    dr["Effective Date"] = entity.EffectuveDate;
                    dr["AUD Equivalent Amount"] = entity.AUDEquivalentAmount;
                    ds.Tables["StateStreetForP2Export"].Rows.Add(dr);
                    if (string.IsNullOrEmpty(ds.ExtendedProperties["SequanceNumber"].ToString()))
                        ds.ExtendedProperties["SequanceNumber"] = entity.FileNameSequanceNumber.ToString();
                }
                else
                {
                    DataRow dr = ds.Tables["StateStreetExport"].NewRow();
                    dr["Unitholder Code"] = entity.UnitholderCode;
                    dr["Unitholder Sub Code"] = entity.UnitHolderSubCode;
                    dr["Fund Code"] = entity.FundCode;
                    dr["MCH Code"] = entity.MCHCode;
                    dr["Trade Date"] = entity.TradeDate.ToString("yyyyMMdd");
                    dr["Clear Date"] = entity.ClearDate.ToString("yyyyMMdd");
                    if (entity.Amount.HasValue)
                        dr["Amount"] = entity.Amount.Value.ToString("N2");
                    dr["Redemption or Exchange Options"] = entity.RedumptionOrExchangeOption;
                    dr["Transaction Type"] = entity.Transactiontype;
                    dr["Number of Shares"] = entity.NumberOfShares;
                    dr["Exchange to Fund"] = entity.ExchangeToFund;
                    dr["Dealer Commission Rate"] = entity.DealerCommissionRate;
                    dr["Currency of Trade"] = entity.CurrencyOfTrade;
                    dr["External ID"] = entity.ExternalID;
                    dr["Purchase Source"] = entity.PurchaseSource;
                    dr["Redemption Source"] = entity.RedumptionSource;
                    dr["Operator Entered By"] = entity.OperatorEnteredBy;
                    dr["Process at NAV"] = entity.ProcessAtNAV;
                    dr["Generator"] = entity.Generator;
                    dr["Effective Date"] = entity.EffectuveDate;
                    dr["AUD Equivalent Amount"] = entity.AUDEquivalentAmount;
                    ds.Tables["StateStreetExport"].Rows.Add(dr);
                    if (string.IsNullOrEmpty(ds.ExtendedProperties["SequanceNumber"].ToString()))
                        ds.ExtendedProperties["SequanceNumber"] = entity.FileNameSequanceNumber.ToString();
                }

            }

            ExportBankWestInstructions(orderEntity, ds);
            ExportMacquarieInstructions(orderEntity, ds);
        }
        #endregion
        #region Export Bulk Instructions
        private static void ExportASXBulkInstructions(BulkOrderEntity bulkOrderEntity, ExportDS ds)
        {
            var asxEntity = bulkOrderEntity.Instructions.Where(ss => ss.Type == InstructionType.ASXBulk).ToList();
            foreach (OrderPadASXBulkInstructionEntity entity in asxEntity)
            {
                DataRow dr = ds.Tables["ASXBulkExport"].NewRow();
                dr["Bulk Order ID"] = entity.BulkOrderNo;
                dr["Investment Code"] = entity.InvestmentCode;
                dr["Investment Name"] = entity.InvestmentName;
                dr["Market Code"] = entity.MarketCode;
                dr["Currency Code"] = entity.CurrencyCode;
                dr["Order Type"] = entity.OrderType;
                dr["Rebalance Unit Price"] = string.Format("{0:N6}", entity.RebalanceUnitPrice);

                switch (entity.OrderPreferedBy)
                {
                    case OrderPreferedBy.Amount:
                        dr["Suggested Order Units"] = string.Empty;
                        break;

                    case OrderPreferedBy.Units:
                        dr["Suggested Order Units"] = string.Format("{0:N4}", entity.SuggestedOrderUnits);
                        break;
                }

                dr["Suggested Order Amount"] = string.Format("{0:N2}", entity.SuggestedOrderAmount);
                dr["Requested By"] = entity.OrderPreferedBy.ToString();
                ds.Tables["ASXBulkExport"].Rows.Add(dr);
            }
        }

        private static void ExportTDBulkInstructions(BulkOrderEntity bulkOrderEntity, ExportDS ds, ICMBroker broker)
        {
            var tdEntity = bulkOrderEntity.Instructions.Where(ss => ss.Type == InstructionType.TDBulk).ToList();
            foreach (OrderPadTDBulkInstructionEntity entity in tdEntity)
            {
                var instName = GetInstituteName(broker, entity.BrokerID, true);
                if (instName.ToLower() == "fiig")
                {
                    DataRow dr = ds.Tables["TDFiiGBulkExport"].NewRow();
                    if (entity.Amount.HasValue)
                        dr["Amount"] = "$  " + entity.Amount.Value.ToString("N2");
                    dr["Rate"] = entity.Rate.ToString() + "%";
                    dr["Term"] = entity.Term;
                    dr["Institution"] = GetInstituteName(broker, entity.BankID, true);
                    ds.Tables["TDFiiGBulkExport"].Rows.Add(dr);

                }
                else if (instName.ToLower() == "amm")
                {
                    DataRow dr = ds.Tables["TDAMMBulkExport"].NewRow();
                    if (entity.Amount.HasValue)
                        dr["Amount"] = "$  " + entity.Amount.Value.ToString("N2");
                    dr["Rate"] = entity.Rate.ToString() + "%";
                    dr["Term"] = entity.Term;
                    dr["Institution"] = GetInstituteName(broker, entity.BankID, true);
                    ds.Tables["TDAMMBulkExport"].Rows.Add(dr);
                }
            }
        }

        #endregion
        #region Export ASX Instructions
        private static void ExportASXInstructions(OrderEntity orderEntity, ExportDS ds)
        {
            var asxEntity = orderEntity.Instructions.Where(ss => ss.Type == InstructionType.ASX).ToList();
            foreach (OrderPadASXInstructionEntity entity in asxEntity)
            {
                DataRow dr = ds.Tables["ASXExport"].NewRow();
                dr["PARENT A/C"] = entity.ParentAcc;
                dr["BOOKED A/C"] = orderEntity.ClientID;
                dr["B/S"] = orderEntity.OrderItemType.ToString();
                dr["QTY"] = entity.Units;
                dr["STOCK"] = entity.InvestmentCode;
                dr["PRICE/AVG"] = entity.UnitPrice;
                dr["Trade As At Date"] = entity.TradeDate.ToString("dd/MM/yyy");
                dr["Client Account Name"] = entity.ASXAccountName;
                dr["Client Account"] = entity.ASXAccountNo;
                ds.Tables["ASXExport"].Rows.Add(dr);
            }
        }
        #endregion
        #region Export Instructions
        private static void ExportMacquarieInstructions(OrderEntity orderEntity, ExportDS ds)
        {
            var macquarieEntity = orderEntity.Instructions.Where(ss => ss.Type == InstructionType.Macquarie).ToList();
            foreach (OrderPadMacquarieInstructionEntity entity in macquarieEntity)
            {
                switch (entity.MACInstructionType)
                {
                    case MacquarieInstructionType.StateStreet:
                        {
                            AddExportRow(ds, entity, "MacquarieExportForStateStreet");
                        }
                        break;
                    case MacquarieInstructionType.P2:
                        {
                            AddExportRow(ds, entity, "MacquarieExportForP2");
                        }
                        break;
                    case MacquarieInstructionType.Macquarie:
                        {
                            AddExportRow(ds, entity, "MacquarieExport");
                        }
                        break;
                }
            }
        }

        private static void AddExportRow(ExportDS ds, OrderPadMacquarieInstructionEntity entity, string tableName)
        {
            DataRow dr = ds.Tables[tableName].NewRow();
            dr["ClientAccountName"] = entity.ClientAccountName;
            dr["ClientAccountNumber"] = entity.ClientAccountNumber;
            if (entity.DebitAmount.HasValue)
                dr["DebitAmount"] = entity.DebitAmount.Value.ToString("N2");
            dr["Narration"] = entity.Narration;
            dr["ToAccountName"] = entity.ToAccountName;
            dr["ToAccountBSB"] = entity.ToAccountBSB;
            dr["ToAccountNumber"] = entity.ToAccountNumber;
            dr["ReceiverDescription"] = entity.ReceiverDescription;
            ds.Tables[tableName].Rows.Add(dr);
        }

        private static void ExportBankWestInstructions(OrderEntity orderEntity, ExportDS ds)
        {
            var bankWestEntity = orderEntity.Instructions.Where(ss => ss.Type == InstructionType.BankWest).ToList();
            foreach (OrderPadBankWestInstructionEntity entity in bankWestEntity)
            {
                if (entity.BWInstructionType == BankWestInstructionType.StateStreet)
                {
                    DataRow dr = ds.Tables["BankWestExportForStateStreet"].NewRow();
                    dr["Customer Name"] = entity.CustomerName;
                    dr["BSB"] = orderEntity.AccountBSB;
                    dr["Account"] = entity.AccountNumber;
                    if (entity.Amount.HasValue)
                        dr["$ Amount"] = "$  " + entity.Amount.Value.ToString("N2");
                    dr["Reference / Narration"] = entity.Reference_Narration;
                    ds.Tables["BankWestExportForStateStreet"].Rows.Add(dr);
                }
                else if (entity.BWInstructionType == BankWestInstructionType.P2)
                {
                    DataRow dr = ds.Tables["BankWestExportForP2"].NewRow();
                    dr["Customer Name"] = entity.CustomerName;
                    dr["BSB"] = orderEntity.AccountBSB;
                    dr["Account"] = entity.AccountNumber;
                    if (entity.Amount.HasValue)
                        dr["$ Amount"] = "$  " + entity.Amount.Value.ToString("N2");
                    dr["Reference / Narration"] = entity.Reference_Narration;
                    ds.Tables["BankWestExportForP2"].Rows.Add(dr);
                }
                else if (entity.BWInstructionType == BankWestInstructionType.BankWest)
                {
                    DataRow dr = ds.Tables["BankWestExport"].NewRow();
                    dr["Customer Name"] = entity.CustomerName;
                    dr["BSB"] = orderEntity.AccountBSB;
                    dr["Account"] = entity.AccountNumber;
                    if (entity.Amount.HasValue)
                        dr["$ Amount"] = "$  " + entity.Amount.Value.ToString("N2");
                    dr["Reference / Narration"] = entity.Reference_Narration;
                    ds.Tables["BankWestExport"].Rows.Add(dr);
                }
            }
        }
        private static void ExportTDInstructions(OrderEntity orderEntity, ExportDS ds, ICMBroker broker)
        {
            var tdEntity = orderEntity.Instructions.Where(ss => ss.Type == InstructionType.TermDeposit).ToList();
            foreach (OrderPadTDInstructionEntity entity in tdEntity)
            {
                var instName = GetInstituteName(broker, entity.BrokerID, true);
                if (instName.ToLower() == "fiig")
                {
                    DataRow dr = ds.Tables["TDFiiGExport"].NewRow();
                    dr["Client"] = entity.CustomerName;
                    dr["Client Code"] = entity.ClientID;
                    dr["BSB"] = orderEntity.AccountBSB;
                    dr["Account No"] = entity.AccountNumber;
                    if (entity.Amount.HasValue)
                        dr["Amount"] = "$  " + entity.Amount.Value.ToString("N2");
                    dr["Rate"] = entity.Percentage.ToString() + "%";
                    dr["Term"] = entity.Duration;
                    dr["Institution"] = GetInstituteName(broker, entity.BankID, false);
                    ds.Tables["TDFiiGExport"].Rows.Add(dr);

                }
                else if (instName.ToLower() == "amm")
                {
                    DataRow dr = ds.Tables["TDAMMExport"].NewRow();
                    dr["Client"] = entity.CustomerName;
                    dr["Client Code"] = entity.ClientID;
                    dr["BSB"] = orderEntity.AccountBSB;
                    dr["Account No"] = entity.AccountNumber;
                    if (entity.Amount.HasValue)
                        dr["Amount"] = "$  " + entity.Amount.Value.ToString("N2");
                    dr["Rate"] = entity.Percentage.ToString() + "%";
                    dr["Term"] = entity.Duration;
                    dr["Institution"] = GetInstituteName(broker, entity.BankID, false);
                    ds.Tables["TDAMMExport"].Rows.Add(dr);
                }
                else
                {
                    DataRow dr = ds.Tables["TDOtherExport"].NewRow();
                    dr["Client"] = entity.CustomerName;
                    dr["Client Code"] = entity.ClientID;
                    dr["BSB"] = orderEntity.AccountBSB;
                    dr["Account No"] = entity.AccountNumber;
                    if (entity.Amount.HasValue)
                        dr["Amount"] = "$  " + entity.Amount.Value.ToString("N2");
                    dr["Rate"] = entity.Percentage.ToString() + "%";
                    dr["Term"] = entity.Duration;
                    dr["Institution"] = GetInstituteName(broker, entity.BankID, false);
                    ds.Tables["TDOtherExport"].Rows.Add(dr);
                }

                //Creating TD Transaction
                CreateTransaction(entity.OrderID, entity.OrderItemID, "TD", "Application", entity.Amount, entity.Percentage, instName, entity.ProductID, entity.BankID, entity.TDAccountCID, entity.ClientID, ds.Unit.CurrentUser, broker, entity.Duration);
            }
        }
        private static void ExportAtCallInstructions(OrderEntity orderEntity, ExportDS ds, ICMBroker broker)
        {
            var atCallEntity = orderEntity.Instructions.Where(ss => ss.Type == InstructionType.AtCall).ToList();
            foreach (OrderPadAtCallInstructionEntity entity in atCallEntity)
            {
                if (entity.AtCallType == AtCallType.BestRates || entity.AtCallType == AtCallType.MoneyMovementBroker)
                {
                    var instName = GetInstituteName(broker, entity.BrokerID, true);
                    DataTable table;

                    if (instName.ToLower() == "fiig")
                    {
                        table = ds.Tables["AtCallFiiGExport"];
                        ExtractAtCallInstructionsData(broker, entity, table);
                    }
                    else if (instName.ToLower() == "amm")
                    {
                        table = ds.Tables["AtCallAMMExport"];
                        ExtractAtCallInstructionsData(broker, entity, table);
                    }

                    //Creating At Call Transaction
                    string sysTransType = string.Empty;
                    if (entity.AtCallType == AtCallType.MoneyMovementBroker)
                    {
                        switch (entity.OrderItemType)
                        {
                            case OrderItemType.Buy:
                                sysTransType = "Transfer In";
                                break;
                            case OrderItemType.Sell:
                                sysTransType = "Redemption";
                                break;
                        }
                    }
                    else
                    {
                        switch (entity.OrderItemType)
                        {
                            case OrderItemType.Buy:
                                sysTransType = "Application";
                                break;
                            case OrderItemType.Sell:
                                sysTransType = "Redemption";
                                break;
                        }
                    }
                    CreateTransaction(entity.OrderID, entity.OrderItemID, "At Call", sysTransType, entity.Amount, entity.Rate, instName, entity.ProductID, entity.BankID, entity.AtCallAccountCID, entity.ClientID, ds.Unit.CurrentUser, broker);
                }
                else if (entity.AtCallType == AtCallType.MoneyMovement)
                {
                    string BankFrom = GetBankInstitute(broker, entity.AccountCID);
                    DataRow dr = null;

                    switch (BankFrom.ToLower())
                    {
                        case "macquarie bank":
                            dr = ds.Tables["MacquarieAtCallExport"].NewRow();

                            dr["ClientAccountName"] = entity.CustomerName;
                            dr["ClientAccountNumber"] = entity.AccountNumber;
                            dr["DebitAmount"] = entity.Amount.Value.ToString("N2");
                            dr["Narration"] = entity.FromNarration;

                            dr["ToAccountName"] = GetAccountName(broker, entity.TransferAccCID);
                            dr["ToAccountNumber"] = entity.TransferAccNumber;
                            dr["ToAccountBSB"] = entity.TransferAccBSB;
                            dr["ReceiverDescription"] = entity.TransferNarration;

                            dr["OrderBankAccountType"] = entity.OrderBankAccountType;

                            ds.Tables["MacquarieAtCallExport"].Rows.Add(dr);
                            break;
                        default:
                            dr = ds.Tables["AtCallExport"].NewRow();
                            dr["Client"] = entity.CustomerName;
                            if (entity.Amount.HasValue)
                                dr["Amount"] = "$  " + entity.Amount.Value.ToString("N2");
                            dr["Bank From"] = BankFrom;
                            dr["Bank To"] = GetBankNameForBuyAtCall(broker, entity.TransferAccCID);
                            dr["Account Name From"] = GetAccountName(broker, entity.AccountCID);
                            dr["Account Name To"] = GetAccountName(broker, entity.TransferAccCID);
                            dr["BSB From"] = entity.BSB;
                            dr["BSB To"] = entity.TransferAccBSB;
                            dr["Account No From"] = entity.AccountNumber;
                            dr["Account No To"] = entity.TransferAccNumber;
                            dr["Narration From"] = entity.FromNarration;
                            dr["Narration To"] = entity.TransferNarration;

                            dr["OrderBankAccountType"] = entity.OrderBankAccountType;

                            ds.Tables["AtCallExport"].Rows.Add(dr);
                            break;
                    }
                }
            }
        }

        private static void ExtractAtCallInstructionsData(ICMBroker broker, OrderPadAtCallInstructionEntity entity, DataTable table)
        {
            DataRow dr = table.NewRow();
            dr["Client"] = entity.CustomerName;
            dr["Client Code"] = entity.ClientID;
            dr["BSB"] = entity.BSB;
            dr["Account No"] = entity.AccountNumber;
            if (entity.Amount.HasValue)
                dr["Amount"] = "$  " + entity.Amount.Value.ToString("N2");
            dr["Investment Account Name"] = entity.AccountTier;
            if (entity.Rate.HasValue && entity.Rate > 0)
                dr["Rate"] = entity.Rate.ToString() + "%";
            else
                dr["Rate"] = string.IsNullOrEmpty(entity.TransferNarration) ? string.Empty : entity.TransferNarration;
            dr["Institution"] = GetInstituteName(broker, entity.BankID, false);
            dr["OrderItemType"] = entity.OrderItemType;
            dr["CustomerNo"] = GetCustomerNumber(broker, entity.AccountCID);
            dr["AccountName"] = GetAccountName(broker, entity.AccountCID);
            table.Rows.Add(dr);
        }
        #endregion
        #endregion
        #region Create export tables
        #region Create export table to State street
        private static DataTable CreateStateStreetTable()
        {
            var dt = new DataTable { TableName = "StateStreetExport" };
            dt.Columns.Add("Unitholder Code", typeof(string));
            dt.Columns.Add("Unitholder Sub Code", typeof(string));
            dt.Columns.Add("Fund Code", typeof(string));
            dt.Columns.Add("MCH Code", typeof(string));
            dt.Columns.Add("Trade Date", typeof(string));
            dt.Columns.Add("Clear Date", typeof(string));
            dt.Columns.Add("Amount", typeof(string));
            dt.Columns.Add("Redemption or Exchange Options", typeof(string));
            dt.Columns.Add("Transaction Type", typeof(string));
            dt.Columns.Add("Number of Shares", typeof(string));
            dt.Columns.Add("Exchange to Fund", typeof(string));
            dt.Columns.Add("Dealer Commission Rate", typeof(string));
            dt.Columns.Add("Currency of Trade", typeof(string));
            dt.Columns.Add("External ID", typeof(string));
            dt.Columns.Add("Purchase Source", typeof(string));
            dt.Columns.Add("Redemption Source", typeof(string));
            dt.Columns.Add("Operator Entered By", typeof(string));
            dt.Columns.Add("Process at NAV", typeof(string));
            dt.Columns.Add("Generator", typeof(string));
            dt.Columns.Add("Effective Date", typeof(string));
            dt.Columns.Add("AUD Equivalent Amount", typeof(string));
            return dt;
        }
        private static DataTable CreateStateStreetForP2Table()
        {
            var dt = new DataTable { TableName = "StateStreetForP2Export" };
            dt.Columns.Add("Unitholder Code", typeof(string));
            dt.Columns.Add("Unitholder Sub Code", typeof(string));
            dt.Columns.Add("Fund Code", typeof(string));
            dt.Columns.Add("MCH Code", typeof(string));
            dt.Columns.Add("Trade Date", typeof(string));
            dt.Columns.Add("Clear Date", typeof(string));
            dt.Columns.Add("Amount", typeof(string));
            dt.Columns.Add("Redemption or Exchange Options", typeof(string));
            dt.Columns.Add("Transaction Type", typeof(string));
            dt.Columns.Add("Number of Shares", typeof(string));
            dt.Columns.Add("Exchange to Fund", typeof(string));
            dt.Columns.Add("Dealer Commission Rate", typeof(string));
            dt.Columns.Add("Currency of Trade", typeof(string));
            dt.Columns.Add("External ID", typeof(string));
            dt.Columns.Add("Purchase Source", typeof(string));
            dt.Columns.Add("Redemption Source", typeof(string));
            dt.Columns.Add("Operator Entered By", typeof(string));
            dt.Columns.Add("Process at NAV", typeof(string));
            dt.Columns.Add("Generator", typeof(string));
            dt.Columns.Add("Effective Date", typeof(string));
            dt.Columns.Add("AUD Equivalent Amount", typeof(string));
            return dt;

        }
        #endregion
        #region Create export table for Bulk Order
        private static DataTable CreateASXBulkExportTable()
        {
            var dt = new DataTable { TableName = "ASXBulkExport" };
            dt.Columns.Add("Bulk Order ID", typeof(string));
            dt.Columns.Add("Investment Code", typeof(string));
            dt.Columns.Add("Investment Name", typeof(string));
            dt.Columns.Add("Market Code", typeof(string));
            dt.Columns.Add("Currency Code", typeof(string));
            dt.Columns.Add("Order Type", typeof(string));
            dt.Columns.Add("Rebalance Unit Price", typeof(string));
            dt.Columns.Add("Suggested Order Units", typeof(string));
            dt.Columns.Add("Suggested Order Amount", typeof(string));
            dt.Columns.Add("Requested By", typeof(string));
            return dt;
        }
        private static DataTable CreateTDFiiGBulkExportTable()
        {
            var dt = new DataTable { TableName = "TDFiiGBulkExport" };
            dt.Columns.Add("Amount", typeof(string));
            dt.Columns.Add("Term", typeof(string));
            dt.Columns.Add("Rate", typeof(string));
            dt.Columns.Add("Institution", typeof(string));
            return dt;
        }
        private static DataTable CreateTDAMMBulkExportTable()
        {
            var dt = new DataTable { TableName = "TDAMMBulkExport" };
            dt.Columns.Add("Amount", typeof(string));
            dt.Columns.Add("Term", typeof(string));
            dt.Columns.Add("Rate", typeof(string));
            dt.Columns.Add("Institution", typeof(string));
            return dt;
        }
        #endregion
        #region Create export table for ASX
        private static DataTable CreateASXExportTable()
        {
            var dt = new DataTable { TableName = "ASXExport" };
            dt.Columns.Add("PARENT A/C", typeof(string));
            dt.Columns.Add("BOOKED A/C", typeof(string));
            dt.Columns.Add("B/S", typeof(string));
            dt.Columns.Add("QTY", typeof(string));
            dt.Columns.Add("STOCK", typeof(string));
            dt.Columns.Add("PRICE/AVG", typeof(string));
            dt.Columns.Add("Trade As At Date", typeof(string));
            dt.Columns.Add("Client Account Name", typeof(string));
            dt.Columns.Add("Client Account", typeof(string));
            return dt;
        }
        #endregion
        #region Create export tables for Bank West
        private static DataTable CreateBankWestExportTableForStateStreet()
        {
            var dt = new DataTable { TableName = "BankWestExportForStateStreet" };
            dt.Columns.Add("Customer Name", typeof(string));
            dt.Columns.Add("BSB", typeof(string));
            dt.Columns.Add("Account", typeof(string));
            dt.Columns.Add("$ Amount", typeof(string));
            dt.Columns.Add("Reference / Narration", typeof(string));
            return dt;
        }
        private static DataTable CreateBankWestExportTableForP2()
        {
            var dt = new DataTable { TableName = "BankWestExportForP2" };
            dt.Columns.Add("Customer Name", typeof(string));
            dt.Columns.Add("BSB", typeof(string));
            dt.Columns.Add("Account", typeof(string));
            dt.Columns.Add("$ Amount", typeof(string));
            dt.Columns.Add("Reference / Narration", typeof(string));
            return dt;
        }
        private static DataTable CreateBankWestExportTable()
        {
            var dt = new DataTable { TableName = "BankWestExport" };
            dt.Columns.Add("Customer Name", typeof(string));
            dt.Columns.Add("BSB", typeof(string));
            dt.Columns.Add("Account", typeof(string));
            dt.Columns.Add("$ Amount", typeof(string));
            dt.Columns.Add("Reference / Narration", typeof(string));
            return dt;
        }
        #endregion
        #region Create export tables for Macquarie
        private static DataTable CreateMacquarieExportTableForStateStreet()
        {
            return GetMacquarieExportTable("MacquarieExportForStateStreet");
        }
        private static DataTable CreateMacquarieExportTableForP2()
        {
            return GetMacquarieExportTable("MacquarieExportForP2");
        }
        private static DataTable CreateMacquarieExportTable()
        {
            return GetMacquarieExportTable("MacquarieExport");
        }

        private static DataTable CreateMacquarieAtCallExportTable()
        {
            return GetMacquarieExportTable("MacquarieAtCallExport");
        }


        private static DataTable GetMacquarieExportTable(string tableName)
        {
            var dt = new DataTable { TableName = tableName };
            dt.Columns.Add("ClientAccountName", typeof(string));
            dt.Columns.Add("ClientAccountNumber", typeof(string));
            dt.Columns.Add("DebitAmount", typeof(string));
            dt.Columns.Add("Narration", typeof(string));
            dt.Columns.Add("ToAccountName", typeof(string));
            dt.Columns.Add("ToAccountBSB", typeof(string));
            dt.Columns.Add("ToAccountNumber", typeof(string));
            dt.Columns.Add("ReceiverDescription", typeof(string));
            dt.Columns.Add("OrderBankAccountType", typeof(string));
            return dt;
        }
        #endregion
        #region Create export tables for TD
        private static DataTable CreateTDFiiGExportTable()
        {
            var dt = new DataTable { TableName = "TDFiiGExport" };
            dt.Columns.Add("Client", typeof(string));
            dt.Columns.Add("Client Code", typeof(string));
            dt.Columns.Add("BSB", typeof(string));
            dt.Columns.Add("Account No", typeof(string));
            dt.Columns.Add("Amount", typeof(string));
            dt.Columns.Add("Term", typeof(string));
            dt.Columns.Add("Rate", typeof(string));
            dt.Columns.Add("Institution", typeof(string));
            return dt;
        }
        private static DataTable CreateTDAMMExportTable()
        {
            var dt = new DataTable { TableName = "TDAMMExport" };
            dt.Columns.Add("Client", typeof(string));
            dt.Columns.Add("Client Code", typeof(string));
            dt.Columns.Add("BSB", typeof(string));
            dt.Columns.Add("Account No", typeof(string));
            dt.Columns.Add("Amount", typeof(string));
            dt.Columns.Add("Term", typeof(string));
            dt.Columns.Add("Rate", typeof(string));
            dt.Columns.Add("Institution", typeof(string));
            return dt;
        }
        private static DataTable CreateTDOtherExportTable()
        {
            var dt = new DataTable { TableName = "TDOtherExport" };
            dt.Columns.Add("Client", typeof(string));
            dt.Columns.Add("Client Code", typeof(string));
            dt.Columns.Add("BSB", typeof(string));
            dt.Columns.Add("Account No", typeof(string));
            dt.Columns.Add("Amount", typeof(string));
            dt.Columns.Add("Term", typeof(string));
            dt.Columns.Add("Rate", typeof(string));
            dt.Columns.Add("Institution", typeof(string));
            return dt;
        }
        #endregion
        #region export table for AtCall
        private static DataTable CreateAtCallOtherExportTable()
        {
            var dt = new DataTable { TableName = "AtCallExport" };
            dt.Columns.Add("Client", typeof(string));
            dt.Columns.Add("Amount", typeof(string));
            dt.Columns.Add("Bank From", typeof(string));
            dt.Columns.Add("Bank To", typeof(string));
            dt.Columns.Add("Account Name From", typeof(string));
            dt.Columns.Add("Account Name To", typeof(string));
            dt.Columns.Add("BSB From", typeof(string));
            dt.Columns.Add("BSB To", typeof(string));
            dt.Columns.Add("Account No From", typeof(string));
            dt.Columns.Add("Account No To", typeof(string));
            dt.Columns.Add("Narration From", typeof(string));
            dt.Columns.Add("Narration To", typeof(string));
            dt.Columns.Add("OrderBankAccountType", typeof(string));
            return dt;
        }
        private static DataTable CreateAtCallFiiGExportTable()
        {
            var dt = new DataTable { TableName = "AtCallFiiGExport" };
            dt.Columns.Add("Client", typeof(string));
            dt.Columns.Add("Client Code", typeof(string));
            dt.Columns.Add("BSB", typeof(string));
            dt.Columns.Add("Account No", typeof(string));
            dt.Columns.Add("Amount", typeof(string));
            dt.Columns.Add("Investment Account Name", typeof(string));
            dt.Columns.Add("Rate", typeof(string));
            dt.Columns.Add("Institution", typeof(string));
            dt.Columns.Add("OrderItemType", typeof(string));
            dt.Columns.Add("CustomerNo", typeof(string));
            dt.Columns.Add("AccountName", typeof(string));
            return dt;
        }
        private static DataTable CreateAtCallAMMExportTable()
        {
            var dt = new DataTable { TableName = "AtCallAMMExport" };
            dt.Columns.Add("Client", typeof(string));
            dt.Columns.Add("Client Code", typeof(string));
            dt.Columns.Add("BSB", typeof(string));
            dt.Columns.Add("Account No", typeof(string));
            dt.Columns.Add("Amount", typeof(string));
            dt.Columns.Add("Investment Account Name", typeof(string));
            dt.Columns.Add("Rate", typeof(string));
            dt.Columns.Add("Institution", typeof(string));
            dt.Columns.Add("OrderItemType", typeof(string));
            dt.Columns.Add("CustomerNo", typeof(string));
            dt.Columns.Add("AccountName", typeof(string));
            return dt;
        }
        #endregion
        private static void AddTablesIntoDataSetForExport(ExportDS ds)
        {
            DataTable stateStreetdt = CreateStateStreetTable();
            DataTable stateStreetForP2 = CreateStateStreetForP2Table();
            DataTable asXdt = CreateASXExportTable();
            DataTable bankWestStateStreetdt = CreateBankWestExportTableForStateStreet();
            DataTable bankWestP2Dt = CreateBankWestExportTableForP2();
            DataTable bankWestdt = CreateBankWestExportTable();
            DataTable tdFiiGExport = CreateTDFiiGExportTable();
            DataTable tdAMMExport = CreateTDAMMExportTable();
            DataTable tdOtherExport = CreateTDOtherExportTable();
            DataTable atCallExport = CreateAtCallOtherExportTable();
            DataTable atCallFiiGExport = CreateAtCallFiiGExportTable();
            DataTable atCallAMMExport = CreateAtCallAMMExportTable();
            DataTable asxBulkDt = CreateASXBulkExportTable();
            DataTable macquaireStateStreetDt = CreateMacquarieExportTableForStateStreet();
            DataTable macquaireP2Dt = CreateMacquarieExportTableForP2();
            DataTable macquaireDt = CreateMacquarieExportTable();
            DataTable macquaireAtCallDt = CreateMacquarieAtCallExportTable();
            DataTable tdFiiGBulkDt = CreateTDFiiGBulkExportTable();
            DataTable tdAMMBulkDt = CreateTDAMMBulkExportTable();


            ds.Tables.Add(stateStreetdt);
            ds.Tables.Add(asXdt);
            ds.Tables.Add(bankWestStateStreetdt);
            ds.Tables.Add(bankWestP2Dt);
            ds.Tables.Add(bankWestdt);
            ds.Tables.Add(tdFiiGExport);
            ds.Tables.Add(tdAMMExport);
            ds.Tables.Add(tdOtherExport);
            ds.Tables.Add(atCallExport);
            ds.Tables.Add(atCallFiiGExport);
            ds.Tables.Add(atCallAMMExport);
            ds.Tables.Add(stateStreetForP2);
            ds.Tables.Add(asxBulkDt);
            ds.Tables.Add(macquaireStateStreetDt);
            ds.Tables.Add(macquaireP2Dt);
            ds.Tables.Add(macquaireDt);
            ds.Tables.Add(macquaireAtCallDt);
            ds.Tables.Add(tdFiiGBulkDt);
            ds.Tables.Add(tdAMMBulkDt);
        }

        #endregion
        #region Create Instructions
        private static void CreateInstructionsForTDBulk(BulkOrderEntity bulkOrder)
        {
            bulkOrder.Instructions.RemoveAll(ss => ss.Type == InstructionType.TDBulk);
            foreach (var tdbulkOrderItem in bulkOrder.TDBulkOrderItems)
            {
                if (!bulkOrder.TradeDate.HasValue)
                {
                    bulkOrder.TradeDate = DateTime.Now;
                }
                var instructionEntity = new OrderPadTDBulkInstructionEntity
                {
                    ID = Guid.NewGuid(),
                    CreatedDate = DateTime.Now,
                    TradeDate = (DateTime)bulkOrder.TradeDate,
                    Status = InstructionStatus.Created,
                    Type = InstructionType.TDBulk,
                    UpdatedDate = DateTime.Now,
                    BulkOrderID = bulkOrder.ID,
                    BulkOrderNo = bulkOrder.BulkOrderID,
                    Amount = tdbulkOrderItem.Amount,
                    Term = tdbulkOrderItem.Term,
                    Rate = (tdbulkOrderItem.Rate.HasValue) ? tdbulkOrderItem.Rate * 100 : 0,
                    BrokerID = tdbulkOrderItem.BrokerId,
                    BankID = tdbulkOrderItem.InstituteID,
                };
                bulkOrder.Instructions.Add(instructionEntity);
            }
        }
        private static void CreateInstructionsForASXBulk(ICMBroker broker, BulkOrderEntity bulkOrder)
        {
            bulkOrder.Instructions.RemoveAll(ss => ss.Type == InstructionType.ASXBulk);
            foreach (var bulkOrderItem in bulkOrder.BulkOrderItems)
            {
                if (!bulkOrder.TradeDate.HasValue)
                {
                    bulkOrder.TradeDate = DateTime.Now;
                }
                var instructionEntity = new OrderPadASXBulkInstructionEntity
                {
                    ID = Guid.NewGuid(),
                    CreatedDate = DateTime.Now,
                    TradeDate = (DateTime)bulkOrder.TradeDate,
                    Status = InstructionStatus.Created,
                    Type = InstructionType.ASXBulk,
                    UpdatedDate = DateTime.Now,
                    InvestmentCode = bulkOrderItem.Code,
                    BulkOrderID = bulkOrder.ID,
                    BulkOrderNo = bulkOrder.BulkOrderID,
                    OrderType = bulkOrder.OrderItemType.ToString(),
                    RebalanceUnitPrice = bulkOrderItem.ExpectedUnitPrice,
                    SuggestedOrderAmount = bulkOrderItem.ExpectedAmount,
                    SuggestedOrderUnits = bulkOrderItem.ExpectedUnits,
                    OrderPreferedBy = bulkOrder.OrderPreferedBy,
                };
                GetSecurityDetail(instructionEntity, broker);
                bulkOrder.Instructions.Add(instructionEntity);
            }
        }
        private static void CreateInstructionsForASX(ICMBroker broker, OrderEntity order)
        {
            order.Instructions.RemoveAll(ss => ss.Type == InstructionType.ASX);
            foreach (ASXOrderItem asxOrderItem in order.Items)
            {
                var instructionEntity = new OrderPadASXInstructionEntity
                                            {
                                                ID = Guid.NewGuid(),
                                                CreatedDate = DateTime.Now,
                                                TradeDate = order.TradeDate,
                                                Status = InstructionStatus.Issued,
                                                Type = InstructionType.ASX,
                                                UpdatedDate = DateTime.Now,
                                                AccountCID = order.AccountCID,
                                                Amount = asxOrderItem.Amount,
                                                ASXAccountNo = asxOrderItem.ASXAccountNo,
                                                InvestmentCode = asxOrderItem.InvestmentCode,
                                                ProductID = asxOrderItem.ProductID,
                                                Units = asxOrderItem.Units,
                                                OrderID = order.ID,
                                                ASXAccountName = string.IsNullOrEmpty(asxOrderItem.ASXAccountName) ? GetClientName(broker, order.ClientCID) : asxOrderItem.ASXAccountName,
                                                ParentAcc = asxOrderItem.ParentAcc,
                                                UnitPrice = (asxOrderItem.UnitPrice.HasValue) ? asxOrderItem.UnitPrice : asxOrderItem.Amount / asxOrderItem.Units

                                            };
                order.Instructions.Add(instructionEntity);
            }

        }
        private static void CreateInstructionsForStateStreet(OrderEntity order, ICMBroker broker, long filenameSequance)
        {
            var filebatchNo = ((OrderPadStateStreetInstructionEntity)order.Instructions.FirstOrDefault(ss => ss.Type == InstructionType.StateStreet));
            int fileBatchNo = (filebatchNo != null) ? filebatchNo.FileBatchNo + 1 : 1;
            order.Instructions.RemoveAll(ss => ss.Type == InstructionType.StateStreet);
            foreach (StateStreetOrderItem stateStreetorderItem in order.Items)
            {
                var instructionEntity = new OrderPadStateStreetInstructionEntity
                                            {
                                                ID = Guid.NewGuid(),
                                                CreatedDate = DateTime.Now,
                                                Status = InstructionStatus.Issued,
                                                Type = InstructionType.StateStreet,
                                                UpdatedDate = DateTime.Now,
                                                UnitholderCode = order.ClientID,
                                                UnitHolderSubCode = "IV",
                                                FundCode = stateStreetorderItem.FundCode,
                                                MCHCode = "XXXX",
                                                TradeDate = order.TradeDate,
                                                ClearDate = order.TradeDate
                                            };
                if (order.OrderItemType == OrderItemType.Buy)
                {
                    instructionEntity.Amount = stateStreetorderItem.Amount;
                    instructionEntity.Transactiontype = "100";
                    instructionEntity.PurchaseSource = "AZ";

                    InstructionType type = GetBankInstructionType(order.AccountCID, broker);
                    switch (type)
                    {
                        case InstructionType.BankWest:
                            CreateStateStreetInstructionsForBankWest(order, broker);
                            break;
                        case InstructionType.Macquarie:
                            CreateStateStreetInstructionsForMacquarie(order, broker);
                            break;
                    }
                }
                else if (order.OrderItemType == OrderItemType.Sell)
                {
                    if (order.OrderPreferedBy == OrderPreferedBy.Units && stateStreetorderItem.Units.HasValue)
                    {
                        instructionEntity.NumberOfShares = stateStreetorderItem.Units;
                    }
                    else
                    {
                        instructionEntity.Amount = stateStreetorderItem.Amount;
                    }
                    instructionEntity.Transactiontype = "690";
                    instructionEntity.RedumptionSource = "P2";
                }
                instructionEntity.RedumptionOrExchangeOption = "";
                instructionEntity.ExchangeToFund = "";
                instructionEntity.DealerCommissionRate = "";
                instructionEntity.CurrencyOfTrade = "AUD";
                instructionEntity.ExternalID = "";
                instructionEntity.OperatorEnteredBy = "";
                instructionEntity.ProcessAtNAV = "FALSE";
                instructionEntity.Generator = "";
                instructionEntity.EffectuveDate = null;
                instructionEntity.AUDEquivalentAmount = "";
                instructionEntity.FileBatchNo = fileBatchNo;
                instructionEntity.FileNameSequanceNumber = filenameSequance;
                order.Instructions.Add(instructionEntity);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bankAccountCid"></param>
        /// <param name="broker"></param>
        /// <returns></returns>
        private static InstructionType GetBankInstructionType(Guid bankAccountCid, ICMBroker broker)
        {
            string type = string.Empty;
            var accType = InstructionType.BankWest;
            var comp = broker.GetBMCInstance(bankAccountCid);
            if (comp != null)
            {
                type = comp.GetDataStream((int)CmCommand.GetType, null);
                broker.ReleaseBrokerManagedComponent(comp);
            }
            if (type.Length > 0 && type.ToLower().Contains("macquarie"))
            {
                accType = InstructionType.Macquarie;
            }
            return accType;
        }

        private static void CreateStateStreetInstructionsForMacquarie(OrderEntity order, ICMBroker broker)
        {
            string clientName = GetClientName(broker, order.ClientCID);
            order.Instructions.RemoveAll(ss => ss.Type == InstructionType.Macquarie);
            foreach (StateStreetOrderItem stateStreetorderItem in order.Items)
            {
                var instructionEntity = new OrderPadMacquarieInstructionEntity
                {
                    ID = Guid.NewGuid(),
                    CreatedDate = DateTime.Now,
                    TradeDate = order.TradeDate,
                    Status = InstructionStatus.Issued,
                    OrderID = order.ID,
                    Type = InstructionType.Macquarie,
                    UpdatedDate = DateTime.Now,
                    ClientAccountName = clientName,
                    ClientAccountNumber = order.AccountNumber,
                    DebitAmount = stateStreetorderItem.Amount,
                    ToAccountBSB = "913001",
                    ToAccountNumber = "812096",
                    ReceiverDescription = "e-Clipse UMA Trsf",
                };

                if (stateStreetorderItem.FundCode.ToLower() == "iv004" || stateStreetorderItem.FundCode.ToLower() == "iv014")
                {
                    instructionEntity.MACInstructionType = MacquarieInstructionType.P2;
                    instructionEntity.ToAccountName = "State Street - EPPA";
                }
                else
                {
                    instructionEntity.MACInstructionType = MacquarieInstructionType.StateStreet;
                    instructionEntity.ToAccountName = "State Street - INAP";
                }

                switch (order.OrderItemType)
                {
                    case OrderItemType.Buy:
                        instructionEntity.Narration = "B-";
                        break;
                    case OrderItemType.Sell:
                        instructionEntity.Narration = "S-";
                        break;
                }

                instructionEntity.Narration += Math.Round(stateStreetorderItem.Amount.Value, 4).ToString() + " " + stateStreetorderItem.FundCode;

                order.Instructions.Add(instructionEntity);
            }
        }

        private static void CreateStateStreetInstructionsForBankWest(OrderEntity order, ICMBroker broker)
        {
            string clientName = GetClientName(broker, order.ClientCID);
            order.Instructions.RemoveAll(ss => ss.Type == InstructionType.BankWest);
            foreach (StateStreetOrderItem stateStreetorderItem in order.Items)
            {
                var instructionEntity = new OrderPadBankWestInstructionEntity
                                            {
                                                ID = Guid.NewGuid(),
                                                CreatedDate = DateTime.Now,
                                                TradeDate = order.TradeDate,
                                                Status = InstructionStatus.Issued
                                            };
                if (stateStreetorderItem.FundCode.ToLower() == "iv004" || stateStreetorderItem.FundCode.ToLower() == "iv014")
                {
                    instructionEntity.BWInstructionType = BankWestInstructionType.P2;
                }
                else
                {
                    instructionEntity.BWInstructionType = BankWestInstructionType.StateStreet;
                }
                instructionEntity.Type = InstructionType.BankWest;
                instructionEntity.UpdatedDate = DateTime.Now;
                instructionEntity.Amount = stateStreetorderItem.Amount;
                instructionEntity.CustomerName = clientName;
                instructionEntity.BSB = "";
                instructionEntity.OrderID = order.ID;
                if (order.OrderItemType == OrderItemType.Buy)
                {
                    instructionEntity.Reference_Narration = "B-" + Math.Round(stateStreetorderItem.Amount.Value, 4).ToString() + " " + stateStreetorderItem.FundCode;
                }
                else if (order.OrderItemType == OrderItemType.Sell)
                {
                    instructionEntity.Reference_Narration = "S-" + Math.Round(stateStreetorderItem.Amount.Value, 4).ToString() + " " + stateStreetorderItem.FundCode;
                }
                instructionEntity.AccountNumber = order.AccountNumber;
                order.Instructions.Add(instructionEntity);
            }
        }
        private static void CreateInstructionsForTD(OrderEntity order, ICMBroker broker)
        {
            string clientName = GetClientName(broker, order.ClientCID);
            order.Instructions.RemoveAll(ss => ss.Type == InstructionType.TermDeposit);
            foreach (TermDepositOrderItem termDepositOrderItem in order.Items)
            {
                var instructionEntity = new OrderPadTDInstructionEntity
                                            {
                                                ID = Guid.NewGuid(),
                                                CreatedDate = DateTime.Now,
                                                TradeDate = order.TradeDate,
                                                Status = InstructionStatus.Issued,
                                                Type = InstructionType.TermDeposit,
                                                UpdatedDate = DateTime.Now,
                                                Amount = termDepositOrderItem.Amount,
                                                Rating = termDepositOrderItem.Rating,
                                                Duration = termDepositOrderItem.Duration,
                                                Percentage = (termDepositOrderItem.Percentage.HasValue) ? termDepositOrderItem.Percentage * 100 : 0,
                                                CustomerName = clientName,
                                                ClientID = order.ClientID,
                                                BrokerID = termDepositOrderItem.BrokerID,
                                                BSB = "",
                                                OrderID = order.ID,
                                                BankID = termDepositOrderItem.BankID,
                                                TDAccountCID = termDepositOrderItem.TDAccountCID,
                                                ProductID = termDepositOrderItem.ProductID,
                                                OrderItemID = termDepositOrderItem.ID,
                                            };
                if (order.OrderItemType == OrderItemType.Buy)
                {
                    instructionEntity.Reference_Narration = "B-" + termDepositOrderItem.Amount.ToString() + " "; //+ termDepositOrderItem;
                }
                else if (order.OrderItemType == OrderItemType.Sell)
                {
                    instructionEntity.Reference_Narration = "S-" + termDepositOrderItem.Amount.ToString() + " "; // +termDepositOrderItem;
                }
                instructionEntity.AccountNumber = order.AccountNumber;
                order.Instructions.Add(instructionEntity);
            }
        }
        private static void CreateInstructionsForAtCall(OrderEntity order, ICMBroker broker)
        {
            string clientName = GetClientName(broker, order.ClientCID);
            order.Instructions.RemoveAll(ss => ss.Type == InstructionType.AtCall);
            foreach (AtCallOrderItem atcallOrderItem in order.Items)
            {
                var instructionEntity = new OrderPadAtCallInstructionEntity
                {
                    ID = Guid.NewGuid(),
                    CreatedDate = DateTime.Now,
                    TradeDate = order.TradeDate,
                    Status = InstructionStatus.Issued,
                    Type = InstructionType.AtCall,
                    UpdatedDate = DateTime.Now,
                    Amount = atcallOrderItem.Amount,
                    TransferAccBSB = atcallOrderItem.TransferAccBSB,
                    TransferAccNumber = atcallOrderItem.TransferAccNumber,
                    TransferTo = atcallOrderItem.TransferTo,
                    TransferAccCID = atcallOrderItem.TransferAccCID,
                    CustomerName = clientName,
                    OrderID = order.ID,
                    AccountNumber = order.AccountNumber,
                    BSB = order.AccountBSB,
                    AccountCID = order.AccountCID,
                    ClientID = order.ClientID,
                    BrokerID = atcallOrderItem.BrokerID,
                    BankID = atcallOrderItem.BankID,
                    Min = atcallOrderItem.Min,
                    Max = atcallOrderItem.Max,
                    Rate = atcallOrderItem.Rate,//atcallOrderItem.GetDecimalRate(),
                    ProductID = atcallOrderItem.ProductID,
                    AtCallAccountCID = atcallOrderItem.AtCallAccountCID,
                    AccountTier = atcallOrderItem.AccountTier,
                    AtCallType = atcallOrderItem.AtCallType,
                    OrderItemID = atcallOrderItem.ID,
                    FromNarration = atcallOrderItem.FromNarration,
                    TransferNarration = atcallOrderItem.TransferNarration,
                    OrderItemType = order.OrderItemType,
                    OrderBankAccountType = order.OrderBankAccountType,
                };
                order.Instructions.Add(instructionEntity);
            }
        }
        #endregion
        #region Update Order after Export
        public static Dictionary<long, string> UpdateOrderStatus(List<OrderEntity> orders, ICMBroker broker, UserEntity user)
        {
            var smaMessages = new Dictionary<long, string>();
            foreach (var order in orders)
            {
                order.Status = OrderStatus.Submitted;
                order.UpdatedDate = DateTime.Now;
                order.UpdatedBy = user.CurrentUserName;
                order.UpdatedByCID = user.CID;
                //string message = SendOrderToSMA(order, broker, user);

                //if (!string.IsNullOrEmpty(message))
                //    smaMessages.Add(order.OrderID, message);
            }
            CreateUnsettledForOrders(broker, user, orders, false);
            return smaMessages;
        }

        public static void UpdateBulkOrderStatus(List<BulkOrderEntity> bulkOrders, List<OrderEntity> orders, UserEntity user)
        {
            foreach (var bulkOrder in bulkOrders)
            {
                bulkOrder.Status = BulkOrderStatus.OnMarket;
                bulkOrder.UpdatedDate = DateTime.Now;
                bulkOrder.UpdatedBy = user.CurrentUserName;
                bulkOrder.UpdatedByCID = user.CID;
                bulkOrder.IsExported = true;

                switch (bulkOrder.OrderAccountType)
                {
                    case OrderAccountType.TermDeposit:
                        {
                            foreach (var tdbulkOrderItem in bulkOrder.TDBulkOrderItems)
                            {
                                foreach (var orderItem in tdbulkOrderItem.OrderItems)
                                {
                                    var order = orders.FirstOrDefault(ss => ss.ID == orderItem.OrderID && ss.OrderBulkStatus == OrderBulkStatus.Batched);
                                    if (order != null)
                                    {
                                        order.OrderBulkStatus = OrderBulkStatus.BulkTrade;
                                        order.UpdatedDate = DateTime.Now;
                                        order.UpdatedBy = user.CurrentUserName;
                                        order.UpdatedByCID = user.CID;
                                        order.SMACallStatus = SMACallStatus.NA;
                                        order.SMARequestID = string.Empty;
                                    }
                                }
                            }
                        }
                        break;
                    case OrderAccountType.ASX:
                        {
                            foreach (var bulkOrderItem in bulkOrder.BulkOrderItems)
                            {
                                foreach (var orderItem in bulkOrderItem.OrderItems)
                                {
                                    var order = orders.FirstOrDefault(ss => ss.ID == orderItem.OrderID && ss.OrderBulkStatus == OrderBulkStatus.Batched);
                                    if (order != null)
                                    {
                                        order.OrderBulkStatus = OrderBulkStatus.BulkTrade;
                                        order.UpdatedDate = DateTime.Now;
                                        order.UpdatedBy = user.CurrentUserName;
                                        order.UpdatedByCID = user.CID;
                                        order.SMACallStatus = SMACallStatus.NA;
                                        order.SMARequestID = string.Empty;
                                    }
                                }
                            }
                        }
                        break;
                }
            }
        }
        #endregion

        private static string GetClientName(ICMBroker broker, Guid clientCID)
        {
            var clientCM = broker.GetBMCInstance(clientCID);
            return clientCM.Name;
        }

        public static string GetInstituteName(ICMBroker broker, Guid instId, bool isShortName)
        {
            string instituteName = string.Empty;
            var org = broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            var institute = org.Institution.FirstOrDefault(ss => ss.ID == instId);
            if (institute != null)
            {
                instituteName = isShortName ? institute.Name : institute.LegalName;
            }
            return instituteName;
        }

        public static void ClearValidationMessages(OrderEntity order)
        {
            order.ValidationMessage = string.Empty;
            foreach (var orderItemEntity in order.Items)
            {
                orderItemEntity.ValidationMessage = string.Empty;
            }
        }

        private static string GetAccountName(ICMBroker broker, Guid accountCID)
        {
            string accountName = string.Empty;
            var accountCM = broker.GetBMCInstance(accountCID);

            if (accountCM != null)
            {
                accountName = accountCM.Name;
            }
            return accountName;
        }

        private static string GetCustomerNumber(ICMBroker broker, Guid accountCID)
        {
            string customerNO = string.Empty;
            var accountCM = broker.GetBMCInstance(accountCID);
            BankAccountDS ds = new BankAccountDS();
            accountCM.GetData(ds);

            if (ds.Tables.Count > 0 && ds.Tables[ds.BankAccountsTable.TableName].Rows.Count > 0)
            {
                customerNO = ds.Tables[ds.BankAccountsTable.TableName].Rows[0][ds.BankAccountsTable.CUSTOMERNO].ToString();
            }
            return customerNO;
        }

        private static string GetBankNameForBuyAtCall(ICMBroker broker, Guid accountCID)
        {
            string accountName = string.Empty;
            var accountCm = broker.GetBMCInstance(accountCID);
            if (accountCm != null)
            {
                var accountEntity = ((Entity.BankAccountCM)(accountCm)).BankAccountEntity;
                var org = broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                var institute = org.Institution.FirstOrDefault(ss => ss.ID == accountEntity.InstitutionID);
                if (institute != null)
                    accountName = institute.Name;

                broker.ReleaseBrokerManagedComponent(org);
            }
            return accountName;
        }

        private static string GetBankInstitute(ICMBroker broker, Guid accountCID)
        {
            string instituteName = string.Empty;
            var accountCm = broker.GetBMCInstance(accountCID);
            if (accountCm != null)
            {
                var accountEntity = ((Entity.BankAccountCM)(accountCm)).BankAccountEntity;
                var org = broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                var institute = org.Institution.FirstOrDefault(ss => ss.ID == accountEntity.InstitutionID);
                if (institute != null)
                    instituteName = institute.Name;
                broker.ReleaseBrokerManagedComponent(org);
            }
            return instituteName;
        }

        private static void GetSecurityDetail(OrderPadASXBulkInstructionEntity instructionEntity, ICMBroker broker)
        {
            var org = broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            var security = org.Securities.Where(ss => ss.AsxCode == instructionEntity.InvestmentCode).FirstOrDefault();
            if (security != null)
            {
                instructionEntity.InvestmentName = security.CompanyName;
                instructionEntity.MarketCode = security.Market;

                if (security.ASXSecurity.Count > 0)
                {
                    var asxSecurity = security.ASXSecurity.OrderByDescending(sec => sec.Date).FirstOrDefault();
                    if (asxSecurity != null)
                    {
                        instructionEntity.CurrencyCode = asxSecurity.Currency;
                    }
                }
            }
            broker.ReleaseBrokerManagedComponent(org);
        }

        public static void CompleteInCompleteOrder(string clientID, OrderEntity order, DatasetCommandTypes command, ICMBroker broker, UserEntity user)
        {
            var settledUnsetteldCID = SettledUnsettledUtilities.CheckIfSettelledCmExists(clientID, broker, user, false);

            if (settledUnsetteldCID != Guid.Empty)
            {
                var ds = new SettledUnsetteledDS
                {
                    Unit = new Data.OrganizationUnit
                    {
                        ClientId = clientID,
                        CurrentUser = user,
                        Type = OrganizationType.SettledUnsettled.ToString(),
                    },

                };
                ds.ExtendedProperties.Add("Order", order);
                var settledCM = broker.GetBMCInstance(settledUnsetteldCID);
                ds.CommandType = command;
                settledCM.SetData(ds);

                broker.ReleaseBrokerManagedComponent(settledCM);
            }
        }
    }

    internal class ClientCurrentholdingContainer
    {
        public string ClientID { get; set; }
        public Guid BrokerID { get; set; }
        public Guid BankID { get; set; }
        public string SecuirtyCode { get; set; }
        public decimal AvailableBalance { get; set; }
    }
}
