﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.CM.Entity;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Enum;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Utilities;
using IdentityCMDetail = Oritax.TaxSimp.Common.IdentityCMDetail;
using System.Collections.ObjectModel;
using CashManagementEntity = Oritax.TaxSimp.Common.CashManagementEntity;
using DoItWithMe = Oritax.TaxSimp.Common.DoItWithMe;
using ServiceType = Oritax.TaxSimp.Common.ServiceType;
using Oritax.TaxSimp.CM.Group;
using AddressEntity = Oritax.TaxSimp.Common.AddressEntity;
using DualAddressEntity = Oritax.TaxSimp.Common.DualAddressEntity;
using System.Diagnostics;

namespace Oritax.TaxSimp.CM
{
    public class ClientUtilities
    {
        private static IMISEntity statetsreet;

        public static bool IsMISAttachedWithFund(string FundCode, string MisName, List<IdentityCMDetail> MISAccounts, ICMBroker Broker)
        {
            bool result = false;

            if (MISAccounts != null && MISAccounts.Count > 0)
            {
                var cm = MISAccounts.First();
                if (statetsreet == null)
                {
                    statetsreet = Broker.GetCMImplementation(cm.Clid, cm.Csid) as IMISEntity;
                }

                foreach (var fund in MISAccounts)
                {
                    if (fund.FundID != null)
                    {
                        if (statetsreet != null)
                        {
                            result = statetsreet.HasFund(FundCode, fund.FundID);
                            if (result)
                                break;
                        }

                    }
                }

            }


            return result;
        }

        public static bool IsMISAttached(Guid clid, Guid csid, List<IdentityCMDetail> MISAccounts)
        {
            bool result = false;
            if (MISAccounts != null)
            {
                result = MISAccounts.Count(identity => identity.Clid == clid && identity.Csid == csid) > 0;
            }
            return result;
        }

        public static bool IsMISFundCodeAttached(Guid FundCodeId, List<IdentityCMDetail> MISAccounts)
        {
            bool result = false;
            if (MISAccounts != null)
            {
                result = MISAccounts.Count(identity => identity.FundID != null && identity.FundID == FundCodeId) > 0;
            }
            return result;
        }

        private static void CalculateDiviidend(Guid clientCID, string ClientID, DividendEntity dv, SecuritiesEntity security, ICMBroker Broker, List<AccountProcessTaskEntity> listAccountProcess, ObservableCollection<DividendEntity> dividendCollection, Guid clientClid, Guid clientCsid, string clientName, List<IdentityCMDetail> managedInvestmentSchemesAccounts, List<IdentityCMDetail> asxAccount, List<IdentityCMDetail> bankAccounts)
        {

            if (listAccountProcess != null)
            {
                int select = 0;

                // remove old unassigned account dividends
                var AccountNA = dividendCollection.Where(
                        d =>
                        d.ID == dv.ID && (d.Account == null || d.Account.Clid == Guid.Empty || d.Account.Csid == Guid.Empty));
                if (AccountNA != null)
                {
                    for (int i = AccountNA.Count() - 1; i >= 0; i--)
                    {
                        dividendCollection.Remove(AccountNA.ElementAt(i));
                    }
                }
                var reportNA =
                    dv.ReportCollection.Where(
                        r =>
                        r.ID == dv.ID && r.Clid == clientClid && r.Csid == clientCsid &&
                        (r.Account == null || r.Account.Clid == Guid.Empty || r.Account.Csid == Guid.Empty));
                if (reportNA != null)
                {
                    for (int i = reportNA.Count() - 1; i >= 0; i--)
                    {
                        dv.ReportCollection.Remove(reportNA.ElementAt(i));
                    }
                }
                //dv.Status = DividendStatus.Accrued;
                if (security.InvestmentType == "Managed Fund /Unit Trust")
                {
                    foreach (var mis in managedInvestmentSchemesAccounts)
                    {
                        ManagedInvestmentSchemesAccountCM temp = (ManagedInvestmentSchemesAccountCM)Broker.GetCMImplementation(mis.Clid, mis.Csid);
                        if (temp != null)
                        {
                            var misfund = temp.ManagedInvestmentSchemesAccountEntity.FundAccounts.Where(fund => fund.ID == mis.FundID && mis.FundID == security.FundID).
                                    FirstOrDefault();

                            if (misfund != null && misfund.Code == security.AsxCode)
                            {
                                decimal transactionShares = 0;
                                transactionShares =
                                    misfund.FundTransactions.Where(
                                        ee => ee.ClientID == ClientID && ee.TradeDate <= dv.RecordDate).Sum(
                                            dd => dd.Shares);
                                if (transactionShares != 0)
                                {
                                    select = Convert.ToInt32(transactionShares);
                                    SetDividends(clientCID, ClientID, string.Empty, string.Empty, clientClid, clientCsid, clientName, bankAccounts, asxAccount, dividendCollection, dv, security, Broker,
                                                 security.AsxCode, mis, select);
                                }
                            }
                        }
                    }
                }
                else
                {
                    foreach (var asxbroker in asxAccount)
                    {
                        DesktopBrokerAccountCM temp = (DesktopBrokerAccountCM)Broker.GetCMImplementation(asxbroker.Clid, asxbroker.Csid);
                        if (temp != null)
                        {

                            var DesktopValue =
                                temp.DesktopBrokerAccountEntity.holdingTransactions.Where(fund => fund.InvestmentCode == security.AsxCode).ToList();

                            if (DesktopValue != null)
                            {
                                decimal transactionShares = 0;
                                transactionShares = DesktopValue.Where(ee => ee.TradeDate <= dv.RecordDate.Value.AddDays(-1)).Sum(dd => dd.Units);

                                if (transactionShares != 0)
                                {
                                    select = Convert.ToInt32(transactionShares);
                                    DesktopBrokerAccountCM tempdesktopbroker =
                                        (DesktopBrokerAccountCM)Broker.GetCMImplementation(asxbroker.Clid, asxbroker.Csid);

                                    SetDividends(clientCID, ClientID, tempdesktopbroker.DesktopBrokerAccountEntity.Name,
                                                 tempdesktopbroker.DesktopBrokerAccountEntity.AccountNumber, clientClid, clientCsid, clientName, bankAccounts, asxAccount, dividendCollection, dv,
                                                 security, Broker, security.AsxCode, asxbroker, select);
                                }
                            }
                        }
                    }
                }
            }
        }
        public static void ApplyDividend(string ClientID, IClientUMAData clientEntity, DividendEntity dv, List<ProductEntity> ProductCollection, SecuritiesEntity security, ICMBroker Broker, IOrganizationUnit orgUnit)
        {
            if (clientEntity.DividendCollection == null)
            {
                clientEntity.DividendCollection = new ObservableCollection<DividendEntity>();
            }
            CalculateDiviidend(orgUnit.CID, ClientID, dv, security, Broker, (clientEntity as IClientEntityOrgUnits).ListAccountProcess, clientEntity.DividendCollection, orgUnit.CLID, orgUnit.CSID, orgUnit.Name, clientEntity.ManagedInvestmentSchemesAccounts, clientEntity.DesktopBrokerAccounts, clientEntity.BankAccounts);
        }

        private static void SetDividends(Guid clientCID, string ClientID, string accountName, string accountNumber, Guid clientClid, Guid clientCsid, string clientName, List<IdentityCMDetail> bankAccounts, List<IdentityCMDetail> asxkAccounts, ObservableCollection<DividendEntity> dividendCollection, DividendEntity dv, SecuritiesEntity security, ICMBroker Broker, string InvestmentCode, IdentityCMDetail account, int units)
        {
            dv.UnitsOnHand = units;
            if (units != 0)
            {
                string eventname = "added";
                var oldReports = dv.ReportCollection.Where(r => r.ID == dv.ID && r.Clid == clientClid && r.Csid == clientCsid && r.Account.Clid == account.Clid && r.Account.Csid == account.Csid);
                if (oldReports != null)
                {
                    for (int i = oldReports.Count() - 1; i >= 0; i--)
                    {
                        dv.ReportCollection.Remove(oldReports.ElementAt(i));
                    }
                    eventname = "updated";
                }


                IdentityCM linkaccount = new IdentityCM();
                linkaccount.Clid = account.Clid;
                linkaccount.Csid = account.Csid;

                dv.ReportCollection.Add(new DividendReportEntity
                                            {
                                                acname = accountName,
                                                acnumber = accountNumber,
                                                Account = linkaccount,
                                                CentsPerShare = dv.CentsPerShare,
                                                Clid = clientClid,
                                                ClientIdentification = ClientID,
                                                Csid = clientCsid,
                                                ID = dv.ID,
                                                Name = clientName,
                                                PaymentDate = dv.PaymentDate,
                                                RecordDate = dv.RecordDate,
                                                UnitsOnHand = units,
                                                PaidDividend = units * dv.CentsPerShare
                                            });

                string banktransID = "";
                string asxtransID = "";
                DividendStatus DividendStatus = DividendStatus.Accrued;

                /*
                 * - Actually dv is not the one get persisted later on for ClientSMSFCorporateTrusteeCM, it belongs to OrganizationCM.
                 *   The one getting persisted is created later on with dividendCollection.Add(AddNewDividend....
                 * - Here I need to set value of PaidDividend, as this value is used to match insted of UnitAmount*DividendPerShare
                 * - dvTemp is used only to get PaidDividend
                 */
                DividendEntity dvTemp = AddNewDividend(dv, InvestmentCode, units, linkaccount, banktransID, asxtransID, DividendStatus);
                dv.PaidDividend = dvTemp.PaidDividend;

                if (!MatchDivWithBankTran(bankAccounts, dv, Broker, units, ref banktransID, ref DividendStatus))
                {
                    MatchDivWithASXtran(asxkAccounts, dv, InvestmentCode, Broker, units, security, ref asxtransID, ref DividendStatus);
                }


                var oldividends = dividendCollection.Where(d => d.ID == dv.ID && d.Account.Clid == account.Clid && d.Account.Csid == account.Csid).FirstOrDefault();
                if (oldividends != null)
                {
                    // if oldividends.IsManualUpdated, that means user had put some manual tax components. And probably it's matched already with a bank account. 
                    // In this situation, we don't need to re-match or update
                    if (!oldividends.IsManualUpdated)
                    {
                        //Work Item ID 535: don't update manually edited dividends
                        if (!oldividends.MannualEdit)
                            UpdateDividendObject(dv, InvestmentCode, units, linkaccount, oldividends);
                        oldividends.Status = DividendStatus;
                        oldividends.CashManagementTransactionID = banktransID;
                        oldividends.DesktopbrokerTransID = asxtransID;
                    }
                }
                else
                    dividendCollection.Add(AddNewDividend(dv, InvestmentCode, units, linkaccount, banktransID, asxtransID, DividendStatus));


                Broker.LogEvent(EventType.UMAClientUpdated, clientCID, string.Format("Income Transaction of {0} {1} in client {2} ({3}), Record Date - {4}, Payment date - {5}", security.AsxCode, eventname, clientName, ClientID, Convert.ToDateTime(dv.RecordDate.ToString()).ToString("d"), Convert.ToDateTime(dv.PaymentDate.ToString()).ToString("d")));
            }
        }
        private static DividendEntity AddNewDividend(DividendEntity dv, string InvestmentCode, int units, IdentityCM linkaccount, string ExternalRef, string asxtransID, DividendStatus DividendStatus)
        {
            DividendEntity dividendEntity = new DividendEntity
            {
                Account = linkaccount,
                AdministrationSystem = dv.AdministrationSystem,
                Comment = dv.Comment,
                ExternalReferenceID = dv.ExternalReferenceID,
                DomesticDiscountedCGT = ((dv.DomesticDiscountedCGT) * ((units * dv.CentsPerShare) / 100)),
                DomesticGrossAmount = ((dv.DomesticGrossAmount) * ((units * dv.CentsPerShare) / 100)),
                DomesticImputationTaxCredits = ((dv.DomesticImputationTaxCredits) * ((units * dv.CentsPerShare) / 100)),
                DomesticIndexationCGT = ((dv.DomesticIndexationCGT) * ((units * dv.CentsPerShare) / 100)),
                DomesticInterestAmount = ((dv.DomesticInterestAmount) * ((units * dv.CentsPerShare) / 100)),
                DomesticOtherCGT = ((dv.DomesticOtherCGT) * ((units * dv.CentsPerShare) / 100)),
                DomesticOtherIncome = ((dv.DomesticOtherIncome) * ((units * dv.CentsPerShare) / 100)),
                DomesticWithHoldingTax = ((dv.DomesticWithHoldingTax) * ((units * dv.CentsPerShare) / 100)),
                ForeignDiscountedCGT = ((dv.ForeignDiscountedCGT) * ((units * dv.CentsPerShare) / 100)),
                ForeignImputationTaxCredits = ((dv.ForeignImputationTaxCredits) * ((units * dv.CentsPerShare) / 100)),
                ForeignIndexationCGT = ((dv.ForeignIndexationCGT) * ((units * dv.CentsPerShare) / 100)),
                ForeignInterestAmount = ((dv.ForeignInterestAmount) * ((units * dv.CentsPerShare) / 100)),
                ForeignInvestmentFunds = ((dv.ForeignInvestmentFunds) * ((units * dv.CentsPerShare) / 100)),
                ForeignOtherCGT = ((dv.ForeignOtherCGT) * ((units * dv.CentsPerShare) / 100)),
                ForeignOtherIncome = ((dv.ForeignOtherIncome) * ((units * dv.CentsPerShare) / 100)),
                ForeignWithHoldingTax = ((dv.ForeignWithHoldingTax) * ((units * dv.CentsPerShare) / 100)),
                FrankedAmount = ((dv.FrankedAmount) * ((units * dv.CentsPerShare) / 100)),
                UnfrankedAmount = ((dv.UnfrankedAmount) * ((units * dv.CentsPerShare) / 100)),
                ReturnOfCapital = ((dv.ReturnOfCapital) * ((units * dv.CentsPerShare) / 100)),
                TaxDeferred = ((dv.TaxDeferred) * ((units * dv.CentsPerShare) / 100)),
                TaxFree = ((dv.TaxFree) * ((units * dv.CentsPerShare) / 100)),
                PercentDomesticDiscountedCGT = dv.DomesticDiscountedCGT,
                PercentDomesticGrossAmount = dv.DomesticGrossAmount,
                PercentDomesticImputationTaxCredits = dv.DomesticImputationTaxCredits,
                PercentDomesticIndexationCGT = dv.DomesticIndexationCGT,
                PercentDomesticInterestAmount = dv.DomesticInterestAmount,
                PercentDomesticOtherCGT = dv.DomesticOtherCGT,
                PercentDomesticOtherIncome = dv.DomesticOtherIncome,
                PercentDomesticWithHoldingTax = dv.DomesticWithHoldingTax,
                PercentForeignDiscountedCGT = dv.ForeignDiscountedCGT,
                PercentForeignImputationTaxCredits = dv.ForeignImputationTaxCredits,
                PercentForeignIndexationCGT = dv.ForeignIndexationCGT,
                PercentForeignInterestAmount = dv.ForeignInterestAmount,
                PercentForeignInvestmentFunds = dv.ForeignInvestmentFunds,
                PercentForeignOtherCGT = dv.ForeignOtherCGT,
                PercentForeignOtherIncome = dv.ForeignOtherIncome,
                PercentForeignWithHoldingTax = dv.ForeignWithHoldingTax,
                PercentFrankedAmount = dv.FrankedAmount,
                PercentUnfrankedAmount = dv.UnfrankedAmount,
                PercentReturnOfCapital = dv.ReturnOfCapital,
                PercentTaxDeferred = dv.TaxDeferred,
                PercentTaxFree = dv.TaxFree,
                ID = dv.ID,
                InvestmentCode = InvestmentCode,
                Investor = dv.Investor,
                PaymentDate = dv.PaymentDate,
                ProcessFlag = dv.ProcessFlag,
                RecordDate = dv.RecordDate,
                TransactionType = dv.TransactionType,
                CentsPerShare = dv.CentsPerShare,
                UnitsOnHand = units,
                Status = DividendStatus,
                CashManagementTransactionID = ExternalRef,
                DesktopbrokerTransID = asxtransID,
                BalanceDate = dv.BalanceDate,
                BooksCloseDate = dv.BooksCloseDate,
                Currency = dv.Currency,
                Dividendtype = dv.Dividendtype

            };

            dividendEntity.PaidDividend = GetPaidDividend(dividendEntity);
            return dividendEntity;
        }

        private static void UpdateDividendObject(DividendEntity dv, string InvestmentCode, int units, IdentityCM linkaccount, DividendEntity oldividends)
        {
            oldividends.Account = linkaccount;
            oldividends.AdministrationSystem = dv.AdministrationSystem;
            oldividends.Comment = dv.Comment;
            oldividends.ExternalReferenceID = dv.ExternalReferenceID;
            oldividends.DomesticDiscountedCGT = ((dv.DomesticDiscountedCGT) * ((units * dv.CentsPerShare) / 100));
            oldividends.DomesticGrossAmount = ((dv.DomesticGrossAmount) * ((units * dv.CentsPerShare) / 100));
            oldividends.DomesticImputationTaxCredits = ((dv.DomesticImputationTaxCredits) * ((units * dv.CentsPerShare) / 100));
            oldividends.DomesticIndexationCGT = ((dv.DomesticIndexationCGT) * ((units * dv.CentsPerShare) / 100));
            oldividends.DomesticInterestAmount = ((dv.DomesticInterestAmount) * ((units * dv.CentsPerShare) / 100));
            oldividends.DomesticOtherCGT = ((dv.DomesticOtherCGT) * ((units * dv.CentsPerShare) / 100));
            oldividends.DomesticOtherIncome = ((dv.DomesticOtherIncome) * ((units * dv.CentsPerShare) / 100));
            oldividends.DomesticWithHoldingTax = ((dv.DomesticWithHoldingTax) * ((units * dv.CentsPerShare) / 100));
            oldividends.ForeignDiscountedCGT = ((dv.ForeignDiscountedCGT) * ((units * dv.CentsPerShare) / 100));
            oldividends.ForeignImputationTaxCredits = ((dv.ForeignImputationTaxCredits) * ((units * dv.CentsPerShare) / 100));
            oldividends.ForeignIndexationCGT = ((dv.ForeignIndexationCGT) * ((units * dv.CentsPerShare) / 100));
            oldividends.ForeignInterestAmount = ((dv.ForeignInterestAmount) * ((units * dv.CentsPerShare) / 100));
            oldividends.ForeignInvestmentFunds = ((dv.ForeignInvestmentFunds) * ((units * dv.CentsPerShare) / 100));
            oldividends.ForeignOtherCGT = ((dv.ForeignOtherCGT) * ((units * dv.CentsPerShare) / 100));
            oldividends.ForeignOtherIncome = ((dv.ForeignOtherIncome) * ((units * dv.CentsPerShare) / 100));
            oldividends.ForeignWithHoldingTax = ((dv.ForeignWithHoldingTax) * ((units * dv.CentsPerShare) / 100));
            oldividends.FrankedAmount = ((dv.FrankedAmount) * ((units * dv.CentsPerShare) / 100));
            oldividends.UnfrankedAmount = ((dv.UnfrankedAmount) * ((units * dv.CentsPerShare) / 100));
            oldividends.ReturnOfCapital = ((dv.ReturnOfCapital) * ((units * dv.CentsPerShare) / 100));
            oldividends.TaxDeferred = ((dv.TaxDeferred) * ((units * dv.CentsPerShare) / 100));
            oldividends.TaxFree = ((dv.TaxFree) * ((units * dv.CentsPerShare) / 100));
            oldividends.PercentDomesticDiscountedCGT = dv.DomesticDiscountedCGT;
            oldividends.PercentDomesticGrossAmount = dv.DomesticGrossAmount;
            oldividends.PercentDomesticImputationTaxCredits = dv.DomesticImputationTaxCredits;
            oldividends.PercentDomesticIndexationCGT = dv.DomesticIndexationCGT;
            oldividends.PercentDomesticInterestAmount = dv.DomesticInterestAmount;
            oldividends.PercentDomesticOtherCGT = dv.DomesticOtherCGT;
            oldividends.PercentDomesticOtherIncome = dv.DomesticOtherIncome;
            oldividends.PercentDomesticWithHoldingTax = dv.DomesticWithHoldingTax;
            oldividends.PercentForeignDiscountedCGT = dv.ForeignDiscountedCGT;
            oldividends.PercentForeignImputationTaxCredits = dv.ForeignImputationTaxCredits;
            oldividends.PercentForeignIndexationCGT = dv.ForeignIndexationCGT;
            oldividends.PercentForeignInterestAmount = dv.ForeignInterestAmount;
            oldividends.PercentForeignInvestmentFunds = dv.ForeignInvestmentFunds;
            oldividends.PercentForeignOtherCGT = dv.ForeignOtherCGT;
            oldividends.PercentForeignOtherIncome = dv.ForeignOtherIncome;
            oldividends.PercentForeignWithHoldingTax = dv.ForeignWithHoldingTax;
            oldividends.PercentFrankedAmount = dv.FrankedAmount;
            oldividends.PercentUnfrankedAmount = dv.UnfrankedAmount;
            oldividends.PercentReturnOfCapital = dv.ReturnOfCapital;
            oldividends.PercentTaxDeferred = dv.TaxDeferred;
            oldividends.PercentTaxFree = dv.TaxFree;
            oldividends.ID = dv.ID;
            oldividends.InvestmentCode = InvestmentCode;
            oldividends.Investor = dv.Investor;
            oldividends.PaymentDate = dv.PaymentDate;
            oldividends.ProcessFlag = dv.ProcessFlag;
            oldividends.RecordDate = dv.RecordDate;
            oldividends.TransactionType = dv.TransactionType;
            oldividends.CentsPerShare = dv.CentsPerShare;
            oldividends.UnitsOnHand = units;
            oldividends.BalanceDate = dv.BalanceDate;
            oldividends.BooksCloseDate = dv.BooksCloseDate;
            oldividends.Currency = dv.Currency;
            oldividends.Dividendtype = dv.Dividendtype;

            oldividends.PaidDividend = GetPaidDividend(oldividends);
        }

        public static double GetPaidDividend(DividendEntity dividendEntity)
        {
            return dividendEntity.DomesticInterestAmount + dividendEntity.FrankedAmount + dividendEntity.UnfrankedAmount + dividendEntity.DomesticOtherIncome +
                           dividendEntity.ForeignInterestAmount + dividendEntity.ForeignOtherIncome +
                           dividendEntity.DomesticIndexationCGT + dividendEntity.ForeignIndexationCGT +
                           dividendEntity.DomesticOtherCGT + dividendEntity.ForeignOtherCGT +
                           dividendEntity.DomesticDiscountedCGT + dividendEntity.ForeignDiscountedCGT +
                           dividendEntity.TaxDeferred + dividendEntity.TaxFree +
                           dividendEntity.ReturnOfCapital;

        }

        private static bool MatchDivWithASXtran(List<IdentityCMDetail> asxkAccounts, DividendEntity dv, string investmentCode, ICMBroker broker, decimal units, SecuritiesEntity security, ref string asxtransID, ref DividendStatus dividendStatus)
        {
            bool matched = false;

            var securityprice = security.ASXSecurity.OrderByDescending(ee => ee.Date).FirstOrDefault(ss => ss.Date <= dv.RecordDate);
            decimal asOfDateUnitPrice = 0;

            if (securityprice != null && securityprice.UnitPrice != 0.0)
            {
                asOfDateUnitPrice = (decimal)securityprice.UnitPrice;

                foreach (var countbankaccount in asxkAccounts)
                {
                    DesktopBrokerAccountCM tempbankaccount = (DesktopBrokerAccountCM)broker.GetCMImplementation(countbankaccount.Clid, countbankaccount.Csid);
                    if (tempbankaccount != null)
                    {
                        if (tempbankaccount.DesktopBrokerAccountEntity.holdingTransactions != null)
                        {

                            var cmTransaction = tempbankaccount.DesktopBrokerAccountEntity.holdingTransactions.Where(ee => ee.IsDividentMatchWithTrans(dv, units, asOfDateUnitPrice)).FirstOrDefault();
                            if (cmTransaction != null)
                            {
                                tempbankaccount.Broker.SetStart();
                                cmTransaction.DividendStatus = TransectionStatus.Matched;
                                cmTransaction.DividendID = dv.ID;
                                asxtransID = cmTransaction.TranstactionUniqueID.ToString();
                                dividendStatus = DividendStatus.Matched;
                                matched = true;
                                tempbankaccount.CalculateToken(true);
                                break;
                            }
                        }
                    }

                }
            }
            return matched;
        }



        private static bool MatchDivWithBankTran(List<IdentityCMDetail> BankAccounts, DividendEntity dv, ICMBroker Broker, int units, ref string ExternalRef, ref DividendStatus DividendStatus)
        {
            bool matched = false;
            foreach (var countbankaccount in BankAccounts)
            {
                BankAccountCM tempbankaccount = (BankAccountCM)Broker.GetCMImplementation(countbankaccount.Clid, countbankaccount.Csid);
                if (tempbankaccount != null)
                {
                    if (tempbankaccount.BankAccountEntity.CashManagementTransactions != null)
                    {
                        CashManagementEntity cmTransaction = tempbankaccount.BankAccountEntity.CashManagementTransactions.FirstOrDefault(ee => ee.IsDividendMatchingTrans(dv, units));
                        if (cmTransaction != null)
                        {
                            tempbankaccount.Broker.SetStart();
                            cmTransaction.DividendStatus = TransectionStatus.Matched;
                            cmTransaction.DividendID = dv.ID;
                            ExternalRef = cmTransaction.ExternalReferenceID;
                            DividendStatus = DividendStatus.Matched;
                            matched = true;
                            tempbankaccount.CalculateToken(true);
                            break;
                        }
                    }
                }

            }
            return matched;
        }

        public static void ImportDistributions(DataRow[] drs, CorporateEntity entity, ICMBroker Broker)
        {
            if (entity.DistributionIncomes == null)
            {
                entity.DistributionIncomes = new System.Collections.ObjectModel.ObservableCollection<DistributionIncomeEntity>();
            }
            SetClientDistributions(drs, entity.ClientId, entity.Name, entity.DistributionIncomes, entity.ManagedInvestmentSchemesAccounts, Broker, entity.BankAccounts);
        }

        public static void ImportDistributions(DataRow[] drs, ClientIndividualEntity entity, ICMBroker Broker)
        {
            if (entity.DistributionIncomes == null)
            {
                entity.DistributionIncomes = new System.Collections.ObjectModel.ObservableCollection<DistributionIncomeEntity>();
            }
            SetClientDistributions(drs, entity.ClientId, entity.Name, entity.DistributionIncomes, entity.ManagedInvestmentSchemesAccounts, Broker, entity.BankAccounts);
        }

        public static void ProcessDistribution(DistributionEntity distributionEntity, ClientIndividualEntity entity, ICMBroker Broker)
        {
            if (entity.DistributionIncomes == null)
            {
                entity.DistributionIncomes = new System.Collections.ObjectModel.ObservableCollection<DistributionIncomeEntity>();
            }

            SetClientDistributions(distributionEntity, entity.ClientId, entity.Name, entity.DistributionIncomes, entity.ManagedInvestmentSchemesAccounts, Broker, entity.BankAccounts);
        }

        public static void ProcessDistribution(DistributionEntity distributionEntity, CorporateEntity entity, ICMBroker Broker)
        {
            if (entity.DistributionIncomes == null)
            {
                entity.DistributionIncomes = new System.Collections.ObjectModel.ObservableCollection<DistributionIncomeEntity>();
            }

            SetClientDistributions(distributionEntity, entity.ClientId, entity.Name, entity.DistributionIncomes, entity.ManagedInvestmentSchemesAccounts, Broker, entity.BankAccounts);
        }

        private static void SetClientDistributions(DistributionEntity distributionEntity, string clientId, string ClienName, ObservableCollection<DistributionIncomeEntity> DistributionIncomes, List<IdentityCMDetail> ManagedInvestmentSchemesAccounts, ICMBroker Broker, List<IdentityCMDetail> bankAccounts)
        {
            ManagedInvestmentSchemesAccountCM misCm = null;
            foreach (var attachedMisAccount in ManagedInvestmentSchemesAccounts)
            {
                if (misCm == null)
                    misCm = (ManagedInvestmentSchemesAccountCM)Broker.GetCMImplementation(attachedMisAccount.Clid, attachedMisAccount.Csid);
                if (misCm != null)
                {
                    var misfund = misCm.ManagedInvestmentSchemesAccountEntity.FundAccounts.Where(fund => fund.ID == attachedMisAccount.FundID).FirstOrDefault();
                    if (misfund != null && misfund.Code == distributionEntity.Code)
                    {
                        var hastransaction = misfund.FundTransactions.Where(ee => ee.ClientID == clientId).Count() > 0;
                        var transactions = misfund.FundTransactions.Where(ee => ee.ClientID == clientId);
                        decimal recordShares = misfund.FundTransactions.Where(ee => ee.ClientID == clientId).Where(tran => tran.TradeDate.Date <= distributionEntity.RecordDate.Value.Date).Sum(tran => tran.Shares);

                        if (hastransaction && recordShares > 0)
                            AddDistribution(distributionEntity, clientId, ClienName, DistributionIncomes, misfund.ID, misfund.Description, recordShares, bankAccounts, ManagedInvestmentSchemesAccounts, Broker);
                    }
                }
            }
        }

        private static void AddDistribution(DistributionEntity distributionEntity, string clientID, string clientName, ObservableCollection<DistributionIncomeEntity> DistributionIncomes, Guid fundId, string fundName, decimal recordShares, List<IdentityCMDetail> bankAccounts, List<IdentityCMDetail> MISAccounts, ICMBroker broker)
        {

            var olddistributionIncome = DistributionIncomes.Where(ss => ss.FundCode == distributionEntity.Code && ss.RecordDate.HasValue && ss.RecordDate.Value.Date == distributionEntity.RecordDate).FirstOrDefault();
            if (olddistributionIncome != null)
            {

                if (!olddistributionIncome.IsManualUpdated)
                {
                    if (olddistributionIncome.Components == null)
                        olddistributionIncome.Components = new List<DistributionComponent>();

                    olddistributionIncome.Components.Clear();
                    olddistributionIncome.RecordDate = distributionEntity.RecordDate;
                    olddistributionIncome.PaymentDate = distributionEntity.PaymentDate;

                    foreach (DistributionComponent distributionComponent in distributionEntity.Components)
                    {
                        DistributionComponent toCopyDistributionComponent = new DistributionComponent();
                        toCopyDistributionComponent.ComponentType = distributionComponent.ComponentType;

                        double value = 0.00;

                        if (distributionComponent.Autax_Credit.HasValue)
                        {
                            value = (distributionComponent.Autax_Credit.Value / 100) * Convert.ToDouble(recordShares);
                            toCopyDistributionComponent.Autax_Credit = Math.Round(value, 2);
                        }
                        if (distributionComponent.Tax_WithHeld.HasValue)
                        {
                            value = (distributionComponent.Tax_WithHeld.Value / 100) * Convert.ToDouble(recordShares);
                            toCopyDistributionComponent.Tax_WithHeld = Math.Round(value, 2);
                        }
                        if (distributionComponent.Unit_Amount.HasValue)
                        {
                            value = (distributionComponent.Unit_Amount.Value / 100) * Convert.ToDouble(recordShares);
                            toCopyDistributionComponent.Unit_Amount = Math.Round(value, 2);
                        }
                        if (toCopyDistributionComponent.DividendRate.HasValue)
                            toCopyDistributionComponent.DividendRate = Math.Round(distributionComponent.DividendRate.Value, 2);
                        olddistributionIncome.Components.Add(toCopyDistributionComponent);
                    }
                    //Matching with Bank transaction for status update; task 716
                   if(! MatchDisWithBankTran(bankAccounts, olddistributionIncome, broker))
                   {
                       MatchDisWithMISTran(MISAccounts, olddistributionIncome, clientID, broker);
                   }
                }


            }
            else
            {
                var distributionIncome = new DistributionIncomeEntity();

                distributionIncome.RecordDate = distributionEntity.RecordDate;
                distributionIncome.PaymentDate = distributionEntity.PaymentDate;
                distributionIncome.ID = Guid.NewGuid();
                distributionIncome.FundCode = distributionEntity.Code;
                distributionIncome.FundID = fundId;
                distributionIncome.FundName = fundName;
                distributionIncome.AccountName = clientName;
                distributionIncome.AccountNumber = clientName;
                distributionIncome.RecodDate_Shares = Convert.ToDouble(recordShares);
                var Components = new List<DistributionComponent>();
                distributionIncome.Components = Components;

                double value = 0.00;

                foreach (DistributionComponent distributionComponent in distributionEntity.Components)
                {
                    DistributionComponent toCopyDistributionComponent = new DistributionComponent();
                    toCopyDistributionComponent.ComponentType = distributionComponent.ComponentType;

                    if (distributionComponent.Autax_Credit.HasValue)
                    {
                        value = (distributionComponent.Autax_Credit.Value / 100) * Convert.ToDouble(recordShares);
                        toCopyDistributionComponent.Autax_Credit = Math.Round(value, 2);
                    }
                    if (distributionComponent.Tax_WithHeld.HasValue)
                    {
                        value = (distributionComponent.Tax_WithHeld.Value / 100) * Convert.ToDouble(recordShares);
                        toCopyDistributionComponent.Tax_WithHeld = Math.Round(value, 2);
                    }
                    if (distributionComponent.Unit_Amount.HasValue)
                    {
                        value = (distributionComponent.Unit_Amount.Value / 100) * Convert.ToDouble(recordShares);
                        toCopyDistributionComponent.Unit_Amount = Math.Round(value, 2);
                    }
                    if (toCopyDistributionComponent.DividendRate.HasValue)
                        toCopyDistributionComponent.DividendRate = Math.Round(distributionComponent.DividendRate.Value, 2);

                    distributionIncome.Components.Add(toCopyDistributionComponent);
                }

                //Matching with Bank transaction for status update; task 716
               if(!MatchDisWithBankTran(bankAccounts, distributionIncome, broker))
               {
                   MatchDisWithMISTran(MISAccounts, distributionIncome, clientID, broker);
               }

                DistributionIncomes.Add(distributionIncome);
            }
        }

        private static void SetClientDistributions(DataRow[] drs, string clientId, string ClienName, System.Collections.ObjectModel.ObservableCollection<DistributionIncomeEntity> DistributionIncomes, List<IdentityCMDetail> ManagedInvestmentSchemesAccounts, ICMBroker Broker, List<IdentityCMDetail> bankAccounts)
        {
            ManagedInvestmentSchemesAccountCM misCm = null;
            foreach (var attachedMisAccount in ManagedInvestmentSchemesAccounts)
            {
                if (misCm == null)
                    misCm = (ManagedInvestmentSchemesAccountCM)Broker.GetCMImplementation(attachedMisAccount.Clid, attachedMisAccount.Csid);
                if (misCm != null)
                {
                    var misfund = misCm.ManagedInvestmentSchemesAccountEntity.FundAccounts.Where(fund => fund.ID == attachedMisAccount.FundID).FirstOrDefault();
                    if (misfund != null)
                    {
                        var hastransaction = misfund.FundTransactions.Where(ee => ee.ClientID == clientId).Count() > 0;
                        if (hastransaction)
                        {
                            var fundrows = drs.Where(ss => ss["FUNDNUMBER"].ToString() == misfund.Code);
                            foreach (DataRow dataRow in fundrows)
                            {
                                AddDistribution(dataRow, clientId, ClienName, DistributionIncomes, misfund.ID, misfund.Description, bankAccounts, ManagedInvestmentSchemesAccounts, Broker);
                            }
                        }
                    }
                }
            }
        }

        private static void AddDistribution(DataRow dataRow, string clientID, string clientName, ObservableCollection<DistributionIncomeEntity> DistributionIncomes, Guid fundId, string fundName, List<IdentityCMDetail> bankAccounts, List<IdentityCMDetail> MISAccounts, ICMBroker broker)
        {
            bool hahError = false;
            string fundCode = dataRow["FUNDNUMBER"].ToString();
            string accountNumber = dataRow["ACCOUNTNUMBER"].ToString();
            DateTimeFormatInfo info = new DateTimeFormatInfo();
            info.ShortDatePattern = "dd/MM/yyyy";


            int componenttype;

            if (!int.TryParse(dataRow["TRANSACTIONTYPEID"].ToString(), out componenttype))
            {
                dataRow["Message"] = dataRow["Message"] + ",Invalid TRANSACTION TYPE ID";
                hahError = true;
            }


            double dividendrate;

            if (!double.TryParse(dataRow["DIVIDENDRATE"].ToString(), out dividendrate))
            {
                dataRow["Message"] = dataRow["Message"] + ",Invalid DIVIDEND RATE";
                hahError = true;
            }

            double unitamount;

            if (!double.TryParse(dataRow["UNITAMOUNT"].ToString(), out unitamount))
            {
                dataRow["Message"] = dataRow["Message"] + ",Invalid UNIT AMOUNT";
                hahError = true;
            }

            double taxwitheld;
            if (!double.TryParse(dataRow["TAXWITHELD"].ToString(), out taxwitheld))
            {
                dataRow["Message"] = dataRow["Message"] + ",Invalid TAX WITHELD";
                hahError = true;
            }

            double autaxcredit;
            if (!double.TryParse(dataRow["AUTAXCREDIT"].ToString(), out autaxcredit))
            {
                dataRow["Message"] = dataRow["Message"] + ",Invalid AUTAX CREDIT";
                hahError = true;
            }

            double recorddateshares;
            if (!double.TryParse(dataRow["RECORDDATESHARES"].ToString(), out recorddateshares))
            {
                dataRow["Message"] = dataRow["Message"] + ",Invalid RECORD DATE SHARES";
                hahError = true;
            }


            DateTime recordDate;

            if (dataRow.Table.Columns["RECORDDATE"].DataType == typeof(System.DateTime) && dataRow["RECORDDATE"] != DBNull.Value)
            {
                recordDate = (DateTime)dataRow["RECORDDATE"];
            }
            else if (!DateTime.TryParse(dataRow["RECORDDATE"].ToString(), info, DateTimeStyles.None, out recordDate))
            {
                dataRow["Message"] = dataRow["Message"] + ",Invalid RECORD DATE";
                hahError = true;
            }

            if (!hahError)
            {
                var olddistributionIncome = DistributionIncomes.Where(ss => ss.FundCode == fundCode && ss.RecordDate.HasValue && ss.RecordDate.Value.Date == recordDate.Date).FirstOrDefault();
                if (olddistributionIncome != null)
                {

                    if (!olddistributionIncome.IsManualUpdated)
                    {
                        if (olddistributionIncome.Components == null)
                        {
                            olddistributionIncome.Components = new List<DistributionComponent>();
                        }

                        var oldComp = olddistributionIncome.Components.Where(ss => ss.ComponentType == componenttype).FirstOrDefault();
                        if (oldComp != null)
                        {
                            oldComp.Autax_Credit = autaxcredit;
                            oldComp.ComponentType = componenttype;
                            oldComp.DividendRate = dividendrate;
                            oldComp.Unit_Amount = unitamount;
                            oldComp.Tax_WithHeld = taxwitheld;
                        }
                        else
                        {
                            var component = new DistributionComponent();
                            component.Autax_Credit = autaxcredit;
                            component.ComponentType = componenttype;
                            component.DividendRate = dividendrate;
                            component.Unit_Amount = unitamount;
                            component.Tax_WithHeld = taxwitheld;
                            olddistributionIncome.Components.Add(component);
                        }
                        //Matching with Bank transaction for status update; task 716
                       if(!MatchDisWithBankTran(bankAccounts, olddistributionIncome, broker))
                       {
                           MatchDisWithMISTran(MISAccounts, olddistributionIncome, clientID, broker);
                       }

                        dataRow["Message"] = "Successfully updated";
                    }
                    else
                    {
                        dataRow["Message"] = "This Distribution already exists as adjusted. It cannot be overwritten.";
                    }
                }
                else
                {
                    var distributionIncome = new DistributionIncomeEntity();
                    distributionIncome.ID = Guid.NewGuid();
                    distributionIncome.FundCode = fundCode;
                    distributionIncome.FundID = fundId;
                    distributionIncome.FundName = fundName;
                    distributionIncome.AccountName = clientName;
                    distributionIncome.AccountNumber = accountNumber;
                    distributionIncome.RecordDate = recordDate;
                    distributionIncome.RecodDate_Shares = recorddateshares;
                    distributionIncome.PaymentDate = recordDate;
                    var Components = new List<DistributionComponent>();
                    var component = new DistributionComponent();
                    component.Autax_Credit = autaxcredit;
                    component.ComponentType = componenttype;
                    component.DividendRate = dividendrate;
                    component.Unit_Amount = unitamount;
                    component.Tax_WithHeld = taxwitheld;
                    Components.Add(component);
                    distributionIncome.Components = Components;

                    //Matching with Bank transaction for status update; task 716
                    if(!MatchDisWithBankTran(bankAccounts, distributionIncome, broker))
                    {
                        MatchDisWithMISTran(MISAccounts, distributionIncome, clientID, broker);
                    }

                    DistributionIncomes.Add(distributionIncome);
                    dataRow["Message"] = "Successfully Added";
                }

            }
        }

        public static void FillAccountProcessDataset(ClientAccountProcessDS ds, List<AccountProcessTaskEntity> accountprocess, ICMBroker Broker)
        {
            var organization = Broker.GetWellKnownCM(WellKnownCM.Organization) as IOrganization;
            foreach (var processentity in accountprocess)
            {
                var row = ds.Tables[ClientAccountProcessDS.ACCOUNTPROCESS_TABLE].NewRow();

                row[ClientAccountProcessDS.LINKENTIYCLID] = processentity.LinkedEntity.Clid;
                row[ClientAccountProcessDS.LINKENTIYCSID] = processentity.LinkedEntity.Csid;
                row[ClientAccountProcessDS.LINKEDENTITYTYPE] = processentity.LinkedEntityType.ToString();
                row[ClientAccountProcessDS.TASKID] = processentity.TaskID;
                row[ClientAccountProcessDS.MODELID] = processentity.ModelID;
                row[ClientAccountProcessDS.ASSETID] = processentity.AssetID;
                var account = Broker.GetCMImplementation(processentity.LinkedEntity.Clid,
                                                            processentity.LinkedEntity.Csid);

                if (organization != null)
                {
                    var model = organization.Model.FirstOrDefault(ss => ss.ID == processentity.ModelID);
                    if (model != null)
                    {
                        row[ClientAccountProcessDS.MODEL] = model.Name;
                        row[ClientAccountProcessDS.SERVICETYPE] = model.ServiceType.GetDescription();

                    }


                    var asset = organization.Assets.FirstOrDefault(ss => ss.ID == processentity.AssetID);
                    if (asset != null)
                    {
                        row[ClientAccountProcessDS.ASSET] = asset.Name;
                    }
                    var product = organization.Products.FirstOrDefault(ss => ss.ID == processentity.TaskID);
                    if (product != null)
                    {
                        var fundID = product.FundAccounts.FirstOrDefault();
                        row[ClientAccountProcessDS.PRODUCT] = product.Name;
                        row[ClientAccountProcessDS.TASK] = product.TaskDescription;

                        if (processentity.LinkedEntityType == OrganizationType.ManagedInvestmentSchemesAccount)
                        {
                            var misaccount = Broker.GetCMImplementation(product.EntityId.Clid, product.EntityId.Csid) as ManagedInvestmentSchemesAccountCM;
                            if (misaccount != null)
                            {
                                row[ClientAccountProcessDS.CID] = misaccount.CID;
                                var fund = misaccount.ManagedInvestmentSchemesAccountEntity.FundAccounts.FirstOrDefault(ss => ss.ID
                                    == fundID);
                                if (fund != null && processentity.LinkedEntity != null && processentity.LinkedEntity.Clid != Guid.Empty && processentity.LinkedEntity.Csid != Guid.Empty)
                                {
                                    row[ClientAccountProcessDS.LINKENTITYFUNDCODE] = fund.Code;
                                    row[ClientAccountProcessDS.LINKENTIYFUNDID] = fundID;
                                }
                                Broker.ReleaseBrokerManagedComponent(misaccount);
                            }
                        }
                        else
                        {
                            row[ClientAccountProcessDS.LINKENTIYFUNDID] = product.FundAccounts.FirstOrDefault();
                        }


                        //add product securities
                        if (processentity.LinkedEntityType == OrganizationType.DesktopBrokerAccount)
                        {
                            var productSecurity = organization.ProductSecurities.FirstOrDefault(ss => ss.ID == product.ProductSecuritiesId);
                            if (productSecurity != null)
                            {
                                foreach (var prosec in productSecurity.Details)
                                {
                                    var secutiry = organization.Securities.FirstOrDefault(ss => ss.ID == prosec.SecurityCodeId);
                                    if (secutiry != null)
                                    {
                                        var secuirtyRow = ds.Tables[ClientAccountProcessDS.PRODUCTSECURITIES_TABLE].NewRow();
                                        secuirtyRow[ClientAccountProcessDS.TASKID] = processentity.TaskID;
                                        secuirtyRow[ClientAccountProcessDS.MODELID] = processentity.ModelID;
                                        secuirtyRow[ClientAccountProcessDS.ASSETID] = processentity.AssetID;
                                        secuirtyRow[ClientAccountProcessDS.INVESTMENTCODE] = secutiry.AsxCode;
                                        ds.Tables[ClientAccountProcessDS.PRODUCTSECURITIES_TABLE].Rows.Add(secuirtyRow);
                                    }
                                }
                            }
                            else
                            {
                                if (account != null)
                                {
                                    var filteredHoldingTransactionsByValuationDate = (account as DesktopBrokerAccountCM).DesktopBrokerAccountEntity.holdingTransactions.Where(holdTrans => holdTrans.TradeDate <= DateTime.Now);
                                    var groupedSecurities = filteredHoldingTransactionsByValuationDate.GroupBy(holdTran => holdTran.InvestmentCode);
                                    foreach (var security in groupedSecurities)
                                    {
                                        var secutiry = organization.Securities.Where(sec => sec.AsxCode == security.Key).FirstOrDefault();
                                        if (secutiry != null)
                                        {
                                            var secuirtyRow = ds.Tables[ClientAccountProcessDS.PRODUCTSECURITIES_TABLE].NewRow();
                                            secuirtyRow[ClientAccountProcessDS.TASKID] = processentity.TaskID;
                                            secuirtyRow[ClientAccountProcessDS.MODELID] = processentity.ModelID;
                                            secuirtyRow[ClientAccountProcessDS.ASSETID] = processentity.AssetID;
                                            secuirtyRow[ClientAccountProcessDS.INVESTMENTCODE] = secutiry.AsxCode;
                                            ds.Tables[ClientAccountProcessDS.PRODUCTSECURITIES_TABLE].Rows.Add(secuirtyRow);
                                        }
                                    }
                                }
                            }
                        }

                    }

                }


                if (account != null)
                {
                    row[ClientAccountProcessDS.CID] = account.CID;
                    row[ClientAccountProcessDS.STATUS] = (account as OrganizationUnitCM).OrganizationStatus;

                    switch (processentity.LinkedEntityType)
                    {
                        case OrganizationType.DesktopBrokerAccount:
                            row[ClientAccountProcessDS.ACCOUNTNUMBER] = (account as DesktopBrokerAccountCM).DesktopBrokerAccountEntity.AccountNumber;
                            break;
                        case OrganizationType.BankAccount:
                            row[ClientAccountProcessDS.INSTITUTEID] = (account as BankAccountCM).BankAccountEntity.InstitutionID;
                            row[ClientAccountProcessDS.INSTITUTENAME] = GetInstitueName(organization, (account as BankAccountCM).BankAccountEntity.InstitutionID);
                            row[ClientAccountProcessDS.ACCOUNTNUMBER] = (account as BankAccountCM).BankAccountEntity.AccountNumber;
                            break;


                        case OrganizationType.TermDepositAccount:
                            row[ClientAccountProcessDS.INSTITUTEID] = (account as BankAccountCM).BankAccountEntity.InstitutionID;
                            row[ClientAccountProcessDS.INSTITUTENAME] = GetInstitueName(organization, (account as BankAccountCM).BankAccountEntity.InstitutionID);
                            if ((account as BankAccountCM).BankAccountEntity.BrokerID != null)
                            {
                                row[ClientAccountProcessDS.BROKERID] = GetInstitueID(organization, (account as BankAccountCM).BankAccountEntity.BrokerID.Name);
                                row[ClientAccountProcessDS.BROKERNAME] = (account as BankAccountCM).BankAccountEntity.BrokerID.Name;
                            }

                            break;


                    }

                    Broker.ReleaseBrokerManagedComponent(account);
                }

                row[ClientAccountProcessDS.COMPLETED] = row[ClientAccountProcessDS.STATUS] != DBNull.Value && row[ClientAccountProcessDS.STATUS].ToString().ToLower().Equals("active");

                if (processentity.LinkedEntityType == OrganizationType.ManagedInvestmentSchemesAccount)
                {
                    if (processentity.LinkedEntity.Clid != Guid.Empty)
                    {
                        row[ClientAccountProcessDS.COMPLETED] = true;
                        row[ClientAccountProcessDS.STATUS] = "Active";
                    }
                }

                ds.Tables[ClientAccountProcessDS.ACCOUNTPROCESS_TABLE].Rows.Add(row);
            }
            if (organization != null)
                Broker.ReleaseBrokerManagedComponent(organization);

        }

        private static string GetInstitueName(IOrganization organization, Guid id)
        {
            string intituteName = string.Empty;
            if (organization != null)
            {
                var inst = organization.Institution.FirstOrDefault(ins => ins.ID == id);
                if (inst != null)
                {
                    intituteName = inst.Name;
                }
            }

            return intituteName;
        }
        private static Guid GetInstitueID(IOrganization organization, string name)
        {
            Guid id = Guid.Empty;
            if (organization != null)
            {
                var inst = organization.Institution.FirstOrDefault(ins => ins.Name.ToLower() == name.ToLower());
                if (inst != null)
                {
                    id = inst.ID;
                }
            }

            return id;
        }

        public static void RemoveAccountProcessAssociation(ClientAccountProcessDS ds, List<AccountProcessTaskEntity> accountprocess)
        {
            if (ds.Tables[ClientAccountProcessDS.ACCOUNTPROCESS_TABLE].Rows.Count > 0)
            {
                var account = accountprocess.FirstOrDefault(ss => ss.ModelID == (Guid)ds.Tables[ClientAccountProcessDS.ACCOUNTPROCESS_TABLE].Rows[0][ClientAccountProcessDS.MODELID] && ss.AssetID == (Guid)ds.Tables[ClientAccountProcessDS.ACCOUNTPROCESS_TABLE].Rows[0][ClientAccountProcessDS.ASSETID] && ss.TaskID == (Guid)ds.Tables[ClientAccountProcessDS.ACCOUNTPROCESS_TABLE].Rows[0][ClientAccountProcessDS.TASKID] && ss.LinkedEntity.Clid == (Guid)ds.Tables[ClientAccountProcessDS.ACCOUNTPROCESS_TABLE].Rows[0][ClientAccountProcessDS.LINKENTIYCLID] && ss.LinkedEntity.Csid == (Guid)ds.Tables[ClientAccountProcessDS.ACCOUNTPROCESS_TABLE].Rows[0][ClientAccountProcessDS.LINKENTIYCSID]);
                if (account != null)
                {
                    account.LinkedEntity = new IdentityCM();
                    account.IsCompleted = false;
                    account.OldStatus = string.Empty;
                }
            }

        }

        public static void AssociateAccountProcess(ClientAccountProcessDS ds, List<AccountProcessTaskEntity> listAccountProcess, List<IdentityCMDetail> bankAccounts, List<IdentityCMDetail> desktopBrokerAccounts, List<IdentityCMDetail> managedInvestmentSchemesAccounts)
        {
            if (ds.Tables[ClientAccountProcessDS.ACCOUNTPROCESS_TABLE].Rows.Count > 0)
            {
                var account = listAccountProcess.FirstOrDefault(ss => ss.ModelID == (Guid)ds.Tables[ClientAccountProcessDS.ACCOUNTPROCESS_TABLE].Rows[0][ClientAccountProcessDS.MODELID] && ss.AssetID == (Guid)ds.Tables[ClientAccountProcessDS.ACCOUNTPROCESS_TABLE].Rows[0][ClientAccountProcessDS.ASSETID] && ss.TaskID == (Guid)ds.Tables[ClientAccountProcessDS.ACCOUNTPROCESS_TABLE].Rows[0][ClientAccountProcessDS.TASKID]);
                if (account != null)
                {
                    account.LinkedEntity = new IdentityCM
                    {
                        Clid = (Guid)ds.Tables[ClientAccountProcessDS.ACCOUNTPROCESS_TABLE].Rows[0][ClientAccountProcessDS.LINKENTIYCLID],
                        Csid = (Guid)ds.Tables[ClientAccountProcessDS.ACCOUNTPROCESS_TABLE].Rows[0][ClientAccountProcessDS.LINKENTIYCSID],
                    };

                    if (account.LinkedEntity.Clid != Guid.Empty && account.LinkedEntity.Csid != Guid.Empty)
                    {
                        switch (account.LinkedEntityType)
                        {
                            case OrganizationType.BankAccount:
                            case OrganizationType.TermDepositAccount:
                                if (bankAccounts.Count(ss => ss.Clid == account.LinkedEntity.Clid && ss.Csid == account.LinkedEntity.Csid) == 0)
                                {
                                    bankAccounts.Add(new IdentityCMDetail { Clid = account.LinkedEntity.Clid, Csid = account.LinkedEntity.Csid });
                                }
                                break;
                            case OrganizationType.DesktopBrokerAccount:
                                if (desktopBrokerAccounts.Count(ss => ss.Clid == account.LinkedEntity.Clid && ss.Csid == account.LinkedEntity.Csid) == 0)
                                {
                                    desktopBrokerAccounts.Add(new IdentityCMDetail { Clid = account.LinkedEntity.Clid, Csid = account.LinkedEntity.Csid });
                                }
                                break;
                            case OrganizationType.ManagedInvestmentSchemesAccount:

                                var fundid = (Guid)ds.Tables[ClientAccountProcessDS.ACCOUNTPROCESS_TABLE].Rows[0][ClientAccountProcessDS.LINKENTIYFUNDID];
                                if (managedInvestmentSchemesAccounts.Count(ss => ss.Clid == account.LinkedEntity.Clid && ss.Csid == account.LinkedEntity.Csid && ss.FundID == fundid) == 0)
                                {
                                    managedInvestmentSchemesAccounts.Add(new IdentityCMDetail { Clid = account.LinkedEntity.Clid, Csid = account.LinkedEntity.Csid, FundID = fundid });
                                }
                                break;
                        }
                    }
                }
            }
        }


        public static void AssociateASXAccount(DesktopBrokerAccountDS ds, List<IdentityCMDetail> desktopBrokerAccounts)
        {
            if (ds.Tables[ds.DesktopBrokerAccountsTable.TABLENAME].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[ds.DesktopBrokerAccountsTable.TABLENAME].Rows)
                {
                    var clid = new Guid(dr[ds.DesktopBrokerAccountsTable.CLID].ToString());
                    var csid = new Guid(dr[ds.DesktopBrokerAccountsTable.CSID].ToString());
                    var asxAccount = desktopBrokerAccounts.FirstOrDefault(ss => ss.Clid == new Guid(dr[ds.DesktopBrokerAccountsTable.CLID].ToString()) && ss.Csid == new Guid(dr[ds.DesktopBrokerAccountsTable.CSID].ToString()));
                    if (asxAccount == null)
                    {
                        if (desktopBrokerAccounts == null)
                        {
                            desktopBrokerAccounts = new List<IdentityCMDetail>();
                        }
                        desktopBrokerAccounts.Add(new IdentityCMDetail { Clid = clid, Csid = csid });

                    }
                }
            }
        }

        public static void RemoveASXAccount(DesktopBrokerAccountDS ds, List<IdentityCMDetail> desktopBrokerAccounts, List<AccountProcessTaskEntity> accountProcess, ICMBroker broker)
        {
            if (ds.Tables[ds.DesktopBrokerAccountsTable.TABLENAME].Rows.Count > 0)
            {
                var asxAccount = desktopBrokerAccounts.FirstOrDefault(ss => ss.Clid == (Guid)ds.Tables[ds.DesktopBrokerAccountsTable.TABLENAME].Rows[0][ds.DesktopBrokerAccountsTable.CLID] && ss.Csid == (Guid)ds.Tables[ds.DesktopBrokerAccountsTable.TABLENAME].Rows[0][ds.DesktopBrokerAccountsTable.CSID]);
                if (asxAccount != null)
                {
                    if (!IsAttachToAccountProcess(asxAccount, accountProcess, OrganizationType.DesktopBrokerAccount, broker))
                    {
                        desktopBrokerAccounts.Remove(asxAccount);
                        ds.ExtendedProperties.Add("Message", "Account has been deleted successfully");
                    }
                    else
                    {
                        ds.ExtendedProperties.Add("Message", "This account is associated with the current client. First remove the association from Account Process then try again.");
                    }
                }
            }
        }

        public static void SetParentChild(string data, IdentityCM Parent, ICMBroker broker, Guid clientCLID, Guid clientCSID, Guid clientCID)
        {
            IdentityCM parent = GetParent(data);

            if (parent != IdentityCM.Null)
            {
                if (parent.Cid != Guid.Empty)
                {
                    AdvisorCM advCM = broker.GetBMCInstance(parent.Cid) as AdvisorCM;
                    IdentityCM child = advCM.Children.Where(ch => ch.Cid == clientCID).FirstOrDefault();
                    if (child == null)
                    {
                        child = advCM.Children.Where(ch => ch.Csid == clientCSID && ch.Clid == clientCLID).FirstOrDefault();

                        //if (child == null)
                        //    advCM.Children.Add(new IdentityCM(clientCID));
                        if (child != null)
                            throw new Exception("Client is already part of Adviser");
                    }
                    else
                        throw new Exception("Client is already part of Adviser");
                }
            }


            Parent = parent;
        }

        public static void RemoveParentChild(string data, IdentityCM Parent, ICMBroker broker, Guid clientCLID, Guid clientCSID, Guid clientCID)
        {
            if (Parent != IdentityCM.Null)
            {
                List<IdentityCM> markedforDeletion = new List<IdentityCM>();
                if (Parent.Cid != Guid.Empty)
                {
                    AdvisorCM advCM = broker.GetBMCInstance(Parent.Cid) as AdvisorCM;
                    var childList = advCM.Children.Where(ch => ch.Cid == clientCID);
                    foreach (IdentityCM child in childList)
                    {
                        if (child == null)
                        {
                            var childObj = advCM.Children.Where(ch => ch.Csid == clientCSID && ch.Clid == clientCLID).FirstOrDefault();
                            if (childObj != null)
                                markedforDeletion.Add(childObj);
                        }
                        else
                            markedforDeletion.Add(child);
                    }

                    foreach (IdentityCM deletedChild in markedforDeletion)
                    {
                        advCM.Children.Remove(deletedChild);
                    }
                }
            }

            Parent = IdentityCM.Null;
        }

        public static IdentityCM GetParent(string data)
        {
            return data.ToIdentity();
        }

        public static void RemoveDistAccount(ClientDistributionsDS ds, ObservableCollection<DistributionIncomeEntity> distIncEntity)
        {
            if (ds.Tables[ClientDistributionsDS.CLIENTDISTRIBUTIONTABLE].Rows.Count > 0)
            {
                var objAccount = distIncEntity.FirstOrDefault(ss => ss.ID ==
                    (Guid)ds.Tables[ClientDistributionsDS.CLIENTDISTRIBUTIONTABLE].Rows[0]["ID"]);
                if (objAccount != null)
                {
                    distIncEntity.Remove(objAccount);
                }
            }
        }

        public static void AssociateTermDepositAccount(TermDepositDS ds, List<IdentityCMDetail> TermDepositAccounts)
        {
            if (ds.Tables[ds.TermDepositTable.TABLENAME].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[ds.TermDepositTable.TABLENAME].Rows)
                {
                    var clid = new Guid(dr[ds.TermDepositTable.CLID].ToString());
                    var csid = new Guid(dr[ds.TermDepositTable.CSID].ToString());
                    var termdepositAccount = TermDepositAccounts.FirstOrDefault(ss => ss.Clid == new Guid(dr[ds.TermDepositTable.CLID].ToString()) && ss.Csid == new Guid(dr[ds.TermDepositTable.CSID].ToString()));
                    if (termdepositAccount == null)
                    {
                        if (TermDepositAccounts == null)
                        {
                            TermDepositAccounts = new List<IdentityCMDetail>();
                        }
                        TermDepositAccounts.Add(new IdentityCMDetail { Clid = clid, Csid = csid });

                    }
                }
            }
        }

        public static void RemoveTermDepositAccount(TermDepositDS ds, List<IdentityCMDetail> TermDepositAccounts)
        {

            if (ds.Tables[ds.TermDepositTable.TABLENAME].Rows.Count > 0)
            {
                if (TermDepositAccounts != null)
                {
                    var asxAccount =
                        TermDepositAccounts.FirstOrDefault(
                            ss =>
                            ss.Clid == (Guid)ds.Tables[ds.TermDepositTable.TABLENAME].Rows[0][ds.TermDepositTable.CLID] &&
                            ss.Csid == (Guid)ds.Tables[ds.TermDepositTable.TABLENAME].Rows[0][ds.TermDepositTable.CSID]);
                    if (asxAccount != null)
                    {
                        TermDepositAccounts.Remove(asxAccount);
                    }
                }
            }
        }

        public static void AssociateBankAccount(BankAccountDS ds, List<IdentityCMDetail> bankAccounts)
        {
            if (ds.Tables[ds.BankAccountsTable.TABLENAME].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[ds.BankAccountsTable.TABLENAME].Rows)
                {
                    var clid = new Guid(dr[ds.BankAccountsTable.CLID].ToString());
                    var csid = new Guid(dr[ds.BankAccountsTable.CSID].ToString());
                    var asxAccount = bankAccounts.FirstOrDefault(ss => ss.Clid == new Guid(dr[ds.BankAccountsTable.CLID].ToString()) && ss.Csid == new Guid(dr[ds.BankAccountsTable.CSID].ToString()));
                    if (asxAccount == null)
                    {
                        if (bankAccounts == null)
                        {
                            bankAccounts = new List<IdentityCMDetail>();
                        }
                        bankAccounts.Add(new IdentityCMDetail { Clid = clid, Csid = csid });

                    }
                }
            }
        }

        public static void RemoveBankAccount(BankAccountDS ds, List<IdentityCMDetail> bankAccounts, List<AccountProcessTaskEntity> accountProcess, ICMBroker broker)
        {
            if (ds.Tables[ds.BankAccountsTable.TABLENAME].Rows.Count > 0)
            {
                var clid = (Guid)ds.Tables[ds.BankAccountsTable.TABLENAME].Rows[0][ds.BankAccountsTable.CLID];
                var csid = (Guid)ds.Tables[ds.BankAccountsTable.TABLENAME].Rows[0][ds.BankAccountsTable.CSID];

                //Remove Bank Account
                var bank = bankAccounts.FirstOrDefault(ss => ss.Clid == clid && ss.Csid == csid);
                if (bank != null)
                {
                    if (!IsAttachToAccountProcess(bank, accountProcess, OrganizationType.BankAccount, broker))
                    {
                        bankAccounts.Remove(bank);
                        ds.ExtendedProperties.Add("Message", "Account has been deleted successfully");
                    }
                    else
                    {
                        ds.ExtendedProperties.Add("Message", "This account is associated with the current client. First remove the association from Account Process then try again.");
                    }
                }
            }
        }

        public static bool IsAttachToAccountProcess(IdentityCMDetail account, List<AccountProcessTaskEntity> accountProcess, OrganizationType type, ICMBroker broker)
        {
            bool result = false;
            var CheckAssociation = accountProcess.Where(ss => ss.LinkedEntity.Clid == account.Clid && ss.LinkedEntity.Csid == account.Csid && ss.LinkedEntityType == type);
            if (CheckAssociation.Count() > 0)
            {


                if (type == OrganizationType.ManagedInvestmentSchemesAccount)
                {
                    var org = broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                    foreach (AccountProcessTaskEntity accountProcessTaskEntity in CheckAssociation)
                    {
                        var model = org.Model.FirstOrDefault(mm => mm.ID == accountProcessTaskEntity.ModelID);
                        if (model != null)
                        {
                            var asset = model.Assets.FirstOrDefault(ss => ss.ID == accountProcessTaskEntity.AssetID);
                            if (asset != null)
                            {

                                var product = asset.Products.FirstOrDefault(pr => pr.ID == accountProcessTaskEntity.TaskID);

                                if (product != null)
                                {
                                    result = product.FundAccounts.Count(ss => ss == account.FundID) > 0;

                                    if (result)//found
                                        break;
                                }
                            }

                        }


                    }
                    broker.ReleaseBrokerManagedComponent(org);
                }
                else
                {
                    result = true;

                }
            }
            return result;
        }

        public static void RemoveAssociationFromAccountProcess(Guid clid, Guid csid, List<AccountProcessTaskEntity> accountProcess)
        {
            //Remove Bank Account Association from Account Process
            var accountList = accountProcess.Where(ss => ss.LinkedEntity.Clid == clid && ss.LinkedEntity.Csid == csid).ToList();
            if (accountList.Count() > 0)
            {
                foreach (var accountEntity in accountList)
                {
                    accountEntity.LinkedEntity = new IdentityCM();
                    accountEntity.IsCompleted = false;
                    accountEntity.OldStatus = string.Empty;
                }
            }
        }

        public static void FillModelInfo(ClientModelsDetailsDS modelDs, Common.ServiceType serviceType, ICMBroker Broker, string ClientID, Guid ClientCid, string ClientName)
        {
            modelDs.DoItForMe = serviceType.DO_IT_FOR_ME;
            modelDs.DoItWithMe = serviceType.DO_IT_WITH_ME;
            modelDs.DoItYourself = serviceType.DO_IT_YOURSELF;
            DataRow dr;
            #region do it with me

            if (serviceType.DoItForMe != null)
            {
                if (serviceType.DO_IT_FOR_ME && serviceType.DoItForMe != null)
                    modelDs.ClientModelsTable.AddRow(ClientCid, ClientID, ClientName, serviceType.DoItForMe.ProgramCode, serviceType.DoItForMe.ProgramName, ServiceTypes.DoItForMe.ToString());
                if (serviceType.DO_IT_WITH_ME && serviceType.DoItWithMe != null)
                    modelDs.ClientModelsTable.AddRow(ClientCid, ClientID, ClientName, serviceType.DoItWithMe.ProgramCode, serviceType.DoItWithMe.ProgramName, ServiceTypes.DoItWithMe.ToString());
                if (serviceType.DO_IT_YOURSELF && serviceType.DoItYourSelf != null)
                    modelDs.ClientModelsTable.AddRow(ClientCid, ClientID, ClientName, serviceType.DoItYourSelf.ProgramCode, serviceType.DoItYourSelf.ProgramName, ServiceTypes.DoItYourSelf.ToString());
                dr = modelDs.DoItForMeTable.NewRow();
                dr[modelDs.DoItForMeTable.PROGRAMCODE] = serviceType.DoItForMe.ProgramCode;
                dr[modelDs.DoItForMeTable.PROGRAMNAME] = serviceType.DoItForMe.ProgramName;
                dr[modelDs.DoItForMeTable.HIN] = serviceType.DoItForMe.HIN;
                dr[modelDs.DoItForMeTable.RETAILINVESTOR] = serviceType.DoItForMe.RetailInvestor;
                dr[modelDs.DoItForMeTable.SPONSORINGBROKER] = serviceType.DoItForMe.SponsoringBroker;
                dr[modelDs.DoItForMeTable.WHOLESALEINVESTOR] = serviceType.DoItForMe.WholesaleInvestor;
                dr[modelDs.DoItForMeTable.AUTHORIASTIONTYPE] = serviceType.DoItForMe.AuthoriastionType;
                modelDs.DoItForMeTable.Rows.Add(dr);
            }

            #endregion

            #region do it with me
            if (serviceType.DoItWithMe != null)
            {
                dr = modelDs.DoItWithMeTable.NewRow();

                dr[modelDs.DoItWithMeTable.PROGRAMNAME] = serviceType.DoItWithMe.ProgramName;
                dr[modelDs.DoItWithMeTable.PROGRAMCODE] = serviceType.DoItWithMe.ProgramCode;
                dr[modelDs.DoItWithMeTable.AUTHORIASTIONTYPE] = serviceType.DoItWithMe.AuthoriastionType;

                if (serviceType.DoItWithMe.DoItYourSelf != null)
                {

                    dr[modelDs.DoItWithMeTable.DOITYOURSELF_AUTHORIASTIONTYPE] = serviceType.DoItWithMe.DoItYourSelf.AuthoriastionType;
                    dr[modelDs.DoItWithMeTable.DOITYOURSELF_ASX] = serviceType.DoItWithMe.DoItYourSelf.ASX;
                    dr[modelDs.DoItWithMeTable.DOITYOURSELF_TDS] = serviceType.DoItWithMe.DoItYourSelf.TDS;
                    dr[modelDs.DoItWithMeTable.DOITYOURSELF_DP] = serviceType.DoItWithMe.DoItYourSelf.DP;
                    dr[modelDs.DoItWithMeTable.DOITYOURSELF_OTHER] = serviceType.DoItWithMe.DoItYourSelf.Other;

                }
                modelDs.DoItWithMeTable.Rows.Add(dr);
            }
            #endregion

            #region do it yourslef
            if (serviceType.DoItYourSelf != null)
            {
                dr = modelDs.DoItYourselfTable.NewRow();

                dr[modelDs.DoItYourselfTable.AUTHORIASTIONTYPE] = serviceType.DoItYourSelf.AuthoriastionType;
                dr[modelDs.DoItYourselfTable.ASX] = serviceType.DoItYourSelf.ASX;
                dr[modelDs.DoItYourselfTable.TDS] = serviceType.DoItYourSelf.TDS;
                dr[modelDs.DoItYourselfTable.DP] = serviceType.DoItYourSelf.DP;
                dr[modelDs.DoItYourselfTable.OTHER] = serviceType.DoItYourSelf.Other;
                dr[modelDs.DoItYourselfTable.PROGRAMCODE] = serviceType.DoItYourSelf.ProgramCode;
                dr[modelDs.DoItYourselfTable.PROGRAMNAME] = serviceType.DoItYourSelf.ProgramName;
                modelDs.DoItYourselfTable.Rows.Add(dr);
            }
            #endregion

        }

        public static void SetClientlModelsInfo(ClientModelsDetailsDS modelDs, Common.ServiceType serviceType, Oritax.TaxSimp.Data.ServiceType orgServiceType, List<AccountProcessTaskEntity> listAccountProcess, ICMBroker Broker, List<IdentityCMDetail> misAccounts)
        {
            var orgnization = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            var models = orgnization.Model;
            Broker.ReleaseBrokerManagedComponent(orgnization);

            if (serviceType == null)
            {
                serviceType = new ServiceType();
            }
            serviceType.DO_IT_FOR_ME = modelDs.DoItForMe;
            serviceType.DO_IT_WITH_ME = modelDs.DoItWithMe;
            serviceType.DO_IT_YOURSELF = modelDs.DoItYourself;

            orgServiceType.DO_IT_FOR_ME = modelDs.DoItForMe;
            orgServiceType.DO_IT_WITH_ME = modelDs.DoItWithMe;
            orgServiceType.DO_IT_YOURSELF = modelDs.DoItYourself;

            DataRow dr;

            #region do it for me

            if (modelDs.DoItForMeTable.Rows.Count > 0)
            {
                dr = modelDs.DoItForMeTable.Rows[0];
                if (serviceType.DoItForMe == null)
                {
                    serviceType.DoItForMe = new DoItForMe();
                }

                if (dr[modelDs.DoItForMeTable.PROGRAMCODE] != DBNull.Value)
                {
                    serviceType.DoItForMe.ProgramCode = dr[modelDs.DoItForMeTable.PROGRAMCODE].ToString();
                }

                if (dr[modelDs.DoItForMeTable.PROGRAMNAME] != DBNull.Value)
                {
                    serviceType.DoItForMe.ProgramName = dr[modelDs.DoItForMeTable.PROGRAMNAME].ToString();
                }

                if (dr[modelDs.DoItForMeTable.HIN] != DBNull.Value)
                {
                    serviceType.DoItForMe.HIN = dr[modelDs.DoItForMeTable.HIN].ToString();
                }

                if (dr[modelDs.DoItForMeTable.RETAILINVESTOR] != DBNull.Value)
                {
                    serviceType.DoItForMe.RetailInvestor = bool.Parse(dr[modelDs.DoItForMeTable.RETAILINVESTOR].ToString());
                }

                if (dr[modelDs.DoItForMeTable.SPONSORINGBROKER] != DBNull.Value)
                {
                    serviceType.DoItForMe.SponsoringBroker = dr[modelDs.DoItForMeTable.SPONSORINGBROKER].ToString();
                }

                if (dr[modelDs.DoItForMeTable.WHOLESALEINVESTOR] != DBNull.Value)
                {
                    serviceType.DoItForMe.WholesaleInvestor = bool.Parse(dr[modelDs.DoItForMeTable.WHOLESALEINVESTOR].ToString());
                }

                if (dr[modelDs.DoItForMeTable.AUTHORIASTIONTYPE] != DBNull.Value)
                {
                    serviceType.DoItForMe.AuthoriastionType = dr[modelDs.DoItForMeTable.AUTHORIASTIONTYPE].ToString();
                }

            }
            else
            {
                serviceType.DoItForMe = null;
            }

            #endregion

            #region do it with me

            if (modelDs.DoItWithMeTable.Rows.Count > 0)
            {
                dr = dr = modelDs.DoItWithMeTable.Rows[0];
                if (serviceType.DoItWithMe == null)
                {
                    serviceType.DoItWithMe = new DoItWithMe();
                }
                if (serviceType.DoItWithMe.DoItYourSelf == null)
                {
                    serviceType.DoItWithMe.DoItYourSelf = new DoItYourSelf() { };
                }

                if (dr[modelDs.DoItWithMeTable.DOITYOURSELF_AUTHORIASTIONTYPE] != DBNull.Value)
                {
                    serviceType.DoItWithMe.DoItYourSelf.AuthoriastionType = dr[modelDs.DoItWithMeTable.DOITYOURSELF_AUTHORIASTIONTYPE].ToString();
                }

                if (dr[modelDs.DoItWithMeTable.DOITYOURSELF_ASX] != DBNull.Value)
                {
                    serviceType.DoItWithMe.DoItYourSelf.ASX =
                        bool.Parse(dr[modelDs.DoItWithMeTable.DOITYOURSELF_ASX].ToString());
                }

                if (dr[modelDs.DoItWithMeTable.DOITYOURSELF_TDS] != DBNull.Value)
                {
                    serviceType.DoItWithMe.DoItYourSelf.TDS =
                        bool.Parse(dr[modelDs.DoItWithMeTable.DOITYOURSELF_TDS].ToString());
                }

                if (dr[modelDs.DoItWithMeTable.DOITYOURSELF_DP] != DBNull.Value)
                {
                    serviceType.DoItWithMe.DoItYourSelf.DP =
                        bool.Parse(dr[modelDs.DoItWithMeTable.DOITYOURSELF_DP].ToString());
                }


                if (dr[modelDs.DoItWithMeTable.DOITYOURSELF_OTHER] != DBNull.Value)
                {
                    serviceType.DoItWithMe.DoItYourSelf.Other = dr[modelDs.DoItWithMeTable.DOITYOURSELF_OTHER].ToString();
                }

                if (dr[modelDs.DoItWithMeTable.PROGRAMCODE] != DBNull.Value)
                {
                    serviceType.DoItWithMe.DoItYourSelf.ProgramCode = dr[modelDs.DoItWithMeTable.PROGRAMCODE].ToString();

                    var model = models.FirstOrDefault(ss => ss.ProgramCode == serviceType.DoItWithMe.ProgramCode);
                    if (model != null)
                    {
                        // have to do this because some of developer have messedup code.
                        var modelString = model.ToXmlString();
                        serviceType.DoItWithMe.DoItYourSelf.Model = modelString.ToNewOrData<Oritax.TaxSimp.Data.ModelEntity>();
                    }
                    else
                    {
                        serviceType.DoItWithMe.DoItYourSelf.Model = null;
                    }


                }
                else
                {
                    serviceType.DoItWithMe.ProgramCode = "";
                    serviceType.DoItWithMe.DoItYourSelf.Model = null;

                }

                if (dr[modelDs.DoItWithMeTable.PROGRAMNAME] != DBNull.Value)
                {
                    serviceType.DoItWithMe.DoItYourSelf.ProgramName = dr[modelDs.DoItWithMeTable.PROGRAMNAME].ToString();
                }

                serviceType.DoItWithMe.Model = serviceType.DoItWithMe.DoItYourSelf.Model;
                serviceType.DoItWithMe.ProgramCode = serviceType.DoItWithMe.DoItYourSelf.ProgramCode;
                serviceType.DoItWithMe.ProgramName = serviceType.DoItWithMe.DoItYourSelf.ProgramName;

                if (dr[modelDs.DoItWithMeTable.AUTHORIASTIONTYPE] != DBNull.Value)
                    serviceType.DoItWithMe.AuthoriastionType = dr[modelDs.DoItWithMeTable.AUTHORIASTIONTYPE].ToString();

            }
            else
            {
                serviceType.DoItWithMe = null;
            }

            #endregion

            #region do it yourslef

            if (modelDs.DoItYourselfTable.Rows.Count > 0)
            {
                dr = dr = modelDs.DoItYourselfTable.Rows[0];
                if (serviceType.DoItYourSelf == null)
                {
                    serviceType.DoItYourSelf = new DoItYourSelf();
                }

                if (dr[modelDs.DoItYourselfTable.AUTHORIASTIONTYPE] != DBNull.Value)
                {
                    serviceType.DoItYourSelf.AuthoriastionType =
                        dr[modelDs.DoItYourselfTable.AUTHORIASTIONTYPE].ToString();
                }
                if (dr[modelDs.DoItYourselfTable.ASX] != DBNull.Value)
                {
                    serviceType.DoItYourSelf.ASX = bool.Parse(dr[modelDs.DoItYourselfTable.ASX].ToString());
                }
                if (dr[modelDs.DoItYourselfTable.TDS] != DBNull.Value)
                {
                    serviceType.DoItYourSelf.TDS = bool.Parse(dr[modelDs.DoItYourselfTable.TDS].ToString());
                }
                if (dr[modelDs.DoItYourselfTable.DP] != DBNull.Value)
                {
                    serviceType.DoItYourSelf.DP = bool.Parse(dr[modelDs.DoItYourselfTable.DP].ToString());
                }
                if (dr[modelDs.DoItYourselfTable.OTHER] != DBNull.Value)
                {
                    serviceType.DoItYourSelf.Other = dr[modelDs.DoItYourselfTable.OTHER].ToString();
                }

                if (dr[modelDs.DoItYourselfTable.PROGRAMCODE] != DBNull.Value)
                {
                    serviceType.DoItYourSelf.ProgramCode = dr[modelDs.DoItYourselfTable.PROGRAMCODE].ToString();
                    var model = models.FirstOrDefault(ss => ss.ProgramCode == serviceType.DoItYourSelf.ProgramCode);
                    if (model != null)
                    {
                        // have to do this because some of developer have messedup code.
                        var modelString = model.ToXmlString();
                        serviceType.DoItYourSelf.Model = modelString.ToNewOrData<Oritax.TaxSimp.Data.ModelEntity>();
                    }
                    else
                    {
                        serviceType.DoItYourSelf.Model = null;
                    }

                }
                else
                {
                    serviceType.DoItYourSelf.ProgramCode = "";
                    serviceType.DoItYourSelf.Model = null;
                }

                if (dr[modelDs.DoItYourselfTable.PROGRAMNAME] != DBNull.Value)
                {
                    serviceType.DoItYourSelf.ProgramName = dr[modelDs.DoItYourselfTable.PROGRAMNAME].ToString();
                }




            }
            else
            {
                serviceType.DoItYourSelf = null;
            }

            #endregion

            #region Set client Account Process
            List<ModelEntity> accountProcessModels = new List<ModelEntity>();
            if (serviceType.DO_IT_FOR_ME)
            {
                var model = models.FirstOrDefault(ss => ss.ProgramCode == serviceType.DoItForMe.ProgramCode);
                if (model != null)
                {
                    accountProcessModels.Add(model);
                }
            }


            if (serviceType.DO_IT_WITH_ME)
            {
                var model = models.FirstOrDefault(ss => ss.ProgramCode == serviceType.DoItWithMe.ProgramCode);
                if (model != null)
                {
                    accountProcessModels.Add(model);
                }
            }

            if (serviceType.DO_IT_YOURSELF)
            {
                var model = models.FirstOrDefault(ss => ss.ProgramCode == serviceType.DoItYourSelf.ProgramCode);
                if (model != null)
                {
                    accountProcessModels.Add(model);
                }
            }

            List<ModelEntity> oldModels = new List<ModelEntity>();

            var modelByGroup = listAccountProcess.GroupBy(ment => ment.ModelID);

            foreach (var modelItem in modelByGroup)
            {
                ModelEntity oldModel = orgnization.Model.Where(modelEnt => modelEnt.ID == modelItem.Key).FirstOrDefault();
                oldModels.Add(ObjectCopier.Clone(oldModel));
            }

            SetModel(oldModels, accountProcessModels, listAccountProcess, misAccounts);

            #endregion
        }

        public static void SetModel(List<ModelEntity> oldModels, List<ModelEntity> models, List<AccountProcessTaskEntity> listAccountProcess, List<IdentityCMDetail> misAccounts)
        {
            if (models == null) return;

            if (listAccountProcess == null)
                listAccountProcess = new List<AccountProcessTaskEntity>();

            foreach (var model in models)
            {
                var TasksNotInModels = listAccountProcess.Where(ss => ss.ModelID == model.ID).Where(ss => ss.LinkedEntity.Clid == Guid.Empty);

                // task which are not in models,should be removed
                for (int i = TasksNotInModels.Count() - 1; i >= 0; i--)
                    listAccountProcess.Remove(TasksNotInModels.ElementAt(i));

                RemoveDeletedProducts(oldModels, model, listAccountProcess);
                // add/set new Tasks.
                foreach (var asset in model.Assets)
                {
                    foreach (var product in asset.Products)
                    {
                        AccountProcessTaskEntity task = null;

                        task = listAccountProcess.Where(e => e.ModelID == model.ID && e.AssetID == asset.ID && e.TaskID == product.ID).FirstOrDefault();

                        if (task != null)
                            task.LinkedEntityType = product.EntityType;

                        if (task == null)
                        {
                            //add if task doesn't exits, add new entry

                            task = new AccountProcessTaskEntity { ModelID = model.ID, TaskID = product.ID, LinkedEntityType = product.EntityType, AssetID = asset.ID, LinkedEntity = new IdentityCM(), IsCompleted = false, IsExempted = false, OldStatus = "" };
                            listAccountProcess.Add(task);
                        }

                        if (model.ServiceType == ServiceTypes.DoItForMe)
                            IfMisAttachedItAutomatically(product, task, misAccounts);
                    }
                }
            }
           //  remove old model which are not assigned now 
            foreach (var oldModel in oldModels)
            {
                var exists= models.Count(ss => ss.ID == oldModel.ID)>0;
                if(!exists)
                {
                    listAccountProcess.RemoveAll(ss => ss.ModelID == oldModel.ID);
                }
            }

        }

        private static void IfMisAttachedItAutomatically(ProductEntity product, AccountProcessTaskEntity task, List<IdentityCMDetail> misAccounts)
        {
            if (product.EntityType == OrganizationType.ManagedInvestmentSchemesAccount)
            {

                var fundID = product.FundAccounts.FirstOrDefault();
                var entiy = product.EntityId;
                if (fundID != Guid.Empty && entiy != null && entiy != IdentityCM.Null && (task.LinkedEntity == null || task.LinkedEntity == IdentityCM.Null))
                {
                    task.LinkedEntity = new IdentityCM() { Clid = entiy.Clid, Csid = entiy.Csid, Cid = entiy.Cid };
                    task.IsCompleted = true;
                }
                if (misAccounts != null && task.IsCompleted)
                {
                    if (misAccounts.Count(ss => ss.Clid == task.LinkedEntity.Clid && ss.Csid == task.LinkedEntity.Csid && ss.FundID == fundID) <= 0)
                    {
                        misAccounts.Add(new IdentityCMDetail { Clid = task.LinkedEntity.Clid, Csid = task.LinkedEntity.Csid, Cid = task.LinkedEntity.Cid, FundID = fundID });
                    }
                }
            }
        }

        public static void SetClientModels(UpdateClientModelsDS ds, ClientIndividualEntity entity)
        {
            if (entity.Servicetype != null)
            {
                if (entity.ManagedInvestmentSchemesAccounts == null)
                    entity.ManagedInvestmentSchemesAccounts = new List<IdentityCMDetail>();
                UpdateClientModel(ds.ModelEntity, entity.Servicetype, entity.ListAccountProcess, entity.ManagedInvestmentSchemesAccounts);
                PrintServiceModel(ds, entity.Servicetype, entity.ClientId, entity.Name);
            }
        }

        public static void SetClientModels(UpdateClientModelsDS ds, CorporateEntity entity)
        {
            if (entity.Servicetype != null)
            {
                if (entity.ManagedInvestmentSchemesAccounts == null)
                    entity.ManagedInvestmentSchemesAccounts = new List<IdentityCMDetail>();
                UpdateClientModel(ds.ModelEntity, entity.Servicetype, entity.ListAccountProcess, entity.ManagedInvestmentSchemesAccounts);

                PrintServiceModel(ds, entity.Servicetype, entity.ClientId, entity.Name);
            }
        }

        private static void PrintServiceModel(UpdateClientModelsDS ds, ServiceType servicetype, string id, string name)
        {
            string diwm = servicetype.DoItWithMe != null ? "DIWM_" + servicetype.DoItWithMe.ProgramCode : "";
            string difm = servicetype.DoItForMe != null ? "DIFM_" + servicetype.DoItForMe.ProgramCode : "";
            string diy = servicetype.DoItYourSelf != null ? "DIY_" + servicetype.DoItYourSelf.ProgramCode : "";
            Debug.WriteLine(String.Format("{0},{1},{2},{3},{4},{5}", id, name, difm, diwm, diy, ds.ModelEntity.ProgramCode));
        }

        private static void UpdateClientModel(ModelEntity model, ServiceType servicetype, List<AccountProcessTaskEntity> listAccountProcess, List<IdentityCMDetail> misAccounts)
        {
            //update Models in client Entity
            if (model.ServiceType == ServiceTypes.DoItWithMe)
            {
                if (servicetype.DoItWithMe != null)
                {
                    if (servicetype.DoItWithMe.ProgramCode == model.ProgramCode)
                    {
                        servicetype.DoItWithMe.Model = servicetype.DoItWithMe.DoItYourSelf.Model = model;
                    }
                }
            }

            else if (model.ServiceType == ServiceTypes.DoItYourSelf)
            {
                if (servicetype.DoItYourSelf != null)
                {
                    if (servicetype.DoItYourSelf.ProgramCode == model.ProgramCode)
                    {
                        servicetype.DoItWithMe.DoItYourSelf.Model = model;
                    }
                }
            }


            if (listAccountProcess != null)
            {
                if (listAccountProcess.Count(task => task.ModelID == model.ID) > 0)
                {
                    RemoveDeletedProducts(model, listAccountProcess);

                    foreach (var asset in model.Assets)
                    {
                        foreach (var product in asset.Products)
                        {
                            AccountProcessTaskEntity task = listAccountProcess.Where(e => e.ModelID == model.ID && e.AssetID == asset.ID && e.TaskID == product.ID).FirstOrDefault();

                            if (task != null)
                                task.LinkedEntityType = product.EntityType;

                            if (task == null)
                            {
                                //add if task doesn't exits, add new entry
                                task = new AccountProcessTaskEntity { ModelID = model.ID, TaskID = product.ID, LinkedEntityType = product.EntityType, AssetID = asset.ID, LinkedEntity = new IdentityCM(), IsCompleted = false, IsExempted = false, OldStatus = "" };
                                listAccountProcess.Add(task);
                            }

                            if (model.ServiceType == ServiceTypes.DoItForMe)
                                IfMisAttachedItAutomatically(product, task, misAccounts);
                        }
                    }
                }
            }
        }

        private static void RemoveDeletedProducts(List<ModelEntity> oldModels, ModelEntity model, List<AccountProcessTaskEntity> listAccountProcess)
        {
            var oldModelEntity = oldModels.Where(e => e.ID == model.ID).FirstOrDefault();
            List<AccountProcessTaskEntity> removableTaks = new List<AccountProcessTaskEntity>();

            if (oldModelEntity == null)
            {
                //It means the new coming model is not the same as Old hence we need to massage and transfer tasks properly
                var oldModelEntityByServiceType = oldModels.Where(e => e.ServiceType == model.ServiceType).FirstOrDefault();
                if (oldModelEntityByServiceType != null)
                {
                    var fiterListAccountProcessByFoundModel = listAccountProcess.Where(lap => lap.ModelID == oldModelEntityByServiceType.ID);
                    #region Some bug here 
                    // seems this For each will not do something. because for each after this loop will remove all entries after all.
                    foreach (var assetItem in model.Assets)
                    {
                        foreach (var proItem in assetItem.Products)
                        {
                            var existingAccountProcess = fiterListAccountProcessByFoundModel.Where(t => t.TaskID == proItem.ID).FirstOrDefault();
                            if (existingAccountProcess != null)
                            {
                                existingAccountProcess.ModelID = model.ID;
                                existingAccountProcess.LinkedEntityType = proItem.EntityType;
                            }
                        }
                    }
 

                    foreach (var taskItem in fiterListAccountProcessByFoundModel)
                        removableTaks.Add(taskItem);
                    #endregion
                }
            }

            //remove old tasks that are not in model
            var modeltasks = listAccountProcess.Where(task => task.ModelID == model.ID);

            foreach (var accountProcessTaskEntity in modeltasks)
            {
                bool productFound = false;
                foreach (var asset in model.Assets)
                {
                    if (accountProcessTaskEntity.AssetID == asset.ID)
                    {
                        foreach (var product in asset.Products)
                        {
                            if (product.ID == accountProcessTaskEntity.TaskID)
                                productFound = true;
                        }
                    }

                    if (productFound)
                        break;
                }

                if (!productFound)
                    removableTaks.Add(accountProcessTaskEntity);
            }


            foreach (var accountProcessTaskEntity in removableTaks)
                listAccountProcess.Remove(accountProcessTaskEntity);
        }

        private static void RemoveDeletedProducts(ModelEntity model, List<AccountProcessTaskEntity> listAccountProcess)
        {
            List<AccountProcessTaskEntity> removableTaks = new List<AccountProcessTaskEntity>();

            //remove old tasks that are not in model
            var modeltasks = listAccountProcess.Where(task => task.ModelID == model.ID);

            foreach (var accountProcessTaskEntity in modeltasks)
            {
                bool productFound = false;
                foreach (var asset in model.Assets)
                {
                    if (accountProcessTaskEntity.AssetID == asset.ID)
                    {
                        foreach (var product in asset.Products)
                        {
                            if (product.ID == accountProcessTaskEntity.TaskID)
                                productFound = true;
                        }
                    }

                    if (productFound)
                        break;
                }

                if (!productFound)
                    removableTaks.Add(accountProcessTaskEntity);
            }


            foreach (var accountProcessTaskEntity in removableTaks)
                listAccountProcess.Remove(accountProcessTaskEntity);
        }

        public static void GetOtherServices(OtherServicesDS otherServicesDs, CorporateEntity entity)
        {
            GetOtherServices(otherServicesDs, entity.BTWrap.AccountNumber, entity.BTWrap.AccountDescription, entity.ExemptionCategory.Category1, entity.ExemptionCategory.Category2, entity.ExemptionCategory.Category3, entity.OperationManner.ANY_ONE_OF_US_TO_SIGN, entity.OperationManner.ANY_TWO_OF_US_TO_SIGN, entity.OperationManner.ALL_OF_US_TO_SIGN, entity.AccessFacilities.PHONEACCESS, entity.AccessFacilities.ONLINEACCESS, entity.AccessFacilities.DEBITCARD, entity.AccessFacilities.CHEQUEBOOK, entity.AccessFacilities.DEPOSITBOOK, entity.BusinessGroup);
        }

        public static void GetOtherServices(OtherServicesDS otherServicesDs, ClientIndividualEntity entity)
        {
            GetOtherServices(otherServicesDs, entity.BTWrap.AccountNumber, entity.BTWrap.AccountDescription, entity.ExemptionCategory.Category1, entity.ExemptionCategory.Category2, entity.ExemptionCategory.Category3, entity.OperationManner.ANY_ONE_OF_US_TO_SIGN, entity.OperationManner.ANY_TWO_OF_US_TO_SIGN, entity.OperationManner.ALL_OF_US_TO_SIGN, entity.AccessFacilities.PHONEACCESS, entity.AccessFacilities.ONLINEACCESS, entity.AccessFacilities.DEBITCARD, entity.AccessFacilities.CHEQUEBOOK, entity.AccessFacilities.DEPOSITBOOK, entity.BusinessGroup);
        }

        private static void GetOtherServices(OtherServicesDS otherServicesDs, string accountNumber, string accountdesc, bool catergory1, bool Category2, bool Category3, bool ANY_ONE_OF_US_TO_SIGN, bool ANY_TWO_OF_US_TO_SIGN, bool ALL_OF_US_TO_SIGN, bool PHONEACCESS, bool ONLINEACCESS, bool DEBITCARD, bool CHEQUEBOOK, bool DEPOSITBOOK, Guid BusinessGroup)
        {
            DataRow dr = otherServicesDs.Tables[otherServicesDs.TABLENAME].NewRow();

            dr[otherServicesDs.ACCOUNTNUMBER] = accountNumber;
            dr[otherServicesDs.ACCOUNTDESCRIPTION] = accountdesc;
            dr[otherServicesDs.CATEGORY1] = catergory1;
            dr[otherServicesDs.CATEGORY2] = Category2;
            dr[otherServicesDs.CATEGORY3] = Category3;
            dr[otherServicesDs.ANYONEOFUSTOSIGN] = ANY_ONE_OF_US_TO_SIGN;
            dr[otherServicesDs.ANYTWOOFUSTOSIGN] = ANY_TWO_OF_US_TO_SIGN;
            dr[otherServicesDs.ALLOFUSTOSIGN] = ALL_OF_US_TO_SIGN;
            dr[otherServicesDs.PHONEACCESS] = PHONEACCESS;
            dr[otherServicesDs.ONLINEACCESS] = ONLINEACCESS;
            dr[otherServicesDs.DEBITCARD] = DEBITCARD;
            dr[otherServicesDs.CHEQUEBOOK] = CHEQUEBOOK;
            dr[otherServicesDs.DEPOSITBOOK] = DEPOSITBOOK;
            dr[otherServicesDs.BUSINESSGROUPID] = BusinessGroup;

            otherServicesDs.Tables[otherServicesDs.TABLENAME].Rows.Add(dr);
        }

        public static void GetBTWrapData(BTWrapDS ds, Guid clientCid, CorporateEntity entity, ICMBroker broker)
        {
            GetBTWrapData(ds, clientCid, entity.BTWrap.AccountNumber, entity.BTWrap.AccountDescription, entity.ClientId, entity.Name, entity.BusinessGroup, broker);
        }

        public static void GetBTWrapData(BTWrapDS ds, Guid clientCid, ClientIndividualEntity entity, ICMBroker broker)
        {
            GetBTWrapData(ds, clientCid, entity.BTWrap.AccountNumber, entity.BTWrap.AccountDescription, entity.ClientId, entity.Name, entity.BusinessGroup, broker);
        }


        private static void GetBTWrapData(BTWrapDS ds, Guid clientCid, string accountNumber, string accountdesc, string clientId, string clientName, Guid businessGroupId, ICMBroker broker)
        {
            DataRow dr = ds.Tables[ds.btwrapTable.TableName].NewRow();

            dr[ds.btwrapTable.CID] = clientCid;
            dr[ds.btwrapTable.CLIENTID] = clientId;
            dr[ds.btwrapTable.CLIENTNAME] = clientName;
            dr[ds.btwrapTable.ACCOUNTNUMBER] = accountNumber;
            dr[ds.btwrapTable.ACCOUNTDESCRIPTION] = accountdesc;
            dr[ds.btwrapTable.BUSINESSGROUPID] = businessGroupId;

            string businessGroupName = GetBusinessGroupName(businessGroupId, broker);
            dr[ds.btwrapTable.BUSINESSGROUPNAME] = businessGroupName;


            ds.Tables[ds.btwrapTable.TableName].Rows.Add(dr);
        }

        private static string GetBusinessGroupName(Guid businessGroupId, ICMBroker broker)
        {
            string businessGroupName = string.Empty;
            if (businessGroupId != Guid.Empty)
            {
                var organization = broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                if (organization != null)
                {
                    List<BusinessGroupEntity> lstBusinessGroup = organization.BusinessGroups;
                    BusinessGroupEntity entity = lstBusinessGroup.FirstOrDefault(x => x.ID == businessGroupId);
                    if (entity != null) businessGroupName = entity.Name;
                }
                broker.ReleaseBrokerManagedComponent(organization);
            }
            return businessGroupName;
        }


        public static void SetOtherServices(OtherServicesDS otherServicesDs, CorporateEntity entity)
        {
            if (otherServicesDs.Tables[otherServicesDs.TABLENAME].Rows.Count > 0)
            {
                //AccessFacilities
                entity.AccessFacilities.PHONEACCESS = (bool)otherServicesDs.Tables[otherServicesDs.TABLENAME].Rows[0][otherServicesDs.PHONEACCESS];
                entity.AccessFacilities.ONLINEACCESS = (bool)otherServicesDs.Tables[otherServicesDs.TABLENAME].Rows[0][otherServicesDs.ONLINEACCESS];
                entity.AccessFacilities.DEBITCARD = (bool)otherServicesDs.Tables[otherServicesDs.TABLENAME].Rows[0][otherServicesDs.DEBITCARD];
                entity.AccessFacilities.CHEQUEBOOK = (bool)otherServicesDs.Tables[otherServicesDs.TABLENAME].Rows[0][otherServicesDs.CHEQUEBOOK];
                entity.AccessFacilities.DEPOSITBOOK = (bool)otherServicesDs.Tables[otherServicesDs.TABLENAME].Rows[0][otherServicesDs.DEPOSITBOOK];

                //OperationManner
                entity.OperationManner.ANY_ONE_OF_US_TO_SIGN = (bool)otherServicesDs.Tables[otherServicesDs.TABLENAME].Rows[0][otherServicesDs.ANYONEOFUSTOSIGN];
                entity.OperationManner.ANY_TWO_OF_US_TO_SIGN = (bool)otherServicesDs.Tables[otherServicesDs.TABLENAME].Rows[0][otherServicesDs.ANYTWOOFUSTOSIGN];
                entity.OperationManner.ALL_OF_US_TO_SIGN = (bool)otherServicesDs.Tables[otherServicesDs.TABLENAME].Rows[0][otherServicesDs.ALLOFUSTOSIGN];

                //ExemptionCategory
                entity.ExemptionCategory.Category1 = (bool)otherServicesDs.Tables[otherServicesDs.TABLENAME].Rows[0][otherServicesDs.CATEGORY1];
                entity.ExemptionCategory.Category2 = (bool)otherServicesDs.Tables[otherServicesDs.TABLENAME].Rows[0][otherServicesDs.CATEGORY2];
                entity.ExemptionCategory.Category3 = (bool)otherServicesDs.Tables[otherServicesDs.TABLENAME].Rows[0][otherServicesDs.CATEGORY3];

                //BTWrap
                entity.BTWrap.AccountNumber = otherServicesDs.Tables[otherServicesDs.TABLENAME].Rows[0][otherServicesDs.ACCOUNTNUMBER].ToString();
                entity.BTWrap.AccountDescription = otherServicesDs.Tables[otherServicesDs.TABLENAME].Rows[0][otherServicesDs.ACCOUNTDESCRIPTION].ToString();

                //BusinessGroupID
                Guid result;
                if (Guid.TryParse(otherServicesDs.Tables[otherServicesDs.TABLENAME].Rows[0][otherServicesDs.BUSINESSGROUPID].ToString(), out result) && result != Guid.Empty)
                    entity.BusinessGroup = result;
            }
        }

        public static void SetOtherServices(OtherServicesDS otherServicesDs, ClientIndividualEntity entity)
        {
            //AccessFacilities
            if (otherServicesDs.Tables[otherServicesDs.TABLENAME].Rows.Count > 0)
            {
                entity.AccessFacilities.PHONEACCESS = (bool)otherServicesDs.Tables[otherServicesDs.TABLENAME].Rows[0][otherServicesDs.PHONEACCESS];
                entity.AccessFacilities.ONLINEACCESS = (bool)otherServicesDs.Tables[otherServicesDs.TABLENAME].Rows[0][otherServicesDs.ONLINEACCESS];
                entity.AccessFacilities.DEBITCARD = (bool)otherServicesDs.Tables[otherServicesDs.TABLENAME].Rows[0][otherServicesDs.DEBITCARD];
                entity.AccessFacilities.CHEQUEBOOK = (bool)otherServicesDs.Tables[otherServicesDs.TABLENAME].Rows[0][otherServicesDs.CHEQUEBOOK];
                entity.AccessFacilities.DEPOSITBOOK = (bool)otherServicesDs.Tables[otherServicesDs.TABLENAME].Rows[0][otherServicesDs.DEPOSITBOOK];

                //OperationManner
                entity.OperationManner.ANY_ONE_OF_US_TO_SIGN = (bool)otherServicesDs.Tables[otherServicesDs.TABLENAME].Rows[0][otherServicesDs.ANYONEOFUSTOSIGN];
                entity.OperationManner.ANY_TWO_OF_US_TO_SIGN = (bool)otherServicesDs.Tables[otherServicesDs.TABLENAME].Rows[0][otherServicesDs.ANYTWOOFUSTOSIGN];
                entity.OperationManner.ALL_OF_US_TO_SIGN = (bool)otherServicesDs.Tables[otherServicesDs.TABLENAME].Rows[0][otherServicesDs.ALLOFUSTOSIGN];

                //ExemptionCategory
                entity.ExemptionCategory.Category1 = (bool)otherServicesDs.Tables[otherServicesDs.TABLENAME].Rows[0][otherServicesDs.CATEGORY1];
                entity.ExemptionCategory.Category2 = (bool)otherServicesDs.Tables[otherServicesDs.TABLENAME].Rows[0][otherServicesDs.CATEGORY2];
                entity.ExemptionCategory.Category3 = (bool)otherServicesDs.Tables[otherServicesDs.TABLENAME].Rows[0][otherServicesDs.CATEGORY3];

                //BTWrap
                entity.BTWrap.AccountNumber = otherServicesDs.Tables[otherServicesDs.TABLENAME].Rows[0][otherServicesDs.ACCOUNTNUMBER].ToString();
                entity.BTWrap.AccountDescription = otherServicesDs.Tables[otherServicesDs.TABLENAME].Rows[0][otherServicesDs.ACCOUNTDESCRIPTION].ToString();

                //BusinessGroupID
                Guid result;
                if (Guid.TryParse(otherServicesDs.Tables[otherServicesDs.TABLENAME].Rows[0][otherServicesDs.BUSINESSGROUPID].ToString(), out result) && result != Guid.Empty)
                    entity.BusinessGroup = result;
            }
        }

        public static void SetClientDividendDs(ClientDividendDetailsDS ds, CorporateEntity Entity, ICMBroker Broker)
        {
            if (Entity.DividendCollection == null)
            {
                Entity.DividendCollection = new ObservableCollection<DividendEntity>();
            }
            SetClientDividentIds(Entity.ClientId, ds, Entity.DividendCollection, Broker, Entity.BankAccounts, Entity.ManagedInvestmentSchemesAccounts, Entity.DesktopBrokerAccounts);
        }

        // ds is data from UI, DividendCollection is object from CM. Use ds values to set DividendCollection
        private static void SetDividendComponents(ClientDividendDetailsDS ds, DividendEntity dividendEntity)
        {
            if (ds == null || ds.Entity == null || ds.Entity.Components == null)
                return;

            if (dividendEntity == null)
                return;

            if (dividendEntity.Components == null)
                dividendEntity.Components = new List<DividendComponent>();

            foreach (DividendComponent distributionComponent in ds.Entity.Components)
            {
                var component = dividendEntity.Components.FirstOrDefault(ss => ss.ComponentType == distributionComponent.ComponentType);
                if (component != null)
                {
                    component.Autax_Credit = distributionComponent.Autax_Credit;
                    component.Tax_WithHeld = distributionComponent.Tax_WithHeld;
                    component.Unit_Amount = distributionComponent.Unit_Amount;
                }
                else
                    dividendEntity.Components.Add(distributionComponent);
            }
        }

        private static void SetClientDividentIds(string clientID, ClientDividendDetailsDS ds, ObservableCollection<DividendEntity> DividendCollection, ICMBroker Broker, List<IdentityCMDetail> bankAccounts, List<IdentityCMDetail> managedInvestmentSchemesAccounts, List<IdentityCMDetail> desktopBrokerAccounts)
        {

            if (ds.CommandType == DatasetCommandTypes.Update)
            {
                var dividendEntity = DividendCollection.FirstOrDefault(details => details.ID == ds.ID);
                if (dividendEntity != null)
                {
                    dividendEntity.CentsPerShare = ds.Entity.CentsPerShare;
                    dividendEntity.Investor = ds.Entity.Investor;
                    dividendEntity.AdministrationSystem = ds.Entity.AdministrationSystem;
                    dividendEntity.TransactionType = ds.Entity.TransactionType;
                    dividendEntity.InvestmentCode = ds.Entity.InvestmentCode;
                    dividendEntity.ExternalReferenceID = ds.Entity.ExternalReferenceID;
                    dividendEntity.RecordDate = ds.Entity.RecordDate;
                    dividendEntity.PaymentDate = ds.Entity.PaymentDate;
                    dividendEntity.UnitsOnHand = ds.Entity.UnitsOnHand;
                    dividendEntity.FrankedAmount = ds.Entity.FrankedAmount;
                    dividendEntity.UnfrankedAmount = ds.Entity.UnfrankedAmount;
                    dividendEntity.TaxFree = ds.Entity.TaxFree;
                    dividendEntity.TaxDeferred = ds.Entity.TaxDeferred;
                    dividendEntity.ReturnOfCapital = ds.Entity.ReturnOfCapital;
                    dividendEntity.DomesticInterestAmount = ds.Entity.DomesticInterestAmount;
                    dividendEntity.ForeignInterestAmount = ds.Entity.ForeignInterestAmount;
                    dividendEntity.DomesticOtherIncome = ds.Entity.DomesticOtherIncome;
                    dividendEntity.ForeignOtherIncome = ds.Entity.ForeignOtherIncome;
                    dividendEntity.DomesticWithHoldingTax = ds.Entity.DomesticWithHoldingTax;
                    dividendEntity.ForeignWithHoldingTax = ds.Entity.ForeignWithHoldingTax;
                    dividendEntity.DomesticImputationTaxCredits = ds.Entity.DomesticImputationTaxCredits;
                    dividendEntity.ForeignImputationTaxCredits = ds.Entity.ForeignImputationTaxCredits;
                    dividendEntity.ForeignInvestmentFunds = ds.Entity.ForeignInvestmentFunds;
                    dividendEntity.DomesticIndexationCGT = ds.Entity.DomesticIndexationCGT;
                    dividendEntity.ForeignIndexationCGT = ds.Entity.ForeignIndexationCGT;
                    dividendEntity.DomesticDiscountedCGT = ds.Entity.DomesticDiscountedCGT;
                    dividendEntity.ForeignDiscountedCGT = ds.Entity.ForeignDiscountedCGT;
                    dividendEntity.DomesticOtherCGT = ds.Entity.DomesticOtherCGT;
                    dividendEntity.ForeignOtherCGT = ds.Entity.ForeignOtherCGT;
                    dividendEntity.DomesticGrossAmount = ds.Entity.DomesticGrossAmount;
                    dividendEntity.PercentFrankedAmount = ds.Entity.PercentFrankedAmount;
                    dividendEntity.PercentUnfrankedAmount = ds.Entity.PercentUnfrankedAmount;
                    dividendEntity.PercentTaxFree = ds.Entity.PercentTaxFree;
                    dividendEntity.PercentTaxDeferred = ds.Entity.PercentTaxDeferred;
                    dividendEntity.PercentReturnOfCapital = ds.Entity.PercentReturnOfCapital;
                    dividendEntity.PercentDomesticInterestAmount = ds.Entity.PercentDomesticInterestAmount;
                    dividendEntity.PercentForeignInterestAmount = ds.Entity.PercentForeignInterestAmount;
                    dividendEntity.PercentDomesticOtherIncome = ds.Entity.PercentDomesticOtherIncome;
                    dividendEntity.PercentForeignOtherIncome = ds.Entity.PercentForeignOtherIncome;
                    dividendEntity.PercentDomesticWithHoldingTax = ds.Entity.PercentDomesticWithHoldingTax;
                    dividendEntity.PercentForeignWithHoldingTax = ds.Entity.PercentForeignWithHoldingTax;
                    dividendEntity.PercentDomesticImputationTaxCredits = ds.Entity.PercentDomesticImputationTaxCredits;
                    dividendEntity.PercentForeignImputationTaxCredits = ds.Entity.PercentForeignImputationTaxCredits;
                    dividendEntity.PercentForeignInvestmentFunds = ds.Entity.PercentForeignInvestmentFunds;
                    dividendEntity.PercentDomesticIndexationCGT = ds.Entity.PercentDomesticIndexationCGT;
                    dividendEntity.PercentForeignIndexationCGT = ds.Entity.PercentForeignIndexationCGT;
                    dividendEntity.PercentDomesticDiscountedCGT = ds.Entity.PercentDomesticDiscountedCGT;
                    dividendEntity.PercentForeignDiscountedCGT = ds.Entity.PercentForeignDiscountedCGT;
                    dividendEntity.PercentDomesticOtherCGT = ds.Entity.PercentDomesticOtherCGT;
                    dividendEntity.PercentForeignOtherCGT = ds.Entity.PercentForeignOtherCGT;
                    dividendEntity.PercentDomesticGrossAmount = ds.Entity.PercentDomesticGrossAmount;
                    dividendEntity.Comment = ds.Entity.Comment;
                    dividendEntity.Account = ds.Entity.Account;
                    dividendEntity.CashManagementTransactionID = ds.Entity.CashManagementTransactionID;
                    dividendEntity.InterestRate = ds.Entity.InterestRate;
                    dividendEntity.InvestmentAmount = ds.Entity.InvestmentAmount;
                    dividendEntity.InterestPaid = ds.Entity.InterestPaid;
                    if (ds.Entity.BalanceDate.HasValue)
                        dividendEntity.BalanceDate = ds.Entity.BalanceDate;
                    if (ds.Entity.BooksCloseDate.HasValue)
                        dividendEntity.BooksCloseDate = ds.Entity.BooksCloseDate;
                    dividendEntity.Currency = ds.Entity.Currency;
                    dividendEntity.Dividendtype = ds.Entity.Dividendtype;
                    dividendEntity.MannualEdit = true;

                    // set paid dividend value from sum of tax components.
                    double UnitAmountColumnTotal = 0;
                    if (ds.Entity.Components != null)
                    {
                        foreach (var component in ds.Entity.Components)
                        {
                            UnitAmountColumnTotal += component.Unit_Amount.HasValue ? component.Unit_Amount.Value : 0;
                        }
                    }
                    dividendEntity.PaidDividend = UnitAmountColumnTotal;

                    dividendEntity.IsManualUpdated = true;
                    SetDividendComponents(ds, dividendEntity);

                    ds.ExtendedProperties.Add("Result", OperationResults.Successfull);
                    ds.ExtendedProperties.Add("Message", "Dividend Entity Updated");
                    MatchDividend(clientID, DividendCollection, Broker, bankAccounts, managedInvestmentSchemesAccounts, desktopBrokerAccounts, dividendEntity);
                }
                else
                {
                    ds.ExtendedProperties.Add("Result", OperationResults.Failed);
                    ds.ExtendedProperties.Add("Message", "Dividend Entity not found");
                }
            }
            else
            {
                if (ds.CommandType == DatasetCommandTypes.Add)
                {
                    ds.Entity.ID = ds.ID;
                    var dividend = ds.Entity;
                    MatchDividend(clientID, DividendCollection, Broker, bankAccounts, managedInvestmentSchemesAccounts, desktopBrokerAccounts, dividend);
                    DividendCollection.Add(dividend);
                    ds.ExtendedProperties.Add("Result", OperationResults.Successfull);
                    ds.ExtendedProperties.Add("Message", "Dividend Entity Added");
                }
            }
        }

        private static void MatchDividend(string clientID, ObservableCollection<DividendEntity> DividendCollection, ICMBroker Broker,
                                          List<IdentityCMDetail> bankAccounts, List<IdentityCMDetail> managedInvestmentSchemesAccounts, List<IdentityCMDetail> desktopBrokerAccounts,
                                          DividendEntity dividendEntity)
        {
            FinancialDataUtility financialDataUtility = new FinancialDataUtility();
            decimal units = financialDataUtility.GetUnits(Broker, clientID, managedInvestmentSchemesAccounts,
                                                      desktopBrokerAccounts, dividendEntity.InvestmentCode, dividendEntity.RecordDate);

            bool matched = MatchBankTrans(DividendCollection, Broker, bankAccounts, units, dividendEntity);
            //if transaction doesn't match then match in ASX transactions
            if (!matched)
            {
                var org = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;

                var sec = org.Securities.FirstOrDefault(ss => ss.AsxCode == dividendEntity.InvestmentCode);
                if (sec != null)
                {

                    var securityprice = sec.ASXSecurity.OrderByDescending(ee => ee.Date).FirstOrDefault(ss => ss.Date <= dividendEntity.RecordDate);
                    decimal asOfDateUnitPrice = 0;

                    if (securityprice != null && securityprice.UnitPrice != 0.0)
                    {
                        asOfDateUnitPrice = (decimal)securityprice.UnitPrice;
                        matched = MatchASXTrans(DividendCollection, Broker, desktopBrokerAccounts, units, dividendEntity, asOfDateUnitPrice);
                    }
                    Broker.ReleaseBrokerManagedComponent(org);

                }
            }
        }

        private static bool MatchASXTrans(ObservableCollection<DividendEntity> DividendCollection, ICMBroker Broker, List<IdentityCMDetail> desktopBrokerAccounts, decimal units, DividendEntity dividendEntity, decimal asOfDateUnitPrice)
        {
            bool matched = false;
            foreach (var account in desktopBrokerAccounts)
            {
                var tempaccountcm = (DesktopBrokerAccountCM)Broker.GetCMImplementation(account.Clid, account.Csid);
                if (tempaccountcm != null && tempaccountcm.DesktopBrokerAccountEntity.holdingTransactions != null)
                {
                    tempaccountcm.Broker.SetStart();
                    var unMatchedList = tempaccountcm.DesktopBrokerAccountEntity.holdingTransactions.Where(ss => ss.DividendStatus != TransectionStatus.Matched).ToList();
                    if (dividendEntity.Status != DividendStatus.Matched)
                    {
                        matched = SetDividendStatusASX(dividendEntity, unMatchedList, units, asOfDateUnitPrice);
                    }
                    else
                    {
                        var alreadyMatchedTrans = tempaccountcm.DesktopBrokerAccountEntity.holdingTransactions.FirstOrDefault(ss => ss.DividendID == dividendEntity.ID);
                        if (alreadyMatchedTrans != null)
                        {
                            matched = SetDividendStatusASX(dividendEntity, unMatchedList, alreadyMatchedTrans, units, asOfDateUnitPrice);
                            if (!matched)
                            {
                                //  this code may cause inconsistency in data because units provided are not actual units
                                //List<DividendEntity> unMatchedDividend = DividendCollection.Where(ss => ss.Status != DividendStatus.Matched).ToList();
                                // SetalreadyAttachedTransStatus(unMatchedDividend, alreadyMatchedTrans, alreadyMatchedTrans.Units, alreadyMatchedTrans.UnitPrice);
                            }
                        }
                    }
                    tempaccountcm.CalculateToken(true);

                    Broker.ReleaseBrokerManagedComponent(tempaccountcm);

                }
                if (matched) break;

            }
            return matched;
        }

        public static bool SetDividendStatusASX(DividendEntity dv, List<HoldingTransactions> unMatchedTrans, decimal dvunits, decimal asOfDateUnitPrice)
        {
            bool matched = false;
            var matchedTran = unMatchedTrans.FirstOrDefault(ee => ee.IsDividentMatchWithTrans(dv, dvunits, asOfDateUnitPrice));
            if (matchedTran != null)
            {
                matched = true;
                matchedTran.DividendID = dv.ID;
                matchedTran.DividendStatus = TransectionStatus.Matched;
                dv.Status = DividendStatus.Matched;
                dv.DesktopbrokerTransID = matchedTran.TranstactionUniqueID.ToString();
            }
            return matched;
        }

        public static bool SetDividendStatusASX(DividendEntity dv, List<HoldingTransactions> unMatchedTrans, HoldingTransactions preMatchedTran, decimal dvunits, decimal asOfDateUnitPrice)
        {
            bool matched = true;
            if (!preMatchedTran.IsDividentMatchWithTrans(dv, dvunits, asOfDateUnitPrice))
            {
                preMatchedTran.DividendID = Guid.Empty;
                preMatchedTran.DividendStatus = TransectionStatus.Loaded;
                dv.Status = DividendStatus.Accrued;
                dv.DesktopbrokerTransID = string.Empty;
                matched = SetDividendStatusASX(dv, unMatchedTrans, dvunits, asOfDateUnitPrice);
            }
            return matched;
        }

        public static bool SetalreadyAttachedTransStatus(List<DividendEntity> DividendCollection, HoldingTransactions transEntity, decimal dvUnits, decimal asOfDateUnitPrice)
        {
            bool matched = false;
            DividendEntity dvEntity = DividendCollection.Where(ss => transEntity.IsDividentMatchWithTrans(ss, dvUnits, asOfDateUnitPrice)).FirstOrDefault();
            if (dvEntity != null)
            {
                transEntity.DividendID = dvEntity.ID;
                transEntity.DividendStatus = TransectionStatus.Matched;
                dvEntity.Status = DividendStatus.Matched;
                dvEntity.DesktopbrokerTransID = transEntity.TranstactionUniqueID.ToString();
                matched = true;
            }

            return matched;
        }

        private static bool MatchBankTrans(ObservableCollection<DividendEntity> DividendCollection, ICMBroker Broker, List<IdentityCMDetail> bankAccounts, decimal units, DividendEntity dividendEntity)
        {
            bool matched = false;
            foreach (var countbankaccount in bankAccounts)
            {
                var tempbankaccount = (BankAccountCM)Broker.GetCMImplementation(countbankaccount.Clid, countbankaccount.Csid);
                if (tempbankaccount != null && tempbankaccount.BankAccountEntity.CashManagementTransactions != null)
                {
                    List<CashManagementEntity> unMatchedList = tempbankaccount.BankAccountEntity.CashManagementTransactions.Where(ss => ss.DividendStatus != TransectionStatus.Matched).ToList();
                    if (dividendEntity.Status != DividendStatus.Matched)// if dividend is not matched
                    {
                        matched = SetDividendStatus(dividendEntity, unMatchedList, units);
                    }
                    else
                    {
                        var oldmatchedTran = tempbankaccount.BankAccountEntity.CashManagementTransactions.FirstOrDefault(ss => ss.DividendID == dividendEntity.ID);
                        if (oldmatchedTran != null)
                        {
                            matched = SetDividendStatus(dividendEntity, unMatchedList, oldmatchedTran, units);
                            if (!matched)
                            {
                                // this code may cause problems in datat because units provided are not proper transaction units
                                //List<DividendEntity> unMatchedDividend = DividendCollection.Where(ss => ss.Status != DividendStatus.Matched).ToList();
                                //SetOldtransitionStatus(unMatchedDividend, oldmatchedTran, units);
                            }
                        }
                    }
                }

                if (matched) break;
            }
            return matched;
        }

        public static void SetClientDividendDs(ClientDividendDetailsDS ds, ClientIndividualEntity Entity, ICMBroker Broker)
        {
            if (Entity.DividendCollection == null)
            {
                Entity.DividendCollection = new ObservableCollection<DividendEntity>();
            }
            SetClientDividentIds(Entity.ClientId, ds, Entity.DividendCollection, Broker, Entity.BankAccounts, Entity.ManagedInvestmentSchemesAccounts, Entity.DesktopBrokerAccounts);
        }

        public static bool SetDividendStatus(DividendEntity dv, List<CashManagementEntity> unMatchedTrans, decimal dvunits)
        {
            bool matched = false;
            CashManagementEntity matchedTran = unMatchedTrans.FirstOrDefault(ss => ss.IsDividendMatchingTrans(dv, dvunits));
            if (matchedTran != null)
            {
                matchedTran.DividendID = dv.ID;
                matchedTran.DividendStatus = TransectionStatus.Matched;
                dv.Status = DividendStatus.Matched;
                dv.CashManagementTransactionID = matchedTran.ExternalReferenceID;
                matched = true;
            }
            return matched;
        }

        private static bool SetDividendStatus(DividendEntity dv, List<CashManagementEntity> unMatchedTrans, CashManagementEntity preMatchedTran, decimal dvunits)
        {
            bool matched = true;
            if (!preMatchedTran.IsDividendMatchingTrans(dv, dvunits))
            {
                preMatchedTran.DividendID = Guid.Empty;
                preMatchedTran.DividendStatus = TransectionStatus.Loaded;
                dv.Status = DividendStatus.Accrued;
                dv.CashManagementTransactionID = string.Empty;

                matched = SetDividendStatus(dv, unMatchedTrans, dvunits);
            }
            return matched;
        }

        public static bool SetOldtransitionStatus(List<DividendEntity> DividendCollection, CashManagementEntity transEntity, decimal dvunits)
        {
            bool matched = false;
            DividendEntity dvEntity = DividendCollection.Where(ss => transEntity.IsDividendMatchingTrans(ss, dvunits)).FirstOrDefault();
            if (dvEntity != null)
            {
                transEntity.DividendID = dvEntity.ID;
                transEntity.DividendStatus = TransectionStatus.Matched;
                dvEntity.Status = DividendStatus.Matched;
                dvEntity.CashManagementTransactionID = transEntity.ExternalReferenceID;
                matched = true;
            }
            return matched;
        }

        public static void GetClientUnitsForASX(ClientTotalUnitsDS ds, List<IdentityCMDetail> ASXlist, ICMBroker broker)
        {
            foreach (var each in ASXlist)
            {
                var asx = broker.GetCMImplementation(each.Clid, each.Csid);
                if (asx != null)
                {
                    asx.GetData(ds);
                }
            }
        }

        public static string GetProductFromAccountProcess(List<AccountProcessTaskEntity> listAccountProcess, string fundCode, ICMBroker broker)
        {
            SerializableDictionary<string, string> ids = new SerializableDictionary<string, string> { { "ProductID", string.Empty }, { "AccountCID", string.Empty }, { "AccountNo", string.Empty }, { "AccountBSB", string.Empty } };
            var models = listAccountProcess.GroupBy(ss => ss.ModelID);
            var org = broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            bool hasfound = false;
            foreach (var model in models)
            {
                var orgModel = org.Model.Where(ss => ss.ID == model.Key).FirstOrDefault();
                if (orgModel != null)
                {
                    //Fin simplicity order import for SS should be only for service type "Do It For Me" 
                    if (orgModel.ServiceType != ServiceTypes.DoItForMe) continue;
                    foreach (var asset in orgModel.Assets)
                    {
                        foreach (var product in asset.Products)
                        {
                            if (product.EntityType != OrganizationType.ManagedInvestmentSchemesAccount) continue;
                            var task = listAccountProcess.FirstOrDefault(ss => ss.TaskID == product.ID && ss.ModelID == orgModel.ID && ss.AssetID == asset.ID);
                            if (task != null && task.LinkedEntity != IdentityCM.Null)
                            {
                                var mis = broker.GetCMImplementation(task.LinkedEntity.Clid, task.LinkedEntity.Csid) as IMISEntity;
                                if (mis != null)
                                {
                                    if (mis.HasFund(fundCode, product.FundAccounts.FirstOrDefault()))
                                    {
                                        ids["ProductID"] = product.ID.ToString();
                                        var Cashtask = listAccountProcess.FirstOrDefault(ss => ss.ModelID == orgModel.ID && ss.LinkedEntityType == OrganizationType.BankAccount && ss.LinkedEntity != IdentityCM.Null);
                                        if (Cashtask != null)
                                        {
                                            var bankAccCM = broker.GetCMImplementation(Cashtask.LinkedEntity.Clid, Cashtask.LinkedEntity.Csid);
                                            if (bankAccCM != null)
                                            {
                                                ids["AccountCID"] = bankAccCM.CID.ToString();
                                                ids["AccountNo"] = bankAccCM.GetDataStream((int)CmCommand.AccountNumber, null);
                                                ids["AccountBSB"] = bankAccCM.GetDataStream((int)CmCommand.BSB, null);

                                            }
                                        }
                                        hasfound = true;
                                        break;
                                    }
                                }
                            }
                        }
                        if (hasfound)
                            break;
                    }
                }
                if (hasfound)
                    break;

            }

            broker.ReleaseBrokerManagedComponent(org);


            return ids.ToXmlString();
        }

        public static string GetProductFromAccountProcessDesktop(List<AccountProcessTaskEntity> listAccountProcess, string investmentCode, string accNo, ICMBroker broker)
        {
            SerializableDictionary<string, string> ids = new SerializableDictionary<string, string> { { "ASXCID", string.Empty }, { "ProductID", string.Empty }, { "AccountCID", string.Empty }, { "AccountNo", string.Empty }, { "AccountBSB", string.Empty }, { "ASXAccountNo", string.Empty }, { "IsSecurityInSystem", false.ToString() } };
            var models = listAccountProcess.GroupBy(ss => ss.ModelID);
            var orgCM = broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            var iSSecurity = orgCM.Securities.Count(ss => ss.AsxCode == investmentCode) > 0;
            ids["IsSecurityInSystem"] = iSSecurity.ToString();
            bool hasfound = false;
            foreach (var model in models)
            {
                var orgModel = orgCM.Model.Where(ss => ss.ID == model.Key).FirstOrDefault();

                if (orgModel != null)
                {
                    //Fin simplicity order import for SS should be only for service type "Do It For Me"
                    if (orgModel.ServiceType != ServiceTypes.DoItForMe) continue;
                    foreach (var asset in orgModel.Assets)
                    {
                        foreach (var product in asset.Products)
                        {
                            if (product.EntityType != OrganizationType.DesktopBrokerAccount) continue;
                            var task = listAccountProcess.FirstOrDefault(ss => ss.TaskID == product.ID && ss.ModelID == orgModel.ID && ss.AssetID == asset.ID);
                            if (task != null && task.LinkedEntity != IdentityCM.Null)
                            {
                                var asx = broker.GetCMImplementation(task.LinkedEntity.Clid, task.LinkedEntity.Csid) as DesktopBrokerAccountCM;
                                if (asx != null)
                                {
                                    ids["ASXCID"] = asx.CID.ToString();
                                    ids["ProductID"] = product.ID.ToString();
                                    ids["ASXAccountNo"] = asx.GetDataStream((int)CmCommand.AccountNumber, null);
                                    var productSecurity = orgCM.ProductSecurities.FirstOrDefault(ss => ss.ID == product.ProductSecuritiesId);
                                    var Cashtask = listAccountProcess.FirstOrDefault(ss => ss.ModelID == orgModel.ID && ss.LinkedEntityType == OrganizationType.BankAccount && ss.LinkedEntity != IdentityCM.Null);
                                    if (Cashtask != null)
                                    {
                                        var bankAccCM = broker.GetCMImplementation(Cashtask.LinkedEntity.Clid, Cashtask.LinkedEntity.Csid);
                                        if (bankAccCM != null)
                                        {
                                            ids["AccountCID"] = bankAccCM.CID.ToString();
                                            ids["AccountNo"] = bankAccCM.GetDataStream((int)CmCommand.AccountNumber, null);
                                            ids["AccountBSB"] = bankAccCM.GetDataStream((int)CmCommand.BSB, null);
                                        }
                                    }
                                    hasfound = IsInvestimentCodeAttached(investmentCode, asx, productSecurity, orgCM.Securities);

                                }
                            }
                            if (hasfound)
                                return ids.ToXmlString();
                        }
                    }
                }
            }
            return ids.ToXmlString();
        }

        public static string GetProductFromAccountProcessDesktopForAllServiceTypes(List<AccountProcessTaskEntity> listAccountProcess, string investmentCode, string accNo, ICMBroker broker)
        {
            SerializableDictionary<string, string> ids = new SerializableDictionary<string, string> { { "ASXCID", string.Empty }, { "ProductID", string.Empty }, { "AccountCID", string.Empty }, { "AccountNo", string.Empty }, { "AccountBSB", string.Empty }, { "ASXAccountNo", string.Empty }, { "IsSecurityInSystem", false.ToString() } };
            var models = listAccountProcess.GroupBy(ss => ss.ModelID);
            var orgCM = broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            var iSSecurity = orgCM.Securities.Count(ss => ss.AsxCode == investmentCode) > 0;
            ids["IsSecurityInSystem"] = iSSecurity.ToString();
            bool hasfound = false;
            foreach (var model in models)
            {
                var orgModel = orgCM.Model.Where(ss => ss.ID == model.Key).FirstOrDefault();

                if (orgModel != null)
                {
                    foreach (var asset in orgModel.Assets)
                    {
                        foreach (var product in asset.Products)
                        {
                            if (product.EntityType != OrganizationType.DesktopBrokerAccount) continue;
                            var task = listAccountProcess.FirstOrDefault(ss => ss.TaskID == product.ID && ss.ModelID == orgModel.ID && ss.AssetID == asset.ID);
                            if (task != null && task.LinkedEntity != IdentityCM.Null)
                            {
                                var asx = broker.GetCMImplementation(task.LinkedEntity.Clid, task.LinkedEntity.Csid) as DesktopBrokerAccountCM;
                                if (asx != null)
                                {
                                    ids["ASXCID"] = asx.CID.ToString();
                                    ids["ProductID"] = product.ID.ToString();
                                    ids["ASXAccountNo"] = asx.GetDataStream((int)CmCommand.AccountNumber, null);
                                    var productSecurity = orgCM.ProductSecurities.FirstOrDefault(ss => ss.ID == product.ProductSecuritiesId);
                                    var Cashtask = listAccountProcess.FirstOrDefault(ss => ss.ModelID == orgModel.ID && ss.LinkedEntityType == OrganizationType.BankAccount && ss.LinkedEntity != IdentityCM.Null);
                                    if (Cashtask != null)
                                    {
                                        var bankAccCM = broker.GetCMImplementation(Cashtask.LinkedEntity.Clid, Cashtask.LinkedEntity.Csid);
                                        if (bankAccCM != null)
                                        {
                                            ids["AccountCID"] = bankAccCM.CID.ToString();
                                            ids["AccountNo"] = bankAccCM.GetDataStream((int)CmCommand.AccountNumber, null);
                                            ids["AccountBSB"] = bankAccCM.GetDataStream((int)CmCommand.BSB, null);
                                        }
                                    }
                                    hasfound = IsInvestimentCodeAttached(investmentCode, asx, productSecurity, orgCM.Securities);
                                }
                            }
                            if (hasfound)
                                return ids.ToXmlString();
                        }
                    }
                }
            }
            return ids.ToXmlString();
        }

        private static bool IsInvestimentCodeAttached(string investmentCode, DesktopBrokerAccountCM asx, ProductSecuritiesEntity productSecurity, List<SecuritiesEntity> securities)
        {
            bool hasfound = false;
            if (productSecurity != null)
            {
                foreach (var prosec in productSecurity.Details)
                {
                    var secutiry = securities.FirstOrDefault(ss => ss.ID == prosec.SecurityCodeId);
                    if (secutiry != null && secutiry.AsxCode == investmentCode)
                    {
                        hasfound = true;
                        break;
                    }
                }
            }
            else
            {
                var filteredHoldingTransactionsByValuationDate =
                    asx.DesktopBrokerAccountEntity.holdingTransactions.Where(holdTrans =>
                                                                             holdTrans.TradeDate <= DateTime.Now);
                var groupedSecurities = filteredHoldingTransactionsByValuationDate.GroupBy(holdTran => holdTran.InvestmentCode);
                foreach (var security in groupedSecurities)
                {
                    var asxBroker = securities.Where(sec => sec.AsxCode == security.Key).FirstOrDefault();
                    if (asxBroker != null && asxBroker.AsxCode == investmentCode)
                    {
                        hasfound = true;
                        break;
                    }
                }
            }
            return hasfound;
        }

        public static void GetProductFromAccountProcessDesktop(OrderPadDS ds, List<AccountProcessTaskEntity> listAccountProcess, ICMBroker broker)
        {
            var models = listAccountProcess.GroupBy(ss => ss.ModelID);
            var orgCM = broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            bool hasfound = false;
            foreach (var model in models)
            {
                ModelEntity orgModel = ds.ExtendedProperties["ServiceType"] != null ? orgCM.Model.Where(ss => ss.ID == model.Key && ss.ServiceType.GetDescription() == ds.ExtendedProperties["ServiceType"].ToString()).FirstOrDefault() : orgCM.Model.Where(ss => ss.ID == model.Key).FirstOrDefault();
                if (orgModel != null)
                {
                    foreach (var asset in orgModel.Assets)
                    {
                        foreach (var product in asset.Products)
                        {
                            if (product.EntityType != OrganizationType.DesktopBrokerAccount) continue;
                            var task = listAccountProcess.FirstOrDefault(ss => ss.TaskID == product.ID && ss.ModelID == orgModel.ID && ss.AssetID == asset.ID);
                            if (task != null && task.LinkedEntity != IdentityCM.Null)
                            {
                                var asx = broker.GetCMImplementation(task.LinkedEntity.Clid, task.LinkedEntity.Csid) as DesktopBrokerAccountCM;
                                if (asx != null)
                                {
                                    foreach (DataRow dr in ds.ASXTable.Rows)
                                    {
                                        dr[ds.ASXTable.ASXACCOUNTNO] = asx.GetDataStream((int)CmCommand.AccountNumber, null);
                                        dr[ds.ASXTable.ASXACCOUNTCID] = asx.CID.ToString();
                                        if (ds.ExtendedProperties["IsBuy"] != null && ds.ExtendedProperties["IsBuy"].ToString().ToLower() == "true")
                                        {
                                            dr[ds.ASXTable.PRODUCTID] = product.ID;
                                            hasfound = true;
                                            break;
                                        }
                                        var productSecurity = orgCM.ProductSecurities.FirstOrDefault(ss => ss.ID == product.ProductSecuritiesId);
                                        if (productSecurity != null)
                                        {
                                            foreach (var prosec in productSecurity.Details)
                                            {
                                                var secutiry = orgCM.Securities.FirstOrDefault(ss => ss.ID == prosec.SecurityCodeId);
                                                if (secutiry != null && secutiry.AsxCode == dr[ds.ASXTable.INVESTMENTCODE].ToString())
                                                {
                                                    if (dr[ds.ASXTable.PRODUCTID].ToString() == product.ID.ToString())
                                                    {
                                                        hasfound = true;
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            var filteredHoldingTransactionsByValuationDate = asx.DesktopBrokerAccountEntity.holdingTransactions.Where(holdTrans => holdTrans.TradeDate <= DateTime.Now);
                                            var groupedSecurities = filteredHoldingTransactionsByValuationDate.GroupBy(holdTran => holdTran.InvestmentCode);
                                            foreach (var security in groupedSecurities)
                                            {
                                                var asxBroker = orgCM.Securities.Where(sec => sec.AsxCode == security.Key).FirstOrDefault();
                                                if (asxBroker != null && asxBroker.AsxCode == dr[ds.ASXTable.INVESTMENTCODE].ToString())
                                                {
                                                    if (dr[ds.ASXTable.PRODUCTID].ToString() == product.ID.ToString())
                                                    {
                                                        hasfound = true;
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            if (hasfound)
                                break;
                        }
                        if (hasfound)
                            break;
                    }
                }
                if (hasfound)
                    break;
            }
        }

        public static void FillClientCID(MISTransactionDS ds, string clientID, Guid clientCID)
        {
            {
                DataRow[] drs = ds.Tables[MISTransactionDS.MISTRANHOLDINGTABLE].Select(MISTransactionDS.CLIENTIDCOL + "='" + clientID + "'");

                foreach (var dr in drs)
                {
                    dr[MISTransactionDS.CLIENTCID] = clientCID;
                }
            }


            {
                DataRow[] drs = ds.Tables[MISTransactionDS.MISTRANTABLE].Select(MISTransactionDS.CLIENTIDCOL + "='" + clientID + "'");

                foreach (var dr in drs)
                {
                    dr[MISTransactionDS.CLIENTCID] = clientCID;
                }
            }

        }

        #region Client Addresses
        public static void FillClientAddressDetailDs(AddressDetailsDS ds, ClientIndividualEntity Entity)
        {
            FillClientAddresses(ds, Entity.Address);
        }
        private static void FillClientAddresses(AddressDetailsDS ds, DualAddressEntity address)
        {
            #region Get Business Address

            if (address.BusinessAddress != null)
            {
                FillAddress(ds, address.BusinessAddress, AddressType.BusinessAddress);
            }
            if (address.MailingAddress != null)
            {
                FillAddress(ds, address.MailingAddress, AddressType.MailingAddress);
            }
            if (address.RegisteredAddress != null)
            {
                FillAddress(ds, address.RegisteredAddress, AddressType.RegisteredAddress);
            }
            if (address.ResidentialAddress != null)
            {
                FillAddress(ds, address.ResidentialAddress, AddressType.ResidentialAddress);
            }
            if (address.DuplicateMailingAddress != null)
            {
                FillAddress(ds, address.DuplicateMailingAddress, AddressType.DuplicateMailingAddress);
            }

            #endregion


        }
        private static void FillAddress(AddressDetailsDS ds, AddressEntity address, AddressType addressType)
        {

            DataRow drBusinessAddress = ds.ClientDetailsTable.NewRow();
            drBusinessAddress[ds.ClientDetailsTable.ADDRESSLINE1] = address.Addressline1;
            drBusinessAddress[ds.ClientDetailsTable.ADDRESSLINE2] = address.Addressline2;
            drBusinessAddress[ds.ClientDetailsTable.SUBURB] = address.Suburb;
            drBusinessAddress[ds.ClientDetailsTable.STATE] = address.State;
            drBusinessAddress[ds.ClientDetailsTable.COUNTRY] = address.Country;
            drBusinessAddress[ds.ClientDetailsTable.POSTCODE] = address.PostCode;
            drBusinessAddress[ds.ClientDetailsTable.ADDRESSTYPE] = addressType;
            ds.ClientDetailsTable.Rows.Add(drBusinessAddress);
        }
        public static void FillClientAddressDetailDs(AddressDetailsDS ds, CorporateEntity Entity)
        {
            FillClientAddresses(ds, Entity.Address);
        }


        public static void SetClientAddressDetail(AddressDetailsDS ds, ClientIndividualEntity Entity)
        {
            SetClientAddresses(ds, Entity.Address);
        }

        private static void SetClientAddresses(AddressDetailsDS ds, DualAddressEntity Address)
        {
            foreach (DataRow dr in ds.ClientDetailsTable.Rows)
            {
                if ((AddressType)System.Enum.Parse(typeof(AddressType), dr[ds.ClientDetailsTable.ADDRESSTYPE].ToString()) == AddressType.BusinessAddress)
                {
                    SetAddress(ds.ClientDetailsTable, Address.BusinessAddress, dr);
                }
                else if (
                    (AddressType)System.Enum.Parse(typeof(AddressType), dr[ds.ClientDetailsTable.ADDRESSTYPE].ToString()) == AddressType.RegisteredAddress)
                {
                    SetAddress(ds.ClientDetailsTable, Address.RegisteredAddress, dr);
                }
                else if (
                    (AddressType)
                    System.Enum.Parse(typeof(AddressType), dr[ds.ClientDetailsTable.ADDRESSTYPE].ToString()) == AddressType.ResidentialAddress)
                {
                    SetAddress(ds.ClientDetailsTable, Address.ResidentialAddress, dr);
                }
                else if (
                    (AddressType)
                    System.Enum.Parse(typeof(AddressType), dr[ds.ClientDetailsTable.ADDRESSTYPE].ToString()) == AddressType.MailingAddress)
                {
                    SetAddress(ds.ClientDetailsTable, Address.MailingAddress, dr);
                }
                else if (
                    (AddressType)
                    System.Enum.Parse(typeof(AddressType), dr[ds.ClientDetailsTable.ADDRESSTYPE].ToString()) == AddressType.DuplicateMailingAddress)
                {
                    SetAddress(ds.ClientDetailsTable, Address.DuplicateMailingAddress, dr);
                }
            }
        }

        private static void SetAddress(AddressDetails ClientDetailsTable, AddressEntity Address, DataRow dr)
        {
            Address.Addressline1 = dr[ClientDetailsTable.ADDRESSLINE1].ToString();
            Address.Addressline2 = dr[ClientDetailsTable.ADDRESSLINE2].ToString();
            Address.Suburb = dr[ClientDetailsTable.SUBURB].ToString();
            Address.State = dr[ClientDetailsTable.STATE].ToString();
            Address.Country = dr[ClientDetailsTable.COUNTRY].ToString();
            Address.PostCode = dr[ClientDetailsTable.POSTCODE].ToString();
        }

        public static void SetClientAddressDetail(AddressDetailsDS ds, CorporateEntity Entity)
        {
            SetClientAddresses(ds, Entity.Address);
        }
        #endregion

        public static bool MatchDisWithBankTran(List<IdentityCMDetail> bankAccounts, DistributionIncomeEntity db, ICMBroker broker)
        {
            bool isMatched = false;
            if (db.Status == DistributionIncomeStatus.Matched) // if dividend is already matched then remove both references
            {
                db.Status = DistributionIncomeStatus.Accrued;
                db.CashManagementTransactionID = string.Empty;

                if (bankAccounts != null)
                    foreach (var countbankaccount in bankAccounts)
                    {
                        var tempbankaccount = (BankAccountCM)broker.GetCMImplementation(countbankaccount.Clid, countbankaccount.Csid);
                        if (tempbankaccount != null)
                        {
                            if (tempbankaccount.BankAccountEntity.CashManagementTransactions != null)
                            {
                                CashManagementEntity cmTransaction = tempbankaccount.BankAccountEntity.CashManagementTransactions.FirstOrDefault(ee => ee.DividendID == db.ID);
                                if (cmTransaction != null)
                                {
                                    tempbankaccount.Broker.SetStart();
                                    tempbankaccount.Broker.SaveOverride = true;
                                    cmTransaction.DividendStatus = TransectionStatus._;// default Enum member
                                    cmTransaction.DividendID = Guid.Empty;
                                    tempbankaccount.CalculateToken(true);
                                    break;
                                }

                            }

                        }
                    }
            }
            if (bankAccounts != null)
                foreach (var countbankaccount in bankAccounts)
                {
                    var tempbankaccount = (BankAccountCM)broker.GetCMImplementation(countbankaccount.Clid, countbankaccount.Csid);
                    if (tempbankaccount != null)
                    {
                        if (tempbankaccount.BankAccountEntity.CashManagementTransactions != null)
                        {
                            CashManagementEntity cmTransaction = tempbankaccount.BankAccountEntity.CashManagementTransactions.FirstOrDefault(ee => Math.Abs(ee.TotalAmount - Convert.ToDecimal(db.NetCashDistribution)) < (decimal)0.5 && ee.SystemTransactionType == "Distribution" && ee.TransactionDate == db.PaymentDate);
                            if (cmTransaction != null)
                            {
                                MatchTransactionBankDistribution(db, tempbankaccount, cmTransaction);
                                isMatched = true;
                                break;
                            }
                            cmTransaction = tempbankaccount.BankAccountEntity.CashManagementTransactions.FirstOrDefault(ee => Math.Abs(ee.TotalAmount - Convert.ToDecimal(db.NetCashDistribution)) < (decimal)0.5 && ee.SystemTransactionType == "Distribution" && (ee.TransactionDate.AddDays(-1) == db.PaymentDate || ee.TransactionDate.AddDays(-2) == db.PaymentDate
                                                                                                                                                                                                                                                                    || ee.TransactionDate.AddDays(-3) == db.PaymentDate || ee.TransactionDate.AddDays(-4) == db.PaymentDate || ee.TransactionDate.AddDays(-5) == db.PaymentDate || ee.TransactionDate.AddDays(-6) == db.PaymentDate || ee.TransactionDate.AddDays(-7) == db.PaymentDate
                                                                                                                                                                                                                                                                    || ee.TransactionDate.AddDays(-8) == db.PaymentDate || ee.TransactionDate.AddDays(-9) == db.PaymentDate || ee.TransactionDate.AddDays(-10) == db.PaymentDate || ee.TransactionDate.AddDays(-11) == db.PaymentDate || ee.TransactionDate.AddDays(-12) == db.PaymentDate
                                                                                                                                                                                                                                                                    || ee.TransactionDate.AddDays(-13) == db.PaymentDate || ee.TransactionDate.AddDays(-14) == db.PaymentDate || ee.TransactionDate.AddDays(-15) == db.PaymentDate || ee.TransactionDate.AddDays(1) == db.PaymentDate || ee.TransactionDate.AddDays(2) == db.PaymentDate || ee.TransactionDate.AddDays(3) == db.PaymentDate || ee.TransactionDate.AddDays(4) == db.PaymentDate || ee.TransactionDate.AddDays(5) == db.PaymentDate
                                                                                                                                                                                                                                                                    || ee.TransactionDate.AddDays(6) == db.PaymentDate || ee.TransactionDate.AddDays(7) == db.PaymentDate || ee.TransactionDate.AddDays(8) == db.PaymentDate || ee.TransactionDate.AddDays(9) == db.PaymentDate || ee.TransactionDate.AddDays(10) == db.PaymentDate
                                                                                                                                                                                                                                                                    || ee.TransactionDate.AddDays(11) == db.PaymentDate || ee.TransactionDate.AddDays(12) == db.PaymentDate || ee.TransactionDate.AddDays(13) == db.PaymentDate || ee.TransactionDate.AddDays(14) == db.PaymentDate));
                            if (cmTransaction != null)
                            {
                                MatchTransactionBankDistribution(db, tempbankaccount, cmTransaction);
                                isMatched = true;
                                break;
                            }
                        }
                    }
                }
            return isMatched;
        }

        public static bool MatchDisWithMISTran(List<IdentityCMDetail> misAccounts, DistributionIncomeEntity db,string clientID, ICMBroker broker)
        {
            bool isMatched = false;
            if (db.Status == DistributionIncomeStatus.Matched) // if distribution is already matched then remove both references
            {
                db.Status = DistributionIncomeStatus.Accrued;
                db.MisTransactionID = string.Empty;

                if (misAccounts != null)
                    foreach (var cmDetail in misAccounts)
                    {
                        var cm = (ManagedInvestmentSchemesAccountCM)broker.GetCMImplementation(cmDetail.Clid, cmDetail.Csid);
                        if (cm != null)
                        {
                            if (cm.ManagedInvestmentSchemesAccountEntity.FundAccounts != null)
                            {
                                var fund = cm.ManagedInvestmentSchemesAccountEntity.FundAccounts.FirstOrDefault(ee => ee.ID == cmDetail.FundID);
                                if (fund != null)
                                {
                                    var trans=fund.FundTransactions.FirstOrDefault(ee => ee.DistributionID == db.ID);
                                    if (trans != null)
                                    {
                                        cm.Broker.SetStart();
                                        cm.Broker.SaveOverride = true;
                                        trans.DistributionStatus = TransectionStatus._; // default Enum member
                                        trans.DistributionID = Guid.Empty;
                                        cm.CalculateToken(true);
                                        break;
                                    }
                                }

                            }

                        }
                    }
            }
            if (misAccounts != null)
                foreach (var cmDetail in misAccounts)
                {
                    var cm = (ManagedInvestmentSchemesAccountCM)broker.GetCMImplementation(cmDetail.Clid, cmDetail.Csid);
                    if (cm != null)
                    {
                        if (cm.ManagedInvestmentSchemesAccountEntity.FundAccounts != null)
                        {

                            var fund =cm.ManagedInvestmentSchemesAccountEntity.FundAccounts.FirstOrDefault(ee => ee.ID == cmDetail.FundID);
                            if (fund != null)
                            {
                                var cmTransaction = fund.FundTransactions.FirstOrDefault(ee => ee.IsDistributionMatchWithTrans(clientID,db, fund.Code));
                                if (cmTransaction != null)
                                {
                                    MatchTransactionBankDistribution(db, cm, cmTransaction);
                                    isMatched = true;
                                    break;
                                }
                               
                            }
                        }
                    }
                }
            return isMatched;
        }





        private static void MatchTransactionBankDistribution(DistributionIncomeEntity db, BankAccountCM tempbankaccount, CashManagementEntity cmTransaction)
        {
            tempbankaccount.Broker.SetStart();
            tempbankaccount.Broker.SaveOverride = true;
            cmTransaction.DividendStatus = TransectionStatus.Matched;
            cmTransaction.DividendID = db.ID;
            db.CashManagementTransactionID = cmTransaction.ExternalReferenceID;
            db.Status = DistributionIncomeStatus.Matched;
            tempbankaccount.CalculateToken(true);

        }


        private static void MatchTransactionBankDistribution(DistributionIncomeEntity db, ManagedInvestmentSchemesAccountCM tempbankaccount, MISFundTransactionEntity cmTransaction)
        {
            tempbankaccount.Broker.SetStart();
            tempbankaccount.Broker.SaveOverride = true;
            cmTransaction.DistributionStatus = TransectionStatus.Matched;
            cmTransaction.DistributionID = db.ID;
            db.MisTransactionID = cmTransaction.ID.ToString();
            db.Status = DistributionIncomeStatus.Matched;
            tempbankaccount.CalculateToken(true);

        }

    }
}




