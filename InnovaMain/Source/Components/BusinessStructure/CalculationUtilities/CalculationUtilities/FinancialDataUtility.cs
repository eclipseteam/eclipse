#region Using
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.Entity;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.Extensions;
using System.Collections.ObjectModel;
using Oritax.TaxSimp.CM.Group;
using CashManagementEntity = Oritax.TaxSimp.Common.CashManagementEntity;
using IdentityCMDetail = Oritax.TaxSimp.Common.IdentityCMDetail;

#endregion

namespace Oritax.TaxSimp.CM
{
    [Serializable]
    public class FinancialDataUtility
    {
        public void SetDividendTransactionDetails(CorporateEntity Entity, DivTransactionDetailsDS divTransactionDetailsDS, ICMBroker Broker)
        {
            if (divTransactionDetailsDS.DataSetOperationType == DataSetOperationType.DeletSingle)
            {
                if (divTransactionDetailsDS.Tables[DivTransactionDetailsDS.DIVTRANSACTIONDETAILSTABLE].Rows.Count > 0)
                {
                    Guid transactionID = (Guid)divTransactionDetailsDS.Tables[DivTransactionDetailsDS.DIVTRANSACTIONDETAILSTABLE].Rows[0][DivTransactionDetailsDS.ID];
                    var dividendEntity = Entity.DividendCollection.FirstOrDefault(divCol => divCol.ID == transactionID);
                    if (dividendEntity != null)
                    {
                        foreach (var countbankaccount in Entity.BankAccounts)
                        {
                            BankAccountCM tempbankaccount = (BankAccountCM)Broker.GetCMImplementation(countbankaccount.Clid, countbankaccount.Csid);
                            // tempbankaccount.Broker.SetStart();
                            if (tempbankaccount != null && tempbankaccount.BankAccountEntity.CashManagementTransactions != null)
                            {
                                decimal units = GetUnits(Broker, Entity.ClientId, Entity.ManagedInvestmentSchemesAccounts, Entity.DesktopBrokerAccounts, dividendEntity.InvestmentCode, dividendEntity.RecordDate);
                                Common.CashManagementEntity cmTransaction = tempbankaccount.BankAccountEntity.CashManagementTransactions.Where(ee => Math.Abs(ee.TotalAmount - ((decimal)dividendEntity.CentsPerShare * units)) < 1 && ee.SystemTransactionType == "Dividend" && ee.TransactionDate == dividendEntity.PaymentDate).FirstOrDefault();
                                if (cmTransaction != null)
                                {
                                    cmTransaction.DividendStatus = TransectionStatus.Loaded;
                                    cmTransaction.DividendID = Guid.Empty;
                                }
                            }
                            //tempbankaccount.CalculateToken(true);
                        }
                        Entity.DividendCollection.Remove(dividendEntity);
                    }
                }
            }
        }
        public void SetDividendTransactionDetails(ClientIndividualEntity Entity, DivTransactionDetailsDS divTransactionDetailsDS, ICMBroker Broker)
        {
            if (divTransactionDetailsDS.DataSetOperationType == DataSetOperationType.DeletSingle)
            {
                if (divTransactionDetailsDS.Tables[DivTransactionDetailsDS.DIVTRANSACTIONDETAILSTABLE].Rows.Count > 0)
                {
                    Guid transactionID = (Guid)divTransactionDetailsDS.Tables[DivTransactionDetailsDS.DIVTRANSACTIONDETAILSTABLE].Rows[0][DivTransactionDetailsDS.ID];
                    var dividendEntity = Entity.DividendCollection.Where(divCol => divCol.ID == transactionID).FirstOrDefault();
                    if (dividendEntity != null)
                    {
                        if (dividendEntity.InvestmentCode.ToLower() != "cash")
                        {
                            foreach (var countbankaccount in Entity.BankAccounts)
                            {
                                BankAccountCM tempbankaccount = (BankAccountCM)Broker.GetCMImplementation(countbankaccount.Clid, countbankaccount.Csid);
                                // tempbankaccount.Broker.SetStart();
                                if (tempbankaccount != null && tempbankaccount.BankAccountEntity.CashManagementTransactions != null)
                                {
                                    decimal units = GetUnits(Broker, Entity.ClientId, Entity.ManagedInvestmentSchemesAccounts, Entity.DesktopBrokerAccounts, dividendEntity.InvestmentCode, dividendEntity.RecordDate);
                                    Common.CashManagementEntity cmTransaction = tempbankaccount.BankAccountEntity.CashManagementTransactions.Where(ee => ee.IsDividendMatchingTrans(dividendEntity, units)).FirstOrDefault();
                                    if (cmTransaction != null)
                                    {
                                        cmTransaction.DividendStatus = TransectionStatus.Loaded;
                                        cmTransaction.DividendID = Guid.Empty;
                                    }
                                }
                                //tempbankaccount.CalculateToken(true);
                            }
                        }
                        Entity.DividendCollection.Remove(dividendEntity);
                    }
                }
            }
        }

        public decimal GetUnits(ICMBroker Broker, string clientdID, List<Common.IdentityCMDetail> MisAccounts, List<Common.IdentityCMDetail> asxAccount, string invesmentCode, DateTime? date)
        {
            IOrganization organization = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            decimal units = 0;
            if (organization != null)
            {

                var security = organization.Securities.FirstOrDefault(sec => sec.AsxCode == invesmentCode);
                if (security != null)
                {
                    if (security.InvestmentType == "Managed Fund /Unit Trust")
                    {
                        foreach (var mis in MisAccounts)
                        {

                            ManagedInvestmentSchemesAccountCM temp = (ManagedInvestmentSchemesAccountCM)Broker.GetCMImplementation(mis.Clid, mis.Csid);
                            if (temp != null)
                            {

                                var misfund = temp.ManagedInvestmentSchemesAccountEntity.FundAccounts.FirstOrDefault(fund => fund.ID == mis.FundID && mis.FundID == security.FundID);

                                if (misfund != null && misfund.Code == security.AsxCode)
                                {
                                    decimal transactionShares = 0;
                                    transactionShares = misfund.FundTransactions.Where(ee => ee.ClientID == clientdID && ee.TradeDate <= date).Sum(dd => dd.Shares);
                                    units = transactionShares;
                                }
                            }
                        }
                    }
                    else
                    {
                        foreach (var asxbroker in asxAccount)
                        {
                            DesktopBrokerAccountCM temp = (DesktopBrokerAccountCM)Broker.GetCMImplementation(asxbroker.Clid, asxbroker.Csid);
                            if (temp != null)
                            {

                                var DesktopValue = temp.DesktopBrokerAccountEntity.holdingTransactions.Where(fund => fund.InvestmentCode == security.AsxCode).ToList();

                                if (DesktopValue != null)
                                {
                                    decimal transactionShares = 0;
                                    transactionShares = DesktopValue.Where(ee => ee.TradeDate <= date).Sum(dd => dd.Units);
                                    units = transactionShares;
                                }
                            }

                        }
                    }
                }
                Broker.ReleaseBrokerManagedComponent(organization);
            }
            return units;
        }

        public void GetBankAccountDataSet(BankTransactionDS bankTransactionDS, IOrganization orgCM, string clientType, IdentityCM Parent, string clientID, string clientName, List<AccountProcessTaskEntity> ListAccountProcess, ICMBroker Broker, CorporateEntity Entity)
        {
            DataTable clientSummaryTable = bankTransactionDS.Tables[HoldingRptDataSet.CLIENTSUMMARYTABLE];
            DataTable contactTable = bankTransactionDS.Tables[HoldingRptDataSet.CONTACTTABLE];

            DataRow clientSummaryRow = clientSummaryTable.NewRow();
            DataRow contactRow = contactTable.NewRow();

            BasicClientDetails(clientType, clientID, clientName, Parent, Broker, Entity.ABN, Entity.ACN, Entity.Address, clientSummaryRow);

            CSVInstance csvinstance = new CSVInstance(Broker);
            CorporateCSV _entitycsv = csvinstance.GetEntityConcreteInstance(Entity);
            foreach (var contact in _entitycsv.Contacts)
            {
                contactRow[BaseClientDetails.CONTACT_TITLE] = contact.PersonalTitle;
                contactRow[BaseClientDetails.CONTACT_NAME] = contact.Fullname;
                contactRow[BaseClientDetails.CONTACT_BIZNO] = contact.WorkPhoneNumber;
                contactRow[BaseClientDetails.CONTACT_MOBNO] = contact.MobilePhoneNumber;
                contactRow[BaseClientDetails.CONTACT_HNO] = contact.HomePhoneNumber;
                contactRow[BaseClientDetails.CONTACT_FAXNO] = contact.Facsimile;
                contactRow[BaseClientDetails.CONTACT_EMAIL] = contact.EmailAddress;
            }

            clientSummaryTable.Rows.Add(clientSummaryRow);
            contactTable.Rows.Add(contactRow);

            GetBankTrasactions(bankTransactionDS, orgCM, ListAccountProcess, Broker, Entity.DividendCollection, clientID);
            AddDividendTable(clientID, Entity.DividendCollection, bankTransactionDS);
        }

        private static void AddDividendTable(string clientID, IEnumerable<DividendEntity> dividendEntities, BankTransactionDS bankTransactionDS)
        {
            if (dividendEntities != null && dividendEntities.Count() > 0)
            {
                DataTable divTab = new DIVTransactionDS().Tables[DIVTransactionDS.INCOMESTRANSACTIONTABLE].Copy();
                divTab.TableName = "ClientAllDividends";

                var dividends = dividendEntities;
                if (bankTransactionDS.StartDate != DateTime.MinValue && bankTransactionDS.EndDate != DateTime.MinValue)
                {
                    dividends = dividendEntities.Where(tran => tran.PaymentDate >= bankTransactionDS.StartDate && tran.PaymentDate <= bankTransactionDS.EndDate);
                }

                foreach (DividendEntity dividendEntity in dividends)
                {
                    AddDividendtoTable(clientID, dividendEntity, divTab);
                }
                bankTransactionDS.Merge(divTab, false, MissingSchemaAction.Add);
            }
        }

        public void GetBankAccountDataSet(BankTransactionDS bankTransactionDS, IOrganization orgCM, string clientType, IdentityCM Parent, string clientID, string clientName, List<AccountProcessTaskEntity> ListAccountProcess, ICMBroker Broker, ClientIndividualEntity Entity)
        {
            DataTable clientSummaryTable = bankTransactionDS.Tables[HoldingRptDataSet.CLIENTSUMMARYTABLE];
            DataTable contactTable = bankTransactionDS.Tables[HoldingRptDataSet.CONTACTTABLE];

            DataRow clientSummaryRow = clientSummaryTable.NewRow();
            DataRow contactRow = contactTable.NewRow();

            BasicClientDetails(clientType, clientID, clientName, Parent, Broker, Entity.ABN, "", Entity.Address, clientSummaryRow);

            CSVInstance csvinstance = new CSVInstance(Broker);
            ClientIndividualEntityCSV _entitycsv = csvinstance.GetEntityConcreteInstance(Entity);
            foreach (var contact in _entitycsv.Applicants)
            {
                contactRow[BaseClientDetails.CONTACT_TITLE] = contact.PersonalTitle;
                contactRow[BaseClientDetails.CONTACT_NAME] = contact.Fullname;
                contactRow[BaseClientDetails.CONTACT_BIZNO] = contact.WorkPhoneNumber;
                contactRow[BaseClientDetails.CONTACT_MOBNO] = contact.MobilePhoneNumber;
                contactRow[BaseClientDetails.CONTACT_HNO] = contact.HomePhoneNumber;
                contactRow[BaseClientDetails.CONTACT_FAXNO] = contact.Facsimile;
                contactRow[BaseClientDetails.CONTACT_EMAIL] = contact.EmailAddress;
            }

            clientSummaryTable.Rows.Add(clientSummaryRow);
            contactTable.Rows.Add(contactRow);

            GetBankTrasactions(bankTransactionDS, orgCM, ListAccountProcess, Broker, Entity.DividendCollection, clientID);
            AddDividendTable(clientID, Entity.DividendCollection, bankTransactionDS);
        }

        public void GetdividendAccountDataSet(DIVTransactionDS divTransactionDS, IOrganization orgCM, string clientType, IdentityCM Parent, string clientID, string clientName, ICMBroker Broker, CorporateEntity Entity)
        {
            DataTable clientSummaryTable = divTransactionDS.Tables[HoldingRptDataSet.CLIENTSUMMARYTABLE];
            DataTable contactTable = divTransactionDS.Tables[HoldingRptDataSet.CONTACTTABLE];

            DataRow clientSummaryRow = clientSummaryTable.NewRow();
            DataRow contactRow = contactTable.NewRow();

            BasicClientDetails(clientType, clientID, clientName, Parent, Broker, Entity.ABN, Entity.ACN, Entity.Address, clientSummaryRow);

            CSVInstance csvinstance = new CSVInstance(Broker);
            CorporateCSV _entitycsv = csvinstance.GetEntityConcreteInstance(Entity);
            foreach (var contact in _entitycsv.Contacts)
            {
                contactRow[BaseClientDetails.CONTACT_TITLE] = contact.PersonalTitle;
                contactRow[BaseClientDetails.CONTACT_NAME] = contact.Fullname;
                contactRow[BaseClientDetails.CONTACT_BIZNO] = contact.WorkPhoneNumber;
                contactRow[BaseClientDetails.CONTACT_MOBNO] = contact.MobilePhoneNumber;
                contactRow[BaseClientDetails.CONTACT_HNO] = contact.HomePhoneNumber;
                contactRow[BaseClientDetails.CONTACT_FAXNO] = contact.Facsimile;
                contactRow[BaseClientDetails.CONTACT_EMAIL] = contact.EmailAddress;
            }

            clientSummaryTable.Rows.Add(clientSummaryRow);
            contactTable.Rows.Add(contactRow);

            GetDIVTransactionDS(divTransactionDS, orgCM, Entity.DividendCollection, Broker, clientID);
        }

        public void GetdividendAccountDataSet(DIVTransactionDS divTransactionDS, IOrganization orgCM, string clientType, IdentityCM Parent, string clientID, string clientName, ICMBroker Broker, ClientIndividualEntity Entity)
        {
            DataTable clientSummaryTable = divTransactionDS.Tables[HoldingRptDataSet.CLIENTSUMMARYTABLE];
            DataTable contactTable = divTransactionDS.Tables[HoldingRptDataSet.CONTACTTABLE];

            DataRow clientSummaryRow = clientSummaryTable.NewRow();
            DataRow contactRow = contactTable.NewRow();

            BasicClientDetails(clientType, clientID, clientName, Parent, Broker, Entity.ABN, "", Entity.Address, clientSummaryRow);

            CSVInstance csvinstance = new CSVInstance(Broker);
            ClientIndividualEntityCSV _entitycsv = csvinstance.GetEntityConcreteInstance(Entity);
            foreach (var contact in _entitycsv.Applicants)
            {
                contactRow[BaseClientDetails.CONTACT_TITLE] = contact.PersonalTitle;
                contactRow[BaseClientDetails.CONTACT_NAME] = contact.Fullname;
                contactRow[BaseClientDetails.CONTACT_BIZNO] = contact.WorkPhoneNumber;
                contactRow[BaseClientDetails.CONTACT_MOBNO] = contact.MobilePhoneNumber;
                contactRow[BaseClientDetails.CONTACT_HNO] = contact.HomePhoneNumber;
                contactRow[BaseClientDetails.CONTACT_FAXNO] = contact.Facsimile;
                contactRow[BaseClientDetails.CONTACT_EMAIL] = contact.EmailAddress;
            }

            clientSummaryTable.Rows.Add(clientSummaryRow);
            contactTable.Rows.Add(contactRow);

            GetDIVTransactionDS(divTransactionDS, orgCM, Entity.DividendCollection, Broker, clientID);
        }

        private static void BasicClientDetails(string clientType, string clientID, string clientName, IdentityCM Parent, ICMBroker Broker, string ABN, string ACN, Common.DualAddressEntity Address, DataRow clientSummaryRow)
        {
            clientSummaryRow[BaseClientDetails.CLIENTID] = clientID;
            clientSummaryRow[BaseClientDetails.CLIENTNAME] = clientName;
            clientSummaryRow[BaseClientDetails.CIENTABN] = ABN;
            clientSummaryRow[BaseClientDetails.CLIENTACN] = ACN;
            clientSummaryRow[BaseClientDetails.CLIENTTYPE] = clientType;

            if (Parent != null && Parent.Clid != Guid.Empty && Parent.Csid != Guid.Empty)
            {
                AdvisorCM adviserUnit = Broker.GetCMImplementation(Parent.Clid, Parent.Csid) as AdvisorCM;
                if (adviserUnit != null)
                    clientSummaryRow[BaseClientDetails.ADVISERFULLNAME] = adviserUnit.AdvisorEntity.FullName;
                Broker.ReleaseBrokerManagedComponent(adviserUnit);
            }

            if (Address.RegisteredAddress.Addressline1 != string.Empty)
            {
                clientSummaryRow[BaseClientDetails.ADDRESSLINE1] = Address.RegisteredAddress.Addressline1;
                clientSummaryRow[BaseClientDetails.ADDRESSLINE2] = Address.RegisteredAddress.Addressline2;
                clientSummaryRow[BaseClientDetails.SUBURB] = Address.RegisteredAddress.Suburb;
                clientSummaryRow[BaseClientDetails.STATE] = Address.RegisteredAddress.State;
                clientSummaryRow[BaseClientDetails.POSTCODE] = Address.RegisteredAddress.PostCode;
                clientSummaryRow[BaseClientDetails.COUNTRY] = Address.RegisteredAddress.Country;
            }
            else if (Address.BusinessAddress.Addressline1 != string.Empty)
            {
                clientSummaryRow[BaseClientDetails.ADDRESSLINE1] = Address.BusinessAddress.Addressline1;
                clientSummaryRow[BaseClientDetails.ADDRESSLINE2] = Address.BusinessAddress.Addressline2;
                clientSummaryRow[BaseClientDetails.SUBURB] = Address.BusinessAddress.Suburb;
                clientSummaryRow[BaseClientDetails.STATE] = Address.BusinessAddress.State;
                clientSummaryRow[BaseClientDetails.POSTCODE] = Address.BusinessAddress.PostCode;
                clientSummaryRow[BaseClientDetails.COUNTRY] = Address.BusinessAddress.Country;
            }
            else if (Address.MailingAddress.Addressline1 != string.Empty)
            {
                clientSummaryRow[BaseClientDetails.ADDRESSLINE1] = Address.MailingAddress.Addressline1;
                clientSummaryRow[BaseClientDetails.ADDRESSLINE2] = Address.MailingAddress.Addressline2;
                clientSummaryRow[BaseClientDetails.SUBURB] = Address.MailingAddress.Suburb;
                clientSummaryRow[BaseClientDetails.STATE] = Address.MailingAddress.State;
                clientSummaryRow[BaseClientDetails.POSTCODE] = Address.MailingAddress.PostCode;
                clientSummaryRow[BaseClientDetails.COUNTRY] = Address.MailingAddress.Country;
            }
            else if (Address.ResidentialAddress.Addressline1 != string.Empty)
            {
                clientSummaryRow[BaseClientDetails.ADDRESSLINE1] = Address.ResidentialAddress.Addressline1;
                clientSummaryRow[BaseClientDetails.ADDRESSLINE2] = Address.ResidentialAddress.Addressline2;
                clientSummaryRow[BaseClientDetails.SUBURB] = Address.ResidentialAddress.Suburb;
                clientSummaryRow[BaseClientDetails.STATE] = Address.ResidentialAddress.State;
                clientSummaryRow[BaseClientDetails.POSTCODE] = Address.ResidentialAddress.PostCode;
                clientSummaryRow[BaseClientDetails.COUNTRY] = Address.ResidentialAddress.Country;
            }

        }

        private static void GetBankTrasactions(BankTransactionDS bankTransactionDS, IOrganization orgCM, List<AccountProcessTaskEntity> ListAccountProcess, ICMBroker Broker, ObservableCollection<DividendEntity> DividendCollection, string clientID)
        {
            foreach (var accountProcessTaskEntity in ListAccountProcess)
            {
                if (accountProcessTaskEntity.LinkedEntity != null && accountProcessTaskEntity.LinkedEntity.Clid != Guid.Empty)
                {
                    var model = orgCM.Model.Where(mod => mod.ID == accountProcessTaskEntity.ModelID).FirstOrDefault();
                    if (model != null)
                    {
                        if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.BankAccount)
                        {
                            BankAccountCM bankAccount = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as BankAccountCM;

                            if (bankAccount != null && bankAccount.BankAccountEntity != null && bankAccount.BankAccountEntity.CashManagementTransactions != null)
                            {
                                if (bankAccount.BankAccountEntity.CashManagementTransactions.Count > 0 && bankTransactionDS.IsSinceInception)
                                    bankTransactionDS.StartDate = bankAccount.BankAccountEntity.CashManagementTransactions.OrderBy(tran => tran.TransactionDate).FirstOrDefault().TransactionDate;

                                DataRow dRow = bankTransactionDS.Tables[BankTransactionDS.BANKACCOUNTLIST].NewRow();
                                dRow[BankTransactionDS.BANKCID] = bankAccount.CID;
                                dRow[BankTransactionDS.BANKCODE] = bankAccount.BankAccountEntity.BSB + "-" + bankAccount.BankAccountEntity.AccountNumber;
                                dRow[BankTransactionDS.BSBNO] = bankAccount.BankAccountEntity.BSB;
                                dRow[BankTransactionDS.BANKACCOUNTNAME] = bankAccount.BankAccountEntity.Name;
                                dRow[BankTransactionDS.BANKACCOUNTNO] = bankAccount.BankAccountEntity.AccountNumber;

                                bankTransactionDS.Tables[BankTransactionDS.BANKACCOUNTLIST].Rows.Add(dRow);

                                decimal startingBalance = bankAccount.BankAccountEntity.CashManagementTransactions.Where(tran => tran.TransactionDate <= bankTransactionDS.StartDate.AddDays(-1)).Sum(tran => tran.TotalAmount);
                                var cashEntityOpening = bankAccount.BankAccountEntity.CashManagementTransactions.Where(tran => tran.TransactionDate >= bankTransactionDS.StartDate && tran.TransactionDate <= bankTransactionDS.EndDate).OrderBy(tran => tran.TransactionDate).FirstOrDefault();

                                if (cashEntityOpening != null)
                                    bankAccount.BankAccountEntity.CashManagementTransactions.Where(tran => tran.TransactionDate >= bankTransactionDS.StartDate && tran.TransactionDate <= bankTransactionDS.EndDate).OrderBy(tran => tran.TransactionDate).First().Balance = cashEntityOpening.TotalAmount + startingBalance;

                                decimal balance = startingBalance;

                                IEnumerable<CashManagementEntity> list = bankAccount.BankAccountEntity.CashManagementTransactions.OrderBy(tran => tran.TransactionDate).ToList();

                                if (bankTransactionDS.StartDate != DateTime.MinValue && bankTransactionDS.EndDate != DateTime.MinValue)
                                {
                                    list = list.Where(tran => tran.TransactionDate >= bankTransactionDS.StartDate && tran.TransactionDate <= bankTransactionDS.EndDate);
                                }

                                int count = 1;

                                foreach (CashManagementEntity item in list)
                                {
                                    if (item.ImportTransactionType == "Withdrawal")
                                        balance -= Math.Abs(item.Amount);
                                    else balance += item.Amount;

                                    item.Balance = balance;
                                    item.Rank = count;
                                    count++;
                                }

                                DataSet ds = new DataSet();
                                ds.Tables.Add(CashTransactionTable());
                                DataTable divTab = new DIVTransactionDS().Tables[DIVTransactionDS.INCOMESTRANSACTIONTABLE].Copy();
                                if (list.Count() > 0)
                                {
                                    foreach (CashManagementEntity cashEntity in list)
                                    {

                                        DataRow row = ds.Tables[0].NewRow();

                                        FillTransactionDataInRow(DividendCollection, clientID, divTab, row, cashEntity, bankAccount);

                                        ds.Tables[0].Rows.Add(row);
                                    }

                                    bankTransactionDS.Merge(ds, false, MissingSchemaAction.Add);
                                    bankTransactionDS.Merge(divTab, false, MissingSchemaAction.Add);
                                }
                                else if (bankTransactionDS.AddEmptyRow)
                                {
                                    //Commented code as it is doing nothing - Task1009
                                    #region commentedcode
                                    /*var entity = new Common.CashManagementEntity();
                                entity.ID = Guid.NewGuid();
                                entity.TransactionDate = DateTime.Now;
                                entity.Rank = 0;
                                entity.ExternalReferenceID = string.Empty;
                                entity.TransactionDate = DateTime.Now;
                                entity.Amount = 0;
                                entity.Adjustment = 0;

                                DataRow row = ds.Tables[0].NewRow();
                                FillTransactionDataInRow(DividendCollection, clientID, divTab, row, entity, bankAccount);
                                
                                //Commented code as it is adding a blank transaction 
                                //also this line of code was added mistakenly during re-factor process in 927 - Task 1009
                                //ds.Tables[0].Rows.Add(row);*/
                                    #endregion
                                    bankTransactionDS.Merge(ds, false, MissingSchemaAction.Add);
                                }
                            }
                        }
                    }
                }
            }
        }

        private static DataTable CashTransactionTable()
        {
            DataTable dt = new DataTable("Cash Transactions");
            dt.Columns.Add("ClientID");
            dt.Columns.Add("BSB");
            dt.Columns.Add("ProductType");
            dt.Columns.Add("AccountType");
            dt.Columns.Add("AccountNo");
            dt.Columns.Add("AmountTotal", typeof(decimal));
            dt.Columns.Add("BankAmount", typeof(decimal));
            dt.Columns.Add("BankAdjustment", typeof(decimal));
            dt.Columns.Add("BankTransactionDate", typeof(DateTime));
            dt.Columns.Add("BankCid", typeof(Guid));
            dt.Columns.Add("ID");
            dt.Columns.Add("InvestmentCode");
            dt.Columns.Add("Status");
            dt.Columns.Add("InvestmentName");
            dt.Columns.Add("ImportTransactionType");
            dt.Columns.Add("ImportTransactionTypeDetail");
            dt.Columns.Add("SystemTransactionType");
            dt.Columns.Add("Category");
            dt.Columns.Add("AdministrationSystem");
            dt.Columns.Add("ExternalReferenceID");
            dt.Columns.Add("TransactionDate", typeof(DateTime));
            dt.Columns.Add("AccountName");
            dt.Columns.Add("Amount");
            dt.Columns.Add("Balance");
            dt.Columns.Add("Adjustment");
            dt.Columns.Add("DividendStatus");
            dt.Columns.Add("Comment");
            dt.Columns.Add("DividendID");
            dt.Columns.Add("ContractNote");
            dt.Columns.Add("MaturityDate", typeof(DateTime));
            dt.Columns.Add("InterestRate");
            dt.Columns.Add("InstitutionID");
            dt.Columns.Add("IsContractNote");
            dt.Columns.Add("ServiceType");
            dt.Columns.Add("Product");
            dt.Columns.Add("OrderID");
            dt.Columns.Add("OrderItemID");
            dt.Columns.Add("Rank", typeof(int));

            return dt;
        }

        private static void FillTransactionDataInRow(ObservableCollection<DividendEntity> DividendCollection, string clientID, DataTable divTab, DataRow row, CashManagementEntity cashEntity, BankAccountCM bankAccount)
        {
            row["ClientID"] = clientID;
            row["ProductType"] = "CASH";
            row["ID"] = cashEntity.ID.ToString();
            row["InvestmentCode"] = cashEntity.InvestmentCode;
            row["Status"] = cashEntity.Status;
            row["InvestmentName"] = cashEntity.InvestmentName;
            row["ImportTransactionType"] = cashEntity.ImportTransactionType;
            row["ImportTransactionTypeDetail"] = cashEntity.ImportTransactionTypeDetail;
            row["SystemTransactionType"] = cashEntity.SystemTransactionType;
            row["Category"] = cashEntity.Category;
            row["AdministrationSystem"] = cashEntity.AdministrationSystem;
            row["ExternalReferenceID"] = cashEntity.ExternalReferenceID;
            row["TransactionDate"] = cashEntity.TransactionDate;
            row["AccountName"] = cashEntity.AccountName;
            row["Amount"] = cashEntity.Amount.ToString();
            row["Balance"] = cashEntity.Balance.ToString();
            row["Adjustment"] = cashEntity.Adjustment.ToString();
            row["DividendStatus"] = cashEntity.DividendStatus.ToString();
            row["Comment"] = cashEntity.Comment;
            row["DividendID"] = cashEntity.DividendID.ToString();
            row["ContractNote"] = cashEntity.ContractNote;
            row["MaturityDate"] = cashEntity.MaturityDate;
            row["InterestRate"] = cashEntity.InterestRate.ToString();
            row["InstitutionID"] = cashEntity.InstitutionID.ToString();
            row["IsContractNote"] = cashEntity.IsContractNote.ToString();
            row["ServiceType"] = cashEntity.ServiceType.ToString();
            row["Product"] = (cashEntity.Product == null) ? Guid.Empty.ToString() : cashEntity.Product.Value.ToString();
            row["OrderID"] = cashEntity.OrderID.ToString();
            row["OrderItemID"] = cashEntity.OrderItemID.ToString();
            row["BSB"] = bankAccount.BankAccountEntity.BSB;
            row["AccountType"] = bankAccount.BankAccountEntity.AccoutType.ToUpper();
            row["AccountNo"] = bankAccount.BankAccountEntity.AccountNumber;
            row["BankTransactionDate"] = cashEntity.TransactionDate;
            row["BankCID"] = bankAccount.CID;
            row["ExternalReferenceID"] = cashEntity.ExternalReferenceID;
            row["AmountTotal"] = cashEntity.TotalAmount;
            row["BankAmount"] = cashEntity.Amount;
            row["BankAdjustment"] = cashEntity.Adjustment;
            row["Rank"] = cashEntity.Rank;
            if (cashEntity.DividendID != Guid.Empty)
            {
                var divientity = DividendCollection.FirstOrDefault(ss => ss.ID == cashEntity.DividendID);
                if (divientity != null)
                    AddDividendtoTable(clientID, divientity, divTab);
            }
        }

        public void SetBankAccountDataSetTransactionFix(BankTransactionDetailsFixDS bankTransactionDS, IOrganization orgCM, List<AccountProcessTaskEntity> ListAccountProcess, ICMBroker Broker)
        {
            foreach (var accountProcessTaskEntity in ListAccountProcess)
            {
                if (accountProcessTaskEntity.LinkedEntity != null && accountProcessTaskEntity.LinkedEntity.Clid != Guid.Empty)
                {
                    var asset = orgCM.Assets.Where(var => var.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                    var model = orgCM.Model.Where(mod => mod.ID == accountProcessTaskEntity.ModelID).FirstOrDefault();
                    var linkedAsset = model.Assets.Where(ast => ast.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                    if (linkedAsset != null)
                    {
                        var product = linkedAsset.Products.Where(pro => pro.ID == accountProcessTaskEntity.TaskID).FirstOrDefault();
                        if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.BankAccount)
                        {
                            BankAccountCM bankAccount = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as BankAccountCM;
                            bankAccount.SetData(bankTransactionDS);
                        }
                    }
                }
            }
        }

        public void GetTDAccountDataSet(TDTransactionDS tdTransactionDS, IOrganization orgCM, string clientType, IdentityCM Parent, string clientID, string clientName, List<AccountProcessTaskEntity> ListAccountProcess, ICMBroker Broker, CorporateEntity Entity)
        {
            DataTable clientSummaryTable = tdTransactionDS.Tables[HoldingRptDataSet.CLIENTSUMMARYTABLE];
            DataTable contactTable = tdTransactionDS.Tables[HoldingRptDataSet.CONTACTTABLE];

            DataRow clientSummaryRow = clientSummaryTable.NewRow();
            DataRow contactRow = contactTable.NewRow();

            BasicClientDetails(clientType, clientID, clientName, Parent, Broker, Entity.ABN, Entity.ACN, Entity.Address, clientSummaryRow);

            CSVInstance csvinstance = new CSVInstance(Broker);
            CorporateCSV _entitycsv = csvinstance.GetEntityConcreteInstance(Entity);
            foreach (var contact in _entitycsv.Contacts)
            {
                contactRow[BaseClientDetails.CONTACT_TITLE] = contact.PersonalTitle;
                contactRow[BaseClientDetails.CONTACT_NAME] = contact.Fullname;
                contactRow[BaseClientDetails.CONTACT_BIZNO] = contact.WorkPhoneNumber;
                contactRow[BaseClientDetails.CONTACT_MOBNO] = contact.MobilePhoneNumber;
                contactRow[BaseClientDetails.CONTACT_HNO] = contact.HomePhoneNumber;
                contactRow[BaseClientDetails.CONTACT_FAXNO] = contact.Facsimile;
                contactRow[BaseClientDetails.CONTACT_EMAIL] = contact.EmailAddress;
            }

            clientSummaryTable.Rows.Add(clientSummaryRow);
            contactTable.Rows.Add(contactRow);

            GetTDsAccountDataSet(tdTransactionDS, orgCM, ListAccountProcess, Broker);
        }

        public void GetTDAccountDataSet(TDTransactionDS tdTransactionDS, IOrganization orgCM, string clientType, IdentityCM Parent, string clientID, string clientName, List<AccountProcessTaskEntity> ListAccountProcess, ICMBroker Broker, ClientIndividualEntity Entity)
        {
            DataTable clientSummaryTable = tdTransactionDS.Tables[HoldingRptDataSet.CLIENTSUMMARYTABLE];
            DataTable contactTable = tdTransactionDS.Tables[HoldingRptDataSet.CONTACTTABLE];

            DataRow clientSummaryRow = clientSummaryTable.NewRow();
            DataRow contactRow = contactTable.NewRow();

            BasicClientDetails(clientType, clientID, clientName, Parent, Broker, Entity.ABN, "", Entity.Address, clientSummaryRow);

            CSVInstance csvinstance = new CSVInstance(Broker);
            ClientIndividualEntityCSV _entitycsv = csvinstance.GetEntityConcreteInstance(Entity);
            foreach (var contact in _entitycsv.Applicants)
            {
                contactRow[BaseClientDetails.CONTACT_TITLE] = contact.PersonalTitle;
                contactRow[BaseClientDetails.CONTACT_NAME] = contact.Fullname;
                contactRow[BaseClientDetails.CONTACT_BIZNO] = contact.WorkPhoneNumber;
                contactRow[BaseClientDetails.CONTACT_MOBNO] = contact.MobilePhoneNumber;
                contactRow[BaseClientDetails.CONTACT_HNO] = contact.HomePhoneNumber;
                contactRow[BaseClientDetails.CONTACT_FAXNO] = contact.Facsimile;
                contactRow[BaseClientDetails.CONTACT_EMAIL] = contact.EmailAddress;
            }

            clientSummaryTable.Rows.Add(clientSummaryRow);
            contactTable.Rows.Add(contactRow);

            GetTDsAccountDataSet(tdTransactionDS, orgCM, ListAccountProcess, Broker);
        }

        public void GetTDsAccountDataSet(TDTransactionDS tdTransactionDS, IOrganization orgCM, List<AccountProcessTaskEntity> ListAccountProcess, ICMBroker Broker)
        {
            foreach (var accountProcessTaskEntity in ListAccountProcess)
            {
                if (accountProcessTaskEntity.LinkedEntity != null && accountProcessTaskEntity.LinkedEntity.Clid != Guid.Empty)
                {
                    var asset = orgCM.Assets.Where(var => var.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                    var model = orgCM.Model.Where(mod => mod.ID == accountProcessTaskEntity.ModelID).FirstOrDefault();
                    var linkedAsset = model.Assets.Where(ast => ast.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                    var product = linkedAsset.Products.Where(pro => pro.ID == accountProcessTaskEntity.TaskID).FirstOrDefault();

                    if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.TermDepositAccount)
                    {
                        BankAccountCM bankAccount = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as BankAccountCM;
                        if (bankAccount != null)
                        {
                            DataRow dRow = tdTransactionDS.Tables[TDTransactionDS.TDACCOUNTLIST].NewRow();
                            dRow[TDTransactionDS.BANKCID] = bankAccount.CID;
                            dRow[TDTransactionDS.BANKCODE] = bankAccount.BankAccountEntity.BSB + "-" + bankAccount.BankAccountEntity.AccountNumber;
                            dRow[TDTransactionDS.BSBNO] = bankAccount.BankAccountEntity.BSB;
                            dRow[TDTransactionDS.BANKACCOUNTNAME] = bankAccount.BankAccountEntity.Name;
                            dRow[TDTransactionDS.BANKACCOUNTNO] = bankAccount.BankAccountEntity.AccountNumber;
                            tdTransactionDS.Tables[TDTransactionDS.TDACCOUNTLIST].Rows.Add(dRow);

                            DataRow proRow = tdTransactionDS.Tables[TDTransactionDS.PRODUCTLIST].NewRow();
                            if (product != null)
                            {
                                proRow[TDTransactionDS.PROID] = product.ID;
                                proRow[TDTransactionDS.PRONAME] = product.Name;
                                if (product.Name.Contains("AMM"))
                                    proRow[TDTransactionDS.PRONAME] = "AMM";
                            }

                            tdTransactionDS.Tables[TDTransactionDS.PRODUCTLIST].Rows.Add(proRow);


                            if (bankAccount.BankAccountEntity.CashManagementTransactions.Count > 0)
                            {
                                DataSet ds =
                                BaseClientDetails.ConvertXMLToDataSet(bankAccount.BankAccountEntity.CashManagementTransactions.ToXmlString());
                                ds.Tables[0].TableName = "TDs Transactions";
                                ds.Tables[0].Columns.Add("BSB");
                                ds.Tables[0].Columns.Add("AccountType");
                                ds.Tables[0].Columns.Add("ProductType");
                                ds.Tables[0].Columns.Add("AccountNo");
                                ds.Tables[0].Columns.Add("InstituteName");
                                ds.Tables[0].Columns.Add("AmountTotal", typeof(decimal));
                                ds.Tables[0].Columns.Add("BankAmount", typeof(decimal));
                                ds.Tables[0].Columns.Add("BankAdjustment", typeof(decimal));
                                ds.Tables[0].Columns.Add("BankTransactionDate", typeof(DateTime));
                                ds.Tables[0].Columns.Add("BankMaturityDate", typeof(DateTime));
                                ds.Tables[0].Columns.Add("BankCid", typeof(Guid));
                                ds.Tables[0].Columns.Add("BrokerID", typeof(Guid));
                                ds.Tables[0].Columns.Add("BrokerName", typeof(string));
                                ds.Tables[0].Columns.Add("AccountTier", typeof(string));

                                if (!ds.Tables[0].Columns.Contains("TransactionType"))
                                {
                                    var dc = new DataColumn("TransactionType", typeof(string));
                                    //       dc.DefaultValue = string.Empty;
                                    ds.Tables[0].Columns.Add(dc);
                                }

                                foreach (DataRow row in ds.Tables[0].Rows)
                                {
                                    string instName = string.Empty;

                                    var inst = orgCM.Institution.Where(var => var.ID == new Guid(row["InstitutionID"].ToString())).FirstOrDefault();
                                    if (inst != null)
                                        instName = inst.Name;

                                    row["ProductType"] = "TERM DEPOSIT";
                                    row["BSB"] = bankAccount.BankAccountEntity.BSB;
                                    row["AccountType"] = bankAccount.BankAccountEntity.AccoutType.ToUpper();
                                    row["AccountNo"] = bankAccount.BankAccountEntity.AccountNumber;
                                    row["AmountTotal"] = bankAccount.BankAccountEntity.CashManagementTransactions.Where(cashTran => cashTran.ID == new Guid(row["ID"].ToString())).FirstOrDefault().TotalAmount;
                                    row["BankAmount"] = bankAccount.BankAccountEntity.CashManagementTransactions.Where(cashTran => cashTran.ID == new Guid(row["ID"].ToString())).FirstOrDefault().Amount;
                                    row["BankAdjustment"] = bankAccount.BankAccountEntity.CashManagementTransactions.Where(cashTran => cashTran.ID == new Guid(row["ID"].ToString())).FirstOrDefault().Adjustment;
                                    row["InstituteName"] = instName;
                                    row["BankMaturityDate"] = DateTime.Parse(row["MaturityDate"].ToString());
                                    row["BankTransactionDate"] = DateTime.Parse(row["TransactionDate"].ToString());
                                    row["BankCID"] = bankAccount.CID;
                                    row["AccountTier"] = bankAccount.Name;
                                    row["BrokerID"] = Guid.Empty;
                                    row["BrokerName"] = string.Empty;
                                    if (bankAccount.BankAccountEntity.BrokerID != null)
                                    {
                                        inst = orgCM.Institution.Where(ss => ss.Name.ToLower() == bankAccount.BankAccountEntity.BrokerID.Name.ToLower()).FirstOrDefault();
                                        if (inst != null)
                                        {
                                            row["BrokerID"] = inst.ID;
                                            row["BrokerName"] = inst.Name;
                                        }
                                    }
                                }

                                tdTransactionDS.Merge(ds, false, MissingSchemaAction.Add);
                            }
                        }
                    }
                }
            }
        }

        public void GetASXTransactionDS(ASXTransactionDS asxTransactionDS, IOrganization orgCM, List<Oritax.TaxSimp.Common.IdentityCMDetail> DesktopBrokerAccounts, ObservableCollection<DividendEntity> DividendCollection, ICMBroker Broker, string clientID)
        {
            DataTable dtaccounts = new DataTable("Accounts");
            dtaccounts.Columns.Add("ID");
            dtaccounts.Columns.Add("HINNumber");
            dtaccounts.Columns.Add("AccountNo");

            DataTable dt = new DataTable("DesktopBroker Transactions");
            dt.Columns.Add("InvestmentCode");
            dt.Columns.Add("Name");
            dt.Columns.Add("TransactionType");
            dt.Columns.Add("AccountNo");
            dt.Columns.Add("FPSInstructionID");
            dt.Columns.Add("TradeDate", typeof(DateTime));
            dt.Columns.Add("SettlementDate", typeof(DateTime));
            dt.Columns.Add("Units");
            dt.Columns.Add("GrossValue", typeof(decimal));
            dt.Columns.Add("UnitPrice", typeof(decimal));
            dt.Columns.Add("BrokerageAmount", typeof(decimal));
            dt.Columns.Add("Charges", typeof(decimal));
            dt.Columns.Add("BrokerageGST", typeof(decimal));
            dt.Columns.Add("NetValue", typeof(decimal));
            dt.Columns.Add("Narrative");
            dt.Columns.Add("TranstactionUniqueID", typeof(Guid));
            dt.Columns.Add("ASXCID", typeof(Guid));
            dt.Columns.Add("ProductType");
            dt.Columns.Add("DividendID", typeof(Guid));
            dt.Columns.Add("DividendStatus", typeof(string));


            DataTable divTable = new DIVTransactionDS().Tables[DIVTransactionDS.INCOMESTRANSACTIONTABLE].Copy();

            foreach (var desktopBrokerAccount in DesktopBrokerAccounts)
            {
                DesktopBrokerAccountCM desktopBrokerAccountCM = Broker.GetCMImplementation(desktopBrokerAccount.Clid, desktopBrokerAccount.Csid) as DesktopBrokerAccountCM;
                if (desktopBrokerAccountCM != null)
                {
                    DataRow accrow = dtaccounts.NewRow();
                    accrow["ID"] = desktopBrokerAccountCM.CID.ToString();
                    accrow["HINNumber"] = desktopBrokerAccountCM.DesktopBrokerAccountEntity.HINNumber.ToString();
                    accrow["AccountNo"] = desktopBrokerAccountCM.DesktopBrokerAccountEntity.AccountNumber.ToString();
                    dtaccounts.Rows.Add(accrow);

                    if (desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions.Count > 0)
                    {
                        foreach (HoldingTransactions holdingTransactions in desktopBrokerAccountCM.DesktopBrokerAccountEntity.holdingTransactions)
                        {
                            DataRow row = dt.NewRow();

                            row["UnitPrice"] = holdingTransactions.UnitPrice;
                            row["TranstactionUniqueID"] = holdingTransactions.TranstactionUniqueID;
                            row["ASXCID"] = desktopBrokerAccountCM.CID;
                            row["Name"] = desktopBrokerAccountCM.DesktopBrokerAccountEntity.Name;
                            row["AccountNo"] = desktopBrokerAccountCM.DesktopBrokerAccountEntity.AccountNumber;
                            row["FPSInstructionID"] = holdingTransactions.FPSInstructionID;
                            row["InvestmentCode"] = holdingTransactions.InvestmentCode;
                            row["TransactionType"] = holdingTransactions.TransactionType;
                            row["TradeDate"] = holdingTransactions.TradeDate;
                            if (holdingTransactions.SettlementDate != null)
                                row["SettlementDate"] = holdingTransactions.SettlementDate;
                            else
                                row["SettlementDate"] = holdingTransactions.TradeDate;
                            row["Units"] = holdingTransactions.Units;
                            row["GrossValue"] = holdingTransactions.GrossValue;
                            row["BrokerageAmount"] = holdingTransactions.BrokerageAmount;
                            row["Charges"] = holdingTransactions.Charges;
                            row["BrokerageGST"] = holdingTransactions.BrokerageGST;
                            row["BrokerageAmount"] = holdingTransactions.BrokerageAmount;
                            if (holdingTransactions.AdjustmentTransactions != null && holdingTransactions.AdjustmentTransactions.Count > 0)
                                row["NetValue"] = holdingTransactions.AdjustmentTransactions.Sum(asx => asx.NetValue);
                            else
                                row["NetValue"] = holdingTransactions.NetValue;
                            row["Narrative"] = holdingTransactions.Narrative;
                            row["ProductType"] = "SEC";
                            row["DividendID"] = holdingTransactions.DividendID;
                            row["DividendStatus"] = holdingTransactions.DividendStatus;

                            dt.Rows.Add(row);

                            if (holdingTransactions.DividendID != Guid.Empty)
                            {
                                var diventity = DividendCollection.FirstOrDefault(ss => ss.ID == holdingTransactions.DividendID);
                                if (diventity != null)
                                {
                                    AddDividendtoTable(clientID, diventity, divTable);

                                }

                            }
                        }
                    }
                }
            }

            asxTransactionDS.Merge(dt, false, MissingSchemaAction.Add);
            asxTransactionDS.Merge(dtaccounts, false, MissingSchemaAction.Add);
            asxTransactionDS.Merge(divTable, false, MissingSchemaAction.Add);
        }

        public void GetClientDistributionsDS(ClientDistributionsDS DS, ObservableCollection<DistributionIncomeEntity> distributionIncomeEntities, ICMBroker Broker, string clientID, Guid clientCID, string clientName)
        {
            DataTable dt = new DataTable(ClientDistributionsDS.CLIENTDISTRIBUTIONTABLE);
            dt.Columns.Add("ID", typeof(Guid));
            dt.Columns.Add("ClientCID", typeof(Guid));
            dt.Columns.Add("ClientID", typeof(string));
            dt.Columns.Add("ClientName", typeof(string));
            dt.Columns.Add("FundCode", typeof(string));
            dt.Columns.Add("ProductType", typeof(string));
            dt.Columns.Add("FundName", typeof(string));
            dt.Columns.Add("RecodDate_Shares", typeof(double));
            dt.Columns.Add("RecordDate", typeof(DateTime));
            dt.Columns.Add("PaymentDate", typeof(DateTime));
            dt.Columns.Add("NetCashDistribution", typeof(double));
            dt.Columns.Add("GrossCashDistribution", typeof(double));
            dt.Columns.Add("Status", typeof(string));
            dt.Columns.Add("MaunuallyAdjusted", typeof(bool));

            if (distributionIncomeEntities != null)
            {
                var org = Broker.GetWellKnownBMC(WellKnownCM.Organization);
                var misDs = new ManagedInvestmentAccountDS
                {
                    Command = (int)WebCommands.GetOrganizationUnitsByType,
                    Unit = new Oritax.TaxSimp.Data.OrganizationUnit
                    {
                        CurrentUser = new UserEntity() { CurrentUserName = "Administrator" },
                        Type = ((int)OrganizationType.ManagedInvestmentSchemesAccount).ToString()
                    }
                };
                org.GetData(misDs);
                Broker.ReleaseBrokerManagedComponent(org);



                foreach (var distributionIncomeEntity in distributionIncomeEntities)
                {
                    //Filtering out records having 0 values for NetCashDistribution and NetGrossDistribution
                    //if (distributionIncomeEntity.NetCashDistribution == 0 && distributionIncomeEntity.NetGrossDistribution == 0) continue;

                    var drs = misDs.FundAccountsTable.Select(string.Format("{0}='{1}'", misDs.FundAccountsTable.FUNDID, distributionIncomeEntity.FundID));
                    if (drs.Length > 0)
                    {
                        distributionIncomeEntity.FundCode = drs[0][misDs.FundAccountsTable.FUNDCODE].ToString();
                        distributionIncomeEntity.FundName = drs[0][misDs.FundAccountsTable.FUNDDESCRIPTION].ToString();

                    }
                    DataRow row = dt.NewRow();
                    row["FundCode"] = distributionIncomeEntity.FundCode;
                    row["ProductType"] = "DISTRIBUTION INCOME";
                    row["ID"] = distributionIncomeEntity.ID;
                    row["FundName"] = distributionIncomeEntity.FundName;
                    row["RecodDate_Shares"] = distributionIncomeEntity.RecodDate_Shares;
                    row["RecordDate"] = distributionIncomeEntity.RecordDate;
                    row["PaymentDate"] = distributionIncomeEntity.PaymentDate;
                    row["NetCashDistribution"] = distributionIncomeEntity.NetCashDistribution;
                    row["GrossCashDistribution"] = distributionIncomeEntity.NetGrossDistribution;
                    row["ClientCID"] = clientCID;
                    row["ClientID"] = clientID;
                    row["ClientName"] = clientName;
                    row["Status"] = distributionIncomeEntity.Status;
                    row["MaunuallyAdjusted"] = distributionIncomeEntity.IsManualUpdated;

                    dt.Rows.Add(row);
                }


                var groupByFundCode = distributionIncomeEntities.GroupBy(dis => dis.FundCode);

                foreach (var disEntityGroup in groupByFundCode)
                {
                    var disEntity = disEntityGroup.FirstOrDefault();

                    DataRow row = DS.Tables[ClientDistributionsDS.INCOMETRANMAPTABLE].NewRow();
                    row[ClientDistributionsDS.INVESTMENTCODE] = disEntity.FundCode;
                    row[ClientDistributionsDS.BGLCODE] = disEntity.BGLCODE;
                    row[UMABaseDS.INVESTMENTTYPE] = "Managed Funds";
                    row[UMABaseDS.BGLDEFAULTCODE] = "238";
                    row[UMABaseDS.INVESTMENTPROVIDER] = "e-Clipse Online";
                    row[UMABaseDS.INVESTMENTACCOUNTFILTERTYPE] = "Distributions from Managed Funds";
                    row[UMABaseDS.INVESTMENTNAME] = disEntity.FundName;
                    row[UMABaseDS.INVESTMENTCOATYPE] = "Distributions";

                    DS.Tables[ClientDistributionsDS.INCOMETRANMAPTABLE].Rows.Add(row);
                }
            }

            DS.Merge(dt, false, MissingSchemaAction.Add);
        }

        public void GetClientDistributionsDS(ClientTaxDistributionsDS DS, ObservableCollection<DistributionIncomeEntity> distributionIncomeEntities, ICMBroker Broker)
        {
            DataTable dt = new DataTable(ClientTaxDistributionsDS.CLIENTDISTRIBUTIONTABLE);
            dt.Columns.Add("FundCode", typeof(string));
            dt.Columns.Add("FundName", typeof(string));
            dt.Columns.Add("FinYearEndDate", typeof(string));
            dt.Columns.Add("FinYearStartDate", typeof(string));
            dt.Columns.Add("NetCashDistribution", typeof(double));
            if (distributionIncomeEntities != null)
            {
                var orderedDistributionIncomeEntities = distributionIncomeEntities.OrderBy(dis => dis.RecordDate);
                var groupByFundCodes = orderedDistributionIncomeEntities.GroupBy(grp => grp.FundCode);

                foreach (var fundCode in groupByFundCodes)
                {
                    AccountingFinancialYear accountingFinancialYear = new AccountingFinancialYear(DateTime.Now.AddYears(1));
                    AccountingFinancialYear accountingFinancialYearLastYear = new AccountingFinancialYear(DateTime.Now);

                    var initialFilteredList = fundCode.Where(dis => dis.RecordDate <= accountingFinancialYearLastYear.FinYearEndDate && dis.RecordDate >= accountingFinancialYearLastYear.FinYearStartDate);

                    if (initialFilteredList.Count() > 0)
                    {
                        double netCashDistribution = initialFilteredList.Sum(dis => dis.NetCashDistribution).Value;

                        DataRow row = dt.NewRow();

                        if (netCashDistribution != 0)
                        {
                            row["FundCode"] = initialFilteredList.FirstOrDefault().FundCode;
                            row["FundName"] = initialFilteredList.FirstOrDefault().FundName;
                            row["FinYearEndDate"] = accountingFinancialYearLastYear.FinYearEndDate.ToString("dd/MM/yyyy");
                            row["FinYearStartDate"] = accountingFinancialYearLastYear.FinYearStartDate.ToString("dd/MM/yyyy");
                            row["NetCashDistribution"] = initialFilteredList.Sum(dis => dis.NetCashDistribution);
                            dt.Rows.Add(row);

                            SetTaxTable(DS, accountingFinancialYearLastYear, initialFilteredList);
                        }

                        bool process = true;

                        while (process)
                        {
                            AccountingFinancialYear nextFinYear = accountingFinancialYearLastYear.NextFinancialYear();
                            var filteredList = fundCode.Where(dis => dis.RecordDate <= nextFinYear.FinYearEndDate && dis.RecordDate >= nextFinYear.FinYearStartDate);

                            if (filteredList.Count() > 0)
                            {
                                netCashDistribution = initialFilteredList.Sum(dis => dis.NetCashDistribution).Value;

                                if (netCashDistribution != 0)
                                {

                                    row = dt.NewRow();
                                    row["FundCode"] = filteredList.FirstOrDefault().FundCode;
                                    row["FundName"] = filteredList.FirstOrDefault().FundName;
                                    row["FinYearEndDate"] = nextFinYear.FinYearEndDate.ToString("dd/MM/yyyy");
                                    row["FinYearStartDate"] = nextFinYear.FinYearStartDate.ToString("dd/MM/yyyy");
                                    row["NetCashDistribution"] = filteredList.Sum(dis => dis.NetCashDistribution);
                                    dt.Rows.Add(row);

                                    SetTaxTable(DS, nextFinYear, filteredList);
                                }
                            }

                            if (nextFinYear.FinYearStartDate == accountingFinancialYear.FinYearStartDate && nextFinYear.FinYearEndDate == accountingFinancialYear.FinYearEndDate)
                                process = false;
                        }

                    }
                }

            }

            DS.Merge(dt, false, MissingSchemaAction.Add);


        }

        private static void SetTaxTable(ClientTaxDistributionsDS DS, AccountingFinancialYear accountingFinancialYear, IEnumerable<DistributionIncomeEntity> initialFilteredList)
        {
            #region Tax Labels

            var ausIncomeComponents = from component in initialFilteredList
                                      select new
                                      {
                                          DistributionComponents = component.Components.Where(comp => comp.Category == ComponentCategory.Australian_Income),
                                      };

            ObservableCollection<DistributionComponent> ausComponents = new ObservableCollection<DistributionComponent>();

            foreach (var comp in ausIncomeComponents)
            {
                foreach (DistributionComponent disComp in comp.DistributionComponents)
                {
                    ausComponents.Add(disComp);
                }
            }


            var foreignIncomeComponents = from component in initialFilteredList
                                          select new
                                          {
                                              DistributionComponents = component.Components.Where(comp => comp.Category == ComponentCategory.Foreign_Income),
                                          };

            ObservableCollection<DistributionComponent> foreignComponents = new ObservableCollection<DistributionComponent>();

            foreach (var comp in foreignIncomeComponents)
            {
                foreach (DistributionComponent disComp in comp.DistributionComponents)
                {
                    foreignComponents.Add(disComp);
                }
            }


            var capitalGains = from component in initialFilteredList
                               select new
                               {
                                   DistributionComponents = component.Components.Where(comp => comp.Category == ComponentCategory.Capital_Gains),
                               };

            ObservableCollection<DistributionComponent> capitalGainsComponents = new ObservableCollection<DistributionComponent>();

            foreach (var comp in capitalGains)
            {
                foreach (DistributionComponent disComp in comp.DistributionComponents)
                {
                    capitalGainsComponents.Add(disComp);
                }
            }


            var nonassComponents = from component in initialFilteredList
                                   select new
                                   {
                                       DistributionComponents = component.Components.Where(comp => comp.Category == ComponentCategory.Other_Non_Assessable_Amount),
                                   };

            ObservableCollection<DistributionComponent> nonassComponentssComponents = new ObservableCollection<DistributionComponent>();

            foreach (var comp in nonassComponents)
            {
                foreach (DistributionComponent disComp in comp.DistributionComponents)
                {
                    nonassComponentssComponents.Add(disComp);
                }
            }


            DataTable taxTable = DS.Tables[ClientTaxDistributionsDS.DISTAXTABLE];

            double amount13U = ausComponents.Sum(ausInc => ausInc.GrossDistribution) + foreignComponents.Where(forgn => forgn.ComponentType == 899).Sum(comp => comp.GrossDistribution);
            double amount13Q = ausComponents.Sum(comp => comp.Autax_Credit.HasValue ? comp.Autax_Credit.Value : 0);
            double amount18H = capitalGainsComponents.Sum(capInc => capInc.GrossDistribution);
            double amount18A = capitalGainsComponents.Sum(capInc => capInc.GrossDistribution);
            double amount20E = foreignComponents.Sum(forgn => forgn.GrossDistribution);
            double amount20M = foreignComponents.Sum(forgn => forgn.GrossDistribution);
            double amount20F = foreignComponents.Where(forgn => forgn.ComponentType == 899).Sum(comp => comp.GrossDistribution);
            double amount20O = foreignComponents.Sum(comp => comp.Autax_Credit.HasValue ? comp.Autax_Credit.Value : 0);

            AddTaxLabels20112012(taxTable, 1, "Non-Primary Production Income", "13U", amount13U, initialFilteredList.FirstOrDefault().FundCode, initialFilteredList.FirstOrDefault().FundName, accountingFinancialYear.FinYearStartDate, accountingFinancialYear.FinYearEndDate);
            AddTaxLabels20112012(taxTable, 2, "Other Deductions Relating to Distributions", "13Y", 0, initialFilteredList.FirstOrDefault().FundCode, initialFilteredList.FirstOrDefault().FundName, accountingFinancialYear.FinYearStartDate, accountingFinancialYear.FinYearEndDate);
            AddTaxLabels20112012(taxTable, 3, "Franking Credits", "13Q", amount13Q, initialFilteredList.FirstOrDefault().FundCode, initialFilteredList.FirstOrDefault().FundName, accountingFinancialYear.FinYearStartDate, accountingFinancialYear.FinYearEndDate);
            AddTaxLabels20112012(taxTable, 4, "Credit for TFN Amounts Withheld", "13R", 0, initialFilteredList.FirstOrDefault().FundCode, initialFilteredList.FirstOrDefault().FundName, accountingFinancialYear.FinYearStartDate, accountingFinancialYear.FinYearEndDate);
            AddTaxLabels20112012(taxTable, 5, "Credit for Tax Paid by Trustee", "13S", 0, initialFilteredList.FirstOrDefault().FundCode, initialFilteredList.FirstOrDefault().FundName, accountingFinancialYear.FinYearStartDate, accountingFinancialYear.FinYearEndDate);
            AddTaxLabels20112012(taxTable, 6, "Total Current Year Capital Gains", "18H", amount18H, initialFilteredList.FirstOrDefault().FundCode, initialFilteredList.FirstOrDefault().FundName, accountingFinancialYear.FinYearStartDate, accountingFinancialYear.FinYearEndDate);
            AddTaxLabels20112012(taxTable, 7, "Net Capital Gains", "18A", amount18A, initialFilteredList.FirstOrDefault().FundCode, initialFilteredList.FirstOrDefault().FundName, accountingFinancialYear.FinYearStartDate, accountingFinancialYear.FinYearEndDate);
            AddTaxLabels20112012(taxTable, 8, "Assessable Foreign Source Income", "20E", amount20E, initialFilteredList.FirstOrDefault().FundCode, initialFilteredList.FirstOrDefault().FundName, accountingFinancialYear.FinYearStartDate, accountingFinancialYear.FinYearEndDate);
            AddTaxLabels20112012(taxTable, 9, "Other Net Foreign Source Income", "20M", amount20M, initialFilteredList.FirstOrDefault().FundCode, initialFilteredList.FirstOrDefault().FundName, accountingFinancialYear.FinYearStartDate, accountingFinancialYear.FinYearEndDate);
            AddTaxLabels20112012(taxTable, 10, "Australian Franking Credits from a New Zealand Company", "20F", amount20F, initialFilteredList.FirstOrDefault().FundCode, initialFilteredList.FirstOrDefault().FundName, accountingFinancialYear.FinYearStartDate, accountingFinancialYear.FinYearEndDate);
            AddTaxLabels20112012(taxTable, 11, "Foreign Income Tax Offsets", "20O", amount20O, initialFilteredList.FirstOrDefault().FundCode, initialFilteredList.FirstOrDefault().FundName, accountingFinancialYear.FinYearStartDate, accountingFinancialYear.FinYearEndDate);

            #endregion
        }

        private static void AddTaxLabels20112012(DataTable taxTable, int orderNO, string taxLabelDesc, string taxLabel, double amount, string fundCode, string fundName, DateTime sDate, DateTime eDate)
        {
            DataRow taxRow = taxTable.NewRow();
            taxRow[ClientTaxDistributionsDS.LABELORDERNO] = orderNO;
            taxRow[ClientTaxDistributionsDS.TAXLABELDESC] = taxLabelDesc;
            taxRow[ClientTaxDistributionsDS.TAXLABEL] = taxLabel;
            taxRow[ClientTaxDistributionsDS.AMOUNT] = amount;
            taxRow[ClientTaxDistributionsDS.FUNDCODE] = fundCode;
            taxRow[ClientTaxDistributionsDS.FUNDNAME] = fundName;
            taxRow[ClientTaxDistributionsDS.STARTDATE] = sDate;
            taxRow[ClientTaxDistributionsDS.ENDDATE] = eDate;

            taxTable.Rows.Add(taxRow);
        }


        public void GetClientDistributionDS(ClientDistributionDetailsDS DS, ObservableCollection<DistributionIncomeEntity> distributionIncomeEntities, ICMBroker Broker)
        {

            if (distributionIncomeEntities != null)
            {
                var entity = distributionIncomeEntities.Where(ss => ss.ID == DS.ID).FirstOrDefault();
                if (entity != null)
                {
                    DS.Entity = entity;
                }
            }


        }
        public void UpdateClientDistribution(ClientDistributionDetailsDS DS, ObservableCollection<DistributionIncomeEntity> distributionIncomes, ICMBroker broker, List<IdentityCMDetail> bankAccounts, List<IdentityCMDetail> MISAccounts, string clientID)
        {
            if (distributionIncomes != null)
            {
                var entity = distributionIncomes.Where(ss => ss.ID == DS.ID).FirstOrDefault();
                if (entity != null)
                {
                    DS.Entity.RecodDate_Shares = DS.UnitsHeld;
                    entity.ManuallyUpdateValues(DS.Entity);
                    if (!ClientUtilities.MatchDisWithBankTran(bankAccounts, entity, broker))
                    {
                        ClientUtilities.MatchDisWithMISTran(MISAccounts, entity, clientID, broker);
                    }
                }
            }
        }
      
        public void GetClientDividendDS(ClientDividendDetailsDS DS, ObservableCollection<DividendEntity> DividendIncomeEntities, ICMBroker Broker)
        {

            if (DividendIncomeEntities != null)
            {
                var entity = DividendIncomeEntities.Where(ss => ss.ID == DS.ID).FirstOrDefault();
                if (entity != null)
                {
                    DS.Entity = entity;
                }
            }
        }

        public void GetMISTransactionDS(MISTransactionDS misTransactionDS, IOrganization orgCM, List<AccountProcessTaskEntity> ListAccountProcess, ICMBroker Broker, string clientID)
        {
            DataSet ds = new DataSet();
            ds.Tables.Add(MISTransactionDS.MISTRANTABLE);

            ds.Tables[0].Columns.Add("Code");
            ds.Tables[0].Columns.Add("ProductType", typeof(string));
            ds.Tables[0].Columns.Add("MISCID");
            ds.Tables[0].Columns.Add("CLIENTID");
            ds.Tables[0].Columns.Add("Description");
            ds.Tables[0].Columns.Add("ID");
            ds.Tables[0].Columns.Add("TradeDate", typeof(DateTime));
            ds.Tables[0].Columns.Add("ClientID");
            ds.Tables[0].Columns.Add("TransactionType");
            ds.Tables[0].Columns.Add("Shares");
            ds.Tables[0].Columns.Add("UnitPrice", typeof(decimal));
            ds.Tables[0].Columns.Add("Amount", typeof(decimal));
            ds.Tables[0].Columns.Add("Status");
            ds.Tables[0].Columns.Add("PriceType");
            ds.Tables[0].Columns.Add("DistributionStatus");

            foreach (var accountProcessTaskEntity in ListAccountProcess)
            {
                if (accountProcessTaskEntity.LinkedEntity != null && accountProcessTaskEntity.LinkedEntity.Clid != Guid.Empty)
                {
                    var asset = orgCM.Assets.Where(var => var.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                    var model = orgCM.Model.Where(mod => mod.ID == accountProcessTaskEntity.ModelID).FirstOrDefault();
                    var linkedAsset = model.Assets.Where(ast => ast.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                    var product = linkedAsset.Products.Where(pro => pro.ID == accountProcessTaskEntity.TaskID).FirstOrDefault();

                    if (product != null)
                    {
                        if (accountProcessTaskEntity.LinkedEntity.Clid == Guid.Empty || accountProcessTaskEntity.LinkedEntity.Csid == Guid.Empty)
                        {
                            accountProcessTaskEntity.LinkedEntity.Clid = product.EntityId.Clid;
                            accountProcessTaskEntity.LinkedEntity.Csid = product.EntityId.Csid;
                        }

                        if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.ManagedInvestmentSchemesAccount)
                        {
                            ManagedInvestmentSchemesAccountCM managedInvestmentSchemesAccountCM = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as ManagedInvestmentSchemesAccountCM;

                            var linkedMISAccount = managedInvestmentSchemesAccountCM.ManagedInvestmentSchemesAccountEntity.FundAccounts.Where(fa => product.TaskDescription.Contains(fa.Code)).FirstOrDefault();
                            var filteredHoldingTransactions = linkedMISAccount.FundTransactions.Where(cliFundTrans => cliFundTrans.ClientID == clientID);


                            foreach (MISFundTransactionEntity misFundTransactionEntity in filteredHoldingTransactions)
                            {
                                DataRow row = ds.Tables[0].NewRow();
                                row["ProductType"] = "FUND";
                                row["Code"] = linkedMISAccount.Code;
                                row["ClientID"] = misFundTransactionEntity.ClientID;
                                row["MISCID"] = managedInvestmentSchemesAccountCM.CID.ToString();
                                row["Description"] = linkedMISAccount.Description;
                                row["ID"] = misFundTransactionEntity.ID;
                                row["TradeDate"] = misFundTransactionEntity.TradeDate;
                                row["ClientID"] = misFundTransactionEntity.ClientID;
                                row["TransactionType"] = misFundTransactionEntity.TransactionType;
                                row["Shares"] = misFundTransactionEntity.Shares;
                                row["UnitPrice"] = Math.Round(misFundTransactionEntity.UnitPrice, 6);
                                row["Amount"] = misFundTransactionEntity.Amount;
                                row["Status"] = misFundTransactionEntity.Status;
                                row["PriceType"] = misFundTransactionEntity.Status;
                                row["DistributionStatus"] = misFundTransactionEntity.DistributionStatus;
                                ds.Tables[0].Rows.Add(row);
                            }


                        }
                    }
                }
            }

            misTransactionDS.Merge(ds, false, MissingSchemaAction.Add);
        }

        public void GetDIVTransactionDS(DIVTransactionDS divTransactionDS, IOrganization orgCM, ObservableCollection<DividendEntity> DividendCollection, ICMBroker Broker, string clientID)
        {
            if (DividendCollection != null)
            {
                foreach (DividendEntity dividendEntity in DividendCollection)
                    AddDividendtoTable(clientID, dividendEntity, divTransactionDS.Tables[DIVTransactionDS.INCOMESTRANSACTIONTABLE]);

                var dividendEntityGroupByInvestCode = DividendCollection.GroupBy(div => div.InvestmentCode);

                foreach (var dividendEntityGroup in dividendEntityGroupByInvestCode)
                {
                    var divEntity = dividendEntityGroup.FirstOrDefault();

                    DataRow row = divTransactionDS.Tables[DIVTransactionDS.INCOMETRANMAPTABLE].NewRow();
                    row[UMABaseDS.INVESTMENTCODE] = divEntity.InvestmentCode;
                    row[UMABaseDS.BGLCODE] = divEntity.BGLCode;
                    row[UMABaseDS.BGLDEFAULTCODE] = "239";
                    row[UMABaseDS.INVESTMENTPROVIDER] = "Desktop Broker";
                    row[UMABaseDS.INVESTMENTACCOUNTFILTERTYPE] = "Dividends from various Securities";
                    row[UMABaseDS.INVESTMENTTYPE] = "Securities";
                    row[UMABaseDS.INVESTMENTCOATYPE] = "Dividends";

                    var secEntity = orgCM.Securities.Where(c => c.AsxCode == divEntity.InvestmentCode).FirstOrDefault();

                    if (secEntity != null)
                        row[BankAccountDS.INVESTMENTNAME] = secEntity.CompanyName;
                    else
                        row[BankAccountDS.INVESTMENTNAME] = divEntity.InvestmentCode;

                    divTransactionDS.Tables[DIVTransactionDS.INCOMETRANMAPTABLE].Rows.Add(row);
                }
            }
        }

        private static void AddDividendtoTable(string clientID, DividendEntity dividendEntity, DataTable dt)
        {
            DataRow row = dt.NewRow();
            row[DIVTransactionDS.ID] = dividendEntity.ID;
            row[DIVTransactionDS.CLIENTID] = clientID;
            row[DIVTransactionDS.PRODUCTTYPE] = "DIVIDEND INCOME";
            row[DIVTransactionDS.INVESTMENTCODE] = dividendEntity.InvestmentCode;
            row[DIVTransactionDS.TRANSACTIONTYPE] = dividendEntity.TransactionType;
            row[DIVTransactionDS.RECORDDATE] = dividendEntity.RecordDate;
            row[DIVTransactionDS.PAYMENTDATE] = dividendEntity.PaymentDate;
            row[DIVTransactionDS.UNITSONHAND] = dividendEntity.UnitsOnHand;
            row[DIVTransactionDS.CENTSPERSHARE] = dividendEntity.CentsPerShare;
            row[DIVTransactionDS.INVESTMENTAMOUNT] = dividendEntity.InvestmentAmount;
            row[DIVTransactionDS.FRANKEDAMOUNT] = dividendEntity.FrankedAmount;
            row[DIVTransactionDS.UNFRANKEDAMOUNT] = dividendEntity.UnfrankedAmount;

            if (dividendEntity.TransactionType == "Interest Income Accrual")
                row[DIVTransactionDS.PAIDDIVIDEND] = 0;
            else
                row[DIVTransactionDS.PAIDDIVIDEND] = dividendEntity.PaidDividend;
            row[DIVTransactionDS.INTERESTPAID] = dividendEntity.InterestPaid;
            row[DIVTransactionDS.INTERESTRATE] = dividendEntity.InterestRate;

            row[DIVTransactionDS.FRANKINGCREDITS] = dividendEntity.FrankingCredit;
            row[DIVTransactionDS.TOTALFRANKINGCREDITS] = dividendEntity.TotalFrankingCredit;

            if (dividendEntity.BalanceDate.HasValue)
                row[DIVTransactionDS.BALANCEDATE] = dividendEntity.BalanceDate;
            if (dividendEntity.BooksCloseDate.HasValue)
                row[DIVTransactionDS.BOOKSCLOSEDATE] = dividendEntity.BooksCloseDate;
            row[DIVTransactionDS.DIVIDENDTYPE] = dividendEntity.Dividendtype;
            row[DIVTransactionDS.CURRENCY] = dividendEntity.Currency;
            row[DIVTransactionDS.STATUS] = dividendEntity.Status;
            row[DIVTransactionDS.BGLCODE] = dividendEntity.BGLCode;
            row[DIVTransactionDS.MANUALLY_ADJUSTED] = dividendEntity.IsManualUpdated;
            dt.Rows.Add(row);
        }

        public void GetManualTransactionDS(ManualTransactionDS manualTransactionDS, IOrganization orgCM, ObservableCollection<ClientManualAssetEntity> ClientManualAssetCollection, ICMBroker Broker)
        {

            DataTable dt = new DataTable("Manual Transactions");
            dt.Columns.Add("ID");
            dt.Columns.Add("ProductType");
            dt.Columns.Add("AdministrationSystem");
            dt.Columns.Add("InvestmentID", typeof(string));
            dt.Columns.Add("InvestmentCode", typeof(string));
            dt.Columns.Add("TransactionType", typeof(string));
            dt.Columns.Add("InvestmentDesc", typeof(string));
            dt.Columns.Add("SettlementDate", typeof(DateTime));
            dt.Columns.Add("TransactionDate", typeof(DateTime));
            dt.Columns.Add("UnitsOnHand", typeof(decimal));
            dt.Columns.Add("UnitPrice", typeof(decimal));
            dt.Columns.Add("Holding", typeof(decimal));
            dt.Columns.Add("PriceType", typeof(PriceType));

            if (ClientManualAssetCollection != null)
            {
                foreach (ClientManualAssetEntity entity in ClientManualAssetCollection)
                {
                    var manualAssetDetails = orgCM.ManualAsset.Where(manAsset => manAsset.ID == entity.InvestmentCode).FirstOrDefault();
                    var latestPrice = manualAssetDetails.ASXSecurity.OrderByDescending(unitprice => unitprice.Date).Where(unitprice => unitprice.Date <= entity.TransactionDate).FirstOrDefault();

                    decimal unitPrice = 0;

                    if (latestPrice != null)
                    {
                        if (entity.PriceType == PriceType.NavPrice)
                            unitPrice = Convert.ToDecimal(latestPrice.NavValue);
                        else if (entity.PriceType == PriceType.PurchasePrice)
                            unitPrice = Convert.ToDecimal(latestPrice.PurValue);
                        else if (entity.PriceType == PriceType.UnitPrice)
                            unitPrice = Convert.ToDecimal(latestPrice.UnitPrice);
                    }

                    DataRow row = dt.NewRow();

                    row["ID"] = entity.ID;
                    row["ProductType"] = "OTHER ASSET";
                    row["InvestmentID"] = entity.InvestmentCode.ToString();
                    row["TransactionDate"] = entity.TransactionDate;
                    row["TransactionType"] = entity.TransactionType;
                    row["SettlementDate"] = entity.SettlementDate;
                    row["AdministrationSystem"] = entity.AdministrationSystem;
                    row["InvestmentCode"] = manualAssetDetails.AsxCode;
                    row["InvestmentDesc"] = manualAssetDetails.CompanyName;
                    row["UnitsOnHand"] = entity.UnitsOnHand;
                    row["UnitPrice"] = unitPrice;
                    row["Holding"] = entity.UnitsOnHand * unitPrice;
                    row["PriceType"] = entity.PriceType;

                    dt.Rows.Add(row);
                }

                manualTransactionDS.Merge(dt, false, MissingSchemaAction.Add);
            }
        }

        public void ClearIV004Transactions(IOrganization orgCM, List<AccountProcessTaskEntity> ListAccountProcess, ICMBroker Broker, string clientID)
        {
            foreach (var accountProcessTaskEntity in ListAccountProcess)
            {
                if (accountProcessTaskEntity.LinkedEntity != null && accountProcessTaskEntity.LinkedEntity.Clid != Guid.Empty)
                {
                    var asset = orgCM.Assets.Where(var => var.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                    var model = orgCM.Model.Where(mod => mod.ID == accountProcessTaskEntity.ModelID).FirstOrDefault();
                    var linkedAsset = model.Assets.Where(ast => ast.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                    var product = linkedAsset.Products.Where(pro => pro.ID == accountProcessTaskEntity.TaskID).FirstOrDefault();

                    if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.ManagedInvestmentSchemesAccount)
                    {
                        ManagedInvestmentSchemesAccountCM managedInvestmentSchemesAccountCM = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as ManagedInvestmentSchemesAccountCM;

                        var linkedMISAccount = managedInvestmentSchemesAccountCM.ManagedInvestmentSchemesAccountEntity.FundAccounts.Where(fa => fa.Code == "IV004").FirstOrDefault();
                        if (linkedMISAccount != null)
                        {
                            linkedMISAccount.FundTransactions.Clear();
                            linkedMISAccount.SecurityHoldings.Clear();
                        }
                    }
                }
            }
        }

        public void SetManualTransactionDetail(ManualTransactionDetailsDS manualTransactionDetailsDS, ObservableCollection<ClientManualAssetEntity> ClientManualAssetCollection)
        {
            if (manualTransactionDetailsDS.DataSetOperationType == DataSetOperationType.UpdateSingle)
            {
                var ClientManualAsset = ClientManualAssetCollection.Where(manTran => manTran.ID == new Guid(manualTransactionDetailsDS.Tables[0].Rows[0][ManualTransactionDetailsDS.ID].ToString())).FirstOrDefault();
                ClientManualAsset.TransactionType = manualTransactionDetailsDS.Tables[0].Rows[0][ManualTransactionDetailsDS.TRANTYPE].ToString();
                ClientManualAsset.UnitsOnHand = Convert.ToDecimal(manualTransactionDetailsDS.Tables[0].Rows[0][ManualTransactionDetailsDS.UNITSONHAND]);
                ClientManualAsset.SettlementDate = (DateTime)manualTransactionDetailsDS.Tables[0].Rows[0][ManualTransactionDetailsDS.SETTLEDATE];
                ClientManualAsset.TransactionDate = (DateTime)manualTransactionDetailsDS.Tables[0].Rows[0][ManualTransactionDetailsDS.TRANDATE];
                ClientManualAsset.PriceType = (PriceType)manualTransactionDetailsDS.Tables[0].Rows[0][ManualTransactionDetailsDS.PRICETYPE];
            }
            else if (manualTransactionDetailsDS.DataSetOperationType == DataSetOperationType.DeletSingle)
            {
                var ClientManualAsset = ClientManualAssetCollection.Where(manTran => manTran.ID == new Guid(manualTransactionDetailsDS.Tables[0].Rows[0][ManualTransactionDetailsDS.ID].ToString())).FirstOrDefault();
                if (ClientManualAsset != null)
                    ClientManualAssetCollection.Remove(ClientManualAsset);
            }
            else if (manualTransactionDetailsDS.DataSetOperationType == DataSetOperationType.NewSingle)
            {
                ClientManualAssetEntity entity = new ClientManualAssetEntity();
                entity.ID = Guid.NewGuid();
                entity.InvestmentCode = (Guid)manualTransactionDetailsDS.Tables[0].Rows[0][ManualTransactionDetailsDS.INVESTID];
                entity.TransactionType = manualTransactionDetailsDS.Tables[0].Rows[0][ManualTransactionDetailsDS.TRANTYPE].ToString();
                entity.UnitsOnHand = Convert.ToDecimal(manualTransactionDetailsDS.Tables[0].Rows[0][ManualTransactionDetailsDS.UNITSONHAND]);
                entity.UnitsOnHand = Convert.ToDecimal(manualTransactionDetailsDS.Tables[0].Rows[0][ManualTransactionDetailsDS.UNITSONHAND]);
                entity.SettlementDate = (DateTime)manualTransactionDetailsDS.Tables[0].Rows[0][ManualTransactionDetailsDS.SETTLEDATE];
                entity.TransactionDate = (DateTime)manualTransactionDetailsDS.Tables[0].Rows[0][ManualTransactionDetailsDS.TRANDATE];
                entity.PriceType = (PriceType)manualTransactionDetailsDS.Tables[0].Rows[0][ManualTransactionDetailsDS.PRICETYPE];
                ClientManualAssetCollection.Add(entity);
            }
        }


    }
}