namespace Oritax.TaxSimp.CM.Group
{
    public class ClientIndividualCMInstall
	{
		#region INSTALLATION PROPERTIES

		// Assembly Installation Properties
        public const string ASSEMBLY_ID = "2A2A95AC-7933-40A6-B6BA-1597CB18014F";
        public const string ASSEMBLY_NAME = "ClientIndividual";
        public const string ASSEMBLY_DISPLAYNAME = "Individual";
		public const string ASSEMBLY_MAJORVERSION="1";
		public const string ASSEMBLY_MINORVERSION="0";
        public const string ASSEMBLY_DATAFORMAT="0"; 
		public const string ASSEMBLY_REVISION="0"; 

		// Component Installation Properties
        public const string COMPONENT_ID = "CC2A8EE2-F93F-4C4B-9662-8468EFDD8EE4";
        public const string COMPONENT_NAME = "ClientIndividual";
        public const string COMPONENT_DISPLAYNAME = "Individual";
		public const string COMPONENT_CATEGORY="BusinessEntity";

		// Component Version Installation Properties
		public const string COMPONENTVERSION_STARTDATE="01/01/1900 00:00:00 AM";
		public const string COMPONENTVERSION_ENDDATE="31/12/2050 12:59:59 AM";
		public const string COMPONENTVERSION_PERSISTERASSEMBLY="CommonPersisters_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";
		public const string COMPONENTVERSION_PERSISTERCLASS="Oritax.TaxSimp.CommonPersistence.GroupPersister";
        public const string COMPONENTVERSION_IMPLEMENTATIONCLASS = "Oritax.TaxSimp.CM.Group.ClientIndividualCM";
        	

		// Data Model
		public const string DATAMODEL_MANAGERASSEMBLY="CommonPersisters_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";
		public const string DATAMODEL_MANAGERCLASS="Oritax.TaxSimp.CommonPersistence.GroupPersister";

		#endregion
	}
}