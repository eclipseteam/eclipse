namespace Oritax.TaxSimp.CM.Group
{
    public class ClientSMSFIndividualTrusteeInstall
	{
		#region INSTALLATION PROPERTIES

		// Assembly Installation Properties
        public const string ASSEMBLY_ID = "7485432C-A44B-4E8B-AD73-9778E071DE2D";
        public const string ASSEMBLY_NAME = "ClientSMSFIndividualTrustee";
        public const string ASSEMBLY_DISPLAYNAME = "Client SMSF Individual Trustee";
		public const string ASSEMBLY_MAJORVERSION="1";
		public const string ASSEMBLY_MINORVERSION="0";
        public const string ASSEMBLY_DATAFORMAT="0"; 
		public const string ASSEMBLY_REVISION="0"; 

		// Component Installation Properties
        public const string COMPONENT_ID = "9CE651E3-1AED-4FD5-BD25-1F8C36A7B0B8";
        public const string COMPONENT_NAME = "ClientSMSFIndividualTrustee";
        public const string COMPONENT_DISPLAYNAME = "SMSF Individual Trustee";
		public const string COMPONENT_CATEGORY="BusinessEntity";

		// Component Version Installation Properties
		public const string COMPONENTVERSION_STARTDATE="01/01/1900 00:00:00 AM";
		public const string COMPONENTVERSION_ENDDATE="31/12/2050 12:59:59 AM";
		public const string COMPONENTVERSION_PERSISTERASSEMBLY="CommonPersisters_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";
		public const string COMPONENTVERSION_PERSISTERCLASS="Oritax.TaxSimp.CommonPersistence.GroupPersister";
        public const string COMPONENTVERSION_IMPLEMENTATIONCLASS = "Oritax.TaxSimp.CM.Group.ClientSMSFIndividualTrusteeCM";
        	

		// Data Model
		public const string DATAMODEL_MANAGERASSEMBLY="CommonPersisters_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";
		public const string DATAMODEL_MANAGERCLASS="Oritax.TaxSimp.CommonPersistence.GroupPersister";

		#endregion
	}
}
