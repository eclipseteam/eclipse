using System;
using System.Diagnostics;
using System.Configuration;
using System.Data.SqlClient;
using System.Reflection;
using System.Data;

using NUnit.Framework;

using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.DSCalculation;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.BusinessStructureUtilities;




namespace Oritax.TaxSimp.CalculationPersistSQL
{
	/// <summary>
	/// Summary description for MPPersisterTestFixture.
	/// </summary>
	/// 
	[TestFixture]
	public class MPPersisterTestFixture
	{
		SqlConnection sqlCon = null;

		[Conditional("DEBUG")] [SetUp] public void PersisterTestSetup()
		{
			string sConn = ConfigurationSettings.AppSettings["TX360ENTERPRISE_DBCONNECTIONSTRING"];
			sqlCon = new SqlConnection(sConn);
			sqlCon.Open();
		}

		[Conditional("DEBUG")] [TearDown] public void PersisterTestTearDown()
		{
			sqlCon.Close();
			sqlCon.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterConstructionTest()
		{
			MPPersister mpPersister = new MPPersister(sqlCon);

			FieldInfo dataAdaptInfo = mpPersister.GetType().GetField("dsGroupCommand", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlDataAdapter dsCommand = (SqlDataAdapter)dataAdaptInfo.GetValue(mpPersister);
			Assertion.AssertNotNull("SqlDataAdapter dsCommand is Null",dsCommand);
			Assertion.AssertNotNull("dsCommand.SelectCommand is Null",dsCommand.SelectCommand);
			Assertion.AssertNotNull("dsCommand.InsertCommand is Null",dsCommand.InsertCommand);
			Assertion.AssertNotNull("dsCommand.UpdateCommand is Null",dsCommand.UpdateCommand);
			Assertion.AssertNotNull("dsCommand.DeleteCommand is Null",dsCommand.DeleteCommand);


			FieldInfo sqlConnectionInfo = mpPersister.GetType().GetField("sqlConnection", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlConnection sqlConnection = (SqlConnection)sqlConnectionInfo.GetValue(mpPersister);
			Assertion.AssertEquals("Connection has not been set", sqlConnection,sqlCon);

			FieldInfo sqlTransactionInfo = mpPersister.GetType().GetField("sqlTransaction", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlTransaction sqlTransaction = (SqlTransaction)sqlTransactionInfo.GetValue(mpPersister);
			Assertion.AssertNull("sqlTransaction is not null", sqlTransaction);

			mpPersister.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterConnectionTest()
		{
			MPPersister mpPersister = new MPPersister(sqlCon);
			mpPersister.Connection = sqlCon;

			FieldInfo sqlConnectionInfo = mpPersister.GetType().GetField("sqlConnection", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlConnection sqlConnection = (SqlConnection)sqlConnectionInfo.GetValue(mpPersister);
			Assertion.AssertEquals("Connection has not been set", sqlConnection,sqlCon);

			mpPersister.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterTransactionTest()
		{
			MPPersister mpPersister = new MPPersister(sqlCon);
			SqlTransaction sqlTran = sqlCon.BeginTransaction();
			mpPersister.Transaction = sqlTran;
			FieldInfo sqlTransactionInfo = mpPersister.GetType().GetField("sqlTransaction", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlTransaction sqlTransaction = (SqlTransaction)sqlTransactionInfo.GetValue(mpPersister);
			Assertion.AssertEquals("sqlTransaction has not been set", sqlTransaction, sqlTran);
			sqlTran.Commit();
			sqlTran.Dispose();
			mpPersister.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterEstablishDBTest()
		{
			MPPersister mpPersister = new MPPersister(sqlCon);

			SqlTransaction sqlTran = sqlCon.BeginTransaction();
			mpPersister.Transaction = sqlTran;
			try
			{
				mpPersister.EstablishDatabase();
			}
			finally
			{
				sqlTran.Rollback();	//DO NOT CORRUPT THE DATABASE
			}
			mpPersister.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterClearDBTest()
		{
			MPPersister mpPersister = new MPPersister(sqlCon);

			SqlTransaction sqlTran = sqlCon.BeginTransaction();
			mpPersister.Transaction = sqlTran;
			try
			{
				mpPersister.ClearDatabase();
			}
			finally
			{
				sqlTran.Rollback();	//DO NOT CORRUPT THE DATABASE
			}
			mpPersister.Dispose();
		}
		[Conditional("DEBUG")] [Test] public void PersisterGetCMByInstanceIdTest()
		{

			//go get the first user Instance
			CMListPersist bmcList= new CMListPersist(sqlCon);
			DSBMCInfo data = (DSBMCInfo)bmcList.FindByTypeName("Organization");

			if(data.Tables[DSBMCInfo.CMLIST_TABLE].Rows.Count == 0)
			{
				throw new Exception("This Test requires an Grouped Organization instance");
			}
			DataRow firstRow=data.Tables[DSBMCInfo.CMLIST_TABLE].Rows[0];

			MPPersister mpPersister = new MPPersister(sqlCon);

			IBrokerManagedComponent ibmc = mpPersister.GetCMByInstanceId((Guid)firstRow[DSBMCInfo.CMID_FIELD]);
			Assertion.AssertNotNull("Could not find instance",ibmc);
			bmcList.Dispose();
			mpPersister.Dispose();
		}

#if false
		//need a broker to run this test
		[Conditional("DEBUG")] [Test] public void PersisterSaveCMTest()
		{

			//go get the first user Instance
			CMListPersist bmcList= new CMListPersist(sqlCon);
			DSBMCInfo data = (DSBMCInfo)bmcList.FindByTypeName("Organization");

			if(data.Tables[DSBMCInfo.CMLIST_TABLE].Rows.Count == 0)
			{
				throw new Exception("This Test requires an Grouped Organization instance");
			}
			DataRow firstRow=data.Tables[DSBMCInfo.CMLIST_TABLE].Rows[0];
			
			MPPersister mpPersister = new MPPersister(sqlCon);
			IBrokerManagedComponent ibmc = mpPersister.GetByInstanceId((Guid)firstRow[DSBMCInfo.CMID_FIELD]);

			SqlTransaction sqlTran = sqlCon.BeginTransaction();
			mpPersister.Transaction = sqlTran;
			try
			{
				mpPersister.SaveCM(ibmc);
			}
			finally
			{
				sqlTran.Rollback();	//DO NOT CORRUPT THE DATABASE
			}

			bmcList.Dispose();
			mpPersister.Dispose();
		}
#endif

		[Conditional("DEBUG")] [Test] public void PersisterDeleteTest()
		{

			//go get the first user Instance
			CMListPersist bmcList= new CMListPersist(sqlCon);
			DSBMCInfo data = (DSBMCInfo)bmcList.FindByTypeName("Organization");

			if(data.Tables[DSBMCInfo.CMLIST_TABLE].Rows.Count == 0)
			{
				throw new Exception("This Test requires an Organization instance");
			}
			DataRow firstRow=data.Tables[DSBMCInfo.CMLIST_TABLE].Rows[0];
			
			MPPersister mpPersister = new MPPersister(sqlCon);
			IBrokerManagedComponent ibmc = mpPersister.GetByInstanceId((Guid)firstRow[DSBMCInfo.CMID_FIELD]);

			SqlTransaction sqlTran = sqlCon.BeginTransaction();
			mpPersister.Transaction = sqlTran;
			try
			{
				mpPersister.Delete(ibmc);
			}
			finally
			{
				sqlTran.Rollback();	//DO NOT CORRUPT THE DATABASE
			}

			bmcList.Dispose();
			mpPersister.Dispose();
		}
	}
}
