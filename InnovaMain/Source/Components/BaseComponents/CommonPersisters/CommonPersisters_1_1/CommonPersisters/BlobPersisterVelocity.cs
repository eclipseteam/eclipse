using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using System.Collections.Generic;

using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.DSCalculation;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.VelocityCache;

namespace Oritax.TaxSimp.CommonPersistence
{
    public class BlobPersisterVelocity : CMPersisterVelocity, IDisposable, ICPersisterVelocity
    {
        private bool disposed = false;

        //private static VelocityCacheString _VelocityDataCache = null;

        //// As BlobPersisterVelocity is the only object which has access to BLOBPERSIST table in DB,
        //// there's no concurrency control issues here.
        //private static VelocityCacheString _DataCache
        //{
        //    get
        //    {
        //        if (_VelocityDataCache == null)
        //            _VelocityDataCache = new VelocityCacheString();

        //        return _VelocityDataCache;
        //    }
        //}

        private static List<string> BLOBPersistTableList = new List<string>();

        private const string BLOB_ID_PREFIX = "BT"; // Blob Table Cached in Velocity

        public BlobPersisterVelocity(SqlConnection connection, SqlTransaction transaction)
            : this(0, connection, transaction)
        {
        }

        public BlobPersisterVelocity(long blobSize, SqlConnection connection, SqlTransaction transaction)
            : base(connection, transaction)
        {
        }

        #region IDisposable Implementation ---------------------------------------

        /// <summary>
        ///		Performs application-defined tasks associated with freeing, releasing, 
        ///		or resetting unmanaged resources.
        /// </summary>
        public new void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        ///		Performs application-defined tasks associated with freeing, releasing, 
        ///		or resetting unmanaged resources.
        /// </summary>
        /// <param name="disposing">
        ///		True if called by the Dispose() method, false if called by the finalizer.
        /// </param>
        protected new void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    base.Dispose();
                }
                disposed = true;
            }
        }

        #endregion // IDisposable Implementation

        public override SqlConnection Connection
        {
            set
            {
                base.Connection = value;

            }
        }

        public override SqlTransaction Transaction
        {
            set
            {
                base.Transaction = value;
            }
        }

        public override IBrokerManagedComponent Get(Guid cID, ref DataSet primaryDS)
        {
            DSCalculationModule data = (DSCalculationModule)VelocityCacheString.VelocityCache.GetObejct(BLOB_ID_PREFIX + cID.ToString());

            MemoryStream memoryStream = new MemoryStream((byte[])data.Tables[DSCalculationModule.INSTANCE_TABLE].Rows[0][DSCalculationModule.PERSISTANT_DATA_FIELD]);
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            long size = memoryStream.Length;
            IBrokerManagedComponent iBMC = (IBrokerManagedComponent)binaryFormatter.Deserialize(memoryStream);

            BrokerManagedComponentDS basePrimaryDataSet = new BrokerManagedComponentDS();
            base.Hydrate(iBMC, cID, basePrimaryDataSet, ref primaryDS);

            return iBMC;
        }

        public override void Save(IBrokerManagedComponent iBMC, ref DataSet persistedPrimaryDS, ITransactionVelocity trx)
        {
            base.Save(iBMC, ref persistedPrimaryDS, trx);
            //base.Save(iBMC, persistedPrimaryDS, trx);

            MemoryStream memoryStream = new MemoryStream(1000);
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(memoryStream, iBMC);

            DSCalculationModule data = new DSCalculationModule(iBMC.CID, iBMC.Name, iBMC.TypeID, memoryStream.ToArray());

            data.Tables[DSCalculationModule.INSTANCE_TABLE].Rows[0][DSCalculationModule.PERSISTANT_DATA_FIELD] = memoryStream.ToArray();
            data.Tables[DSCalculationModule.INSTANCE_TABLE].Rows[0][DSCalculationModule.NAME_FIELD] = iBMC.Name;
            data.Tables[DSCalculationModule.INSTANCE_TABLE].Rows[0][DSCalculationModule.TYPE_FIELD] = iBMC.TypeID;

            //object[] state = new object[2];
            //state[0] = BLOB_ID_PREFIX + iBMC.CID.ToString();
            //state[1] = data;
            //WaitCallback callBack = new WaitCallback(PooledPutObject);

            //ThreadPool.QueueUserWorkItem(callBack, state);

            //BlobPersisterVelocity._DataCache.PutObject(BLOB_ID_PREFIX + iBMC.CID.ToString(), data);

            // BLOBPersistTableList can't be put into threadpool, as it could be overlapped between different threads.
            //if (!BLOBPersistTableList.Contains(BLOB_ID_PREFIX + iBMC.CID.ToString()))
            //{
            //    BLOBPersistTableList.Add(BLOB_ID_PREFIX + iBMC.CID.ToString());
            //    VelocityCacheString.VelocityCache.PutObject(VelocityConfiguration.BLOBPersistTableListKey, BLOBPersistTableList);
            //}

            VelocityRMBase velocityRM = new VelocityRMBase(BLOB_ID_PREFIX + iBMC.CID.ToString(), data);
            trx.EnlistRM(velocityRM, "VelocityRM");


            sQLRMBlobPersister sqlRM = new sQLRMBlobPersister(0, trx.SQLConnection, trx.SQLTransaction, iBMC, trx);
            trx.EnlistRM(sqlRM, "DBRM");

        }



        public override void Save(IBrokerManagedComponent iBMC, ref DataSet persistedDS)
        {
            base.Save(iBMC, ref persistedDS);
        }

        //private static void PooledPutObject(object state)
        //{
        //    object[] objArray = (object[])state;

        //    string id = (string)objArray[0];
        //    object data = objArray[1];

        //    VelocityCacheString.VelocityCache.PutObject(id, data);
        //}

        public override void Delete(IBrokerManagedComponent iBMC)
        {
            base.Delete(iBMC, new BrokerManagedComponentDS());

            VelocityCacheString.VelocityCache.RemoveObject(BLOB_ID_PREFIX + iBMC.CID.ToString());

            if (BLOBPersistTableList.Contains(BLOB_ID_PREFIX + iBMC.CID.ToString()))
            {
                BLOBPersistTableList.Remove(BLOB_ID_PREFIX + iBMC.CID.ToString());
                VelocityCacheString.VelocityCache.PutObject(VelocityConfiguration.BLOBPersistTableListKey, BLOBPersistTableList);
            }


        }

        public override void EstablishDatabase()
        {
            base.EstablishDatabase();

            SqlCommand adminCommand = DBCommandFactory.Instance.NewSqlCommand();
            adminCommand.Connection = this.Connection;
            adminCommand.Transaction = this.Transaction;
            adminCommand.CommandText = "if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[BLOBPERSIST]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)"
                + "CREATE TABLE [dbo].[BLOBPERSIST] ("
                + "[INSTANCEID] [uniqueidentifier] NOT NULL ,"
                + "[NAME] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,"
                + "[TYPE] [uniqueidentifier] NULL ,"
                + "[PERSISTENT_DATA] [image] NULL "
                + ") ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]";
            adminCommand.ExecuteNonQuery();
        }

        public override void ClearDatabase()
        {
            base.ClearDatabase();

            SqlCommand adminCommand = DBCommandFactory.Instance.NewSqlCommand();
            adminCommand.Connection = this.Connection;
            adminCommand.Transaction = this.Transaction;

            adminCommand.CommandText = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[BLOBPERSIST]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)"
                + "DELETE BLOBPERSIST";
            adminCommand.ExecuteNonQuery();
        }
    }
}
