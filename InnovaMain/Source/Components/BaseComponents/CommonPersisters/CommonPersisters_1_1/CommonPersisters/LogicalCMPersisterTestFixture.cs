using System;
using System.Diagnostics;
using System.Configuration;
using System.Data.SqlClient;
using System.Reflection;
using System.Data;

using NUnit.Framework;

using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.DSCalculation;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.BusinessStructureUtilities;




namespace Oritax.TaxSimp.CalculationPersistSQL
{
	/// <summary>
	/// Summary description for LogicalCMPersisterTestFixture.
	/// </summary>
	/// 
	[TestFixture]
	public class LogicalCMPersisterTestFixture
	{
		SqlConnection sqlCon = null;

		[Conditional("DEBUG")] [SetUp] public void PersisterTestSetup()
		{
			string sConn = ConfigurationSettings.AppSettings["TX360ENTERPRISE_DBCONNECTIONSTRING"];
			sqlCon = new SqlConnection(sConn);
			sqlCon.Open();
		}

		[Conditional("DEBUG")] [TearDown] public void PersisterTestTearDown()
		{
			sqlCon.Close();
			sqlCon.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterConstructionTest()
		{
			LogicalCMPersister logicalPersister = new LogicalCMPersister(sqlCon);

			FieldInfo dataAdaptInfo = logicalPersister.GetType().GetField("dsLCMCommand", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlDataAdapter dsCommand = (SqlDataAdapter)dataAdaptInfo.GetValue(logicalPersister);
			Assertion.AssertNotNull("SqlDataAdapter dsLCMCommand is Null",dsCommand);
			Assertion.AssertNotNull("dsLCMCommand.SelectCommand is Null",dsCommand.SelectCommand);
			Assertion.AssertNotNull("dsLCMCommand.InsertCommand is Null",dsCommand.InsertCommand);
			Assertion.AssertNotNull("dsLCMCommand.UpdateCommand is Null",dsCommand.UpdateCommand);
			Assertion.AssertNotNull("dsLCMCommand.DeleteCommand is Null",dsCommand.DeleteCommand);

			dataAdaptInfo = logicalPersister.GetType().GetField("dsCMSCommand", BindingFlags.NonPublic | BindingFlags.Instance);
			dsCommand = (SqlDataAdapter)dataAdaptInfo.GetValue(logicalPersister);
			Assertion.AssertNotNull("SqlDataAdapter dsCMSCommand is Null",dsCommand);
			Assertion.AssertNotNull("dsCMSCommand.SelectCommand is Null",dsCommand.SelectCommand);
			Assertion.AssertNotNull("dsCMSCommand.InsertCommand is Null",dsCommand.InsertCommand);
			Assertion.AssertNotNull("dsCMSCommand.UpdateCommand is Null",dsCommand.UpdateCommand);
			Assertion.AssertNotNull("dsCMSCommand.DeleteCommand is Null",dsCommand.DeleteCommand);

			dataAdaptInfo = logicalPersister.GetType().GetField("dsChildLCMCommand", BindingFlags.NonPublic | BindingFlags.Instance);
			dsCommand = (SqlDataAdapter)dataAdaptInfo.GetValue(logicalPersister);
			Assertion.AssertNotNull("SqlDataAdapter dsCMSCommand is Null",dsCommand);
			Assertion.AssertNotNull("dsChildLCMCommand.SelectCommand is Null",dsCommand.SelectCommand);
			Assertion.AssertNull("dsChildLCMCommand.InsertCommand is not Null",dsCommand.InsertCommand);
			Assertion.AssertNull("dsChildLCMCommand.UpdateCommand is not Null",dsCommand.UpdateCommand);
			Assertion.AssertNull("dsChildLCMCommand.DeleteCommand is not Null",dsCommand.DeleteCommand);

			dataAdaptInfo = logicalPersister.GetType().GetField("dsSubscriptionsCommand", BindingFlags.NonPublic | BindingFlags.Instance);
			dsCommand = (SqlDataAdapter)dataAdaptInfo.GetValue(logicalPersister);
			Assertion.AssertNotNull("SqlDataAdapter dsCMSCommand is Null",dsCommand);
			Assertion.AssertNotNull("dsSubscriptionsCommand.SelectCommand is Null",dsCommand.SelectCommand);
			Assertion.AssertNotNull("dsSubscriptionsCommand.InsertCommand is Null",dsCommand.InsertCommand);
			Assertion.AssertNotNull("dsSubscriptionsCommand.UpdateCommand is Null",dsCommand.UpdateCommand);
			Assertion.AssertNotNull("dsSubscriptionsCommand.DeleteCommand is Null",dsCommand.DeleteCommand);

			FieldInfo sqlConnectionInfo = logicalPersister.GetType().GetField("sqlConnection", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlConnection sqlConnection = (SqlConnection)sqlConnectionInfo.GetValue(logicalPersister);
			Assertion.AssertEquals("Connection has not been set", sqlConnection,sqlCon);

			FieldInfo sqlTransactionInfo = logicalPersister.GetType().GetField("sqlTransaction", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlTransaction sqlTransaction = (SqlTransaction)sqlTransactionInfo.GetValue(logicalPersister);
			Assertion.AssertNull("sqlTransaction is not null", sqlTransaction);

			logicalPersister.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterConnectionTest()
		{
			LogicalCMPersister logicalPersister = new LogicalCMPersister(sqlCon);
			logicalPersister.Connection = sqlCon;

			FieldInfo sqlConnectionInfo = logicalPersister.GetType().GetField("sqlConnection", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlConnection sqlConnection = (SqlConnection)sqlConnectionInfo.GetValue(logicalPersister);
			Assertion.AssertEquals("Connection has not been set", sqlConnection,sqlCon);

			logicalPersister.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterTransactionTest()
		{
			LogicalCMPersister logicalPersister = new LogicalCMPersister(sqlCon);
			SqlTransaction sqlTran = sqlCon.BeginTransaction();
			logicalPersister.Transaction = sqlTran;
			FieldInfo sqlTransactionInfo = logicalPersister.GetType().GetField("sqlTransaction", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlTransaction sqlTransaction = (SqlTransaction)sqlTransactionInfo.GetValue(logicalPersister);
			Assertion.AssertEquals("sqlTransaction has not been set", sqlTransaction, sqlTran);
			sqlTran.Commit();
			sqlTran.Dispose();
			logicalPersister.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterEstablishDBTest()
		{
			LogicalCMPersister logicalPersister = new LogicalCMPersister(sqlCon);

			SqlTransaction sqlTran = sqlCon.BeginTransaction();
			logicalPersister.Transaction = sqlTran;
			try
			{
				logicalPersister.EstablishDatabase();
			}
			finally
			{
				sqlTran.Rollback();	//DO NOT CORRUPT THE DATABASE
			}
			logicalPersister.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterClearDBTest()
		{
			LogicalCMPersister logicalPersister = new LogicalCMPersister(sqlCon);

			SqlTransaction sqlTran = sqlCon.BeginTransaction();
			logicalPersister.Transaction = sqlTran;
			try
			{
				logicalPersister.ClearDatabase();
			}
			finally
			{
				sqlTran.Rollback();	//DO NOT CORRUPT THE DATABASE
			}
			logicalPersister.Dispose();
		}
		[Conditional("DEBUG")] [Test] public void PersisterGetByInstanceIdTest()
		{

			//go get the first user Instance
			CMListPersist bmcList= new CMListPersist(sqlCon);
			DSBMCInfo data = (DSBMCInfo)bmcList.FindAll();

			if(data.Tables[DSBMCInfo.CMLIST_TABLE].Rows.Count == 0)
			{
				throw new Exception("This Test requires a instance in CINSTANCES");
			}
			DataRow firstRow=data.Tables[DSBMCInfo.CMLIST_TABLE].Rows[0];

			LogicalCMPersister logicalPersister = new LogicalCMPersister(sqlCon);

			IBrokerManagedComponent ibmc = logicalPersister.GetByInstanceId((Guid)firstRow[DSBMCInfo.CMID_FIELD]);
			Assertion.AssertNotNull("Could not find instance",ibmc);
			bmcList.Dispose();
			logicalPersister.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterSaveCMTest()
		{

			//go get the first user Instance
			CMListPersist bmcList= new CMListPersist(sqlCon);
			DSBMCInfo data = (DSBMCInfo)bmcList.FindByTypeName("Organization");

			if(data.Tables[DSBMCInfo.CMLIST_TABLE].Rows.Count == 0)
			{
				throw new Exception("This Test requires an Organization instance");
			}
			DataRow firstRow=data.Tables[DSBMCInfo.CMLIST_TABLE].Rows[0];
			
			LogicalCMPersister logicalPersister = new LogicalCMPersister(sqlCon);
			IBrokerManagedComponent ibmc = logicalPersister.GetByInstanceId((Guid)firstRow[DSBMCInfo.CMID_FIELD]);

			SqlTransaction sqlTran = sqlCon.BeginTransaction();
			logicalPersister.Transaction = sqlTran;
			try
			{
				logicalPersister.Save(ibmc);
			}
			finally
			{
				sqlTran.Rollback();	//DO NOT CORRUPT THE DATABASE
			}

			bmcList.Dispose();
			logicalPersister.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterDeleteTest()
		{

			//go get the first user Instance
			CMListPersist bmcList= new CMListPersist(sqlCon);
			DSBMCInfo data = (DSBMCInfo)bmcList.FindByTypeName("Organization");

			if(data.Tables[DSBMCInfo.CMLIST_TABLE].Rows.Count == 0)
			{
				throw new Exception("This Test requires an Organization instance");
			}
			DataRow firstRow=data.Tables[DSBMCInfo.CMLIST_TABLE].Rows[0];
			
			LogicalCMPersister logicalPersister = new LogicalCMPersister(sqlCon);
			IBrokerManagedComponent ibmc = logicalPersister.GetByInstanceId((Guid)firstRow[DSBMCInfo.CMID_FIELD]);

			SqlTransaction sqlTran = sqlCon.BeginTransaction();
			logicalPersister.Transaction = sqlTran;
			try
			{
				logicalPersister.Delete(ibmc);
			}
			finally
			{
				sqlTran.Rollback();	//DO NOT CORRUPT THE DATABASE
			}

			bmcList.Dispose();
			logicalPersister.Dispose();
		}


	}
}
