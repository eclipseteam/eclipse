using System;
using System.Diagnostics;
using System.Configuration;
using System.Data.SqlClient;
using System.Reflection;
using System.Data;

using NUnit.Framework;

using Oritax.TaxSimp.Calculation;




namespace Oritax.TaxSimp.CalculationPersistSQL
{
	/// <summary>
	/// Summary description for BMCPersisterTestFixture.
	/// </summary>
	/// 
	[TestFixture]
	public class BMCPersisterTestFixture
	{
		SqlConnection sqlCon = null;

		[Conditional("DEBUG")] [SetUp] public void PersisterTestSetup()
		{
			string sConn = ConfigurationSettings.AppSettings["TX360ENTERPRISE_DBCONNECTIONSTRING"];
			sqlCon = new SqlConnection(sConn);
			sqlCon.Open();
		}

		[Conditional("DEBUG")] [TearDown] public void PersisterTestTearDown()
		{
			sqlCon.Close();
			sqlCon.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterConstructionTest()
		{
			BMCPersister bmcPersist = new BMCPersister(sqlCon);

			FieldInfo dataAdaptInfo = bmcPersist.GetType().GetField("dsBMCCommand", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlDataAdapter dsBMCCommand = (SqlDataAdapter)dataAdaptInfo.GetValue(bmcPersist);
			Assertion.AssertNotNull("SqlDataAdapter dsBMCCommand is Null",dsBMCCommand);
			Assertion.AssertNotNull("dsBMCCommand.SelectCommand is Null",dsBMCCommand.SelectCommand);
			Assertion.AssertNotNull("dsBMCCommand.InsertCommand is Null",dsBMCCommand.InsertCommand);
			Assertion.AssertNotNull("dsBMCCommand.UpdateCommand is Null",dsBMCCommand.UpdateCommand);
			Assertion.AssertNotNull("dsBMCCommand.DeleteCommand is Null",dsBMCCommand.DeleteCommand);

			dataAdaptInfo = bmcPersist.GetType().GetField("dsACLCommand", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlDataAdapter dsACLCommand = (SqlDataAdapter)dataAdaptInfo.GetValue(bmcPersist);
			Assertion.AssertNotNull("SqlDataAdapter dsACLCommand is Null",dsACLCommand);
			Assertion.AssertNotNull("dsACLCommand.SelectCommand is Null",dsACLCommand.SelectCommand);
			Assertion.AssertNotNull("dsACLCommand.InsertCommand is Null",dsACLCommand.InsertCommand);
			Assertion.AssertNotNull("dsACLCommand.UpdateCommand is Null",dsACLCommand.UpdateCommand);
			Assertion.AssertNotNull("dsACLCommand.DeleteCommand is Null",dsACLCommand.DeleteCommand);

			FieldInfo sqlConnectionInfo = bmcPersist.GetType().GetField("sqlConnection", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlConnection sqlConnection = (SqlConnection)sqlConnectionInfo.GetValue(bmcPersist);
			Assertion.AssertEquals("Connection has not been set", sqlConnection,sqlCon);

			FieldInfo sqlTransactionInfo = bmcPersist.GetType().GetField("sqlTransaction", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlTransaction sqlTransaction = (SqlTransaction)sqlTransactionInfo.GetValue(bmcPersist);
			Assertion.AssertNull("sqlTransaction is not null", sqlTransaction);

			bmcPersist.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterConnectionTest()
		{
			BMCPersister bmcPersist = new BMCPersister(sqlCon);
			bmcPersist.Connection = sqlCon;

			FieldInfo sqlConnectionInfo = bmcPersist.GetType().GetField("sqlConnection", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlConnection sqlConnection = (SqlConnection)sqlConnectionInfo.GetValue(bmcPersist);
			Assertion.AssertEquals("Connection has not been set", sqlConnection,sqlCon);

			bmcPersist.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterTransactionTest()
		{
			BMCPersister bmcPersist = new BMCPersister(sqlCon);
			SqlTransaction sqlTran = sqlCon.BeginTransaction();
			bmcPersist.Transaction = sqlTran;
			FieldInfo sqlTransactionInfo = bmcPersist.GetType().GetField("sqlTransaction", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlTransaction sqlTransaction = (SqlTransaction)sqlTransactionInfo.GetValue(bmcPersist);
			Assertion.AssertEquals("sqlTransaction has not been set", sqlTransaction, sqlTran);
			sqlTran.Commit();
			sqlTran.Dispose();
			bmcPersist.Dispose();
		}

		/// <summary>
		/// All we want to do here is exercise the code to see if it causes an exception
		/// </summary>
		[Conditional("DEBUG")] [Test] public void PersisterDehydrateTest()
		{
			BMCPersister bmcPersist = new BMCPersister(sqlCon);

			BrokerManagedComponent bmc = new BrokerManagedComponent();
			bmc.Initialize("BMCPersisterTest");
			Guid ID = new Guid();
			bmc.CID = ID;
			bmc.TypeID = ID;

			MethodInfo mi = bmcPersist.GetType().GetMethod("DehydrateByInstanceId", BindingFlags.NonPublic | BindingFlags.Instance);
			object [] args = {bmc, ID};

			SqlTransaction sqlTran = sqlCon.BeginTransaction();
			bmcPersist.Transaction = sqlTran;
			try
			{
				mi.Invoke(bmcPersist,args);
			}
			finally
			{
				sqlTran.Rollback();//DO NOT CORRUPT THE DATABASE
			}

			bmcPersist.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterHydrateTest()
		{

			//go get the first user Instance
			CMListPersist bmcList= new CMListPersist(sqlCon);
			DSBMCInfo data = (DSBMCInfo)bmcList.FindByTypeName("DBUser");

			if(data.Tables[DSBMCInfo.CMLIST_TABLE].Rows.Count == 0)
			{
				throw new Exception("This Test requires a user instance");
			}
			DataRow firstRow=data.Tables[DSBMCInfo.CMLIST_TABLE].Rows[0];

			BMCPersister bmcPersist = new BMCPersister(sqlCon);


			BrokerManagedComponent bmcHydrate = new BrokerManagedComponent();
			bmcHydrate.Initialize("BMCPersisterTestHydrate");
			MethodInfo miHydrate = bmcPersist.GetType().GetMethod("HydrateByInstanceId", BindingFlags.NonPublic | BindingFlags.Instance);

			SqlTransaction sqlTran = sqlCon.BeginTransaction();
			bmcPersist.Transaction = sqlTran;

			try
			{
				object [] hydargs = {bmcHydrate, (Guid)firstRow[BrokerManagedComponentDS.CID_FIELD]};
				miHydrate.Invoke(bmcPersist,hydargs);
			}
			finally
			{
				sqlTran.Rollback();	//DO NOT CORRUPT THE DATABASE
			}

			Assertion.AssertEquals("",(Guid)firstRow[BrokerManagedComponentDS.CID_FIELD],bmcHydrate.CID);
			Assertion.AssertEquals("",(string)firstRow[BrokerManagedComponentDS.NAME_FIELD],bmcHydrate.Name);
			Assertion.AssertEquals("",(Guid)firstRow[BrokerManagedComponentDS.CTID_FIELD],bmcHydrate.TypeID);
			Assertion.AssertEquals("",(string)firstRow[BrokerManagedComponentDS.TYPENAME_FIELD],bmcHydrate.TypeName);
			Assertion.AssertEquals("",(string)firstRow[BrokerManagedComponentDS.CATEGORYNAME_FIELD],bmcHydrate.CategoryName);

			bmcPersist.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterEstablishDBTest()
		{
			BMCPersister bmcPersist = new BMCPersister(sqlCon);

			SqlTransaction sqlTran = sqlCon.BeginTransaction();
			bmcPersist.Transaction = sqlTran;
			try
			{
				bmcPersist.EstablishDatabase();
			}
			finally
			{
				sqlTran.Rollback();	//DO NOT CORRUPT THE DATABASE
			}
			bmcPersist.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterClearDBTest()
		{
			BMCPersister bmcPersist = new BMCPersister(sqlCon);

			SqlTransaction sqlTran = sqlCon.BeginTransaction();
			bmcPersist.Transaction = sqlTran;
			try
			{
				bmcPersist.ClearDatabase();
			}
			finally
			{
				sqlTran.Rollback();	//DO NOT CORRUPT THE DATABASE
			}
			bmcPersist.Dispose();
		}
	}
}
