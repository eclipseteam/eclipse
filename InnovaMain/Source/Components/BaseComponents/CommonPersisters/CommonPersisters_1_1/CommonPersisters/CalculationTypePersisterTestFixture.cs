using System;
using System.Diagnostics;
using System.Configuration;
using System.Data.SqlClient;
using System.Reflection;
using System.Data;

using NUnit.Framework;

using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.DSCalculation;


namespace Oritax.TaxSimp.CalculationPersistSQL
{
	/// <summary>
	/// Summary description for CalculationTypePersistTestFixture.
	/// </summary>
	/// 
	[TestFixture]
	public class CalculationTypePersistTestFixture
	{
		SqlConnection sqlCon = null;

		[Conditional("DEBUG")] [SetUp] public void PersisterTestSetup()
		{
			string sConn = ConfigurationSettings.AppSettings["TX360ENTERPRISE_DBCONNECTIONSTRING"];
			sqlCon = new SqlConnection(sConn);
			sqlCon.Open();
		}

		[Conditional("DEBUG")] [TearDown] public void PersisterTestTearDown()
		{
			sqlCon.Close();
			sqlCon.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterConstructionTest()
		{
			CalculationTypePersist calcTypePersist = new CalculationTypePersist(sqlCon);

			FieldInfo dataAdaptInfo = calcTypePersist.GetType().GetField("dsCommand", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlDataAdapter dsCommand = (SqlDataAdapter)dataAdaptInfo.GetValue(calcTypePersist);
			Assertion.AssertNotNull("SqlDataAdapter dsBMCCommand is Null",dsCommand);
			Assertion.AssertNotNull("dsCommand.SelectCommand is Null",dsCommand.SelectCommand);
			Assertion.AssertNotNull("dsCommand.InsertCommand is Null",dsCommand.InsertCommand);
			Assertion.AssertNotNull("dsCommand.UpdateCommand is Null",dsCommand.UpdateCommand);
			Assertion.AssertNull("dsCommand.DeleteCommand is not Null",dsCommand.DeleteCommand);

			FieldInfo sqlConnectionInfo = calcTypePersist.GetType().GetField("sqlConnection", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlConnection sqlConnection = (SqlConnection)sqlConnectionInfo.GetValue(calcTypePersist);
			Assertion.AssertEquals("Connection has not been set", sqlConnection,sqlCon);

			FieldInfo sqlTransactionInfo = calcTypePersist.GetType().GetField("sqlTransaction", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlTransaction sqlTransaction = (SqlTransaction)sqlTransactionInfo.GetValue(calcTypePersist);
			Assertion.AssertNull("sqlTransaction is not null", sqlTransaction);

			calcTypePersist.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterConnectionTest()
		{
			CalculationTypePersist calcTypePersist = new CalculationTypePersist(sqlCon);
			calcTypePersist.Connection = sqlCon;

			FieldInfo sqlConnectionInfo = calcTypePersist.GetType().GetField("sqlConnection", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlConnection sqlConnection = (SqlConnection)sqlConnectionInfo.GetValue(calcTypePersist);
			Assertion.AssertEquals("Connection has not been set", sqlConnection,sqlCon);

			calcTypePersist.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterTransactionTest()
		{
			CalculationTypePersist calcTypePersist = new CalculationTypePersist(sqlCon);
			SqlTransaction sqlTran = sqlCon.BeginTransaction();
			calcTypePersist.Transaction = sqlTran;
			FieldInfo sqlTransactionInfo = calcTypePersist.GetType().GetField("sqlTransaction", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlTransaction sqlTransaction = (SqlTransaction)sqlTransactionInfo.GetValue(calcTypePersist);
			Assertion.AssertEquals("sqlTransaction has not been set", sqlTransaction, sqlTran);
			sqlTran.Commit();
			sqlTran.Dispose();
			calcTypePersist.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterFindAllTest()
		{
			CalculationTypePersist calcTypePersist = new CalculationTypePersist(sqlCon);
			DSCalculationModuleType data = (DSCalculationModuleType)calcTypePersist.FindAll();
			Assertion.AssertNotNull("Could not find any instance in CMTYPES Table",data);
			calcTypePersist.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterFindByTypeIdTest()
		{
			CalculationTypePersist calcTypePersist = new CalculationTypePersist(sqlCon);
			DSCalculationModuleType allData = (DSCalculationModuleType)calcTypePersist.FindAll();
			Assertion.AssertNotNull("Could not find any instance in CMTYPES Table",allData);

			DataRow firstRow = allData.Tables[DSCalculationModuleType.TYPE_TABLE].Rows[0];
			DSCalculationModuleType data = (DSCalculationModuleType)calcTypePersist.FindByTypeId((Guid)firstRow[DSCalculationModuleType.CMTYPEID_FIELD]);
			Assertion.AssertNotNull("Could not find instance",data);
			calcTypePersist.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterFindByTypeNameTest()
		{
			CalculationTypePersist calcTypePersist = new CalculationTypePersist(sqlCon);
			DSCalculationModuleType allData = (DSCalculationModuleType)calcTypePersist.FindAll();
			Assertion.AssertNotNull("Could not find any instance in CMTYPES Table",allData);

			DataRow firstRow = allData.Tables[DSCalculationModuleType.TYPE_TABLE].Rows[0];
			DSCalculationModuleType data = (DSCalculationModuleType)calcTypePersist.FindByTypeName((string)firstRow[DSCalculationModuleType.CMTYPENAME_FIELD]);
			Assertion.AssertNotNull("Could not find instance",data);
			calcTypePersist.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterFindByCategoryTest()
		{
			CalculationTypePersist calcTypePersist = new CalculationTypePersist(sqlCon);
			DSCalculationModuleType allData = (DSCalculationModuleType)calcTypePersist.FindAll();
			Assertion.AssertNotNull("Could not find any instance in CMTYPES Table",allData);

			DataRow firstRow = allData.Tables[DSCalculationModuleType.TYPE_TABLE].Rows[0];
			DSCalculationModuleType data = (DSCalculationModuleType)calcTypePersist.FindByCategory((string)firstRow[DSCalculationModuleType.CMCATEGORYNAME_FIELD]);
			Assertion.AssertNotNull("Could not find instance",data);
			calcTypePersist.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterFindByCategoryArrayTest()
		{
			CalculationTypePersist calcTypePersist = new CalculationTypePersist(sqlCon);
			DSCalculationModuleType allData = (DSCalculationModuleType)calcTypePersist.FindAll();
			Assertion.AssertNotNull("Could not find any instance in CMTYPES Table",allData);


			//populate the array with 2 categories if possible
			string [] sCats = new String [2];
			sCats[0] = (string)allData.Tables[DSCalculationModuleType.TYPE_TABLE].Rows[0][DSCalculationModuleType.CMCATEGORYNAME_FIELD];
			for(int i=1; i<allData.Tables[DSCalculationModuleType.TYPE_TABLE].Rows.Count; i++)
			{
				if((string)allData.Tables[DSCalculationModuleType.TYPE_TABLE].Rows[i][DSCalculationModuleType.CMCATEGORYNAME_FIELD] != sCats[0])
				{
					sCats[1] = (string)allData.Tables[DSCalculationModuleType.TYPE_TABLE].Rows[i][DSCalculationModuleType.CMCATEGORYNAME_FIELD];
					break;
				}
			}

			DSCalculationModuleType data = (DSCalculationModuleType)calcTypePersist.FindByCategory(sCats);
			Assertion.AssertNotNull("Could not find instance",data);
			calcTypePersist.Dispose();
		}

		//exercise the code to catch exceptions
		[Conditional("DEBUG")] [Test] public void PersisterUpdateTest()
		{
			CalculationTypePersist calcTypePersist = new CalculationTypePersist(sqlCon);
			DSCalculationModuleType allData = (DSCalculationModuleType)calcTypePersist.FindAll();
			Assertion.AssertNotNull("Could not find any instance in CMTYPES Table",allData);

			SqlTransaction sqlTran = sqlCon.BeginTransaction();
			calcTypePersist.Transaction = sqlTran;
			try
			{
				calcTypePersist.Update(allData);
			}
			finally
			{
				sqlTran.Rollback();	//DO NOT CORRUPT THE DATABASE
			}
			
			calcTypePersist.Dispose();
		}

		//exercise the code to catch exceptions
		[Conditional("DEBUG")] [Test] public void PersisterDeleteAllTest()
		{
			CalculationTypePersist calcTypePersist = new CalculationTypePersist(sqlCon);

			SqlTransaction sqlTran = sqlCon.BeginTransaction();
			calcTypePersist.Transaction = sqlTran;
			try
			{
				calcTypePersist.DeleteAll();
			}
			finally
			{
				sqlTran.Rollback();	//DO NOT CORRUPT THE DATABASE
			}
			
			calcTypePersist.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterEstablishDBTest()
		{
			CalculationTypePersist calcTypePersist = new CalculationTypePersist(sqlCon);

			SqlTransaction sqlTran = sqlCon.BeginTransaction();
			calcTypePersist.Transaction = sqlTran;
			try
			{
				calcTypePersist.EstablishDatabase();
			}
			finally
			{
				sqlTran.Rollback();	//DO NOT CORRUPT THE DATABASE
			}
			calcTypePersist.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterClearDBTest()
		{
			CalculationTypePersist calcTypePersist = new CalculationTypePersist(sqlCon);

			SqlTransaction sqlTran = sqlCon.BeginTransaction();
			calcTypePersist.Transaction = sqlTran;
			try
			{
				calcTypePersist.ClearDatabase();
			}
			finally
			{
				sqlTran.Rollback();	//DO NOT CORRUPT THE DATABASE
			}
			calcTypePersist.Dispose();
		}
	}
}
