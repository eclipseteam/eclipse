using System;
using System.Diagnostics;
using System.Configuration;
using System.Data.SqlClient;
using System.Reflection;
using System.Data;

using NUnit.Framework;

using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.DSCalculation;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.BusinessStructureUtilities;




namespace Oritax.TaxSimp.CalculationPersistSQL
{
	/// <summary>
	/// Summary description for GroupPersisterTestFixture.
	/// </summary>
	/// 
	[TestFixture]
	public class GroupPersisterTestFixture
	{
		SqlConnection sqlCon = null;

		[Conditional("DEBUG")] [SetUp] public void PersisterTestSetup()
		{
			string sConn = ConfigurationSettings.AppSettings["TX360ENTERPRISE_DBCONNECTIONSTRING"];
			sqlCon = new SqlConnection(sConn);
			sqlCon.Open();
		}

		[Conditional("DEBUG")] [TearDown] public void PersisterTestTearDown()
		{
			sqlCon.Close();
			sqlCon.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterConstructionTest()
		{
			GroupPersister groupPersister = new GroupPersister(sqlCon);

			FieldInfo dataAdaptInfo = groupPersister.GetType().GetField("dsGroupCommand", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlDataAdapter dsCommand = (SqlDataAdapter)dataAdaptInfo.GetValue(groupPersister);
			Assertion.AssertNotNull("SqlDataAdapter dsCommand is Null",dsCommand);
			Assertion.AssertNotNull("dsCommand.SelectCommand is Null",dsCommand.SelectCommand);
			Assertion.AssertNotNull("dsCommand.InsertCommand is Null",dsCommand.InsertCommand);
			Assertion.AssertNotNull("dsCommand.UpdateCommand is Null",dsCommand.UpdateCommand);
			Assertion.AssertNotNull("dsCommand.DeleteCommand is Null",dsCommand.DeleteCommand);


			FieldInfo sqlConnectionInfo = groupPersister.GetType().GetField("sqlConnection", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlConnection sqlConnection = (SqlConnection)sqlConnectionInfo.GetValue(groupPersister);
			Assertion.AssertEquals("Connection has not been set", sqlConnection,sqlCon);

			FieldInfo sqlTransactionInfo = groupPersister.GetType().GetField("sqlTransaction", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlTransaction sqlTransaction = (SqlTransaction)sqlTransactionInfo.GetValue(groupPersister);
			Assertion.AssertNull("sqlTransaction is not null", sqlTransaction);

			groupPersister.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterConnectionTest()
		{
			GroupPersister groupPersister = new GroupPersister(sqlCon);
			groupPersister.Connection = sqlCon;

			FieldInfo sqlConnectionInfo = groupPersister.GetType().GetField("sqlConnection", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlConnection sqlConnection = (SqlConnection)sqlConnectionInfo.GetValue(groupPersister);
			Assertion.AssertEquals("Connection has not been set", sqlConnection,sqlCon);

			groupPersister.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterTransactionTest()
		{
			GroupPersister groupPersister = new GroupPersister(sqlCon);
			SqlTransaction sqlTran = sqlCon.BeginTransaction();
			groupPersister.Transaction = sqlTran;
			FieldInfo sqlTransactionInfo = groupPersister.GetType().GetField("sqlTransaction", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlTransaction sqlTransaction = (SqlTransaction)sqlTransactionInfo.GetValue(groupPersister);
			Assertion.AssertEquals("sqlTransaction has not been set", sqlTransaction, sqlTran);
			sqlTran.Commit();
			sqlTran.Dispose();
			groupPersister.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterEstablishDBTest()
		{
			GroupPersister groupPersister = new GroupPersister(sqlCon);

			SqlTransaction sqlTran = sqlCon.BeginTransaction();
			groupPersister.Transaction = sqlTran;
			try
			{
				groupPersister.EstablishDatabase();
			}
			finally
			{
				sqlTran.Rollback();	//DO NOT CORRUPT THE DATABASE
			}
			groupPersister.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterClearDBTest()
		{
			GroupPersister groupPersister = new GroupPersister(sqlCon);

			SqlTransaction sqlTran = sqlCon.BeginTransaction();
			groupPersister.Transaction = sqlTran;
			try
			{
				groupPersister.ClearDatabase();
			}
			finally
			{
				sqlTran.Rollback();	//DO NOT CORRUPT THE DATABASE
			}
			groupPersister.Dispose();
		}
		[Conditional("DEBUG")] [Test] public void PersisterGetByInstanceIdTest()
		{

			//go get the first user Instance
			CMListPersist bmcList= new CMListPersist(sqlCon);
			DSBMCInfo data = (DSBMCInfo)bmcList.FindByTypeName("Organization");

			if(data.Tables[DSBMCInfo.CMLIST_TABLE].Rows.Count == 0)
			{
				throw new Exception("This Test requires an Grouped Organization instance");
			}
			DataRow firstRow=data.Tables[DSBMCInfo.CMLIST_TABLE].Rows[0];

			GroupPersister groupPersister = new GroupPersister(sqlCon);

			IBrokerManagedComponent ibmc = groupPersister.GetByInstanceId((Guid)firstRow[DSBMCInfo.CMID_FIELD]);
			Assertion.AssertNotNull("Could not find instance",ibmc);
			bmcList.Dispose();
			groupPersister.Dispose();
		}

#if false
		//need a broker to run this test
		[Conditional("DEBUG")] [Test] public void PersisterSaveCMTest()
		{

			//go get the first user Instance
			CMListPersist bmcList= new CMListPersist(sqlCon);
			DSBMCInfo data = (DSBMCInfo)bmcList.FindByTypeName("Organization");

			if(data.Tables[DSBMCInfo.CMLIST_TABLE].Rows.Count == 0)
			{
				throw new Exception("This Test requires an Grouped Organization instance");
			}
			DataRow firstRow=data.Tables[DSBMCInfo.CMLIST_TABLE].Rows[0];
			
			GroupPersister groupPersister = new GroupPersister(sqlCon);
			IBrokerManagedComponent ibmc = groupPersister.GetByInstanceId((Guid)firstRow[DSBMCInfo.CMID_FIELD]);

			SqlTransaction sqlTran = sqlCon.BeginTransaction();
			groupPersister.Transaction = sqlTran;
			try
			{
				groupPersister.SaveCM(ibmc);
			}
			finally
			{
				sqlTran.Rollback();	//DO NOT CORRUPT THE DATABASE
			}

			bmcList.Dispose();
			groupPersister.Dispose();
		}
#endif

		[Conditional("DEBUG")] [Test] public void PersisterDeleteTest()
		{

			//go get the first user Instance
			CMListPersist bmcList= new CMListPersist(sqlCon);
			DSBMCInfo data = (DSBMCInfo)bmcList.FindByTypeName("Organization");

			if(data.Tables[DSBMCInfo.CMLIST_TABLE].Rows.Count == 0)
			{
				throw new Exception("This Test requires an Organization instance");
			}
			DataRow firstRow=data.Tables[DSBMCInfo.CMLIST_TABLE].Rows[0];
			
			GroupPersister groupPersister = new GroupPersister(sqlCon);
			IBrokerManagedComponent ibmc = groupPersister.GetByInstanceId((Guid)firstRow[DSBMCInfo.CMID_FIELD]);

			SqlTransaction sqlTran = sqlCon.BeginTransaction();
			groupPersister.Transaction = sqlTran;
			try
			{
				groupPersister.Delete(ibmc);
			}
			finally
			{
				sqlTran.Rollback();	//DO NOT CORRUPT THE DATABASE
			}

			bmcList.Dispose();
			groupPersister.Dispose();
		}


	}
}
