using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.DSCalculation;
using Oritax.TaxSimp.BusinessStructureUtilities;

namespace Oritax.TaxSimp.CommonPersistence
{

    public class CMPersisterVelocity : BrokerManagedPersisterVelocity, IDisposable
    {
        private bool disposed = false;

        public CMPersisterVelocity(SqlConnection connection, SqlTransaction transaction)
            : base(connection, transaction)
        {
        }

        #region IDisposable Implementation ---------------------------------------

        /// <summary>
        ///		Performs application-defined tasks associated with freeing, releasing, 
        ///		or resetting unmanaged resources.
        /// </summary>
        public new void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        ///		Performs application-defined tasks associated with freeing, releasing, 
        ///		or resetting unmanaged resources.
        /// </summary>
        /// <param name="disposing">
        ///		True if called by the Dispose() method, false if called by the finalizer.
        /// </param>
        protected void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    base.Dispose();
                }
                disposed = true;
            }
        }

        #endregion // IDisposable Implementation

        public override SqlConnection Connection
        {
            set
            {
                base.Connection = value;
            }
        }

        public override SqlTransaction Transaction
        {
            set
            {
                base.Transaction = value;
            }
        }

        /// <summary>
        /// This member function gets an specific instance of a CM from the database.  It is intended to be overridden
        /// at each level in the persistence hierarchy.  Each get function should not call the its base.Get() function
        /// but its base.Hydrate function.  This is because each type of CM will need to create a specific class to
        /// form the in-memory implementation of the instance.  The class will be derived from other base BMC classes
        /// ultimately from BrokeManagedComponent itself, so it may be hydrated by calling each base.Hydrate() function in turn
        /// up the hierarchy.
        /// </summary>
        /// <param name="cID">The instance ID (CID) of the CM.</param>
        /// <param name="primaryDS"></param>
        /// <returns>An interface to the BMC obtained.</returns>
        public override IBrokerManagedComponent Get(Guid cID, ref DataSet primaryDS)
        {
            IBrokerManagedComponent iBMC = new CalculationModuleBase();

            base.Hydrate(iBMC, cID, ref primaryDS);
            return iBMC;
        }

        public override void Save(IBrokerManagedComponent iBMC, ref DataSet persistedDS)
        {
            if (iBMC is ICalculationModule)
            {
                if (!this.Broker.MigrationInProgress)
                {
                    ICalculationModule iCM = (ICalculationModule)iBMC;
                    Guid cLID = iCM.CLID;
                    ILogicalModule lcm = broker.GetLogicalCM(cLID);

                    lcm.DivergeScenario(iCM);

                    this.Broker.ReleaseBrokerManagedComponent(lcm);
                }

                base.Save(iBMC, new BrokerManagedComponentDS(), ref persistedDS);
            }
        }

        public override void Save(IBrokerManagedComponent iBMC, ref DataSet persistedDS, ITransactionVelocity trx)
        {
            if (iBMC is ICalculationModule)
            {
                if (!this.Broker.MigrationInProgress)
                {
                    ICalculationModule iCM = (ICalculationModule)iBMC;
                    Guid cLID = iCM.CLID;
                    ILogicalModule lcm = broker.GetLogicalCM(cLID);

                    lcm.DivergeScenario(iCM);

                    this.Broker.ReleaseBrokerManagedComponent(lcm);
                }

                base.Save(iBMC, new BrokerManagedComponentDS(), ref persistedDS, trx);
            }
        }

        //public override void Delete(IBrokerManagedComponent iBMC)
        //{
        //    iBMC.Broker = this.Broker;
        //    iBMC.OnDeleteCM();
        //}

        public override void EstablishDatabase()
        {
            base.EstablishDatabase();
        }

        public override void ClearDatabase()
        {
            base.ClearDatabase();
        }
    }
}
