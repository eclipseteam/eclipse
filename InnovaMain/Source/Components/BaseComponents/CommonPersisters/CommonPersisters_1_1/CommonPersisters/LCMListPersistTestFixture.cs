using System;
using System.Diagnostics;
using System.Configuration;
using System.Data.SqlClient;
using System.Reflection;
using System.Data;

using NUnit.Framework;

using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.DSCalculation;


namespace Oritax.TaxSimp.CalculationPersistSQL
{
	/// <summary>
	/// Summary description for LCMListPersistTestFixture.
	/// </summary>
	/// 
	[TestFixture]
	public class LCMListPersistTestFixture
	{
		SqlConnection sqlCon = null;

		[Conditional("DEBUG")] [SetUp] public void PersisterTestSetup()
		{
			string sConn = ConfigurationSettings.AppSettings["TX360ENTERPRISE_DBCONNECTIONSTRING"];
			sqlCon = new SqlConnection(sConn);
			sqlCon.Open();
		}

		[Conditional("DEBUG")] [TearDown] public void PersisterTestTearDown()
		{
			sqlCon.Close();
			sqlCon.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterConstructionTest()
		{
			LCMListPersist lcmListPersist = new LCMListPersist(sqlCon);

			FieldInfo dataAdaptInfo = lcmListPersist.GetType().GetField("dsCommand", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlDataAdapter dsCommand = (SqlDataAdapter)dataAdaptInfo.GetValue(lcmListPersist);
			Assertion.AssertNotNull("SqlDataAdapter dsBMCCommand is Null",dsCommand);
			Assertion.AssertNotNull("dsCommand.SelectCommand is Null",dsCommand.SelectCommand);
			Assertion.AssertNotNull("dsCommand.InsertCommand is Null",dsCommand.InsertCommand);
			Assertion.AssertNotNull("dsCommand.UpdateCommand is Null",dsCommand.UpdateCommand);
			Assertion.AssertNotNull("dsCommand.DeleteCommand is Null",dsCommand.DeleteCommand);

			FieldInfo sqlConnectionInfo = lcmListPersist.GetType().GetField("sqlConnection", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlConnection sqlConnection = (SqlConnection)sqlConnectionInfo.GetValue(lcmListPersist);
			Assertion.AssertEquals("Connection has not been set", sqlConnection,sqlCon);

			FieldInfo sqlTransactionInfo = lcmListPersist.GetType().GetField("sqlTransaction", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlTransaction sqlTransaction = (SqlTransaction)sqlTransactionInfo.GetValue(lcmListPersist);
			Assertion.AssertNull("sqlTransaction is not null", sqlTransaction);

			lcmListPersist.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterConnectionTest()
		{
			LCMListPersist lcmListPersist = new LCMListPersist(sqlCon);
			lcmListPersist.Connection = sqlCon;

			FieldInfo sqlConnectionInfo = lcmListPersist.GetType().GetField("sqlConnection", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlConnection sqlConnection = (SqlConnection)sqlConnectionInfo.GetValue(lcmListPersist);
			Assertion.AssertEquals("Connection has not been set", sqlConnection,sqlCon);

			lcmListPersist.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterTransactionTest()
		{
			LCMListPersist lcmListPersist = new LCMListPersist(sqlCon);
			SqlTransaction sqlTran = sqlCon.BeginTransaction();
			lcmListPersist.Transaction = sqlTran;
			FieldInfo sqlTransactionInfo = lcmListPersist.GetType().GetField("sqlTransaction", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlTransaction sqlTransaction = (SqlTransaction)sqlTransactionInfo.GetValue(lcmListPersist);
			Assertion.AssertEquals("sqlTransaction has not been set", sqlTransaction, sqlTran);
			sqlTran.Commit();
			sqlTran.Dispose();
			lcmListPersist.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterFindAllTest()
		{
			LCMListPersist lcmListPersist = new LCMListPersist(sqlCon);
			DSLogicalModulesInfo data = (DSLogicalModulesInfo )lcmListPersist.FindAll();
			Assertion.AssertNotNull("Could not find any instance in CMLOGICAL Table",data);
			lcmListPersist.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterFindByTypeIdTest()
		{
			LCMListPersist lcmListPersist = new LCMListPersist(sqlCon);
			DSLogicalModulesInfo allData = (DSLogicalModulesInfo)lcmListPersist.FindAll();
			Assertion.AssertNotNull("Could not find any instance in CMLOGICAL Table",allData);

			DataRow firstRow = allData.Tables[DSLogicalModulesInfo.LCMLIST_TABLE].Rows[0];
			DSLogicalModulesInfo  data = (DSLogicalModulesInfo )lcmListPersist.FindByTypeId((Guid)firstRow[DSLogicalModulesInfo.LCM_TYPEID_FIELD]);
			Assertion.AssertNotNull("Could not find instance",data);
			lcmListPersist.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterFindByParentIdTest()
		{
			LCMListPersist lcmListPersist = new LCMListPersist(sqlCon);
			DSLogicalModulesInfo allData = (DSLogicalModulesInfo)lcmListPersist.FindAll();
			Assertion.AssertNotNull("Could not find any instance in CMLOGICAL Table",allData);

			DataRow firstRow = null;
			for(int i=0; i< allData.Tables[DSLogicalModulesInfo.LCMLIST_TABLE].Rows.Count; i++)
			{
				if( ((Guid)allData.Tables[DSLogicalModulesInfo.LCMLIST_TABLE].Rows[i][DSLogicalModulesInfo.LCM_PARENTID_FIELD]) != Guid.Empty)
				{
					firstRow = allData.Tables[DSLogicalModulesInfo.LCMLIST_TABLE].Rows[i];
					break;
				}
			}
			Assertion.AssertNotNull("Could not find a Logical Module with a Parent", firstRow);

			DSLogicalModulesInfo  data = (DSLogicalModulesInfo )lcmListPersist.FindByParentId((Guid)firstRow[DSLogicalModulesInfo.LCM_PARENTID_FIELD]);
			Assertion.AssertNotNull("Could not find instance",data);
			lcmListPersist.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterFindByTypeNameTest()
		{
			LCMListPersist lcmListPersist = new LCMListPersist(sqlCon);
			DSLogicalModulesInfo allData = (DSLogicalModulesInfo)lcmListPersist.FindAll();
			Assertion.AssertNotNull("Could not find any instance in CMLOGICAL Table",allData);

			DataRow firstRow = allData.Tables[DSLogicalModulesInfo.LCMLIST_TABLE].Rows[0];
			DSLogicalModulesInfo data = (DSLogicalModulesInfo)lcmListPersist.FindByTypeName((string)firstRow[DSLogicalModulesInfo.LCM_TYPENAME_FIELD]);
			Assertion.AssertNotNull("Could not find instance",data);
			lcmListPersist.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterFindByInstanceIdTest()
		{
			LCMListPersist lcmListPersist = new LCMListPersist(sqlCon);
			DSLogicalModulesInfo allData = (DSLogicalModulesInfo)lcmListPersist.FindAll();
			Assertion.AssertNotNull("Could not find any instance in CMLOGICAL Table",allData);

			DataRow firstRow = allData.Tables[DSLogicalModulesInfo.LCMLIST_TABLE].Rows[0];
			DSLogicalModulesInfo data = (DSLogicalModulesInfo)lcmListPersist.FindByInstanceID((Guid)firstRow[DSLogicalModulesInfo.LCM_CLID_FIELD]);
			Assertion.AssertNotNull("Could not find instance",data);
			lcmListPersist.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterFindLCMsByCIIDTest()
		{
			CMListPersist cmListPersist = new CMListPersist(sqlCon);
			DSBMCInfo allData = (DSBMCInfo)cmListPersist.FindAll();
			Assertion.AssertNotNull("Could not find any instance in CINSTANCES Table",allData);
			DataRow firstRow = allData.Tables[DSBMCInfo.CMLIST_TABLE].Rows[0];

			LCMListPersist lcmListPersist = new LCMListPersist(sqlCon);
			DSLogicalModulesInfo data = (DSLogicalModulesInfo)lcmListPersist.FindLCMsByCIID((Guid)firstRow[DSBMCInfo.CMID_FIELD]);
			Assertion.AssertNotNull("Could not find instance",data);
			lcmListPersist.Dispose();
			cmListPersist.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterEstablishDBTest()
		{
			LCMListPersist lcmListPersist = new LCMListPersist(sqlCon);

			SqlTransaction sqlTran = sqlCon.BeginTransaction();
			lcmListPersist.Transaction = sqlTran;
			try
			{
				lcmListPersist.EstablishDatabase();
			}
			finally
			{
				sqlTran.Rollback();	//DO NOT CORRUPT THE DATABASE
			}
			lcmListPersist.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterClearDBTest()
		{
			LCMListPersist lcmListPersist = new LCMListPersist(sqlCon);

			SqlTransaction sqlTran = sqlCon.BeginTransaction();
			lcmListPersist.Transaction = sqlTran;
			try
			{
				lcmListPersist.ClearDatabase();
			}
			finally
			{
				sqlTran.Rollback();	//DO NOT CORRUPT THE DATABASE
			}
			lcmListPersist.Dispose();
		}
	}
}
