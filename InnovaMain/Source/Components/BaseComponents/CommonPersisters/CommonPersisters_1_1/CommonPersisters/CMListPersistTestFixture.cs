using System;
using System.Diagnostics;
using System.Configuration;
using System.Data.SqlClient;
using System.Reflection;
using System.Data;

using NUnit.Framework;

using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.DSCalculation;


namespace Oritax.TaxSimp.CalculationPersistSQL
{
	/// <summary>
	/// Summary description for CMListPersistTestFixture.
	/// </summary>
	/// 
	[TestFixture]
	public class CMListPersistTestFixture
	{
		SqlConnection sqlCon = null;

		[Conditional("DEBUG")] [SetUp] public void PersisterTestSetup()
		{
			string sConn = ConfigurationSettings.AppSettings["TX360ENTERPRISE_DBCONNECTIONSTRING"];
			sqlCon = new SqlConnection(sConn);
			sqlCon.Open();
		}

		[Conditional("DEBUG")] [TearDown] public void PersisterTestTearDown()
		{
			sqlCon.Close();
			sqlCon.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterConstructionTest()
		{
			CMListPersist cmListPersist = new CMListPersist(sqlCon);

			FieldInfo dataAdaptInfo = cmListPersist.GetType().GetField("dsCommand", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlDataAdapter dsCommand = (SqlDataAdapter)dataAdaptInfo.GetValue(cmListPersist);
			Assertion.AssertNotNull("SqlDataAdapter dsBMCCommand is Null",dsCommand);
			Assertion.AssertNotNull("dsCommand.SelectCommand is Null",dsCommand.SelectCommand);
			Assertion.AssertNotNull("dsCommand.InsertCommand is Null",dsCommand.InsertCommand);
			Assertion.AssertNotNull("dsCommand.UpdateCommand is Null",dsCommand.UpdateCommand);
			Assertion.AssertNotNull("dsCommand.DeleteCommand is Null",dsCommand.DeleteCommand);

			FieldInfo sqlConnectionInfo = cmListPersist.GetType().GetField("sqlConnection", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlConnection sqlConnection = (SqlConnection)sqlConnectionInfo.GetValue(cmListPersist);
			Assertion.AssertEquals("Connection has not been set", sqlConnection,sqlCon);

			FieldInfo sqlTransactionInfo = cmListPersist.GetType().GetField("sqlTransaction", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlTransaction sqlTransaction = (SqlTransaction)sqlTransactionInfo.GetValue(cmListPersist);
			Assertion.AssertNull("sqlTransaction is not null", sqlTransaction);

			cmListPersist.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterConnectionTest()
		{
			CMListPersist cmListPersist = new CMListPersist(sqlCon);
			cmListPersist.Connection = sqlCon;

			FieldInfo sqlConnectionInfo = cmListPersist.GetType().GetField("sqlConnection", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlConnection sqlConnection = (SqlConnection)sqlConnectionInfo.GetValue(cmListPersist);
			Assertion.AssertEquals("Connection has not been set", sqlConnection,sqlCon);

			cmListPersist.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterTransactionTest()
		{
			CMListPersist cmListPersist = new CMListPersist(sqlCon);
			SqlTransaction sqlTran = sqlCon.BeginTransaction();
			cmListPersist.Transaction = sqlTran;
			FieldInfo sqlTransactionInfo = cmListPersist.GetType().GetField("sqlTransaction", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlTransaction sqlTransaction = (SqlTransaction)sqlTransactionInfo.GetValue(cmListPersist);
			Assertion.AssertEquals("sqlTransaction has not been set", sqlTransaction, sqlTran);
			sqlTran.Commit();
			sqlTran.Dispose();
			cmListPersist.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterFindAllTest()
		{
			CMListPersist cmListPersist = new CMListPersist(sqlCon);
			DSBMCInfo  data = (DSBMCInfo )cmListPersist.FindAll();
			Assertion.AssertNotNull("Could not find any instance in CINSTANCES,CMTYPES Table",data);
			cmListPersist.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterFindByTypeIdTest()
		{
			CMListPersist cmListPersist = new CMListPersist(sqlCon);
			DSBMCInfo allData = (DSBMCInfo)cmListPersist.FindAll();
			Assertion.AssertNotNull("Could not find any instance in CINSTANCES,CMTYPES Table",allData);

			DataRow firstRow = allData.Tables[DSBMCInfo.CMLIST_TABLE].Rows[0];
			DSBMCInfo  data = (DSBMCInfo )cmListPersist.FindByTypeId((Guid)firstRow[DSBMCInfo.CMTYPEID_FIELD]);
			Assertion.AssertNotNull("Could not find instance",data);
			cmListPersist.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterFindByTypeNameTest()
		{
			CMListPersist cmListPersist = new CMListPersist(sqlCon);
			DSBMCInfo allData = (DSBMCInfo)cmListPersist.FindAll();
			Assertion.AssertNotNull("Could not find any instance in CINSTANCES,CMTYPES Table",allData);

			DataRow firstRow = allData.Tables[DSBMCInfo.CMLIST_TABLE].Rows[0];
			DSBMCInfo data = (DSBMCInfo)cmListPersist.FindByTypeName((string)firstRow[DSBMCInfo.CMTYPENAME_FIELD]);
			Assertion.AssertNotNull("Could not find instance",data);
			cmListPersist.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterFindByNameTest()
		{
			CMListPersist cmListPersist = new CMListPersist(sqlCon);
			DSBMCInfo allData = (DSBMCInfo)cmListPersist.FindAll();
			Assertion.AssertNotNull("Could not find any instance in CINSTANCES,CMTYPES Table",allData);

			DataRow firstRow = allData.Tables[DSBMCInfo.CMLIST_TABLE].Rows[0];
			DSBMCInfo data = (DSBMCInfo)cmListPersist.FindByName((string)firstRow[DSBMCInfo.CMTYPENAME_FIELD]);
			Assertion.AssertNotNull("Could not find instance",data);
			cmListPersist.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterFindByNameAndTypeTest()
		{
			CMListPersist cmListPersist = new CMListPersist(sqlCon);
			DSBMCInfo allData = (DSBMCInfo)cmListPersist.FindAll();
			Assertion.AssertNotNull("Could not find any instance in CINSTANCES,CMTYPES Table",allData);

			DataRow firstRow = allData.Tables[DSBMCInfo.CMLIST_TABLE].Rows[0];
			DSBMCInfo data = (DSBMCInfo)cmListPersist.FindByName((string)firstRow[DSBMCInfo.CMTYPENAME_FIELD],(string)firstRow[DSBMCInfo.CMTYPENAME_FIELD]);
			Assertion.AssertNotNull("Could not find instance",data);
			cmListPersist.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterFindByInstanceIdTest()
		{
			CMListPersist cmListPersist = new CMListPersist(sqlCon);
			DSBMCInfo allData = (DSBMCInfo)cmListPersist.FindAll();
			Assertion.AssertNotNull("Could not find any instance in CINSTANCES,CMTYPES Table",allData);

			DataRow firstRow = allData.Tables[DSBMCInfo.CMLIST_TABLE].Rows[0];
			DSCalculationModulesInfo data = (DSCalculationModulesInfo)cmListPersist.FindByInstanceID((Guid)firstRow[DSBMCInfo.CMID_FIELD]);
			Assertion.AssertNotNull("Could not find instance",data);
			cmListPersist.Dispose();
		}


		//exercise the code to catch exceptions
		[Conditional("DEBUG")] [Test] public void PersisterUpdateTest()
		{
			CMListPersist cmListPersist = new CMListPersist(sqlCon);
			DSBMCInfo allData = (DSBMCInfo)cmListPersist.FindAll();
			Assertion.AssertNotNull("Could not find any instance in CINSTANCES,CMTYPES Table",allData);

			SqlTransaction sqlTran = sqlCon.BeginTransaction();
			cmListPersist.Transaction = sqlTran;
			try
			{
				cmListPersist.Update(allData);
			}
			finally
			{
				sqlTran.Rollback();	//DO NOT CORRUPT THE DATABASE
			}
			
			cmListPersist.Dispose();
		}


		[Conditional("DEBUG")] [Test] public void PersisterEstablishDBTest()
		{
			CMListPersist cmListPersist = new CMListPersist(sqlCon);

			SqlTransaction sqlTran = sqlCon.BeginTransaction();
			cmListPersist.Transaction = sqlTran;
			try
			{
				cmListPersist.EstablishDatabase();
			}
			finally
			{
				sqlTran.Rollback();	//DO NOT CORRUPT THE DATABASE
			}
			cmListPersist.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterClearDBTest()
		{
			CMListPersist cmListPersist = new CMListPersist(sqlCon);

			SqlTransaction sqlTran = sqlCon.BeginTransaction();
			cmListPersist.Transaction = sqlTran;
			try
			{
				cmListPersist.ClearDatabase();
			}
			finally
			{
				sqlTran.Rollback();	//DO NOT CORRUPT THE DATABASE
			}
			cmListPersist.Dispose();
		}
	}
}
