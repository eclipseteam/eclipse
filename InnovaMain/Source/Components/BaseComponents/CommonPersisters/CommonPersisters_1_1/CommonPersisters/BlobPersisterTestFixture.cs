using System;
using System.Diagnostics;
using System.Configuration;
using System.Data.SqlClient;
using System.Reflection;
using System.Data;

using NUnit.Framework;

using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.DSCalculation;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.BusinessStructureUtilities;




namespace Oritax.TaxSimp.CalculationPersistSQL
{
	/// <summary>
	/// Summary description for BlobPersisterTestFixture.
	/// </summary>
	/// 
	[TestFixture]
	public class BlobPersisterTestFixture
	{
		SqlConnection sqlCon = null;

		[Conditional("DEBUG")] [SetUp] public void PersisterTestSetup()
		{
			string sConn = ConfigurationSettings.AppSettings["TX360ENTERPRISE_DBCONNECTIONSTRING"];
			sqlCon = new SqlConnection(sConn);
			sqlCon.Open();
		}

		[Conditional("DEBUG")] [TearDown] public void PersisterTestTearDown()
		{
			sqlCon.Close();
			sqlCon.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterConstructionTest()
		{
			BlobPersister blobPersister = new BlobPersister(sqlCon);

			FieldInfo dataAdaptInfo = blobPersister.GetType().GetField("dsCommand", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlDataAdapter dsCommand = (SqlDataAdapter)dataAdaptInfo.GetValue(blobPersister);
			Assertion.AssertNotNull("SqlDataAdapter dsBMCCommand is Null",dsCommand);
			Assertion.AssertNotNull("dsCommand.SelectCommand is Null",dsCommand.SelectCommand);
			Assertion.AssertNotNull("dsCommand.InsertCommand is Null",dsCommand.InsertCommand);
			Assertion.AssertNotNull("dsCommand.UpdateCommand is Null",dsCommand.UpdateCommand);
			Assertion.AssertNotNull("dsCommand.DeleteCommand is Null",dsCommand.DeleteCommand);


			FieldInfo sqlConnectionInfo = blobPersister.GetType().GetField("sqlConnection", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlConnection sqlConnection = (SqlConnection)sqlConnectionInfo.GetValue(blobPersister);
			Assertion.AssertEquals("Connection has not been set", sqlConnection,sqlCon);

			FieldInfo sqlTransactionInfo = blobPersister.GetType().GetField("sqlTransaction", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlTransaction sqlTransaction = (SqlTransaction)sqlTransactionInfo.GetValue(blobPersister);
			Assertion.AssertNull("sqlTransaction is not null", sqlTransaction);

			blobPersister.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterConnectionTest()
		{
			BlobPersister blobPersister = new BlobPersister(sqlCon);
			blobPersister.Connection = sqlCon;

			FieldInfo sqlConnectionInfo = blobPersister.GetType().GetField("sqlConnection", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlConnection sqlConnection = (SqlConnection)sqlConnectionInfo.GetValue(blobPersister);
			Assertion.AssertEquals("Connection has not been set", sqlConnection,sqlCon);

			blobPersister.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterTransactionTest()
		{
			BlobPersister blobPersister = new BlobPersister(sqlCon);
			SqlTransaction sqlTran = sqlCon.BeginTransaction();
			blobPersister.Transaction = sqlTran;
			FieldInfo sqlTransactionInfo = blobPersister.GetType().GetField("sqlTransaction", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlTransaction sqlTransaction = (SqlTransaction)sqlTransactionInfo.GetValue(blobPersister);
			Assertion.AssertEquals("sqlTransaction has not been set", sqlTransaction, sqlTran);
			sqlTran.Commit();
			sqlTran.Dispose();
			blobPersister.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterEstablishDBTest()
		{
			BlobPersister blobPersister = new BlobPersister(sqlCon);

			SqlTransaction sqlTran = sqlCon.BeginTransaction();
			blobPersister.Transaction = sqlTran;
			try
			{
				blobPersister.EstablishDatabase();
			}
			finally
			{
				sqlTran.Rollback();	//DO NOT CORRUPT THE DATABASE
			}
			blobPersister.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterClearDBTest()
		{
			BlobPersister blobPersister = new BlobPersister(sqlCon);

			SqlTransaction sqlTran = sqlCon.BeginTransaction();
			blobPersister.Transaction = sqlTran;
			try
			{
				blobPersister.ClearDatabase();
			}
			finally
			{
				sqlTran.Rollback();	//DO NOT CORRUPT THE DATABASE
			}
			blobPersister.Dispose();
		}
		[Conditional("DEBUG")] [Test] public void PersisterGetByInstanceIdTest()
		{

			//go get the first user Instance
			CMListPersist bmcList= new CMListPersist(sqlCon);
			DSBMCInfo data = (DSBMCInfo)bmcList.FindByTypeName("Organization");

			if(data.Tables[DSBMCInfo.CMLIST_TABLE].Rows.Count == 0)
			{
				throw new Exception("This Test requires an Organization instance");
			}
			DataRow firstRow=data.Tables[DSBMCInfo.CMLIST_TABLE].Rows[0];

			BlobPersister blobPersister = new BlobPersister(sqlCon);

			IBrokerManagedComponent ibmc = blobPersister.GetByInstanceId((Guid)firstRow[DSBMCInfo.CMID_FIELD]);
			Assertion.AssertNotNull("Could not find instance",ibmc);
			bmcList.Dispose();
			blobPersister.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterSaveCMTest()
		{

			//go get the first user Instance
			CMListPersist bmcList= new CMListPersist(sqlCon);
			DSBMCInfo data = (DSBMCInfo)bmcList.FindByTypeName("Organization");

			if(data.Tables[DSBMCInfo.CMLIST_TABLE].Rows.Count == 0)
			{
				throw new Exception("This Test requires an Organization instance");
			}
			DataRow firstRow=data.Tables[DSBMCInfo.CMLIST_TABLE].Rows[0];
			
			BlobPersister blobPersister = new BlobPersister(sqlCon);
			IBrokerManagedComponent ibmc = blobPersister.GetByInstanceId((Guid)firstRow[DSBMCInfo.CMID_FIELD]);

			SqlTransaction sqlTran = sqlCon.BeginTransaction();
			blobPersister.Transaction = sqlTran;
			try
			{
				blobPersister.SaveCM(ibmc);
			}
			finally
			{
				sqlTran.Rollback();	//DO NOT CORRUPT THE DATABASE
			}

			bmcList.Dispose();
			blobPersister.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterDeleteTest()
		{

			//go get the first user Instance
			CMListPersist bmcList= new CMListPersist(sqlCon);
			DSBMCInfo data = (DSBMCInfo)bmcList.FindByTypeName("Organization");

			if(data.Tables[DSBMCInfo.CMLIST_TABLE].Rows.Count == 0)
			{
				throw new Exception("This Test requires an Organization instance");
			}
			DataRow firstRow=data.Tables[DSBMCInfo.CMLIST_TABLE].Rows[0];
			
			BlobPersister blobPersister = new BlobPersister(sqlCon);
			IBrokerManagedComponent ibmc = blobPersister.GetByInstanceId((Guid)firstRow[DSBMCInfo.CMID_FIELD]);

			SqlTransaction sqlTran = sqlCon.BeginTransaction();
			blobPersister.Transaction = sqlTran;
			try
			{
				blobPersister.Delete(ibmc);
			}
			finally
			{
				sqlTran.Rollback();	//DO NOT CORRUPT THE DATABASE
			}

			bmcList.Dispose();
			blobPersister.Dispose();
		}


	}
}
