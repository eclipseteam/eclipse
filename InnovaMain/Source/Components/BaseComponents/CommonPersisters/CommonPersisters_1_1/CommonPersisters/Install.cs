using System;

namespace Oritax.TaxSimp.CommonPersistence
{
	/// <summary>
	/// Summary description for Install.
	/// </summary>
	public class Install
	{
		#region INSTALLATION PROPERTIES

		// Assembly Installation Properties
		public const string ASSEMBLY_ID="AC4FEC04-ACFE-4698-929D-C9927D4FE49C";
		public const string ASSEMBLY_NAME="CommonPersisters_1_1";
		public const string ASSEMBLY_DISPLAYNAME="CommonPersisters V1.1";
		public const string ASSEMBLY_MAJORVERSION="1";
		public const string ASSEMBLY_MINORVERSION="1";
		public const string ASSEMBLY_DATAFORMAT="0";
		//public const string ASSEMBLY_REVISION="1"; 
		public const string ASSEMBLY_REVISION="2"; //2005.1

		// Data Model
		public const string ASSEMBLY_PERSISTERASSEMBLY="CommonPersisters_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";
		public const string ASSEMBLY_PERSISTERCLASS="Oritax.TaxSimp.CommonPersistence.BlobPersister";

		#endregion
	}
}
