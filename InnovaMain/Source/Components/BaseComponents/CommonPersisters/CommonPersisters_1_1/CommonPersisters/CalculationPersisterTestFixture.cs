using System;
using System.Diagnostics;
using System.Configuration;
using System.Data.SqlClient;
using System.Reflection;
using System.Data;

using NUnit.Framework;

using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.DSCalculation;




namespace Oritax.TaxSimp.CalculationPersistSQL
{
	/// <summary>
	/// Summary description for CalculationPersistTestFixture.
	/// </summary>
	/// 
	[TestFixture]
	public class CalculationPersistTestFixture
	{
		SqlConnection sqlCon = null;

		[Conditional("DEBUG")] [SetUp] public void PersisterTestSetup()
		{
			string sConn = ConfigurationSettings.AppSettings["TX360ENTERPRISE_DBCONNECTIONSTRING"];
			sqlCon = new SqlConnection(sConn);
			sqlCon.Open();
		}

		[Conditional("DEBUG")] [TearDown] public void PersisterTestTearDown()
		{
			sqlCon.Close();
			sqlCon.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterConstructionTest()
		{
			CalculationPersist calcPersist = new CalculationPersist(sqlCon);

			FieldInfo dataAdaptInfo = calcPersist.GetType().GetField("dsCommand", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlDataAdapter dsCommand = (SqlDataAdapter)dataAdaptInfo.GetValue(calcPersist);
			Assertion.AssertNotNull("SqlDataAdapter dsBMCCommand is Null",dsCommand);
			Assertion.AssertNotNull("dsCommand.SelectCommand is Null",dsCommand.SelectCommand);
			Assertion.AssertNotNull("dsCommand.InsertCommand is Null",dsCommand.InsertCommand);
			Assertion.AssertNotNull("dsCommand.UpdateCommand is Null",dsCommand.UpdateCommand);
			Assertion.AssertNotNull("dsCommand.DeleteCommand is Null",dsCommand.DeleteCommand);

			FieldInfo sqlConnectionInfo = calcPersist.GetType().GetField("sqlConnection", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlConnection sqlConnection = (SqlConnection)sqlConnectionInfo.GetValue(calcPersist);
			Assertion.AssertEquals("Connection has not been set", sqlConnection,sqlCon);

			FieldInfo sqlTransactionInfo = calcPersist.GetType().GetField("sqlTransaction", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlTransaction sqlTransaction = (SqlTransaction)sqlTransactionInfo.GetValue(calcPersist);
			Assertion.AssertNull("sqlTransaction is not null", sqlTransaction);

			calcPersist.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterConnectionTest()
		{
			CalculationPersist calcPersist = new CalculationPersist(sqlCon);
			calcPersist.Connection = sqlCon;

			FieldInfo sqlConnectionInfo = calcPersist.GetType().GetField("sqlConnection", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlConnection sqlConnection = (SqlConnection)sqlConnectionInfo.GetValue(calcPersist);
			Assertion.AssertEquals("Connection has not been set", sqlConnection,sqlCon);

			calcPersist.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterTransactionTest()
		{
			CalculationPersist calcPersist = new CalculationPersist(sqlCon);
			SqlTransaction sqlTran = sqlCon.BeginTransaction();
			calcPersist.Transaction = sqlTran;
			FieldInfo sqlTransactionInfo = calcPersist.GetType().GetField("sqlTransaction", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlTransaction sqlTransaction = (SqlTransaction)sqlTransactionInfo.GetValue(calcPersist);
			Assertion.AssertEquals("sqlTransaction has not been set", sqlTransaction, sqlTran);
			sqlTran.Commit();
			sqlTran.Dispose();
			calcPersist.Dispose();
		}

		//Just exercise the code testing for exceptions
		[Conditional("DEBUG")] [Test] public void PersisterFindAllTest()
		{
			CalculationPersist calcPersist = new CalculationPersist(sqlCon);
			DSCalculationModule data = (DSCalculationModule)calcPersist.FindAll();
			Assertion.AssertNotNull("Could not find any instance in BLOBPERSIST Table",data);
			calcPersist.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterFindByInstanceIdTest()
		{
			CalculationPersist calcPersist = new CalculationPersist(sqlCon);
			DSCalculationModule allData = (DSCalculationModule)calcPersist.FindAll();

			if(allData.Tables[DSCalculationModule.INSTANCE_TABLE].Rows.Count == 0)
			{
				Assertion.AssertNotNull("Could not find any instance in BLOBPERSIST Table",allData);
			}
			DataRow firstRow = allData.Tables[DSCalculationModule.INSTANCE_TABLE].Rows[0];
			DSCalculationModule data = (DSCalculationModule)calcPersist.FindByInstanceId((Guid)firstRow[DSCalculationModule.INSTANCEID_FIELD]);
			Assertion.AssertNotNull("Could not find instance",data);
			calcPersist.Dispose();
		}


		[Conditional("DEBUG")] [Test] public void PersisterEstablishDBTest()
		{
			CalculationPersist calcPersist = new CalculationPersist(sqlCon);

			SqlTransaction sqlTran = sqlCon.BeginTransaction();
			calcPersist.Transaction = sqlTran;
			try
			{
				calcPersist.EstablishDatabase();
			}
			finally
			{
				sqlTran.Rollback();	//DO NOT CORRUPT THE DATABASE
			}
			calcPersist.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterClearDBTest()
		{
			CalculationPersist calcPersist = new CalculationPersist(sqlCon);

			SqlTransaction sqlTran = sqlCon.BeginTransaction();
			calcPersist.Transaction = sqlTran;
			try
			{
				calcPersist.ClearDatabase();
			}
			finally
			{
				sqlTran.Rollback();	//DO NOT CORRUPT THE DATABASE
			}
			calcPersist.Dispose();
		}
	}
}
