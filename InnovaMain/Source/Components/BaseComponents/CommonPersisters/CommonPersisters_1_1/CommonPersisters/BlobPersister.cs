using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;

using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.DSCalculation;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.VelocityCache;

namespace Oritax.TaxSimp.CommonPersistence
{
    /// <summary>
    /// Summary description for BlobPersister.
    /// </summary>
    public class BlobPersister : CMPersister, IDisposable
    {
        public long CMBlobSize
        {
            set
            {
                dsCommand.InsertCommand.Parameters["@PERSISTENT_DATA"].Size = (int)value;
                dsCommand.UpdateCommand.Parameters["@PERSISTENT_DATA"].Size = (int)value;
            }
        }

        private SqlDataAdapter dsCommand;
        private bool disposed = false;

        private static VelocityCacheString _VelocityDataCache = null;

        // As BlobPersister is the only object which has access to BLOBPERSIST table in DB,
        // there's no concurrency control issues here.
        private static VelocityCacheString _DataCache
        {
            get
            {
                if (_VelocityDataCache == null)
                    _VelocityDataCache = new VelocityCacheString();

                return _VelocityDataCache;
            }
        }

        private const string BLOB_ID_PREFIX = "BL";

        public BlobPersister(SqlConnection connection, SqlTransaction transaction)
            : this(0, connection, transaction)
        {
        }

        public BlobPersister(long blobSize, SqlConnection connection, SqlTransaction transaction)
            : base(connection, transaction)
        {
            //
            // Create the adapter
            //
            dsCommand = new SqlDataAdapter();
            //
            // Create the DataSetCommand SelectCommand
            //
            dsCommand.SelectCommand = DBCommandFactory.Instance.NewSqlCommand();
            dsCommand.SelectCommand.CommandType = CommandType.Text;
            dsCommand.SelectCommand.Connection = connection;
            dsCommand.SelectCommand.Transaction = transaction;

            dsCommand.InsertCommand = DBCommandFactory.Instance.NewSqlCommand();
            dsCommand.InsertCommand.CommandText = "INSERT INTO BLOBPERSIST (INSTANCEID,NAME,TYPE,PERSISTENT_DATA) VALUES(@INSTANCEID,@NAME,@TYPE,@PERSISTENT_DATA)";
            dsCommand.InsertCommand.CommandType = CommandType.Text;
            dsCommand.InsertCommand.Parameters.Add(new SqlParameter("@INSTANCEID", SqlDbType.UniqueIdentifier, 16, DSCalculationModule.INSTANCEID_FIELD));
            dsCommand.InsertCommand.Parameters.Add(new SqlParameter("@NAME", SqlDbType.VarChar, 128, DSCalculationModule.NAME_FIELD));
            dsCommand.InsertCommand.Parameters.Add(new SqlParameter("@TYPE", SqlDbType.UniqueIdentifier, 16, DSCalculationModule.TYPE_FIELD));
            dsCommand.InsertCommand.Parameters.Add(new SqlParameter("@PERSISTENT_DATA", SqlDbType.Image, (int)blobSize, DSCalculationModule.PERSISTANT_DATA_FIELD));
            dsCommand.InsertCommand.Connection = dsCommand.SelectCommand.Connection;
            dsCommand.InsertCommand.Transaction = dsCommand.SelectCommand.Transaction;

            dsCommand.UpdateCommand = DBCommandFactory.Instance.NewSqlCommand();
            dsCommand.UpdateCommand.CommandText = "UPDATE BLOBPERSIST SET PERSISTENT_DATA=@PERSISTENT_DATA, TYPE=@TYPE, NAME=@NAME WHERE INSTANCEID=@INSTANCEID";
            dsCommand.UpdateCommand.CommandType = CommandType.Text;
            dsCommand.UpdateCommand.Parameters.Add(new SqlParameter("@PERSISTENT_DATA", SqlDbType.Image, (int)blobSize, DSCalculationModule.PERSISTANT_DATA_FIELD));
            dsCommand.UpdateCommand.Parameters.Add(new SqlParameter("@NAME", SqlDbType.VarChar, 128, DSCalculationModule.NAME_FIELD));
            dsCommand.UpdateCommand.Parameters.Add(new SqlParameter("@TYPE", SqlDbType.UniqueIdentifier, 16, DSCalculationModule.TYPE_FIELD));
            dsCommand.UpdateCommand.Parameters.Add(new SqlParameter("@INSTANCEID", SqlDbType.UniqueIdentifier, 16, DSCalculationModule.INSTANCEID_FIELD));
            dsCommand.UpdateCommand.Connection = dsCommand.SelectCommand.Connection;
            dsCommand.UpdateCommand.Transaction = dsCommand.SelectCommand.Transaction;

            dsCommand.DeleteCommand = DBCommandFactory.Instance.NewSqlCommand();
            dsCommand.DeleteCommand.CommandText = "DELETE BLOBPERSIST WHERE BLOBPERSIST.INSTANCEID=@INSTANCEID";
            dsCommand.DeleteCommand.CommandType = CommandType.Text;
            dsCommand.DeleteCommand.Parameters.Add(new SqlParameter("@INSTANCEID", SqlDbType.UniqueIdentifier, 16, DSCalculationModule.INSTANCEID_FIELD));
            dsCommand.DeleteCommand.Connection = dsCommand.SelectCommand.Connection;
            dsCommand.DeleteCommand.Transaction = dsCommand.SelectCommand.Transaction;

            dsCommand.TableMappings.Add("Table", DSCalculationModule.INSTANCE_TABLE);
        }

        #region IDisposable Implementation ---------------------------------------

        /// <summary>
        ///		Performs application-defined tasks associated with freeing, releasing, 
        ///		or resetting unmanaged resources.
        /// </summary>
        public new void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        ///		Performs application-defined tasks associated with freeing, releasing, 
        ///		or resetting unmanaged resources.
        /// </summary>
        /// <param name="disposing">
        ///		True if called by the Dispose() method, false if called by the finalizer.
        /// </param>
        protected new void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    base.Dispose();
                    dsCommand.Dispose();
                }
                disposed = true;
            }
        }

        #endregion // IDisposable Implementation

        public override SqlConnection Connection
        {
            set
            {
                base.Connection = value;

                dsCommand.InsertCommand.Connection = value;
                dsCommand.SelectCommand.Connection = value;
                dsCommand.UpdateCommand.Connection = value;
                dsCommand.DeleteCommand.Connection = value;
            }
        }

        public override SqlTransaction Transaction
        {
            set
            {
                base.Transaction = value;

                dsCommand.InsertCommand.Transaction = value;
                dsCommand.SelectCommand.Transaction = value;
                dsCommand.UpdateCommand.Transaction = value;
                dsCommand.DeleteCommand.Transaction = value;
            }
        }

        private static void PooledPut(object state)
        {
            object[] objArray = (object[])state;

            string id = (string)objArray[0];
            object data = objArray[1];

            BlobPersister._DataCache.PutObject(id, data);
        }

        private void SaveToVelocity(string id, object data)
        {
            object[] state = new object[2];

            state[0] = id;
            state[1] = data;

            WaitCallback callBack = new WaitCallback(PooledPut);

            ThreadPool.QueueUserWorkItem(callBack, state);
        }

        public override IBrokerManagedComponent Get(Guid cID, ref DataSet primaryDS)
        {
            DSCalculationModule data;
            DSCalculationModule dataInCache = null;

            if (VelocityConfiguration.IsBlobPersisterCacheEnabled)
                dataInCache = (DSCalculationModule)BlobPersister._DataCache.GetObejct(BLOB_ID_PREFIX + cID.ToString());

            if (dataInCache == null)
            {

                dsCommand.SelectCommand.CommandText = "SELECT [INSTANCEID] "
                + " ,[NAME] "
                + " ,[TYPE] "
                + " ,[PERSISTENT_DATA] "
                + " ,CMSCENARIO.CSID "
                + " ,CMSCENARIO.CLID "
                + " FROM BLOBPERSIST LEFT JOIN CMSCENARIO ON "
                + " BLOBPERSIST.INSTANCEID = CMSCENARIO.CIID AND BLOBPERSIST.TYPE =  CMSCENARIO.CTID "
              + " WHERE BLOBPERSIST.INSTANCEID='{" + cID.ToString() + "}'";

                //dsCommand.SelectCommand.CommandText = "SELECT BLOBPERSIST.INSTANCEID,BLOBPERSIST.NAME,BLOBPERSIST.TYPE,BLOBPERSIST.PERSISTENT_DATA"
                //    + " FROM BLOBPERSIST"
                //    + " WHERE BLOBPERSIST.INSTANCEID='{" + cID.ToString() + "}'";

                data = new DSCalculationModule();
                dsCommand.Fill(data);

                // Put ojbect into Velocity Cache after data is retrieved from DataBase.
                if (VelocityConfiguration.IsBlobPersisterCacheEnabled)
                    this.SaveToVelocity(BLOB_ID_PREFIX + cID.ToString(), data);
            }
            else
                data = dataInCache;

            //dsCommand.SelectCommand.CommandText = "SELECT BLOBPERSIST.INSTANCEID,BLOBPERSIST.NAME,BLOBPERSIST.TYPE,BLOBPERSIST.PERSISTENT_DATA"
            //    + " FROM BLOBPERSIST"
            //    + " WHERE BLOBPERSIST.INSTANCEID='{" + cID.ToString() + "}'";

            //DSCalculationModule data = new DSCalculationModule();
            //dsCommand.Fill(data);

            MemoryStream memoryStream = new MemoryStream((byte[])data.Tables[DSCalculationModule.INSTANCE_TABLE].Rows[0][DSCalculationModule.PERSISTANT_DATA_FIELD]);
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            long size = memoryStream.Length;
            IBrokerManagedComponent iBMC = (IBrokerManagedComponent)binaryFormatter.Deserialize(memoryStream);
            //This needs to be sourced via CMSENARIO TABLE properly. Also we need to ensure all asoosications in objects are referring to CID instead of CLID/CSID combo. This is a temp. Change

            if(iBMC is ICalculationModule)
                ((ICalculationModule)iBMC).CSID = (Guid)data.Tables[DSCalculationModule.INSTANCE_TABLE].Rows[0]["CSID"];

            BrokerManagedComponentDS basePrimaryDataSet = new BrokerManagedComponentDS();
            base.Hydrate(iBMC, cID, basePrimaryDataSet, ref primaryDS);

            if (ConfigurationManager.AppSettings["LoggingBlob"] == "*"
                || ConfigurationManager.AppSettings["LoggingBlob"] == "ON"
                || ConfigurationManager.AppSettings["LoggingBlob"] == iBMC.GetType().FullName)
            {
                BrokerManagedComponent.SaveBMCAsXML(@"c:\temp\" + "Load_" + iBMC.GetType().FullName + Environment.TickCount.ToString() + ".xml", iBMC);
            }



            return iBMC;
        }

        public override void Save(IBrokerManagedComponent iBMC, ref DataSet persistedDS)
        {
            base.Save(iBMC, ref persistedDS);

            MemoryStream memoryStream = new MemoryStream(1000);
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(memoryStream, iBMC);
            this.CMBlobSize = memoryStream.Length;

            if (ConfigurationManager.AppSettings["LoggingBlob"] == "*"
                || ConfigurationManager.AppSettings["LoggingBlob"] == "ON"
                || ConfigurationManager.AppSettings["LoggingBlob"] == iBMC.GetType().FullName)
            {
                BrokerManagedComponent.SaveBMCAsXML(@"c:\temp\" + "Save_" + iBMC.GetType().FullName + Environment.TickCount.ToString() + ".xml", iBMC);
            }

            dsCommand.SelectCommand.CommandText = "SELECT BLOBPERSIST.INSTANCEID,BLOBPERSIST.NAME,BLOBPERSIST.TYPE, CAST( NULL AS image) AS PERSISTENT_DATA"
                + " FROM BLOBPERSIST"
                + " WHERE BLOBPERSIST.INSTANCEID='{" + iBMC.CID.ToString() + "}'";

            DSCalculationModule data = new DSCalculationModule();
            dsCommand.Fill(data);

            if (data.Tables[DSCalculationModule.INSTANCE_TABLE].Rows.Count == 0)
                data = new DSCalculationModule(iBMC.CID, iBMC.Name, iBMC.TypeID, memoryStream.ToArray());

            data.Tables[DSCalculationModule.INSTANCE_TABLE].Rows[0][DSCalculationModule.PERSISTANT_DATA_FIELD] = memoryStream.ToArray();
            data.Tables[DSCalculationModule.INSTANCE_TABLE].Rows[0][DSCalculationModule.NAME_FIELD] = iBMC.Name;
            data.Tables[DSCalculationModule.INSTANCE_TABLE].Rows[0][DSCalculationModule.TYPE_FIELD] = iBMC.TypeID;

            dsCommand.Update(data);

            if (VelocityConfiguration.IsBlobPersisterCacheEnabled)
                this.SaveToVelocity(BLOB_ID_PREFIX + iBMC.CID.ToString(), data);
        }

        public override void Delete(IBrokerManagedComponent iBMC)
        {
            base.Delete(iBMC, new BrokerManagedComponentDS());

            dsCommand.SelectCommand.CommandText = "SELECT BLOBPERSIST.INSTANCEID"
                + " FROM BLOBPERSIST"
                + " WHERE BLOBPERSIST.INSTANCEID='{" + iBMC.CID.ToString() + "}'";

            DSCalculationModule data = new DSCalculationModule();
            dsCommand.Fill(data);

            foreach (DataRow row in data.Tables[DSCalculationModule.INSTANCE_TABLE].Rows)
            {
                row.Delete();
            }

            dsCommand.Update(data);

            if (VelocityConfiguration.IsBlobPersisterCacheEnabled)
                BlobPersister._DataCache.RemoveObject(BLOB_ID_PREFIX + iBMC.CID.ToString());
        }

        public override void EstablishDatabase()
        {
            base.EstablishDatabase();

            SqlCommand adminCommand = DBCommandFactory.Instance.NewSqlCommand();
            adminCommand.Connection = this.Connection;
            adminCommand.Transaction = this.Transaction;
            adminCommand.CommandText = "if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[BLOBPERSIST]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)"
                + "CREATE TABLE [dbo].[BLOBPERSIST] ("
                + "[INSTANCEID] [uniqueidentifier] NOT NULL ,"
                + "[NAME] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,"
                + "[TYPE] [uniqueidentifier] NULL ,"
                + "[PERSISTENT_DATA] [image] NULL "
                + ") ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]";
            adminCommand.ExecuteNonQuery();
        }

        public override void ClearDatabase()
        {
            base.ClearDatabase();

            SqlCommand adminCommand = DBCommandFactory.Instance.NewSqlCommand();
            adminCommand.Connection = this.Connection;
            adminCommand.Transaction = this.Transaction;

            adminCommand.CommandText = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[BLOBPERSIST]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)"
                + "DELETE BLOBPERSIST";
            adminCommand.ExecuteNonQuery();
        }
    }
}
