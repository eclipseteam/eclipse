using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.DSCalculation;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.CommonPersistence
{
	/// <summary>
	/// Summary description for CalculationPersist.
	/// </summary>
	public class CalculationPersist : ICalculationModulePersist, IDisposable
	{
		public void CommitTransaction(){}
		public void RollbackTransaction(){}
		public void StartTransaction(){}
		private bool disposed = false;

		public long CMBlobSize
		{
			set
			{
				dsCommand.InsertCommand.Parameters["@PERSISTENT_DATA"].Size=(int)value;
				dsCommand.UpdateCommand.Parameters["@PERSISTENT_DATA"].Size=(int)value;
			}
		}

		private SqlDataAdapter dsCommand;
		protected SqlConnection sqlConnection;
		protected SqlTransaction sqlTransaction;

		public CalculationPersist(SqlConnection connection)
			: this(0,connection)
		{
		}

		public CalculationPersist(long blobSize, SqlConnection connection)
		{

			sqlConnection = connection;
			sqlTransaction=null;

			// Create the adapter
			//
			dsCommand = new SqlDataAdapter();
			//
			// Create the DataSetCommand SelectCommand
			//
			dsCommand.SelectCommand = DBCommandFactory.Instance.NewSqlCommand();
			dsCommand.SelectCommand.CommandType = CommandType.Text;
			dsCommand.SelectCommand.Connection=sqlConnection;

			dsCommand.InsertCommand = DBCommandFactory.Instance.NewSqlCommand();
			dsCommand.InsertCommand.CommandText = "INSERT INTO BLOBPERSIST (INSTANCEID,NAME,TYPE,PERSISTENT_DATA) VALUES(@INSTANCEID,@NAME,@TYPE,@PERSISTENT_DATA)";
			dsCommand.InsertCommand.CommandType = CommandType.Text;
			dsCommand.InsertCommand.Parameters.Add(new SqlParameter("@INSTANCEID", SqlDbType.UniqueIdentifier,16,DSCalculationModule.INSTANCEID_FIELD));
			dsCommand.InsertCommand.Parameters.Add(new SqlParameter("@NAME", SqlDbType.VarChar,128,DSCalculationModule.NAME_FIELD));
			dsCommand.InsertCommand.Parameters.Add(new SqlParameter("@TYPE", SqlDbType.UniqueIdentifier,16,DSCalculationModule.TYPE_FIELD));
			dsCommand.InsertCommand.Parameters.Add(new SqlParameter("@PERSISTENT_DATA", SqlDbType.Image,(int)blobSize,DSCalculationModule.PERSISTANT_DATA_FIELD));
			dsCommand.InsertCommand.Connection=dsCommand.SelectCommand.Connection;

			dsCommand.UpdateCommand = DBCommandFactory.Instance.NewSqlCommand();
			dsCommand.UpdateCommand.CommandText = "UPDATE BLOBPERSIST SET PERSISTENT_DATA=@PERSISTENT_DATA, TYPE=@TYPE, NAME=@NAME WHERE INSTANCEID=@INSTANCEID";
			dsCommand.UpdateCommand.CommandType = CommandType.Text;
			dsCommand.UpdateCommand.Parameters.Add(new SqlParameter("@PERSISTENT_DATA", SqlDbType.Image,(int)blobSize,DSCalculationModule.PERSISTANT_DATA_FIELD));
			dsCommand.UpdateCommand.Parameters.Add(new SqlParameter("@NAME", SqlDbType.VarChar,128,DSCalculationModule.NAME_FIELD));
			dsCommand.UpdateCommand.Parameters.Add(new SqlParameter("@TYPE", SqlDbType.UniqueIdentifier,16,DSCalculationModule.TYPE_FIELD));
			dsCommand.UpdateCommand.Parameters.Add(new SqlParameter("@INSTANCEID", SqlDbType.UniqueIdentifier,16,DSCalculationModule.INSTANCEID_FIELD));
			dsCommand.UpdateCommand.Connection=dsCommand.SelectCommand.Connection;

			dsCommand.DeleteCommand = DBCommandFactory.Instance.NewSqlCommand();
			dsCommand.DeleteCommand.CommandText = "DELETE BLOBPERSIST WHERE BLOBPERSIST.INSTANCEID=@INSTANCEID";
			dsCommand.DeleteCommand.CommandType = CommandType.Text;
			dsCommand.DeleteCommand.Parameters.Add(new SqlParameter("@INSTANCEID", SqlDbType.UniqueIdentifier,16,DSCalculationModule.INSTANCEID_FIELD));
			dsCommand.DeleteCommand.Connection=dsCommand.SelectCommand.Connection;

			dsCommand.TableMappings.Add("Table", DSCalculationModule.INSTANCE_TABLE);
		}

		#region IDisposable Implementation ---------------------------------------

		/// <summary>
		///		Performs application-defined tasks associated with freeing, releasing, 
		///		or resetting unmanaged resources.
		/// </summary>
		public void Dispose() 
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		/// <summary>
		///		Performs application-defined tasks associated with freeing, releasing, 
		///		or resetting unmanaged resources.
		/// </summary>
		/// <param name="disposing">
		///		True if called by the Dispose() method, false if called by the finalizer.
		/// </param>
		protected void Dispose(bool disposing) 
		{
			if (!disposed)
			{
				if (disposing)
				{
					dsCommand.Dispose();
				}
				disposed = true;
			}
		}

		#endregion // IDisposable Implementation

		public DataSet Update(DataSet data)
		{
			dsCommand.Update(data);
			return data;
		}

		public DataSet FindByInstanceId(Guid instanceId)
		{
			dsCommand.SelectCommand.CommandText = "SELECT BLOBPERSIST.INSTANCEID,BLOBPERSIST.NAME,BLOBPERSIST.TYPE,BLOBPERSIST.PERSISTENT_DATA"
				+" FROM BLOBPERSIST"
				+" WHERE BLOBPERSIST.INSTANCEID='{"+instanceId.ToString()+"}'";

			DSCalculationModule   data    = new DSCalculationModule();
			dsCommand.Fill(data);
			return data;
		}

		public DataSet FindAll()
		{
			dsCommand.SelectCommand.CommandText = "SELECT BLOBPERSIST.INSTANCEID,BLOBPERSIST.NAME,BLOBPERSIST.TYPE,BLOBPERSIST.PERSISTENT_DATA"
				+" FROM BLOBPERSIST";

			DSCalculationModule   data    = new DSCalculationModule();
			dsCommand.Fill(data);
			return data;
		}


		public virtual SqlConnection Connection
		{
			set
			{
				sqlConnection = value;

				dsCommand.InsertCommand.Connection=value;
				dsCommand.SelectCommand.Connection=value;
				dsCommand.UpdateCommand.Connection=value;
				dsCommand.DeleteCommand.Connection=value;
			}
		}

		public virtual SqlTransaction Transaction
		{
			set
			{
				sqlTransaction=value;
				dsCommand.InsertCommand.Transaction=value;
				dsCommand.SelectCommand.Transaction=value;
				dsCommand.UpdateCommand.Transaction=value;
				dsCommand.DeleteCommand.Transaction=value;
			}
		}

		public virtual void EstablishDatabase(){}
		public virtual void ClearDatabase(){}
		public virtual void RemoveDatabase(){}
	}
}
