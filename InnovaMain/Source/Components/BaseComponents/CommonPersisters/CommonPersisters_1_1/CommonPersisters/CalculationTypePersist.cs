using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.DSCalculation;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.CommonPersistence
{
	/// <summary>
	/// Summary description for CalculationTypePersist.
	/// </summary>
	public class CalculationTypePersist : IDisposable
	{
		#region Fields -----------------------------------------------------------------------------
		private SqlDataAdapter dsCommand;
		protected SqlConnection sqlConnection;
		protected SqlTransaction sqlTransaction;
		private bool disposed = false;
		#endregion

		#region Constructors -----------------------------------------------------------------------
		public CalculationTypePersist(SqlConnection connection, SqlTransaction transaction)
		{
			//
			// Create the adapter
			//
			this.dsCommand = new SqlDataAdapter();

			// Create the DataSetCommand SelectCommand
			this.dsCommand.SelectCommand = DBCommandFactory.Instance.NewSqlCommand();
			this.dsCommand.SelectCommand.CommandType = CommandType.Text;
			this.dsCommand.SelectCommand.Connection = sqlConnection;

			// Create the DataSetCommand InsertCommand
			this.dsCommand.InsertCommand = DBCommandFactory.Instance.NewSqlCommand();
			this.dsCommand.InsertCommand.CommandType = CommandType.Text;
			this.dsCommand.InsertCommand.CommandText = "INSERT INTO CMTYPES (CMTYPEID,TYPENAME,ASSEMBLY,TYPE,DISPLAYNAME,CATEGORYNAME,APPLICABILITY,MAJORVERSION,MINORVERSION,RELEASE, PASSEMBLY, PCLASS) VALUES(@CMTYPEID,@TYPENAME,@ASSEMBLY,@TYPE,@DISPLAYNAME,@CATEGORYNAME,@APPLICABILITY,@MAJORVERSION,@MINORVERSION,@RELEASE, @PASSEMBLY, @PCLASS)";
			this.dsCommand.InsertCommand.Parameters.Add(new SqlParameter("@CMTYPEID",		SqlDbType.UniqueIdentifier,	16,		DSCalculationModuleType.CMTYPEID_FIELD));
			this.dsCommand.InsertCommand.Parameters.Add(new SqlParameter("@TYPENAME",		SqlDbType.VarChar,			128,	DSCalculationModuleType.CMTYPENAME_FIELD));
			this.dsCommand.InsertCommand.Parameters.Add(new SqlParameter("@DISPLAYNAME",		SqlDbType.VarChar,			128,	DSCalculationModuleType.CMTYPEDISPLAYNAME_FIELD));
			this.dsCommand.InsertCommand.Parameters.Add(new SqlParameter("@CATEGORYNAME",	SqlDbType.VarChar,			128,	DSCalculationModuleType.CMCATEGORYNAME_FIELD));
			this.dsCommand.InsertCommand.Parameters.Add(new SqlParameter("@APPLICABILITY",	SqlDbType.Int,				4,		DSCalculationModuleType.CMAPPLICABILITY_FIELD));
			this.dsCommand.InsertCommand.Parameters.Add(new SqlParameter("@ASSEMBLY",		SqlDbType.VarChar,			128,	DSCalculationModuleType.CMASSEMBLY_FIELD));
			this.dsCommand.InsertCommand.Parameters.Add(new SqlParameter("@TYPE",			SqlDbType.VarChar,			128,	DSCalculationModuleType.CMTYPE_FIELD));
			this.dsCommand.InsertCommand.Parameters.Add(new SqlParameter("@MAJORVERSION",	SqlDbType.Int,				4,		DSCalculationModuleType.CMMAJORVERSION_FIELD));
			this.dsCommand.InsertCommand.Parameters.Add(new SqlParameter("@MINORVERSION",	SqlDbType.Int,				4,		DSCalculationModuleType.CMMINORVERSION_FIELD));
			this.dsCommand.InsertCommand.Parameters.Add(new SqlParameter("@RELEASE",			SqlDbType.Int,				4,		DSCalculationModuleType.CMRELEASE_FIELD));
			
			// SJD 5/2/2002 Added pAssembly and pClass 
			this.dsCommand.InsertCommand.Parameters.Add(new SqlParameter("@PASSEMBLY",		SqlDbType.VarChar,			128,	DSCalculationModuleType.CMPASSEMBLY_FIELD));
			this.dsCommand.InsertCommand.Parameters.Add(new SqlParameter("@PCLASS",			SqlDbType.VarChar,			128,	DSCalculationModuleType.CMPCLASS_FIELD));
			this.dsCommand.InsertCommand.Connection = this.dsCommand.SelectCommand.Connection;

			// Create the DataSetCommand UpdateCommand
			this.dsCommand.UpdateCommand = DBCommandFactory.Instance.NewSqlCommand();
			this.dsCommand.UpdateCommand.CommandType = CommandType.Text;
			this.dsCommand.UpdateCommand.CommandText = "UPDATE CMTYPES SET CMTYPEID=@CMTYPEID,TYPENAME=@TYPENAME,ASSEMBLY=@ASSEMBLY,TYPE=@TYPE,DISPLAYNAME=@DISPLAYNAME,CATEGORYNAME=@CATEGORYNAME,APPLICABILITY=@APPLICABILITY,MAJORVERSION=@MAJORVERSION,MINORVERSION=@MINORVERSION,RELEASE=@RELEASE, PASSEMBLY=@PASSEMBLY, PCLASS=@PCLASS where CMTYPEID=@CMTYPEID";
			this.dsCommand.UpdateCommand.Parameters.Add(new SqlParameter("@CMTYPEID",		SqlDbType.UniqueIdentifier,	16,		DSCalculationModuleType.CMTYPEID_FIELD));
			this.dsCommand.UpdateCommand.Parameters.Add(new SqlParameter("@TYPENAME",		SqlDbType.VarChar,			128,	DSCalculationModuleType.CMTYPENAME_FIELD));
			this.dsCommand.UpdateCommand.Parameters.Add(new SqlParameter("@DISPLAYNAME",		SqlDbType.VarChar,			128,	DSCalculationModuleType.CMTYPEDISPLAYNAME_FIELD));
			this.dsCommand.UpdateCommand.Parameters.Add(new SqlParameter("@CATEGORYNAME",	SqlDbType.VarChar,			128,	DSCalculationModuleType.CMCATEGORYNAME_FIELD));
			this.dsCommand.UpdateCommand.Parameters.Add(new SqlParameter("@APPLICABILITY",	SqlDbType.Int,				4,		DSCalculationModuleType.CMAPPLICABILITY_FIELD));
			this.dsCommand.UpdateCommand.Parameters.Add(new SqlParameter("@ASSEMBLY",		SqlDbType.VarChar,			128,	DSCalculationModuleType.CMASSEMBLY_FIELD));
			this.dsCommand.UpdateCommand.Parameters.Add(new SqlParameter("@TYPE",			SqlDbType.VarChar,			128,	DSCalculationModuleType.CMTYPE_FIELD));
			this.dsCommand.UpdateCommand.Parameters.Add(new SqlParameter("@MAJORVERSION",	SqlDbType.Int,				4,		DSCalculationModuleType.CMMAJORVERSION_FIELD));
			this.dsCommand.UpdateCommand.Parameters.Add(new SqlParameter("@MINORVERSION",	SqlDbType.Int,				4,		DSCalculationModuleType.CMMINORVERSION_FIELD));
			this.dsCommand.UpdateCommand.Parameters.Add(new SqlParameter("@RELEASE",			SqlDbType.Int,				4,		DSCalculationModuleType.CMRELEASE_FIELD));
			
			// SJD 5/2/2002 Added pAssembly and pClass 
			this.dsCommand.UpdateCommand.Parameters.Add(new SqlParameter("@PASSEMBLY",		SqlDbType.VarChar,			128,	DSCalculationModuleType.CMPASSEMBLY_FIELD));
			this.dsCommand.UpdateCommand.Parameters.Add(new SqlParameter("@PCLASS",			SqlDbType.VarChar,			128,	DSCalculationModuleType.CMPCLASS_FIELD));
			this.dsCommand.UpdateCommand.Connection = this.dsCommand.SelectCommand.Connection;

			this.dsCommand.TableMappings.Add("Table", DSCalculationModuleType.TYPE_TABLE);

			this.Connection = connection;
			this.Transaction = transaction;
		}
		#endregion

		#region Public Methods ---------------------------------------------------------------------
		/// <summary>
		///		Performs application-defined tasks associated with freeing, releasing, 
		///		or resetting unmanaged resources.
		/// </summary>
		public void Dispose() 
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		/// <summary>
		///		Performs application-defined tasks associated with freeing, releasing, 
		///		or resetting unmanaged resources.
		/// </summary>
		/// <param name="disposing">
		///		True if called by the Dispose() method, false if called by the finalizer.
		/// </param>
		protected void Dispose(bool disposing) 
		{
			if (!disposed)
			{
				if (disposing)
				{
					this.dsCommand.Dispose();
				}
				disposed = true;
			}
		}

		public DataSet FindByTypeId(Guid instanceId)
		{
			this.dsCommand.SelectCommand.Parameters.Clear();
            this.dsCommand.SelectCommand.Parameters.AddWithValue("@INSTANCEID", typeof(Guid)).Value = instanceId;
			this.dsCommand.SelectCommand.CommandText = "SELECT COMPONENT.ID AS COMPONENTID, ASSEMBLY.ID AS CMTYPEID,ASSEMBLY.STRONGNAME AS ASSEMBLY,COMPONENTVERSION.IMPLEMENTATIONCLASS AS TYPE,ASSEMBLY.NAME AS TYPENAME,COMPONENT.CATEGORY AS CATEGORYNAME,COMPONENT.APPLICABILITY,ASSEMBLY.DISPLAYNAME,ASSEMBLY.MAJORVERSION,ASSEMBLY.MINORVERSION,COMPONENTVERSION.PERSISTERASSEMBLYSTRONGNAME AS PASSEMBLY,COMPONENTVERSION.PERSISTERCLASS AS PCLASS FROM ASSEMBLY,COMPONENTVERSION,COMPONENT WHERE ASSEMBLY.ID=COMPONENTVERSION.ID AND COMPONENTVERSION.COMPONENTID=COMPONENT.ID AND ASSEMBLY.ID=@INSTANCEID";

			DSCalculationModuleType data = new DSCalculationModuleType();
			this.dsCommand.Fill(data);
			return data;
		}

		public DataSet FindByTypeName(string typeName)
		{
			this.dsCommand.SelectCommand.Parameters.Clear();
            this.dsCommand.SelectCommand.Parameters.AddWithValue("@TYPENAME", typeof(string)).Value = typeName;
			this.dsCommand.SelectCommand.CommandText = "SELECT COMPONENT.ID AS COMPONENTID, ASSEMBLY.ID AS CMTYPEID,ASSEMBLY.STRONGNAME AS ASSEMBLY,COMPONENTVERSION.IMPLEMENTATIONCLASS AS TYPE,ASSEMBLY.NAME AS TYPENAME,COMPONENT.CATEGORY AS CATEGORYNAME,COMPONENT.APPLICABILITY,ASSEMBLY.DISPLAYNAME,ASSEMBLY.MAJORVERSION,ASSEMBLY.MINORVERSION,COMPONENTVERSION.PERSISTERASSEMBLYSTRONGNAME AS PASSEMBLY,COMPONENTVERSION.PERSISTERCLASS AS PCLASS FROM ASSEMBLY,COMPONENTVERSION,COMPONENT WHERE ASSEMBLY.ID=COMPONENTVERSION.ID AND COMPONENTVERSION.COMPONENTID=COMPONENT.ID AND ASSEMBLY.NAME=@TYPENAME";

			DSCalculationModuleType data = new DSCalculationModuleType();
			this.dsCommand.Fill(data);
			return data;
		}

		public DataSet FindAll()
		{
			this.dsCommand.SelectCommand.CommandText = "SELECT COMPONENT.ID AS COMPONENTID,COMPONENTVERSION.ID AS CMTYPEID,ASSEMBLY.STRONGNAME AS ASSEMBLY,COMPONENTVERSION.IMPLEMENTATIONCLASS AS TYPE,ASSEMBLY.NAME AS TYPENAME,COMPONENT.CATEGORY AS CATEGORYNAME,COMPONENT.APPLICABILITY,COMPONENT.DISPLAYNAME,ASSEMBLY.MAJORVERSION,ASSEMBLY.MINORVERSION,COMPONENTVERSION.PERSISTERASSEMBLYSTRONGNAME AS PASSEMBLY,COMPONENTVERSION.PERSISTERCLASS AS PCLASS FROM ASSEMBLY,COMPONENTVERSION,COMPONENT WHERE ASSEMBLY.ID=COMPONENTVERSION.ID AND COMPONENTVERSION.COMPONENTID=COMPONENT.ID";

			DSCalculationModuleType data = new DSCalculationModuleType();
			this.dsCommand.Fill(data);
			return data;
		}

		public DataSet FindByCategory(string categoryName)
		{
			this.dsCommand.SelectCommand.Parameters.Clear();
            this.dsCommand.SelectCommand.Parameters.AddWithValue("@CATEGORYNAME", typeof(string)).Value = categoryName;
			this.dsCommand.SelectCommand.CommandText = "SELECT COMPONENT.ID AS COMPONENTID, COMPONENTVERSION.ID AS CMTYPEID,ASSEMBLY.STRONGNAME AS ASSEMBLY,COMPONENTVERSION.IMPLEMENTATIONCLASS AS TYPE,ASSEMBLY.NAME AS TYPENAME,COMPONENT.CATEGORY AS CATEGORYNAME,COMPONENT.APPLICABILITY,COMPONENT.DISPLAYNAME,ASSEMBLY.MAJORVERSION,ASSEMBLY.MINORVERSION,COMPONENTVERSION.PERSISTERASSEMBLYSTRONGNAME AS PASSEMBLY,COMPONENTVERSION.PERSISTERCLASS AS PCLASS FROM ASSEMBLY,COMPONENTVERSION,COMPONENT WHERE ASSEMBLY.ID=COMPONENTVERSION.ID AND COMPONENTVERSION.COMPONENTID=COMPONENT.ID AND COMPONENT.CATEGORY=@CATEGORYNAME";

			DSCalculationModuleType data = new DSCalculationModuleType();
			this.dsCommand.Fill(data);
			return data;
		}

		public DataSet FindByCategory(string[] categoryNames)
		{
			this.dsCommand.SelectCommand.Parameters.Clear();
			StringBuilder selectCommand = new StringBuilder();
			
			selectCommand.Append("SELECT COMPONENT.ID AS COMPONENTID, COMPONENTVERSION.ID AS CMTYPEID,ASSEMBLY.STRONGNAME AS ASSEMBLY,COMPONENTVERSION.IMPLEMENTATIONCLASS AS TYPE,ASSEMBLY.NAME AS TYPENAME,COMPONENT.CATEGORY AS CATEGORYNAME,COMPONENT.APPLICABILITY,COMPONENT.DISPLAYNAME,ASSEMBLY.MAJORVERSION,ASSEMBLY.MINORVERSION,COMPONENTVERSION.PERSISTERASSEMBLYSTRONGNAME AS PASSEMBLY,COMPONENTVERSION.PERSISTERCLASS AS PCLASS FROM ASSEMBLY,COMPONENTVERSION,COMPONENT WHERE ASSEMBLY.ID=COMPONENTVERSION.ID AND COMPONENTVERSION.COMPONENTID=COMPONENT.ID ");

			if(categoryNames.Length > 0)
			{
                this.dsCommand.SelectCommand.Parameters.AddWithValue("@CATEGORYNAME0", typeof(string)).Value = categoryNames[0];
				selectCommand.Append(" AND (COMPONENT.CATEGORY=@CATEGORYNAME0");

				for(int category = 1; category < categoryNames.Length; category++)
				{
					string parameterName = "@CATEGORYNAME" + category.ToString();

                    this.dsCommand.SelectCommand.Parameters.AddWithValue(parameterName, typeof(string)).Value = categoryNames[category];
					selectCommand.Append(" AND (COMPONENT.CATEGORY=").Append(parameterName);
				}

				selectCommand.Append(" )");
			}

			this.dsCommand.SelectCommand.CommandText = selectCommand.ToString();

			DSCalculationModuleType data = new DSCalculationModuleType();
			this.dsCommand.Fill(data);
			return data;
		}

		public DataSet Update(DataSet data)
		{
			this.dsCommand.Update(data);
			return data;
		}

		public void DeleteAll()
		{
            SqlCommand sqlCommand = DBCommandFactory.Instance.NewSqlCommand();
            sqlCommand.Connection = this.sqlConnection;
			sqlCommand.CommandText = "DELETE CMTYPES";
			sqlCommand.Transaction = sqlTransaction;
			sqlCommand.ExecuteNonQuery();
		}

		public virtual SqlConnection Connection
		{
			set
			{
				sqlConnection = value;

				this.dsCommand.InsertCommand.Connection = value;
				this.dsCommand.SelectCommand.Connection = value;
				this.dsCommand.UpdateCommand.Connection = value;
			}
		}

		public virtual SqlTransaction Transaction
		{
			set
			{
				sqlTransaction = value;
				this.dsCommand.InsertCommand.Transaction = value;
				this.dsCommand.SelectCommand.Transaction = value;
				this.dsCommand.UpdateCommand.Transaction = value;
			}
		}

		public virtual void EstablishDatabase()
		{
            SqlCommand adminCommand = DBCommandFactory.Instance.NewSqlCommand();
            adminCommand.Connection = this.sqlConnection;
			adminCommand.Transaction = sqlTransaction;
			adminCommand.CommandText = "if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CMTYPES]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)"
			+"CREATE TABLE [dbo].[CMTYPES] ("
			+"[CMTYPEID] [uniqueidentifier] NOT NULL ,"
			+"[ASSEMBLY] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,"
			+"[TYPE] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,"
			+"[TYPENAME] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,"
			+"[DISPLAYNAME] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,"
			+"[CATEGORYNAME] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,"
			+"[APPLICABILITY] [int] NULL ,"
			+"[MAJORVERSION] [int] NULL ,"
			+"[MINORVERSION] [int] NULL ,"
			+"[RELEASE] [int] NULL ,"
			+"[PASSEMBLY] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,"
			+"[PCLASS] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL "
			+")ON [PRIMARY]";
			adminCommand.ExecuteNonQuery();

			// This code creates the tables for CMBroker  THIS CODE IS TEMPORARY ***********************
            SqlCommand establishCommand = DBCommandFactory.Instance.NewSqlCommand();
            establishCommand.Connection = this.sqlConnection;
			//========================================================================================================================
			// Create the ASSEMBLY table
			establishCommand.CommandText = 
				"if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ASSEMBLY]') and OBJECTPROPERTY(id, N'IsUserTable') = 1) "
				+"drop table [dbo].[ASSEMBLY]; "

				+"CREATE TABLE [dbo].[ASSEMBLY] ("
				+"[ID] [uniqueidentifier] NOT NULL ,"
				+"[NAME] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL ,"
				+"[DISPLAYNAME] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL ,"
				+"[STRONGNAME] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL ,"
				+"[MAJORVERSION] [int] NOT NULL ,"
				+"[MINORVERSION] [int] NOT NULL ,"
				+"[DATAFORMAT] [int] NOT NULL ,"
				+"[REVISION] [int] NOT NULL "
				+") ON [PRIMARY];";

			establishCommand.ExecuteNonQuery();

			//========================================================================================================================
			// Create the COMPONENT table
			establishCommand.CommandText = 
				"if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_COMPONENTVERSION_COMPONENT]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)"
				+"ALTER TABLE [dbo].[COMPONENTVERSION] DROP CONSTRAINT FK_COMPONENTVERSION_COMPONENT;"

				+"if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[COMPONENT]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)"
				+"drop table [dbo].[COMPONENT];"

				+"CREATE TABLE [dbo].[COMPONENT] ("
				+"[ID] [uniqueidentifier] NOT NULL ,"
				+"[NAME] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL ,"
				+"[DISPLAYNAME] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL ,"
				+"[CATEGORY] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL ,"
				+"[APPLICABILITY] [int] NULL "
				+") ON [PRIMARY];";

			establishCommand.ExecuteNonQuery();

			//========================================================================================================================
			// Create the COMPONENTVERSION table
			establishCommand.CommandText = 
				"if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PK_ASSEMBLY_COMPONENTVERSION]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)"
				+"ALTER TABLE [dbo].[ASSEMBLY] DROP CONSTRAINT PK_ASSEMBLY_COMPONENTVERSION;"

				+"if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[COMPONENTVERSION]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)"
				+"drop table [dbo].[COMPONENTVERSION];"

				+"CREATE TABLE [dbo].[COMPONENTVERSION] ("
				+"[ID] [uniqueidentifier] NOT NULL ,"
				+"[VERSIONNAME] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL ,"
				+"[COMPONENTID] [uniqueidentifier] NOT NULL ,"
				+"[STARTDATE] [datetime] NOT NULL ,"
				+"[ENDDATE] [datetime] NOT NULL ,"
				+"[PERSISTERASSEMBLYSTRONGNAME] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL ,"
				+"[PERSISTERCLASS] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL ,"
				+"[IMPLEMENTATIONCLASS] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL ,"
				+") ON [PRIMARY];";

			establishCommand.ExecuteNonQuery();

			// END OF TEMPORARY CODE **************************************************************
		}
		public virtual void ClearDatabase()
		{
            SqlCommand adminCommand = DBCommandFactory.Instance.NewSqlCommand();
            adminCommand.Connection = this.sqlConnection;
			adminCommand.Transaction = sqlTransaction;

			adminCommand.CommandText = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CMTYPES]') and OBJECTPROPERTY(id, N'IsUserTable') = 1) DELETE CMTYPES";
			adminCommand.ExecuteNonQuery();

		}
		public virtual void RemoveDatabase(){}
		#endregion
	}
}
