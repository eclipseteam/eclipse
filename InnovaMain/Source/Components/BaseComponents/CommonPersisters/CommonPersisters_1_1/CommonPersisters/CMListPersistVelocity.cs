using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;

using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.DSCalculation;
using Oritax.TaxSimp.Utilities;

using Oritax.TaxSimp.VelocityCache;

namespace Oritax.TaxSimp.CommonPersistence
{
    /// <summary>
    /// Summary description for CalculationTypePersist.
    /// </summary>
    public class CMListPersistVelocity : CMListPersist
    {
        #region Fields -----------------------------------------------------------------------------
        private SqlDataAdapter dsCommand;
        private bool disposed = false;

        private static VelocityCacheString _VelocityDataCache = null;

        protected static VelocityCacheString _DataCache
        {
            get
            {
                if (_VelocityDataCache == null)
                    _VelocityDataCache = new VelocityCacheString();

                return _VelocityDataCache;
            }
        }
        #endregion

        #region Constructors -----------------------------------------------------------------------
        public CMListPersistVelocity(SqlConnection connection)
        {

            sqlConnection = connection;
            sqlTransaction = null;

            //
            // Create the adapter
            //
            this.dsCommand = new SqlDataAdapter();
            //
            // Create the DataSetCommand SelectCommand
            //
            this.dsCommand.SelectCommand = DBCommandFactory.Instance.NewSqlCommand();
            this.dsCommand.SelectCommand.CommandType = CommandType.Text;
            this.dsCommand.SelectCommand.Connection = sqlConnection;

            this.dsCommand.UpdateCommand = DBCommandFactory.Instance.NewSqlCommand();
            this.dsCommand.UpdateCommand.CommandType = CommandType.Text;
            this.dsCommand.UpdateCommand.Connection = this.dsCommand.SelectCommand.Connection;

            this.dsCommand.InsertCommand = DBCommandFactory.Instance.NewSqlCommand();
            this.dsCommand.InsertCommand.CommandType = CommandType.Text;
            this.dsCommand.InsertCommand.Connection = this.dsCommand.SelectCommand.Connection;

            this.dsCommand.DeleteCommand = DBCommandFactory.Instance.NewSqlCommand();
            this.dsCommand.DeleteCommand.CommandType = CommandType.Text;
            this.dsCommand.DeleteCommand.Connection = this.dsCommand.SelectCommand.Connection;

            this.dsCommand.TableMappings.Add("Table", DSCalculationModulesInfo.CMLIST_TABLE);
        }
        #endregion

        #region Properties -------------------------------------------------------------------------
        public override SqlConnection Connection
        {
            set
            {
                sqlConnection = value;
                this.dsCommand.SelectCommand.Connection = value;
                this.dsCommand.UpdateCommand.Connection = value;
                this.dsCommand.InsertCommand.Connection = value;
                this.dsCommand.DeleteCommand.Connection = value;
            }
        }

        public override SqlTransaction Transaction
        {
            set
            {
                sqlTransaction = value;
                this.dsCommand.SelectCommand.Transaction = value;
                this.dsCommand.UpdateCommand.Transaction = value;
                this.dsCommand.InsertCommand.Transaction = value;
                this.dsCommand.DeleteCommand.Transaction = value;
            }
        }
        #endregion

        #region Public Methods ---------------------------------------------------------------------
        /// <summary>
        ///		Performs application-defined tasks associated with freeing, releasing, 
        ///		or resetting unmanaged resources.
        /// </summary>
        public override void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        ///		Performs application-defined tasks associated with freeing, releasing, 
        ///		or resetting unmanaged resources.
        /// </summary>
        /// <param name="disposing">
        ///		True if called by the Dispose() method, false if called by the finalizer.
        /// </param>
        protected override void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    this.dsCommand.Dispose();
                }
                disposed = true;
            }
        }

        public override DataSet FindByTypeId(Guid typeId)
        {
            this.dsCommand.SelectCommand.Parameters.Clear();
            this.dsCommand.SelectCommand.Parameters.AddWithValue("@TYPEID", typeof(System.Guid)).Value = typeId;
            StringBuilder builder = new StringBuilder();
            builder.Append("SELECT CINSTANCES.CID,CINSTANCES.NAME,CINSTANCES.CTID,CINSTANCES.UPDATETOKEN,ASSEMBLY.NAME AS TYPENAME,COMPONENT.CATEGORY AS CATEGORYNAME,ASSEMBLY.MAJORVERSION,ASSEMBLY.MINORVERSION,ASSEMBLY.REVISION,COMPONENTVERSION.PERSISTERASSEMBLYSTRONGNAME AS PASSEMBLY,COMPONENTVERSION.PERSISTERCLASS AS PCLASS FROM ");
            builder.Append(BrokerManagedComponentDS.CINSTANCES_TABLE).Append(" CINSTANCES, ASSEMBLY, COMPONENT, COMPONENTVERSION WHERE CINSTANCES.CTID=ASSEMBLY.ID AND ASSEMBLY.ID = COMPONENTVERSION.ID AND COMPONENTVERSION.COMPONENTID = COMPONENT.ID AND CINSTANCES.CTID=@TYPEID");
            this.dsCommand.SelectCommand.CommandText = builder.ToString();

            DSBMCInfo data = new DSBMCInfo();
            this.dsCommand.Fill(data);
            return data;
        }

        public override DataSet FindByTypeName(string typeName)
        {
            List<CInstanceTable> intanceList = (List<CInstanceTable>)VelocityCacheString.VelocityCache.GetObejct(VelocityConfiguration.VelocityKeys["CINSTANCES"]);
            List<AssemblyTable> assemblyList = (List<AssemblyTable>)VelocityCacheString.VelocityCache.GetObejct(VelocityConfiguration.VelocityKeys["ASSEMBLY"]);
            List<ComponentVersionTable> cvList = (List<ComponentVersionTable>)VelocityCacheString.VelocityCache.GetObejct(VelocityConfiguration.VelocityKeys["COMPONENTVERSION"]);
            List<ComponentTable> componentList = (List<ComponentTable>)VelocityCacheString.VelocityCache.GetObejct(VelocityConfiguration.VelocityKeys["COMPONENT"]);

            var q = from i in intanceList
                    join a in assemblyList on i.CTID equals a.ID
                    join cv in cvList on a.ID equals cv.ID
                    join c in componentList on cv.COMPONENTID equals c.ID
                    where c.NAME == typeName
                    select new
                    {
                        i.CID,
                        i.Name,
                        i.CTID,
                        i.UpdateToken,
                        TYPENAME = a.NAME,
                        CATEGORYNAME = c.CATEGORY,
                        a.MAJORVERSION,
                        a.MINORVERSION,
                        PASSEMBLY = cv.PERSISTERASSEMBLYSTRONGNAME,
                        PCLASS = cv.PERSISTERCLASS
                    };

            DSBMCInfo data = new DSBMCInfo();
            DataTable dt = data.Tables[DSCalculationModulesInfo.CMLIST_TABLE];

            foreach (var i in q)
            {
                DataRow dr = dt.NewRow();
                dr["CID"] = i.CID;
                dr["NAME"] = i.Name;
                dr["CTID"] = i.CTID;
                dr["UPDATETOKEN"] = i.UpdateToken;
                dr["TYPENAME"] = i.TYPENAME;
                dr["CATEGORYNAME"] = i.CATEGORYNAME;
                dr["MAJORVERSION"] = i.MAJORVERSION;
                dr["MINORVERSION"] = i.MINORVERSION;
                dr["PASSEMBLY"] = i.PASSEMBLY;
                dr["PCLASS"] = i.PCLASS;

                dt.Rows.Add(dr);
            }

            return data;
        }

        public override DataSet FindByName(string instanceName)
        {
            this.dsCommand.SelectCommand.Parameters.Clear();
            this.dsCommand.SelectCommand.Parameters.AddWithValue("@INSTANCENAME", typeof(string)).Value = instanceName;
            StringBuilder builder = new StringBuilder();
            builder.Append("SELECT CINSTANCES.CID,CINSTANCES.NAME,CINSTANCES.CTID,CINSTANCES.UPDATETOKEN,ASSEMBLY.NAME,COMPONENT.CATEGORY,ASSEMBLY.MAJORVERSION,ASSEMBLY.MINORVERSION,ASSEMBLY.REVISION,COMPONENTVERSION.PERSISTERASSEMBLYSTRONGNAME,COMPONENTVERSION.PERSISTERCLASS FROM ");
            builder.Append(BrokerManagedComponentDS.CINSTANCES_TABLE).Append(" CINSTANCES, ASSEMBLY, COMPONENT, COMPONENTVERSION WHERE CINSTANCES.CTID=ASSEMBLY.ID AND ASSEMBLY.ID=COMPONENTVERSION.ID AND COMPONENTVERSION.COMPONENTID=COMPONENT.ID AND CINSTANCES.NAME=@INSTANCENAME");
            this.dsCommand.SelectCommand.CommandText = builder.ToString();

            DSBMCInfo data = new DSBMCInfo();
            this.dsCommand.Fill(data);
            return data;
        }

        /// <summary>
        /// Locate a BMC Instance (see: CINSTANCES table) based on provided name and BMC Type.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public override DataSet FindByName(string name, string type)
        {
            this.dsCommand.SelectCommand.Parameters.Clear();
            this.dsCommand.SelectCommand.Parameters.AddWithValue("@INSTANCENAME", typeof(string)).Value = name;
            this.dsCommand.SelectCommand.Parameters.AddWithValue("@TYPENAME", typeof(string)).Value = type;

            StringBuilder builder = new StringBuilder();
            builder.Append("SELECT CINSTANCES.CID,CINSTANCES.NAME,CINSTANCES.CTID,CINSTANCES.UPDATETOKEN,COMPONENT.NAME AS TYPENAME,COMPONENT.CATEGORY AS CATEGORYNAME,ASSEMBLY.MAJORVERSION,ASSEMBLY.MINORVERSION,COMPONENTVERSION.PERSISTERASSEMBLYSTRONGNAME AS PASSEMBLY,COMPONENTVERSION.PERSISTERCLASS AS PCLASS FROM ");
            builder.Append(BrokerManagedComponentDS.CINSTANCES_TABLE).Append(" CINSTANCES,COMPONENTVERSION,ASSEMBLY,COMPONENT WHERE CINSTANCES.CTID=ASSEMBLY.ID AND ASSEMBLY.ID=COMPONENTVERSION.ID AND COMPONENTVERSION.COMPONENTID=COMPONENT.ID AND CINSTANCES.NAME=@INSTANCENAME AND ASSEMBLY.NAME=@TYPENAME");
            this.dsCommand.SelectCommand.CommandText = builder.ToString();

            DSBMCInfo data = new DSBMCInfo();
            this.dsCommand.Fill(data);
            return data;
        }

        public override DataSet FindByInstanceID(Guid instanceId)
        {
            this.dsCommand.SelectCommand.Parameters.Clear();
            this.dsCommand.SelectCommand.Parameters.AddWithValue("@INSTANCEID", typeof(System.Guid)).Value = instanceId;
            StringBuilder builder = new StringBuilder();
            builder.Append("SELECT CINSTANCES.CID,CINSTANCES.NAME,CINSTANCES.CTID,CINSTANCES.UPDATETOKEN,ASSEMBLY.NAME AS TYPENAME,COMPONENT.CATEGORY AS CATEGORYNAME,ASSEMBLY.MAJORVERSION,ASSEMBLY.MINORVERSION,COMPONENTVERSION.PERSISTERASSEMBLYSTRONGNAME AS PASSEMBLY,COMPONENTVERSION.PERSISTERCLASS AS PCLASS FROM ");
            builder.Append(BrokerManagedComponentDS.CINSTANCES_TABLE).Append(" CINSTANCES, COMPONENTVERSION,ASSEMBLY,COMPONENT WHERE CINSTANCES.CTID=ASSEMBLY.ID AND ASSEMBLY.ID=COMPONENTVERSION.ID AND COMPONENTVERSION.COMPONENTID=COMPONENT.ID AND CINSTANCES.CID=@INSTANCEID");
            this.dsCommand.SelectCommand.CommandText = builder.ToString();

            DSCalculationModulesInfo data = new DSCalculationModulesInfo();
            this.dsCommand.Fill(data);
            return data;
        }

        public override DataSet FindAll()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("SELECT CINSTANCES.CID,CINSTANCES.NAME,CINSTANCES.CTID,CINSTANCES.UPDATETOKEN,ASSEMBLY.NAME AS TYPENAME,COMPONENT.CATEGORY AS CATEGORYNAME,ASSEMBLY.MAJORVERSION,ASSEMBLY.MINORVERSION,ASSEMBLY.REVISION,COMPONENTVERSION.PERSISTERASSEMBLYSTRONGNAME AS PASSEMBLY,COMPONENTVERSION.PERSISTERCLASS AS PCLASS FROM ");
            builder.Append(BrokerManagedComponentDS.CINSTANCES_TABLE).Append(" CINSTANCES, ASSEMBLY, COMPONENT, COMPONENTVERSION WHERE CINSTANCES.CTID=ASSEMBLY.ID AND ASSEMBLY.ID=COMPONENTVERSION.ID AND COMPONENTVERSION.COMPONENTID=COMPONENT.ID");
            this.dsCommand.SelectCommand.CommandText = builder.ToString();

            DSBMCInfo data = new DSBMCInfo();
            this.dsCommand.Fill(data);
            return data;
        }

        public override DataSet FindByMember(Guid memberCLID)
        {
            this.dsCommand.SelectCommand.Parameters.Clear();
            this.dsCommand.SelectCommand.Parameters.AddWithValue("@MEMBERCLID", typeof(System.Guid)).Value = memberCLID;

            StringBuilder builder = new StringBuilder();
            builder.Append("SELECT CINSTANCES.CID,CINSTANCES.NAME,CINSTANCES.CTID,CINSTANCES.UPDATETOKEN,ASSEMBLY.NAME AS TYPENAME,COMPONENT.CATEGORY AS CATEGORYNAME,ASSEMBLY.MAJORVERSION,ASSEMBLY.MINORVERSION,ASSEMBLY.REVISION,COMPONENTVERSION.PERSISTERASSEMBLYSTRONGNAME AS PASSEMBLY,COMPONENTVERSION.PERSISTERCLASS AS PCLASS FROM ");
            builder.Append(BrokerManagedComponentDS.CINSTANCES_TABLE).Append(" CINSTANCES, ASSEMBLY, COMPONENT, COMPONENTVERSION, CMMEMBERS, CMSCENARIO WHERE CMMEMBERS.MEMBERCLID = @MEMBERCLID AND CMMEMBERS.PARENTCLID=CMSCENARIO.CLID AND CMMEMBERS.PARENTCSID=CMSCENARIO.CSID AND CMSCENARIO.CIID=CINSTANCES.CID AND CINSTANCES.CTID=ASSEMBLY.ID AND ASSEMBLY.ID=COMPONENTVERSION.ID AND COMPONENTVERSION.COMPONENTID=COMPONENT.ID");
            this.dsCommand.SelectCommand.CommandText = builder.ToString();

            DSBMCInfo data = new DSBMCInfo();
            this.dsCommand.Fill(data);
            return data;
        }

        public override DataSet FindByMember(Guid memberCLID, Guid memberCSID)
        {
            this.dsCommand.SelectCommand.Parameters.Clear();
            this.dsCommand.SelectCommand.Parameters.AddWithValue("@MEMBERCLID", typeof(System.Guid)).Value = memberCLID;
            this.dsCommand.SelectCommand.Parameters.AddWithValue("@MEMBERCSID", typeof(System.Guid)).Value = memberCSID;

            StringBuilder builder = new StringBuilder();
            builder.Append("SELECT CINSTANCES.CID,CINSTANCES.NAME,CMMEMBERS.PARENTCSID, CINSTANCES.CTID,CINSTANCES.UPDATETOKEN,ASSEMBLY.NAME AS TYPENAME,COMPONENT.CATEGORY AS CATEGORYNAME,ASSEMBLY.MAJORVERSION,ASSEMBLY.MINORVERSION,ASSEMBLY.REVISION,COMPONENTVERSION.PERSISTERASSEMBLYSTRONGNAME AS PASSEMBLY,COMPONENTVERSION.PERSISTERCLASS AS PCLASS FROM ");
            builder.Append(BrokerManagedComponentDS.CINSTANCES_TABLE).Append(" CINSTANCES, ASSEMBLY, COMPONENT, COMPONENTVERSION, CMMEMBERS, CMSCENARIO WHERE CMMEMBERS.MEMBERCLID = @MEMBERCLID AND CMMEMBERS.MEMBERCSID = @MEMBERCSID AND CMMEMBERS.PARENTCLID=CMSCENARIO.CLID AND CMMEMBERS.PARENTCSID=CMSCENARIO.CSID AND CMSCENARIO.CIID=CINSTANCES.CID AND CINSTANCES.CTID=ASSEMBLY.ID AND ASSEMBLY.ID=COMPONENTVERSION.ID AND COMPONENTVERSION.COMPONENTID=COMPONENT.ID");
            this.dsCommand.SelectCommand.CommandText = builder.ToString();

            DSBMCInfo data = new DSBMCInfo();
            this.dsCommand.Fill(data);
            return data;
        }

        /// <summary>
        /// Return the CID and Name for any entity members of the Group for the specified
        /// CLID and CSID
        /// </summary>
        /// <param name="groupCLID"></param>
        /// <param name="groupCSID"></param>
        /// <returns></returns>
        public override DataSet FindByConsolidationParent(Guid groupCLID, Guid groupCSID)
        {
            this.dsCommand.SelectCommand.Parameters.Clear();
            this.dsCommand.SelectCommand.Parameters.AddWithValue("@GROUPCLID", typeof(System.Guid)).Value = groupCLID;
            this.dsCommand.SelectCommand.Parameters.AddWithValue("@GROUPCSID", typeof(System.Guid)).Value = groupCSID;

            StringBuilder builder = new StringBuilder();
            builder.Append("SELECT CINSTANCES.CID,CINSTANCES.NAME FROM ");
            builder.Append(BrokerManagedComponentDS.CINSTANCES_TABLE).Append(" CINSTANCES, CMMEMBERS, CMSCENARIO,  WHERE CMMEMBERS.PARENTCLID=@GROUPCLID AND CMMEMBERS.PARENTCSID = @GROUPCSID AND CMMEMBERS.MEMBERCLID=CMSCENARIO.CLID and CMMEMBERS.MEMBERCSID=CMSCENARIO.CSID and CINSTANCES.CID=CMSCENARIO.CIID");
            this.dsCommand.SelectCommand.CommandText = builder.ToString();

            DSBMCInfo data = new DSBMCInfo();
            this.dsCommand.Fill(data);
            return data;
        }

        public override DataSet Update(DataSet data)
        {
            this.dsCommand.UpdateCommand.CommandText = "UPDATE " + BrokerManagedComponentDS.CINSTANCES_TABLE + " SET CTID=@CTID, NAME=@NAME, UPDATETOKEN=@UPDATETOKEN WHERE CID=@CID";
            this.dsCommand.UpdateCommand.CommandType = CommandType.Text;
            this.dsCommand.UpdateCommand.Parameters.Add(new SqlParameter("@CID", SqlDbType.UniqueIdentifier, 16, DSCalculationModulesInfo.CMID_FIELD));
            this.dsCommand.UpdateCommand.Parameters.Add(new SqlParameter("@NAME", SqlDbType.VarChar, 128, DSCalculationModulesInfo.CMNAME_FIELD));
            this.dsCommand.UpdateCommand.Parameters.Add(new SqlParameter("@CTID", SqlDbType.UniqueIdentifier, 16, DSCalculationModulesInfo.CMTYPEID_FIELD));
            this.dsCommand.UpdateCommand.Parameters.Add(new SqlParameter("@UPDATETOKEN", SqlDbType.Image, 16, DSCalculationModulesInfo.CMUPDATETOKEN_FIELD));

            this.dsCommand.InsertCommand.CommandText = "INSERT INTO " + BrokerManagedComponentDS.CINSTANCES_TABLE + " (CID,NAME,CTID,UPDATETOKEN) VALUES(@CID,@NAME,@CTID,@UPDATETOKEN)";
            this.dsCommand.InsertCommand.CommandType = CommandType.Text;
            this.dsCommand.InsertCommand.Parameters.Add(new SqlParameter("@CID", SqlDbType.UniqueIdentifier, 16, DSCalculationModulesInfo.CMID_FIELD));
            this.dsCommand.InsertCommand.Parameters.Add(new SqlParameter("@NAME", SqlDbType.VarChar, 128, DSCalculationModulesInfo.CMNAME_FIELD));
            this.dsCommand.InsertCommand.Parameters.Add(new SqlParameter("@CTID", SqlDbType.UniqueIdentifier, 16, DSCalculationModulesInfo.CMTYPEID_FIELD));
            this.dsCommand.InsertCommand.Parameters.Add(new SqlParameter("@UPDATETOKEN", SqlDbType.Image, 16, DSCalculationModulesInfo.CMUPDATETOKEN_FIELD));

            this.dsCommand.DeleteCommand.CommandText = "DELETE " + BrokerManagedComponentDS.CINSTANCES_TABLE + " WHERE CINSTANCES.CID=@CID";
            this.dsCommand.DeleteCommand.CommandType = CommandType.Text;
            this.dsCommand.DeleteCommand.Parameters.Add(new SqlParameter("@CID", SqlDbType.UniqueIdentifier, 16, DSCalculationModulesInfo.CMID_FIELD));

            this.dsCommand.Update(data);
            return data;
        }

        public override void EstablishDatabase() { }
        public override void ClearDatabase() { }
        public override void RemoveDatabase() { }
        #endregion

        #region private method
        #endregion
    }
}

