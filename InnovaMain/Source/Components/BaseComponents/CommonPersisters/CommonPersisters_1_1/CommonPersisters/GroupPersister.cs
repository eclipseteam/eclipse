using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections;

using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.DSCalculation;
using Oritax.TaxSimp.BusinessStructureUtilities;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.CommonPersistence
{
	/// <summary>
	/// Summary description for GroupPersister.
	/// </summary>
	public class GroupPersister : CMPersister, IDisposable
	{
		private SqlDataAdapter dsGroupCommand;
		private BlobPersister blobPersister;
		private bool disposed = false;

		public GroupPersister(SqlConnection connection, SqlTransaction transaction): base(connection, transaction)
		{
			blobPersister = new BlobPersister(connection, transaction);

			//
			// Create the adapter for Group CMs
			//
			dsGroupCommand = new SqlDataAdapter();	// Adapter for Group CMs
			//
			// Create the DataSetCommand SelectCommand
			//
			dsGroupCommand.SelectCommand = DBCommandFactory.Instance.NewSqlCommand();
			dsGroupCommand.SelectCommand.CommandType = CommandType.Text;

			dsGroupCommand.SelectCommand.Connection = connection;
			dsGroupCommand.SelectCommand.Transaction = transaction;

			dsGroupCommand.InsertCommand = DBCommandFactory.Instance.NewSqlCommand();
			dsGroupCommand.InsertCommand.CommandText = "INSERT INTO CMMEMBERS(PARENTCLID,PARENTCSID,MEMBERCLID,MEMBERCSID, ADJUSTMENT) VALUES(@PARENTCLID,@PARENTCSID,@MEMBERCLID,@MEMBERCSID, @ADJUSTMENT)";
			dsGroupCommand.InsertCommand.CommandType = CommandType.Text;
			dsGroupCommand.InsertCommand.Parameters.Add(new SqlParameter("@PARENTCLID", SqlDbType.UniqueIdentifier,16,MembersDS.PARENTCLID_FIELD));
			dsGroupCommand.InsertCommand.Parameters.Add(new SqlParameter("@PARENTCSID",  SqlDbType.UniqueIdentifier,16,MembersDS.PARENTCSID_FIELD));
			dsGroupCommand.InsertCommand.Parameters.Add(new SqlParameter("@MEMBERCLID",  SqlDbType.UniqueIdentifier,16,MembersDS.MEMBERCLID_FIELD));
			dsGroupCommand.InsertCommand.Parameters.Add(new SqlParameter("@MEMBERCSID",  SqlDbType.UniqueIdentifier,16,MembersDS.MEMBERCSID_FIELD));
			dsGroupCommand.InsertCommand.Parameters.Add(new SqlParameter("@ADJUSTMENT",  SqlDbType.Bit,1,MembersDS.ADJUSTMENT_FIELD));
			dsGroupCommand.InsertCommand.Connection=dsGroupCommand.SelectCommand.Connection;
			dsGroupCommand.InsertCommand.Transaction=dsGroupCommand.SelectCommand.Transaction;

			dsGroupCommand.UpdateCommand = DBCommandFactory.Instance.NewSqlCommand();
			dsGroupCommand.UpdateCommand.CommandText = "UPDATE CMMEMBERS SET PARENTCLID=@PARENTCLID,PARENTCSID=@PARENTCSID,MEMBERCLID=@MEMBERCLID,MEMBERCSID=@MEMBERCSID WHERE PARENTCLID=@PARENTCLID AND PARENTCSID=@PARENTCSID AND MEMBERCLID=@MEMBERCLID";
			dsGroupCommand.UpdateCommand.CommandType = CommandType.Text;
			dsGroupCommand.UpdateCommand.Parameters.Add(new SqlParameter("@PARENTCLID", SqlDbType.UniqueIdentifier,16,MembersDS.PARENTCLID_FIELD));
			dsGroupCommand.UpdateCommand.Parameters.Add(new SqlParameter("@PARENTCSID", SqlDbType.UniqueIdentifier,16,MembersDS.PARENTCSID_FIELD));
			dsGroupCommand.UpdateCommand.Parameters.Add(new SqlParameter("@MEMBERCLID",  SqlDbType.UniqueIdentifier,16,MembersDS.MEMBERCLID_FIELD));
			dsGroupCommand.UpdateCommand.Parameters.Add(new SqlParameter("@MEMBERCSID",  SqlDbType.UniqueIdentifier,16,MembersDS.MEMBERCSID_FIELD));
			dsGroupCommand.UpdateCommand.Connection=dsGroupCommand.SelectCommand.Connection;
			dsGroupCommand.UpdateCommand.Transaction=dsGroupCommand.SelectCommand.Transaction;

			dsGroupCommand.DeleteCommand = DBCommandFactory.Instance.NewSqlCommand();
			dsGroupCommand.DeleteCommand.CommandText = "DELETE CMMEMBERS WHERE PARENTCLID=@PARENTCLID AND PARENTCSID=@PARENTCSID AND MEMBERCLID=@MEMBERCLID AND MEMBERCSID=@MEMBERCSID";
			dsGroupCommand.DeleteCommand.CommandType = CommandType.Text;
			dsGroupCommand.DeleteCommand.Parameters.Add(new SqlParameter("@PARENTCLID", SqlDbType.UniqueIdentifier,16,MembersDS.PARENTCLID_FIELD));
			dsGroupCommand.DeleteCommand.Parameters.Add(new SqlParameter("@PARENTCSID",  SqlDbType.UniqueIdentifier,16,MembersDS.PARENTCSID_FIELD));
			dsGroupCommand.DeleteCommand.Parameters.Add(new SqlParameter("@MEMBERCLID",  SqlDbType.UniqueIdentifier,16,MembersDS.MEMBERCLID_FIELD));
			dsGroupCommand.DeleteCommand.Parameters.Add(new SqlParameter("@MEMBERCSID",  SqlDbType.UniqueIdentifier,16,MembersDS.MEMBERCSID_FIELD));
			dsGroupCommand.DeleteCommand.Connection=dsGroupCommand.SelectCommand.Connection;
			dsGroupCommand.DeleteCommand.Transaction=dsGroupCommand.SelectCommand.Transaction;

			dsGroupCommand.TableMappings.Add("Table",MembersDS.CMMEMBERS_TABLE);
		}

		#region IDisposable Implementation ---------------------------------------

		/// <summary>
		///		Performs application-defined tasks associated with freeing, releasing, 
		///		or resetting unmanaged resources.
		/// </summary>
		public new void Dispose() 
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		/// <summary>
		///		Performs application-defined tasks associated with freeing, releasing, 
		///		or resetting unmanaged resources.
		/// </summary>
		/// <param name="disposing">
		///		True if called by the Dispose() method, false if called by the finalizer.
		/// </param>
		protected new void Dispose(bool disposing) 
		{
			if (!disposed)
			{
				if (disposing)
				{
					base.Dispose();
					blobPersister.Dispose();
					dsGroupCommand.Dispose();
				}
				disposed = true;
			}
		}

		#endregion // IDisposable Implementation

		public override IBrokerManagedComponent Get(Guid cID, ref DataSet primaryDS)
		{
			IBrokerManagedComponent iBMC = blobPersister.Get(cID, ref primaryDS);
			
			iBMC.MigrationInProgress = true;
			
			Guid cLID = ((ICalculationModule)iBMC).CLID;
			Guid cSID = ((ICalculationModule)iBMC).CSID;

			dsGroupCommand.SelectCommand.CommandText = "SELECT CMMEMBERS.PARENTCLID,CMMEMBERS.PARENTCSID,CMMEMBERS.MEMBERCLID,CMMEMBERS.MEMBERCSID, CMMEMBERS.ADJUSTMENT"
				+" FROM CMMEMBERS, CMSCENARIO"
				+" WHERE CMSCENARIO.CIID='{"+cID.ToString()+"}' "
				+"AND CMSCENARIO.CLID=CMMEMBERS.PARENTCLID AND CMSCENARIO.CSID=CMMEMBERS.PARENTCSID";

			MembersDS data=new MembersDS();
			dsGroupCommand.Fill(data.Tables[MembersDS.CMMEMBERS_TABLE]);

			iBMC.SetData(data);
			iBMC.MigrationInProgress = false;
			
			return iBMC;
		}

		public override void Save(IBrokerManagedComponent iBMC, ref DataSet persistedDS)
		{
			base.Save(iBMC, ref persistedDS);

			Guid cLID=((ICalculationModule)iBMC).CLID;
				
			dsGroupCommand.SelectCommand.CommandText = "SELECT CMMEMBERS.PARENTCLID,CMMEMBERS.PARENTCSID,CMMEMBERS.MEMBERCLID,CMMEMBERS.MEMBERCSID, CMMEMBERS.ADJUSTMENT"
				+" FROM CMMEMBERS, CMSCENARIO"
				+" WHERE CMSCENARIO.CIID='{"+iBMC.CID.ToString()+"}' AND CMSCENARIO.CSID='{"+((ICalculationModule)iBMC).CSID.ToString()+"}' AND CMSCENARIO.CLID='{"+cLID.ToString()+"}' AND CMSCENARIO.CLID=CMMEMBERS.PARENTCLID AND CMSCENARIO.CSID=CMMEMBERS.PARENTCSID";

			MembersDS dataTarget=new MembersDS();
			dsGroupCommand.Fill(dataTarget.Tables[MembersDS.CMMEMBERS_TABLE]);

			MembersDS dataSource=new MembersDS();
			iBMC.GetData(dataSource);
			dataTarget.Merge(dataSource,false,MissingSchemaAction.Ignore);

			dsGroupCommand.Update(dataTarget.Tables[MembersDS.CMMEMBERS_TABLE]);
			
			//Save all other group data as a Blob
			blobPersister.Broker = this.broker;
			blobPersister.Save(iBMC, ref persistedDS);
		}

		public override void Delete(IBrokerManagedComponent iBMC)
		{
			dsGroupCommand.SelectCommand.CommandText = "SELECT CMMEMBERS.PARENTCLID,CMMEMBERS.PARENTCSID,CMMEMBERS.MEMBERCLID,CMMEMBERS.MEMBERCSID,CMMEMBERS.ADJUSTMENT"
				+" FROM CMMEMBERS"
				+" WHERE CMMEMBERS.PARENTCLID ='{"+((ICalculationModule)iBMC).CLID.ToString()+"}' AND CMMEMBERS.PARENTCSID = '{"+((ICalculationModule)iBMC).CSID.ToString()+"}'";
			
			MembersDS dataTarget=new MembersDS();
			dsGroupCommand.Fill(dataTarget.Tables[MembersDS.CMMEMBERS_TABLE]);
		
			foreach(DataRow row in dataTarget.Tables[MembersDS.CMMEMBERS_TABLE].Rows)
				row.Delete();
			
			dsGroupCommand.Update(dataTarget.Tables[MembersDS.CMMEMBERS_TABLE]);

			blobPersister.Delete(iBMC);

			base.Delete(iBMC);
		}

		public override SqlConnection Connection
		{
			set
			{
				base.Connection=value;
				dsGroupCommand.InsertCommand.Connection=value;
				dsGroupCommand.SelectCommand.Connection=value;
				dsGroupCommand.UpdateCommand.Connection=value;
				dsGroupCommand.DeleteCommand.Connection=value;
			}
		}

		public override SqlTransaction Transaction
		{
			set
			{
				base.Transaction=value;
				dsGroupCommand.InsertCommand.Transaction=value;
				dsGroupCommand.SelectCommand.Transaction=value;
				dsGroupCommand.UpdateCommand.Transaction=value;
				dsGroupCommand.DeleteCommand.Transaction=value;
				blobPersister.Transaction = value;
			}
		}

		public override void EstablishDatabase()
		{
			base.EstablishDatabase();

            SqlCommand adminCommand = DBCommandFactory.Instance.NewSqlCommand();
            adminCommand.Connection = this.Connection;
			adminCommand.Transaction = this.Transaction;
			adminCommand.CommandText="if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CMMEMBERS]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)"
			+"CREATE TABLE [dbo].[CMMEMBERS] ("
			+"[PARENTCLID] [uniqueidentifier] NOT NULL ,"
			+"[PARENTCSID] [uniqueidentifier] NOT NULL ,"
			+"[MEMBERCLID] [uniqueidentifier] NOT NULL ,"
			+"[MEMBERCSID] [uniqueidentifier] NOT NULL ,"
			+"[ADDITION] [bit] NULL ,"
			+"[ADJUSTMENT] [bit] NULL "
			+") ON [PRIMARY]";
			adminCommand.ExecuteNonQuery();
		}

		public override void ClearDatabase()
		{
			base.ClearDatabase();
            SqlCommand adminCommand = DBCommandFactory.Instance.NewSqlCommand();
            adminCommand.Connection = this.Connection;
            adminCommand.Transaction = this.Transaction;

			adminCommand.CommandText="if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CMMEMBERS]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)"
				+"DELETE CMMEMBERS";
			adminCommand.ExecuteNonQuery();
		}

		public override void RemoveDatabase(){base.RemoveDatabase();}

		public override void FindDependentBMCs(Guid cID,ref ArrayList dependentCIDs, PersisterActionType actionType)
		{
			// In this case the dependent BMC's are those BMC's that are also Logical CM's which either:
			//   (A) represent Entities that have at least one scenario that is a member of a scenario of this group
			//   (B) represent Groups that have at least one scenario of which a scenario of this group is a member. 
			//
			base.FindDependentBMCs(cID,ref dependentCIDs, actionType);

            SqlCommand loadCommand = DBCommandFactory.Instance.NewSqlCommand();
            loadCommand.Connection = this.Connection;
			loadCommand.Transaction = this.Transaction;

			// Special case dependency SQL for this type of BMC.
			loadCommand.CommandText="SELECT DISTINCT DEPCMLOGICAL.CLID AS DEPENDENT_CLID FROM "
				+"CMLOGICAL AS DEPCMLOGICAL,CMSCENARIO AS DEPCMSCENARIO,SCENARIO,CMSCENARIO,CMLOGICAL,CMMEMBERS "
				+"WHERE DEPCMSCENARIO.CLID=DEPCMLOGICAL.CLID "
				+"AND SCENARIO.CSID=DEPCMSCENARIO.CSID "
				+"AND ( "
				+"(SCENARIO.CSID=CMMEMBERS.MEMBERCSID AND CMMEMBERS.PARENTCLID=CMSCENARIO.CLID AND CMSCENARIO.CIID='"+cID.ToString()+"') "
				+") ";

			SqlDataReader loadReader=loadCommand.ExecuteReader();

			while(loadReader.Read())
			{
				// lazy initialise if required
				if(dependentCIDs == null)
					dependentCIDs = new ArrayList();

				dependentCIDs.Add(loadReader.GetGuid(0));
			}

			loadReader.Close();
		}
	}
}

