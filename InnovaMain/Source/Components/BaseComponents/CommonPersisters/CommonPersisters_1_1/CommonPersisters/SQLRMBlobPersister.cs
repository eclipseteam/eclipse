using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using System.Transactions;

using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.DSCalculation;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.VelocityCache;

namespace Oritax.TaxSimp.CommonPersistence
{
    /// <summary>
    /// Summary description for BlobPersister.
    /// </summary>
    public class sQLRMBlobPersister : SQLRMBase
    {
        private IBrokerManagedComponent _IBMC;
        private SqlDataAdapter dsCommand;
        public long CMBlobSize
        {
            set
            {
                dsCommand.InsertCommand.Parameters["@PERSISTENT_DATA"].Size = (int)value;
                dsCommand.UpdateCommand.Parameters["@PERSISTENT_DATA"].Size = (int)value;
            }
        }

        public sQLRMBlobPersister(long blobSize, SqlConnection connection, SqlTransaction transaction, IBrokerManagedComponent iBMC, ITransactionVelocity wrappedTX)
            : base(wrappedTX)
        {
            //
            // Create the adapter
            //
            dsCommand = new SqlDataAdapter();
            //
            // Create the DataSetCommand SelectCommand
            //
            dsCommand.SelectCommand = DBCommandFactory.Instance.NewSqlCommand();
            dsCommand.SelectCommand.CommandType = CommandType.Text;
            dsCommand.SelectCommand.Connection = connection;
            dsCommand.SelectCommand.Transaction = transaction;

            dsCommand.InsertCommand = DBCommandFactory.Instance.NewSqlCommand();
            dsCommand.InsertCommand.CommandText = "INSERT INTO BLOBPERSIST (INSTANCEID,NAME,TYPE,PERSISTENT_DATA) VALUES(@INSTANCEID,@NAME,@TYPE,@PERSISTENT_DATA)";
            dsCommand.InsertCommand.CommandType = CommandType.Text;
            dsCommand.InsertCommand.Parameters.Add(new SqlParameter("@INSTANCEID", SqlDbType.UniqueIdentifier, 16, DSCalculationModule.INSTANCEID_FIELD));
            dsCommand.InsertCommand.Parameters.Add(new SqlParameter("@NAME", SqlDbType.VarChar, 128, DSCalculationModule.NAME_FIELD));
            dsCommand.InsertCommand.Parameters.Add(new SqlParameter("@TYPE", SqlDbType.UniqueIdentifier, 16, DSCalculationModule.TYPE_FIELD));
            dsCommand.InsertCommand.Parameters.Add(new SqlParameter("@PERSISTENT_DATA", SqlDbType.Image, (int)blobSize, DSCalculationModule.PERSISTANT_DATA_FIELD));
            dsCommand.InsertCommand.Connection = dsCommand.SelectCommand.Connection;
            dsCommand.InsertCommand.Transaction = dsCommand.SelectCommand.Transaction;

            dsCommand.UpdateCommand = DBCommandFactory.Instance.NewSqlCommand();
            dsCommand.UpdateCommand.CommandText = "UPDATE BLOBPERSIST SET PERSISTENT_DATA=@PERSISTENT_DATA, TYPE=@TYPE, NAME=@NAME WHERE INSTANCEID=@INSTANCEID";
            dsCommand.UpdateCommand.CommandType = CommandType.Text;
            dsCommand.UpdateCommand.Parameters.Add(new SqlParameter("@PERSISTENT_DATA", SqlDbType.Image, (int)blobSize, DSCalculationModule.PERSISTANT_DATA_FIELD));
            dsCommand.UpdateCommand.Parameters.Add(new SqlParameter("@NAME", SqlDbType.VarChar, 128, DSCalculationModule.NAME_FIELD));
            dsCommand.UpdateCommand.Parameters.Add(new SqlParameter("@TYPE", SqlDbType.UniqueIdentifier, 16, DSCalculationModule.TYPE_FIELD));
            dsCommand.UpdateCommand.Parameters.Add(new SqlParameter("@INSTANCEID", SqlDbType.UniqueIdentifier, 16, DSCalculationModule.INSTANCEID_FIELD));
            dsCommand.UpdateCommand.Connection = dsCommand.SelectCommand.Connection;
            dsCommand.UpdateCommand.Transaction = dsCommand.SelectCommand.Transaction;

            dsCommand.DeleteCommand = DBCommandFactory.Instance.NewSqlCommand();
            dsCommand.DeleteCommand.CommandText = "DELETE BLOBPERSIST WHERE BLOBPERSIST.INSTANCEID=@INSTANCEID";
            dsCommand.DeleteCommand.CommandType = CommandType.Text;
            dsCommand.DeleteCommand.Parameters.Add(new SqlParameter("@INSTANCEID", SqlDbType.UniqueIdentifier, 16, DSCalculationModule.INSTANCEID_FIELD));
            dsCommand.DeleteCommand.Connection = dsCommand.SelectCommand.Connection;
            dsCommand.DeleteCommand.Transaction = dsCommand.SelectCommand.Transaction;

            dsCommand.TableMappings.Add("Table", DSCalculationModule.INSTANCE_TABLE);

            this._IBMC = iBMC;
        }

        public override void Prepare(PreparingEnlistment preparingEnlistment)
        {
            //try
            //{
                MemoryStream memoryStream = new MemoryStream(1000);
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                binaryFormatter.Serialize(memoryStream, this._IBMC);
                this.CMBlobSize = memoryStream.Length;

                dsCommand.SelectCommand.CommandText = "SELECT BLOBPERSIST.INSTANCEID,BLOBPERSIST.NAME,BLOBPERSIST.TYPE, CAST( NULL AS image) AS PERSISTENT_DATA"
                    + " FROM BLOBPERSIST"
                    + " WHERE BLOBPERSIST.INSTANCEID='{" + this._IBMC.CID.ToString() + "}'";

                DSCalculationModule data = new DSCalculationModule();
                dsCommand.Fill(data);

                if (data.Tables[DSCalculationModule.INSTANCE_TABLE].Rows.Count == 0)
                    data = new DSCalculationModule(this._IBMC.CID, this._IBMC.Name, this._IBMC.TypeID, memoryStream.ToArray());

                data.Tables[DSCalculationModule.INSTANCE_TABLE].Rows[0][DSCalculationModule.PERSISTANT_DATA_FIELD] = memoryStream.ToArray();
                data.Tables[DSCalculationModule.INSTANCE_TABLE].Rows[0][DSCalculationModule.NAME_FIELD] = this._IBMC.Name;
                data.Tables[DSCalculationModule.INSTANCE_TABLE].Rows[0][DSCalculationModule.TYPE_FIELD] = this._IBMC.TypeID;

                dsCommand.Update(data);

            //    preparingEnlistment.Prepared();
            //}
            //catch
            //{
            //    preparingEnlistment.ForceRollback();
            //}
        }

        public void Delete(IBrokerManagedComponent iBMC)
        {
            dsCommand.SelectCommand.CommandText = "SELECT BLOBPERSIST.INSTANCEID"
                + " FROM BLOBPERSIST"
                + " WHERE BLOBPERSIST.INSTANCEID='{" + this._IBMC.CID.ToString() + "}'";

            DSCalculationModule data = new DSCalculationModule();
            dsCommand.Fill(data);

            foreach (DataRow row in data.Tables[DSCalculationModule.INSTANCE_TABLE].Rows)
            {
                row.Delete();
            }

            dsCommand.Update(data);

        }

        public override void Commit(Enlistment enlistment)
        {
            //if (!this._WrappedTransaction.IsSQLTransactionDisposed)
            //{
            //    this._WrappedTransaction.IsSQLTransactionDisposed = true;
            //    this._WrappedTransaction.SQLTransaction.Commit();
            //}

            //enlistment.Done();
        }

        public override void Rollback(Enlistment enlistment)
        {
            //if (!this._WrappedTransaction.IsSQLTransactionDisposed)
            //{
            //    this._WrappedTransaction.IsSQLTransactionDisposed = true;
            //    this._WrappedTransaction.SQLTransaction.Rollback();
            //}

            //enlistment.Done();
        }

    }
}
