using System;
using System.Diagnostics;
using System.Configuration;
using System.Data.SqlClient;
using System.Reflection;
using System.Data;

using NUnit.Framework;

using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.DSCalculation;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.BusinessStructureUtilities;




namespace Oritax.TaxSimp.CalculationPersistSQL
{
	/// <summary>
	/// Summary description for CMPersisterTestFixture.
	/// </summary>
	/// 

	public class CMPersisterChild : CMPersister
	{
		public CMPersisterChild(SqlConnection connection):base(connection){}
		public override IBrokerManagedComponent GetCMByInstanceId(Guid instanceId){return new BrokerManagedComponent();}
		public override void SaveCM(IBrokerManagedComponent iBMC){}
		public override DataSet UpdateCM(DataSet data){ return new DataSet();}
	}


	[TestFixture]
	public class CMPersisterTestFixture
	{
		SqlConnection sqlCon = null;

		[Conditional("DEBUG")] [SetUp] public void PersisterTestSetup()
		{
			string sConn = ConfigurationSettings.AppSettings["TX360ENTERPRISE_DBCONNECTIONSTRING"];
			sqlCon = new SqlConnection(sConn);
			sqlCon.Open();
		}

		[Conditional("DEBUG")] [TearDown] public void PersisterTestTearDown()
		{
			sqlCon.Close();
			sqlCon.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterConstructionTest()
		{
			CMPersisterChild cmPersist = new CMPersisterChild(sqlCon);

			FieldInfo sqlConnectionInfo = cmPersist.GetType().GetField("sqlConnection", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlConnection sqlConnection = (SqlConnection)sqlConnectionInfo.GetValue(cmPersist);
			Assertion.AssertEquals("Connection has not been set", sqlConnection,sqlCon);

			FieldInfo sqlTransactionInfo = cmPersist.GetType().GetField("sqlTransaction", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlTransaction sqlTransaction = (SqlTransaction)sqlTransactionInfo.GetValue(cmPersist);
			Assertion.AssertNull("sqlTransaction is not null", sqlTransaction);

			cmPersist.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterConnectionTest()
		{
			CMPersisterChild cmPersist = new CMPersisterChild(sqlCon);
			cmPersist.Connection = sqlCon;

			FieldInfo sqlConnectionInfo = cmPersist.GetType().GetField("sqlConnection", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlConnection sqlConnection = (SqlConnection)sqlConnectionInfo.GetValue(cmPersist);
			Assertion.AssertEquals("Connection has not been set", sqlConnection,sqlCon);

			cmPersist.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterTransactionTest()
		{
			CMPersisterChild cmPersist = new CMPersisterChild(sqlCon);
			SqlTransaction sqlTran = sqlCon.BeginTransaction();
			cmPersist.Transaction = sqlTran;
			FieldInfo sqlTransactionInfo = cmPersist.GetType().GetField("sqlTransaction", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlTransaction sqlTransaction = (SqlTransaction)sqlTransactionInfo.GetValue(cmPersist);
			Assertion.AssertEquals("sqlTransaction has not been set", sqlTransaction, sqlTran);
			sqlTran.Commit();
			sqlTran.Dispose();
			cmPersist.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterEstablishDBTest()
		{
			CMPersisterChild cmPersist = new CMPersisterChild(sqlCon);

			SqlTransaction sqlTran = sqlCon.BeginTransaction();
			cmPersist.Transaction = sqlTran;
			try
			{
				cmPersist.EstablishDatabase();
			}
			finally
			{
				sqlTran.Rollback();	//DO NOT CORRUPT THE DATABASE
			}
			cmPersist.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterClearDBTest()
		{
			CMPersisterChild cmPersist = new CMPersisterChild(sqlCon);

			SqlTransaction sqlTran = sqlCon.BeginTransaction();
			cmPersist.Transaction = sqlTran;
			try
			{
				cmPersist.ClearDatabase();
			}
			finally
			{
				sqlTran.Rollback();	//DO NOT CORRUPT THE DATABASE
			}
			cmPersist.Dispose();
		}
		[Conditional("DEBUG")] [Test] public void PersisterGetByInstanceIdTest()
		{

			//go get the first user Instance
			CMListPersist bmcList= new CMListPersist(sqlCon);
			DSBMCInfo data = (DSBMCInfo)bmcList.FindByTypeName("DBUser");

			if(data.Tables[DSBMCInfo.CMLIST_TABLE].Rows.Count == 0)
			{
				throw new Exception("This Test requires a user instance");
			}
			DataRow firstRow=data.Tables[DSBMCInfo.CMLIST_TABLE].Rows[0];

			CMPersisterChild cmPersist = new CMPersisterChild(sqlCon);

			IBrokerManagedComponent ibmc = cmPersist.GetByInstanceId((Guid)firstRow[DSBMCInfo.CMID_FIELD]);
			Assertion.AssertNotNull("Could not find instance",ibmc);
			bmcList.Dispose();
			cmPersist.Dispose();
		}

#if false
		[Conditional("DEBUG")] [Test] public void PersisterSaveTest()
		{
			//Need a CM broker to test this function but don't want to add
			//it to the project
		}
#endif

	}
}
