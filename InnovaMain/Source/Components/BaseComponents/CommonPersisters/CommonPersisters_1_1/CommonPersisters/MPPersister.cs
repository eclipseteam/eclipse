using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.DSCalculation;
using Oritax.TaxSimp.BusinessStructureUtilities;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.CommonPersistence
{
	/// <summary>
	/// Summary description for GroupPersister.
	/// </summary>
	public class MPPersister : CMPersister, IDisposable
	{
		
		private SqlDataAdapter dsGroupCommand;
		private BlobPersister blobPersister;
		private bool disposed = false;

		public MPPersister(SqlConnection connection, SqlTransaction transaction):base(connection, transaction)
		{
			blobPersister = new BlobPersister(connection, transaction);
			//
			// Create the adapter for Group CMs
			//
			dsGroupCommand = new SqlDataAdapter();	// Adapter for Group CMs
			//
			// Create the DataSetCommand SelectCommand
			//
			dsGroupCommand.SelectCommand = DBCommandFactory.Instance.NewSqlCommand();
			dsGroupCommand.SelectCommand.CommandType = CommandType.Text;

			dsGroupCommand.SelectCommand.Connection = this.Connection;

			dsGroupCommand.InsertCommand = DBCommandFactory.Instance.NewSqlCommand();
			dsGroupCommand.InsertCommand.CommandText = "INSERT INTO CMMEMBERS(PARENTCLID,PARENTCSID,MEMBERCLID,MEMBERCSID, ADDITION, ADJUSTMENT) VALUES(@PARENTCLID,@PARENTCSID,@MEMBERCLID,@MEMBERCSID, @ADDITION, @ADJUSTMENT)";
			dsGroupCommand.InsertCommand.CommandType = CommandType.Text;
			dsGroupCommand.InsertCommand.Parameters.Add(new SqlParameter("@PARENTCLID", SqlDbType.UniqueIdentifier,16,MembersDS.PARENTCLID_FIELD));
			dsGroupCommand.InsertCommand.Parameters.Add(new SqlParameter("@PARENTCSID",  SqlDbType.UniqueIdentifier,16,MembersDS.PARENTCSID_FIELD));
			dsGroupCommand.InsertCommand.Parameters.Add(new SqlParameter("@MEMBERCLID",  SqlDbType.UniqueIdentifier,16,MembersDS.MEMBERCLID_FIELD));
			dsGroupCommand.InsertCommand.Parameters.Add(new SqlParameter("@MEMBERCSID",  SqlDbType.UniqueIdentifier,16,MembersDS.MEMBERCSID_FIELD));
			dsGroupCommand.InsertCommand.Parameters.Add(new SqlParameter("@ADDITION",  SqlDbType.Bit,1,MembersDS.ADDITION_FIELD));
			dsGroupCommand.InsertCommand.Parameters.Add(new SqlParameter("@ADJUSTMENT",  SqlDbType.Bit,1,MembersDS.ADJUSTMENT_FIELD));
			dsGroupCommand.InsertCommand.Connection=dsGroupCommand.SelectCommand.Connection;

			dsGroupCommand.UpdateCommand = DBCommandFactory.Instance.NewSqlCommand();
			dsGroupCommand.UpdateCommand.CommandText = "UPDATE CMMEMBERS SET PARENTCLID=@PARENTCLID,PARENTCSID=@PARENTCSID,MEMBERCLID=@MEMBERCLID,MEMBERCSID=@MEMBERCSID, ADDITION=@ADDITION, ADJUSTMENT=@ADJUSTMENT WHERE PARENTCLID=@PARENTCLID AND PARENTCSID=@PARENTCSID AND MEMBERCLID=@MEMBERCLID AND MEMBERCSID=@MEMBERCSID";
			dsGroupCommand.UpdateCommand.CommandType = CommandType.Text;
			dsGroupCommand.UpdateCommand.Parameters.Add(new SqlParameter("@PARENTCLID", SqlDbType.UniqueIdentifier,16,MembersDS.PARENTCLID_FIELD));
			dsGroupCommand.UpdateCommand.Parameters.Add(new SqlParameter("@PARENTCSID", SqlDbType.UniqueIdentifier,16,MembersDS.PARENTCSID_FIELD));
			dsGroupCommand.UpdateCommand.Parameters.Add(new SqlParameter("@MEMBERCLID",  SqlDbType.UniqueIdentifier,16,MembersDS.MEMBERCLID_FIELD));
			dsGroupCommand.UpdateCommand.Parameters.Add(new SqlParameter("@MEMBERCSID",  SqlDbType.UniqueIdentifier,16,MembersDS.MEMBERCSID_FIELD));
			dsGroupCommand.UpdateCommand.Parameters.Add(new SqlParameter("@ADDITION",  SqlDbType.Bit,1,MembersDS.ADDITION_FIELD));
			dsGroupCommand.UpdateCommand.Parameters.Add(new SqlParameter("@ADJUSTMENT",  SqlDbType.Bit,1,MembersDS.ADJUSTMENT_FIELD));
			dsGroupCommand.UpdateCommand.Connection=dsGroupCommand.SelectCommand.Connection;

			dsGroupCommand.DeleteCommand = DBCommandFactory.Instance.NewSqlCommand();
			dsGroupCommand.DeleteCommand.CommandText = "DELETE CMMEMBERS WHERE PARENTCLID=@PARENTCLID AND PARENTCSID=@PARENTCSID AND MEMBERCLID=@MEMBERCLID AND MEMBERCSID=@MEMBERCSID AND ADDITION=@ADDITION AND ADJUSTMENT=@ADJUSTMENT";
			dsGroupCommand.DeleteCommand.CommandType = CommandType.Text;
			dsGroupCommand.DeleteCommand.Parameters.Add(new SqlParameter("@PARENTCLID", SqlDbType.UniqueIdentifier,16,MembersDS.PARENTCLID_FIELD));
			dsGroupCommand.DeleteCommand.Parameters.Add(new SqlParameter("@PARENTCSID",  SqlDbType.UniqueIdentifier,16,MembersDS.PARENTCSID_FIELD));
			dsGroupCommand.DeleteCommand.Parameters.Add(new SqlParameter("@MEMBERCLID",  SqlDbType.UniqueIdentifier,16,MembersDS.MEMBERCLID_FIELD));
			dsGroupCommand.DeleteCommand.Parameters.Add(new SqlParameter("@MEMBERCSID",  SqlDbType.UniqueIdentifier,16,MembersDS.MEMBERCSID_FIELD));
			dsGroupCommand.DeleteCommand.Parameters.Add(new SqlParameter("@ADDITION",  SqlDbType.Bit,1,MembersDS.ADDITION_FIELD));
			dsGroupCommand.DeleteCommand.Parameters.Add(new SqlParameter("@ADJUSTMENT",  SqlDbType.Bit,1,MembersDS.ADJUSTMENT_FIELD));
			
			dsGroupCommand.DeleteCommand.Connection=dsGroupCommand.SelectCommand.Connection;

			dsGroupCommand.TableMappings.Add("Table",MembersDS.CMMEMBERS_TABLE);
		}

		#region IDisposable Implementation ---------------------------------------

		/// <summary>
		///		Performs application-defined tasks associated with freeing, releasing, 
		///		or resetting unmanaged resources.
		/// </summary>
		public new void Dispose() 
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		/// <summary>
		///		Performs application-defined tasks associated with freeing, releasing, 
		///		or resetting unmanaged resources.
		/// </summary>
		/// <param name="disposing">
		///		True if called by the Dispose() method, false if called by the finalizer.
		/// </param>
		protected new void Dispose(bool disposing) 
		{
			if (!disposed)
			{
				if (disposing)
				{
					base.Dispose();
					dsGroupCommand.Dispose();
				}
				disposed = true;
			}
		}

		#endregion // IDisposable Implementation

		public override IBrokerManagedComponent Get(Guid cID, ref DataSet primaryDS)
		{
			IBrokerManagedComponent iBMC = blobPersister.Get(cID, ref primaryDS);
			
			//Guid cLID=this.Broker.CIIDToCLID(instanceId);
			Guid cLID = ((ICalculationModule)iBMC).CLID;
			Guid cSID = ((ICalculationModule)iBMC).CSID;

			dsGroupCommand.SelectCommand.CommandText = "SELECT CMMEMBERS.PARENTCLID,CMMEMBERS.PARENTCSID,CMMEMBERS.MEMBERCLID,CMMEMBERS.MEMBERCSID, CMMEMBERS.ADDITION, CMMEMBERS.ADJUSTMENT"
				+" FROM CMMEMBERS, CMSCENARIO"
				+" WHERE CMSCENARIO.CIID='{"+cID.ToString()+"}' AND CMSCENARIO.CLID='{"+cLID.ToString()+"}' AND CMSCENARIO.CSID ='{"+cSID.ToString()+"}' "
				+"AND CMSCENARIO.CLID=CMMEMBERS.PARENTCLID AND CMSCENARIO.CSID=CMMEMBERS.PARENTCSID";


			MembersDS data=new MembersDS();
			dsGroupCommand.Fill(data.Tables[MembersDS.CMMEMBERS_TABLE]);

			iBMC.DeliverData(data);

			base.Hydrate(iBMC,cID, ref primaryDS);

			return iBMC;
		}

		public override void Save(IBrokerManagedComponent iBMC, ref DataSet persistedDS)
		{
			Guid cLID=this.Broker.CIIDToCLID(iBMC.CID);
				
			dsGroupCommand.SelectCommand.CommandText = "SELECT CMMEMBERS.PARENTCLID,CMMEMBERS.PARENTCSID,CMMEMBERS.MEMBERCLID,CMMEMBERS.MEMBERCSID"
				+" FROM CMMEMBERS, CMSCENARIO"
				+" WHERE CMSCENARIO.CIID='{"+iBMC.CID.ToString()+"}' AND CMSCENARIO.CLID='{"+cLID.ToString()+"}' AND CMSCENARIO.CLID=CMMEMBERS.PARENTCLID AND CMSCENARIO.CSID=CMMEMBERS.PARENTCSID";

			MembersDS dataTarget=new MembersDS();
			dsGroupCommand.Fill(dataTarget.Tables[MembersDS.CMMEMBERS_TABLE]);

			MembersDS dataSource=new MembersDS();
			iBMC.ExtractData(dataSource);
			dataTarget.Merge(dataSource,false,MissingSchemaAction.Ignore);

			dsGroupCommand.Update(dataTarget.Tables[MembersDS.CMMEMBERS_TABLE]);
			
			//Save all other group data as a Blob
			blobPersister.Save(iBMC, ref persistedDS);

			base.Save(iBMC, ref persistedDS);
		}

		public override void Delete(IBrokerManagedComponent iBMC)
		{			
			dsGroupCommand.SelectCommand.CommandText = "SELECT CMMEMBERS.PARENTCLID,CMMEMBERS.PARENTCSID,CMMEMBERS.MEMBERCLID,CMMEMBERS.MEMBERCSID, CMMEMBERS.ADDITION, CMMEMBERS.ADJUSTMENT"
				+" FROM CMMEMBERS"
				+" WHERE CMMEMBERS.PARENTCLID='{"+((ICalculationModule)iBMC).CLID.ToString()+"}' AND CMMEMBERS.PARENTCSID='{"+((ICalculationModule)iBMC).CSID.ToString()+"}'";
				
			MembersDS dataTarget=new MembersDS();
			dsGroupCommand.Fill(dataTarget.Tables[MembersDS.CMMEMBERS_TABLE]);
		
			foreach(DataRow row in dataTarget.Tables[MembersDS.CMMEMBERS_TABLE].Rows)
				row.Delete();
			
			dsGroupCommand.Update(dataTarget.Tables[MembersDS.CMMEMBERS_TABLE]);

			blobPersister.Delete(iBMC);
		}

		public override SqlConnection Connection
		{
			set
			{
				base.Connection = value;
				dsGroupCommand.InsertCommand.Connection=value;
				dsGroupCommand.SelectCommand.Connection=value;
				dsGroupCommand.UpdateCommand.Connection=value;
				dsGroupCommand.DeleteCommand.Connection=value;
			}
		}

		public override SqlTransaction Transaction
		{
			set
			{
				base.Transaction = value;
				dsGroupCommand.InsertCommand.Transaction=value;
				dsGroupCommand.SelectCommand.Transaction=value;
				dsGroupCommand.UpdateCommand.Transaction=value;
				dsGroupCommand.DeleteCommand.Transaction=value;
				blobPersister.Transaction = value;
			}
		}
		public override void EstablishDatabase(){base.EstablishDatabase();}
		public override void ClearDatabase(){base.ClearDatabase();}
		public override void RemoveDatabase(){base.RemoveDatabase();}
	}
}

