using System;
using System.Data;

namespace Oritax.TaxSimp.Calculation
{
	/// <summary>
	/// DSCalculationModule is a DataSet that contains all of the persistent data for a CM instance.
	/// </summary>
	[Serializable] 
	public class ScenariosDS : BusinessStructureWorkpaperDS
	{
		public const String SCENARIOS_TABLE			= "SCENARIOS_TABLE";
		public const String NAME_FIELD				= "SCENARIONAME_FIELD";
		public const String CSID_FIELD				= "CSID_FIELD";
		public const String CIID_FIELD				= "CIID_FIELD";
		public const String CLID_FIELD				= "CLID_FIELD";
		public const String TYPE_FIELD				= "TYPE_FIELD";
		public const String STATUS_FIELD			= "STATUS_FIELD";
		public const String	CREATIONDATETIME_FIELD	= "CREATIONDATETIME_FIELD";
		public const String CURRENT_FIELD			= "CURRENT_FIELD";
		public const String LOCKED_FIELD			= "LOCKED_FIELD";


		private bool deleteInMembers = false;
		private Guid cLID;  // The logical CMID for which this dataset contains the scenarios
		private string breadCrumb;
		
		public ScenariosDS()
		{
			DataTable table;

			table = new DataTable(SCENARIOS_TABLE);
			table.Columns.Add(NAME_FIELD, typeof(System.String));
			table.Columns.Add(CSID_FIELD, typeof(System.String));
			table.Columns.Add(CIID_FIELD, typeof(System.String));
			table.Columns.Add(CLID_FIELD, typeof(System.String));
			table.Columns.Add(TYPE_FIELD, typeof(System.String));
			table.Columns.Add(STATUS_FIELD, typeof(System.String));
			table.Columns.Add(CREATIONDATETIME_FIELD, typeof(System.DateTime));
			table.Columns.Add(CURRENT_FIELD, typeof(System.Boolean));
			table.Columns.Add(LOCKED_FIELD, typeof(System.Boolean));
			this.Tables.Add(table);
		}

		public Guid CLID{get{return cLID;}set{cLID=value;}}
		public string BreadCrumb{get{return breadCrumb;}set{breadCrumb=value;}}
		public bool DeleteInMembers{get{return deleteInMembers;}set{deleteInMembers=value;}}
		
	}
}

