using System;
using System.Collections;
using System.Data;
using System.Configuration;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Security.Permissions ;

using Oritax.TaxSimp.BusinessStructureUtilities;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.Group;
using Oritax.TaxSimp.CM.Entity;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.Security;
using Oritax.TaxSimp.Exceptions;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.CM.Period;

namespace Oritax.TaxSimp.Calculation
{
	/// <summary>
	/// Summary description for BusinessStructureBase.
	/// </summary>
	[Serializable] 
	public class BusinessStructureBase : CalculationModuleBase, IBusinessStructureModule
	{
		#region FIELD VARIABLES
		// CM Non-Persistent Data
		[NonSerialized]	static private bool		messageLoggingOn=GetLoggingEnabled();
		[NonSerialized]	static private string	logPath=GetLogPath();

	    [NonSerialized]
		public OnMessageBeingDeliveredDelagate MessageDeliveredDelegate;
	
		#endregion

		#region CONSTRUCTORS
		public BusinessStructureBase()
			: base()
		{
		}

		protected BusinessStructureBase(SerializationInfo si, StreamingContext context)
			: base(si,context)
		{
		}
	
		//Only allow the .NET Serialization core code to call this function
		[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]
		public override void GetObjectData(SerializationInfo si, StreamingContext context)
		{
			base.GetObjectData(si,context);
		}

		#endregion

		#region OVERRIDABLE CM FUNCTIONS

		public override DataSet PrimaryDataSet{get{return new BusinessStructureBaseDS();}}

		public override void Initialize(String name)
		{
			base.Initialize(name);
		}

		#region OnGetData Overloads
		protected override void OnGetData(DataSet data)
		{
			base.OnGetData(data);
			if (data is BusinessStructureWorkpaperDS)
			{
				this.OnGetData((BusinessStructureWorkpaperDS)data);
			}
			if (data is ScenariosDS)
			{
				this.OnGetData((ScenariosDS)data);
			}
			if (data is ScenarioDS)
			{
				this.OnGetData((ScenarioDS)data);
			}
		}

		protected void OnGetData( BusinessStructureWorkpaperDS objData )
		{
			if(objData.Username != null && objData.Username != String.Empty)
			{
				IDBUser objUser = (IDBUser) Broker.GetBMCInstance( objData.Username, "DBUser_1_1" );
			
				objData.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows.Clear();
				DataRow dRow = objData.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].NewRow();
				dRow[BusinessStructureWorkpaperDS.REPORTINGUNITNAME_FIELD] = objUser.Context.ReportingUnitName;
				dRow[BusinessStructureWorkpaperDS.REPORTINGUNITCLID_FIELD] = objUser.Context.ReportingUnitCLID;
				dRow[BusinessStructureWorkpaperDS.SCENARIONAME_FIELD] = objUser.Context.ScenarioName;
				dRow[BusinessStructureWorkpaperDS.SCENARIOCSID_FIELD] = objUser.Context.ScenarioCSID;
				dRow[BusinessStructureWorkpaperDS.PERIODNAME_FIELD] = objUser.Context.PeriodName;
				dRow[BusinessStructureWorkpaperDS.PERIODCLID_FIELD] = objUser.Context.PeriodCLID;
				objData.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows.Add(dRow);

				this.Broker.ReleaseBrokerManagedComponent((IBrokerManagedComponent) objUser);
			}
			else
			{
				objData.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows.Clear();
				DataRow dRow = objData.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].NewRow();
				dRow[BusinessStructureWorkpaperDS.REPORTINGUNITNAME_FIELD] = String.Empty;
				dRow[BusinessStructureWorkpaperDS.REPORTINGUNITCLID_FIELD] = Guid.Empty;
				dRow[BusinessStructureWorkpaperDS.SCENARIONAME_FIELD] = String.Empty;
				dRow[BusinessStructureWorkpaperDS.SCENARIOCSID_FIELD] = Guid.Empty;
				dRow[BusinessStructureWorkpaperDS.PERIODNAME_FIELD] = String.Empty;
				dRow[BusinessStructureWorkpaperDS.PERIODCLID_FIELD] = Guid.Empty;
				objData.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows.Add(dRow);
			}

		}

		protected void OnGetData( ScenariosDS objData )
		{
			ILogicalModule logicalEntity=this.Broker.GetLogicalCM(objData.CLID);

			foreach(DictionaryEntry dictionaryEntry in logicalEntity.Scenarios)
			{
				CMScenario cMScenario=(CMScenario)dictionaryEntry.Value;

				//Only get scenarios which are not marked as deleted.
				
				DataRow newRow=objData.Tables[ScenariosDS.SCENARIOS_TABLE].NewRow();
				if(cMScenario.CSID==logicalEntity.CurrentScenario)
				{
					newRow[ScenariosDS.NAME_FIELD]=cMScenario.Name+" [default]";
					newRow[ScenariosDS.CURRENT_FIELD]=true;
				}
				else
				{
					newRow[ScenariosDS.NAME_FIELD]=cMScenario.Name;
					newRow[ScenariosDS.CURRENT_FIELD]=false;
				}
					
				if(logicalEntity.IsScenarioLocked(cMScenario.CSID))
					newRow[ScenariosDS.LOCKED_FIELD]=true;
				else
					newRow[ScenariosDS.LOCKED_FIELD]=false;

				newRow[ScenariosDS.CIID_FIELD]=cMScenario.CIID.ToString();
				newRow[ScenariosDS.CSID_FIELD]=cMScenario.CSID.ToString();
				newRow[ScenariosDS.CLID_FIELD]=cMScenario.CLID.ToString();
				newRow[ScenariosDS.TYPE_FIELD]=cMScenario.Type.ToString();
				newRow[ScenariosDS.STATUS_FIELD]=cMScenario.Status.ToString();
				newRow[ScenariosDS.CREATIONDATETIME_FIELD]=cMScenario.CreationDateTime;
				objData.Tables[ScenariosDS.SCENARIOS_TABLE].Rows.Add(newRow);
			}

			this.Broker.ReleaseBrokerManagedComponent(logicalEntity);
		}
		
		protected void OnGetData( ScenarioDS objData )
		{
			string cSIDStr=objData.CSID;
			string cLIDStr=objData.CLID;
			Guid cLID=new Guid(cLIDStr);
			Guid cSID=new Guid(cSIDStr);

			ILogicalModule logicalEntity=this.Broker.GetLogicalCM(cLID);
			CMScenario cMScenario=logicalEntity.GetScenario(cSID);

			objData.CSID=cMScenario.CSID.ToString();
			objData.CopiedCSID=cMScenario.CSID.ToString();
			objData.Name=cMScenario.Name;
			objData.Type=cMScenario.Type;
			objData.CreationDateTime=cMScenario.CreationDateTime;
			objData.ModificationDateTime=cMScenario.ModificationDateTime;
			objData.Status=cMScenario.Status;
			objData.Locked=cMScenario.Locked;
			objData.ParentLocked= logicalEntity.IsConsolidationOwnerLocked(cSID);
			objData.BreadCrumb = this.ConstructBreadcrumb();

			this.Broker.ReleaseBrokerManagedComponent(logicalEntity);
		}

		#endregion
		#region OnSetData overloads
		protected override ModifiedState OnSetData(DataSet data)
		{
			base.OnSetData(data);
			if (data is BusinessStructureBaseDS)
			{
				DeliverPrimaryData((BusinessStructureBaseDS)data);
			}
			else if (data is BusinessStructureWorkpaperDS)
			{
				this.OnSetData((BusinessStructureWorkpaperDS)data);
			}

			return ModifiedState.MODIFIED;
		}

		protected ModifiedState OnSetData( BusinessStructureWorkpaperDS objData )
		{
			if(objData.Username != null && objData.Username != String.Empty)
			{
				IDBUser objUser = (IDBUser) Broker.GetBMCInstance( objData.Username, "DBUser_1_1" );
				DataRow dRow = objData.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows[0];
				objUser.Context.ReportingUnitName = dRow[BusinessStructureWorkpaperDS.REPORTINGUNITNAME_FIELD].ToString();
				objUser.Context.ReportingUnitCLID = (Guid)dRow[BusinessStructureWorkpaperDS.REPORTINGUNITCLID_FIELD];
				objUser.Context.ScenarioName = dRow[BusinessStructureWorkpaperDS.SCENARIONAME_FIELD].ToString();
				objUser.Context.ScenarioCSID = (Guid)dRow[BusinessStructureWorkpaperDS.SCENARIOCSID_FIELD];
				objUser.Context.PeriodName = dRow[BusinessStructureWorkpaperDS.PERIODNAME_FIELD].ToString();
				objUser.Context.PeriodCLID = (Guid)dRow[BusinessStructureWorkpaperDS.PERIODCLID_FIELD];
				((IBrokerManagedComponent)objUser).CalculateToken(true);
				this.Broker.ReleaseBrokerManagedComponent((IBrokerManagedComponent) objUser);
			}
			return ModifiedState.UNCHANGED;
		}

		public override void DeliverData(DataSet data)
		{

			base.DeliverData(data);

			if(data is BusinessStructureBaseDS)
			{
				DeliverPrimaryData(data);
			}

			if(data is ScenarioDS)
			{
				// Update the scenario characteristics for this and all child CMs
				string cSIDStr=((ScenarioDS)data).CSID;
				string cLIDStr=((ScenarioDS)data).CLID;
				string cIIDStr=((ScenarioDS)data).CIID;
				string copiedCSIDStr=((ScenarioDS)data).CopiedCSID;

				Guid cLID=new Guid(cLIDStr);
				Guid cSID=new Guid(cSIDStr);
				Guid cIID=new Guid(cIIDStr);

				ILogicalModule logicalEntity=this.Broker.GetLogicalCM(cLID);
				CMScenario cMScenario=logicalEntity.GetScenario(cSID);

				if(null==cMScenario)
				{	
					//If we are copying a scenario then we need to set modified to false
					//as by default the CMBase deliver data method will set modified to true
					//However in the case of creating a new scenario then we do not want this 
					//to occur as no changes are being made to the CM.
					//this.Modified = false;

					// The case where a new scenario is being created
					Guid copiedCSID=new Guid(copiedCSIDStr);
					cMScenario = logicalEntity.CreateCopyScenario(
						((ScenarioDS)data).Name,
						cSID,
						((ScenarioDS)data).Type,
						copiedCSID,
						((ScenarioDS)data).Status,
						((ScenarioDS)data).CreationDateTime,
						((ScenarioDS)data).ModificationDateTime
						);
					logicalEntity.CurrentScenario = cMScenario.CSID; //Set the newly created scenario to be the current
				}
				else
				{
					cMScenario.Name=((ScenarioDS)data).Name;
					cMScenario.Type=((ScenarioDS)data).Type;
					//cMScenario.CreationDateTime=((ScenarioDS)data).CreationDateTime;
					cMScenario.ModificationDateTime=((ScenarioDS)data).ModificationDateTime;
					cMScenario.Status=((ScenarioDS)data).Status;
					//logicalEntity.Modified=true;
				}

				// need to recalculate the logical module token as we have
				// possibly changed the data in the logical module
				logicalEntity.CalculateToken( true );

				this.Broker.ReleaseBrokerManagedComponent(logicalEntity);
				this.status=((ScenarioDS)data).Status;
			}
		}
		#endregion

		private new void DeliverPrimaryData(DataSet data)
		{
		
		}
	
		public override DataSet MigrateDataSet(DataSet data)
		{
			DataSet baseDataset = base.MigrateDataSet(data);
			return baseDataset;
		}


		/// <summary>
		/// May be overridden by an extending class implementing a specific CM functionality.
		/// This function is called when the CM instance receives a subscription update from another
		/// CM instance internal to this CM instance.  The overriding function should implement
		/// the behavior required of this CM on receipt of messages from its child CM instances.
		/// Such behavior could include on-publishing to external CM instances.  To do this, the
		/// function should create an on-published message and call PublishMessage().
		/// </summary>
		/// <param name="message"></param>
		public virtual void OnInternalSubscriptionUpdate(IMessage message){}

		public virtual IsSubscriberType IsSubscriber(IMessage message, SubscriptionsList objSubscriptions, Guid subscriberCLID, Guid subscriberCSID)
		{
			return IsSubscriberType.Unknown;
		}

		#endregion

		#region OTHER MEMBER FUNCTIONS
		/// <summary>
		/// Access point for navigator object
		/// </summary>
		/// <param name="navigator"></param>
		void IBusinessStructureModule.PerformBusinessStructreNavigationOperation(IBusinessStructureNavigatorVisitor navigator)
		{		
		}
		private static bool GetLoggingEnabled()
		{
            return "ON" == ConfigurationManager.AppSettings["TX360ENTERPRISE_MESSAGE_LOGGING_MODE"];
		}

        public IOrganizationUnit GetParentReportingUnit()
        {
            ILogicalModule thisLogicalModule = this.GetLogicalModule();
            ILogicalModule periodLogicalModule = thisLogicalModule.ParentLCM;
            IPeriod parentPeriod = this.GetParentPeriod();

            ILogicalModule parentPeriodLogical = parentPeriod.GetLogicalModule();

            this.Broker.ReleaseBrokerManagedComponent(parentPeriod);

            ILogicalModule parentReportingUnit = parentPeriodLogical.ParentLCM;

            this.Broker.ReleaseBrokerManagedComponent(parentPeriodLogical);

            try
            {
                return (IOrganizationUnit)this.Broker.GetCMImplementation(parentReportingUnit.CLID, this.CSID);
            }
            finally
            {
                this.Broker.ReleaseBrokerManagedComponent(parentReportingUnit);
            }
        }

        public IPeriod GetParentPeriod()
        {
            ILogicalModule thisLogicalModule = this.GetLogicalModule();
            ILogicalModule periodLogicalModule = thisLogicalModule.ParentLCM;

            this.Broker.ReleaseBrokerManagedComponent(thisLogicalModule);

            try
            {
                return (IPeriod)this.Broker.GetCMImplementation(periodLogicalModule.CLID, this.CSID);
            }
            finally
            {
                this.Broker.ReleaseBrokerManagedComponent(periodLogicalModule);
            }
        }

		private static string GetLogPath()
		{
            return ConfigurationManager.AppSettings["TX360ENTERPRISE_MESSAGE_LOGGING_PATH"];
		}

		/// <summary>
		/// Publish a message to all child CM instances and also treat as an internal subscription
		/// update for the purposes of this CM itself.
		/// The CM, having received an internal publication may then choose whether or not to on-publish
		/// it externally.
		/// </summary>
		/// <param name="message">The message containing the subscription update.</param>

		public void PostMessage(IMessage message)
		{
			DeliverMessage(message);

			this.OnInternalSubscriptionUpdate(message);
		}

		public virtual void DeliverMessage(IMessage message)
		{
			string dataType=(string)message["DataType"];

			ILogicalModule myLCM=this.Broker.GetLogicalCM(this.CLID);

			for( int iCount=0; iCount < myLCM.ChildCLIDs.Count; iCount++ )
			{
				Guid childCLID = (Guid) myLCM.ChildCLIDs[ iCount ];

				if(message.InterScenario || message.MSID==Guid.Empty)
				{
					ILogicalModule childLCM=this.Broker.GetLogicalCM(childCLID);

					foreach(DictionaryEntry dictionaryEntry in childLCM.Scenarios)  // Consider all scenarios of the subscriber
					{
						CMScenario cMScenario=(CMScenario)dictionaryEntry.Value;
						this.DeliverToCM(childCLID, cMScenario.CSID, myLCM, message);
					
						if ( this.MessageDeliveredDelegate != null )
							this.MessageDeliveredDelegate( message, myLCM, childLCM, cMScenario.CSID );
					}

					this.Broker.ReleaseBrokerManagedComponent(childLCM);
				}
				else //This is not an interscenario message, so only check any children of the same CSID for subscriptions
				{
					this.DeliverToCM(childCLID,this.CSID,myLCM,message);
				
					if ( this.MessageDeliveredDelegate != null )
					{
						ILogicalModule childLCM=this.Broker.GetLogicalCM(childCLID);
						this.MessageDeliveredDelegate( message, myLCM, childLCM, this.CSID );
						this.Broker.ReleaseBrokerManagedComponent(childLCM);
					}
				
				}
			}

			this.Broker.ReleaseBrokerManagedComponent(myLCM);
		}

		protected void DeliverToCM(Guid childCLID, Guid gdCSID, ILogicalModule myLCM, IMessage objMessage)
		{
			ICalculationModule childCM = null;
			if(!objMessage.IsPublisher(childCLID) && myLCM.IsSubscriber(this,objMessage,childCLID,gdCSID,objMessage.MSID))
			{
				childCM=this.Broker.GetCMImplementation(childCLID,gdCSID);

				if(childCM != null)
					childCM.SetSubscriptionData(objMessage);

				this.Broker.ReleaseBrokerManagedComponent(childCM);
			}
		}

		public virtual bool FilterMessage(IMessage message,Object[] parameters)
		{
			return true;
		}

		private void WriteLogMessageLogLine(string lineText)
		{
			try
			{
				if(messageLoggingOn)
				{
					FileStream logStream=new FileStream(logPath,FileMode.Append, FileAccess.Write, FileShare.ReadWrite);
					StreamWriter logWriter=new StreamWriter(logStream);
					logWriter.WriteLine(lineText);
					logWriter.Close();
					logStream.Close();
				}
			}
			catch(Exception ex)
			{
				string message=ex.Message;
			}
		}
		#endregion
	}
}
