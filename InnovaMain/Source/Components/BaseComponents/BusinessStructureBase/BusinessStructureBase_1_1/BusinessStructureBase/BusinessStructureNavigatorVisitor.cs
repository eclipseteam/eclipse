using System;
using System.Collections;

using Oritax.TaxSimp.BusinessStructureUtilities;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.IOrganizationUnits;
using Oritax.TaxSimp.CM.Entity;
using Oritax.TaxSimp.CM.Group;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CM.Period;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.Exceptions;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.Calculation
{	
	/// <summary>
	/// Summary description for BusinessStructureNavigatorVisitor.
	/// </summary>
	public abstract class BusinessStructureNavigatorVisitor : IBusinessStructureNavigatorVisitor
	{
		#region Private variables------------------------------------------------------------------
		private ICMBroker brokerInstance; 
		private BusinessStrucutureNavigatorLevel lowestNavigationLevel = BusinessStrucutureNavigatorLevel.None;  
		private Guid [] reportingUnitIDs = new Guid []{}; 
		private string[] calculationModuleTypes = new String[]{}; 
		private PeriodSpecifier periodDate = new PeriodSpecifier(); 	
		private bool continuePushDownOperation = false; 
		private Hashtable strutureExceptionList = new Hashtable();
		private Hashtable groupMembers = new Hashtable();
		private int groupLevelsNavigated = 0;
		private BusinessStructureNavigationDirection navigationDirection = BusinessStructureNavigationDirection.DownAndAcross;
		#endregion 

		#region Public properties------------------------------------------------------------------
		/// <summary>
		/// Gets or sets Broker
		/// </summary>
		public ICMBroker BrokerInstance
		{
			get
			{
				return this.brokerInstance; 
			}
		}
		/// <summary>
		/// returns ans sets exception list
		/// </summary>
		public Hashtable StrutureExceptionList
		{
			get
			{
				return this.strutureExceptionList; 
			}
			set
			{
				this.strutureExceptionList = value;
			}
		}

		/// <summary>
		/// Get group members as hashtable
		/// </summary>
		protected Hashtable GroupMembers 
		{
			get
			{
				return this.groupMembers;
			}
			set
			{
				this.groupMembers = value; 
			}
		}

		/// <summary>
		/// Sets Lowest level in strcuture
		/// </summary>
		protected BusinessStrucutureNavigatorLevel LowestNavigationLevel
		{
			get
			{
				return this.lowestNavigationLevel;
			}
		}
		
		/// <summary>
		/// Sets value to continue push down through group. Needs to public as is accessed in Administration solution. 
		/// </summary>
		public bool ContinuePushDownOperation
		{
			get
			{
				return this.continuePushDownOperation;
			}
			set
			{
				this.continuePushDownOperation = value;
			}
		}

		/// <summary>
		/// Gets or Sets reporting unit IDS for members 
		/// </summary>
		protected Guid [] ReportingUnitIDs
		{
			get
			{
				return this.reportingUnitIDs;
			}
		}

		
		/// <summary>
		/// Gets and sets the list of calculation module types to visit
		/// for example: "AtoForms" will ensure visiting such modules as  AtoForms_1_1, AtoForms_2_1, etc.
		/// "AustralianITL", "TaxEffectAccounting", "Ledger_1_1"
		/// </summary>
		protected string[] CalculationModuleTypes
		{
			get
			{
				return this.calculationModuleTypes;
			}
			set
			{
				calculationModuleTypes = value;
			}		
		}

		/// <summary>
		/// Gets or sets PeriodDate. 
		/// </summary>
		protected PeriodSpecifier PeriodDate
		{
			get
			{
				return this.periodDate ;
			}
			set
			{
				this.periodDate = value;
			}

		}

		/// <summary>
		/// Indicates the number of group levels that were navigated to get to this point
		/// </summary>
		protected int GroupLevelsNavigated
		{
			get
			{
				return this.groupLevelsNavigated;
			}
		}

		/// <summary>
		/// Indicates the direction of travel through the structure that the visitor takes
		/// </summary>
		public BusinessStructureNavigationDirection NavigationDirection
		{
			get
			{
				return this.navigationDirection;
			}
		}
		#endregion 

		#region Default constructor----------------------------------------------------------------
	
		/// <summary>
		/// Constructor passed with Broker instance, Period start date and end date.  
		/// </summary>
		/// <param name="BrokerInstance"></param>
		/// <param name="periodStartDate"></param>
		/// <param name="periodEndDate"></param>
		public BusinessStructureNavigatorVisitor(ICMBroker BrokerInstance,DateTime periodStartDate, DateTime periodEndDate) 
		{
			this.brokerInstance = BrokerInstance;
			this.periodDate.StartDate = periodStartDate;
			this.periodDate.EndDate = periodEndDate;
		}
		
		/// <summary>
		/// Constructor passed with Broker instance. 
		/// </summary>
		/// <param name="BrokerInstance"></param>
		public BusinessStructureNavigatorVisitor(ICMBroker BrokerInstance) 
		{
			this.brokerInstance = BrokerInstance;
		}

		/// <summary>
		/// Constructor passed with Broker instance, Period start date, end date and navigation direction
		/// </summary>
		/// <param name="BrokerInstance"></param>
		/// <param name="periodStartDate"></param>
		/// <param name="periodEndDate"></param>
		/// <param name="navigationDirection"></param>
		public BusinessStructureNavigatorVisitor(ICMBroker BrokerInstance, DateTime periodStartDate, DateTime periodEndDate, BusinessStructureNavigationDirection navigationDirection)
		{
			this.brokerInstance = BrokerInstance;
			this.periodDate.StartDate = periodStartDate;
			this.periodDate.EndDate = periodEndDate;
			this.navigationDirection = navigationDirection;
		}

		/// <summary>
		/// Constructor passed with Broker instance and navigation direction
		/// </summary>
		/// <param name="BrokerInstance"></param>
		/// <param name="navigationDirection"></param>
		public BusinessStructureNavigatorVisitor(ICMBroker BrokerInstance, BusinessStructureNavigationDirection navigationDirection)
		{
			this.brokerInstance = BrokerInstance;
			this.navigationDirection = navigationDirection;
		}
		#endregion 

		#region Virtual methods--------------------------------------------------------------------
		
		/// <summary>
		/// Visits organisation node. Implementaion is not required for ATO MIGRATION PUSH DOWN. 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		public virtual void VisitOrganisationNode(object sender, OrganisationNodeEventArgs e)
		{
			
		}

		/// <summary>
		/// The following method visit's group object. 
		/// 1. It validates business structure and add information to Exception list. 
		/// 2. It navigates period(s). 
		/// 3. It navigates to member(s). 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		public virtual void VisitGroupNode(object sender, GroupNodeEventArgs e)
		{
			
		}

		/// <summary>
		/// Visits Entity node. 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		public virtual void VisitEntityNode(object sender, EntityNodeEventArgs e)
		{
			
		}

		/// <summary>
		/// Visits period object. 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		public virtual void VisitPeriodNode(object sender, PeriodNodeEventArgs e)
		{
			
		}
		
		/// <summary>
		/// The following method visits Calculation node. 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		public virtual void VisitCalculationNode(object sender, CalculationModuleNodeEventArgs e)
		{
		
		}

		/// <summary>
		/// Check to see which level is Navigator at. 
		/// </summary>
		/// <param name="currentLevel"></param>
		/// <returns></returns>
		public virtual bool ShouldNavigateToLowerLevels(BusinessStrucutureNavigatorLevel currentLevel)
		{
			return true; 
		}

		/// <summary>
		/// The following method returns true/false. It checks whether object of reporting unit type is not in exception list e.g. 
		/// Entity A is member of one or more groups or it has wrong ATO configured. If a reporting unit had wrong ATO configured 
		/// it will not visit that object. 
		/// </summary>
		/// <param name="reportingUnitCLID"></param>
		/// <param name="reportingUnitCSID"></param>
		/// <returns></returns>
		public virtual bool ShouldVisitReportingUnitNode(Guid reportingUnitCLID, Guid reportingUnitCSID)
		{
			return true; 
		}
		
		/// <summary>
		/// Checks if it needs to visit period object
		/// </summary>
		/// <param name="periodDate"></param>
		/// <returns></returns>
		public virtual bool ShouldVisitPeriodNode(PeriodSpecifier periodDate)
		{
			return true; 
		}

		/// <summary>
		/// Check is navigator should visit calculation module node.
		/// </summary>
		/// <param name="typeName">type of the calculation node</param>
		/// <returns>false, if there is a predefined list of CalculationModuleTypes and the module is not in the list
		/// otherwise, returns true</returns>
		public virtual bool ShouldVisitCalculationModule(string typeName)
		{
			if (calculationModuleTypes.Length > 0)
			{
				foreach( string module in calculationModuleTypes)
				{
					if (typeName.StartsWith(module))
						return true;
				}
				return false;
			}
			else 
				return true; 
		}
		
		/// <summary>
		/// Gets reporting unit name. 
		/// </summary>
		/// <param name="CSID"></param>
		/// <param name="CLID"></param>
		/// <returns></returns>
		protected string GetMemberName(Guid CSID, Guid CLID)
		{
			ILogicalModule lCM = this.BrokerInstance.GetLogicalCM(CLID);
			ICalculationModule memberCM = lCM[CSID];

			try
			{
				return memberCM.Name;
			}
			finally
			{
				this.brokerInstance.ReleaseBrokerManagedComponent(lCM);
				this.brokerInstance.ReleaseBrokerManagedComponent(memberCM);
			}
		}
		
		#endregion 

		#region Support functions------------------------------------------------------------------
		
		/// <summary>
		/// Add reporting unit name to exception list
		/// </summary>
		/// <param name="entityName"></param>
		protected void AddToExceptionList(string entityName)
		{
			// Find if entityName already in the the Exception list or not. Only add it if entityName does not exist.
			ArrayList reportingUnitDetails;
			if(!this.strutureExceptionList.Contains(entityName))
			{
				reportingUnitDetails = new ArrayList();
				reportingUnitDetails.Add(entityName); 
				//more information can be added and later object is added Hashtable
				this.strutureExceptionList.Add(entityName,reportingUnitDetails);
			}
		}

		/// <summary>
		/// Increments the number of group levels that have been visited at this point
		/// in the structure
		/// </summary>
		public void IncrementGroupLevelsVisited()
		{
			this.groupLevelsNavigated++;
		}

		/// <summary>
		/// Decrements the number of group levels that have been visited at this point
		/// in the structure
		/// </summary>
		public void DecrementGroupLevelsVisited()
		{
			this.groupLevelsNavigated--;
		}
		#endregion
	}
}
