using System;
using Oritax.TaxSimp.CalculationInterface;

namespace Oritax.TaxSimp.Calculation
{
	public delegate void OnMessageBeingDeliveredDelagate( IMessage objMessage, ILogicalModule objModuleDelivering, ILogicalModule objModuleReceiving, Guid objReceivingScenario );

	public struct MessageDeliveryPackage
	{
		private IMessage m_objMessage;
		private ILogicalModule m_objModuleDelivering;
		private ILogicalModule m_objModuleReceiving;
		private Guid m_objReceivingScenario;

		public MessageDeliveryPackage( IMessage objMessage, ILogicalModule objModuleDelivering, ILogicalModule objModuleReceiving, Guid objReceivingScenario )
		{
			this.m_objMessage = objMessage;
			this.m_objModuleDelivering = objModuleDelivering;
			this.m_objModuleReceiving = objModuleReceiving;
			this.m_objReceivingScenario = objReceivingScenario;
		}

		public IMessage Message
		{
			get
			{
				return this.m_objMessage;
			}
		}

		public ILogicalModule ModuleDelivering
		{
			get
			{
				return this.m_objModuleDelivering;
			}
		}

		public ILogicalModule ModuleReceiving
		{
			get
			{
				return this.m_objModuleReceiving;
			}
		}

		public Guid ReceivingScenario
		{
			get
			{
				return this.m_objReceivingScenario;
			}
		}
	}
}
