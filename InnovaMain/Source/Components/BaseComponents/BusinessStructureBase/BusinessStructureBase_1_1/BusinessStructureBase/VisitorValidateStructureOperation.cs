using System;
using System.Data;
using System.Collections;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.BusinessStructureUtilities;
using Oritax.TaxSimp.Exceptions;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.Group;
using Oritax.TaxSimp.CM.Period;
using Oritax.TaxSimp.CM.Entity;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CM.OrganizationUnit;


namespace Oritax.TaxSimp.Calculation
{
	/// <summary>
	/// Summary description for VisitorValidateStructureOperation.
	/// </summary>
	public class VisitorValidatePushDownOperation : BusinessStructureNavigatorVisitor
	{
		#region Constant---------------------------------------------------------------------------

		#endregion
	
		#region Private variables------------------------------------------------------------------
		private DataRow [] selectedCMs = new DataRow[]{}; 
		private Hashtable configuredCMExceptionList = new Hashtable();
	
		private bool userConfirmedMigration = false;

		#endregion 
	
		#region Public properties------------------------------------------------------------------
		/// <summary>
		/// returns ans sets exception list
		/// </summary>
		public Hashtable ConfiguredCMExceptionList
		{
			get
			{
				return this.configuredCMExceptionList; 
			}
			set
			{
				this.configuredCMExceptionList = value;
			}
		}
		#endregion
	
		#region Overrideble Properties and methods-------------------------------------------------
		/// <summary>
		/// The following method returns true/false. It checks whether object of reporting unit type is not in exception list e.g. 
		/// Entity A is member of one or more groups or it has wrong ATO configured. If a reporting unit had wrong ATO configured 
		/// it will not visit that object. 
		/// </summary>
		/// <param name="reportingUnitCLID"></param>
		/// <param name="reportingUnitCSID"></param>
		/// <returns></returns>
		public override bool ShouldVisitReportingUnitNode(Guid reportingUnitCLID, Guid reportingUnitCSID)
		{
			return true;
		}
		/// <summary>
		/// The following method checks if navigator object should visit period node. The check is to make sure that navigator is only
		/// visiting objects period which are of same dates. 
		/// </summary>
		/// <param name="periodDate"></param>
		/// <returns></returns>
		public override bool ShouldVisitPeriodNode(PeriodSpecifier periodDate)
		{
			return (periodDate.EndDate == this.PeriodDate.EndDate && periodDate.StartDate == this.PeriodDate.StartDate);
		}

		/// <summary>
		/// The following method will always return true, implementation was not required at this point for ATO MIGRATION PUSH DOWN
		/// </summary>
		/// <param name="typeName"></param>
		/// <returns></returns>
		public override bool ShouldVisitCalculationModule(string typeName)
		{
			return true; 
		}

		/// <summary>
		/// The following method visit's group object. 
		/// 1. It validates business structure and add information to Exception list. 
		/// 2. It navigates period(s). 
		/// 3. It navigates to member(s). 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		public override void VisitGroupNode(object sender, GroupNodeEventArgs e)
		{	
			if(!this.GroupMembers.Contains(this.GetName(e.CSID,e.CLID)))
				this.GroupMembers.Add(this.GetName(e.CSID,e.CLID),this.GetName(e.CSID,e.CLID)); 
			else
				this.StrutureExceptionList.Add(this.GetName(e.CSID,e.CLID),this.GetName(e.CSID,e.CLID));
		}
		
		/// <summary>
		/// Visits Entity node. In this instance if it is already in Members list then there is any exception in strucuture
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		public override void VisitEntityNode(object sender, EntityNodeEventArgs e)
		{
			if(!this.GroupMembers.Contains(this.GetName(e.CSID,e.CLID)))
				this.GroupMembers.Add(this.GetName(e.CSID,e.CLID),this.GetName(e.CSID,e.CLID)); 
			else
				this.StrutureExceptionList.Add(this.GetName(e.CSID,e.CLID),this.GetName(e.CSID,e.CLID));
		}

		/// <summary>
		/// Visits period object. 
		/// 1. In this instance migrates ATO FORMS 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></par   am>    
		public override void VisitPeriodNode(object sender, PeriodNodeEventArgs e)
		{
			string reportingUnitName = this.GetName(e.CLID); 
			string cmException = e.Period.CheckForExistingComponentException(this.selectedCMs);
			if(cmException != String.Empty && !this.configuredCMExceptionList.Contains(reportingUnitName))
				this.configuredCMExceptionList.Add(reportingUnitName, cmException);
		}

		#endregion 
		
		#region Support methods--------------------------------------------------------------------
		/// <summary>
		/// If user has confirmed to proceed with migration process.
		/// </summary>
		public bool UserConfirmedMigration
		{
			get
			{
				return this.userConfirmedMigration;
			}
			set
			{
				this.userConfirmedMigration = value;  
			}
		}

		/// <summary>
		/// Default constructor. 
		/// </summary>
		/// <param name="BrokerInstance"></param>
		/// <param name="selectedCMs"></param>
		/// <param name="startTime"></param>
		/// <param name="endTime"></param>
		public VisitorValidatePushDownOperation(ICMBroker BrokerInstance,DataRow [] selectedCMs,DateTime startTime, DateTime endTime):base(BrokerInstance,startTime, endTime)
		{
			this.selectedCMs = selectedCMs;
		}

        /// <summary>
		/// Checks if reporting unit is not in exception list. 
		/// </summary>
		/// <param name="CLID"></param>
		/// <param name="CSID"></param>
		/// <returns></returns>
		private bool IsInExceptionList(Guid CLID, Guid CSID)
		{
			return (this.configuredCMExceptionList.Contains(GetMemberName(CSID,CLID)));
		}

		/// <summary>
		/// Validates group structure. 
		/// 1. Checks there is no duplication of reporting units. 
		/// 2. Checks wrong ATO CM is not configured.
		/// </summary>
		/// <param name="e"></param>
		/// <param name="CLID"></param>
		/// <param name="CSID"></param>
		private void ValidateGroupStruture(GroupNodeEventArgs e, Guid CLID, Guid CSID)
		{
			this.GroupMembers.Clear(); 
			this.configuredCMExceptionList.Clear();
			this.StrutureExceptionList.Clear(); 

			if(!this.GroupMembers.Contains(GetName(e.CSID,e.CLID)))
				this.GroupMembers.Add(GetName(e.CSID,e.CLID),GetName(e.CSID,e.CLID));
			else
				this.AddToExceptionList(GetName(e.CSID,e.CLID));
		}
		/// <summary>
		/// Returns reporting unit name as string
		/// </summary>
		/// <param name="CSID"></param>
		/// <param name="CLID"></param>
		/// <returns></returns>
		private string GetName(Guid CSID, Guid CLID)
		{
			ILogicalModule lCM = this.BrokerInstance.GetLogicalCM(CLID);
			ICalculationModule memberCM = lCM[CSID];

			try
			{
				return memberCM.Name;
			}
			finally
			{
				this.BrokerInstance.ReleaseBrokerManagedComponent(lCM);
				this.BrokerInstance.ReleaseBrokerManagedComponent(memberCM);
			}
		}

		/// <summary>
		/// Returns reporting unit name as a string
		/// </summary>
		/// <returns></returns>
		private string GetName(Guid CLID)
		{
			if (CLID != Guid.Empty)
			{
				ILogicalModule logicalModule = this.BrokerInstance.GetLogicalCM(CLID) as ILogicalModule;

				if (logicalModule != null)
				{
					ILogicalModule reportingUnitLM = logicalModule.ParentLCM;

					if (reportingUnitLM != null)
						return reportingUnitLM.Name;

					this.BrokerInstance.ReleaseBrokerManagedComponent(reportingUnitLM);
				}

				this.BrokerInstance.ReleaseBrokerManagedComponent(logicalModule);
			}
		return "";
		}
		#endregion 

	}
}
