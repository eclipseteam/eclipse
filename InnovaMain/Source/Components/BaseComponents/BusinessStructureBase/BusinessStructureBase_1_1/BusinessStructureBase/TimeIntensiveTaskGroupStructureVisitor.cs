using System;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using Oritax.TaxSimp.BusinessStructureUtilities;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Exceptions;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.Calculation
{
    public class TimeIntensiveTaskGroupStructureVisitor : BusinessStructureNavigatorVisitor
    {
        /// <summary>
        /// The maximum number of reporting units in a structure before the operation becomes
        /// a time intensive task
        /// </summary>
        private const int MaximumReportingUnitCount = 20;

        /// <summary>
        /// The maximum depth of groups in the structure before the operation becomes
        /// a time intensive task 
        /// </summary>
        private int MaximumGroupLevelCount = 3;
        
        /// <summary>
        /// The maximum time (in seconds) that the users should wait to see how big the
        /// structure is.
        /// </summary>
        private static readonly int MaximumCheckWaitPeriodSeconds;

        private DateTime checkExpiryTime = DateTime.Now.AddSeconds(MaximumCheckWaitPeriodSeconds);

        private int reportingUnitCount = 0;

        private string userConfirmationValue = "TimeIntensiveTaskConfirmation";

        static TimeIntensiveTaskGroupStructureVisitor()
        {
            string setting = ConfigurationManager.AppSettings["NableTimeIntensiveTaskMaxWaitTimeSeconds"];

            if (!String.IsNullOrEmpty(setting))
                MaximumCheckWaitPeriodSeconds = Int32.Parse(setting);
            else
                MaximumCheckWaitPeriodSeconds = 4;
        }

        public TimeIntensiveTaskGroupStructureVisitor(ICMBroker broker, DateTime periodStartDate, DateTime periodEndDate, BusinessStructureNavigationDirection navigationDirection) : base(broker, periodStartDate, periodEndDate, navigationDirection)
        {
        }

        public TimeIntensiveTaskGroupStructureVisitor(ICMBroker broker, DateTime periodStartDate, DateTime periodEndDate, BusinessStructureNavigationDirection navigationDirection, string userConfirmationValue)
            : base(broker, periodStartDate, periodEndDate, navigationDirection)
        {
            this.userConfirmationValue = userConfirmationValue;
        }

        public TimeIntensiveTaskGroupStructureVisitor(ICMBroker broker, BusinessStructureNavigationDirection navigationDirection)
            : base(broker, navigationDirection)
        {
        }

        public TimeIntensiveTaskGroupStructureVisitor(ICMBroker broker, BusinessStructureNavigationDirection navigationDirection, string userConfirmationValue)
            : base(broker, navigationDirection)
        {
            this.userConfirmationValue = userConfirmationValue;
        }

        public override bool ShouldNavigateToLowerLevels(BusinessStrucutureNavigatorLevel currentLevel)
        {
            if (this.NavigationDirection == BusinessStructureNavigationDirection.DownAndAcross)
                return (currentLevel == BusinessStrucutureNavigatorLevel.Group);
            else
                return true;
        }

        public override bool ShouldVisitPeriodNode(PeriodSpecifier periodDate)
        {
            if (this.NavigationDirection == BusinessStructureNavigationDirection.DownAndAcross)
                return false;
            else
            {
                if (periodDate.StartDate == this.PeriodDate.StartDate &&
                   periodDate.EndDate == this.PeriodDate.EndDate)
                {
                    return true;
                }
                else
                    return false;
            }
        }

        public override bool ShouldVisitCalculationModule(string typeName)
        {
            return false;
        }

        public override void VisitEntityNode(object sender, EntityNodeEventArgs e)
        {
            reportingUnitCount++;
            this.CheckStructureWithinParameters();
        }

        public override void VisitGroupNode(object sender, GroupNodeEventArgs e)
        {
            reportingUnitCount++;
            this.CheckStructureWithinParameters();
        }

        private void CheckStructureWithinParameters()
        {
            if (reportingUnitCount >= MaximumReportingUnitCount ||
               base.GroupLevelsNavigated >= MaximumGroupLevelCount ||
               DateTime.Now >= checkExpiryTime)
            {
                throw new GetUserConfirmationException(new MessageBoxDefinition(MessageBoxDefinition.MSG_CONFIRM_TIMEINTENSIVETASK), userConfirmationValue, true);
            }
        }
    }
}
