using System;
using System.Collections;
using System.Data;

using Oritax.TaxSimp.BusinessStructureUtilities;
using Oritax.TaxSimp.Exceptions;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.Group;
using Oritax.TaxSimp.CM.Period;
using Oritax.TaxSimp.CM.Entity;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CM.OrganizationUnit;

namespace Oritax.TaxSimp.Calculation
{
	/// <summary>
	/// The following enum used to get reportingUnitDetails type
	/// </summary>
	public enum ReportingUnitDetails 
	{
		Name,
		CLID, 
		CSID,
		Period
	}
	/// <summary>
	/// Summary description for ATOFormPushdownOperation.
	/// Following class is derived from BusinessStructureNavigatorVisitor. 
	/// </summary>
	
	public class VisitorPushdownOperation : BusinessStructureNavigatorVisitor
	{
		#region Constant---------------------------------------------------------------------------

		public const string ATOPUSHDOWN_CHKKEY = "6156D89F-C348-4214-88AB-A010274338D9";
		public const string ATOEXPCONTINUE = "ATOEXPCONTINUE";
		

		#endregion
	
		#region Private variables------------------------------------------------------------------
		private Hashtable configuredCMExceptionList = new Hashtable();
		private DataRow [] selectedCMs = new DataRow[]{}; 
		private bool userConfirmedMigration = false;
		private Hashtable migrated = new Hashtable();

		#endregion 
		
		#region Public properties------------------------------------------------------------------
		/// <summary>
		/// returns ans sets exception list
		/// </summary>
		public Hashtable ConfiguredCMExceptionList
		{
			get
			{
				return this.configuredCMExceptionList; 
			}
			set
			{
				this.configuredCMExceptionList = value;
			}
		}

		/// <summary>
		/// Returns and sets Migrated CM list
		/// </summary>
		public Hashtable Migrated
		{
			get
			{
				return this.migrated; 
			}
			set
			{
				this.migrated = value;
			}
		}

		#endregion 
		
		#region Overrideble Properties and methods-------------------------------------------------
		/// <summary>
		/// The following method returns true/false. It checks whether object of reporting unit type is not in exception list e.g. 
		/// Entity A is member of one or more groups or it has wrong ATO configured. If a reporting unit had wrong ATO configured 
		/// it will not visit that object. 
		/// </summary>
		/// <param name="reportingUnitCLID"></param>
		/// <param name="reportingUnitCSID"></param>
		/// <returns></returns>
		public override bool ShouldVisitReportingUnitNode(Guid reportingUnitCLID, Guid reportingUnitCSID)
		{
			return !IsInExceptionList(reportingUnitCLID, reportingUnitCSID); 
		}

		/// <summary>
		/// The following method will always return true, implementation was not required at this point for ATO MIGRATION PUSH DOWN
		/// </summary>
		/// <param name="typeName"></param>
		/// <returns></returns>
		public override bool ShouldVisitCalculationModule(string typeName)
		{
			return true; 
		
		}

		/// <summary>
		/// The following method visit's group object. 
		/// 1. It adds information in Migrated hashtable that reporting unit has already been visited
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		public override void VisitGroupNode(object sender, GroupNodeEventArgs e)
		{	
			string reportingUnitName = GetMemberName(e.CSID,e.CLID); 
			if(!this.migrated.Contains(reportingUnitName))
				migrated.Add(reportingUnitName,reportingUnitName); 
		}
		/// <summary>
		/// The following method visit's entity object. 
		/// 1. It adds information in Migrated hashtable that reporting unit has already been visited
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		
		public override void VisitEntityNode(object sender, EntityNodeEventArgs e)
		{
			string reportingUnitName = GetMemberName(e.CSID,e.CLID); 
			if(!this.migrated.Contains(reportingUnitName))
				migrated.Add(reportingUnitName,reportingUnitName); 
		}
		/// <summary>
		/// Check to see which level is Navigator at. 
		/// </summary>
		/// <param name="currentLevel"></param>
		/// <returns></returns>
	
		public override bool ShouldNavigateToLowerLevels(BusinessStrucutureNavigatorLevel currentLevel)
		{
			return (currentLevel != BusinessStrucutureNavigatorLevel.Period);
		}
		
		/// <summary>
		/// The following method checks if navigator object should visit period node. The check is to make sure that navigator is only
		/// visiting objects period which are of same dates. 
		/// </summary>
		/// <param name="periodDate"></param>
		/// <returns></returns>
		public override bool ShouldVisitPeriodNode(PeriodSpecifier periodDate)
		{
			return (periodDate.EndDate == this.PeriodDate.EndDate && periodDate.StartDate == this.PeriodDate.StartDate);
		}
		
		/// <summary>
		/// Visits period object. 
		/// 1. In this instance migrates ATO FORMS 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		public override void VisitPeriodNode(object sender, PeriodNodeEventArgs e)
		{
			e.Period.MigrateATOFormsPushDown(this.selectedCMs, true);  
		}
		#endregion 

		#region Support methods--------------------------------------------------------------------
		/// <summary>
		/// If user has confirmed to proceed with migration process.
		/// </summary>
		public bool UserConfirmedMigration
		{
			get
			{
				return this.userConfirmedMigration;
			}
			set
			{
				this.userConfirmedMigration = value;  
			}
		}

		/// <summary>
		/// Default constructor. 
		/// </summary>
		/// <param name="BrokerInstance"></param>
		/// <param name="startTime"></param>
		/// <param name="endTime"></param>
		/// <param name="selectedCMs"></param>
		public VisitorPushdownOperation(ICMBroker BrokerInstance, DateTime startTime, DateTime endTime,DataRow [] selectedCMs):base(BrokerInstance,startTime, endTime)
		{
			this.selectedCMs = selectedCMs;
		}

		/// <summary>
		/// Checks if reporting unit is not in exception list. 
		/// </summary>
		/// <param name="CLID"></param>
		/// <param name="CSID"></param>
		/// <returns></returns>
		private bool IsInExceptionList(Guid CLID, Guid CSID)
		{
			return (this.configuredCMExceptionList.Contains(GetMemberName(CSID,CLID)) || this.migrated.Contains(GetMemberName(CSID,CLID)));
		}

		/// <summary>
		/// Validates group structure. 
		/// 1. Checks there is no duplication of reporting units. 
		/// 2. Checks wrong ATO CM is not configured.
		/// </summary>
		public void ValidateGroupStruture()
		{
		}

	#endregion 
	}
}
