using System;
using System.Data;
using Oritax.TaxSimp.WPDatasetBase;

namespace Oritax.TaxSimp.Calculation
{
	/// <summary>
	/// Summary description for BusinessStructureWorkpaperDS.
	/// </summary>
	[Serializable]
	public class BusinessStructureWorkpaperDS: WPDatasetBaseDS
	{
		public string Username;
		
		public const String USERCONTEXT_TABLE				= "USERCONTEXT_TABLE";

		public const String REPORTINGUNITNAME_FIELD			= "REPORTINGUNITNAME_FIELD";
		public const String REPORTINGUNITCLID_FIELD			= "REPORTINGUNITCLID_FIELD";

		public const String SCENARIONAME_FIELD				= "SCENARIONAME_FIELD";
		public const String SCENARIOCSID_FIELD				= "SCENARIOCSID_FIELD";

		public const String PERIODNAME_FIELD				= "PERIODNAME_FIELD";
		public const String PERIODCLID_FIELD				= "PERIODCLID_FIELD";

		public BusinessStructureWorkpaperDS()
		{
			DataTable dtContext;

			dtContext = new DataTable( USERCONTEXT_TABLE );

			dtContext.Columns.Add( REPORTINGUNITNAME_FIELD, typeof( System.String) );
			dtContext.Columns.Add( REPORTINGUNITCLID_FIELD, typeof( System.Guid ) );
			
			dtContext.Columns.Add( SCENARIONAME_FIELD, typeof( System.String) );
			dtContext.Columns.Add( SCENARIOCSID_FIELD, typeof( System.Guid ) );
			
			dtContext.Columns.Add( PERIODNAME_FIELD, typeof( System.String) );
			dtContext.Columns.Add( PERIODCLID_FIELD, typeof( System.Guid ) );
			
			this.Tables.Add( dtContext );
		}

		#region Properties to access user context
		public Guid ReportingUnitCLID
		{
			get
			{
				if(this.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows.Count == 1)
				{
					return (Guid)this.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows[0][BusinessStructureWorkpaperDS.REPORTINGUNITCLID_FIELD];
				}
				else
					return Guid.Empty;
			}
			set
			{
				if(this.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows.Count == 1)
					this.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows[0][BusinessStructureWorkpaperDS.REPORTINGUNITCLID_FIELD] = value;

			}
		}

		public String ReportingUnitName
		{
			get
			{
				if(this.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows.Count == 1)
				{
					return this.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows[0][BusinessStructureWorkpaperDS.REPORTINGUNITNAME_FIELD].ToString();
				}
				else
					return String.Empty;
			}
			set
			{
				if(this.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows.Count == 1)
					this.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows[0][BusinessStructureWorkpaperDS.REPORTINGUNITNAME_FIELD] = value;

			}
		}

		public Guid ScenarioCSID
		{
			get
			{
				if(this.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows.Count == 1)
				{
					return (Guid)this.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows[0][BusinessStructureWorkpaperDS.SCENARIOCSID_FIELD];
				}
				else
					return Guid.Empty;
			}
			set
			{
				if(this.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows.Count == 1)
					this.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows[0][BusinessStructureWorkpaperDS.SCENARIOCSID_FIELD] = value;

			}
		}

		public String ScenarioName
		{
			get
			{
				if(this.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows.Count == 1)
				{
					return this.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows[0][BusinessStructureWorkpaperDS.SCENARIONAME_FIELD].ToString();
				}
				else
					return String.Empty;
			}
			set
			{
				if(this.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows.Count == 1)
					this.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows[0][BusinessStructureWorkpaperDS.SCENARIONAME_FIELD] = value;

			}
		}

		public Guid PeriodCLID
		{
			get
			{
				if(this.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows.Count == 1)
				{
					return (Guid)this.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows[0][BusinessStructureWorkpaperDS.PERIODCLID_FIELD];
				}
				else
					return Guid.Empty;
			}
			set
			{
				if(this.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows.Count == 1)
					this.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows[0][BusinessStructureWorkpaperDS.PERIODCLID_FIELD] = value;

			}
		}

		public String PeriodName
		{
			get
			{
				if(this.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows.Count == 1)
				{
					return this.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows[0][BusinessStructureWorkpaperDS.PERIODNAME_FIELD].ToString();
				}
				else
					return String.Empty;
			}
			set
			{
				if(this.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows.Count == 1)
					this.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows[0][BusinessStructureWorkpaperDS.PERIODNAME_FIELD] = value;

			}
		}

		#endregion
	}
}
