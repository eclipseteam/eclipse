using System;
using System.Data;
using Oritax.TaxSimp.CalculationInterface;

namespace Oritax.TaxSimp.Calculation
{
	/// <summary>
	/// DSCalculationModule is a DataSet that contains all of the persistent data for a CM instance.
	/// </summary>
	[Serializable] 
	public class BusinessStructureBaseDS : BrokerManagedComponentDS
	{
		public BusinessStructureBaseDS() : base()
		{

		}
	}
}

