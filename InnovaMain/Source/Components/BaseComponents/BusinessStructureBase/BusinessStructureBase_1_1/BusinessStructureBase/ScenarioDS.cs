using System;
using System.Data;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.BusinessStructureUtilities;

namespace Oritax.TaxSimp.Calculation
{
	/// <summary>
	/// DSCalculationModule is a DataSet that contains all of the persistent data for a CM instance.
	/// </summary>
	[Serializable] 
	public class ScenarioDS : BusinessStructureWorkpaperDS
	{
		string			cSID;
		string			copiedCSID;
		string			cIID;
		string			cLID;
		string			name;
		ScenarioType	type;
		bool			readOnly;
		ScenarioStatus	status;
		DateTime		creationDateTime;
		DateTime		modificationDateTime;
		string			m_strBreadCrumb;
		bool			copyScenario;
		private Guid	m_objParentGroupScenarioID;
		bool			m_blCreateInMembers;
		bool			locked=false;
		bool			parentLocked=false;

		public ScenarioDS()
		{
			name=String.Empty;
		}

		public ScenarioDS(ScenarioDS original)
			: base()
		{
			this.cSID=original.CSID;
			this.cIID=original.CIID;
			this.cLID=original.CLID;
			this.name=original.Name;
			this.type=original.Type;
			this.readOnly=original.ReadOnly;
			this.status=original.Status;
			this.creationDateTime=original.CreationDateTime;
			this.modificationDateTime=original.ModificationDateTime;
			this.m_objParentGroupScenarioID = original.m_objParentGroupScenarioID;
			this.locked=original.locked;
			this.parentLocked=original.parentLocked;
		}

		public string CSID {get{return cSID;}set{cSID=value;}}
		public string CopiedCSID {get{return copiedCSID;}set{copiedCSID=value;}}
		public string CIID	{get{return cIID;}set{cIID=value;}}
		public string CLID	{get{return cLID;}set{cLID=value;}}
		public string Name{get{return name;}set{name=value;}}
		public ScenarioType Type{get{return type;}set{type=value;}}
		public bool ReadOnly{get{return readOnly;}set{readOnly=value;}}
		public ScenarioStatus Status{get{return status;}set{status=value;}}
		public DateTime CreationDateTime{get{return creationDateTime;}set{creationDateTime=value;}}
		public DateTime ModificationDateTime{get{return modificationDateTime;}set{modificationDateTime=value;}}
		public string BreadCrumb{get{return this.m_strBreadCrumb;}set{this.m_strBreadCrumb = value;}}
		public bool CopyScenario{get{return copyScenario;}set{copyScenario=value;}}
		public bool CreateInMembers{get{return m_blCreateInMembers;}set{m_blCreateInMembers=value;}}
		public bool Locked{get{return locked;}set{locked=value;}}
		public bool ParentLocked{get{return parentLocked;}set{parentLocked=value;}}

		public Guid ParentGroupScenarioID
		{
			get
			{
				return m_objParentGroupScenarioID;
			}
			set
			{
				m_objParentGroupScenarioID = value;
			}
		}
	}
}

