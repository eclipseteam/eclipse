using System;
using Oritax.TaxSimp.CalculationInterface;

namespace Oritax.TaxSimp.Calculation
{
	/// <summary>
	/// Summary description for BusinessStructureElement.
	/// </summary>
	public class BusinessStructureElement
	{
		/// <summary>
		/// Default constructor
		/// </summary>
		public BusinessStructureElement()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		/// <summary>
		/// Common method used across derived classes for navigator object.  
		/// </summary>
		/// <param name="navigator"></param>
		public void PerformBusinessStructreNavigationOperation(IBusinessStructureNavigatorVisitor navigator)
		{

		}
	}
}
