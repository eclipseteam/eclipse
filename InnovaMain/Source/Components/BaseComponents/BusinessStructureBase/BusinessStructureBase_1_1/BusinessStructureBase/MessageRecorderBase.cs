using System;
using System.Collections;
using Oritax.TaxSimp.CalculationInterface;

namespace Oritax.TaxSimp.Calculation
{
	/// <summary>
	/// 
	/// </summary>
	public class MessageRecorderBase
	{
		ArrayList m_objMessagesSent = new ArrayList( );
		bool m_bolRecording = false;

		public MessageRecorderBase( BusinessStructureBase objBSCM )
		{
			objBSCM.MessageDeliveredDelegate += new OnMessageBeingDeliveredDelagate( this.MessageDelivered );
		}

		public void StartRecording( )
		{
			this.m_bolRecording = true;
		}

		public void StopRecording( )
		{
			this.m_bolRecording = false;
		}

		public void ResetRecording( )
		{
			this.m_objMessagesSent.Clear( );
		}

		private void MessageDelivered( IMessage objMessage, ILogicalModule objModuleDelivering, ILogicalModule objModuleReceiving, Guid objReceivingScenario )
		{
			if ( m_bolRecording )
			{
				MessageDeliveryPackage objPackage = new MessageDeliveryPackage(
													objMessage, objModuleDelivering,
													objModuleReceiving, objReceivingScenario );
				this.m_objMessagesSent.Add( objPackage );
			}
		}

		public ArrayList MessagesSent
		{
			get
			{
				return this.m_objMessagesSent;
			}
		}
	}
}
