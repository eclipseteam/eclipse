using System;

namespace Oritax.TaxSimp.Calculation
{
	/// <summary>
	/// Summary description for Install.
	/// </summary>
	public class BusinessStructureBaseInstall
	{
		#region INSTALLATION PROPERTIES

		// Assembly Installation Properties
		public const string ASSEMBLY_ID="00A63172-134C-47f5-989F-DE61972AED48";
		public const string ASSEMBLY_NAME="BusinessStructureBase_1_1";
		public const string ASSEMBLY_DISPLAYNAME="BusinessStructureBase V1.1";
		public const string ASSEMBLY_MAJORVERSION="1";
		public const string ASSEMBLY_MINORVERSION="1";
		public const string ASSEMBLY_DATAFORMAT="0";
//		public const string ASSEMBLY_REVISION="0"; 
		public const string ASSEMBLY_REVISION="1"; //2005.1 

		#endregion
	}
}
