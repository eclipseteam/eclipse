﻿using Oritax.TaxSimp.VelocityCache;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

namespace VelocityTest
{


    /// <summary>
    ///This is a test class for VelocityDataCacheWithRegionTest and is intended
    ///to contain all VelocityDataCacheWithRegionTest Unit Tests
    ///</summary>
    [TestClass()]
    public class VelocityDataCacheWithRegionTest
    {
        private const string VELOCITY_REGION_NAME = "UnitTestRegion";

        private VelocityDataCacheWithRegion _DataCache
        {
            get
            {
                VelocityDataCacheWithRegion dataCache;
                LocalDataStoreSlot _VelocitySlot = Thread.GetNamedDataSlot(VelocityConfiguration.VELOCITY_THREAD_SLOT_NAME);

                if (_VelocitySlot == null)
                    _VelocitySlot = Thread.AllocateNamedDataSlot(VelocityConfiguration.VELOCITY_THREAD_SLOT_NAME);

                dataCache = (VelocityDataCacheWithRegion)Thread.GetData(_VelocitySlot);

                if (dataCache == null)
                {
                    dataCache = new VelocityDataCacheWithRegion(VELOCITY_REGION_NAME);
                    Thread.SetData(_VelocitySlot, dataCache);
                }

                return dataCache;
            }
        }


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        [TestMethod()]
        public void ConcurrencyTest()
        {
            VelocityDataCacheWithRegion cache1 = new VelocityDataCacheWithRegion(VELOCITY_REGION_NAME);
            VelocityDataCacheWithRegion cahce2 = new VelocityDataCacheWithRegion(VELOCITY_REGION_NAME);

            Guid id = Guid.NewGuid();
            Random r = new Random();
            int obj1 = r.Next();
            int obj2 = r.Next();
            int obj3 = r.Next();

            bool actual1 = cache1.PutObject(id, obj1);
            bool actual2 = cahce2.PutObject(id, obj2);
            bool actual3 = cache1.PutObject(id, obj3);

            Assert.AreEqual(actual1, true);
            Assert.AreEqual(actual2, false);
            Assert.AreEqual(actual3, true, "Third update should fail as of version mismatch.");
        }


        /// <summary>
        ///A test for GetCollectionList
        ///</summary>
        [TestMethod()]
        public void GetCollectionListTest()
        {
            _DataCache.RemoveAll();

            Dictionary<Guid, int> localDict = new Dictionary<Guid, int>();
            for (int i = 0; i < 100; i++)
            {
                Guid id = Guid.NewGuid();
                int obj = (new Random()).Next();

                localDict.Add(id, obj);
                this._DataCache.PutObject(id, obj);
            }

            Hashtable actual = _DataCache.GetCollectionList();

            foreach (KeyValuePair<Guid, int> kv in localDict)
            {
                Assert.AreEqual(kv.Value, this._DataCache.GetObejct(kv.Key));
            }
        }

        /// <summary>
        ///A test for GetObejct
        ///</summary>
        [TestMethod()]
        public void GetObejctTest()
        {
            Guid id = Guid.NewGuid();
            Random r = new Random();

            int expected = r.Next();
            int actual;

            _DataCache.PutObject(id, expected);
            actual = (int)_DataCache.GetObejct(id);

            Assert.AreEqual(expected, actual);

        }

        /// <summary>
        ///A test for PutObject
        ///</summary>
        [TestMethod()]
        public void PutObjectTest()
        {
            Guid id = Guid.NewGuid();
            Random r = new Random();

            int obj = r.Next();
            bool expected = true;
            bool actual;

            actual = _DataCache.PutObject(id, obj);

            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for RemoveAll
        ///</summary>
        [TestMethod()]
        public void RemoveAllTest()
        {

            this._DataCache.RemoveAll();
            Assert.AreEqual(0, this.GetEntryCount(), "There still are some entries after RemoveAll().");
        }

        /// <summary>
        ///A test for Remove
        ///</summary>
        [TestMethod()]
        public void RemoveTest()
        {
            Guid id = Guid.NewGuid();
            Random r = new Random();

            int originalCount = this.GetEntryCount();

            _DataCache.PutObject(id, new object());
            int CountAfterPut = this.GetEntryCount();

            Assert.AreEqual(originalCount + 1, CountAfterPut, "Number didn't increase after put!");

            this._DataCache.Remove(id);
            int CountAfterRemove = this.GetEntryCount();

            Assert.AreEqual(originalCount, CountAfterRemove, "Number didn't increase after put!");


        }

        private int GetEntryCount()
        {
            Hashtable ht = _DataCache.GetCollectionList();
            return ht.Count;
        }
    }
}
