﻿using Oritax.TaxSimp.VelocityCache;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading;
using Microsoft.Data.Caching;

namespace VelocityTest
{


    /// <summary>
    ///This is a test class for VelocityDataCacheTest and is intended
    ///to contain all VelocityDataCacheTest Unit Tests
    ///</summary>
    [TestClass()]
    public class VelocityDataCacheTest
    {
        private VelocityDataCache _DataCache
        {
            get
            {
                VelocityDataCache dataCache;
                LocalDataStoreSlot _VelocitySlot = Thread.GetNamedDataSlot(VelocityConfiguration.VELOCITY_THREAD_SLOT_NAME);

                if (_VelocitySlot == null)
                    _VelocitySlot = Thread.AllocateNamedDataSlot(VelocityConfiguration.VELOCITY_THREAD_SLOT_NAME);

                dataCache = (VelocityDataCache)Thread.GetData(_VelocitySlot);

                if (dataCache == null)
                {
                    dataCache = new VelocityDataCache();
                    Thread.SetData(_VelocitySlot, dataCache);
                }

                return dataCache;
            }
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        [TestInitialize()]
        public void MyTestInitialize()
        {
        }
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        [TestMethod()]
        public void PutObjectInNutShell()
        {
            Guid id = Guid.NewGuid();
            Random r = new Random();
            int obj = r.Next();

            DataCacheServerEndpoint[] servers = new DataCacheServerEndpoint[3];

            servers[0] = new DataCacheServerEndpoint("10.222.100.151", 22233, "DistributedCacheService");
            servers[1] = new DataCacheServerEndpoint("10.222.100.152", 22233, "DistributedCacheService");
            servers[2] = new DataCacheServerEndpoint("10.222.100.153", 22233, "DistributedCacheService");

            DataCacheFactory cacheFactory = new DataCacheFactory(servers, true, false, DataCacheLocalCacheSyncPolicy.NotificationBased, 300, 300);

            // get cache client for cache "FirstCache"
            DataCache _DC = cacheFactory.GetCache("VelocityCache");

            _DC.Put(id.ToString(), obj);
        }



        /// <summary>
        ///A test for PutObject
        ///</summary>
        [TestMethod()]
        public void PutObjectTest()
        {
            Guid id = Guid.NewGuid();
            Random r = new Random();

            int obj = r.Next();
            bool expected = true;
            bool actual;

            actual = _DataCache.PutObject(id, obj);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void ResetVelocityLock()
        {
            DataCacheServerEndpoint[] servers = new DataCacheServerEndpoint[3];

            servers[0] = new DataCacheServerEndpoint("10.222.100.151", 22233, "DistributedCacheService");
            servers[1] = new DataCacheServerEndpoint("10.222.100.152", 22233, "DistributedCacheService");
            servers[2] = new DataCacheServerEndpoint("10.222.100.153", 22233, "DistributedCacheService");

            DataCacheFactory cacheFactory = new DataCacheFactory(servers, true, false, DataCacheLocalCacheSyncPolicy.NotificationBased, 300, 300);

            // get cache client for cache "FirstCache"
            DataCache _DC = cacheFactory.GetCache("VelocityCache");

            _DC.Put("VelocityLock", 0);
        }

        [TestMethod()]
        public void GetObjectTest()
        {
            Guid id = Guid.NewGuid();
            Random r = new Random();

            int expected = r.Next();
            int actual;

            _DataCache.PutObject(id, expected);
            actual = (int)_DataCache.GetObejct(id);

            Assert.AreEqual(expected, actual);


        }

        [TestMethod()]
        public void RemoveObjectTest()
        {
            Guid id = Guid.NewGuid();
            Random r = new Random();

            int expected = r.Next();

            _DataCache.PutObject(id, expected);
            _DataCache.RemoveObject(id);

            object actual = _DataCache.GetObejct(id);
            Assert.AreEqual(actual, null);
        }

        [TestMethod()]
        public void ConcurrencyTest()
        {
            VelocityDataCache cache1 = new VelocityDataCache();
            VelocityDataCache cahce2 = new VelocityDataCache();

            Guid id = Guid.NewGuid();
            Random r = new Random();
            int obj1 = r.Next();
            int obj2 = r.Next();
            int obj3 = r.Next();

            bool actual1 = cache1.PutObject(id, obj1);
            int obj1Acutal = (int)cache1.GetObejct(id);

            bool actual2 = cahce2.PutObject(id, obj2);
            int obj2Acutal = (int)cahce2.GetObejct(id);

            bool actual3 = cache1.PutObject(id, obj3);
            int obj3Acutal = (int)cache1.GetObejct(id);


            Assert.AreEqual(actual1, true);
            Assert.AreEqual(obj1Acutal, obj1);

            Assert.AreEqual(actual2, false);
            Assert.AreNotEqual(obj2Acutal, obj2);

            Assert.AreEqual(actual3, true, "Third update should fail as of version mismatch.");
            Assert.AreEqual(obj3Acutal, obj3);
        }

    }
}
