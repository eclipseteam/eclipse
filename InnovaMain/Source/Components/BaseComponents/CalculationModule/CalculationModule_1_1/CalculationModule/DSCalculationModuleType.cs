using System;
using System.Data;

namespace Oritax.TaxSimp.DSCalculation
{
	/// <summary>
	/// DSCalculationModule is a DataSet that contains all of the persistent data for a CM instance.
	/// </summary>
	[System.ComponentModel.DesignerCategory("Code")]
	[SerializableAttribute] 
	public class DSCalculationModuleType : DataSet
	{
		public const String TYPE_TABLE					= "TYPE_TABLE";
		public const String CMTYPEID_FIELD				= "CMTYPEID";
		public const String CMASSEMBLY_FIELD			= "ASSEMBLY";
		public const String CMTYPE_FIELD				= "TYPE";
		public const String CMTYPENAME_FIELD			= "TYPENAME";
		public const String CMCATEGORYNAME_FIELD		= "CATEGORYNAME";
		public const String CMAPPLICABILITY_FIELD		= "APPLICABILITY";	
		public const String CMTYPEDISPLAYNAME_FIELD		= "DISPLAYNAME";
		public const String CMMAJORVERSION_FIELD		= "MAJORVERSION";
		public const String CMMINORVERSION_FIELD		= "MINORVERSION";
		public const String CMRELEASE_FIELD				= "RELEASE";
	
		// SJD 5/2/2002 Added pAssembly and pClass 
		public const String CMPASSEMBLY_FIELD			= "PASSEMBLY";
		public const String CMPCLASS_FIELD				= "PCLASS";


		public DSCalculationModuleType()
		{
			DataTable table;
			DataColumnCollection columns;

			// Add the table for the instance data
			table = new DataTable(TYPE_TABLE);
			columns = table.Columns;
			columns.Add(CMTYPEID_FIELD, typeof(System.Guid));
			columns.Add(CMTYPENAME_FIELD, typeof(System.String));
			columns.Add(CMTYPEDISPLAYNAME_FIELD, typeof(System.String));
			columns.Add(CMCATEGORYNAME_FIELD, typeof(System.String));
			columns.Add(CMAPPLICABILITY_FIELD, typeof(System.Int32));
			columns.Add(CMASSEMBLY_FIELD, typeof(System.String));
			columns.Add(CMTYPE_FIELD, typeof(System.String));
			columns.Add(CMMAJORVERSION_FIELD, typeof(System.Int32));
			columns.Add(CMMINORVERSION_FIELD, typeof(System.Int32));
			columns.Add(CMRELEASE_FIELD, typeof(System.Int32));
			
			// SJD 5/2/2002 Added pAssembly and pClass 
			columns.Add(CMPASSEMBLY_FIELD, typeof(String));
			columns.Add(CMPCLASS_FIELD, typeof(String));

			table.PrimaryKey = new DataColumn[] { columns[CMTYPEID_FIELD] };
			this.Tables.Add(table);
		}
		
		// SJD 5/2/2002 Added pAssembly and pClass 
		public DSCalculationModuleType(	Guid iD,
										string typeName,
										string displayName,
										string category,
										int applicability,
										string assemblyName,
										string className,
										int majorVersion,
										int minorVersion,
										int release,	
										String passembly,
										String pclass)
			: this()
		{
			this.Tables[DSCalculationModuleType.TYPE_TABLE].Rows.Add(new object[]
										{iD,typeName,displayName,category,applicability,assemblyName,className,majorVersion,minorVersion,release, passembly, pclass});
		}
	}
}
