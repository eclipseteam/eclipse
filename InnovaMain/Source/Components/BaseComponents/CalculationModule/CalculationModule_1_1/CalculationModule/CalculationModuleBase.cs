using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data;
using System.IO;
using System.Configuration;
using System.Reflection;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Threading;
using System.Text.RegularExpressions;

using Oritax.TaxSimp.CalculationInterface;

using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.BusinessStructureUtilities;
using Oritax.TaxSimp.CM.Period;
using Oritax.TaxSimp.WPDatasetBase;
using Oritax.TaxSimp.Security;
using Oritax.TaxSimp.Exceptions;
using System.Collections.Generic;

namespace Oritax.TaxSimp.Calculation
{
    public enum ActivitySettings
    {
        Show = 0,// CM includes AA funcationality 
        Hide	// Do not show workpaper in the tree view, do not publish amounts - different to unconfigure, because it retains data
    }

    /// <summary>
    /// CalculationModuleBase is the base class for all Calculation Modules.
    /// </summary>
    [Serializable]
    public class CalculationModuleBase : BrokerManagedComponent, ICalculationModule
    {
        #region INSTALLATION PROPERITES
        public override string InstallTypeID { get { return "2A491827-11DB-4b3b-8C08-AFA071F3F6BF"; } }
        public override string InstallTypeName { get { return "CalculationModuleBase"; } }
        public override string InstallDisplayName { get { return "Calculation Module Base"; } }
        public override string InstallCategory { get { return "Framework"; } }
        public override string InstallAssemblyName { get { return "BrokerManagedComponentBase"; } }
        public override string InstallClassName { get { return "CalculationModuleBase"; } }
        public override int InstallMajVersion { get { return Assembly.GetExecutingAssembly().GetName().Version.Major; } }
        public override int InstallMinVersion { get { return Assembly.GetExecutingAssembly().GetName().Version.Minor; } }
        public override int InstallRelease { get { return Assembly.GetExecutingAssembly().GetName().Version.Revision; } }
        public override string InstallPersisterAssembly { get { return "CalculationPersistSQL"; } }
        public override string InstallPersisterClass { get { return "BlobPersister"; } }
        #endregion

        #region INTERNAL CLASSES
        public class StaleCMReferenceException : Exception
        {
            public StaleCMReferenceException() : base("The reference used to access the CM Scenario Implementation is stale.") { }
        }
        #endregion

        #region FIELD VARIABLES

        //USING CUSTOM SERIALIZATION.
        //Still mark the fields as [NonSerialized] for your own infomation but this is ignored now by .NET
        //You must add your own serialzation code for each instance
        //See GetObjectData & Special Constructor


        // CM Non-Persistent Data
        protected ICalculationModule parentInstance;
        protected StreamControlType m_objCurrentStreamControlType = StreamControlType.Update;
        protected StreamControlType m_objPriorStreamControlType = StreamControlType.Update;
        protected Stack m_objOverriddenStreamControlType = new Stack();
        protected bool m_bolHasInheritedStreamControlType = false;
        protected bool m_bolIsOverriddenStreamControlType = false;
        protected bool cmIsJoining = false;
        protected bool cmIsLeaving = false;

        /// <summary>
        /// remembers workpackage being published
        /// Should be reset immediately after publishing or when received another package
        /// (in the middle of package publishing)
        /// to avoid circular reference
        /// </summary>
        [NonSerialized] protected ArrayList WorkPackageSent = null; 
        [NonSerialized] public bool PublishInProgress = false;
        [NonSerialized] public bool ProcessingPackage = false;
        [NonSerialized] protected bool cmChanged = false;

        // CM Persistent Data
        protected Guid cmLogicalGuid;		// The unique identifier of the logical CM that this CM implements
        protected ScenarioStatus status;			// Draft or final
        private BusinessStructureContext m_objContext;
        private TaxYear m_objTaxYear;
        private FinancialYear m_objFinancialYear;
        protected ActivitySettings activeStatus = ActivitySettings.Show;
        private bool stale;
        private bool scenarioLockOverridden;
        #endregion

        #region PROPERTIES
        public virtual IEnumerable WorkPapers { get { return new ArrayList(); } }
        public virtual IEnumerable PrintableWorkPapers { get { return this.WorkPapers; } }

        /// <summary>
        /// Returns an array of DataSet derived type objects that represents all of the DataSet interface
        /// types supported by the CM.
        /// </summary>

        public virtual MapTargetCollection MapTargetCollection { get { return new MapTargetCollection(); } }
        public virtual string EditWorkPaper { get { return "CMEdit.aspx"; } }
        public virtual string OpenWorkPaper { get { return "CMManager.aspx"; } }
        public virtual string ConfigureWPURL { get { return ""; } }

        public Guid CMID { get { return cID; } set { cID = value; } }
        public Guid CIID { get { return cID; } set { cID = value; } }
        public Guid CLID { get { return cmLogicalGuid; } set { cmLogicalGuid = value; } }
        public Guid CSID { 
            get { return cmScenarioGuid; } 
            set { cmScenarioGuid = value; } 
        }
        public override string Name
        {
            get { return name; }
            set
            {
                name = value;
            }
        }
        public virtual IEnumerable Subscriptions { get { return new ArrayList(); } }
        public ScenarioStatus Status { get { return status; } set { status = value; } }

        public StreamControlType CurrentStreamControlType
        {
            get
            {
                return this.m_objCurrentStreamControlType;
            }
        }

        public BusinessStructureContext CurrentBusinessStructureContext { get { return this.m_objContext; } set { this.m_objContext = value; } }

        public TaxYear TaxYear
        {
            get
            {
                if (this.m_objTaxYear == null)
                {
                    this.m_objTaxYear = this.GetTaxYear();
                }
                return this.m_objTaxYear;
            }
        }

        public FinancialYear FinancialYear
        {
            get
            {
                if (this.m_objFinancialYear == null)
                {
                    this.m_objFinancialYear = this.GetFinancialYear();
                }
                return this.m_objFinancialYear;
            }
        }

        public bool ScenarioLockOverridden
        {
            get { return scenarioLockOverridden; }
            set { scenarioLockOverridden = value; }
        }

        private Guid cmScenarioGuid = Guid.Empty;


        /// <summary>
        /// Indicates if the CM is joining a parent module
        /// </summary>
        public bool IsJoining
        {
            get
            {
                return this.cmIsJoining;
            }
        }
        public ActivitySettings ActiveStatus
        {
            get { return activeStatus; }
            set
            {
                //If there is currently not a prior period CM
                //and one has been added, then handle any actions
                //that need to take place.
                if (activeStatus != value)
                {
                    activeStatus = value;
                    cmChanged = true;
                    HandleActiveStatusChanged();
                }

                CalculateToken(true);
            }
        }
        /// <summary>
        /// Indicates if the CM is leaving a parent module
        /// </summary>
        public bool IsLeaving
        {
            get
            {
                return this.cmIsLeaving || (ActiveStatus == ActivitySettings.Hide);
            }
        }
        #endregion

        #region CONSTRUCTORS
        public CalculationModuleBase()
            : base()
        {
            status = ScenarioStatus.Draft;
        }

        protected CalculationModuleBase(SerializationInfo si, StreamingContext context)
            : base(si, context)
        {
            cmLogicalGuid = Serialize.GetSerializedGuid(si, "cmb_cmLogicalGuid");
            status = (ScenarioStatus)Serialize.GetSerializedValue(si, "cmb_status", typeof(ScenarioStatus), new ScenarioStatus(ScenarioStatus.Draft));
            m_objContext = (BusinessStructureContext)Serialize.GetSerializedValue(si, "cmb_m_objContext", typeof(BusinessStructureContext), null);
            m_objTaxYear = (TaxYear)Serialize.GetSerializedValue(si, "cmb_m_objTaxYear", typeof(TaxYear), null);
            m_objFinancialYear = (FinancialYear)Serialize.GetSerializedValue(si, "cmb_m_objFinancialYear", typeof(FinancialYear), null);
            activeStatus = (ActivitySettings)Serialize.GetSerializedInt32(si, "ttb_activeStatus");
        }

        //Only allow the .NET Serialization core code to call this function
        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo si, StreamingContext context)
        {
            base.GetObjectData(si, context);
            Serialize.AddSerializedValue(si, "cmb_cmLogicalGuid", cmLogicalGuid);
            Serialize.AddSerializedValue(si, "cmb_status", status);
            Serialize.AddSerializedValue(si, "cmb_m_objContext", m_objContext);
            Serialize.AddSerializedValue(si, "cmb_m_objTaxYear", m_objTaxYear);
            Serialize.AddSerializedValue(si, "cmb_m_objFinancialYear", m_objFinancialYear);
            Serialize.AddSerializedValue(si, "ttb_activeStatus", activeStatus);
        }

        #endregion

        #region OVERRIDABLE CM FUNCTIONS
        /// <summary>
        /// The method is used for navigator object
        /// </summary>
        /// <param name="navigator"></param>
        public void PerformBusinessStructreNavigationOperation(IBusinessStructureNavigatorVisitor navigator)
        {
            if (navigator.NavigationDirection == BusinessStructureNavigationDirection.UpAndAcross)
            {
                IPeriod parentPeriod = this.GetParentPeriod();
                ILogicalModule parentPeriodLM = parentPeriod.GetLogicalModule();

                List<CMScenario> childCMScenarios = parentPeriodLM.GetChildCMScenarios(this.CSID);

                foreach (CMScenario cmScenario in childCMScenarios)
                {
                    if (navigator.ShouldVisitCalculationModule(cmScenario.CMTypeName))
                    {
                        ICalculationModule calculationModule = this.Broker.GetCMImplementation(cmScenario.CLID, cmScenario.CSID);
                        navigator.VisitCalculationNode(this, new CalculationModuleNodeEventArgs(calculationModule));
                        this.Broker.ReleaseBrokerManagedComponent(calculationModule);
                    }
                }

                ((IBusinessStructureModule)parentPeriod).PerformBusinessStructreNavigationOperation(navigator);
            }
        }

        public override void Initialize(String name)
        {
            base.Initialize(name);

            this.CIID = Guid.NewGuid();
            this.stale = false;
        }

        public void SetAsStale()
        {
            this.stale = true;
        }

        public virtual string ConstructBreadcrumb()
        {
            try
            {
                ILogicalModule objThisLM = this.Broker.GetLogicalCM(this.CLID);

                if (objThisLM == null)
                    return "";

                ILogicalModule objParentLM = objThisLM.ParentLCM;

                this.Broker.ReleaseBrokerManagedComponent(objThisLM);

                if (objParentLM != null)
                {
                    ICalculationModule objParentCM = this.Broker.GetCMImplementation(objParentLM.CLID, this.CSID);

                    this.Broker.ReleaseBrokerManagedComponent(objParentLM);

                    if (objParentCM != null)
                    {
                        string strParentBreadcrumb = objParentCM.ConstructBreadcrumb();

                        this.Broker.ReleaseBrokerManagedComponent(objParentCM);

                        if (strParentBreadcrumb != String.Empty)
                            return strParentBreadcrumb;
                    }
                }
                return this.name;
            }
            catch
            {
                return "";
            }
        }

        public virtual void UpdateConfiguration()
        {
        }

        protected void OnGetData(WPDatasetBaseDS objData)
        {
            DataTable objBreadcrumbTable = objData.Tables[WPDatasetBaseDS.BREADCRUMBTABLE];
            DataRow objBreadcrumbRow = objBreadcrumbTable.NewRow();
            // make this breadcrumb the real breadcrumb when the context stuff has been sorted out
            objBreadcrumbRow[WPDatasetBaseDS.BREADCRUMBFIELD] = this.ConstructBreadcrumb();
            objBreadcrumbTable.Rows.Add(objBreadcrumbRow);

            objData.NotesCount = this.NotesCount;
            objData.AttachmentsCount = this.AttachmentsCount;

            DataTable notesDetailsTable = objData.Tables[WPDatasetBaseDS.NOTESDETAILSTABLE];

            foreach (DictionaryEntry noteDE in this.notes)
            {
                NoteItem note = (NoteItem)noteDE.Value;

                DataRow noteDetails = notesDetailsTable.NewRow();
                noteDetails[WPDatasetBaseDS.NOTESNAMEFIELD] = note.Description;
                noteDetails[WPDatasetBaseDS.NOTESWORKPAPERIDFIELD] = note.WorkpaperID;
                notesDetailsTable.Rows.Add(noteDetails);
            }
        }

        public virtual void OnWPPost(IWorkPaper wp) { }
        public virtual void PopulateWP(IWorkPaper wp) { }

        public override void SetData(DataSet data)
        {
            if (stale && !MigrationInProgress)
            {
                throw new StaleCMReferenceException();
            }

            this.ScenarioLockOverridden = false;

            base.SetData(data);

            if (this.Modified && !this.ScenarioLockOverridden && !MigrationInProgress && (null != this.Broker && this.IsScenarioLocked()))
            {
                throw new ScenarioLockException();
            }

            if (this.Modified && this.Broker != null && !MigrationInProgress)
            {
                ILogicalModule iLM = this.Broker.GetLogicalCM(this.CLID);

                iLM.DivergeScenario(this);

                this.Broker.ReleaseBrokerManagedComponent(iLM);
            }
        }


        public virtual void SetSubscriptionData(IMessage message)
        {
            this.CheckIfObjectIsInvolvedInCurrentTransaction();

            if (stale)
            {
                throw new StaleCMReferenceException();
            }

            ModifiedState modifiedState = OnSetSubscriptionData(message);

            if (ModifiedState.MODIFIED == modifiedState || ModifiedState.UNKNOWN == modifiedState)
            {
                CalculateToken(true);
            }

            if (this.Modified && !this.ScenarioLockOverridden && (null != this.Broker && this.IsScenarioLocked()))
            {
                throw new ScenarioLockException();
            }

            if (this.Modified)
            {
                ILogicalModule iLM = this.Broker.GetLogicalCM(this.CLID);

                iLM.DivergeScenario(this);

                this.Broker.ReleaseBrokerManagedComponent(iLM);
            }
        }


        protected virtual ModifiedState OnSetSubscriptionData(IMessage message)
        {
            return ModifiedState.UNCHANGED;
        }

        public override void OnCopyInstance()
        {
            Guid csid = this.CSID;
            base.OnCopyInstance();
            this.CSID = csid;
        }

        /// <summary>
        /// Perform any actions for when an the activeStatus
        /// for a cm is changed
        /// </summary>
        public virtual void HandleActiveStatusChanged()
        {
            if (ProcessingPackage)
                return;
            PublishAll();
        }
        public override void GetData(DataSet data)
        {
            if (stale)
            {
                throw new StaleCMReferenceException();
            }

            base.GetData(data);
        }

        protected override void OnGetData(DataSet data)
        {
            base.OnGetData(data);

            if (data is WPDatasetBaseDS)
            {
                this.OnGetData((WPDatasetBaseDS)data);
            }
        }





        public virtual void PerformCalculation() { }

        /// <summary>
        /// This member function is called by the framework when the CM is added to
        /// a calculation set, such as a Period.  It may be overridden by derived
        /// classes to provided functioality that occurs when a CM is first added to
        /// a set, such as sending out message to anounce its characteristics, or
        /// to poll to determine other CMs that are present.
        /// </summary>
        public virtual void OnJoinCalculationSet()
        {
            this.cmIsJoining = true;
            //At this stage no need to expand on publish all
            //PublishAll();
            this.cmIsJoining = false;
        }

        /// <summary>
        /// This member function is called by the default implementation of OnJoinCalculationSet().
        /// It should be overridden by extending classes to implement the publication of all their
        /// normally published messages.
        /// </summary>
        public virtual void PublishAll() { }

        /// <summary>
        /// SJD Created 3/3/2003 This member function publishes GL messages
        /// GL Enabled CMs should override it to to publish GL messages
        /// </summary>
        public virtual void PublishGL() { }

        /// <summary>
        /// Member function publishes any cross period messages.
        /// Should be overridden in any CM's that contain cross period messaging.
        /// </summary>
        public virtual void PublishCrossPeriod() { }

        /// <summary>
        /// Publish data changes when parent period start/end dates change
        /// </summary>
        public virtual void PublishOnPeriodDatesChange(DateTime startDate, DateTime endDate) { }


        /// <summary>
        /// Member function to publish stream end messages for cross period data.
        /// </summary>
        public virtual void PublishCrossPeriodStreamEnd()
        {
            this.OverrideStreamControlType(StreamControlType.End);
            this.PublishCrossPeriod();
            this.ResetOverrideStreamControlType();
        }
        /// <summary>
        /// This member function is called by the framework when the CM is removed
        /// from calculation set, such as a Period, or other parent CM.  It may be
        /// overridden by derived classes to provided functioality that occurs when
        /// a CM is removed, such as sending out a message to anounce its exit, so that
        /// other CMs may take account of its disappearance in their work papers.
        /// </summary>
        public virtual void OnLeaveCalculationSet()
        {
            this.cmIsLeaving = true;
            this.PublishAll();
        }
        /// <summary>
        /// To reset cmIsLeaving = false when operation OnLeaveCalculationSet is completed in 
        /// base and derived classes.
        /// </summary>
        public void EndLeaveCalculationSet()
        {
            this.cmIsLeaving = false;
        }
        /// <summary>
        /// Called after the calculation module is created in order for the calculation module to initialise any business structure context information
        /// </summary>
        public virtual void InitialiseBusinessStructureContext()
        {
            // no default implementation
        }

        /// <summary>
        /// Returns a bool to indicate that the lock status of the requested scenario for this CM.
        /// </summary>
        /// <returns>Returns true if the requested scenario is locked.</returns>
        public virtual bool IsScenarioLocked()
        {
            // Get the logical CM and determine if the specified scenario is locked
            ILogicalModule objThisLM = this.Broker.GetLogicalCM(this.CLID);

            try
            {
                if (objThisLM != null)
                    return objThisLM.IsScenarioLocked(this.cmScenarioGuid);
                else
                    return false;
            }
            finally
            {
                this.Broker.ReleaseBrokerManagedComponent(objThisLM);
            }
        }

        #endregion

        #region MIGRATION


        public override void Resubscribe()
        {
            base.Resubscribe();

            // All the subscriptions to the parent logical CM are removed and then re-created
            ILogicalModule iLCM = this.Broker.GetLogicalCM(this.CLID);
            ILogicalModule parentLCM = iLCM.ParentLCM;
            this.Broker.ReleaseBrokerManagedComponent(iLCM);

            if (parentLCM != null)
            {
                parentLCM.UnSubscribe(this.CLID, Guid.Empty, this.CSID, Guid.Empty);

                foreach (string dataType in this.Subscriptions)
                    parentLCM.Subscribe(dataType, this.CLID, this.CSID);

                this.Broker.ReleaseBrokerManagedComponent(parentLCM);
            }
        }


        #endregion

        #region OTHER MEMBER FUNCTIONS

        public void ChangeName(string newName)
        {
            name = newName;
            this.Modified = true;

            ILogicalModule iLM = this.Broker.GetLogicalCM(this.CLID);

            iLM.DivergeScenario(this);

            this.Broker.ReleaseBrokerManagedComponent(iLM);
        }

        //Controls a messages stream control depending on the condition being valid
        //as well as the state of the cmIsJoining and cmIsLeaving flags.
        //TODO: THis method is used by messages which are not Validation messages 
        //and should be renamed to something more generic (not done in 2004.3 due
        //to too late in cycle and would cause modifications to lots of TaxTopic Cm's)
        public void PublishValidationMessage(IMessage message, bool validCondition)
        {
            IMessage msg = GetValidationMessage(message, validCondition);
            if (msg != null)
                PublishMessage(msg);
        }
       
        public IMessage GetValidationMessage(IMessage message, bool validCondition)
        {
            if (message == null)
                throw new ArgumentNullException("message");
            else
                this.AddStreamControlInformation(message);

            message.Add("ValidCondition", validCondition);

            if (this.cmIsJoining)
            {
                if (!validCondition)
                    return message;
            }
            else if (this.IsLeaving)
                return message;
            else
            {
                if (validCondition)
                {
                    this.OverrideStreamControlType(StreamControlType.End);
                    this.AddStreamControlInformation(message);
                    this.ResetOverrideStreamControlType();
                    return message;
                }
                else
                    return message;
            }
            return null;
        }

        /// <summary>
        /// Publishes a message within the context of the CM instance.  That is, the message
        /// is delivered to the parent CM for transmission to all of this CM's sibling CM and
        /// further according to the programmed behavior of the parent CM.
        /// </summary>
        /// <param name="message"></param>
        public virtual void PublishMessage(IMessage message)
        {
            if (message == null)
                throw new ArgumentNullException("message");

            //If we are Migrating (or rehydrating a relationally persisted BMC) 
            //then we cannot publish as not all of the BMC
            //may not be created in the system.
            //Use the Override the OnMigrationCompleted to publish
            if (MigrationInProgress)
                return;

            this.AddStreamControlInformation(message);

            IBusinessStructureModule parentCM;
            ILogicalModule lcm = this.Broker.GetLogicalCM(this.CLID);

            if (lcm == null)
                throw new InvalidOperationException("Cannot publish message, unable to find the logical module associated with this CM");

            Guid parentCLID = lcm.ParentCLID;
            this.Broker.ReleaseBrokerManagedComponent(lcm);

            ILogicalModule parent = this.Broker.GetLogicalCM(parentCLID);

            if (parent == null)
                throw new InvalidOperationException("Cannot publish message, unable to locate the parent logical module for this CM");

            if (message.InterScenario)
                parentCM = (IBusinessStructureModule)parent[parent.CurrentScenario];
            else
                parentCM = (IBusinessStructureModule)parent[this.CSID];

            this.Broker.ReleaseBrokerManagedComponent(parent);

            NablePerformanceCounters.GetCounter("Message Count per Second").Increment();

            if (parentCM == null)
                throw new InvalidOperationException("Cannot publish message, unable to locate the parent CM for this CM");

            parentCM.PostMessage(message);

            this.Broker.ReleaseBrokerManagedComponent(parentCM);
        }

        /// <summary>
        /// Internal method to add stream control information to every message
        /// so that the receive knows what to do with the message.
        /// </summary>
        /// <param name="objMessage">The message being sent</param>
        public void AddStreamControlInformation(IMessage objMessage)
        {
            StreamControlType objStreamControlType = this.CurrentStreamControlType;

            // if the stream control has been inherited by this CM by receiving a message from
            // a cm that is ending and the message is an output from this CM, it is actually an
            // update message.
            if (this.m_bolHasInheritedStreamControlType && this.IsOutputMessage(objMessage))
                objStreamControlType = StreamControlType.Update;

            if (this.m_bolIsOverriddenStreamControlType)
                objStreamControlType = (StreamControlType)this.m_objOverriddenStreamControlType.Peek();

            // ignore any overrides if the cm is joining or leaving
            if (this.cmIsJoining)
                objStreamControlType = StreamControlType.Start;
            else if (this.IsLeaving)
                objStreamControlType = StreamControlType.End;
            if (!objMessage.Contains("StreamControl"))
                objMessage.Add("StreamControl", objStreamControlType);
            else
                objMessage["StreamControl"] = objStreamControlType;
        }

        /// <summary>
        /// Sets the current stream control setting based on the input message
        /// </summary>
        /// <param name="objMessage">The message to use to set the stream control</param>
        protected void SetInheritedStreamControl(IMessage objMessage)
        {
            BusinessStructureContext context = this.CurrentBusinessStructureContext;

            if (context.OrganisationUnitType == OrganisationUnitType.Entity &&
               context.Period == PeriodType.SinglePeriod)
            {
                this.m_objPriorStreamControlType = this.m_objCurrentStreamControlType;
                this.m_bolHasInheritedStreamControlType = true;

                if (objMessage.Contains("StreamControl"))
                    this.m_objCurrentStreamControlType = (StreamControlType)objMessage["StreamControl"];
            }
        }

        /// <summary>
        /// After a message has been processed, update the stream control type.
        /// </summary>
        protected void UnsetInheritedStreamControl()
        {
            if (m_bolHasInheritedStreamControlType)
            {
                this.m_objCurrentStreamControlType = this.m_objPriorStreamControlType;
                this.m_bolHasInheritedStreamControlType = false;
                this.m_objPriorStreamControlType = StreamControlType.Update;
            }
        }

        /// <summary>
        /// Override the stream control type for the current CM, where you want to send messages in a different state than the CM is in
        /// </summary>
        /// <param name="objStreamControl">The stream control type to set</param>
        public void OverrideStreamControlType(StreamControlType objStreamControl)
        {
            this.m_bolIsOverriddenStreamControlType = true;
            this.m_objOverriddenStreamControlType.Push(objStreamControl);
        }

        /// <summary>
        /// Remove a previously overriden stream control type
        /// </summary>
        public void ResetOverrideStreamControlType()
        {
            this.m_objOverriddenStreamControlType.Pop();

            if (this.m_objOverriddenStreamControlType.Count <= 0)
                this.m_bolIsOverriddenStreamControlType = false;
        }

        /// <summary>
        /// Returns true if the message is an output message for this CM
        /// </summary>
        /// <param name="objMessage">The message to check</param>
        /// <returns>true if the message is an output message from the CM, false otherwise</returns>
        public virtual bool IsOutputMessage(IMessage objMessage)
        {
            return false;
        }

        public DataSet CreateWPInterfaceClass(string className)
        {
            Type type = this.GetType();
            Assembly assembly = type.Assembly;
            return (DataSet)assembly.CreateInstance(className);
        }

        private TaxYear GetTaxYear()
        {
            IPeriod objParentPeriod = this.GetParentPeriod();

            try
            {
                if (objParentPeriod != null)
                    return objParentPeriod.GetTaxYear();
                else
                    return null;
            }
            finally
            {
                this.Broker.ReleaseBrokerManagedComponent(objParentPeriod);
            }
        }

        private FinancialYear GetFinancialYear()
        {
            IPeriod objParentPeriod = this.GetParentPeriod();

            if (objParentPeriod != null)
                return objParentPeriod.GetFinancialYear();
            else
                return null;
        }

        private IPeriod GetParentPeriod()
        {
            ILogicalModule objLogicalCM = this.Broker.GetLogicalCM(this.CLID);

            if (objLogicalCM != null)
            {
                ILogicalModule objLogicalPeriod = objLogicalCM.ParentLCM;
                this.Broker.ReleaseBrokerManagedComponent(objLogicalCM);

                try
                {
                    if (objLogicalPeriod != null)
                    {
                        ICalculationModule objParentCM = objLogicalPeriod[this.CSID];

                        try
                        {
                            if (objParentCM is IPeriod)
                                return (IPeriod)objLogicalPeriod[this.CSID];
                            else
                                return null;
                        }
                        finally
                        {
                            this.Broker.ReleaseBrokerManagedComponent(objParentCM);
                        }
                    }
                    else
                        return null;
                }
                finally
                {
                    this.Broker.ReleaseBrokerManagedComponent(objLogicalPeriod);
                }
            }
            else
                return null;
        }

        public ILogicalModule GetLogicalModule()
        {
            return this.Broker.GetLogicalCM(this.CLID);
        }

        /// <summary>
        /// Determine the privilege set for the current user to this CalculationModule. If no
        /// privilege is determined, recurse up the logical structure until specified privilege can
        /// be determined, or, the top of the structure is reached.
        /// </summary>
        /// <param name="privilege"></param>
        /// <param name="userCID"></param>
        /// <returns></returns>
        public override PrivilegeOption CheckPrivilege(PrivilegeType privilege, Guid userCID)
        {
            // Check the privilege for this CM - see: BrokerManageComponent.CheckPrivilege.
            PrivilegeOption objPrivilegeCheck = base.CheckPrivilege(privilege, userCID);

            if (objPrivilegeCheck == PrivilegeOption.Unspecified)
            {
                ILogicalModule objLogical = this.Broker.GetLogicalCM(this.CLID);
                // If no privilege has been determined for this CM instance, check if
                // a privilege has been set against the LogicalModule.
                objPrivilegeCheck = objLogical.CheckPrivilege(privilege, userCID);

                if (objPrivilegeCheck == PrivilegeOption.Unspecified)
                {
                    if (objLogical.ParentCLID != Guid.Empty)
                    {
                        // No privilege has been determined at this level, recurse up to next
                        // level in the logical structure and check privilege.
                        ICalculationModule objParent = this.Broker.GetCMImplementation(objLogical.ParentCLID, this.CSID);

                        this.Broker.ReleaseBrokerManagedComponent(objLogical);

                        if (objParent != null)
                            objPrivilegeCheck = objParent.CheckPrivilege(privilege, userCID);
                        else
                            objPrivilegeCheck = PrivilegeOption.Deny;

                        this.Broker.ReleaseBrokerManagedComponent(objParent);
                    }
                    else
                        objPrivilegeCheck = PrivilegeOption.Deny;
                }
            }

            return (objPrivilegeCheck);
        }


        /// <summary>
        /// Get link updated with current scenario CSID.
        /// Optionally add Csid parameter to the link if it was not included
        /// </summary>
        /// <param name="link">existing link</param>
        /// <param name="addCsid">whether to add Csid parameter to the link if it is not included originally</param>
        /// <returns>updated link</returns>
        public string GetCurrentScenarioUrl(string url, bool addCsid)
        {
            if (url == null)
                throw new ArgumentNullException("url can not be null");
            string newCsid = "CSID=" + this.CSID;
            string csidRegex = @"CSID=(\w+)-(\w+)-(\w+)-(\w+)-(\w+)";
            Regex regEx = new Regex(csidRegex);

            if (regEx.Match(url).Success)
                url = regEx.Replace(url, newCsid);
            else if (addCsid)
                url = url + "&" + newCsid;
            return url;
        }
        /// <summary>
        /// fix links to an incorrect scenario - use current scenario instead
        /// </summary>
        /// <param name="table">table containing links including CSID</param>
        /// <param name="column">column name</param>
        public void UpdateScenarioLinks(DataTable table, string column)
        {
            if (table == null)
                throw new ArgumentNullException("table can not be null");
            if (string.IsNullOrEmpty(column))
                throw new ArgumentNullException("column can not be null or empty");
            if (!table.Columns.Contains(column))
                throw new ArgumentException(String.Format("Table {0} does not contain column {1}", table, column));
            DataColumn col = table.Columns[column];
            if (col.DataType != typeof(string))
                throw new ArgumentException(String.Format("Column {0} type must be string", column));

            foreach (DataRow row in table.Rows)
            {
                if (row[column] != DBNull.Value)
                {
                    string url = (string)row[column];
                    if (url == "")
                        row[column] = url;
                    else
                        row[column] = GetCurrentScenarioUrl(url, true);
                }
            }
        }
        #endregion

        #region Message packaging - WorkPackage Methods

        protected virtual void PublishWorkPackage(string dataType, string key, ArrayList workPackage)
        {
            if (workPackage != null && workPackage.Count != 0)
            {
                WorkPackageSent = workPackage;

                int scope = 0;
                Message message = new Message(this, scope);
                message.Add("DataType", dataType);
                message.Add("Key", key);
                message.Add("MessagePackage", workPackage);
                PublishMessage(message);

                WorkPackageSent.Clear();
            }
        }
        public virtual ModifiedState ProcessWorkPackageData(IMessage objMessage)
        {
            ArrayList workPackage = (ArrayList)objMessage["MessagePackage"];
            if (workPackage == null)
                return ModifiedState.UNCHANGED;

            if (WorkPackageSent != null) //to avoid circular reference, for example TEA->ITC and ITC->TEA
                WorkPackageSent.Clear();

            ModifiedState commonState = ModifiedState.UNCHANGED;
            this.ProcessingPackage = true;
            for (int i = 0; i < workPackage.Count; i++)
            {
                Message message = (Message)workPackage[i];
                message["ParentPackage"] = objMessage;

                if (message["StreamControl"] == null && objMessage["StreamControl"] != null)
                    message["StreamControl"] = objMessage["StreamControl"];
                ModifiedState state = OnSetSubscriptionData(message);
                if (state == ModifiedState.MODIFIED)
                    commonState = ModifiedState.MODIFIED;
            }
            ProcessingPackage = false;
            PublishMessagePackageChanges();
            return commonState;
        }
        public bool IsWorkPackage(IMessage objMessage)
        {
            if (objMessage.Contains("MessagePackage") && objMessage["MessagePackage"] != null)
                return true;
            else
                return false;
        }

        public virtual void PublishMessagePackageChanges()
        {

        }

        protected bool DelayPublish()
        {
            bool delay = PublishInProgress || ProcessingPackage || cmIsJoining || cmIsLeaving;
            return delay;
        }

        #endregion //Message packaging - WorkPackage Methods
        
        public static DateTime GetAIFRSPeriodEnd()
        {
            return new DateTime(2008, 10, 1);
        }

        public static DateTime GetAIFRSPeriodStart()
        {
            return new DateTime(2008, 1, 1);
        }

        public static bool IsAIFRSPeriod(DateTime startDate, DateTime endDate)
        {
            if ((DateTime.Compare(startDate, GetAIFRSPeriodStart()) >= 0 && (DateTime.Compare(endDate, GetAIFRSPeriodEnd()) >= 0)))
                return true;
            else
                return false;
        }


        public static bool CheckComponentIsIncluded(DateTime componentVersionStartDate, DateTime componentVersionEndDate, DateTime periodStartDate, DateTime periodEndDate, string componentDisplayName)
        {
            if (CalculationModuleBase.IsAIFRSPeriod(periodStartDate, periodEndDate))
            {
                if (componentDisplayName.Contains("AIFRS"))
                    return true;
                else // Need to test Borrwing cost, this will also go ahead for ATOFORMS 
                    return (periodStartDate >= componentVersionStartDate
               && periodEndDate <= componentVersionEndDate);
            }
            else
            {
                if (componentDisplayName.Contains("AIFRS"))
                    return false;
                else // Need to test Borrwing cost, this will also go ahead for ATOFORMS 
                    return (componentVersionStartDate <= periodEndDate
               && componentVersionEndDate >= periodStartDate);
            }
        }
    }
}
