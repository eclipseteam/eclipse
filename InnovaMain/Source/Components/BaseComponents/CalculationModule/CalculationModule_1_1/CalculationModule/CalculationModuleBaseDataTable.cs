using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Oritax.TaxSimp.Calculation
{
    abstract public class CalculationModuleBaseDataTable
    {
        #region Fields

        private DataTable table;

        #endregion // Fields

        #region Constructors

        protected CalculationModuleBaseDataTable(DataTable dataTable)
        {
            if (dataTable == null)
            {
                throw new ArgumentNullException("dataTable");
            }
            table = dataTable;
        }

        #endregion // Constructors

        #region Public Properties

        public DataRowCollection Rows
        {
            get
            {
                return table.Rows;
            }
        }

        #endregion // Public Properties

        #region Public Methods

        public DataRow[] Select(string filterExpression)
        {
            if (filterExpression == null)
                throw new ArgumentNullException("filterExpression");
            return this.table.Select(filterExpression);
        }

        public DataView CreateDataView()
        {
            return new DataView(this.table);
        }

        #endregion // Public Methods

        #region Protected And Internal Properties

        protected DataTable Table
        {
            get
            {
                return table;
            }
        }

        #endregion // Protected And Internal Properties

        #region Protected And Internal Methods

        protected abstract bool IsValid();

        #endregion // Protected And Internal Methods
    }
}
