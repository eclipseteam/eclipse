using System;

namespace Oritax.TaxSimp.Calculation
{
	/// <summary>
	/// Summary description for BrokerManagedComponentBaseFW.
	/// </summary>
	public class CalculationModuleInstall
	{
		#region INSTALLATION PROPERTIES

		// Assembly Installation Properties
		public const string ASSEMBLY_ID="9ACF1A7E-19B5-48d6-88FE-88D122E28DD5";
		public const string ASSEMBLY_NAME="CalculationModule_1_1";
		public const string ASSEMBLY_DISPLAYNAME="CalculationModule V1.1";
		public const string ASSEMBLY_MAJORVERSION="1";
		public const string ASSEMBLY_MINORVERSION="1";
		public const string ASSEMBLY_DATAFORMAT="0";
		//public const string ASSEMBLY_REVISION="0";		//2004.3.1
		//public const string ASSEMBLY_REVISION="1";		//2005.3.2
		//public const string ASSEMBLY_REVISION="2";		//2004.5
		public const string ASSEMBLY_REVISION="3";		//2005.1


		// Component Installation Properties
		public const string COMPONENT_ID="3981982D-3D81-4419-B831-90B1C8254976";
		public const string COMPONENT_NAME="CalculationModule";
		public const string COMPONENT_DISPLAYNAME="Calculation Module";
		public const string COMPONENT_CATEGORY="Base";

		// Component Version Installation Properties
		public const string COMPONENTVERSION_STARTDATE="01/01/1990 00:00:00 AM";
		public const string COMPONENTVERSION_ENDDATE="31/12/2050 12:59:59 AM";
		public const string COMPONENTVERSION_PERSISTERASSEMBLY="CommonPersisters_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";
		public const string COMPONENTVERSION_PERSISTERCLASS="Oritax.TaxSimp.CommonPersistence.CalculationPersist";
		public const string COMPONENTVERSION_IMPLEMENTATIONCLASS="Oritax.TaxSimp.Calculation.CalculationModuleBase";

		// Data Model
//		public const string DATAMODEL_MANAGERASSEMBLY="CalculationPersistSQL_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";
//		public const string DATAMODEL_MANAGERCLASS="Oritax.TaxSimp.CalculationPersistSQL.CMPersister";

		#endregion
	}
}
