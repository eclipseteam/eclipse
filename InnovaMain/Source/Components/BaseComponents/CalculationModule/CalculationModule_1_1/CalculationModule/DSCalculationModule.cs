using System;
using System.Data;

namespace Oritax.TaxSimp.DSCalculation
{
	/// <summary>
	/// DSCalculationModule is a DataSet that contains all of the persistent data for a CM instance.
	/// </summary>
	[System.ComponentModel.DesignerCategory("Code")]
	[SerializableAttribute] 
	public class DSCalculationModule : DataSet
	{
		public const String INSTANCE_TABLE					= "INSTANCE_TABLE";
		public const String INSTANCEID_FIELD				= "INSTANCEID";
		public const String NAME_FIELD						= "NAME";
		public const String TYPE_FIELD						= "TYPE";
		public const String PERSISTANT_DATA_FIELD			= "PERSISTENT_DATA";
		
		public const String CHILDINSTANCE_TABLE				= "CHILDINSTANCE";
		public const String CHILDINSTANCEID_FIELD			= "CHILDINSTANCEID";

		public DSCalculationModule()
		{
			DataTable table;
			DataColumnCollection columns;

			// Add the table for the instance data
			table = new DataTable(INSTANCE_TABLE);
			columns = table.Columns;
			columns.Add(INSTANCEID_FIELD, typeof(System.Guid));
			columns.Add(NAME_FIELD, typeof(System.String));
			columns.Add(TYPE_FIELD, typeof(System.Guid));
			columns.Add(PERSISTANT_DATA_FIELD, typeof(byte[]));
			this.Tables.Add(table);
			// Add the table for the child instances
			table = new DataTable(CHILDINSTANCE_TABLE);
			columns = table.Columns;
			columns.Add(CHILDINSTANCEID_FIELD, typeof(System.Guid));
			this.Tables.Add(table);
		}

		public DSCalculationModule(Guid cmGuid,String name,Guid cmType,byte[] serialData)
			: this()
		{
			object[] rowData={cmGuid,name,cmType,serialData};
			DataRow newRow=Tables[INSTANCE_TABLE].Rows.Add(rowData);
		}

	}
}
