﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Transactions;

using Oritax.TaxSimp.CalculationInterface;

namespace Oritax.TaxSimp.VelocityCache
{
    public abstract class SQLRMBase : IEnlistmentNotification
    {
        protected ITransactionVelocity _WrappedTransaction;

        public SQLRMBase(ITransactionVelocity wrappedTransaction)
        {
            this._WrappedTransaction = wrappedTransaction;
        }

        public virtual void Commit(Enlistment enlistment)
        {
            //enlistment.Done();
        }

        public virtual void InDoubt(Enlistment enlistment)
        {
        }

        public virtual void Prepare(PreparingEnlistment preparingEnlistment)
        {
            //preparingEnlistment.Prepared();
        }

        public virtual void Rollback(Enlistment enlistment)
        {
            //enlistment.Done();
        }

    }
}
