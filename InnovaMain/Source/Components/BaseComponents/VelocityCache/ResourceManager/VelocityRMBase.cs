﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;

namespace Oritax.TaxSimp.VelocityCache
{
    public class VelocityRMBase : IEnlistmentNotification
    {
        private string _ID;
        private object _OBJ;
        private object _OldValue = null;

        // If we need rollback function in Velocity, the old value should be saved in prepare method.
        // So that in Rollback(), we can put old value back.
        // But bear in mind, this feature has impact for performance as we need extra "get" to retrieve old value.
        // private object _oldValue;
        // bool IsRollbackFunctionProvided = false;

        public VelocityRMBase()
        {
        }


        public VelocityRMBase(string id, object obj)
        {
            this._ID = id;
            this._OBJ = obj;
        }

        public virtual void Commit(Enlistment enlistment)
        {
            //enlistment.Done();
        }

        public virtual void InDoubt(Enlistment enlistment)
        {
        }

        public virtual void Prepare(PreparingEnlistment preparingEnlistment)
        {
            //try
            //{
                this._OldValue = VelocityCacheString.VelocityCache.GetObejct(this._ID);

                VelocityCacheString.VelocityCache.PutObject(this._ID, this._OBJ);
                //preparingEnlistment.Prepared();
            //}
            //catch
            //{
            //    preparingEnlistment.ForceRollback();
            //}
        }

        public virtual void Rollback(Enlistment enlistment)
        {
            if (this._OldValue != null)
                VelocityCacheString.VelocityCache.PutObject(this._ID, this._OldValue);

            //VelocityCacheString.VelocityCache.RemoveObject(this._ID);
            //enlistment.Done();

        }

    }
}
