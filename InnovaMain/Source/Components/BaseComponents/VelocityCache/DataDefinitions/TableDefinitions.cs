﻿using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Data;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Linq.Expressions;
using System.ComponentModel;
using System;


namespace Oritax.TaxSimp.VelocityCache
{
    public partial class AssemblyTable
    {

        private System.Guid _ID;

        private string _NAME;

        private string _DISPLAYNAME;

        private string _STRONGNAME;

        private int _MAJORVERSION;

        private int _MINORVERSION;

        private int _DATAFORMAT;

        private int _REVISION;

        public AssemblyTable()
        {
        }

        [Column(Storage = "_ID", DbType = "UniqueIdentifier NOT NULL")]
        public System.Guid ID
        {
            get
            {
                return this._ID;
            }
            set
            {
                if ((this._ID != value))
                {
                    this._ID = value;
                }
            }
        }

        [Column(Storage = "_NAME", DbType = "VarChar(255) NOT NULL", CanBeNull = false)]
        public string NAME
        {
            get
            {
                return this._NAME;
            }
            set
            {
                if ((this._NAME != value))
                {
                    this._NAME = value;
                }
            }
        }

        [Column(Storage = "_DISPLAYNAME", DbType = "VarChar(255) NOT NULL", CanBeNull = false)]
        public string DISPLAYNAME
        {
            get
            {
                return this._DISPLAYNAME;
            }
            set
            {
                if ((this._DISPLAYNAME != value))
                {
                    this._DISPLAYNAME = value;
                }
            }
        }

        [Column(Storage = "_STRONGNAME", DbType = "VarChar(255) NOT NULL", CanBeNull = false)]
        public string STRONGNAME
        {
            get
            {
                return this._STRONGNAME;
            }
            set
            {
                if ((this._STRONGNAME != value))
                {
                    this._STRONGNAME = value;
                }
            }
        }

        [Column(Storage = "_MAJORVERSION", DbType = "Int NOT NULL")]
        public int MAJORVERSION
        {
            get
            {
                return this._MAJORVERSION;
            }
            set
            {
                if ((this._MAJORVERSION != value))
                {
                    this._MAJORVERSION = value;
                }
            }
        }

        [Column(Storage = "_MINORVERSION", DbType = "Int NOT NULL")]
        public int MINORVERSION
        {
            get
            {
                return this._MINORVERSION;
            }
            set
            {
                if ((this._MINORVERSION != value))
                {
                    this._MINORVERSION = value;
                }
            }
        }

        [Column(Storage = "_DATAFORMAT", DbType = "Int NOT NULL")]
        public int DATAFORMAT
        {
            get
            {
                return this._DATAFORMAT;
            }
            set
            {
                if ((this._DATAFORMAT != value))
                {
                    this._DATAFORMAT = value;
                }
            }
        }

        [Column(Storage = "_REVISION", DbType = "Int NOT NULL")]
        public int REVISION
        {
            get
            {
                return this._REVISION;
            }
            set
            {
                if ((this._REVISION != value))
                {
                    this._REVISION = value;
                }
            }
        }
    }

    [Table(Name = "dbo.COMPONENTVERSION")]
    public partial class ComponentVersionTable
    {

        private System.Guid _ID;

        private string _VERSIONNAME;

        private System.Guid _COMPONENTID;

        private System.DateTime _STARTDATE;

        private System.DateTime _ENDDATE;

        private string _PERSISTERASSEMBLYSTRONGNAME;

        private string _PERSISTERCLASS;

        private string _IMPLEMENTATIONCLASS;

        private bool _OBSOLETE;

        public ComponentVersionTable()
        {
        }

        [Column(Storage = "_ID", DbType = "UniqueIdentifier NOT NULL")]
        public System.Guid ID
        {
            get
            {
                return this._ID;
            }
            set
            {
                if ((this._ID != value))
                {
                    this._ID = value;
                }
            }
        }

        [Column(Storage = "_VERSIONNAME", DbType = "VarChar(255) NOT NULL", CanBeNull = false)]
        public string VERSIONNAME
        {
            get
            {
                return this._VERSIONNAME;
            }
            set
            {
                if ((this._VERSIONNAME != value))
                {
                    this._VERSIONNAME = value;
                }
            }
        }

        [Column(Storage = "_COMPONENTID", DbType = "UniqueIdentifier NOT NULL")]
        public System.Guid COMPONENTID
        {
            get
            {
                return this._COMPONENTID;
            }
            set
            {
                if ((this._COMPONENTID != value))
                {
                    this._COMPONENTID = value;
                }
            }
        }

        [Column(Storage = "_STARTDATE", DbType = "DateTime NOT NULL")]
        public System.DateTime STARTDATE
        {
            get
            {
                return this._STARTDATE;
            }
            set
            {
                if ((this._STARTDATE != value))
                {
                    this._STARTDATE = value;
                }
            }
        }

        [Column(Storage = "_ENDDATE", DbType = "DateTime NOT NULL")]
        public System.DateTime ENDDATE
        {
            get
            {
                return this._ENDDATE;
            }
            set
            {
                if ((this._ENDDATE != value))
                {
                    this._ENDDATE = value;
                }
            }
        }

        [Column(Storage = "_PERSISTERASSEMBLYSTRONGNAME", DbType = "VarChar(255) NOT NULL", CanBeNull = false)]
        public string PERSISTERASSEMBLYSTRONGNAME
        {
            get
            {
                return this._PERSISTERASSEMBLYSTRONGNAME;
            }
            set
            {
                if ((this._PERSISTERASSEMBLYSTRONGNAME != value))
                {
                    this._PERSISTERASSEMBLYSTRONGNAME = value;
                }
            }
        }

        [Column(Storage = "_PERSISTERCLASS", DbType = "VarChar(255) NOT NULL", CanBeNull = false)]
        public string PERSISTERCLASS
        {
            get
            {
                return this._PERSISTERCLASS;
            }
            set
            {
                if ((this._PERSISTERCLASS != value))
                {
                    this._PERSISTERCLASS = value;
                }
            }
        }

        [Column(Storage = "_IMPLEMENTATIONCLASS", DbType = "VarChar(255) NOT NULL", CanBeNull = false)]
        public string IMPLEMENTATIONCLASS
        {
            get
            {
                return this._IMPLEMENTATIONCLASS;
            }
            set
            {
                if ((this._IMPLEMENTATIONCLASS != value))
                {
                    this._IMPLEMENTATIONCLASS = value;
                }
            }
        }

        [Column(Storage = "_OBSOLETE", DbType = "Bit NOT NULL")]
        public bool OBSOLETE
        {
            get
            {
                return this._OBSOLETE;
            }
            set
            {
                if ((this._OBSOLETE != value))
                {
                    this._OBSOLETE = value;
                }
            }
        }
    }

    [Table(Name = "dbo.COMPONENT")]
    public partial class ComponentTable
    {

        private System.Guid _ID;

        private string _NAME;

        private string _DISPLAYNAME;

        private string _CATEGORY;

        private int _APPLICABILITY;

        private bool _OBSOLETE;

        public ComponentTable()
        {
        }

        [Column(Storage = "_ID", DbType = "UniqueIdentifier NOT NULL")]
        public System.Guid ID
        {
            get
            {
                return this._ID;
            }
            set
            {
                if ((this._ID != value))
                {
                    this._ID = value;
                }
            }
        }

        [Column(Storage = "_NAME", DbType = "VarChar(255) NOT NULL", CanBeNull = false)]
        public string NAME
        {
            get
            {
                return this._NAME;
            }
            set
            {
                if ((this._NAME != value))
                {
                    this._NAME = value;
                }
            }
        }

        [Column(Storage = "_DISPLAYNAME", DbType = "VarChar(255) NOT NULL", CanBeNull = false)]
        public string DISPLAYNAME
        {
            get
            {
                return this._DISPLAYNAME;
            }
            set
            {
                if ((this._DISPLAYNAME != value))
                {
                    this._DISPLAYNAME = value;
                }
            }
        }

        [Column(Storage = "_CATEGORY", DbType = "VarChar(255) NOT NULL", CanBeNull = false)]
        public string CATEGORY
        {
            get
            {
                return this._CATEGORY;
            }
            set
            {
                if ((this._CATEGORY != value))
                {
                    this._CATEGORY = value;
                }
            }
        }

        [Column(Storage = "_APPLICABILITY", DbType = "Int NOT NULL")]
        public int APPLICABILITY
        {
            get
            {
                return this._APPLICABILITY;
            }
            set
            {
                if ((this._APPLICABILITY != value))
                {
                    this._APPLICABILITY = value;
                }
            }
        }

        [Column(Storage = "_OBSOLETE", DbType = "Bit NOT NULL")]
        public bool OBSOLETE
        {
            get
            {
                return this._OBSOLETE;
            }
            set
            {
                if ((this._OBSOLETE != value))
                {
                    this._OBSOLETE = value;
                }
            }
        }
    }

    [Table(Name = "dbo.SUBSCRIPTIONS")]
    public partial class SUBSCRIPTION : INotifyPropertyChanging, INotifyPropertyChanged
    {

        private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);

        private System.Guid _SBID;

        private System.Guid _CLID;

        private string _DATATYPE;

        private System.Guid _SUBSCRIBERCLID;

        private System.Guid _SUBSCRIBERCSID;

        private System.Guid _SUBSCRIBEDCSID;

        private System.Guid _SUBSCRIBEDCLID;

        private bool _CONSOLIDATION;

        private System.Data.Linq.Binary _PARAMETERS;

        #region Extensibility Method Definitions
        partial void OnLoaded();
        partial void OnValidate(System.Data.Linq.ChangeAction action);
        partial void OnCreated();
        partial void OnSBIDChanging(System.Guid value);
        partial void OnSBIDChanged();
        partial void OnCLIDChanging(System.Guid value);
        partial void OnCLIDChanged();
        partial void OnDATATYPEChanging(string value);
        partial void OnDATATYPEChanged();
        partial void OnSUBSCRIBERCLIDChanging(System.Guid value);
        partial void OnSUBSCRIBERCLIDChanged();
        partial void OnSUBSCRIBERCSIDChanging(System.Guid value);
        partial void OnSUBSCRIBERCSIDChanged();
        partial void OnSUBSCRIBEDCSIDChanging(System.Guid value);
        partial void OnSUBSCRIBEDCSIDChanged();
        partial void OnSUBSCRIBEDCLIDChanging(System.Guid value);
        partial void OnSUBSCRIBEDCLIDChanged();
        partial void OnCONSOLIDATIONChanging(bool value);
        partial void OnCONSOLIDATIONChanged();
        partial void OnPARAMETERSChanging(System.Data.Linq.Binary value);
        partial void OnPARAMETERSChanged();
        #endregion

        public SUBSCRIPTION()
        {
            OnCreated();
        }

        [Column(Storage = "_SBID", DbType = "UniqueIdentifier NOT NULL", IsPrimaryKey = true)]
        public System.Guid SBID
        {
            get
            {
                return this._SBID;
            }
            set
            {
                if ((this._SBID != value))
                {
                    this.OnSBIDChanging(value);
                    this.SendPropertyChanging();
                    this._SBID = value;
                    this.SendPropertyChanged("SBID");
                    this.OnSBIDChanged();
                }
            }
        }

        [Column(Storage = "_CLID", DbType = "UniqueIdentifier NOT NULL")]
        public System.Guid CLID
        {
            get
            {
                return this._CLID;
            }
            set
            {
                if ((this._CLID != value))
                {
                    this.OnCLIDChanging(value);
                    this.SendPropertyChanging();
                    this._CLID = value;
                    this.SendPropertyChanged("CLID");
                    this.OnCLIDChanged();
                }
            }
        }

        [Column(Storage = "_DATATYPE", DbType = "VarChar(355)")]
        public string DATATYPE
        {
            get
            {
                return this._DATATYPE;
            }
            set
            {
                if ((this._DATATYPE != value))
                {
                    this.OnDATATYPEChanging(value);
                    this.SendPropertyChanging();
                    this._DATATYPE = value;
                    this.SendPropertyChanged("DATATYPE");
                    this.OnDATATYPEChanged();
                }
            }
        }

        [Column(Storage = "_SUBSCRIBERCLID", DbType = "UniqueIdentifier NOT NULL")]
        public System.Guid SUBSCRIBERCLID
        {
            get
            {
                return this._SUBSCRIBERCLID;
            }
            set
            {
                if ((this._SUBSCRIBERCLID != value))
                {
                    this.OnSUBSCRIBERCLIDChanging(value);
                    this.SendPropertyChanging();
                    this._SUBSCRIBERCLID = value;
                    this.SendPropertyChanged("SUBSCRIBERCLID");
                    this.OnSUBSCRIBERCLIDChanged();
                }
            }
        }

        [Column(Storage = "_SUBSCRIBERCSID", DbType = "UniqueIdentifier NOT NULL")]
        public System.Guid SUBSCRIBERCSID
        {
            get
            {
                return this._SUBSCRIBERCSID;
            }
            set
            {
                if ((this._SUBSCRIBERCSID != value))
                {
                    this.OnSUBSCRIBERCSIDChanging(value);
                    this.SendPropertyChanging();
                    this._SUBSCRIBERCSID = value;
                    this.SendPropertyChanged("SUBSCRIBERCSID");
                    this.OnSUBSCRIBERCSIDChanged();
                }
            }
        }

        [Column(Storage = "_SUBSCRIBEDCSID", DbType = "UniqueIdentifier NOT NULL")]
        public System.Guid SUBSCRIBEDCSID
        {
            get
            {
                return this._SUBSCRIBEDCSID;
            }
            set
            {
                if ((this._SUBSCRIBEDCSID != value))
                {
                    this.OnSUBSCRIBEDCSIDChanging(value);
                    this.SendPropertyChanging();
                    this._SUBSCRIBEDCSID = value;
                    this.SendPropertyChanged("SUBSCRIBEDCSID");
                    this.OnSUBSCRIBEDCSIDChanged();
                }
            }
        }

        [Column(Storage = "_SUBSCRIBEDCLID", DbType = "UniqueIdentifier NOT NULL")]
        public System.Guid SUBSCRIBEDCLID
        {
            get
            {
                return this._SUBSCRIBEDCLID;
            }
            set
            {
                if ((this._SUBSCRIBEDCLID != value))
                {
                    this.OnSUBSCRIBEDCLIDChanging(value);
                    this.SendPropertyChanging();
                    this._SUBSCRIBEDCLID = value;
                    this.SendPropertyChanged("SUBSCRIBEDCLID");
                    this.OnSUBSCRIBEDCLIDChanged();
                }
            }
        }

        [Column(Storage = "_CONSOLIDATION", DbType = "Bit NOT NULL")]
        public bool CONSOLIDATION
        {
            get
            {
                return this._CONSOLIDATION;
            }
            set
            {
                if ((this._CONSOLIDATION != value))
                {
                    this.OnCONSOLIDATIONChanging(value);
                    this.SendPropertyChanging();
                    this._CONSOLIDATION = value;
                    this.SendPropertyChanged("CONSOLIDATION");
                    this.OnCONSOLIDATIONChanged();
                }
            }
        }

        [Column(Storage = "_PARAMETERS", DbType = "Image", UpdateCheck = UpdateCheck.Never)]
        public System.Data.Linq.Binary PARAMETERS
        {
            get
            {
                return this._PARAMETERS;
            }
            set
            {
                if ((this._PARAMETERS != value))
                {
                    this.OnPARAMETERSChanging(value);
                    this.SendPropertyChanging();
                    this._PARAMETERS = value;
                    this.SendPropertyChanged("PARAMETERS");
                    this.OnPARAMETERSChanged();
                }
            }
        }

        public event PropertyChangingEventHandler PropertyChanging;

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void SendPropertyChanging()
        {
            if ((this.PropertyChanging != null))
            {
                this.PropertyChanging(this, emptyChangingEventArgs);
            }
        }

        protected virtual void SendPropertyChanged(String propertyName)
        {
            if ((this.PropertyChanged != null))
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }

    [Table(Name = "dbo.TEA_Adjustments")]
    public partial class TEA_Adjustment : INotifyPropertyChanging, INotifyPropertyChanged
    {

        private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);

        private System.Guid _AdjustmentsID;

        private string _AdjustmentsKey;

        private System.Guid _CalcID;

        private string _Name;

        private decimal _Amount;

        private decimal _LossConsiderationAmount;

        private string _Sign;

        private string _Type;

        private bool _Permanent;

        private string _ProofCategory;

        private string _Ref;

        private string _RefCLID;

        private string _RefWorkpaper;

        private string _WPTYPE;

        private bool _ITLSOURCED;

        private System.Nullable<decimal> _Equity;

        private System.Nullable<decimal> _Other;

        private int _Allocation;

        #region Extensibility Method Definitions
        partial void OnLoaded();
        partial void OnValidate(System.Data.Linq.ChangeAction action);
        partial void OnCreated();
        partial void OnAdjustmentsIDChanging(System.Guid value);
        partial void OnAdjustmentsIDChanged();
        partial void OnAdjustmentsKeyChanging(string value);
        partial void OnAdjustmentsKeyChanged();
        partial void OnCalcIDChanging(System.Guid value);
        partial void OnCalcIDChanged();
        partial void OnNameChanging(string value);
        partial void OnNameChanged();
        partial void OnAmountChanging(decimal value);
        partial void OnAmountChanged();
        partial void OnLossConsiderationAmountChanging(decimal value);
        partial void OnLossConsiderationAmountChanged();
        partial void OnSignChanging(string value);
        partial void OnSignChanged();
        partial void OnTypeChanging(string value);
        partial void OnTypeChanged();
        partial void OnPermanentChanging(bool value);
        partial void OnPermanentChanged();
        partial void OnProofCategoryChanging(string value);
        partial void OnProofCategoryChanged();
        partial void OnRefChanging(string value);
        partial void OnRefChanged();
        partial void OnRefCLIDChanging(string value);
        partial void OnRefCLIDChanged();
        partial void OnRefWorkpaperChanging(string value);
        partial void OnRefWorkpaperChanged();
        partial void OnWPTYPEChanging(string value);
        partial void OnWPTYPEChanged();
        partial void OnITLSOURCEDChanging(bool value);
        partial void OnITLSOURCEDChanged();
        partial void OnEquityChanging(System.Nullable<decimal> value);
        partial void OnEquityChanged();
        partial void OnOtherChanging(System.Nullable<decimal> value);
        partial void OnOtherChanged();
        partial void OnAllocationChanging(int value);
        partial void OnAllocationChanged();
        #endregion

        public TEA_Adjustment()
        {
            OnCreated();
        }

        [Column(Storage = "_AdjustmentsID", DbType = "UniqueIdentifier NOT NULL", IsPrimaryKey = true)]
        public System.Guid AdjustmentsID
        {
            get
            {
                return this._AdjustmentsID;
            }
            set
            {
                if ((this._AdjustmentsID != value))
                {
                    this.OnAdjustmentsIDChanging(value);
                    this.SendPropertyChanging();
                    this._AdjustmentsID = value;
                    this.SendPropertyChanged("AdjustmentsID");
                    this.OnAdjustmentsIDChanged();
                }
            }
        }

        [Column(Storage = "_AdjustmentsKey", DbType = "VarChar(355) NOT NULL", CanBeNull = false)]
        public string AdjustmentsKey
        {
            get
            {
                return this._AdjustmentsKey;
            }
            set
            {
                if ((this._AdjustmentsKey != value))
                {
                    this.OnAdjustmentsKeyChanging(value);
                    this.SendPropertyChanging();
                    this._AdjustmentsKey = value;
                    this.SendPropertyChanged("AdjustmentsKey");
                    this.OnAdjustmentsKeyChanged();
                }
            }
        }

        [Column(Storage = "_CalcID", DbType = "UniqueIdentifier NOT NULL")]
        public System.Guid CalcID
        {
            get
            {
                return this._CalcID;
            }
            set
            {
                if ((this._CalcID != value))
                {
                    this.OnCalcIDChanging(value);
                    this.SendPropertyChanging();
                    this._CalcID = value;
                    this.SendPropertyChanged("CalcID");
                    this.OnCalcIDChanged();
                }
            }
        }

        [Column(Storage = "_Name", DbType = "VarChar(355) NOT NULL", CanBeNull = false)]
        public string Name
        {
            get
            {
                return this._Name;
            }
            set
            {
                if ((this._Name != value))
                {
                    this.OnNameChanging(value);
                    this.SendPropertyChanging();
                    this._Name = value;
                    this.SendPropertyChanged("Name");
                    this.OnNameChanged();
                }
            }
        }

        [Column(Storage = "_Amount", DbType = "Decimal(18,2) NOT NULL")]
        public decimal Amount
        {
            get
            {
                return this._Amount;
            }
            set
            {
                if ((this._Amount != value))
                {
                    this.OnAmountChanging(value);
                    this.SendPropertyChanging();
                    this._Amount = value;
                    this.SendPropertyChanged("Amount");
                    this.OnAmountChanged();
                }
            }
        }

        [Column(Storage = "_LossConsiderationAmount", DbType = "Decimal(18,2) NOT NULL")]
        public decimal LossConsiderationAmount
        {
            get
            {
                return this._LossConsiderationAmount;
            }
            set
            {
                if ((this._LossConsiderationAmount != value))
                {
                    this.OnLossConsiderationAmountChanging(value);
                    this.SendPropertyChanging();
                    this._LossConsiderationAmount = value;
                    this.SendPropertyChanged("LossConsiderationAmount");
                    this.OnLossConsiderationAmountChanged();
                }
            }
        }

        [Column(Storage = "_Sign", DbType = "VarChar(355) NOT NULL", CanBeNull = false)]
        public string Sign
        {
            get
            {
                return this._Sign;
            }
            set
            {
                if ((this._Sign != value))
                {
                    this.OnSignChanging(value);
                    this.SendPropertyChanging();
                    this._Sign = value;
                    this.SendPropertyChanged("Sign");
                    this.OnSignChanged();
                }
            }
        }

        [Column(Storage = "_Type", DbType = "VarChar(355) NOT NULL", CanBeNull = false)]
        public string Type
        {
            get
            {
                return this._Type;
            }
            set
            {
                if ((this._Type != value))
                {
                    this.OnTypeChanging(value);
                    this.SendPropertyChanging();
                    this._Type = value;
                    this.SendPropertyChanged("Type");
                    this.OnTypeChanged();
                }
            }
        }

        [Column(Name = "[Permanent]", Storage = "_Permanent", DbType = "Bit NOT NULL")]
        public bool Permanent
        {
            get
            {
                return this._Permanent;
            }
            set
            {
                if ((this._Permanent != value))
                {
                    this.OnPermanentChanging(value);
                    this.SendPropertyChanging();
                    this._Permanent = value;
                    this.SendPropertyChanged("Permanent");
                    this.OnPermanentChanged();
                }
            }
        }

        [Column(Storage = "_ProofCategory", DbType = "VarChar(355) NOT NULL", CanBeNull = false)]
        public string ProofCategory
        {
            get
            {
                return this._ProofCategory;
            }
            set
            {
                if ((this._ProofCategory != value))
                {
                    this.OnProofCategoryChanging(value);
                    this.SendPropertyChanging();
                    this._ProofCategory = value;
                    this.SendPropertyChanged("ProofCategory");
                    this.OnProofCategoryChanged();
                }
            }
        }

        [Column(Storage = "_Ref", DbType = "VarChar(355)")]
        public string Ref
        {
            get
            {
                return this._Ref;
            }
            set
            {
                if ((this._Ref != value))
                {
                    this.OnRefChanging(value);
                    this.SendPropertyChanging();
                    this._Ref = value;
                    this.SendPropertyChanged("Ref");
                    this.OnRefChanged();
                }
            }
        }

        [Column(Storage = "_RefCLID", DbType = "VarChar(355)")]
        public string RefCLID
        {
            get
            {
                return this._RefCLID;
            }
            set
            {
                if ((this._RefCLID != value))
                {
                    this.OnRefCLIDChanging(value);
                    this.SendPropertyChanging();
                    this._RefCLID = value;
                    this.SendPropertyChanged("RefCLID");
                    this.OnRefCLIDChanged();
                }
            }
        }

        [Column(Storage = "_RefWorkpaper", DbType = "VarChar(355)")]
        public string RefWorkpaper
        {
            get
            {
                return this._RefWorkpaper;
            }
            set
            {
                if ((this._RefWorkpaper != value))
                {
                    this.OnRefWorkpaperChanging(value);
                    this.SendPropertyChanging();
                    this._RefWorkpaper = value;
                    this.SendPropertyChanged("RefWorkpaper");
                    this.OnRefWorkpaperChanged();
                }
            }
        }

        [Column(Storage = "_WPTYPE", DbType = "VarChar(355) NOT NULL", CanBeNull = false)]
        public string WPTYPE
        {
            get
            {
                return this._WPTYPE;
            }
            set
            {
                if ((this._WPTYPE != value))
                {
                    this.OnWPTYPEChanging(value);
                    this.SendPropertyChanging();
                    this._WPTYPE = value;
                    this.SendPropertyChanged("WPTYPE");
                    this.OnWPTYPEChanged();
                }
            }
        }

        [Column(Storage = "_ITLSOURCED", DbType = "Bit NOT NULL")]
        public bool ITLSOURCED
        {
            get
            {
                return this._ITLSOURCED;
            }
            set
            {
                if ((this._ITLSOURCED != value))
                {
                    this.OnITLSOURCEDChanging(value);
                    this.SendPropertyChanging();
                    this._ITLSOURCED = value;
                    this.SendPropertyChanged("ITLSOURCED");
                    this.OnITLSOURCEDChanged();
                }
            }
        }

        [Column(Storage = "_Equity", DbType = "Decimal(18,2)")]
        public System.Nullable<decimal> Equity
        {
            get
            {
                return this._Equity;
            }
            set
            {
                if ((this._Equity != value))
                {
                    this.OnEquityChanging(value);
                    this.SendPropertyChanging();
                    this._Equity = value;
                    this.SendPropertyChanged("Equity");
                    this.OnEquityChanged();
                }
            }
        }

        [Column(Storage = "_Other", DbType = "Decimal(18,2)")]
        public System.Nullable<decimal> Other
        {
            get
            {
                return this._Other;
            }
            set
            {
                if ((this._Other != value))
                {
                    this.OnOtherChanging(value);
                    this.SendPropertyChanging();
                    this._Other = value;
                    this.SendPropertyChanged("Other");
                    this.OnOtherChanged();
                }
            }
        }

        [Column(Storage = "_Allocation", DbType = "Int NOT NULL")]
        public int Allocation
        {
            get
            {
                return this._Allocation;
            }
            set
            {
                if ((this._Allocation != value))
                {
                    this.OnAllocationChanging(value);
                    this.SendPropertyChanging();
                    this._Allocation = value;
                    this.SendPropertyChanged("Allocation");
                    this.OnAllocationChanged();
                }
            }
        }

        public event PropertyChangingEventHandler PropertyChanging;

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void SendPropertyChanging()
        {
            if ((this.PropertyChanging != null))
            {
                this.PropertyChanging(this, emptyChangingEventArgs);
            }
        }

        protected virtual void SendPropertyChanged(String propertyName)
        {
            if ((this.PropertyChanged != null))
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }

    [Table(Name = "dbo.TEA_CALCULATOR")]
    public partial class TEA_CALCULATOR : INotifyPropertyChanging, INotifyPropertyChanged
    {

        private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);

        private System.Guid _ID;

        private System.Guid _ConsolCMID;

        private int _OwnerModule;

        #region Extensibility Method Definitions
        partial void OnLoaded();
        partial void OnValidate(System.Data.Linq.ChangeAction action);
        partial void OnCreated();
        partial void OnIDChanging(System.Guid value);
        partial void OnIDChanged();
        partial void OnConsolCMIDChanging(System.Guid value);
        partial void OnConsolCMIDChanged();
        partial void OnOwnerModuleChanging(int value);
        partial void OnOwnerModuleChanged();
        #endregion

        public TEA_CALCULATOR()
        {
            OnCreated();
        }

        [Column(Storage = "_ID", DbType = "UniqueIdentifier NOT NULL", IsPrimaryKey = true)]
        public System.Guid ID
        {
            get
            {
                return this._ID;
            }
            set
            {
                if ((this._ID != value))
                {
                    this.OnIDChanging(value);
                    this.SendPropertyChanging();
                    this._ID = value;
                    this.SendPropertyChanged("ID");
                    this.OnIDChanged();
                }
            }
        }

        [Column(Storage = "_ConsolCMID", DbType = "UniqueIdentifier NOT NULL")]
        public System.Guid ConsolCMID
        {
            get
            {
                return this._ConsolCMID;
            }
            set
            {
                if ((this._ConsolCMID != value))
                {
                    this.OnConsolCMIDChanging(value);
                    this.SendPropertyChanging();
                    this._ConsolCMID = value;
                    this.SendPropertyChanged("ConsolCMID");
                    this.OnConsolCMIDChanged();
                }
            }
        }

        [Column(Storage = "_OwnerModule", DbType = "Int NOT NULL")]
        public int OwnerModule
        {
            get
            {
                return this._OwnerModule;
            }
            set
            {
                if ((this._OwnerModule != value))
                {
                    this.OnOwnerModuleChanging(value);
                    this.SendPropertyChanging();
                    this._OwnerModule = value;
                    this.SendPropertyChanged("OwnerModule");
                    this.OnOwnerModuleChanged();
                }
            }
        }

        public event PropertyChangingEventHandler PropertyChanging;

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void SendPropertyChanging()
        {
            if ((this.PropertyChanging != null))
            {
                this.PropertyChanging(this, emptyChangingEventArgs);
            }
        }

        protected virtual void SendPropertyChanged(String propertyName)
        {
            if ((this.PropertyChanged != null))
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }

    [Table(Name = "dbo.AGGREGATION")]
    public partial class AGGREGATION : INotifyPropertyChanging, INotifyPropertyChanged
    {

        private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);

        private System.Guid _ID;

        private System.Guid _ConsolCMID;

        private string _StorageKey;

        private System.Data.Linq.Binary _Message;

        #region Extensibility Method Definitions
        partial void OnLoaded();
        partial void OnValidate(System.Data.Linq.ChangeAction action);
        partial void OnCreated();
        partial void OnIDChanging(System.Guid value);
        partial void OnIDChanged();
        partial void OnConsolCMIDChanging(System.Guid value);
        partial void OnConsolCMIDChanged();
        partial void OnStorageKeyChanging(string value);
        partial void OnStorageKeyChanged();
        partial void OnMessageChanging(System.Data.Linq.Binary value);
        partial void OnMessageChanged();
        #endregion

        public AGGREGATION()
        {
            OnCreated();
        }

        [Column(Storage = "_ID", DbType = "UniqueIdentifier NOT NULL", IsPrimaryKey = true)]
        public System.Guid ID
        {
            get
            {
                return this._ID;
            }
            set
            {
                if ((this._ID != value))
                {
                    this.OnIDChanging(value);
                    this.SendPropertyChanging();
                    this._ID = value;
                    this.SendPropertyChanged("ID");
                    this.OnIDChanged();
                }
            }
        }

        [Column(Storage = "_ConsolCMID", DbType = "UniqueIdentifier NOT NULL")]
        public System.Guid ConsolCMID
        {
            get
            {
                return this._ConsolCMID;
            }
            set
            {
                if ((this._ConsolCMID != value))
                {
                    this.OnConsolCMIDChanging(value);
                    this.SendPropertyChanging();
                    this._ConsolCMID = value;
                    this.SendPropertyChanged("ConsolCMID");
                    this.OnConsolCMIDChanged();
                }
            }
        }

        [Column(Storage = "_StorageKey", DbType = "VarChar(355) NOT NULL", CanBeNull = false)]
        public string StorageKey
        {
            get
            {
                return this._StorageKey;
            }
            set
            {
                if ((this._StorageKey != value))
                {
                    this.OnStorageKeyChanging(value);
                    this.SendPropertyChanging();
                    this._StorageKey = value;
                    this.SendPropertyChanged("StorageKey");
                    this.OnStorageKeyChanged();
                }
            }
        }

        [Column(Storage = "_Message", DbType = "Image NOT NULL", CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
        public System.Data.Linq.Binary Message
        {
            get
            {
                return this._Message;
            }
            set
            {
                if ((this._Message != value))
                {
                    this.OnMessageChanging(value);
                    this.SendPropertyChanging();
                    this._Message = value;
                    this.SendPropertyChanged("Message");
                    this.OnMessageChanged();
                }
            }
        }

        public event PropertyChangingEventHandler PropertyChanging;

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void SendPropertyChanging()
        {
            if ((this.PropertyChanging != null))
            {
                this.PropertyChanging(this, emptyChangingEventArgs);
            }
        }

        protected virtual void SendPropertyChanged(String propertyName)
        {
            if ((this.PropertyChanged != null))
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }

    [Table(Name = "dbo.TEA_AGGREGATIONVALUE")]
    public partial class TEA_AGGREGATIONVALUE : INotifyPropertyChanging, INotifyPropertyChanged
    {

        private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);

        private string _ID;

        private System.Guid _AggID;

        private string _CLID;

        private System.Nullable<decimal> _Value;

        private System.Nullable<decimal> _GrossAmount;

        private System.Nullable<decimal> _RecognisedAmount;

        private System.Nullable<decimal> _TaxAmount;

        private System.Guid _ConsolCMID;

        private System.Nullable<decimal> _Value2;

        private System.Nullable<decimal> _Value3;

        private System.Nullable<decimal> _Value4;

        private System.Nullable<decimal> _Value5;

        #region Extensibility Method Definitions
        partial void OnLoaded();
        partial void OnValidate(System.Data.Linq.ChangeAction action);
        partial void OnCreated();
        partial void OnIDChanging(string value);
        partial void OnIDChanged();
        partial void OnAggIDChanging(System.Guid value);
        partial void OnAggIDChanged();
        partial void OnCLIDChanging(string value);
        partial void OnCLIDChanged();
        partial void OnValueChanging(System.Nullable<decimal> value);
        partial void OnValueChanged();
        partial void OnGrossAmountChanging(System.Nullable<decimal> value);
        partial void OnGrossAmountChanged();
        partial void OnRecognisedAmountChanging(System.Nullable<decimal> value);
        partial void OnRecognisedAmountChanged();
        partial void OnTaxAmountChanging(System.Nullable<decimal> value);
        partial void OnTaxAmountChanged();
        partial void OnConsolCMIDChanging(System.Guid value);
        partial void OnConsolCMIDChanged();
        partial void OnValue2Changing(System.Nullable<decimal> value);
        partial void OnValue2Changed();
        partial void OnValue3Changing(System.Nullable<decimal> value);
        partial void OnValue3Changed();
        partial void OnValue4Changing(System.Nullable<decimal> value);
        partial void OnValue4Changed();
        partial void OnValue5Changing(System.Nullable<decimal> value);
        partial void OnValue5Changed();
        #endregion

        public TEA_AGGREGATIONVALUE()
        {
            OnCreated();
        }

        [Column(Storage = "_ID", DbType = "VarChar(355) NOT NULL", CanBeNull = false, IsPrimaryKey = true)]
        public string ID
        {
            get
            {
                return this._ID;
            }
            set
            {
                if ((this._ID != value))
                {
                    this.OnIDChanging(value);
                    this.SendPropertyChanging();
                    this._ID = value;
                    this.SendPropertyChanged("ID");
                    this.OnIDChanged();
                }
            }
        }

        [Column(Storage = "_AggID", DbType = "UniqueIdentifier NOT NULL")]
        public System.Guid AggID
        {
            get
            {
                return this._AggID;
            }
            set
            {
                if ((this._AggID != value))
                {
                    this.OnAggIDChanging(value);
                    this.SendPropertyChanging();
                    this._AggID = value;
                    this.SendPropertyChanged("AggID");
                    this.OnAggIDChanged();
                }
            }
        }

        [Column(Storage = "_CLID", DbType = "VarChar(355) NOT NULL", CanBeNull = false)]
        public string CLID
        {
            get
            {
                return this._CLID;
            }
            set
            {
                if ((this._CLID != value))
                {
                    this.OnCLIDChanging(value);
                    this.SendPropertyChanging();
                    this._CLID = value;
                    this.SendPropertyChanged("CLID");
                    this.OnCLIDChanged();
                }
            }
        }

        [Column(Storage = "_Value", DbType = "Decimal(18,2)")]
        public System.Nullable<decimal> Value
        {
            get
            {
                return this._Value;
            }
            set
            {
                if ((this._Value != value))
                {
                    this.OnValueChanging(value);
                    this.SendPropertyChanging();
                    this._Value = value;
                    this.SendPropertyChanged("Value");
                    this.OnValueChanged();
                }
            }
        }

        [Column(Storage = "_GrossAmount", DbType = "Decimal(18,2)")]
        public System.Nullable<decimal> GrossAmount
        {
            get
            {
                return this._GrossAmount;
            }
            set
            {
                if ((this._GrossAmount != value))
                {
                    this.OnGrossAmountChanging(value);
                    this.SendPropertyChanging();
                    this._GrossAmount = value;
                    this.SendPropertyChanged("GrossAmount");
                    this.OnGrossAmountChanged();
                }
            }
        }

        [Column(Storage = "_RecognisedAmount", DbType = "Decimal(18,2)")]
        public System.Nullable<decimal> RecognisedAmount
        {
            get
            {
                return this._RecognisedAmount;
            }
            set
            {
                if ((this._RecognisedAmount != value))
                {
                    this.OnRecognisedAmountChanging(value);
                    this.SendPropertyChanging();
                    this._RecognisedAmount = value;
                    this.SendPropertyChanged("RecognisedAmount");
                    this.OnRecognisedAmountChanged();
                }
            }
        }

        [Column(Storage = "_TaxAmount", DbType = "Decimal(18,2)")]
        public System.Nullable<decimal> TaxAmount
        {
            get
            {
                return this._TaxAmount;
            }
            set
            {
                if ((this._TaxAmount != value))
                {
                    this.OnTaxAmountChanging(value);
                    this.SendPropertyChanging();
                    this._TaxAmount = value;
                    this.SendPropertyChanged("TaxAmount");
                    this.OnTaxAmountChanged();
                }
            }
        }

        [Column(Storage = "_ConsolCMID", DbType = "UniqueIdentifier NOT NULL")]
        public System.Guid ConsolCMID
        {
            get
            {
                return this._ConsolCMID;
            }
            set
            {
                if ((this._ConsolCMID != value))
                {
                    this.OnConsolCMIDChanging(value);
                    this.SendPropertyChanging();
                    this._ConsolCMID = value;
                    this.SendPropertyChanged("ConsolCMID");
                    this.OnConsolCMIDChanged();
                }
            }
        }

        [Column(Storage = "_Value2", DbType = "Decimal(18,2)")]
        public System.Nullable<decimal> Value2
        {
            get
            {
                return this._Value2;
            }
            set
            {
                if ((this._Value2 != value))
                {
                    this.OnValue2Changing(value);
                    this.SendPropertyChanging();
                    this._Value2 = value;
                    this.SendPropertyChanged("Value2");
                    this.OnValue2Changed();
                }
            }
        }

        [Column(Storage = "_Value3", DbType = "Decimal(18,2)")]
        public System.Nullable<decimal> Value3
        {
            get
            {
                return this._Value3;
            }
            set
            {
                if ((this._Value3 != value))
                {
                    this.OnValue3Changing(value);
                    this.SendPropertyChanging();
                    this._Value3 = value;
                    this.SendPropertyChanged("Value3");
                    this.OnValue3Changed();
                }
            }
        }

        [Column(Storage = "_Value4", DbType = "Decimal(18,2)")]
        public System.Nullable<decimal> Value4
        {
            get
            {
                return this._Value4;
            }
            set
            {
                if ((this._Value4 != value))
                {
                    this.OnValue4Changing(value);
                    this.SendPropertyChanging();
                    this._Value4 = value;
                    this.SendPropertyChanged("Value4");
                    this.OnValue4Changed();
                }
            }
        }

        [Column(Storage = "_Value5", DbType = "Decimal(18,2)")]
        public System.Nullable<decimal> Value5
        {
            get
            {
                return this._Value5;
            }
            set
            {
                if ((this._Value5 != value))
                {
                    this.OnValue5Changing(value);
                    this.SendPropertyChanging();
                    this._Value5 = value;
                    this.SendPropertyChanged("Value5");
                    this.OnValue5Changed();
                }
            }
        }

        public event PropertyChangingEventHandler PropertyChanging;

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void SendPropertyChanging()
        {
            if ((this.PropertyChanging != null))
            {
                this.PropertyChanging(this, emptyChangingEventArgs);
            }
        }

        protected virtual void SendPropertyChanged(String propertyName)
        {
            if ((this.PropertyChanged != null))
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }

    [Table(Name = "dbo.TEA_AdjustmentsTotals")]
    public partial class TEA_AdjustmentsTotal : INotifyPropertyChanging, INotifyPropertyChanged
    {

        private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);

        private System.Guid _AdjustmentsTotalsID;

        private string _Type;

        private System.Guid _CalcID;

        private decimal _Amount;

        private string _Ref;

        private string _RefCLID;

        private string _RefWorkpaper;

        private string _WorkpaperType;

        private System.Nullable<decimal> _Equity;

        private System.Nullable<decimal> _ITE;

        private System.Nullable<decimal> _Other;

        #region Extensibility Method Definitions
        partial void OnLoaded();
        partial void OnValidate(System.Data.Linq.ChangeAction action);
        partial void OnCreated();
        partial void OnAdjustmentsTotalsIDChanging(System.Guid value);
        partial void OnAdjustmentsTotalsIDChanged();
        partial void OnTypeChanging(string value);
        partial void OnTypeChanged();
        partial void OnCalcIDChanging(System.Guid value);
        partial void OnCalcIDChanged();
        partial void OnAmountChanging(decimal value);
        partial void OnAmountChanged();
        partial void OnRefChanging(string value);
        partial void OnRefChanged();
        partial void OnRefCLIDChanging(string value);
        partial void OnRefCLIDChanged();
        partial void OnRefWorkpaperChanging(string value);
        partial void OnRefWorkpaperChanged();
        partial void OnWorkpaperTypeChanging(string value);
        partial void OnWorkpaperTypeChanged();
        partial void OnEquityChanging(System.Nullable<decimal> value);
        partial void OnEquityChanged();
        partial void OnITEChanging(System.Nullable<decimal> value);
        partial void OnITEChanged();
        partial void OnOtherChanging(System.Nullable<decimal> value);
        partial void OnOtherChanged();
        #endregion

        public TEA_AdjustmentsTotal()
        {
            OnCreated();
        }

        [Column(Storage = "_AdjustmentsTotalsID", DbType = "UniqueIdentifier NOT NULL", IsPrimaryKey = true)]
        public System.Guid AdjustmentsTotalsID
        {
            get
            {
                return this._AdjustmentsTotalsID;
            }
            set
            {
                if ((this._AdjustmentsTotalsID != value))
                {
                    this.OnAdjustmentsTotalsIDChanging(value);
                    this.SendPropertyChanging();
                    this._AdjustmentsTotalsID = value;
                    this.SendPropertyChanged("AdjustmentsTotalsID");
                    this.OnAdjustmentsTotalsIDChanged();
                }
            }
        }

        [Column(Storage = "_Type", DbType = "VarChar(355) NOT NULL", CanBeNull = false)]
        public string Type
        {
            get
            {
                return this._Type;
            }
            set
            {
                if ((this._Type != value))
                {
                    this.OnTypeChanging(value);
                    this.SendPropertyChanging();
                    this._Type = value;
                    this.SendPropertyChanged("Type");
                    this.OnTypeChanged();
                }
            }
        }

        [Column(Storage = "_CalcID", DbType = "UniqueIdentifier NOT NULL")]
        public System.Guid CalcID
        {
            get
            {
                return this._CalcID;
            }
            set
            {
                if ((this._CalcID != value))
                {
                    this.OnCalcIDChanging(value);
                    this.SendPropertyChanging();
                    this._CalcID = value;
                    this.SendPropertyChanged("CalcID");
                    this.OnCalcIDChanged();
                }
            }
        }

        [Column(Storage = "_Amount", DbType = "Decimal(18,2) NOT NULL")]
        public decimal Amount
        {
            get
            {
                return this._Amount;
            }
            set
            {
                if ((this._Amount != value))
                {
                    this.OnAmountChanging(value);
                    this.SendPropertyChanging();
                    this._Amount = value;
                    this.SendPropertyChanged("Amount");
                    this.OnAmountChanged();
                }
            }
        }

        [Column(Storage = "_Ref", DbType = "VarChar(355)")]
        public string Ref
        {
            get
            {
                return this._Ref;
            }
            set
            {
                if ((this._Ref != value))
                {
                    this.OnRefChanging(value);
                    this.SendPropertyChanging();
                    this._Ref = value;
                    this.SendPropertyChanged("Ref");
                    this.OnRefChanged();
                }
            }
        }

        [Column(Storage = "_RefCLID", DbType = "VarChar(355)")]
        public string RefCLID
        {
            get
            {
                return this._RefCLID;
            }
            set
            {
                if ((this._RefCLID != value))
                {
                    this.OnRefCLIDChanging(value);
                    this.SendPropertyChanging();
                    this._RefCLID = value;
                    this.SendPropertyChanged("RefCLID");
                    this.OnRefCLIDChanged();
                }
            }
        }

        [Column(Storage = "_RefWorkpaper", DbType = "VarChar(355)")]
        public string RefWorkpaper
        {
            get
            {
                return this._RefWorkpaper;
            }
            set
            {
                if ((this._RefWorkpaper != value))
                {
                    this.OnRefWorkpaperChanging(value);
                    this.SendPropertyChanging();
                    this._RefWorkpaper = value;
                    this.SendPropertyChanged("RefWorkpaper");
                    this.OnRefWorkpaperChanged();
                }
            }
        }

        [Column(Storage = "_WorkpaperType", DbType = "VarChar(355) NOT NULL", CanBeNull = false)]
        public string WorkpaperType
        {
            get
            {
                return this._WorkpaperType;
            }
            set
            {
                if ((this._WorkpaperType != value))
                {
                    this.OnWorkpaperTypeChanging(value);
                    this.SendPropertyChanging();
                    this._WorkpaperType = value;
                    this.SendPropertyChanged("WorkpaperType");
                    this.OnWorkpaperTypeChanged();
                }
            }
        }

        [Column(Storage = "_Equity", DbType = "Decimal(18,2)")]
        public System.Nullable<decimal> Equity
        {
            get
            {
                return this._Equity;
            }
            set
            {
                if ((this._Equity != value))
                {
                    this.OnEquityChanging(value);
                    this.SendPropertyChanging();
                    this._Equity = value;
                    this.SendPropertyChanged("Equity");
                    this.OnEquityChanged();
                }
            }
        }

        [Column(Storage = "_ITE", DbType = "Decimal(18,2)")]
        public System.Nullable<decimal> ITE
        {
            get
            {
                return this._ITE;
            }
            set
            {
                if ((this._ITE != value))
                {
                    this.OnITEChanging(value);
                    this.SendPropertyChanging();
                    this._ITE = value;
                    this.SendPropertyChanged("ITE");
                    this.OnITEChanged();
                }
            }
        }

        [Column(Storage = "_Other", DbType = "Decimal(18,2)")]
        public System.Nullable<decimal> Other
        {
            get
            {
                return this._Other;
            }
            set
            {
                if ((this._Other != value))
                {
                    this.OnOtherChanging(value);
                    this.SendPropertyChanging();
                    this._Other = value;
                    this.SendPropertyChanged("Other");
                    this.OnOtherChanged();
                }
            }
        }

        public event PropertyChangingEventHandler PropertyChanging;

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void SendPropertyChanging()
        {
            if ((this.PropertyChanging != null))
            {
                this.PropertyChanging(this, emptyChangingEventArgs);
            }
        }

        protected virtual void SendPropertyChanged(String propertyName)
        {
            if ((this.PropertyChanged != null))
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
