﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.VelocityCache
{
    public class CMLogicalTable
    {
        public Guid CLID { get; set; }
        public string Name { get; set; }
        public Guid CurrCSID { get; set; }
        public Guid CTID { get; set; }
        public Guid PLID { get; set; }
        public string BSConsolidationMode { get; set; }
        public string BSPerIODType { get; set; }
        public string BSConSol { get; set; }
        public string BSOuType { get; set; }


        public CMLogicalTable()
        {
        }

        public CMLogicalTable(Guid clid, string name, Guid currCSID, Guid ctid, Guid plid, string bsConsolidationMode, string bsPerIODType,
            string bsConsol, string bsOuType)
        {
            this.CLID = clid;
            this.Name = name;
            this.CurrCSID = currCSID;
            this.CTID = ctid;
            this.PLID = plid;
            this.BSConsolidationMode = bsConsolidationMode;
            this.BSPerIODType = BSPerIODType;
            this.BSConSol = bsConsol;
            this.BSOuType = bsOuType;
        }

    }
}
