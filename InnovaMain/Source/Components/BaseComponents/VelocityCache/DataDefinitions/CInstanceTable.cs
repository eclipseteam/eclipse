﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.VelocityCache
{
    public class CInstanceTable
    {
         public Guid CID 
        {
            get;set;
        }

        public Guid CTID
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public Byte[] UpdateToken
        {
            get;
            set;
        }

        public CInstanceTable()
        {
        }


        public CInstanceTable(Guid cid, Guid ctid, string name
            , Byte[] updateToken
            )
        {
            this.CID = cid;
            this.CTID = ctid;
            this.Name = name;
            this.UpdateToken = updateToken;
        }

    }
}
