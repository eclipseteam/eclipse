﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.VelocityCache
{
    public class ScenarioTable
    {
        public Guid CSID { get; set; }
        public string Name { get; set; }
        public int Type { get; set; }
        public int Status { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public bool Locked { get; set; }


        public ScenarioTable()
        {
        }

        public ScenarioTable(Guid csid, string name, int type, int status, DateTime created, DateTime modified, bool locked)
        {
            this.CSID = csid;
            this.Name = name;
            this.Type = type;
            this.Status = status;
            this.Created = created;
            this.Modified = modified;
            this.Locked = locked;


        }

    }
}
