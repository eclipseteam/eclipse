﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.VelocityCache
{
    public class CMScenarioTable
    {
        public Guid ID { get; set; }
        public Guid CLID { get; set; }
        public Guid CSID { get; set; }
        public Guid CIID { get; set; }
        public Guid CTID { get; set; }

        public CMScenarioTable()
        {
        }

        public CMScenarioTable(Guid id, Guid clid, Guid csid, Guid ciid, Guid ctid)
        {
            this.ID = id;
            this.CLID = clid;
            this.CSID = csid;
            this.CIID = ciid;
            this.CTID = ctid;
        }

    }
}
