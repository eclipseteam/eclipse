﻿//#define VELOCITY_DEBUG_OUTPUT
//#define VELOCITY_FILE_OUTPUT

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Microsoft.Data.Caching;

using System.Diagnostics;

namespace Oritax.TaxSimp.VelocityCache
{
    /// <summary>
    /// Attention:-
    /// 1. Only create one instance VelocityDataCache for each thread, as Initialise() takes long time.
    /// 2. Don't make VelocityDataCache static, as there's no concurrency control inside VelocityDataCache.
    /// </summary>
    public class VelocityDataCache
    {
        protected DataCache _DC;

        public VelocityDataCache()
        {
            Initialise();
        }

        public virtual object GetObejct(Guid id)
        {
            Object obj;

            obj = _DC.Get(id.ToString());

#if VELOCITY_DEBUG_OUTPUT
            Debug.WriteLineIf(obj == null, id.ToString(), "Velocity Cache Miss");
            Debug.WriteLineIf(obj != null, id.ToString(), "Velocity Cache Hit");
#endif
            return obj;
        }

        public virtual bool PutObject(Guid id, Object obj)
        {
            DateTime start = DateTime.Now;
#if VELOCITY_DEBUG_OUTPUT
            Debug.WriteLine(id.ToString(), "Velocity Cache Put");
#endif
            _DC.Put(id.ToString(), obj);
            DateTime end = DateTime.Now;
            Debug.WriteLine("Velocity Cache Put: cid: " + id.ToString() + ", Time: " + (end - start));

            return true;

        }

        public virtual void RemoveObject(Guid id)
        {
            try
            {
                _DC.Remove(id.ToString());
            }
            catch (DataCacheException e)
            {
                if (e.ErrorCode != DataCacheErrorCode.KeyDoesNotExist)
                    throw;
            }
        }


        #region private fields & methods

        protected virtual void Initialise()
        {
            _DC = VelocityConfiguration.CacheFactory.GetCache(VelocityConfiguration.VelocityCacheName);

#if VELOCITY_FILE_OUTPUT
            this.SetupLogSink();
            DataCacheFactory.EnableAllAvailableSinks();
#endif

        }

        protected virtual void SetupLogSink()
        {
            //create a list for the desired log sinks
            List<DataCacheLogSink> sinklist = new List<DataCacheLogSink>(2);

            //create file-based log sink, capture warnings and errors
            DataCacheLogSink fileBasedSink = new DataCacheLogSink(DataCacheSinkType.FILE,
                TraceLevel.Warning);

            //create console-based log sink, capture warnings and errors
            DataCacheLogSink consoleBasedSink = new DataCacheLogSink(DataCacheSinkType.CONSOLE,
                TraceLevel.Warning);

            //add the log sinks to the sink list
            sinklist.Add(fileBasedSink);
            sinklist.Add(consoleBasedSink);

            //enable the sinks
            DataCacheFactory.CreateLogSinks(sinklist);
        }

        #endregion


    }
}
