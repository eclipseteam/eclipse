﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using Microsoft.Data.Caching;

namespace Oritax.TaxSimp.VelocityCache
{
    public static class VelocityConfiguration
    {
        private const string IS_VELOCITY_CONFIGURED = "IsVelocityConfigured";
        private const string IS_VELOCITY_TRANSACTION_CONFIGURRED = "IsVelocityTransactionConfigured";
        private const string IS_REOUTING_CLIENT = "IsRoutingClient";
        private const string IS_LOCAL_CACHE_ENABLED = "IsLocalCacheEnabled";
        private const string IS_BLOB_PERSISTER_CACHE_ENABLED = "IsBlobPersisterCacheEnabled";
        private const string VELOCITY_SERVERS = "VelocityServers";
        private const string VELOCITY_CACHE_NAME = "VelocityCacheName";
        private const string VELOCITY_CACHE_PORT = "CachePort";
        private const string VELOCITY_CACHE_HOST_NAME = "CacheHostName";
        private const string LOCAL_CACHE_SYNC_POLICY = "LocalCacheSyncPolicy";
        private const string LOCAL_CACHE_TIME_OUT = "LocalCacheTimeOut";
        private const string POLL_INTERVAL = "PollInterval";
        private const string BLOB_PERSIST_TABLE_LIST_KEY = "BLOBPersistTableListKey";

        private const string IS_BLOB_PERSIST_TABLE_CACHED = "IsBLOBPersistTableCached";
        private const string IS_LOGICAL_CM_PERSISTER_TABLE_CACHED = "IsLogicalCMPersisterTableCached";

        private const string IS_ITL_PDS_CACHED = "IsITLPDSCached";
        private const string IS_ASYNC_MESSAGE = "IsAsyncMessage";

        public const string VELOCITY_THREAD_SLOT_NAME = "VelocityThreadSlot";
        private static DataCacheFactory _CacheFactory = null;

        public const string ITL_PDS_Key = "DS";

        public static DataCacheFactory CacheFactory
        {
            get
            {
                if (_CacheFactory == null)
                {
                    int serverCount = VelocityConfiguration.VelocityServers.GetLength(0);

                    DataCacheServerEndpoint[] servers = new DataCacheServerEndpoint[serverCount];

                    for (int i = 0; i < serverCount; i++)
                    {
                        servers[i] = new DataCacheServerEndpoint(
                            VelocityConfiguration.VelocityServers[i],
                            VelocityConfiguration.CachePort,
                            VelocityConfiguration.CacheHostName
                            );
                    }

                    //specify cache client configuration
                    _CacheFactory = new DataCacheFactory(
                        servers,
                        VelocityConfiguration.IsRoutingClient,
                        VelocityConfiguration.IsLocalCacheEnabled,
                        (DataCacheLocalCacheSyncPolicy)Enum.Parse(typeof(DataCacheLocalCacheSyncPolicy), VelocityConfiguration.LocalCacheSyncPolicy),
                        VelocityConfiguration.LocalCacheTimeOut,
                        VelocityConfiguration.PollInterval
                        );
                }

                return _CacheFactory;
            }
        }


        public static int CachePort
        {
            get
            {
                // default value 22233
                string configuredValue = ConfigurationSettings.AppSettings[VELOCITY_CACHE_PORT];
                return Convert.ToInt32(configuredValue);
            }
        }

        public static string CacheHostName
        {
            get
            {
                // default value DistributedCacheService
                string configuredValue = ConfigurationSettings.AppSettings[VELOCITY_CACHE_HOST_NAME];
                return configuredValue;
            }
        }

        public static bool IsBlobPersistTableCached
        {
            get
            {
                if (ConfigurationSettings.AppSettings[IS_BLOB_PERSIST_TABLE_CACHED] == null)
                    return false;

                string configuredValue = ConfigurationSettings.AppSettings[IS_BLOB_PERSIST_TABLE_CACHED].ToUpper();
                if (configuredValue == "TRUE" || configuredValue == "YES")
                    return true;
                else
                    return false;
            }
        }

        public static bool IsITLPDSCached
        {
            get
            {
                if (ConfigurationSettings.AppSettings[IS_ITL_PDS_CACHED] == null)
                    return false;

                string configuredValue = ConfigurationSettings.AppSettings[IS_ITL_PDS_CACHED].ToUpper();
                if (configuredValue == "TRUE" || configuredValue == "YES")
                    return true;
                else
                    return false;
            }
        }

        public static bool IsAysncMessage
        {
            get
            {
                if (ConfigurationSettings.AppSettings[IS_ASYNC_MESSAGE] == null)
                    return false;

                string configuredValue = ConfigurationSettings.AppSettings[IS_ASYNC_MESSAGE].ToUpper();
                if (configuredValue == "TRUE" || configuredValue == "YES")
                    return true;
                else
                    return false;
            }
        }

        public static bool IsLogicalCMPersisterTableCached
        {
            get
            {
                if (ConfigurationSettings.AppSettings[IS_LOGICAL_CM_PERSISTER_TABLE_CACHED] == null)
                    return false;

                string configuredValue = ConfigurationSettings.AppSettings[IS_LOGICAL_CM_PERSISTER_TABLE_CACHED].ToUpper();
                if (configuredValue == "TRUE" || configuredValue == "YES")
                    return true;
                else
                    return false;
            }
        }

        public static bool IsVelocityTransactionConfigured
        {
            get
            {
                if (ConfigurationSettings.AppSettings[IS_VELOCITY_TRANSACTION_CONFIGURRED] == null)
                    return false;

                string configuredValue = ConfigurationSettings.AppSettings[IS_VELOCITY_TRANSACTION_CONFIGURRED].ToUpper();
                if (configuredValue == "TRUE" || configuredValue == "YES")
                    return true;
                else
                    return false;
            }
        }

        public static bool IsBlobPersisterCacheEnabled
        {
            get
            {
                if (ConfigurationSettings.AppSettings[IS_BLOB_PERSISTER_CACHE_ENABLED] == null)
                    return false;

                string configuredValue = ConfigurationSettings.AppSettings[IS_BLOB_PERSISTER_CACHE_ENABLED].ToUpper();
                if (configuredValue == "TRUE" || configuredValue == "YES")
                    return true;
                else
                    return false;
            }
        }

        public static bool IsRoutingClient
        {
            get
            {
                string configuredValue = ConfigurationSettings.AppSettings[IS_REOUTING_CLIENT].ToUpper();
                if (configuredValue == "TRUE" || configuredValue == "YES")
                    return true;
                else
                    return false;
            }
        }

        public static bool IsLocalCacheEnabled
        {
            get
            {
                string configuredValue = ConfigurationSettings.AppSettings[IS_LOCAL_CACHE_ENABLED].ToUpper();
                if (configuredValue == "TRUE" || configuredValue == "YES")
                    return true;
                else
                    return false;
            }
        }

        public static bool IsVelocityConfigured
        {
            get
            {
                if (ConfigurationSettings.AppSettings[IS_VELOCITY_CONFIGURED] == null)
                    return false;

                string configuredValue = ConfigurationSettings.AppSettings[IS_VELOCITY_CONFIGURED].ToUpper();
                if (configuredValue == "TRUE" || configuredValue == "YES")
                    return true;
                else
                    return false;
            }
        }

        public static string[] VelocityServers
        {
            get
            {
                string configuredValue = ConfigurationSettings.AppSettings[VELOCITY_SERVERS];

                return configuredValue.Split(new char[] { ';' });
            }
        }

        public static string VelocityCacheName
        {
            get
            {
                string configuredValue = ConfigurationSettings.AppSettings[VELOCITY_CACHE_NAME];

                return configuredValue;
            }
        }

        public static string BLOBPersistTableListKey
        {
            get
            {
                // default value DistributedCacheService
                string configuredValue = ConfigurationSettings.AppSettings[BLOB_PERSIST_TABLE_LIST_KEY];
                return configuredValue;
            }
        }

        public static string LocalCacheSyncPolicy
        {
            get
            {
                string configuredValue = ConfigurationSettings.AppSettings[LOCAL_CACHE_SYNC_POLICY];

                return configuredValue;
            }
        }

        public static int LocalCacheTimeOut
        {
            get
            {
                string configuredValue = ConfigurationSettings.AppSettings[LOCAL_CACHE_TIME_OUT];

                return Convert.ToInt32(configuredValue);
            }
        }

        public static int PollInterval
        {
            get
            {
                string configuredValue = ConfigurationSettings.AppSettings[POLL_INTERVAL];

                return Convert.ToInt32(configuredValue);
            }
        }

        public static List<string> TypeNameExcluded = new List<string>()
        {
			"LogicalModuleBase"	,
			"OrganizationCM"	,
			"LicenceSM"	,
			"DBUser"	,
			"SAS70PasswordSM"	,
			"AustralianEntityCompanyCM"	,
        };

        public static List<string> TypeNameWithTransientPDT = new List<string>()
        {
			"LogicalModuleBase"	,
        };

        public static Dictionary<string, string> VelocityKeys = new Dictionary<string, string>()
        {
            {"ACLENTRY", "SQ_ACLENTRY"},
            {"AGGREGATION", "SQ_AGGREGATION"},
            {"AGGREGATIONVALUE", "SQ_AGGREGATIONVALUE"},
            {"ASSEMBLY", "SQ_ASSEMBLY"},
            {"ATO_CALCULATOR", "SQ_ATO_CALCULATOR"},
            {"ATO_DISCLOSUREAMOUNTS", "SQ_ATO_DISCLOSUREAMOUNTS"},
            {"ATO_INFORMATIONITEMS", "SQ_ATO_INFORMATIONITEMS"},
            {"ATO_INVALIDCONFIGURATION", "SQ_ATO_INVALIDCONFIGURATION"},
            {"ATO_NONCONSOLIDATINGITEMS", "SQ_ATO_NONCONSOLIDATINGITEMS"},
            {"ATO_PROFITITEMS", "SQ_ATO_PROFITITEMS"},
            {"ATO_TAXRECONCILIATION", "SQ_ATO_TAXRECONCILIATION"},
            {"ATO_VALIDATION", "SQ_ATO_VALIDATION"},
            {"ATTACHMENT", "SQ_ATTACHMENT"},
            {"BLOBPERSIST", "SQ_BLOBPERSIST"},
            {"BS_OtherAssets", "SQ_BS_OtherAssets"},
            {"BS_OtherLiabilities", "SQ_BS_OtherLiabilities"},
            {"CALCULATIONCM", "SQ_CALCULATIONCM"},
            {"CHARTOFACCOUNTS", "SQ_CHARTOFACCOUNTS"},
            {"CINSTANCES", "SQ_CINSTANCES"},
            {"CLMAPENTRY", "SQ_CLMAPENTRY"},
            {"CLMAPTEMPLATE", "SQ_CLMAPTEMPLATE"},
            {"CMLOGICAL", "SQ_CMLOGICAL"},
            {"CMMEMBERS", "SQ_CMMEMBERS"},
            {"CMSCENARIO", "SQ_CMSCENARIO"},
            {"COMPONENT", "SQ_COMPONENT"},
            {"COMPONENTVERSION", "SQ_COMPONENTVERSION"},
            {"CONSOLCM", "SQ_CONSOLCM"},
            {"CONSOLIDATEDTB", "SQ_CONSOLIDATEDTB"},
            {"EVENTLOG", "SQ_EVENTLOG"},
            {"FavouritesFolder", "SQ_FavouritesFolder"},
            {"FavouriteWorkpapers", "SQ_FavouriteWorkpapers"},
            {"FILEIMPORT", "SQ_FILEIMPORT"},
            {"ITL_ADJUSTMENT", "SQ_ITL_ADJUSTMENT"},
            {"ITL_CALCULATOR", "SQ_ITL_CALCULATOR"},
            {"ITL_CAPITALLOSS", "SQ_ITL_CAPITALLOSS"},
            {"ITL_FSIAMOUNT", "SQ_ITL_FSIAMOUNT"},
            {"ITL_FSILOSS", "SQ_ITL_FSILOSS"},
            {"ITL_FSITOTAL", "SQ_ITL_FSITOTAL"},
            {"ITL_TAXLOSS", "SQ_ITL_TAXLOSS"},
            {"ITL_TAXRECTOTAL", "SQ_ITL_TAXRECTOTAL"},
            {"ITL_VALIDATIONISSUE", "SQ_ITL_VALIDATIONISSUE"},
            {"JOURNAL", "SQ_JOURNAL"},
            {"JOURNALLINEITEM", "SQ_JOURNALLINEITEM"},
            {"LEDGER", "SQ_LEDGER"},
            {"LEDGERACCOUNT", "SQ_LEDGERACCOUNT"},
            {"LEDGERADJUSTMENT", "SQ_LEDGERADJUSTMENT"},
            {"MAPENTRY", "SQ_MAPENTRY"},
            {"MAPTARGET", "SQ_MAPTARGET"},
            {"MAPTARGETCATEGORY", "SQ_MAPTARGETCATEGORY"},
            {"MAPTEMPLATE", "SQ_MAPTEMPLATE"},
            {"NableReports_StatNotes_CurrentTaxAccountColumns", "SQ_NableReports_StatNotes_CurrentTaxAccountColumns"},
            {"NableReports_StatNotes_DeferredTaxAccountColumns", "SQ_NableReports_StatNotes_DeferredTaxAccountColumns"},
            {"NOTES_TABLE", "SQ_NOTES_TABLE"},
            {"OtherItems_TABLE", "SQ_OtherItems_TABLE"},
            {"ProductVersion", "SQ_ProductVersion"},
            {"ReasonForVariance_TABLE", "SQ_ReasonForVariance_TABLE"},
            {"SCENARIO", "SQ_SCENARIO"},
            {"SecurityGroup", "SQ_SecurityGroup"},
            {"STAT_NOTES_DIVIDEND_TABLE", "SQ_STAT_NOTES_DIVIDEND_TABLE"},
            {"SUBSCRIPTIONS", "SQ_SUBSCRIPTIONS"},
            {"TAXTARGETSET", "SQ_TAXTARGETSET"},
            {"TBIMPORTERRORS", "SQ_TBIMPORTERRORS"},
            {"TEA_Adjustments", "SQ_TEA_Adjustments"},
            {"TEA_AdjustmentsTotals", "SQ_TEA_AdjustmentsTotals"},
            {"TEA_AGGREGATIONVALUE", "SQ_TEA_AGGREGATIONVALUE"},
            {"TEA_CALCULATOR", "SQ_TEA_CALCULATOR"},
            {"TEA_CURRENT_REPORTING_CALCULATOR", "SQ_TEA_CURRENT_REPORTING_CALCULATOR"},
            {"TEA_DTAAmounts", "SQ_TEA_DTAAmounts"},
            {"TEA_DTAccounts", "SQ_TEA_DTAccounts"},
            {"TEA_Journals", "SQ_TEA_Journals"},
            {"TEA_JournalValues", "SQ_TEA_JournalValues"},
            {"TEA_PARAMETERS", "SQ_TEA_PARAMETERS"},
            {"TEA_PRIOR_ITR_REPORTING_CALCULATOR", "SQ_TEA_PRIOR_ITR_REPORTING_CALCULATOR"},
            {"TEA_PRIOR_REPORTING_CALCULATOR", "SQ_TEA_PRIOR_REPORTING_CALCULATOR"},
            {"TEA_PriorPeriodAdjustmentsSummary", "SQ_TEA_PriorPeriodAdjustmentsSummary"},
            {"TEA_Profit", "SQ_TEA_Profit"},
            {"TEA_ProofOfTax", "SQ_TEA_ProofOfTax"},
            {"TEA_ProofOfTaxCategory", "SQ_TEA_ProofOfTaxCategory"},
            {"TEA_ProofOfTaxItem", "SQ_TEA_ProofOfTaxItem"},
            {"TEA_TaxAccount", "SQ_TEA_TaxAccount"},
            {"TEA_TaxAccountValue", "SQ_TEA_TaxAccountValue"},
            {"TEA_TaxPayment", "SQ_TEA_TaxPayment"},
            {"TEA_TaxRate", "SQ_TEA_TaxRate"},
            {"TEA_TBSAccount", "SQ_TEA_TBSAccount"},
            {"TEA_TBSAccountAssetGLDataTable", "SQ_TEA_TBSAccountAssetGLDataTable"},
            {"TEA_TBSAccountLiabilityGLData", "SQ_TEA_TBSAccountLiabilityGLData"},
            {"TEA_TBSAccountValue", "SQ_TEA_TBSAccountValue"},
            {"TRIALBALANCE", "SQ_TRIALBALANCE"},
            {"UserDetail", "SQ_UserDetail"},
            {"UserDetail_SecurityGroup", "SQ_UserDetail_SecurityGroup"},
            {"VERSIONHISTORY", "SQ_VERSIONHISTORY"},
        };

    }
}
