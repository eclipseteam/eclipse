﻿//#define VELOCITY_DEBUG_OUTPUT
//#define VELOCITY_FILE_OUTPUT

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Microsoft.Data.Caching;

using System.Diagnostics;

namespace Oritax.TaxSimp.VelocityCache
{
    /// <summary>
    /// Attention:-
    /// 1. Only create one instance VelocityCacheString for each thread, as Initialise() takes long time.
    /// 2. Don't make VelocityCacheString static, as there's no concurrency control inside VelocityCacheString.
    /// </summary>
    public class VelocityCacheString
    {

        private static VelocityCacheString velocityCache = new VelocityCacheString();
        private static VelocityCacheString velocityCacheTimer = new VelocityCacheString();

        public static VelocityCacheString VelocityCache
        {
            get { return velocityCache; }
        }

        public static VelocityCacheString VelocityCacheTimer
        {
            get { return velocityCacheTimer; }
        }

        protected DataCache _DC;
        protected DataCacheLockHandle _VelocityLockHandle;

        public VelocityCacheString()
        {
            Initialise();
        }

        public virtual object GetObejct(string id)
        {
            Object obj;

            obj = _DC.Get(id);

#if VELOCITY_DEBUG_OUTPUT
            Debug.WriteLineIf(obj == null, id, "Velocity Cache Miss");
            Debug.WriteLineIf(obj != null, id, "Velocity Cache Hit");
#endif
            return obj;
        }

        public virtual bool PutObject(string id, Object obj)
        {

#if VELOCITY_DEBUG_OUTPUT
            Debug.WriteLine(id, "Velocity Cache Put");
#endif
            _DC.Put(id, obj);

            return true;

        }

        public virtual void RemoveObject(string id)
        {
            try
            {
                _DC.Remove(id);
            }
            catch (DataCacheException e)
            {
                if (e.ErrorCode != DataCacheErrorCode.KeyDoesNotExist)
                    throw;
            }
        }

        public bool GetAndLock(string key)
        {
            bool isSuccessful = true;

            try
            {
                object o = this._DC.GetAndLock(key, TimeSpan.MaxValue, out this._VelocityLockHandle);
            }
            catch (DataCacheException e)
            {
                if (e.ErrorCode == DataCacheErrorCode.ObjectLocked)
                    isSuccessful = false;
            }

            return isSuccessful;
        }

        public void Unlock(string key)
        {
            if (this._VelocityLockHandle != null)
            {
                this._DC.Unlock(key, this._VelocityLockHandle);
                this._VelocityLockHandle = null;
            }
        }


        #region private fields & methods

        protected virtual void Initialise()
        {
            _DC = VelocityConfiguration.CacheFactory.GetCache(VelocityConfiguration.VelocityCacheName);

#if VELOCITY_FILE_OUTPUT
            this.SetupLogSink();
            DataCacheFactory.EnableAllAvailableSinks();
#endif

        }

        protected virtual void SetupLogSink()
        {
            //create a list for the desired log sinks
            List<DataCacheLogSink> sinklist = new List<DataCacheLogSink>(2);

            //create file-based log sink, capture warnings and errors
            DataCacheLogSink fileBasedSink = new DataCacheLogSink(DataCacheSinkType.FILE,
                TraceLevel.Warning);

            //create console-based log sink, capture warnings and errors
            DataCacheLogSink consoleBasedSink = new DataCacheLogSink(DataCacheSinkType.CONSOLE,
                TraceLevel.Warning);

            //add the log sinks to the sink list
            sinklist.Add(fileBasedSink);
            sinklist.Add(consoleBasedSink);

            //enable the sinks
            DataCacheFactory.CreateLogSinks(sinklist);
        }

        #endregion


    }
}
