﻿//#define VELOCITY_DEBUG_OUTPUT
//#define VELOCITY_FILE_OUTPUT

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Microsoft.Data.Caching;

using System.Diagnostics;

namespace Oritax.TaxSimp.VelocityCache
{
    /// <summary>
    /// Attention:-
    /// 1. Only create one instance VelocityDataCacheWithConcurrencyControl for each thread, as Initialise() takes long time.
    /// 2. Don't make VelocityDataCacheWithConcurrencyControl static, as there's no concurrency control inside VelocityDataCacheWithConcurrencyControl.
    /// </summary>
    public class VelocityDataCacheWithConcurrencyControl
    {
        protected Dictionary<Guid, DataCacheItemVersion> _VersionDictionary = new Dictionary<Guid, DataCacheItemVersion>();
        protected DataCache _DC;

        public VelocityDataCacheWithConcurrencyControl()
        {
            Initialise();
        }

        public virtual object GetObejct(Guid id)
        {
            Object obj = null;

            try
            {
                obj = _DC.Get(id.ToString());
            }
            catch (DataCacheException e)
            {
                if (e.ErrorCode == DataCacheErrorCode.KeyDoesNotExist)
                    return null;
                else
                    throw;
            }

            if (obj != null)
                UpdateVersionDict(id);

#if VELOCITY_DEBUG_OUTPUT
            Debug.WriteLineIf(obj == null, id.ToString(), "Velocity Cache Miss");
            Debug.WriteLineIf(obj != null, id.ToString(), "Velocity Cache Hit");
#endif
            return obj;
        }

        public virtual bool PutObject(Guid id, Object obj)
        {

#if VELOCITY_DEBUG_OUTPUT
            Debug.WriteLine(id.ToString(), "Velocity Cache Put");
#endif
            if (_VersionDictionary.ContainsKey(id))
                return this.UpdateExistingEntry(id, obj, _VersionDictionary[id]);
            else
                return this.InsertNewEntry(id, obj);

        }

        public virtual void RemoveObject(Guid id)
        {
            if (_VersionDictionary.ContainsKey(id))
                _VersionDictionary.Remove(id);

            try
            {
                _DC.Remove(id.ToString());
            }
            catch (DataCacheException e)
            {
                if (e.ErrorCode != DataCacheErrorCode.KeyDoesNotExist)
                    throw;
            }

        }


        #region private fields & methods

        protected virtual bool InsertNewEntry(Guid id, Object obj)
        {
            if (_DC.Get(id.ToString()) == null)
            {
                DataCacheItemVersion newItemVersion = _DC.Put(id.ToString(), obj);

                if (newItemVersion == null)
                    throw new Exception("Velocity Cache Temporary Error: Save operation failed! Please try again.");
                else
                {
                    _VersionDictionary[id] = newItemVersion;
                    return true;
                }
            }
            else
            {
                throw new Exception("Velocity Cache Error: Item can't be saved, as it's already existed.");
            }

        }

        protected virtual bool UpdateExistingEntry(Guid id, Object obj, DataCacheItemVersion oldVersion)
        {
            try
            {
                DataCacheItemVersion newItemVersion = _DC.Put(id.ToString(), obj, oldVersion);
                if (newItemVersion == null)
                    return false;
                else
                {
                    _VersionDictionary[id] = newItemVersion;
                    return true;
                }
            }
            catch (DataCacheException e)
            {
                _VersionDictionary.Remove(id);

                if (e.ErrorCode == DataCacheErrorCode.CacheItemVersionMismatch)
                    return false;
                else
                    throw;
            }
        }

        protected virtual void UpdateVersionDict(Guid id)
        {
            if (!_VersionDictionary.ContainsKey(id))
            {
                DataCacheItem cacheItem = _DC.GetCacheItem(id.ToString());

                if (cacheItem != null)
                {
                    DataCacheItemVersion cacheItemVersion = cacheItem.Version;
                    _VersionDictionary[id] = cacheItemVersion;
                }
            }

        }

        protected virtual void Initialise()
        {
            _DC = VelocityConfiguration.CacheFactory.GetCache(VelocityConfiguration.VelocityCacheName);

#if VELOCITY_FILE_OUTPUT
            this.SetupLogSink();
            DataCacheFactory.EnableAllAvailableSinks();
#endif

        }

        protected virtual void SetupLogSink()
        {
            //create a list for the desired log sinks
            List<DataCacheLogSink> sinklist = new List<DataCacheLogSink>(2);

            //create file-based log sink, capture warnings and errors
            DataCacheLogSink fileBasedSink = new DataCacheLogSink(DataCacheSinkType.FILE,
                TraceLevel.Warning);

            //create console-based log sink, capture warnings and errors
            DataCacheLogSink consoleBasedSink = new DataCacheLogSink(DataCacheSinkType.CONSOLE,
                TraceLevel.Warning);

            //add the log sinks to the sink list
            sinklist.Add(fileBasedSink);
            sinklist.Add(consoleBasedSink);

            //enable the sinks
            DataCacheFactory.CreateLogSinks(sinklist);
        }

        #endregion


    }
}
