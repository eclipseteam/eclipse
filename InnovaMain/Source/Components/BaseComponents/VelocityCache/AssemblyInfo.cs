﻿using System.Reflection;
using System.Runtime.CompilerServices;

using Oritax.TaxSimp.VelocityCache;
using Oritax.TaxSimp.InstallInfo;

[assembly: AssemblyTitle("")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("")]
[assembly: AssemblyCopyright("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: AssemblyVersion(InstallVelocityCache.ASSEMBLY_MAJORVERSION + "." + InstallVelocityCache.ASSEMBLY_MINORVERSION)]
[assembly: AssemblyInformationalVersion(InstallVelocityCache.ASSEMBLY_MAJORVERSION + "." + InstallVelocityCache.ASSEMBLY_MINORVERSION + "." + InstallVelocityCache.ASSEMBLY_DATAFORMAT + "." + InstallVelocityCache.ASSEMBLY_REVISION)]
[assembly: AssemblyInstallInfo(InstallVelocityCache.ASSEMBLY_ID, InstallVelocityCache.ASSEMBLY_NAME, InstallVelocityCache.ASSEMBLY_DISPLAYNAME)]
[assembly: AssemblyDelaySign(false)]
[assembly: AssociatedFileInstallInfoAttribute
    (
    new string[]
    {
        InstallVelocityCache.ASSOCIATED_FILE_VELOCITY_DLL_BASE,
        InstallVelocityCache.ASSOCIATED_FILE_VELOCITY_DLL_CLIENT,
        InstallVelocityCache.ASSOCIATED_FILE_VELOCITY_DLL_FABRIC,
        InstallVelocityCache.ASSOCIATED_FILE_VELOCITY_DLL_CASBASE,
    }, 

    new string[]
    {
	    "bin",
	    "bin",
	    "bin",
	    "bin",
    }
    )
]
