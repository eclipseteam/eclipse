using System;

namespace Oritax.TaxSimp.Calculation
{
	/// <summary>
	/// Summary description for BrokerManagedComponentBaseFW.
	/// </summary>
	public class LogicalModuleInstall
	{
		#region INSTALLATION PROPERTIES

		// Assembly Installation Properties
		public const string ASSEMBLY_ID="568DFC5D-372E-47CB-A88E-CEEAE745887C";
		public const string ASSEMBLY_NAME="LogicalModule_1_1";
		public const string ASSEMBLY_DISPLAYNAME="LogicalModule V1.1";
		public const string ASSEMBLY_MAJORVERSION="1";
		public const string ASSEMBLY_MINORVERSION="1";
		public const string ASSEMBLY_DATAFORMAT="0";	//2005.1
		//public const string ASSEMBLY_REVISION="0";	//2004.3.1
		//public const string ASSEMBLY_REVISION="1";	//2004.3.2
//		public const string ASSEMBLY_REVISION="2";		//2005.1
		public const string ASSEMBLY_REVISION="3";		//2006.1

	
		// Component Installation Properties
		public const string COMPONENT_ID="DB489B76-49E4-4a6d-942C-9F59061080D8";
		public const string COMPONENT_NAME="LogicalModule";
		public const string COMPONENT_DISPLAYNAME="Logical Module";
		public const string COMPONENT_CATEGORY="Base";

		// Component Version Installation Properties
		public const string COMPONENTVERSION_STARTDATE="01/01/1990 00:00:00 AM";
		public const string COMPONENTVERSION_ENDDATE="31/12/2050 12:59:59 AM";
		public const string COMPONENTVERSION_PERSISTERASSEMBLY="LogicalModule_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";
		public const string COMPONENTVERSION_PERSISTERCLASS="Oritax.TaxSimp.Calculation.LogicalCMPersister";
		public const string COMPONENTVERSION_IMPLEMENTATIONCLASS="Oritax.TaxSimp.Calculation.LogicalModuleBase";

		// Data Model
		public const string DATAMODEL_MANAGERASSEMBLY="LogicalModule_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";
		public const string DATAMODEL_MANAGERCLASS="Oritax.TaxSimp.Calculation.LogicalCMPersister";

		#endregion
	}
}
