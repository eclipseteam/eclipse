using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.DSCalculation;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.Calculation
{
	/// <summary>
	/// Summary description for CalculationTypePersist.
	/// </summary>
	public class LCMListPersist: IDisposable
	{
		#region Fields -----------------------------------------------------------------------------
		private SqlDataAdapter dsCommand;
		protected SqlConnection sqlConnection;
		protected SqlTransaction sqlTransaction;
		private bool disposed = false;
		#endregion

		#region Properties -------------------------------------------------------------------------
		
		public virtual SqlConnection Connection
		{
			set
			{
				sqlConnection=value;
				this.dsCommand.InsertCommand.Connection=value;
				this.dsCommand.SelectCommand.Connection=value;
				this.dsCommand.UpdateCommand.Connection=value;
				this.dsCommand.DeleteCommand.Connection=value;
			}
		}

		public virtual SqlTransaction Transaction
		{
			set
			{
				sqlTransaction=value;
				this.dsCommand.InsertCommand.Transaction=value;
				this.dsCommand.SelectCommand.Transaction=value;
				this.dsCommand.UpdateCommand.Transaction=value;
				this.dsCommand.DeleteCommand.Transaction=value;
			}
		}
		#endregion

		#region Constructors -----------------------------------------------------------------------
		public LCMListPersist(SqlConnection connection)
		{
			sqlConnection = connection;
			sqlTransaction=null;

			//
			// Create the adapter
			//
			this.dsCommand = new SqlDataAdapter();
			//
			// Create the DataSetCommand SelectCommand
			//
			this.dsCommand.SelectCommand = DBCommandFactory.Instance.NewSqlCommand();
			this.dsCommand.SelectCommand.CommandType = CommandType.Text;
			this.dsCommand.SelectCommand.Connection=sqlConnection;

			this.dsCommand.UpdateCommand = DBCommandFactory.Instance.NewSqlCommand();
			this.dsCommand.UpdateCommand.CommandType = CommandType.Text;
			this.dsCommand.UpdateCommand.Connection=this.dsCommand.SelectCommand.Connection;

			this.dsCommand.InsertCommand = DBCommandFactory.Instance.NewSqlCommand();
			this.dsCommand.InsertCommand.CommandType = CommandType.Text;
			this.dsCommand.InsertCommand.Connection=this.dsCommand.SelectCommand.Connection;

			this.dsCommand.DeleteCommand = DBCommandFactory.Instance.NewSqlCommand();
			this.dsCommand.DeleteCommand.CommandType = CommandType.Text;
			this.dsCommand.DeleteCommand.Connection=this.dsCommand.SelectCommand.Connection;

			this.dsCommand.TableMappings.Add("Table", DSLogicalModulesInfo.LCMLIST_TABLE);
		}
		#endregion

		#region Public Methods ---------------------------------------------------------------------------
		#region IDisposable Implementation ---------------------------------------

		/// <summary>
		///		Performs application-defined tasks associated with freeing, releasing, 
		///		or resetting unmanaged resources.
		/// </summary>
		public void Dispose() 
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		/// <summary>
		///		Performs application-defined tasks associated with freeing, releasing, 
		///		or resetting unmanaged resources.
		/// </summary>
		/// <param name="disposing">
		///		True if called by the Dispose() method, false if called by the finalizer.
		/// </param>
		protected void Dispose(bool disposing) 
		{
			if (!disposed)
			{
				if (disposing)
				{
					dsCommand.Dispose();
				}
				disposed = true;
			}
		}

		#endregion // IDisposable Implementation


		public DataSet FindByTypeId(Guid typeId)
		{
			this.dsCommand.SelectCommand.CommandText = "SELECT CMLOGICAL.CLID,CMLOGICAL.NAME,CMLOGICAL.CTID,CMLOGICAL.PLID,CMLOGICAL.CURRCSID,COMPONENT.NAME AS TYPENAME,COMPONENT.CATEGORY AS CATEGORYNAME "
				+"FROM CMLOGICAL,COMPONENTVERSION,COMPONENT "
				+"WHERE CMLOGICAL.CTID=COMPONENTVERSION.ID "
				+"AND COMPONENTVERSION.COMPONENTID=COMPONENT.ID "
				+"AND CMLOGICAL.CTID='{"+typeId.ToString()+"}'";

			DSLogicalModulesInfo   data    = new DSLogicalModulesInfo();
			this.dsCommand.Fill(data);
			return data;
		}

		public DataSet FindByParentId(Guid parentCLID)
		{
			this.dsCommand.SelectCommand.Parameters.Clear();
			this.dsCommand.SelectCommand.Parameters.AddWithValue("@PARENTCLID", typeof(System.Guid)).Value = parentCLID;
			this.dsCommand.SelectCommand.CommandText = "SELECT CINSTANCES.NAME as INSTANCENAME, COMPONENT.DISPLAYNAME, CINSTANCES.CID, CMLOGICAL.CLID,CMLOGICAL.NAME,CMLOGICAL.CTID,CMLOGICAL.PLID,CMLOGICAL.CURRCSID,COMPONENT.NAME AS TYPENAME,COMPONENT.CATEGORY AS CATEGORYNAME FROM CMLOGICAL,COMPONENTVERSION,COMPONENT, CMSCENARIO, CINSTANCES WHERE CMLOGICAL.CTID=COMPONENTVERSION.ID AND COMPONENTVERSION.COMPONENTID=COMPONENT.ID AND CMLOGICAL.CLID = CMSCENARIO.CLID AND CMSCENARIO.CIID = CINSTANCES.CID AND CMLOGICAL.PLID=@PARENTCLID";

			DSLogicalModulesInfo data = new DSLogicalModulesInfo();
			this.dsCommand.Fill(data);
			return data;
		}

		public DataSet FindByParentId(Guid parentCLID, Guid parentCSID)
		{
			this.dsCommand.SelectCommand.Parameters.Clear();
            this.dsCommand.SelectCommand.Parameters.AddWithValue("@PARENTCLID", typeof(System.Guid)).Value = parentCLID;
            this.dsCommand.SelectCommand.Parameters.AddWithValue("@PARENTCSID", typeof(System.Guid)).Value = parentCSID;

			this.dsCommand.SelectCommand.CommandText = "SELECT CMLOGICAL.CLID,CMLOGICAL.NAME,CMLOGICAL.CTID,CMLOGICAL.PLID,CMLOGICAL.CURRCSID,COMPONENT.NAME AS TYPENAME,COMPONENT.CATEGORY AS CATEGORYNAME FROM CMLOGICAL,CMSCENARIO,COMPONENTVERSION,COMPONENT WHERE CMLOGICAL.CTID=COMPONENTVERSION.ID AND COMPONENTVERSION.COMPONENTID=COMPONENT.ID AND CMLOGICAL.CLID=CMSCENARIO.CLID AND CMLOGICAL.PLID=@PARENTCLID AND CMSCENARIO.CSID=@PARENTCSID";
			
			DSLogicalModulesInfo data = new DSLogicalModulesInfo();
			this.dsCommand.Fill(data);
			return data;
		}

		public DataSet FindByTypeName(string typeName)
		{
			this.dsCommand.SelectCommand.Parameters.Clear();
            this.dsCommand.SelectCommand.Parameters.AddWithValue("@TYPENAME", typeof(string)).Value = typeName;
			this.dsCommand.SelectCommand.CommandText = "SELECT CMLOGICAL.CLID,CMLOGICAL.NAME,CMLOGICAL.CTID,CMLOGICAL.PLID,CMLOGICAL.CURRCSID,COMPONENT.NAME AS TYPENAME,COMPONENT.CATEGORY AS CATEGORYNAME FROM CMLOGICAL,COMPONENTVERSION,COMPONENT WHERE CMLOGICAL.CTID=COMPONENTVERSION.ID AND COMPONENTVERSION.COMPONENTID=COMPONENT.ID AND CMTYPES.TYPENAME=@TYPENAME";

			DSLogicalModulesInfo data = new DSLogicalModulesInfo();
			this.dsCommand.Fill(data);
			return data;
		}

		public DataSet FindByInstanceID(Guid instanceId)
		{
			this.dsCommand.SelectCommand.Parameters.Clear();
            this.dsCommand.SelectCommand.Parameters.AddWithValue("@INSTANCEID", typeof(System.Guid)).Value = instanceId;
			this.dsCommand.SelectCommand.CommandText = "SELECT CMLOGICAL.CLID,CMLOGICAL.NAME,CMLOGICAL.CTID,CMLOGICAL.PLID,CMLOGICAL.CURRCSID,COMPONENT.NAME AS TYPENAME,COMPONENT.CATEGORY AS CATEGORYNAME FROM CMLOGICAL,COMPONENTVERSION,COMPONENT WHERE CMLOGICAL.CTID=COMPONENTVERSION.ID AND COMPONENTVERSION.COMPONENTID=COMPONENT.ID AND CMLOGICAL.CLID=@INSTANCEID";

			DSLogicalModulesInfo data = new DSLogicalModulesInfo();
			this.dsCommand.Fill(data);
			return data;
		}

		public DataSet FindLCMsByCIID(Guid cIID)
		{
			this.dsCommand.SelectCommand.Parameters.Clear();
            this.dsCommand.SelectCommand.Parameters.AddWithValue("@CIID", typeof(System.Guid)).Value = cIID;
			this.dsCommand.SelectCommand.CommandText = "SELECT DISTINCT CMLOGICAL.CLID,CMLOGICAL.NAME,CMLOGICAL.CTID,CMLOGICAL.PLID,CMLOGICAL.CURRCSID,COMPONENT.NAME AS TYPENAME,COMPONENT.CATEGORY AS CATEGORYNAME FROM CMLOGICAL,CMSCENARIO,COMPONENTVERSION,COMPONENT WHERE CMLOGICAL.CTID=COMPONENTVERSION.ID AND COMPONENTVERSION.COMPONENTID=COMPONENT.ID AND CMLOGICAL.CLID=CMSCENARIO.CLID AND CMSCENARIO.CIID=@CIID";

			DSLogicalModulesInfo data = new DSLogicalModulesInfo();
			this.dsCommand.Fill(data.Tables[DSLogicalModulesInfo.LCMLIST_TABLE]);
			return data;
		}

		public DataSet FindAll()
		{
			this.dsCommand.SelectCommand.CommandText = "SELECT CMLOGICAL.CLID,CMLOGICAL.NAME,CMLOGICAL.CTID,CMLOGICAL.PLID,CMLOGICAL.CURRCSID,COMPONENT.NAME AS TYPENAME,COMPONENT.CATEGORY AS CATEGORYNAME FROM CMLOGICAL,COMPONENTVERSION,COMPONENT WHERE CMLOGICAL.CTID=COMPONENTVERSION.ID AND COMPONENTVERSION.COMPONENTID=COMPONENT.ID";

			DSLogicalModulesInfo data = new DSLogicalModulesInfo();
			this.dsCommand.Fill(data);
			return data;
		}

		public virtual void EstablishDatabase(){}
		public virtual void ClearDatabase(){}
		public virtual void RemoveDatabase(){}
	}
	#endregion
}

