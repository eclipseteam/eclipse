using System;
using System.Data;

using Oritax.TaxSimp.CalculationInterface;

namespace Oritax.TaxSimp.Calculation
{
	/// <summary>
	/// DSCalculationModule is a DataSet that contains all of the persistent data for a CM instance.
	/// </summary>
	[Serializable] 
	public class LogicalModuleBaseDS : BrokerManagedComponentDS
	{
		public const string CMLOGICAL_TABLE				= "CMLOGICAL";			// Logical CMs of this persisted CM
		public const string LC_CLID_FIELD				= "CLID";				// CM Logical ID
		public const string LC_NAME_FIELD				= "NAME";				// Logical CM Name
		public const string LC_CURSCENARIO_FIELD		= "CURRCSID";			// Logical CM Current Scenario
		public const string LC_TYPEID_FIELD				= "CTID";				// Logical CM Type Identifier
		public const string LC_PARENTID_FIELD			= "PLID";				// Logical CM Category Name
		public const string LC_BSCONSOLIDATIONMODE_FIELD= "BSCONSOLIDATIONMODE";// Logical CM Business structure consolidation mode
		public const string LC_BSPERIODTYPE_FIELD		= "BSPERIODTYPE";		// Logical CM business structure period type
		public const string LC_BSCONSOL_FIELD			= "BSCONSOL";			// Logical CM business structure consolidation type, e.g. TEA only or ITL and TEA
		public const string LC_BSOUTYPE_FIELD			= "BSOUTYPE";			// Logical CM business strutcure organisation unit type

		public const string TYPEINFO_TABLE				= "TYPEINFO";
		public const string TI_TYPENAME_FIELD			= "TYPENAME";
		public const string TI_CATEGORYNAME_FIELD		= "CATEGORYNAME";

		public const string CMSCENARIO_TABLE			= "CMSCENARIOVIEW";	// CM scenarios in memory table view across multiple persisted tables.
		public const string SC_TYPEID_FIELD				= "CMTYPEID";		// CM TypeID
		public const string SC_TYPENAME_FIELD			= "TYPENAME";		// CM Type Name
		
		public const string CMSCENARIO_PERSISTED_TABLE	= "CMSCENARIO";		// CM scenarios of this persisted CM
		public const string SC_ID_FIELD					= "ID";				// Scenario primary key ID
		public const string SC_CLID_FIELD				= "CLID";			// CM Logical ID
		public const string SC_CSID_FIELD				= "CSID";			// CM Scenario ID
		public const string SC_CIID_FIELD				= "CIID";			// Implementation Physical CM ID.
		public const string SC_CTID_FIELD				= "CTID";			// Implementation Physical CM ID.
		
		public const string SCENARIO_TABLE				= "SCENARIO";		// CM scenarios of this persisted CM
		public const string SC_NAME_FIELD				= "NAME";			// Scenario Name
		public const string SC_TYPE_FIELD				= "TYPE";			// Scenario type.
		public const string SC_STATUS_FIELD				= "STATUS";			// Scenario status.
		public const string SC_CREATIONDATETIME_FIELD	= "CREATED";		// Date and time of creation
		public const string SC_MODIFICATIONDATE_TIME	= "MODIFIED";		// Date and time of last modification
		public const string SC_LOCKED					= "LOCKED";			// Locked Status (true if locked)
				
		public const string CHILDLCM_TABLE				= "CHILDLCM";		// child LCMs of this persisted CM
		public const string CLCM_CLID_FIELD				= "CLID";			// Child CM Logical ID

		public const string OWNERLCM_TABLE				= "OWNERLCM";		// Owner LCMs of this persisted CM
		public const string OWNERCM_ID_FIELD			= "ID";		// Owner CM Logical ID
		public const string OWNERCM_CLID_FIELD			= "PARENTCLID";		// Owner CM Logical ID
		public const string OWNERCM_CSID_FIELD			= "PARENTCSID";		// Owner CM Scenario ID
		public const string MEMBERCM_CSID_FIELD			= "MEMBERCSID";		// Member CM Scenario ID
	
		public const string SUBSCRIPTIONS_TABLE			= "SUBSCRIPTIONS";	// Subscriptions of this persisted CM
		public const string SUB_SBID_FIELD				= "SBID";			// Subscription ID
		public const string SUB_CLID_FIELD				= "CLID";			// Logical CM ID
		public const string SUB_DATATYPE_FIELD			= "DATATYPE";		// Logical CM Name
		public const string SUB_SUBSCRIBERCLID_FIELD	= "SUBSCRIBERCLID";	// Subscriber CLID
		public const string SUB_SUBSCRIBERCSID_FIELD	= "SUBSCRIBERCSID";	// Subscriber CSID
		public const string SUB_SUBSCRIBEDCLID_FIELD	= "SUBSCRIBEDCLID";	// Subscribed CLID
		public const string SUB_SUBSCRIBEDCSID_FIELD	= "SUBSCRIBEDCSID";	// Subscribed CSID
		public const string SUB_CONSOLIDATION_FIELD		= "CONSOLIDATION";	// indicator if the subscription is a consolidation subscription
		public const string SUB_PARAMETERS_FIELD		= "PARAMETERS";		// indicator if the subscription is a consolidation subscription

		public LogicalModuleBaseDS() : base()
		{
			PDSDataTable logicalModuleTable = this.DefineRootTable(
				CMLOGICAL_TABLE,
				new FieldDefinition[]{ 
										new FieldDefinition(LC_CLID_FIELD ,typeof(System.Guid),false),
										new FieldDefinition(LC_NAME_FIELD ,typeof(System.String),false),
										new FieldDefinition(LC_CURSCENARIO_FIELD ,typeof(System.Guid),false),
										new FieldDefinition(LC_TYPEID_FIELD ,typeof(System.Guid),false),
										new FieldDefinition(LC_PARENTID_FIELD ,typeof(System.Guid),false),
										new FieldDefinition(LC_BSCONSOLIDATIONMODE_FIELD ,typeof(System.Int32),true),
										new FieldDefinition(LC_BSPERIODTYPE_FIELD ,typeof(System.Int32),true),	
										new FieldDefinition(LC_BSCONSOL_FIELD ,typeof(System.Int32),true),	
										new FieldDefinition(LC_BSOUTYPE_FIELD ,typeof(System.Int32),true),	
									 
									 },
				LC_CLID_FIELD,
				PersistenceTypeOption.READWRITE);

            logicalModuleTable.Indexes.Add(new DatabaseIndex("IX_CMLOGICAL", false, false, true, new DataColumn[] { logicalModuleTable.Columns[LC_PARENTID_FIELD], logicalModuleTable.Columns[LC_CLID_FIELD] }));
            logicalModuleTable.Indexes.Add(new DatabaseIndex("IX_CMLOGICAL_1", false, false, true, new DataColumn[] { logicalModuleTable.Columns[LC_PARENTID_FIELD] }));
            logicalModuleTable.Indexes.Add(new DatabaseIndex("IX_CMLOGICAL_2", false, false, true, new DataColumn[] { logicalModuleTable.Columns[LC_TYPEID_FIELD], logicalModuleTable.Columns[LC_PARENTID_FIELD], logicalModuleTable.Columns[LC_CLID_FIELD], logicalModuleTable.Columns[LC_NAME_FIELD], logicalModuleTable.Columns[LC_CURSCENARIO_FIELD] }));
            logicalModuleTable.Indexes.Add(new DatabaseIndex("IX_CMLOGICAL_3", false, false, true, new DataColumn[] { logicalModuleTable.Columns[LC_CLID_FIELD] }));

            logicalModuleTable.Statistics.Add(new DatabaseStatistic("statistic_cmlogical_clid_ctid", new DataColumn[] { logicalModuleTable.Columns[LC_CLID_FIELD], logicalModuleTable.Columns[LC_TYPEID_FIELD] }));

			this.DefineChildTable(
				CMSCENARIO_PERSISTED_TABLE,
				new FieldDefinition[]{
										 new FieldDefinition(SC_ID_FIELD ,typeof(System.Guid),false),
										 new FieldDefinition(SC_CLID_FIELD,typeof(System.Guid),false),
										 new FieldDefinition(SC_CSID_FIELD,typeof(System.Guid),false),
										 new FieldDefinition(SC_CIID_FIELD,typeof(System.Guid),false),
										 new FieldDefinition(SC_CTID_FIELD,typeof(System.Guid),false), 
				},
				SC_ID_FIELD,
				PersistenceTypeOption.READWRITE,
				CMLOGICAL_TABLE,
				LC_CLID_FIELD,
				SC_CLID_FIELD);

			PDSDataTable scenarioTable = this.DefineChildTable(
				SCENARIO_TABLE,
				new FieldDefinition[]{
										 new FieldDefinition(SC_CSID_FIELD,typeof(System.Guid),false),
										 new FieldDefinition(SC_NAME_FIELD ,typeof(System.String),false),
										 new FieldDefinition(SC_TYPE_FIELD,typeof(System.Int32),false),
										 new FieldDefinition(SC_STATUS_FIELD,typeof(System.Int32),false),
										 new FieldDefinition(SC_CREATIONDATETIME_FIELD,typeof(System.DateTime),false),
										 new FieldDefinition(SC_MODIFICATIONDATE_TIME,typeof(System.DateTime),false),
										 new FieldDefinition(SC_LOCKED,typeof(System.Boolean),false),
										
			},
				SC_CSID_FIELD,
				PersistenceTypeOption.READWRITE,
				CMLOGICAL_TABLE);

            scenarioTable.PrimaryKeyIndexIsClustered = false;
            scenarioTable.Indexes.Add(new DatabaseIndex("IX_SCENARIO", true, true, true, new DataColumn[] { scenarioTable.Columns[SC_CSID_FIELD] }));
			
			PDSDataTable subscriptionsTable = this.DefineChildTable(
				SUBSCRIPTIONS_TABLE,
				new FieldDefinition[]{
										 new FieldDefinition(SUB_SBID_FIELD ,typeof(System.Guid),false),
										 new FieldDefinition(SUB_CLID_FIELD ,typeof(System.Guid),false),
										 new FieldDefinition(SUB_DATATYPE_FIELD ,typeof(System.String),true),
										 new FieldDefinition(SUB_SUBSCRIBERCLID_FIELD ,typeof(System.Guid),false),
										 new FieldDefinition(SUB_SUBSCRIBERCSID_FIELD,typeof(System.Guid),false),
										 new FieldDefinition(SUB_SUBSCRIBEDCSID_FIELD,typeof(System.Guid),false),
										 new FieldDefinition(SUB_SUBSCRIBEDCLID_FIELD,typeof(System.Guid),false),
										 new FieldDefinition(SUB_CONSOLIDATION_FIELD,typeof(System.Boolean),false),
										 new FieldDefinition(SUB_PARAMETERS_FIELD,typeof(System.Byte[]),true),
			},
				SUB_SBID_FIELD,
				PersistenceTypeOption.READWRITE,
				CMLOGICAL_TABLE,
				LC_CLID_FIELD,
				SUB_CLID_FIELD);

            subscriptionsTable.Indexes.Add(new DatabaseIndex("IX_SUBSCRIPTIONS_1", false, false, true, new DataColumn[] { subscriptionsTable.Columns[SUB_CLID_FIELD] }));

			PDSDataTable cmScenarioTable = this.DefineChildTable(
				CMSCENARIO_TABLE,
				new FieldDefinition[]{
										 new FieldDefinition(SC_ID_FIELD ,typeof(System.Guid),false),
										 new FieldDefinition(SC_NAME_FIELD ,typeof(System.String),false),
										 new FieldDefinition(SC_TYPEID_FIELD ,typeof(System.Guid),false),
										 new FieldDefinition(SC_TYPENAME_FIELD ,typeof(System.String),false),
										 new FieldDefinition(SC_CLID_FIELD,typeof(System.Guid),false),
										 new FieldDefinition(SC_CSID_FIELD,typeof(System.Guid),false),
										 new FieldDefinition(SC_CIID_FIELD,typeof(System.Guid),false),
										 new FieldDefinition(SC_TYPE_FIELD,typeof(System.Int32),false),
										 new FieldDefinition(SC_STATUS_FIELD,typeof(System.Int32),false),
										 new FieldDefinition(SC_CREATIONDATETIME_FIELD,typeof(System.DateTime),false),
										 new FieldDefinition(SC_MODIFICATIONDATE_TIME,typeof(System.DateTime),false),
										 new FieldDefinition(SC_LOCKED,typeof(System.Boolean),false),
			},
				SC_ID_FIELD,
				PersistenceTypeOption.READONLY,
				CMLOGICAL_TABLE,
				LC_CLID_FIELD,
				SC_CLID_FIELD);

            cmScenarioTable.Indexes.Add(new DatabaseIndex("IX_CMSCENARIO_1", false, false, true, new DataColumn[] { cmScenarioTable.Columns[SC_CLID_FIELD] }));
            cmScenarioTable.Indexes.Add(new DatabaseIndex("IX_CMSCENARIO_2", false, false, true, new DataColumn[] { cmScenarioTable.Columns[SC_CLID_FIELD], cmScenarioTable.Columns[SC_CSID_FIELD] }));

			this.DefineChildTable(
				CHILDLCM_TABLE,
				new FieldDefinition[]{
										 new FieldDefinition(CLCM_CLID_FIELD ,typeof(System.Guid),false),
			},
				CLCM_CLID_FIELD,
				PersistenceTypeOption.READONLY,
				CMLOGICAL_TABLE);

			this.DefineChildTable(
				OWNERLCM_TABLE,
				new FieldDefinition[]{
										new FieldDefinition(OWNERCM_ID_FIELD ,typeof(System.Guid),false),
										new FieldDefinition(OWNERCM_CLID_FIELD ,typeof(System.Guid),false),
										new FieldDefinition(OWNERCM_CSID_FIELD ,typeof(System.Guid),false),
										new FieldDefinition(MEMBERCM_CSID_FIELD ,typeof(System.Guid),false),
			},
				OWNERCM_ID_FIELD,
				PersistenceTypeOption.READONLY,
				CMLOGICAL_TABLE);

			this.DefineChildTable(
				TYPEINFO_TABLE,
				new FieldDefinition[]{
//										 new FieldDefinition(TI_TYPENAME_FIELD ,typeof(System.Guid),false),
//										 new FieldDefinition(TI_CATEGORYNAME_FIELD ,typeof(System.Guid),false),
		
										 new FieldDefinition(TI_TYPENAME_FIELD ,typeof(System.String),false),
										 new FieldDefinition(TI_CATEGORYNAME_FIELD ,typeof(System.String),false),
			},
				TI_TYPENAME_FIELD,
				PersistenceTypeOption.READONLY,
				CMLOGICAL_TABLE);
		}
	}
}

