using System;
using System.Data;

namespace Oritax.TaxSimp.DSCalculation
{
	/// <summary>
	/// DSCalculationModule is a DataSet that contains all of the persistent data for a CM instance.
	/// </summary>
	[System.ComponentModel.DesignerCategory("Code")]
	[SerializableAttribute] 
	public class DSLogicalModulesInfo : DataSet
	{
		public const string LCMLIST_TABLE				= "LCMLIST_TABLE";
		public const string LCM_CLID_FIELD				= "CLID";
		public const string LCM_NAME_FIELD				= "NAME";
        public const string LCM_INSTANCENAME_FIELD = "INSTANCENAME";
        public const string LCM_COMPDISPLAYNAME_FIELD = "DISPLAYNAME";
        public const string LCM_CID_FIELD = "CID";

		public const string LCM_TYPEID_FIELD			= "CTID";
		public const string LCM_PARENTID_FIELD			= "PLID";
		public const string LCM_TYPENAME_FIELD			= "TYPENAME";
		public const string LCM_CATEGORYNAME_FIELD		= "CATEGORYNAME";
		public const string LCM_CURRENTCSID_FIELD		= "CURRCSID";
		public const string LC_BSCONSOLIDATIONMODE_FIELD= "BSCONSOLIDATIONMODE";// Logical CM Business structure consolidation mode
		public const string LC_BSPERIODTYPE_FIELD		= "BSPERIODTYPE";		// Logical CM business structure period type
		public const string LC_BSCONSOL_FIELD			= "BSCONSOL";			// Logical CM business structure consolidation type, e.g. TEA only or ITL and TEA
		public const string LC_BSOUTYPE_FIELD			= "BSOUTYPE";			// Logical CM business strutcure organisation unit type

		public DSLogicalModulesInfo()
		{
			DataTable table;
			DataColumnCollection columns;

			// Add the table for the instance data
			table = new DataTable(LCMLIST_TABLE);
			columns = table.Columns;
			columns.Add(LCM_CLID_FIELD, typeof(System.Guid));
			columns.Add(LCM_NAME_FIELD, typeof(System.String));
			columns.Add(LCM_TYPEID_FIELD, typeof(System.Guid));
			columns.Add(LCM_PARENTID_FIELD, typeof(System.Guid));
			columns.Add(LCM_TYPENAME_FIELD, typeof(System.String));
			columns.Add(LCM_CATEGORYNAME_FIELD, typeof(System.String));
			columns.Add(LCM_CURRENTCSID_FIELD, typeof(Guid));
			columns.Add(LC_BSCONSOLIDATIONMODE_FIELD, typeof(System.String));
			columns.Add(LC_BSPERIODTYPE_FIELD, typeof(System.String));
			columns.Add(LC_BSCONSOL_FIELD, typeof(System.String));
			columns.Add(LC_BSOUTYPE_FIELD, typeof(System.String));
			DataColumn[] lcPrimaryKey={table.Columns[LCM_CLID_FIELD]};
			table.PrimaryKey=lcPrimaryKey;

			this.Tables.Add(table);
		}
	}
}
