using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Data;
using System.Collections;
using System.Reflection;
using System.Runtime.Serialization;
using System.Security.Permissions;

using Oritax.TaxSimp.BusinessStructureUtilities;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.DSCalculation;
using Oritax.TaxSimp.Exceptions;
using Oritax.TaxSimp.Calculation;
//using Oritax.TaxSimp.Services.CMBroker;

using System.Collections.Generic;

namespace Oritax.TaxSimp.Calculation
{
    /// <summary>
    /// 
    /// </summary>
    /// <remarks>
    ///	This class is sealed because DeliverData calculates a new Token which should
    ///	only be done in leaf classes in the inheritance chain.		
    ///</remarks>
    [Serializable]
    public sealed class LogicalModuleBase : BrokerManagedComponent, ILogicalModule, ISerializable
    {
        public class LogicalModuleException : Exception
        {
            public LogicalModuleException(string message) : base(message) { }
        }


        [Serializable]
        public class CMScenarios : SerializableHashtable, ISerializable
        {
            [NonSerialized]
            protected SerializableHashtable deletedScenarios = new SerializableHashtable();

            public CMScenarios()
                : base()
            {
                deletedScenarios = new SerializableHashtable();
            }

            protected CMScenarios(SerializationInfo si, StreamingContext context)
                : base(si, context)
            {
                try
                {
                    deletedScenarios = (SerializableHashtable)si.GetValue("cms_deletedScenarios", typeof(SerializableHashtable));
                }
                catch (SerializationException)
                {
                    deletedScenarios = new SerializableHashtable();
                }

            }

            //Only allow the .NET Serialization core code to call this function
            [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
            public override void GetObjectData(SerializationInfo si, StreamingContext context)
            {
                base.GetObjectData(si, context);

                Serialize.AddSerializedValue(si, "cms_deletedScenarios", deletedScenarios);

            }

            public CMScenario this[Guid cSID]
            {
                get
                {
                    return (CMScenario)base[cSID];
                }
                set
                {
                    base[cSID] = value;
                }
            }

            private void Remove(Guid cSID)
            {
                base.Remove(cSID);
            }

            public void SetDelete(Guid cSID)
            {
                CMScenario cMScenario = (CMScenario)base[cSID];
                if (null != cMScenario)
                {
                    base.Remove(cSID);
                    deletedScenarios.Add(cSID, cMScenario);
                }
            }

            public new int Count { get { return NumNotDeleted(); } }

            public int CountMainScenarios { get { return NumMainScenarios(); } }

            private int NumNotDeleted()
            {
                return (base.Count);
            }

            private int NumMainScenarios()
            {
                int intNumMain = 0;
                foreach (DictionaryEntry dictionaryEntry in this)
                {
                    CMScenario cMScenario = (CMScenario)dictionaryEntry.Value;
                    if (cMScenario.Type == ScenarioType.Standard)
                        intNumMain++;
                }
                return (intNumMain);
            }

            public void CommitChanges()
            {
                deletedScenarios.Clear();
            }

            public void OnGetData(LogicalModuleBaseDS dSLogicalModule)
            {
                foreach (DictionaryEntry dictionaryEntry in this)
                {
                    CMScenario cmScenario = (CMScenario)dictionaryEntry.Value;
                    DataRow cmScenarioRow = dSLogicalModule.Tables[LogicalModuleBaseDS.CMSCENARIO_TABLE].NewRow();
                    ExtractCMScenarioRowData(cmScenario, cmScenarioRow);
                    dSLogicalModule.Tables[LogicalModuleBaseDS.CMSCENARIO_TABLE].Rows.Add(cmScenarioRow);

                    cmScenarioRow = dSLogicalModule.Tables[LogicalModuleBaseDS.CMSCENARIO_PERSISTED_TABLE].NewRow();
                    ExtractPersistedCMScenarioRowData(cmScenario, cmScenarioRow);
                    dSLogicalModule.Tables[LogicalModuleBaseDS.CMSCENARIO_PERSISTED_TABLE].Rows.Add(cmScenarioRow);

                    cmScenarioRow = dSLogicalModule.Tables[LogicalModuleBaseDS.SCENARIO_TABLE].NewRow();
                    ExtractPersistedScenarioRowData(cmScenario, cmScenarioRow);
                    dSLogicalModule.Tables[LogicalModuleBaseDS.SCENARIO_TABLE].Rows.Add(cmScenarioRow);

                }
            }

            private void ExtractCMScenarioRowData(CMScenario cmScenario, DataRow cmScenarioRow)
            {
                cmScenarioRow[LogicalModuleBaseDS.SC_CSID_FIELD] = cmScenario.CSID;
                cmScenarioRow[LogicalModuleBaseDS.SC_CLID_FIELD] = cmScenario.CLID;
                cmScenarioRow[LogicalModuleBaseDS.SC_CIID_FIELD] = cmScenario.CIID;
                cmScenarioRow[LogicalModuleBaseDS.SC_NAME_FIELD] = cmScenario.Name;
                cmScenarioRow[LogicalModuleBaseDS.SC_STATUS_FIELD] = cmScenario.Status.ToInt();
                cmScenarioRow[LogicalModuleBaseDS.SC_TYPE_FIELD] = cmScenario.Type.ToInt();
                cmScenarioRow[LogicalModuleBaseDS.SC_CREATIONDATETIME_FIELD] = cmScenario.CreationDateTime;
                cmScenarioRow[LogicalModuleBaseDS.SC_MODIFICATIONDATE_TIME] = cmScenario.ModificationDateTime;
                //SB 1/6/2004 Added fields missing
                cmScenarioRow[LogicalModuleBaseDS.SC_TYPEID_FIELD] = cmScenario.CMTypeID;
                cmScenarioRow[LogicalModuleBaseDS.SC_TYPENAME_FIELD] = cmScenario.CMTypeName;
                cmScenarioRow[LogicalModuleBaseDS.SC_LOCKED] = cmScenario.Locked;
            }

            private void ExtractPersistedCMScenarioRowData(CMScenario cmScenario, DataRow cmScenarioRow)
            {
                cmScenarioRow[LogicalModuleBaseDS.SC_ID_FIELD] = cmScenario.ID;
                cmScenarioRow[LogicalModuleBaseDS.SC_CSID_FIELD] = cmScenario.CSID;
                cmScenarioRow[LogicalModuleBaseDS.SC_CLID_FIELD] = cmScenario.CLID;
                cmScenarioRow[LogicalModuleBaseDS.SC_CIID_FIELD] = cmScenario.CIID;
                cmScenarioRow[LogicalModuleBaseDS.SC_CTID_FIELD] = cmScenario.CMTypeID;
            }

            private void ExtractPersistedScenarioRowData(CMScenario cmScenario, DataRow cmScenarioRow)
            {
                cmScenarioRow[LogicalModuleBaseDS.SC_CSID_FIELD] = cmScenario.CSID;
                //cmScenarioRow[LogicalModuleBaseDS.SC_OWNING_CLID_FIELD]=cmScenario.CLID;
                cmScenarioRow[LogicalModuleBaseDS.SC_NAME_FIELD] = cmScenario.Name;
                cmScenarioRow[LogicalModuleBaseDS.SC_TYPE_FIELD] = cmScenario.Type.ToInt();
                cmScenarioRow[LogicalModuleBaseDS.SC_STATUS_FIELD] = cmScenario.Status.ToInt();
                cmScenarioRow[LogicalModuleBaseDS.SC_CREATIONDATETIME_FIELD] = cmScenario.CreationDateTime;
                cmScenarioRow[LogicalModuleBaseDS.SC_MODIFICATIONDATE_TIME] = cmScenario.ModificationDateTime;
                cmScenarioRow[LogicalModuleBaseDS.SC_LOCKED] = cmScenario.Locked;
            }
        }

        [Serializable]
        public class OwnerLCM
        {
            public Guid MemberScenario;
            public Guid OwnerScenario;
            public Guid OwnerCLID;

            public OwnerLCM(Guid memberScenario, Guid ownerScenario, Guid ownerCLID)
            {
                MemberScenario = memberScenario;
                OwnerScenario = ownerScenario;
                OwnerCLID = ownerCLID;
            }
        }

        /// <summary>
        /// Holds a set of owners that hold the logical CM in one of its scenarios.
        /// </summary>
        [Serializable]
        public class OwnedMemberScenario : SerializableList
        {
            public void Add(OwnerLCM ownerLCM)
            {
                base.Add(ownerLCM);
            }

            public new OwnerLCM this[int index]
            {
                get
                {
                    return (OwnerLCM)base[index];
                }
            }

            public void Remove(OwnerLCM ownerLCM)
            {
                base.Remove(ownerLCM);
            }
        }

        /// <summary>
        /// Represents a collection of owners for a given logical module
        /// </summary>
        /// <example>A group logical module will own the child entity logical module if it is a member of the group</example>
        [Serializable]
        public class Owners : SerializableHashtable, ISerializable
        {
            [Serializable]
            public class DuplicateMemberScenarioException : Exception
            {
                public DuplicateMemberScenarioException(Guid memberScenarioID)
                    : base("Attempted duplicate member scenario: " + memberScenarioID.ToString()) { }
            }

            /// <summary>
            /// Adds an owner to the logical module
            /// </summary>
            /// <param name="owner"></param>
            /// <example>A group logical module will own the child entity logical module</example>
            public void Add(OwnerLCM owner)
            {
                if (base.Contains(owner.MemberScenario))
                {
                    OwnedMemberScenario ownedMemberScenario = (OwnedMemberScenario)base[owner.MemberScenario];
                    ownedMemberScenario.Add(owner);
                }
                else
                {
                    OwnedMemberScenario ownedMemberScenario = new OwnedMemberScenario();
                    ownedMemberScenario.Add(owner);
                    base.Add(owner.MemberScenario, ownedMemberScenario);
                }
            }

            /// <summary>
            /// Removes an owner from the logical module
            /// </summary>
            /// <param name="owner">The owner to remove</param>
            public void Remove(OwnerLCM owner)
            {
                if (owner == null)
                    throw new ArgumentNullException("owner");

                if (base.Contains(owner.MemberScenario))
                {
                    OwnedMemberScenario ownedMemberScenario = (OwnedMemberScenario)base[owner.MemberScenario];
                    ownedMemberScenario.Remove(owner);

                    if (ownedMemberScenario.Count == 0)
                        base.Remove(owner.MemberScenario);
                }
                else
                    throw new InvalidOperationException("Unable to remove the owner as the member scenario id: " + owner.MemberScenario + " does not exist");
            }

            /// <summary>
            /// Gets the OwnedMemberScenario for the given ID
            /// </summary>
            public OwnedMemberScenario this[Guid memberScenario]
            {
                get
                {
                    return (OwnedMemberScenario)base[memberScenario];
                }
            }
        }

        //USING CUSTOM SERIALIZATION.
        //Still mark the fields as [NonSerialized] for your own infomation but this is ignored now by .NET
        //You must add your own serialzation code for each instance
        //See GetObjectData & Special Constructor

        SubscriptionsList subscriptions; // An arraylist of Subscription objects.
        [NonSerialized]
        SerializableList deletedSubscriptions = new SerializableList(); // An arraylist of deleted Subscription objects.

        Guid cmType;
        string cmTypeName;
        string cmCategory;
        Guid currentScenario;
        CMScenarios cmScenarios = new CMScenarios();

        // The parent and children CLID's.  These define the LCM's place in the business structure hierarchy.
        Guid parentCLID;
        SerializableList childCLIDs = new SerializableList();

        // The owner CLID's.  This defines the LCM's place in the group, entity ownership hierarchy.
        Owners owners = new Owners();

        ILogicalModule[] childLCMs = null;
        BusinessStructureContext m_objContext;

        [NonSerialized]
        private bool scenarioLockOverridden;

        private LogicalModuleBase(SerializationInfo si, StreamingContext context)
            : base(si, context)
        {
            subscriptions = (SubscriptionsList)Serialize.GetSerializedValue(si, "lmb_subscriptions", typeof(SubscriptionsList), new SubscriptionsList());
            cmType = Serialize.GetSerializedGuid(si, "lmb_cmType");
            cmTypeName = Serialize.GetSerializedString(si, "lmb_cmTypeName");
            cmCategory = Serialize.GetSerializedString(si, "lmb_cmCategory");
            currentScenario = Serialize.GetSerializedGuid(si, "lmb_currentScenario");
            cmScenarios = (CMScenarios)Serialize.GetSerializedValue(si, "lmb_cmScenarios", typeof(CMScenarios), new CMScenarios());
            parentCLID = Serialize.GetSerializedGuid(si, "lmb_parentCLID");
            childCLIDs = (SerializableList)Serialize.GetSerializedValue(si, "lmb_childCLIDs", typeof(SerializableList), new SerializableList());
            childLCMs = (ILogicalModule[])Serialize.GetSerializedValue(si, "lmb_childLCMs", typeof(ILogicalModule[]), null);
            m_objContext = (BusinessStructureContext)Serialize.GetSerializedValue(si, "lmb_m_objContext", typeof(BusinessStructureContext), null);

            try
            {
                deletedSubscriptions = (SerializableList)si.GetValue("lmb_deletedSubscriptions", typeof(SerializableList));
            }
            catch (SerializationException)
            {
                deletedSubscriptions = new SerializableList();
            }
            scenarioLockOverridden = Serialize.GetSerializedBool(si, "lmb_scenarioLockOverridden");
        }

        //Only allow the .NET Serialization core code to call this function
        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo si, StreamingContext context)
        {
            base.GetObjectData(si, context);

            Serialize.AddSerializedValue(si, "lmb_subscriptions", subscriptions);
            Serialize.AddSerializedValue(si, "lmb_cmType", cmType);
            Serialize.AddSerializedValue(si, "lmb_cmTypeName", cmTypeName);
            Serialize.AddSerializedValue(si, "lmb_cmCategory", cmCategory);
            Serialize.AddSerializedValue(si, "lmb_currentScenario", currentScenario);
            Serialize.AddSerializedValue(si, "lmb_cmScenarios", cmScenarios);
            Serialize.AddSerializedValue(si, "lmb_parentCLID", parentCLID);
            Serialize.AddSerializedValue(si, "lmb_childCLIDs", childCLIDs);
            Serialize.AddSerializedValue(si, "lmb_childLCMs", childLCMs);
            Serialize.AddSerializedValue(si, "lmb_m_objContext", m_objContext);

            Serialize.AddSerializedValue(si, "lmb_deletedSubscriptions", deletedSubscriptions);
            Serialize.AddSerializedValue(si, "lmb_scenarioLockOverridden", scenarioLockOverridden);
        }

        public LogicalModuleBase()
            : base()
        {
            subscriptions = new SubscriptionsList();
            deletedSubscriptions = new SerializableList();
        }

        public LogicalModuleBase(Guid clid, Guid cMTypeId, String cMTypeName, Guid initialCSID)
            : this()
        {
            if (initialCSID != Guid.Empty)
            {
                cmScenarios[initialCSID] = new CMScenario("Initial",
                    clid,
                    initialCSID,
                    ScenarioType.Standard,
                    Guid.Empty,
                    ScenarioStatus.Final,
                    cMTypeId,
                    cMTypeName,
                    DateTime.Now,
                    DateTime.Now,
                    false);
                currentScenario = initialCSID;
            }

            this.typeID = new Guid(LogicalModuleInstall.ASSEMBLY_ID);
        }

        public ICalculationModule this[Guid cSID]
        {
            get
            {
                CMScenario cMScenario = cmScenarios[cSID];

                if (cMScenario != null)
                {
                    Guid cIID = cmScenarios[cSID].CIID;

                    ICalculationModule iCM = (ICalculationModule)this.Broker.GetBMCInstance(cIID);

                    if (iCM == null)
                        return null;
                    else
                    {
                        iCM.CLID = this.CLID;
                        iCM.CSID = cSID;

                        return iCM;
                    }
                }
                else
                    return null;
            }
        }

        public void DivergeScenario(ICalculationModule iCM)
        {
            if (this.IsShared(iCM.CSID))
            {
                ((IBrokerManagedComponent)iCM).OnCopyInstance();
                ((IBrokerManagedComponent)iCM).CalculateToken(true);
                ((CMScenario)this.cmScenarios[iCM.CSID]).CIID = iCM.CIID;
                this.CalculateToken(true);
            }
        }

        /// <summary>
        /// Returns the lock status of the requested scenario of this logical CM.  If any ancestor CM (parent etc.) has this scenario locked,
        /// then this scenario is considered locked.
        /// </summary>
        /// <param name="cSID">CSID of the scenario for which the lock status is required.</param>
        /// <returns>True if the scenario is locked.</returns>
        public bool IsScenarioLocked(Guid cSID)
        {
            CMScenario cMScenario = this.cmScenarios[cSID];
            if (cMScenario != null)
            {
                if (cMScenario.Locked)
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Returns true if any scenario within the logical module is locked.
        /// </summary>
        /// <returns></returns>
        public bool ContainsLockedScenario()
        {
            foreach (DictionaryEntry dictEntry in this.cmScenarios)
            {
                CMScenario cMScenario = (CMScenario)dictEntry.Value;
                if (this.IsScenarioLocked(cMScenario.CSID))
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Return the locked status as true if the scenario is a member of a consolidated group structure
        /// which is locked at any level.
        /// </summary>
        /// <param name="cSID"></param>
        /// <returns></returns>
        public bool IsConsolidationOwnerLocked(Guid cSID)
        {
            OwnedMemberScenario ownedMemberScenario = this.owners[cSID];

            if (null == ownedMemberScenario)
                return false;

            foreach (OwnerLCM ownerLCM in ownedMemberScenario)
            {
                Guid ownerCLID = ownerLCM.OwnerCLID;
                Guid ownerCSID = ownerLCM.OwnerScenario;

                ILogicalModule parentLM = this.Broker.GetLogicalCM(ownerCLID);

                try
                {
                    if (parentLM != null && parentLM.IsScenarioLocked(ownerCSID))
                        return true;
                }
                finally
                {
                    this.Broker.ReleaseBrokerManagedComponent(parentLM);
                }
            }
            return false;
        }

        /// <summary>
        /// Lock the specified scenario in this module and all children
        /// </summary>
        /// <param name="scenarioID">The ID of the scenario to lock</param>
        public void LockScenario(Guid scenarioID)
        {
            CMScenario scenario = this.cmScenarios[scenarioID];

            if (scenario != null)
                scenario.Locked = true;

            // lock each child module also
            foreach (ILogicalModule childLCM in this.ChildLCMs)
            {
                childLCM.LockScenario(scenarioID);
                this.Broker.ReleaseBrokerManagedComponent(childLCM);
            }

            this.CalculateToken(true);
        }

        /// <summary>
        /// Unlock the specified scenario in this module and all children
        /// </summary>
        /// <param name="scenarioID">The ID of the scenario to unlock</param>
        public void UnLockScenario(Guid scenarioID)
        {
            CMScenario scenario = this.cmScenarios[scenarioID];

            if (scenario != null)
                scenario.Locked = false;

            // unlock each child module also
            foreach (ILogicalModule childLCM in this.ChildLCMs)
                childLCM.UnLockScenario(scenarioID);

            this.CalculateToken(true);
        }

        /// <summary>
        /// Return the CLID of the reporting unit's logical module. Climbs up the business structure
        /// tree until the current logicals reporting unit parent is found.
        /// </summary>
        /// <returns></returns>
        private Guid DetermineReportingUnitCLID()
        {
            ILogicalModule logicalModule = this;
            ICalculationModule organizationCM = this.Broker.GetWellKnownCM(WellKnownCM.Organization);

            try
            {
                while (logicalModule.ParentCLID != Guid.Empty && logicalModule.ParentCLID != organizationCM.CLID)
                {
                    logicalModule = this.Broker.GetLogicalCM(logicalModule.ParentCLID);
                    if (logicalModule.ParentCLID == Guid.Empty)
                        break;
                }
                return logicalModule.CLID;
            }
            finally
            {
                this.Broker.ReleaseBrokerManagedComponent(organizationCM);
                this.Broker.ReleaseBrokerManagedComponent(logicalModule);
            }
        }

        public bool ScenarioLockOverridden
        {
            get { return scenarioLockOverridden; }
            set { scenarioLockOverridden = value; }
        }

        public IEnumerable Scenarios
        {
            get { return cmScenarios; }
        }

        public Guid CMType
        {
            get { return this.cmType; }
            set { this.cmType = value; }
        }

        public string CMTypeName
        {
            get { return this.cmTypeName; }
            set { this.cmTypeName = value; }
        }

        public string CMCategory
        {
            get { return this.cmCategory; }
            set { this.cmCategory = value; }
        }

        public Guid CLID
        {
            get { return this.cID; }
        }

        public Guid CurrentScenario
        {
            get { return currentScenario; }
            set { currentScenario = value; }
        }

        public Guid ParentCLID
        {
            get { return parentCLID; }
            set { parentCLID = value; }
        }

        public ArrayList ChildCLIDs
        {
            get { return childCLIDs.ToArrayList(); }
        }

        public ILogicalModule ParentLCM
        {
            get { return this.Broker.GetLogicalCM(parentCLID); }
        }

        /// <summary>
        /// Returns a collection of child logical modules
        /// </summary>
        /// <remarks>Remember to release these modules to avoid leaking
        /// objects in the cache</remarks>
        public ILogicalModule[] ChildLCMs
        {
            get
            {
                ArrayList childModules = new ArrayList();

                foreach (Guid childID in this.ChildCLIDs)
                    childModules.Add(this.Broker.GetLogicalCM(childID));

                return (ILogicalModule[])childModules.ToArray(typeof(ILogicalModule));
            }
        }

        public override DataSet PrimaryDataSet
        {
            get
            {

                if (CMTypeName != TypeName && this.Broker != null && this.Broker.MigrationInProgress)
                {
                    // need to get a transient instance, to see if the physical instance is migratable
                    // if not then we shouldn't migrate the logical module
                    IBrokerManagedComponent objInstance = this.Broker.CreateTransientComponentInstance(this.CMType);

                    if (objInstance == null || objInstance.PrimaryDataSet == null)
                        return null;
                }
                return new LogicalModuleBaseDS();
            }
        }

        public BusinessStructureContext CurrentBusinessStructureContext
        {
            get
            {
                return this.m_objContext;
            }
            set
            {
                this.m_objContext = value;
            }
        }

        /// <summary>
        /// Gets a CMScenario for a given scenario ID
        /// </summary>
        /// <param name="scenarioID">The scenario ID to get</param>
        /// <returns>The CMScenario for the specified ID</returns>
        public CMScenario GetCMScenario(Guid scenarioID)
        {
            return this.cmScenarios[scenarioID];
        }

        public ICalculationModule GetParentCM(Guid cSID)
        {
            return (ICalculationModule)null;
        }

        public ArrayList GetChildCMs(Guid cSID)
        {
            ArrayList result = new ArrayList();
            foreach (Guid gdCLID in this.ChildCLIDs)
            {
                ILogicalModule logicalCM = this.Broker.GetLogicalCM(gdCLID);

                if (logicalCM != null)
                {
                    foreach (DictionaryEntry dictionaryEntry in logicalCM.Scenarios)
                    {
                        CMScenario cMScenario = (CMScenario)dictionaryEntry.Value;
                        if (cMScenario.CSID == cSID)
                        {
                            result.Add(cMScenario);
                            break;
                        }
                    }

                    this.Broker.ReleaseBrokerManagedComponent(logicalCM);
                }
                else
                    this.Broker.LogEvent(EventType.ErrorPrompt, Guid.Empty, "Logical CM was null ID: " + gdCLID.ToString());
            }
            return result;
        }

        /// <summary>
        /// Gets a list of CMScenarios that are children of this particular logical modules
        /// </summary>
        /// <param name="cSID">The scenario to return items for</param>
        /// <returns>A list of CMScenarios</returns>
        public List<CMScenario> GetChildCMScenarios(Guid cSID)
        {
            List<CMScenario> result = new List<CMScenario>(this.childCLIDs.Count);

            foreach (Guid gdCLID in this.ChildCLIDs)
            {
                ILogicalModule logicalCM = this.Broker.GetLogicalCM(gdCLID);

                foreach (DictionaryEntry dictionaryEntry in logicalCM.Scenarios)
                {
                    CMScenario cMScenario = (CMScenario)dictionaryEntry.Value;
                    if (cMScenario.CSID == cSID)
                    {
                        result.Add(cMScenario);
                        break;
                    }
                }

                this.Broker.ReleaseBrokerManagedComponent(logicalCM);
            }

            return result;
        }

        public ArrayList GetChildCMsByType(Guid gdTypeId, Guid cSID)
        {
            ArrayList result = new ArrayList();
            foreach (Guid gdCLID in this.ChildCLIDs)
            {
                ILogicalModule logicalCM = this.Broker.GetLogicalCM(gdCLID);

                if (logicalCM.CMType == gdTypeId)
                {
                    foreach (DictionaryEntry dictionaryEntry1 in logicalCM.Scenarios)
                    {
                        CMScenario cMScenario = (CMScenario)dictionaryEntry1.Value;

                        if (cMScenario.CSID == cSID)
                            result.Add(dictionaryEntry1.Value);
                    }
                }

                this.Broker.ReleaseBrokerManagedComponent(logicalCM);
            }
            return result;
        }

        public CMScenario GetScenario(Guid cSID)
        {
            return cmScenarios[cSID];
        }

        /// <summary>
        /// Returns an array list of the subscriptions that have not been marked for deletion
        /// </summary>
        public SubscriptionsList Subscriptions
        {
            get { return subscriptions; }
        }

        public Guid GetCIID(Guid cSID)
        {
            CMScenario cMScenario = cmScenarios[cSID];
            if (null != cMScenario)
                return cMScenario.CIID;
            else
                return Guid.Empty;
        }

        #region OnGetData Methods

        private void OnGetData(DSLogicalModulesInfo dSLogicalModulesInfo)
        {
            DataRow logicalCMRow = dSLogicalModulesInfo.Tables[DSLogicalModulesInfo.LCMLIST_TABLE].NewRow();

            logicalCMRow[DSLogicalModulesInfo.LCM_CLID_FIELD] = this.CID;
            logicalCMRow[DSLogicalModulesInfo.LCM_NAME_FIELD] = this.Name;
            logicalCMRow[DSLogicalModulesInfo.LCM_PARENTID_FIELD] = this.parentCLID;
            logicalCMRow[DSLogicalModulesInfo.LCM_TYPEID_FIELD] = this.typeID;
            logicalCMRow[DSLogicalModulesInfo.LCM_TYPENAME_FIELD] = this.TypeName;
            dSLogicalModulesInfo.Tables[DSLogicalModulesInfo.LCMLIST_TABLE].Rows.Add(logicalCMRow);
        }

        protected override void OnGetData(DataSet data)
        {
            base.OnGetData(data);
            if (data is LogicalModuleBaseDS)
            {
                LogicalModuleBaseDS dSLogicalModule = data as LogicalModuleBaseDS;
                DataRow logicalCMRow = dSLogicalModule.Tables[LogicalModuleBaseDS.CMLOGICAL_TABLE].NewRow();

                logicalCMRow[LogicalModuleBaseDS.LC_CLID_FIELD] = this.CID;
                logicalCMRow[LogicalModuleBaseDS.LC_TYPEID_FIELD] = this.cmType;
                logicalCMRow[LogicalModuleBaseDS.LC_NAME_FIELD] = this.name;
                logicalCMRow[LogicalModuleBaseDS.LC_CURSCENARIO_FIELD] = this.currentScenario;
                logicalCMRow[LogicalModuleBaseDS.LC_PARENTID_FIELD] = this.parentCLID;

                if (this.m_objContext != null)
                {
                    logicalCMRow[LogicalModuleBaseDS.LC_BSCONSOLIDATIONMODE_FIELD] = (int)this.m_objContext.Consolidation;
                    logicalCMRow[LogicalModuleBaseDS.LC_BSPERIODTYPE_FIELD] = (int)this.m_objContext.Period;
                    logicalCMRow[LogicalModuleBaseDS.LC_BSCONSOL_FIELD] = (int)this.m_objContext.ConsolMode;
                    logicalCMRow[LogicalModuleBaseDS.LC_BSOUTYPE_FIELD] = (int)this.m_objContext.OrganisationUnitType;
                }

                dSLogicalModule.Tables[LogicalModuleBaseDS.CMLOGICAL_TABLE].Rows.Add(logicalCMRow);

                cmScenarios.OnGetData(dSLogicalModule);

                foreach (Subscription subscription in this.subscriptions)
                {
                    DataRow subscriptionRow = dSLogicalModule.Tables[LogicalModuleBaseDS.SUBSCRIPTIONS_TABLE].NewRow();
                    subscriptionRow[LogicalModuleBaseDS.SUB_SBID_FIELD] = subscription.ID;
                    subscriptionRow[LogicalModuleBaseDS.SUB_CLID_FIELD] = this.CLID;
                    subscriptionRow[LogicalModuleBaseDS.SUB_DATATYPE_FIELD] = subscription.DataType;
                    subscriptionRow[LogicalModuleBaseDS.SUB_SUBSCRIBERCLID_FIELD] = subscription.SubscriberCLID;
                    subscriptionRow[LogicalModuleBaseDS.SUB_SUBSCRIBERCSID_FIELD] = subscription.SubscriberCSID;
                    subscriptionRow[LogicalModuleBaseDS.SUB_SUBSCRIBEDCLID_FIELD] = subscription.SubscribedCLID;
                    subscriptionRow[LogicalModuleBaseDS.SUB_SUBSCRIBEDCSID_FIELD] = subscription.SubscribedCSID;
                    subscriptionRow[LogicalModuleBaseDS.SUB_CONSOLIDATION_FIELD] = subscription.Consolidation;

                    if (subscription.Parameters != null)
                        subscriptionRow[LogicalModuleBaseDS.SUB_PARAMETERS_FIELD] = this.SerializeParameters(subscription.Parameters);
                    dSLogicalModule.Tables[LogicalModuleBaseDS.SUBSCRIPTIONS_TABLE].Rows.Add(subscriptionRow);
                }

                foreach (Guid gdChildCLID in this.childCLIDs)
                {
                    DataRow childLCMRow = dSLogicalModule.Tables[LogicalModuleBaseDS.CHILDLCM_TABLE].NewRow();
                    childLCMRow[LogicalModuleBaseDS.CLCM_CLID_FIELD] = gdChildCLID;
                    dSLogicalModule.Tables[LogicalModuleBaseDS.CHILDLCM_TABLE].Rows.Add(childLCMRow);
                }

                foreach (DictionaryEntry dictionaryEntry in this.owners)
                {
                    OwnedMemberScenario ownerMemberScenario = (OwnedMemberScenario)dictionaryEntry.Value;
                    foreach (OwnerLCM ownerLCM in ownerMemberScenario)
                    {
                        DataRow ownerLCMRow = dSLogicalModule.Tables[LogicalModuleBaseDS.OWNERLCM_TABLE].NewRow();
                        ownerLCMRow[LogicalModuleBaseDS.OWNERCM_CLID_FIELD] = ownerLCM.OwnerCLID;
                        ownerLCMRow[LogicalModuleBaseDS.OWNERCM_CSID_FIELD] = ownerLCM.OwnerScenario;
                        ownerLCMRow[LogicalModuleBaseDS.MEMBERCM_CSID_FIELD] = ownerLCM.MemberScenario;
                        dSLogicalModule.Tables[LogicalModuleBaseDS.OWNERLCM_TABLE].Rows.Add(ownerLCMRow);
                    }
                }
            }
            else if (data is LogicalModuleBaseDS)
            {
                this.OnGetData((LogicalModuleBaseDS)data);
            }
            else if (data is DSLogicalModulesInfo)
            {
                this.OnGetData((DSLogicalModulesInfo)data);
            }
        }


        #endregion

        private byte[] SerializeParameters(object[] objParameters)
        {
            MemoryStream memoryStream = new MemoryStream();
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(memoryStream, objParameters);

            return memoryStream.ToArray();
        }

        private object[] DeserializeParameters(byte[] objParameters)
        {

            MemoryStream memoryStream = new MemoryStream(objParameters);
            BinaryFormatter binaryFormatter = new BinaryFormatter();

            return (object[])binaryFormatter.Deserialize(memoryStream);
        }

        public void SetPersistenceData(DataSet data)
        {
            if (data is LogicalModuleBaseDS)
            {
                DeliverPrimaryData(data);
            }

            //	CalculateToken(true);
        }

        protected override ModifiedState OnSetData(DataSet data)
        {
            base.OnSetData(data);
            if (data is LogicalModuleBaseDS)
            {
                DeliverPrimaryData((LogicalModuleBaseDS)data);
            }
            return ModifiedState.UNKNOWN;
        }

        private ModifiedState OnSetData(LogicalModuleBaseDS logicalModuleDS)
        {
            DeliverPrimaryData(logicalModuleDS);
            return ModifiedState.UNKNOWN;
        }

        private new void DeliverPrimaryData(DataSet data)
        {
            LogicalModuleBaseDS dSLogicalModule = (LogicalModuleBaseDS)data;


            if (dSLogicalModule.Tables[LogicalModuleBaseDS.CMLOGICAL_TABLE].Rows.Count == 1)
            {
                DataRow logicalCMRow = dSLogicalModule.Tables[LogicalModuleBaseDS.CMLOGICAL_TABLE].Rows[0];
                DataRow typeInfoRow = dSLogicalModule.Tables[LogicalModuleBaseDS.TYPEINFO_TABLE].Rows[0];

                this.CID = (Guid)logicalCMRow[LogicalModuleBaseDS.LC_CLID_FIELD];
                this.cmType = (Guid)logicalCMRow[LogicalModuleBaseDS.LC_TYPEID_FIELD];

                this.cmTypeName = typeInfoRow[LogicalModuleBaseDS.TI_TYPENAME_FIELD].ToString();
                this.cmCategory = typeInfoRow[LogicalModuleBaseDS.TI_CATEGORYNAME_FIELD].ToString();

                this.typeName = "LogicalModuleBase";
                this.name = logicalCMRow[LogicalModuleBaseDS.LC_NAME_FIELD].ToString();
                this.categoryName = "Framework";
                this.currentScenario = (Guid)logicalCMRow[LogicalModuleBaseDS.LC_CURSCENARIO_FIELD];
                this.parentCLID = (Guid)logicalCMRow[LogicalModuleBaseDS.LC_PARENTID_FIELD];

                if (logicalCMRow[LogicalModuleBaseDS.LC_BSCONSOLIDATIONMODE_FIELD] != DBNull.Value &&
                    logicalCMRow[LogicalModuleBaseDS.LC_BSPERIODTYPE_FIELD] != DBNull.Value &&
                    logicalCMRow[LogicalModuleBaseDS.LC_BSCONSOL_FIELD] != DBNull.Value &&
                    logicalCMRow[LogicalModuleBaseDS.LC_BSOUTYPE_FIELD] != DBNull.Value)
                {
                    this.m_objContext = new BusinessStructureContext((ConsolidationType)logicalCMRow[LogicalModuleBaseDS.LC_BSCONSOLIDATIONMODE_FIELD],
                        (PeriodType)logicalCMRow[LogicalModuleBaseDS.LC_BSPERIODTYPE_FIELD],
                        (ConsolidationMode)logicalCMRow[LogicalModuleBaseDS.LC_BSCONSOL_FIELD],
                        (OrganisationUnitType)logicalCMRow[LogicalModuleBaseDS.LC_BSOUTYPE_FIELD]);
                }

                cmScenarios.Clear();
                foreach (DataRow cmScenarioRow in dSLogicalModule.Tables[LogicalModuleBaseDS.CMSCENARIO_TABLE].Rows)
                {
                    Guid cSID = (Guid)cmScenarioRow[LogicalModuleBaseDS.SC_CSID_FIELD];

                    CMScenario cMScenario = new CMScenario(
                        (Guid)cmScenarioRow[LogicalModuleBaseDS.SC_ID_FIELD],						// ID
                        cmScenarioRow[LogicalModuleBaseDS.SC_NAME_FIELD].ToString(),				// Name
                        this.CID,																	// CLID,
                        cSID,																		// CSID
                        new ScenarioType((int)cmScenarioRow[LogicalModuleBaseDS.SC_TYPE_FIELD]),	// Scenario Type
                        (Guid)cmScenarioRow[LogicalModuleBaseDS.SC_CIID_FIELD],						// CIID 
                        new ScenarioStatus((int)cmScenarioRow[LogicalModuleBaseDS.SC_STATUS_FIELD]),// Scenario Status
                        (Guid)cmScenarioRow[LogicalModuleBaseDS.SC_TYPEID_FIELD],					// CM Type ID											// CM Type
                        (string)cmScenarioRow[LogicalModuleBaseDS.SC_TYPENAME_FIELD],				// Type Name
                        (DateTime)cmScenarioRow[LogicalModuleBaseDS.SC_CREATIONDATETIME_FIELD],		// Creation Date/Time
                        (DateTime)cmScenarioRow[LogicalModuleBaseDS.SC_MODIFICATIONDATE_TIME],		// Modification Date/Time
                        (bool)cmScenarioRow[LogicalModuleBaseDS.SC_LOCKED]							// Locked status
                        );

                    cmScenarios[cSID] = cMScenario;
                }

                childCLIDs.Clear();
                foreach (DataRow childLCMRow in dSLogicalModule.Tables[LogicalModuleBaseDS.CHILDLCM_TABLE].Rows)
                {
                    this.childCLIDs.Add((Guid)childLCMRow[LogicalModuleBaseDS.CLCM_CLID_FIELD]);
                }

                owners.Clear();
                foreach (DataRow ownerLCMRow in dSLogicalModule.Tables[LogicalModuleBaseDS.OWNERLCM_TABLE].Rows)
                {
                    Guid ownerCLID = (Guid)ownerLCMRow[LogicalModuleBaseDS.OWNERCM_CLID_FIELD];
                    Guid ownerCSID = (Guid)ownerLCMRow[LogicalModuleBaseDS.OWNERCM_CSID_FIELD];
                    Guid memberCSID = (Guid)ownerLCMRow[LogicalModuleBaseDS.MEMBERCM_CSID_FIELD];
                    OwnerLCM owner = new OwnerLCM(memberCSID, ownerCSID, ownerCLID);
                    this.owners.Add(owner);
                }

                foreach (DataRow subscriptionRow in dSLogicalModule.Tables[LogicalModuleBaseDS.SUBSCRIPTIONS_TABLE].Rows)
                {
                    Subscription subscription;
                    object[] objParameters;

                    if (subscriptionRow[LogicalModuleBaseDS.SUB_PARAMETERS_FIELD] != DBNull.Value)
                        objParameters = this.DeserializeParameters((byte[])subscriptionRow[LogicalModuleBaseDS.SUB_PARAMETERS_FIELD]);
                    else
                        objParameters = null;

                    if (subscriptionRow[LogicalModuleBaseDS.SUB_DATATYPE_FIELD] != DBNull.Value)
                    {
                        subscription = new Subscription(
                            (Guid)subscriptionRow[LogicalModuleBaseDS.SUB_SBID_FIELD],
                            subscriptionRow[LogicalModuleBaseDS.SUB_DATATYPE_FIELD].ToString(),
                            (Guid)subscriptionRow[LogicalModuleBaseDS.SUB_SUBSCRIBERCLID_FIELD],
                            (Guid)subscriptionRow[LogicalModuleBaseDS.SUB_SUBSCRIBERCSID_FIELD],
                            (Guid)subscriptionRow[LogicalModuleBaseDS.SUB_SUBSCRIBEDCSID_FIELD],
                            objParameters
                            );
                    }
                    else
                    {
                        subscription = new Subscription(
                            (Guid)subscriptionRow[LogicalModuleBaseDS.SUB_SBID_FIELD],
                            (bool)subscriptionRow[LogicalModuleBaseDS.SUB_CONSOLIDATION_FIELD],
                            (Guid)subscriptionRow[LogicalModuleBaseDS.SUB_SUBSCRIBERCLID_FIELD],
                            (Guid)subscriptionRow[LogicalModuleBaseDS.SUB_SUBSCRIBEDCLID_FIELD],
                            (Guid)subscriptionRow[LogicalModuleBaseDS.SUB_SUBSCRIBERCSID_FIELD],
                            (Guid)subscriptionRow[LogicalModuleBaseDS.SUB_SUBSCRIBEDCSID_FIELD],
                            objParameters
                            );
                    }
                    subscription.IsPersisted = true;
                    subscriptions.Add(subscription);
                }
            }
        }

        #region MIGRATION

        public override DataSet MigrateDataSet(DataSet data)
        {
            DataSet baseDataset = base.MigrateDataSet(data);

            DataSet convData = ConvertV_1_0_3_0toV_1_1_0_0DataSet(data, data);
            DataSet convData2 = ConvertV_1_1_0_0toV_1_1_1_1DataSet(data, convData);
            //convData = ConvertV_XtoV_YDataSet(data, convData);

            if (convData == null)
                return null;

            LogicalModuleBaseDS ds = new LogicalModuleBaseDS();

            //Merge in all of the component tables
            ds.Merge(convData2.Tables[LogicalModuleBaseDS.CMLOGICAL_TABLE]);
            ds.Merge(convData2.Tables[LogicalModuleBaseDS.CMSCENARIO_PERSISTED_TABLE]);
            ds.Merge(convData2.Tables[LogicalModuleBaseDS.SCENARIO_TABLE]);
            ds.Merge(convData2.Tables[LogicalModuleBaseDS.CHILDLCM_TABLE]);
            ds.Merge(convData2.Tables[LogicalModuleBaseDS.SUBSCRIPTIONS_TABLE]);
            ds.Merge(convData2.Tables[LogicalModuleBaseDS.TYPEINFO_TABLE]);

            //merge in the base dataset
            ds.Merge(baseDataset);

            return ds;
        }

        public override void MigrationCompleted()
        {
            base.MigrationCompleted();

            if (this.CMCategory == "BusinessEntity")
            {
                IBrokerManagedComponent iBMC = this.Broker.GetBMCInstance(this.GetCIID(this.CurrentScenario));
                if (iBMC != null)
                {
                    this.Name = iBMC.Name;
                }
            }
            //Event Log
            this.Broker.LogEvent(EventType.PerformMigration, Guid.Empty, "Perform Migration for '" + this.Name + "[" + this.GetScenario(this.currentScenario).Name + "]'");
        }

        /// <summary>
        /// Convert a V1.0.3.0 dataset to a V1.1.0.0 dataset
        /// V1.1.0.0 is identical
        /// </summary>
        /// <param name="data">The orginal untampered dataset</param>
        /// <param name="convData">A converted dataset passed in from a previous conversion function</param>
        /// <returns>A converted dataset</returns>
        private DataSet ConvertV_1_0_3_0toV_1_1_0_0DataSet(DataSet data, DataSet convData)
        {
            //Swallowing errors
            //The dataset passed in may not be a correct dataset
            //The whole shape of the dataset may have changed
            //We want to carry on regardless even if there are errors
            //so that other convert functions can have a chance of converting.
            if (convData == null)
                return null;

            //In complete contradiction to normal software development standards we DO want to use
            //Literals and not constants here. This hard codes the software to the old version.
            string fn = BrokerManagedComponentDS.GetFullName(convData.Tables["CMLOGICAL"]);
            AssemblyFullName afn = new AssemblyFullName(fn);

            //if not the appropriate verion for correction then just return back
            if (afn.Version.ToString() != "1.0.3.0")
            {
                return convData;
            }

            //Build up the target dataset manually to avoid unintentional version change

            DataSet ds = new DataSet();
            DataTable table;

            table = new DataTable("CMSCENARIO");
            table.Columns.Add("NAME", typeof(System.String));
            table.Columns.Add("CMTYPEID", typeof(System.Guid));
            table.Columns.Add("TYPENAME", typeof(System.String));
            table.Columns.Add("CLID", typeof(System.Guid));
            table.Columns.Add("CSID", typeof(System.Guid));
            table.Columns.Add("CIID", typeof(System.Guid));
            table.Columns.Add("TYPE", typeof(System.Int32));
            table.Columns.Add("STATUS", typeof(System.Int32));
            table.Columns.Add("CREATED", typeof(System.DateTime));
            table.Columns.Add("MODIFIED", typeof(System.DateTime));
            table.Columns.Add("PARENTGROUPCSID", typeof(System.Guid));
            DataColumn[] scPrimaryKey = { table.Columns["CSID"], table.Columns["CLID"] };
            table.PrimaryKey = scPrimaryKey;
            ds.Tables.Add(table);

            table = new DataTable("CMLOGICAL");
            table.Columns.Add("CLID", typeof(System.Guid));
            table.Columns.Add("NAME", typeof(System.String));
            table.Columns.Add("CURRCSID", typeof(System.Guid));
            table.Columns.Add("CTID", typeof(System.Guid));
            table.Columns.Add("TYPENAME", typeof(System.String));
            table.Columns.Add("CATEGORYNAME", typeof(System.String));
            table.Columns.Add("PLID", typeof(System.Guid));
            table.Columns.Add("BSCONSOLIDATIONMODE", typeof(int));
            table.Columns.Add("BSPERIODTYPE", typeof(int));
            table.Columns.Add("BSCONSOL", typeof(int));
            table.Columns.Add("BSOUTYPE", typeof(int));
            DataColumn[] lcmPrimaryKey = { table.Columns["CLID"] };
            table.PrimaryKey = lcmPrimaryKey;
            ds.Tables.Add(table);


            table = new DataTable("CHILDLCM");
            table.Columns.Add("CLID", typeof(System.Guid));
            ds.Tables.Add(table);

            table = new DataTable("SUBSCRIPTIONS");
            table.Columns.Add("SBID", typeof(System.Guid));
            table.Columns.Add("CLID", typeof(System.Guid));
            table.Columns.Add("DATATYPE", typeof(System.String));
            table.Columns.Add("SUBSCRIBERCLID", typeof(System.Guid));
            table.Columns.Add("SUBSCRIBERCSID", typeof(System.Guid));
            table.Columns.Add("SUBSCRIBEDCSID", typeof(System.Guid));
            table.Columns.Add("SUBSCRIBEDCLID", typeof(System.Guid));
            table.Columns.Add("CONSOLIDATION", typeof(bool));
            table.Columns.Add("PARAMETERS", typeof(byte[]));
            DataColumn[] sbPrimaryKey = { table.Columns["SBID"] };
            table.PrimaryKey = sbPrimaryKey;
            ds.Tables.Add(table);

            ds.EnforceConstraints = false;

            //All subscriptions need to be readded.
            //Clear this table so that the group subscriptions containing blobbed Date parameters
            //does not cause exceptions to be thrown. Subscriptions readding is handled in the 
            //onmigrationcompleted methods.
            convData.Tables["SUBSCRIPTIONS"].Rows.Clear();

            ds.Merge(convData, true, MissingSchemaAction.Ignore);

            //Perform Conversions
            IAssemblyManagement objAM = (IAssemblyManagement)this.Broker.GetComponentDictionary();

            Guid gdTID = (Guid)ds.Tables["CMLOGICAL"].Rows[0]["CTID"];
            IAssembly objAssembly = objAM[gdTID];

            //Add new column
            ds.Tables["CMSCENARIO"].Columns.Add(LogicalModuleBaseDS.SC_LOCKED, typeof(System.Boolean));

            foreach (DataRow dRow in ds.Tables["CMSCENARIO"].Rows)
            {
                dRow["CMTYPEID"] = objAssembly.ID;
                dRow["TYPENAME"] = objAssembly.Name;
                dRow["CMNAME"] = objAssembly.DisplayName;
                dRow["LOCKED"] = false;
            }

            foreach (DataRow row in ds.Tables["CMLOGICAL"].Rows)
            {
                if (((string)row["Name"]).EndsWith("_LM"))
                {
                    row["Name"] = ((string)row["Name"]).TrimEnd(new char[3] { '_', 'L', 'M' });
                }
            }

            ds.EnforceConstraints = true;

            //stamp all version 1.1.0.0
            afn.Version = new Version("1.1.0.0");
            ds.Tables["CMSCENARIO"].ExtendedProperties["FullName"] = afn.ToString();
            ds.Tables["CMLOGICAL"].ExtendedProperties["FullName"] = afn.ToString();
            ds.Tables["CHILDLCM"].ExtendedProperties["FullName"] = afn.ToString();
            ds.Tables["SUBSCRIPTIONS"].ExtendedProperties["FullName"] = afn.ToString();

            return ds;
        }


        /// <summary>
        /// Convert a V1.0.3.0 dataset to a V1.1.0.0 dataset
        /// V1.1.0.0 is identical
        /// </summary>
        /// <param name="data">The orginal untampered dataset</param>
        /// <param name="convData">A converted dataset passed in from a previous conversion function</param>
        /// <returns>A converted dataset</returns>
        private DataSet ConvertV_1_1_0_0toV_1_1_1_1DataSet(DataSet data, DataSet convData)
        {
            //Swallowing errors
            //The dataset passed in may not be a correct dataset
            //The whole shape of the dataset may have changed
            //We want to carry on regardless even if there are errors
            //so that other convert functions can have a chance of converting.
            try
            {

                if (convData == null)
                    return null;

                //In complete contradiction to normal software development standards we DO want to use
                //Literals and not constants here. This hard codes the software to the old version.
                string fn = BrokerManagedComponentDS.GetFullName(convData.Tables["CMLOGICAL"]);
                AssemblyFullName afn = new AssemblyFullName(fn);

                //if not the appropriate verion for correction then just return back
                if (afn.Version.ToString() != "1.1.0.0")
                {
                    return convData;
                }

                //Build up the target dataset manually to avoid unintentional version change

                DataSet ds = new DataSet();
                DataTable table;

                table = new DataTable("TYPEINFO");
                table.Columns.Add("TYPENAME", typeof(System.String));
                table.Columns.Add("CATEGORYNAME", typeof(System.String));
                ds.Tables.Add(table);

                table = new DataTable("CMLOGICAL");
                table.Columns.Add("CLID", typeof(System.Guid));
                table.Columns.Add("NAME", typeof(System.String));
                table.Columns.Add("CURRCSID", typeof(System.Guid));
                table.Columns.Add("CTID", typeof(System.Guid));
                table.Columns.Add("PLID", typeof(System.Guid));
                table.Columns.Add("BSCONSOLIDATIONMODE", typeof(int));
                table.Columns.Add("BSPERIODTYPE", typeof(int));
                table.Columns.Add("BSCONSOL", typeof(int));
                table.Columns.Add("BSOUTYPE", typeof(int));
                DataColumn[] lcmPrimaryKey = { table.Columns["CLID"] };
                table.PrimaryKey = lcmPrimaryKey;
                ds.Tables.Add(table);

                table = new DataTable("CHILDLCM");
                table.Columns.Add("CLID", typeof(System.Guid));
                ds.Tables.Add(table);

                table = new DataTable("CMSCENARIO");
                table.Columns.Add("ID", typeof(System.Guid));
                table.Columns.Add("CLID", typeof(System.Guid));
                table.Columns.Add("CSID", typeof(System.Guid));
                table.Columns.Add("CIID", typeof(System.Guid));
                DataColumn[] scPrimaryKey = { table.Columns["ID"] };
                table.PrimaryKey = scPrimaryKey;
                ds.Tables.Add(table);

                table = new DataTable("SCENARIO");
                table.Columns.Add("CSID", typeof(System.Guid));
                table.Columns.Add("NAME", typeof(System.String));
                table.Columns.Add("TYPE", typeof(System.Int32));
                table.Columns.Add("STATUS", typeof(System.Int32));
                table.Columns.Add("CREATED", typeof(System.DateTime));
                table.Columns.Add("MODIFIED", typeof(System.DateTime));
                table.Columns.Add("LOCKED", typeof(System.Boolean));
                DataColumn[] sPrimaryKey = { table.Columns["CSID"] };
                table.PrimaryKey = sPrimaryKey;
                ds.Tables.Add(table);

                table = new DataTable("SUBSCRIPTIONS");
                table.Columns.Add("SBID", typeof(System.Guid));
                table.Columns.Add("CLID", typeof(System.Guid));
                table.Columns.Add("DATATYPE", typeof(System.String));
                table.Columns.Add("SUBSCRIBERCLID", typeof(System.Guid));
                table.Columns.Add("SUBSCRIBERCSID", typeof(System.Guid));
                table.Columns.Add("SUBSCRIBEDCSID", typeof(System.Guid));
                table.Columns.Add("SUBSCRIBEDCLID", typeof(System.Guid));
                table.Columns.Add("CONSOLIDATION", typeof(bool));
                table.Columns.Add("PARAMETERS", typeof(byte[]));
                DataColumn[] sbPrimaryKey = { table.Columns["SBID"] };
                table.PrimaryKey = sbPrimaryKey;
                ds.Tables.Add(table);

                ds.EnforceConstraints = false;

                ds.Merge(convData, true, MissingSchemaAction.Ignore);

                //Perform Conversions
                convData.Tables["SUBSCRIPTIONS"].Rows.Clear();

                foreach (DataRow dRow in convData.Tables["CMSCENARIO"].Rows)
                {
                    DataRow newRow = ds.Tables["SCENARIO"].NewRow();
                    newRow["CSID"] = dRow["CSID"];
                    newRow["NAME"] = dRow["NAME"];
                    newRow["TYPE"] = dRow["TYPE"];
                    newRow["STATUS"] = dRow["STATUS"];
                    newRow["CREATED"] = dRow["CREATED"];
                    newRow["MODIFIED"] = dRow["MODIFIED"];
                    newRow["LOCKED"] = dRow["LOCKED"];
                    ds.Tables["SCENARIO"].Rows.Add(newRow);
                }

                foreach (DataRow dRow in convData.Tables["CMLOGICAL"].Rows)
                {
                    DataRow newRow = ds.Tables["TYPEINFO"].NewRow();
                    newRow["TYPENAME"] = dRow["TYPENAME"];
                    newRow["CATEGORYNAME"] = dRow["CATEGORYNAME"];
                    ds.Tables["TYPEINFO"].Rows.Add(newRow);
                }

                foreach (DataRow dRow in ds.Tables["CMSCENARIO"].Rows)
                {
                    dRow["ID"] = Guid.NewGuid();

                }

                ds.EnforceConstraints = true;

                ds.Tables["TYPEINFO"].Rows[0]["TYPENAME"] = string.Empty;
                ds.Tables["TYPEINFO"].Rows[0]["CATEGORYNAME"] = string.Empty;
                //stamp all version 1.1.1.1
                afn.Version = new Version("1.1.1.1");
                ds.Tables["CHILDLCM"].ExtendedProperties["FullName"] = afn.ToString();
                ds.Tables["TYPEINFO"].ExtendedProperties["FullName"] = afn.ToString();
                ds.Tables["CMLOGICAL"].ExtendedProperties["FullName"] = afn.ToString();
                ds.Tables["CMSCENARIO"].ExtendedProperties["FullName"] = afn.ToString();
                ds.Tables["SCENARIO"].ExtendedProperties["FullName"] = afn.ToString();
                ds.Tables["SUBSCRIPTIONS"].ExtendedProperties["FullName"] = afn.ToString();

                return ds;
            }
            catch { }

            return convData;
        }



        #endregion MIGRATION



        public CMScenario AddScenario(CMScenario cMScenario)
        {
            // Add the new scenario to this logical module and to all child logical modules
            cmScenarios[cMScenario.CSID] = cMScenario;	// Add to this LCM
            return cMScenario;
        }

        public CMScenario CreateCopyScenario(
            string scenarioName,
            Guid newCSID,
            ScenarioType type,
            Guid copiedCSID,
            ScenarioStatus status,
            DateTime creationDateTime,
            DateTime modificationDateTime
            )
        {
            Guid cIID = this.GetCIID(copiedCSID);

            CMScenario newScenario = null;

            if (cIID != Guid.Empty)
            {
                CMScenario currentScenario = this.cmScenarios[copiedCSID];

                newScenario = new CMScenario(
                    scenarioName,
                    this.CID,
                    newCSID,
                    type,
                    cIID,
                    status,
                    currentScenario.CMTypeID,
                    currentScenario.CMTypeName,
                    creationDateTime,
                    modificationDateTime,
                    false);

                this.AddScenario(newScenario);

                foreach (Guid childCLID in this.ChildCLIDs)
                {
                    ILogicalModule childLCM = this.Broker.GetLogicalCM(childCLID);
                    childLCM.CreateCopyScenario(scenarioName, newCSID, type, copiedCSID, status, creationDateTime, modificationDateTime);
                    this.Broker.ReleaseBrokerManagedComponent(childLCM);
                }

                //Copy subscriptions for new scenario
                ArrayList alNewSubscriptions = new ArrayList();
                ILogicalModule parentLCM = this.ParentLCM;

                if (null != parentLCM)
                {
                    foreach (Subscription objSubscription in parentLCM.Subscriptions)
                    {
                        if (objSubscription.SubscriberCLID == this.CLID && objSubscription.SubscriberCSID == copiedCSID)
                        {
                            Subscription objNewSubscription = new Subscription(objSubscription);
                            objNewSubscription.SubscriberCSID = newCSID;
                            alNewSubscriptions.Add(objNewSubscription);
                        }
                    }

                    foreach (Subscription objSubscription in alNewSubscriptions)
                        parentLCM.Subscriptions.Add(objSubscription);

                    parentLCM.CalculateToken(true);

                    this.Broker.ReleaseBrokerManagedComponent(parentLCM);
                }
                CalculateToken(true);
            }
            return newScenario;
        }

        public CMScenario CreateScenario(
            string scenarioName,
            Guid newCSID,
            ScenarioType type,
            Guid cIID,
            ScenarioStatus status,
            DateTime creationDateTime,
            DateTime modificationDateTime,
            bool locked
            )
        {
            CMScenario newScenario = new CMScenario(
                scenarioName,
                this.CID,
                newCSID,
                type,
                cIID,
                status,
                this.CMType,
                this.cmTypeName,
                creationDateTime,
                modificationDateTime,
                locked);

            this.AddScenario(newScenario);

            foreach (Guid childCLID in this.ChildCLIDs)
            {
                ILogicalModule childLCM = this.Broker.GetLogicalCM(childCLID);
                childLCM.CreateScenario(scenarioName, newCSID, type, cIID, status, creationDateTime, modificationDateTime, locked);
                this.Broker.ReleaseBrokerManagedComponent(childLCM);
            }

            CalculateToken(true);
            return newScenario;
        }

        public CMScenario CreateScenario(
            string scenarioName,
            Guid newCSID,
            ScenarioType type,
            Guid cIID,
            ScenarioStatus status,
            DateTime creationDateTime,
            DateTime modificationDateTime
            )
        {
            return CreateScenario(scenarioName, newCSID, type, cIID, status, creationDateTime, modificationDateTime, false);
        }

        public void DeleteScenario(Guid cSID)
        {
            this.DeleteScenario(cSID, Guid.Empty);
        }

        public void DeleteScenario(Guid cSID, Guid objNewScenarioID)
        {
            CMScenario cMScenario = cmScenarios[cSID];
            if (null != cMScenario)
            {
                if (cMScenario.Locked)
                    throw new ScenarioLockException();

                ILogicalModule parentLCM = this.ParentLCM;

                //Need to remove any scenario subscriptions from the parent logical module
                if (parentLCM != null)
                {
                    parentLCM.UnSubscribe(this.CLID, cSID);
                    parentLCM.CalculateToken(true);
                }

                this.Broker.ReleaseBrokerManagedComponent(parentLCM);

                Guid[] objChildCLIDs = (Guid[])this.childCLIDs.ToArray(typeof(Guid));

                foreach (Guid cLID in objChildCLIDs)
                {
                    LogicalModuleBase childLCM = (LogicalModuleBase)this.Broker.GetLogicalCM(cLID);
                    childLCM.DeleteScenario(cSID, this.CurrentScenario);

                    if (childLCM.cmScenarios.Count <= 0)
                    {
                        this.ChildCLIDs.Remove(childLCM.CLID);
                        this.UnSubscribe(childLCM.CLID);
                    }
                    else
                        this.UnSubscribe(childLCM.CLID, cSID);

                    this.Broker.ReleaseBrokerManagedComponent(childLCM);

                    CalculateToken(true);
                }

                if (!IsShared(cSID))
                {
                    // get the cm instance so that the CSID is set properly, don't need to use it though
                    ICalculationModule cm = this.Broker.GetCMImplementation(this.CLID, cSID);
                    this.Broker.DeleteCMInstance(cMScenario.CIID);
                    this.Broker.ReleaseBrokerManagedComponent(cm);
                }

                cmScenarios.SetDelete(cSID);

                // if we are deleting the current scenario
                if (this.CurrentScenario == cSID)
                {
                    // check if we have a new scenario id, if not then find one
                    if (objNewScenarioID != Guid.Empty)
                        this.CurrentScenario = objNewScenarioID;
                    else
                        this.CurrentScenario = this.FindLatestScenario();
                }

                this.CalculateToken(true);

                if (this.cmScenarios.Count <= 0)
                    this.Broker.DeleteCMInstance(this.CLID);
            }

        }

        private Guid FindLatestScenario()
        {
            Guid gMostRecentScenarioCSID = Guid.Empty;
            DateTime dtScenarioCreateDate, dtMostRecentScenarioDate;
            dtMostRecentScenarioDate = new DateTime(1900, 1, 1);

            IEnumerator objScenarioEn = this.Scenarios.GetEnumerator();

            // Iterate over the Scenarios, recording the most recent Scenario date ( dtMostRecentScenarioDate )
            // and CSID (gMostRecentScenarioCSID ).
            //
            while (objScenarioEn.MoveNext())
            {
                CMScenario objScenario = (CMScenario)(((DictionaryEntry)objScenarioEn.Current).Value);
                //Only standard 'main' scenarios should be defaulted to : PB 05-03-03
                if (objScenario.Type == ScenarioType.Standard)
                {
                    dtScenarioCreateDate = Convert.ToDateTime(objScenario.CreationDateTime);
                    if ((DateTime.Compare(dtMostRecentScenarioDate, dtScenarioCreateDate) < 0) && objScenario.CSID != this.CurrentScenario)
                    {
                        dtMostRecentScenarioDate = dtScenarioCreateDate;
                        gMostRecentScenarioCSID = objScenario.CSID;
                    }
                }
            }

            return gMostRecentScenarioCSID;
        }

        /// <summary>
        /// Adds an owner of this logical module
        /// </summary>
        /// <param name="ownerCLID">The CLID of the owner</param>
        /// <param name="ownerCSID">The CSID of the owner</param>
        /// <param name="memberCSID">The CSID of the member which is a member of the group</param>
        /// <example>A group is the owner of an entity if it is a member of that group</example>
        public void AddOwner(Guid ownerCLID, Guid ownerCSID, Guid memberCSID)
        {
            if (ownerCLID == Guid.Empty)
                throw new ArgumentOutOfRangeException("ownerCLID", Guid.Empty, "The ownerCLID cannot be an empty Guid");

            if (ownerCSID == Guid.Empty)
                throw new ArgumentOutOfRangeException("ownerCSID", Guid.Empty, "The ownerCSID cannot be an empty Guid");

            if (memberCSID == Guid.Empty)
                throw new ArgumentOutOfRangeException("memberCSID", Guid.Empty, "The memberCSID cannot be an empty Guid");

            OwnerLCM ownerLCM = new OwnerLCM(memberCSID, ownerCSID, ownerCLID);

            this.owners.Add(ownerLCM);
        }

        /// <summary>
        /// Removes an owner of this logical module
        /// </summary>
        /// <param name="ownerCLID">The CLID of the owner</param>
        /// <param name="ownerCSID">The CSID of the owner</param>
        /// <param name="memberCSID">The CSID of the member which is a member of the group</param>
        public void RemoveOwner(Guid ownerCLID, Guid ownerCSID, Guid memberCSID)
        {
            if (ownerCLID == Guid.Empty)
                throw new ArgumentOutOfRangeException("ownerCLID", Guid.Empty, "The ownerCLID cannot be an empty Guid");

            if (ownerCSID == Guid.Empty)
                throw new ArgumentOutOfRangeException("ownerCSID", Guid.Empty, "The ownerCSID cannot be an empty Guid");

            if (memberCSID == Guid.Empty)
                throw new ArgumentOutOfRangeException("memberCSID", Guid.Empty, "The memberCSID cannot be an empty Guid");

            this.owners.Remove(new OwnerLCM(memberCSID, ownerCSID, ownerCLID));
        }

        public override void PostPersistActions()
        {
            cmScenarios.CommitChanges();
            this.deletedSubscriptions.Clear();
            foreach (Subscription subscription in this.subscriptions)
                subscription.IsPersisted = true;
        }

        /// <summary>
        /// Adds a child logical CM in all of the parent's scenarios.
        /// </summary>
        /// <param name="childLCM">The child logical CM to be added.</param>
        public ICalculationModule AddChildLogicalCM(ILogicalModule childLCM)
        {
            // Add the child LCM into the parent's childCLIDs table
            this.childCLIDs.Add(childLCM.CLID);
            // Make the parent the entry in the child
            childLCM.ParentCLID = this.CLID;
            // Add all of the parent's scenarios to the child

            ICalculationModule newCMInstance = null;

            foreach (DictionaryEntry dictionaryEntry in this.Scenarios)
            {
                CMScenario cMScenario = (CMScenario)dictionaryEntry.Value;

                // One new instance is used for the physical CM for all scenarios
                if (null == newCMInstance)
                {
                    IBrokerManagedComponent iBMC = this.Broker.CreateComponentInstance(childLCM.CMType, childLCM.Name);
                    newCMInstance = iBMC as ICalculationModule;
                    newCMInstance.CLID = childLCM.CLID;
                    newCMInstance.CSID = cMScenario.CSID;

                    newCMInstance.CurrentBusinessStructureContext = childLCM.CurrentBusinessStructureContext;
                    newCMInstance.InitialiseBusinessStructureContext();

                    newCMInstance.CalculateToken(true);
                }

                Guid objNewCSID = Guid.Empty;
                DateTime dtCreationDateTime;
                DateTime dtModificationDateTime;

                // check if the logical module being copied is the Organisation
                // if it is, we want to create the CM under it in its own scenario
                // - don't want all CMs belonging to 1 scenario.
                if (this.CMTypeName == "Organization_1_1")
                {
                    objNewCSID = Guid.NewGuid();
                    dtCreationDateTime = DateTime.Now;
                    dtModificationDateTime = dtCreationDateTime;
                }
                else
                {
                    objNewCSID = cMScenario.CSID;
                    dtCreationDateTime = cMScenario.CreationDateTime;
                    dtModificationDateTime = cMScenario.ModificationDateTime;
                }

                childLCM.CreateScenario(cMScenario.Name,
                    objNewCSID,
                    cMScenario.Type,
                    newCMInstance.CID,
                    cMScenario.Status,
                    dtCreationDateTime,
                    dtModificationDateTime);

                if (cMScenario.CSID == this.currentScenario)
                    childLCM.CurrentScenario = objNewCSID;

                foreach (string dataType in newCMInstance.Subscriptions)
                    Subscribe(dataType, newCMInstance.CLID, objNewCSID);
            }

            this.Broker.UpdateCacheEntry(childLCM);
            this.Broker.UpdateCacheEntry(newCMInstance);

            newCMInstance.OnJoinCalculationSet();

            childLCM.CalculateToken(true);
            CalculateToken(true);

            return newCMInstance;
        }

        /// <summary>
        /// Adds a child logical CM in only the specified scenario of the parent.
        /// </summary>
        /// <param name="childLCM">The child logical CM to be added.</param>
        /// <param name="cSID">The scenario of the parent in which the child is to be added.</param>
        public ICalculationModule AddChildLogicalCM(ILogicalModule childLCM, Guid cSID)
        {
            // Add the child LCM into the parent's childCLIDs table
            this.childCLIDs.Add(childLCM.CLID);
            // Make the parent the entry in the child
            childLCM.ParentCLID = this.CLID;
            // Add all of the parent's scenarios to the child

            ICalculationModule newCMInstance = null;

            CMScenario cMScenario = this.GetScenario(cSID);
            IBrokerManagedComponent newBMCInstance = this.Broker.CreateComponentInstance(childLCM.CMType, childLCM.Name);
            if (newBMCInstance is ICalculationModule)
            {
                newCMInstance = newBMCInstance as ICalculationModule;
                newCMInstance.CLID = childLCM.CLID;
                newCMInstance.CSID = cMScenario.CSID;
                newCMInstance.CurrentBusinessStructureContext = childLCM.CurrentBusinessStructureContext;
                this.Broker.UpdateCacheEntry(childLCM);
                newCMInstance.InitialiseBusinessStructureContext();
                newCMInstance.CalculateToken(true);
            }
            else
                throw new LogicalModuleException("Requested Child BMC Type is not a CM");

            childLCM.CreateScenario(cMScenario.Name, cMScenario.CSID, cMScenario.Type, newCMInstance.CID, cMScenario.Status, cMScenario.CreationDateTime, cMScenario.ModificationDateTime, cMScenario.Locked);

            if (cMScenario.CSID == this.currentScenario)
                childLCM.CurrentScenario = cMScenario.CSID;

            foreach (string dataType in newCMInstance.Subscriptions)
                Subscribe(dataType, newCMInstance.CLID, cSID);

            this.Broker.UpdateCacheEntry(childLCM);

            this.Broker.UpdateCacheEntry(newCMInstance);
            newCMInstance.OnJoinCalculationSet();
            childLCM.CalculateToken(true);
            CalculateToken(true);

            //this.Broker.UpdateCacheEntry(childLCM);
            //CMCacheManager.CacheManager.UpdateCacheEntry(childLCM);

            return newCMInstance;
        }

        /// <summary>
        /// Removes a child logical CM in only the specified scenario of the parent.
        /// </summary>
        /// <param name="childLCM">The child logical CM to be removed.</param>
        /// <param name="cSID">The scenario of the parent in which the child is to be removed.</param>
        public void RemoveChildLogicalCM(ILogicalModule childLCM, Guid cSID)
        {
            // Remove the scenario in the child LCM
            ICalculationModule objChildCM = childLCM[cSID];
            if (objChildCM != null)
            {
                objChildCM.OnLeaveCalculationSet();
                objChildCM.EndLeaveCalculationSet();
            }
            childLCM.DeleteScenario(cSID);

            if (((CMScenarios)childLCM.Scenarios).Count == 0)
            {
                this.ChildCLIDs.Remove(childLCM.CLID);
                this.UnSubscribe(childLCM.CLID); //If we are removing the logicalCM, then delete any subscriptions corresponding to that logicalmodule
            }
            else //Remove any subscriptions for the CLID and CSID being removed.
                this.UnSubscribe(childLCM.CLID, cSID);

            childLCM.CalculateToken(true);
            this.CalculateToken(true);

            this.Broker.ReleaseBrokerManagedComponent(childLCM);
        }

        /// <summary>
        /// Removes a child logical CM in all its scenarios.
        /// </summary>
        /// <param name="childLCM">The child logical CM to be removed.</param>
        public void RemoveChildLogicalCM(ILogicalModule childLCM)
        {
            this.ChildCLIDs.Remove(childLCM.CLID);
            this.Broker.DeleteLogicalCM(childLCM.CLID);
            this.UnSubscribe(childLCM.CLID);

            childLCM.CalculateToken(true);
            CalculateToken(true);
        }

        public void Subscribe(string dataType, Guid subscriberCLID)
        {
            //First check if the subscription already exists in the deleted subscriptions
            foreach (Subscription subscription in this.deletedSubscriptions)
            {
                if (subscription.DataType == dataType && subscription.SubscriberCLID == subscriberCLID)
                {
                    this.subscriptions.Add(subscription);
                    this.deletedSubscriptions.Remove(subscription);
                    CalculateToken(true);
                    return;
                }
            }
            subscriptions.Add(new Subscription(dataType, subscriberCLID));

            CalculateToken(true);
        }

        public void Subscribe(bool blConsol, Guid subscriberCLID, Guid subscribedCLID)
        {
            //First check if the subscription already exists in the deleted subscriptions
            foreach (Subscription subscription in this.deletedSubscriptions)
            {
                if (subscription.Consolidation == blConsol && subscription.SubscriberCLID == subscriberCLID && subscription.SubscribedCLID == subscribedCLID)
                {
                    this.subscriptions.Add(subscription);
                    this.deletedSubscriptions.Remove(subscription);
                    CalculateToken(true);
                    return;
                }
            }
            subscriptions.Add(new Subscription(blConsol, subscriberCLID, subscribedCLID));

            CalculateToken(true);
        }

        public void Subscribe(string dataType, Guid subscriberCLID, Guid subscriberCSID, Object[] parameters)
        {
            //First check if the subscription already exists in the deleted subscriptions
            foreach (Subscription subscription in this.deletedSubscriptions)
            {
                if (subscription.DataType == dataType && subscription.SubscriberCLID == subscriberCLID && subscription.SubscriberCSID == subscriberCSID)
                {
                    subscription.Parameters = parameters;
                    this.subscriptions.Add(subscription);
                    this.deletedSubscriptions.Remove(subscription);
                    CalculateToken(true);
                    return;
                }
            }
            Subscription objSubscription = new Subscription(dataType, subscriberCLID, parameters);
            objSubscription.SubscriberCSID = subscriberCSID;
            subscriptions.Add(objSubscription);

            CalculateToken(true);
        }

        public void Subscribe(bool bolConsolidation, Guid objSubscriberCLID, Guid objSubscribedCLID, Guid objSubscriberCSID, Guid objSubscribedCSID)
        {
            //First check if the subscription already exists in the deleted subscriptions
            foreach (Subscription subscription in this.deletedSubscriptions)
            {
                if (subscription.Consolidation == bolConsolidation &&
                    subscription.SubscriberCLID == objSubscriberCLID &&
                    subscription.SubscribedCLID == objSubscribedCLID &&
                    subscription.SubscriberCSID == objSubscriberCSID &&
                    subscription.SubscribedCSID == objSubscribedCSID)
                {
                    this.subscriptions.Add(subscription);
                    this.deletedSubscriptions.Remove(subscription);
                    CalculateToken(true);
                    return;
                }
            }
            this.subscriptions.Add(new Subscription(bolConsolidation, objSubscriberCLID, objSubscribedCLID, objSubscriberCSID, objSubscribedCSID));
            //this.Modified = true;
            CalculateToken(true);
        }

        public void Subscribe(string dataType, Guid objSubscriberCLID, Guid objSubscriberCSID)
        {
            //First check if the subscription already exists in the deleted subscriptions
            foreach (Subscription subscription in this.deletedSubscriptions)
            {
                if (subscription.DataType == dataType &&
                    subscription.SubscriberCLID == objSubscriberCLID &&
                    subscription.SubscribedCLID == Guid.Empty &&
                    subscription.SubscriberCSID == objSubscriberCSID &&
                    subscription.SubscribedCSID == Guid.Empty)
                {
                    this.subscriptions.Add(subscription);
                    this.deletedSubscriptions.Remove(subscription);
                    CalculateToken(true);
                    return;
                }
            }
            this.subscriptions.Add(new Subscription(dataType, objSubscriberCLID, objSubscriberCSID, Guid.Empty));
            //this.Modified = true;
            CalculateToken(true);
        }

        public void Subscribe(bool bolSubscription, Guid objSubscriberCLID, Guid objSubscriberCSID, Object[] objParameters)
        {
            //First check if the subscription already exists in the deleted subscriptions
            foreach (Subscription subscription in this.deletedSubscriptions)
            {
                if (subscription.Consolidation == bolSubscription &&
                    subscription.SubscriberCLID == objSubscriberCLID && subscription.SubscriberCSID == objSubscriberCSID)
                {
                    subscription.Parameters = objParameters;
                    this.subscriptions.Add(subscription);
                    this.deletedSubscriptions.Remove(subscription);
                    CalculateToken(true);
                    return;
                }
            }

            Subscription objSubscription = new Subscription(bolSubscription, objSubscriberCLID, objParameters);
            objSubscription.SubscriberCSID = objSubscriberCSID;

            subscriptions.Add(objSubscription);
            //this.Modified = true;
            CalculateToken(true);
        }


        public void UnSubscribe(bool blConsolidation, Guid subscriberCLID, Guid subscriberCSID)
        {
            bool modified = false;
            Subscription subscription;
            IEnumerator en = this.subscriptions.GetEnumerator();
            while (en.MoveNext())
            {
                subscription = (Subscription)en.Current;
                if (subscription.SubscriberCLID == subscriberCLID && subscription.SubscriberCSID == subscriberCSID
                    && subscription.Consolidation == blConsolidation)
                {
                    if (subscription.IsPersisted)
                        this.deletedSubscriptions.Add(subscription);
                    this.subscriptions.Remove(subscription);
                    en = this.subscriptions.GetEnumerator();
                    modified = true;
                }
            }
            if (modified)
                CalculateToken(true);
        }

        public void UnSubscribe(string dataType, Guid subscriberCLID, Guid subscriberCSID)
        {
            bool modified = false;
            Subscription subscription;
            IEnumerator en = this.subscriptions.GetEnumerator();
            while (en.MoveNext())
            {
                subscription = (Subscription)en.Current;
                if (subscription.DataType == dataType && subscription.SubscriberCLID == subscriberCLID && subscription.SubscriberCSID == subscriberCSID)
                {
                    if (subscription.IsPersisted)
                        this.deletedSubscriptions.Add(subscription);
                    this.subscriptions.Remove(subscription);
                    en = this.subscriptions.GetEnumerator();
                    modified = true;
                }
            }
            if (modified)
                CalculateToken(true);
        }

        /// <summary>
        /// Unsubscribes all subscriptions for a given subscriber CLID and CSID which
        /// are GL subscriptions (denoted by the 'GL_' prefix to the subscription datatype)
        /// However if it is a GL subscription defined in the Subscriptions array of the 
        /// CM (passed in through defaultSubscriptions parameter) then it will not be deleted.
        /// </summary>
        /// <param name="subscriberCLID"></param>
        /// <param name="subscriberCSID"></param>
        /// <param name="defaultSubscriptions"></param>
        public void UnSubscribeGL(Guid subscriberCLID, Guid subscriberCSID, IEnumerable defaultSubscriptions)
        {
            ArrayList defaultSubscriptionsAL = new ArrayList();

            IEnumerator en = defaultSubscriptions.GetEnumerator();
            while (en.MoveNext())
                defaultSubscriptionsAL.Add(en.Current.ToString());

            bool modified = false;
            Subscription subscription;
            en = this.subscriptions.GetEnumerator();
            while (en.MoveNext())
            {
                subscription = (Subscription)en.Current;
                if (subscription.DataType.StartsWith("GL_") && subscription.SubscriberCLID == subscriberCLID && subscription.SubscriberCSID == subscriberCSID && !defaultSubscriptionsAL.Contains(subscription.DataType))
                {
                    if (subscription.IsPersisted)
                        this.deletedSubscriptions.Add(subscription);
                    this.subscriptions.Remove(subscription);
                    en = this.subscriptions.GetEnumerator();
                    modified = true;
                }
            }
            if (modified)
                CalculateToken(true);
        }

        public void UnSubscribe()
        {
            bool modified = false;
            Subscription subscription;
            IEnumerator en = this.subscriptions.GetEnumerator();
            while (en.MoveNext())
            {
                subscription = (Subscription)en.Current;
                if (subscription.IsPersisted)
                    this.deletedSubscriptions.Add(subscription);
                this.subscriptions.Remove(subscription);
                en = this.subscriptions.GetEnumerator();
                modified = true;
            }
            if (modified)
                CalculateToken(true);
        }


        public void UnSubscribe(Guid subscriberCLID)
        {
            bool modified = false;
            Subscription subscription;
            IEnumerator en = this.subscriptions.GetEnumerator();
            while (en.MoveNext())
            {
                subscription = (Subscription)en.Current;
                if (subscription.SubscriberCLID == subscriberCLID)
                {
                    if (subscription.IsPersisted)
                        this.deletedSubscriptions.Add(subscription);
                    this.subscriptions.Remove(subscription);
                    en = this.subscriptions.GetEnumerator();
                    modified = true;
                }
            }
            if (modified)
                CalculateToken(true);
        }

        public void UnSubscribe(Guid subscriberCLID, Guid subscriberCSID)
        {
            bool modified = false;
            Subscription subscription;
            IEnumerator en = this.subscriptions.GetEnumerator();
            while (en.MoveNext())
            {
                subscription = (Subscription)en.Current;
                if (subscription.SubscriberCLID == subscriberCLID
                    && subscription.SubscriberCSID == subscriberCSID)
                {
                    if (subscription.IsPersisted)
                        this.deletedSubscriptions.Add(subscription);
                    this.subscriptions.Remove(subscription);
                    en = this.subscriptions.GetEnumerator();
                    modified = true;
                }
            }
            if (modified)
                CalculateToken(true);
        }

        /// <summary>
        /// Removes all the subscriptions as qualified by the paraters.
        /// </summary>
        /// <param name="objSubscriberCLID">Logical ID of the subscribing CM</param>
        /// <param name="objSubscribedCLID">Logical ID of the subscribed CM</param>
        /// <param name="objSubscriberCSID">Scenario ID of the subscribing CM</param>
        /// <param name="objSubscribedCSID">Scenario ID of the subscribed CM</param>
        public void UnSubscribe(Guid objSubscriberCLID, Guid objSubscribedCLID, Guid objSubscriberCSID, Guid objSubscribedCSID)
        {
            Subscription subscription;
            IEnumerator en = this.subscriptions.GetEnumerator();
            while (en.MoveNext())
            {
                subscription = (Subscription)en.Current;
                if (subscription.SubscriberCLID == objSubscriberCLID
                    && subscription.SubscribedCLID == objSubscribedCLID
                    && subscription.SubscriberCSID == objSubscriberCSID
                    && subscription.SubscribedCSID == objSubscribedCSID)
                {
                    if (subscription.IsPersisted)
                        this.deletedSubscriptions.Add(subscription);
                    this.subscriptions.Remove(subscription);
                    en = this.subscriptions.GetEnumerator();
                }
            }

            CalculateToken(true);
        }

        /// <summary>
        /// Determines if a particular CLID is a subscriber to a particular data type, regardless of scenario of the sender.
        /// </summary>
        /// <param name="businessStructureCM"></param>
        /// <param name="message"></param>
        /// <param name="cLID">the CLID of the CM</param>
        /// <returns>true if it is a subscriber</returns>
        public bool IsSubscriber(ICalculationModule businessStructureCM, IMessage message, Guid cLID)
        {
            for (int iCount = 0; iCount < this.subscriptions.Count; iCount++)
            {
                Subscription subscription = (Subscription)this.subscriptions[iCount];
                if (string.Compare(subscription.DataType, message.Datatype, true) == 0
                    && subscription.SubscriberCLID == cLID
                    && ((IBusinessStructureModule)businessStructureCM).FilterMessage(message, subscription.Parameters))
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Determines if a particular CLID is a subscriber to a particular data type from a particular sender scenario.
        /// </summary>
        /// <param name="businessStructureCM"></param>
        /// <param name="message"></param>
        /// <param name="subscriberCLID">the CLID of the CM</param>
        /// <param name="subscriberCSID">the CSID of the sending scenario</param>
        /// <param name="subscribedCSID"></param>
        /// <returns>true if it is a subscriber</returns>
        public bool IsSubscriber(ICalculationModule businessStructureCM, IMessage message, Guid subscriberCLID, Guid subscriberCSID, Guid subscribedCSID)
        {
            //If the message contains TargetReached, then return false, as the message has already served
            //its purpopse
            if (!message.Contains("TargetReached"))
            {
                //First check if this is a targetted stream control message
                if (message.Contains("StreamControl") &&
                    message.Contains("StreamControlTargetCLID") &&
                    message.Contains("StreamControlTargetCSID"))
                {

                    Message objStreamControlMessage = new Message(businessStructureCM, message);
                    Guid objTargetCLID = (Guid)message["StreamControlTargetCLID"];
                    Guid objTargetCSID = (Guid)message["StreamControlTargetCSID"];

                    if (objTargetCLID == subscriberCLID &&
                        objTargetCSID == subscriberCSID)
                    {
                        // when it reaches it's target, remove the target clid and csid
                        // to ensure that the message continues to propergate down
                        message.Remove("StreamControlTargetCLID");
                        message.Remove("StreamControlTargetCSID");
                        message.Add("TargetReached", true);
                        return true;
                    }
                    else
                        return false;
                }

                /*
                 * Ask the business structure object if it is a subscriber to Message ( overriden by OrganisationCM, EntityCM and GroupCM).
                 * The default implementation returns IsSubscriberType.DontKnow, which means that the "normal" subscription mechanism is used
                 */
                IsSubscriberType objIsSubscriber = ((IBusinessStructureModule)businessStructureCM).IsSubscriber(message, this.subscriptions.FilterSubscriptions(subscriberCSID), subscriberCLID, subscriberCSID);
                if (objIsSubscriber == IsSubscriberType.True)
                    return true;
                else if (objIsSubscriber == IsSubscriberType.False)
                    return false;
                else
                {
                    /*
                     * If the business structure CM did not indicate if it was a subscriber or not
                     * then we need to perform the "normal" subscription checking by asking each
                     * logical module if it is a subscriber
                     */
                    for (int iCount = 0; iCount < this.Subscriptions.Count; iCount++)
                    {
                        Subscription subscription = (Subscription)this.Subscriptions[iCount];

                        if (string.Compare(subscription.DataType, message.Datatype, true) == 0
                            && subscription.SubscriberCLID == subscriberCLID
                            && (subscription.SubscriberCSID == subscriberCSID || subscription.SubscriberCSID == Guid.Empty || subscriberCSID == Guid.Empty)
                            && (subscription.SubscribedCSID == subscribedCSID || subscription.SubscribedCSID == Guid.Empty || subscribedCSID == Guid.Empty)
                            && ((IBusinessStructureModule)businessStructureCM).FilterMessage(message, subscription.Parameters)
                            )
                            return true;
                    }
                }
            }
            return false;
        }

        public bool IsShared(Guid cSID)
        {
            Guid cIID = this.GetCIID(cSID);

            if (cIID == Guid.Empty)
                return false;

            int count = 0;

            foreach (DictionaryEntry dictionaryMember in this.Scenarios)
            {
                CMScenario cMScenario = (CMScenario)dictionaryMember.Value;

                if (cMScenario.CIID == cIID)
                    count++;
            }
            return count > 1;
        }
    }
}
