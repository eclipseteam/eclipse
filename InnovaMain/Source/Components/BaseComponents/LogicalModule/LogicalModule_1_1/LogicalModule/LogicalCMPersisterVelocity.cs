using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Reflection;

using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.DSCalculation;
using Oritax.TaxSimp.BusinessStructureUtilities;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.Calculation
{
	/// <summary>
	/// The ComponentManagedPersister persists a component to the database by obtaining a copy of
	/// its primary dataset and persisting each row in each table to corresponding tables in the
	/// database.  This persister leaves the component responsible for mapping between its internal
	/// objects and its primary dataset, with the assumption that the primary dataset is replicated
	/// in the database.
	/// </summary>
	public class LogicalCMPersisterVelocity : BrokerManagedPersisterVelocity, IDisposable
	{
		public LogicalCMPersisterVelocity(SqlConnection connection, SqlTransaction transaction): base(connection, transaction){}

        public override void Save(IBrokerManagedComponent iBMC, ref DataSet persistedPrimaryDS, ITransactionVelocity trx)
        {
            // Get the primary dataset from the instance to be saved
            DataSet primaryDataSet = iBMC.PrimaryDataSet;

            SQLRMBrokerManagedPersister sqlRM = new SQLRMLogicalCMPersister(trx.SQLConnection, trx.SQLTransaction, iBMC, persistedPrimaryDS);
            trx.EnlistRM(sqlRM, "DBRM");

            VelocityRMBrokerManagedPersister velocityRM = new VelocityRMBrokerManagedPersister(iBMC, Guid.Empty);
            trx.EnlistRM(velocityRM, "VelocityRM");

        }
        
        public override void EstablishDatabase()
		{
			Guid typeID=new Guid(LogicalModuleInstall.ASSEMBLY_ID);
			PrimaryDS primaryDataSet=new LogicalModuleBaseDS();

			EstablishDatabase(typeID,primaryDataSet);
			UpdateDataModel();
		}
		public void UpdateDataModel()
		{
			SqlCommand command = DBCommandFactory.Instance.NewSqlCommand();
			command.Connection = this.Connection;
			command.Transaction = this.Transaction;
			command.CommandText = "select count(*) from dbo.sysobjects where id = object_id(N'[dbo].[CMSCENARIO]') and OBJECTPROPERTY(id, N'IsUserTable') = 1";
			int tableCount = (int) command.ExecuteScalar();
		
			SqlCommand cmd = DBCommandFactory.Instance.NewSqlCommand();
			cmd.Connection = this.Connection;
			cmd.Transaction = this.Transaction;
			cmd.CommandText ="if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CMSCENARIO_TEMP]') and OBJECTPROPERTY(id, N'IsUserTable') = 1) BEGIN CREATE TABLE [dbo].[CMSCENARIO_TEMP] ([ID] [uniqueidentifier] NOT NULL, [CLID] [uniqueidentifier] NOT NULL ,[CSID] [uniqueidentifier] NOT NULL ,	[CIID] [uniqueidentifier] NOT NULL ,[CTID] [uniqueidentifier] NOT NULL , CONSTRAINT CMSCENARIO_PK1 PRIMARY KEY (ID))	ON [PRIMARY]; END"; 
			cmd.ExecuteNonQuery(); 
			
			//Code to create a tempTable in DB 
			if(tableCount == 1)
			{
				SqlCommand sqlCmd = DBCommandFactory.Instance.NewSqlCommand();
				sqlCmd.Connection = this.Connection;
				sqlCmd.Transaction = this.Transaction;
				sqlCmd.CommandText = "select * from CMSCENARIO";
					
				//Get all information from CMSCENARIO table and temp. purposes save it in Dataset 
				SqlDataAdapter tempDA = new SqlDataAdapter();
				DataSet oldDataSet  = new DataSet(); 
				tempDA.SelectCommand = sqlCmd; 
				tempDA.Fill(oldDataSet,"CMSCENARIO");
				tempDA.SelectCommand.CommandText = "select * from CMLOGICAL"; 
				tempDA.Fill(oldDataSet,"CMLOGICAL");
				tempDA.SelectCommand.CommandText = "select * from CINSTANCES";
				tempDA.Fill(oldDataSet,"CINSTANCES");
 					
							
				//DataSet Carrying changed table structure, creating all table first in dataset
				DataSet newDataSet = new DataSet();
				DataTable table; 
					
				table = new DataTable("CMSCENARIO");
				table.Columns.Add("ID", typeof(System.Guid));
				table.Columns.Add("CLID", typeof(System.Guid));
				table.Columns.Add("CSID", typeof(System.Guid));
				table.Columns.Add("CIID", typeof(System.Guid));
				table.Columns.Add("CTID", typeof(System.Guid));
				DataColumn[] scPrimaryKey={table.Columns["ID"]};
				table.PrimaryKey=scPrimaryKey;
				newDataSet.Tables.Add(table);

				table = new DataTable("SCENARIO");
				table.Columns.Add("CSID", typeof(System.Guid));
				table.Columns.Add("NAME", typeof(System.String));
				table.Columns.Add("TYPE", typeof(System.Int32));
				table.Columns.Add("STATUS", typeof(System.Int32));
				table.Columns.Add("CREATED", typeof(System.DateTime));
				table.Columns.Add("MODIFIED", typeof(System.DateTime));
				table.Columns.Add("LOCKED", typeof(System.Boolean));
				DataColumn[] sPrimaryKey={table.Columns["CSID"]};
				table.PrimaryKey=sPrimaryKey;
				newDataSet.Tables.Add(table);

				table = new DataTable("TYPEINFO");
				table.Columns.Add("TYPENAME",typeof(System.String)); 
				table.Columns.Add("CATEGORYNAME",typeof(System.String)); 
				newDataSet.Tables.Add(table); 

				
				//Transfer information from old Dataset (CMSCENARIO TABLE) to new DataSet (CMSCENARIO TABLE)
				string	commandText = ""; 
				
				//Set Primary key for CINSTANCE in oldDataSet as CID, becasue DataTable.PrimaryKey is not set when SqlDataAdapter.Fill method is called.   
				//This primary key is set becasue then the table will have index and it will be faster to loop through. 
				DataColumn[] primaryKeyColumn={oldDataSet.Tables["CMLOGICAL"].Columns["CLID"]};
				oldDataSet.Tables["CMLOGICAL"].PrimaryKey=primaryKeyColumn;
			
				foreach(DataRow dRow in oldDataSet.Tables["CMSCENARIO"].Rows)
				{
					DataRow newRow = newDataSet.Tables["CMSCENARIO"].NewRow();
					newRow["ID"] = Guid.NewGuid() ;
					newRow["CLID"] = dRow["CLID"];
					newRow["CSID"] = dRow["CSID"];
					newRow["CIID"] = dRow["CIID"];
					DataRow[] cInstanceRow = oldDataSet.Tables["CMLOGICAL"].Select("CLID ='" +dRow["CLID"].ToString()+"'"); 
					if(cInstanceRow.Length > 0)
					{
						newRow["CTID"] = cInstanceRow[0]["CTID"];
						newDataSet.Tables["CMSCENARIO"].Rows.Add(newRow);
						commandText += "INSERT INTO CMSCENARIO_TEMP (ID,CLID,CSID,CIID,CTID) VALUES('"+newRow["ID"]+"','"+newRow["CLID"]+"','"+newRow["CSID"]+"','"+newRow["CIID"]+"','"+newRow["CTID"]+"')   ";
					}
				}
				if(commandText != "")
				{
					SqlCommand insertCommand = DBCommandFactory.Instance.NewSqlCommand();
					insertCommand.Connection = this.Connection;
					insertCommand.Transaction = this.Transaction;
					insertCommand.CommandText = commandText; 
					insertCommand.ExecuteNonQuery(); 
				}

				//Modify Subscriptions table column DATATYPE to size 355
				command = DBCommandFactory.Instance.NewSqlCommand();
				command.Connection = this.Connection;
				command.Transaction = this.Transaction;
				command.CommandText = "ALTER TABLE SUBSCRIPTIONS ALTER COLUMN DATATYPE varchar(355)";
				command.ExecuteNonQuery();
			
				//	Now drop the old tables
				command = DBCommandFactory.Instance.NewSqlCommand();
				command.Connection = this.Connection;
				command.Transaction = this.Transaction;
				command.CommandText = "drop table CMSCENARIO";
				command.ExecuteNonQuery();
				
				//Rename the table for CMSCENARIO 	
				SqlCommand renameCMD = DBCommandFactory.Instance.NewSqlCommand();
				renameCMD.Connection = this.Connection;
				renameCMD.Transaction = this.Transaction;
				renameCMD.CommandText = "sp_rename";  
				renameCMD.CommandType = CommandType.StoredProcedure; 
				renameCMD.Parameters.Add(new SqlParameter("@objname","CMSCENARIO_TEMP"));
				renameCMD.Parameters.Add(new SqlParameter("@newname","CMSCENARIO"));
				renameCMD.ExecuteNonQuery(); 
					
				//rename constraints 
				SqlCommand renameConstraint = DBCommandFactory.Instance.NewSqlCommand();
				renameConstraint.Connection = this.Connection;
				renameConstraint.Transaction = this.Transaction;
				renameConstraint.CommandText = "sp_rename";  
				renameConstraint.CommandType = CommandType.StoredProcedure; 
				renameConstraint.Parameters.Add(new SqlParameter("@objname","CMSCENARIO_PK1"));
				renameConstraint.Parameters.Add(new SqlParameter("@newname","CMSCENARIO_PK"));
				renameConstraint.ExecuteNonQuery(); 
		
			}
		}

		protected override SqlNamedParameterValue[] GetChildParameterValues(DataSet primaryDataSet,string rootTableName,string parentTableName,DataRow rootRow)
		{
			Guid ID;

			switch(parentTableName)
			{
				case LogicalModuleBaseDS.CHILDLCM_TABLE:
				case LogicalModuleBaseDS.OWNERLCM_TABLE:
				case LogicalModuleBaseDS.CMSCENARIO_TABLE:
				case LogicalModuleBaseDS.TYPEINFO_TABLE:
					ID=(Guid)rootRow[LogicalModuleBaseDS.LC_CLID_FIELD];
					return new SqlNamedParameterValue[]{new SqlNamedParameterValue("@ID",ID)};
				case LogicalModuleBaseDS.SCENARIO_TABLE:
					ID=(Guid)rootRow[LogicalModuleBaseDS.LC_CLID_FIELD];
					return new SqlNamedParameterValue[]{new SqlNamedParameterValue("@ID",ID)};
				default:
					return base.GetChildParameterValues(primaryDataSet,rootTableName,parentTableName,rootRow);
			}
		}

		protected override void GenerateInsertCommandText(PrimaryDS.PDSDataTable dataTable,out string commandText,out SqlParameter[] commandParameters)
		{
			base.GenerateInsertCommandText(dataTable, out commandText, out commandParameters);
			if(dataTable.TableName == LogicalModuleBaseDS.SCENARIO_TABLE)
				commandText="IF ( NOT EXISTS (select CSID from SCENARIO where CSID=@CSID))"+commandText;
		}

		protected override void GenerateDeleteCommandText(PrimaryDS.PDSDataTable dataTable,out string commandText,out SqlParameter[] commandParameters)
		{
			base.GenerateDeleteCommandText(dataTable, out commandText, out commandParameters);
			if(dataTable.TableName == LogicalModuleBaseDS.SCENARIO_TABLE)
				commandText="IF ( NOT EXISTS (select CSID from CMSCENARIO where CSID=@CSID))"+commandText;
		}
	 
		protected override void GenerateSelectCommandText(PrimaryDS.PDSDataTable dataTable,out string commandText,out SqlParameter[] commandParameters)
		{
			SqlDbTypeDescription sqlDbTypeDescription;

			switch(dataTable.TableName)
			{
				case LogicalModuleBaseDS.CHILDLCM_TABLE:
					commandText="SELECT CMLOGICAL.CLID"
						+" FROM CMLOGICAL"
						+" WHERE CMLOGICAL.PLID=@ID";
					sqlDbTypeDescription=TypeToSQLType(typeof(System.Guid));
					commandParameters=new SqlParameter[]{
															new SqlParameter("@ID",sqlDbTypeDescription.DbType,sqlDbTypeDescription.Size),
					};
					break;
				case LogicalModuleBaseDS.OWNERLCM_TABLE:
					commandText="SELECT CMMEMBERS.MEMBERCSID,CMMEMBERS.PARENTCLID,CMMEMBERS.PARENTCSID "
						+"FROM CMMEMBERS,CMSCENARIO "
						+"WHERE CMSCENARIO.CSID=CMMEMBERS.MEMBERCSID "
						+"AND CMSCENARIO.CLID=@ID";
					sqlDbTypeDescription=TypeToSQLType(typeof(System.Guid));
					commandParameters=new SqlParameter[]{
															new SqlParameter("@ID",sqlDbTypeDescription.DbType,sqlDbTypeDescription.Size),
					};
					break;
				case LogicalModuleBaseDS.CMSCENARIO_TABLE:
					commandText="SELECT CMSCENARIO.ID, CMSCENARIO.CSID,SCENARIO.NAME,CMSCENARIO.CLID,CMSCENARIO.CIID,SCENARIO.TYPE,SCENARIO.STATUS,SCENARIO.CREATED,SCENARIO.MODIFIED,SCENARIO.LOCKED,CMSCENARIO.CTID AS CMTYPEID,ASSEMBLY.NAME AS TYPENAME "
						+"FROM CMLOGICAL,CMSCENARIO,SCENARIO,ASSEMBLY "
						+"WHERE CMLOGICAL.CLID=CMSCENARIO.CLID "
						+"AND CMSCENARIO.CSID=SCENARIO.CSID "
						+"AND ASSEMBLY.ID=CMLOGICAL.CTID "
						+"AND CMLOGICAL.CLID=@ID";
					sqlDbTypeDescription=TypeToSQLType(typeof(System.Guid));
					commandParameters=new SqlParameter[]{
															new SqlParameter("@ID",sqlDbTypeDescription.DbType,sqlDbTypeDescription.Size),
					};
					break;
				case LogicalModuleBaseDS.TYPEINFO_TABLE:
					commandText="SELECT ASSEMBLY.NAME AS TYPENAME,COMPONENT.CATEGORY AS CATEGORYNAME "
						+"FROM CMLOGICAL,COMPONENTVERSION,COMPONENT, ASSEMBLY "
						+"WHERE CMLOGICAL.CTID=COMPONENTVERSION.ID "
						+"AND COMPONENTVERSION.COMPONENTID=COMPONENT.ID "
						+"AND ASSEMBLY.ID=COMPONENTVERSION.ID "
						+"AND CMLOGICAL.CLID=@ID";
					sqlDbTypeDescription=TypeToSQLType(typeof(System.Guid));
					commandParameters=new SqlParameter[]{
															new SqlParameter("@ID",sqlDbTypeDescription.DbType,sqlDbTypeDescription.Size),
					};
					break;
				case LogicalModuleBaseDS.SCENARIO_TABLE:
					commandText="SELECT SCENARIO.CSID, SCENARIO.NAME, SCENARIO.TYPE,SCENARIO.STATUS,SCENARIO.CREATED,SCENARIO.MODIFIED,SCENARIO.LOCKED "
						+"FROM CMLOGICAL,CMSCENARIO,SCENARIO "
						+"WHERE CMLOGICAL.CLID=CMSCENARIO.CLID "
						+"AND CMSCENARIO.CSID=SCENARIO.CSID "
						+"AND CMLOGICAL.CLID=@ID";
					sqlDbTypeDescription=TypeToSQLType(typeof(System.Guid));
					commandParameters=new SqlParameter[]{
															new SqlParameter("@ID",sqlDbTypeDescription.DbType,sqlDbTypeDescription.Size),
					};
					break;
				default:
					base.GenerateSelectCommandText(dataTable,out commandText,out commandParameters);
					break;
			}
		}
		
		/// <summary>
		/// Returns an array of CID's that define those BMC instances that have read-only persistence models that overlap
		/// the read-write portion of this BMC persistence.  Note that Read-Write portions are not allowed to overlap.
		/// </summary>
		/// <param name="cID">CID of BMC whose dependents are to be found.</param>
		/// <param name="dependentCIDs"></param>
		/// <param name="actionType"></param>
		/// <returns>Array of dependent BMC CID's</returns>
		public override void FindDependentBMCs(Guid cID,ref ArrayList dependentCIDs, PersisterActionType actionType)
		{
			base.FindDependentBMCs(cID,ref dependentCIDs,actionType);

			// lazy initialise if required
			if(dependentCIDs == null)
				dependentCIDs = new ArrayList();

			SqlCommand loadCommand = DBCommandFactory.Instance.NewSqlCommand();
			loadCommand.Connection = this.Connection;
			loadCommand.Transaction = this.Transaction;

			// Special case dependency SQL for this type of BMC.
			loadCommand.CommandText="SELECT DEPCMLOGICAL.CLID FROM "
				+"CMLOGICAL AS DEPCMLOGICAL,CMSCENARIO AS DEPCMSCENARIO,SCENARIO,CMSCENARIO,CMLOGICAL "
				+"WHERE DEPCMSCENARIO.CLID=DEPCMLOGICAL.CLID "
				+"AND DEPCMSCENARIO.CSID=SCENARIO.CSID "
				+"AND SCENARIO.CSID=CMSCENARIO.CSID "
				+"AND CMLOGICAL.CLID=CMSCENARIO.CLID "
				+"AND CMLOGICAL.CLID='{"+cID.ToString()+"}' "
				+"AND DEPCMLOGICAL.CLID!='{"+cID.ToString()+"}' ";

			using(SqlDataReader loadReader=loadCommand.ExecuteReader())
			{
				while(loadReader.Read())
					dependentCIDs.Add(loadReader.GetGuid(0));

				loadReader.Close();
			}

			Hashtable childCLIDsInMemory = new Hashtable();
			ILogicalModule persistingLM = (ILogicalModule) this.broker.GetBMCInstance(cID);

			// if the peristingLM is null, it means that the LM is being deleted
			// therefore we don't need to perform any of this checking
			if(persistingLM != null)
			{
				foreach(Guid childCLID in persistingLM.ChildCLIDs)
				{
					if(!childCLIDsInMemory.Contains(childCLID))
						childCLIDsInMemory.Add(childCLID, null);
				}
			}

			this.broker.ReleaseBrokerManagedComponent(persistingLM);

			SqlCommand logicalDependenciesCommand = DBCommandFactory.Instance.NewSqlCommand();
			logicalDependenciesCommand.Connection = this.Connection;
			logicalDependenciesCommand.Transaction = this.Transaction;

			logicalDependenciesCommand.CommandText = "select * from cmlogical where plid = '" + cID.ToString() + "'";

			using(SqlDataReader logicalDependenciesReader = logicalDependenciesCommand.ExecuteReader())
			{
				while(logicalDependenciesReader.Read())
				{
					Guid logicalID = logicalDependenciesReader.GetGuid(0);

					if(!dependentCIDs.Contains(logicalID))
						dependentCIDs.Add(logicalID);
				}
			}
			

			foreach(DictionaryEntry cidDE in childCLIDsInMemory)
			{
				Guid cid = (Guid) cidDE.Key;

				if(!dependentCIDs.Contains(cid))
					dependentCIDs.Add(cid);
			}

			//For logical modules add the ID of the Logical being persisted to the invalid array
			//so that it will be rehydrated containing the refreshed read only data.
			if(!dependentCIDs.Contains(cID))
				dependentCIDs.Add(cID);
		}
	}
}
