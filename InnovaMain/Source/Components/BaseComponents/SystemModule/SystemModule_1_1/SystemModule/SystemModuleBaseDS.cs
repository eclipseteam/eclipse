using System;
using System.Data;

using Oritax.TaxSimp.CalculationInterface;

namespace Oritax.TaxSimp.Calculation
{
	/// <summary>
	/// DSCalculationModule is a DataSet that contains all of the persistent data for a CM instance.
	/// </summary>
	[Serializable] 
	public class SystemModuleBaseDS : BrokerManagedComponentDS
	{
		public const string SYSTEMSM_TABLE			= "SYSTEMSM";
//		public const string SYSTEMCIID_FIELD		= "CIID";
//		public const string SYSTEMNAME_FIELD		= "NAME";
		public const string SYSTEMPARENTBREADCRUMB_FIELD		= "PARENTBREADCRUMB";
		public const string SYSTEMBREADCRUMB_FIELD		= "BREADCRUMB";

		public SystemModuleBaseDS() : base()
		{
			DataTable table;
			table = new DataTable(SYSTEMSM_TABLE);
//			table.Columns.Add(SYSTEMCIID_FIELD, typeof(System.Guid));
//			table.Columns.Add(SYSTEMNAME_FIELD, typeof(System.String));
			table.Columns.Add(SYSTEMPARENTBREADCRUMB_FIELD, typeof(System.String));
			table.Columns.Add(SYSTEMBREADCRUMB_FIELD, typeof(System.String));
			this.Tables.Add(table);
			BrokerManagedComponentDS.AddVersionStamp(table,typeof(SystemModuleBaseDS));
		}

//		public Guid SysCIID
//		{
//			set
//			{
//
//				if(this.Tables[SystemModuleBaseDS.SYSTEMSM_TABLE].Rows.Count>0)
//				{
//					DataRow row=this.Tables[SystemModuleBaseDS.SYSTEMSM_TABLE].Rows[0];
//					row[SystemModuleBaseDS.SYSTEMCIID_FIELD]=value;
//				}
//				else
//				{
//					DataRow row=this.Tables[SystemModuleBaseDS.SYSTEMSM_TABLE].NewRow();
//					row[SystemModuleBaseDS.SYSTEMCIID_FIELD]=value;
//					this.Tables[SystemModuleBaseDS.SYSTEMSM_TABLE].Rows.Add(row);
//				}
//			}
//			get
//			{
//				if(this.Tables[SystemModuleBaseDS.SYSTEMSM_TABLE].Rows.Count>0)
//				{
//					DataRow row=this.Tables[SystemModuleBaseDS.SYSTEMSM_TABLE].Rows[0];
//					return (Guid)row[SystemModuleBaseDS.SYSTEMCIID_FIELD];
//				}
//				else
//					return Guid.Empty;
//			}
//		}
//		public string SysName
//		{
//			set
//			{
//
//				if(this.Tables[SystemModuleBaseDS.SYSTEMSM_TABLE].Rows.Count>0)
//				{
//					DataRow row=this.Tables[SystemModuleBaseDS.SYSTEMSM_TABLE].Rows[0];
//					row[SystemModuleBaseDS.SYSTEMNAME_FIELD]=value;
//				}
//				else
//				{
//					DataRow row=this.Tables[SystemModuleBaseDS.SYSTEMSM_TABLE].NewRow();
//					row[SystemModuleBaseDS.SYSTEMNAME_FIELD]=value;
//					this.Tables[SystemModuleBaseDS.SYSTEMSM_TABLE].Rows.Add(row);
//				}
//			}
//			get
//			{
//				if(this.Tables[SystemModuleBaseDS.SYSTEMSM_TABLE].Rows.Count>0)
//				{
//					DataRow row=this.Tables[SystemModuleBaseDS.SYSTEMSM_TABLE].Rows[0];
//					return (string)row[SystemModuleBaseDS.SYSTEMNAME_FIELD];
//				}
//				else
//					return string.Empty;
//			}
//		}
		public string SysParentBreadcrumb
		{
			set
			{

				if(this.Tables[SystemModuleBaseDS.SYSTEMSM_TABLE].Rows.Count>0)
				{
					DataRow row=this.Tables[SystemModuleBaseDS.SYSTEMSM_TABLE].Rows[0];
					row[SystemModuleBaseDS.SYSTEMPARENTBREADCRUMB_FIELD]=value;
				}
				else
				{
					DataRow row=this.Tables[SystemModuleBaseDS.SYSTEMSM_TABLE].NewRow();
					row[SystemModuleBaseDS.SYSTEMPARENTBREADCRUMB_FIELD]=value;
					this.Tables[SystemModuleBaseDS.SYSTEMSM_TABLE].Rows.Add(row);
				}
			}
			get
			{
				if(this.Tables[SystemModuleBaseDS.SYSTEMSM_TABLE].Rows.Count>0)
				{
					DataRow row=this.Tables[SystemModuleBaseDS.SYSTEMSM_TABLE].Rows[0];
					return row[SystemModuleBaseDS.SYSTEMPARENTBREADCRUMB_FIELD].ToString();
				}
				else
					return string.Empty;
			}
		}
		public string SysBreadcrumb
		{
			set
			{

				if(this.Tables[SystemModuleBaseDS.SYSTEMSM_TABLE].Rows.Count>0)
				{
					DataRow row=this.Tables[SystemModuleBaseDS.SYSTEMSM_TABLE].Rows[0];
					row[SystemModuleBaseDS.SYSTEMBREADCRUMB_FIELD]=value;
				}
				else
				{
					DataRow row=this.Tables[SystemModuleBaseDS.SYSTEMSM_TABLE].NewRow();
					row[SystemModuleBaseDS.SYSTEMBREADCRUMB_FIELD]=value;
					this.Tables[SystemModuleBaseDS.SYSTEMSM_TABLE].Rows.Add(row);
				}
			}
			get
			{
				if(this.Tables[SystemModuleBaseDS.SYSTEMSM_TABLE].Rows.Count>0)
				{
					DataRow row=this.Tables[SystemModuleBaseDS.SYSTEMSM_TABLE].Rows[0];
					return row[SystemModuleBaseDS.SYSTEMBREADCRUMB_FIELD].ToString();
				}
				else
					return string.Empty;
			}
		}
	}
}

