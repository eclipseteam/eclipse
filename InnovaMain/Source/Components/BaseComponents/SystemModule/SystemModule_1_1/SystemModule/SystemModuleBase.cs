using System;
using System.Collections;
using System.Data;
using System.Reflection;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Text;

using Oritax.TaxSimp.Calculation;
//using Oritax.TaxSimp.DSCalculation;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.Calculation
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class SystemModuleBase : BrokerManagedComponent//, ISerializable
	{

		//USING CUSTOM SERIALIZATION.
		//Still mark the fields as [NonSerialized] for your own infomation but this is ignored now by .NET
		//You must add your own serialzation code for each instance
		//See GetObjectData & Special Constructor

		private string m_strParentBreadCrumb = String.Empty;
		private string m_strBreadCrumb = String.Empty;


		public SystemModuleBase() : base()
		{
		}

		protected SystemModuleBase(SerializationInfo si, StreamingContext context)
			: base(si,context)
		{
			m_strParentBreadCrumb=Serialize.GetSerializedString(si,"smb_m_strParentBreadCrumb");
			m_strBreadCrumb=Serialize.GetSerializedString(si,"smb_m_strBreadCrumb");
		}
	
		//Only allow the .NET Serialization core code to call this function
		[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]
		public override void GetObjectData(SerializationInfo si, StreamingContext context)
		{
			base.GetObjectData(si,context);

			Serialize.AddSerializedValue(si,"smb_m_strParentBreadCrumb",m_strParentBreadCrumb);
			Serialize.AddSerializedValue(si,"smb_m_strBreadCrumb",m_strBreadCrumb);
		}


		public override void Initialize( String name )
		{
			base.Initialize( name );

			if( this.CID == Guid.Empty )
				this.CID = Guid.NewGuid();
		}

		public virtual IEnumerable WorkPapers
		{
			get
			{
				return new ArrayList();
			}
		}


		public virtual string ConfigureWPURL
		{
			get
			{
				return String.Empty;
			}
		}

		public Guid CIID
		{
			get
			{
				return this.CID;
			}
			set
			{
				this.CID = value;
			}
		}


		public string BreadCrumb
		{
			get
			{
				return this.m_strParentBreadCrumb + "/" + this.m_strBreadCrumb;
			}
		}

		public string ParentBreadCrumb
		{
			get
			{
				return this.m_strParentBreadCrumb;
			}
			set
			{
				this.m_strParentBreadCrumb = value;
			}
		}

		protected override void OnGetData(DataSet data)
		{
			base.OnGetData(data);
			if (data is SystemModuleBaseDS)
			{
				SystemModuleBaseDS oSystemModuleBaseDS = data as SystemModuleBaseDS;
				oSystemModuleBaseDS.SysParentBreadcrumb=m_strParentBreadCrumb;
				oSystemModuleBaseDS.SysBreadcrumb=m_strBreadCrumb;
			}
		}

		protected override ModifiedState OnSetData(DataSet data)
		{
			base.OnSetData(data);
			if (data is SystemModuleBaseDS)
			{
				DeliverPrimaryData((SystemModuleBaseDS)data);
			}
			return ModifiedState.MODIFIED;
		}

		public override void DeliverData(DataSet data)
		{
			base.DeliverData(data);
			if(data is SystemModuleBaseDS)
			{
				DeliverPrimaryData(data);
			}
		}

		private new void DeliverPrimaryData(DataSet data)
		{
			SystemModuleBaseDS primaryDS=(SystemModuleBaseDS)data;
			m_strParentBreadCrumb=primaryDS.SysParentBreadcrumb;
			m_strBreadCrumb=primaryDS.SysBreadcrumb;
		}

		public override DataSet MigrateDataSet(DataSet data)
		{
			DataSet baseDataset = base.MigrateDataSet(data);
			//can't find previous version
			Assembly thisAssembly = Assembly.GetExecutingAssembly();
			if(BrokerManagedComponentDS.GetFullName(data.Tables[SystemModuleBaseDS.SYSTEMSM_TABLE]) == thisAssembly.FullName)
			{
				SystemModuleBaseDS ds = new SystemModuleBaseDS();

				//Merge in all of the component tables
				ds.Merge(data.Tables[SystemModuleBaseDS.SYSTEMSM_TABLE]);
				DeliverPrimaryData(ds);

				return ds;
			}
			return baseDataset;
		}
		
		public override void MigrationCompleted()
		{
			base.MigrationCompleted ();
            this.Broker.LogEvent(EventType.PerformMigration, Guid.Empty, "Perform Migration for '" + this.BreadCrumb + "'");
		}


		/// <summary>
		/// Creates a unique BMC name for this type of BMC. Use this method if you want to ensure that a name is unique for this type of BMC.
		/// </summary>
		/// <param name="sourceName">Proposed name of BMC</param>
		/// <returns>A unqiue "copy" name for this BMC, i.e. "Copy (1) of My BMC Name"</returns>
		public string CreateUniqueNameForCopy(string sourceName)
		{
			StringBuilder sb = new StringBuilder();
			int attemptCount=1;
			string newName = "Copy of " + sourceName;

			for(SystemModuleBase bmc=(SystemModuleBase)Broker.GetBMCInstance(newName, this.TypeName); bmc!=null && bmc.CID!=CID; bmc=(SystemModuleBase)Broker.GetBMCInstance(newName, this.TypeName))
				newName = sb.Remove(0,sb.Length).Append("Copy (").Append((++attemptCount).ToString()).Append(") of ").Append(sourceName).ToString();

			return newName;
		}

		public bool NameIsUniqueForType()
		{
			return this.NameIsUniqueForType(this.Name);
		}

		public bool NameIsUniqueForType(string name)
		{
			if(this.Broker != null)
			{
				SystemModuleBase bmc=(SystemModuleBase)Broker.GetBMCInstance(name, this.TypeName);
				return bmc==null || bmc.CID==CID;
			}
			else
				return true;
		}
	}
}
