using System;

namespace Oritax.TaxSimp.Calculation
{
	/// <summary>
	/// Summary description for BrokerManagedComponentBaseFW.
	/// </summary>
	public class SystemModuleInstall
	{
		#region INSTALLATION PROPERTIES

		// Assembly Installation Properties
		public const string ASSEMBLY_ID="BE13189E-76C0-4c51-8B35-E88197E2ED92";
		public const string ASSEMBLY_NAME="SystemModule_1_1";
		public const string ASSEMBLY_DISPLAYNAME="SystemModule V1.1";
		public const string ASSEMBLY_MAJORVERSION="1";
		public const string ASSEMBLY_MINORVERSION="1";
		public const string ASSEMBLY_DATAFORMAT="0";
//		public const string ASSEMBLY_REVISION="0";
		public const string ASSEMBLY_REVISION="1"; //2005.1

		// Component Installation Properties
		public const string COMPONENT_ID="281A7C1D-D5ED-46d4-A6FB-AE7AF05D7926";
		public const string COMPONENT_NAME="SystemModule";
		public const string COMPONENT_DISPLAYNAME="System Module";
		public const string COMPONENT_CATEGORY="Base";

		// Component Version Installation Properties
		public const string COMPONENTVERSION_STARTDATE="01/01/1990 00:00:00 AM";
		public const string COMPONENTVERSION_ENDDATE="31/12/2050 12:59:59 AM";
		public const string COMPONENTVERSION_PERSISTERASSEMBLY="BrokerManagedComponent_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";
		public const string COMPONENTVERSION_PERSISTERCLASS="Oritax.TaxSimp.Calculation.BMCPersister";
		public const string COMPONENTVERSION_IMPLEMENTATIONCLASS="Oritax.TaxSimp.Calculation.SystemModuleBase";

		#endregion
	}
}
