using System;

namespace Oritax.TaxSimp.Calculation
{
	/// <summary>
	/// Summary description for BrokerManagedComponentBaseFW.
	/// </summary>
	public class BrokerManagedComponentInstall
	{
		#region INSTALLATION PROPERTIES

		// Assembly Installation Properties
		public const string ASSEMBLY_ID="0B43BFD2-EF53-4fda-90C6-7626321EBF23";
		public const string ASSEMBLY_NAME="BrokerManagedComponent_1_1";
		public const string ASSEMBLY_DISPLAYNAME="BrokerManagedComponent V1.1";
		public const string ASSEMBLY_MAJORVERSION="1";
		public const string ASSEMBLY_MINORVERSION="1";
		//public const string ASSEMBLY_DATAFORMAT="0";
		public const string ASSEMBLY_DATAFORMAT="1";	//2005.1
		//public const string ASSEMBLY_REVISION="0";	//2004.3.1
		//public const string ASSEMBLY_REVISION="1";	//2004.3.2
		//public const string ASSEMBLY_REVISION="2";	//2004.3.3
		//public const string ASSEMBLY_REVISION="3";		//2005.1
		public const string ASSEMBLY_REVISION="4";		//2006.1


		// Component Installation Properties
		public const string COMPONENT_ID="94F0B06B-505E-4194-9FA6-79E5244D779F";
		public const string COMPONENT_NAME="BrokerManagedComponent";
		public const string COMPONENT_DISPLAYNAME="Broker Managed Component";
		public const string COMPONENT_CATEGORY="Base";

		// Component Version Installation Properties
		public const string COMPONENTVERSION_STARTDATE="01/01/1990 00:00:00 AM";
		public const string COMPONENTVERSION_ENDDATE="31/12/2050 12:59:59 AM";
		public const string COMPONENTVERSION_PERSISTERASSEMBLY="BrokerManagedComponent_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";
		public const string COMPONENTVERSION_PERSISTERCLASS="Oritax.TaxSimp.Calculation.BrokerManagedComponentPersister";
		public const string COMPONENTVERSION_IMPLEMENTATIONCLASS="Oritax.TaxSimp.Calculation.BrokerManagedComponent";

		// Data Model
		public const string DATAMODEL_MANAGERASSEMBLY="BrokerManagedComponent_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";
		public const string DATAMODEL_MANAGERCLASS="Oritax.TaxSimp.Calculation.BrokerManagedComponentPersister";

		#endregion
	}
}
