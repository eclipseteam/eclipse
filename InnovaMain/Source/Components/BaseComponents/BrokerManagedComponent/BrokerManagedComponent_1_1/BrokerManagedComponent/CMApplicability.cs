using System;

namespace Oritax.TaxSimp.Calculation
{
	/// <summary>
	/// Summary description for CMApplicability.
	/// </summary>
	public enum CMApplicability
	{
		Entity = 1,
		Group = 2,
		All = 3
	}
}
