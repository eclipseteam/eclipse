using System;
using System.Data;

namespace DTT.TX360Enterprise.Calculation
{
	/// <summary>
	/// DSCalculationModule is a DataSet that contains all of the persistent data for a CM instance.
	/// </summary>
	[System.ComponentModel.DesignerCategory("Code")]
	[SerializableAttribute] 
	public class DSACLEntries : DataSet
	{
		public const string ACLENTRY_TABLE				= "ACLENTRY";
		public const string BMCCID_FIELD				= "CID";
		public const string PARTYCID_FIELD				= "PARTYCID";
		public const string PARTYNAME_FIELD				= "PARTYNAME";
		public const string PRIVILEGETYPE_FIELD			= "PRIVILEGETYPE";
		public const string PRIVILEGEVALUE_FIELD		= "PRIVILEGEVALUE";

		public DSACLEntries()
		{
			DataTable table;
			DataColumnCollection columns;

			// Add the table for the instance data
			table = new DataTable(ACLENTRY_TABLE);
			columns = table.Columns;
			columns.Add(BMCCID_FIELD, typeof(System.String));
			columns.Add(PARTYCID_FIELD, typeof(System.Guid));
			columns.Add(PARTYNAME_FIELD, typeof(System.String));
			columns.Add(PRIVILEGETYPE_FIELD, typeof(System.Int32));
			columns.Add(PRIVILEGEVALUE_FIELD, typeof(System.Int32));
			this.Tables.Add(table);
		}
	}
}
