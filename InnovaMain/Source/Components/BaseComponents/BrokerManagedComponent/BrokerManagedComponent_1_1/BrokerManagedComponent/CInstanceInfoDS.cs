using System;
using System.Data;
using System.Reflection;

namespace Oritax.TaxSimp.Calculation
{
	[Serializable]
	public class CInstanceInfoDS : PrimaryDS
	{
		public class CInstanceInfoDSInvalidStateException : Exception
		{
			public CInstanceInfoDSInvalidStateException()
				: base("The DataSet is not validly populated"){}
		}

		//public const string CINSTANCES_TABLE			= "CINSTANCES";
		public const string CID_FIELD					= "CID";
		public const string CTID_FIELD					= "CTID";
		public const string NAME_FIELD					= "NAME";

		//public const string CMTYPES_TABLE				= "CMTYPES";
		public const string TYPEID_FIELD				= "CMTYPEID";
		public const string TYPENAME_FIELD				= "TYPENAME";
		public const string ASSEMBLY_FIELD				= "ASSEMBLY";
		public const string TYPE_FIELD					= "TYPE";
		public const string DISPLAYNAME_FIELD			= "DISPLAYNAME";
		public const string CATEGORYNAME_FIELD			= "CATEGORYNAME";
		public const string PASSEMBLY_FIELD				= "PASSEMBLY";
		public const string PCLASS_FIELD				= "PCLASS";
		public const string APPLICABILITY_FIELD			= "APPLICABILITY";
		public const string MAJORVERSION_FIELD			= "MAJORVERSION";
		public const string MINORVERSION_FIELD			= "MINORVERSION";
		public const string RELEASE_FIELD				= "RELEASE";


		public Guid InstanceCID{get{return (Guid)GetInstanceRowProperty(CID_FIELD);}}
		public Guid InstanceCTID{get{return (Guid)GetInstanceRowProperty(CTID_FIELD);}}
		public string InstanceName{get{return (string)GetInstanceRowProperty(NAME_FIELD);}}
		public string ImplementationAssemblyName{get{return (string)GetTypeRowProperty(ASSEMBLY_FIELD);}}
		public string ImplementationClassName{get{return (string)GetTypeRowProperty(TYPE_FIELD);}}

		private object GetInstanceRowProperty(string name)
		{
			try
			{
				return this.Tables[BrokerManagedComponentDS.CINSTANCES_TABLE].Rows[0][name];
			}
			catch(Exception)
			{
				throw new CInstanceInfoDSInvalidStateException();
			}		
		}

		private object GetTypeRowProperty(string name)
		{
			try
			{
				return this.Tables[BrokerManagedComponentDS.CMTYPES_TABLE].Rows[0][name];
			}
			catch(Exception)
			{
				throw new CInstanceInfoDSInvalidStateException();
			}		
		}

		public CInstanceInfoDS()
		{
			// CINSTANCES TABLE=======================================================================
			DefineRootTable(			
				BrokerManagedComponentDS.CINSTANCES_TABLE,
				new FieldDefinition[]{
										 new FieldDefinition(CID_FIELD,typeof(System.Guid)),
										 new FieldDefinition(CTID_FIELD,typeof(System.Guid)),
										 new FieldDefinition(NAME_FIELD,typeof(System.String))
									 },
				CID_FIELD,
				PersistenceTypeOption.READONLY
				);

			// CMTYPES TABLE==========================================================================
			DefineRootTable(
				BrokerManagedComponentDS.CMTYPES_TABLE,
				new FieldDefinition[]{
					new FieldDefinition(TYPEID_FIELD,typeof(System.Guid)),
					new FieldDefinition(TYPENAME_FIELD,typeof(System.String)),
					new FieldDefinition(ASSEMBLY_FIELD,typeof(System.String)),
					new FieldDefinition(TYPE_FIELD,typeof(System.String)),
					new FieldDefinition(CATEGORYNAME_FIELD,typeof(System.String)),
					new FieldDefinition(PASSEMBLY_FIELD,typeof(System.String)),
					new FieldDefinition(PCLASS_FIELD,typeof(System.String)),
					new FieldDefinition(APPLICABILITY_FIELD,typeof(System.Int32)),
					new FieldDefinition(MAJORVERSION_FIELD,typeof(System.Int32)),
					new FieldDefinition(MINORVERSION_FIELD,typeof(System.Int32)),
					new FieldDefinition(RELEASE_FIELD,typeof(System.Int32)),
				},
				TYPEID_FIELD,
				PersistenceTypeOption.READONLY
			);
		}
	}
}
