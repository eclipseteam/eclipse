using System;
using System.Data;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.WPDatasetBase;

namespace Oritax.TaxSimp.Calculation
{
	/// <summary>
	/// The DataSet Class used for Notes
	/// </summary>
	[Serializable()]
	public class NoteListDS : WPDatasetBaseDS
	{
		[NonSerialized]public string BreadCrumb;

		public const String NOTES_TABLE = "NOTES_TABLE";
		public const String NOTES_PRIMARYKEY_FIELD = "Key";
		public const String NOTES_PREV_PRIMARYKEY_FIELD = "PreKey";
		public const String NOTES_DESCRIPTION_FIELD = "NOTES_DESCRIPTION_FIELD";
		public const String NOTES_CREATEDBY_FIELD = "NOTES_CREATEDBY_FIELD";
		public const String NOTES_CREATIONDATE_FIELD = "NOTES_CREATEDDATE_FIELD";
		public const String NOTES_LASTUSERUPDATED_FIELD = "NOTES_LASTUSERUPDATED_FIELD";
		public const String NOTES_LASTDATEUPDATED_FIELD = "NOTES_LASTDATEUPDATED_FIELD";
		public const String NOTES_ROLLFORWARD_FIELD = "NOTES_ROLLFORWARD_FIELD";
		public const String NOTES_WORKPAPERID_FIELD = "NOTES_WORKPAPERID_FIELD";

		public NoteListDS()
		{
			DataTable oDT = new DataTable(NoteListDS.NOTES_TABLE);
			oDT.Columns.Add(NoteListDS.NOTES_DESCRIPTION_FIELD,typeof(System.String)); 
			oDT.Columns.Add(NoteListDS.NOTES_LASTUSERUPDATED_FIELD,typeof(System.String)); 
			oDT.Columns.Add(NoteListDS.NOTES_LASTDATEUPDATED_FIELD,typeof(System.DateTime)); 
			oDT.Columns.Add(NoteListDS.NOTES_ROLLFORWARD_FIELD,typeof(System.Boolean)); 
			oDT.Columns.Add(NoteListDS.NOTES_CREATEDBY_FIELD,typeof(System.String));  
			oDT.Columns.Add(NoteListDS.NOTES_CREATIONDATE_FIELD,typeof(System.DateTime)); 
			oDT.Columns.Add(NoteListDS.NOTES_PRIMARYKEY_FIELD,typeof(System.Guid));
			oDT.Columns.Add(NoteListDS.NOTES_PREV_PRIMARYKEY_FIELD, typeof(System.Guid)); 
			oDT.Columns.Add(NoteListDS.NOTES_WORKPAPERID_FIELD,typeof(System.Guid)); 
			Tables.Add(oDT);
			BrokerManagedComponentDS.AddVersionStamp(oDT,typeof(NoteListDS));
		}
	}
}
