using System;
using System.Data;
using System.Collections;
using System.Runtime.Serialization;
using System.Security.Permissions;

using Oritax.TaxSimp.CalculationInterface;

namespace Oritax.TaxSimp.Utilities
{

	/// <summary>
	/// Summary description for Message.
	/// </summary>
	[Serializable]
	public class Message : SerializableHashtable, IMessage, ISerializable
	{
		#region INTERNAL CLASSES
		/// <summary>
		/// Contains the source information of the message, a list of the publisher
		/// and on-publisher CM IDs.
		/// </summary>
		[Serializable]
		public class PublisherCategorySet : SerializableHashtable, ISerializable
		{
			public PublisherCategorySet():base(){}

			protected PublisherCategorySet(SerializationInfo si, StreamingContext context)
				:base(si,context)
			{
			}
	
			//Only allow the .NET Serialization core code to call this function
			[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]
			public override void GetObjectData(SerializationInfo si, StreamingContext context)
			{
				base.GetObjectData(si,context);
			}

			public void Add(PublisherData publisherData)
			{
				if(base.Contains(publisherData.CMCategoryName))
					base[publisherData.CMCategoryName]=publisherData;
				else
					base.Add(publisherData.CMCategoryName,publisherData);
			}
			public void Add(Guid cLID,Guid cSID,Guid cIID,string cMName,string cmTypeName,string cMCategoryName)
			{
				base.Add(cMCategoryName,new PublisherData(cLID,cSID,cIID,cMName,cmTypeName,cMCategoryName));
			}
			public PublisherData this[string cMCategoryName]
			{
				get
				{
					Object publisher=base[cMCategoryName];
					if(((object)publisher)!=null)
						return (PublisherData)publisher;
					else
						return null;
				}
			}
		}
		[Serializable]
		public class PublisherTypeSet : SerializableHashtable, ISerializable
		{
			public PublisherTypeSet():base(){}
			protected PublisherTypeSet(SerializationInfo si, StreamingContext context)
				:base(si,context)
			{
			}
	
			//Only allow the .NET Serialization core code to call this function
			[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]
			public override void GetObjectData(SerializationInfo si, StreamingContext context)
			{
				base.GetObjectData(si,context);
			}

			public void Add(PublisherData publisherData)
			{
				if(base.Contains(publisherData.CMTypeName))
					base[publisherData.CMTypeName]=publisherData;
				else
					base.Add(publisherData.CMTypeName,publisherData);
			}
			public void Add(Guid cLID,Guid cSID,Guid cIID,string cMName,string cmTypeName,string cMTypeName)
			{
				base.Add(cMTypeName,new PublisherData(cLID,cSID,cIID,cMName,cmTypeName,cMTypeName));
			}
			public PublisherData this[string cMTypeName]
			{
				get
				{
					Object publisher=base[cMTypeName];
					if(((object)publisher)!=null)
						return (PublisherData)publisher;
					else
						return null;
				}
			}
		}
		#endregion
		#region FIELD VARIABLES
		//USING CUSTOM SERIALIZATION.
		//Still mark the fields as [NonSerialized] for your own infomation but this is ignored now by .NET
		//You must add your own serialzation code for each instance
		//See GetObjectData & Special Constructor

		[NonSerialized]
		PublisherCategorySet publishersByCategory=new PublisherCategorySet();

		[NonSerialized]
		PublisherTypeSet publishersByType=new PublisherTypeSet();

		private PublisherData m_objOriginatingPublisher;

		/// <summary>
		/// The number of levels of hierarchy through which the message will be allowed to pass.
		/// </summary>
		[NonSerialized]
		int scope=0;

		[NonSerialized]
		Guid mSID;				//  Message source scenario ID

		[NonSerialized]
		bool interScenario;		// If true, the message is routed across scenarios
		
		string dataType;		// For debug

		[NonSerialized]
		Guid iD = Guid.NewGuid();

		#endregion
		#region PROPERTIES
		public IEnumerable PublishersByType{get{return publishersByType;}}
		public IEnumerable PublishersByCategory{get{return publishersByCategory;}}
		public int Scope{get{return scope;}set{scope=value;}}
		public bool InterScenario{get{return interScenario;}set{interScenario=value;}}
		public Guid MSID{get{return mSID;}set{mSID=value;}}
		public string Datatype{get{return dataType;}}
		public PublisherData OriginatingPublisher{get{return this.m_objOriginatingPublisher;}}
		public Guid ID {get{return iD;}set{iD = value;}}
		#endregion
		#region CONSTRUCTORS
		public Message(){}

		/// <summary>
		/// Constructs a new message object adding all entries in the sortedlist to the 
		/// message
		/// </summary>
		/// <param name="list"></param>
		public Message(SortedList list)
		{
			foreach(DictionaryEntry dictEntry in list)
				this.Add((string)dictEntry.Key, dictEntry.Value);
		}
		
		protected Message(SerializationInfo si, StreamingContext context)
			:base(si,context)
		{
			m_objOriginatingPublisher=(PublisherData)Serialize.GetSerializedValue(si,"msg_m_objOriginatingPublisher",typeof(PublisherData),null);
			dataType=Serialize.GetSerializedString(si,"msg_dataType");
		}
	
		//Only allow the .NET Serialization core code to call this function
		[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]
		public override void GetObjectData(SerializationInfo si, StreamingContext context)
		{
			base.GetObjectData(si,context);
			Serialize.AddSerializedValue(si,"msg_m_objOriginatingPublisher",m_objOriginatingPublisher);
			Serialize.AddSerializedValue(si,"msg_dataType",dataType);
		}

		/// <summary>
		/// Constructor that brands message with publisher information.
		/// </summary>
		/// <param name="publisher">The publishing CM</param>
		/// <param name="scope"></param>
		public Message(ICalculationModule publisher,int scope)
		{
			PublisherData publisherData=new PublisherData(publisher.CLID,publisher.CSID,publisher.CMID,((IBrokerManagedComponent)publisher).Name,((IBrokerManagedComponent)publisher).TypeName,((IBrokerManagedComponent)publisher).CategoryName);
			publishersByCategory.Add(publisherData);
			publishersByType.Add(publisherData);
			this.m_objOriginatingPublisher = publisherData;
			this.scope=scope;
			this.MSID=publisher.CSID;
			this.interScenario=false;
		}

		public Message(ICalculationModule publisher,int scope, bool interScenario)
		{
			PublisherData publisherData=new PublisherData(publisher.CLID,publisher.CSID,publisher.CMID,((IBrokerManagedComponent)publisher).Name,((IBrokerManagedComponent)publisher).TypeName,((IBrokerManagedComponent)publisher).CategoryName);
			publishersByCategory.Add(publisherData);
			publishersByType.Add(publisherData);
			this.m_objOriginatingPublisher = publisherData;
			this.scope=scope;
			this.MSID=publisher.CSID;
			this.interScenario=interScenario;
		}

		/// <summary>
		/// Copy constructor that adds on-publisher message information. The scope is reduced by one.
		/// </summary>
		/// <param name="publisher">The publishing CM</param>
		/// <param name="message"></param>
		public Message(ICalculationModule publisher, IMessage message)
		{
			// Copy all the message object references - SHALLOW COPY
			this.CopyMessageData(message);

			// Copy all the publishers - SHALLOW COPY
			foreach(DictionaryEntry publisherData in message.PublishersByType)
			{
				publishersByType.Add((PublisherData)publisherData.Value);
				publishersByCategory.Add((PublisherData)publisherData.Value);
			}
			// Add the on-publisher as a new publisher
			PublisherData onPublisherData=new PublisherData(publisher.CLID,publisher.CSID,publisher.CMID,((IBrokerManagedComponent)publisher).Name,((IBrokerManagedComponent)publisher).TypeName,((IBrokerManagedComponent)publisher).CategoryName);
			publishersByCategory.Add(onPublisherData);
			publishersByType.Add(onPublisherData);
			this.m_objOriginatingPublisher = message.OriginatingPublisher;
			scope=message.Scope-1;
			interScenario=message.InterScenario;
			this.MSID=message.MSID;
			this.dataType=message.Datatype;
		}

		/// <summary>
		/// Copy constructor that does not add on-publisher message information. The scope is increased by one.
		/// </summary>
		/// <param name="message">The publishing CM</param>
		public Message(IMessage message)
		{
			// Copy all the message object references - SHALLOW COPY
			this.CopyMessageData(message);

			// Copy all the publishers - SHALLOW COPY
			foreach(DictionaryEntry publisherData in message.PublishersByType)
			{
				publishersByType.Add((PublisherData)publisherData.Value);
				publishersByCategory.Add((PublisherData)publisherData.Value);
			}
			scope=message.Scope+1;
			interScenario=message.InterScenario;
			this.m_objOriginatingPublisher = message.OriginatingPublisher;
			this.MSID=message.MSID;
			this.dataType=message.Datatype;
		}
		#endregion
		#region MEMBER FUNCTIONS
		public PublisherData GetPublisherByCMTypeName(string cMTypeName)
		{
			PublisherData publisher=publishersByType[cMTypeName];
			return ((Object)publisher)!=null ? publisher : null;
		}

		public PublisherData GetPublisherByCMCategoryName(string cMCategoryName)
		{
			PublisherData publisher=publishersByCategory[cMCategoryName];
			return ((Object)publisher)!=null ? publisher : null;
		}

		public bool IsPublisher(Guid cLID)
		{
			foreach(DictionaryEntry publisherData in publishersByType)
			{
				if(((PublisherData)publisherData.Value).CLID==cLID)
					return true;
			}
			return false;
		}

		// Add an item to collection
		public void Add(String key, object newObject)
		{
			base.Add(key, newObject);
			if(key=="DataType")
				dataType=(string)newObject;
		}

		// Return true if collection contains an item with key
		public bool Contains(String key)
		{
			return base.Contains(key);
		}

		public void Remove(String key)
		{
			base.Remove(key);
		}

		// Get/Set selected item
		public object this[String key]
		{
			get
			{
				return base[key];
			}
			set
			{
				base[key]=value;
				if(key=="DataType")
					dataType=(string)value;
			}
		}

		/// <summary>
		/// Copies all dictionary entries 
		/// </summary>
		/// <param name="objMessge"></param>
		public void CopyMessageData(IMessage objMessge)
		{
			// Copy all the message object references - SHALLOW COPY
			foreach(DictionaryEntry  entry in objMessge)
			{
				if(!base.Contains(entry.Key))
					this.Add(entry.Key.ToString(), entry.Value);
				else
					this[entry.Key.ToString()] = entry.Value;
			}

		}

		
		public void DeliverData(DataSet dataSet)
		{
			if(dataSet is MessageDS)
			{
				MessageDS messageDataSet = (MessageDS)dataSet;

				publishersByType.Clear();
				publishersByCategory.Clear();

				this.dataType = messageDataSet.Datatype;
				this.Scope = messageDataSet.Scope;
				this.InterScenario = messageDataSet.InterScenario;
				this.MSID = messageDataSet.MSID;

				base.Clear();
				//Extract all dicitionary entries
				foreach(DataRow dRow in messageDataSet.Tables[MessageDS.MESSAGEDATA_TABLE].Rows)
					base.Add(dRow[MessageDS.KEY_FIELD].ToString(), MigrationUtilities.GetColumnValue( dRow, MessageDS.VALUE_FIELD ) );
			
				// Copy all the publisher data
				foreach(DataRow dRow in messageDataSet.Tables[MessageDS.PUBLISHERDATA_TABLE].Rows)
				{
					PublisherData pubData = new PublisherData(
						(Guid)dRow[MessageDS.CLID_FIELD],
						(Guid)dRow[MessageDS.CSID_FIELD],
						(Guid)dRow[MessageDS.CIID_FIELD],
						dRow[MessageDS.CMNAME_FIELD].ToString(),
						dRow[MessageDS.CMTYPENAME_FIELD].ToString(),
						dRow[MessageDS.CMCATEGORYNAME_FIELD].ToString());

					if( (bool)dRow[MessageDS.ISORIGINATOR_FIELD] )
						this.m_objOriginatingPublisher = pubData;

					publishersByType.Add(pubData);
					publishersByCategory.Add(pubData);
				}
			}

		}
		//Extract all data for the message and populate MessageDS
		public void ExtractData(DataSet dataSet)
		{
			if(dataSet is MessageDS)
			{
				MessageDS messageDataSet = (MessageDS)dataSet;

				DataRow newRow;

				messageDataSet.Datatype			= this.Datatype;
				messageDataSet.Scope			= this.Scope;
				messageDataSet.InterScenario	= this.InterScenario;
				messageDataSet.MSID				= this.MSID;

				//Extract all dicitionary entries
				foreach(DictionaryEntry entry in this)
				{
					newRow = messageDataSet.Tables[MessageDS.MESSAGEDATA_TABLE].NewRow();
					newRow[MessageDS.KEY_FIELD] = entry.Key.ToString();
					MigrationUtilities.SetColumnValue( newRow, MessageDS.VALUE_FIELD, entry.Value );
					messageDataSet.Tables[MessageDS.MESSAGEDATA_TABLE].Rows.Add(newRow);
				}

				// Copy all the publisher data
				foreach(DictionaryEntry publisherData in this.PublishersByType)
				{
					PublisherData pubData = (PublisherData)publisherData.Value;
					newRow = dataSet.Tables[MessageDS.PUBLISHERDATA_TABLE].NewRow();
					newRow[MessageDS.CLID_FIELD] = pubData.CLID;
					newRow[MessageDS.CSID_FIELD] = pubData.CSID;
					newRow[MessageDS.CIID_FIELD] = pubData.CIID;
					newRow[MessageDS.CMNAME_FIELD] = pubData.CMName;
					newRow[MessageDS.CMTYPENAME_FIELD] = pubData.CMTypeName;
					newRow[MessageDS.CMCATEGORYNAME_FIELD] = pubData.CMCategoryName;
					if(pubData.CIID == this.OriginatingPublisher.CIID)
						newRow[MessageDS.ISORIGINATOR_FIELD] = true;
					else
						newRow[MessageDS.ISORIGINATOR_FIELD] = false;
					dataSet.Tables[MessageDS.PUBLISHERDATA_TABLE].Rows.Add(newRow);
				}
			}
		}

		
		/// <summary>
		/// Clears out all existing publisher data and adds a
		/// new entry for a "BusinessEntity" of a given CLID.
		/// </summary>
		/// <param name="publisherCLID"></param>
		public void SetBusinessEntityPublisherData(Guid publisherCLID)
		{
			this.publishersByCategory = new PublisherCategorySet();
			this.publishersByType =new PublisherTypeSet();

			PublisherData onPublisherData=new PublisherData(publisherCLID,Guid.Empty,Guid.Empty,String.Empty, String.Empty, "BusinessEntity");
			publishersByCategory.Add(onPublisherData);
			publishersByType.Add(onPublisherData);
		}

		/// <summary>
		/// Returns a sorted list of the messages base hashtable.
		/// </summary>
		/// <returns></returns>
		public SortedList ToSortedList()
		{
			return new SortedList((IDictionary)this);
		}

		
	
		#endregion
	}
}
