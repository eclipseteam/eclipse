using System;
using System.Collections;
using System.Data;
using System.Runtime.Serialization;
using System.Reflection;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Calculation;
using System.IO;

namespace Oritax.TaxSimp.WPDatasetBase
{
	/// <summary>
	/// Base dataset class containing the WorkPaperType member
	/// </summary>
	public abstract class WPDatasetBaseDS : DataSet
    {
		#region Constants
		public const string BREADCRUMBTABLE = "BREADCRUMBTABLE";
		public const string BREADCRUMBFIELD = "BREADCRUMB";

		public const string NOTESATTACHMENTSUMMARYTABLE = "NOTESATTACHMENTSUMMARYTABLE";
		public const string NOTESCOUNTFIELD = "NOTESCOUNTFIELD";
		public const string ATTACHMENTSCOUNTFIELD = "ATTACHMENTSCOUNTFIELD";

		public const string NOTESDETAILSTABLE = "NOTESDETAILSTABLE";
		public const string NOTESNAMEFIELD = "NOTESNAMEFIELD";
		public const string NOTESWORKPAPERIDFIELD = "NOTESWORKPAPERIDFIELD";
		#endregion //Constants
		#region Member variables
		protected WorkPaperType wpType;
		#endregion
		#region Constructors
		public WPDatasetBaseDS()
		{
			AddBreadCrumbTable();
			wpType = WorkPaperType.Primary;
			this.AddNotesAndAttachmentSummary();
			this.AddNotesDetailsTable();
		}

		public WPDatasetBaseDS(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			AddBreadCrumbTable();
			wpType = WorkPaperType.Primary;
			this.AddNotesAndAttachmentSummary();
			this.AddNotesDetailsTable();
		}

		public WPDatasetBaseDS(WorkPaperType wpTypeIn)
		{
			AddBreadCrumbTable();
			wpType = wpTypeIn;
			this.AddNotesAndAttachmentSummary();
			this.AddNotesDetailsTable();
		}
		#endregion //Constructors
		#region Properties

        public CMStatus CMStatus;

        public DataTable UserConfirmedTable
		{
			get
			{
				if(!this.Tables.Contains("UserConfirmed"))
					this.AddUserConfirmedTable();

				return this.Tables["UserConfirmed"];
			}
		}

		/// <summary>
		/// Returns the row for the default type of confirmation
		/// </summary>
		/// <param name="confirmType"></param>
		/// <returns></returns>
		private DataRow UserConfirmedRow(string confirmType)
		{
			DataRow userConfirmedRow;

			if ( confirmType == null || confirmType.Equals( string.Empty ) )
				confirmType = "DEFAULT";
			else
				confirmType = confirmType.ToUpper();

			// now get the desired row for the specified confirmation type
			DataRow [] rows = this.UserConfirmedTable.Select( "ID = '" + confirmType + "'" );
			if ( rows.Length == 0 )
			{
				// if it doesn't exist then create it
				userConfirmedRow = this.UserConfirmedTable.NewRow();
				userConfirmedRow["ID"] = confirmType;               // set key field
				userConfirmedRow["UserConfirmed"] = false;          // set default value
				this.UserConfirmedTable.Rows.Add(userConfirmedRow);
			}
			else
			{
				// it exists already so return it
				userConfirmedRow = rows[0];
			}

			return userConfirmedRow;
		}

		public string Breadcrumb
		{
			set
			{
				if(this.Tables[BREADCRUMBTABLE].Rows.Count==0)
					this.Tables[BREADCRUMBTABLE].Rows.Add(this.Tables[BREADCRUMBTABLE].NewRow());

				this.Tables[BREADCRUMBTABLE].Rows[0][BREADCRUMBFIELD] = value;
			}

			get
			{
				if(this.Tables[BREADCRUMBTABLE].Rows.Count==0)
					return String.Empty;
				else
					return this.Tables[BREADCRUMBTABLE].Rows[0][BREADCRUMBFIELD].ToString();
			}
		}

		public WorkPaperType WPType
		{
			get
			{
				return wpType;
			}
			set
			{
				wpType=value;
			}
		}

		/// <summary>
		/// Indicates that the user has confirmed a particular action explicitly
		/// </summary>
		public bool UserConfirmed
		{
			get
			{
				return GetUserConfirmed("DEFAULT");
			}
			set
			{
				SetUserConfirmed("DEFAULT", value);
			}
		}

		private DataTable WarningsTable
		{
			get
			{
				if(!this.Tables.Contains("WarningUserDisplayMessages"))
					this.AddWarningsTable();

				return this.Tables["WarningUserDisplayMessages"];
			}
		}

		public int NotesCount
		{
			get
			{
				if(this.Tables[NOTESATTACHMENTSUMMARYTABLE].Rows.Count == 0)
					AddInitialNotesAndAttachmentSummaryRow();

				return (int) this.Tables[NOTESATTACHMENTSUMMARYTABLE].Rows[0][NOTESCOUNTFIELD];
			}
			set
			{
				if(this.Tables[NOTESATTACHMENTSUMMARYTABLE].Rows.Count == 0)
					AddInitialNotesAndAttachmentSummaryRow();

				this.Tables[NOTESATTACHMENTSUMMARYTABLE].Rows[0][NOTESCOUNTFIELD] = value;
			}
		}

		public int AttachmentsCount
		{
			get
			{
				if(this.Tables[NOTESATTACHMENTSUMMARYTABLE].Rows.Count == 0)
					AddInitialNotesAndAttachmentSummaryRow();

				return (int) this.Tables[NOTESATTACHMENTSUMMARYTABLE].Rows[0][ATTACHMENTSCOUNTFIELD];
			}
			set
			{
				if(this.Tables[NOTESATTACHMENTSUMMARYTABLE].Rows.Count == 0)
					AddInitialNotesAndAttachmentSummaryRow();

				this.Tables[NOTESATTACHMENTSUMMARYTABLE].Rows[0][ATTACHMENTSCOUNTFIELD] = value;
			}
		}
		#endregion
		#region Methods
		private void AddBreadCrumbTable()
		{
			DataTable objBreadcrumbTable = this.Tables.Add( BREADCRUMBTABLE );
			objBreadcrumbTable.Columns.Add(	BREADCRUMBFIELD, typeof( string ) );
			
			Assembly assembly=Assembly.GetCallingAssembly();
			objBreadcrumbTable.ExtendedProperties.Add("FullName", assembly.FullName);
			objBreadcrumbTable.ExtendedProperties.Add("CodeBase", assembly.CodeBase);
			objBreadcrumbTable.ExtendedProperties.Add("DataType", this.GetType().ToString());
		}

		private void AddUserConfirmedTable()
		{
			// set up the table
			DataTable userConfirmedTable = this.Tables.Add("UserConfirmed");
			userConfirmedTable.Columns.Add("ID", typeof(string));
			userConfirmedTable.Columns.Add("UserConfirmed", typeof(bool));
        
			// add the required default row
			DataRow userConfirmedRow = userConfirmedTable.NewRow();
			userConfirmedRow["ID"] = "DEFAULT";
			userConfirmedRow["UserConfirmed"] = false;
			userConfirmedTable.Rows.Add(userConfirmedRow);
       	}

		private void AddWarningsTable()
		{
			DataTable warningsTable = new DataTable("WarningUserDisplayMessages");
			warningsTable.Columns.Add("Message");
			this.Tables.Add(warningsTable);
		}

		/// <summary>
		/// Adds a warning message that will be displayed to the user when the workpaper is re-displayed
		/// </summary>
		/// <param name="warningMessage"></param>
		public void AddWarningUserDisplayMessage(string warningMessage)
		{
			DataTable warningsTable = this.WarningsTable;
			DataRow warningRow = warningsTable.NewRow();
			warningRow["Message"] = warningMessage;
			warningsTable.Rows.Add(warningRow);
		}

		/// <summary>
		/// Gets a list of the user warnings that have been added to the dataset
		/// </summary>
		/// <returns></returns>
		public ArrayList GetWarningUserDisplayMessages()
		{
			ArrayList warningsList = new ArrayList();
			DataTable warningsTable = this.WarningsTable;

			foreach(DataRow row in warningsTable.Rows)
				warningsList.Add(row["Message"]);

			return warningsList;
		}

		private void AddNotesAndAttachmentSummary()
		{
			DataTable notesAttachmentsSummaryTable = new DataTable(NOTESATTACHMENTSUMMARYTABLE);
			notesAttachmentsSummaryTable.Columns.Add(NOTESCOUNTFIELD, typeof(int));
			notesAttachmentsSummaryTable.Columns.Add(ATTACHMENTSCOUNTFIELD, typeof(int));
			this.Tables.Add(notesAttachmentsSummaryTable);

			this.AddInitialNotesAndAttachmentSummaryRow();
		}

		private void AddInitialNotesAndAttachmentSummaryRow()
		{
			DataRow summaryRow = this.Tables[NOTESATTACHMENTSUMMARYTABLE].NewRow();
			summaryRow[NOTESCOUNTFIELD] = 0;
			summaryRow[ATTACHMENTSCOUNTFIELD] = 0;
			this.Tables[NOTESATTACHMENTSUMMARYTABLE].Rows.Add(summaryRow);
		}


		private void AddNotesDetailsTable()
		{
			DataTable notesDetailsTable = new DataTable("NOTESDETAILSTABLE");
			notesDetailsTable.Columns.Add("NOTESNAMEFIELD");
			notesDetailsTable.Columns.Add("NOTESWORKPAPERIDFIELD", typeof(Guid));
			this.Tables.Add(notesDetailsTable);
		}

		public int GetNotesCount(Guid workpaperID)
		{
			DataView filteredNotesView = new DataView(this.Tables[NOTESDETAILSTABLE]);
			filteredNotesView.RowFilter = NOTESWORKPAPERIDFIELD + " = '" + workpaperID.ToString() + "'";
			return filteredNotesView.Count;
		}
		/// <summary>
		/// Set the user confirmation for the specified confirmation type
		/// </summary>
		/// <param name="confirmType"></param>
		/// <param name="value"></param>
		public void SetUserConfirmed( string confirmType,bool value)
		{
			
			DataRow userConfirmedRow = this.UserConfirmedRow(confirmType);
			userConfirmedRow["UserConfirmed"] = value;
		}		
		/// <summary>
		/// Get the user confirmation for the specified confirmation type
		/// </summary>
		/// <param name="confirmType"></param>
		public bool GetUserConfirmed( string confirmType )
		{
			DataRow userConfirmedRow = this.UserConfirmedRow(confirmType);
			return (bool) userConfirmedRow["UserConfirmed"];
		}
		
		/// <summary>
		/// Check for any user confirmation. Return true if there is one.
		/// </summary>
		/// <returns></returns>
		public bool AnyUserConfirmation()
		{
			// retrieve all rows in the confirmation table
			DataRow [] rows = this.UserConfirmedTable.Select();
			if ( rows.Length > 0 )
			{
				foreach ( DataRow row in rows )
				{
					if ( (bool)row["UserConfirmed"] )
						return true;
				}
			}
			// no user confirmation
			return false;
		}

        /// <summary>
        /// Whether dataset has any amounts that are not empty and not zeros
        /// </summary>
        /// <param name="ds"></param>
        /// <returns></returns>
        public virtual bool HasNonZeroAmounts(DataSet ds)
        {
            foreach (DataTable table in ds.Tables)
            {
                if (CheckNonZeroTable(table.TableName) && HasNonZeroAmounts(table))
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Some pre-defined tables do not contain amounts, and should not be used when determine non-empty tables
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public virtual bool CheckNonZeroTable(string name)
        {
            if (name == BREADCRUMBTABLE || name == NOTESATTACHMENTSUMMARYTABLE || name == NOTESDETAILSTABLE ||
                name == "UserConfirmed" || name == "WarningUserDisplayMessages")
                return false;
            else
                return true;
        }

        /// <summary>
        /// The colums with these pre-defined names do not contain amounts
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public virtual bool CheckNonZeroColumn(string name)
        {
            switch (name.ToLower())
            {
                case "key":
                case "primarykey":
                case "priorperiodkey":
                case "description":
                case "ref":
                case "refurl":
                case "url":
                case "index":
                case "itemtype":
                case "type":
                case "order":
                case "sort":
                case "sortorder":
                case "year":
                case "amount_number_field":
                case "nondedpercent":
                case "referencetb":
                case "accountkey":
                    return false;
                default:
                    return true;
            }
        }

        /// <summary>
        /// Determine whether table has non-empty and non-zero amounts
        /// </summary>
        /// <param name="table"></param>
        /// <returns></returns>
        public virtual bool HasNonZeroAmounts(DataTable table)
        {
            int colNumber = table.Columns.Count;
            decimal decimalZero = new decimal(0);
            CurrencyValue cvZero = new CurrencyValue(0);
            foreach (DataRow row in table.Rows)
            {
                for (int colIndex = 0; colIndex < colNumber; colIndex++)
                {
                    if (!row.IsNull(colIndex) && CheckNonZeroColumn(table.Columns[colIndex].ColumnName))
                    {
                        Type colType = table.Columns[colIndex].DataType;
                        if (colType == System.Type.GetType("System.String"))
                        {
                            decimal result;
                            if (Decimal.TryParse((string)row[colIndex], out result) && result != decimalZero)
                                return true;
                        }
                        else if (colType == System.Type.GetType("System.Decimal"))
                        {
                            if (((Decimal)row[colIndex]) != decimalZero)
                                return true;
                        }
                        else if (colType == System.Type.GetType("System.Double"))
                        {
                            double result;
                            if (Double.TryParse((string)row[colIndex], out result) && (result < 0.0001))
                                return true;
                        }
                        else if (colType.Name == "CurrencyValue")
                        {
                            if ((CurrencyValue)row[colIndex] != 0)
                                return true;
                        }
                    }
                }
            }

            return false;
        }

		#endregion //Methods
	}
}
