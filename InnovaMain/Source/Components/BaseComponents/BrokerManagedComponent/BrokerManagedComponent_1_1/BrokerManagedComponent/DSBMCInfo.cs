using System;
using System.Data;

namespace Oritax.TaxSimp.Calculation
{
	/// <summary>
	/// DSCalculationModule is a DataSet that contains all of the persistent data for a CM instance.
	/// </summary>
	[System.ComponentModel.DesignerCategory("Code")]
	[SerializableAttribute] 
	public class DSBMCInfo : DataSet
	{
		public const string CMLIST_TABLE				= "CMLIST_TABLE";
		public const string CMID_FIELD					= "CID";
		public const string CMNAME_FIELD				= "NAME";
		public const string CMPARENTCSID_FIELD			= "PARENTCSID";
		public const string CMTYPEID_FIELD				= "CTID";
		public const string CMUPDATETOKEN_FIELD			= "UPDATETOKEN";
		public const string CMTYPENAME_FIELD			= "TYPENAME";
		public const string CMCATEGORYNAME_FIELD		= "CATEGORYNAME";
		public const string CMMAJORVERSIONNAME_FIELD	= "MAJORVERSION";
		public const string CMMINORVERSIONNAME_FIELD	= "MINORVERSION";
		public const string CMRELEASENAME_FIELD			= "RELEASE";
		public const string CMPERSISTERASSY_FIELD		= "PASSEMBLY";
		public const string CMPERSISTERCLASS_FIELD		= "PCLASS";

		public DSBMCInfo()
		{
			DataTable table;
			DataColumnCollection columns;

			// Add the table for the instance data
			table = new DataTable(CMLIST_TABLE);
			columns = table.Columns;
			columns.Add(CMID_FIELD, typeof(System.Guid));
			columns.Add(CMNAME_FIELD, typeof(System.String));
			columns.Add(CMTYPEID_FIELD, typeof(System.Guid));
			columns.Add(CMUPDATETOKEN_FIELD, typeof(byte[]));
			columns.Add(CMTYPENAME_FIELD, typeof(System.String));
			columns.Add(CMCATEGORYNAME_FIELD, typeof(System.String));
			columns.Add(CMMAJORVERSIONNAME_FIELD, typeof(System.Int32));
			columns.Add(CMMINORVERSIONNAME_FIELD, typeof(System.Int32));
			columns.Add(CMRELEASENAME_FIELD, typeof(System.Int32));
			columns.Add(CMPERSISTERASSY_FIELD, typeof(System.String));
			columns.Add(CMPERSISTERCLASS_FIELD, typeof(System.String));
			columns.Add(CMPARENTCSID_FIELD, typeof(System.Guid));
			this.Tables.Add(table);
		}

		public DSBMCInfo(Guid cmGuid,String name,Guid cmType, byte [] cmUpdateToken)
			: this()
		{
			object[] rowData={cmGuid,name,cmType,cmUpdateToken};
			DataRow newRow=Tables[CMLIST_TABLE].Rows.Add(rowData);
		}
	}
}
