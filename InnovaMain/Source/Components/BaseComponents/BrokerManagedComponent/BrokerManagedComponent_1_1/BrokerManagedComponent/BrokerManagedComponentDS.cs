using System;
using System.Data;
using System.Reflection;
using Oritax.TaxSimp.Utilities;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace Oritax.TaxSimp.Calculation
{
	/// <summary>
	/// The primary DataSet for BrokerManagedComponent.  It contains all of the persistent data
	/// for BrokerManagedComponent.
	/// </summary>
	/// 
	[Serializable]
	public class BrokerManagedComponentDS : PrimaryDS
	{
		public const string ACL_TABLE					= "ACLENTRY";
		public const string ACLID_FIELD					= "ID";
		public const string PARTYCID_FIELD				= "PARTYCID";
		public const string BMCCID_FIELD				= "BMCCID";
		public const string PRIVILEGETYPE_FIELD			= "PRIVILEGETYPE";
		public const string PRIVILEGEVALUE_FIELD		= "PRIVILEGEVALUE";

		public const string CINSTANCES_TABLE			= "CINSTANCES";
		public const string CID_FIELD					= "CID";
		public const string CTID_FIELD					= "CTID";
		public const string NAME_FIELD					= "NAME";
        public const string ECLIPSECLIENTID_FIELD       = "ECLIPSECLIENTID";
        public const string STATUS_FIELD                = "STATUS";
        public const string OTHERID_FIELD               = "OTHERID";
        public const string UPDATETOKEN_FIELD			= "UPDATETOKEN";

		public const string CMTYPES_TABLE				= "CMTYPES";
		public const string TYPEID_FIELD				= "CMTYPEID";
		public const string TYPENAME_FIELD				= "TYPENAME";
		public const string CATEGORYNAME_FIELD			= "CATEGORYNAME";

		public const string NOTES_PRIMARYKEY_FIELD		= "ID";
		public const string NOTES_PREVPERIODKEY_FIELD	= "PREVPERIODID";
		public const string NOTEBMCID_FIELD				= "BMCCID";

		public const string ATTACHMENT_TABLE			= "ATTACHMENT";
		public const string ATTACHMENTID_FIELD			= "ID";
		public const string ATTACHEDBMCID_FIELD			= "ATTACHEDBMCID";
		public const string ATTACHMENTBMCID_FIELD		= "BMCCID";
		public const string ATTACHMENTDATEIMPORTED_FIELD= "DATEIMPORTED";
		public const string ATTACHMENTIMPORTEDBY_FIELD  = "IMPORTEDBY";
		public const string ATTACHMENTORIGINPATH_FIELD  = "ORIGINPATH";
		public const string ATTACHMENTDESCRIPTION_FIELD = "DESCRIPTION";
		public const string ATTACHMENTLINKED_FIELD		= "LINKED";
        public const string ATTACHMENTTYPE_FIELD        = "ATTACHMENTTYPE";


		public const string VERSION_HISTORY_TABLE		= "VERSIONHISTORY";
		public const string VHID_FIELD					= "ID";
		public const string VHBMCID_FIELD				= "BMCCID";
		public const string VHTYPE_FIELD				= "TYPE";
		public const string VHVERSION_FIELD				= "VERSION";

        public BrokerManagedComponentDS(SerializationInfo si, StreamingContext context)
			:base(si,context)
        {
        }

        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo si, StreamingContext context)
        {
            base.GetObjectData(si, context);
        }



		public BrokerManagedComponentDS()
			: base()
		{
			// CINSTANCES TABLE=======================================================================
			PDSDataTable cinstancesTable = DefineRootTable(			
				CINSTANCES_TABLE,
				new FieldDefinition[]{
					new FieldDefinition(CID_FIELD,typeof(System.Guid),false),
					new FieldDefinition(CTID_FIELD,typeof(System.Guid),false),
   					new FieldDefinition(NAME_FIELD,typeof(System.String)),
                    new FieldDefinition(UPDATETOKEN_FIELD,typeof(byte[])),
                    new FieldDefinition(ECLIPSECLIENTID_FIELD,typeof(System.String)),
                    new FieldDefinition(STATUS_FIELD,typeof(System.Int32)),
                    new FieldDefinition(OTHERID_FIELD,typeof(System.String))
                },
				CID_FIELD,
				PersistenceTypeOption.READWRITE
			);

            cinstancesTable.Indexes.Add(new DatabaseIndex("IX_CINSTANCES", false, false, true, new DataColumn[] { cinstancesTable.Columns[CTID_FIELD] }));

            cinstancesTable.Statistics.Add(new DatabaseStatistic("statistic_cinstances_cid_ctid", new DataColumn[] { cinstancesTable.Columns[CID_FIELD], cinstancesTable.Columns[CTID_FIELD] }));
            cinstancesTable.Statistics.Add(new DatabaseStatistic("statistic_cinstances_ctid_cid", new DataColumn[] { cinstancesTable.Columns[CTID_FIELD], cinstancesTable.Columns[CID_FIELD] }));

            BrokerManagedComponentDS.AddVersionStamp(cinstancesTable, typeof(BrokerManagedComponentDS));

			// ACL TABLE==========================================================================
			PDSDataTable aclEntryTable = DefineChildTable(
				ACL_TABLE,
				new FieldDefinition[]{
					new FieldDefinition(ACLID_FIELD,typeof(System.Guid),false),
					new FieldDefinition(BMCCID_FIELD,typeof(System.Guid),false),
					new FieldDefinition(PARTYCID_FIELD,typeof(System.Guid),false),
					new FieldDefinition(PRIVILEGETYPE_FIELD,typeof(System.Int32)),
					new FieldDefinition(PRIVILEGEVALUE_FIELD,typeof(System.Int32)),
				},
				ACLID_FIELD,														// Primary Key
				PersistenceTypeOption.READWRITE,
				CINSTANCES_TABLE,
				CID_FIELD,
				BMCCID_FIELD
			);

            aclEntryTable.Indexes.Add(new DatabaseIndex("IX_ACLENTRY_1", false, false, true, new DataColumn[] { aclEntryTable.Columns[BMCCID_FIELD] }));
            BrokerManagedComponentDS.AddVersionStamp(aclEntryTable, typeof(BrokerManagedComponentDS));


			// ATTACHMENT TABLE===================================================================
			DefineChildTable(
				ATTACHMENT_TABLE,
				new FieldDefinition[]{
										 new FieldDefinition(ATTACHMENTID_FIELD,typeof(System.Guid),false),
										 new FieldDefinition(ATTACHMENTBMCID_FIELD,typeof(System.Guid),false),
										 new FieldDefinition(ATTACHEDBMCID_FIELD,typeof(System.Guid),false),
										 new FieldDefinition(ATTACHMENTDATEIMPORTED_FIELD,typeof(System.DateTime)),
										 new FieldDefinition(ATTACHMENTIMPORTEDBY_FIELD,typeof(System.String)),
										 new FieldDefinition(ATTACHMENTORIGINPATH_FIELD,typeof(System.String)),
										 new FieldDefinition(ATTACHMENTDESCRIPTION_FIELD,typeof(System.String)),
										 new FieldDefinition(ATTACHMENTLINKED_FIELD,typeof(System.Boolean),false),
                                         new FieldDefinition(ATTACHMENTTYPE_FIELD,typeof(System.Int32),false),
				},
				ATTACHMENTID_FIELD,														// Primary Key
				PersistenceTypeOption.READWRITE,
				CINSTANCES_TABLE,
				CID_FIELD,
				ATTACHMENTBMCID_FIELD
				);
				BrokerManagedComponentDS.AddVersionStamp(this.Tables[ATTACHMENT_TABLE],typeof(BrokerManagedComponentDS));

                PDSDataTable notesTable = DefineChildTable(
				NoteListDS.NOTES_TABLE,
				new FieldDefinition[]{
										 new FieldDefinition(NOTES_PRIMARYKEY_FIELD ,typeof(System.Guid),false),
										 new FieldDefinition(NOTES_PREVPERIODKEY_FIELD ,typeof(System.Guid),false),
										 new FieldDefinition(NOTEBMCID_FIELD,typeof(System.Guid),false),
										 new FieldDefinition(NoteListDS.NOTES_WORKPAPERID_FIELD ,typeof(System.Guid),false),
										 new FieldDefinition(NoteListDS.NOTES_DESCRIPTION_FIELD ,typeof(System.String),false,2000),
										 new FieldDefinition(NoteListDS.NOTES_LASTUSERUPDATED_FIELD ,typeof(System.String),false),
										 new FieldDefinition(NoteListDS.NOTES_LASTDATEUPDATED_FIELD ,typeof(System.DateTime),false),
										 new FieldDefinition(NoteListDS.NOTES_ROLLFORWARD_FIELD ,typeof(System.Boolean),false),
										 new FieldDefinition(NoteListDS.NOTES_CREATEDBY_FIELD ,typeof(System.String),false),
										 new FieldDefinition(NoteListDS.NOTES_CREATIONDATE_FIELD ,typeof(System.DateTime),false),
										
				},
				NOTES_PRIMARYKEY_FIELD,
				PersistenceTypeOption.READWRITE,
				CINSTANCES_TABLE,
				CID_FIELD,
				ATTACHMENTBMCID_FIELD
				);

                notesTable.Indexes.Add(new DatabaseIndex("IX_NOTES_TABLE_1", false, false, true, new DataColumn[] { notesTable.Columns[NOTEBMCID_FIELD] }));
                BrokerManagedComponentDS.AddVersionStamp(notesTable, typeof(BrokerManagedComponentDS));

			// VERSION HISTORY TABLE==============================================================
			PDSDataTable versionHistoryTable = DefineChildTable(
				VERSION_HISTORY_TABLE,
				new FieldDefinition[]{
										 new FieldDefinition(VHID_FIELD,typeof(System.Guid),false),
										 new FieldDefinition(VHBMCID_FIELD,typeof(System.Guid),false),
										 new FieldDefinition(VHTYPE_FIELD,typeof(System.String),false,1024),
										 new FieldDefinition(VHVERSION_FIELD,typeof(System.String),false,1024),
			},
				VHID_FIELD,														// Primary Key
				PersistenceTypeOption.READWRITE,
				CINSTANCES_TABLE,
				CID_FIELD,
				BMCCID_FIELD
				);

            versionHistoryTable.Indexes.Add(new DatabaseIndex("IX_VERSIONHISTORY_1", false, false, true, new DataColumn[] { versionHistoryTable.Columns[VHBMCID_FIELD] }));

            BrokerManagedComponentDS.AddVersionStamp(versionHistoryTable, typeof(BrokerManagedComponentDS));
		}

		public static void AddVersionStamp(DataTable table, Type type)
		{
			Assembly assembly=Assembly.GetCallingAssembly();
			if(table.ExtendedProperties.Contains("FullName"))
				table.ExtendedProperties["FullName"] = assembly.FullName;
			else
				table.ExtendedProperties.Add("FullName", assembly.FullName);

			if(table.ExtendedProperties.Contains("CodeBase"))
				table.ExtendedProperties["CodeBase"] = assembly.CodeBase;
			else
				table.ExtendedProperties.Add("CodeBase", assembly.CodeBase);

			if(table.ExtendedProperties.Contains("DataType"))
				table.ExtendedProperties["DataType"] = type.ToString();
			else
				table.ExtendedProperties.Add("DataType", type.ToString());

			Type aitype = typeof(AssemblyInformationalVersionAttribute);

			object[] attributeArray = assembly.GetCustomAttributes(aitype,true);
			if(attributeArray.Length == 0)
			{
				throw new Exception("Assembly does not have an InformationalVersion");
			}
			AssemblyInformationalVersionAttribute versionAttribute;
			versionAttribute = (AssemblyInformationalVersionAttribute)attributeArray[0];
			table.ExtendedProperties.Add("InformationalVersion", versionAttribute.InformationalVersion);
		}

		public static string GetFullName(DataTable table)
		{
			return (string)table.ExtendedProperties["FullName"];
		}

		public static string GetCodeBase(DataTable table)
		{
			return (string)table.ExtendedProperties["CodeBase"];
		}

		public static string GetDataType(DataTable table)
		{
			return (string)table.ExtendedProperties["DataType"];
		}

		public static string GetInformationalVersion(DataTable table)
		{
			return (string)table.ExtendedProperties["InformationalVersion"];
		}

		public static string GetDataSetVersion(DataSet data)
		{
			if(data.Tables["CINSTANCES"]!= null)
			{
				return GetDataSetVersion(data,"CINSTANCES").ToString();
			}
			else 
				return GetDataSetVersion(data, CINSTANCES_TABLE).ToString();
		}

		protected static Version GetDataSetVersion(DataSet data, string tableName)
		{
			string fn = GetInformationalVersion(data.Tables[tableName]);
			if( fn != null)
			{
				return new Version(fn);
			}
			fn = GetFullName(data.Tables[tableName]);
			AssemblyFullName afn = new AssemblyFullName(fn);
			return afn.Version;
		}

		public Guid CIID
		{
			set
			{
                if (this.Tables[BrokerManagedComponentDS.CINSTANCES_TABLE] != null)
                {
                    if (this.Tables[BrokerManagedComponentDS.CINSTANCES_TABLE].Rows.Count > 0)
                    {
                        DataRow bmcRow = this.Tables[BrokerManagedComponentDS.CINSTANCES_TABLE].Rows[0];
                        bmcRow[BrokerManagedComponentDS.CID_FIELD] = value;
                    }
                    else
                    {
                        //DataRow bmcRow=this.Tables[BrokerManagedComponentDS.CINSTANCES_TABLE].NewRow();
                        //bmcRow[BrokerManagedComponentDS.CID_FIELD]=value;
                        //this.Tables[BrokerManagedComponentDS.CINSTANCES_TABLE].Rows.Add(bmcRow);
                    }
                }
			}
			get
			{
				if(this.Tables[BrokerManagedComponentDS.CINSTANCES_TABLE].Rows.Count>0)
				{
					DataRow bmcRow=this.Tables[BrokerManagedComponentDS.CINSTANCES_TABLE].Rows[0];
					return (Guid)bmcRow[BrokerManagedComponentDS.CID_FIELD];
				}
				else
					return Guid.Empty;
			}
		}

		public Guid CTID
		{
			set
			{
                if (this.Tables[BrokerManagedComponentDS.CINSTANCES_TABLE] != null)
                {

                    if (this.Tables[BrokerManagedComponentDS.CINSTANCES_TABLE].Rows.Count > 0)
                    {
                        DataRow bmcRow = this.Tables[BrokerManagedComponentDS.CINSTANCES_TABLE].Rows[0];
                        bmcRow[BrokerManagedComponentDS.CTID_FIELD] = value;
                    }
                    else
                    {
                        //DataRow bmcRow=this.Tables[BrokerManagedComponentDS.CINSTANCES_TABLE].NewRow();
                        //bmcRow[BrokerManagedComponentDS.CTID_FIELD]=value;
                        //this.Tables[BrokerManagedComponentDS.CINSTANCES_TABLE].Rows.Add(bmcRow);
                    }
                }
			}
			get
			{
				if(this.Tables[BrokerManagedComponentDS.CINSTANCES_TABLE].Rows.Count>0)
				{
					DataRow bmcRow=this.Tables[BrokerManagedComponentDS.CINSTANCES_TABLE].Rows[0];
					return (Guid)bmcRow[BrokerManagedComponentDS.CTID_FIELD];
				}
				else
					return Guid.Empty;
			}
		}

		public string Name
		{
			set
			{
                if (this.Tables[BrokerManagedComponentDS.CINSTANCES_TABLE] != null)
                {

				    if(this.Tables[BrokerManagedComponentDS.CINSTANCES_TABLE].Rows.Count>0)
				    {
					    DataRow bmcRow=this.Tables[BrokerManagedComponentDS.CINSTANCES_TABLE].Rows[0];
					    bmcRow[BrokerManagedComponentDS.NAME_FIELD]=value;
				    }
				    else
				    {
                        //DataRow bmcRow=this.Tables[BrokerManagedComponentDS.CINSTANCES_TABLE].NewRow();
                        //bmcRow[BrokerManagedComponentDS.NAME_FIELD]=value;
                        //this.Tables[BrokerManagedComponentDS.CINSTANCES_TABLE].Rows.Add(bmcRow);
				    }
                }
			}
			get
			{
				if(this.Tables[BrokerManagedComponentDS.CINSTANCES_TABLE].Rows.Count>0)
				{
					DataRow bmcRow=this.Tables[BrokerManagedComponentDS.CINSTANCES_TABLE].Rows[0];
					return bmcRow[BrokerManagedComponentDS.NAME_FIELD].ToString();
				}
				else
					return null;
			}
		}

		public byte [] UpdateToken
		{
			set
			{
                if (this.Tables[BrokerManagedComponentDS.CINSTANCES_TABLE] != null)
                {
                    if (this.Tables[BrokerManagedComponentDS.CINSTANCES_TABLE].Rows.Count > 0)
                    {
                        DataRow bmcRow = this.Tables[BrokerManagedComponentDS.CINSTANCES_TABLE].Rows[0];
                        bmcRow[BrokerManagedComponentDS.UPDATETOKEN_FIELD] = value;
                    }
                    else
                    {
                        //DataRow bmcRow=this.Tables[BrokerManagedComponentDS.CINSTANCES_TABLE].NewRow();
                        //bmcRow[BrokerManagedComponentDS.UPDATETOKEN_FIELD]=value;
                        //this.Tables[BrokerManagedComponentDS.CINSTANCES_TABLE].Rows.Add(bmcRow);
                    }
                }
			}
			get
			{
				if(this.Tables[BrokerManagedComponentDS.CINSTANCES_TABLE].Rows.Count>0)
				{
					DataRow bmcRow=this.Tables[BrokerManagedComponentDS.CINSTANCES_TABLE].Rows[0];
					return (byte [])bmcRow[BrokerManagedComponentDS.UPDATETOKEN_FIELD];
				}
				else
					return null;
			}
		}
	}
}
