using System;

using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.CalculationInterface;

namespace Oritax.TaxSimp.Calculation
{
	/// <summary>
	/// Allows a BMC to store information from the DeliverData phase to the MigrationComplete phase
	/// </summary>
	public class MigrationDataMemento : SerializableHashtable, IMigrationDataMemento
	{
		public MigrationDataMemento()
		{
		}

		public void Add(string key, object value)
		{
			base.Add(key, value);
		}

		public void Remove(string key)
		{
			base.Remove(key);
		}

		public object this[string key]
		{
			get
			{
				return base[key];
			}
		}
	}
}
