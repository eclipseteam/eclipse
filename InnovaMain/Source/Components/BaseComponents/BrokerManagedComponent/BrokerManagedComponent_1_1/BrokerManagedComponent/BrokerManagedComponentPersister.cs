using System;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Diagnostics;

using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.Calculation
{
	/// <summary>
	/// This persister provides features that are peculiar to BrokerManagedComponent instances.
	/// </summary>
	public class BrokerManagedComponentPersister : BrokerManagedPersister
	{
		public BrokerManagedComponentPersister(SqlConnection connection, SqlTransaction transaction)
			: base(connection, transaction)
		{
		}

		protected override string GenerateEstablishTableColumnText(PrimaryDS.PDSDataTable dataTable,DataColumn dataColumn)
		{
			if(dataTable.TableName==NoteListDS.NOTES_TABLE && dataColumn.ColumnName=="DESCRIPTION")
			{
				return "[DESCRIPTION] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL";
			}
			else
				return base.GenerateEstablishTableColumnText(dataTable,dataColumn);

		}

		public override void EstablishDatabase()
		{
			Guid typeID=new Guid(BrokerManagedComponentInstall.ASSEMBLY_ID);
			PrimaryDS primaryDataSet=new BrokerManagedComponentDS();

			EstablishDatabase(typeID,primaryDataSet);

			UpdateDataModel();
		}

		public void UpdateDataModel()
		{
            SqlCommand command = DBCommandFactory.Instance.NewSqlCommand();
            command.Connection = this.Connection;

			command.Transaction = this.Transaction;
			command.CommandText = "if not exists(select * from syscolumns where id=object_id('ATTACHMENT') and name ='LINKED') ALTER TABLE ATTACHMENT ADD LINKED bit DEFAULT 0 NOT NULL" ;
			command.ExecuteNonQuery();
		}
		
		public override void RemoveOldDatabase()
		{
		
		}
	}
}
