using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace Oritax.TaxSimp.Calculation
{
    /// <summary>
    /// Represents database statistics
    /// </summary>
    public class DatabaseStatistic
    {
        private string name;
        private DataColumn[] statisticColumns;

        /// <summary>
        /// Constructs a new database statistic
        /// </summary>
        /// <param name="name">The name of the statistic</param>
        /// <param name="statisticColumns">The columnns included in the statistic</param>
        public DatabaseStatistic(string name, DataColumn[] statisticColumns)
        {
            this.name = name;
            this.statisticColumns = statisticColumns;
        }

        /// <summary>
        /// The name of the statistic
        /// </summary>
        public string Name
        {
            get 
            {
                return name; 
            }
        }

        /// <summary>
        /// The columns included in the statistic
        /// </summary>
        public DataColumn[] StatisticColumns
        {
            get 
            { 
                return statisticColumns; 
            }
        }
    }
}
