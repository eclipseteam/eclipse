using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Reflection;
using System.Transactions;

using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.Persistence;
using Oritax.TaxSimp.Utilities;
using System.Text;

namespace Oritax.TaxSimp.Calculation
{
	/// <summary>
	/// Summary description for BrokerManagedPersister.
	/// </summary>
    public class SQLRMBrokerManagedPersister : Persister, IEnlistmentNotification
	{
		#region INTERNAL CLASSES

		public class UnimplementedFieldTypeException : Exception
		{
			public UnimplementedFieldTypeException(string typeFullName)
				: base("Attempt to persist an unimplemented System type: "+typeFullName){}
		}

		public class InvalidRelationshipException : Exception
		{
			public InvalidRelationshipException(string table1,string table2)
				: base("Invalid relationship between tables: "+table1+" and "+table2){}
		}
		
		public class MissingParentAdapterException : Exception
		{
			public MissingParentAdapterException(string parentTableName,string tableName)
				: base("Attempt was made to add a SQL Adapter with a parent specified that does not exist in the collection. Parent Table Name: "
				+parentTableName+", Table Name: "+tableName){}
		}

		public class MissingSqlDataAdapterException : Exception
		{
			public MissingSqlDataAdapterException(string tableName)
				: base("No SQL Data adapter was found for the table: "+tableName){}
		}

		public class DuplicateSqlDataAdapterException : Exception
		{
			public DuplicateSqlDataAdapterException(string tableName)
				: base("Attempt to add a duplicate SQL Data Adapter for table: "+tableName){}
		}
		
		public class MissingSQLStringException : Exception
		{
			public MissingSQLStringException(string tableName)
				: base("Attempt to access a missing SQL string for table: "+tableName){}
		}

		public class DuplicateSQLStringException : Exception
		{
			public DuplicateSQLStringException(string tableName)
				: base("Attempt to add a duplicate SQL table string set for table: "+tableName){}
		}

		public class DuplicateSQLBMCTypeAdapterException : Exception
		{
			public DuplicateSQLBMCTypeAdapterException(string typeName)
				: base("Attempt to add a duplicate SQL BMC Type Adapter for type: "+typeName){}
		}
        		
		public struct SqlDbTypeDescription
		{
			public SqlDbType DbType;
			public int Size;
			public string SqlTypeString;

			public SqlDbTypeDescription(SqlDbType dbType,int size,string sqlTypeString)
			{
				DbType=dbType;
				Size=size;
				SqlTypeString=sqlTypeString;
			}
		}

		public struct SqlNamedParameterValue
		{
			public string Name;
			public object Value;

			public SqlNamedParameterValue(string name,object parameterValue)
			{
				Name=name;
				Value=parameterValue;
			}
		}

		public class SQLBMCTypeAdapter : DictionaryBase
		{
			public class AdapterEntry
			{
				public SqlDataAdapter Adapter;
				public PrimaryDS.TableNames Children;
				public PrimaryDS.TableNames Parents;
				public string TableName;
				public string EstablishString;

				public AdapterEntry(string tableName,SqlDataAdapter sqlDataAdapter,PrimaryDS.TableNames childTables,PrimaryDS.TableNames parentTables,string establishString)
				{
					Adapter=sqlDataAdapter;
					TableName=tableName;
					Children=childTables;
					Parents=parentTables;
					EstablishString=establishString;
				}
			}

			private ArrayList rootTableNames=new ArrayList();

			public ArrayList RootTableNames
			{
				get{return rootTableNames;}
				set{rootTableNames=value;}
			}

			public AdapterEntry this[string tableName]
			{
				get
				{
					object sqlAdapterEntry=this.Dictionary[tableName];
					if(null==sqlAdapterEntry)
						throw new MissingSqlDataAdapterException(tableName);
					else
						return (AdapterEntry)sqlAdapterEntry;
				}
			}

			public void Add(SQLTableStrings sqlTableStrings,SqlDataAdapter sqlDataAdapter)
			{
				//				if(null!=parentTableName)
				//				{
				//					object parentEntry=this.Dictionary[parentTableName];
				//					if(null==parentEntry)
				//					{
				//						throw new MissingParentAdapterException(parentTableName,tableName);
				//					}
				//					((AdapterEntry)parentEntry).Children.Add(tableName);
				//				}

				AdapterEntry adapterEntry=new AdapterEntry(sqlTableStrings.TableName,
					sqlDataAdapter,sqlTableStrings.ChildTables,sqlTableStrings.ParentTables,sqlTableStrings.EstablishString);
				this.Dictionary.Add(sqlTableStrings.TableName,adapterEntry);

				if(sqlTableStrings.RootTable)
					rootTableNames.Add(sqlTableStrings.TableName);
			}

			public SqlConnection Connection
			{
				set
				{
					foreach(DictionaryEntry dictionaryEntry in this.Dictionary)
					{
						AdapterEntry adapterEntry=dictionaryEntry.Value as AdapterEntry;
						adapterEntry.Adapter.InsertCommand.Connection=value;
						adapterEntry.Adapter.UpdateCommand.Connection=value;
						adapterEntry.Adapter.SelectCommand.Connection=value;
						adapterEntry.Adapter.DeleteCommand.Connection=value;
					}
				}
			}

			public SqlTransaction Transaction
			{
				set
				{
					foreach(DictionaryEntry dictionaryEntry in this.Dictionary)
					{
						AdapterEntry adapterEntry=dictionaryEntry.Value as AdapterEntry;
						adapterEntry.Adapter.InsertCommand.Transaction=value;
						adapterEntry.Adapter.UpdateCommand.Transaction=value;
						adapterEntry.Adapter.SelectCommand.Transaction=value;
						adapterEntry.Adapter.DeleteCommand.Transaction=value;
					}
				}
			}

            public void PrepareCommands()
            {
                foreach (AdapterEntry adapterEntry in this.Dictionary.Values)
                {
                    SqlDataAdapter adapter = adapterEntry.Adapter;

                    adapter.SelectCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;
                    adapter.UpdateCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;
                    adapter.DeleteCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;
                    adapter.InsertCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;

                    adapter.UpdateBatchSize = 500;
                }
            }
		}

		public class SQLBMCTypeAdapters : DictionaryBase
		{
			public SQLBMCTypeAdapter this[Guid typeID]
			{
				get
				{
					return this.Dictionary[typeID] as SQLBMCTypeAdapter;
				}
			}

			public void Add(Guid typeID,SqlDataAdapter sQLDataAdapter)
			{
				if(this.Dictionary.Contains(typeID))
					throw new DuplicateSQLBMCTypeAdapterException(typeID.ToString());
				else
					this.Dictionary.Add(typeID,sQLDataAdapter);
			}
		}

		public class SQLTableStrings
		{
			public string TableName;
			//			public string ParentTableName;
			public bool RootTable;
			public PrimaryDS.TableNames ChildTables;
			public PrimaryDS.TableNames ParentTables;

			public string InsertString;
			public string UpdateString;
			public string SelectString;
			public string DeleteString;
			public string EstablishString;

			public SqlParameter[] InsertParameters;
			public SqlParameter[] UpdateParameters;
			public SqlParameter[] SelectParameters;
			public SqlParameter[] DeleteParameters;
			public SqlParameter[] EstablishParameters;

			public SQLTableStrings(	string tableName,bool rootTable,PrimaryDS.TableNames childTables,PrimaryDS.TableNames parentTables,
				string insertString,SqlParameter[] insertParameters,
				string updateString,SqlParameter[] updateParameters,
				string selectString,SqlParameter[] selectParameters,
				string deleteString,SqlParameter[] deleteParameters,
				string establishString,SqlParameter[] establishParameters)
			{
				TableName=tableName;
				RootTable=rootTable;
				ChildTables=childTables;
				ParentTables=parentTables;

				InsertString=insertString;
				InsertParameters=insertParameters;
				UpdateString=updateString;
				UpdateParameters=updateParameters;
				SelectString=selectString;
				SelectParameters=selectParameters;
				DeleteString=deleteString;
				DeleteParameters=deleteParameters;
				EstablishString=establishString;
				EstablishParameters=establishParameters;
			}
		}

		public class SQLBMCTypeStrings : DictionaryBase
		{
			public SQLTableStrings this[string tableName]
			{
				get
				{
					object sQLString=this.Dictionary[tableName];
					if(null==sQLString)
						throw new MissingSQLStringException(tableName);
					else
						return (SQLTableStrings)sQLString;
				}
			}

			public void Add(string tableName,SQLTableStrings sQLTableStrings)
			{
				if(this.Dictionary.Contains(tableName))
					throw new DuplicateSQLStringException(tableName);
				else
					this.Dictionary.Add(tableName,sQLTableStrings);
			}
		}
		#endregion

		#region FIELD VARIABLES
		private SQLBMCTypeAdapters sQLBMCTypeAdapters=new SQLBMCTypeAdapters();
        private IBrokerManagedComponent _IBMC;
        private Guid _CSID;
        private DataSet _PersistedPrimaryDS;
        //private SqlDataAdapter _SqlDataAdapter;
        //private DataTable _ExistingTable;
		#endregion

        //public SQLRMBrokerManagedPersister(SqlConnection connection, SqlTransaction transaction)
        //    : base(connection, transaction)
        //{
        //}


        public SQLRMBrokerManagedPersister(SqlConnection connection, SqlTransaction transaction, IBrokerManagedComponent iBMC, DataSet persistedPrimaryDS)
            : this(connection, transaction, iBMC, persistedPrimaryDS, Guid.Empty)
        {
            this._IBMC = iBMC;
            this._PersistedPrimaryDS = persistedPrimaryDS;

        }

        public SQLRMBrokerManagedPersister(SqlConnection connection, SqlTransaction transaction, IBrokerManagedComponent iBMC, DataSet persistedPrimaryDS, Guid csid)
            : base(connection, transaction)
        {
            this._IBMC = iBMC;
            this._PersistedPrimaryDS = persistedPrimaryDS;
            this._CSID = csid;

        }


        //public SQLRMBrokerManagedPersister(SqlConnection connection, SqlTransaction transaction, SqlDataAdapter da, DataTable dt)
        //    : base(connection, transaction)
        //{
        //    this._SqlDataAdapter = da;
        //    this._ExistingTable = dt;
            
        //}

		public override IBrokerManagedComponent Get(Guid cID, ref DataSet persistedDS)
		{
            IBrokerManagedComponent iBMC=null;

			// The tree of objects constituting the BMC instance is instantiated from their respective tables
			// beginning with the root table.  Paths from the root table to the branches are defined by relations and extended properties in
			// the primary dataset.  The root tables are idenfified by the "root_table" property in their extended properties set.
			// The structure of the tree is defined by DataRelations in the primary data set (PDS), that relate foreign keys in
			// branch tables to primary keys in the table nearer to the root.
			// This table structure is "discovered" by processing the PDS as part of the construction of the SQLBMCTypeAdapeter object.

			// Get the CINSTANCES entry for the CID of the instance
			// This enables the type of the BMC instance to be determined
			Guid bMCInfoTypeID=new Guid("898E7243-19EC-48ae-9D8D-CC6673490BDB");

			CInstanceInfoDS cInstanceInfoDS=new CInstanceInfoDS(); 
			SQLBMCTypeAdapter sQLBMCTypeAdapter=GetSqlDataAdapter(bMCInfoTypeID,cInstanceInfoDS);
			sQLBMCTypeAdapter.Connection=this.Connection;
			sQLBMCTypeAdapter.Transaction=this.Transaction;

			// Read the CINSTANCES table setting the primary key parameters to the CID
			SqlDataAdapter sqlDataAdapter=sQLBMCTypeAdapter[BrokerManagedComponentDS.CINSTANCES_TABLE].Adapter;
			foreach( SqlParameter sqlParameter in sqlDataAdapter.SelectCommand.Parameters)
			{
				sqlParameter.Value=cID;
			}
			sqlDataAdapter.Fill(cInstanceInfoDS.Tables[BrokerManagedComponentDS.CINSTANCES_TABLE]);

			// Read the CMTYPES table by the primary key parameters to the type of the BMC instance
			sqlDataAdapter=sQLBMCTypeAdapter[BrokerManagedComponentDS.CMTYPES_TABLE].Adapter;
			Guid cTID=cInstanceInfoDS.InstanceCTID;

			IComponentManagement componentDictionary=this.broker.GetComponentDictionary();

			IComponentVersion componentVersion=componentDictionary[cTID];
			IComponent component=componentDictionary.GetComponent(componentVersion.ComponentID);
			string implementationAssemblyName=((IAssembly)componentVersion).StrongName;
			string implementationClassName=componentVersion.ImplementationClass;


			// Create an instance of the BMC Type implementation class
			Assembly implementationAssy=System.Reflection.Assembly.Load(implementationAssemblyName);
			Type persisterType=implementationAssy.GetType(implementationClassName);
			iBMC=(IBrokerManagedComponent)Activator.CreateInstance(persisterType);
			iBMC.TypeName=((IAssembly)componentVersion).Name;
			iBMC.CategoryName=component.Category;
			iBMC.CategoryName=component.Category;
			iBMC.TypeID = ((IAssembly)componentVersion).ID; 
			// Create a primary data set for the BMC Type
			DataSet primaryDataSet=iBMC.PrimaryDataSet;
			Hydrate(iBMC, cID, primaryDataSet, ref persistedDS);

			return iBMC;
		}

		private void PopulateTables(DataSet primaryDataSet,SQLBMCTypeAdapter sQLBMCTypeAdapter,Guid cID)
		{
			// Iterate through the root tables, loading the row in each with PK cols equal to the PK parameter values
			primaryDataSet.EnforceConstraints=false;
			foreach(string rootTableName in sQLBMCTypeAdapter.RootTableNames)
			{
				// Recursively iterate through the child tables (and grandchild tables etc.) to read all rows in
				// each child table with all foreign key columns equal to the primary key column of its parent.
				PrimaryDS.PDSDataTable pDSDataTable = primaryDataSet.Tables[rootTableName] as PrimaryDS.PDSDataTable;
				if(pDSDataTable.PersistenceType!=PrimaryDS.PersistenceTypeOption.TRANSIENT)
				{
					if(1==primaryDataSet.Tables[rootTableName].PrimaryKey.Length)
					{
						string primaryKeyName=primaryDataSet.Tables[rootTableName].PrimaryKey[0].ColumnName;
						SqlNamedParameterValue[] sqlNamedParameterValues=new SqlNamedParameterValue[]{new SqlNamedParameterValue("@PK"+primaryKeyName,cID)};
						PopulateTableRecursive(primaryDataSet,sQLBMCTypeAdapter,rootTableName,sqlNamedParameterValues);
					}
				}
			}
			//primaryDataSet.EnforceConstraints=true;
		}

		private void PopulateTableRecursive(DataSet primaryDataSet,SQLBMCTypeAdapter sQLBMCTypeAdapter,string rootTableName,SqlNamedParameterValue[] namedParameterValues)
		{
			// This function populates a table by first obtaining an adapter entry for the root name, then populating
			// the parameters of the select command from the set of parameters provided in the array of parameters.
			// This is done by matching the named parameter supplied with the name of the parameter in the list.
			SQLBMCTypeAdapter.AdapterEntry adapterEntry=sQLBMCTypeAdapter[rootTableName];

			foreach(SqlNamedParameterValue sqlNamedParameterValue in namedParameterValues)
			{
				SqlParameter sqlParameter=adapterEntry.Adapter.SelectCommand.Parameters[sqlNamedParameterValue.Name];
				if(null!=sqlParameter)
				{
					sqlParameter.Value=sqlNamedParameterValue.Value;
				}
			}

			adapterEntry.Adapter.Fill(primaryDataSet.Tables[rootTableName]);

			// Recursively iterate through the parent tables (and grandparent tables etc.) to read all rows in
			// each parent table with all primary key column equal to the foreign key column of its child.
			foreach(string parentTableName in adapterEntry.Parents)
			{
				PrimaryDS.PDSDataTable parentDataTable = primaryDataSet.Tables[parentTableName] as PrimaryDS.PDSDataTable;
				if(parentDataTable.PersistenceType!=PrimaryDS.PersistenceTypeOption.TRANSIENT)
				{
					foreach(DataRow childRow in primaryDataSet.Tables[rootTableName].Rows)
					{
						SqlNamedParameterValue[] sqlNamedParameterValues=GetParentParameterValues(primaryDataSet,rootTableName,parentTableName,childRow);
						PopulateTableRecursive(primaryDataSet,sQLBMCTypeAdapter,parentTableName,sqlNamedParameterValues);
					}
				}
			}


			// Recursively iterate through the child tables (and grandchild tables etc.) to read all rows in
			// each child table with all foreign key columns equal to the primary key columns of its parent.
			foreach(string childTableName in adapterEntry.Children)
			{
				PrimaryDS.PDSDataTable childDataTable = primaryDataSet.Tables[childTableName] as PrimaryDS.PDSDataTable;
				if(childDataTable.PersistenceType!=PrimaryDS.PersistenceTypeOption.TRANSIENT)
				{
					foreach(DataRow parentRow in primaryDataSet.Tables[rootTableName].Rows)
					{
						SqlNamedParameterValue[] sqlNamedParameterValues=GetChildParameterValues(primaryDataSet,rootTableName,childTableName,parentRow);
						PopulateTableRecursive(primaryDataSet,sQLBMCTypeAdapter,childTableName,sqlNamedParameterValues);
					}
				}
			}
		}

        protected virtual SqlNamedParameterValue[] GetChildParameterValues(DataSet primaryDataSet, string rootTableName, string parentTableName, DataRow rootRow)
        {
            Guid ID;

            switch (parentTableName)
            {
                case "CHILDLCM":
                case "OWNERLCM":
                case "CMSCENARIOVIEW":
                case "TYPEINFO":
                    ID = (Guid)rootRow["CLID"];
                    return new SqlNamedParameterValue[] { new SqlNamedParameterValue("@ID", ID) };
                case "SCENARIO":
                    ID = (Guid)rootRow["CLID"];
                    return new SqlNamedParameterValue[] { new SqlNamedParameterValue("@ID", ID) };
                default:
                    DataRelationCollection relationsWithChild = primaryDataSet.Tables[rootTableName].ChildRelations;
                    string childColumnName = "";

                    foreach (DataRelation childRelation in relationsWithChild)
                        if (childRelation.ChildTable.TableName == parentTableName)
                        {
                            if (childRelation.ChildColumns.Length != 1)
                                throw new InvalidRelationshipException(rootTableName, parentTableName);
                            childColumnName = childRelation.ChildColumns[0].ColumnName;
                        }


                    DataColumn[] primaryKeyColumns = primaryDataSet.Tables[rootTableName].PrimaryKey;
                    if (primaryKeyColumns.Length != 1 || !(rootRow[primaryKeyColumns[0].ColumnName] is Guid))
                        throw new InvalidRelationshipException(rootTableName, parentTableName);

                    return new SqlNamedParameterValue[] { new SqlNamedParameterValue("@FK" + childColumnName, (Guid)rootRow[primaryKeyColumns[0].ColumnName]) };

            }
        }

		protected virtual SqlNamedParameterValue[] _GetChildParameterValues(DataSet primaryDataSet,string rootTableName,string childTableName,DataRow rootRow)
		{
			DataRelationCollection relationsWithChild=primaryDataSet.Tables[rootTableName].ChildRelations;
			string childColumnName="";

			foreach(DataRelation childRelation in relationsWithChild)
				if(childRelation.ChildTable.TableName==childTableName)
				{
					if(childRelation.ChildColumns.Length!=1)
						throw new InvalidRelationshipException(rootTableName,childTableName);
					childColumnName=childRelation.ChildColumns[0].ColumnName;
				}


			DataColumn[] primaryKeyColumns=primaryDataSet.Tables[rootTableName].PrimaryKey;
			if(primaryKeyColumns.Length!=1 || !(rootRow[primaryKeyColumns[0].ColumnName] is Guid))
				throw new InvalidRelationshipException(rootTableName,childTableName);

			return new SqlNamedParameterValue[]{new SqlNamedParameterValue("@FK"+childColumnName,(Guid)rootRow[primaryKeyColumns[0].ColumnName])};
		}

		/// <summary>
		/// Overridable.  This virtual function calculates a set of parameter values to be passed to the SQL that is responsible
		/// for implementing the Select statement for the rows to be selected in a parent table.  The default implementation of
		/// this function is to return an array of parameters containing one entry which is the Guid value of the field specified as
		/// the foreign key in a single relation with a parent table.
		/// </summary>
		/// <param name="primaryDataSet"></param>
		/// <param name="rootTableName"></param>
		/// <param name="parentTableName"></param>
		/// <param name="rootRow"></param>
		/// <returns></returns>
		protected virtual SqlNamedParameterValue[] GetParentParameterValues(DataSet primaryDataSet,string rootTableName,string parentTableName,DataRow rootRow)
		{
			DataRelationCollection relationsWithParent=primaryDataSet.Tables[rootTableName].ParentRelations;
			if(relationsWithParent.Count!=1 || relationsWithParent[0].ChildColumns.Length!=1 || !(rootRow[relationsWithParent[0].ChildColumns[0].ColumnName] is Guid))
				throw new InvalidRelationshipException(parentTableName,rootTableName);

			return new SqlNamedParameterValue[]{new SqlNamedParameterValue("@PK"+relationsWithParent[0].ParentColumns[0].ColumnName,
												   (Guid)rootRow[relationsWithParent[0].ChildColumns[0].ColumnName])};
		}

		public override void Hydrate(IBrokerManagedComponent iBMC,Guid cID, ref DataSet persistedPrimaryDS)
		{
			SQLBMCTypeAdapter sQLBMCTypeAdapter;

			iBMC.MigrationInProgress = true;

			// Create a primary data set for the BMC Type
			DataSet primaryDataSet=iBMC.PrimaryDataSet;

			// Get the BMCType Adapter for the type
			sQLBMCTypeAdapter=GetSqlDataAdapter(iBMC.TypeID,primaryDataSet);
			sQLBMCTypeAdapter.Connection=this.Connection;
			sQLBMCTypeAdapter.Transaction=this.Transaction;

			PopulateTables(primaryDataSet,sQLBMCTypeAdapter,cID);

//			iBMC.CID=cID;
			iBMC.SetData(primaryDataSet);

			persistedPrimaryDS = primaryDataSet;

			iBMC.MigrationInProgress = false;
		}

		public void Hydrate(IBrokerManagedComponent iBMC,Guid cID,DataSet basePrimaryDataSet, ref DataSet persistedDS)
		{
			SQLBMCTypeAdapter sQLBMCTypeAdapter;

			iBMC.MigrationInProgress = true;

			// Get the BMCType Adapter for the type
			sQLBMCTypeAdapter=GetSqlDataAdapter(iBMC.TypeID,basePrimaryDataSet);
			sQLBMCTypeAdapter.Connection=this.Connection;
			sQLBMCTypeAdapter.Transaction=this.Transaction;

            sQLBMCTypeAdapter.PrepareCommands();

			PopulateTables(basePrimaryDataSet,sQLBMCTypeAdapter,cID);

//			iBMC.CID=cID;
			iBMC.SetData(basePrimaryDataSet);

			iBMC.MigrationInProgress = false;
			
			persistedDS = basePrimaryDataSet;
			
			iBMC.ResetTokenToPersistedToken();
			iBMC.Modified = false;
		}

		public void Save(IBrokerManagedComponent iBMC, DataSet primaryDataSet, ref DataSet persistedDS)
		{
			// Populate the Primary DataSet
			iBMC.GetData(primaryDataSet);
			
			primaryDataSet.AcceptChanges();

			// Get the SQL Adapter set for this type
			SQLBMCTypeAdapter sQLBMCTypeAdapter=GetSqlDataAdapter(iBMC.TypeID,primaryDataSet);
			sQLBMCTypeAdapter.Connection=this.Connection;
			sQLBMCTypeAdapter.Transaction=this.Transaction;

            sQLBMCTypeAdapter.PrepareCommands();

			// Get the existing state of all of the tables in the persistence model.
			if(persistedDS == null)
			{
				DataSet existingDataSet=primaryDataSet.Clone();
				PopulateTables(existingDataSet,sQLBMCTypeAdapter,iBMC.CID);
				existingDataSet.AcceptChanges();

                // need to set this flag manually here
                foreach(DataTable dataTable in primaryDataSet.Tables)
                {
                    PrimaryDS.PDSDataTable pDSDataTable = dataTable as PrimaryDS.PDSDataTable;
                    (existingDataSet.Tables[pDSDataTable.TableName] as PrimaryDS.PDSDataTable).ApplyUpdatesDynamically = pDSDataTable.ApplyUpdatesDynamically;
                }

                persistedDS = existingDataSet;
			}

			// Save each table in turn, unless they are read-only
			persistedDS.EnforceConstraints=false;
			foreach(DataTable dataTable in primaryDataSet.Tables)
			{
				PrimaryDS.PDSDataTable pDSDataTable=dataTable as PrimaryDS.PDSDataTable;

                // only do save comparisons if not updating dynamically
                if ( !pDSDataTable.ApplyUpdatesDynamically )
                {
                    if(pDSDataTable!=null && pDSDataTable.PersistenceType==PrimaryDS.PersistenceTypeOption.READWRITE)
                    {
                        SqlDataAdapter sqlDataAdapter=sQLBMCTypeAdapter[dataTable.TableName].Adapter;

                        // Get the existing rows in the database for this instance of the BMC type
                        DataTable existingTable=persistedDS.Tables[dataTable.TableName];

                        // Determine the changes between the existing table in the database and the latest table from the
                        // BMC implementation class.  The row state is set appropriately in the existing data table.
                        DataColumn primaryKeyColumn=dataTable.PrimaryKey[0];
                        MarkChanges(dataTable,existingTable,primaryKeyColumn.ColumnName);

                        // Update the table in the database with only thos rows which are new, have changed or have been deleted
                        sqlDataAdapter.Update(existingTable);
                    }
                }
			}		
		}

		public override void Save(IBrokerManagedComponent iBMC, ref DataSet persistedPrimaryDS)
		{
			// Get the primary dataset from the instance to be saved
			DataSet primaryDataSet=iBMC.PrimaryDataSet;
			Save(iBMC,primaryDataSet, ref persistedPrimaryDS);
		}

        public virtual void Commit(Enlistment enlistment)
        {
            //enlistment.Done();
        }

        public virtual void InDoubt(Enlistment enlistment)
        {
        }

        public virtual void Prepare(PreparingEnlistment preparingEnlistment)
        {
            if (_IBMC is ICalculationModule)
                ((ICalculationModule)this._IBMC).CSID = this._CSID;

            this.Save(this._IBMC, ref this._PersistedPrimaryDS);
        }

        public virtual void Rollback(Enlistment enlistment)
        {
            //enlistment.Done();
        }

		public override void Delete(IBrokerManagedComponent iBMC)
		{
			// Get the primary dataset from the instance to be saved
			DataSet primaryDataSet=iBMC.PrimaryDataSet;

			Delete(iBMC, primaryDataSet);
		}

		public void Delete(IBrokerManagedComponent iBMC,DataSet primaryDataSet)
		{
			// Get the SQL Adapter set for this type
			SQLBMCTypeAdapter sQLBMCTypeAdapter=GetSqlDataAdapter(iBMC.TypeID,primaryDataSet);
			sQLBMCTypeAdapter.Connection=this.Connection;
			sQLBMCTypeAdapter.Transaction=this.Transaction;

            sQLBMCTypeAdapter.PrepareCommands();
			
			// Get the existing state of all of the tables in the persistence model.
			DataSet existingDataSet=primaryDataSet.Clone();
			PopulateTables(existingDataSet,sQLBMCTypeAdapter,iBMC.CID);
			existingDataSet.AcceptChanges();

			primaryDataSet.EnforceConstraints=false;
			// Save each table in turn, unless they are read-only
			foreach(DataTable dataTable in primaryDataSet.Tables)
			{
				PrimaryDS.PDSDataTable pDSDataTable=dataTable as PrimaryDS.PDSDataTable;

				if(pDSDataTable!=null && pDSDataTable.PersistenceType==PrimaryDS.PersistenceTypeOption.READWRITE)
				{
					// Get the existing rows in the database for this instance of the BMC type
					DataTable existingTable=existingDataSet.Tables[dataTable.TableName];

					foreach(DataRow dRow in existingTable.Rows)
						dRow.Delete();
					
					SqlDataAdapter sqlDataAdapter=sQLBMCTypeAdapter[dataTable.TableName].Adapter;
					sqlDataAdapter.Update(existingTable);
				}
			}
		}

		public override void EstablishDatabase()
		{
		}

		
		public override void RemoveOldDatabase()
		{
		}

		public void EstablishDatabase(Guid typeID,PrimaryDS primaryDataSet)
		{
			SQLBMCTypeAdapter sQLBMCTypeAdapter=GetSqlDataAdapter(typeID,primaryDataSet);

            SqlCommand establishCommand = DBCommandFactory.Instance.NewSqlCommand();
            establishCommand.Connection = this.Connection;
			establishCommand.Transaction = this.Transaction;

			foreach(PrimaryDS.PDSDataTable pDSDataTable in primaryDataSet.Tables)
			{
				if(pDSDataTable.PersistenceType==PrimaryDS.PersistenceTypeOption.READWRITE)
				{
					SQLBMCTypeAdapter.AdapterEntry adapterEntry=sQLBMCTypeAdapter[pDSDataTable.TableName];
					establishCommand.CommandText=adapterEntry.EstablishString;
					establishCommand.ExecuteNonQuery();
				}
			}
		}

		public override void EstablishDatabase(CMTypeInfo typeInfo)
		{
			Assembly implementationAssy=System.Reflection.Assembly.Load(typeInfo.AssemblyName);
			Type persisterType=implementationAssy.GetType(typeInfo.ClassName);
			IBrokerManagedComponent iBMC=(IBrokerManagedComponent)Activator.CreateInstance(persisterType);

			// Create a primary data set for the BMC Type
			DataSet primaryDataSet=iBMC.PrimaryDataSet;

			SQLBMCTypeAdapter sQLBMCTypeAdapter=GetSqlDataAdapter(iBMC.TypeID,primaryDataSet);

            SqlCommand establishCommand = DBCommandFactory.Instance.NewSqlCommand();
            establishCommand.Connection = this.Connection;
            establishCommand.Transaction = this.Transaction;

			foreach(PrimaryDS.PDSDataTable pDSDataTable in primaryDataSet.Tables)
			{
				if(pDSDataTable.PersistenceType==PrimaryDS.PersistenceTypeOption.READWRITE)
				{
					SQLBMCTypeAdapter.AdapterEntry adapterEntry=sQLBMCTypeAdapter[pDSDataTable.TableName];
					establishCommand.CommandText=adapterEntry.EstablishString;
					establishCommand.ExecuteNonQuery();
				}
			}
		}

		public override void ClearDatabase()
		{

		}

		public override void RemoveDatabase()
		{
		}

		public override DataSet FindDetailsByTypeId(Guid typeId, object[] parameters)
		{
			return new DataSet();
		}

		private SQLBMCTypeStrings CreateSQLBMCTypeStrings(Guid typeID,DataSet primaryDataSet)
		{
			SQLBMCTypeStrings sQLBMCTypeStrings=new SQLBMCTypeStrings();

			string insertString, updateString, selectString, deleteString, establishString;
			SqlParameter[] insertParameters,updateParameters,selectParameters,deleteParameters,establishParameters;

			foreach(DataTable dataTable in primaryDataSet.Tables)
			{
				if(dataTable is PrimaryDS.PDSDataTable)
				{
					PrimaryDS.PDSDataTable pDSDataTable=dataTable as PrimaryDS.PDSDataTable;

					// Generate the insert SQL strings
					GenerateInsertCommandText(pDSDataTable,out insertString,out insertParameters);

					// Generate the update SQL strings
					GenerateUpdateCommandText(pDSDataTable,out updateString,out updateParameters);

					// Generate the delete SQL strings
					GenerateDeleteCommandText(pDSDataTable,out deleteString,out deleteParameters);

					// Generate the select SQL strings
					GenerateSelectCommandText(pDSDataTable,out selectString,out selectParameters);

					// Generate the establish database SQL strings
					GenerateEstablishCommandText(pDSDataTable,out establishString,out establishParameters);

					bool root_table=pDSDataTable.PersistenceMethod==PrimaryDS.PersistenceMethodOption.ROOT;

					SQLTableStrings sQLTableStrings=new SQLTableStrings(dataTable.TableName,
						root_table,
						pDSDataTable.ChildTableNames,
						pDSDataTable.ParentTableNames,
						insertString,insertParameters,
						updateString,updateParameters,
						selectString,selectParameters,
						deleteString,deleteParameters,
						establishString,establishParameters);

					sQLBMCTypeStrings.Add(dataTable.TableName,sQLTableStrings);
				}
			}

			return sQLBMCTypeStrings;
		}

		protected virtual void GenerateInsertCommandText(PrimaryDS.PDSDataTable dataTable,out string commandText,out SqlParameter[] commandParameters)
		{
			if(dataTable.PersistenceType==PrimaryDS.PersistenceTypeOption.READWRITE)
			{
				commandParameters=new SqlParameter[dataTable.Columns.Count];
				string columnsString,valuesString,delim;
				columnsString=valuesString=delim="";
				int parameterIndex=0;
				foreach(DataColumn dataColumn in dataTable.Columns)
				{
					columnsString+=delim+dataColumn.ColumnName;
					valuesString+=delim+"@"+dataColumn.ColumnName;
					delim=",";
					SqlDbTypeDescription sqlDbTypeDescription=TypeToSQLType(dataColumn);

					if(dataColumn.ExtendedProperties.ContainsKey("Length"))
						sqlDbTypeDescription.Size=int.Parse((string)dataColumn.ExtendedProperties["Length"]);

					SqlParameter sqlParameter;
					if(sqlDbTypeDescription.DbType != SqlDbType.Image)
						sqlParameter=new SqlParameter("@"+dataColumn.ColumnName,sqlDbTypeDescription.DbType,sqlDbTypeDescription.Size,dataColumn.ColumnName);
                    else
                    {
                        sqlParameter = new SqlParameter("@" + dataColumn.ColumnName, sqlDbTypeDescription.DbType);
                        sqlParameter.SourceColumn = dataColumn.ColumnName;
                    }
					commandParameters[parameterIndex++]=sqlParameter;
				}
				commandText="INSERT INTO "+dataTable.TableName+" ("+columnsString+") VALUES ("+valuesString+");";
			}
			else
			{
				commandText="";
				commandParameters=new SqlParameter[0];
			}
		}

		
		protected virtual void GenerateUpdateCommandText(PrimaryDS.PDSDataTable dataTable,out string commandText,out SqlParameter[] commandParameters)
		{
			if(dataTable.PersistenceType==PrimaryDS.PersistenceTypeOption.READWRITE)
			{
				string delim="";
				commandParameters=new SqlParameter[dataTable.Columns.Count+dataTable.PrimaryKey.Length];
				commandText="UPDATE "+dataTable.TableName+" SET ";
				int parameterIndex=0;

				foreach(DataColumn dataColumn in dataTable.Columns)
				{
					commandText+=delim+dataColumn.ColumnName+"=";
					commandText+="@"+dataColumn.ColumnName;
					delim=",";
					SqlDbTypeDescription sqlDbTypeDescription=TypeToSQLType(dataColumn);

					if(dataColumn.ExtendedProperties.ContainsKey("Length"))
						sqlDbTypeDescription.Size=int.Parse((string)dataColumn.ExtendedProperties["Length"]);

					SqlParameter sqlParameter;
                    if (sqlDbTypeDescription.DbType != SqlDbType.Image)
                        sqlParameter = new SqlParameter("@" + dataColumn.ColumnName, sqlDbTypeDescription.DbType, sqlDbTypeDescription.Size, dataColumn.ColumnName);
                    else
                    {
                        sqlParameter = new SqlParameter("@" + dataColumn.ColumnName, sqlDbTypeDescription.DbType);
                        sqlParameter.SourceColumn = dataColumn.ColumnName;
                    }
					commandParameters[parameterIndex++]=sqlParameter;
				}
				commandText+=" WHERE ";
				delim="";
				foreach(DataColumn dataColumn in dataTable.PrimaryKey)
				{
					commandText+=delim+dataColumn.ColumnName+"=@PK"+dataColumn.ColumnName;
					delim=" AND ";
					SqlDbTypeDescription sqlDbTypeDescription=TypeToSQLType(dataColumn);
					
					SqlParameter sqlParameter;
					if(sqlDbTypeDescription.DbType != SqlDbType.Image)
						sqlParameter=new SqlParameter("@PK"+dataColumn.ColumnName,sqlDbTypeDescription.DbType,sqlDbTypeDescription.Size,dataColumn.ColumnName);
					else
					{
						sqlParameter=new SqlParameter("@PK"+dataColumn.ColumnName,sqlDbTypeDescription.DbType);
						sqlParameter.SourceColumn = dataColumn.ColumnName;
					}
					commandParameters[parameterIndex++]=sqlParameter;
				}
			}
			else
			{
				commandText="";
				commandParameters=new SqlParameter[0];
			}
		}

		
		protected virtual void GenerateSelectCommandText(PrimaryDS.PDSDataTable dataTable,out string commandText,out SqlParameter[] commandParameters)
		{
			string delim;
			int parameterIndex;

			if(dataTable.PersistenceType!=PrimaryDS.PersistenceTypeOption.TRANSIENT)
			{
				commandText="SELECT ";
				delim="";
				foreach(DataColumn dataColumn in dataTable.Columns)
				{
					commandText+=delim+dataColumn.ColumnName;
					delim=",";
				}
				commandText+=" FROM "+dataTable.TableName+" WHERE (";
				delim="";
				parameterIndex=0;

				switch(dataTable.PersistenceMethod)
				{
					case PrimaryDS.PersistenceMethodOption.ROOT:
						commandParameters=new SqlParameter[dataTable.PrimaryKey.Length];
						foreach(DataColumn dataColumn in dataTable.PrimaryKey)
						{
							commandText+=delim+dataColumn.ColumnName+"=@PK"+dataColumn.ColumnName;
							delim=" AND ";
							SqlDbTypeDescription sqlDbTypeDescription=TypeToSQLType(dataColumn);
							SqlParameter sqlParameter;

                            if (sqlDbTypeDescription.DbType != SqlDbType.Image)
								sqlParameter=new SqlParameter("@PK"+dataColumn.ColumnName,sqlDbTypeDescription.DbType,sqlDbTypeDescription.Size,dataColumn.ColumnName);
                            else
                            {
                                sqlParameter = new SqlParameter("@PK" + dataColumn.ColumnName, sqlDbTypeDescription.DbType);
                                sqlParameter.SourceColumn = dataColumn.ColumnName;
                            }

                            commandParameters[parameterIndex++] = sqlParameter;
                        }
                        break;

					case PrimaryDS.PersistenceMethodOption.CHILD:
						commandParameters=new SqlParameter[dataTable.ParentRelations.Count];
						foreach(DataRelation dataRelation in dataTable.ParentRelations)
						{
							foreach(DataColumn dataColumn in dataRelation.ChildColumns)
							{
								commandText+=delim+dataColumn.ColumnName+"=@FK"+dataColumn.ColumnName;
								delim=" AND ";
								SqlDbTypeDescription sqlDbTypeDescription=TypeToSQLType(dataColumn);
								SqlParameter sqlParameter;
								if(sqlDbTypeDescription.DbType != SqlDbType.Image)
									sqlParameter=new SqlParameter("@FK"+dataColumn.ColumnName,sqlDbTypeDescription.DbType,sqlDbTypeDescription.Size,dataColumn.ColumnName);
								else
								{
									sqlParameter=new SqlParameter("@FK"+dataColumn.ColumnName,sqlDbTypeDescription.DbType);
									sqlParameter.SourceColumn = dataColumn.ColumnName;
								}
								
								commandParameters[parameterIndex++]=sqlParameter;
							}
						}
						break;

					case PrimaryDS.PersistenceMethodOption.PARENT:
						commandParameters=new SqlParameter[dataTable.ChildRelations.Count];
						foreach(DataRelation dataRelation in dataTable.ChildRelations)
						{
							foreach(DataColumn dataColumn in dataRelation.ParentColumns)
							{
								commandText+=delim+dataColumn.ColumnName+"=@PK"+dataColumn.ColumnName;
								delim=" AND ";
								SqlDbTypeDescription sqlDbTypeDescription=TypeToSQLType(dataColumn);
								SqlParameter sqlParameter;

								if(sqlDbTypeDescription.DbType != SqlDbType.Image)
									sqlParameter=new SqlParameter("@PK"+dataColumn.ColumnName,sqlDbTypeDescription.DbType,sqlDbTypeDescription.Size,dataColumn.ColumnName);
								else
								{
									sqlParameter=new SqlParameter("@PK"+dataColumn.ColumnName,sqlDbTypeDescription.DbType);
									sqlParameter.SourceColumn = dataColumn.ColumnName;
								}

								commandParameters[parameterIndex++]=sqlParameter;
							}
						}
						break;
					default:
						commandParameters=new SqlParameter[0];
						break;
				}
				commandText+=");";
			}
			else
			{
				commandText="";
				commandParameters=new SqlParameter[0];
			}		
		}

		
		protected virtual void GenerateDeleteCommandText(PrimaryDS.PDSDataTable dataTable,out string commandText,out SqlParameter[] commandParameters)
		{
			string delim;
			int parameterIndex;

			if(dataTable.PersistenceType==PrimaryDS.PersistenceTypeOption.READWRITE)
			{
				commandText="DELETE "+dataTable.TableName+" WHERE ";
				delim="";
				commandParameters=new SqlParameter[dataTable.PrimaryKey.Length];
				parameterIndex=0;
				foreach(DataColumn dataColumn in dataTable.PrimaryKey)
				{
					commandText+=delim+dataColumn.ColumnName+"=@"+dataColumn.ColumnName;
					delim=" AND ";
					SqlDbTypeDescription sqlDbTypeDescription=TypeToSQLType(dataColumn);
					SqlParameter sqlParameter;
					if(sqlDbTypeDescription.DbType != SqlDbType.Image)
						sqlParameter=new SqlParameter("@"+dataColumn.ColumnName,sqlDbTypeDescription.DbType,sqlDbTypeDescription.Size,dataColumn.ColumnName);
                    else
                    {
                        sqlParameter = new SqlParameter("@" + dataColumn.ColumnName, sqlDbTypeDescription.DbType);
                        sqlParameter.SourceColumn = dataColumn.ColumnName;
                    }

					commandParameters[parameterIndex++]=sqlParameter;
				}
			}
			else
			{
				commandText="";
				commandParameters=new SqlParameter[0];
			}
		}

        /// <summary>
        /// Generates the SQL text necessary to create the table in the database
        /// </summary>
        /// <param name="dataTable">The table to create the SQL for</param>
        /// <param name="commandText">The SQL command that is generated (out parameter)</param>
        /// <param name="commandParameters">Any command parameters created</param>
        /// <exception cref="System.ArgumentNullException">dataTable cannot be null</exception>
        protected virtual void GenerateEstablishCommandText(PrimaryDS.PDSDataTable dataTable, out string commandText, out SqlParameter[] commandParameters)
        {
            if (dataTable == null)
                throw new ArgumentNullException("dataTable");

            const string EstablishTableCommand = "if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[{0}]') and OBJECTPROPERTY(id, N'IsUserTable') = 1) BEGIN CREATE TABLE [dbo].[{0}] ({1}) ON [PRIMARY]; {2}{3} END";

            string delim;
            commandParameters = new SqlParameter[0];

            if (dataTable.PersistenceType == PrimaryDS.PersistenceTypeOption.READWRITE)
            {
                string tableDefinition = String.Empty;

                delim = "";

                foreach (DataColumn dataColumn in dataTable.Columns)
                {
                    tableDefinition += delim + GenerateEstablishTableColumnText(dataTable, dataColumn);
                    delim = ",";
                }

                tableDefinition += ", ";

                tableDefinition += this.GenerateEstablishTablePrimaryKeyText(dataTable);

                commandText = String.Format(EstablishTableCommand, dataTable.TableName, tableDefinition, this.GenerateEstablishTableIndexes(dataTable), this.GenerateEstablishTableStatistics(dataTable));
            }
            else
            {
                commandText = String.Empty;
            }
		}

        /// <summary>
        /// Generates the Create database table SQL command for the columns part of the statement
        /// </summary>
        /// <param name="dataTable">The table to create the SQL for</param>
        /// <param name="dataColumn">The datacolumn to create the SQL for</param>
        /// <exception cref="System.ArgumentNullException">dataTable cannot be null, dataColumn cannot be null</exception>
        protected virtual string GenerateEstablishTableColumnText(PrimaryDS.PDSDataTable dataTable, DataColumn dataColumn)
        {
            if (dataTable == null)
                throw new ArgumentNullException("dataTable");

            if (dataColumn == null)
                throw new ArgumentNullException("dataColumn");

            SqlDbTypeDescription sqlDbTypeDescription = TypeToSQLType(dataColumn);

            string columnText = "[" + dataColumn.ColumnName + "] " + sqlDbTypeDescription.SqlTypeString + ""
                + (dataColumn.AllowDBNull == true ? " NULL " : " NOT NULL ");

            return columnText;
		}

        /// <summary>
        /// Generates the SQL for the primary key constriant for a table
        /// </summary>
        /// <param name="dataTable">The table to create the SQL for</param>
        /// <exception cref="System.ArgumentNullException">dataTable cannot be null</exception>
        protected virtual string GenerateEstablishTablePrimaryKeyText(PrimaryDS.PDSDataTable dataTable)
        {
            if (dataTable == null)
                throw new ArgumentNullException("dataTable");

            if (dataTable.PrimaryKey == null || dataTable.PrimaryKey.Length == 0)
                return String.Empty;

            const string ConstraintsCommand = "CONSTRAINT {0}_PK PRIMARY KEY {1} ({2})";

            string primaryKeyColumnText = String.Empty;
            string delim = String.Empty;

            foreach (DataColumn dataColumn in dataTable.PrimaryKey)
            {
                primaryKeyColumnText += delim + dataColumn.ColumnName;
                delim = ",";
            }

            string clusteredString = dataTable.PrimaryKeyIndexIsClustered ? "CLUSTERED" : "NONCLUSTERED";

            return String.Format(ConstraintsCommand, dataTable.TableName, clusteredString, primaryKeyColumnText);
        }

        /// <summary>
        /// Creates any SQL required for indexes for the table
        /// </summary>
        /// <param name="dataTable">The table to create the SQL for</param>
        /// <exception cref="System.ArgumentNullException">dataTable cannot be null</exception>
        protected virtual string GenerateEstablishTableIndexes(PrimaryDS.PDSDataTable dataTable)
        {
            const string IndexSQL = "CREATE {0} {1} INDEX {2} ON {3} ({4}) ON [Primary]";
            const string ClusteredText = "CLUSTERED";
            const string NonClusteredText = "NONCLUSTERED";
            const string UniqueText = "UNIQUE";
            const string AscText = "ASC";
            const string DescText = "DESC";

            if (dataTable == null)
                throw new ArgumentNullException("dataTable");

            if (dataTable.Indexes.Count == 0)
                return String.Empty;

            StringBuilder indexSQL = new StringBuilder();

            foreach(DatabaseIndex index in dataTable.Indexes)
            {
                StringBuilder columnSQL = new StringBuilder();

                if (index.IndexColumns.Length == 0)
                    throw new ArgumentException("DataTable: " + dataTable.TableName + ", index: " + index.Name + ", contains no columns to create an index on");

                foreach (DataColumn column in index.IndexColumns)
                {
                    if (column.Table != dataTable)
                        throw new ArgumentException("DataTable: " + dataTable.TableName + ", index: " + index.Name + ", contains a column: " + column.ColumnName + ", which does not belong to the table on which it is defined");

                    columnSQL.Append(column.ColumnName);
                    columnSQL.Append(" ");
                    columnSQL.Append(index.SortAscending ? AscText : DescText);
                    columnSQL.Append(", ");
                }

                string trimmedColumnSQL = columnSQL.ToString().TrimEnd(' ').TrimEnd(',');
                string uniqueText = index.Unique ? UniqueText : String.Empty;
                string clusteringText = index.Clustered ? ClusteredText : NonClusteredText;

                indexSQL.Append(String.Format(IndexSQL, uniqueText, clusteringText, index.Name, dataTable.TableName, trimmedColumnSQL));
                indexSQL.Append(";");
            }

            return indexSQL.ToString();
        }

        /// <summary>
        /// Creates any SQL required for statistics for the table
        /// </summary>
        /// <param name="dataTable">The table to create the SQL for</param>
        /// <exception cref="System.ArgumentNullException">dataTable cannot be null</exception>
        protected virtual string GenerateEstablishTableStatistics(PrimaryDS.PDSDataTable dataTable)
        {
            const string StatisticSQL = "CREATE STATISTICS [{0}] ON {1} ({2})";

            if (dataTable == null)
                throw new ArgumentNullException("dataTable");

            if (dataTable.Statistics.Count == 0)
                return String.Empty;

            StringBuilder statisticSQL = new StringBuilder();

            foreach (DatabaseStatistic statistic in dataTable.Statistics)
            {
                StringBuilder columnSQL = new StringBuilder();

                if (statistic.StatisticColumns.Length == 0)
                    throw new ArgumentException("DataTable: " + dataTable.TableName + ", statistic: " + statistic.Name + ", contains no columns to create statistics on");

                foreach (DataColumn column in statistic.StatisticColumns)
                {
                    if (column.Table != dataTable)
                        throw new ArgumentException("DataTable: " + dataTable.TableName + ", statistic: " + statistic.Name + ", contains a column: " + column.ColumnName + ", which does not belong to the table on which it is defined");

                    columnSQL.Append(column.ColumnName);
                    columnSQL.Append(", ");
                }

                string trimmedColumnSQL = columnSQL.ToString().TrimEnd(' ').TrimEnd(',');

                statisticSQL.Append(String.Format(StatisticSQL, statistic.Name, dataTable.TableName, trimmedColumnSQL));
                statisticSQL.Append(";");
            }

            return statisticSQL.ToString();
        }

        protected SQLBMCTypeAdapter GetSqlDataAdapter(Guid typeID, DataSet primaryDataSet)
		{
			SQLBMCTypeAdapter sQLBMCTypeAdapter;

			sQLBMCTypeAdapter=sQLBMCTypeAdapters[typeID];

			if(sQLBMCTypeAdapter==null)
			{
				sQLBMCTypeAdapter=new SQLBMCTypeAdapter();

				SQLBMCTypeStrings sQLBMCTypeStrings=CreateSQLBMCTypeStrings(typeID,primaryDataSet);

				sQLBMCTypeAdapter=new SQLBMCTypeAdapter();

				foreach(DictionaryEntry dictionaryEntry in sQLBMCTypeStrings)
				{
					SQLTableStrings sQLTableStrings=dictionaryEntry.Value as SQLTableStrings;
					string tableName=(string)dictionaryEntry.Key;

					SqlDataAdapter sqlDataAdapter=new SqlDataAdapter();

					sqlDataAdapter.InsertCommand = DBCommandFactory.Instance.NewSqlCommand(sQLTableStrings.InsertString);
					foreach(SqlParameter sqlParameter in sQLTableStrings.InsertParameters)
					{
						sqlDataAdapter.InsertCommand.Parameters.Add(sqlParameter);
					}
	
					sqlDataAdapter.UpdateCommand = DBCommandFactory.Instance.NewSqlCommand(sQLTableStrings.UpdateString);
					foreach(SqlParameter sqlParameter in sQLTableStrings.UpdateParameters)
					{
						sqlDataAdapter.UpdateCommand.Parameters.Add(sqlParameter);
					}

					sqlDataAdapter.DeleteCommand = DBCommandFactory.Instance.NewSqlCommand(sQLTableStrings.DeleteString);
					foreach(SqlParameter sqlParameter in sQLTableStrings.DeleteParameters)
					{
						sqlDataAdapter.DeleteCommand.Parameters.Add(sqlParameter);
					}

					sqlDataAdapter.SelectCommand = DBCommandFactory.Instance.NewSqlCommand(sQLTableStrings.SelectString);
					foreach(SqlParameter sqlParameter in sQLTableStrings.SelectParameters)
					{
						sqlDataAdapter.SelectCommand.Parameters.Add(sqlParameter);
					}

					sqlDataAdapter.TableMappings.Add("Table",tableName);

					sQLBMCTypeAdapter.Add(sQLTableStrings,sqlDataAdapter);
				}
			}

			return sQLBMCTypeAdapter;
		}

        /// <summary>
        /// Converts a .NET type to a SQL equivalent type
        /// </summary>
        /// <param name="dataType">The .NET type to convert</param>
        /// <exception cref="System.ArgumentNullException">dataType cannot be null</exception>
        /// <exception cref="UnimplementedFieldTypeException">The type cannot be converted to a SQL type</exception>
        protected SqlDbTypeDescription TypeToSQLType(Type dataType)
        {
            if (dataType == null)
                throw new ArgumentNullException("dataType");

            switch (dataType.FullName)
            {
                case "System.String":
                    return new SqlDbTypeDescription(SqlDbType.VarChar, 355, "[varchar] (355) COLLATE Latin1_General_CI_AS");
                case "System.Int16":
                case "System.Int32":
                    return new SqlDbTypeDescription(SqlDbType.Int, 4, "[integer]");
                case "System.DateTime":
                    return new SqlDbTypeDescription(SqlDbType.DateTime, 8, "[datetime]");
                case "System.Guid":
                    return new SqlDbTypeDescription(SqlDbType.UniqueIdentifier, 16, "[uniqueidentifier]");
                case "System.Boolean":
                    return new SqlDbTypeDescription(SqlDbType.Bit, 16, "[bit]");
                case "System.Byte[]":
                    return new SqlDbTypeDescription(SqlDbType.Image, 16, "[image]");
                case "System.Decimal":
                    return new SqlDbTypeDescription(SqlDbType.Decimal, 0, "[decimal](18, 2)");
                default:
                    throw new UnimplementedFieldTypeException(dataType.FullName);
            }
		}

        /// <summary>
        /// Converts a data column .NET type to a SQL equivalent type
        /// </summary>
        /// <param name="dataColumn">The data column to convert</param>
        /// <exception cref="System.ArgumentNullException">dataColumn cannot be null</exception>
        /// <exception cref="UnimplementedFieldTypeException">The type cannot be converted to a SQL type</exception>
        protected SqlDbTypeDescription TypeToSQLType(DataColumn dataColumn)
        {
            if (dataColumn == null)
                throw new ArgumentNullException("dataColumn");

            Type dataType = dataColumn.DataType;

            switch (dataType.FullName)
            {
                case "System.String":
                    if (dataColumn.ExtendedProperties["Length"] == null)
                        return this.TypeToSQLType(dataType);
                    else
                    {
                        string lengthString = (string)dataColumn.ExtendedProperties["Length"];
                        return new SqlDbTypeDescription(SqlDbType.VarChar, 355, "[varchar] (" + lengthString + ") COLLATE Latin1_General_CI_AS");
                    }
                default:
                    return this.TypeToSQLType(dataType);
            }
		}

		/// <summary>
		/// Method for copying rows from one table in the databse to another in the in memory dataset
		/// where there is no schema change
		/// </summary>
		/// <param name="oldTableName"></param>
		/// <param name="newTableName"></param>
		/// <param name="newDS"></param>
		protected void CopyTableNoStructureChange(string oldTableName, string newTableName, DataSet newDS)
		{
			SqlDataAdapter dataAdapter = new SqlDataAdapter("select * from "+oldTableName,this.Connection);
			DataSet ds = new DataSet(oldTableName);
			dataAdapter.Fill(ds, oldTableName);
			foreach(DataRow dRow in ds.Tables[oldTableName].Rows)
				newDS.Tables[newTableName].Rows.Add(dRow.ItemArray);
		}
	}
}
