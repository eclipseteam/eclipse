using System;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using Oritax.TaxSimp.Calculation; 

namespace Oritax.TaxSimp.Calculation
{
	public class AttacherListDS : DataSet
	{
		public const string ATTACHERTABLE				= "ATTACHER";
		public const string ATTACHMENTID_FIELD			= "ID";
		public const string ATTACHMENTBMCID_FIELD		= "BMCCID";
		public const string ATTACHEDBMCID_FIELD			= "ATTACHEDBMCID";
		//public const string ATTACHMENTNAME_FIELD		= "NAME";
		public const string ATTACHMENTDATEIMPORTED_FIELD= "DATEIMPORTED";
		public const string ATTACHMENTIMPORTEDBY_FIELD  = "IMPORTEDBY";
		public const string ATTACHMENTORIGINPATH_FIELD  = "ORIGINPATH";
		public const string ATTACHMENTDESCRIPTION_FIELD = "DESCRIPTION";

		Guid attachmentID;

		public Guid AttachmentID
		{
			get{return attachmentID;}
			set{attachmentID=value;}
		}

		public AttacherListDS()
		{
			DataTable dt=new DataTable(ATTACHERTABLE);
			dt.Columns.Add(ATTACHMENTID_FIELD, typeof(string));
			dt.Columns.Add(ATTACHMENTBMCID_FIELD, typeof(string));
			dt.Columns.Add(ATTACHEDBMCID_FIELD, typeof(string));
			//dt.Columns.Add(ATTACHMENTNAME_FIELD, typeof(string));
			dt.Columns.Add(ATTACHMENTDATEIMPORTED_FIELD, typeof(DateTime));
			dt.Columns.Add(ATTACHMENTIMPORTEDBY_FIELD, typeof(string));
			dt.Columns.Add(ATTACHMENTORIGINPATH_FIELD, typeof(string));
			dt.Columns.Add(ATTACHMENTDESCRIPTION_FIELD, typeof(string));
			Tables.Add(dt);
		}
	}
}
