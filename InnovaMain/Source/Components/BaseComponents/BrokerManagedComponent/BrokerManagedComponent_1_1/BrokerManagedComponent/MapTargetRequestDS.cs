using System;
using System.Collections;
using System.Data;
using System.Text;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.MapTarget
{
	/// <summary>
	/// Summary description for MapTargetRequestDs.
	/// </summary>
	public class MapTargetRequestDS : DataSet
	{
		public const string MAPTARGETREQUESTTABLE	= "MAPTARGETREQUEST";
		public const string MAPTARGETREQUEST_DATATYPE_FIELD = "DATATYPE";
		public const string MAPTARGETREQUEST_DESCRIPTION_FIELD = "DESCRIPTION";
		public const string MAPTARGETREQUEST_CATEGORY_FIELD = "CATEGORY";
        public const string MAPTARGETREQUEST_BSCATEGORY_FIELD = "BSCATEGORY";
        public const string MAPTARGETREQUEST_COMPONENTNAME_FIELD = "COMPONENTNAME";
        public const string MAPTARGETREQUEST_COMPONENTID_FIELD = "COMPONENTID";
        public const string MAPTARGETREQUEST_ASSEMBLYID_FIELD = "ASSEMBLYID";

        private string description = String.Empty;
        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        private string componentName = String.Empty;
        public string ComponentName
        {
            get { return componentName; }
            set { componentName = value; }
        }

        private Guid componentID = Guid.Empty;
        public Guid ComponentID
        {
            get { return componentID; }
            set { componentID = value; }
        }

        private Guid assemblyID = Guid.Empty;
        public Guid AssemblyID
        {
            get { return assemblyID; }
            set { assemblyID = value; }
        }

        public void AddTarget(string DataType, string Description, string Category, AccountBalanceCategoryType balanceCategoryType)
		{
			DataRow dr=Tables[0].NewRow();
			dr[MAPTARGETREQUEST_DATATYPE_FIELD]=DataType;
			dr[MAPTARGETREQUEST_DESCRIPTION_FIELD]=Description;
			dr[MAPTARGETREQUEST_CATEGORY_FIELD]=Category;
            dr[MAPTARGETREQUEST_BSCATEGORY_FIELD] = balanceCategoryType;
            dr[MAPTARGETREQUEST_COMPONENTID_FIELD] = this.ComponentID;
            dr[MAPTARGETREQUEST_ASSEMBLYID_FIELD] = this.AssemblyID;
            dr[MAPTARGETREQUEST_COMPONENTNAME_FIELD] = this.ComponentName;
			Tables[MAPTARGETREQUESTTABLE].Rows.Add(dr);
		}

        public void AddTarget(string DataType, string Description, string Category)
        {
            DataRow dr = Tables[0].NewRow();
            dr[MAPTARGETREQUEST_DATATYPE_FIELD] = DataType;
            dr[MAPTARGETREQUEST_DESCRIPTION_FIELD] = Description;
            dr[MAPTARGETREQUEST_CATEGORY_FIELD] = Category;
            dr[MAPTARGETREQUEST_BSCATEGORY_FIELD] = AccountBalanceCategoryType.None;
            dr[MAPTARGETREQUEST_COMPONENTID_FIELD] = this.ComponentID;
            dr[MAPTARGETREQUEST_ASSEMBLYID_FIELD] = this.AssemblyID;
            dr[MAPTARGETREQUEST_COMPONENTNAME_FIELD] = this.ComponentName;
            Tables[MAPTARGETREQUESTTABLE].Rows.Add(dr);
        }

		public MapTargetRequestDS()
		{
			DataTable dt=new DataTable(MAPTARGETREQUESTTABLE);
			dt.Columns.Add(MAPTARGETREQUEST_DATATYPE_FIELD, typeof(string));
			dt.Columns.Add(MAPTARGETREQUEST_DESCRIPTION_FIELD, typeof(string));
			dt.Columns[MAPTARGETREQUEST_DESCRIPTION_FIELD].AllowDBNull = false;

			dt.Columns.Add(MAPTARGETREQUEST_CATEGORY_FIELD, typeof(string));
            dt.Columns[MAPTARGETREQUEST_CATEGORY_FIELD].AllowDBNull = false;
            dt.Columns.Add(MAPTARGETREQUEST_BSCATEGORY_FIELD, typeof(int));
            //CheckCat
            dt.Columns[MAPTARGETREQUEST_BSCATEGORY_FIELD].AllowDBNull = true;

            dt.Columns.Add(MAPTARGETREQUEST_COMPONENTID_FIELD, typeof(Guid));
            dt.Columns[MAPTARGETREQUEST_COMPONENTID_FIELD].AllowDBNull = false;

            dt.Columns.Add(MAPTARGETREQUEST_ASSEMBLYID_FIELD, typeof(Guid));
            dt.Columns[MAPTARGETREQUEST_ASSEMBLYID_FIELD].AllowDBNull = false;

            dt.Columns.Add(MAPTARGETREQUEST_COMPONENTNAME_FIELD, typeof(string));
            dt.Columns[MAPTARGETREQUEST_COMPONENTNAME_FIELD].AllowDBNull = false;

			dt.PrimaryKey=new DataColumn[] { dt.Columns[MAPTARGETREQUEST_DATATYPE_FIELD] }; 
			Tables.Add(dt);

			// Add a version table
			dt=new DataTable("TYPEINFO");
			dt.Columns.Add("VERSION", typeof(int));
			DataRow dr=dt.NewRow();
			dr["VERSION"]=0x00010000;
			dt.Rows.Add(dr);

			Tables.Add(dt);
		}
	}
}