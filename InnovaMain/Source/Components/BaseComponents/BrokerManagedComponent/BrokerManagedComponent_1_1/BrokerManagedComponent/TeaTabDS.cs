using System;
using System.Data;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.CalculationInterface;
using System.Collections;

namespace Oritax.TaxSimp.WPDatasetBase
{
	public enum RowType
	{
		Header = 0,
		Body,
        SubTotal,
		Total
	}

    public enum TEA40Recognition
    {
        DTL = 0,
        DTA,
        EXP40WP
    }
    public enum TEAGridStyle
    {
        Standard,   //1 header line, 1 footer line, editable fields as defined by the grid
        ReadOnlyBodyRows,    // no header or footer, all rows are readonly, row style is controlled by RowType
        OtherEditable // This flag is used for Provisions workpaper to make other column editable. 
    }	
	/// <summary>
	/// The DataSet Class used for Notes
	/// </summary>
	[Serializable()]
	public class TeaTabDS : WPDatasetBaseDS
	{
		#region Constants
		public const string WP_SPECIFIC_TABLE			= "WP_SPECIFIC_TABLE";
		public const string CLOSING_TEMP_DIFF_TABLE		= "CLOSING_TEMP_DIFF_TABLE";
		public const string TAX_BASE_FIELD				= "TAX_BASE_FIELD";
		public const string DESCRIPTION_FIELD			= "DESCRIPTION";
		public const string REF_FIELD = "REF";
		public const string URL_FIELD = "URL";
		public const string DIFFERENCE_FIELD			= "DIFFERENCE_FIELD";
        //This field is not needed any more as the TEA040 Recognition column in the 
        //Summary of closing temporary difference has to be removed due to the TEA040 DTA/DTL splitting
        //So this filter is not used any more. 
		public const string CATEGORY_FIELD				= "CATEGORY";

		public const string TEMP_DIFF_MOVEMENT_TABLE	= "TEMP_DIFF_MOVEMENT_TABLE";
		public const string OPENING_BALANCE_FIELD		= "OPENING_BALANCE";
		public const string EQUITY_FIELD				= "EQUITY";
		public const string TAX_EXPENSE_FIELD			= "TAX_EXPENSE";
		public const string OTHER_FIELD					= "OTHER";
		public const string CLOSING_BALANCE_FIELD		= "CLOSING_BALANCE";
		public const string ROW_TYPE_FIELD				= "ROW_TYPE";
		public const string PRIMARYKEY_FIELD = "KEY";
		public const string CLOSINGBAL_FIELD = "ClosingBal";//this should match to the definition of CLOSINGBAL_FIELD in the datasets derived from TaxTopicDetailsDS

		public const string TEMP_DIFF_MOVEMENT_DESCRIPTIONS_TABLE = "MOVEMENT_DESCRIPTIONS_TABLE";
		public const string MOVEMENT_TYPE_FIELD = "MOVEMENT_TYPE";
		

		#endregion Constants

		#region Private members
			private string wpSpecificTableName = WP_SPECIFIC_TABLE;
		#endregion Private members
		#region Public members

        /// <summary>
        /// Defines MovementAnalysisControlStyle:
        ///      Standard,   //no header line, 1 footer line, row style is defined by .ascx page
        ///      ReadOnlyBodyRows    // no header or footer, all rows are readonly, row style is controlled by RowType
        /// </summary>
        public TEAGridStyle ClosingTempDiffControlStyle = TEAGridStyle.Standard;

        /// <summary>
        /// Defines MovementAnalysisControlStyle:
        ///      Standard,   //1 header line, 1 footer line, editable fields "Equity" and "Other"
        ///      ReadOnlyBodyRows    // no header or footer, all rows are readonly, row style is controlled by RowType
        /// </summary>
        public TEAGridStyle MovementAnalysisControlStyle = TEAGridStyle.Standard;
		/// <summary>
		/// Whether prior period exists
		/// </summary>
		public bool HasPriorPeriod = false;

		/// <summary>
		/// Whether ClosingTemporaryDifferernceTable has one amount column active only- the last column
		/// If this is true, the rest of the amount columns will be disabled
		/// </summary>
		public bool ClosingTempDiffLastAmountOnly = false;
		/// <summary>
		/// Whether ClosingTemporaryDifferernceTable needs to display total row 
		/// </summary>
		public bool ClosingTempDiffDisplayTotalRow = true;
		/// <summary>
		/// name of the workpaper Tea tab belongs to
		/// </summary>
		public string MainWorkpaperName = "";

		public string MainWpUrl = "";
		public string MainWpRef = "";
		public string Tea040Url = "";
		public string Tea100Url = "";

        /// <summary>
        /// Depending on the value set to the TeaRecognition (which can be any of DTL, DTA or EXP40WP),
        /// the Column headings of ClosingTemporaryDifferernce table are displayed 
        /// </summary>
        public TEA40Recognition TeaRecognition = TEA40Recognition.DTL;

        /// <summary>
        /// This is used when we need to change the Label of the Summary of closing temporary difference table
        /// </summary>
        public bool Tea_SummaryLabel = true;

        /// <summary>
        /// This is used to display a note under the Summary of closing temporary difference grid in the TEA Tab
        /// </summary>
        public bool Tea_SummaryNote = false;

        /// <summary>
        /// When this flag is set the reference of the Body rows should be displayed whereas the reference
        /// of the total should be empty with a Green background
        /// </summary>
        public bool ClosingTempDiffRefBodyRowOnly = false;

		#endregion Private members
		#region Public Properties

        /// <summary>
		/// This table cab be different for every workpaper, 
		/// so it is created by Workpaper CM
		/// </summary>
		public DataTable WpSpecificTable
		{
			get
			{
				return this.Tables[wpSpecificTableName];
			}
			set
			{
				wpSpecificTableName = value.TableName;
				if (this.Tables[wpSpecificTableName] != null)
				{
					this.Tables.Remove(wpSpecificTableName);
				}
				this.Tables.Add(value);
			}
		}
		/// <summary>
		/// Is used by the main grid in ClosingTemporaryDifferernce control
		/// </summary>
		public DataTable ClosingTemporaryDifferernceTable
		{
			get
			{
				if (this.Tables[CLOSING_TEMP_DIFF_TABLE] == null)
				{
					DataTable table = AddClosingTempDiffTable(); 
				}
				return this.Tables[CLOSING_TEMP_DIFF_TABLE];
			}
		}
		///// <summary>
		///// Is used by the main grid in TeaTabMovementAnalysis control
		///// This property refwers to the TEMP_DIFF_MOVEMENT_TABLE with index 0
		///// </summary>
		//public DataTable TempDiffMovementTable
		//{
		//    get
		//    {
		//        DataTable table = GetTempDiffMovementTable(0);
		//        return table;
		//    }
		//}
		/// <summary>
		/// Is used by the main grid in TeaTabMovementAnalysis control
		/// </summary>
		public DataTable MovementDescriptionsTable
		{
			get
			{
				if (this.Tables[TEMP_DIFF_MOVEMENT_DESCRIPTIONS_TABLE] == null)
				{
					DataTable table = AddMovementDescriptionsTable(); 
				}
				return this.Tables[TEMP_DIFF_MOVEMENT_DESCRIPTIONS_TABLE];
			}
		}
		#endregion Public Properties
		#region Constructors
		public TeaTabDS()
		{
		}
		#endregion Constructors

		#region Public Methods
		/// <summary>
		/// Creates and adds to the dataset several tables of type TEMP_DIFF_MOVEMENT_TABLE
		/// </summary>
		/// <returns>created table</returns>
		public void AddTempDiffMovementTables(int numTables)
		{
			for (int index = 0; index < numTables; index++)
			{
				AddTempDiffMovementTable(index);
			}
		}

		/// <summary>
		/// Add a row with the given values to the MovenentDescriptionsTable
		/// </summary>
		/// <param name="type"></param>
		/// <param name="description"></param>
		public void AddMovementDescription(int type, string description)
		{
			DataTable table = MovementDescriptionsTable;
			DataRow row = table.NewRow();
			row[MOVEMENT_TYPE_FIELD] = type;
			row[DESCRIPTION_FIELD]= description;
			table.Rows.Add(row);
		}

		/// <summary>
		/// Get a Type with the given index from  MovementDescriptionsTable
		/// </summary>
		/// <param name="type"></param>
		public int GetMovementType(int index)
		{
			DataRow row = MovementDescriptionsTable.Rows[index];
			int type = (int)row[MOVEMENT_TYPE_FIELD];
			return type;
		}
		/// <summary>
		/// Get a Description with the given index from  MovementDescriptionsTable
		/// </summary>
		/// <param name="index"></param>
		public string GetMovementDescription(int index)
		{
			DataRow row = MovementDescriptionsTable.Rows[index];
			return (string)row[DESCRIPTION_FIELD];
		}
		/// <summary>
		/// Gets table of type TEMP_DIFF_MOVEMENT_TABLE based on the index
		/// </summary>
		public DataTable GetTempDiffMovementTable(int index)
		{
			string tableName = GetIndexedName(TEMP_DIFF_MOVEMENT_TABLE, index);
			if (this.Tables[tableName] == null)
			{
				DataTable table = AddTempDiffMovementTable(index);
			}
			return this.Tables[tableName];
		}
		/// <summary>
		/// Set data into 1st and total rows in ClosingTemporaryDifferernce table 
		/// based on data from other tables
		/// </summary>
		/// <param name="name">description to set in the 1st row</param>
		/// <param name="category">category to set in the total row</param>
		/// <param name="colName">name of the column in WpSpecificTable to copy data from</param>
		public void PopulateClosingBalanceTable(string description, string category, string colName)
		{
			ClosingTemporaryDifferernceTable.Rows.Clear();
			DataRow dr = GenerateClosingBalanceRowFromOtherTables(description, "", colName);
			ClosingTemporaryDifferernceTable.Rows.Add(dr);
			AddClosingBalanceTableTotalRow(dr, category);
		}
		/// <summary>
		/// Set data into a new row in ClosingTemporaryDifferernce table 
		/// based on data from TempDiffMovementTable and WpSpecificTable
		/// </summary>
		/// <param name="name">description to set in the row</param>
		/// <param name="category">category to set in the  row</param>
		/// <param name="colName">name of the column in WpSpecificTable to copy data from</param>
		public DataRow GenerateClosingBalanceRowFromOtherTables(string description, string category, string colName)
		{
			DataRow dr = ClosingTemporaryDifferernceTable.NewRow();
			dr[TeaTabDS.DESCRIPTION_FIELD] = description;
			dr[TeaTabDS.CATEGORY_FIELD] = category;
			dr[TeaTabDS.ROW_TYPE_FIELD] = RowType.Body;

			if ((WpSpecificTable != null) && (WpSpecificTable.Columns.Contains(colName)))
			{
				DataRow wpRow = GetBodyRow(WpSpecificTable, 0);
				if (wpRow != null)
					dr[TeaTabDS.CLOSING_BALANCE_FIELD] = wpRow[colName];

				wpRow = GetBodyRow(WpSpecificTable, 1);
				if (wpRow != null)
					dr[TeaTabDS.TAX_BASE_FIELD] = wpRow[colName];
			}
			DataRow diffRow = GetHeaderRow(GetTempDiffMovementTable(0));
			if (diffRow != null)
				dr[TeaTabDS.DIFFERENCE_FIELD] = diffRow[TeaTabDS.CLOSING_BALANCE_FIELD];
			return dr;
		}
		/// <summary>
		/// Set basic data into a new row in ClosingTemporaryDifferernce table 
		/// </summary>
		/// <param name="description">description to set in the row</param>
		/// <param name="category">category to set in the  row</param>
		/// <param name="type">type of the row</param>
		public DataRow GenerateClosingBalanceRow(string description, string category, RowType type)
		{
			DataRow dr = ClosingTemporaryDifferernceTable.NewRow();
			dr[TeaTabDS.DESCRIPTION_FIELD] = description;
			dr[TeaTabDS.CATEGORY_FIELD] = category;
			dr[TeaTabDS.ROW_TYPE_FIELD] = type;
			return dr;
		}
		/// <summary>
		/// Create and add Total row to ClosingTemporaryDifferernceTable with amounts copied from the given row
		/// </summary>
		/// <param name="dr">row to copy amounts from</param>
		/// <param name="category">category for the total row</param>
		public void AddClosingBalanceTableTotalRow(DataRow dr, string category)
		{
			DataRow drTotal = ClosingTemporaryDifferernceTable.NewRow();
			drTotal[TeaTabDS.DESCRIPTION_FIELD] = "Total";
			drTotal[TeaTabDS.REF_FIELD] = "TEA100";
			drTotal[TeaTabDS.URL_FIELD] = Tea100Url;
			drTotal[TeaTabDS.CLOSING_BALANCE_FIELD] = dr[TeaTabDS.CLOSING_BALANCE_FIELD];
			drTotal[TeaTabDS.TAX_BASE_FIELD] = dr[TeaTabDS.TAX_BASE_FIELD];
			drTotal[TeaTabDS.DIFFERENCE_FIELD] = dr[TeaTabDS.DIFFERENCE_FIELD];
			drTotal[TeaTabDS.CATEGORY_FIELD] = category;
			drTotal[TeaTabDS.ROW_TYPE_FIELD] = RowType.Total;
			ClosingTemporaryDifferernceTable.Rows.Add(drTotal);
		}

		/// <summary>
		/// returns a list of tables that should be included in a primary dataset
		/// for migration purposes
		/// </summary>
		/// <returns></returns>
		public ArrayList GetPrimarytables()
		{
			ArrayList tables = new ArrayList();
			tables.Add(MovementDescriptionsTable);

			int numRows = MovementDescriptionsTable.Rows.Count;
			for (int index = 0; index < numRows; index++)
			{
				DataTable table = GetTempDiffMovementTable(index);
				tables.Add(table);
			}
			return tables;
		}

		#endregion Public Methods

		#region Static Metods
		public static bool IsPrimaryTable(DataTable table)
		{
			bool result = false;
			if (table.TableName == TEMP_DIFF_MOVEMENT_DESCRIPTIONS_TABLE)
				result = true;
			else if (table.TableName.StartsWith(TEMP_DIFF_MOVEMENT_TABLE))
				result = true;

			return result;
		}
		/// <summary>
		/// Creates a filter for rows with rowType Total
		/// </summary>
		/// <returns>string expression for the filter</returns>
		public static string Filter_Total()
		{
			string strFilter = String.Format("{0} = {1}", ROW_TYPE_FIELD, (int)RowType.Total);
			return strFilter;
		}
		/// <summary>
		/// Creates a filter for rows with rowType Body
		/// </summary>
		/// <returns>string expression for the filter</returns>
		public static string Filter_Body()
		{
			string strFilter = String.Format("{0} = {1}", ROW_TYPE_FIELD, (int)RowType.Body);
			return strFilter;
		}
		/// <summary>
		/// Creates a filter for rows with rowType Total
		/// </summary>
		/// <returns>string expression for the filter</returns>
		public static string Filter_Header()
		{
			string strFilter = String.Format("{0} = {1}", ROW_TYPE_FIELD, (int)RowType.Header);
			return strFilter;
		}
		/// <summary>
		/// finds 1st row with rowType Total
		/// </summary>
		/// <returns>row or null if not found</returns>
		public static DataRow GetTotalRow(DataTable table)
		{
			DataRow row = null;
			DataView dv = new DataView(table);
			dv.RowFilter = TeaTabDS.Filter_Total();
			if (dv.Count > 0)
				row = dv[0].Row;
			return row;
		}
		/// <summary>
		/// finds 1st row with rowType Header
		/// </summary>
		/// <returns>row or null if not found</returns>
		public static DataRow GetHeaderRow(DataTable table)
		{
			DataRow row = null;
			DataView dv = new DataView(table);
			dv.RowFilter = TeaTabDS.Filter_Header();
			if (dv.Count > 0)
				row = dv[0].Row;
			return row;
		}
		/// <summary>
		/// Gets a row of type Body
		/// </summary>
		/// <param name="table">datatable</param>
		/// <param name="index">index of a row to get</param>
		/// <returns></returns>
		public static DataRow GetBodyRow(DataTable table, int index)
		{
			DataRow row = null;
			if (table.Columns.Contains(ROW_TYPE_FIELD))
			{
				DataRowView rowView = null;
				DataView dv = new DataView(table);
				dv.RowFilter = TeaTabDS.Filter_Body();
				if (index <= dv.Count)
					rowView = dv[index];
				if (rowView != null)
					row = rowView.Row;
			}
			else
			{
				if (index <= table.Rows.Count)
					row = table.Rows[index];
			}
			return row;
		}

		public static string GetIndexedName(string name, int index)
		{
			string str = name + "_" + index.ToString();
			return str;
		}

        #endregion Static Metods

		#region Private Metods
		/// <summary>
		/// Creates and adds to the dataset table CLOSING_TEMP_DIFF_TABLE
		/// </summary>
		/// <returns>created table</returns>
		private DataTable AddClosingTempDiffTable()
		{
			DataTable table = new DataTable(CLOSING_TEMP_DIFF_TABLE);
			table.Columns.Add(DESCRIPTION_FIELD, typeof(System.String));
			table.Columns.Add(REF_FIELD, typeof(System.String));
			table.Columns.Add(URL_FIELD, typeof(System.String));
			table.Columns.Add(CLOSING_BALANCE_FIELD, typeof(String));
			table.Columns.Add(TAX_BASE_FIELD, typeof(String));
			table.Columns.Add(DIFFERENCE_FIELD, typeof(String));
			table.Columns.Add(CATEGORY_FIELD, typeof(System.String));
			table.Columns.Add(ROW_TYPE_FIELD, typeof(System.Int16));

			this.Tables.Add(table);
			return table;
		}
		/// <summary>
		/// Creates and adds to the dataset a table of type TEMP_DIFF_MOVEMENT_TABLE
		/// </summary>
		/// <returns>created table</returns>
		private DataTable AddTempDiffMovementTable(int index)
		{
			string tableName = GetIndexedName(TEMP_DIFF_MOVEMENT_TABLE, index);
			DataTable table = CreateTempDiffMovementTable(tableName);
			this.Tables.Add(table);
			return table;
		}
		/// <summary>
		/// Creates table with a given name using TEMP_DIFF_MOVEMENT_TABLE
		/// </summary>
		/// <returns>created table</returns>
		private DataTable CreateTempDiffMovementTable(string name)
		{
			DataTable table = new DataTable(name);
			table.Columns.Add(DESCRIPTION_FIELD, typeof(System.String));
			table.Columns.Add(REF_FIELD, typeof(System.String));
			table.Columns.Add(OPENING_BALANCE_FIELD, typeof(String));
			table.Columns.Add(TAX_EXPENSE_FIELD, typeof(String));
			table.Columns.Add(EQUITY_FIELD, typeof(String));
			table.Columns.Add(OTHER_FIELD, typeof(String));
			table.Columns.Add(CLOSING_BALANCE_FIELD, typeof(String));
			table.Columns.Add(ROW_TYPE_FIELD, typeof(System.Int16));
			table.Columns.Add(URL_FIELD, typeof(System.String));
			table.Columns.Add(PRIMARYKEY_FIELD, typeof(System.String));
			BrokerManagedComponentDS.AddVersionStamp(table, typeof(TeaTabDS));
			return table;
		}
		/// <summary>
		/// Creates and adds to the dataset table MOVEMENT_DESCRIPTIONS_TABLE
		/// </summary>
		/// <returns>created table</returns>
		private DataTable AddMovementDescriptionsTable()
		{
			DataTable table = new DataTable(TEMP_DIFF_MOVEMENT_DESCRIPTIONS_TABLE);
			table.Columns.Add(MOVEMENT_TYPE_FIELD, typeof(System.Int32));
			table.Columns.Add(DESCRIPTION_FIELD, typeof(System.String));
			BrokerManagedComponentDS.AddVersionStamp(table, typeof(TeaTabDS));
			this.Tables.Add(table);
			return table;
		}
		#endregion Private Metods
	}
}
