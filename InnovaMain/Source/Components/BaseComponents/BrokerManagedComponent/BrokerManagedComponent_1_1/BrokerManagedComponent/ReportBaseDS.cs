using System;
using System.Data;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.WPDatasetBase;

namespace Oritax.TaxSimp.Calculation
{
	/// <summary>
	/// Summary description for ReportBase.
	/// </summary>
	public abstract class ReportBaseDS: WPDatasetBaseDS
	{
		// Variables
		string errorMessage;
		public static string REPORT_BREADCRUMB_TABLE				="BREADCRUMB_TABLE";
		public static string REPORT_BREADCRUMB_FIELD				="BREADCRUMB";

		// Messages Table
		public static string REPORT_MESSAGE_TABLE					="MESSAGES_TABLE";
		public static string REPORT_MESSAGE_TEXT					="MESSAGETEXT";
		public static string REPORT_MESSAGE_FLAG					="MESSAGEFLAG";
		
		public ReportBaseDS()
		{
			DataTable table;

			// The Bread Crumb Table
			table = new DataTable(REPORT_BREADCRUMB_TABLE);
			table.Columns.Add(REPORT_BREADCRUMB_FIELD, typeof(System.String));
			Tables.Add(table);

			// The message Table
			table = new DataTable(REPORT_MESSAGE_TABLE);
			table.Columns.Add(REPORT_MESSAGE_TEXT , typeof(System.String));
			table.Columns.Add(REPORT_MESSAGE_FLAG , typeof(System.String));
			Tables.Add(table);
 
		}
				
		// Error Message
		public string ErrorMessage
		{
			get{return errorMessage;}
			set{errorMessage=value;}
		}

		
		public string BreadCrumb
		{
			set
			{
				DataRow objRow = this.GetDataRow( );

				if ( value != null )
					objRow[ REPORT_BREADCRUMB_FIELD ] = value;
			}
			get
			{
				DataRow objRow = this.GetDataRow( );
				if ( objRow[ REPORT_BREADCRUMB_FIELD] != DBNull.Value )
					return (string)objRow[REPORT_BREADCRUMB_FIELD];
				else
					return null;
			}
		}
		
		// Message Table Function//
		 public void ReportMessage(string strReportFlag, string strReportMessageText)
		{
			// Add the Message to Table
			// If report Flag  is = to 1 then that means the Message Will be displayed.
			// Anything else Message Will not be Displayed


			DataRow objRow = this.ReportMessageRow();

			if ((strReportFlag != string.Empty) || strReportMessageText != string.Empty)
			{
				objRow[REPORT_MESSAGE_FLAG] = strReportFlag.ToString();
				objRow[REPORT_MESSAGE_TEXT] = strReportMessageText.ToString();
			}
			else
			{
				//
				objRow[REPORT_MESSAGE_FLAG] = " " ;
				objRow[REPORT_MESSAGE_TEXT] = " ";

			}


		}
		private DataRow ReportMessageRow( )
		{
			DataTable objBCTable = this.Tables[ REPORT_MESSAGE_TABLE  ];

			if ( objBCTable.Rows.Count > 0 )
				return objBCTable.Rows[ 0 ];
			else
			{
				DataRow objBCRow = objBCTable.NewRow( );
				objBCTable.Rows.Add( objBCRow );
				return objBCRow;
			}
		}

		private DataRow GetDataRow( )
		{
			DataTable objBCTable = this.Tables[ REPORT_BREADCRUMB_TABLE ];

			if ( objBCTable.Rows.Count > 0 )
				return objBCTable.Rows[ 0 ];
			else
			{
				DataRow objBCRow = objBCTable.NewRow( );
				objBCTable.Rows.Add( objBCRow );
				return objBCRow;
			}
		}

	}
}
