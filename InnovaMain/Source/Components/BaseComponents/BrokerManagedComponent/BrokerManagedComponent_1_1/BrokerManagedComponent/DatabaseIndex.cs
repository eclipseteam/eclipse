using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace Oritax.TaxSimp.Calculation
{
    /// <summary>
    /// Defines an index to be created on a database table
    /// </summary>
    public class DatabaseIndex
    {
        private string name;
        private bool unique;
        private bool clustered;
        private bool sortAscending;
        private DataColumn[] indexColumns;

        /// <summary>
        /// Constructs a new database index
        /// </summary>
        /// <param name="name">The name of the index</param>
        /// <param name="unique">If the index requires unique values in the column</param>
        /// <param name="clustered">If the index should be a clustered index</param>
        /// <param name="sortAscending">If the index should be sorted in ascending order, if false then it is sorted descending</param>
        /// <param name="indexColumns">The columns to include in the index</param>
        public DatabaseIndex(string name, bool unique, bool clustered, bool sortAscending, DataColumn[] indexColumns)
        {
            this.unique = unique;
            this.name = name;
            this.clustered = clustered;
            this.sortAscending = sortAscending;
            this.indexColumns = indexColumns;
        }

        /// <summary>
        /// The name of the index
        /// </summary>
        public string Name
        {
            get 
            {
                return name; 
            }
        }

        /// <summary>
        /// Indicates if the index is unique
        /// </summary>
        public bool Unique
        {
            get 
            { 
                return unique; 
            }
        }

        /// <summary>
        /// If the index is clustered
        /// </summary>
        public bool Clustered
        {
            get 
            { 
                return clustered; 
            }
        }

        /// <summary>
        /// If the index should be sorted in ascending
        /// order, if false then sorted in descending order
        /// </summary>
        public bool SortAscending
        {
            get 
            { 
                return sortAscending; 
            }
        }

        /// <summary>
        /// The columns that are in the index
        /// </summary>
        public DataColumn[] IndexColumns
        {
            get 
            { 
                return indexColumns; 
            }
        }
    }
}
