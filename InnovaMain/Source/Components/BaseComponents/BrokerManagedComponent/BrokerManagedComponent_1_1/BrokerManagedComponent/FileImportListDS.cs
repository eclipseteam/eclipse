using System;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using Oritax.TaxSimp.Calculation; 

namespace Oritax.TaxSimp.FileImport
{
	public class FileImportListDS : DataSet
	{
		public const string FILEIMPORTLISTTABLE		= "FILEIMPORTLIST";
		public const string FILEIMPORT_ID			= "ID";
		public const string FILEIMPORT_NAME			= "NAME";
		public const string FILEIMPORT_DATEIMPORTED = "DATEIMPORTED";
		public const string FILEIMPORT_IMPORTEDBY   = "IMPORTEDBY";
		public const string FILEIMPORT_ORIGINPATH   = "ORIGINPATH";
		public const string FILEIMPORT_DESCRIPTION  = "DESCRIPTION";
		public const string FILEIMPORT_BMCID_FIELD	= "BMCID";
		public const string FILEIMPORT_SUBBMCID_FIELD = "SUBBMCID";

		public FileImportListDS()
		{
			DataTable dt=new DataTable(FILEIMPORTLISTTABLE);
			dt.Columns.Add(FILEIMPORT_ID, typeof(Guid));
			dt.Columns.Add(FILEIMPORT_NAME, typeof(string));
			dt.Columns.Add(FILEIMPORT_DATEIMPORTED, typeof(DateTime));
			dt.Columns.Add(FILEIMPORT_IMPORTEDBY, typeof(string));
			dt.Columns.Add(FILEIMPORT_ORIGINPATH, typeof(string));
			dt.Columns.Add(FILEIMPORT_BMCID_FIELD, typeof(string));
			dt.Columns.Add(FILEIMPORT_SUBBMCID_FIELD, typeof(string));
			dt.Columns.Add(FILEIMPORT_DESCRIPTION, typeof(string));
			Tables.Add(dt);
		}
	}
}
