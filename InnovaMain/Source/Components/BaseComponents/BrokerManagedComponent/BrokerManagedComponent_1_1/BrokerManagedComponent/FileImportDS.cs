using System;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using Oritax.TaxSimp.Calculation;

namespace Oritax.TaxSimp.FileImport
{
    [Serializable]
    public class FileImportDS : DataSet
    {
        public const string FILEIMPORTTABLE = "FILEIMPORT";
        public const string FILEIMPORT_ID_FIELD = "ID";
        public const string FILEIMPORT_NAME_FIELD = "NAME";
        public const string FILEIMPORT_DATEIMPORTED_FIELD = "DATEIMPORTED";
        public const string FILEIMPORT_IMPORTEDBY_FIELD = "IMPORTEDBY";
        public const string FILEIMPORT_ORIGINPATH_FIELD = "ORIGINPATH";
        public const string FILEIMPORT_LINKED_FIELD = "IsLinked";
        public const string FILEIMPORT_ATTACHMENTTYPE_FIELD = "ATTACHMENTTYPE";

        public Guid BMCID = Guid.Empty;
        public Guid SUBBMCID = Guid.Empty;

        private Stream stream;
        public Stream Stream
        {
            get { return stream; }
            set { stream = value; }
        }

        /// <summary>
        /// Date and time the file was imported.
        /// </summary>
        public DateTime DateImported
        {
            get
            {
                return (DateTime)(Tables[FILEIMPORTTABLE].Rows.Count == 0 ? null : Tables[FILEIMPORTTABLE].Rows[0][FILEIMPORT_DATEIMPORTED_FIELD]);
            }
            set { Tables[FILEIMPORTTABLE].Rows[0][FILEIMPORT_DATEIMPORTED_FIELD] = value; }
        }

        /// <summary>
        /// Document Type
        /// </summary>
        public int DocumentType
        {
            get
            {
                return (int)(Tables[FILEIMPORTTABLE].Rows.Count == 0 ? null : Tables[FILEIMPORTTABLE].Rows[0][FILEIMPORT_ATTACHMENTTYPE_FIELD]);
            }
            set { Tables[FILEIMPORTTABLE].Rows[0][FILEIMPORT_ATTACHMENTTYPE_FIELD] = value; }
        }

        /// <summary>
        /// Attachement ID
        /// </summary>
        public Guid AttactmentID
        {
            get
            {
                Guid id;
                return (Guid)(Tables[FILEIMPORTTABLE].Rows.Count == 0 ? Guid.NewGuid() : (Guid.TryParse(Tables[FILEIMPORTTABLE].Rows[0][FILEIMPORT_ID_FIELD].ToString(), out id)) ? id : Guid.NewGuid());
            }
            set { Tables[FILEIMPORTTABLE].Rows[0][FILEIMPORT_ID_FIELD] = value; }
        }


        /// <summary>
        /// Date and time the file was imported.
        /// </summary>
        public string Description
        {
            get
            {
                return (Tables[FILEIMPORTTABLE].Rows.Count == 0 ? null : Tables[FILEIMPORTTABLE].Rows[0][FILEIMPORT_NAME_FIELD].ToString());
            }
            set { Tables[FILEIMPORTTABLE].Rows[0][FILEIMPORT_NAME_FIELD] = value; }
        }



        /// <summary>
        /// Username of user who imported the file
        /// </summary>
        public string ImportedBy
        {
            get
            {
                return Tables[FILEIMPORTTABLE].Rows.Count == 0 ? null : (string)Tables[FILEIMPORTTABLE].Rows[0][FILEIMPORT_IMPORTEDBY_FIELD];
            }
            set { Tables[FILEIMPORTTABLE].Rows[0][FILEIMPORT_IMPORTEDBY_FIELD] = value; }
        }

        /// <summary>
        /// Original path of imported file
        /// </summary>
        public string OriginPath
        {
            get
            {
                return Tables[FILEIMPORTTABLE].Rows.Count == 0 ? null : (string)Tables[FILEIMPORTTABLE].Rows[0][FILEIMPORT_ORIGINPATH_FIELD];
            }
            set { Tables[FILEIMPORTTABLE].Rows[0][FILEIMPORT_ORIGINPATH_FIELD] = value; }
        }

        /// <summary>
        /// Original path of imported file
        /// </summary>
        public bool Linked
        {
            get
            {
                return Tables[FILEIMPORTTABLE].Rows.Count == 0 ? false : (bool)Tables[FILEIMPORTTABLE].Rows[0][FILEIMPORT_LINKED_FIELD];
            }
            set { Tables[FILEIMPORTTABLE].Rows[0][FILEIMPORT_LINKED_FIELD] = value; }
        }

        private bool isMigratingData = false;
        public bool IsMigratingData
        {
            get { return isMigratingData; }
            set { isMigratingData = value; }
        }

        public FileImportDS()
        {
            DataTable dt = new DataTable(FILEIMPORTTABLE);
            dt.Columns.Add(FILEIMPORT_ID_FIELD, typeof(Guid));
            dt.Columns.Add(FILEIMPORT_DATEIMPORTED_FIELD, typeof(DateTime));
            dt.Columns.Add(FILEIMPORT_IMPORTEDBY_FIELD, typeof(string));
            dt.Columns.Add(FILEIMPORT_ORIGINPATH_FIELD, typeof(string));
            dt.Columns.Add(FILEIMPORT_NAME_FIELD, typeof(string));
            dt.Columns.Add(FILEIMPORT_LINKED_FIELD, typeof(bool));
            dt.Columns.Add(FILEIMPORT_ATTACHMENTTYPE_FIELD, typeof(int));

            dt.PrimaryKey = new DataColumn[] { dt.Columns[FILEIMPORT_ID_FIELD] };
            // version stamp this table
            BrokerManagedComponentDS.AddVersionStamp(dt, typeof(FileImportDS));

            Tables.Add(dt);
        }
    }
}
