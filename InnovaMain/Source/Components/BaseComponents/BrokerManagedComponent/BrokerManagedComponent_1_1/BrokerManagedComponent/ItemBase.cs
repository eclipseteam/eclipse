﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using Oritax.TaxSimp.Utilities;
using System.Security.Permissions;


namespace Oritax.TaxSimp.Calculation
{
    public class ItemBase : ISerializable
    {
        protected Guid key = Guid.NewGuid();
        [Key]
        public Guid PrimaryKey
        {
            get
            {
                return key;
            }
            set
            {
                key = value;
            }
        }
        public string Description {get;set;}
        public string Reference { get; set; }

        #region Constructors   
		public ItemBase()
		{
		}

		public ItemBase(SerializationInfo si, StreamingContext context)
		{
			key = Serialize.GetSerializedGuid(si, "Key");
		}
	
		//Only allow the .NET Serialization core code to call this function
		[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]
		public virtual void GetObjectData(SerializationInfo si, StreamingContext context)
		{
			Serialize.AddSerializedValue(si, "Key", key);
		} 
        #endregion Constructors    

    }
}
