﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Data.Caching;
using System.Data;
using Oritax.TaxSimp.VelocityCache;
using Oritax.TaxSimp.CalculationInterface;

namespace Oritax.TaxSimp.Calculation
{
    public abstract class BaseVelocityWriter
    {
        protected VelocityCacheString _DC;

        public string VelocityKeyName { get; set; }
        public string PrimaryTableName { get; set; }
        public DataSet PrimaryDS;

        public BaseVelocityWriter(VelocityCacheString dc)
        {
            this._DC = dc;
        }

        public BaseVelocityWriter(VelocityCacheString dc, string velocitykey, string primaryTableName, DataSet ds)
        {
            this._DC = dc;
            this.VelocityKeyName = velocitykey;
            this.PrimaryTableName = primaryTableName;
            this.PrimaryDS = ds;
        }

        public abstract void Save(IBrokerManagedComponent iBMC);
        //public abstract void Save(IBrokerManagedComponent iBMC, ITransactionVelocity trx);
        public abstract void Delete(IBrokerManagedComponent iBMC);

    }
}
