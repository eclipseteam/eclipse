﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Data.Caching;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.VelocityCache;
using System.Data;

namespace Oritax.TaxSimp.Calculation
{
    public class BrokerManagedVelocityWriter : BaseVelocityWriter
    {
        private const string CINSTANCES_PRIMARY_KEY_NAME = "CID";


        public BrokerManagedVelocityWriter(VelocityCacheString dc)
            : base(dc)
        {
        }

        public BrokerManagedVelocityWriter(VelocityCacheString dc, string velocitykey, string primaryTableName, DataSet primaryDS)
            : base(dc, velocitykey, primaryTableName, primaryDS)
        {
        }

        public override void Delete(IBrokerManagedComponent iBMC)
        {
            object obj = this._DC.GetObejct(this.VelocityKeyName);

            switch (this.PrimaryTableName)
            {
                case "CINSTANCES":
                    DeleteCInstances(iBMC, (List<CInstanceTable>)obj);
                    break;
                case "CMLOGICAL":
                    DeleteCMLogical(iBMC, (List<CMLogicalTable>)obj);
                    break;
                case "CMSCENARIO":
                    DeleteCMScenario(iBMC, (List<CMScenarioTable>)obj);
                    break;
                case "SCENARIO":
                    DeleteScenario(iBMC, (List<ScenarioTable>)obj);
                    break;
                //case "SUBSCRIPTIONS":
                //    DeleteSubscriptions(iBMC, (List<CMLogicalTable>)obj);
                //    break;
            }
        }

        //public override void Save(IBrokerManagedComponent iBMC, ITransactionVelocity trx)
        //{
        //    object obj = this._DC.GetObejct(this.VelocityKeyName);

        //    switch (this.PrimaryTableName)
        //    {
        //        case "CINSTANCES":
        //            SaveCInstances(iBMC, (List<CInstanceTable>)obj, trx);
        //            break;
        //        case "CMLOGICAL":
        //            SaveCMLogical(iBMC, (List<CMLogicalTable>)obj, trx);
        //            break;
        //        case "CMSCENARIO":
        //            SaveCMScenario(iBMC, (List<CMScenarioTable>)obj, trx);
        //            break;
        //        case "SCENARIO":
        //            SaveScenario(iBMC, (List<ScenarioTable>)obj, trx);
        //            break;
        //        //case "SUBSCRIPTIONS":
        //        //    SaveSubscriptions(iBMC, (List<CMLogicalTable>)obj);
        //        //    break;
        //    }
        //}


        public override void Save(IBrokerManagedComponent iBMC)
        {
            object obj = this._DC.GetObejct(this.VelocityKeyName);

            switch (this.PrimaryTableName)
            {
                case "CINSTANCES":
                    SaveCInstances(iBMC, (List<CInstanceTable>)obj);
                    break;
                case "CMLOGICAL":
                    SaveCMLogical(iBMC, (List<CMLogicalTable>)obj);
                    break;
                case "CMSCENARIO":
                    SaveCMScenario(iBMC, (List<CMScenarioTable>)obj);
                    break;
                case "SCENARIO":
                    SaveScenario(iBMC, (List<ScenarioTable>)obj);
                    break;
                //case "SUBSCRIPTIONS":
                //    SaveSubscriptions(iBMC, (List<CMLogicalTable>)obj);
                //    break;
            }
        }

        private void SaveScenario(IBrokerManagedComponent iBMC, List<ScenarioTable> list)
        {
            DataTable dt = this.PrimaryDS.Tables[this.PrimaryTableName];

            foreach (DataRow dr in dt.Rows)
            {
                ScenarioTable recordInVelocity = list.Find(d => d.CSID == (Guid)dr["CSID"]);
                if (recordInVelocity == null)
                {
                    ScenarioTable newRow = new ScenarioTable((Guid)dr["CSID"], (string)dr["NAME"], (int)dr["TYPE"],
                        (int)dr["STATUS"], (DateTime)dr["CREATED"],
                        (DateTime)dr["MODIFIED"], (bool)dr["LOCKED"]);

                    list.Add(newRow);
                }
                else
                {
                    if (
                        (string)dr["NAME"] != recordInVelocity.Name ||
                        (int)dr["TYPE"] != recordInVelocity.Type ||
                        (int)dr["STATUS"] != recordInVelocity.Status ||
                        (bool)dr["LOCKED"] != recordInVelocity.Locked
                        )
                    {
                        list.Remove(recordInVelocity);

                        ScenarioTable newRecord = new ScenarioTable((Guid)dr["CSID"], (string)dr["NAME"], (int)dr["TYPE"],
                            (int)dr["STATUS"], (DateTime)dr["CREATED"],
                            (DateTime)dr["MODIFIED"], (bool)dr["LOCKED"]);

                        list.Add(newRecord);

                    }
                }
            }

            this._DC.PutObject(this.VelocityKeyName, list);
        }

        //private void SaveScenario(IBrokerManagedComponent iBMC, List<ScenarioTable> list, ITransactionVelocity trx)
        //{
        //    DataTable dt = this.PrimaryDS.Tables[this.PrimaryTableName];

        //    foreach (DataRow dr in dt.Rows)
        //    {
        //        ScenarioTable recordInVelocity = list.Find(d => d.CSID == (Guid)dr["CSID"]);
        //        if (recordInVelocity == null)
        //        {
        //            ScenarioTable newRow = new ScenarioTable((Guid)dr["CSID"], (string)dr["NAME"], (int)dr["TYPE"],
        //                (int)dr["STATUS"], (DateTime)dr["CREATED"],
        //                (DateTime)dr["MODIFIED"], (bool)dr["LOCKED"]);

        //            list.Add(newRow);
        //        }
        //        else
        //        {
        //            if (
        //                (string)dr["NAME"] != recordInVelocity.Name ||
        //                (int)dr["TYPE"] != recordInVelocity.Type ||
        //                (int)dr["STATUS"] != recordInVelocity.Status ||
        //                (bool)dr["LOCKED"] != recordInVelocity.Locked
        //                )
        //            {
        //                list.Remove(recordInVelocity);

        //                ScenarioTable newRecord = new ScenarioTable((Guid)dr["CSID"], (string)dr["NAME"], (int)dr["TYPE"],
        //                    (int)dr["STATUS"], (DateTime)dr["CREATED"],
        //                    (DateTime)dr["MODIFIED"], (bool)dr["LOCKED"]);

        //                list.Add(newRecord);

        //            }
        //        }
        //    }

        //    //this._DC.PutObject(this.VelocityKeyName, list);
        //    this.ThroughToCommittableTRX(this.VelocityKeyName, list, trx);
        //}

        private void DeleteScenario(IBrokerManagedComponent iBMC, List<ScenarioTable> list)
        {
            DataTable dt = this.PrimaryDS.Tables[this.PrimaryTableName];

            foreach (DataRow dr in dt.Rows)
            {
                ScenarioTable recordInVelocity = list.Find(d => d.CSID == (Guid)dr["CSID"]);

                list.Remove(recordInVelocity);
            }

            this._DC.PutObject(this.VelocityKeyName, list);
        }



        private void SaveCMLogical(IBrokerManagedComponent iBMC, List<CMLogicalTable> list)
        {
            DataTable dt = this.PrimaryDS.Tables[this.PrimaryTableName];

            IEnumerable<DataRow> query =
                from cmLogical in dt.AsEnumerable()
                select cmLogical;

            IEnumerable<DataRow> queryOne =
                query.Where(p => p.Field<Guid>("CLID") == iBMC.CID);

            DataRow dr = queryOne.ElementAt<DataRow>(0);

            CMLogicalTable recordInVelocity = list.Find(d => d.CLID == iBMC.CID);
            if (recordInVelocity == null)
            {
                CMLogicalTable newRow = new CMLogicalTable(iBMC.CID,
                    (string)dr["NAME"], (Guid)dr["CURRCSID"], (Guid)dr["CTID"],
                    (Guid)dr["PLID"], dr["BSCONSOLIDATIONMODE"].ToString(),
                    dr["BSPERIODTYPE"].ToString(), dr["BSCONSOL"].ToString(), dr["BSOUTYPE"].ToString());

                list.Add(newRow);
                this._DC.PutObject(this.VelocityKeyName, list);
            }
            else
            {
                if (
                    (string)dr["NAME"] != recordInVelocity.Name ||
                    (Guid)dr["CURRCSID"] != recordInVelocity.CurrCSID ||
                    (Guid)dr["CTID"] != recordInVelocity.CTID ||
                    (Guid)dr["PLID"] != recordInVelocity.PLID ||
                    dr["BSCONSOLIDATIONMODE"].ToString() != recordInVelocity.BSConsolidationMode ||
                    dr["BSPERIODTYPE"].ToString() != recordInVelocity.BSPerIODType ||
                    dr["BSCONSOL"].ToString() != recordInVelocity.BSConSol ||
                    dr["BSOUTYPE"].ToString() != recordInVelocity.BSOuType
                    )
                {
                    list.Remove(recordInVelocity);

                    CMLogicalTable newRecord = new CMLogicalTable(iBMC.CID,
                        (string)dr["NAME"], (Guid)dr["CURRCSID"], (Guid)dr["CTID"],
                        (Guid)dr["PLID"], dr["BSCONSOLIDATIONMODE"].ToString(),
                        dr["BSPERIODTYPE"].ToString(), dr["BSCONSOL"].ToString(), dr["BSOUTYPE"].ToString());
                    list.Add(newRecord);

                    this._DC.PutObject(this.VelocityKeyName, list);
                }
            }
        }

        //private void SaveCMLogical(IBrokerManagedComponent iBMC, List<CMLogicalTable> list, ITransactionVelocity trx)
        //{
        //    DataTable dt = this.PrimaryDS.Tables[this.PrimaryTableName];

        //    IEnumerable<DataRow> query =
        //        from cmLogical in dt.AsEnumerable()
        //        select cmLogical;

        //    IEnumerable<DataRow> queryOne =
        //        query.Where(p => p.Field<Guid>("CLID") == iBMC.CID);

        //    DataRow dr = queryOne.ElementAt<DataRow>(0);

        //    CMLogicalTable recordInVelocity = list.Find(d => d.CLID == iBMC.CID);
        //    if (recordInVelocity == null)
        //    {
        //        CMLogicalTable newRow = new CMLogicalTable(iBMC.CID,
        //            (string)dr["NAME"], (Guid)dr["CURRCSID"], (Guid)dr["CTID"],
        //            (Guid)dr["PLID"], dr["BSCONSOLIDATIONMODE"].ToString(),
        //            dr["BSPERIODTYPE"].ToString(), dr["BSCONSOL"].ToString(), dr["BSOUTYPE"].ToString());

        //        list.Add(newRow);
        //        //this._DC.PutObject(this.VelocityKeyName, list);
        //        this.ThroughToCommittableTRX(this.VelocityKeyName, list, trx);
        //    }
        //    else
        //    {
        //        if (
        //            (string)dr["NAME"] != recordInVelocity.Name ||
        //            (Guid)dr["CURRCSID"] != recordInVelocity.CurrCSID ||
        //            (Guid)dr["CTID"] != recordInVelocity.CTID ||
        //            (Guid)dr["PLID"] != recordInVelocity.PLID ||
        //            dr["BSCONSOLIDATIONMODE"].ToString() != recordInVelocity.BSConsolidationMode ||
        //            dr["BSPERIODTYPE"].ToString() != recordInVelocity.BSPerIODType ||
        //            dr["BSCONSOL"].ToString() != recordInVelocity.BSConSol ||
        //            dr["BSOUTYPE"].ToString() != recordInVelocity.BSOuType
        //            )
        //        {
        //            list.Remove(recordInVelocity);

        //            CMLogicalTable newRecord = new CMLogicalTable(iBMC.CID,
        //                (string)dr["NAME"], (Guid)dr["CURRCSID"], (Guid)dr["CTID"],
        //                (Guid)dr["PLID"], dr["BSCONSOLIDATIONMODE"].ToString(),
        //                dr["BSPERIODTYPE"].ToString(), dr["BSCONSOL"].ToString(), dr["BSOUTYPE"].ToString());
        //            list.Add(newRecord);

        //            this.ThroughToCommittableTRX(this.VelocityKeyName, list, trx);
        //            //this._DC.PutObject(this.VelocityKeyName, list);
        //        }
        //    }
        //}

        private void DeleteCMScenario(IBrokerManagedComponent iBMC, List<CMScenarioTable> list)
        {
            DataTable dt = this.PrimaryDS.Tables[this.PrimaryTableName];

            IEnumerable<DataRow> query =
                from cmLogical in dt.AsEnumerable()
                select cmLogical;

            IEnumerable<DataRow> querySeveral =
                query.Where(p => p.Field<Guid>("CIID") == iBMC.CID || p.Field<Guid>("CLID") == iBMC.CID
                );

            foreach (DataRow dr in querySeveral)
            {
                CMScenarioTable recordInVelocity = list.Find(d =>
                    d.CLID == (Guid)dr["CLID"] &&
                    d.CSID == (Guid)dr["CSID"] &&
                    d.CIID == (Guid)dr["CIID"] &&
                    d.CTID == (Guid)dr["CTID"]
                    );

                list.Remove(recordInVelocity);
            }

            this._DC.PutObject(this.VelocityKeyName, list);
        }

        private void SaveCMScenario(IBrokerManagedComponent iBMC, List<CMScenarioTable> list)
        {
            DataTable dt = this.PrimaryDS.Tables[this.PrimaryTableName];

            IEnumerable<DataRow> query =
                from cmScenario in dt.AsEnumerable()
                select cmScenario;

            IEnumerable<DataRow> querySeveral =
                query.Where(p => p.Field<Guid>("CIID") == iBMC.CID || p.Field<Guid>("CLID") == iBMC.CID
                );

            foreach (DataRow dr in querySeveral)
            {
                CMScenarioTable recordInVelocity = list.Find(d =>
                    d.CLID == (Guid)dr["CLID"] &&
                    d.CSID == (Guid)dr["CSID"] &&
                    d.CIID == (Guid)dr["CIID"] &&
                    d.CTID == (Guid)dr["CTID"]
                    );

                if (recordInVelocity == null)
                {
                    CMScenarioTable newRow = new CMScenarioTable((Guid)dr["ID"], (Guid)dr["CLID"], (Guid)dr["CSID"], (Guid)dr["CIID"], (Guid)dr["CTID"]);

                    list.Add(newRow);
                }
            }

            this._DC.PutObject(this.VelocityKeyName, list);
        }

        //private void SaveCMScenario(IBrokerManagedComponent iBMC, List<CMScenarioTable> list, ITransactionVelocity trx)
        //{
        //    DataTable dt = this.PrimaryDS.Tables[this.PrimaryTableName];

        //    IEnumerable<DataRow> query =
        //        from cmScenario in dt.AsEnumerable()
        //        select cmScenario;

        //    IEnumerable<DataRow> querySeveral =
        //        query.Where(p => p.Field<Guid>("CIID") == iBMC.CID || p.Field<Guid>("CLID") == iBMC.CID
        //        );

        //    foreach (DataRow dr in querySeveral)
        //    {
        //        CMScenarioTable recordInVelocity = list.Find(d =>
        //            d.CLID == (Guid)dr["CLID"] &&
        //            d.CSID == (Guid)dr["CSID"] &&
        //            d.CIID == (Guid)dr["CIID"] &&
        //            d.CTID == (Guid)dr["CTID"]
        //            );

        //        if (recordInVelocity == null)
        //        {
        //            CMScenarioTable newRow = new CMScenarioTable((Guid)dr["ID"], (Guid)dr["CLID"], (Guid)dr["CSID"], (Guid)dr["CIID"], (Guid)dr["CTID"]);

        //            list.Add(newRow);
        //        }
        //    }

        //    //this._DC.PutObject(this.VelocityKeyName, list);
        //    this.ThroughToCommittableTRX(this.VelocityKeyName, list, trx);
        //}

        private void DeleteCMLogical(IBrokerManagedComponent iBMC, List<CMLogicalTable> list)
        {
            CMLogicalTable recordInVelocity = list.Find(d => d.CLID == iBMC.CID);

            list.Remove(recordInVelocity);
            this._DC.PutObject(this.VelocityKeyName, list);

        }


        private void DeleteCInstances(IBrokerManagedComponent iBMC, List<CInstanceTable> list)
        {
            CInstanceTable recordInVelocity = list.Find(d => d.CID == iBMC.CID);

            list.Remove(recordInVelocity);
            this._DC.PutObject(this.VelocityKeyName, list);

        }

        //private void SaveCInstances(IBrokerManagedComponent iBMC, List<CInstanceTable> list, ITransactionVelocity trx)
        //{
        //    DataTable dt = this.PrimaryDS.Tables[this.PrimaryTableName];

        //    IEnumerable<DataRow> query =
        //        from instance in dt.AsEnumerable()
        //        select instance;

        //    IEnumerable<DataRow> queryOne =
        //        query.Where(p => p.Field<Guid>(CINSTANCES_PRIMARY_KEY_NAME) == iBMC.CID);

        //    DataRow dr = queryOne.ElementAt<DataRow>(0);

        //    CInstanceTable recordInVelocity = list.Find(d => d.CID == iBMC.CID);
        //    if (recordInVelocity == null)
        //    {
        //        CInstanceTable newRow = new CInstanceTable(iBMC.CID, (Guid)dr["CTID"], (string)dr["NAME"], new Byte[] { });

        //        list.Add(newRow);

        //        this.ThroughToCommittableTRX(this.VelocityKeyName, list, trx);
        //        //this._DC.PutObject(this.VelocityKeyName, list);
        //    }
        //    else
        //    {
        //        if ((Guid)dr["CTID"] != recordInVelocity.CTID ||
        //            (string)dr["NAME"] != recordInVelocity.Name)
        //        {
        //            list.Remove(recordInVelocity);
        //            CInstanceTable newRecord = new CInstanceTable(iBMC.CID, (Guid)dr["CTID"], (string)dr["NAME"], new Byte[] { });
        //            list.Add(newRecord);

        //            this.ThroughToCommittableTRX(this.VelocityKeyName, list, trx);
        //            //this._DC.PutObject(this.VelocityKeyName, list);
        //        }
        //    }

        //}


        private void SaveCInstances(IBrokerManagedComponent iBMC, List<CInstanceTable> list)
        {
            DataTable dt = this.PrimaryDS.Tables[this.PrimaryTableName];

            IEnumerable<DataRow> query =
                from instance in dt.AsEnumerable()
                select instance;

            IEnumerable<DataRow> queryOne =
                query.Where(p => p.Field<Guid>(CINSTANCES_PRIMARY_KEY_NAME) == iBMC.CID);

            DataRow dr = queryOne.ElementAt<DataRow>(0);

            CInstanceTable recordInVelocity = list.Find(d => d.CID == iBMC.CID);
            if (recordInVelocity == null)
            {
                CInstanceTable newRow = new CInstanceTable(iBMC.CID, (Guid)dr["CTID"], (string)dr["NAME"], new Byte[] { });

                list.Add(newRow);
                this._DC.PutObject(this.VelocityKeyName, list);
            }
            else
            {
                if ((Guid)dr["CTID"] != recordInVelocity.CTID ||
                    (string)dr["NAME"] != recordInVelocity.Name)
                {
                    list.Remove(recordInVelocity);
                    CInstanceTable newRecord = new CInstanceTable(iBMC.CID, (Guid)dr["CTID"], (string)dr["NAME"], new Byte[] { });
                    list.Add(newRecord);

                    this._DC.PutObject(this.VelocityKeyName, list);
                }
            }

        }

        //private void ThroughToCommittableTRX(string key, object obj, ITransactionVelocity trx)
        //{
        //    VelocityRMBase velocityRM = new VelocityRMBase(key, obj);
        //    trx.EnlistRM(velocityRM);

        //}

    }
}
