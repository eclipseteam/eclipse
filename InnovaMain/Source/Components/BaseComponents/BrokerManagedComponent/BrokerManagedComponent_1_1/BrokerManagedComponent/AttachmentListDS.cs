using System;
using System.Data;
using System.IO;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.WPDatasetBase;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.Calculation
{
	public class AttachmentListDS : WPDatasetBaseDS
	{
		private bool workpaperIsLinkedAttachment		= false;
		public const string ATTACHMENTTABLE				= "ATTACHMENT";
		public const string ATTACHMENTID_FIELD			= "ID";
		public const string ATTACHEDBMCID_FIELD			= "ATTACHEDBMCID";
		public const string ATTACHMENTDATEIMPORTED_FIELD= "DATEIMPORTED";
		public const string ATTACHMENTIMPORTEDBY_FIELD  = "IMPORTEDBY";
		public const string ATTACHMENTORIGINPATH_FIELD  = "ORIGINPATH";
		public const string ATTACHMENTDESCRIPTION_FIELD = "DESCRIPTION";
		public const string ATTACHMENTDOWNLOADURL_FIELD = "FileDownLoadURL";
		public const string ATTACHMENTFILE_FIELD		= "ATTACHMENTFILE";
		public const string ATTACHMENTLINKED_FIELD		= "LINKED";
        public const string ATTACHMENTTYPE_FIELD = "ATTACHMENTTYPE";
        public const string ATTACHMENTTYPENAME_FIELD = "ATTACHMENTTYPENAME";
        private string fileDownloadURL = string.Empty;

        public string FileDownloadURL
        {
            get { return fileDownloadURL; }
            set { fileDownloadURL = value; }
        } 

        /// <summary>
		/// This propery is used during page load for link attachment tab. e.g. if workpaper is configured for link attachment then the check box will be displayed. 
		/// </summary>
		public bool WorkpaperIsLinkedAttachment
		{
			get
			{
				return this.workpaperIsLinkedAttachment;
			}
			set
			{
				this.workpaperIsLinkedAttachment = value; 
			}
		}

		public AttachmentListDS()
		{
			DataTable dt=new DataTable(ATTACHMENTTABLE);
			dt.Columns.Add(ATTACHMENTID_FIELD, typeof(string));
			dt.Columns.Add(ATTACHEDBMCID_FIELD, typeof(string));
			dt.Columns.Add(ATTACHMENTDATEIMPORTED_FIELD, typeof(DateTime));
			dt.Columns.Add(ATTACHMENTIMPORTEDBY_FIELD, typeof(string));
			dt.Columns.Add(ATTACHMENTORIGINPATH_FIELD, typeof(string));
			dt.Columns.Add(ATTACHMENTDESCRIPTION_FIELD, typeof(string));
			dt.Columns.Add( ATTACHMENTDOWNLOADURL_FIELD, typeof( string ) );
			dt.Columns.Add( ATTACHMENTFILE_FIELD, typeof( Stream ) );
			dt.Columns.Add( ATTACHMENTLINKED_FIELD, typeof( bool ) );
            dt.Columns.Add(ATTACHMENTTYPE_FIELD, typeof(AttachmentType));
            Tables.Add(dt);
		}
	}
}
