using System;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using Oritax.TaxSimp.Calculation; 

namespace Oritax.TaxSimp.FileImport
{
	[Serializable]
	public class FileImportDetailsDS : DataSet
	{
		public const string FILEIMPORTTABLE					= "FILEIMPORT";
		public const string FILEIMPORT_ID_FIELD				= "ID";
		public const string FILEIMPORT_NAME_FIELD			= "NAME";
		public const string FILEIMPORT_DESCRIPTION_FIELD	= "DESCRIPTION";
		public const string FILEIMPORT_DATEIMPORTED_FIELD	= "DATEIMPORTED";
		public const string FILEIMPORT_IMPORTEDBY_FIELD		= "IMPORTEDBY";
		public const string FILEIMPORT_ORIGINPATH_FIELD		= "ORIGINPATH";

		public Guid BMCID = Guid.Empty;
		public Guid SUBBMCID = Guid.Empty;

		public const string MIGRATIONDATATABLE				= "STREAMDATA";
		public const string MIGRATIONDATA_FIELD				= "DATA";

//		private Stream stream;
//		public Stream Stream
//		{
//			get { return stream; }
//			set { stream=value;	}
//		}

		/// <summary>
		/// Date and time the file was imported.
		/// </summary>
		public string Name
		{
			get 
			{ 
				return (string)(Tables[FILEIMPORTTABLE].Rows.Count==0?null:Tables[FILEIMPORTTABLE].Rows[0][FILEIMPORT_NAME_FIELD]); 
			}
			set	{ Tables[FILEIMPORTTABLE].Rows[0][FILEIMPORT_NAME_FIELD]=value; }
		}

		/// <summary>
		/// Date and time the file was imported.
		/// </summary>
		public DateTime DateImported
		{
			get 
			{ 
				return (DateTime)(Tables[FILEIMPORTTABLE].Rows.Count==0?null:Tables[FILEIMPORTTABLE].Rows[0][FILEIMPORT_DATEIMPORTED_FIELD]); 
			}
			set	{ Tables[FILEIMPORTTABLE].Rows[0][FILEIMPORT_DATEIMPORTED_FIELD]=value; }
		}

		/// <summary>
		/// Date and time the file was imported.
		/// </summary>
		public string Description
		{
			get 
			{ 
				return (Tables[FILEIMPORTTABLE].Rows.Count==0?null:Tables[FILEIMPORTTABLE].Rows[0][FILEIMPORT_DESCRIPTION_FIELD].ToString()); 
			}
			set	{ Tables[FILEIMPORTTABLE].Rows[0][FILEIMPORT_DESCRIPTION_FIELD]=value; }
		}



		/// <summary>
		/// Username of user who imported the file
		/// </summary>
		public string ImportedBy
		{
			get 
			{ 
				return Tables[FILEIMPORTTABLE].Rows.Count==0?null:(string)Tables[FILEIMPORTTABLE].Rows[0][FILEIMPORT_IMPORTEDBY_FIELD]; 
			}
			set { Tables[FILEIMPORTTABLE].Rows[0][FILEIMPORT_IMPORTEDBY_FIELD]=value; }
		}

		/// <summary>
		/// Original path of imported file
		/// </summary>
		public string OriginPath
		{
			get	
			{ 
				return Tables[FILEIMPORTTABLE].Rows.Count==0?null:(string)Tables[FILEIMPORTTABLE].Rows[0][FILEIMPORT_ORIGINPATH_FIELD]; 
			}
			set	{ Tables[FILEIMPORTTABLE].Rows[0][FILEIMPORT_ORIGINPATH_FIELD]=value;	}
		}

		private bool isMigratingData=false;
		public bool IsMigratingData
		{
			get { return isMigratingData; }
			set { isMigratingData=value; }
		}

		public FileImportDetailsDS()
		{
			DataTable dt=new DataTable(FILEIMPORTTABLE);
			dt.Columns.Add(FILEIMPORT_ID_FIELD, typeof(Guid));
			dt.Columns.Add(FILEIMPORT_DATEIMPORTED_FIELD, typeof(DateTime));
			dt.Columns.Add(FILEIMPORT_IMPORTEDBY_FIELD, typeof(string));
			dt.Columns.Add(FILEIMPORT_ORIGINPATH_FIELD, typeof(string));
			dt.Columns.Add(FILEIMPORT_DESCRIPTION_FIELD, typeof(string));
			dt.Columns.Add(FILEIMPORT_NAME_FIELD, typeof(string));

//			DataTable dt2 = new DataTable(ATTACHMENTTABLE);
//			dt2.Columns.Add(ATTACHMENT_ID_FIELD, typeof(Guid));
//			dt2.Columns.Add(ATTACHMENT_BMCID_FIELD, typeof(Guid));
//			dt2.Columns.Add(ATTACHMENT_FILEIMPORTID_FIELD, typeof(Guid));

			dt.PrimaryKey = new DataColumn[] { dt.Columns[FILEIMPORT_ID_FIELD] };
//			dt2.PrimaryKey = new DataColumn[] { dt2.Columns[ATTACHMENT_ID_FIELD] };

			// version stamp this table
			BrokerManagedComponentDS.AddVersionStamp(dt,typeof(FileImportDS));

			Tables.Add(dt);
//			Tables.Add(dt2);


			dt=new DataTable(MIGRATIONDATATABLE);
			dt.Columns.Add(MIGRATIONDATA_FIELD, typeof(byte[]));

				// version stamp this table
				BrokerManagedComponentDS.AddVersionStamp(dt,typeof(FileImportDS));

			Tables.Add(dt);
		}
	}
}
