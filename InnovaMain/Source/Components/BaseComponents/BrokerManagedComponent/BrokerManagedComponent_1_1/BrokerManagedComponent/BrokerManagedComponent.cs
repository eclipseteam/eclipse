using System;
using System.Data;
using System.Collections;
using System.Configuration;
using System.Security.Cryptography;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization.Formatters.Soap;
using System.Security.Permissions;
using System.Text;
using System.Reflection;
using System.Threading;
using System.Xml;
using System.ComponentModel;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.BusinessStructureUtilities;
using Oritax.TaxSimp.Security;
using Oritax.TaxSimp.Exceptions;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.FileImport;
using Oritax.TaxSimp.WPDatasetBase;
using System.Diagnostics;
using System.Data.SqlClient;
using System.Collections.Generic;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;


namespace Oritax.TaxSimp.Calculation
{
	/// <summary>
	/// BrokerManagedComponent is the base class for all objects that are handled by the CMBroker.
	/// </summary>
	[Serializable]
	public partial class BrokerManagedComponent : IBrokerManagedComponent//, ISerializable
	{	
		private const string fileImportTypeIDStr="B575C57C-15BC-4B02-85B5-DDC4C005A8E6";

		#region INSTALLATION PROPERITES
		public virtual string InstallTypeID{get{return "BA962890-2A7D-4cdd-A9B4-45FAC9B079BF";}}
		public virtual string InstallTypeName{get{return "BrokerManagedComponent";}}
		public virtual string InstallDisplayName{get{return "Broker Managed Component";}}
		public virtual string InstallCategory{get{return "Framework";}}
		public virtual int InstallApplicability{get{return (int)CMApplicability.All;}}
		public virtual string InstallAssemblyName{get{return "BrokerManagedComponentBase";}}
		public virtual string InstallClassName{get{return "BrokerManagedComponent";}}
		public virtual int InstallMajVersion{get{return Assembly.GetExecutingAssembly().GetName().Version.Major;}}
		public virtual int InstallMinVersion{get{return Assembly.GetExecutingAssembly().GetName().Version.Minor;}}
		public virtual int InstallRelease{get{return Assembly.GetExecutingAssembly().GetName().Version.Revision;}}
		public virtual string InstallPersisterAssembly{get{return "CalculationPersistSQL";}}
		public virtual string InstallPersisterClass{get{return "BMCPersister";}}
		public virtual bool InstallObsolete{get{return false;}}
        public bool inPlaceUpgradeInProgress = false; 
		#endregion

		#region INTERNAL CLASSES

		public enum ModifiedState
		{
			UNCHANGED			=	0,
			MODIFIED			=	1,
			UNKNOWN				=	3
		}

		public class AttacherCollectionSpecifier : CollectionSpecifier
		{
			public Guid AttachmentID=Guid.Empty;

			public AttacherCollectionSpecifier(Guid typeID,Guid attachmentID)
				: base(typeID)
			{
				AttachmentID=attachmentID;
			}
		}

		public class AttacherDT : DataTable
		{
			public const string ID_FIELD="ID";
			public const string BMCCID_FIELD="BMCCID";
			public const string ATTACHEDBMCID_FIELD="ATTACHEDBMCID";
			public const string IMPORTEDBY_FIELD="IMPORTEDBY";
			public const string DATEIMPORTED_FIELD="DATEIMPORTED";
			public const string ORIGINPATH_FIELD="ORIGINPATH";
			public const string DESCRIPTION_FIELD="DESCRIPTION";

			public AttacherDT()
			{
				this.Columns.Add(ID_FIELD, typeof(System.Guid));
				this.Columns.Add(BMCCID_FIELD, typeof(System.Guid));
				this.Columns.Add(ATTACHEDBMCID_FIELD, typeof(System.Guid));
				this.Columns.Add(IMPORTEDBY_FIELD, typeof(System.String));
				this.Columns.Add(DATEIMPORTED_FIELD, typeof(System.DateTime));
				this.Columns.Add(ORIGINPATH_FIELD, typeof(System.String));
				this.Columns.Add(DESCRIPTION_FIELD, typeof(System.String));
				DataColumn[] primaryKey={this.Columns[ID_FIELD]};
				this.PrimaryKey=primaryKey;
			}
		}

		[Serializable]
			public class ACLEntry : ISerializable
		{
			public Guid ID = Guid.NewGuid();
			public Guid PartyCID;
			public PrivilegeType Privilege;
			public PrivilegeOption PrivilegeValue;
			public bool Deleted;

			public ACLEntry(Guid partyCID,PrivilegeType privilege,PrivilegeOption privilegeValue)//,string partyName)
			{
				PartyCID=partyCID;
				Privilege=privilege;
				PrivilegeValue=privilegeValue;
				Deleted=false;
			}

			protected ACLEntry(SerializationInfo si, StreamingContext context)
			{
				ID=Serialize.GetSerializedGuid(si,"acl_ID");
				PartyCID=Serialize.GetSerializedGuid(si,"acl_PartyCID");
				Privilege=(PrivilegeType)Serialize.GetSerializedValue(si,"acl_Privilege",typeof(PrivilegeType),PrivilegeType.SysAddUsers);
				PrivilegeValue=(PrivilegeOption)Serialize.GetSerializedValue(si,"acl_PrivilegeValue",typeof(PrivilegeOption),PrivilegeOption.Deny);
				Deleted=Serialize.GetSerializedBool(si,"acl_Deleted");
			}
	
			//Only allow the .NET Serialization core code to call this function
			[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]
			public virtual void GetObjectData(SerializationInfo si, StreamingContext context)
			{
				Serialize.AddSerializedValue(si,"acl_ID", ID);
				Serialize.AddSerializedValue(si,"acl_PartyCID", PartyCID);
				Serialize.AddSerializedValue(si,"acl_Privilege", Privilege);
				Serialize.AddSerializedValue(si,"acl_PrivilegeValue", PrivilegeValue);
				Serialize.AddSerializedValue(si,"acl_Deleted", Deleted);
			}
		}

		[Serializable]
			public class ACLSet : SerializableHashtable, ISerializable
		{
			public ACLSet():base(){}

			protected ACLSet(SerializationInfo si, StreamingContext context)
				:base(si,context)
			{
			}
	
			//Only allow the .NET Serialization core code to call this function
			[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]
			public override void GetObjectData(SerializationInfo si, StreamingContext context)
			{
				base.GetObjectData(si,context);
			}

			public IEnumerable this[Guid partyCID]
			{
				get
				{
					return (SerializableList)base[partyCID];
				}
			}

			public void Add(Guid partyCID,ACLEntry aCLEntry)			
			{
				SerializableList arrayList=(SerializableList)base[partyCID];
				if(null!=arrayList)
					arrayList.Add(aCLEntry);
				else
				{
					arrayList=new SerializableList();
					arrayList.Add(aCLEntry);
					base[partyCID]=arrayList;
				}
			}

			void Add(Guid partyCID,PrivilegeType privilege,PrivilegeOption privilegeValue)
			{
				Add(partyCID,new ACLEntry(partyCID,privilege,privilegeValue));
			}

			public void CommitChanges()
			{
				foreach( DictionaryEntry objDictionaryEntry in this )
				{
					IEnumerable privilegeArray=(IEnumerable)objDictionaryEntry.Value;
					for(int iCount = ((SerializableList)privilegeArray).Count; iCount > 0; iCount--)
					{
						ACLEntry objACLEntry = (ACLEntry)((SerializableList)privilegeArray)[iCount - 1];

						if( objACLEntry.Deleted )
							((SerializableList)privilegeArray).RemoveAt(iCount - 1);
					}
				}
			}
		}

        [Serializable]
	    public class Attachment : ISerializable
		{
			public Guid ID;
			public Guid AttachedBMCID;
			public DateTime DateImported;
			public string ImportedBy;
			public string OriginPath;
			public string Description;
			public bool Linked;
			public bool Deleted;
            public AttachmentType AttachmentType = AttachmentType.Other;

            public Attachment(Guid iD, Guid attachedBMCID, DateTime dateImported, string importedBy, string originPath, string desription, bool linked, AttachmentType attachmentType)
			{
				ID=iD;
				AttachedBMCID=attachedBMCID;
				DateImported= dateImported;
				ImportedBy=importedBy;
				OriginPath=originPath;
				Description=desription;
				Linked=linked;
				Deleted=false;
                AttachmentType = attachmentType;
			}

			protected Attachment(SerializationInfo si, StreamingContext context)
			{
				ID=Serialize.GetSerializedGuid(si,"attachment_ID");
				AttachedBMCID=Serialize.GetSerializedGuid(si,"attachment_AttachedBMCID");
				DateImported=Serialize.GetSerializedDateTime(si,"attachment_DateImported");
				ImportedBy=Serialize.GetSerializedString(si,"attachment_ImportedBy");
				OriginPath=Serialize.GetSerializedString(si,"attachment_OriginPath");
				Description=Serialize.GetSerializedString(si,"attachment_Description");
				Linked=Serialize.GetSerializedBool(si,"attachment_Linked");
				Deleted=Serialize.GetSerializedBool(si,"attachment_Deleted");
                AttachmentType = (AttachmentType)Serialize.GetSerializedValue(si, "attachment_AttachmentType",typeof(AttachmentType),AttachmentType.Other);
            }

			//Only allow the .NET Serialization core code to call this function
			[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]
			public virtual void GetObjectData(SerializationInfo si, StreamingContext context)
			{
				Serialize.AddSerializedValue(si,"attachment_ID",ID);
				Serialize.AddSerializedValue(si,"attachment_AttachedBMCID",AttachedBMCID);
				Serialize.AddSerializedValue(si,"attachment_DateImported",DateImported);
				Serialize.AddSerializedValue(si,"attachment_ImportedBy",ImportedBy);
				Serialize.AddSerializedValue(si,"attachment_OriginPath",OriginPath);
				Serialize.AddSerializedValue(si,"attachment_Description",Description);
				Serialize.AddSerializedValue(si,"attachment_Linked",Linked);
				Serialize.AddSerializedValue(si,"attachment_Deleted",Deleted);
                Serialize.AddSerializedValue(si, "attachment_AttachmentType", AttachmentType);
			}

			internal void ExtractData( DataRow objDataRow )
			{
				objDataRow[ AttachmentListDS.ATTACHMENTID_FIELD ] = this.ID.ToString();
				objDataRow[ AttachmentListDS.ATTACHEDBMCID_FIELD ] = this.AttachedBMCID.ToString();
				objDataRow[ AttachmentListDS.ATTACHMENTIMPORTEDBY_FIELD ] = this.ImportedBy;
				objDataRow[ AttachmentListDS.ATTACHMENTDATEIMPORTED_FIELD ] = this.DateImported;
				objDataRow[ AttachmentListDS.ATTACHMENTDESCRIPTION_FIELD ] = this.Description;
				objDataRow[ AttachmentListDS.ATTACHMENTORIGINPATH_FIELD ] = this.OriginPath;
				objDataRow[ AttachmentListDS.ATTACHMENTLINKED_FIELD ] = this.Linked;
                objDataRow[AttachmentListDS.ATTACHMENTTYPE_FIELD] = this.AttachmentType;
                if(objDataRow.Table.Columns.Contains(AttachmentListDS.ATTACHMENTTYPE_FIELD))
                    objDataRow[AttachmentListDS.ATTACHMENTTYPENAME_FIELD] = Enumeration.GetDescription(this.AttachmentType);
			}

			internal void DeliverData( DataRow objDataRow )
			{
				this.ID = new Guid( (string) objDataRow[ AttachmentListDS.ATTACHMENTID_FIELD ] );
				this.AttachedBMCID = new Guid( (string) objDataRow[ AttachmentListDS.ATTACHEDBMCID_FIELD ] );
				this.ImportedBy = (string) objDataRow[AttachmentListDS.ATTACHMENTIMPORTEDBY_FIELD];
				this.DateImported = (DateTime)objDataRow[AttachmentListDS.ATTACHMENTDATEIMPORTED_FIELD];
				this.OriginPath = (string) objDataRow[AttachmentListDS.ATTACHMENTORIGINPATH_FIELD];
				this.Description = (string)objDataRow[AttachmentListDS.ATTACHMENTDESCRIPTION_FIELD];
				this.Linked= (bool)objDataRow[AttachmentListDS.ATTACHMENTLINKED_FIELD];
                this.AttachmentType = (AttachmentType)objDataRow[AttachmentListDS.ATTACHMENTTYPE_FIELD];
			}
		}

		[Serializable]
			public class Attachments : SerializableHashtable, ISerializable
		{
			public Attachments():base(){}

			protected Attachments(SerializationInfo si, StreamingContext context)
				:base(si,context)
			{
			}
	
			//Only allow the .NET Serialization core code to call this function
			[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]
			public override void GetObjectData(SerializationInfo si, StreamingContext context)
			{
				base.GetObjectData(si,context);
			}

			public Attachment this[Guid iD]
			{
				get{return (Attachment)base[iD];}
			}

            public void Add(Guid iD, Guid attachedBMCID, string importedBy, DateTime dateImported, string originPath, string description, bool linked, AttachmentType attachmentType)
			{
                Attachment attachment = new Attachment(iD, attachedBMCID, dateImported, importedBy, originPath, description, linked, attachmentType);
				this.Add(iD,attachment);
			}

			public void DeleteAll()
			{
				foreach(DictionaryEntry dictionaryEntry in this)
				{
					Attachment attachment=dictionaryEntry.Value as Attachment;
					attachment.Deleted=true;
				}
			}

			public void Delete(Guid iD)
			{
				Object attachment=this[iD];
				if(null!=attachment)
					((Attachment)attachment).Deleted=true;
			}

			public void AcceptChanges()
			{
				bool more;
				do
				{
					more=false;
					foreach(DictionaryEntry dictionaryEntry in this)
					{
						Attachment attachment=(Attachment)dictionaryEntry.Value;
						if(attachment.Deleted)
						{
							this.Remove(attachment.ID);
							more=true;
							break;
						}
					}
				}while(more);
			}
	
			internal void ExtractData( DataTable objAttachmentsTable )
			{
				foreach( DictionaryEntry objAttachmentDE in this )
				{
					Attachment objAttachment = (Attachment) objAttachmentDE.Value;

					if ( !objAttachment.Deleted )
					{
						DataRow objAttachmentRow = objAttachmentsTable.NewRow( );
						objAttachment.ExtractData( objAttachmentRow );
						objAttachmentsTable.Rows.Add( objAttachmentRow );
					}
				}
			}

			internal void DeliverData( DataTable objAttachmentsTable )
			{
				foreach ( DataRow objRow in objAttachmentsTable.Rows )
				{
					Guid objAttachmentID = new Guid( (string) objRow[ AttachmentListDS.ATTACHMENTID_FIELD ] );
					Attachment objAttachment = this[ objAttachmentID ];

					if ( objAttachment != null )
						objAttachment.DeliverData( objRow );
				}
			}
		}

		[Serializable]
			public class VersionEntry : ISerializable
		{
			public Guid ID;
			public Guid BMCID;
			public string Type;
			public string Version;

			public VersionEntry(Guid iD, Guid bMCID, string type, string version)
			{
				this.ID = iD;
				this.BMCID = bMCID;
				this.Type = type;
				this.Version = version;
			}

			protected VersionEntry(SerializationInfo si, StreamingContext context)
			{
				ID=Serialize.GetSerializedGuid(si,"ve_ID");
				BMCID=Serialize.GetSerializedGuid(si,"ve_BMCID");
				Type=Serialize.GetSerializedString(si,"ve_Type");
				Version=Serialize.GetSerializedString(si,"ve_Version");
			}
	
			//Only allow the .NET Serialization core code to call this function
			[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]
			public virtual void GetObjectData(SerializationInfo si, StreamingContext context)
			{
				Serialize.AddSerializedValue(si,"ve_ID",ID);
				Serialize.AddSerializedValue(si,"ve_BMCID",BMCID);
				Serialize.AddSerializedValue(si,"ve_Type",Type);
				Serialize.AddSerializedValue(si,"ve_Version", Version);
			}
		}


		[Serializable]
		public class VersionHistory : SerializableHashtable, ISerializable
		{
			public VersionHistory():base(){}

			protected VersionHistory(SerializationInfo si, StreamingContext context)
				:base(si,context)
			{
			}
	
			//Only allow the .NET Serialization core code to call this function
			[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]
			public override void GetObjectData(SerializationInfo si, StreamingContext context)
			{
				base.GetObjectData(si,context);
			}

			//			public string this[string type]
			//			{
			//				get
			//				{
			//					VersionEntry ve = (VersionEntry)base[type];
			//					if(ve != null)
			//						return ve.Version;
			//					else
			//						return "";
			//				}
			//			}

			public VersionEntry this[string type]
			{
				get{return (VersionEntry)base[type];}
			}


			public void Add(Guid iD, Guid bMCID, string type, string version)
			{
				VersionEntry entry = new VersionEntry(iD, bMCID, type, version);
				this.Add(type,entry);
			}
			public void Add(string type, string version)
			{
				VersionEntry entry = (VersionEntry)base[type];
				if(entry != null)
				{
					entry.Version = version;
				}
				else
				{
					this.Add(type, new VersionEntry(Guid.NewGuid(), Guid.Empty, type, version));
				}
			}
		}

		#endregion

		#region FIELD VARIABLES

		//USING CUSTOM SERIALIZATION.
		//Still mark the fields as  for your own infomation but this is ignored now by .NET
		//You must add your own serialzation code for each instance
		//See GetObjectData & Special Constructor
        
			private bool					transientInstance=false;
			private bool					modified=false;
		 private bool					migrationInProgress = false;
		 private IMigrationDataMemento	migrationDataMemento;

		protected Guid									cID;
        private string eclipseClientID;
        private string otherID  = string.Empty;
		protected Guid									typeID;
		protected string								name;	
		 private Token					persistedUpdateToken = Token.Empty;
    	 private Token					updateToken = Token.Empty;
		protected string								typeName;		// The type name of this CM.
		protected string								categoryName;	// The category name of this CM.                                                      
		private ACLSet									aCLSet;
		protected Attachments							attachments;
		protected NoteList								notes;
		protected VersionHistory						versionHistory;
        private CMStatus currentCmStatus;
        private StatusType statusType = StatusType.None;

      	private static bool relaxedBrokerConsistencyChecks = false;

		#endregion

		#region PROPERITES

        public string OtherID
        {
            get { return otherID; }
            set { otherID = value; }
        }

        public string EclipseClientID
        {
            get { return eclipseClientID; }
            set { eclipseClientID = value; }
        }

        public StatusType StatusType
        {
            get { return statusType; }
            set { statusType = value; }
        } 

        public CMStatus CMStatus
        {
            get
            {
                return this.currentCmStatus;
            }
            set
            {
                this.currentCmStatus = value;
            }
        }

		public CMTypeInfo TypeInfo 
		{
			get
			{
				return new CMTypeInfo(new Guid(InstallTypeID),
					InstallTypeName,
					InstallDisplayName,
					InstallCategory,
					InstallApplicability,
					InstallAssemblyName,
					InstallClassName,
					InstallMajVersion,
					InstallMinVersion,
					InstallRelease,
					InstallPersisterAssembly,
					InstallPersisterClass,
					InstallObsolete);
			}
		}

		public void CheckIfObjectIsInvolvedInCurrentTransaction()
		{
			if(this.Broker.Transaction.CanBeCollected(this.cID))
			{
				this.Broker.LogEvent(EventType.ErrorLogGenerated,Guid.Empty, "BrokerManagedComponent.CheckIfObjectIsInvolvedInCurrentTransaction, operation cannot complete as the object: " + this.cID + " is not involved in this transaction");
				
				if(!relaxedBrokerConsistencyChecks)
					throw new InvalidOperationException("BrokerManagedComponent.CheckIfObjectIsInvolvedInCurrentTransaction, operation cannot complete as the object: " + this.cID + " is not involved in this transaction");
			}
		}

		public Oritax.TaxSimp.CalculationInterface.IComponent ComponentInformation
		{
			get
			{
				IComponentManagement componentInformation = this.Broker.GetComponentDictionary();
				return componentInformation.GetComponent(this.ComponentVersionInformation.ComponentID);
			}
		}

		public IComponentVersion ComponentVersionInformation
		{
			get
			{
				IComponentManagement componentInformation = this.Broker.GetComponentDictionary();
				return componentInformation[this.TypeID];
			}
		}

		public string VersionString
		{
			get
			{
				return Global.GetUnderscoreVersionString(Assembly.GetAssembly( this.GetType( ) ));
			}
		}

		public bool Modified
		{
			get
			{
				return modified;
			}
			set
			{
				//Setting the modified state to false
				//This is usually done in the cache controller to signify
				//that the CM has been written to DB.
				if(value == false)
				{
					this.persistedUpdateToken = UpdateToken.Copy( );
					modified = false;
				}
					//Setting the modified state to true
					//This should only be done in PrimaryCMs and is for performance reasons only.
					//It was found in practice that a normal CalculateToken in a subscription
					//avalanche would impact performance by 100x
				else
				{
					this.CheckIfObjectIsInvolvedInCurrentTransaction();

					this.UpdateToken.IncrementIllegalToken( );
					modified = true;
				}
			}
		}

		public ICMBroker Owner
		{
			get
			{
				return this.Broker;
			}
		}

		public ICMBroker Broker
		{
			get
			{
				LocalDataStoreSlot brokerSlot = Thread.GetNamedDataSlot("Broker");
				object broker = Thread.GetData(brokerSlot);
				return (ICMBroker) broker;
			}
		}
		public virtual Guid CID{get{return cID;}set{cID=value;}}
		public virtual string Name{get{return name;}set{name=value;}}
		public Guid TypeID{get{return typeID;}set{typeID=value;}}
		/// <summary>
		/// Returns an unpopulated instance of the primary dataset derived type.
		/// This is a dataset that holds all the persistent data of the CM.  Default
		/// behavior is that none is defined.
		/// </summary>
		public virtual DataSet PrimaryDataSet{get{return new BrokerManagedComponentDS();}}
		public virtual DataSet MigrationPrimaryDataSet{get{return this.PrimaryDataSet;}}
		public string TypeName{get{return typeName;}set{typeName=value;}}
		public string CategoryName{get{return categoryName;}set{categoryName=value;}}

		public Token PersistedUpdateToken
		{
			get
			{
				if( persistedUpdateToken == null )
					persistedUpdateToken = Token.Empty;

				return persistedUpdateToken;
			}
		}

		public Token UpdateToken
		{
			get
			{
				if( updateToken == null )
					updateToken = Token.Empty;

				return updateToken;
			}
		}

		/// <summary>
		/// Only to be called by the broker to reset update token to persisted
		/// update token so that when it loads a bmc from disk it is not marked
		/// as modified
		/// </summary>
		public void ResetTokenToPersistedToken()
		{
			this.updateToken = this.persistedUpdateToken;
		}

		public bool MigrationInProgress
		{
			get
			{
				return migrationInProgress;
			}
			set
			{
				migrationInProgress = value;
			}
		}

        public bool InPlaceUpgradeInProgress
        {
            get
            {
                return inPlaceUpgradeInProgress;
            }
            set
            {
                inPlaceUpgradeInProgress = value;
            }
        }

		public int NotesCount
		{
			get
			{
				return this.notes.Count;
			}
		}

		public int AttachmentsCount
		{
			get
			{
				return this.attachments.Count;
			}
		}

		public IMigrationDataMemento MigrationDataMemento
		{
			get
			{
				return this.migrationDataMemento;
			}
		}
		#endregion

		#region CONSTRUCTORS
		static BrokerManagedComponent()
		{
			string relaxedConsistencyCheckRules = ConfigurationManager.AppSettings["NableRelaxBrokerConsistencyChecks"];
			
			if(relaxedConsistencyCheckRules == bool.TrueString)
				relaxedBrokerConsistencyChecks = true;
		}

		public BrokerManagedComponent()
		{
			aCLSet=new ACLSet();
			attachments=new Attachments();
			notes = new NoteList();
			versionHistory = new VersionHistory();
            this.currentCmStatus = CMStatus.NotStarted;
		}

		protected BrokerManagedComponent(SerializationInfo si, StreamingContext context)
		{
			cID=Serialize.GetSerializedGuid(si,"bmc_cID");
            eclipseClientID = Serialize.GetSerializedString(si, "bmc_eclipseClientID");
            otherID = Serialize.GetSerializedString(si, "bmc_otherID");
            statusType = (StatusType)Serialize.GetSerializedInt32(si, "bmc_statusType");
        	typeID=Serialize.GetSerializedGuid(si,"bmc_typeID");
			name=Serialize.GetSerializedString(si,"bmc_name");
			typeName=Serialize.GetSerializedString(si,"bmc_typeName");
			categoryName=Serialize.GetSerializedString(si,"bmc_categoryName");
			aCLSet=(ACLSet)Serialize.GetSerializedValue(si,"bmc_aCLSet",typeof(ACLSet),new ACLSet());
			attachments = (Attachments) Serialize.GetSerializedValue(si,"bmc_Attachments",typeof(Attachments),new Attachments());
			notes = (NoteList) Serialize.GetSerializedValue(si,"bmc_Notes",typeof(NoteList),new NoteList());
			versionHistory = (VersionHistory) Serialize.GetSerializedValue(si,"bmc_VersionHistory",typeof(VersionHistory),new VersionHistory());
			notes = new NoteList();
            currentCmStatus = (CMStatus)Serialize.GetSerializedValue(si, "bmc_cmStatus",typeof(CMStatus),CMStatus.NotStarted);
            
            transientInstance = Serialize.GetSerializedBool(si, "bmc_transientInstance");
            modified = Serialize.GetSerializedBool(si, "bmc_modified");
            migrationInProgress = Serialize.GetSerializedBool(si, "bmc_migrationInProgress");
            migrationDataMemento = (IMigrationDataMemento)Serialize.GetSerializedValue(si, "bmc_migrationDataMemento", typeof(IMigrationDataMemento), null);
            persistedUpdateToken = (Token)Serialize.GetSerializedValue(si, "bmc_persistedUpdateToken", typeof(Token), Token.Empty);
            updateToken = (Token)Serialize.GetSerializedValue(si, "bmc_updateToken", typeof(Token), Token.Empty);
            relaxedBrokerConsistencyChecks = Serialize.GetSerializedBool(si, "bmc_relaxedBrokerConsistencyChecks");

			if( this.aCLSet == null )
				this.aCLSet = new ACLSet();
		}
	
		//Only allow the .NET Serialization core code to call this function
		[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]
		public virtual void GetObjectData(SerializationInfo si, StreamingContext context)
		{
			Serialize.AddSerializedValue(si,"bmc_cID",cID);
            Serialize.AddSerializedValue(si, "bmc_cmStatus", currentCmStatus);
            Serialize.AddSerializedValue(si,"bmc_typeID",typeID);
			Serialize.AddSerializedValue(si,"bmc_name",name);
			Serialize.AddSerializedValue(si,"bmc_typeName",typeName);
			Serialize.AddSerializedValue(si,"bmc_categoryName",categoryName);
			Serialize.AddSerializedValue(si,"bmc_aCLSet",aCLSet);
			Serialize.AddSerializedValue(si, "bmc_Attachments", this.attachments);
			Serialize.AddSerializedValue(si, "bmc_Notes", this.notes);
			Serialize.AddSerializedValue(si,"bmc_VersionHistory", this.versionHistory);

            Serialize.AddSerializedValue(si, "bmc_transientInstance", this.transientInstance);
            Serialize.AddSerializedValue(si, "bmc_modified", this.modified);
            Serialize.AddSerializedValue(si, "bmc_migrationInProgress", this.migrationInProgress);
            Serialize.AddSerializedValue(si, "bmc_migrationDataMemento", this.migrationDataMemento);
            Serialize.AddSerializedValue(si, "bmc_persistedUpdateToken", this.persistedUpdateToken);
            Serialize.AddSerializedValue(si, "bmc_updateToken", this.updateToken);
            Serialize.AddSerializedValue(si, "bmc_relaxedBrokerConsistencyChecks", relaxedBrokerConsistencyChecks);
            Serialize.AddSerializedValue(si, "bmc_eclipseClientID", eclipseClientID);
            Serialize.AddSerializedValue(si, "bmc_statusType", statusType);
            Serialize.AddSerializedValue(si, "bmc_otherID", otherID);
        
            if (this.aCLSet == null)
				this.aCLSet = new ACLSet();
		}
		#endregion

		public virtual void Initialize(String name)
		{
			this.name=name;
		}

		public bool IsCustomSerialized()
		{
			if (this is ISerializable)
				return true;
			else
				return false;
		}

		public bool TransientInstance
		{
			get
			{
				return this.transientInstance;
			}
		}


		#region MIGRATION

        virtual public void UpdateDataSelectively(string xml)
        {
        }

		public string ExtractXML()
		{	
			//For breaking the process here.
			if(this.MigrationPrimaryDataSet == null)	//the cm has signified that it can't migrate
				return "NULL";

				migrationInProgress = true;

				DataSet ds = MigrationPrimaryDataSet;
				GetData(ds);

				//check to see if the extended properties are set and if not then throw
				//This is used as part of testing before deployment of the new components
				//to ensure that the developers have added version information to the datatables
				for(int i = 0; i< ds.Tables.Count; i++)
				{
					if(BrokerManagedComponentDS.GetFullName(ds.Tables[i]) == null)
						throw new MigrationException(string.Format("Table {0} does not have version information", ds.Tables[i].TableName));
				}
			//Add Version Stamp Function:

			for(int i = 0; i < ds.Tables.Count;i++)
				if(BrokerManagedComponentDS.GetInformationalVersion(ds.Tables[i]) == null) 
				{
					BrokerManagedComponentDS.AddVersionStamp(ds.Tables[i],typeof(BrokerManagedComponentDS));
				}
			migrationInProgress = false;
	
			StringBuilder sb = new StringBuilder();
			ds.WriteXml(new StringWriter(sb),XmlWriteMode.WriteSchema);
			return sb.ToString();
		} 

		public void ExportXMLToFile(string fileName)
		{
			if(MigrationPrimaryDataSet == null)	//the cm has signified that it can't migrate
			{
				return;
			}

			migrationInProgress = true;
        
			DataSet ds = MigrationPrimaryDataSet;
			GetData(ds);

			//check to see if the extended properties are set and if not then throw
			//This is used as part of testing before deployment of the new components
			//to ensure that the developers have added version information to the datatables
			for(int i = 0; i< ds.Tables.Count; i++)
			{
				if(BrokerManagedComponentDS.GetFullName(ds.Tables[i]) == null)
				{
					migrationInProgress = false;
					throw new MigrationException(string.Format("Table {0} does not have version information", ds.Tables[i].TableName));
				}
			}

			migrationInProgress = false;
	
			ds.WriteXml(fileName, XmlWriteMode.WriteSchema);
		}

		public string DeliverXML(string xml, bool verify, IMigrationDataMemento migrationDataMemento)
		{
			if(xml=="NULL") //the cm has signified that it can't migrate so don't try
				return "NULL";
			
			DataSet ds = new DataSet();
			try
			{
				migrationInProgress = true;
				this.migrationDataMemento = migrationDataMemento;

				ds.EnforceConstraints=false;		
				ds.ReadXml(new StringReader(xml));
				ds.CaseSensitive = true;
                //if (this.Name == "Tax effect accounting (AIFRS)")
                //{
                //    DataTable teaAggregation = ds.Tables["Aggregation"];
                //    DataTable teaAggregationValue = ds.Tables["TEA_AGGREGATIONVALUE"];

                //    DataSet teaAggregationDSOnly = new DataSet();
                //    teaAggregationDSOnly.Tables.Add(teaAggregation.Copy());

                //    DataSet teaAggregationValueDSOnly = new DataSet();
                //    teaAggregationValueDSOnly.Tables.Add(teaAggregationValue.Copy());

                //    DataSet teaAggregationDSOnlyBoth = new DataSet();
                //    teaAggregationDSOnlyBoth.Tables.Add(teaAggregationValue.Copy());
                //    teaAggregationDSOnlyBoth.Tables.Add(teaAggregation.Copy());

                //    foreach (DataRow aggValue in teaAggregationValue.Rows)
                //    {
                //        DataRow[] parentRows = teaAggregation.Select("ID ='" + aggValue["AggID"].ToString() + "'");
                //        if (parentRows.Length < 1)
                //            Debug.WriteLine(aggValue["AggID"].ToString());
                //    }
                //}

				ds.EnforceConstraints=true;

				//check to see if the extended properties are set and if not then throw
				//This is used as part of testing before deployment of the new components
				//to ensure that the developers have added version information to the datatables
				for(int i = 0; i< ds.Tables.Count; i++)
				{
					if(BrokerManagedComponentDS.GetFullName(ds.Tables[i]) == null)
					{
						if(BrokerManagedComponentDS.GetFullName(ds.Tables[i]) == null)
							throw new MigrationException(string.Format("Table {0} does not have version information", ds.Tables[i].TableName));
					}
				}
			
				
				// 1: The old version (2004.3.2) didn't had any versioning information for BMC and CalculationModule base tables. Thus during migration pahse
				// becasue the new release dont know about any version information of old release, the migration will not be done properly. For this reason this function
				// will manually add version inforamtion to table.  
				
				for(int i = 0; i < ds.Tables.Count;i++)
				{
					if(BrokerManagedComponentDS.GetInformationalVersion(ds.Tables[i]) == null) 
					{
						if (ds.Tables[i].TableName=="CINSTANCES") 
						{
							ds.Tables[i].ExtendedProperties.Add("InformationalVersion", "1.1.0.1");
						}
						else if (ds.Tables[i].TableName=="ACLENTRY") 
						{
							ds.Tables[i].ExtendedProperties.Add("InformationalVersion", "1.1.0.1");
						}	
						else if(ds.Tables[i].TableName=="CMTYPES") 
						{
							ds.Tables[i].ExtendedProperties.Add("InformationalVersion", "1.1.0.1");
						}
						else if (ds.Tables[i].TableName=="ATTACHMENT") 
						{
							ds.Tables[i].ExtendedProperties.Add("InformationalVersion", "1.1.0.1");
						}
						else if (ds.Tables[i].TableName=="VERSIONHISTORY") 
						{
							ds.Tables[i].ExtendedProperties.Add("InformationalVersion", "1.1.0.1");
						}
						else if(ds.Tables[i].TableName=="CALCULATIONCM") 
						{
							ds.Tables[i].ExtendedProperties.Add("InformationalVersion", "1.1.0.1");
						}
					}
				}
				versionHistory.Clear();
				try
				{
					SetData(MigrateDataSet(ds));
				}
				catch(Exception ex)
				{
					throw ex;
				}
		
				migrationInProgress = false;
				if(verify)
					return ExtractXML();
				else
					return "";
			}
			finally
			{
				this.migrationDataMemento = null;
			}
		}

		public void DeliverXMLFromFile(string fileName, IMigrationDataMemento migrationDataMemento)
		{
			DataSet ds = new DataSet();
			try
			{
				migrationInProgress = true;
				this.migrationDataMemento = migrationDataMemento;

				ds.EnforceConstraints=false;		
				ds.ReadXml(fileName);
				ds.CaseSensitive = true;
				ds.EnforceConstraints=true;

				//check to see if the extended properties are set and if not then throw
				//This is used as part of testing before deployment of the new components
				//to ensure that the developers have added version information to the datatables
				for(int i = 0; i< ds.Tables.Count; i++)
				{
					if(BrokerManagedComponentDS.GetFullName(ds.Tables[i]) == null)
					{
						if(BrokerManagedComponentDS.GetFullName(ds.Tables[i]) == null)
							throw new MigrationException(string.Format("Table {0} does not have version information", ds.Tables[i].TableName));
					}
				}
			
				
				// 1: The old version (2004.3.2) didn't had any versioning information for BMC and CalculationModule base tables. Thus during migration pahse
				// becasue the new release dont know about any version information of old release, the migration will not be done properly. For this reason this function
				// will manually add version inforamtion to table.  
				for(int i = 0; i < ds.Tables.Count;i++)
				{
					if(BrokerManagedComponentDS.GetInformationalVersion(ds.Tables[i]) == null) 
					{
						if (ds.Tables[i].TableName=="CINSTANCES") 
						{
							ds.Tables[i].ExtendedProperties.Add("InformationalVersion", "1.1.0.1");
						}
						else if (ds.Tables[i].TableName=="ACLENTRY") 
						{
							ds.Tables[i].ExtendedProperties.Add("InformationalVersion", "1.1.0.1");
						}	
						else if(ds.Tables[i].TableName=="CMTYPES") 
						{
							ds.Tables[i].ExtendedProperties.Add("InformationalVersion", "1.1.0.1");
						}
						else if (ds.Tables[i].TableName=="ATTACHMENT") 
						{
							ds.Tables[i].ExtendedProperties.Add("InformationalVersion", "1.1.0.1");
						}
						else if (ds.Tables[i].TableName=="VERSIONHISTORY") 
						{
							ds.Tables[i].ExtendedProperties.Add("InformationalVersion", "1.1.0.1");
						}
						else if(ds.Tables[i].TableName=="CALCULATIONCM") 
						{
							ds.Tables[i].ExtendedProperties.Add("InformationalVersion", "1.1.0.1");
						}
					}
				}
				versionHistory.Clear();
				try
				{
					SetData(MigrateDataSet(ds));
				}
				catch(Exception ex)
				{
					throw ex;
				}
		
				migrationInProgress = false;
			}
			finally
			{
				this.migrationDataMemento = null;
			}
		}

		/// <summary>
		/// Notify that the total Migration is completed
		/// </summary>
		public virtual void MigrationCompleted(IMigrationDataMemento migrationDataMemento)
		{
			this.MigrationCompleted();
		}

		/// <summary>
		/// Notify that the total Migration is completed
		/// </summary>
		public virtual void MigrationCompleted()
		{
			versionHistory.Remove(BrokerManagedComponentInstall.COMPONENTVERSION_IMPLEMENTATIONCLASS);

			//Set any primary cm's to contain an illegaltoken
			//but all other cm's mark as modified, then set token to empty
			//so that the token is calculated at persistance.
			if(this.CategoryName == "Primary" || this.CategoryName == "Accounting")
				this.Modified = true;
			else 
			{
				modified = true;
				this.updateToken = Token.Empty;
			}
		}

		/// <summary>
		///		Notify to resubscribe during a migration.
		/// </summary>
		public virtual void Resubscribe()
		{
		}

		/// <summary>
		/// Notify that the total Migration is completed
		/// </summary>
		/// <returns>Whether the BMCs migration finalisation was successful</returns>
		public virtual void DeletionOccurring()
		{
			DeletionOccurring(this.GetType());
		}

		/// <summary>
		/// Calls the OnMigrationCompleted() function for this BMC as cast to the specified type and all base types.
		/// </summary>
		/// <param name="bMCType">Type to which the BMC is to be cast</param>
		private void DeletionOccurring(Type bMCType)
		{
			MethodInfo method = bMCType.GetMethod("OnDeletionOccurring", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.DeclaredOnly | BindingFlags.ExactBinding,
				null,new Type[]{},null); 

			if (method != null)
			{
				method.Invoke(this,new object[]{});
			}

			Type baseBMCType=bMCType.BaseType;

			if(null!=baseBMCType)
			{
				DeletionOccurring(baseBMCType);
			}
		}

		/// <summary>
		/// notifies to the cm that it is being deleted
		/// </summary>
		protected void OnDeletionOccurring()
		{
            if(!this.inPlaceUpgradeInProgress)
    	        this.DeleteAllAttachments();
		}
		
		private void AddVersionHistory(DataSet ds)
		{
			DataTable table = ds.Tables[BrokerManagedComponentDS.VERSION_HISTORY_TABLE];
			foreach(DictionaryEntry dictionaryEntry in this.versionHistory)
			{
				VersionEntry version = dictionaryEntry.Value as VersionEntry;

				DataRow row = table.NewRow();
				row["ID"] = version.ID;
				row["BMCCID"] = this.CID;
				row["TYPE"] = version.Type;
				row["VERSION"] = version.Version;
				table.Rows.Add(row);
			}
		}
		
		public virtual DataSet MigrateDataSet(DataSet data)
		{	//Function to evaluate if the information coming is from 2004_3_2 and also to stamp the table with information version is it does not have one

			
			versionHistory.Add(BrokerManagedComponentInstall.COMPONENTVERSION_IMPLEMENTATIONCLASS, BrokerManagedComponentDS.GetDataSetVersion(data));
			DataSet convData = ConvertV_1_0_3_0toV_1_1_0_0DataSet(data, data);
			convData = ConvertV_1_1_0_toV_1_1_1_DataSet( convData );
			//convData = ConvertV_XtoV_YDataSet(data, convData);

			if(convData == null)
				return null;

			BrokerManagedComponentDS ds = new BrokerManagedComponentDS();

			//Merge in all of the component tables

			ds.Merge(convData.Tables[BrokerManagedComponentDS.CINSTANCES_TABLE]);
			ds.Merge(convData.Tables[BrokerManagedComponentDS.ACL_TABLE]);
			ds.Merge(convData.Tables[BrokerManagedComponentDS.ATTACHMENT_TABLE]);
			ds.Merge(convData.Tables[NoteListDS.NOTES_TABLE]);
			AddVersionHistory(ds);

			// Use the TypeID that was originally placed in the BMC when it was created
			// rather than the one extracted from the XML
			ds.Tables[BrokerManagedComponentDS.CINSTANCES_TABLE].Rows[0][BrokerManagedComponentDS.CTID_FIELD]=this.typeID;
			//			// Use the CID that was placed in the BMC when it was created rather than the one extracted from the XML
			//			ds.Tables["CINSTANCES"].Rows[0][BrokerManagedComponentDS.CID_FIELD]=this.cID;

			return ds;
		}
		
		/// <summary>
		/// Convert a V1.0.3.0 dataset to a V1.1.0.0 dataset
		/// V1.1.0.0 gained the notes table from  TaxTopicBase
		/// </summary>
		/// <param name="data">The orginal untampered dataset</param>
		/// <param name="convData">A converted dataset passed in from a previous conversion function</param>
		/// <returns>A converted dataset</returns>
		private DataSet ConvertV_1_0_3_0toV_1_1_0_0DataSet(DataSet data, DataSet convData)
		{
			if(convData == null)
				return null;

			string ver = BrokerManagedComponentDS.GetDataSetVersion(convData);

			//In complete contradiction to normal software development standards we DO want to use
			//Literals and not constants here. This hard codes the software to the old version.

			//if not the appropriate verion for correction then just return back
			if(ver != "1.0.3.0")
			{
				return convData;
			}

			//Build up the target dataset manually to avoid unintentional version change

			DataSet ds = new DataSet();

			DataTable table = new DataTable("CINSTANCES");
			table.Columns.Add("CID",typeof(System.Guid));
			table.Columns.Add("CTID",typeof(System.Guid));
			table.Columns.Add("NAME",typeof(System.String));
			table.Columns.Add("UPDATETOKEN",typeof(byte[]));
			//no longer have a typename column
			//no longer have a category name column
			table.PrimaryKey=new DataColumn[]{table.Columns["CID"]};
			ds.Tables.Add(table);

			//These were not used in v1.0.3.0
			table =  new DataTable("ACLENTRY");
			table.Columns.Add("ID",typeof(System.Guid));	//New Field in V1.1.0.0
			table.Columns.Add("BMCCID",typeof(System.Guid));
			table.Columns.Add("PARTYCID",typeof(System.Guid));
			table.Columns.Add("PRIVILEGETYPE",typeof(System.Int32));
			table.Columns.Add("PRIVILEGEVALUE",typeof(System.Int32));
			table.PrimaryKey=new DataColumn[]{table.Columns["ID"]};
			//The old primary key was a combination of BMCCID, PARTYCID & PRIVELIGETYPE
			ds.Tables.Add(table);

			//These were not used in V1.0.3.0
			table =  new DataTable("ATTACHMENT");
			table.Columns.Add("ID",typeof(System.Guid));
			table.Columns.Add("ATTACHEDBMCID",typeof(System.Guid));
			table.Columns.Add("BMCCID",typeof(System.Guid));
			table.Columns.Add("NAME",typeof(System.String));
			table.Columns.Add("DATEIMPORTED",typeof(System.DateTime));
			table.Columns.Add("IMPORTEDBY",typeof(System.String));
			table.Columns.Add("ORIGINPATH",typeof(System.String));
			table.Columns.Add("DESCRIPTION",typeof(System.String));
			table.PrimaryKey=new DataColumn[]{table.Columns["ID"]};
			ds.Tables.Add(table);

			table =  new DataTable("NOTES_TABLE");
			table.Columns.Add("ID" ,typeof(System.Guid));	//Named Key in 1.0.3.0
			table.Columns.Add("BMCCID",typeof(System.Guid)); //New column
			table.Columns.Add("NOTES_WORKPAPERID_FIELD" ,typeof(System.Guid)); //String in 1.0.3.0
			table.Columns.Add("NOTES_DESCRIPTION_FIELD" ,typeof(System.String));
			table.Columns.Add("NOTES_LASTUSERUPDATED_FIELD" ,typeof(System.String));
			table.Columns.Add("NOTES_LASTDATEUPDATED_FIELD" ,typeof(System.DateTime));
			table.Columns.Add("NOTES_ROLLFORWARD_FIELD" ,typeof(System.Boolean));
			table.Columns.Add("NOTES_CREATEDBY_FIELD" ,typeof(System.String));
			table.Columns.Add("NOTES_CREATEDDATE_FIELD" ,typeof(System.DateTime));
			table.PrimaryKey=new DataColumn[]{table.Columns["ID"]};
			ds.Tables.Add(table);

			ds.EnforceConstraints = false;

			ds.Merge(convData,true,MissingSchemaAction.Ignore);

			//Perform Conversions

			foreach(DataRow row in ds.Tables["ACLENTRY"].Rows)
			{
				row["ID"] = Guid.NewGuid();
				row.AcceptChanges();
			}

			Guid bmcID = (Guid)convData.Tables["CINSTANCES"].Rows[0]["CID"];

			foreach( DataRow row in ds.Tables["NOTES_TABLE"].Rows)
			{
				row["ID"] = Guid.NewGuid();
				row["BMCCID"] = bmcID;

				//wierdness
				//The merge above converts this column to a string because the source is a string
				//so we must convert to a Guid
				Guid wpID = new Guid(row["NOTES_WORKPAPERID_FIELD"].ToString());
				row["NOTES_WORKPAPERID_FIELD"] = wpID;
			
				row.AcceptChanges();
			}

			ds.EnforceConstraints = true;

			//stamp all version 1.1.0.0
			ver = "1.1.0.0";
			ds.Tables["CINSTANCES"].ExtendedProperties["InformationalVersion"] = ver;
			ds.Tables["ACLENTRY"].ExtendedProperties["InformationalVersion"] = ver;
			ds.Tables["ATTACHMENT"].ExtendedProperties["InformationalVersion"] = ver;
			ds.Tables["NOTES_TABLE"].ExtendedProperties["InformationalVersion"] = ver;

			return ds;
		}


		/// <summary>
		/// Convert a V1.1.0.0 dataset to a V1.1.1.1 dataset
		/// V1.1.1.1 gained the 'LINKED' column to the Attachments table
		/// </summary>
		/// <param name="convData">A converted dataset passed in from a previous conversion function</param>
		/// <returns>A converted dataset</returns>
		private DataSet ConvertV_1_1_0_toV_1_1_1_DataSet(DataSet convData)
		{
			
			if(convData == null)
				return null;

			string ver = BrokerManagedComponentDS.GetDataSetVersion(convData);

			//In complete contradiction to normal software development standards we DO want to use
			//Literals and not constants here. This hard codes the software to the old version.

			//if not the appropriate verion for correction then just return back
			//Note: Not interested in Revision version as this does not affect DataFormat
			if(!ver.StartsWith("1.1.0"))
			{
				return convData;
			}

			//Build up the target dataset manually to avoid unintentional version change

			DataSet ds = new DataSet();

			DataTable table = new DataTable("CINSTANCES");
			table.Columns.Add("CID",typeof(System.Guid));
			table.Columns.Add("CTID",typeof(System.Guid));
			table.Columns.Add("NAME",typeof(System.String));
			table.Columns.Add("UPDATETOKEN",typeof(byte[]));
			table.PrimaryKey=new DataColumn[]{table.Columns["CID"]};
			ds.Tables.Add(table);

			table =  new DataTable("ACLENTRY");
			table.Columns.Add("ID",typeof(System.Guid));	
			table.Columns.Add("BMCCID",typeof(System.Guid));
			table.Columns.Add("PARTYCID",typeof(System.Guid));
			table.Columns.Add("PRIVILEGETYPE",typeof(System.Int32));
			table.Columns.Add("PRIVILEGEVALUE",typeof(System.Int32));
			table.PrimaryKey=new DataColumn[]{table.Columns["ID"]};
			ds.Tables.Add(table);

			table =  new DataTable("ATTACHMENT");
			table.Columns.Add("ID",typeof(System.Guid));
			table.Columns.Add("ATTACHEDBMCID",typeof(System.Guid));
			table.Columns.Add("BMCCID",typeof(System.Guid));
			table.Columns.Add("NAME",typeof(System.String));
			table.Columns.Add("DATEIMPORTED",typeof(System.DateTime));
			table.Columns.Add("IMPORTEDBY",typeof(System.String));
			table.Columns.Add("ORIGINPATH",typeof(System.String));
			table.Columns.Add("DESCRIPTION",typeof(System.String));
			table.Columns.Add("LINKED",typeof(System.Boolean));  //New column for 1.1.1.1
			table.PrimaryKey=new DataColumn[]{table.Columns["ID"]};
			ds.Tables.Add(table);

			table =  new DataTable("NOTES_TABLE");
			table.Columns.Add("ID" ,typeof(System.Guid));	//Named Key in 1.0.3.0
			table.Columns.Add("PREVPERIODID",typeof(System.Guid));
			table.Columns.Add("BMCCID",typeof(System.Guid)); //New column
			table.Columns.Add("NOTES_WORKPAPERID_FIELD" ,typeof(System.Guid)); //String in 1.0.3.0
			table.Columns.Add("NOTES_DESCRIPTION_FIELD" ,typeof(System.String));
			table.Columns.Add("NOTES_LASTUSERUPDATED_FIELD" ,typeof(System.String));
			table.Columns.Add("NOTES_LASTDATEUPDATED_FIELD" ,typeof(System.DateTime));
			table.Columns.Add("NOTES_ROLLFORWARD_FIELD" ,typeof(System.Boolean));
			table.Columns.Add("NOTES_CREATEDBY_FIELD" ,typeof(System.String));
			table.Columns.Add("NOTES_CREATEDDATE_FIELD" ,typeof(System.DateTime));
			table.PrimaryKey=new DataColumn[]{table.Columns["ID"]};
			ds.Tables.Add(table);

			ds.EnforceConstraints = false;

			ds.Merge(convData,true,MissingSchemaAction.Ignore);

			//Perform Conversions

			//Rename tables to new names
			ds.Tables["CINSTANCES"].TableName = BrokerManagedComponentDS.CINSTANCES_TABLE;
			ds.Tables["ACLENTRY"].TableName = BrokerManagedComponentDS.ACL_TABLE;
			ds.Tables["ATTACHMENT"].TableName = BrokerManagedComponentDS.ATTACHMENT_TABLE;
			ds.Tables["NOTES_TABLE"].TableName = NoteListDS.NOTES_TABLE;

			//Set the default value for LINKED column of Attachments table to be false
			foreach(DataRow row in ds.Tables[BrokerManagedComponentDS.ATTACHMENT_TABLE].Rows)
				row["LINKED"] = false;

			ds.EnforceConstraints = true;

			//stamp all version 1.1.1.2
			ver = "1.1.1.2";
			ds.Tables[BrokerManagedComponentDS.CINSTANCES_TABLE].ExtendedProperties["InformationalVersion"] = ver;
			ds.Tables[BrokerManagedComponentDS.ACL_TABLE].ExtendedProperties["InformationalVersion"] = ver;
			ds.Tables[BrokerManagedComponentDS.ATTACHMENT_TABLE].ExtendedProperties["InformationalVersion"] = ver;
			ds.Tables[NoteListDS.NOTES_TABLE].ExtendedProperties["InformationalVersion"] = ver;

			return ds;
			
		}
		#endregion MIGRATION

        /// <summary>
        /// Save ClientData 
        /// </summary>
        /// <param name="cid"></param>
        /// <param name="ds"></param>
        public void SaveClientData(Guid cid, DataSet ds)
        {
            this.Broker.SaveOverride = true;
            IBrokerManagedComponent clientData = this.Broker.GetBMCInstance(cid);
            clientData.SetData(ds);
            this.Broker.SetComplete();
            this.Broker.SetStart(); 
        }

		/// <summary>
		///	Calculates an MD5 digital signature on a BrokerManagedComponent.
		/// </summary>
		/// <param name="Set">
		///	If set is true sets the signature to the object if different and then sets modified
		/// </param>
		/// <returns>
		///	The Digital Signature calculated
		/// </returns>
		public Token CalculateToken(bool Set)
		{
			Token newToken = this.updateToken.Copy( );
			
			/* SC - 30JUL03 - Modified Illegal to Update Token comparison. When the Modified flag is 
			 * manually set to True the UpdateToken was set to IllegalToken (0xFFFF..)- however this meant that 
			 * change of state (used to determine Multi-User modifications) was not captured.
			 * Now using a counter in the IllegalToken low order bytes to indicate state change
			 * see also: Modified.Set, CompareIllegalToken.
			 */
			if ( updateToken.IsIllegalToken || persistedUpdateToken.IsIllegalToken )
			{
				if ( Set )
				{
					this.CheckIfObjectIsInvolvedInCurrentTransaction();
					modified = true;
					updateToken.IncrementIllegalToken( );
				}

				return updateToken;
			}

            //MemoryStream memoryStream = new MemoryStream();
            //BinaryFormatter binaryFormatter = new BinaryFormatter();
            //binaryFormatter.Serialize(memoryStream, this);
            //newToken.CalculateToken(memoryStream.GetBuffer());
            //memoryStream.Close();

            newToken.CalculateToken();

			if(ConfigurationManager.AppSettings["LoggingToken"] == "*"
                || ConfigurationManager.AppSettings["LoggingToken"] == "ON"
                || ConfigurationManager.AppSettings["LoggingToken"] == GetType().FullName)
			{
				SaveBMCAsXML(@"c:\temp\" + this.GetType().FullName + Environment.TickCount.ToString()+ ".xml", this);
			}
			if(Set)
			{
				//if the new token is different from the current token then there has been a change
				if( newToken != updateToken )
				{
					//just check if the new token is the same as the persisted token
					//if it is then perhaps something has been removed in the object graph and the
					//object may have returned to its original state
					if( newToken != persistedUpdateToken )
					{
						this.CheckIfObjectIsInvolvedInCurrentTransaction();
						modified = true;
						this.updateToken = newToken;
					}
					else
					{
						//if the token is the same as that persisted then there is nothing to do :-)
						modified = false;
					}
				}
			}
			return newToken;
		}

        public virtual IList GetItemsList(int type)
        {
            return new SerializableList();
        }

        #region Service Methods

        public virtual DataSet ProcessDataSet(int type, DataSet data)
        {
            return new DataSet();
        }

        public virtual string GetAllDataStream(int type)
        {
            return string.Empty;
        }

        public virtual string GetDataStream(int type, string data)
        {
            return string.Empty;
        }

        public virtual string AddDataStream(int type, string data)
        {
            //The returning of ModifiedState was a bit interesting as it always returns modified or unknown
            // at the top level so makes it a bit useless further down the chain.
            string value = OnAddDataStream(type, data);

            if (!this.TransientInstance)
            {
                this.CheckIfObjectIsInvolvedInCurrentTransaction();
                CalculateToken(true);
            }
            return value;
        }

        public virtual string OnAddDataStream(int type, string data)
        {
            return string.Empty;
        }

        public virtual string UpdateDataStream(int type, string data)
        {
            //The returning of ModifiedState was a bit interesting as it always returns modified or unknown
            // at the top level so makes it a bit useless further down the chain.
            string value = OnUpdateDataStream(type, data);

            if (!this.TransientInstance)
            {
                this.CheckIfObjectIsInvolvedInCurrentTransaction();
                CalculateToken(true);
            }
            return value;
        }

        public virtual string OnUpdateDataStream(int type, string data)
        {
            return string.Empty;
        }

        public virtual string DeleteDataStream(int type, string data)
        {
            //The returning of ModifiedState was a bit interesting as it always returns modified or unknown
            // at the top level so makes it a bit useless further down the chain.
            string value = OnDeleteDataStream(type, data);

            if (!this.TransientInstance)
            {
                this.CheckIfObjectIsInvolvedInCurrentTransaction();
                CalculateToken(true);
            }
            return value;
        }

        public virtual string OnDeleteDataStream(int type, string data)
        {
            return string.Empty;
        }

        #endregion
		
        public static void SaveBMCAsXML(string Filename, IBrokerManagedComponent iBMC)
		{
			//stream the object graph down to a file in xml format.
			try
			{
				FileStream debugStream = new FileStream(Filename,FileMode.Create);
				SoapFormatter soapFormatter=new SoapFormatter();
				soapFormatter.Serialize(debugStream,iBMC);

				//				Handy code to uncomment to check the actual text being streamed
				//				debugStream.Position=0;
				//				StreamReader sr = new StreamReader(debugStream);
				//				string txt = sr.ReadToEnd();

				debugStream.Flush();					
				debugStream.Close();
			}
			catch
			{
			}
		}

		protected void OnGetData(AttacherListDS attacherListDS)
		{
			AttacherCollectionSpecifier attacherCollectionSpecifier=new AttacherCollectionSpecifier(this.TypeID,attacherListDS.AttachmentID);

			AttacherDT attacherTable=(AttacherDT)this.Broker.ListBMCCollection(attacherCollectionSpecifier);

			foreach(DataRow attacherRow1 in attacherTable.Rows)
			{
				IBrokerManagedComponent attacherBMC = null;
				Guid attacherBMCID = new Guid(attacherRow1[AttacherDT.BMCCID_FIELD].ToString());
				
				//If this is not the current instance, then attempt to get the BMC
				//so as to confirm that it is not deleted yet (ie is not null)
				//This is needed as if a RU is deleted a BMC will be returned in the 
				//collection which may be deleted in the current transaction.
				if(attacherBMCID != this.CID)
					attacherBMC = this.Broker.GetBMCInstance(attacherBMCID);
				
				if(attacherBMCID == this.CID || attacherBMC != null)
				{
					DataRow attacherRow2=attacherListDS.Tables[AttacherListDS.ATTACHERTABLE].NewRow();
					attacherRow2[AttacherListDS.ATTACHMENTID_FIELD]=attacherRow1[AttacherDT.ID_FIELD].ToString();
					attacherRow2[AttacherListDS.ATTACHMENTBMCID_FIELD]=attacherRow1[AttacherDT.BMCCID_FIELD].ToString();
					attacherRow2[AttacherListDS.ATTACHEDBMCID_FIELD]=attacherRow1[AttacherDT.ATTACHEDBMCID_FIELD].ToString();
					attacherRow2[AttacherListDS.ATTACHMENTIMPORTEDBY_FIELD]=attacherRow1[AttacherDT.IMPORTEDBY_FIELD];
					attacherRow2[AttacherListDS.ATTACHMENTDATEIMPORTED_FIELD]=attacherRow1[AttacherDT.DATEIMPORTED_FIELD];
					attacherRow2[AttacherListDS.ATTACHMENTORIGINPATH_FIELD]=attacherRow1[AttacherDT.ORIGINPATH_FIELD];
					attacherListDS.Tables[AttacherListDS.ATTACHERTABLE].Rows.Add(attacherRow2);
				}
			}
			attacherListDS.Merge(attacherTable);
		}

		protected void OnGetData(AttachmentListDS attachmentListDS)
		{
			attachmentListDS.WorkpaperIsLinkedAttachment = this.IsLinkAttachment;
			attachmentListDS.NotesCount = this.notes.Count;
			attachmentListDS.AttachmentsCount = this.attachments.Count;
			DataTable objAttachmentsTable = attachmentListDS.Tables[ AttachmentListDS.ATTACHMENTTABLE ];
            if(!objAttachmentsTable.Columns.Contains(AttachmentListDS.ATTACHMENTTYPENAME_FIELD))
                objAttachmentsTable.Columns.Add(AttachmentListDS.ATTACHMENTTYPENAME_FIELD);
			this.attachments.ExtractData( objAttachmentsTable );
			this.AddDownloadURLToAttachmentList( objAttachmentsTable );
		}

		protected void OnGetData(DSBMCInfo dSBMCInfo)
		{
			DataRow dataRow=dSBMCInfo.Tables[DSBMCInfo.CMLIST_TABLE].NewRow();
			dataRow[DSBMCInfo.CMID_FIELD]=cID;
			dataRow[DSBMCInfo.CMTYPENAME_FIELD]=typeName;
			dataRow[DSBMCInfo.CMCATEGORYNAME_FIELD]=categoryName;
			dataRow[DSBMCInfo.CMTYPEID_FIELD]=typeID;
			dataRow[DSBMCInfo.CMNAME_FIELD]=Name;
			dataRow[DSBMCInfo.CMUPDATETOKEN_FIELD] = UpdateToken.Hash;
			dSBMCInfo.Tables[DSBMCInfo.CMLIST_TABLE].Rows.Add(dataRow);
		}

		protected void OnGetData(NoteListDS noteListDS)
		{
			noteListDS.NotesCount = this.notes.Count;
			noteListDS.AttachmentsCount = this.attachments.Count;
			notes.ExtractData(noteListDS.Tables[NoteListDS.NOTES_TABLE]);
		}
		/// <summary>
		/// Should be overided by workpapers which have link attachment functionality.
		/// </summary>
		public virtual bool IsLinkAttachment
		{
			get
			{
				return false; 
			}
		}

		protected virtual void OnGetData(DataSet data)
		{
			if (data is BrokerManagedComponentDS)
			{
				BrokerManagedComponentDS brokerManagedComponentDS = data as BrokerManagedComponentDS;
				DataRow newInstanceRow=brokerManagedComponentDS.Tables[BrokerManagedComponentDS.CINSTANCES_TABLE].NewRow();
				newInstanceRow[BrokerManagedComponentDS.CID_FIELD]=this.CID;
				newInstanceRow[BrokerManagedComponentDS.CTID_FIELD]=this.TypeID;
				newInstanceRow[BrokerManagedComponentDS.NAME_FIELD]=this.Name;
                newInstanceRow[BrokerManagedComponentDS.ECLIPSECLIENTID_FIELD] = this.EclipseClientID;
                newInstanceRow[BrokerManagedComponentDS.STATUS_FIELD] = (int)this.StatusType;
                newInstanceRow[BrokerManagedComponentDS.OTHERID_FIELD] = this.otherID;
                newInstanceRow[BrokerManagedComponentDS.UPDATETOKEN_FIELD] = this.UpdateToken.Hash;
				brokerManagedComponentDS.Tables[BrokerManagedComponentDS.CINSTANCES_TABLE].Rows.Add(newInstanceRow);

				foreach(DictionaryEntry dictionaryEntry in this.aCLSet)
				{
					IEnumerable privilegeArray=(IEnumerable)dictionaryEntry.Value;
					for(int i=((SerializableList)privilegeArray).Count;i>0;i--)
					{
						ACLEntry aCLEntry = (ACLEntry)((SerializableList)privilegeArray)[i-1];
						DataRow newACLEntryRow=brokerManagedComponentDS.Tables[BrokerManagedComponentDS.ACL_TABLE].NewRow();
						newACLEntryRow[BrokerManagedComponentDS.ACLID_FIELD]=aCLEntry.ID;
						newACLEntryRow[BrokerManagedComponentDS.BMCCID_FIELD]=this.CID;
						newACLEntryRow[BrokerManagedComponentDS.PARTYCID_FIELD]=aCLEntry.PartyCID;
						newACLEntryRow[BrokerManagedComponentDS.PRIVILEGETYPE_FIELD]=aCLEntry.Privilege.ToInt();
						newACLEntryRow[BrokerManagedComponentDS.PRIVILEGEVALUE_FIELD]=aCLEntry.PrivilegeValue.ToInt();
						brokerManagedComponentDS.Tables[BrokerManagedComponentDS.ACL_TABLE].Rows.Add(newACLEntryRow);

						if(aCLEntry.Deleted)
						{
							newACLEntryRow.AcceptChanges();
							newACLEntryRow.Delete();
							((SerializableList)privilegeArray).RemoveAt(i-1);
						}
					}
				}

				foreach(DictionaryEntry dictionaryEntry in attachments)
				{
					Attachment attachment=dictionaryEntry.Value as Attachment;

					DataRow attachmentRow=brokerManagedComponentDS.Tables[BrokerManagedComponentDS.ATTACHMENT_TABLE].NewRow();
					attachmentRow[BrokerManagedComponentDS.ATTACHMENTID_FIELD]=attachment.ID;
					attachmentRow[BrokerManagedComponentDS.ATTACHMENTBMCID_FIELD]=this.CID;
					attachmentRow[BrokerManagedComponentDS.ATTACHEDBMCID_FIELD]=attachment.AttachedBMCID;
					attachmentRow[BrokerManagedComponentDS.ATTACHMENTIMPORTEDBY_FIELD]=attachment.ImportedBy;
					attachmentRow[BrokerManagedComponentDS.ATTACHMENTDATEIMPORTED_FIELD]=attachment.DateImported;
					attachmentRow[BrokerManagedComponentDS.ATTACHMENTORIGINPATH_FIELD]=attachment.OriginPath;
					attachmentRow[BrokerManagedComponentDS.ATTACHMENTDESCRIPTION_FIELD]=attachment.Description;
					attachmentRow[BrokerManagedComponentDS.ATTACHMENTLINKED_FIELD]=attachment.Linked;
					brokerManagedComponentDS.Tables[BrokerManagedComponentDS.ATTACHMENT_TABLE].Rows.Add(attachmentRow);

					if(attachment.Deleted)
					{
						attachmentRow.AcceptChanges();
						attachmentRow.Delete();
					}
				}

				attachments.AcceptChanges();

				foreach(DictionaryEntry dictionaryEntry in this.notes)
				{
					NoteItem noteItem=dictionaryEntry.Value as NoteItem;

					DataRow noteRow=brokerManagedComponentDS.Tables[NoteListDS.NOTES_TABLE].NewRow();
					noteRow[BrokerManagedComponentDS.NOTES_PRIMARYKEY_FIELD]=noteItem.PrimaryKey;
					noteRow[BrokerManagedComponentDS.NOTES_PREVPERIODKEY_FIELD]=noteItem.PrevPeriodKey;
					noteRow[BrokerManagedComponentDS.NOTEBMCID_FIELD]=this.CID;
					noteRow[NoteListDS.NOTES_WORKPAPERID_FIELD]=noteItem.WorkpaperID;
					noteRow[NoteListDS.NOTES_DESCRIPTION_FIELD]=noteItem.Description;
					noteRow[NoteListDS.NOTES_CREATEDBY_FIELD]=noteItem.CreatedBy;
					noteRow[NoteListDS.NOTES_CREATIONDATE_FIELD]=noteItem.CreationDate;
					noteRow[NoteListDS.NOTES_LASTUSERUPDATED_FIELD]=noteItem.LastUserUpdated;
					noteRow[NoteListDS.NOTES_LASTDATEUPDATED_FIELD]=noteItem.LastDateUpdated;
					noteRow[NoteListDS.NOTES_ROLLFORWARD_FIELD]=noteItem.RollForward;

					brokerManagedComponentDS.Tables[NoteListDS.NOTES_TABLE].Rows.Add(noteRow);
				}

				foreach(DictionaryEntry dictionaryEntry in this.versionHistory)
				{
					VersionEntry entry=dictionaryEntry.Value as VersionEntry;

					DataRow versionRow=brokerManagedComponentDS.Tables[BrokerManagedComponentDS.VERSION_HISTORY_TABLE].NewRow();
					versionRow[BrokerManagedComponentDS.VHID_FIELD]=entry.ID;
					versionRow[BrokerManagedComponentDS.VHBMCID_FIELD]=this.CID;
					versionRow[BrokerManagedComponentDS.VHTYPE_FIELD]=entry.Type;
					versionRow[BrokerManagedComponentDS.VHVERSION_FIELD]=entry.Version;

					brokerManagedComponentDS.Tables[BrokerManagedComponentDS.VERSION_HISTORY_TABLE].Rows.Add(versionRow);
				}
			}
			else if (data is AttachmentListDS)
			{
				this.OnGetData((AttachmentListDS)data);
			}
			else if (data is AttacherListDS)
			{
				this.OnGetData((AttacherListDS)data);
			}
			else if (data is DSBMCInfo)
			{
				this.OnGetData((DSBMCInfo)data);
			}
			else if (data is NoteListDS)
			{
				this.OnGetData((NoteListDS)data);
			}
			else if (data is PartyDS)
			{
				this.OnGetData((PartyDS)data);
			}
		}

		protected void OnGetData(PartyDS partyDS)
		{
			DataSet dsUsers = Broker.GetBMCDetailList( "DBUser_1_1" );
			DataTable dtUsers = dsUsers.Tables[ "UserDetail" ];

			if( this is ICalculationModule )
			{
				ICalculationModule objCM = (ICalculationModule) this;
				partyDS.ConfigureWorkpaper = objCM.ConfigureWPURL;
				ILogicalModule objLogical = this.Broker.GetLogicalCM( objCM.CLID );
				objLogical.GetData( partyDS );

				this.Broker.ReleaseBrokerManagedComponent(objLogical);
			}
			else
			{
				//Get a list of users so that teh first and surnames can be built up from this
				
				partyDS.Tables[ PartyDS.INCLUDEDPARTY_TABLE].Clear();
				foreach(DictionaryEntry dictionaryEntry in this.aCLSet)
				{
					IEnumerable privilegeArray=(IEnumerable)dictionaryEntry.Value;
					IEnumerator privilege=privilegeArray.GetEnumerator();

					while( privilege.MoveNext() )
					{
						ACLEntry aCLEntry= (ACLEntry) privilege.Current;
						if(!(aCLEntry.Deleted))
						{
                            // KGM: 29/04/2005 - make sure we have a row before it is used
                            DataRow[] drCollection = dtUsers.Select("CID = '"+aCLEntry.PartyCID.ToString()+"'");
                            if ( drCollection.Length > 0 )
                            {
                                DataRow partyRow=partyDS.Tables[PartyDS.INCLUDEDPARTY_TABLE].NewRow();
                                partyRow[PartyDS.PARTYCID_FIELD]=aCLEntry.PartyCID;
							
                                StringBuilder partyName = new StringBuilder();

                                
                                partyName.Append(drCollection[0]["Firstname"].ToString());
                                partyName.Append(" ");
                                partyName.Append(drCollection[0]["Lastname"].ToString());
                                partyName.Append(", ");
                                partyName.Append(drCollection[0]["Username"].ToString());
                                partyName.Append(", ");
                                if (drCollection[0]["UserType"].ToString() == "0")
                                    partyName.Append("Adviser");
                                else if (drCollection[0]["UserType"].ToString() == "1")
                                    partyName.Append("Accountant");
                                else if (drCollection[0]["UserType"].ToString() == "2")
                                    partyName.Append("IFA");
                                else if (drCollection[0]["UserType"].ToString() == "3")
                                    partyName.Append("DealerGroup");
                                else if (drCollection[0]["UserType"].ToString() == "4")
                                    partyName.Append("Innova");
                                else if (drCollection[0]["UserType"].ToString() == "5")
                                    partyName.Append("Client");
                                                                
                                partyRow[PartyDS.PARTYNAME_FIELD] = partyName.ToString();
                                partyDS.Tables[PartyDS.INCLUDEDPARTY_TABLE].Rows.Add(partyRow);
                            }
                        }
					}
				}
			}

			partyDS.Tables[ PartyDS.AVAILABLEPARTY_TABLE ].Clear();
			
			foreach( DataRow drUser in dtUsers.Rows )
			{
				DataRow partyRow=partyDS.Tables[PartyDS.AVAILABLEPARTY_TABLE].NewRow();
				partyRow[PartyDS.PARTYCID_FIELD]	=	drUser["CID"].ToString();
				
				StringBuilder partyName = new StringBuilder();
                partyName.Append(drUser["Firstname"].ToString());
                partyName.Append(" ");
                partyName.Append(drUser["Lastname"].ToString());
                partyName.Append(", ");
                partyName.Append(drUser["Username"].ToString());
                partyName.Append(", ");
                if (drUser["UserType"].ToString() == "0")
                    partyName.Append("Adviser");
                else if (drUser["UserType"].ToString() == "1")
                    partyName.Append("Accountant");
                else if (drUser["UserType"].ToString() == "2")
                    partyName.Append("IFA");
                else if (drUser["UserType"].ToString() == "3")
                    partyName.Append("DealerGroup");
                else if (drUser["UserType"].ToString() == "4")
                    partyName.Append("Innova");
                else if (drUser["UserType"].ToString() == "5")
                    partyName.Append("Client");
				
				partyRow[PartyDS.PARTYNAME_FIELD]	=	partyName.ToString(); 
				partyDS.Tables[PartyDS.AVAILABLEPARTY_TABLE].Rows.Add(partyRow);					
			}
		}

		private void AddDownloadURLToAttachmentList( DataTable objAttachmentsTable )
		{
			string strFileImportWorkpaper = this.Broker.GetVersionedWorkpaper( "FileImport.aspx", "FileImport" );

			foreach ( DataRow objRow in objAttachmentsTable.Rows )
			{
				objRow[ AttachmentListDS.ATTACHMENTDOWNLOADURL_FIELD ] = strFileImportWorkpaper + 
					"?FileID=" + objRow[AttachmentListDS.ATTACHEDBMCID_FIELD] + 
					"&CID=" + this.CID.ToString();
			}		
		}

        public string GetAttachmentURL(string attachmentBMCID)
        {
            string strFileImportWorkpaper = this.Broker.GetVersionedWorkpaper("FileImport.aspx", "FileImport");
            return strFileImportWorkpaper +
                "?FileID=" + attachmentBMCID +
                "&CID=" + this.CID.ToString();
        }

        public virtual void SetData(DataSet data)
        {
            //The returning of ModifiedState was a bit interesting as it always returns modified or unknown
            // at the top level so makes it a bit useless further down the chain.
            OnSetData(data);
            if (!this.TransientInstance)
            {

                this.CheckIfObjectIsInvolvedInCurrentTransaction();
                CalculateToken(true);
            }
        }

		public virtual void GetData(DataSet data)
		{
            this.OnGetData(data);

		}

		public virtual void DeliverData(DataSet data)
		{
			if(data!=null)
			{
				try
				{

					Type type = this.GetType();
					MethodInfo method = type.GetMethod("OnSetData", BindingFlags.NonPublic | BindingFlags.Instance,
						null, new Type[] {data.GetType()}, null); 
					if (method != null)
					{
						method.Invoke(this, new object[] {data});
					}
				}
				catch (Exception e)
				{
					if (e.InnerException != null)
					{
						throw e.InnerException;
					}
					else throw e;
				}
			}
		}

		public virtual void ExtractData(DataSet data)
		{
			OnGetData(data);
		}

		protected ModifiedState OnSetData(FileImportDS data)
		{
			ModifiedState modifiedState=ModifiedState.UNCHANGED;

			if(this.TypeName != "FileImport_1_1")
			{
				UploadAttachment(((FileImportDS) data));	

				modifiedState=ModifiedState.MODIFIED;
			}

			return modifiedState;
		}

		protected ModifiedState OnSetData(AttachmentListDS attachmentListDS)
		{
			bool linkedAttachmentsChanged = false;

			DataTable objAttachmentsTable = attachmentListDS.Tables[ AttachmentListDS.ATTACHMENTTABLE ];

			DataTable objAddedAttachments = objAttachmentsTable.GetChanges( DataRowState.Added );
			DataTable objDeletedAttachments = objAttachmentsTable.GetChanges( DataRowState.Deleted );
			DataTable objModifiedAttachments = objAttachmentsTable.GetChanges( DataRowState.Modified );

			if ( objModifiedAttachments != null )
				this.attachments.DeliverData( objModifiedAttachments );

			if ( objDeletedAttachments != null )
			{
				foreach( DataRow objRow in objDeletedAttachments.Rows )
				{
					objRow.RejectChanges();
					if((bool)objRow[AttachmentListDS.ATTACHMENTLINKED_FIELD])
						linkedAttachmentsChanged = true;
					objRow.Delete();
					// reject changes so that the row can be accessed to get
					// the primary key
					objRow.RejectChanges( );
					this.DeleteAttachment( objRow );
				}
			}

			if ( objAddedAttachments != null )
			{
				foreach( DataRow objRow in objAddedAttachments.Rows )
				{
					if((bool)objRow[AttachmentListDS.ATTACHMENTLINKED_FIELD])
						linkedAttachmentsChanged = true;

					FileImportDS objFileImport = new FileImportDS( );
					// need to add a row to the table - should be done by the constructor, but this dataset is
					// used somewhere else
					DataTable objFileImportTable = objFileImport.Tables[ FileImportDS.FILEIMPORTTABLE ];
					DataRow objFileImportRow = objFileImportTable.NewRow( );

					objFileImportRow[ FileImportDS.FILEIMPORT_ID_FIELD ] = objRow[ AttachmentListDS.ATTACHMENTID_FIELD ];
					objFileImportRow[ FileImportDS.FILEIMPORT_DATEIMPORTED_FIELD ] = objRow[ AttachmentListDS.ATTACHMENTDATEIMPORTED_FIELD ];
					objFileImportRow[ FileImportDS.FILEIMPORT_ORIGINPATH_FIELD ] = objRow[ AttachmentListDS.ATTACHMENTORIGINPATH_FIELD ];
					objFileImportRow[ FileImportDS.FILEIMPORT_IMPORTEDBY_FIELD ] = objRow[ AttachmentListDS.ATTACHMENTIMPORTEDBY_FIELD ];
					objFileImportRow[ FileImportDS.FILEIMPORT_NAME_FIELD ] = objRow[ AttachmentListDS.ATTACHMENTDESCRIPTION_FIELD ];
					objFileImportRow[ FileImportDS.FILEIMPORT_LINKED_FIELD ] = objRow[ AttachmentListDS.ATTACHMENTLINKED_FIELD ];
                    objFileImportRow[FileImportDS.FILEIMPORT_ATTACHMENTTYPE_FIELD] = objRow[AttachmentListDS.ATTACHMENTTYPE_FIELD];
					objFileImportTable.Rows.Add( objFileImportRow );

					objFileImport.Stream = (Stream) objRow[ AttachmentListDS.ATTACHMENTFILE_FIELD ];

                    Guid fileID = this.UploadAttachment(objFileImport);
                    attachmentListDS.FileDownloadURL = GetAttachmentURL(fileID.ToString()); 
				}
			}

			//If any linked attachments have either been deleted or uploaded, then
			//signal the BMC to handle any recalculations which need to be updated
			//as a result
			if(linkedAttachmentsChanged)
				LinkedAttachmentsUpdated();

			return ModifiedState.MODIFIED;
		}

		protected virtual void LinkedAttachmentsUpdated(){}
	
		protected virtual ModifiedState OnSetData(DataSet data)
		{
			if (data is BrokerManagedComponentDS)
			{
				DeliverPrimaryData((BrokerManagedComponentDS)data);
			}
			else if (data is PartyDS)
			{
				OnSetData((PartyDS)data);
			}
			else if (data is AttacherListDS)
			{
				OnSetData((AttacherListDS)data);
			}
			else if (data is AttachmentListDS)
			{
				OnSetData((AttachmentListDS)data);
			}
			else if (data is NoteListDS)
			{
				OnSetData((NoteListDS)data);
			}
			else if (data is FileImportDS)
			{
				OnSetData((FileImportDS)data);
			}

			return ModifiedState.UNKNOWN;
		}


		protected ModifiedState OnSetData(PartyDS partyDS)
		{
			if( this is ICalculationModule )
			{
				// Deliver the included users to the Logical Module.
				ICalculationModule objCM = (ICalculationModule) this;
				objCM.ScenarioLockOverridden = true;
				ILogicalModule objLogical = this.Broker.GetLogicalCM( objCM.CLID );
				objLogical.SetData(partyDS);

				this.Broker.ReleaseBrokerManagedComponent(objLogical);
			}
			else if ( this is ILogicalModule)
			{
				foreach(DataRow dataRow in partyDS.Tables[PartyDS.INCLUDEDPARTY_TABLE].Rows)
				{
					if(dataRow.RowState != DataRowState.Deleted)
					{
						Guid partyCID=new Guid(dataRow[PartyDS.PARTYCID_FIELD].ToString());

						if( !aCLSet.Contains( partyCID ) )
						{
							ACLEntry aCLEntry=new ACLEntry( partyCID, PrivilegeType.BMCFullAccess, PrivilegeOption.Allow );
							aCLSet.Add(partyCID,aCLEntry);
						}
					}
					else
					{
						dataRow.RejectChanges();
						Guid partyCID=new Guid(dataRow[PartyDS.PARTYCID_FIELD].ToString());
						if(aCLSet.Contains(partyCID))
							aCLSet.Remove(partyCID);
						dataRow.Delete();
					}
				}

				//Override scenario lock as if one scenario of a cm is locked
				//then still need to allow ability to add users.
				((ILogicalModule)this).ScenarioLockOverridden = true;
			}

			return ModifiedState.UNKNOWN;
		}

		protected ModifiedState OnSetData(NoteListDS noteListDS)
		{
			notes.Clear();
			notes.DeliverData(noteListDS.Tables[NoteListDS.NOTES_TABLE]); 

			return ModifiedState.MODIFIED;
		}

		public void DeliverPrimaryData(DataSet data)
		{
			BrokerManagedComponentDS brokerManagedComponentDS=data as BrokerManagedComponentDS;

			this.aCLSet.Clear();

			foreach(DataRow aCLEntryRow in brokerManagedComponentDS.Tables[BrokerManagedComponentDS.ACL_TABLE].Rows)
			{
				Guid aCLID=(Guid)aCLEntryRow[BrokerManagedComponentDS.ACLID_FIELD];
				Guid partyCID=(Guid)aCLEntryRow[BrokerManagedComponentDS.PARTYCID_FIELD];
				PrivilegeType privilegeType=new PrivilegeType((int)aCLEntryRow[BrokerManagedComponentDS.PRIVILEGETYPE_FIELD]);
				PrivilegeOption privilegeValue=new PrivilegeOption((int)aCLEntryRow[BrokerManagedComponentDS.PRIVILEGEVALUE_FIELD]);


				ACLEntry aCLEntry=new ACLEntry(partyCID,privilegeType,privilegeValue);
				aCLEntry.ID = aCLID;
				aCLSet.Add(partyCID,aCLEntry);
			}

			this.attachments.Clear();

			foreach(DataRow attachmentRow in brokerManagedComponentDS.Tables[BrokerManagedComponentDS.ATTACHMENT_TABLE].Rows)
			{
				Guid iD=(Guid)attachmentRow[AttachmentListDS.ATTACHMENTID_FIELD];
				Guid attachedBMCID=(Guid)attachmentRow[AttachmentListDS.ATTACHEDBMCID_FIELD];
				string importedBy=(string)attachmentRow[AttachmentListDS.ATTACHMENTIMPORTEDBY_FIELD];
				DateTime dateImported=(DateTime)attachmentRow[AttachmentListDS.ATTACHMENTDATEIMPORTED_FIELD];
				string originPath=(string)attachmentRow[AttachmentListDS.ATTACHMENTORIGINPATH_FIELD];
				string description=(string)attachmentRow[AttachmentListDS.ATTACHMENTDESCRIPTION_FIELD];
				bool linked=(bool)attachmentRow[AttachmentListDS.ATTACHMENTLINKED_FIELD];
                AttachmentType attachmentType= AttachmentType.Other;
                if(attachmentRow[AttachmentListDS.ATTACHMENTTYPE_FIELD].ToString() != string.Empty)
                    attachmentType = (AttachmentType)Enum.Parse(typeof(AttachmentType), attachmentRow[AttachmentListDS.ATTACHMENTTYPE_FIELD].ToString());
          
                attachments.Add(iD, attachedBMCID, importedBy, dateImported, originPath, description, linked, attachmentType);
			}

			this.notes.Clear();
			foreach(DataRow notesRow in brokerManagedComponentDS.Tables[NoteListDS.NOTES_TABLE].Rows)
			{
				Guid noteID=(Guid)notesRow[BrokerManagedComponentDS.NOTES_PRIMARYKEY_FIELD];
				Guid preNoteID=(Guid)notesRow[BrokerManagedComponentDS.NOTES_PREVPERIODKEY_FIELD];
				Guid workpaperID=(Guid)notesRow[NoteListDS.NOTES_WORKPAPERID_FIELD];
				string description=(string)notesRow[NoteListDS.NOTES_DESCRIPTION_FIELD];
				string createdBy=(string)notesRow[NoteListDS.NOTES_CREATEDBY_FIELD];
				DateTime createdDate=(DateTime)notesRow[NoteListDS.NOTES_CREATIONDATE_FIELD];
				string lastUserUpdated=(string)notesRow[NoteListDS.NOTES_LASTUSERUPDATED_FIELD];
				DateTime lastDateUpdated=(DateTime)notesRow[NoteListDS.NOTES_LASTDATEUPDATED_FIELD];
				bool rollForward=(bool)notesRow[NoteListDS.NOTES_ROLLFORWARD_FIELD];

				NoteItem noteItem=new NoteItem(noteID,preNoteID,workpaperID,description,
					createdBy,createdDate,lastUserUpdated,lastDateUpdated,rollForward);

				this.notes.Add(noteID,noteItem);
			}

			this.versionHistory.Clear();
			foreach(DataRow versionRow in brokerManagedComponentDS.Tables[BrokerManagedComponentDS.VERSION_HISTORY_TABLE].Rows)
			{
				Guid iD=(Guid)versionRow[BrokerManagedComponentDS.VHID_FIELD];
				Guid bMCID=(Guid)versionRow[BrokerManagedComponentDS.VHBMCID_FIELD];
				string type=(string)versionRow[BrokerManagedComponentDS.VHTYPE_FIELD];
				string version=(string)versionRow[BrokerManagedComponentDS.VHVERSION_FIELD];

				versionHistory.Add(iD,bMCID,type,version);
			}

			DataRow newInstanceRow=brokerManagedComponentDS.Tables[BrokerManagedComponentDS.CINSTANCES_TABLE].Rows[0];

			this.CID=(Guid)newInstanceRow[BrokerManagedComponentDS.CID_FIELD];
			this.TypeID=(Guid)newInstanceRow[BrokerManagedComponentDS.CTID_FIELD];
            
            if (newInstanceRow[BrokerManagedComponentDS.ECLIPSECLIENTID_FIELD] == System.DBNull.Value)
                this.EclipseClientID = string.Empty;
            else
                this.EclipseClientID = (string)newInstanceRow[BrokerManagedComponentDS.ECLIPSECLIENTID_FIELD];

            if (newInstanceRow[BrokerManagedComponentDS.OTHERID_FIELD] == System.DBNull.Value)
                this.otherID = string.Empty;
            else
                this.otherID = (string)newInstanceRow[BrokerManagedComponentDS.OTHERID_FIELD];

            if (newInstanceRow[BrokerManagedComponentDS.STATUS_FIELD] == System.DBNull.Value)
                this.StatusType = StatusType.None;
            else
                this.StatusType = (StatusType)newInstanceRow[BrokerManagedComponentDS.STATUS_FIELD];

			this.Name=newInstanceRow[BrokerManagedComponentDS.NAME_FIELD].ToString();

			/* Commented out because the CMTypes table no longer exists in the primary dataset
			DataRow newTypeRow=brokerManagedComponentDS.Tables[BrokerManagedComponentDS.CMTYPES_TABLE].Rows[0];
			this.TypeName=newTypeRow[BrokerManagedComponentDS.TYPENAME_FIELD].ToString();
			this.CategoryName=newTypeRow[BrokerManagedComponentDS.CATEGORYNAME_FIELD].ToString();
			*/

			//If the PersistedUpdateToken is zero then let deliver data update it.
			//This happens when the object has just been initialised and is being filled
			//by the persister.
			//Otherwise leave the peristedupdatetoken alone and ignore the datasets token.
			//Also if the object is signalled as modified then leave everything alone

			if(this.PersistedUpdateToken == Token.Empty && !modified)
			{
				if(!newInstanceRow.IsNull(BrokerManagedComponentDS.UPDATETOKEN_FIELD))
				{
					this.updateToken = new Token((byte[])newInstanceRow[BrokerManagedComponentDS.UPDATETOKEN_FIELD]);
					this.persistedUpdateToken = this.UpdateToken.Copy( );
					modified=false;
				}
				else
				{
					//if the field in the database is null then set the token and mark object as dirty
					CalculateToken(true);
				}
			}
		}

		public virtual void PrePersistActions(){}

		public virtual void PostPersistActions()
		{
			this.aCLSet.CommitChanges();
		}

		public virtual void PostSaveActions(IWorkPaper wp){}

		/// <summary>
		/// Returns the highest PrivilegeOption granted to the specified user.
		/// </summary>
		public virtual PrivilegeOption CheckPrivilege(PrivilegeType privilege,Guid userCID)
		{
			IEnumerable userACLs=aCLSet[userCID];
			PrivilegeOption resultPrivilegeValue=PrivilegeOption.Unspecified;
			IDBUser userBMC = (IDBUser)this.Broker.GetBMCInstance(userCID);

			if(userBMC.Administrator)
				return PrivilegeOption.Allow;
			
			// Check if Security is active.
			if( this.Broker.SecuritySetting == 1 )
			{
				if( userACLs != null )
				{
					foreach(ACLEntry aCLEntry in userACLs)
					{
						if(aCLEntry.Privilege==privilege && aCLEntry.PrivilegeValue>resultPrivilegeValue)
							resultPrivilegeValue=aCLEntry.PrivilegeValue;
					}
				}
			}
			else
			{
				// If security is inactive, then allow the user to access the feature.
				resultPrivilegeValue = PrivilegeOption.Allow;
			}

			return( resultPrivilegeValue );
		}


		/// <summary>
		/// This member function is required to perform all of the fixups necessary to convert an in-memory BMC
		/// instance into a new instance which is a copy of the old one.  Examples of fix-up action are replacing the CID
		/// with a new Guid and replacing IDs of owned subsidiary objects in a tree with new Guids.  The purpose of this
		/// would be to ensure that new rows representing owned subsidiary objects will be created by the persister.
		/// </summary>
		public virtual void OnCopyInstance()
		{
			this.CheckIfObjectIsInvolvedInCurrentTransaction();

			// Create a new CID
			Guid oldCID=this.CID;
			this.CID=Guid.NewGuid();
			// Register the change of ID with the transaction broker so that the cache can be updated
			this.Broker.RegisterIDChange(oldCID,this.CID);

			Attachment[] attachmentsTemp=new Attachment[this.attachments.Count];

			int i=0;
			foreach(DictionaryEntry dictionaryEntry in attachments)
			{
				Attachment attachment=dictionaryEntry.Value as Attachment;

				attachment.ID=Guid.NewGuid();
				attachmentsTemp[i++]=attachment;
			}

			attachments.Clear();

			foreach(Attachment attachmentTemp in attachmentsTemp)
			{
				attachments.Add(attachmentTemp.ID,attachmentTemp);
			}

			this.notes.Diverge();

			foreach(DictionaryEntry dictionaryEntry in this.aCLSet)
			{
				SerializableList aCLList = (SerializableList)dictionaryEntry.Value;
				foreach(ACLEntry aCLEntry in aCLList)
					aCLEntry.ID = Guid.NewGuid();
			}

			ArrayList versionsTemp = new ArrayList();

			foreach(DictionaryEntry dictionaryEntry in versionHistory)
			{
				VersionEntry versionEntry = dictionaryEntry.Value as VersionEntry;
				VersionEntry tempVersionEntry = new VersionEntry(Guid.NewGuid(), versionEntry.BMCID, versionEntry.Type, versionEntry.Version);
				versionsTemp.Add(tempVersionEntry);
			}

			versionHistory.Clear();

			foreach(VersionEntry tempVersionEntry in versionsTemp)
			{
				versionHistory.Add(tempVersionEntry.Type, tempVersionEntry);
			}
		}

		private void DeleteAllAttachments()
		{
			foreach(DictionaryEntry dictionaryEntry in attachments)
			{
				BrokerManagedComponent.Attachment attachment=dictionaryEntry.Value as BrokerManagedComponent.Attachment;

				RemoveOrphanFileImports(attachment);
			}

			attachments.DeleteAll();
		}

		/// <summary>
		/// Delete a File Upload SM
		/// </summary>
		/// <param name="objAttachmentRow"></param>
		private void DeleteAttachment( DataRow objAttachmentRow )
		{
			Guid objAttachmentID = new Guid( (string) objAttachmentRow[ AttachmentListDS.ATTACHMENTID_FIELD ] );
			Attachment attachment = attachments[ objAttachmentID ];

			if(null!=attachment)
			{
				this.attachments.Delete( objAttachmentID );
				RemoveOrphanFileImports(attachment);
			}

			this.attachments.Delete( objAttachmentID );
		}
		
		void RemoveOrphanFileImports(Attachment attachment)
		{
			AttacherListDS attachmentListDS=new AttacherListDS();
			attachmentListDS.AttachmentID=attachment.AttachedBMCID;

			IBrokerManagedComponent attachmentBMC=this.Broker.GetBMCInstance(attachment.AttachedBMCID);
			if(null!=attachmentBMC)	// Check that it still exists
			{
				attachmentBMC.GetData(attachmentListDS);

				int attacherCount=attachmentListDS.Tables[AttacherListDS.ATTACHERTABLE].Rows.Count;

				if(attacherCount<=1)
				{
					if(this is ICalculationModule)
					{
						ICalculationModule iCM=this as ICalculationModule;
						ILogicalModule iLM=this.Broker.GetLogicalCM(iCM.CLID);

						if(!iLM.IsShared(iCM.CSID))
							this.Broker.DeleteCMInstance(attachment.AttachedBMCID);

						this.Broker.ReleaseBrokerManagedComponent(iLM);
					}
					else
					{
						this.Broker.DeleteCMInstance(attachment.AttachedBMCID);
					}
				}
			}		
		}

		/// <summary>
		/// Create A file upload SM and add the attachment to the CM
		/// </summary>
		/// <param name="oFileImportDS"></param>
		/// <returns></returns>
		private Guid UploadAttachment(FileImportDS oFileImportDS)
		{
			FileImportDS oFileImportDetailsDSSendToSM = new FileImportDS();
			IBrokerManagedComponent smFileImport = this.Broker.CreateComponentInstance(new Guid(fileImportTypeIDStr),oFileImportDS.Description);
		
			smFileImport.ExtractData(oFileImportDetailsDSSendToSM);	
			
			oFileImportDetailsDSSendToSM.DateImported = oFileImportDS.DateImported;
			DateTime dateImported = (DateTime)oFileImportDetailsDSSendToSM.DateImported;
			oFileImportDetailsDSSendToSM.DateImported = new DateTime(dateImported.Year,dateImported.Month,dateImported.Day,dateImported.Hour, dateImported.Minute,dateImported.Second,0 );
			
			oFileImportDetailsDSSendToSM.OriginPath = oFileImportDS.OriginPath;
			oFileImportDetailsDSSendToSM.ImportedBy = oFileImportDS.ImportedBy;
			oFileImportDetailsDSSendToSM.Stream = oFileImportDS.Stream;
			oFileImportDetailsDSSendToSM.Description = oFileImportDS.Description;
		
			smFileImport.SetData(oFileImportDetailsDSSendToSM);

            this.attachments.Add(oFileImportDS.AttactmentID, smFileImport.CID, oFileImportDS.ImportedBy, oFileImportDS.DateImported, oFileImportDS.OriginPath, oFileImportDS.Description, oFileImportDS.Linked, (AttachmentType)Enum.ToObject(typeof(AttachmentType), oFileImportDS.DocumentType));

            return smFileImport.CID;
        }

        /// <summary>
        /// Apply changes for this component to the dataset
        /// </summary>
        /// <param name="data"></param>
        /// <param name="resetOnFinish"></param>
        public virtual void ApplyUpdateData( DataSet data, bool resetOnFinish )
        {
            this.OnApplyUpdateData( data, resetOnFinish );
        }

        /// <summary>
        /// Apply changes for this component to the dataset
        /// </summary>
        /// <param name="data"></param>
        /// <param name="resetOnFinish"></param>
        protected virtual void OnApplyUpdateData( DataSet data, bool resetOnFinish ){}

		/// <summary>
		/// A module overrides this method to be notified when data update changes
		/// should be accepted, for example after having been loaded from the database
		/// </summary>
		public virtual void MarkModuleAsUnmodified()
		{
			this.ResetTokenToPersistedToken();
			this.Modified = false;
		}
	}
}
