using System;

namespace Oritax.TaxSimp.Utilities
{
	// Adjustment sign types
	public enum AdjustmentSignTypes
	{
		Addback =+1,
		Subtraction =-1
	}

	//Used to mark a message as being either BF or CF or none,
	//so that when such messages are used in a  multiperiod
	//then unnecessary BF and CF values can be filtered out.
	public enum BalanceType
	{
		None,
		BroughtForward,
		CarriedForward
	}

	// Adjustment type
	public enum AdjustmentTypes
	{
		Normal, // Standard pre-tax adjustment
		TaxCredit, // Rebate/credit
		LossCurrentYearIncurred, // Current year tax loss incurred
		LossBroughtForward, // Loss brought forward/carried back
		LossTransferIn, // Tax loss transferred in
		LossTransferOut, // Tax loss transferred out
        TaxPaid,
        Equity,
	}

	public enum AccountColumnTypes
	{
		BroughtForward=0,
		TaxExpense,
		Payment,
		Transfer,
		RateChange,
		[ Obsolete( "Foreign exchange is no longer a supported column type and must not be used", true ) ]
		ForeignExchange,
		CarriedForward,
		PriorPeriodBroughtForward,
		PriorPeriodTaxExpense,
		PriorPeriodTransfer,
		PriorPeriodRateChange,
		MovementTaxExpense,
		MovementTransfer,
		MovementRateChange,
        Reallocation,
        ClosingBalanceTaxExpenseTB,
        ClosingBalanceTB,
	}
    /// <summary>
    /// Type of account in Balance sheet(Tea100)
    /// </summary>
	public enum AccountBalanceType
	{
		Asset=-1,
        Net = 0,
		Liability=1
	}
    /// <summary>
    /// Type of every value in an account. Each account can have several AccountValueTypes.
    /// Used in Tea045, Tea040
    /// </summary>
    public enum AccountValueType
    {
        DTA = -1,
        Auto = 0,
        DTL = 1
    }
    /// <summary>
    /// Type of a sub-category in Balance sheet(Tea100)
    /// Each AccountBalanceType can have several sub-categories
    /// </summary>
    public enum AccountBalanceCategoryType
    {
        AssetNonCurrent,
        AssetCurrent,
        LiabilitiesNonCurrent,
        LiabilitiesCurrent,
        ReallocatedItemsAssets,
        ReallocatedItemsLiabilities,
        None,
        NonFinancialItems       
    }

    public enum AdjustmentAllocationType
    {
        None,
        Permanent,
        Temporary,
        Equity
    }

    /// <summary>
	/// A message tag that indicates that the structure of the system is changing
	/// </summary>
	public enum StreamControlType
	{
		/// <summary>
		/// A Calculation Module is updating some values
		/// </summary>
		Update,
		/// <summary>
		/// A Calculation Module is joining the system
		/// </summary>
		Start,
		/// <summary>
		/// A Calculation Module is leaving the system
		/// </summary>
		End,
		/// <summary>
		/// A Period is joining an entity, group or multi-period
		/// </summary>
		PeriodJoining,
		/// <summary>
		/// A period is leaving an entity, group or multi-period
		/// </summary>
		PeriodLeaving,
		/// <summary>
		/// A Period's details are changing, e.g. the start or end date are changing or the SAP for the period is changing
		/// </summary>
		PeriodChanging,
		/// <summary>
		/// An organisation unit (group or entity) is joining a group
		/// </summary>
		OrganisationUnitJoining,
		/// <summary>
		/// An organisation unit is changing, e.g. The ownership history is changing
		/// </summary>
		OrganisationUnitChanging,
		/// <summary>
		/// An organisation unit is leaving, e.g. The ownership history is changing
		/// </summary>
		OrganisationUnitLeaving,
		/// <summary>
		/// A stream end has been signalled for a organisation unit CM, where 
		/// the organisation unit is a member of a consolidation.
		/// </summary>
		OrganisationUnitCMEnd,
		/// <summary>
		/// A period had been deleted from an entity/group and this entity/group
		/// is a member of a group.
		/// </summary>
		MemberPeriodDeleted,
	}


}
