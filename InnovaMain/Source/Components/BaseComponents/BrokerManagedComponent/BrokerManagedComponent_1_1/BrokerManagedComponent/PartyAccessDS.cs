using System;
using System.Data;

namespace DTT.TX360Enterprise.Calculation
{
	[Serializable] 
	public class PartyAccessDS : DataSet
	{
		public const string ACLENTRY_TABLE			= "ACLENTRY";
		public const string PARTYCID_FIELD			= "PARTYCID";
		public const string PARTYNAME_FIELD			= "PARTYNAME";
		public const string PRIVILEGETYPE_FIELD		= "PRIVILEGETYPE";
		public const string PRIVILEGETYPECODE_FIELD	= "PRIVILEGETYPECODE";
		public const string PRIVILEGEVALUE_FIELD	= "PRIVILEGEVALUE";
		public const string PRIVILEGEVALUECODE_FIELD= "PRIVILEGEVALUECODE";

		private Guid partyID;

		public PartyAccessDS()
		{
			DataTable table;

			table = new DataTable(ACLENTRY_TABLE);
			table.Columns.Add(PARTYCID_FIELD, typeof(System.String));
			table.Columns.Add(PARTYNAME_FIELD, typeof(System.String));
			table.Columns.Add(PRIVILEGETYPE_FIELD, typeof(System.String));
			table.Columns.Add(PRIVILEGETYPECODE_FIELD, typeof(System.Int32));
			table.Columns.Add(PRIVILEGEVALUE_FIELD, typeof(System.String));
			table.Columns.Add(PRIVILEGEVALUECODE_FIELD, typeof(System.Int32));
			this.Tables.Add(table);
		}

		public Guid PartyID
		{
			get{return partyID;}
			set{partyID=value;}
		}
	}
}


