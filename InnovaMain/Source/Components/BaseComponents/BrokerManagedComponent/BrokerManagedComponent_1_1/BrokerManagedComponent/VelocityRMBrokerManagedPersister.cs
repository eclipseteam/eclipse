﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using System.Data;

using Oritax.TaxSimp.VelocityCache;
using Oritax.TaxSimp.CalculationInterface;

namespace Oritax.TaxSimp.Calculation
{
    public class VelocityRMBrokerManagedPersister : VelocityRMBase
    {
        IBrokerManagedComponent _IBMC;
        Guid _CSID;

        public VelocityRMBrokerManagedPersister(IBrokerManagedComponent iBMC, Guid csid)
        {
            this._IBMC = iBMC;
            this._CSID = csid;
        }

        public override void Prepare(PreparingEnlistment preparingEnlistment)
        {
            if (this._IBMC is ICalculationModule)
                ((ICalculationModule)this._IBMC).CSID = this._CSID;

            DataSet primaryDataSet = this._IBMC.PrimaryDataSet;
            this._IBMC.GetData(primaryDataSet);

            foreach (DataTable dataTable in primaryDataSet.Tables)
            {
                PrimaryDS.PDSDataTable pDSDataTable = dataTable as PrimaryDS.PDSDataTable;

                if (pDSDataTable != null && pDSDataTable.PersistenceType == PrimaryDS.PersistenceTypeOption.READWRITE)
                {
                    BaseVelocityWriter vw = this.GetVelocityWriter(pDSDataTable.TableName, primaryDataSet);

                    if (vw != null)
                        vw.Save(this._IBMC);
                }
            }
        }

        private BaseVelocityWriter GetVelocityWriter(string tableName, DataSet primaryDS)
        {
            BaseVelocityWriter vw = null;
            BrokerManagedVelocityWriter bmWriter;

            switch (tableName)
            {
                case "CINSTANCES":
                case "CMLOGICAL":
                case "CMSCENARIO":
                case "SCENARIO":
                case "SUBSCRIPTIONS":
                    bmWriter = new BrokerManagedVelocityWriter(
                        VelocityCacheString.VelocityCache,
                        VelocityConfiguration.VelocityKeys[tableName],
                        tableName,
                        primaryDS);

                    vw = bmWriter;
                    break;
            }

            return vw;

        }



    }
}
