#region Imports

using System;
using System.Data;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.WPDatasetBase;

#endregion // Imports

namespace Oritax.TaxSimp.Calculation
{
	/// <summary>
	/// The DataSet Class used for Notes
	/// </summary>
	[Serializable()]
	public class CMTargetsDS : WPDatasetBaseDS
    {
        #region Fields

        public const String AUTOADJUSTMENT_TABLE		= "AUTOADJUSTMENT_TABLE";
		public const String AUTOADJUSTMENTON_FIELD		= "AutoAdjustmentOn";
		public const String ISCONFIGURABLECM_FIELD		= "IsConfigurableCM";
		public const String SHOWZEROS_FIELD				= "ShowZeros";
		
		public const String TARGETS_TABLE				= "TARGETS_TABLE";
		public const String TARGETS_PRIMARYKEY_FIELD	= "Key";
		public const String TARGETS_DESCRIPTION_FIELD	= "Description";
		public const String TARGETS_COMPONENTID_FIELD	= "ComponentID";

        public const String WORKPAPER_TABLE             = "WORKPAPER_TABLE";
        public const String WORKPAPER_NAME_FIELD        = "WorkpaperName";
        public const String ISCONFIGURABLEWP_FIELD      = "IsConfigurableWorkpaper";
		
		public Guid ComponentID;

        #endregion // Fields

        #region Constructors

        public CMTargetsDS()
		{
			DataTable table = new DataTable(AUTOADJUSTMENT_TABLE);
			table.Columns.Add(AUTOADJUSTMENTON_FIELD, typeof(System.Boolean));
			table.Columns.Add(ISCONFIGURABLECM_FIELD, typeof(System.Boolean));
			table.Columns.Add(SHOWZEROS_FIELD, typeof(System.Boolean));
            table.Columns.Add(WORKPAPER_NAME_FIELD, typeof(System.String));
            Tables.Add(table);
			BrokerManagedComponentDS.AddVersionStamp(table,typeof(CMTargetsDS));

			DataTable oDT = new DataTable(TARGETS_TABLE);
			oDT.Columns.Add(TARGETS_DESCRIPTION_FIELD,typeof(System.String)); 
			oDT.Columns.Add(TARGETS_COMPONENTID_FIELD,typeof(System.Guid)); 
			oDT.Columns.Add(TARGETS_PRIMARYKEY_FIELD,typeof(System.Guid)); 
			Tables.Add(oDT);
			BrokerManagedComponentDS.AddVersionStamp(oDT,typeof(CMTargetsDS));

            this.CreateWorkpaperTable();
        }

        #endregion // Constructors

        #region Public Methods

        public bool IsConfigurableCM
		{
			get
			{
				if(this.Tables[AUTOADJUSTMENT_TABLE].Rows.Count == 0)
				{
					DataRow newRow = this.Tables[AUTOADJUSTMENT_TABLE].NewRow();
					newRow[ISCONFIGURABLECM_FIELD] = false;
					this.Tables[AUTOADJUSTMENT_TABLE].Rows.Add(newRow);
				}

				return (bool)this.Tables[AUTOADJUSTMENT_TABLE].Rows[0][ISCONFIGURABLECM_FIELD];
			}
			set
			{
				if(this.Tables[AUTOADJUSTMENT_TABLE].Rows.Count == 0)
				{
					DataRow newRow = this.Tables[AUTOADJUSTMENT_TABLE].NewRow();
					newRow[ISCONFIGURABLECM_FIELD] = false;
					this.Tables[AUTOADJUSTMENT_TABLE].Rows.Add(newRow);
				}
				this.Tables[AUTOADJUSTMENT_TABLE].Rows[0][ISCONFIGURABLECM_FIELD] = value;
			}
		}

		public bool AutomaticAdjustmentsOn
		{
			get
			{
				if(this.Tables[AUTOADJUSTMENT_TABLE].Rows.Count == 0)
				{
					DataRow newRow = this.Tables[AUTOADJUSTMENT_TABLE].NewRow();
					newRow[AUTOADJUSTMENTON_FIELD] = false;
					this.Tables[AUTOADJUSTMENT_TABLE].Rows.Add(newRow);
				}

				return (bool)this.Tables[AUTOADJUSTMENT_TABLE].Rows[0][AUTOADJUSTMENTON_FIELD];
			}
			set
			{
				if(this.Tables[AUTOADJUSTMENT_TABLE].Rows.Count == 0)
				{
					DataRow newRow = this.Tables[AUTOADJUSTMENT_TABLE].NewRow();
					newRow[AUTOADJUSTMENTON_FIELD] = false;
					this.Tables[AUTOADJUSTMENT_TABLE].Rows.Add(newRow);
				}
				this.Tables[AUTOADJUSTMENT_TABLE].Rows[0][AUTOADJUSTMENTON_FIELD] = value;
			}
		}
		public bool ShowZeros
		{
			get
			{
				if(this.Tables[AUTOADJUSTMENT_TABLE].Rows.Count == 0)
				{
					DataRow newRow = this.Tables[AUTOADJUSTMENT_TABLE].NewRow();
					newRow[SHOWZEROS_FIELD] = false;
					this.Tables[AUTOADJUSTMENT_TABLE].Rows.Add(newRow);
				}

				return (bool)this.Tables[AUTOADJUSTMENT_TABLE].Rows[0][SHOWZEROS_FIELD];
			}
			set
			{
				if(this.Tables[AUTOADJUSTMENT_TABLE].Rows.Count == 0)
				{
					DataRow newRow = this.Tables[AUTOADJUSTMENT_TABLE].NewRow();
					newRow[SHOWZEROS_FIELD] = false;
					this.Tables[AUTOADJUSTMENT_TABLE].Rows.Add(newRow);
				}
				this.Tables[AUTOADJUSTMENT_TABLE].Rows[0][SHOWZEROS_FIELD] = value;
			}
		}

        public String WorkpaperName
        {
            get
            {
                if (this.Tables[WORKPAPER_TABLE].Rows.Count == 0)
                {
                    DataRow newRow = this.Tables[WORKPAPER_TABLE].NewRow();
                    newRow[WORKPAPER_NAME_FIELD] = String.Empty;
                    this.Tables[WORKPAPER_TABLE].Rows.Add(newRow);
                }
                return (String)this.Tables[WORKPAPER_TABLE].Rows[0][WORKPAPER_NAME_FIELD];
            }

            set
            {
                if (this.Tables[WORKPAPER_TABLE].Rows.Count == 0)
                {
                    DataRow newRow = this.Tables[WORKPAPER_TABLE].NewRow();
                    newRow[WORKPAPER_NAME_FIELD] = String.Empty;
                    this.Tables[WORKPAPER_TABLE].Rows.Add(newRow);
                }
                this.Tables[WORKPAPER_TABLE].Rows[0][WORKPAPER_NAME_FIELD] = value;
            }
        }

        public Boolean HideTargetControlPanels
        {
            get
            {
                if (this.Tables[WORKPAPER_TABLE].Rows.Count == 0)
                {
                    DataRow newRow = this.Tables[WORKPAPER_TABLE].NewRow();
                    newRow[ISCONFIGURABLEWP_FIELD] = false;
                    this.Tables[WORKPAPER_TABLE].Rows.Add(newRow);
                }
                return (Boolean)this.Tables[WORKPAPER_TABLE].Rows[0][ISCONFIGURABLEWP_FIELD];
            }

            set
            {
                if (this.Tables[WORKPAPER_TABLE].Rows.Count == 0)
                {
                    DataRow newRow = this.Tables[WORKPAPER_TABLE].NewRow();
                    newRow[ISCONFIGURABLEWP_FIELD] = false;
                    this.Tables[WORKPAPER_TABLE].Rows.Add(newRow);
                }
                this.Tables[WORKPAPER_TABLE].Rows[0][ISCONFIGURABLEWP_FIELD] = value;
            }
        }

        #endregion // Public Methods

        #region Private Methods

        private void CreateWorkpaperTable()
        {
            DataTable workpaperTable = new DataTable(WORKPAPER_TABLE);
            workpaperTable.Columns.Add(WORKPAPER_NAME_FIELD, typeof(System.String));
            workpaperTable.Columns.Add(ISCONFIGURABLEWP_FIELD, typeof(System.Boolean));
            Tables.Add(workpaperTable);
            BrokerManagedComponentDS.AddVersionStamp(workpaperTable, typeof(CMTargetsDS));
            DataRow newRow = workpaperTable.NewRow();
            newRow[WORKPAPER_NAME_FIELD] = String.Empty;
            newRow[ISCONFIGURABLEWP_FIELD] = false;
            workpaperTable.Rows.Add(newRow);
        }

        #endregion // Private Methods
    }
}
