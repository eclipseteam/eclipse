using System;
using System.Data;
using System.Runtime.Serialization;
using System.Security.Permissions;

using Oritax.TaxSimp.Utilities; 
using Oritax.TaxSimp.Calculation;


namespace Oritax.TaxSimp.Calculation
{
	/// <summary>
	/// Base class for all user notes
	/// </summary>
	[Serializable]
	public class NoteItem: ISerializable
	{
		public NoteItem()
		{
			
		}
		
		public NoteItem(Guid oNoteID, Guid preNoteID, Guid oWorkpaperID, String sDescription,
						String sCreatedBy, DateTime oCreatedDate, String oLastUserUpdated, DateTime oLastDateUpdated, bool bRollForward)
		{
			PrimaryKey = oNoteID;
			WorkpaperID = oWorkpaperID;
			this.PrevPeriodKey = preNoteID;
			Description = sDescription;
			CreatedBy = sCreatedBy;
			CreationDate = oCreatedDate;
			LastUserUpdated = oLastUserUpdated;
			LastDateUpdated = oLastDateUpdated;
			RollForward = bRollForward;
		}
		
		public NoteItem(DataRow row)
		{
			DeliverRow(row);
		}

		//NOTE: Note Items are relationally persisted with the serialization only being used
		//so that the token of a BMC can be calculated and changes in state of notes
		//taken into account. As such only the primary key and teh last date updated
		//fields are serialized, this causes the last updated field to be used
		//as a kind of timestamp to enable detecting modification.
		protected NoteItem(SerializationInfo si, StreamingContext context)
		{
			PrimaryKey=Serialize.GetSerializedGuid(si,"ni_PrimaryKey");
			LastDateUpdated=Serialize.GetSerializedDateTime(si,"ni_LastDateUpdated");
		}
	
		//Only allow the .NET Serialization core code to call this function
		[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]
		public virtual void GetObjectData(SerializationInfo si, StreamingContext context)
		{
			Serialize.AddSerializedValue(si,"ni_PrimaryKey",PrimaryKey);
			Serialize.AddSerializedValue(si,"ni_LastDateUpdated",LastDateUpdated);
		}


		//USING CUSTOM SERIALIZATION.
		//Still mark the fields as [NonSerialized] for your own infomation but this is ignored now by .NET
		//You must add your own serialzation code for each instance
		//See GetObjectData & Special Constructor

		/// <summary>
		/// Unique Identifier for the Note
		/// </summary>
		public Guid PrimaryKey = Guid.NewGuid();

		/// <summary>
		/// Unique Identifier for the Note
		/// </summary>
		public Guid PrevPeriodKey = Guid.Empty;

		/// <summary>
		/// The Workpaper the Note Belongs to
		/// </summary>
		public Guid WorkpaperID = Guid.Empty;

		/// <summary>
		/// Holds the actual note information
		/// </summary>
		public String Description = String.Empty;
		
		/// <summary>
		/// The Initial user who create the Note
		/// </summary>
		public String CreatedBy = String.Empty;

		/// <summary>
		/// The Date When the Note Was Created
		/// </summary>
		public DateTime CreationDate = DateTime.Now;

		/// <summary>
		/// Last User who updated the Note
		/// </summary>
		public String LastUserUpdated = String.Empty;
		
		/// <summary>
		/// Last Date Updated
		/// </summary>
		public DateTime LastDateUpdated = DateTime.Now;

		/// <summary>
		/// If the note is rolled forward to the next period
		/// </summary>
		public bool RollForward = false;

		
		


		/// <summary>
		/// Extract the information from the Note Item and Put it in the DataRow
		/// </summary>
		/// <param name="oDataRow"></param>
		public void ExtractRow(DataRow oDataRow)
		{
			oDataRow[NoteListDS.NOTES_PRIMARYKEY_FIELD] = PrimaryKey;
			oDataRow[NoteListDS.NOTES_PREV_PRIMARYKEY_FIELD] = this.PrevPeriodKey; 	
			oDataRow[NoteListDS.NOTES_WORKPAPERID_FIELD] = WorkpaperID;
			oDataRow[NoteListDS.NOTES_DESCRIPTION_FIELD] = Description;
			oDataRow[NoteListDS.NOTES_CREATEDBY_FIELD] = CreatedBy;
			oDataRow[NoteListDS.NOTES_CREATIONDATE_FIELD] = CreationDate; 
			oDataRow[NoteListDS.NOTES_LASTDATEUPDATED_FIELD] = LastDateUpdated; 
			oDataRow[NoteListDS.NOTES_LASTUSERUPDATED_FIELD] = LastUserUpdated; 
			oDataRow[NoteListDS.NOTES_ROLLFORWARD_FIELD] = RollForward; 
		}

		/// <summary>
		/// Deliver the information in the DataRow and put it in the NoteItem
		/// </summary>
		/// <param name="oDataRow"></param>
		public void DeliverRow(DataRow oDataRow)
		{
			PrimaryKey = new Guid(oDataRow[NoteListDS.NOTES_PRIMARYKEY_FIELD].ToString());
			if ( !(oDataRow[NoteListDS.NOTES_PREV_PRIMARYKEY_FIELD] is Guid) || Guid.Empty.Equals(oDataRow[NoteListDS.NOTES_PREV_PRIMARYKEY_FIELD]) )
			{
				this.PrevPeriodKey = Guid.Empty;
			}
			else
			{
				this.PrevPeriodKey = new Guid(oDataRow[NoteListDS.NOTES_PREV_PRIMARYKEY_FIELD].ToString());
			} 	
			WorkpaperID = new Guid(oDataRow[NoteListDS.NOTES_WORKPAPERID_FIELD].ToString()); 
			Description = oDataRow[NoteListDS.NOTES_DESCRIPTION_FIELD].ToString();
			CreatedBy = oDataRow[NoteListDS.NOTES_CREATEDBY_FIELD].ToString();

			CreationDate = (DateTime)oDataRow[NoteListDS.NOTES_CREATIONDATE_FIELD]; 
			CreationDate = new DateTime(CreationDate.Year,CreationDate.Month,CreationDate.Day,CreationDate.Hour, CreationDate.Minute,CreationDate.Second,0 );
			LastDateUpdated = (DateTime)oDataRow[NoteListDS.NOTES_LASTDATEUPDATED_FIELD]; 
			LastUserUpdated = oDataRow[NoteListDS.NOTES_LASTUSERUPDATED_FIELD].ToString();
			RollForward = bool.Parse(oDataRow[NoteListDS.NOTES_ROLLFORWARD_FIELD].ToString());
		}
	}
}
