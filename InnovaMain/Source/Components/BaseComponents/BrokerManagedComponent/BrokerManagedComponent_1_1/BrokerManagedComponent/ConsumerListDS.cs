using System;
using System.Data;

namespace Oritax.TaxSimp.Calculation
{
	/// <summary>
	/// ConsumerListDS provide a data set for listing the consumers (BMCs that are referencing) for a particular BMC.
	/// </summary>
	public class ConsumerListDT : DataTable
	{
		#region TABLE NAMES AND FIELDS
		public const string CONSUMERS_TABLE			= "CONSUMERS";

		public const string CID_FIELD				= "CID";
		public const string NAME_FIELD				= "NAME";
		public const string TYPE_NAME_FIELD			= "DISPLAYNAME";
		#endregion

		public ConsumerListDT()
		{
			this.TableName=CONSUMERS_TABLE;
			this.Columns.Add( CID_FIELD, typeof( System.Guid) );
			this.Columns.Add( NAME_FIELD, typeof( System.String) );
			this.Columns.Add( TYPE_NAME_FIELD, typeof( System.String) );
		}

		public bool HasConsumers { get { return Rows.Count > 0; } }
	}

	public class ConsumerListDS : DataSet
	{
		public ConsumerListDS()
		{
			this.Tables.Add(new ConsumerListDT());
		}

		public ConsumerListDT ConsumerTable { get {  return (ConsumerListDT)Tables[ConsumerListDT.CONSUMERS_TABLE]; } }
	}
}
