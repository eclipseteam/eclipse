using System;
using System.Data;
using Oritax.TaxSimp.Calculation;

namespace Oritax.TaxSimp.Utilities
{
	/// <summary>
	/// Summary description for MessageDS.
	/// </summary>
	/// 
	[Serializable] 
	public class MessageDS : DataSet
	{
		public const String PUBLISHERDATA_TABLE						= "PublisherData";
		public const String CLID_FIELD								= "CLID";
		public const String CSID_FIELD								= "CSID";
		public const String CIID_FIELD								= "CIID";
		public const String CMNAME_FIELD							= "CMName";
		public const String CMTYPENAME_FIELD						= "CMTypeName";
		public const String CMCATEGORYNAME_FIELD					= "CMCategoryName";
		public const String ISORIGINATOR_FIELD						= "OriginatingPublisher";

		public const String MESSAGEMEMBERS_TABLE					= "MessageMembers";
		public const String SCOPE_FIELD								= "Scope";
		public const String MSID_FIELD								= "MSID";
		public const String INTERSCENARIO_FIELD						= "InterScenario";
		public const String DATATYPE_FIELD							= "DataType";

		public const String MESSAGEDATA_TABLE						= "MessageData";
		public const String KEY_FIELD								= "Key";
		public const String VALUE_FIELD								= "Value";
		
		public MessageDS() 
		{
			this.DataSetName = this.GetType().Name;

			DataTable table;

			table = new DataTable(PUBLISHERDATA_TABLE);
			table.Columns.Add(CLID_FIELD, typeof(System.Guid));
			table.Columns.Add(CSID_FIELD, typeof(System.Guid));
			table.Columns.Add(CIID_FIELD, typeof(System.Guid));
			table.Columns.Add(CMNAME_FIELD, typeof(System.String));
			table.Columns.Add(CMTYPENAME_FIELD, typeof(System.String));
			table.Columns.Add(CMCATEGORYNAME_FIELD, typeof(System.String));
			table.Columns.Add(ISORIGINATOR_FIELD, typeof(System.Boolean));
			this.Tables.Add(table);
			BrokerManagedComponentDS.AddVersionStamp(table,typeof(MessageDS));
			
			table = new DataTable(MESSAGEMEMBERS_TABLE);
			table.Columns.Add(SCOPE_FIELD, typeof(System.Int32));
			table.Columns.Add(MSID_FIELD, typeof(System.Guid));
			table.Columns.Add(INTERSCENARIO_FIELD, typeof(System.Boolean));
			table.Columns.Add(DATATYPE_FIELD, typeof(System.String));
			this.Tables.Add(table);
			BrokerManagedComponentDS.AddVersionStamp(table,typeof(MessageDS));

			table = new DataTable(MESSAGEDATA_TABLE);
			table.Columns.Add(KEY_FIELD, typeof(System.String));
			table.Columns.Add(VALUE_FIELD, typeof(System.Object));
			this.Tables.Add(table);
			BrokerManagedComponentDS.AddVersionStamp(table,typeof(MessageDS));
		
		}

		public Guid MSID
		{
			set
			{

				if(this.Tables[MESSAGEMEMBERS_TABLE].Rows.Count>0)
				{
					DataRow bmcRow=this.Tables[MESSAGEMEMBERS_TABLE].Rows[0];
					bmcRow[MSID_FIELD]=value;
				}
				else
				{
					DataRow bmcRow=this.Tables[MESSAGEMEMBERS_TABLE].NewRow();
					bmcRow[MSID_FIELD]=value;
					this.Tables[MESSAGEMEMBERS_TABLE].Rows.Add(bmcRow);
				}
			}
			get
			{
				if(this.Tables[MESSAGEMEMBERS_TABLE].Rows.Count>0)
				{
					DataRow bmcRow=this.Tables[MESSAGEMEMBERS_TABLE].Rows[0];
					return (Guid)bmcRow[MSID_FIELD];
				}
				else
					return Guid.Empty;
			}
		}

		public int Scope
		{
			set
			{

				if(this.Tables[MESSAGEMEMBERS_TABLE].Rows.Count>0)
				{
					DataRow bmcRow=this.Tables[MESSAGEMEMBERS_TABLE].Rows[0];
					bmcRow[SCOPE_FIELD]=value;
				}
				else
				{
					DataRow bmcRow=this.Tables[MESSAGEMEMBERS_TABLE].NewRow();
					bmcRow[SCOPE_FIELD]=value;
					this.Tables[MESSAGEMEMBERS_TABLE].Rows.Add(bmcRow);
				}
			}
			get
			{
				if(this.Tables[MESSAGEMEMBERS_TABLE].Rows.Count>0)
				{
					DataRow bmcRow=this.Tables[MESSAGEMEMBERS_TABLE].Rows[0];
					return (int)bmcRow[SCOPE_FIELD];
				}
				else
					return 0;
			}
		}

		public bool InterScenario
		{
			set
			{

				if(this.Tables[MESSAGEMEMBERS_TABLE].Rows.Count>0)
				{
					DataRow bmcRow=this.Tables[MESSAGEMEMBERS_TABLE].Rows[0];
					bmcRow[INTERSCENARIO_FIELD]=value;
				}
				else
				{
					DataRow bmcRow=this.Tables[MESSAGEMEMBERS_TABLE].NewRow();
					bmcRow[INTERSCENARIO_FIELD]=value;
					this.Tables[MESSAGEMEMBERS_TABLE].Rows.Add(bmcRow);
				}
			}
			get
			{
				if(this.Tables[MESSAGEMEMBERS_TABLE].Rows.Count>0)
				{
					DataRow bmcRow=this.Tables[MESSAGEMEMBERS_TABLE].Rows[0];
					return (bool)bmcRow[INTERSCENARIO_FIELD];
				}
				else
					return false;
			}
		}

		public string Datatype
		{
			set
			{

				if(this.Tables[MESSAGEMEMBERS_TABLE].Rows.Count>0)
				{
					DataRow bmcRow=this.Tables[MESSAGEMEMBERS_TABLE].Rows[0];
					bmcRow[DATATYPE_FIELD]=value;
				}
				else
				{
					DataRow bmcRow=this.Tables[MESSAGEMEMBERS_TABLE].NewRow();
					bmcRow[DATATYPE_FIELD]=value;
					this.Tables[MESSAGEMEMBERS_TABLE].Rows.Add(bmcRow);
				}
			}
			get
			{
				if(this.Tables[MESSAGEMEMBERS_TABLE].Rows.Count>0)
				{
					DataRow bmcRow=this.Tables[MESSAGEMEMBERS_TABLE].Rows[0];
					return bmcRow[DATATYPE_FIELD].ToString();
				}
				else
					return String.Empty;
			}
		}


	}
}
