using System;
using System.Collections;
using System.Data;
using System.Runtime.Serialization;
using System.Security.Permissions;

using Oritax.TaxSimp.Utilities; 



namespace Oritax.TaxSimp.Calculation
{
	/// <summary>
	/// Collection for notes in the workpaper
	/// </summary>
	[Serializable]
	public class NoteList : SerializableHashtable
	{

		//USING CUSTOM SERIALIZATION.
		//Still mark the fields as [NonSerialized] for your own infomation but this is ignored now by .NET
		//You must add your own serialzation code for each instance
		//See GetObjectData & Special Constructor

		public NoteList()
		{
		}

		public NoteList(SerializationInfo si, StreamingContext context)
			: base(si,context)
		{	
		}
	
		//Only allow the .NET Serialization core code to call this function
		[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]
		public override void GetObjectData(SerializationInfo si, StreamingContext context)
		{
			base.GetObjectData(si,context);
		}

		/// <summary>
		/// Extract the NoteItem information out of the ArrayList and put it
		/// in the DataTable
		/// </summary>
		/// <param name="oTable"></param>
		public void ExtractData(DataTable oTable)
		{
			foreach(DictionaryEntry dictionaryEntry in this)
			{
				NoteItem noteItem=dictionaryEntry.Value as NoteItem;

				DataRow row = oTable.NewRow();
				noteItem.ExtractRow(row);
				oTable.Rows.Add(row);
			}
		}

		/// <summary>
		/// Deliver the Note Information into the array list
		/// </summary>
		/// <param name="oTable"></param>
		public void DeliverData(DataTable oTable)
		{
			foreach(DataRow row in oTable.Rows)
			{
				if(row.RowState!=DataRowState.Deleted)
				{
					NoteItem oNote = new NoteItem(row);
					//CQ TXE200004452
					Add(oNote.PrimaryKey,oNote);
				}
			}
		}

		/// <summary>
		/// Roll prior period notes from Notes DataTable
		/// </summary>
		/// <param name="oTable"></param>
		public void RollForwardNotes(DataTable oTable)
		{
			foreach(DataRow row in oTable.Rows)
			{
				NoteItem oNoteItem = new NoteItem(row);
				
				if (oNoteItem.RollForward == true)
				{
					oNoteItem.RollForward = false;
					oNoteItem.PrevPeriodKey=oNoteItem.PrimaryKey;	// Update the previous period key

					NoteItem previousItem = this.GetNoteItemByPreviousID(oNoteItem.PrimaryKey);

					if(previousItem != null)
					{
						oNoteItem.PrimaryKey = previousItem.PrimaryKey;
						this[oNoteItem.PrimaryKey] = oNoteItem;
					}
					else
					{
						oNoteItem.PrimaryKey = Guid.NewGuid();
						this.Add(oNoteItem.PrimaryKey, oNoteItem);
					}
				}
			}
		}

		private NoteItem GetNoteItemByPreviousID(Guid previousID)
		{
			foreach(DictionaryEntry entry in this)
			{
				NoteItem item = entry.Value as NoteItem;

				if (item.PrevPeriodKey == previousID)
					return item;
			}
			return null;
		}

		public void Diverge()
		{
			ArrayList keys = new ArrayList(this.Keys);

			foreach(Guid noteItemKey in keys)
			{
				NoteItem noteItem = (NoteItem) base[noteItemKey];
				base.Remove(noteItem.PrimaryKey);

				noteItem.PrimaryKey = Guid.NewGuid();
				base.Add(noteItem.PrimaryKey, noteItem);
			}
		}
	}
}
