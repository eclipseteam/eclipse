using System;
using System.Data;
using System.Collections;
using System.Reflection;
using System.Collections.Generic;

using System.Runtime.Serialization;
using System.Security.Permissions;

namespace Oritax.TaxSimp.Calculation
{
    /// <summary>
    /// Summary description for PrimaryDS.
    /// </summary>
    public class PrimaryDS : DataSet
    {
        #region CONSTANTS
        public const string TABLE_PROPERTY_READONLY				= "read_only";
        public const string TABLE_PROPERTY_TRANSIENT			= "transient";
        public const string TABLE_PROPERTY_ROOTTABLE			= "root_table";
        public const string TABLE_PROPERTY_CHILDTABLE			= "child_table";
        public const string TABLE_PROPERTY_PARENTTABLE			= "parent_table";
        public const string TABLE_PROPERTY_CHILDTABLESET		= "child_tables";
        public const string TABLE_PROPERTY_PARENTTABLESET		= "parent_tables";
        #endregion

        #region INTERNAL CLASSES

        public enum PersistenceTypeOption
        {
            UNDEFINED		=	0,
            READWRITE		=	1,
            READONLY		=	2,
            TRANSIENT		=	3
        }

        public enum PersistenceMethodOption
        {
            UNDEFINED		=	0,
            ROOT			=	1,
            PARENT			=	2,
            CHILD			=	3
        }

        public class DuplicateTableException : Exception
        {
            public DuplicateTableException(string tableName)
                : base("Attempt to add a duplicate table: "+tableName){}
        }

        public class MissingRelationTableException : Exception
        {
            public MissingRelationTableException(string tableName)
                : base("Attempt to add relation to table: "+tableName+" which does not exist in PDS"){}
        }

        public class DuplicatePersistenceRelationException : Exception
        {
            public DuplicatePersistenceRelationException(string parentTableName,string childTableName)
                : base("Attempt to add a duplicate persistence relation - parent: "+parentTableName+" child: "+childTableName){}
        }

        public class TableNames : ArrayList
        {
            public void Add(string name){base.Add(name);}
            public new string this[int i]{get{return (string)base[i];}}

            public TableNames() : base(){}
            public new TableNames Clone()
            {
                TableNames cloneTableNames=new TableNames();
                for(int i=0;i<this.Count;i++)
                    cloneTableNames.Add(this[i]);

                return cloneTableNames;
            }
        }

        public class PDSDataTable : DataTable
        {
            public TableNames ChildTableNames;
            public TableNames ParentTableNames;
            public PersistenceTypeOption PersistenceType;
            public PersistenceMethodOption PersistenceMethod;
            private List<DatabaseIndex> indexes = new List<DatabaseIndex>();
            private List<DatabaseStatistic> statistics = new List<DatabaseStatistic>();
            private bool primaryKeyIndexIsClustered = true;

            // indicate whether dynamic tracking is enabled for this datatable. The original persistence model
            // required that datasets be compared (see MarkChanges() in Framework->Persistence->Perister )
            // The new persistence model doesn't compare datasets but applies updates dynamically.
            // Default to false and set to true for any datatable that has dynamic tracking set. (See ApplyUpdateData() calls)
            private bool applyUpdatesDynamically = false;

            public PDSDataTable(string name,PersistenceTypeOption persistenceType,PersistenceMethodOption persistenceMethod) : base(name)
            {
                ChildTableNames=new TableNames();
                ParentTableNames=new TableNames();
                PersistenceType=persistenceType;
                PersistenceMethod=persistenceMethod;
            }

            public PDSDataTable() : base()
            {
                ChildTableNames=new TableNames();
                ParentTableNames=new TableNames();
                PersistenceType=PersistenceTypeOption.UNDEFINED;
                PersistenceMethod=PersistenceMethodOption.UNDEFINED;
            }

            public bool ApplyUpdatesDynamically
            {
                get { return applyUpdatesDynamically; }
                set { applyUpdatesDynamically = value; }
            }

            /// <summary>
            /// Gets a list of the database indexes for the table
            /// </summary>
            public List<DatabaseIndex> Indexes
            {
                get 
                {
                    return indexes; 
                }
            }

            /// <summary>
            /// Gets a list of the database statistics for the table
            /// </summary>
            public List<DatabaseStatistic> Statistics
            {
                get
                {
                    return this.statistics;
                }
            }

            /// <summary>
            /// Indicates if the primary key index is clustered
            /// </summary>
            public bool PrimaryKeyIndexIsClustered
            {
                get 
                { 
                    return primaryKeyIndexIsClustered; 
                }
                set 
                { 
                    primaryKeyIndexIsClustered = value; 
                }
            }
        }

        protected struct FieldDefinition
        {
            public string	Name;
            public Type		Type;
            public bool		Nullable;
            public int		Length;

            /// <summary>
            /// Constructor for the FieldDefinition struct in which the field is defined as allowing nulls.
            /// </summary>
            /// <param name="name">Field name (string).</param>
            /// <param name="type">Field Type (Type).</param>
            public FieldDefinition(string name,Type type){Name=name;Type=type;Nullable=true;Length=-1;}
            public FieldDefinition(string name,Type type,bool nullable){Name=name;Type=type;Nullable=nullable;Length=-1;}
            public FieldDefinition(string name,Type type,bool nullable,int length){Name=name;Type=type;Nullable=nullable;Length=length;}
        }
        #endregion

        public PrimaryDS()
            : base()
        {
            EnforceConstraints=false;
        }

        public PrimaryDS(SerializationInfo si, StreamingContext context)
			:base(si,context)
        {
        }

        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo si, StreamingContext context)
        {
            base.GetObjectData(si, context);
        }


        private PDSDataTable DefineTable(
            string name,
            FieldDefinition[] fields,
            string pKFieldName,
            PersistenceTypeOption persistenceType,
            PersistenceMethodOption persistenceMethod )
        {
            if(this.Tables.Contains(name))
                throw new DuplicateTableException(name);

            PDSDataTable table = new PDSDataTable(name, persistenceType, persistenceMethod);

            for(int i=0;i<fields.Length;i++)
            {
                DataColumn newColumn=new DataColumn(fields[i].Name,fields[i].Type);
                newColumn.AllowDBNull=fields[i].Nullable;
                if(fields[i].Length!=-1)
                    newColumn.ExtendedProperties.Add("Length",fields[i].Length.ToString());
                table.Columns.Add(newColumn);
            }

            table.PrimaryKey=new DataColumn[]{table.Columns[pKFieldName]};

            Assembly assembly=Assembly.GetCallingAssembly();
            table.ExtendedProperties.Add("FullName", assembly.FullName);
            table.ExtendedProperties.Add("CodeBase", assembly.CodeBase);
            table.ExtendedProperties.Add("DataType", this.GetType().ToString());

            this.Tables.Add(table);

            return table;
        }

        private PDSDataTable DefineTable(
            string name, 
            FieldDefinition[] fields, 
            string pKFieldName, 
            PersistenceTypeOption persistenceType, 
            PersistenceMethodOption persistenceMethod, 
            bool bUpdateDynamic)
        {
            if(this.Tables.Contains(name))
                throw new DuplicateTableException(name);

            PDSDataTable table = new PDSDataTable(name, persistenceType, persistenceMethod);

            for(int i=0;i<fields.Length;i++)
            {
                DataColumn newColumn=new DataColumn(fields[i].Name,fields[i].Type);
                newColumn.AllowDBNull=fields[i].Nullable;
                if(fields[i].Length!=-1)
                    newColumn.ExtendedProperties.Add("Length",fields[i].Length.ToString());
                table.Columns.Add(newColumn);
            }

            table.PrimaryKey=new DataColumn[]{table.Columns[pKFieldName]};

            Assembly assembly=Assembly.GetCallingAssembly();
            table.ExtendedProperties.Add("FullName", assembly.FullName);
            table.ExtendedProperties.Add("CodeBase", assembly.CodeBase);
            table.ExtendedProperties.Add("DataType", this.GetType().ToString());

            table.ApplyUpdatesDynamically = bUpdateDynamic;

            this.Tables.Add(table);

            return table;
        }


        protected PDSDataTable DefineRootTable(string name, FieldDefinition[] fields, string pKFieldName, PersistenceTypeOption persistenceType)
        {
            return this.DefineTable(name,fields,pKFieldName,persistenceType,PersistenceMethodOption.ROOT);
        }

        protected PDSDataTable DefineRootTable(string name, FieldDefinition[] fields, string pKFieldName, PersistenceTypeOption persistenceType, bool bUpdateDynamic)
        {
            return this.DefineTable(name,fields,pKFieldName,persistenceType,PersistenceMethodOption.ROOT, bUpdateDynamic);
        }

        protected PDSDataTable DefineChildTable(
            string name,
            FieldDefinition[] fields,
            string pKFieldName,
            PersistenceTypeOption persistenceType,
            string parentTableName)
        {
            PDSDataTable childTable = DefineTable(name, fields, pKFieldName, persistenceType, PersistenceMethodOption.CHILD);

            if(!this.Tables.Contains(parentTableName))
                throw new MissingRelationTableException(parentTableName);

            PDSDataTable parentTable=(PDSDataTable)this.Tables[parentTableName];
            parentTable.ChildTableNames.Add(name);

			return childTable;
        }

        protected PDSDataTable DefineChildTable(
            string name,
            FieldDefinition[] fields,
            string pKFieldName,
            PersistenceTypeOption persistenceType,
            string parentTableName,
            string parentColumnName,
            string childColumnName)
        {
            PDSDataTable childTable = DefineTable(name, fields, pKFieldName, persistenceType, PersistenceMethodOption.CHILD);

            if(!this.Tables.Contains(parentTableName))
                throw new MissingRelationTableException(parentTableName);

            PDSDataTable parentTable=(PDSDataTable)this.Tables[parentTableName];
            parentTable.ChildTableNames.Add(name);
            this.Relations.Add(name+"_"+parentColumnName+"_"+childColumnName,parentTable.Columns[parentColumnName],childTable.Columns[childColumnName], false);

			return childTable;
        }

        protected PDSDataTable DefineChildTable(
            string name, 
            FieldDefinition[] fields, 
            string pKFieldName,
            PersistenceTypeOption persistenceType, 
            string parentTableName,
            string parentColumnName, 
            string childColumnName, 
            bool bUpdateDynamic)
        {
            PDSDataTable childTable = DefineTable(name, fields, pKFieldName, persistenceType, PersistenceMethodOption.CHILD, bUpdateDynamic);

            if(!this.Tables.Contains(parentTableName))
                throw new MissingRelationTableException(parentTableName);

            PDSDataTable parentTable=(PDSDataTable)this.Tables[parentTableName];
            parentTable.ChildTableNames.Add(name);
            this.Relations.Add(name+"_"+parentColumnName+"_"+childColumnName,parentTable.Columns[parentColumnName],childTable.Columns[childColumnName], false);

			return childTable;
        }

        protected PDSDataTable DefineParentTable(string name, FieldDefinition[] fields, string pKFieldName, PersistenceTypeOption persistenceType, string childTableName)
        {
            PDSDataTable parentTable = DefineTable(name, fields, pKFieldName, persistenceType, PersistenceMethodOption.PARENT);

            if(!this.Tables.Contains(childTableName))
                throw new MissingRelationTableException(childTableName);

            PDSDataTable childTable=(PDSDataTable)this.Tables[childTableName];
            childTable.ParentTableNames.Add(name);

			return parentTable;
        }

        protected PDSDataTable DefineParentTable(string name, FieldDefinition[] fields, string pKFieldName, PersistenceTypeOption persistenceType, string childTableName, string parentColumnName, string childColumnName)
        {
            PDSDataTable parentTable = DefineTable(name, fields, pKFieldName, persistenceType, PersistenceMethodOption.PARENT);

            if(!this.Tables.Contains(childTableName))
                throw new MissingRelationTableException(childTableName);

            PDSDataTable childTable=(PDSDataTable)this.Tables[childTableName];
            childTable.ParentTableNames.Add(name);

            this.Relations.Add(name+"_"+parentColumnName+"_"+childColumnName,parentTable.Columns[parentColumnName],childTable.Columns[childColumnName], false);

			return parentTable;
        }

        protected PDSDataTable DefineParentTable(string name, FieldDefinition[] fields, string pKFieldName, 
            PersistenceTypeOption persistenceType, string childTableName, bool bUpdateDynamic)
        {
            PDSDataTable parentTable = DefineTable(name, fields, pKFieldName, persistenceType, PersistenceMethodOption.PARENT, bUpdateDynamic);

            if(!this.Tables.Contains(childTableName))
                throw new MissingRelationTableException(childTableName);

            PDSDataTable childTable = (PDSDataTable)this.Tables[childTableName];
            childTable.ParentTableNames.Add(name);

			return parentTable;
        }

        public override DataSet Clone()
        {
            DataSet copyPrimaryDS=base.Clone();

            foreach(DataTable dataTable in copyPrimaryDS.Tables)
            {
                PDSDataTable pDSDataTable=dataTable as PDSDataTable;

                if(pDSDataTable!=null)
                {
                    pDSDataTable.ChildTableNames=(TableNames)((PDSDataTable)this.Tables[dataTable.TableName]).ChildTableNames.Clone();
                    pDSDataTable.ParentTableNames=(TableNames)((PDSDataTable)this.Tables[dataTable.TableName]).ParentTableNames.Clone();
                    pDSDataTable.PersistenceType=((PDSDataTable)this.Tables[dataTable.TableName]).PersistenceType;
                    pDSDataTable.PersistenceMethod=((PDSDataTable)this.Tables[dataTable.TableName]).PersistenceMethod;
                }
            }

            return copyPrimaryDS;
        }
    }
}
