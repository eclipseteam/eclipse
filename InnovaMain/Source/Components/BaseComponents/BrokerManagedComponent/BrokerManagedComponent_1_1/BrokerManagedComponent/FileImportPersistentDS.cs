using System;
using System.Data;

using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.CalculationInterface;

namespace Oritax.TaxSimp.FileImport
{
	/// <summary>
	/// Summary description for FileImportPersistentDS.
	/// </summary>
	public class FileImportPersistentDS : BrokerManagedComponentDS
	{
		public const string FILEIMPORT_TABLE					= "FILEIMPORT";
		public const string FILEIMPORT_ID_FIELD					= "ID";
		public const string FILEIMPORT_DATEIMPORTED_FIELD		= "DATEIMPORTED";
		public const string FILEIMPORT_IMPORTEDBY_FIELD			= "IMPORTEDBY";
		public const string FILEIMPORT_ORIGINPATH_FIELD			= "ORIGINPATH";

		public const string BLOBPERSIST_TABLE					= "BLOBPERSIST";
		public const string BLOBPERSIST_INSTANCEID_FIELD		= "INSTANCEID";
		public const string BLOBPERSIST_NAME_FIELD				= "NAME";
		public const string BLOBPERSIST_TYPE_FIELD				= "TYPE";
		public const string BLOBPERSIST_PERSISTENT_DATA_FIELD	= "PERSISTENT_DATA";

		public FileImportPersistentDS()
		{
			this.DefineRootTable(FILEIMPORT_TABLE,
				new FieldDefinition[] {
										  new FieldDefinition(FILEIMPORT_ID_FIELD, typeof(System.Guid), false),
										  new FieldDefinition(FILEIMPORT_DATEIMPORTED_FIELD, typeof(System.DateTime), false),
										  new FieldDefinition(FILEIMPORT_IMPORTEDBY_FIELD, typeof(System.String), false),
										  new FieldDefinition(FILEIMPORT_ORIGINPATH_FIELD, typeof(System.String), false)
									  },
				FILEIMPORT_ID_FIELD, 
				PersistenceTypeOption.READWRITE);

			PDSDataTable blobPersistTable = this.DefineRootTable(BLOBPERSIST_TABLE,
				new FieldDefinition[] {
										  new FieldDefinition(BLOBPERSIST_INSTANCEID_FIELD, typeof(System.Guid), false),
										  new FieldDefinition(BLOBPERSIST_NAME_FIELD, typeof(System.String), false),
										  new FieldDefinition(BLOBPERSIST_TYPE_FIELD, typeof(System.String), false),
										  new FieldDefinition(BLOBPERSIST_PERSISTENT_DATA_FIELD, typeof(byte[])) 
									  },
				BLOBPERSIST_INSTANCEID_FIELD,
				PersistenceTypeOption.READWRITE);

            blobPersistTable.Indexes.Add(new DatabaseIndex("IX_BLOBPERSIST_1", true, true, true, new DataColumn[] { blobPersistTable.Columns[BLOBPERSIST_INSTANCEID_FIELD] }));
		}
	}
}
