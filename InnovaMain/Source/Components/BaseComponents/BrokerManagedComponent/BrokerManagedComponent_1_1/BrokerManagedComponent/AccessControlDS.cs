#region CHANGE_HISTORY
/*
 $Header: /2002/3. Implementation/Construction/BrokerManagedComponentBase/BrokerManagedComponentBase-1-0/BrokerManagedComponentBase/AccessCo $
 $History: AccessControlDS.cs $
 * 
 * *****************  Version 2  *****************
 * User: Sshrimpton   Date: 19/03/03   Time: 4:46p
 * Updated in $/2002/3. Implementation/Construction/BrokerManagedComponentBase/BrokerManagedComponentBase-1-0/BrokerManagedComponentBase
 * 
 * *****************  Version 1  *****************
 * User: Sshrimpton   Date: 13/03/03   Time: 2:34p
 * Created in $/2002/3. Implementation/Construction/BrokerManagedComponentBase/BrokerManagedComponentBase-1-0/BrokerManagedComponentBase
 * 
 * *****************  Version 1  *****************
 * User: Sshrimpton   Date: 18/12/02   Time: 4:55p
 * Created in $/2002/3. Implementation/Construction/BusinessStructureCM/Entity/Entity-1-1-1
 * 
 * *****************  Version 4  *****************
 * User: Pveitch      Date: 7/11/02    Time: 3:07p
 * Updated in $/2002/3. Implementation/Elaboration4/CM/Entity
 * Header Information Added
*/
#endregion
using System;
using System.Data;

namespace DTT.TX360Enterprise.Calculation
{
	[Serializable] 
	public class AccessControlDS : DataSet
	{
		public const string ACLENTRY_TABLE			= "ACLENTRY";
		public const string PARTYCID_FIELD			= "PARTYCID";
		public const string PARTYNAME_FIELD			= "PARTYNAME";
		public const string PRIVILEGETYPE_FIELD		= "PRIVILEGETYPE";
		public const string PRIVILEGEVALUE_FIELD	= "PRIVILEGEVALUE";

		public const string PARTY_TABLE				= "PARTY";

		public AccessControlDS()
		{
			DataTable table;

			table = new DataTable(ACLENTRY_TABLE);
			table.Columns.Add(PARTYCID_FIELD, typeof(System.String));
			table.Columns.Add(PARTYNAME_FIELD, typeof(System.String));
			table.Columns.Add(PRIVILEGETYPE_FIELD, typeof(System.String));
			table.Columns.Add(PRIVILEGEVALUE_FIELD, typeof(System.String));
			this.Tables.Add(table);
		}
	}
}

