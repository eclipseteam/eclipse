#region CHANGE_HISTORY
/*
 $Header: /2002/3. Implementation/Construction/BrokerManagedComponentBase/BrokerManagedComponentBase-1-0/BrokerManagedComponentBase/PartyDS. $
 $History: PartyDS.cs $
 * 
 * *****************  Version 1  *****************
 * User: Sshrimpton   Date: 19/03/03   Time: 4:47p
 * Created in $/2002/3. Implementation/Construction/BrokerManagedComponentBase/BrokerManagedComponentBase-1-0/BrokerManagedComponentBase
 * 
 * *****************  Version 1  *****************
 * User: Sshrimpton   Date: 13/03/03   Time: 2:34p
 * Created in $/2002/3. Implementation/Construction/BrokerManagedComponentBase/BrokerManagedComponentBase-1-0/BrokerManagedComponentBase
 * 
 * *****************  Version 1  *****************
 * User: Sshrimpton   Date: 18/12/02   Time: 4:55p
 * Created in $/2002/3. Implementation/Construction/BusinessStructureCM/Entity/Entity-1-1-1
 * 
 * *****************  Version 4  *****************
 * User: Pveitch      Date: 7/11/02    Time: 3:07p
 * Updated in $/2002/3. Implementation/Elaboration4/CM/Entity
 * Header Information Added
*/
#endregion
using System;
using System.Data;

namespace Oritax.TaxSimp.Calculation
{
	[Serializable] 
	public class PartyDS : DataSet
	{
		public const string INCLUDEDPARTY_TABLE		= "INCLUDEDPARTY";
		public const string AVAILABLEPARTY_TABLE	= "AVAILABLEPARTY";
		public const string PARTYCID_FIELD			= "PARTYCID";
		public const string PARTYNAME_FIELD			= "PARTYNAME";

		public string ConfigureWorkpaper = String.Empty;

		public PartyDS()
		{
			DataTable table;

			table = new DataTable(INCLUDEDPARTY_TABLE);
			table.Columns.Add(PARTYCID_FIELD, typeof(System.String));
			table.Columns.Add(PARTYNAME_FIELD, typeof(System.String));
			this.Tables.Add(table);

			table = new DataTable(AVAILABLEPARTY_TABLE);
			table.Columns.Add(PARTYCID_FIELD, typeof(System.String));
			table.Columns.Add(PARTYNAME_FIELD, typeof(System.String));
			this.Tables.Add(table);
		}

	}
}

