#region CHANGE_HISTORY
/*
 $Header: /2002/3. Implementation/Construction/BusinessStructureCM/Group/Group-1-0/GroupMemberScenariosDS.cs 2     15/04/03 12:02p Sireland $
 $History: GroupMemberScenariosDS.cs $
 * 
 * *****************  Version 2  *****************
 * User: Sireland     Date: 15/04/03   Time: 12:02p
 * Updated in $/2002/3. Implementation/Construction/BusinessStructureCM/Group/Group-1-0
 * Remove Warnings
 * 
 * *****************  Version 1  *****************
 * User: Sshrimpton   Date: 18/12/02   Time: 4:56p
 * Created in $/2002/3. Implementation/Construction/BusinessStructureCM/Group/Group-1-1-1
 * 
 * *****************  Version 2  *****************
 * User: Pveitch      Date: 7/11/02    Time: 3:07p
 * Updated in $/2002/3. Implementation/Elaboration4/CM/Group
 * Header information added
*/
#endregion
using System;
using System.Data;

using Oritax.TaxSimp.Calculation;


namespace Oritax.TaxSimp.CM.Group
{
	/// <summary>
	/// DSCalculationModule is a DataSet that contains all of the persistent data for a CM instance.
	/// </summary>
	[Serializable] 
	public class GroupMemberScenariosDS : BusinessStructureWorkpaperDS
	{
		public const String AVAILABLESCENARIOS_TABLE				= "AVAILABLESCENARIOS_TABLE";
		public const String MEMBERSCENARIONAME_FIELD				= "ENTITYNAME_FIELD";
		public const String MEMBERSCENARIOCSID_FIELD				= "SCENARIOCSID_FIELD";

		private Guid memberCLID;
		private Guid memberCSID;

		public string memberName;

		public string ConfigureWorkpaper = String.Empty;

		public GroupMemberScenariosDS()
		{
			DataTable table;

			table = new DataTable(AVAILABLESCENARIOS_TABLE);
			table.Columns.Add(MEMBERSCENARIONAME_FIELD, typeof(System.String));
			table.Columns.Add(MEMBERSCENARIOCSID_FIELD, typeof(System.Guid));
			this.Tables.Add(table);
		}

		public Guid MemberCLID{get{return memberCLID;}set{memberCLID=value;}}
		public Guid MemberCSID{get{return memberCSID;}set{memberCSID=value;}}
	}
}

