using System;
using System.Collections;

using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.BusinessStructureUtilities;
using Oritax.TaxSimp.CM.Period;
using Oritax.TaxSimp.CM.OrganizationUnit;
using System.Collections.Generic;

namespace Oritax.TaxSimp.CM.Group
{
    /// <summary>
    /// Sorts IPeriods by the reporting unit they belong to
    /// </summary>
    public class BMCSorter : IComparer<IPeriod>
    {
        public BMCSorter()
        {
        }

        #region IComparer<IPeriod> Members

        /// <summary>
        /// Compares 2 period modules to see if they are greater, less than or equal
        /// </summary>
        /// <param name="objA">The first object to check</param>
        /// <param name="objB">The second object to check</param>
        /// <returns>0 if the object are equal, 1 if x is greater than y, -1 if y is greater than x</returns>
        /// <exception cref="System.ArgumentNullException">x cannot be null, y cannot be null</exception>
        public int Compare(IPeriod x, IPeriod y)
        {
            if (x == null)
                throw new ArgumentNullException("x");

            if (y == null)
                throw new ArgumentNullException("y");

            IOrganizationUnit reportingUnitA = x.GetParentOrganisationUnit();
            IOrganizationUnit reportingUnitB = y.GetParentOrganisationUnit();

            OrganisationUnitType objBMCAType = OrganisationUnitType.Entity;

            if (reportingUnitA is IGroup)
                objBMCAType = OrganisationUnitType.Group;

            OrganisationUnitType objBMCBType = OrganisationUnitType.Entity;

            if (reportingUnitB is IGroup)
                objBMCBType = OrganisationUnitType.Group;

            string strBMCAName = reportingUnitA.GetLogicalModule().Name;
            string strBMCBName = reportingUnitB.GetLogicalModule().Name;

            // if the objects are the same type, sort by string
            if (objBMCAType == objBMCBType)
                return strBMCAName.CompareTo(strBMCBName);
            else if (objBMCAType == OrganisationUnitType.Entity &&
                objBMCBType == OrganisationUnitType.Group)
            {
                // if the CM A is in an entity and CM B is in a group, sort the entity after the group
                return -1;
            }
            else
            {
                return 1;
            }
        }

        #endregion
    }
}