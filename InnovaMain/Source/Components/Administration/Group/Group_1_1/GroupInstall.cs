using System;

namespace Oritax.TaxSimp.CM.Group
{
	/// <summary>
	/// Summary description for GroupInstall.
	/// </summary>
	public class GroupInstall
	{
		#region INSTALLATION PROPERTIES

		// Assembly Installation Properties
		public const string ASSEMBLY_ID="052FA5EF-697A-481F-A45F-CE32B4C23335";
		public const string ASSEMBLY_NAME="Group_1_1";
		public const string ASSEMBLY_DISPLAYNAME="Group V1.1";
		public const string ASSEMBLY_MAJORVERSION="1";
		public const string ASSEMBLY_MINORVERSION="1";
		public const string ASSEMBLY_DATAFORMAT="0";
        public const string ASSEMBLY_REVISION="3";		//2005.1 

		// Component Installation Properties
		public const string COMPONENT_ID="196EED92-D604-4b07-BF49-C5C322400A05";
		public const string COMPONENT_NAME="Group";
		public const string COMPONENT_DISPLAYNAME="Group";
		public const string COMPONENT_CATEGORY="BusinessStructure";

		// Component Version Installation Properties
		public const string COMPONENTVERSION_STARTDATE="01/01/1990 00:00:00 AM";
		public const string COMPONENTVERSION_ENDDATE="31/12/2050 12:59:59 AM";
		public const string COMPONENTVERSION_PERSISTERASSEMBLY="CommonPersisters_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";
		public const string COMPONENTVERSION_PERSISTERCLASS="Oritax.TaxSimp.CommonPersistence.BlobPersister";
		public const string COMPONENTVERSION_IMPLEMENTATIONCLASS="Oritax.TaxSimp.CM.Group.GroupCM";
    
        #endregion
	}
}
