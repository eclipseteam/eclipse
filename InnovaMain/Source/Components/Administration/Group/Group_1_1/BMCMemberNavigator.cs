using System;
using System.Collections;
using Oritax.TaxSimp.BusinessStructureUtilities;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.CalculationInterface;

namespace Oritax.TaxSimp.CM.Group
{
    public class BMCMemberNavigator : BusinessStructureNavigatorVisitor
    {
 
        #region Private variables------------------------------------------------------------------

        private Hashtable visitedMemberList = new Hashtable(); 
        private string typeName = String.Empty;
        private ArrayList bmcCollection = new ArrayList(); 
        
        #endregion

        #region Public properties------------------------------------------------------------------
        /// <summary>
        /// returns ans sets visitedMemberList
        /// </summary>
        public Hashtable VisitedMemberList
        {
            get
            {
                return this.visitedMemberList;
            }
            set
            {
                this.visitedMemberList = value;
            }
        }
        /// <summary>
        /// returns ans sets visitedMemberList
        /// </summary>
        public ArrayList BMCCollection
        {
            get
            {
                return this.bmcCollection;
            }
        }
        #endregion 

        /// <summary>
		/// Default constructor. 
		/// </summary>
		/// <param name="BrokerInstance"></param>
		/// <param name="startTime"></param>
		/// <param name="endTime"></param>
        public BMCMemberNavigator(ICMBroker BrokerInstance, DateTime startTime, DateTime endTime, string typeName)
            : base(BrokerInstance, startTime, endTime)
		{
            CalculationModuleTypes = new string[] { typeName };
		}

        #region Overrideble Properties and methods-------------------------------------------------
        
        /// <summary>
        /// The following method will always return true, implementation was not required at this point for ATO MIGRATION PUSH DOWN
        /// </summary>
        /// <param name="typeName"></param>
        /// <returns></returns>
        public override bool ShouldVisitCalculationModule(string typeName)
        {
            return base.ShouldVisitCalculationModule(typeName); 
        }

        /// <summary>
        /// The following method visit's group object. 
        /// 1. It adds information in VisitedMember hashtable that reporting unit has already been visited
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public override void VisitGroupNode(object sender, GroupNodeEventArgs e)
        {
        }
        /// <summary>
        /// The following method visit's entity object. 
        /// 1. It adds information in VisitedMember hashtable that reporting unit has already been visited
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        public override void VisitEntityNode(object sender, EntityNodeEventArgs e)
        {
        }
        /// <summary>
        /// Check current level
        /// </summary>
        /// <param name="currentLevel"></param>
        /// <returns></returns>
        public override bool ShouldNavigateToLowerLevels(BusinessStrucutureNavigatorLevel currentLevel)
        {
            return (currentLevel != BusinessStrucutureNavigatorLevel.CalculationModule) && (this.GroupLevelsNavigated <= 1);
        }
        /// <summary>
        /// The following method checks if navigator object should visit period node. The check is to make sure that navigator is only
        /// visiting objects period which are of same dates. 
        /// </summary>
        /// <param name="periodDate"></param>
        /// <returns></returns>
        public override bool ShouldVisitPeriodNode(PeriodSpecifier periodDate)
        {
            return (periodDate.EndDate == this.PeriodDate.EndDate && periodDate.StartDate == this.PeriodDate.StartDate);
        }
        /// <summary>
        /// In this instance when calculation module is AustraliaITLCM then getdata will be called to extractData
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public override void VisitCalculationNode(object sender, Oritax.TaxSimp.CalculationInterface.CalculationModuleNodeEventArgs e)
        {
            this.bmcCollection.Add(e.CalculationModule); 
        }

        public override bool ShouldVisitReportingUnitNode(Guid reportingUnitCLID, Guid reportingUnitCSID)
        {
            return this.GroupLevelsNavigated < 1;
        } 
        #endregion 
        
      	#region Support methods--------------------------------------------------------------------
		/// <summary>
		/// Checks if reporting unit is not in exception list. 
		/// </summary>
		/// <param name="CLID"></param>
		/// <param name="CSID"></param>
		/// <returns></returns>
		private bool IsInExceptionList(Guid CLID, Guid CSID)
		{
			return this.visitedMemberList.Contains(GetMemberName(CSID,CLID));
		}

	    #endregion 
    }
}
