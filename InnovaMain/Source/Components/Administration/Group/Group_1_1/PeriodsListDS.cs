using System;
using System.Data;

namespace Oritax.TaxSimp.CM.Group
{
	/// <summary>
	/// PeriodsDS is a DataSet that contains data for listing periods within an entity.
	/// </summary>
	[Serializable] 
	public class PeriodsListDS : DataSet
	{
		public const String PERIODS_TABLE			= "PERIODS_TABLE";
		public const String PERIODNAME_FIELD		= "PERIODNAME_FIELD";
		public const String PERIODCLID_FIELD		= "PERIODCLID_FIELD";
		public const String PERIODCSID_FIELD		= "PERIODCSID_FIELD";
		public const String PERIODCIID_FIELD		= "PERIODCIID_FIELD";

		private string groupName;
		private string breadCrumb;
		string			cSID;
		string			cIID;
		string			cLID;

		public PeriodsListDS()
		{
			DataTable table;

			table = new DataTable(PERIODS_TABLE);
			table.Columns.Add(PERIODNAME_FIELD, typeof(System.String));
			table.Columns.Add(PERIODCLID_FIELD, typeof(System.String));
			table.Columns.Add(PERIODCSID_FIELD, typeof(System.String));
			table.Columns.Add(PERIODCIID_FIELD, typeof(System.String));

			this.Tables.Add(table);

			groupName="";
			breadCrumb="";
		}

		public string GroupName{get{return groupName;}set{groupName=value;}}
		public string BreadCrumb{get{return breadCrumb;}set{breadCrumb=value;}}
		public string CSID {get{return cSID;}set{cSID=value;}}
		public string CIID	{get{return cIID;}set{cIID=value;}}
		public string CLID	{get{return cLID;}set{cLID=value;}}
	}
}

