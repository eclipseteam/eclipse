using System;
using System.Data;

using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.CM.Group
{
	/// <summary>
	/// DSCalculationModule is a DataSet that contains all of the persistent data for a CM instance.
	/// </summary>
	[Serializable] 
	public class GroupMembersDS : BusinessStructureWorkpaperDS
	{
		public const String AVAILABLEENTITIES_TABLE			= "AVAILABLEENTITIES_TABLE";
		public const String INCLUDEDENTITIES_TABLE			= "INCLUDEDENTITIES_TABLE";
		public const String ENTITYNAME_FIELD				= "ENTITYNAME_FIELD";
		public const String ENTITYCLID_FIELD				= "ENTITYCLID_FIELD";
        public const String ENTITYCTID_FIELD                = "ENTITYCTID_FIELD";
        public const String ENTITYCOMPDISPLAYNAME_FIELD     = "ENTITYCOMPDISPLAYNAME_FIELD";
        public const String ENTITYCIID_FIELD                = "ENTITYCIID_FIELD";
	    public const String ENTITYCSID_FIELD				= "ENTITYCSID_FIELD";
		public const String TAXSTATUS_FIELD					= "TAXSTATUS_FIELD";
		public const String ADJUSTMENTENTITY_FIELD			= "ADJUSTMENTENTITY_FIELD";
        public string AVAILABLEFILTER = string.Empty;
		private string m_strBreadCrumb;
        public GroupMemberOperations GroupMemberOperations = GroupMemberOperations.None; 
        public string ConfigureWorkpaper = String.Empty;

		public GroupMembersDS():base()
		{
			DataTable table;

			table = new DataTable(AVAILABLEENTITIES_TABLE);
			table.Columns.Add(ENTITYNAME_FIELD, typeof(System.String));
			table.Columns.Add(ENTITYCLID_FIELD, typeof(System.String));
            table.Columns.Add(ENTITYCTID_FIELD, typeof(System.String));
            table.Columns.Add(ENTITYCSID_FIELD, typeof(System.String));
            table.Columns.Add(ENTITYCIID_FIELD, typeof(System.String));
            table.Columns.Add(ENTITYCOMPDISPLAYNAME_FIELD, typeof(System.String));
            this.Tables.Add(table);

			table = new DataTable(INCLUDEDENTITIES_TABLE);
			table.Columns.Add(ENTITYNAME_FIELD, typeof(System.String));
            DataColumn clidField = table.Columns.Add(ENTITYCLID_FIELD, typeof(System.String));
			table.Columns.Add(ENTITYCSID_FIELD, typeof(System.String));
			table.Columns.Add(TAXSTATUS_FIELD, typeof(System.String));
			table.Columns.Add(ADJUSTMENTENTITY_FIELD, typeof(System.String));
            table.PrimaryKey = new DataColumn[] { clidField };
			this.Tables.Add(table);
		}

        public static string GetRowFilterAllClients()
        {
            string filter = string.Empty;
            filter = "CTID ='b95e5a7b-8c82-4c05-934d-f6ab7d95ae41' OR " +
                                                "CTID ='96c93d8f-9d3d-4989-89cf-cff47d377b43' OR " +
                                                "CTID ='e9d16670-d8f9-4a2d-844b-4ebe67e4629f' OR " +
                                                "CTID ='2a2a95ac-7933-40a6-b6ba-1597cb18014f' OR " +
                                                "CTID ='815051a3-d82d-41b6-8536-147d8a2055d2' OR " +
                                                "CTID ='6660cead-ae7c-485e-af8e-622fa54f7af2' OR " +
                                                "CTID ='8256f2aa-3ccc-43b3-a683-90c8b603934a' OR " +
                                                "CTID ='aba693e9-be2f-4228-a300-92100d418f9d' OR " +
                                                "CTID ='7485432c-a44b-4e8b-ad73-9778e071de2d'";
            return filter;
        }

		public string BreadCrumb
		{
			get
			{
				return this.m_strBreadCrumb;
			}
			set
			{
				this.m_strBreadCrumb = value;
			}
		}
	}
}

