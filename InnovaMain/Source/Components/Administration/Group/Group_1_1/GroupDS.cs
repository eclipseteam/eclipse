#region CHANGE_HISTORY
/*
 $Header: /2002/3. Implementation/NewConstruction/Components/BusinessStructure/Group/Group-1-0/GroupDS.cs 1     17/08/03 1:09p Secampbell $
 $History: GroupDS.cs $
 * 
 * *****************  Version 1  *****************
 * User: Secampbell   Date: 17/08/03   Time: 1:09p
 * Created in $/2002/3. Implementation/NewConstruction/Components/BusinessStructure/Group/Group-1-0
 */ 
#endregion

using System;
using System.Data;
using System.Data.SqlClient;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.CM.OrganizationUnit;


namespace Oritax.TaxSimp.CM.Group
{
	/// <summary>
	/// Summary description for DBUserDS.
	/// </summary>
	[Serializable]
	public class GroupDS : OrganizationUnitDS
	{
		public const String GROUP_TABLE				= "GROUPCM";
		public const String INCLUDEDMEMBERS_TABLE	= "INCLUDEDGROUPMEMBERS";
		public const String AVAILABLEMEMBERS_TABLE	= "AVAILABLEGROUPMEMBERS";
		
		/* GroupCM table constants */
		public const String ENTITYCLID_FIELD		= "ENTITYCLID";
		public const String ENTITYCSID_FIELD		= "ENTITYCSID";

		/* GroupMembers table(s) constants */
		public const String GROUPCLID_FIELD			= "GROUPCLID";
		public const String GROUPCSID_FIELD			= "GROUPCSID";
		public const String ISDELETED_FIELD			= "ISDELETED";
		public const String ADJUSTMENT_FIELD		= "ADJUSTMENT";


		public GroupDS() : base()
		{
			DataTable dt;

			dt = new DataTable( GROUP_TABLE );
			dt.Columns.Add( new DataColumn( ENTITYCLID_FIELD, typeof( System.Guid ) ) );
			dt.Columns.Add( new DataColumn( ENTITYCSID_FIELD, typeof( System.Guid ) ) );
			this.Tables.Add( dt );
			BrokerManagedComponentDS.AddVersionStamp(dt,typeof(GroupDS));

			dt = new DataTable( INCLUDEDMEMBERS_TABLE );
			dt.Columns.Add( new DataColumn( GROUPCLID_FIELD, typeof( System.Guid ) ) );
			dt.Columns.Add( new DataColumn( GROUPCSID_FIELD, typeof( System.Guid ) ) );
			dt.Columns.Add( new DataColumn( ISDELETED_FIELD, typeof( System.Boolean ) ) );
			dt.Columns.Add( new DataColumn( ADJUSTMENT_FIELD, typeof( System.Boolean ) ) );
			this.Tables.Add( dt );
			BrokerManagedComponentDS.AddVersionStamp(dt,typeof(GroupDS));

			dt = new DataTable( AVAILABLEMEMBERS_TABLE );
			dt.Columns.Add( new DataColumn( GROUPCLID_FIELD, typeof( System.Guid ) ) );
			dt.Columns.Add( new DataColumn( GROUPCSID_FIELD, typeof( System.Guid ) ) );
			dt.Columns.Add( new DataColumn( ISDELETED_FIELD, typeof( System.Boolean ) ) );
			dt.Columns.Add( new DataColumn( ADJUSTMENT_FIELD, typeof( System.Boolean ) ) );
			this.Tables.Add( dt );
			BrokerManagedComponentDS.AddVersionStamp(dt,typeof(GroupDS));
		}

		public static new string GetDataSetVersion(DataSet data)
		{
			return GetDataSetVersion(data, "GROUPCM").ToString();
		}

		/// <summary>
		///		Convert a V1.0.3.0 dataset to a V1.1.0.0 dataset
		/// </summary>
		/// <param name="data">The orginal untampered dataset</param>
		/// <param name="convData">A converted dataset passed in from a previous conversion function</param>
		/// <returns>A converted dataset</returns>
		public static DataSet ConvertV_1_0_3_0toV_1_1_0_0DataSet(DataSet data, DataSet convData)
		{
			if(convData == null)
				return null;

			string ver = GetDataSetVersion(convData);

			//if not the appropriate verion for correction then just return back
			if(ver != "1.0.3.0")
				return convData;
			
			//the conversion is merely changing the full name to the correct version number.
			ver = "1.1.0.0";

			convData.Tables["GROUPCM"].ExtendedProperties["FullName"] = ver;
			convData.Tables["INCLUDEDGROUPMEMBERS"].ExtendedProperties["FullName"] = ver;
			convData.Tables["AVAILABLEGROUPMEMBERS"].ExtendedProperties["FullName"] = ver;
			
			return convData;
		}
	}
}
			