using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Runtime.Serialization;
using System.Security.Permissions;
using Oritax.TaxSimp.BusinessStructureUtilities;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.Entity;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.CM.Period;
using Oritax.TaxSimp.DSCalculation;
using Oritax.TaxSimp.Exceptions;
using Oritax.TaxSimp.SAPBase;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Common;
using System.Linq;
using Oritax.TaxSimp.IOrganizationUnits;

namespace Oritax.TaxSimp.CM.Group
{
	/// <summary>
	/// Summary description for CalculationModule.
	/// </summary>
	///
	[Serializable]
	public partial class GroupCM : OrganizationUnitCM, IGroup, IOrganizationUnit, IBusinessStructureModule
	{
        internal const string GroupMembershipConfirmAddKey = "GroupMembershipConfirmAddTimeIntensiveTask";
        internal const string GroupMembershipConfirmRemoveKey = "GroupMembershipConfirmRemoveTimeIntensiveTask";
        internal const string MemberScenarioChangeConfirmKey = "MemberScenarioChangeConfirmTimeIntensiveTask";
        internal const string CreateScenarioConfirmKey = "CreateScenarioConfirmTimeIntensiveTask";

		#region INTERNAL CLASSES
		#endregion
		#region STATEFUL FIELD VARIABLES
		//USING CUSTOM SERIALIZATION.
		//Still mark the fields as [NonSerialized] for your own infomation but this is ignored now by .NET
		//You must add your own serialzation code for each instance
		//See GetObjectData & Special Constructor
		OrganizationUnit.GroupMembers includedGroupMembers;
		OrganizationUnit.GroupMembers availableGroupMembers;
		private Guid m_objConsolidationAdjustmentEntityCLID;
		private Guid m_objConsolidationAdjustmentEntityCSID;
		#endregion
		#region PROPERTIES
		public OrganizationUnit.GroupMembers IncludedGroupMembers{get{return includedGroupMembers;}}

		public override ISubstitutedAccountingPeriods SAPs
		{
			get
			{
				return null;
			}
		}

		public Guid ConsolidationAdjustmentEntityCLID
		{
			get
			{
				return this.m_objConsolidationAdjustmentEntityCLID;
			}
			set
			{
				this.m_objConsolidationAdjustmentEntityCLID = value;
			}
		}

		public Guid ConsolidationAdjustmentEntityCSID
		{
			get
			{
				return this.m_objConsolidationAdjustmentEntityCSID;
			}
			set
			{
				this.m_objConsolidationAdjustmentEntityCSID = value;
			}
		}
		#endregion
		#region CONSTRUCTORS
		public GroupCM()
		{
			includedGroupMembers=new OrganizationUnit.GroupMembers();
			availableGroupMembers=new OrganizationUnit.GroupMembers();
		}
		protected GroupCM(SerializationInfo si, StreamingContext context)
			:base(si,context)
		{
			includedGroupMembers = (OrganizationUnit.GroupMembers)Serialize.GetSerializedValue(si,"grp_includedGroupMembers",typeof(OrganizationUnit.GroupMembers),new OrganizationUnit.GroupMembers());
			availableGroupMembers = (OrganizationUnit.GroupMembers)Serialize.GetSerializedValue(si,"grp_availableGroupMembers",typeof(OrganizationUnit.GroupMembers),new OrganizationUnit.GroupMembers());
			m_objConsolidationAdjustmentEntityCLID=Serialize.GetSerializedGuid(si,"grp_m_objConsolidationAdjustmentEntityCLID");
			m_objConsolidationAdjustmentEntityCSID=Serialize.GetSerializedGuid(si,"grp_m_objConsolidationAdjustmentEntityCSID");
		}
	
		//Only allow the .NET Serialization core code to call this function
		[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]
		public override void GetObjectData(SerializationInfo si, StreamingContext context)
		{
			base.GetObjectData(si,context);
			Serialize.AddSerializedValue(si,"grp_includedGroupMembers",includedGroupMembers);
			Serialize.AddSerializedValue(si,"grp_availableGroupMembers",availableGroupMembers);
			Serialize.AddSerializedValue(si,"grp_m_objConsolidationAdjustmentEntityCLID",m_objConsolidationAdjustmentEntityCLID);
			Serialize.AddSerializedValue(si,"grp_m_objConsolidationAdjustmentEntityCSID",m_objConsolidationAdjustmentEntityCSID);
		}

		#endregion
		#region OVERRIDE MEMBERS FOR CM BEHAVIOR
		public override IEnumerable Subscriptions
		{
			get
			{
				return new string[]{
									   "StreamControl"
								   };
			}
		}

		public override void Initialize( String cmName )
		{
			base.Initialize( cmName );
			this.ConsolidationAdjustmentEntityCLID = Guid.Empty;
			this.ConsolidationAdjustmentEntityCSID = Guid.Empty;
		}
		
		public Guid CreateNewScenario(DataSet data,bool setAsCurrent)
		{
			return Guid.Empty;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="groupStructureDS">DataSet to store the Group Structure.</param>
		/// <param name="rowIDCounter">Unique row counter- use in Cystal report</param>
		/// <param name="parentRowID">RowIDCounter of the parent row  that this child row belongs to.</param>
		/// <param name="level">Current Level of group structure.</param>
		/// <param name="csid">Reporting Unit Group/Sub group CSID</param>
		/// <param name="clid">Reporting Unit Group/Sub group CLID</param>
		/// <param name="parentRUName">Reporting Unit name of the parent Reporting unit.</param>
		/// <returns>Returns int rowIDCounter.</returns>
		protected int LoadGroupStructure(GroupStructureDS groupStructureDS, int rowIDCounter ,int parentRowID,int level, Guid csid, Guid clid,string parentRUName)
		{
			GroupMembersDS objGroupMembers = new  GroupMembersDS();

			objGroupMembers.Username = groupStructureDS.UserName;
			GetAllGroupMember(objGroupMembers,clid,csid);

			// get the information for included entities,
			DataTable objIncludedEntitiesTable = objGroupMembers.Tables[GroupMembersDS.INCLUDEDENTITIES_TABLE];
			DataView objIncludedEntitiesView   = new DataView( objIncludedEntitiesTable );
			objIncludedEntitiesView.RowFilter  = GroupMembersDS.ADJUSTMENTENTITY_FIELD + " <> '" + bool.TrueString + "'";
			objIncludedEntitiesView.Sort = GroupMembersDS.ENTITYNAME_FIELD + " ASC";

			foreach(DataRowView drow in objIncludedEntitiesView )
			{
				string unitType = string.Empty;	
				string taxStatus = string.Empty;
				bool isGroup = false;
				IOrganizationUnit objEntityCM;
				ILogicalModule objEntityLM;

				string memberCSID =drow[GroupMembersDS.ENTITYCSID_FIELD].ToString();
				string memberCLID =drow[GroupMembersDS.ENTITYCLID_FIELD].ToString();

				rowIDCounter++;

				objEntityLM = this.Broker.GetLogicalCM(new Guid(memberCLID));
				objEntityCM = (IOrganizationUnit)objEntityLM[ new Guid(memberCSID)];

				this.Broker.ReleaseBrokerManagedComponent(objEntityLM);

				// Check for duplicate Entry
				if(groupStructureDS.isDuplicateEntry(memberCLID))
                    groupStructureDS.AddToExceptionList(memberCSID,memberCLID,level.ToString(),drow[GroupMembersDS.ENTITYNAME_FIELD].ToString(),parentRUName); 

				if (objEntityCM is IGroup)
					isGroup=true;
				else
					isGroup=false;

				#region Show only Reporting Unit Names
				if((!groupStructureDS.ViewTaxStatus) && (!groupStructureDS.ViewUnitType) && (!groupStructureDS.ViewMemberScenario))
				{
					if (level==1)
					{
						// Add Only Name
						groupStructureDS.AddRow(rowIDCounter.ToString(),
							level.ToString(),
							rowIDCounter.ToString(),
							drow[GroupMembersDS.ENTITYNAME_FIELD].ToString(),
							string.Empty,
                            string.Empty,
                            string.Empty, 
                            "0", isGroup, memberCSID, memberCLID);
					}
					else
					{
						// Add Only Name
						groupStructureDS.AddChildRow(rowIDCounter.ToString(),
							parentRowID.ToString(),
							level.ToString(),
							rowIDCounter.ToString(),
							drow[GroupMembersDS.ENTITYNAME_FIELD].ToString(),
							string.Empty,
                            string.Empty,
                            string.Empty, 
                            "0", isGroup, memberCSID, memberCLID);
					}
				}
				#endregion

				#region check which check boxes are checked
				if(groupStructureDS.ViewTaxStatus)
				{

					if (drow[GroupMembersDS.TAXSTATUS_FIELD].Equals(string.Empty))
						taxStatus = "-";
					else
						taxStatus =drow[GroupMembersDS.TAXSTATUS_FIELD].ToString();

					#region Issue TXE200007714
					// this is a CQ issues requirement.
					ILogicalModule lCM = this.Broker.GetLogicalCM(new Guid(drow[GroupMembersDS.ENTITYCLID_FIELD].ToString()));
					DataRowCollection objSourceEntityTypes = (DataRowCollection) this.Owner.GetCMTypeList( "BusinessEntity" );
					
					foreach(DataRow drowUnitType in objSourceEntityTypes)
					{
						if(drowUnitType["CMTYPEID"].Equals(lCM.CMType))
							unitType = drowUnitType["DISPLAYNAME"].ToString();
					}
					
					this.Broker.ReleaseBrokerManagedComponent(lCM);

					if (unitType.Equals("Australian Division of an entity"))
						taxStatus ="-";
					#endregion

					if (level==1)
					{
						groupStructureDS.AddRow(rowIDCounter.ToString(),
							level.ToString(),
							rowIDCounter.ToString(),
							drow[GroupMembersDS.ENTITYNAME_FIELD].ToString(),
							string.Empty,
							"Tax Status",	
							taxStatus,"2",isGroup,memberCSID ,memberCLID);
					}
					else
					{
						groupStructureDS.AddChildRow(rowIDCounter.ToString(),parentRowID.ToString(), 
							level.ToString(),
							rowIDCounter.ToString(),
							drow[GroupMembersDS.ENTITYNAME_FIELD].ToString(),
							string.Empty,
							"Tax Status",	
							taxStatus,"2",isGroup,memberCSID ,memberCLID);
					}
				}

				if(groupStructureDS.ViewUnitType)
				{
					ILogicalModule lCM = this.Broker.GetLogicalCM(new Guid(drow[GroupMembersDS.ENTITYCLID_FIELD].ToString()));
					DataRowCollection objSourceEntityTypes = (DataRowCollection) this.Owner.GetCMTypeList( "BusinessEntity" );
					
					foreach(DataRow drowUnitType in objSourceEntityTypes)
					{
						if(drowUnitType["CMTYPEID"].Equals(lCM.CMType))
							unitType = drowUnitType["DISPLAYNAME"].ToString();
					}

					this.Broker.ReleaseBrokerManagedComponent(lCM);

					if (level==1)
					{
						groupStructureDS.AddRow(rowIDCounter.ToString(),
							level.ToString(),
							rowIDCounter.ToString(),
							drow[GroupMembersDS.ENTITYNAME_FIELD].ToString(),
							string.Empty,
							"Unit Type",	
							unitType  ,"1",isGroup,memberCSID ,memberCLID);
					}
					else
					{
						groupStructureDS.AddChildRow(rowIDCounter.ToString(),parentRowID.ToString(), 
							level.ToString(),
							rowIDCounter.ToString(),
							drow[GroupMembersDS.ENTITYNAME_FIELD].ToString(),
							string.Empty,
							"Unit Type",	
							unitType ,"1",isGroup,memberCSID ,memberCLID);
					}
				}

				if(groupStructureDS.ViewMemberScenario )
				{
					ILogicalModule groupMemberLM = this.Broker.GetLogicalCM(new Guid(drow[GroupMembersDS.ENTITYCLID_FIELD].ToString()));
					CMScenario cMScenario = groupMemberLM.GetScenario(new Guid(drow[GroupMembersDS.ENTITYCSID_FIELD].ToString()));

					this.Broker.ReleaseBrokerManagedComponent(groupMemberLM);

					if (level==1)
					{
						groupStructureDS.AddRow(rowIDCounter.ToString(),
							level.ToString(),
							rowIDCounter.ToString(),
							drow[GroupMembersDS.ENTITYNAME_FIELD].ToString(),
							string.Empty,
							"Member Scenario",	
							cMScenario.Name ,"3",isGroup,memberCSID ,memberCLID);
					}
					else
					{
						groupStructureDS.AddChildRow(rowIDCounter.ToString(),parentRowID.ToString(),
							level.ToString(),
							rowIDCounter.ToString(),
							drow[GroupMembersDS.ENTITYNAME_FIELD].ToString(),
							string.Empty,
							"Member Scenario",	
							cMScenario.Name ,"3",isGroup,memberCSID ,memberCLID);
					}
				}
				#endregion

				#region Test if Reporting unit is a Group/SubGroup 
				if(objEntityCM is IGroup)
				{
					//level;
					int paramLevel = level;
					paramLevel++;
					rowIDCounter = LoadGroupStructure(
						groupStructureDS,
						rowIDCounter,
						rowIDCounter, 
						paramLevel,
						new Guid(drow[GroupMembersDS.ENTITYCSID_FIELD].ToString()),
						new Guid(drow[GroupMembersDS.ENTITYCLID_FIELD].ToString()),drow[GroupMembersDS.ENTITYNAME_FIELD].ToString());
				}
				#endregion

				this.Broker.ReleaseBrokerManagedComponent(objEntityCM);
			}
			return rowIDCounter;
		}
		
		/// <summary>
		/// This method will get all the members allocated to the passed in Group.
		/// </summary>
		/// <param name="data">GroupMembersDS: Will be popluated will the included group memeber of the passed in Reporting Unit Group</param>
		/// <param name="clid">Reporting Unit Group CLID</param>
		/// <param name="csid">Reporting Unit Group CSID</param>
		protected void GetAllGroupMember(GroupMembersDS data,Guid clid, Guid csid)
		{
			ICalculationModule reportingUnit = this.Broker.GetCMImplementation(clid,csid);

			if(reportingUnit is IGroup)
			{
				OrganizationUnit.GroupMembers groupMembers = ((IGroup)reportingUnit).IncludedGroupMembers;

				this.Broker.ReleaseBrokerManagedComponent(reportingUnit);
 
				DataTable groupMembersTable =  data.Tables[GroupMembersDS.INCLUDEDENTITIES_TABLE]; 

				foreach(DictionaryEntry dictionarEntry in groupMembers)
				{
					OrganizationUnit.GroupMember gMember = (GroupMember) dictionarEntry.Value;

					Guid groupMemberCLID = new Guid(gMember.CLID.ToString());
					Guid groupMemberCSID = new Guid(gMember.CSID.ToString());

					ILogicalModule lCM = this.Broker.GetLogicalCM(groupMemberCLID);
					
					ICalculationModule businessEntityCM = lCM[groupMemberCSID];

					this.Broker.ReleaseBrokerManagedComponent(lCM);

					string strCalculatedName = businessEntityCM.Name;
				
					ILogicalModule objEntityLM = this.Broker.GetLogicalCM(groupMemberCLID);
					IOrganizationUnit objEntityCM = (IOrganizationUnit) objEntityLM[groupMemberCSID];

					this.Broker.ReleaseBrokerManagedComponent(objEntityLM);

					if ( gMember.Adjustment.Equals(false))
					{
						string sTaxStatus = string.Empty;

						if(!(objEntityCM is  IGroup))
							sTaxStatus = ((IEntity)businessEntityCM).TaxStatusName.ToString();
						else
							sTaxStatus = "-";

						DataRow drow = groupMembersTable.NewRow();
					
						drow[GroupMembersDS.ENTITYNAME_FIELD] = strCalculatedName;
						drow[GroupMembersDS.ENTITYCLID_FIELD] = groupMemberCLID.ToString();
						drow[GroupMembersDS.ENTITYCSID_FIELD] = groupMemberCSID.ToString();
						drow[GroupMembersDS.TAXSTATUS_FIELD]  = sTaxStatus;

						drow[GroupMembersDS.ADJUSTMENTENTITY_FIELD]= gMember.Adjustment;

						groupMembersTable.Rows.Add(drow); 
					}

					this.Broker.ReleaseBrokerManagedComponent(businessEntityCM);
					this.Broker.ReleaseBrokerManagedComponent(objEntityCM);
				}
			}
		}

		#region OnGetData Methods
		/// <summary>
		/// This method will load the group structure for a given Reporting unit group. Based on CLID and CSID of the selected reporting unit.
		/// </summary>
		/// <param name="data">Dataset for the group structure.</param>
		protected virtual void OnGetData(GroupStructureDS data)
		{
			#region Add the Group Selected 
			Guid csid = new Guid(data.ThisGroupCSID);
			Guid clid = new Guid(data.ThisGroupCLID);

			ILogicalModule lCM = this.Broker.GetLogicalCM(clid);
			ICalculationModule businessEntityCM = lCM[csid];

			string strCalculatedName = businessEntityCM.Name;
			data.BreadCrumbName =  businessEntityCM.Name;

			ILogicalModule mgroupMemberLM = this.Broker.GetLogicalCM(clid);
			CMScenario mcMScenario = mgroupMemberLM.GetScenario(csid);
			data.BreadCrumbScenario = mcMScenario.Name;

			data.Tables[ GroupStructureDS.BREADCRUMBTABLE ].Rows[0][GroupStructureDS.BREADCRUMBFIELD]=((GroupStructureDS)data).GetBreadCrumb;
			data.Tables[ GroupStructureDS.BREADCRUMBTABLE ].AcceptChanges();

			this.Broker.ReleaseBrokerManagedComponent(mgroupMemberLM);
			this.Broker.ReleaseBrokerManagedComponent(businessEntityCM);
			
			if((!data.ViewTaxStatus) && (!data.ViewUnitType) && (!data.ViewMemberScenario))
                data.AddRow("0", "0", "0", strCalculatedName, string.Empty, string.Empty, string.Empty, "0", true, csid.ToString(), clid.ToString());

			#region Scenario Name
			// ViewMemberScenario
			if(data.ViewMemberScenario )
			{
				ILogicalModule groupMemberLM = this.Broker.GetLogicalCM(clid);
				CMScenario cMScenario = groupMemberLM.GetScenario(csid);
				this.Broker.ReleaseBrokerManagedComponent(groupMemberLM);

				data.AddRow("0",
						"0",
						"0",
						strCalculatedName,
						string.Empty,
						"Member Scenario",	
						cMScenario.Name ,"3",true,csid.ToString() ,clid.ToString());
			}
			#endregion

			#region Tax Status
			// TaxStatus
			if(data.ViewTaxStatus )
			{

				data.AddRow("0",
					"0",
					"0",
					strCalculatedName,
					string.Empty,
					"Tax Status",	
					"-","2",true,csid.ToString() ,clid.ToString());
			}
			#endregion

			#region Unit Type
			// UnitType
			if(data.ViewUnitType )
			{
				string unitType= string.Empty;

				DataRowCollection objSourceEntityTypes = (DataRowCollection) this.Owner.GetCMTypeList( "BusinessEntity" );

				foreach(DataRow drow in objSourceEntityTypes)
				{
					if(drow["CMTYPEID"].Equals(lCM.CMType))
						unitType = drow["DISPLAYNAME"].ToString();
				}

				data.AddRow("0",
					"0",
					"0",
					strCalculatedName,
					string.Empty,
					"Unit Type",	
					unitType ,"1",true,csid.ToString() ,clid.ToString());
			}
			#endregion

			#endregion
			
			this.Broker.ReleaseBrokerManagedComponent(lCM);
    
			#region Load group structure
			// Load the Group Structure Recursively
			this.LoadGroupStructure(
				data,
				0,
				0,
				1,
				new Guid(data.ThisGroupCSID),
				new Guid(data.ThisGroupCLID),strCalculatedName); 

			// Find and set all duplicate entries to true
			data.HighlightDuplicateEntries();
			#endregion
		}

		protected void OnGetData( GroupMembersDS data )
		{
			GroupMembersDS objGroupMembers = (GroupMembersDS) data;
			objGroupMembers.BreadCrumb = this.ConstructBreadcrumb();

			//Get the name of the configuration workpaper for the group type
			//so that screens can redirect back to it.
			objGroupMembers.ConfigureWorkpaper = this.ConfigureWPURL;

			// List the available business entities (groups and entities)
			// Get the organization CM so that the entities and groups can be listed
			objGroupMembers.Tables[GroupMembersDS.INCLUDEDENTITIES_TABLE].Rows.Clear();

			ICalculationModule orgCM=Broker.GetWellKnownCM(WellKnownCM.Organization);
				
			DataTable logicalModuleRowsTable= this.Broker.GetLCMsByParent(orgCM.CLID);
            DataView logicalModuleRowsViewFiltered = new DataView(logicalModuleRowsTable);
            //Frist Filter out entities type
            if (data.GroupMemberOperations == GroupMemberOperations.AllClients)
                logicalModuleRowsViewFiltered.RowFilter = GroupMembersDS.GetRowFilterAllClients();

            DataView logicalModuleRowsView = new DataView(logicalModuleRowsViewFiltered.ToTable());

            logicalModuleRowsView.RowFilter = objGroupMembers.AVAILABLEFILTER;
            DataTable logicalModuleFilteredTable = logicalModuleRowsView.ToTable(); 

			this.Broker.ReleaseBrokerManagedComponent(orgCM);

            foreach (DataRow logicalModuleRow in logicalModuleFilteredTable.Rows)
			{
				Guid entityCLID=(Guid)logicalModuleRow[DSLogicalModulesInfo.LCM_CLID_FIELD];
                Guid entityCTID = (Guid)logicalModuleRow[DSLogicalModulesInfo.LCM_TYPEID_FIELD];
                GroupMember objGroupMember = this.includedGroupMembers[entityCLID];
                ICalculationModule businessEntityCM;

                if (null == objGroupMember || objGroupMember.IsDeleted)
                {
                    if (entityCTID != new Guid("5A5BC2D6-50CB-412A-B151-BF8E16E17403"))//If it is a consolidation adjustment entity then stop it from being listed in available entities
                    {
                        DataRow entityRow = objGroupMembers.Tables[GroupMembersDS.AVAILABLEENTITIES_TABLE].NewRow();
                        entityRow[GroupMembersDS.ENTITYNAME_FIELD] = logicalModuleRow[DSLogicalModulesInfo.LCM_INSTANCENAME_FIELD].ToString();
                        entityRow[GroupMembersDS.ENTITYCLID_FIELD] = entityCLID;
                        entityRow[GroupMembersDS.ENTITYCOMPDISPLAYNAME_FIELD] = logicalModuleRow[DSLogicalModulesInfo.LCM_COMPDISPLAYNAME_FIELD];
                        entityRow[GroupMembersDS.ENTITYCIID_FIELD] = logicalModuleRow[DSLogicalModulesInfo.LCM_CID_FIELD];
                        entityRow[GroupMembersDS.ENTITYCLID_FIELD] = entityCLID;

                        objGroupMembers.Tables[GroupMembersDS.AVAILABLEENTITIES_TABLE].Rows.Add(entityRow);
                    }
                }
                else //This entity is part of the group
                {
                    ILogicalModule lCM = this.Broker.GetLogicalCM(entityCLID);

                    businessEntityCM = lCM[objGroupMember.CSID];

                    DataRow entityRow = objGroupMembers.Tables[GroupMembersDS.INCLUDEDENTITIES_TABLE].NewRow();

                    string strCalculatedName = String.Empty;
                    strCalculatedName += businessEntityCM.Name;

                    entityRow[GroupMembersDS.ENTITYNAME_FIELD] = strCalculatedName;
                    entityRow[GroupMembersDS.ENTITYCLID_FIELD] = objGroupMember.CLID.ToString();
                    entityRow[GroupMembersDS.ENTITYCSID_FIELD] = objGroupMember.CSID.ToString();
                    entityRow[GroupMembersDS.ADJUSTMENTENTITY_FIELD] = objGroupMember.Adjustment;
                    objGroupMembers.Tables[GroupMembersDS.INCLUDEDENTITIES_TABLE].Rows.Add(entityRow);

                    this.Broker.ReleaseBrokerManagedComponent(businessEntityCM);

                    this.Broker.ReleaseBrokerManagedComponent(lCM);

                }
			}
		}

		protected void OnGetData( GroupMemberScenariosDS data )
		{
			GroupMemberScenariosDS groupMemberScenariosDS = data;
				
			//Get the name of the configuration workpaper for the group type
			//so that screens can redirect back to it.
			groupMemberScenariosDS.ConfigureWorkpaper = this.ConfigureWPURL;

			Guid memberCLID=groupMemberScenariosDS.MemberCLID;
			ILogicalModule iLM = this.Owner.GetLogicalCM(memberCLID);
			
			data.memberName = iLM.Name;

			foreach(DictionaryEntry dictionaryEntry in iLM.Scenarios)
			{
				CMScenario memberScenario=(CMScenario)dictionaryEntry.Value;
					
				DataRow scenarioRow=groupMemberScenariosDS.Tables[GroupMemberScenariosDS.AVAILABLESCENARIOS_TABLE].NewRow();
				scenarioRow[GroupMemberScenariosDS.MEMBERSCENARIONAME_FIELD]=memberScenario.Name;
				scenarioRow[GroupMemberScenariosDS.MEMBERSCENARIOCSID_FIELD]=memberScenario.CSID;
				groupMemberScenariosDS.Tables[GroupMemberScenariosDS.AVAILABLESCENARIOS_TABLE].Rows.Add(scenarioRow);
				
			}

			this.Broker.ReleaseBrokerManagedComponent(iLM);

			GroupMember groupMember=includedGroupMembers[memberCLID];

			if(null!=groupMember)	
				groupMemberScenariosDS.MemberCSID=groupMember.CSID;
		}

		protected void OnGetData( OwnershipHistoryDS data )
		{
			OwnershipHistoryDS objOHData = (OwnershipHistoryDS) data;
			GroupMember objGroupMember = this.includedGroupMembers[ objOHData.EntityID ];

			OwnershipHistories objOwnershipHistory = objGroupMember.OwnershipHistory;
			objOwnershipHistory.ExtractPresentationData( objOHData.OwnershipHistoryTable );
			
		}

		protected void OnGetData( MembersDS data )
		{
			//Extract member data for persistence into CMMEMBERS table
			IDictionaryEnumerator en = this.IncludedGroupMembers.GetEnumerator();
			while(en.MoveNext())
			{
				GroupMember member = (GroupMember)en.Value;

				DataRow memberRow=((MembersDS)data).Tables[MembersDS.CMMEMBERS_TABLE].NewRow();
				memberRow[MembersDS.PARENTCLID_FIELD] = this.CLID;
				memberRow[MembersDS.PARENTCSID_FIELD] = this.CSID;
				memberRow[MembersDS.MEMBERCLID_FIELD] = member.CLID;
				memberRow[MembersDS.MEMBERCSID_FIELD] = member.CSID;
				memberRow[MembersDS.ADJUSTMENT_FIELD] = member.Adjustment;
				((MembersDS)data).Tables[MembersDS.CMMEMBERS_TABLE].Rows.Add(memberRow);
				if(member.IsDeleted)
				{
					memberRow.AcceptChanges();
					memberRow.Delete();
				}
			}
		}

		protected override void OnGetData(DataSet data )
		{
			base.OnGetData(data);
			if (data is GroupDS)
			{
				GroupDS objGroupDS = data as GroupDS;
				DataRow drGroup = objGroupDS.Tables[ GroupDS.GROUP_TABLE ].NewRow();
				drGroup[ GroupDS.ENTITYCLID_FIELD ] = this.ConsolidationAdjustmentEntityCLID;
				drGroup[ GroupDS.ENTITYCSID_FIELD ] = this.ConsolidationAdjustmentEntityCSID;

				objGroupDS.Tables[ GroupDS.GROUP_TABLE ].Rows.Add( drGroup );

				foreach( DictionaryEntry objDictionaryEntry in this.includedGroupMembers )
				{
					GroupMember objGroupMember = (GroupMember) objDictionaryEntry.Value;
					DataRow drIncludedMembers = objGroupDS.Tables[ GroupDS.INCLUDEDMEMBERS_TABLE ].NewRow();
					drIncludedMembers[ GroupDS.GROUPCLID_FIELD ] = objGroupMember.CLID;
					drIncludedMembers[ GroupDS.GROUPCSID_FIELD ] = objGroupMember.CSID;
					drIncludedMembers[ GroupDS.ISDELETED_FIELD ] = objGroupMember.IsDeleted;
					drIncludedMembers[ GroupDS.ADJUSTMENT_FIELD] = objGroupMember.Adjustment;

					objGroupDS.Tables[ GroupDS.INCLUDEDMEMBERS_TABLE ].Rows.Add( drIncludedMembers );
				}

				foreach( DictionaryEntry objDictionaryEntry in this.availableGroupMembers )
				{
					GroupMember objGroupMember = (GroupMember) objDictionaryEntry.Value;
					DataRow drAvailableMembers = objGroupDS.Tables[ GroupDS.AVAILABLEMEMBERS_TABLE ].NewRow();
					drAvailableMembers[ GroupDS.GROUPCLID_FIELD ] = objGroupMember.CLID;
					drAvailableMembers[ GroupDS.GROUPCSID_FIELD ] = objGroupMember.CSID;
					drAvailableMembers[ GroupDS.ISDELETED_FIELD ] = objGroupMember.IsDeleted;
					drAvailableMembers[ GroupDS.ADJUSTMENT_FIELD] = objGroupMember.Adjustment;

					objGroupDS.Tables[ GroupDS.AVAILABLEMEMBERS_TABLE ].Rows.Add( drAvailableMembers );
				}
			}
			else if (data is GroupMembersDS)
			{
				this.OnGetData((GroupMembersDS)data);
			}
			else if (data is GroupMemberScenariosDS)
			{
				this.OnGetData((GroupMemberScenariosDS)data);
			}
			else if (data is OwnershipHistoryDS)
			{
				this.OnGetData((OwnershipHistoryDS)data);
			}
			else if (data is MembersDS)
			{
				this.OnGetData((MembersDS)data);
			}
			else if (data is GroupStructureDS)
			{
				this.OnGetData((GroupStructureDS)data);
			}
		}

		#endregion

		#region OnSetData Methods
		protected ModifiedState OnSetData( GroupMembersDS objData )
		{
			ILogicalModule groupLogicalModule = this.GetLogicalModule();

			ICalculationModule objOrgCM = this.Broker.GetWellKnownCM(WellKnownCM.Organization);
			
			ILogicalModule objOrgLM = this.Broker.GetLogicalCM(objOrgCM.CLID);

			this.Broker.ReleaseBrokerManagedComponent(objOrgCM);
				
			for (int i=0 ;i<objData.Tables[GroupMembersDS.INCLUDEDENTITIES_TABLE].Rows.Count;i++)
			{
				DataRow groupMemberRow=objData.Tables[GroupMembersDS.INCLUDEDENTITIES_TABLE].Rows[i];
				if(groupMemberRow.RowState != DataRowState.Deleted)
				{
					string groupMemberName=(string)groupMemberRow[GroupMembersDS.ENTITYNAME_FIELD];
					string groupMemberCLIDStr=(string)groupMemberRow[GroupMembersDS.ENTITYCLID_FIELD];
					bool blAdjustmentEntity = bool.Parse(groupMemberRow[GroupMembersDS.ADJUSTMENTENTITY_FIELD].ToString());
					Guid groupMemberCLID=new Guid(groupMemberCLIDStr);
				
					if ( this.IsMemberOfGroup( new Guid( groupMemberCLIDStr ), true ) )
						throw new DisplayUserMessageException(new MessageBoxDefinition(MessageBoxDefinition.MSG_GROUP_MEMBER_CIRCULAR_REF,new string[]{groupMemberName}),true,MessageDisplayMethod.MainWindowError);

					//If the CLID is not in the includedgroupmembers, or it is in the collection but is marked as deleted
					if(!includedGroupMembers.Contains(groupMemberCLID) 
						|| (includedGroupMembers.Contains(groupMemberCLID) && includedGroupMembers[groupMemberCLID].IsDeleted))
					{
                        if (!objData.GetUserConfirmed(GroupMembershipConfirmAddKey))
                            this.CheckIfStructureWillBeTimeIntensive(GroupMembershipConfirmAddKey, BusinessStructureNavigationDirection.UpAndAcross);
                        else
                            this.Broker.Transaction.IsLongRunningTransaction = true;

						ILogicalModule businessEntityCM = this.Owner.GetLogicalCM(groupMemberCLID);
						
						if(null!=businessEntityCM)
						{
							GroupMember includedGroupMember;
							//If the Group member is already in collection but has been marked to delete
							//then mark is as false for IsDeleted
							//Otherwise create a new group member instance and add it
							if(includedGroupMembers.Contains(groupMemberCLID))
							{
								includedGroupMember=includedGroupMembers[groupMemberCLID];
								includedGroupMember.IsDeleted = false;
							}
							else
							{
								includedGroupMember= this.NewGroupMember(groupMemberCLID,businessEntityCM.CurrentScenario, blAdjustmentEntity);//groupMemberName,groupMemberCLID,businessEntityCM.CurrentScenario);
								includedGroupMembers.Add(includedGroupMember.CLID,includedGroupMember);
							}

							if(!blAdjustmentEntity)
								this.Broker.LogEvent(EventType.GroupMemberAdd,Guid.Empty, "Entity '"+businessEntityCM.Name + "' added to Group '" + this.Name + "' in '" + objData.BreadCrumb + "'");
							
							includedGroupMember.Selected=true;

							businessEntityCM.AddOwner(this.CLID, this.CSID, businessEntityCM.CurrentScenario);
							
                            objOrgLM.Subscribe(true, this.CLID, includedGroupMember.CLID, this.CSID, includedGroupMember.CSID );
							this.PublishStreamControlToAffectedMember( StreamControlType.OrganisationUnitJoining, includedGroupMember.CLID, includedGroupMember.CSID );
						}

						this.Broker.ReleaseBrokerManagedComponent(businessEntityCM);
					}
					else
						includedGroupMembers[groupMemberCLID].Selected=true;
				}
			}
			// Delete all of the group members not marked as selected.  These are all the members that did not
			// correspond to an entry from the workpaper.
			bool more;
			do
			{
				more=false;
				foreach(DictionaryEntry dictionaryEntry in includedGroupMembers)
				{
					OrganizationUnit.GroupMember objGroupMember = (OrganizationUnit.GroupMember)dictionaryEntry.Value;
					//Delete any entries that are not marked as selected,but do not delete any adjustments, as these are handled seperately yet
					//still need to act as a member entity
					if(!objGroupMember.Selected && !((GroupMember)dictionaryEntry.Value).Adjustment)
					{
                        if (!objData.GetUserConfirmed(GroupMembershipConfirmRemoveKey))
                            this.CheckIfStructureWillBeTimeIntensive(GroupMembershipConfirmRemoveKey, BusinessStructureNavigationDirection.UpAndAcross);
                        else
                            this.Broker.Transaction.IsLongRunningTransaction = true;

                        objGroupMember.IsDeleted = true; //Mark to be deleted
						objGroupMember.Selected = true; //Mark as selected otherwise an endless loop is encountered.

						ILogicalModule memberRemovedLM=this.Owner.GetLogicalCM(objGroupMember.CLID);

						// remove this group as an owner of the entity logical module
						memberRemovedLM.RemoveOwner(this.CLID, this.CSID, objGroupMember.CSID);

                        this.Broker.LogEvent(EventType.GroupMemberRemove, Guid.Empty, "Group member '" + memberRemovedLM.Name + "' removed from Group '" + this.Name + "' in '" + objData.BreadCrumb + "'");
						this.Broker.ReleaseBrokerManagedComponent(memberRemovedLM);

						more=true;
						//Remove the subscription to group member
						objOrgLM.UnSubscribe(this.CLID, objGroupMember.CLID, this.CSID, objGroupMember.CSID );
						this.PublishStreamControlToAffectedMember( StreamControlType.OrganisationUnitLeaving, objGroupMember.CLID, objGroupMember.CSID );
						break;
					}
				}
			}
			while(more);

			this.Broker.ReleaseBrokerManagedComponent(objOrgLM);
			
			foreach(DictionaryEntry dictionaryEntry in includedGroupMembers)
				((GroupMember)dictionaryEntry.Value).Selected=false;
		
			return ModifiedState.UNKNOWN;
		}

		protected ModifiedState OnSetData( GroupMemberScenariosDS groupMemberScenariosDS )
		{
			Guid groupMemberCLID=groupMemberScenariosDS.MemberCLID;
			Guid groupMemberCSID=groupMemberScenariosDS.MemberCSID;

			if ( this.IsMemberOfGroup( groupMemberCLID, groupMemberCSID, true ) )
			{
				ILogicalModule logicalMember=this.Broker.GetLogicalCM(groupMemberCLID);
				CMScenario memberScenario=logicalMember.GetScenario(groupMemberCSID);
				this.Broker.ReleaseBrokerManagedComponent(logicalMember);
				throw new DisplayUserMessageException(new MessageBoxDefinition(MessageBoxDefinition.MSG_INVALID_MEMBER_SCENARIO,new string[]{memberScenario.Name}),true,MessageDisplayMethod.MainWindowError);
			}

			if(includedGroupMembers.Contains(groupMemberCLID))
			{
				if(includedGroupMembers[groupMemberCLID].CSID != groupMemberCSID)
				{
                    if (!groupMemberScenariosDS.GetUserConfirmed(MemberScenarioChangeConfirmKey))
                        this.CheckIfStructureWillBeTimeIntensive(MemberScenarioChangeConfirmKey, BusinessStructureNavigationDirection.UpAndAcross);
                    else
                        this.Broker.Transaction.IsLongRunningTransaction = true;

					ICalculationModule organizationCM=this.Owner.GetWellKnownCM(WellKnownCM.Organization);
					ILogicalModule objOrgLM = this.Broker.GetLogicalCM(organizationCM.CLID);

					this.Broker.ReleaseBrokerManagedComponent(organizationCM);

					this.PublishStreamControlToAffectedMember( StreamControlType.OrganisationUnitLeaving, groupMemberCLID, includedGroupMembers[groupMemberCLID].CSID );

					//Remove the old subscription 
					objOrgLM.UnSubscribe(this.CLID,groupMemberCLID, this.CSID, includedGroupMembers[groupMemberCLID].CSID);
					//Set the selected member scenario
					includedGroupMembers[groupMemberCLID].CSID = groupMemberCSID;
					//Add a new subscription to the newly selected scenario.
					objOrgLM.Subscribe(true, this.CLID,groupMemberCLID, this.CSID, includedGroupMembers[groupMemberCLID].CSID);

					this.PublishStreamControlToAffectedMember( StreamControlType.OrganisationUnitJoining, groupMemberCLID, groupMemberCSID );

					this.Broker.ReleaseBrokerManagedComponent(objOrgLM);
				}
			}

			return ModifiedState.UNKNOWN;
		}

		protected new ModifiedState OnSetData(ScenarioDS objData)
		{
			Guid cLID=new Guid(objData.CLID);
			Guid cSID=new Guid(objData.CSID);
			ILogicalModule logicalEntity=this.Broker.GetLogicalCM(cLID);
			CMScenario cmScenario=logicalEntity.GetScenario(cSID);

			this.Broker.ReleaseBrokerManagedComponent(logicalEntity);

			//If a scenario is being copied. ie a new scenario is created.
			//then the included members need to also have a new scenario 
			//of the same details created, and the new group scenario is required to
			//register subscriptions to the newly created entity scenarios.
			if(objData.CopyScenario)
			{
                if (!objData.GetUserConfirmed(CreateScenarioConfirmKey))
                    this.CheckIfStructureWillBeTimeIntensive(CreateScenarioConfirmKey, BusinessStructureNavigationDirection.DownAndAcross);

				ICalculationModule objOrgCM = this.Broker.GetWellKnownCM(WellKnownCM.Organization);

				ILogicalModule objOrgLM = this.Broker.GetLogicalCM(objOrgCM.CLID);

				this.Broker.ReleaseBrokerManagedComponent(objOrgCM);

				Guid gdNewCSID = new Guid(objData.CSID);

				IGroup objGroupCM = (IGroup)this.Broker.GetCMImplementation(this.CLID, gdNewCSID);

				// Iterate over all included members, groups and entities
				foreach(DictionaryEntry dictionaryEntry in objGroupCM.IncludedGroupMembers)
				{
					OrganizationUnit.GroupMember groupMember=(OrganizationUnit.GroupMember)dictionaryEntry.Value;
					//Get the group members logical module and the cmscenario being copied
					//so the details can be used in the event logging
					ILogicalModule groupMemberLM = this.Broker.GetLogicalCM(groupMember.CLID);
					CMScenario cMScenario = groupMemberLM.GetScenario(groupMember.CSID);

					//If the option was selected to create the new scenario in group members,
					//OR the current member is the consolidation adjustment entity for the group
					//as a groups adjustment entity will always need to have a new scenario created.
					if(objData.CreateInMembers || groupMemberLM.CMType == new Guid("5A5BC2D6-50CB-412A-B151-BF8E16E17403"))
					{
						ICalculationModule objEntityCM = this.Broker.GetCMImplementation(groupMember.CLID, groupMember.CSID);

						Guid gdNewMemberCSID = Guid.NewGuid();

						objData.CSID= gdNewMemberCSID.ToString();
						objData.CIID=objEntityCM.CIID.ToString();	// The Implementation ID of the CM for which a new scenario is being made
						objData.CLID=groupMember.CLID.ToString();  // The logical ID of the CM for which a new scenario is being made
						objData.CopiedCSID = groupMember.CSID.ToString();
						((IBrokerManagedComponent)objEntityCM).SetData(objData);

						// Add the group as an owner of the child module
						groupMemberLM.AddOwner(objGroupCM.CLID, objGroupCM.CSID, gdNewMemberCSID);

						this.Broker.ReleaseBrokerManagedComponent(objEntityCM);
					
						//Do not log any information if the member being updated is a consolidation adjustment
						//as these are really part of the group and just act as virtual entities.
						if(groupMemberLM.CMType != new Guid("5A5BC2D6-50CB-412A-B151-BF8E16E17403")) //TypeID of Consolidation Adjustment
                            this.Broker.LogEvent(EventType.ScenarioCopy, Guid.Empty, "Group '" + this.Name + "' created new scenario '" + objData.Name + "' as a copy of scenario '" + cMScenario.Name + "' in its group member '" + groupMemberLM.Name + "' in '" + this.ConstructBreadcrumb() + "'");
						else 
							//If it is the consol adjustment entity, then need to set guid to point at 
							//new scenario for the adjustment entity.
							objGroupCM.ConsolidationAdjustmentEntityCSID = gdNewMemberCSID;
						
						//Remove any subscriptions to previous group member scenarios for this new group scenario
						objOrgLM.UnSubscribe(this.CLID, groupMember.CLID, gdNewCSID, groupMember.CSID );

						groupMember.CSID = gdNewMemberCSID;
					
						//24Jan03 - PB
						//Add a subscription for the new group scenario to the new entity scenario.
						objOrgLM.Subscribe(true, this.CLID, groupMember.CLID, gdNewCSID, gdNewMemberCSID );
					}

					this.Broker.ReleaseBrokerManagedComponent(groupMemberLM);
				}

				this.Broker.ReleaseBrokerManagedComponent(objOrgLM);
				this.Broker.ReleaseBrokerManagedComponent(objGroupCM);
			}

			return ModifiedState.MODIFIED;
		}

		protected ModifiedState OnSetData( MembersDS objData )
		{
			//The members are persisted in the blob, clear any existing collection of members, and rebuild it
			//from the data retrieved from the MembersDS
			this.includedGroupMembers=new OrganizationUnit.GroupMembers();

			for (int i=0 ;i<objData.Tables[MembersDS.CMMEMBERS_TABLE].Rows.Count;i++)
			{
				DataRow groupMemberRow=objData.Tables[MembersDS.CMMEMBERS_TABLE].Rows[i];
				Guid groupMemberCLID = new Guid(groupMemberRow[MembersDS.MEMBERCLID_FIELD].ToString());
				Guid groupMemberCSID = new Guid(groupMemberRow[MembersDS.MEMBERCSID_FIELD].ToString());
				bool blAdjustment = bool.Parse(groupMemberRow[MembersDS.ADJUSTMENT_FIELD].ToString());
				GroupMember includedGroupMember= this.NewGroupMember(groupMemberCLID,groupMemberCSID, blAdjustment);
										
				this.includedGroupMembers.Add(includedGroupMember.CLID,includedGroupMember);
			}
			return ModifiedState.UNKNOWN;
		}

		protected override ModifiedState OnSetData(DataSet data)
		{
			base.OnSetData(data);

			if (data is GroupDS)
			{
				GroupDS primaryDS = data as GroupDS;
				GroupMember objGroupMember = null;
		
				if(primaryDS.Tables[GroupDS.GROUP_TABLE].Rows.Count==1)
				{
					DataRow row = primaryDS.Tables[GroupDS.GROUP_TABLE].Rows[0];
					this.ConsolidationAdjustmentEntityCLID = new Guid(row[ GroupDS.ENTITYCLID_FIELD ].ToString());
					this.ConsolidationAdjustmentEntityCSID = new Guid(row[ GroupDS.ENTITYCSID_FIELD ].ToString());
				}

				/* Populate Available Groups */
				this.availableGroupMembers.Clear();
				foreach(DataRow row in primaryDS.Tables[GroupDS.AVAILABLEMEMBERS_TABLE].Rows)
				{
					Guid gCLID			= new Guid( row[ GroupDS.GROUPCLID_FIELD ].ToString() );
					Guid gCSID			= new Guid( row[ GroupDS.GROUPCSID_FIELD ].ToString() );
					bool bAdjustment	= (bool) row[ GroupDS.ADJUSTMENT_FIELD ];
					bool bDeleted		= (bool) row[ GroupDS.ISDELETED_FIELD ];

					objGroupMember = new GroupMember( gCLID, gCSID, bAdjustment );
					objGroupMember.IsDeleted = bDeleted;

					this.availableGroupMembers.Add( objGroupMember.CLID, objGroupMember );
				}

				/* Populate Included Groups */
				this.includedGroupMembers.Clear();
				foreach(DataRow row in primaryDS.Tables[GroupDS.INCLUDEDMEMBERS_TABLE].Rows)
				{
					Guid gCLID			= new Guid( row[ GroupDS.GROUPCLID_FIELD ].ToString() );
					Guid gCSID			= new Guid( row[ GroupDS.GROUPCSID_FIELD ].ToString() );
					bool bAdjustment	= (bool) row[ GroupDS.ADJUSTMENT_FIELD ];
					bool bDeleted		= (bool) row[ GroupDS.ISDELETED_FIELD ];

					objGroupMember = new GroupMember( gCLID, gCSID, bAdjustment );
					objGroupMember.IsDeleted = bDeleted;

					this.includedGroupMembers.Add( objGroupMember.CLID, objGroupMember );
				}
			}
			else if (data is GroupMembersDS)
			{
				OnSetData((GroupMembersDS)data);
			}
			else if (data is MembersDS)
			{
				OnSetData((MembersDS)data);
			}
			else if (data is PartyDS)
			{
				OnSetData((PartyDS)data);
			}
			else if (data is ScenarioDS)
			{
				OnSetData((ScenarioDS)data);
			}
			else if (data is GroupMemberScenariosDS)
			{
				OnSetData((GroupMemberScenariosDS)data);
			}
			return ModifiedState.MODIFIED;
		}

		/// <summary>
		/// OnSetData - Set the data for included party access.
		/// </summary>
		/// <param name="partyDS"></param>
		/// <returns></returns>
		protected new ModifiedState OnSetData(PartyDS partyDS)
		{
			// update CM and LM for current user
			base.OnSetData( partyDS );

			// also update the logical module associated with the CAEntity
            //ILogicalModule objLM = this.Broker.GetLogicalCM( ConsolidationAdjustmentEntityCLID );
            //objLM.SetData(partyDS);

			//this.Broker.ReleaseBrokerManagedComponent(objLM);

			return ModifiedState.UNKNOWN;
		}

		#endregion
		 
		public override DataSet PrimaryDataSet{get{return new GroupDS();}}

		public void PublishStreamControlToAffectedMember( StreamControlType objStreamControl, Guid objAffectedMemberCLID, Guid objAffectedMemberCSID )
		{
			Message objMessage = new Message( this, 1, true );
			objMessage.Add( "DataType", "StreamControl" );
			objMessage.Add( "StreamControl", objStreamControl );
			//If it is a oraganization unit leaving stream control type, then we only want to publish to children
			if( objStreamControl == StreamControlType.OrganisationUnitLeaving)
			{
				objMessage.Scope = 0;
				objMessage.InterScenario = false;
				objMessage.Add( "MemberRemoveCLID", objAffectedMemberCLID );
				this.DeliverMessage( objMessage ); //If a period leaving message then post only to children
			}
			else
			{
				objMessage.Add( "StreamControlTargetCLID", objAffectedMemberCLID );
				objMessage.Add( "StreamControlTargetCSID", objAffectedMemberCSID );
				objMessage.Add( "StreamControlSourceCLID", this.CLID );
				objMessage.Add( "StreamControlSourceCSID", this.CSID );
				this.OverrideStreamControlType(objStreamControl);
				this.PublishMessage( objMessage );
				this.ResetOverrideStreamControlType();
			}
		}

		public void PublishNewPeriodStreamControlToMembers(DateTime dtPeriodStart, DateTime dtPeriodEnd )
		{
			Message objMessage;
			
			IDictionaryEnumerator en = this.IncludedGroupMembers.GetEnumerator();
			while(en.MoveNext())
			{
				GroupMember gMember = (GroupMember)en.Value;

				objMessage = new Message( this, 1000, true );
				objMessage.Add( "DataType", "StreamControl" );
				objMessage.Add( "StreamControl", StreamControlType.PeriodJoining );
				objMessage.Add( "StreamControlTargetCLID", gMember.CLID );
				objMessage.Add( "StreamControlTargetCSID", gMember.CSID );
				objMessage.Add( "StreamControlSourceCLID", this.CLID );
				objMessage.Add( "StreamControlSourceCSID", this.CSID );
				objMessage.Add( "StartDate", dtPeriodStart);
				objMessage.Add( "EndDate", dtPeriodEnd);

				this.OverrideStreamControlType(StreamControlType.PeriodJoining);
				this.PublishMessage( objMessage );
				this.ResetOverrideStreamControlType();
			}
		}

        /// <summary>
        /// Returns an IEnumerable of calculation modules from each group members period with the supplied start 
        /// and end dates, and containing the BMC of given TypeName. Will not throw an exception if
        /// the cm does not exist in a member.
        /// </summary>
        /// <param name="periodStart">The start date to check</param>
        /// <param name="periodEnd">The end date to check</param>
        /// <param name="strTypeName">The typename of the Calculation Module to return</param>
        /// <returns>An IEnumerable<ICalculationModule> containing calculation modules which are in the group</returns>
        public IEnumerable<ICalculationModule> GetBMCTypeFromMembers(DateTime periodStart, DateTime periodEnd, string strTypeName)
		{
       		return this.GetBMCTypeFromMembers(periodStart,periodEnd,strTypeName, false);
		}

        /// <summary>
        /// Returns an IEnumerable of calculation modules from each group members period with the supplied start 
        /// and end dates, and containing the BMC of given TypeName.
        /// </summary>
        /// <param name="periodStart">Start date of the member period to look for CM in</param>
        /// <param name="periodEnd">End date of the member period to look for CM in</param>
        /// <param name="strTypeName">The type of the CM</param>
        /// <param name="exceptionIfCMNotExist">Flag to throw an exception if the CM is not found in a members period.</param>
        /// <returns>An IEnumerable<ICalculationModule> containing calculation modules which are in the group</returns>
        public IEnumerable<ICalculationModule> GetBMCTypeFromMembers(DateTime periodStart, DateTime periodEnd, string strTypeName, bool exceptionIfCMNotExist)
        {
            string[] typeComponents = strTypeName.Split('_');
            List<IPeriod> periods = GetPeriodList(periodStart, periodEnd);
            periods.Sort(new BMCSorter());

            foreach (IPeriod period in periods)
            {
                ILogicalModule periodLM = period.GetLogicalModule();
                bool cmFound = false;

                foreach (ILogicalModule childLM in periodLM.ChildLCMs)
                {
                    // ignore the version information, as we want to include
                    // those that are different versions with the logical module
                    // this is possible with the in-place upgrade that the same
                    // logical module can have 2 different versions of the same CM
                    if (childLM.CMTypeName.StartsWith(typeComponents[0]))
                    {
                        ICalculationModule objCM = childLM[period.CSID];

                        // check that the type name of the actual component is the correct version
                        if (objCM != null && objCM.TypeName == strTypeName)
                        {
                            cmFound = true;
                            yield return objCM;
                        }
                    }

                    this.Broker.ReleaseBrokerManagedComponent(childLM);
                }

                this.Broker.ReleaseBrokerManagedComponent(periodLM);

                if (exceptionIfCMNotExist && !cmFound)
                    throw new ApplicationException("GroupCM.GetBMCTypeFromMembers, the specified CM could not be found in the group member:" + period.GetParentOrganisationUnit().Name);
            }
        }

        private List<IPeriod> GetPeriodList(DateTime periodStart, DateTime periodEnd)
        {
            List<IPeriod> periodList = new List<IPeriod>();

            foreach (GroupMember gMember in this.includedGroupMembers.Values)
            {
                ILogicalModule memberEntityLM = Broker.GetLogicalCM(gMember.CLID);

                foreach (Guid periodCLID in memberEntityLM.ChildCLIDs)
                {
                    IPeriod memberPeriod = (IPeriod)Broker.GetCMImplementation(periodCLID, gMember.CSID);

                    //Check that member period is not null, as some periods may exist in one scenario
                    // of an entity but not in another.
                    if (memberPeriod != null)
                    {
                        if (memberPeriod.StartDate.ToString().Equals(periodStart.ToString()) && memberPeriod.EndDate.ToString().Equals(periodEnd.ToString()))
                            periodList.Add(memberPeriod);
                    }

                    this.Broker.ReleaseBrokerManagedComponent(memberPeriod);
                }

                this.Broker.ReleaseBrokerManagedComponent(memberEntityLM);
            }

            return periodList;
        }

        /// <summary>
        /// Delivers data to the group module
        /// </summary>
        /// <param name="message">The message to deliver</param>
        /// <exception cref="System.ArgumentNullException">Thrown if message is null</exception>
        protected override ModifiedState OnSetSubscriptionData(IMessage message)
        {
            if (message == null)
                throw new ArgumentNullException("message");

            ModifiedState modState = base.OnSetSubscriptionData(message);

            // Tax adjustments are on-published only if they come from Entities or Groups that have been included as part of this group.
            Message onPublishedMessage = new Message(message);	// Down published message
            onPublishedMessage.Scope = 0;					    // publish only internally to the group's scope
            onPublishedMessage.InterScenario = false;		    // publish only internally to the scenario
            onPublishedMessage.MSID = this.CSID;    			// identify as this scenario

            if (onPublishedMessage.Contains("TargetReached"))
                onPublishedMessage.Remove("TargetReached");

            DeliverMessage(onPublishedMessage);

            if (modState != ModifiedState.UNCHANGED)
                return ModifiedState.UNKNOWN;
            else
                return ModifiedState.UNCHANGED;
		}

		public override IsSubscriberType IsSubscriber( IMessage objMessage, SubscriptionsList objSubscriptionsIn, Guid objSubscriberCLID, Guid objSubscriberCSID )
		{
			if ( objMessage == null )
				throw new ArgumentNullException( "objMessage", "Parameter cannot be null: objMessage passed to IsSubscriber method of GroupCM is null" );

			if ( objSubscriptionsIn == null )
				throw new ArgumentNullException( "objSubscriptionsIn", "Parameter cannot be null: objSubscriptionsIn passed to IsSubscriber method of GroupCM is null" );

			if ( objMessage.Datatype == null ||
				objMessage.Datatype == String.Empty )
				throw new ArgumentException( "objMessage", "DataType not specified in message, datatype must be present in IsSubscriber method of GroupCM" );

			Guid subscribedCSID = objMessage.MSID;

			foreach( Subscription objSubscription in objSubscriptionsIn )
			{
				if(objSubscription.Consolidation == false) //Handle as per previously
				{
					if((objSubscription.DataType==objMessage.Datatype) || ( objMessage.Datatype.StartsWith("XP") && objSubscription.DataType == "XP"))
						if(objSubscription.SubscriberCLID==objSubscriberCLID
							&& FilterMessage(objMessage,objSubscription.Parameters)
							&& (objSubscription.SubscriberCSID==objSubscriberCSID || objSubscription.SubscriberCSID==Guid.Empty || objSubscriberCSID==Guid.Empty)
							&& (objSubscription.SubscribedCSID==subscribedCSID || objSubscription.SubscribedCSID==Guid.Empty || subscribedCSID==Guid.Empty)
							)
							return IsSubscriberType.True;
				}
				else if( objMessage.Contains("Consolidation") )
				{
					bool bolConsolidation = (bool) objMessage["Consolidation"];

					if( objSubscription.Consolidation == bolConsolidation 
						&& objSubscription.SubscriberCLID == objSubscriberCLID
						&&( ( objMessage.IsPublisher(objSubscription.SubscribedCLID)
						&& objSubscription.SubscriberCSID==objSubscriberCSID
						&& objSubscription.SubscribedCSID==objMessage.MSID)
						|| this.FilterMessage( objMessage, objSubscription.Parameters )) )
						return IsSubscriberType.True;
				}
				if ( objSubscription.DataType == objMessage.Datatype)
					return IsSubscriberType.True;
				
			}
			return IsSubscriberType.False;
		}

		public override bool FilterMessage( IMessage objMessage, Object[] objParameters )
		{
			if ( objMessage == null )
				throw new ArgumentNullException( "objMessage", "Message paramerer passed to filter message was null" );

			if ( objMessage[ "StartDate" ] == null && objMessage[ "EndDate" ] == null )
				return true;
			if(objParameters == null)
				return false;

			// if it is a cross period message, pass it off to the base class
			if ( objParameters.Length == 1 &&
				objParameters[ 0 ] is DateTime )
				return base.FilterMessage( objMessage, objParameters );

			if ( objParameters.Length < 2 ||
				!(objParameters[ 0 ] is DateTime) ||
				!(objParameters[ 1 ] is DateTime) )
				throw new Exception( "GroupCM -> FilterMessage, subscription parameters do not contain the correct parameters, expect start date as first parameter and end date as second parameter" );

			DateTime objSubscriptionStartDate = (DateTime) objParameters[ 0 ];
			DateTime objSubscriptionEndDate = (DateTime) objParameters[ 1 ];
			DateTime objMessageStartDate = (DateTime) objMessage["StartDate"];
			DateTime objMessageEndDate = (DateTime) objMessage["EndDate"];
			
			if ( objSubscriptionStartDate == objMessageStartDate &&
				objSubscriptionEndDate == objMessageEndDate )
				return true;
			else
				return false;
		}

		public override void PostPersistActions()
		{
			this.IncludedGroupMembers.CommitChanges();
		}
		
		//When deleting a group we need to make sure that any Consolidation adjustments that belong to
		//that group are also deleted from the system
		public override void PreDeleteActions()
		{
			base.PreDeleteActions( );

			ICalculationModule objOrganizationCM=Broker.GetWellKnownCM(WellKnownCM.Organization);
			//Get Organization logical module
			ILogicalModule objOrganizationLM = this.Broker.GetLogicalCM(objOrganizationCM.CLID);

			this.Broker.ReleaseBrokerManagedComponent(objOrganizationCM);

			IDictionaryEnumerator en = this.includedGroupMembers.GetEnumerator();
			while(en.MoveNext())
			{
				GroupMember groupMember = (GroupMember)en.Value;
				//Delete all adjustments from the organization that belong to this group
				if(groupMember.Adjustment)
				{
					ILogicalModule iLM = this.Broker.GetLogicalCM(groupMember.CLID);
					//Check that logical is not null, as the adj entity
					// may have already been deleted by another scenario
					//for the same group.
					if(iLM != null)
						objOrganizationLM.RemoveChildLogicalCM(iLM);
				}
			}

			this.Broker.ReleaseBrokerManagedComponent(objOrganizationLM);
		}

		public override bool DeleteScenario( Guid objScenarioCSID, Guid objParentCSID, ArrayList objLastScenarioConflicts, ArrayList objMemberOfGroupScenario, ArrayList objLockedScenarios )
		{
			if ( this.IsMemberOfOtherGroupScenario( objParentCSID ) )
			{
				objMemberOfGroupScenario.Add( this.Name );
				return false;
			}

			ILogicalModule objThisLM=this.Broker.GetLogicalCM(this.CLID);
			LogicalModuleBase.CMScenarios cmScenarios = (LogicalModuleBase.CMScenarios)objThisLM.Scenarios;

			try
			{
				//Cannot delete the last scenario
				if( cmScenarios.Count > 1 )
				{
					//Get scenario name for use in logging details
					CMScenario cMScenario = objThisLM.GetScenario(objScenarioCSID);
					if(cMScenario.Locked)
					{
						objLockedScenarios.Add(this.Name);
						return false;
					}

                    this.Broker.LogEvent(EventType.ScenarioDeleted, Guid.Empty, "Deleted scenario '" + cMScenario.Name + "' from '" + this.Name + "' in '" + this.ConstructBreadcrumb() + "'");
					
					Guid gdCurrentScenarioBeforeDelete = objThisLM.CurrentScenario;
					objThisLM.DeleteScenario(objScenarioCSID);
					Guid gdCurrentScenarioAfterDelete = objThisLM.CurrentScenario;

					if(gdCurrentScenarioBeforeDelete != gdCurrentScenarioAfterDelete)
					{
						cMScenario = objThisLM.GetScenario(gdCurrentScenarioAfterDelete);
                        this.Broker.LogEvent(EventType.SetCurrentScenario, Guid.Empty, "Current scenario for '" + objThisLM.Name + "' changed to '" + cMScenario.Name + "' in '" + this.ConstructBreadcrumb() + "'");
					}

					return true;
				}
				else
				{
					objLastScenarioConflicts.Add( this.Name );
					return false;
				}
			}
			finally
			{
				objThisLM.CalculateToken(true);
				this.Broker.ReleaseBrokerManagedComponent(objThisLM);
			}
		}
		
		#endregion
		#region OTHER MEMBER FUNCTION
		void PublishEntityPoll()
		{
			Message message=new Message(this,1000,true);
			message.Add("DataType","EntityPoll");
			PublishMessage(message);
		}
		/// <summary>
		/// Access point for navigator object
		/// </summary>
		/// <param name="navigator"></param>
		void IBusinessStructureModule.PerformBusinessStructreNavigationOperation(IBusinessStructureNavigatorVisitor navigator)
		{
			if (navigator.NavigationDirection == BusinessStructureNavigationDirection.DownAndAcross 
				&& navigator.ShouldVisitReportingUnitNode(this.CLID,this.CSID))
			{
				GroupNodeEventArgs groupNodeEventArgs = new GroupNodeEventArgs();
				groupNodeEventArgs.Group = (IGroup)this;
				groupNodeEventArgs.CLID = this.CLID; 
				groupNodeEventArgs.CSID = this.CSID;
				groupNodeEventArgs.OrganisationUnit = this;

				navigator.VisitGroupNode(this,groupNodeEventArgs);

				ICalculationModule iCM = this.Broker.GetCMImplementation(this.CLID,this.CSID);
				ILogicalModule lCM = this.Broker.GetLogicalCM(iCM.CLID);
					
				for(int i=0;i<=lCM.ChildCLIDs.Count-1;i++) 
				{
					IBusinessStructureModule period = (IBusinessStructureModule)this.Broker.GetCMImplementation((Guid)lCM.ChildCLIDs[i],iCM.CSID);

					// period will be null if the period does not exist in
					// the scenario
					if(period != null)
						period.PerformBusinessStructreNavigationOperation(navigator);

					this.Broker.ReleaseBrokerManagedComponent(period);
				}
		
				this.Broker.ReleaseBrokerManagedComponent(lCM);
				this.Broker.ReleaseBrokerManagedComponent(iCM);
				
				Hashtable groupMembers = GetGroupMember((IGroup)this);

                navigator.IncrementGroupLevelsVisited();
                foreach (DictionaryEntry dict in groupMembers)
				{
					Hashtable memberDetails = (Hashtable)dict.Value; 

					ICalculationModule reportingUnit = this.Broker.GetCMImplementation((Guid)memberDetails[ReportingUnitDetails.CLID],(Guid)memberDetails[ReportingUnitDetails.CSID]);
                    ((IBusinessStructureModule)reportingUnit).PerformBusinessStructreNavigationOperation(navigator);
					this.Broker.ReleaseBrokerManagedComponent(reportingUnit);
				}
                navigator.DecrementGroupLevelsVisited();
			}
            else if (navigator.NavigationDirection == BusinessStructureNavigationDirection.UpAndAcross)
            {
                ReadOnlyCollection<IGroup> parentReportingUnits = this.GetParentGroups();
                navigator.IncrementGroupLevelsVisited();

                foreach (IGroup group in parentReportingUnits)
                {
                    if (navigator.ShouldVisitReportingUnitNode(group.CLID, group.CSID))
                    {
                        GroupNodeEventArgs eventArgs = new GroupNodeEventArgs();
                        eventArgs.Group = group;
                        navigator.VisitGroupNode(this, eventArgs);

                        ((IBusinessStructureModule)group).PerformBusinessStructreNavigationOperation(navigator);

                    }
                }
                navigator.DecrementGroupLevelsVisited();
            }
        }

		/// <summary>
		/// Gets the consolidation adjustment entity for this group
		/// </summary>
		/// <returns>The consolidation adjustment entity for the group</returns>
		public IEntity GetConsolidationAdjustmentEntity()
		{
			return (IEntity) this.Broker.GetCMImplementation(this.ConsolidationAdjustmentEntityCLID, this.ConsolidationAdjustmentEntityCSID);
		}

		/// <summary>
		/// Determines if the entity passed in is the consolidation adjustment
		/// entity for this group
		/// </summary>
		/// <param name="entity">The entity to check</param>
		/// <returns>True if it is the entity, false otherwise</returns>
		/// <exception cref="System.ArgumentNullException">Thrown if entity is null</exception>
		public bool IsConsolidationAdjustmentEntity(IEntity entity)
		{
			if(entity == null)
				throw new ArgumentNullException("entity");

			return (entity == this.GetConsolidationAdjustmentEntity());
		}
        /// <summary>
        /// Determines if the CLID passed in is the CLID for consolidation adjustment
        /// entity for this group
        /// </summary>
        /// <param name="clid">CLID to check</param>
        /// <returns>True if CLID is matching, false otherwise</returns>
        /// <exception cref="System.ArgumentNullException">Thrown if entity is null</exception>
        public bool IsConsolidationAdjustmentEntity(Guid clid)
        {
            return (clid == this.ConsolidationAdjustmentEntityCLID);
        }
		/// <summary>
		/// Gets a member organisation unit from the logical module
		/// </summary>
		/// <param name="organizationUnitLM">The logical organisation unit that is a member of the group</param>
		/// <returns>The organisation unit that is the member of the group</returns>
		/// <exception cref="System.ArgumentNullException">Thrown if organizationUnitLM is null</exception>
		/// <exception cref="System.ApplicationException">Thrown if the organizationUnitLM is not a member of the group</exception>
		public IOrganizationUnit GetMemberOrganisationUnit(ILogicalModule organizationUnitLM)
		{
			if(organizationUnitLM == null)
				throw new ArgumentNullException("organizationUnitLM");

			GroupMember groupMember = this.IncludedGroupMembers[organizationUnitLM.CLID];
			
			if(groupMember == null)
				return null;

			return (IOrganizationUnit) this.Broker.GetCMImplementation(groupMember.CLID, groupMember.CSID);
		}

		/// <summary>
		///	Get	all members for group. 
		/// </summary>
		/// <param name="reportingUnit"></param>
		/// <returns></returns>
		public Hashtable GetGroupMember(IGroup reportingUnit)
		{
			Hashtable ht = new Hashtable();		
			Hashtable memberDetails = new Hashtable(); 
			
			Oritax.TaxSimp.CM.OrganizationUnit.GroupMembers groupMembers = ((IGroup)reportingUnit).IncludedGroupMembers;
 
			foreach(DictionaryEntry dictionarEntry in groupMembers)
			{
				Oritax.TaxSimp.CM.OrganizationUnit.GroupMember member = (GroupMember) dictionarEntry.Value;

				string memberName = this.GetMemberName(member.CSID,member.CLID);
					
				memberDetails = new Hashtable(); 
				memberDetails.Add(ReportingUnitDetails.Name,memberName); 
				memberDetails.Add(ReportingUnitDetails.CLID,member.CLID); 
				memberDetails.Add(ReportingUnitDetails.CSID,member.CSID); 
				ht.Add(memberName,memberDetails); 
			}
			return ht;
		}

		protected string GetMemberName(Guid CSID, Guid CLID)
		{
			ILogicalModule lCM = this.Broker.GetLogicalCM(CLID);
					
			ICalculationModule memberCM;
				
			memberCM=lCM[CSID];
					
			return ((IBrokerManagedComponent)memberCM).Name;
		}

		public override void PublishAll()
		{
			PublishEntityPoll();
		}

		bool IsIncludedInGroup(PublisherData publisherData)
		{
			GroupMember groupMember=includedGroupMembers[publisherData.CLID];
			return groupMember!=null && groupMember.CSID==publisherData.CSID;
		}

		public virtual GroupMember NewGroupMember(Guid cLID, Guid cSID, bool blAdjustment ) 
		{
			return new GroupMember( cLID, cSID, blAdjustment );
		}

		/// <summary>
		/// Method to allow the removal a member from the group via a method call
		/// instead of through passing a Dataset.
		/// The purpose of this method is to enable Adjustments to be removed in a different
		/// manner to standard periods, as the adjustments are not displayed on the MultiPeriodMembers page.
		/// </summary>
		/// <param name="gdCLID"></param>
		public void RemoveAdjMember(Guid gdCLID)
		{
			GroupMember objMember = (GroupMember) this.includedGroupMembers[gdCLID];
							
			if(objMember!= null)
			{
				objMember.IsDeleted = true;
				this.Modified=true;
				this.PublishStreamControlToAffectedMember( StreamControlType.OrganisationUnitLeaving, objMember.CLID, objMember.CSID );						
			}
		}

		public bool VerifyPeriodDoesNotExistInMembers(DateTime dtPeriodStart, DateTime dtPeriodEnd, ref ArrayList alMemberNames)
		{
			bool blReturnValue = true;
			
			//Search all members of the group to see if they already contain the period
			foreach (DictionaryEntry dictionaryEntry in this.IncludedGroupMembers)	
			{
				OrganizationUnit.GroupMember groupMember = (OrganizationUnit.GroupMember)dictionaryEntry.Value;
				
				ILogicalModule objEntityLM = this.Broker.GetLogicalCM(groupMember.CLID);
				IOrganizationUnit objEntityCM = (IOrganizationUnit) objEntityLM[groupMember.CSID];

				this.Broker.ReleaseBrokerManagedComponent(objEntityLM);
				
				//If the member organization unit is another group, then recursively call this method to check if 
				//any of its members also might contain the period already.
				if(objEntityCM is IGroup)
				{
					if(!((IGroup)objEntityCM).VerifyPeriodDoesNotExistInMembers( dtPeriodStart,  dtPeriodEnd, ref  alMemberNames))
						blReturnValue = false;
				}

				this.Broker.ReleaseBrokerManagedComponent(objEntityCM);
			}

			return blReturnValue;
		}
		
		public bool VerifyPeriodDoesNotOverlapInMembers(DateTime dtStartDate, DateTime dtEndDate, Guid gdOwnerCLID, ref ArrayList alMemberNames)
		{
			bool blReturnValue = true;
		
			//Search all members of the group to see if the period overlaps
			foreach (DictionaryEntry dictionaryEntry in this.IncludedGroupMembers)	
			{
				OrganizationUnit.GroupMember groupMember=(OrganizationUnit.GroupMember)dictionaryEntry.Value;
				
				ILogicalModule objEntityLM = this.Broker.GetLogicalCM(groupMember.CLID);
				IOrganizationUnit objEntityCM = (IOrganizationUnit) objEntityLM[groupMember.CSID];

				this.Broker.ReleaseBrokerManagedComponent(objEntityLM);

				
				//If the member organization unit is another group, then recursively call this method to check if 
				//any of its members also might contain the period already.
				if(objEntityCM is IGroup)
				{
					if(!((IGroup)objEntityCM).VerifyPeriodDoesNotOverlapInMembers( dtStartDate,  dtEndDate, gdOwnerCLID, ref  alMemberNames) )
						blReturnValue = false;
				}

				this.Broker.ReleaseBrokerManagedComponent(objEntityCM);
			}

			return blReturnValue;
		}

		public bool VerifyPeriodIsSAPCompliantInMembers( DateTime dtStartDate, DateTime dtEndDate, ref ArrayList alMemberNames )
		{
			bool blReturnValue = true;
			
			//Search all members of the group to see if the period is SAP compliant
			foreach (DictionaryEntry dictionaryEntry in this.IncludedGroupMembers)	
			{
				OrganizationUnit.GroupMember groupMember=(OrganizationUnit.GroupMember)dictionaryEntry.Value;
				
				ILogicalModule objEntityLM = this.Broker.GetLogicalCM(groupMember.CLID);
				IOrganizationUnit objEntityCM = (IOrganizationUnit) objEntityLM[groupMember.CSID];
				
				this.Broker.ReleaseBrokerManagedComponent(objEntityLM);

				//Check to see that the SAP is compliant for the entity 
				if( !objEntityCM.SAPs.VerifyPeriodIsSAPCompliant( dtStartDate, dtEndDate ) )
				{
					blReturnValue = false;
					//Add the name of the member entity/group which contains the period
					alMemberNames.Add(((IBrokerManagedComponent)objEntityCM).Name);
				}

				//If the member organization unit is another group, then recursively call this method to check SAPs
				if(objEntityCM is IGroup)
				{
					if(!((IGroup)objEntityCM).VerifyPeriodIsSAPCompliantInMembers( dtStartDate,  dtEndDate, ref  alMemberNames) )
						blReturnValue = false;
				}

				this.Broker.ReleaseBrokerManagedComponent(objEntityCM);
			}
			return blReturnValue;
		}

		/// <summary>
		/// Check if this group is a member of the target group, used when adding a member, specify a CSID as well, if you are checking if a specific scenario is a member when changing scenarios
		/// </summary>
		/// <param name="objTargetGroupCLID">The CLID of the group to check if you are a member of, checks the current scenario of the target group</param>
		/// <param name="bolSearchSubGroups">Search all sub-groups as well</param>
		/// <returns>True if the group is a member of the target group</returns>
		public bool IsMemberOfGroup( Guid objTargetGroupCLID, bool bolSearchSubGroups )
		{
			bool bolIsMember = false;
			ILogicalModule objThisLogicalModule = this.Broker.GetLogicalCM( objTargetGroupCLID );
			OrganizationUnitCM objSubOU = (OrganizationUnitCM) this.Broker.GetCMImplementation( objTargetGroupCLID, objThisLogicalModule.CurrentScenario );

			this.Broker.ReleaseBrokerManagedComponent(objThisLogicalModule);

			try
			{
				// check that the member is a group - in which case we have to recurse into it
				if ( objSubOU is GroupCM )
				{
					GroupCM objSubGroup = (GroupCM) objSubOU;
					bolIsMember = objSubGroup.IsMemberOfSubGroup( this.CLID, this.CSID, bolSearchSubGroups );
				
					// return true straight away if it is a member
					if ( bolIsMember )
						return bolIsMember;
				}
			}
			finally
			{
				this.Broker.ReleaseBrokerManagedComponent(objSubOU);
			}

			return bolIsMember;
		}

		/// <summary>
		/// Checks if this group is a member of the target group, used when changing the scenario for a member and you are checking a particular scenario
		/// </summary>
		/// <param name="objTargetGroupCLID">The CLID of the group to check if you are a member of</param>
		/// <param name="objTargetGroupCSID">The CSID of the group scenario to check if you are a member of</param>
		/// <param name="bolSearchSubGroups">Search sub groups too</param>
		/// <returns>True if the group is a member of the target group and scenario</returns>
		public bool IsMemberOfGroup( Guid objTargetGroupCLID, Guid objTargetGroupCSID, bool bolSearchSubGroups )
		{
			bool bolIsMember = false;
			OrganizationUnitCM objSubOU = (OrganizationUnitCM) this.Broker.GetCMImplementation( objTargetGroupCLID, objTargetGroupCSID );

			try
			{
				// check that the member is a group - in which case we have to recurse into it
				if ( objSubOU is GroupCM )
				{
					GroupCM objSubGroup = (GroupCM) objSubOU;
					bolIsMember = objSubGroup.IsMemberOfSubGroup( this.CLID, this.CSID, bolSearchSubGroups );

					// return true straight away if it is a member
					if ( bolIsMember )
						return bolIsMember;
				}
			}
			finally
			{
				this.Broker.ReleaseBrokerManagedComponent(objSubOU);
			}

			return bolIsMember;
		}

		public bool IsMemberOfSubGroup( Guid objGroupCLID, Guid objGroupCSID, bool bolSearchSubGroups )
		{
			bool bolIsMember = false;
			
			IDictionaryEnumerator objGMEn = this.IncludedGroupMembers.GetEnumerator( );
			
			while ( objGMEn.MoveNext( ) )
			{
				GroupMember objGM = (GroupMember) objGMEn.Value;
				OrganizationUnitCM objSubOU = (OrganizationUnitCM) this.Broker.GetCMImplementation( objGM.CLID, objGM.CSID );

				try
				{
					// check that the member is a group - in which case we have to recurse into it
					if ( objSubOU is GroupCM )
					{
						GroupCM objSubGroup = (GroupCM) objSubOU;

						// check to see if the sub group we are about to check is the member, if it is return
						// straight away, otherwise, we need to walk down another level
						if ( objSubGroup.CLID == objGroupCLID &&
							objSubGroup.CSID == objGroupCSID )
							return true;

						bolIsMember = objSubGroup.IsMemberOfSubGroup( objGroupCLID, objGroupCSID, bolSearchSubGroups );

						// try to escape early if you find it
						if ( bolIsMember )
							return true;
					}
				}
				finally
				{
					this.Broker.ReleaseBrokerManagedComponent(objSubOU);
				}
			}
			
			return bolIsMember;
		}

		protected override bool HasScenario( string strScenarioName, bool bolSearchSubGroups, ArrayList objConflictingOUNames )
		{
			ILogicalModule objThisLogicalModule = this.Broker.GetLogicalCM( this.CLID );

			try
			{
				if ( this.ContainsScenario( strScenarioName, objThisLogicalModule ) )
				{
					objConflictingOUNames.Add( this.Name );
					return true;
				}
			}
			finally
			{
				this.Broker.ReleaseBrokerManagedComponent(objThisLogicalModule);
			}

			return this.HasScenarioInSubGroup( strScenarioName, bolSearchSubGroups, objConflictingOUNames );				
		}

		private bool HasScenarioInSubGroup( string strScenarioName, bool bolSearchSubGroups, ArrayList objConflictingOUNames )
		{
			bool bolHasScenario = false;
			
			IDictionaryEnumerator objGMEn = this.IncludedGroupMembers.GetEnumerator( );
			
			while ( objGMEn.MoveNext( ) )
			{
				GroupMember objGM = (GroupMember) objGMEn.Value;
				OrganizationUnitCM objSubOU = (OrganizationUnitCM) this.Broker.GetCMImplementation( objGM.CLID, objGM.CSID );
				ILogicalModule objSubOULogical = this.Broker.GetLogicalCM( objSubOU.CLID );

				try
				{
					if ( this.ContainsScenario( strScenarioName, objSubOULogical ) )
					{
						objConflictingOUNames.Add( objSubOU.Name );
						return true;
					}

					// check that the member is a group - in which case we have to recurse into it
					if ( objSubOU is GroupCM )
					{
						GroupCM objSubGroup = (GroupCM) objSubOU;

						if ( objSubGroup.HasScenarioInSubGroup( strScenarioName, bolSearchSubGroups, objConflictingOUNames ) == true )
							bolHasScenario = true;
					}
				}
				finally
				{
					this.Broker.ReleaseBrokerManagedComponent(objSubOU);
					this.Broker.ReleaseBrokerManagedComponent(objSubOULogical);
				}
			}
			
			return bolHasScenario;
		}

		private bool ContainsScenario( string strScenarioName, ILogicalModule objLM )
		{
			IDictionaryEnumerator objScenarioEn = (IDictionaryEnumerator) objLM.Scenarios.GetEnumerator( );

			while( objScenarioEn.MoveNext( ) )
			{
				CMScenario objScenario = (CMScenario) objScenarioEn.Value;
				
				if ( objScenario.Name == strScenarioName )
					return true;
			}
			return false;
		}

		/// <summary>
		/// Deletes a scenario from the group
		/// </summary>
		/// <param name="objScenarioID">The scenario id of the scenario to delete</param>
		/// <param name="bolDeleteFromSubGroups">If the scenario should be deleted from sub-groups</param>
		/// <param name="objLastScenarioConflicts">A list of scenarios that couldn't be deleted because they are the last scenario</param>
		/// <param name="objMemberOfGroupScenario">A list of scenarios that couldn't be deleted because they are a member of another group</param>
		/// <param name="objLockedScenarios">A list of scenarios that couldn't be deleted because they are locked</param>
		public void DeleteScenario( Guid objScenarioID, bool bolDeleteFromSubGroups, ArrayList objLastScenarioConflicts, ArrayList objMemberOfGroupScenario, ArrayList objLockedScenarios )
		{
			bool scenarioDeleted = this.DeleteScenario( objScenarioID, this.CSID, objLastScenarioConflicts, objMemberOfGroupScenario, objLockedScenarios );

			//Only delete from any subgroups if, the bolDeleteFromSubGroups is true,
			//and the scenario could be deleted at this level
			if(scenarioDeleted)
			{
				if ( bolDeleteFromSubGroups )
					this.DeleteSubGroupScenario( objScenarioID, objLastScenarioConflicts,  objMemberOfGroupScenario, objLockedScenarios );
				else
					this.DeleteMemberScenario(this.ConsolidationAdjustmentEntityCLID, this.ConsolidationAdjustmentEntityCSID, objScenarioID, objLastScenarioConflicts, objMemberOfGroupScenario, objLockedScenarios);
			}
		}

		/// <summary>
		/// Delete all member scenarios currently associated to the groups scenario, provided that they are not also members
		/// of other groups.
		/// </summary>
		/// <param name="objScenarioID"></param>
		/// <param name="objLastScenarioConflicts"></param>
		/// <param name="objMemberOfGroupScenario"></param>
		/// <param name="objLockedScenarios"></param>
		private void DeleteSubGroupScenario( Guid objScenarioID, ArrayList objLastScenarioConflicts, ArrayList objMemberOfGroupScenario,ArrayList objLockedScenarios  )
		{
			IDictionaryEnumerator objGMEn = this.IncludedGroupMembers.GetEnumerator( );
			
			while ( objGMEn.MoveNext( ) )
			{
				GroupMember objGM = (GroupMember) objGMEn.Value;
				this.DeleteMemberScenario(objGM.CLID, objGM.CSID, objScenarioID,objLastScenarioConflicts, objMemberOfGroupScenario, objLockedScenarios);
			}
		}

		private void DeleteMemberScenario(Guid gdCLID, Guid gdCSID, Guid gdScenarioID,  ArrayList objLastScenarioConflicts, ArrayList objMemberOfGroupScenario, ArrayList objLockedScenarios )
		{
			ILogicalModule memberOULogical = this.Broker.GetLogicalCM(gdCLID);
			
			//Only delete the member's scenario if it is not locked.
			if(!memberOULogical.IsScenarioLocked(gdCSID))
			{
				IOrganizationUnit objSubOU = (IOrganizationUnit) this.Broker.GetCMImplementation( gdCLID, gdCSID );

                bool scenarioDeleted = objSubOU.DeleteScenario( gdCSID, gdScenarioID, objLastScenarioConflicts, objMemberOfGroupScenario, objLockedScenarios );

				// Remove this group as an owner of the sub-group
				ILogicalModule subGroupLogicalModule = objSubOU.GetLogicalModule();
				subGroupLogicalModule.RemoveOwner(this.CLID, this.CSID, gdCSID);

				// check that the scenario could be deleted and
				// the member is a group - in which case we have to recurse into it
				if ( scenarioDeleted && objSubOU is GroupCM )
				{
					GroupCM objSubGroup = (GroupCM) objSubOU;
					objSubGroup.DeleteSubGroupScenario( gdCSID,  objLastScenarioConflicts, objMemberOfGroupScenario, objLockedScenarios );
				}

				this.Broker.ReleaseBrokerManagedComponent(objSubOU);
			}
			else
				objLockedScenarios.Add(memberOULogical.Name);

			this.Broker.ReleaseBrokerManagedComponent(memberOULogical);
		}

		//Method for setting the current period in all members that contain same period
		public void SetCurrentPeriodInMembers(String startDate, String endDate)
		{
			//Set up the same period in all entities that are part of this group		
			foreach (DictionaryEntry dictionaryEntry in this.IncludedGroupMembers)	
			{
				OrganizationUnit.GroupMember groupMember = (OrganizationUnit.GroupMember)dictionaryEntry.Value;
				
				IOrganizationUnit entityCM = (IOrganizationUnit) this.Broker.GetCMImplementation(groupMember.CLID,groupMember.CSID);
				SetToCurrentPeriod(entityCM, DateTime.Parse(startDate), DateTime.Parse(endDate));

				if(entityCM is IGroup)
					((IGroup)entityCM).SetCurrentPeriodInMembers(startDate, endDate);

				this.Broker.ReleaseBrokerManagedComponent(entityCM);
			}
		}

		//If a period attempting to be added already exists within an Entity, then this method
		//will set that period to being the current period.
		private void SetToCurrentPeriod(IOrganizationUnit entityCM,DateTime dtStartDate, DateTime dtEndDate)
		{
			Guid gdPeriodCLID = entityCM.GetCLIDOfMemberPeriod(dtStartDate, dtEndDate);

			if(gdPeriodCLID != Guid.Empty)
			{
				entityCM.CurrentPeriod = gdPeriodCLID;
				entityCM.CalculateToken(true);
				ICalculationModule objPeriodCM = this.Broker.GetCMImplementation(gdPeriodCLID, entityCM.CSID);
                this.Broker.LogEvent(EventType.PeriodSetCurrent, Guid.Empty, "Current period set to '" + objPeriodCM.Name + "' in '" + entityCM.Name + "' in '" + this.ConstructBreadcrumb() + "'");
				this.Broker.ReleaseBrokerManagedComponent(objPeriodCM);
			}
			
		}

        /// <summary>
        /// Checks if the size of structure qualifies as a time intensive structure
        /// </summary>
        /// <param name="userConfirmationKey">The key thrown in the user confirmation message</param>
        /// <param name="direction">The direction to check e.g. Up or Down</param>
        /// <exception cref="Oritax.TaxSimp.Exceptions.GetUserConfirmationException">Thrown if the task is time intensive</exception>
        public void CheckIfStructureWillBeTimeIntensive(string userConfirmationKey, BusinessStructureNavigationDirection direction)
        {
            TimeIntensiveTaskGroupStructureVisitor timeIntensiveTaskVisitor = new TimeIntensiveTaskGroupStructureVisitor(this.Broker, direction, userConfirmationKey);
            ((IBusinessStructureModule)this).PerformBusinessStructreNavigationOperation(timeIntensiveTaskVisitor);
        }
		#endregion
		#region MIGRATION
		
		public override DataSet MigrateDataSet(DataSet data)
		{
			versionHistory.Add(GroupInstall.COMPONENTVERSION_IMPLEMENTATIONCLASS, GroupDS.GetDataSetVersion(data));

			DataSet baseDataset = base.MigrateDataSet(data);

			DataSet convData = GroupDS.ConvertV_1_0_3_0toV_1_1_0_0DataSet(data, data);
			//convData = ConvertV_XtoV_YDataSet(data, convData);

			if(convData == null)
				return null;

			GroupDS ds = new GroupDS();

			ds.Merge(convData.Tables[GroupDS.GROUP_TABLE]);
			ds.Merge(convData.Tables[GroupDS.INCLUDEDMEMBERS_TABLE]);
			ds.Merge(convData.Tables[GroupDS.AVAILABLEMEMBERS_TABLE]);
			
			//merge in the base dataset
			ds.Merge(baseDataset);

			return ds;
		}

		/// <summary>
		/// notifies to the cm that the migration is completed
		/// </summary>
		public override void MigrationCompleted()
		{
			base.MigrationCompleted();

			ICalculationModule objOrgCM = this.Broker.GetWellKnownCM(WellKnownCM.Organization);
			
			LogicalModuleBase objOrgLM = (LogicalModuleBase)this.Broker.GetLogicalCM(objOrgCM.CLID);

			this.Broker.ReleaseBrokerManagedComponent(objOrgCM);

			foreach(DictionaryEntry dictionaryEntry in includedGroupMembers)
			{
				OrganizationUnit.GroupMember objGroupMember = (OrganizationUnit.GroupMember)dictionaryEntry.Value;
		        objOrgLM.Subscribe(true, this.CLID, objGroupMember.CLID, this.CSID, objGroupMember.CSID );
			}
		}

		/// <summary>
		/// Apply any updates to data prior to version 1.1.0.3
		/// </summary>
		private void UpdateDataPriorToV1_1_0_3()
		{
			// KGM: need to modify the CAEntity's LM access list so allowed
			// users can view CAE workpapers. (cq: 5858)
            
			// only do it for the correct versions (i.e. ver is before 1.1.0.3)
			if ( versionHistory.ContainsKey(GroupInstall.COMPONENTVERSION_IMPLEMENTATIONCLASS) )
			{
				string ver = (versionHistory[GroupInstall.COMPONENTVERSION_IMPLEMENTATIONCLASS] as VersionEntry).Version;
				string [] versionParts = ver.Split('.');

				// only interested in the ASSEMBLY_REVISION part (last part) of the version. 
				// In this case must be less than revision 3
				if( versionParts.Length == 4 && Convert.ToInt32( versionParts[3] ) < 3 )
				{
					// first get the existing party info for this CM
					PartyDS partyDS = new PartyDS();
					this.OnGetData( partyDS );
					// now retrieve and update the CAE's LM using this party info
					ILogicalModule objLM = this.Broker.GetLogicalCM( ConsolidationAdjustmentEntityCLID );
					objLM.SetData( partyDS );
					this.Broker.ReleaseBrokerManagedComponent(objLM);
				}
			}
		}

		public override void Resubscribe()
		{
			base.Resubscribe();

			ICalculationModule objOrgCM = this.Broker.GetWellKnownCM(WellKnownCM.Organization);
			ILogicalModule objOrgLM = this.Broker.GetLogicalCM(objOrgCM.CLID);

			this.Broker.ReleaseBrokerManagedComponent(objOrgCM);

			foreach( DictionaryEntry objDictionaryEntry in this.IncludedGroupMembers )
			{
				GroupMember objGroupMember = (GroupMember) objDictionaryEntry.Value;
				objOrgLM.Subscribe(true, this.CLID, objGroupMember.CLID, this.CSID, objGroupMember.CSID );
			}
	
			this.Broker.ReleaseBrokerManagedComponent(objOrgLM);
		}
		#endregion

        #region EXPORT TO DESKTOP BROKER
        public void AddIndividualEntityToDesktopBrokerAccountResponse(DesktopBrokerDataResponse response,
                                                                          Common.IndividualEntity individualEntity, DesktopBrokerExportKeys key, KeyValuePair<string, string> identificationData)
        {
            DataTable table = null;

            if (response.Data.ContainsKey(key.ToString()))
                table = response.Data[key.ToString()];
            else
            {
                AccountHolderDesktopBrokerColumns accountHolderDesktopBroker = new AccountHolderDesktopBrokerColumns();
                table = accountHolderDesktopBroker.Table;
            }

            DataRow applicant = table.NewRow();
            individualEntity.ToAccountHolderDataTable(ref applicant, identificationData);
            table.Rows.Add(applicant);

            //if (!response.Data.ContainsKey(DesktopBrokerExportKeys.Applicant_1.ToString()))
            //    response.Data.Add(DesktopBrokerExportKeys.Applicant_1.ToString(), table);
            //else if (!response.Data.ContainsKey(DesktopBrokerExportKeys.Applicant_2.ToString()))
            //    response.Data.Add(DesktopBrokerExportKeys.Applicant_2.ToString(), table);

            if (response.Data.ContainsKey(key.ToString()))
                response.Data[key.ToString()] = table;
            else
            {
                response.Data.Add(key.ToString(), table);
            }

        }

        public void AddClientToDesktopBrokerAccountResponse(DesktopBrokerDataResponse response, Common.AddressEntity addressEntity)
        {
            ClientDesktopBrokerColumns clientDesktopBrokerColumns = new ClientDesktopBrokerColumns();

            DataRow row = clientDesktopBrokerColumns.Table.NewRow();

            row[ClientDesktopBrokerColumns.LinceseeNumber] = "";
            row[ClientDesktopBrokerColumns.AdviserNumber] = "";

            row[ClientDesktopBrokerColumns.ClientReferenceNumber] = ClientId;

            if (addressEntity != null)
            {
                row[ClientDesktopBrokerColumns.Postcode] = addressEntity.PostCode;
                row[ClientDesktopBrokerColumns.RegistrationAddressLine1] = addressEntity.Addressline1;
                row[ClientDesktopBrokerColumns.RegistrationAddressLine2] = addressEntity.Addressline2;
                row[ClientDesktopBrokerColumns.State] = Address.GetStateCode(addressEntity.State);
                row[ClientDesktopBrokerColumns.Suburb] = addressEntity.Suburb;
            }

            clientDesktopBrokerColumns.Table.Rows.Add(row);

            response.Data.Add(DesktopBrokerExportKeys.Client.ToString(), clientDesktopBrokerColumns.Table);
        }

        public void AddTransferStockToDesktopBrokerAccountResponse(DesktopBrokerDataResponse response)
        {
            TransferingStockDesktopBrokerColumns transferingStockDesktopBrokerColumns = new TransferingStockDesktopBrokerColumns();
            DataRow row = transferingStockDesktopBrokerColumns.Table.NewRow();
            row[TransferingStockDesktopBrokerColumns.CompanyName] = "";
            row[TransferingStockDesktopBrokerColumns.HIN] = "";
            row[TransferingStockDesktopBrokerColumns.PID] = "";

            if (row[TransferingStockDesktopBrokerColumns.CompanyName].ToString() == "")
            {
                row[TransferingStockDesktopBrokerColumns.CompanyName] = "N";
            }

            if (row[TransferingStockDesktopBrokerColumns.HIN].ToString() == "")
            {
                row[TransferingStockDesktopBrokerColumns.HIN] = "N";
            }

            if (row[TransferingStockDesktopBrokerColumns.PID].ToString() == "")
            {
                row[TransferingStockDesktopBrokerColumns.PID] = "N";
            }

            transferingStockDesktopBrokerColumns.Table.Rows.Add(row);

            //if(transferingStockDesktopBrokerColumns.Table.Rows[0].ItemArray[0].ToString() == "")
            //{
            //    transferingStockDesktopBrokerColumns.Table.Rows[0].ItemArray[0] = "N";
            //}
            //if (transferingStockDesktopBrokerColumns.Table.Rows[0].ItemArray[1].ToString() == "")
            //{
            //    transferingStockDesktopBrokerColumns.Table.Rows[0].ItemArray[1] = "N";
            //}
            //if (transferingStockDesktopBrokerColumns.Table.Rows[0].ItemArray[2].ToString() == "")
            //{
            //    transferingStockDesktopBrokerColumns.Table.Rows[0].ItemArray[2] = "N";
            //}

            response.Data.Add(DesktopBrokerExportKeys.TransferingStock.ToString(), transferingStockDesktopBrokerColumns.Table);

        }

        public void AddBankAccountDetailsToDesktopBrokerAccountResponse(DesktopBrokerDataResponse response, List<Common.BankAccountEntity> bankAccounts)
        {
            BankAccountDesktopBrokerColumns bankAccountDesktopBrokerColumns = new BankAccountDesktopBrokerColumns();
            DataRow row = bankAccountDesktopBrokerColumns.Table.NewRow();

            if (bankAccounts != null)
            {

                var cmaAccount = bankAccounts.Where(b => b.AccoutType.ToLower() == "cma").FirstOrDefault();
                if (cmaAccount != null)
                {
                    row[BankAccountDesktopBrokerColumns.AccountName] = cmaAccount.Name;
                    row[BankAccountDesktopBrokerColumns.AccountNumber] = cmaAccount.AccountNumber;
                    row[BankAccountDesktopBrokerColumns.BSB] = cmaAccount.BSB.Replace("-", "");
                }

            }
            bankAccountDesktopBrokerColumns.Table.Rows.Add(row);
            response.Data.Add(DesktopBrokerExportKeys.BankAccount.ToString(), bankAccountDesktopBrokerColumns.Table);

        }

        public void AddApplicants(DesktopBrokerDataResponse response, List<Common.IndividualEntity> individualEntities, List<Oritax.TaxSimp.Common.IdentityCMDetail> individuals)
        {
            List<Common.IndividualEntity> entities = SelectApplicants(individualEntities, individuals);
            int notNullEntityCount = 0;
            for (int i = 0; i < entities.Count; i++)
            {

                if (entities[i] == null) continue;

                DesktopBrokerExportKeys key = notNullEntityCount == 0
                                                  ? DesktopBrokerExportKeys.Applicant_1
                                                  : DesktopBrokerExportKeys.Applicant_2;


                notNullEntityCount++;


                if (entities[i].HasDocAustralianPassport)
                {
                    //KeyValuePair<string, string> identificationData = new KeyValuePair<string, string>(entities[i].DocAustralianPassport.Type, entities[i].DocAustralianPassport.DocNumber);
                    KeyValuePair<string, string> identificationData = new KeyValuePair<string, string>(entities[i].DocAustralianPassport.Type, "2");
                    AddIndividualEntityToDesktopBrokerAccountResponse(response, entities[i], key, identificationData);

                }
                if (entities[i].HasDocDrivingLicence)
                {
                    //KeyValuePair<string, string> identificationData = new KeyValuePair<string, string>(entities[i].DocDrivingLicence.Type, entities[i].DocDrivingLicence.DocNumber);
                    KeyValuePair<string, string> identificationData = new KeyValuePair<string, string>(entities[i].DocDrivingLicence.Type, "1");
                    AddIndividualEntityToDesktopBrokerAccountResponse(response, entities[i], key, identificationData);
                }

                if (entities[i].HasDocDrivingLicence == false && entities[i].HasDocAustralianPassport == false)
                {
                    AddIndividualEntityToDesktopBrokerAccountResponse(response, entities[i], key, new KeyValuePair<string, string>());
                }

            }

        }

        public List<Common.IndividualEntity> SelectApplicants(List<Oritax.TaxSimp.Common.IndividualEntity> individualEntities, List<Oritax.TaxSimp.Common.IdentityCMDetail> individuals)
        {
            List<Common.IndividualEntity> entities = new List<Oritax.TaxSimp.Common.IndividualEntity>();

            IdentityCM primaryApplicant = individuals.Where(a => a.IsPrimary == true).FirstOrDefault();

            if (primaryApplicant == null)
            {
                entities.Add(individualEntities[0]);

                if (individualEntities.Count > 1)
                    entities.Add(individualEntities[1]);
            }
            else
            {
                var individualCM = Broker.GetCMImplementation(primaryApplicant.Clid, primaryApplicant.Csid) as IHasIndividualEntity;
                if (individualCM == null || individualCM.Individual == null)
                {
                    entities.Add(individualEntities[0]);

                    if (individualEntities.Count > 1)
                        entities.Add(individualEntities[1]);
                    return entities;

                }
                Common.IndividualEntity primaryEntity = individualCM.Individual;
                entities.Add(primaryEntity);

                if (individuals.Count > 1)
                {
                    Common.IndividualEntity firstEntity = null;

                    if (!(individuals[0].Clid == primaryApplicant.Clid && individuals[0].Csid == primaryApplicant.Csid))
                    {
                        var firstIndividualCM = Broker.GetCMImplementation(primaryApplicant.Clid, primaryApplicant.Csid) as IHasIndividualEntity;

                        firstEntity = firstIndividualCM.Individual;
                    }
                    else
                    {
                        var firstIndividualCM = Broker.GetCMImplementation(primaryApplicant.Clid, primaryApplicant.Csid) as IHasIndividualEntity;
                        firstEntity = firstIndividualCM.Individual;
                    }
                    entities.Add(firstEntity);
                }
            }
            return entities;
        }

        public void AddSuperFundToDesktopBrokerAccountResponse(DesktopBrokerDataResponse response, ClientIndividualEntity entity)
        {
            SuperFundDesktopBrokerColumns superFundDesktopBrokerColumns = new SuperFundDesktopBrokerColumns();
            DataRow row = superFundDesktopBrokerColumns.Table.NewRow();
            if (entity != null)
            {
                row[SuperFundDesktopBrokerColumns.ABN] = entity.ABN;
                row[SuperFundDesktopBrokerColumns.DesignationForAccount] = entity.AccountDesignation;
                row[SuperFundDesktopBrokerColumns.SFName] = entity.Name;
                row[SuperFundDesktopBrokerColumns.TaxFileNumber] = entity.TFN;
                if (entity.TFN != "")
                    row[SuperFundDesktopBrokerColumns.TFNExemption] = "";
                else
                    row[SuperFundDesktopBrokerColumns.TFNExemption] = "1";
            }

            superFundDesktopBrokerColumns.Table.Rows.Add(row);
            response.Data.Add(DesktopBrokerExportKeys.SuperFund.ToString(), superFundDesktopBrokerColumns.Table);
        }

        public void AddSuperFundToDesktopBrokerAccountResponse(DesktopBrokerDataResponse response, CorporateEntity entity)
        {
            SuperFundDesktopBrokerColumns superFundDesktopBrokerColumns = new SuperFundDesktopBrokerColumns();
            DataRow row = superFundDesktopBrokerColumns.Table.NewRow();

            if (entity != null)
            {
                row[SuperFundDesktopBrokerColumns.ABN] = entity.ABN;
                row[SuperFundDesktopBrokerColumns.DesignationForAccount] = entity.AccountDesignation;
                row[SuperFundDesktopBrokerColumns.SFName] = entity.Name;
                row[SuperFundDesktopBrokerColumns.TaxFileNumber] = entity.TFN;
                if (entity.TFN != "")
                    row[SuperFundDesktopBrokerColumns.TFNExemption] = "";
                else
                    row[SuperFundDesktopBrokerColumns.TFNExemption] = "1";
            }

            superFundDesktopBrokerColumns.Table.Rows.Add(row);

            response.Data.Add(DesktopBrokerExportKeys.SuperFund.ToString(), superFundDesktopBrokerColumns.Table);
        }

        public void AddCompnayDetailsToDesktopBrokerAccountResponse(DesktopBrokerDataResponse response, CorporateEntity entity)
        {
            CompanyDetailsDesktopBrokerColumns companyDetailsDesktopBrokerColumns = new CompanyDetailsDesktopBrokerColumns();
            DataRow row = companyDetailsDesktopBrokerColumns.Table.NewRow();
            if (entity != null)
            {
                row[CompanyDetailsDesktopBrokerColumns.ABN] = entity.ABN;
                row[CompanyDetailsDesktopBrokerColumns.ACN] = entity.ACN;
                row[CompanyDetailsDesktopBrokerColumns.CompanyName] = entity.Name;
            }

            companyDetailsDesktopBrokerColumns.Table.Rows.Add(row);
            response.Data.Add(DesktopBrokerExportKeys.CompanyDetails.ToString(), companyDetailsDesktopBrokerColumns.Table);
        }

        public void AddTrustAccountToDesktopBrokerAccountResponse(DesktopBrokerDataResponse response, ClientIndividualEntity entity)
        {
            TrustAccountDetailsDesktopBrokerColumns trustAccountDetailsDesktopBrokerColumns = new TrustAccountDetailsDesktopBrokerColumns();
            DataRow row = trustAccountDetailsDesktopBrokerColumns.Table.NewRow();
            if (entity != null)
            {
                row[TrustAccountDetailsDesktopBrokerColumns.TrustABN] = entity.ABN;
                row[TrustAccountDetailsDesktopBrokerColumns.DesignationForAccount] = entity.AccountDesignation;
                row[TrustAccountDetailsDesktopBrokerColumns.TrustName] = entity.Name;
                row[TrustAccountDetailsDesktopBrokerColumns.TaxFileNumber] = entity.TFN;
                if (entity.TFN != "")
                    row[SuperFundDesktopBrokerColumns.TFNExemption] = "";
                else
                    row[SuperFundDesktopBrokerColumns.TFNExemption] = "1";
            }
            trustAccountDetailsDesktopBrokerColumns.Table.Rows.Add(row);

            response.Data.Add(DesktopBrokerExportKeys.TrustAccountDetails.ToString(), trustAccountDetailsDesktopBrokerColumns.Table);
        }

        public void AddTrustAccountToDesktopBrokerAccountResponse(DesktopBrokerDataResponse response, CorporateEntity entity)
        {
            TrustAccountDetailsDesktopBrokerColumns trustAccountDetailsDesktopBrokerColumns = new TrustAccountDetailsDesktopBrokerColumns();
            DataRow row = trustAccountDetailsDesktopBrokerColumns.Table.NewRow();
            if (entity != null)
            {
                row[TrustAccountDetailsDesktopBrokerColumns.TrustABN] = entity.ABN;
                row[TrustAccountDetailsDesktopBrokerColumns.DesignationForAccount] = entity.AccountDesignation;
                row[TrustAccountDetailsDesktopBrokerColumns.TrustName] = entity.Name;
                row[TrustAccountDetailsDesktopBrokerColumns.TaxFileNumber] = entity.TFN;
                if (entity.TFN != "")
                    row[SuperFundDesktopBrokerColumns.TFNExemption] = "";
                else
                    row[SuperFundDesktopBrokerColumns.TFNExemption] = "1";
            }
            trustAccountDetailsDesktopBrokerColumns.Table.Rows.Add(row);

            response.Data.Add(DesktopBrokerExportKeys.TrustAccountDetails.ToString(), trustAccountDetailsDesktopBrokerColumns.Table);
        }

        public void CheckDoubleRows(DesktopBrokerDataResponse response)
        {

            if (!response.Data.ContainsKey(DesktopBrokerExportKeys.Applicant_1.ToString())) return;
       
            if (response.Data[DesktopBrokerExportKeys.Applicant_1.ToString()].Rows.Count == 2 ||
             (response.Data.ContainsKey(DesktopBrokerExportKeys.Applicant_2.ToString()) && response.Data[DesktopBrokerExportKeys.Applicant_2.ToString()].Rows.Count == 2))
            {
                foreach (var value in response.Data)
                {
                    if (value.Value.Rows.Count == 1)
                    {
                        DataRow row = value.Value.NewRow();
                        row.ItemArray = value.Value.Rows[0].ItemArray;
                        value.Value.Rows.Add(row);
                        response.Data[value.Key] = value.Value;
                    }
                }
            }
        }

        public void AddEmptyApplicant(DesktopBrokerDataResponse response, string key)
        {
            AccountHolderDesktopBrokerColumns accountHolderDesktopBroker = new AccountHolderDesktopBrokerColumns();
            accountHolderDesktopBroker.Table.Rows.Add(accountHolderDesktopBroker.Table.NewRow());
            response.Data.Add(key, accountHolderDesktopBroker.Table);
        }

        #endregion

        #region Account Process Status

        public string GetAccountProcessStatus(CorporateEntity entity)
        {
            if (entity == null) return string.Empty;
            return GetAccountProcessStatus(entity.ListAccountProcess, GetAssociatedModels(entity.Servicetype));
        }

        public string GetAccountProcessStatus(ClientIndividualEntity entity)
        {
            if (entity == null) return string.Empty;
            return GetAccountProcessStatus(entity.ListAccountProcess, GetAssociatedModels(entity.Servicetype));
        }


        private List<ModelEntity> GetAssociatedModels(Oritax.TaxSimp.Common.ServiceType serviceType)
        {
            var modelCodes = new List<string>();
            var models = new List<ModelEntity>();

            if (serviceType != null)
            {
                if (serviceType.DO_IT_FOR_ME)
                {
                    if (serviceType.DoItForMe != null)
                        modelCodes.Add(serviceType.DoItForMe.ProgramCode);
                }

                if (serviceType.DO_IT_WITH_ME)
                {
                    if (serviceType.DoItWithMe != null)
                        modelCodes.Add(serviceType.DoItWithMe.ProgramCode);
                }

                if (serviceType.DO_IT_YOURSELF)
                {
                    if (serviceType.DoItYourSelf != null)
                        modelCodes.Add(serviceType.DoItYourSelf.ProgramCode);
                }
            }

            var organization = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IModelStructure;
            if (organization != null)
            {
                foreach (var code in modelCodes)
                {
                    if (string.IsNullOrWhiteSpace(code)) continue;
                    var model = organization.Model.FirstOrDefault(c => c.ProgramCode == code);
                    if (model != null)
                        models.Add(model);
                }
            }
            return models;
        }

        private string GetAccountProcessStatus(List<AccountProcessTaskEntity> taskList, List<ModelEntity> models)
        {
            if (taskList == null) return string.Empty;
            List<string> StatusList = new List<string>();
            int greenCount = 0;
            int redCount = 0;
            int orangeCount = 0;
            int totalCount = 0;


            if (models != null)
            {
                foreach (var model in models)
                {
                    foreach (var asset in model.Assets)
                    {
                        foreach (var product in asset.Products)
                        {
                            var task = taskList.FirstOrDefault(c => c.TaskID == product.ID);
                            if (task == null)
                            {
                                taskList.Add(new AccountProcessTaskEntity { AssetID = asset.ID, TaskID = product.ID, ModelID = model.ID, LinkedEntityType = product.EntityType, IsCompleted = false, IsExempted = false, OldStatus = string.Empty });
                            }
                        }
                    }
                }
            }

            List<Guid> removedTasks = new List<Guid>();
            foreach (var task in taskList)
            {
                bool notfound = true;
                foreach (var model in models)
                {
                    foreach (var asset in model.Assets)
                    {
                        foreach (var product in asset.Products)
                        {
                            if (task.TaskID == product.ID)
                                notfound = false;
                        }
                    }
                }
                if (notfound)
                    removedTasks.Add(task.TaskID);
            }

            foreach (var item in removedTasks)
            {
                var task = taskList.FirstOrDefault(c => c.TaskID == item);
                if (task != null)
                    taskList.Remove(task);
            }

            foreach (var each in taskList)
            {
                var organizationunitcm = Broker.GetCMImplementation(each.LinkedEntity.Clid, each.LinkedEntity.Csid) as OrganizationUnitCM;
                if (each.IsExempted == false && each.LinkedEntityType == OrganizationType.ManagedInvestmentSchemesAccount)
                {
                    StatusList.Add("Active"); //Patch to fix MIS Accounts with no status Jed Detera 5/6/12
                }
                else
                {
                    StatusList.Add(organizationunitcm == null ? null : organizationunitcm.OrganizationStatus);
                }
            }

            totalCount = StatusList.Count;
            foreach (var status in StatusList)
            {

                if (string.IsNullOrWhiteSpace(status))
                {
                    redCount++;
                    continue;
                }
                else
                {
                    switch (status.ToLower())
                    {
                        case "pending":
                            orangeCount++;
                            break;
                        case "active":
                            greenCount++;
                            break;
                        default:
                            redCount++;
                            break;
                    }
                }
            }

            if (redCount == totalCount)
                return "";
            else if (greenCount == totalCount)
                return "Active";
            else
                return "Pending";
        }

        #endregion
	}
}
