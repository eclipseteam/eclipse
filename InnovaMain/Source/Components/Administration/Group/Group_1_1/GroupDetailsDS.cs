#region CHANGE_HISTORY
/*
 $Header: /2002/3. Implementation/NewConstruction/Components/BusinessStructure/Group/Group-1-0/GroupDetailsDS.cs 5     26/06/03 2:02p Pveitc $
 $History: GroupDetailsDS.cs $
 * 
 * *****************  Version 5  *****************
 * User: Pveitch      Date: 26/06/03   Time: 2:02p
 * Updated in $/2002/3. Implementation/NewConstruction/Components/BusinessStructure/Group/Group-1-0
 * Updates to bring Australian Group into line with the use cases
 * 
 * *****************  Version 4  *****************
 * User: Pveitch      Date: 29/04/03   Time: 4:30p
 * Updated in $/2002/3. Implementation/Construction/BusinessStructureCM/Group/Group-1-0
 * Updates to bring the Business structure into line with the use cases
 * 
 * *****************  Version 3  *****************
 * User: Pveitch      Date: 17/04/03   Time: 5:27p
 * Updated in $/2002/3. Implementation/Construction/BusinessStructureCM/Group/Group-1-0
 * 
 * *****************  Version 2  *****************
 * User: Sireland     Date: 15/04/03   Time: 12:01p
 * Updated in $/2002/3. Implementation/Construction/BusinessStructureCM/Group/Group-1-0
 * Remove Warnings
 * 
 * *****************  Version 1  *****************
 * User: Sshrimpton   Date: 18/12/02   Time: 4:56p
 * Created in $/2002/3. Implementation/Construction/BusinessStructureCM/Group/Group-1-1-1
 * 
 * *****************  Version 6  *****************
 * User: Pveitch      Date: 7/11/02    Time: 3:07p
 * Updated in $/2002/3. Implementation/Elaboration4/CM/Group
 * Header information added
*/
#endregion
using System;
using System.Data;
using Oritax.TaxSimp.CM.OrganizationUnit;

namespace Oritax.TaxSimp.CM.Group
{
	/// <summary>
	/// DSCalculationModule is a DataSet that contains all of the persistent data for a CM instance.
	/// </summary>
	[Serializable] 
	public class GroupDetailsDS : OrganisationUnitDetailsDS
	{
		public const String ENTITYNAME_FIELD				= "ENTITYNAME_FIELD";
		public const String ENTITYCSID_FIELD				= "ENTITYCSID_FIELD";

		private Guid cLID;
		private Guid cSID;
		private Guid cIID;
		private Guid currentScenario;

		public GroupDetailsDS()
		{
			this.cLID = Guid.Empty;
			this.cSID = Guid.Empty;
			this.cIID = Guid.Empty;
		}

		public Guid CLID{get{return cLID;}set{cLID=value;}}
		public Guid CSID{get{return cSID;}set{cSID=value;}}
		public Guid CIID{get{return cIID;}set{cIID=value;}}
		public Guid CurrentScenario{get{return currentScenario;}set{currentScenario=value;}}
	}
}

