using System;
using System.Data;
using Oritax.TaxSimp.Calculation;

namespace Oritax.TaxSimp.CM.Group
{
	public class GroupStructureDS:ReportBaseDS
	{
		#region Constants --------------------------------------------------------------
		// Group Structure table
		public const string GROUPSTRUCTURE_TABLE = "REPORTTABLE";
		public const string ID_FIELD = "ID";
		public const string PARENTID_FIELD = "PARENTID";
		public const string LEVEL_FIELD = "LEVEL";
		public const string DISPLAY_ORDER_FIELD = "DISPLAY_ORDER";
		public const string ENTITY_NAME_FIELD = "ENTITY_NAME";
		public const string ENTITY_URL_FIELD = "ENTITY_URL";
		public const string COLUMN_TITLE_FIELD = "COLUMN_TITLE";
		public const string COLUMN_VALUE_FIELD = "COLUMN_VALUE";
		public const string COLUMN_ORDER_FIELD = "COLUMN_ORDER";
		public const string ISGROUP_FIELD = "ISGROUP";
		public const string CSID_FIELD = "CSID";
		public const string CLID_FIELD = "CLID";
		public const string DUPLICATE = "DUPLICATE";
		// Exceptions Table
		public const string GROUPSTRUCTURE_EXCEPTIONSLIST_TABLE ="EXCEPTIONSLIST_TABLE";
		public const string EL_CSID_FIELD = "EL_CSID";
		public const string EL_CLID_FIELD = "EL_CLID";
		public const string EL_ENTITY_NAME_FIELD = "EL_ENTITY_NAME";
		public const string EL_LEVEL_FIELD = "EL_LEVEL";
		public const string EL_PARENT_ENTITY_NAME_FIELD = "EL_PARENT_ENTITY_NAME";
		#endregion
		#region Private members --------------------------------------------------------
		private string currentReportingUnitCLID = string.Empty;
		private string currentReportingUnitCSID = string.Empty;
		private bool viewUnitType = false;
		private bool viewTaxStatus = false;
		private bool viewMemberScenario = false;
		private string userName = string.Empty;
		private string breabCrumbName = string.Empty;
		private string breadCrumbScenario = string.Empty;
		#endregion
		#region Main Constructor -------------------------------------------------------
		public GroupStructureDS()
		{
			buildTable();
		}
		#endregion
		#region Public Methods ---------------------------------------------------------
		/// <summary>
		/// This method will add a row as  the Parent Row. It will have a default value of 0 (zero) as its parentid. There for making this row the Root row.
		/// </summary>
		/// <param name="id">Unique ID for the Row</param>
		/// <param name="level">The level(Tier) of the reporting unit. </param>
		/// <param name="display_order">Display order in ascending order. Used by crystal reports</param>
		/// <param name="entity_name">Name of reporting unit. (Entity or Group)</param>
		/// <param name="entity_url">[Currently not used- future requirement] - The URL for this reporting unit. URL must contain the full path. i.e. Http://... with page name, CSID and CLID </param>
		/// <param name="column_title">Title of Column. Tax Status, Unit Type or Member Scenario</param>
		/// <param name="column_value">Actual value in relation to the column title.</param>
		/// <param name="column_order">Display order of the columns. Used by crystal report.</param>
		/// <param name="isGroup">Denote if reporting unit is group or not. Used by crystal report for the formatting.</param>
		/// <param name="csid">CSID</param>
		/// <param name="clid">CLID</param>
		public void AddRow(string id,string level,string display_order,string entity_name,string entity_url,string column_title,string column_value,string column_order,bool isGroup,string csid, string clid)
		{
			addToTable(int.Parse(id),0,int.Parse(level),display_order,entity_name,entity_url,column_title,column_value,column_order,isGroup,csid,clid);
		}

		/// <summary>
		/// This method will add a row as the Child Row. The parent Id is the id number of a Group in the structure.
		/// </summary>
		/// <param name="id">Unique ID for the Row</param>
		/// <param name="parentid"></param>
		/// <param name="level">The level(Tier) of the reporting unit. </param>
		/// <param name="display_order">Display order in ascending order. Used by crystal reports</param>
		/// <param name="entity_name">Name of reporting unit. (Entity or Group)</param>
		/// <param name="entity_url">[Currently not used- future requirement] - The URL for this reporting unit. URL must contain the full path. i.e. Http://... with page name, CSID and CLID </param>
		/// <param name="column_title">Title of Column. Tax Status, Unit Type or Member Scenario</param>
		/// <param name="column_value">Actual value in relation to the column title.</param>
		/// <param name="column_order">Display order of the columns. Used by crystal report.</param>
		/// <param name="isGroup">Denote if reporting unit is group or not. Used by crystal report for the formatting.</param>
		/// <param name="csid">CSID</param>
		/// <param name="clid">CLID</param>
		public void AddChildRow(string id,string parentid,string level,string display_order,string entity_name,string entity_url,string column_title,string column_value,string column_order,bool isGroup,string csid, string clid)
		{
			addToTable(int.Parse(id),int.Parse(parentid),int.Parse(level),display_order,entity_name,entity_url,column_title,column_value,column_order,isGroup,csid,clid);
		}

		/// <summary>
		/// If a reporting unit has appeared more than once in the group structure, that reporting unit will added to this list.
		/// </summary>
		/// <param name="csid">CSID of the reporting unit</param>
		/// <param name="clid">CLID of the reporting unit</param>
		/// <param name="level">Level number at where this has appeared as a duplicate entry</param>
		/// <param name="entityName">Reporting unit name</param>
		/// <param name="parentEntityName">Reporting Unit name of the parent Reporting unit that it belongs to.</param>
		public void AddToExceptionList(string csid, string clid, string level, string entityName, string parentEntityName)
		{
			// Find if entityName already in the the Exception list or not. Only add it if entityName does not exist.
			DataRow[] foundMatchingRows = this.Tables[GROUPSTRUCTURE_EXCEPTIONSLIST_TABLE].Select(EL_ENTITY_NAME_FIELD+"='"+entityName+"'");

			if(foundMatchingRows.Length<1)
			{
				DataRow newRow = this.Tables[GROUPSTRUCTURE_EXCEPTIONSLIST_TABLE].NewRow();

				newRow[EL_CSID_FIELD] =csid;
				newRow[EL_CLID_FIELD] =clid;
				newRow[EL_ENTITY_NAME_FIELD] =entityName;
				newRow[EL_PARENT_ENTITY_NAME_FIELD] =parentEntityName;
				newRow[EL_LEVEL_FIELD] =int.Parse(level);

				this.Tables[GROUPSTRUCTURE_EXCEPTIONSLIST_TABLE].Rows.Add(newRow);
			}
		}

		/// <summary>
		/// This method iterates through the exception list and matches the reporting unit CLID in the group structure table. If a Match is found, it will flag that 
		/// entry as duplicate entry. Sets the DUPLICATE field to true. This is used the crystal reports to format it the color red.
		/// </summary>
		public void HighlightDuplicateEntries()
		{
			if(this.Tables[GROUPSTRUCTURE_EXCEPTIONSLIST_TABLE].Rows.Count>0)
			{
				
				foreach(DataRow exceptionListRow in this.Tables[GROUPSTRUCTURE_EXCEPTIONSLIST_TABLE].Rows)
				{
					string exceptionListCLID = exceptionListRow[EL_CLID_FIELD].ToString();

					foreach(DataRow drow in this.Tables[GROUPSTRUCTURE_TABLE].Rows)
					{
						if(exceptionListCLID.Equals(drow[CLID_FIELD].ToString()))
							drow[DUPLICATE] = "True";
					}
				}
				// apply changes made
				this.Tables[GROUPSTRUCTURE_TABLE].AcceptChanges();
			}
			else
			{
				// done purely for the Report formating purposes...
				this.AddToExceptionList("-","-","0","-","-");
			}
		}

		/// <summary>
		/// Check if the passed in CLID already exists in the group structure. Returns True if match found, else returns false.
		/// </summary>
		/// <param name="clid"></param>
		/// <returns></returns>
		public bool isDuplicateEntry(string clid)
		{
			// this will check for duplicate Entries in the main Table...
			DataRow[] duplicateRows = this.Tables[GROUPSTRUCTURE_TABLE].Select(CLID_FIELD+"='" +clid+"'");

			if(duplicateRows.Length>0)
				return true;
			else
				return false;
		}

        public bool ContainsDuplicateEntities()
        {
            return GetDuplicateEntities().Length > 0;
        }

        public DataTable GetGroupStructureTable()
        {
            DataTable groupStructureTable = this.Tables[GroupStructureDS.GROUPSTRUCTURE_TABLE];
            return groupStructureTable;
        }

		#endregion
		#region Public Properties ------------------------------------------------------
		
		public string BreadCrumbName
		{
			get
			{
				return this.breabCrumbName;
			}
			set
			{
				this.breabCrumbName=value;
			}
		}

		public string BreadCrumbScenario
		{
			get
			{
				return this.breadCrumbScenario;
			}
			set
			{
				this.breadCrumbScenario=value;
			}
		}

		public string GetBreadCrumb
		{
			get
			{
				return this.BreadCrumbName+"["+this.BreadCrumbScenario+"]";
			}
		}


		/// <summary>
		/// Holds the CLID of the selected Reporting Unit (Group).
		/// </summary>
		public string ThisGroupCLID
		{
			get
			{
				return currentReportingUnitCLID;
			}
			set
			{
				currentReportingUnitCLID = value;
			}
		}

		/// <summary>
		/// Holds the SCID of the selected Reporting Unit (Group).
		/// </summary>
		public string ThisGroupCSID
		{
			get
			{
				return currentReportingUnitCSID;
			}
			set
			{
				currentReportingUnitCSID = value;
			}
		}

		/// <summary>
		/// Holds state of the view status check box checked value.
		/// </summary>
		public bool ViewTaxStatus
		{
			get
			{
				return viewTaxStatus;
			}
			set
			{
				viewTaxStatus = value;
			}
		}

		/// <summary>
		///  Holds state of the Unit Type check box checked value.
		/// </summary>
		public bool ViewUnitType
		{
			get
			{
				return viewUnitType;
			}
			set
			{
				viewUnitType = value;
			}
		}

		/// <summary>
		///  Holds state of the view member Scenario check box checked value.
		/// </summary>
		public bool ViewMemberScenario
		{
			get
			{
				return viewMemberScenario;
			}
			set
			{
				viewMemberScenario = value;
			}
		}

		/// <summary>
		/// Currently logged in username
		/// </summary>
		public string UserName
		{
			get
			{
				return userName;
			}
			set
			{
				userName = value;
			}
		}

		#endregion
		#region Private methods --------------------------------------------------------

		private void buildTable()
		{
			#region Main Grouip structure Table
			DataTable table;

			table = new DataTable(GROUPSTRUCTURE_TABLE);
			table.Columns.Add(ID_FIELD , typeof(System.Int32));
			table.Columns.Add(PARENTID_FIELD , typeof(System.Int32));
			table.Columns.Add(LEVEL_FIELD , typeof(System.Int32));
			table.Columns.Add(DISPLAY_ORDER_FIELD , typeof(System.String));
			table.Columns.Add(ENTITY_NAME_FIELD , typeof(System.String));
			table.Columns.Add(ENTITY_URL_FIELD , typeof(System.String));
			table.Columns.Add(COLUMN_TITLE_FIELD , typeof(System.String));
			table.Columns.Add(COLUMN_VALUE_FIELD , typeof(System.String));
			table.Columns.Add(COLUMN_ORDER_FIELD , typeof(System.String));
			table.Columns.Add(ISGROUP_FIELD , typeof(System.String));
			table.Columns.Add(CSID_FIELD , typeof(System.String));
			table.Columns.Add(CLID_FIELD , typeof(System.String));
			table.Columns.Add(DUPLICATE , typeof(System.String));

			this.Tables.Add(table);
			#endregion
			#region Group Structure duplicates entry Exception List 
			DataTable exceptionList_table;

			exceptionList_table = new DataTable(GROUPSTRUCTURE_EXCEPTIONSLIST_TABLE);

			exceptionList_table.Columns.Add(EL_CSID_FIELD , typeof(System.String));
			exceptionList_table.Columns.Add(EL_CLID_FIELD , typeof(System.String));
			exceptionList_table.Columns.Add(EL_ENTITY_NAME_FIELD , typeof(System.String));
			exceptionList_table.Columns.Add(EL_PARENT_ENTITY_NAME_FIELD , typeof(System.String));
			exceptionList_table.Columns.Add(EL_LEVEL_FIELD , typeof(System.Int32));

			this.Tables.Add(exceptionList_table);
			#endregion
		}

		private void addToTable (int id,int parentid,int level,string display_order,string entity_name,string entity_url,string column_title,string column_value,string column_order,bool isGroup,string csid, string clid)
		{
			DataRow newRow = this.Tables[GROUPSTRUCTURE_TABLE].NewRow();

			newRow[ID_FIELD] =id;
			newRow[PARENTID_FIELD] =parentid;
			newRow[LEVEL_FIELD] =level;
			newRow[DISPLAY_ORDER_FIELD] =display_order;
			newRow[ENTITY_NAME_FIELD] =entity_name;
			newRow[ENTITY_URL_FIELD] =entity_url;
			newRow[COLUMN_TITLE_FIELD] =column_title;
			
			if(column_value.Length>0 && column_value.Length>30) 
				newRow[COLUMN_VALUE_FIELD] =column_value.Substring(0,27) +"..." ;
			else
				newRow[COLUMN_VALUE_FIELD] =column_value;

			newRow[COLUMN_ORDER_FIELD] =column_order;
			newRow[ISGROUP_FIELD] =isGroup;
			newRow[CSID_FIELD] =csid;
			newRow[CLID_FIELD] =clid;
			newRow[DUPLICATE] ="False";
		
			this.Tables[GROUPSTRUCTURE_TABLE].Rows.Add(newRow);
		}

        private DataRow[] GetDuplicateEntities()
        {
            return GetGroupStructureTable().Select("DUPLICATE = 'True'");
        }

        #endregion

	}
}
