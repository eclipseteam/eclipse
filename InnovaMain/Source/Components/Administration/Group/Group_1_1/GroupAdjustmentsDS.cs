#region CHANGE_HISTORY
/*
 $Header: /2002/3. Implementation/Construction/BusinessStructureCM/Group/Group-1-1-1/GroupAdjustmentsDS.cs 2     6/02/03 2:46p Paubailey $
 $History: GroupAdjustmentsDS.cs $
 * 
 * *****************  Version 2  *****************
 * User: Paubailey    Date: 6/02/03    Time: 2:46p
 * Updated in $/2002/3. Implementation/Construction/BusinessStructureCM/Group/Group-1-1-1
 * 
 * *****************  Version 1  *****************
 * User: Sshrimpton   Date: 18/12/02   Time: 4:56p
 * Created in $/2002/3. Implementation/Construction/BusinessStructureCM/Group/Group-1-1-1
 * 
 * *****************  Version 1  *****************
 * User: Paubailey    Date: 10/12/02   Time: 5:13p
 * Created in $/2002/3. Implementation/Elaboration4/CM/Group
 * 
 * *****************  Version 3  *****************
 * User: Pveitch      Date: 14/11/02   Time: 12:05p
 * Updated in $/2002/3. Implementation/Elaboration4/CM/Group
 * 
 * *****************  Version 2  *****************
 * User: Pveitch      Date: 7/11/02    Time: 3:07p
 * Updated in $/2002/3. Implementation/Elaboration4/CM/Group
 * Header information added
*/
#endregion
using System;
using System.Data;

namespace Oritax.TaxSimp.CM.Group
{
	/// <summary>
	/// DSCalculationModule is a DataSet that contains all of the persistent data for a CM instance.
	/// </summary>
	[Serializable] 
	public class GroupAdjustmentsDS : DataSet
	{
		public const String INCLUDEDADJUSTMENTS_TABLE			= "INCLUDEDADJUSTMENTS_TABLE";
		public const String ADJUSTMENTNAME_FIELD				= "ADJUSTMENTNAME_FIELD";
		public const String ADJUSTMENTCLID_FIELD				= "ADJUSTMENTCLID_FIELD";
		public const String ADJUSTMENTCSID_FIELD				= "ADJUSTMENTCSID_FIELD";

		// added by PV, Nov 14 2002, CQ Issue #: 669
		private string m_strBreadCrumb;

		public GroupAdjustmentsDS()
		{
			DataTable table;

			table = new DataTable(INCLUDEDADJUSTMENTS_TABLE);
			table.Columns.Add(ADJUSTMENTNAME_FIELD, typeof(System.String));
			table.Columns.Add(ADJUSTMENTCLID_FIELD, typeof(System.String));
			table.Columns.Add(ADJUSTMENTCSID_FIELD, typeof(System.String));
			this.Tables.Add(table);
		}

		// added by PV, Nov 14 2002, CQ Issue #: 669
		public string BreadCrumb
		{
			get
			{
				return this.m_strBreadCrumb;
			}
			set
			{
				this.m_strBreadCrumb = value;
			}
		}
	}
}

