﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Oritax.TaxSimp.CM.Group.Data
{
    [Serializable]
    public class UserBaseClass
    {
        [XmlElement("CurrentUserName")]
        public String CurrentUserName { get; set; }
    }
}
