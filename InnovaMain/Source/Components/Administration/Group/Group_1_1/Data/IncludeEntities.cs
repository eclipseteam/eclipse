﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Oritax.TaxSimp.CM.Group.Data
{
    [Serializable]
    public class IncludeEntities
    {
        [XmlElement("EntityName")]
        public string EntityName { get; set; }

        [XmlElement("EntityCLID")]
        public string EntityCLID { get; set; }

        [XmlElement("EntityCSID")]
        public string EntityCSID { get; set; }

        [XmlElement("TaxStatus")]
        public string TaxStatus { get; set;}

        [XmlElement("AdjustmentEntity")]
        public string AdjustmentEntity { get; set; }
    }
}
