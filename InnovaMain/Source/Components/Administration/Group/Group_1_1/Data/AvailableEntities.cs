﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Oritax.TaxSimp.CM.Group.Data
{
    [Serializable]
    public class AvailableEntities
    {
        [XmlElement("EntityName")]
        public string EntityName { get; set; }

        [XmlElement("EntityCLID")]
        public string EntityCLID { get; set; }
    }
}
