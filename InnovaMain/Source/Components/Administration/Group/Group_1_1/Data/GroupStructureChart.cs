﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Oritax.TaxSimp.CM.Group.Data
{
    [Serializable]
    //[XmlRoot("GroupStructureChart")]
    public class GroupStructureChart
    {
        //public string GROUPSTRUCTURE_TABLE { get; set; } // "REPORTTABLE";

        [XmlElement("ID_FIELD")]
        public string ID_FIELD { get; set; } // "ID";

        [XmlElement("PARENTID_FIELD")]
        public string PARENTID_FIELD { get; set; } // "PARENTID";

        [XmlElement("LEVEL_FIELD")]
        public string LEVEL_FIELD { get; set; } // "LEVEL";

        [XmlElement("DISPLAY_ORDER_FIELD")]
        public string DISPLAY_ORDER_FIELD { get; set; } // "DISPLAY_ORDER";

        [XmlElement("ENTITY_NAME_FIELD")]
        public string ENTITY_NAME_FIELD { get; set; } // "ENTITY_NAME";

        [XmlElement("ENTITY_URL_FIELD")]
        public string ENTITY_URL_FIELD { get; set; } // "ENTITY_URL";

        [XmlElement("COLUMN_TITLE_FIELD")]
        public string COLUMN_TITLE_FIELD { get; set; } // "COLUMN_TITLE";

        [XmlElement("COLUMN_VALUE_FIELD")]
        public string COLUMN_VALUE_FIELD { get; set; } // "COLUMN_VALUE";

        [XmlElement("COLUMN_ORDER_FIELD")]
        public string COLUMN_ORDER_FIELD { get; set; } // "COLUMN_ORDER";

        [XmlElement("ISGROUP_FIELD")]
        public string ISGROUP_FIELD { get; set; } // "ISGROUP";

        [XmlElement("CSID_FIELD")]
        public string CSID_FIELD { get; set; } // "CSID";

        [XmlElement("CLID_FIELD")]
        public string CLID_FIELD { get; set; } // "CLID";

        [XmlElement("DUPLICATE")]
        public string DUPLICATE { get; set; } // "DUPLICATE";
    }
}
