﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Oritax.TaxSimp.CM.Group.Data
{
    [Serializable]
    [XmlRoot("GroupStructureChartList")]
    public class GroupStructureChartList : List<GroupStructureChart>
    {

    }
}
