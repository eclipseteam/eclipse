﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.CM.Group.Data
{
    [Serializable]
    [XmlRoot("GroupMembersData")]
    public class GroupMembersData: UserBaseClass
    {
        [XmlElement("AvailableEntitiesList")]
        public AvailableEntitiesList AvailableEntitiesList { get; set; }

        [XmlElement("AvailableEntitiesOnlyList")]
        public EntitiesList AvailableEntitiesOnlyList { get; set; }

        [XmlElement("IncludeEntitiesList")]
        public IncludeEntitiesList IncludeEntitiesList { get; set; }

        [XmlElement("IncludeEntitiesOnlyList")]
        public EntitiesList IncludeEntitiesOnlyList { get; set; }
    }
}
