﻿using System;
using System.Collections.Generic;
using System.Data;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.Group.Data;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DSCalculation;
using Oritax.TaxSimp.Extensions;

namespace Oritax.TaxSimp.CM.Group
{
    public partial class GroupCM
    {
        public String UserName { get; set; }

        #region Override
        public override string GetDataStream(int type, string data)
        {
            String xml = null;
            switch (type)
            {
                case 0:
                case 1:
                case 2:
                case 3:
                    {
                        UserBaseClass userbase = data.ToNewOrData<UserBaseClass>();
                        this.UserName = userbase.CurrentUserName;
                        xml = this.GetDataForExtraction().ToXmlString();
                    }
                    break;
                case 4:
                case 5:
                case 6:
                    {
                        xml = this.GetGroupStructureChartData(this.CLID).ToXmlString();
                    }
                    break;
                default:
                    throw new Exception("Unsupported data type");
            }
            return xml;
        }

        public override string OnUpdateDataStream(int type, string data)
        {
            String xml = null;
            switch (type)
            {
                case 4:                    
                case 5:
                    {
                        GroupMembersData groupmembersdata = data.ToNewOrData<GroupMembersData>();
                        this.UserName = groupmembersdata.CurrentUserName;
                        xml = this.UpdateDataGroup(groupmembersdata, type);
                    }
                    break;                
                default:
                    throw new Exception("Unsupported data type");
            }
            return xml;
        }
        #endregion

        #region GroupMembers
        public GroupMembersDS OnGetDataGroupCMSL(String username)
        {
            GroupMembersDS data = new GroupMembersDS();
            data.Username = username;
            base.OnGetData(data);
            GroupMembersDS objGroupMembers = (GroupMembersDS)data;

            objGroupMembers.BreadCrumb = this.ConstructBreadcrumb();
            //Get the name of the configuration workpaper for the group type
            //so that screens can redirect back to it.
            objGroupMembers.ConfigureWorkpaper = this.ConfigureWPURL;

            // List the available business entities (groups and entities)
            // Get the organization CM so that the entities and groups can be listed
            objGroupMembers.Tables[GroupMembersDS.INCLUDEDENTITIES_TABLE].Rows.Clear();

            ICalculationModule orgCM = Broker.GetWellKnownCM(WellKnownCM.Organization);

            DataTable logicalModuleRows = this.Broker.GetLCMsByParent(orgCM.CLID);

            this.Broker.ReleaseBrokerManagedComponent(orgCM);

            foreach (DataRow logicalModuleRow in logicalModuleRows.Rows)
            {
                Guid entityCLID = (Guid)logicalModuleRow[DSLogicalModulesInfo.LCM_CLID_FIELD];

                ILogicalModule lCM = this.Broker.GetLogicalCM(entityCLID);

                ICalculationModule businessEntityCM;
                GroupMember objGroupMember = this.includedGroupMembers[lCM.CLID];
                if (null == objGroupMember || objGroupMember.IsDeleted)
                {
                    //Do not want consolidation adjustment entities to be displayed in any available entities lists.
                    if (lCM.CMType != new Guid("5A5BC2D6-50CB-412A-B151-BF8E16E17403"))//If it is a consolidation adjustment entity then stop it from being listed in available entities
                    {
                        businessEntityCM = lCM[lCM.CurrentScenario];

                        DataRow entityRow = objGroupMembers.Tables[GroupMembersDS.AVAILABLEENTITIES_TABLE].NewRow();
                        entityRow[GroupMembersDS.ENTITYNAME_FIELD] = ((IBrokerManagedComponent)businessEntityCM).Name;
                        entityRow[GroupMembersDS.ENTITYCLID_FIELD] = businessEntityCM.CLID;
                        objGroupMembers.Tables[GroupMembersDS.AVAILABLEENTITIES_TABLE].Rows.Add(entityRow);

                        this.Broker.ReleaseBrokerManagedComponent(businessEntityCM);
                    }
                }
                else //This entity is part of the group
                {
                    businessEntityCM = lCM[objGroupMember.CSID];

                    DataRow entityRow = objGroupMembers.Tables[GroupMembersDS.INCLUDEDENTITIES_TABLE].NewRow();

                    string strCalculatedName = String.Empty;
                    strCalculatedName += businessEntityCM.Name;

                    entityRow[GroupMembersDS.ENTITYNAME_FIELD] = strCalculatedName;
                    entityRow[GroupMembersDS.ENTITYCLID_FIELD] = objGroupMember.CLID.ToString();
                    entityRow[GroupMembersDS.ENTITYCSID_FIELD] = objGroupMember.CSID.ToString();
                    entityRow[GroupMembersDS.ADJUSTMENTENTITY_FIELD] = objGroupMember.Adjustment;
                    objGroupMembers.Tables[GroupMembersDS.INCLUDEDENTITIES_TABLE].Rows.Add(entityRow);

                    this.Broker.ReleaseBrokerManagedComponent(businessEntityCM);
                }
                this.Broker.ReleaseBrokerManagedComponent(lCM);
            }
            return objGroupMembers;
        }

        private string UpdateDataGroup(GroupMembersData groupmembersdata, int type)
        {
            GroupMembersDS groupmembersds = this.OnGetDataGroupCMSL(this.UserName);
            if (type == 4)
            {
                foreach (Entities each1 in groupmembersdata.AvailableEntitiesOnlyList)
                {
                    this.GetAvailableEntities(groupmembersdata.AvailableEntitiesList, each1);
                }
                foreach (AvailableEntities each2 in groupmembersdata.AvailableEntitiesList)
                {
                    ProcessAvailable(each2.EntityCLID, each2.EntityName, groupmembersds.Tables[GroupMembersDS.INCLUDEDENTITIES_TABLE]);
                }
            }
            if (type == 5)
            {
                foreach (Entities each1 in groupmembersdata.IncludeEntitiesOnlyList)
                {
                    this.GetIncludeEntities(groupmembersdata.IncludeEntitiesList, each1);
                }
                foreach (IncludeEntities each2 in groupmembersdata.IncludeEntitiesList)
                {
                    ProcessIncluded(each2, groupmembersds.Tables[GroupMembersDS.INCLUDEDENTITIES_TABLE]);
                }
            }
            groupmembersds.AcceptChanges();
            this.SetData(groupmembersds);
            return "";
        }

        private void GetAvailableEntities(AvailableEntitiesList availableentitieslist, Entities entities)
        {
            foreach (AvailableEntities each in availableentitieslist)
            {
                if (entities.EntityCLID == each.EntityCLID)
                {
                    availableentitieslist.Remove(each);
                    break;
                }
            }
        }

        private void GetIncludeEntities(IncludeEntitiesList includeentitieslist, Entities entities)
        {
            foreach (IncludeEntities each in includeentitieslist)
            {
                if (entities.EntityCLID == each.EntityCLID)
                {
                    includeentitieslist.Remove(each);
                    break;
                }
            }
        }

        private void ProcessIncluded(IncludeEntities item, DataTable table)
        {
            List<Guid> guids = new List<Guid>();
            Guid guid = new Guid(item.EntityCLID);
            guids.Add(guid);
            foreach (DataRow each in table.Rows)
            {
                Guid clid = new Guid((string)each[GroupMembersDS.ENTITYCLID_FIELD]);
                if (guids.Contains(clid))
                {
                    each.Delete();
                    break;
                }
            }
        }

        private void ProcessAvailable(string clid, string name, DataTable table)
        {
            Guid guid = new Guid(clid);
            DataRow row = table.Rows.Find(guid);

            if (row == null)
            {
                row = table.NewRow();
                row[GroupMembersDS.ENTITYNAME_FIELD] = name;
                row[GroupMembersDS.ENTITYCLID_FIELD] = guid;
                row[GroupMembersDS.ADJUSTMENTENTITY_FIELD] = bool.FalseString;
                table.Rows.Add(row);
            }

        }

        private GroupMembersData GetDataForExtraction()
        {
            GroupMembersDS groupmembersds = this.OnGetDataGroupCMSL(this.UserName);
            GroupMembersData groupmembersdata = new GroupMembersData();

            AvailableEntitiesList availableentitieslist = new AvailableEntitiesList();
            IncludeEntitiesList includeentitieslist = new IncludeEntitiesList();
            EntitiesList entitieslist = new EntitiesList();

            foreach (DataRow _row in groupmembersds.Tables[GroupMembersDS.AVAILABLEENTITIES_TABLE].Rows)
            {
                if (this.CLID.ToString() != _row[GroupMembersDS.ENTITYCLID_FIELD].ToString())
                {
                    availableentitieslist.Add(this.ExtractRow_AvailableEntities(_row));
                    entitieslist.Add(this.ExtractRow_Entities(_row));
                }
            }
            groupmembersdata.AvailableEntitiesList = availableentitieslist;
            groupmembersdata.AvailableEntitiesOnlyList = entitieslist;
            entitieslist = new EntitiesList();
            foreach (DataRow _row in groupmembersds.Tables[GroupMembersDS.INCLUDEDENTITIES_TABLE].Rows)
            {
                includeentitieslist.Add(this.ExtractRow_IncludeEntities(_row));
                entitieslist.Add(this.ExtractRow_Entities(_row));
            }
            groupmembersdata.IncludeEntitiesList = includeentitieslist;
            groupmembersdata.IncludeEntitiesOnlyList = entitieslist;
            return groupmembersdata;
        }

        private AvailableEntities ExtractRow_AvailableEntities(DataRow row)
        {
            AvailableEntities availableentities = new AvailableEntities();
            availableentities.EntityCLID = row[GroupMembersDS.ENTITYCLID_FIELD].ToString();
            availableentities.EntityName = row[GroupMembersDS.ENTITYNAME_FIELD].ToString();
            return availableentities;
        }

        private IncludeEntities ExtractRow_IncludeEntities(DataRow row)
        {
            IncludeEntities includeentities = new IncludeEntities();
            includeentities.EntityCLID = row[GroupMembersDS.ENTITYCLID_FIELD].ToString();
            includeentities.EntityName = row[GroupMembersDS.ENTITYNAME_FIELD].ToString();
            includeentities.EntityCSID = row[GroupMembersDS.ENTITYCSID_FIELD].ToString();
            includeentities.TaxStatus = row[GroupMembersDS.TAXSTATUS_FIELD].ToString();
            includeentities.AdjustmentEntity = row[GroupMembersDS.ADJUSTMENTENTITY_FIELD].ToString();
            return includeentities;
        }

        private Entities ExtractRow_Entities(DataRow row)
        {
            Entities entities = new Entities();
            entities.EntityCLID = row[GroupMembersDS.ENTITYCLID_FIELD].ToString();
            entities.EntityName = row[GroupMembersDS.ENTITYNAME_FIELD].ToString();
            return entities;
        }

        private GroupStructureChartList GetGroupStructureChartData(Guid CLIDKey)
        {
            GroupStructureChartList groupstructurechartlist = new GroupStructureChartList();
            ILogicalModule ruLogicalModule = this.Broker.GetLogicalCM(CLIDKey);
            ICalculationModule iCM = Broker.GetCMImplementation(CLIDKey, ruLogicalModule.CurrentScenario);

            GroupStructureDS groupStructureDS = new GroupStructureDS();
            groupStructureDS.ThisGroupCLID = CLIDKey.ToString();
            groupStructureDS.ThisGroupCSID = ruLogicalModule.CurrentScenario.ToString();
            iCM.GetData(groupStructureDS);
            foreach (DataRow row in groupStructureDS.Tables[GroupStructureDS.GROUPSTRUCTURE_TABLE].Rows)
            {
                groupstructurechartlist.Add(ExtractRow_GroupStructureChart(row)); 
            }
            return groupstructurechartlist;
        }

        private GroupStructureChart ExtractRow_GroupStructureChart(DataRow row)
        {
            GroupStructureChart groupstructurechart = new GroupStructureChart();
            groupstructurechart.ID_FIELD = row[GroupStructureDS.ID_FIELD].ToString();
            groupstructurechart.PARENTID_FIELD = row[GroupStructureDS.PARENTID_FIELD].ToString();
            groupstructurechart.LEVEL_FIELD = row[GroupStructureDS.LEVEL_FIELD].ToString();
            groupstructurechart.DISPLAY_ORDER_FIELD = row[GroupStructureDS.DISPLAY_ORDER_FIELD].ToString();
            groupstructurechart.ENTITY_NAME_FIELD = row[GroupStructureDS.ENTITY_NAME_FIELD].ToString();
            groupstructurechart.ENTITY_URL_FIELD = row[GroupStructureDS.ENTITY_URL_FIELD].ToString();
            groupstructurechart.COLUMN_TITLE_FIELD = row[GroupStructureDS.COLUMN_TITLE_FIELD].ToString();
            groupstructurechart.COLUMN_VALUE_FIELD = row[GroupStructureDS.COLUMN_VALUE_FIELD].ToString();
            groupstructurechart.COLUMN_ORDER_FIELD = row[GroupStructureDS.COLUMN_ORDER_FIELD].ToString();
            groupstructurechart.ISGROUP_FIELD = row[GroupStructureDS.ISGROUP_FIELD].ToString();
            groupstructurechart.CSID_FIELD = row[GroupStructureDS.CSID_FIELD].ToString();
            groupstructurechart.CLID_FIELD = row[GroupStructureDS.CLID_FIELD].ToString();
            groupstructurechart.DUPLICATE = row[GroupStructureDS.DUPLICATE].ToString();
            return groupstructurechart;
        }

        #endregion

    }
}