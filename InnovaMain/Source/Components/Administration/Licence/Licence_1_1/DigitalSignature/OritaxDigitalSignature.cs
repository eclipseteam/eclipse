using System;
using System.Security.Cryptography;
using System.Text;
using System.Data;
using System.Diagnostics;

namespace LicenseManager.DigitalSignature
{
	/// <summary>
	/// Summary description for OritaxDigitalSign.
	/// </summary>
	public class OritaxDigitalSignature 
	{

		private RSAParameters rsaPubParams= new RSAParameters();		//stores public key
		private RSAParameters rsaPrivateParams = new RSAParameters();	//stores private key
		public byte[] toEncrypt;
		public byte[] encrypted;
		public byte[] signature;
		public UTF8Encoding EncodingType =new UTF8Encoding();

		public OritaxDigitalSignature()
		{
			RSACryptoServiceProvider rsaCSP = new RSACryptoServiceProvider();

			rsaPrivateParams = rsaCSP.ExportParameters(true);
			rsaPubParams = rsaCSP.ExportParameters(false);
		}

		public virtual RSAParameters PublicParameters
		{
			get
			{
				return rsaPubParams;
			}
		}

		public virtual byte[] SignAndHash(byte [] EncryptedXML)
		{

			RSACryptoServiceProvider rsaCSP  = new RSACryptoServiceProvider();
			SHA1Managed  Hash = new SHA1Managed();
			
			byte[] HashedData;

			// Import the private key parameters
			rsaCSP.ImportParameters(rsaPrivateParams);

			// Compute the Hash on the encrytpedXML
			HashedData = Hash.ComputeHash(EncryptedXML);
			
			// return signed
			return rsaCSP.SignHash(HashedData,CryptoConfig.MapNameToOID(this.LicenseText));
		}

		//Encrypts using only the public key data.
		public virtual byte[] EncryptData(RSAParameters rsaParams, byte[] ToEncrypt)
		{
			//create new instance of RSACryptoServiceProvider
	        
			RSACryptoServiceProvider rsaCSP = new RSACryptoServiceProvider();
			//private key params into instance of RSACryptoServiceProvider
			rsaCSP.ImportParameters(rsaParams);
			//true to use OAEP padding PKCS#1 v2  (only available on Windows XP or later)
			// otherwise, false to use Direct Encryption using PKCS#1 v1.5 padding
			return rsaCSP.Encrypt(ToEncrypt, false);
		}

		protected virtual string LicenseText
		{	
			// tnis used for the Private Key
			get{return "SHA1";}
			//get{return "ddSHA1";}
		}
	}
}
