using System;
using System.Security.Cryptography;
using System.Text;
using System.Data;
using System.Diagnostics;

namespace LicenseManager.DigitalSignature
{
	/// <summary>
	/// Summary description for ClientDigitalSignature.
	/// </summary>
	public class ClientDigitalSignature
	{
		private RSAParameters rsaPubParams= new RSAParameters();		//stores public key
		private RSAParameters rsaPrivateParams = new RSAParameters();	//stores private key
		public byte[] toEncrypt;
		public byte[] encrypted;
		public byte[] signature;
		public UTF8Encoding EncodingType =new UTF8Encoding();


		public ClientDigitalSignature()
		{
			//RSAParameters rsaPubParams = new RSAParameters();
			//RSAParameters rsaPrivateParams = new RSAParameters();
			RSACryptoServiceProvider rsaCSP = new RSACryptoServiceProvider();

			rsaPrivateParams = rsaCSP.ExportParameters(true);
			rsaPubParams = rsaCSP.ExportParameters(false);
		}

		public virtual RSAParameters PublicParameters
		{
			get
			{
				return rsaPubParams;
			}
		}

		public virtual bool VerifyHash( RSAParameters rsaParams, byte[] SignedData,byte[] signature)
		{
			//create new instance of RSACryptoServiceProvider
			RSACryptoServiceProvider rsaCSP = new RSACryptoServiceProvider();

			//create new instance of SHA1 hash algorithm to compute hash
			SHA1Managed hash = new SHA1Managed();
       
			//a byte array to store hash value
			byte[] hashedData;

			//import  public key params into instance of RSACryptoServiceProvider
			rsaCSP.ImportParameters(rsaParams);

			//compute hash with algorithm specified as here we have SHA1
			hashedData = hash.ComputeHash(SignedData);
        
			// Sign Data using public key and  OID is simple name of the algorithm for which to get the object identifier (OID)
			return rsaCSP.VerifyHash(hashedData, CryptoConfig.MapNameToOID(this.LicenseText), signature);
		}

		public virtual string DecryptData(byte[] EncryptedXML)
		{
			byte[] FromEncrypt ;
			string RoundTrip = string.Empty;

			RSACryptoServiceProvider rsaCSP = new RSACryptoServiceProvider();
			rsaCSP.ImportParameters(this.rsaPrivateParams);

			FromEncrypt = rsaCSP.Decrypt(EncryptedXML,false);
  
			return EncodingType.GetString(FromEncrypt);

		}



		protected virtual string LicenseText
		{	
			// tnis used for the Private Key
			get{return "SHA1";}
			//get{return "ddSHA1";}
		}
	}
}
