using System;
using System.Security.Cryptography;
using System.Text;
using System.Data;
using System.Diagnostics;

namespace LicenseManager.DigitalSignature
{

	public class DigitalSignatureHash
	{
		private RSAParameters rsaPubParams= new RSAParameters();			//stores public key
		private RSAParameters rsaPrivateParams = new RSAParameters();	//stores private key
		public byte[] toEncrypt;
		public byte[] encrypted;
		public byte[] signature;
		public UTF8Encoding EncodingType =new UTF8Encoding();

		public DigitalSignatureHash()
		{
			RSACryptoServiceProvider rsaCSP = new RSACryptoServiceProvider();

			rsaPrivateParams = rsaCSP.ExportParameters(true);
			rsaPubParams = rsaCSP.ExportParameters(false);

		}

		public virtual RSAParameters PublicParameters
		{
			get
			{
				return rsaPubParams;
			}
		}

		public virtual byte[] SignAndHash(byte [] EncryptedXML)
		{

			RSACryptoServiceProvider rsaCSP  = new RSACryptoServiceProvider();
			SHA1Managed  Hash = new SHA1Managed();
			
			// Import the private key parameters
			rsaCSP.ImportParameters(rsaPrivateParams);

			// Compute the Hash on the encrytpedXML
			byte[] HashedData = Hash.ComputeHash(EncryptedXML);
			
			// return signed
			return rsaCSP.SignHash(HashedData,CryptoConfig.MapNameToOID(this.LicenseText));
		}

		//Encrypts using only the public key data.
		public virtual byte[] EncryptData(RSAParameters rsaParams, byte[] ToEncrypt)
		{
			//create new instance of RSACryptoServiceProvider
	        
			RSACryptoServiceProvider rsaCSP = new RSACryptoServiceProvider();
			//private key params into instance of RSACryptoServiceProvider
			rsaCSP.ImportParameters(rsaParams);
			//true to use OAEP padding PKCS#1 v2  (only available on Windows XP or later)
			// otherwise, false to use Direct Encryption using PKCS#1 v1.5 padding
			return rsaCSP.Encrypt(ToEncrypt, false);
		}
		
		public virtual string DecryptData(byte[] EncryptedXML)
		{
			string RoundTrip = string.Empty;

			RSACryptoServiceProvider rsaCSP = new RSACryptoServiceProvider();
			rsaCSP.ImportParameters(this.rsaPrivateParams);

			byte[] FromEncrypt = rsaCSP.Decrypt(EncryptedXML,false);
  
			return EncodingType.GetString(FromEncrypt);

		}

		public virtual bool VerifyHash( RSAParameters rsaParams, byte[] SignedData, byte[] signature)
		{
			//create new instance of RSACryptoServiceProvider
			RSACryptoServiceProvider rsaCSP = new RSACryptoServiceProvider();

			//create new instance of SHA1 hash algorithm to compute hash
			SHA1Managed hash = new SHA1Managed();
       
			//import  public key params into instance of RSACryptoServiceProvider
			rsaCSP.ImportParameters(rsaParams);

			//compute hash with algorithm specified as here we have SHA1
			byte[] hashedData = hash.ComputeHash(SignedData);
		
        
			// Sign Data using public key and  OID is simple name of the algorithm for which to get the object identifier (OID)
    		return rsaCSP.VerifyHash(hashedData, CryptoConfig.MapNameToOID(this.LicenseText), signature);
		}

		protected virtual string LicenseText
		{	
			// this used for the Private Key
			get{return "SHA1";}
		}

	
	}
}
