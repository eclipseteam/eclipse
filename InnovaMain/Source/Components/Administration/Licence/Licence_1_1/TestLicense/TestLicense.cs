using System;

namespace Oritax.TaxSimp.TaxSimpLicence
{
	/// <summary>
	/// Only for testing Purpose.......
	/// </summary>
	internal class TestLicense
	{
		#region Constants --------------------------------------------------------------
		private const string GLOBAL_INFO_COMPANYNAME		= "My ABC Test Company PTY Limited";
		private const string GLOBAL_INFO_COMPANYADDRESS		= "Level 9 190 Geroge Street Sydney CBD 2000";
		private const string GLOBAL_INFO_COMPANYABN			= "123-456-789-99";
		private const string GLOBAL_INFO_COMPANYACN			= "123-456-789";
		private const string GLOBAL_INFO_TIMELENGTH			= "20";
		private const string GLOBAL_INFO_LICENSEDATE		= "01/01/2005";
		private const string GLOBAL_INFO_LICENSEEXPIRYDATE	= "30/06/2006";
		private const string GLOBAL_INFO_ORGANISATIONMAX	= "1";


		#endregion

		#region Member Variables -------------------------------------------------------
		private LicenceDS licenceDS = new LicenceDS();

		#endregion

		#region Main Constructor -------------------------------------------------------
		public TestLicense()
		{
			this.CreateTestLicence();
		}
		public TestLicense(out LicenceDS Licence)
		{
			this.CreateTestLicence();
			Licence=this.GetTestLicense; 
		}

		public LicenceDS GetTestLicense
		{
			get
			{
				return this.licenceDS;
			}
		}

		#endregion

		#region Create Test License File -----------------------------------------------

		private void CreateTestLicence()
		{
			// the Global Information - Nable being Licensed to
			this.licenceDS.AddGlobalInfoRow(GLOBAL_INFO_COMPANYNAME,
											GLOBAL_INFO_COMPANYADDRESS,
											GLOBAL_INFO_COMPANYABN,
											GLOBAL_INFO_COMPANYACN,
											GLOBAL_INFO_TIMELENGTH,
											GLOBAL_INFO_LICENSEDATE,
											GLOBAL_INFO_LICENSEEXPIRYDATE,
											GLOBAL_INFO_ORGANISATIONMAX,
											GLOBAL_INFO_LICENSEEXPIRYDATE,
											LicenceDS.LicenceType.Registered.ToString(),
											"NABLE_2005_1");

			this.AddEntityGroup();
			this.AddGroupStructureGroup();
			this.AddLedgerGroup();
			this.AddComponentsList();

			
            
		}
		#endregion

		
		#region Add Component Groups to the license file -------------------------------

		private void AddLedgerGroup()
		{
			// Setup Contants
			const string GroupName		= "Ledger";
			const string GroupInstance	= "10";
			
			this.licenceDS.AddComponentGroupRow(GroupName,GroupInstance);  


			// Now Add all the Components list for this Group
			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("F9402117-5B8C-471f-B4B6-13DE7406BC91"),
				"Accounting Records",
				"2",
				"1.1.0.1");

			// Now Add all the Components list for this Group
			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("E15E5928-DC89-40f3-A047-73232A1608EE"),
				"Ledger Map",
				"2",
				"1.1.0.1");

			// Now Add all the Components list for this Group
			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("E7A63C80-7FA8-4875-82E4-3643DF9AE6B3"),
				"Map Target",
				"1",
				"1.1.0.1");

			// Now Add all the Components list for this Group
			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("17141A0D-42E2-4346-A9F7-16BCF76C193A"),
				"Chart Of Accounts",
				"2",
				"1.1.0.1");

			// Now Add all the Components list for this Group
			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("1D5D34D91-5BD5-4ca5-8353-9478299E4188"),
				"Consolidation adjustment entity",
				"1",
				"1.1.0.1");

			// Now Add all the Components list for this Group
			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("9E6A9C5F-A9B4-4d0e-AF98-7D0029B62421"),
				"File Import",
				"3",
				"1.1.0.1");
		}

		private void AddGroupStructureGroup()
		{
			// Setup Contants
			const string GroupName		= "Group";
			const string GroupInstance	= "2";

			// Add the Entity Group
			this.licenceDS.AddComponentGroupRow(GroupName,GroupInstance);  

			// Now Add all the Components list for this Group
			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("A69D1EA1-10B6-498a-A9A7-BB23CEB4C7CE"),
				"Australian Sub-group",
				"1",
				"1.1.0.1");

			// Now Add all the Components list for this Group
			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("AE9FE16C-F731-4d04-8CF2-F6A18C362EDE"),
				"Australian Ultimate tax group",
				"1",
				"1.1.0.1");
		}

		private void AddEntityGroup()
		{
			// Setup Contants
			const string GroupName		= "Entity";
			const string GroupInstance	= "10";

			// Add the Entity Group
			this.licenceDS.AddComponentGroupRow(GroupName,GroupInstance);  

			// Now Add all the Components list for this Group
			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("4C0456B3-C6B8-4560-9D17-B33E65F7F528"),
				"AAustralian Company",
				"5",
				"1.1.0.1");

			// Australian Entity
			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("0AA37352-3610-4ebb-9C15-856F4E2B7E2A"),
				"Australian entity",
				"4",
				"1.1.0.1");

			// Australian Division of an Entity
			this.licenceDS.AddGroupedComponentInfoRow(GroupName,
				new Guid("44A99034-79F6-453a-89F6-1E47BBAFB24F"),
				"Australian Division of an entity",
				"1",
				"1.1.0.1");
		}
		#endregion

		#region Components List to the License File ------------------------------------

		private void AddComponentsList()
		{
			this.licenceDS.AddComponentListRow(
				new Guid("5B144E3D-EE2F-4804-A01B-D219A7F227E5"),
				"ATO Forms",
				"1",
				"1.1.0.0"
				);

			this.licenceDS.AddComponentListRow(
				new Guid("2C81CFA2-16BD-4879-B2EA-412E1296A202"),
				"Australian Exempt Income",
				"1",
				"1.1.0.0"
				);

			this.licenceDS.AddComponentListRow(
				new Guid("04F630BF-F59C-45e4-8856-30D2913732D4"),
				"Australian Legals",
				"1",
				"1.1.0.0"
				);

			this.licenceDS.AddComponentListRow(
				new Guid("EB5E3368-90BE-4483-A22A-872F91CE8D1E"),
				"Bad debts",
				"1",
				"1.1.0.0"
				);

			this.licenceDS.AddComponentListRow(
				new Guid("FA541B84-3896-4504-84B1-C36A290E7404"),
				"Australian Entertainment",
				"1",
				"1.1.0.0"
				);

		}


		#endregion
	}
}
