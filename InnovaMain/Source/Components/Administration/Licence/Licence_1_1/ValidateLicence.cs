using System;
using System.Data;
using System.Diagnostics;
using System.Collections;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Text;
using System.Web;

using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.DSCalculation;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.BusinessStructureUtilities;
using Oritax.TaxSimp.CM.Group;
using Oritax.TaxSimp.CM.Entity;
using Oritax.TaxSimp.Exceptions;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CM.Period;

namespace Oritax.TaxSimp.TaxSimpLicence
{
	internal class ValidateLicence
	{
		private ValidateLicence(){}

		public static void ValidateImportedLicence(LicenceDS importedLicence, Guid InstallationID)
		{
			ArrayList arrVmessages = new ArrayList();

			// dectect if a demo licence file for extention or a fully registered Key.
			checkInstallationID(importedLicence,InstallationID,arrVmessages);
			checkEntities(importedLicence,arrVmessages);
			checkGroups(importedLicence,arrVmessages);
			checkPeriods(importedLicence,arrVmessages);
			checkUsers(importedLicence,arrVmessages); 

			if (arrVmessages.Count>0)
			{
				MessageBoxDefinition[]  messages = (MessageBoxDefinition[]) arrVmessages.ToArray(typeof(MessageBoxDefinition)); 

				throw new DisplayUserMessageException(messages, false,MessageDisplayMethod.MainWindowPopup);
			}
		}

		#region Private Methods --------------------------------------------------------

		private static void checkUsers(LicenceDS importedLicence, ArrayList Messages)
		{
			
		}

		private static void checkGroups(LicenceDS importedLicence, ArrayList Messages)
		{
			//ICalculationModule objOrganisation = this.Owner.GetWellKnownCM( WellKnownCM.Organization );



		}

		private static void checkEntities(LicenceDS importedLicence, ArrayList Messages)
		{

		} 

		private static void checkPeriods(LicenceDS importedLicence, ArrayList Messages)
		{
			//IPeriod 


		}

		private static void checkInstallationID(LicenceDS importedLicence,Guid InstallationID, ArrayList Messages)
		{
			DataRowCollection drCol = importedLicence.Tables[LicenceDS.GLOBAL_TABLE].Rows;

			if(drCol.Count>0)
			{
				DataRow drow = drCol[0];

				Guid importedLicenceGUID = new Guid(drow[LicenceDS.GLOBAL_LICENCE_GUID_FLD].ToString());
				
				if(!importedLicenceGUID.Equals(Guid.Empty))
				{
					if(!importedLicenceGUID.Equals(InstallationID))
					{
						MessageBoxDefinition mbd = new MessageBoxDefinition(MessageBoxDefinition.MSG_ERR_LK_INSTALLID_INVALID);
						Messages.Add(mbd); 

					}
				}
			}
		}
		
		#endregion
	}
}
