using System;
using System.IO;
using System.Xml;
using System.Diagnostics;
using System.Data;
using System.Collections;


namespace LicenseManager
{
	public class NableLicense
	{
		#region Variables --------------------------------------------------------------
		private string LicenseFile;
		private GlobalInfoDS GlobalInfo;
		
		#endregion

		#region Main Constructor -------------------------------------------------------
		public NableLicense()
		{
			this.Init();
		}

		private void Init()
		{
			// Initialse member variables
			LicenseFile = string.Empty;
			GlobalInfo = new GlobalInfoDS();

		}

		#endregion

		#region Public Properties ------------------------------------------------------
		public GlobalInfoDS GlobalInformation
		{
			get
			{
				return GlobalInfo;
			}
			set
			{
				GlobalInfo = value;
			}
		}

		#endregion

		#region Read and Write License File --------------------------------------------

		public void ReadLicenseFile(string Licensefile)
		{
			LicenseFile = Licensefile; 
			
			this.LoadLicenseStructure();

		}

		public void WriteLicenseFile(string LicenseFile)
		{

		}


		#endregion

		#region Private Methods --------------------------------------------------------
		private void DecryptFileContent()
		{

		}

		private void EncryptFileContent()
		{

		}


		private void LoadLicenseStructure()
		{
			XmlTextReader reader = new XmlTextReader(@LicenseFile);
			
			reader.WhitespaceHandling = WhitespaceHandling.None;
			XmlDocument xmlDoc = new XmlDocument();
			
			xmlDoc.Load(reader);
			
			reader.Close();
			
			//Find the root nede, and add it togather with its childeren
			XmlNode xnod = xmlDoc.DocumentElement;
			AddWithChildren(xnod,1);
		}

		private void AddWithChildren(XmlNode xnod, Int32 intLevel)
		{
			//intLevel controls the depth of indenting
			XmlNode xnodWorking;
			String strIndent = new string(' ',2 * intLevel);
			//Get the value of the node (if any)
			string strValue = (string) xnod.Value;
			if(strValue != null)
			{
				strValue = " : " + strValue;
			}
		
			Debug.WriteLine(strIndent + xnod.Name + strValue);

			//For an element node, retrive the attributes
			if (xnod.NodeType == XmlNodeType.Element)
			{
				XmlNamedNodeMap mapAttributes = xnod.Attributes;
				
				foreach(XmlNode xnodAttribute in mapAttributes)
				{
					Debug.WriteLine(strIndent + " " + xnodAttribute.Name + " : " + xnodAttribute.Value);
				}

				//If there are any child node, call this procedrue recursively
				if(xnod.HasChildNodes)
				{
					xnodWorking = xnod.FirstChild;
					while (xnodWorking != null)
					{
						AddWithChildren(xnodWorking, intLevel +1);
						xnodWorking = xnodWorking.NextSibling;
					}
				}
			}
		}

		#endregion

		private void CreateLicenseDocument(string LicenseFile)
		{
			// Create Oritax Setion
			this.CreateOritaxSection();
		}

		private string CreateOritaxSection()
		{
			string OritaxStructure = string.Empty;





			return OritaxStructure;
		}


	}
}
