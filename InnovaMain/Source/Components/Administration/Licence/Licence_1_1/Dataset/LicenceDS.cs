using System;
using System.Collections;
using System.IO;
using System.Data;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Data.SqlClient;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.TaxSimpLicence
{
	[Serializable]
	public class LicenceDS : DataSet
	{
		public enum LicenceType
		{
			Demo,
			Registered
		}

		#region Constants --------------------------------------------------------------

		#region License Component Constants --------------------------------------------
			
		// Grouped component list table. 
		public const string LICENSECOMPONENT_GROUPEDTABLE			= "GroupedComponent";
		// Component List Table
		public const string LICENSECOMPONENT_LISTTABLE				= "ComponentList";

		public const string LICENSECOMPONENT_COMPONENTID_FLD		= "ComponentID";
		public const string LICENSECOMPONENT_DISCRIPTION_FLD		= "Description";
		public const string LICENSECOMPONENT_ALLOWEDINSTANCES_FLD	= "AllowedInstances";
		public const string LICENSECOMPONENT_COMPONENTVERSION_FLD	= "ComponentVersion";
		public const string LICENSECOMPONENT_ASSEMBLYID_FLD			= "AssemblyID";

		// this field is only used on the grouped component table.
		public const string LICENSECOMPONENT_GROUPNAME_FLD			= "GroupName";
		#endregion
		
		#region Global Information Constants--------------------------------------------
			
		public const string GLOBAL_TABLE				= "GLOBAL";
		public const string GLOBAL_NAME_FLD				= "CompanyName";
		public const string GLOBAL_ADDRESS_FLD			= "CompanyAddress";
		public const string GLOBAL_ABN_FLD				= "CompanyABN";
		public const string GLOBAL_ACN_FLD				= "CompanyACN";
		public const string GLOBAL_TIMELENGTH_FLD		= "LicenseTimeLength";
		public const string GLOBAL_LICENSEDATE_FLD		= "LicenseDate";
		public const string GLOBAL_EXPIRYDATE_FLD		= "LicenseExpiryDate";
		public const string GLOBAL_MAXORGANISATION_FLD  = "OrganisationMaxNo";
		public const string GLOBAL_PERIOD_ENDDATE_FLD	= "PeriodEndDate";
		public const string GLOBAL_LICENCETYPE_FLD		= "LicenceType";
		public const string GLOBAL_NABLE_VERSION_FLD	= "NableVersion";
		public const string GLOBAL_LICENCE_GUID_FLD     = "LicenceGUID";
		public const string GLOBAL_LICENCE_IMPORT_LEDGER_FLD= "ImportLedger";
		public const string GLOBAL_LICENCE_SCENARIOS_FLD	= "Scenarios";
		#endregion

		#region Grouped Components Constants -------------------------------------------
			
		// Component group table.
		public const string CG_TABLE				= "ComponentGroup";
		public const string CG_NAME_FLD				= "GroupName";
		public const string CG_ALLOWEDINSTANCES_FLD	= "AllowedInstances";
		#endregion

		private Guid _licenceGUID;
		#endregion

		#region Constructor ------------------------------------------------------------
		public LicenceDS()
		{
			this.CreateTablesAndRelations();
		}

		protected LicenceDS(SerializationInfo si, StreamingContext context)
			: base(si,context)
		{
		}

		private void CreateTablesAndRelations()
		{
			#region Build Table --------------------------------------------------------
			this.BuildGlobalInformationTable();
			this.BuildComponentInfoTable(LICENSECOMPONENT_LISTTABLE,false);
			this.BuildComponentInfoTable(LICENSECOMPONENT_GROUPEDTABLE,true);
			this.BuidGroupedComponentTable();
			#endregion

            //#region Build Relations ----------------------------------------------------
            //// 1 component Group can have many Licensed Components
            //this.Relations.Add(this.Tables[CG_TABLE].Columns[CG_NAME_FLD], this.Tables[LICENSECOMPONENT_GROUPEDTABLE].Columns[LICENSECOMPONENT_GROUPNAME_FLD]); 
            //this.EnforceConstraints  = true;	
            //#endregion
 
		}
		#endregion

		#region Private methods --------------------------------------------------------

		private void BuildComponentInfoTable (string TableName, bool IsGroupedComponent)
		{
			DataTable dt=new DataTable(TableName);

			dt.Columns.Add(LICENSECOMPONENT_COMPONENTID_FLD, typeof(Guid));
			dt.Columns.Add(LICENSECOMPONENT_DISCRIPTION_FLD, typeof(string));
			dt.Columns.Add(LICENSECOMPONENT_ALLOWEDINSTANCES_FLD, typeof(string));
			dt.Columns.Add(LICENSECOMPONENT_COMPONENTVERSION_FLD, typeof(string));
			if(IsGroupedComponent)
			{
				dt.Columns.Add(LICENSECOMPONENT_GROUPNAME_FLD, typeof(string));
			}

			this.Tables.Add(dt);

		}
		
		private void BuildGlobalInformationTable()
		{
			DataTable dt=new DataTable(GLOBAL_TABLE);

			dt.Columns.Add(GLOBAL_NAME_FLD, typeof(string));
			dt.Columns.Add(GLOBAL_ADDRESS_FLD, typeof(string));
			dt.Columns.Add(GLOBAL_ABN_FLD, typeof(string));
			dt.Columns.Add(GLOBAL_ACN_FLD, typeof(string));
			dt.Columns.Add(GLOBAL_TIMELENGTH_FLD, typeof(string));
			dt.Columns.Add(GLOBAL_LICENSEDATE_FLD, typeof(string));
			dt.Columns.Add(GLOBAL_EXPIRYDATE_FLD, typeof(string));
			dt.Columns.Add(GLOBAL_MAXORGANISATION_FLD, typeof(string));
			dt.Columns.Add(GLOBAL_PERIOD_ENDDATE_FLD , typeof(string));
			dt.Columns.Add(GLOBAL_LICENCETYPE_FLD, typeof(string));
			dt.Columns.Add(GLOBAL_NABLE_VERSION_FLD, typeof(string));
			dt.Columns.Add(GLOBAL_LICENCE_GUID_FLD, typeof(Guid));
			dt.Columns.Add(GLOBAL_LICENCE_IMPORT_LEDGER_FLD, typeof(bool));
			dt.Columns.Add(GLOBAL_LICENCE_SCENARIOS_FLD, typeof(string));

			this.Tables.Add(dt);
		}

		private void BuidGroupedComponentTable()
		{
			DataTable dt=new DataTable(CG_TABLE);

			dt.Columns.Add(CG_ALLOWEDINSTANCES_FLD , typeof(string));
			dt.PrimaryKey=new DataColumn [] {dt.Columns.Add(CG_NAME_FLD , typeof(string))};

			this.Tables.Add(dt);
			
		}
		#endregion

		#region Public Methods ---------------------------------------------------------
		public void AddGlobalInfoRow(string CompanyName, string Address, string ABN, string ACN, string TimeLength, string LicenseDate, string ExpiryDate, string MaxOrg, string PeriodEndDate, string LicenceType, string NableVersion, string Scenarios, bool ImportLedger)
		{
			// the Row
			DataRow newRow = this.Tables[GLOBAL_TABLE].NewRow();

			newRow[GLOBAL_NAME_FLD]				= CompanyName;
			newRow[GLOBAL_ADDRESS_FLD]			= Address;
			newRow[GLOBAL_ABN_FLD]				= ABN;
			newRow[GLOBAL_ACN_FLD]				= ACN;
			newRow[GLOBAL_TIMELENGTH_FLD]		= TimeLength;
			newRow[GLOBAL_LICENSEDATE_FLD]		= LicenseDate;
			DateTime expire = DateTime.Parse(ExpiryDate);
			newRow[GLOBAL_EXPIRYDATE_FLD]		= expire.ToShortDateString();
			newRow[GLOBAL_MAXORGANISATION_FLD]	= MaxOrg;
			newRow[GLOBAL_PERIOD_ENDDATE_FLD]	= PeriodEndDate; 
			newRow[GLOBAL_LICENCETYPE_FLD]		= LicenceType; 
			newRow[GLOBAL_NABLE_VERSION_FLD]	= NableVersion;
			newRow[GLOBAL_LICENCE_GUID_FLD]		= this.LicenceGUID;
			newRow[GLOBAL_LICENCE_SCENARIOS_FLD]	= Scenarios;
			newRow[GLOBAL_LICENCE_IMPORT_LEDGER_FLD]= ImportLedger;
			

			this.Tables[GLOBAL_TABLE].Rows.Add(newRow);

		}
		
		public void AddGroupedComponentInfoRow(string GroupName, Guid ComponentID, string Description, string AllowedInstances, string ComponentVersion)
		{
			// the Row for the Grouped Components
			DataRow newRow = this.Tables[LICENSECOMPONENT_GROUPEDTABLE].NewRow();

			newRow[LICENSECOMPONENT_COMPONENTID_FLD]			= ComponentID;
			newRow[LICENSECOMPONENT_DISCRIPTION_FLD]			= Description;
			newRow[LICENSECOMPONENT_ALLOWEDINSTANCES_FLD]		= AllowedInstances;
			newRow[LICENSECOMPONENT_COMPONENTVERSION_FLD]		= ComponentVersion;
			newRow[LICENSECOMPONENT_GROUPNAME_FLD]				= GroupName;
 
			this.Tables[LICENSECOMPONENT_GROUPEDTABLE].Rows.Add(newRow);
		}

		public void AddComponentListRow( Guid ComponentID, string Description, string AllowedInstances, string ComponentVersion)
		{
			// the Row for the Components List
			DataRow newRow = this.Tables[LICENSECOMPONENT_LISTTABLE].NewRow();

			newRow[LICENSECOMPONENT_COMPONENTID_FLD]			= ComponentID;
			newRow[LICENSECOMPONENT_DISCRIPTION_FLD]			= Description;
			newRow[LICENSECOMPONENT_ALLOWEDINSTANCES_FLD]		= AllowedInstances;
			newRow[LICENSECOMPONENT_COMPONENTVERSION_FLD]		= ComponentVersion;

			this.Tables[LICENSECOMPONENT_LISTTABLE].Rows.Add(newRow);
		}

		
		public void AddComponentGroupRow(string Name, string AllowedInstances)
		{
			// Add Component Group Row
			DataRow newRow = this.Tables[CG_TABLE].NewRow();

			newRow[CG_ALLOWEDINSTANCES_FLD]			= AllowedInstances;
			newRow[CG_NAME_FLD]						= Name;

			this.Tables[CG_TABLE].Rows.Add(newRow);
		}

		public Guid LicenceGUID
		{
			get
			{ 
				return _licenceGUID;
			}
			set
			{	
				_licenceGUID = value;
			}
		}

		#endregion

		#region Public properties ------------------------------------------------------

		public int GlobalInfoCount
		{
			get
			{
				return this.Tables[GLOBAL_TABLE].Rows.Count;
			}
		}

		
		public DataRow[] GetGroupedComponentsList(string GroupName)
		{
			DataRow[] FetchedRows = this.Tables[LICENSECOMPONENT_GROUPEDTABLE].Select(LICENSECOMPONENT_GROUPNAME_FLD+"='"+GroupName+"'");
            
			if(FetchedRows.Length>0)
				return FetchedRows;
			else
				return null;
		}
		
		public int GroupComponentListCount(string GroupName)
		{
			DataRow[] FetchedRows = this.Tables[LICENSECOMPONENT_GROUPEDTABLE].Select(LICENSECOMPONENT_GROUPNAME_FLD+"='"+GroupName+"'");
          
			return FetchedRows.Length;
		}
	
		public int DaysRemainingOnLicence()
		{
			TimeSpan numberOfDays = new TimeSpan(0);
			// Search Component Groups for ComponentID
			DataRowCollection FetchedRows = this.Tables[GLOBAL_TABLE].Rows;

			if(FetchedRows.Count>0)
			{
				DataRow drow = this.Tables[GLOBAL_TABLE].Rows[0];

				DateTime licenceDateExpiry = DateTime.Parse(drow[GLOBAL_EXPIRYDATE_FLD].ToString());
				licenceDateExpiry.Add(new TimeSpan(23,59,59));
				numberOfDays = licenceDateExpiry - DateTime.Now;
			}
			
			return numberOfDays.Days;
		}

		#endregion
	}
}
