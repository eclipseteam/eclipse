using System;
using System.Data;
using System.IO;
using Oritax.TaxSimp.WPDatasetBase;

namespace  Oritax.TaxSimp.TaxSimpLicence
{

	public class LicenceImportDS :WPDatasetBaseDS
	{
		#region Constants --------------------------------------------------------------
		public const string STREAM_TABLE = "STREAMTABLE";
		public const string STREAM_STREAM_FIELD = "STREAM";
		#endregion

		#region Main constructor -------------------------------------------------------
		public LicenceImportDS()
		{
			BuildTable();
		}

		#endregion

		#region Public properties ------------------------------------------------------
		public Stream InputStream 
		{
			get { return this.Tables[STREAM_TABLE].Rows[0][STREAM_STREAM_FIELD] as Stream; }
			set { this.Tables[STREAM_TABLE].Rows[0][STREAM_STREAM_FIELD] = value; }
		}

		public bool HasStream { get { return Tables[STREAM_TABLE].Rows[0][STREAM_STREAM_FIELD] != DBNull.Value; } }

		#endregion

		#region Protected Methods ------------------------------------------------------
		protected void AddInfoColumn(string name, System.Type type)
		{
			this.Tables[STREAM_TABLE].Columns.Add(name, type);
		}

		protected object GetInfo(string name)
		{
			return this.Tables[STREAM_TABLE].Rows[0][name];
		}

		protected void SetInfo(string name, object val)
		{
			this.Tables[STREAM_TABLE].Rows[0][name]=val;
		}

		#endregion

		#region Private methods --------------------------------------------------------
		private void BuildTable()
		{

			// create a table to hold the stream
			DataTable streamTable = new DataTable(STREAM_TABLE);
			streamTable.Columns.Add(STREAM_STREAM_FIELD, typeof(System.IO.Stream));

			// add a default row
			streamTable.Rows.Add(streamTable.NewRow());
			streamTable.Rows[0][STREAM_STREAM_FIELD]=DBNull.Value;

			this.Tables.Add(streamTable);
		}
		#endregion

	}
}
