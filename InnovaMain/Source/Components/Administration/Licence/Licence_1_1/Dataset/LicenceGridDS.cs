using System;
using System.Data;
using System.IO;

namespace  Oritax.TaxSimp.TaxSimpLicence
{

	internal class LicenceGridDS :DataSet
	{
		#region Constants --------------------------------------------------------------
		public const string LICENSECOMPONENT_GROUPEDTABLE			= "GroupedComponent";
		public const string LICENSECOMPONENT_COMPONENTID_FLD		= "ComponentID";
		public const string LICENSECOMPONENT_DISCRIPTION_FLD		= "Discription";
		public const string LICENSECOMPONENT_ALLOWEDINSTANCES_FLD	= "AllowedInstances";
		public const string LICENSECOMPONENT_COMPONENTVERSION_FLD	= "ComponentVersion";
		public const string LICENSECOMPONENT_ASSEMBLYID_FLD			= "AssemblyID";
		public const string LICENSECOMPONENT_GROUPNAME_FLD			= "GroupName";
		#endregion

		#region Main constructor -------------------------------------------------------
		public LicenceGridDS()
		{
			BuildTable();
		}

		#endregion

		#region Private methods --------------------------------------------------------
		private void BuildTable()
		{

			DataTable dt=new DataTable(LICENSECOMPONENT_GROUPEDTABLE);

			dt.Columns.Add(LICENSECOMPONENT_COMPONENTID_FLD, typeof(Guid));
			dt.Columns.Add(LICENSECOMPONENT_DISCRIPTION_FLD, typeof(string));
			dt.Columns.Add(LICENSECOMPONENT_ALLOWEDINSTANCES_FLD, typeof(string));
			dt.Columns.Add(LICENSECOMPONENT_COMPONENTVERSION_FLD, typeof(string));
			dt.Columns.Add(LICENSECOMPONENT_GROUPNAME_FLD, typeof(string));

			this.Tables.Add(dt);
		}
		#endregion

		#region Public Methods ---------------------------------------------------------
		public void AddRow(string GroupName, Guid ComponentID, string Description, string AllowedInstances, string ComponentVersion)
		{
			// the Row for the Grouped Components
			DataRow newRow = this.Tables[LICENSECOMPONENT_GROUPEDTABLE].NewRow();

			newRow[LICENSECOMPONENT_COMPONENTID_FLD]			= ComponentID;
			newRow[LICENSECOMPONENT_DISCRIPTION_FLD]			= Description;
			newRow[LICENSECOMPONENT_ALLOWEDINSTANCES_FLD]		= AllowedInstances;
			newRow[LICENSECOMPONENT_COMPONENTVERSION_FLD]		= ComponentVersion;
			newRow[LICENSECOMPONENT_GROUPNAME_FLD]				= GroupName;
 
			this.Tables[LICENSECOMPONENT_GROUPEDTABLE].Rows.Add(newRow);
		}
		#endregion

	}
}
