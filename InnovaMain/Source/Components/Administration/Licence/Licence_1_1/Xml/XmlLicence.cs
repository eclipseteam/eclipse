using System;
using System.Drawing;
using System.Collections;
using System.Diagnostics;
using System.ComponentModel;
using System.Data;
using System.Xml;
using System.IO;
using System.Web;

using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.DSCalculation;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.BusinessStructureUtilities;
using Oritax.TaxSimp.CM.Group;
using Oritax.TaxSimp.CM.Entity;
using Oritax.TaxSimp.Exceptions;
using Oritax.TaxSimp.CM.Organization;

namespace Oritax.TaxSimp.TaxSimpLicence
{
	public class XmlLicence
	{
		#region Private Members --------------------------------------------------------
		private Stream inputXMLStream;
		private string inputXMLContent = string.Empty;
		private LicenceDS licenceds;
		#endregion

		#region Constants --------------------------------------------------------------
		private const string GLOBAL = "GLOBAL";
		private const string COMPONENT_LIST = "COMPONENT_LIST";
		private const string COMPONENTGROUP = "Componentgroup";
		private const string COMPONENT	= "COMPONENT";


		#endregion

		#region Main Constructor -------------------------------------------------------
		public XmlLicence()
		{
			licenceds = new LicenceDS();
		}

		#endregion

		#region Public Methods ---------------------------------------------------------

		public LicenceDS GetLicenceDS
		{
			get
			{
				return this.licenceds;
			}
		}

		public Stream InputStream 
		{
			get { return inputXMLStream; }
			set { inputXMLStream = value; }
		}

		public string FileContent 
		{
			get { return inputXMLContent; }
			set { inputXMLContent = value; }
		}

		public void LoadLicense()
		{
			if(HasStream)
			{
				this.PopulateDataSet();
			}
		}

		public void LoadLicense(bool useFileContent)
		{
			if(HasContent)
			{
				this.PopulateDataSet(true);
			}
		}

		public  bool HasStream { get { return inputXMLStream != null; } }
		public  bool HasContent { get { return (!inputXMLContent.Equals(string.Empty)); } }

		public LicenceDS GetLicencedData
		{
			get
			{
				return licenceds ;
			}
		}

		#endregion

		#region Private Methods --------------------------------------------------------
		private void PopulateDataSet()
		{
			try
			{
				licenceds = new LicenceDS();
				XmlDocument xmldoc = new XmlDocument();
				XmlReader reader =  new XmlTextReader(inputXMLStream);
				
				xmldoc.Load(reader) ;
				PopulateLicenceFromXML(xmldoc.DocumentElement); 
			}
			catch
			{
				MessageBoxDefinition mbd = new MessageBoxDefinition(MessageBoxDefinition.MSG_ERR_LK_INVALID);
				throw new DisplayUserMessageException(mbd, false,MessageDisplayMethod.MainWindowPopup);
			}

		}

		private void PopulateDataSet(bool useContent)
		{
			try
			{
				licenceds = new LicenceDS();
				XmlDocument xmldoc = new XmlDocument();
				try
				{
					xmldoc.LoadXml(inputXMLContent); 
				}
				catch(Exception ex)
				{
					throw new Exception(ex.Message); 
				}
				PopulateLicenceFromXML(xmldoc.DocumentElement); 
			}
			catch
			{
				MessageBoxDefinition mbd = new MessageBoxDefinition(MessageBoxDefinition.MSG_ERR_LK_INVALID);
				throw new DisplayUserMessageException(mbd, false,MessageDisplayMethod.MainWindowPopup);
			}

		}


		private void PopulateLicenceFromXML(XmlNode node)
		{
			// End recursion if the node is a text type
			if(node == null || node.NodeType == XmlNodeType.Text || node.NodeType == XmlNodeType.CDATA)
				return;

			// Load the Global Section
			if(node.Name.Equals(GLOBAL))
				GlobalInformation( node );

			// Load the Component Groups and Component lists
			if(node.Name.Equals(COMPONENT_LIST))
				GroupedComponents(node); 

			// Add all the children of the current node to the treeview
			foreach(XmlNode childnode in node.ChildNodes)
			{
				PopulateLicenceFromXML(childnode);
			}			
		}

		private void AddGroupedComponent(XmlNode node)
		{
			string GroupName =string.Empty;
			string GroupInstance =  string.Empty;

			if(node.Attributes.Count >0)
			{
				XmlAttributeCollection theAttrib = node.Attributes;

				foreach(XmlAttribute attrib in theAttrib)
				{
					if(attrib.Name.Equals(LicenceDS.LICENSECOMPONENT_GROUPNAME_FLD))
						GroupName = attrib.InnerText.ToString();
					
					if(attrib.Name.Equals(LicenceDS.LICENSECOMPONENT_ALLOWEDINSTANCES_FLD))
						GroupInstance = attrib.InnerText.ToString();

				}
				// Add the Group Information
				licenceds.AddComponentGroupRow(GroupName,GroupInstance); 
			}

			if(node.ChildNodes.Count>0)
			{
				foreach(XmlNode childnode in node.ChildNodes)
				{

					if (childnode.Attributes.Count >0)
					{
						XmlAttributeCollection theAttrib = childnode.Attributes;//xn.ChildNodes[0].Attributes;

						Hashtable ht = new Hashtable();

						foreach(XmlAttribute attrib in theAttrib)
						{
							ht.Add(attrib.Name.ToString(),attrib.InnerText.ToString());
						}

						string Desription = ht[LicenceDS.LICENSECOMPONENT_DISCRIPTION_FLD].ToString();
						Guid ComponentID = new Guid(ht[LicenceDS.LICENSECOMPONENT_COMPONENTID_FLD].ToString());
						string AllowedInstances = ht[LicenceDS.LICENSECOMPONENT_ALLOWEDINSTANCES_FLD].ToString();
						string ComponentVersion = ht[LicenceDS.LICENSECOMPONENT_COMPONENTVERSION_FLD].ToString();

						licenceds.AddGroupedComponentInfoRow(
							GroupName
							,ComponentID
							,Desription
							,AllowedInstances
							,ComponentVersion);

					}
				}
			}
		}

		private void AddComponentList(XmlNode node)
		{
			if(node.Attributes.Count >0)
			{
				XmlAttributeCollection theAttrib = node.Attributes;

				Hashtable ht = new Hashtable();

				foreach(XmlAttribute attrib in theAttrib)
					ht.Add(attrib.Name.ToString(),attrib.InnerText.ToString());

				string Description = ht[LicenceDS.LICENSECOMPONENT_DISCRIPTION_FLD].ToString();
				Guid ComponentID = new Guid(ht[LicenceDS.LICENSECOMPONENT_COMPONENTID_FLD].ToString());
				string AllowedInstances = ht[LicenceDS.LICENSECOMPONENT_ALLOWEDINSTANCES_FLD].ToString();
				string ComponentVersion = ht[LicenceDS.LICENSECOMPONENT_COMPONENTVERSION_FLD].ToString();

				licenceds.AddComponentListRow(ComponentID,Description,AllowedInstances ,ComponentVersion); 
			}
		}

		private void GroupedComponents(XmlNode node)
		{
			foreach( XmlNode childnode in node.ChildNodes)
			{
				if(childnode.Name.Equals(COMPONENTGROUP))
					AddGroupedComponent(childnode);
				
				if(childnode.Name.Equals(COMPONENT))
					AddComponentList(childnode);
			}
		}

		private void GlobalInformation(XmlNode node)
		{
			Hashtable ht = new Hashtable();


			foreach( XmlNode childnode in node.ChildNodes)
			{
				ht.Add(childnode.Name.ToString(),string.Empty);

				if(childnode.ChildNodes.Count>0)
				{
					XmlNode tmpChildNode =  childnode.ChildNodes[0];
					ht[childnode.Name.ToString()] = tmpChildNode.InnerText.ToString();
				}
			}

			// Determine the licence Type
			
			LicenceDS.LicenceType licencetype;
			if (!ht[LicenceDS.GLOBAL_LICENCE_GUID_FLD].ToString().Equals(string.Empty))
			{
				licenceds.LicenceGUID = new Guid(ht[LicenceDS.GLOBAL_LICENCE_GUID_FLD].ToString());
				licencetype = LicenceDS.LicenceType.Registered;
			}
			else
			{
				licencetype = LicenceDS.LicenceType.Demo;  
			}

			licenceds.AddGlobalInfoRow(
				ht[LicenceDS.GLOBAL_NAME_FLD].ToString(),
				ht[LicenceDS.GLOBAL_ADDRESS_FLD].ToString(),
				ht[LicenceDS.GLOBAL_ABN_FLD].ToString(),
				ht[LicenceDS.GLOBAL_ACN_FLD].ToString(),
				ht[LicenceDS.GLOBAL_TIMELENGTH_FLD].ToString(),
				ht[LicenceDS.GLOBAL_LICENSEDATE_FLD].ToString(),
				ht[LicenceDS.GLOBAL_EXPIRYDATE_FLD].ToString(),
				ht[LicenceDS.GLOBAL_MAXORGANISATION_FLD].ToString(),
				ht[LicenceDS.GLOBAL_PERIOD_ENDDATE_FLD].ToString(),
				licencetype.ToString(),
				ht[LicenceDS.GLOBAL_NABLE_VERSION_FLD].ToString(),
				ht[LicenceDS.GLOBAL_LICENCE_SCENARIOS_FLD].ToString(),
				bool.Parse(ht[LicenceDS.GLOBAL_LICENCE_IMPORT_LEDGER_FLD].ToString())
				
				);
		}

		#endregion
	}

}
