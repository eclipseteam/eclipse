using System;
using System.Data;
using System.Diagnostics;
using System.Collections;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Text;
using System.Web;
using System.Security.Cryptography; 
using System.IO;

using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.DSCalculation;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.BusinessStructureUtilities;
using Oritax.TaxSimp.CM.Group;
using Oritax.TaxSimp.CM.Entity;
using Oritax.TaxSimp.Exceptions;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CM.Period;

namespace Oritax.TaxSimp.TaxSimpLicence
{
	[Serializable]
	public class LicenceSM : SystemModuleBase , ILicence, ISerializable
	{
		#region Member Variables -------------------------------------------------------
		private LicenceDS licenceds;
		private Guid _licenceGuid;
		#endregion

		#region constants --------------------------------------------------------------
		
		/* -1 = Unlimited
		 *  0 = Not Allowed
		 *  9 = Any number apart from the above, is the number of instances you are allowed to have.
		 */

		private const int DEMO_PERIOD		= 15;
		public const string STANDARDFILES	= "StandardFiles";
		public const string ENTITY			= "Entity";
		public const string GROUP			= "Group";
		public const string SCENARIOS		= "Scenarios";
		public const string USERACCOUNTS	= "UserAccounts";
		public const string MODULES			= "Modules";
		public const string TEA				= "TEA";
		public const string LEDGER			= "Ledger";
		public const string PERIODS			= "Periods";
		private const int	UNLIMITED		= -1;
		private const int	NOTALLOWED		= 0;
		private const string LICENCE		= "Licence";


		#endregion

		#region Main Contructor --------------------------------------------------------

		private Guid licenceGUID 
		{
			get
			{
				return _licenceGuid;
			}
			set
			{
				_licenceGuid = value;
			}

		}

		public LicenceSM()
		{
			InitialiseMembers();
			this.licenceGUID = Guid.NewGuid();

		}

		protected LicenceSM(SerializationInfo si, StreamingContext context) : base(si,context)
		{
			this.licenceds = (LicenceDS) Serialize.GetSerializedValue(si,"licence_licenceds",typeof(LicenceDS),null);
		}

		//Only allow the .NET Serialization core code to call this function
		[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]
		public override void GetObjectData(SerializationInfo si, StreamingContext context)
		{
			base.GetObjectData(si,context);
			Serialize.AddSerializedValue(si,"licence_licenceds",this.licenceds);
		}
		
		private void InitialiseMembers()
		{
			if(licenceds==null)
			{
				// if nothing is returned then create a DemoLicense as pass it back
				licenceds = new LicenceDS();
				NableDemoLicense DemoLicense = new NableDemoLicense(out licenceds);
			}

		}

		#endregion
		
		#region Protected methods OnGetData --------------------------------------------
		protected override void OnGetData(DataSet data)
		{
			base.OnGetData(data);
			if (data is LicenceDS)
			{
				LicenceDS ds = data as LicenceDS;
				#region Global Information
				DataRowCollection globalInfo = this.licenceds.Tables[LicenceDS.GLOBAL_TABLE].Rows;

				if(globalInfo.Count>0) 
				{
					// There should only be 1 Row.
					DataRow dRow = globalInfo[0];

					ds.LicenceGUID=this.CID;
					ds.AddGlobalInfoRow(dRow[LicenceDS.GLOBAL_NAME_FLD].ToString(),
						dRow[LicenceDS.GLOBAL_ADDRESS_FLD].ToString(),
						dRow[LicenceDS.GLOBAL_ABN_FLD].ToString(),
						dRow[LicenceDS.GLOBAL_ACN_FLD].ToString(),
						dRow[LicenceDS.GLOBAL_TIMELENGTH_FLD].ToString(),
						dRow[LicenceDS.GLOBAL_LICENSEDATE_FLD].ToString(),
						dRow[LicenceDS.GLOBAL_EXPIRYDATE_FLD].ToString(),
						dRow[LicenceDS.GLOBAL_MAXORGANISATION_FLD].ToString(),
						dRow[LicenceDS.GLOBAL_PERIOD_ENDDATE_FLD].ToString(),
						dRow[LicenceDS.GLOBAL_LICENCETYPE_FLD].ToString(),
						dRow[LicenceDS.GLOBAL_NABLE_VERSION_FLD].ToString(),
						dRow[LicenceDS.GLOBAL_LICENCE_SCENARIOS_FLD].ToString(),
						bool.Parse(dRow[LicenceDS.GLOBAL_LICENCE_IMPORT_LEDGER_FLD].ToString())
					
						);
				}

				#endregion

				#region Component Groups
			
				// Component Groups
				foreach(DataRow GroupRow in licenceds.Tables[LicenceDS.CG_TABLE].Rows)
				{
					ds.AddComponentGroupRow(GroupRow[LicenceDS.CG_NAME_FLD].ToString(),
						GroupRow[LicenceDS.CG_ALLOWEDINSTANCES_FLD].ToString());
				}

				// Component Group Component List
				foreach(DataRow GroupComponentRow in licenceds.Tables[LicenceDS.LICENSECOMPONENT_GROUPEDTABLE].Rows)
				{
					ds.AddGroupedComponentInfoRow(
						GroupComponentRow[LicenceDS.LICENSECOMPONENT_GROUPNAME_FLD].ToString(),
						new Guid(GroupComponentRow[LicenceDS.LICENSECOMPONENT_COMPONENTID_FLD].ToString()),
						GroupComponentRow[LicenceDS.LICENSECOMPONENT_DISCRIPTION_FLD].ToString(),
						GroupComponentRow[LicenceDS.LICENSECOMPONENT_ALLOWEDINSTANCES_FLD].ToString(),
						GroupComponentRow[LicenceDS.LICENSECOMPONENT_COMPONENTVERSION_FLD].ToString());
				
				}
				#endregion

				#region Component List
				//Component  List
				foreach(DataRow ComponentListRow in licenceds.Tables[LicenceDS.LICENSECOMPONENT_LISTTABLE].Rows)
				{
					ds.AddComponentListRow(
						new Guid(ComponentListRow[LicenceDS.LICENSECOMPONENT_COMPONENTID_FLD].ToString()),
						ComponentListRow[LicenceDS.LICENSECOMPONENT_DISCRIPTION_FLD].ToString(),
						ComponentListRow[LicenceDS.LICENSECOMPONENT_ALLOWEDINSTANCES_FLD].ToString(),
						ComponentListRow[LicenceDS.LICENSECOMPONENT_COMPONENTVERSION_FLD].ToString());
				}
				#endregion
			}
		}


		protected void OnGetData(LicenceImportDS ds)
		{

		}
		
		#endregion

		#region Protected methods OnSetData --------------------------------------------
		protected ModifiedState OnSetData(LicenceDS ds)
		{
			this.licenceds = ds;
			return ModifiedState.MODIFIED;
		}

		protected override ModifiedState OnSetData(DataSet data)
		{
			base.OnSetData(data);
			if (data is LicenceImportDS)
			{
				LicenceImportDS licenceImportds = data as LicenceImportDS;
				if(!licenceImportds.HasStream) 
					return ModifiedState.UNCHANGED;
			
				XmlLicence licenceFile  = new XmlLicence();
			
				StreamReader streamReader = new StreamReader(licenceImportds.InputStream);
			
				string fileContent =  validateLicenceHash(streamReader.ReadToEnd().ToString());

				licenceFile.FileContent= fileContent;
				licenceFile.LoadLicense(true);

				LicenceDS licence = licenceFile.GetLicenceDS;
				this.ValidateImportedLicence(licence,this.CID) ;
			
				return this.OnSetData(licence);
			}
			else if (data is LicenceDS)
			{
				this.OnSetData((LicenceDS)data);
			}
			return ModifiedState.MODIFIED;
		}

		#endregion

		#region Is Licenced Public methods ---------------------------------------------

        public bool IsTeaLicenced()
        {
            if (DemoMode)
                return true;
            else if (LicenceConfigurationInfo(LicenceSM.TEA).Equals(0))
                return false;
            else
                return true;

        }

		public bool IsLicenced(string fileText, LicenceValidationType licenceValidationType)
		{
			if(RegisteredMode)
			{
				switch (licenceValidationType)
				{
					case LicenceValidationType.LedgerImport:
						if (!GetLicencedLedgerImport) 
							LedgerImportValidated(fileText);
							
						return true;
					default:
						return false;
				}
			}
			else if(DemoMode) {	return true; }	
			else { return false; }
		}

		public bool IsLicenced(Guid componentGUID, LicenceValidationType LicenceValidationtype)
		{
			if(RegisteredMode)
			{
				switch (LicenceValidationtype)
				{
					case LicenceValidationType.Entity:
						componentValidation(componentGUID,ENTITY);
						return true;

					case LicenceValidationType.StandardFiles:
						componentValidation(componentGUID,STANDARDFILES);
						return true;

					case LicenceValidationType.Groups:
						componentValidation(componentGUID,GROUP);
						return true;

					case LicenceValidationType.User:
					{
						userValidated(componentGUID);
						return true;
					}
					case LicenceValidationType.Scenarios:
					{
						isScenarioLicenced(componentGUID);
						return true;
					}
					
					default:
						return false;
				}
			}
			else if(DemoMode) {	return true; }	
			else { return false; }
	
		}

		public bool IsLicenced(Guid componentID)
		{
			if(RegisteredMode) 
			{
				
				Guid familyGuid = this.ConvertToComponentGUID(componentID);
		
				//Check if this componentID is part of Standardfiles
				bool licenseIsValid= false;
				if(familyGuid == Guid.Empty)
				{
					licenseIsValid = this.IsLicenced(componentID,LicenceValidationType.StandardFiles);
					if(licenseIsValid)
						return licenseIsValid;
				}

				string sectionName = getGroupName(familyGuid);

				switch (sectionName)
				{
					case ENTITY:
						return this.IsLicenced(componentID,LicenceValidationType.Entity);  
						
					case GROUP:
						return this.IsLicenced(componentID,LicenceValidationType.Groups); 
					
					case USERACCOUNTS:
						return this.IsLicenced(componentID,LicenceValidationType.User); 

					case MODULES:
						return true; //Currently modules are Unlimited licence.
						
					default:
						return false;
				}
			}
			else if(DemoMode) {	return true; }	
			else { return false; }
		}

		public bool IsLicenced(Guid componentGUID, bool throwOnFailure)
		{	
			if(RegisteredMode) 
			{
				if (!throwOnFailure)
				{
					return (this.getLicenceGroupAllowedCount(componentGUID) != 0 || this.getLicenceComponentAllowedCount(componentGUID) != 0);
				}
				else
				{
					return IsLicenced(componentGUID);
				}
			}
			else if(DemoMode) { return true; }
			else { return false; }
		}
	
		public bool IsLicenced(string typeName)
		{
			IComponentManagement manager =  this.Broker.GetComponentDictionary();
			Guid componentID = manager[typeName];
			return IsLicenced(componentID);
		}
		
		public bool IsLicenced(DateTime periodEndDate, string reportingUnitName)
		{
			if(RegisteredMode) 
			{
				DateTime licencedPeriodEndDate = DateTime.Parse(this.LicencedPeriodDate);
			
				if(periodEndDate<=licencedPeriodEndDate)
				{
					return true;
				}
				else 
				{ 
					MessageBoxDefinition licenceValidationMessage = new  MessageBoxDefinition(MessageBoxDefinition.MSG_ERR_LK_EXCEED_PERIODS , new string[]{licencedPeriodEndDate.ToShortDateString(), reportingUnitName});
					MessageBoxDefinition contactOritaxMessage = new MessageBoxDefinition(MessageBoxDefinition.MSG_INFO_LK_CONTACTOritax);
					this.Broker.LogEvent(EventType.LicencePeriodBreach,Guid.Empty, "Attempt to create or modify a period with end date greater allowed in Licence. The end date should not be greater than '" + this.LicencedPeriodDate + "'");
					throw new DisplayUserMessageException(new MessageBoxDefinition[]{licenceValidationMessage, contactOritaxMessage}, false,MessageDisplayMethod.MainWindowPopup);
				}
			}
			else if(DemoMode) {	return true; }	
			else { return false; }
		}

		private bool isScenarioLicenced(Guid CLID)
		{
			int licenceCount = GetLicencedScenariosCount;

			if (licenceCount != -1)
			{
				int currentScenarioCount = getScenarioCount(CLID);

				if (currentScenarioCount >= licenceCount)
				{
					MessageBoxDefinition licenceValidationMessage = new  MessageBoxDefinition(MessageBoxDefinition.MSG_ERR_LK_EXCESS_SCENARIOS , new string[]{licenceCount.ToString(), getLogicalModuleName(CLID)});
					MessageBoxDefinition contactOritaxMessage = new MessageBoxDefinition(MessageBoxDefinition.MSG_INFO_LK_CONTACTOritax);
                    this.Broker.LogEvent(EventType.LicenceScenarioBreach, Guid.Empty, "Attempt to create more scenarios than allowed in Licence. It is allowed to create " + licenceCount.ToString() + " scenarios at most");
					throw new DisplayUserMessageException(new MessageBoxDefinition[]{licenceValidationMessage, contactOritaxMessage}, false,MessageDisplayMethod.MainWindowPopup);
				}
			}
			return true;
		}

		public int DaysRemainingOnLicence()
		{
			TimeSpan numberOfDays = new TimeSpan(0);
			// Search Component Groups for ComponentID
			DataRowCollection FetchedRows = licenceds.Tables[LicenceDS.GLOBAL_TABLE].Rows;

			if(FetchedRows.Count>0)
			{
				DataRow drow = licenceds.Tables[LicenceDS.GLOBAL_TABLE].Rows[0];

				DateTime licenceDateExpiry = DateTime.Parse(drow[LicenceDS.GLOBAL_EXPIRYDATE_FLD].ToString());
				licenceDateExpiry.Add(new TimeSpan(23,59,59));
				numberOfDays = licenceDateExpiry - DateTime.Now;
			}
			
			return numberOfDays.Days;
		}

		#endregion

		#region Public properties ------------------------------------------------------
		public string LicencedPeriodDate
		{
			get
			{
				return getLicencedPeriodDate(licenceds); 
			}
		}


		public int GetLicencedEntitiesCount
		{
			get
			{
				return this.getSectionInstanceCount(ENTITY, licenceds); 
			}
		}

		public int GetLicencedGroupsCount
		{
			get
			{
				return this.getSectionInstanceCount(GROUP, licenceds); 
			}
		}

		public int GetLicencedScenariosCount
		{
			get
			{
				return getScenarioLicenceCount(licenceds);
			}
		}

		public int GetLicencedUserAccountCount
		{
			get
			{
				return this.getSectionInstanceCount(USERACCOUNTS, licenceds); 
			}
		}

		public bool GetLicencedLedgerImport
		{
			get
			{
				bool Result = false;
				
				if(licenceds.Tables[LicenceDS.GLOBAL_TABLE].Rows.Count>0)
				{
					DataRow FetchedRow =  licenceds.Tables[LicenceDS.GLOBAL_TABLE].Rows[0];

					Result = bool.Parse(FetchedRow[LicenceDS.GLOBAL_LICENCE_IMPORT_LEDGER_FLD].ToString());
				}

				return Result;
			}
		}


		public bool IsLicenceValid()
		{
			int daysRemaining = DaysRemainingOnLicence();
			
			return (daysRemaining>=0); 
		}

		public bool IsDemonstrationMode
		{
			get 
			{
				return this.DemoMode;
			}
		}
		#endregion

		#region Private Methods --------------------------------------------------------

		#region Ledger Import
		private void LedgerImportValidated(string fileText)
		{
			validateHash(fileText,LEDGER);
		}

		private string validateLicenceHash(string fileText)
		{
			validateHash(fileText,LICENCE);
			
			int licenseStart = fileText.IndexOf("<md5>");
			int licenseFinish = fileText.IndexOf("</md5>", 0);

			fileText = fileText.Substring(0, licenseStart);
			
			return fileText;

		}

		private void validateHash(string fileText, string type)
		{
			MessageBoxDefinition licenceValidationMessage = null;

			int licenseStart = fileText.IndexOf("<md5>");
			int licenseFinish = fileText.IndexOf("</md5>", 0);
		
			// Make sure that the MD5 tags are found
			if (!(licenseStart == -1 || licenseFinish == -1))
			{
				string md5String = fileText.Substring(licenseStart + 5, licenseFinish - licenseStart - 5);
				fileText = fileText.Substring(0, licenseStart);
				
				try
				{
					md5String = Encryption.DecryptData(md5String);
					
					if(!createMD5(fileText.Trim()).Equals(md5String))
					{
						if(type.Equals(LEDGER))
							 licenceValidationMessage = new  MessageBoxDefinition(MessageBoxDefinition.MSG_LEDGER_NO_LICENCE);
						else 
							licenceValidationMessage = new  MessageBoxDefinition(MessageBoxDefinition.MSG_ERR_LK_INVALID);
						
						throw new DisplayUserMessageException(licenceValidationMessage, false,MessageDisplayMethod.MainWindowPopup);
					}
				}
				catch
				{
					if(type.Equals(LEDGER))
						licenceValidationMessage = new  MessageBoxDefinition(MessageBoxDefinition.MSG_LEDGER_NO_LICENCE);
					else 
						licenceValidationMessage = new  MessageBoxDefinition(MessageBoxDefinition.MSG_ERR_LK_INVALID);

					throw new DisplayUserMessageException(licenceValidationMessage, false,MessageDisplayMethod.MainWindowPopup);
				}
			}
			else
			{
				if(type.Equals(LEDGER))
					licenceValidationMessage = new  MessageBoxDefinition(MessageBoxDefinition.MSG_LEDGER_NO_LICENCE);
				else 
					licenceValidationMessage = new  MessageBoxDefinition(MessageBoxDefinition.MSG_ERR_LK_INVALID);

				throw new DisplayUserMessageException(licenceValidationMessage, false,MessageDisplayMethod.MainWindowPopup);
			}
		
		}

		private string createMD5(string text)
		{
			MD5 md5 = new MD5CryptoServiceProvider();
			Encoding encoding = Encoding.ASCII;
			
			// Generate MD5 Hash
			byte[] result = md5.ComputeHash(encoding.GetBytes(text.Trim()));
			return encoding.GetString(result);
		}
		#endregion

		#region Component Validation
		
		private void componentValidation(Guid ComponentGUID,string sectionName)
		{
			int allowedCount = 0;
			int allowedComponentCount = 0;
			string componentMessage = "";
			
			bool groupCheck = checkGroupCount(ComponentGUID, sectionName, out allowedCount);
			bool componentCheck = checkComponentCount(ComponentGUID, sectionName, out allowedComponentCount, out componentMessage);
			
			if (!componentCheck)
			{
				allowedCount = allowedComponentCount;
			}
			
			if (!groupCheck || !componentCheck)
			{
				switch (sectionName)
				{
					case ENTITY:
					
						MessageBoxDefinition licenceValidationMessage = new  MessageBoxDefinition(MessageBoxDefinition.MSG_ERR_LK_EXCESS_ENTITIES, new string[]{allowedCount.ToString(), componentMessage});
						MessageBoxDefinition contactOritaxMessage = new MessageBoxDefinition(MessageBoxDefinition.MSG_INFO_LK_CONTACTOritax);
                        this.Broker.LogEvent(EventType.LicenceEntityBreach, Guid.Empty, "Attempt to create more entities than allowed in Licence. It is allowed to create " + allowedCount.ToString() + " entities at most");
						throw new DisplayUserMessageException(new MessageBoxDefinition[]{licenceValidationMessage, contactOritaxMessage}, false,MessageDisplayMethod.MainWindowPopup);

					case GROUP:
						licenceValidationMessage =  new MessageBoxDefinition(MessageBoxDefinition.MSG_ERR_LK_EXCESS_GROUPS, new string[]{allowedCount.ToString(), componentMessage});
						contactOritaxMessage = new MessageBoxDefinition(MessageBoxDefinition.MSG_INFO_LK_CONTACTOritax);
                        this.Broker.LogEvent(EventType.LicenceGroupBreach, Guid.Empty, "Attempt to create more groups than allowed in Licence. It is allowed to create " + allowedCount.ToString() + " groups at most");
						throw new DisplayUserMessageException(new MessageBoxDefinition[]{licenceValidationMessage, contactOritaxMessage}, false,MessageDisplayMethod.MainWindowPopup);

					default:
						break;
				}
			}
			
		}

		private bool checkGroupCount(Guid componentID, string sectionName, out int allowedNumber)
		{
			int currentCount = 0;
			allowedNumber = 0;

			DataRow[] rows = licenceds.Tables[LicenceDS.LICENSECOMPONENT_GROUPEDTABLE].Select(LicenceDS.LICENSECOMPONENT_GROUPNAME_FLD+"='"+sectionName+"'");
			
			foreach(DataRow row in rows)
				currentCount += this.GetCurrentReportingCount(new Guid(row[LicenceDS.LICENSECOMPONENT_COMPONENTID_FLD].ToString()));
			
			if (sectionName.Equals(ENTITY))
				allowedNumber= this.GetLicencedEntitiesCount;

			if (sectionName.Equals(GROUP))
				allowedNumber= this.GetLicencedGroupsCount;

			if (sectionName.Equals(STANDARDFILES))
				allowedNumber= this.GetLicencedGroupsCount;

			if (allowedNumber != -1)
			{
				if (!(currentCount < allowedNumber))
				{
					return false;
				}
			}
			
			return true;
		}

		private bool checkComponentCount(Guid componentID, string sectionName, out int allowedNumber, out string componentMessage)
		{
			
			int currentCount = 0;
			allowedNumber = 0;
			componentMessage = "";

			Guid familyGuid = this.ConvertToComponentGUID(componentID);
			DataRow[] rows = licenceds.Tables[LicenceDS.LICENSECOMPONENT_GROUPEDTABLE].Select(LicenceDS.LICENSECOMPONENT_GROUPNAME_FLD+"='"+sectionName+"' AND " +LicenceDS.LICENSECOMPONENT_COMPONENTID_FLD+"='" +familyGuid.ToString() +"'");
			
			if (rows.Length == 1)
			{
				currentCount = this.GetCurrentReportingCount(new Guid(rows[0][LicenceDS.LICENSECOMPONENT_COMPONENTID_FLD].ToString()));
				allowedNumber = int.Parse(rows[0][LicenceDS.LICENSECOMPONENT_ALLOWEDINSTANCES_FLD].ToString());
			}
			
			if (allowedNumber != -1)
			{
				if (!(currentCount < allowedNumber))
				{
					componentMessage = "of Type " + rows[0][LicenceDS.LICENSECOMPONENT_DISCRIPTION_FLD].ToString() +" ";
					return false;
				}
			}
			
			return true;
		}
		
		private void userValidated(Guid componentGuid)
		{
			int licenceCount = GetLicencedUserAccountCount;

			if (licenceCount != -1)
			{
				int currentUserCount = getCurrentUserCount(componentGuid);
		
				if (currentUserCount >= licenceCount)
				{
					MessageBoxDefinition licenceValidationMessage = new  MessageBoxDefinition(MessageBoxDefinition.MSG_ERR_LK_EXCESS_USERS, new string[]{licenceCount.ToString()});
					MessageBoxDefinition contactOritaxMessage = new  MessageBoxDefinition(MessageBoxDefinition.MSG_INFO_LK_CONTACTOritax);
                    this.Broker.LogEvent(EventType.LicenceUserAccountBreach, Guid.Empty, "Attempt to create more user accounts than allowed in Licence. It is allowed to create " + licenceCount.ToString() + " user accounts at most.");
					throw new DisplayUserMessageException(new MessageBoxDefinition[]{licenceValidationMessage,contactOritaxMessage}, false,MessageDisplayMethod.MainWindowPopup);
				}
			}
		}

		private string getLicencedPeriodDate(LicenceDS licence)
		{
			string Result = string.Empty;
			
			if(licence.Tables[LicenceDS.GLOBAL_TABLE].Rows.Count>0)
			{
				DataRow FetchedRow =  licence.Tables[LicenceDS.GLOBAL_TABLE].Rows[0];

				Result = FetchedRow[LicenceDS.GLOBAL_PERIOD_ENDDATE_FLD].ToString();
			}

			return Result;
		}

		#endregion

		#region Component ID conversion
		private Guid ConvertToComponentGUID(Guid AssemblyGUID, bool EntityType)
		{
			IComponentManagement componentDictionary=this.Broker.GetComponentDictionary();
			IComponentVersion componentVersion=componentDictionary[AssemblyGUID];

			if(componentVersion!=null)
			{
				IComponent component=componentDictionary.GetComponent(componentVersion.ComponentID);
				return component.ID;  
			}
			
			return Guid.Empty;
		}

		private Guid ConvertToComponentGUID(Guid AssemblyGUID)
		{
			IComponentManagement componentDictionary=this.Broker.GetComponentDictionary();

			Guid assemblyguid  = new Guid(AssemblyGUID.ToString()); 
			
			IComponentVersion componentVersion=componentDictionary[assemblyguid];
			
			if(componentVersion!=null)
			{
				IComponent component=componentDictionary.GetComponent(componentVersion.ComponentID);
			
				return component.ID;  
			}

			return Guid.Empty;

		}
		#endregion

		#region Component information Retrieval
		
		public int getSectionInstanceCount(string section, LicenceDS licence)
		{
			DataTable sections = licence.Tables[LicenceDS.CG_TABLE];

			if(sections!=null)
			{
				DataView sectionsView = new DataView(sections);
				sectionsView.RowFilter = LicenceDS.CG_NAME_FLD+"='"+section+"'";
				if (sectionsView.Count>0)
				{
					return int.Parse(sectionsView[0].Row[LicenceDS.CG_ALLOWEDINSTANCES_FLD].ToString());
				}
				else
				{
					return  0;
				}
			}

			return  0;
		}

		private string getGroupName(Guid componentID)
		{
			string name = "";

			DataRow[] rows = licenceds.Tables[LicenceDS.LICENSECOMPONENT_GROUPEDTABLE].Select(LicenceDS.LICENSECOMPONENT_COMPONENTID_FLD+"='"+componentID.ToString()+"'");
			if(rows.Length>0)
			{
				name = rows[0][LicenceDS.LICENSECOMPONENT_GROUPNAME_FLD].ToString();
			}
			return name;
		}

		private int getLicenceGroupAllowedCount(Guid ComponentGUID)
		{
			int groupInstancesAllowed = 0;
			
			DataRow[] rows = licenceds.Tables[LicenceDS.LICENSECOMPONENT_GROUPEDTABLE].Select(LicenceDS.LICENSECOMPONENT_COMPONENTID_FLD+"='"+ComponentGUID.ToString()+"'");

			if(rows.Length>0)
			{
				DataRow row = rows[0];
				
				string groupName = row[LicenceDS.LICENSECOMPONENT_GROUPNAME_FLD].ToString();

				DataRow[] groupRows = licenceds.Tables[LicenceDS.CG_TABLE].Select(LicenceDS.CG_NAME_FLD+"='"+groupName+"'");
				
				if(groupRows.Length>0 && groupName != STANDARDFILES)
				{	
					DataRow gRow = groupRows[0];
					groupInstancesAllowed = int.Parse(gRow[LicenceDS.CG_ALLOWEDINSTANCES_FLD].ToString());
				}
				else
					//This is to make sure allowed instances field is used from actual licensecomponent row 
					groupInstancesAllowed = int.Parse(row[LicenceDS.LICENSECOMPONENT_ALLOWEDINSTANCES_FLD].ToString());
			}

			return groupInstancesAllowed;
		}
		
		private int getLicenceComponentAllowedCount(Guid ComponentGUID) 
		{
			int instancesAllowed = 0;
	
			DataRow[] rows =licenceds.Tables[LicenceDS.LICENSECOMPONENT_LISTTABLE].Select(LicenceDS.LICENSECOMPONENT_COMPONENTID_FLD+"='"+ComponentGUID.ToString()+"'");
			
			if(rows.Length>0)
			{
				DataRow row = rows[0];
				instancesAllowed = int.Parse(row[LicenceDS.LICENSECOMPONENT_ALLOWEDINSTANCES_FLD].ToString());
			}
			
			return instancesAllowed;
		}

		#endregion

		#endregion

		#region Entities and Groups Licence check --------------------------------------

		private DataSet getAllReportingUnits()
		{
			IOrganization organisation = (IOrganization) this.Broker.GetWellKnownCM( WellKnownCM.Organization );
			DataSet reportingUnitsds = organisation.GetReportingUnits();
			this.Broker.ReleaseBrokerManagedComponent(organisation);
			return reportingUnitsds;
		}

		public int GetCurrentReportingCount(string typeName)
		{
			DataSet reportingUnitsds = getAllReportingUnits();
			DataTable reportingUnitList = reportingUnitsds.Tables["ReportingUnitListTable"];
			
			if(reportingUnitList!=null)
			{
				if(typeName == String.Empty)
					return reportingUnitList.Rows.Count;
				else
				{
					DataView reportingUnitListView = new DataView(reportingUnitList);
					reportingUnitListView.RowFilter = "ReportingUnitListTypeName = '" + typeName + "'";
					return reportingUnitListView.Count;
				}
			}
			return  0;
		}

		public int GetCurrentReportingCount(Guid componentID)
		{
			int reportingUnitCount = 0;
            IComponentManagement components = this.Broker.GetComponentDictionary();
            IOrganization organisation = (IOrganization) this.Broker.GetWellKnownCM( WellKnownCM.Organization );
			DataSet reportingUnitsds = organisation.GetReportingUnits();
			this.Broker.ReleaseBrokerManagedComponent(organisation);
			DataTable reportingUnitList = reportingUnitsds.Tables["ReportingUnitListTable"];

			if(componentID.Equals(Guid.Empty))
				return reportingUnitList.Rows.Count;

			IComponentVersion[] componentVersions = components.GetComponentVersions(componentID);

			foreach(IComponentVersion componentVersion in componentVersions)
			{
				DataRow[] reportingUnits = reportingUnitList.Select("ReportingUnitListTypeID = '" + componentVersion.ID + "'");
				reportingUnitCount += reportingUnits.Length;
			}

			return reportingUnitCount;
		}

		private int getCurrentUserCount(Guid typeID)
		{
			int userCount = 0;

			Guid componentID = ConvertToComponentGUID(typeID);
			IComponentManagement components = this.Broker.GetComponentDictionary();
			IComponentVersion[] componentVersions = components.GetComponentVersions(componentID);

			foreach(IComponentVersion componentVersion in componentVersions)
			{
				DataRowCollection collection = this.Broker.GetBMCList(componentVersion.ID) as DataRowCollection;
				if (collection != null)
				{
					userCount += collection.Count -1 ;
				}
			}

			// we always have at least one user and that is the System Administrator user.
			// The systme should never return a negative count for users.
			if (userCount<0)
				userCount = 1;

			return userCount;
		}

		#endregion

		#region Private properties -----------------------------------------------------
		private string getLicenseType
		{
			get
			{
				string Result =string.Empty;
				
				if(licenceds.Tables[LicenceDS.GLOBAL_TABLE].Rows.Count>0)
				{
					DataRow FetchedRow =  licenceds.Tables[LicenceDS.GLOBAL_TABLE].Rows[0];
					Result =  FetchedRow[LicenceDS.GLOBAL_LICENCETYPE_FLD].ToString();
				}

				return Result;
			}
		}

		private bool DemoMode
		{
			get
			{
				return (getLicenseType.Equals(LicenceDS.LicenceType.Demo.ToString()));
			}
		}

		private bool RegisteredMode
		{
			get
			{
				return (getLicenseType.Equals(LicenceDS.LicenceType.Registered.ToString()));
			}
		}

		private int getScenarioCount(Guid clid)
		{
			int count = 0;
			
			LogicalModuleBase logicalModule = this.Broker.GetLogicalCM(clid) as LogicalModuleBase;

			if (logicalModule != null)
			{
				LogicalModuleBase.CMScenarios scenarios = (LogicalModuleBase.CMScenarios)logicalModule.Scenarios as LogicalModuleBase.CMScenarios;
				if (scenarios != null)
					count = scenarios.CountMainScenarios;
			}

			this.Broker.ReleaseBrokerManagedComponent(logicalModule);
			
			return count;
		}

		private string getLogicalModuleName(Guid clid)
		{
			LogicalModuleBase logicalModule = this.Broker.GetLogicalCM(clid) as LogicalModuleBase;

			try
			{
				if (logicalModule != null)
					return logicalModule.Name;
				else
					return String.Empty;
			}
			finally
			{
				this.Broker.ReleaseBrokerManagedComponent(logicalModule);
			}
		}

		#endregion

		#region Import Licence Validation ----------------------------------------------

		#region Main Validation method
		private void ValidateImportedLicence(LicenceDS importedLicence, Guid InstallationID)
		{
			// if demo licence being uploaded, we don't validate
			if(!isDemonstrationLicence(importedLicence))
			{
				ArrayList arrVmessages = new ArrayList();

				checkInstallationID(importedLicence,InstallationID,arrVmessages);
				checkEntities(importedLicence,arrVmessages);
				checkGroups(importedLicence,arrVmessages);
				checkPeriods(importedLicence,arrVmessages);
				checkUsers(importedLicence,arrVmessages); 
				checkScenarios(importedLicence,arrVmessages); 
				checkTEA(importedLicence,arrVmessages); 

				if (arrVmessages.Count>0)
				{										
					arrVmessages.Insert(0,(new MessageBoxDefinition(MessageBoxDefinition.MSG_INFO_LK_CONTACTOritax)));					
					MessageBoxDefinition[] messages = (MessageBoxDefinition[]) arrVmessages.ToArray(typeof(MessageBoxDefinition)); 
					throw new DisplayUserMessageException(messages, false,MessageDisplayMethod.MainWindowPopup);
				}
			}
		}
		#endregion

		#region Group and Entities
		private void checkGroups_Entities(LicenceDS importedLicence,string sectionName, ArrayList Messages)
		{

			// list of entities from system and Make sure that they exist in the imported licence
			int currentCount = 0;
			int licencedComponentCount = 0;

			DataRow[] FetchedRows =importedLicence.Tables[LicenceDS.LICENSECOMPONENT_GROUPEDTABLE].Select(LicenceDS.LICENSECOMPONENT_GROUPNAME_FLD+"='"+sectionName+"'");
			
			foreach(DataRow dRow in FetchedRows)
				currentCount += this.GetCurrentReportingCount(new Guid(dRow[LicenceDS.LICENSECOMPONENT_COMPONENTID_FLD].ToString()));
				
			if (sectionName.Equals(ENTITY))
				licencedComponentCount= getSectionInstanceCount(ENTITY,importedLicence);

			if (sectionName.Equals(GROUP))
				licencedComponentCount= getSectionInstanceCount(GROUP,importedLicence);

			if(!licencedComponentCount.Equals(UNLIMITED)) 
			{
				if(!(currentCount<=licencedComponentCount))
				{
					switch (sectionName)
					{
						case ENTITY:
							MessageBoxDefinition licenceValidationMessage = new  MessageBoxDefinition(MessageBoxDefinition.MSG_ERR_LK_EXCESS_ENTITIES, new string[]{licencedComponentCount.ToString(), ""});
							Messages.Add(licenceValidationMessage);
							break;
						case GROUP:
							licenceValidationMessage =  new MessageBoxDefinition(MessageBoxDefinition.MSG_ERR_LK_EXCESS_GROUPS, new string[]{licencedComponentCount.ToString(), ""});
							Messages.Add(licenceValidationMessage);
							break;
						default:
							break;
					}
				}
			}
		}

		private void checkEntities(LicenceDS importedLicence, ArrayList Messages)
		{
			checkGroups_Entities(importedLicence,ENTITY,Messages);
		} 
		
		private void checkGroups(LicenceDS importedLicence, ArrayList Messages)
		{
			checkGroups_Entities(importedLicence,GROUP,Messages);
		}
		#endregion

        private int LicenceConfigurationInfo(string section)
        {
            DataTable sections = licenceds.Tables[LicenceDS.CG_TABLE];

            if (sections != null)
            {
                DataView sectionsView = new DataView(sections);
                sectionsView.RowFilter = LicenceDS.CG_NAME_FLD + "='" + section + "'";
                if (sectionsView.Count > 0)
                    return int.Parse(sectionsView[0].Row[LicenceDS.CG_ALLOWEDINSTANCES_FLD].ToString());
                else
                    return -1;
            }
            return -1;
        }

		private void checkUsers(LicenceDS importedLicence, ArrayList messages)
		{
			int currentUserCount = 0;
			int licenceCount = getSectionInstanceCount(USERACCOUNTS,importedLicence) ;

			DataRow[] fetchedRows = importedLicence.Tables[LicenceDS.LICENSECOMPONENT_GROUPEDTABLE].Select(LicenceDS.LICENSECOMPONENT_GROUPNAME_FLD+"='" + USERACCOUNTS+"'");
			
			if(fetchedRows.Length>0)
			{
				foreach(DataRow drow in fetchedRows)
					currentUserCount += getCurrentUserCount(new Guid(drow[LicenceDS.LICENSECOMPONENT_COMPONENTID_FLD].ToString()));
			}
			
			if(!licenceCount.Equals(UNLIMITED))
			{
				if (currentUserCount > licenceCount)
				{
					MessageBoxDefinition licenceValidationMessage = new  MessageBoxDefinition(MessageBoxDefinition.MSG_ERR_LK_EXCESS_USERS, new string[]{licenceCount.ToString()});
					messages.Add(licenceValidationMessage);
				}
			}
		}

		private void checkPeriods(LicenceDS importedLicence, ArrayList messages)
		{
			DateTime licencedPeriodEndDate = DateTime.Parse(getLicencedPeriodDate(importedLicence));
			checkPeriodsByReportingUnit(licencedPeriodEndDate,messages);
		}

		private void checkPeriodsByReportingUnit(DateTime licencedPeriodEndDate, ArrayList messages)
		{
			DateTime systemPeriodDate = DateTime.Now;
			
			DataSet allReportingUnitsDS = getAllReportingUnits();
			DataTable reportingUnitList = allReportingUnitsDS.Tables["ReportingUnitListTable"];

			foreach(DataRow entityRow in reportingUnitList.Rows)
			{
				Guid ctid = (Guid) entityRow["ReportingUnitListTypeID"];
				Guid clid = (Guid) entityRow["ReportingUnitListCLID"];

				IComponentManagement componentDictionary = Broker.GetComponentDictionary();
				ILogicalModule logicalRU=this.Broker.GetLogicalCM(clid);

				ArrayList allPeriodCMs = new ArrayList();
				IEnumerable lRUScenarios =  logicalRU.Scenarios;

				foreach(DictionaryEntry ruScenarioDE in lRUScenarios)
				{
					CMScenario ruScenario = (CMScenario) ruScenarioDE.Value;
					ArrayList ruchildCMScenarios=logicalRU.GetChildCMs(ruScenario.CSID);
					allPeriodCMs.AddRange(ruchildCMScenarios);
				}

				this.Broker.ReleaseBrokerManagedComponent(logicalRU);
				
				foreach(CMScenario periodCMScenario in allPeriodCMs)
				{
					#region Period check
					ICalculationModule periodCM = this.Broker.GetCMImplementation(periodCMScenario.CLID,periodCMScenario.CSID);
					systemPeriodDate = ((IPeriod)periodCM).EndDate;

					this.Broker.ReleaseBrokerManagedComponent(periodCM);
					
					if(systemPeriodDate>licencedPeriodEndDate)
					{
						string licencedPeriodDate = licencedPeriodEndDate.ToString("d");
						MessageBoxDefinition licenceValidationMessage = new  MessageBoxDefinition(MessageBoxDefinition.MSG_ERR_LK_EXCEED_PERIODS, new string[]{licencedPeriodDate,entityRow["ReportingUnitListName"].ToString()});
						messages.Add(licenceValidationMessage);
					}
					#endregion
				}
			}
		}

		private void checkScenarios(LicenceDS importedLicence, ArrayList messages)
		{
			int licenceCount = getScenarioLicenceCount(importedLicence);

			if (licenceCount != -1)
			{
				IOrganization organisation = (IOrganization) this.Broker.GetWellKnownCM( WellKnownCM.Organization );
				DataSet reportingUnitsds = organisation.GetReportingUnits();
				this.Broker.ReleaseBrokerManagedComponent(organisation);
				DataTable reportingUnitList = reportingUnitsds.Tables["ReportingUnitListTable"];

				foreach (DataRow row in reportingUnitList.Rows)
				{
					Guid clid = (Guid)row["ReportingUnitListCLID"];
					int scenarioCount = getScenarioCount(clid);

					if (scenarioCount > licenceCount)
					{
						MessageBoxDefinition licenceValidationMessage = new  MessageBoxDefinition(MessageBoxDefinition.MSG_ERR_LK_EXCESS_SCENARIOS, new string[]{licenceCount.ToString(), getLogicalModuleName(clid)});
						messages.Add(licenceValidationMessage);
					}
				}
			}
		}

		private void checkTEA(LicenceDS importedLicence, ArrayList messages)
		{
			Guid teaGuid = new Guid("69C2186E-9549-42ba-A621-2B5F17C75356");

			int instancesAllowed = getSectionInstanceCount(TEA,importedLicence);

			IEnumerable enumenum = this.Broker.GetCMTypes("TaxEffectAccounting");
			IEnumerable enumenum1 = this.Broker.GetCMTypes("TaxEffectAccounting_1_1");
			
			if (isComponentConfigured(teaGuid))
			{
				if(instancesAllowed.Equals(0))
				{
					MessageBoxDefinition licenceValidationMessage = new  MessageBoxDefinition(MessageBoxDefinition.MSG_ERR_LK_TEA_INVALID);
					messages.Add(licenceValidationMessage);
				}
			}
		}

		private bool isComponentConfigured(Guid componentID)
		{
			IComponentManagement components = this.Broker.GetComponentDictionary();
			IComponentVersion[] componentVersions = components.GetComponentVersions(componentID);

			foreach(IComponentVersion componentVersion in componentVersions)
			{
				DataRowCollection collection = this.Broker.GetBMCList(componentVersion.ID) as DataRowCollection;
				if (collection != null)
				{
					if (collection.Count > 0)
					{
						return true;
					}
				}
			}
			return false;

		}

		private int getScenarioLicenceCount(LicenceDS licenceDS)
		{
			int count = 0;
			if(licenceDS.Tables[LicenceDS.GLOBAL_TABLE].Rows.Count>0)
			{
				DataRow row =  licenceDS.Tables[LicenceDS.GLOBAL_TABLE].Rows[0];

				count = int.Parse(row[LicenceDS.GLOBAL_LICENCE_SCENARIOS_FLD].ToString());
			}

			return count;
		}

		#region Installation ID validation
		private void checkInstallationID(LicenceDS importedLicence,Guid InstallationID, ArrayList Messages)
		{
			DataRowCollection drCol = importedLicence.Tables[LicenceDS.GLOBAL_TABLE].Rows;

			if(drCol.Count>0)
			{
				DataRow drow = drCol[0];

				Guid importedLicenceGUID = new Guid(drow[LicenceDS.GLOBAL_LICENCE_GUID_FLD].ToString());
				
				if(!importedLicenceGUID.Equals(Guid.Empty))
				{
					if(!importedLicenceGUID.Equals(InstallationID))
					{
						MessageBoxDefinition mbd = new MessageBoxDefinition(MessageBoxDefinition.MSG_ERR_LK_INVALID);
						Messages.Add(mbd); 
					}
				}
			}
		}


		private bool isDemonstrationLicence(LicenceDS importedLicence)
		{
			bool isDemo = false;
			
			DataRowCollection drCol = importedLicence.Tables[LicenceDS.GLOBAL_TABLE].Rows;

			if(drCol.Count>0)
			{
				DataRow drow = drCol[0];

				Guid importedLicenceGUID = new Guid(drow[LicenceDS.GLOBAL_LICENCE_GUID_FLD].ToString());
			
				if(!importedLicenceGUID.Equals(Guid.Empty))
					isDemo=false;
				else
					isDemo=true;
			}
			return isDemo;
		}

		#endregion

		#endregion
	}


}
