using System;

namespace Oritax.TaxSimp.TaxSimpLicence
{
	public class LicenceInstall
	{
		#region INSTALLATION PROPERTIES ------------------------------------------------

		// Assembly Installation Properties
		public const string ASSEMBLY_ID="26437F7E-10D8-4887-A462-E92A7EF3F05C";
		public const string ASSEMBLY_NAME="Licence_1_1";
		public const string ASSEMBLY_DISPLAYNAME="Nable Licence";
		public const string ASSEMBLY_MAJORVERSION="1";
		public const string ASSEMBLY_MINORVERSION="1";
		public const string ASSEMBLY_DATAFORMAT="0";
		public const string ASSEMBLY_REVISION="0";

		// Component Installation Properties
		public const string COMPONENT_ID="9E6870FD-3F53-45c6-A398-FA5C20A3CB42";
		public const string COMPONENT_NAME="Licence";
		public const string COMPONENT_DISPLAYNAME="Nable Licence";
		public const string COMPONENT_CATEGORY="Licence";

		// Component Version Installation Properties
		public const string COMPONENTVERSION_STARTDATE="01/01/1990 00:00:00 AM";
		public const string COMPONENTVERSION_ENDDATE="31/12/2050 12:59:59 AM";
		public const string COMPONENTVERSION_PERSISTERASSEMBLY="Licence_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";
		public const string COMPONENTVERSION_PERSISTERCLASS="Oritax.TaxSimp.TaxSimpLicence.LicencePersister";
		public const string COMPONENTVERSION_IMPLEMENTATIONCLASS="Oritax.TaxSimp.TaxSimpLicence.LicenceSM";

		
		// Data Model
		public const string ASSEMBLY_PERSISTERASSEMBLY="Licence_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";
		public const string ASSEMBLY_PERSISTERCLASS="Oritax.TaxSimp.TaxSimpLicence.LicencePersister";

		#endregion

	}
}
