﻿using System;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.CM.Organization
{
    [Serializable]
    public class OrganizationUnit : IIdentityCM
    {
        public UserEntity CurrentUser { get; set; }
        public Guid Clid { get; set; }
        public Guid Csid { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Data { get; set; }
        public bool IsInvestableClient { get; set; }
        public string OrganizationStatus { get; set; }
        public string ClientId { get; set; }
        public string ApplicationID { get; set; }
        public string AccountProcessStatus { get; set; }
        public bool HasAdviser { get; set; }
    }

    [Serializable]
    public class OrganizationUnitExport : OrganizationUnit
    {
        public string IsExport { get; set; }
        public string ApplicationDate { get; set; }
    }
}
