using System;

namespace Oritax.TaxSimp.CM.Organization
{
    /// <summary>
    /// Summary description for OrganizationInstall.
    /// </summary>
    public class OrganizationInstall
    {
        #region INSTALLATION PROPERTIES

        // Assembly Installation Properties
        public const string ASSEMBLY_ID = "5BDAB7B9-3623-455C-8987-B8F9C166DCEA";
        public const string ASSEMBLY_NAME = "Organization_1_1";
        public const string ASSEMBLY_DISPLAYNAME = "Organization V1.1";
        public const string ASSEMBLY_MAJORVERSION = "1";
        public const string ASSEMBLY_MINORVERSION = "1";
        public const string ASSEMBLY_DATAFORMAT = "1"; 
        public const string ASSEMBLY_REVISION = "0";	

        // Component Installation Properties
        public const string COMPONENT_ID = "59DFAAA8-FB81-492c-964D-967A2847ED83";
        public const string COMPONENT_NAME = "Organization";
        public const string COMPONENT_DISPLAYNAME = "Organization";
        public const string COMPONENT_CATEGORY = "BusinessStructure";

        // Component Version Installation Properties
        public const string COMPONENTVERSION_STARTDATE = "01/01/1900 00:00:00 AM";
        public const string COMPONENTVERSION_ENDDATE = "31/12/2050 12:59:59 AM";
        public const string COMPONENTVERSION_PERSISTERASSEMBLY = "Organization_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";
        public const string COMPONENTVERSION_PERSISTERCLASS = "Oritax.TaxSimp.CM.Organization.OrganizationPersister";
        public const string COMPONENTVERSION_IMPLEMENTATIONCLASS = "Oritax.TaxSimp.CM.Organization.OrganizationCM";

        // Data Model
        public const string ASSEMBLY_PERSISTERASSEMBLY = "Organization_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";
        public const string ASSEMBLY_PERSISTERCLASS = "Oritax.TaxSimp.CM.Organization.OrganizationPersister";

        #endregion
    }
}
