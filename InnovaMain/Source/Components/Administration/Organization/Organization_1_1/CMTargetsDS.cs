using System;
using System.Data;

namespace Oritax.TaxSimp.CM.Organization
{
	/// <summary>
	/// Summary description for CMTargetsDS.
	/// </summary>
	public class CMTargetsDS : DataSet
	{
		public const String CMTARGETS_TABLE					= "CMTargetsTable";
		
		public const String PRIMARYKEY_FIELD				= "ID";
		public const String COMPONENTID_FIELD				= "ComponentID";
		public const String DESCRIPTION_FIELD				= "Description";
		public const String HIDE_FIELD						= "Hide";
		
		public CMTargetsDS()
		{
			DataTable table;
			
			table = new DataTable(CMTARGETS_TABLE);
			table.Columns.Add(PRIMARYKEY_FIELD, typeof(System.Guid));
			table.Columns.Add(COMPONENTID_FIELD, typeof(System.Guid));
			table.Columns.Add(DESCRIPTION_FIELD, typeof(System.String));
			table.Columns.Add(HIDE_FIELD, typeof(System.Boolean));
			this.Tables.Add(table);
		}
	}
}
