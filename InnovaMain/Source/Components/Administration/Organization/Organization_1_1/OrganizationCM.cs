﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Xml.Linq;
using System.Linq;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.IOrganizationUnits;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.CM.Organization.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.CM.Organization.UMAForm;
using Oritax.TaxSimp.CM.Organization.Model.Organization;
using Oritax.TaxSimp.CM.Organization.Commands;
using Oritax.TaxSimp.Common;
using System.Globalization;
using Oritax.TaxSimp.Common.UMAFee;
using Oritax.TaxSimp.Common.Data;
using AddressEntity = Oritax.TaxSimp.Data.AddressEntity;

namespace Oritax.TaxSimp.CM.Organization
{
    [Serializable]
    public class Status
    {
        public string Name { get; set; }
        public string Type { get; set; }
    }

    [Serializable]
    public class OrganizationValue
    {
        public int SecurityEnabled { get; set; }
        public bool DenyNewLogins { get; set; }
        public string LoginAdminMessage { get; set; }
        public string SystemMessage { get; set; }
        public CMTargetsList TargetsList { get; set; }
        public string OrganisationName { get; set; }
    }

    [Serializable]
    public class ProgramCode
    {
        public int Code { get; set; }
        public ProgramCode() { }
        public ProgramCode(int code) { Code = code; }
    }

    [Serializable]
    public partial class OrganizationCM : BusinessStructureBase, IOrganization, ISerializable, ISupportMigration, IModelStructure, ISecuritesStructure
    {
        private static readonly string TableName = "Entities_Table";

        public List<Status> StatusList { get { return _StatusList; } }
        private List<Status> _StatusList;

        public List<FeeEntity> FeeEnttyList { get { return _FeeEnttyList; } set { _FeeEnttyList = value; } }
        private List<FeeEntity> _FeeEnttyList;

        private SmtpServerEntity _SmtpSettings;
        public SmtpServerEntity SmtpSettings { set { _SmtpSettings = value; } get { return _SmtpSettings; } }

        public List<ProductEntity> Products { get { return _Products; } set { _Products = value; } }
        private List<ProductEntity> _Products;

        public List<AssetEntity> Assets { get { return _Assets; } set { _Assets = value; } }
        private List<AssetEntity> _Assets;

        public List<UMAFormEntity> UMAFormEntityList { get { return _UMAFormEntityList; } set { _UMAFormEntityList = value; } }
        private List<UMAFormEntity> _UMAFormEntityList;

        public List<FormOrganization> FormOrganizations { get { return _FormOrganizations; } set { _FormOrganizations = value; } }
        private List<FormOrganization> _FormOrganizations;

        public List<BusinessGroupEntity> BusinessGroups { get { return _BusinessGroups; } set { _BusinessGroups = value; } }
        private List<BusinessGroupEntity> _BusinessGroups;

        public List<ModelEntity> Model { get { return _Model; } set { _Model = value; } }
        private List<ModelEntity> _Model;

        public ProgramCode MaxProgramCode { get { return _maxProgramCode; } set { _maxProgramCode = value; } }
        private ProgramCode _maxProgramCode;

        public List<InstitutionEntity> Institution { get { return _Institution; } set { _Institution = value; } }
        private List<InstitutionEntity> _Institution;

        public List<SecuritiesEntity> Securities { get { return _Securities; } set { _Securities = value; } }
        private List<SecuritiesEntity> _Securities;

        public List<ManualAssetEntity> ManualAsset { get { return _ManualAsset; } set { _ManualAsset = value; } }
        private List<ManualAssetEntity> _ManualAsset;

        public List<ProductSecuritiesEntity> ProductSecurities { get { return _ProductSecurities; } set { _ProductSecurities = value; } }
        private List<ProductSecuritiesEntity> _ProductSecurities;

        public List<InvestmentManagementEntity> InvestmentManagementDetails { get { return _InvestmentManagementDetails; } set { _InvestmentManagementDetails = value; } }
        [OptionalField]
        private List<InvestmentManagementEntity> _InvestmentManagementDetails = new List<InvestmentManagementEntity>();
        public Dictionary<string, string> BussinessTitles { get { return _BussinessTitles; } set { _BussinessTitles = value; } }
        private Dictionary<string, string> _BussinessTitles;
        public OrganizationalSettings OrganizationalSettings { get; set; }

        public List<CashBalanceEntity> CashBalances { get { return _CashBalances; } set { _CashBalances = value; } }
        private List<CashBalanceEntity> _CashBalances;

        #region IOrganization Implementation
        public void DeleteOrganizationUnit(Guid gdOrgUnitCLID)
        {
            throw new System.NotImplementedException();
        }
        public DataSet GetReportingUnits()
        {
            return new DataSet();
        }
        //public string OrganisationName { get; set; }
        public int SecuritySetting { get; set; }
        public string SystemWideDisplayMessage { get; set; }
        #endregion

        private OrganizationValue _Organization;

        public OrganizationCM()
            : base()
        {
            _StatusList = new List<Status>();
            _SmtpSettings = new SmtpServerEntity();
            _Products = new List<ProductEntity>();
            _Assets = new List<AssetEntity>();
            _UMAFormEntityList = new List<UMAFormEntity>();
            _FormOrganizations = new List<FormOrganization>();
            _BusinessGroups = new List<BusinessGroupEntity>();
            _Model = new List<ModelEntity>();
            _Institution = new List<InstitutionEntity>();
            _maxProgramCode = new ProgramCode();
            _Securities = new List<SecuritiesEntity>();
            _ManualAsset = new List<ManualAssetEntity>();
            _ProductSecurities = new List<ProductSecuritiesEntity>();
            OrganizationalSettings = new OrganizationalSettings();
            _Organization = new OrganizationValue
            {
                SecurityEnabled = 1,
                DenyNewLogins = false,
                LoginAdminMessage = string.Empty,
                SystemMessage = string.Empty,
                OrganisationName = "Default",
                TargetsList = new CMTargetsList()
            };
            _CashBalances = new List<CashBalanceEntity>();
        }

        protected OrganizationCM(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            _Organization = info.GetValue<OrganizationValue>("Oritax.TaxSimp.CM.Organization.OrganizationCM.Organization");
            _UMAFormEntityList = info.GetValue<List<UMAFormEntity>>("Oritax.TaxSimp.CM.Organization.UMAForm.UMAFormEntity");
            _FormOrganizations = info.GetValue<List<FormOrganization>>("Oritax.TaxSimp.CM.Organization.Model.Organization.FormOrganization");
            _StatusList = info.GetValue<List<Status>>("Oritax.TaxSimp.CM.Organization.OrganizationCM.StatusList");
            _SmtpSettings = info.GetValue<SmtpServerEntity>("Oritax.TaxSimp.CM.Organization.OrganizationCM.SmtpSettings");
            _Products = info.GetValue<List<ProductEntity>>("Oritax.TaxSimp.CM.Organization.OrganizationCM.Products");
            _Assets = info.GetValue<List<AssetEntity>>("Oritax.TaxSimp.CM.Organization.OrganizationCM.Assets");
            _BusinessGroups = info.GetValue<List<BusinessGroupEntity>>("Oritax.TaxSimp.CM.Organization.OrganizationCM.BusinessGroups");
            _Model = info.GetValue<List<ModelEntity>>("Oritax.TaxSimp.CM.Organization.OrganizationCM.Model");
            _Institution = info.GetValue<List<InstitutionEntity>>("Oritax.TaxSimp.CM.Organization.OrganizationCM.Institution");
            _maxProgramCode = info.GetValue<ProgramCode>("Oritax.TaxSimp.CM.Organization.OrganizationCM.MaxProgramCode");
            _Securities = info.GetValue<List<SecuritiesEntity>>("Oritax.TaxSimp.CM.Organization.OrganizationCM.Securities");
            _ManualAsset = info.GetValue<List<ManualAssetEntity>>("Oritax.TaxSimp.CM.Organization.OrganizationCM.ManualAsset");
            _ProductSecurities = info.GetValue<List<ProductSecuritiesEntity>>("Oritax.TaxSimp.CM.Organization.OrganizationCM.ProductSecurities");
            _FeeEnttyList = info.GetValue<List<FeeEntity>>("Oritax.TaxSimp.Common.UMAFee.FeeEntty");
            _InvestmentManagementDetails = info.GetValue<List<InvestmentManagementEntity>>("Investments");
            _BussinessTitles = info.GetValue<Dictionary<string, string>>("Oritax.TaxSimp.CM.Organization.OrganizationCM.BussinessTitles");
            OrganizationalSettings = info.GetValue<OrganizationalSettings>("Oritax.TaxSimp.CM.Organization.OrganizationCM.OrganizationalSettings") ?? new OrganizationalSettings();
            _CashBalances = info.GetValue<List<CashBalanceEntity>>("Oritax.TaxSimp.CM.Organization.OrganizationCM.CashBalances");
            //Setting Default Values for _CashBalances 
            FillCashBalancesDefaultValues();
        }

        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValueX("Oritax.TaxSimp.CM.Organization.UMAForm.UMAFormEntity", _UMAFormEntityList);
            info.AddValueX("Oritax.TaxSimp.CM.Organization.Model.Organization.FormOrganization", _FormOrganizations);
            info.AddValueX("Oritax.TaxSimp.CM.Organization.OrganizationCM.Organization", _Organization);
            info.AddValueX("Oritax.TaxSimp.CM.Organization.OrganizationCM.StatusList", _StatusList);
            info.AddValueX("Oritax.TaxSimp.CM.Organization.OrganizationCM.SmtpSettings", _SmtpSettings);
            info.AddValueX("Oritax.TaxSimp.CM.Organization.OrganizationCM.Products", _Products);
            info.AddValueX("Oritax.TaxSimp.CM.Organization.OrganizationCM.Assets", _Assets);
            info.AddValueX("Oritax.TaxSimp.CM.Organization.OrganizationCM.BusinessGroups", _BusinessGroups);
            info.AddValueX("Oritax.TaxSimp.CM.Organization.OrganizationCM.Model", _Model);
            info.AddValueX("Oritax.TaxSimp.CM.Organization.OrganizationCM.Institution", _Institution);
            info.AddValueX("Oritax.TaxSimp.CM.Organization.OrganizationCM.MaxProgramCode", _maxProgramCode);
            info.AddValueX("Oritax.TaxSimp.CM.Organization.OrganizationCM.Securities", _Securities);
            info.AddValueX("Oritax.TaxSimp.CM.Organization.OrganizationCM.ManualAsset", _ManualAsset);
            info.AddValueX("Oritax.TaxSimp.CM.Organization.OrganizationCM.ProductSecurities", _ProductSecurities);
            info.AddValueX("Oritax.TaxSimp.Common.UMAFee.FeeEntty", _FeeEnttyList);
            info.AddValueX("Investments", _InvestmentManagementDetails);
            info.AddValueX("Oritax.TaxSimp.CM.Organization.OrganizationCM.BussinessTitles", _BussinessTitles);
            info.AddValueX("Oritax.TaxSimp.CM.Organization.OrganizationCM.OrganizationalSettings", OrganizationalSettings);
            info.AddValueX("Oritax.TaxSimp.CM.Organization.OrganizationCM.CashBalances", _CashBalances);
        }

        public string OrganisationName
        {
            get { return _Organization.OrganisationName; }
            set { _Organization.OrganisationName = value; }
        }

        public void Export(XElement root)
        {
            XElement element = new XElement("Organization");
            element.Add(_Organization.ToXElement("OrganizationValue"));
            element.Add(_StatusList.ToXElement("StatusList"));
            element.Add(_SmtpSettings.ToXElement("SmtpSettings"));
            root.Add(element);
        }

        public void Import(XElement root, IdentityCmMap map)
        {
            _Organization.FromXElement(root, "OrganizationValue");
            _StatusList.FromXElement(root, "StatusList");
            _SmtpSettings.FromXElement(root, "SmtpSettings");
        }

        private bool IsCorporateUMAForm(string type)
        {
            bool isCorporateType = true;
            switch (type)
            {
                case "Individual":
                case "Trust - Joint Trustee":
                case "Joint":
                case "Trust - Individual Trustee":
                case "Self Managed Super Fund (SMSF) - Individual Trustee":
                case "Self Managed Super Fund (SMSF) - Joint Trustee":
                    {
                        isCorporateType = false;
                        break;
                    }
                case "Company - Public":
                case "Company - Private":
                case "Trust - Corporate Trustee":
                case "Self Managed Super Fund (SMSF) - Corporate Trustee":
                    break;
                default:
                    break;
            }

            return isCorporateType;
        }

        private Guid TypeIDUMAForm(string type)
        {
            Guid typeid = Guid.Empty;

            switch (type)
            {
                case "Individual":
                    {
                        typeid = new Guid("2A2A95AC-7933-40A6-B6BA-1597CB18014F");
                        break;
                    }
                case "Joint":
                    {
                        typeid = new Guid("2A2A95AC-7933-40A6-B6BA-1597CB18014F");
                        break;
                    }
                case "Trust - Joint Trustee":
                    {
                        typeid = new Guid("6660CEAD-AE7C-485E-AF8E-622FA54F7AF2");
                        break;
                    }
                case "Trust - Individual Trustee":
                    {
                        typeid = new Guid("6660CEAD-AE7C-485E-AF8E-622FA54F7AF2");
                        break;
                    }
                case "Self Managed Super Fund (SMSF) - Individual Trustee":
                case "Self Managed Super Fund (SMSF) - Joint Trustee":
                    {
                        typeid = new Guid("7485432C-A44B-4E8B-AD73-9778E071DE2D");
                        break;
                    }
                case "Company - Public":
                    {
                        typeid = new Guid("96C93D8F-9D3D-4989-89CF-CFF47D377B43");
                        break;
                    }
                case "Company - Private":
                    {
                        typeid = new Guid("B95E5A7B-8C82-4C05-934D-F6AB7D95AE41");
                        break;
                    }
                case "Trust - Corporate Trustee":
                    {
                        typeid = new Guid("815051A3-D82D-41B6-8536-147D8A2055D2");
                        break;
                    }
                case "Self Managed Super Fund (SMSF) - Corporate Trustee":
                    {
                        typeid = new Guid("8256F2AA-3CCC-43B3-A683-90C8B603934A");
                        break;
                    }
                default:
                    break;
            }

            return typeid;
        }

        private void UpdateProductTaskDescription(ProductEntity product)
        {

            var produ = Products.Where(prod => prod.ID == product.ID).FirstOrDefault();
            if (produ != null)
            {
                product.ProductSecuritiesId = produ.ProductSecuritiesId;
                product.IsDefaultProductSecurity = produ.IsDefaultProductSecurity;
            }


            string taskDescription = string.Empty;
            string organizationUnit = string.Empty;
            string accountName = string.Empty;
            string fundCode = string.Empty;

            switch (product.EntityType)
            {
                case OrganizationType.BankAccount:
                    var bankInstitute = Institution.Where(e => e.ID == product.EntityId.Clid).FirstOrDefault();
                    taskDescription = string.Format("{0} - {1} - {2}", product.EntityType.GetDescription(), bankInstitute.Name, product.BankAccountType.ToUpper());
                    break;
                case OrganizationType.DesktopBrokerAccount:
                    var brokerInstitute = Institution.Where(e => e.ID == product.EntityId.Clid).FirstOrDefault();
                    taskDescription = string.Format("{0} - {1}", product.EntityType.GetDescription(), brokerInstitute.Name);
                    break;

                case OrganizationType.TermDepositAccount:

                    Oritax.TaxSimp.Common.TermDepositAccountEntity termDepositAccount = null;
                    IBrokerManagedComponent bmc = Broker.GetCMImplementation(product.EntityId.Clid, product.EntityId.Csid);
                    if (bmc != null)
                    {
                        termDepositAccount = (bmc as OrganizationUnitCM).GetDataStream(0, string.Empty).ToNewOrData<Oritax.TaxSimp.Common.TermDepositAccountEntity>();
                        organizationUnit = (bmc as OrganizationUnitCM).Name;
                    }

                    if (product.IncludedAccounts != null && product.IncludedAccounts.Count > 0)
                    {
                        bmc = Broker.GetCMImplementation(product.IncludedAccounts[0].Clid, product.IncludedAccounts[0].Csid);
                        if (bmc != null)
                        {
                            accountName = (bmc as OrganizationUnitCM).Name;
                        }
                    }

                    //taskDescription = string.Format("{0} - {1} - {2}", product.EntityType.GetDescription(), organizationUnit, accountName);

                    if (organizationUnit == null || organizationUnit == "")
                    {
                        organizationUnit = "Default";
                    }
                    taskDescription = string.Format("{0} - {1}", product.EntityType.GetDescription(), organizationUnit);

                    break;

                case OrganizationType.ManagedInvestmentSchemesAccount:

                    Guid fundId = new Guid();
                    if (product.FundAccounts != null && product.FundAccounts.Count > 0)
                        fundId = product.FundAccounts[0];
                    bmc = Broker.GetCMImplementation(product.EntityId.Clid, product.EntityId.Csid);
                    var miaAccount = (bmc as OrganizationUnitCM).GetDataStream(0, string.Empty).ToNewOrData<ManagedInvestmentSchemesAccountEntity>();
                    var fund = miaAccount.FundAccounts.FirstOrDefault(e => e.ID.ToString() == fundId.ToString());
                    if (fund != null)
                        fundCode = fund.Code + " " + fund.Description;
                    taskDescription = string.Format("{0} - {1} - {2}", product.EntityType.GetDescription(), miaAccount.Name, fundCode);
                    break;
            }

            product.TaskDescription = taskDescription;

        }

        protected override BrokerManagedComponent.ModifiedState OnSetData(DataSet data)
        {
            base.OnSetData(data);

            if (data is UMAFeesDS)
            {
                UMAFeesDS umaFeesDS = (UMAFeesDS)data;
                if (umaFeesDS.DataSetOperationType == DataSetOperationType.NewSingle)
                    this._FeeEnttyList.Add(umaFeesDS.FeeEntityToAddUpdate);
                if (umaFeesDS.DataSetOperationType == DataSetOperationType.UpdateSingle)
                {
                    DataTable feeTable = umaFeesDS.Tables[UMAFeesDS.FEELISTABLE];
                    DataRow feeRow = feeTable.Rows[0];

                    var feeEntity = _FeeEnttyList.Where(fee => fee.ID == (Guid)feeRow[UMAFeesDS.ID]).FirstOrDefault();

                    if (feeEntity != null)
                    {
                        feeEntity.ServiceTypes = (ServiceTypes)feeRow[UMAFeesDS.SERVICESTYPEENUM];
                        feeEntity.StartDate = (DateTime)feeRow[UMAFeesDS.STARTDATE];
                        feeEntity.EndDate = (DateTime)feeRow[UMAFeesDS.ENDDATE];
                        feeEntity.Description = (String)feeRow[UMAFeesDS.DESCRIPTION];
                        feeEntity.Comments = (String)feeRow[UMAFeesDS.COMMENTS];
                        feeEntity.FeeType = (FeeType)feeRow[UMAFeesDS.FEETYPEENUM];

                        foreach (FeeTieredRange item in feeEntity.FeeTieredRangeList)
                        {
                            item.StartDate = feeEntity.StartDate;
                            item.EndDate = feeEntity.EndDate;
                        }

                        foreach (OngoingFeePercent item in feeEntity.OngoingFeePercentList)
                        {
                            item.StartDate = feeEntity.StartDate;
                            item.EndDate = feeEntity.EndDate;
                        }

                        foreach (OngoingFeeValue item in feeEntity.OngoingFeeValueList)
                        {
                            item.StartDate = feeEntity.StartDate;
                            item.EndDate = feeEntity.EndDate;
                        }
                    }
                }
                if (umaFeesDS.DataSetOperationType == DataSetOperationType.DeletSingle)
                {
                    var feeEntity = _FeeEnttyList.Where(fee => fee.ID == umaFeesDS.FeeEntityToAddUpdate.ID).FirstOrDefault();

                    OrganisationListingDS organisationListingDS = new OrganisationListingDS();
                    IBrokerManagedComponent user = this.Broker.GetBMCInstance(this.Broker.UserContext.Identity.Name, "DBUser_1_1");
                    object[] args = new Object[3] { this.CLID, user.CID, EnableSecurity.SecuritySetting };
                    DataView view = new DataView(this.Broker.GetBMCDetailList(OrganizationInstall.ASSEMBLY_NAME, args).Tables[TableName]);
                    view.RowFilter = organisationListingDS.GetRowFilter();
                    view.Sort = "ENTITYNAME_FIELD ASC";
                    DataTable orgFiteredTable = view.ToTable();

                    foreach (DataRow row in orgFiteredTable.Rows)
                    {
                        Guid ciid = new Guid(row["ENTITYCIID_FIELD"].ToString());

                        IOrganizationUnit organizationUnit = this.Broker.GetBMCInstance(ciid) as IOrganizationUnit;

                        if (organizationUnit.IsInvestableClient)
                        {
                            OrgUnitConfiguredFeesDS orgUnitConfiguredFeesDS = new OrgUnitConfiguredFeesDS();
                            orgUnitConfiguredFeesDS.FeeEntityToConfigure = feeEntity.ID;
                            orgUnitConfiguredFeesDS.DataSetOperationType = DataSetOperationType.DeletSingle;
                            organizationUnit.SetData(orgUnitConfiguredFeesDS);
                        }

                        Broker.ReleaseBrokerManagedComponent(organizationUnit);
                    }


                    if (feeEntity != null)
                        _FeeEnttyList.Remove(feeEntity);
                }

                if (umaFeesDS.DataSetOperationType == DataSetOperationType.PushUpdate)
                {

                    var feeEntity = _FeeEnttyList.Where(fee => fee.ID == umaFeesDS.FeeEntityToAddUpdate.ID).FirstOrDefault();
                    OrganisationListingDS organisationListingDS = new OrganisationListingDS();
                    IBrokerManagedComponent user = this.Broker.GetBMCInstance(this.Broker.UserContext.Identity.Name, "DBUser_1_1");
                    object[] args = new Object[3] { this.CLID, user.CID, EnableSecurity.SecuritySetting };
                    DataView view = new DataView(this.Broker.GetBMCDetailList(OrganizationInstall.ASSEMBLY_NAME, args).Tables[TableName]);
                    view.RowFilter = organisationListingDS.GetRowFilter();
                    view.Sort = "ENTITYNAME_FIELD ASC";
                    DataTable orgFiteredTable = view.ToTable();

                    foreach (DataRow row in orgFiteredTable.Rows)
                    {
                        Guid ciid = new Guid(row["ENTITYCIID_FIELD"].ToString());

                        IOrganizationUnit organizationUnit = this.Broker.GetBMCInstance(ciid) as IOrganizationUnit;

                        if (organizationUnit.IsInvestableClient)
                        {
                            OrgUnitConfiguredFeesDS orgUnitConfiguredFeesDS = new OrgUnitConfiguredFeesDS();
                            orgUnitConfiguredFeesDS.DataSetOperationType = DataSetOperationType.UpdateSingle;
                            orgUnitConfiguredFeesDS.FeeEntityToConfigure = feeEntity.ID;
                            organizationUnit.SetData(orgUnitConfiguredFeesDS);
                        }

                        Broker.ReleaseBrokerManagedComponent(organizationUnit);
                    }
                }
            }
            else if (data is UMAFeesDetailsDS)
            {
                UMAFeesDetailsDS umaFeesDetailsDS = (UMAFeesDetailsDS)data;

                if (umaFeesDetailsDS.DataSetOperationType == DataSetOperationType.UpdateSingle)
                {
                    var feeEntity = this._FeeEnttyList.Where(feeEnt => feeEnt.ID == umaFeesDetailsDS.FeeEntity.ID).FirstOrDefault();
                    if (feeEntity != null)
                    {
                        _FeeEnttyList.Remove(feeEntity);
                        _FeeEnttyList.Add(umaFeesDetailsDS.FeeEntity);
                    }
                }

                if (umaFeesDetailsDS.FeeOperationType == FeeOperationType.NewProduct)
                {
                    var feeEntity = this._FeeEnttyList.Where(feeEnt => feeEnt.ID == umaFeesDetailsDS.FeeEntityID).FirstOrDefault();
                    if (feeEntity != null)
                    {
                        DataTable proTable = umaFeesDetailsDS.Tables[UMAFeesDetailsDS.PRODUCTSTABLENAME];
                        foreach (DataRow row in proTable.Rows)
                        {
                            if (feeEntity.ProductList == null)
                                feeEntity.ProductList = new List<Guid>();

                            if (!feeEntity.ProductList.Contains((Guid)row[UMAFeesDetailsDS.PROID]))
                                feeEntity.ProductList.Add((Guid)row[UMAFeesDetailsDS.PROID]);
                        }
                    }
                }

                if (umaFeesDetailsDS.FeeOperationType == FeeOperationType.DeleteProduct)
                {
                    var feeEntity = this._FeeEnttyList.Where(feeEnt => feeEnt.ID == umaFeesDetailsDS.FeeEntityID).FirstOrDefault();
                    if (feeEntity != null)
                    {

                        if (feeEntity.ProductList == null)
                            feeEntity.ProductList = new List<Guid>();

                        if (feeEntity.ProductList.Contains(umaFeesDetailsDS.ProductID))
                            feeEntity.ProductList.Remove(umaFeesDetailsDS.ProductID);
                    }
                }
            }
            else if (data is UMAFormDS)
            {
                UMAFormDS UMAFormDS = (UMAFormDS)data;
                if (UMAFormDS.UMAFormDSOperation == UMAFormDSOperation.Add)
                {
                    var oldformObject = this._UMAFormEntityList.Where(forglist => forglist.accountNO == UMAFormDS.UMAFormEntity.accountNO).FirstOrDefault();
                    if (oldformObject != null)
                    {
                        UMAFormDS.UMAFormEntity.ClientCID = oldformObject.ClientCID;
                        UMAFormDS.UMAFormEntity.ClientID = oldformObject.ClientID;

                        foreach (SignatoryEntity sigObj in oldformObject.SignatoryEntityList)
                        {
                            var newSigObject = UMAFormDS.UMAFormEntity.SignatoryEntityList.Where(sig => sig.SigNo == sigObj.SigNo).FirstOrDefault();

                            if (newSigObject != null)
                                newSigObject.IndividualInstanceID = sigObj.IndividualInstanceID;
                        }
                    }

                    if (oldformObject != null)
                        this._UMAFormEntityList.Remove(oldformObject);

                    this._UMAFormEntityList.Add(UMAFormDS.UMAFormEntity);
                }
                else if (UMAFormDS.UMAFormDSOperation == UMAFormDSOperation.Delete)
                {
                    var oldformObject = this._UMAFormEntityList.Where(forglist => forglist.accountNO.ToLower() == UMAFormDS.AppNo.ToLower()).FirstOrDefault();

                    if (oldformObject != null)
                        this._UMAFormEntityList.Remove(oldformObject);
                }
                else if (UMAFormDS.UMAFormDSOperation == UMAFormDSOperation.CreateClient)
                {
                    var formEntiy = this._UMAFormEntityList.Where(forglist => forglist.accountNO.ToLower() == UMAFormDS.AppNo.ToLower()).FirstOrDefault();

                    if (IsCorporateUMAForm(formEntiy.accountType))
                        CreateCorporate(formEntiy);
                    else
                        CreateIndividual(formEntiy);
                }
                else if (UMAFormDS.UMAFormDSOperation == UMAFormDSOperation.AssociateAllClients)
                {
                    IBrokerManagedComponent user = this.Broker.GetBMCInstance("Administrator", "DBUser_1_1");
                    object[] args = new Object[3] { this.CLID, user.CID, EnableSecurity.SecuritySetting };
                    DataView view = new DataView(this.Broker.GetBMCDetailList(OrganizationInstall.ASSEMBLY_NAME, args).Tables[TableName]);
                    view.RowFilter = OrganisationListingDS.GetRowFilter("");
                    view.Sort = "ENTITYNAME_FIELD ASC";
                    DataTable orgFiteredTable = view.ToTable();

                    foreach (var formEntiy in this._UMAFormEntityList)
                    {
                        DataView entityView = new DataView(orgFiteredTable);
                        entityView.RowFilter = "ENTITYNAME_FIELD='" + formEntiy.accountName + "'";
                        DataTable entityTable = entityView.ToTable();
                        if (entityTable.Rows.Count > 0)
                        {
                            Guid cid = new Guid(entityTable.Rows[0]["ENTITYCIID_FIELD"].ToString());
                            IOrganizationUnit iorg = this.Broker.GetBMCInstance(cid) as IOrganizationUnit;
                            formEntiy.ClientCID = iorg.CID;
                            formEntiy.ClientID = iorg.ClientId;
                            formEntiy.UMAFormStatus = UMAFormStatus.Finalised;
                            this.Broker.ReleaseBrokerManagedComponent(iorg);
                        }
                        else
                        {
                            formEntiy.ClientCID = Guid.Empty;
                            formEntiy.ClientID = String.Empty;
                        }

                    }
                }

                else if (UMAFormDS.UMAFormDSOperation == UMAFormDSOperation.UpdateKeyClientRecords)
                {
                    foreach (var formEntiy in this._UMAFormEntityList)
                    {
                        List<Oritax.TaxSimp.Common.IdentityCMDetail> signatories = new List<Oritax.TaxSimp.Common.IdentityCMDetail>();

                        if (formEntiy.ClientCID != Guid.Empty)
                        {
                            IOrganizationUnit iorg = this.Broker.GetBMCInstance(formEntiy.ClientCID) as IOrganizationUnit;
                            if (iorg != null)
                            {
                                if (iorg.ClientEntity is CorporateEntity)
                                {
                                    CorporateEntity corporateEntity = iorg.ClientEntity as CorporateEntity;
                                    corporateEntity.TFN = formEntiy.TFNTrustFund.RemoveWhiteSpacesStartEndInBetween();

                                    if (formEntiy.TFNTrustFund.RemoveWhiteSpacesStartEndInBetween() == string.Empty)
                                        corporateEntity.TFN = formEntiy.TFNSig1.RemoveWhiteSpacesStartEndInBetween();

                                    signatories = corporateEntity.Signatories;
                                    if (corporateEntity.IsSMSFTrust || corporateEntity.IsOtherTrust)
                                        corporateEntity.LegalName = formEntiy.trustName + " ATF " + formEntiy.accountName;
                                }

                                else if (iorg.ClientEntity is ClientIndividualEntity)
                                {
                                    ClientIndividualEntity clientIndividualEntity = iorg.ClientEntity as ClientIndividualEntity;
                                    signatories = clientIndividualEntity.Applicants;
                                    if (clientIndividualEntity.IsSMSFTrust || clientIndividualEntity.IsOtherTrust)
                                    {
                                        clientIndividualEntity.TFN = formEntiy.TFNTrustFund;
                                        clientIndividualEntity.LegalName = formEntiy.trustName + " ATF " + formEntiy.accountName;
                                    }
                                }

                                foreach (SignatoryEntity sigEnt in formEntiy.SignatoryEntityList)
                                {
                                    IOrganizationUnit indOrg = Broker.GetBMCInstance(sigEnt.IndividualInstanceID) as IOrganizationUnit;
                                    if (indOrg == null)
                                    {
                                        foreach (Oritax.TaxSimp.Common.IdentityCMDetail identityCM in signatories)
                                        {
                                            IOrganizationUnit indOrgIndv = Broker.GetCMImplementation(identityCM.Clid, identityCM.Csid) as IOrganizationUnit;
                                            if (indOrgIndv == null)
                                                indOrgIndv = Broker.GetBMCInstance(identityCM.Cid) as IOrganizationUnit;

                                            if (indOrgIndv != null)
                                            {
                                                Oritax.TaxSimp.Common.IndividualEntity individualEntityToChk = indOrgIndv.ClientEntity as Oritax.TaxSimp.Common.IndividualEntity;
                                                if (sigEnt.GivenName == individualEntityToChk.Name && sigEnt.FamilyName == individualEntityToChk.Surname)
                                                    indOrg = indOrgIndv;
                                            }
                                        }
                                    }
                                    if (indOrg != null)
                                    {
                                        Oritax.TaxSimp.Common.IdentityCMDetail primaryIndividual = signatories.Where(identityCM => identityCM.Clid == indOrg.CLID && identityCM.Csid == indOrg.CSID).FirstOrDefault();
                                        if (primaryIndividual == null)
                                            primaryIndividual = signatories.Where(identityCM => identityCM.Cid == indOrg.CID).FirstOrDefault();

                                        Oritax.TaxSimp.Common.IndividualEntity individualEntity = indOrg.ClientEntity as Oritax.TaxSimp.Common.IndividualEntity;


                                        UpdateIndividualEntity(sigEnt, indOrg, formEntiy);

                                        if (sigEnt.SigNo == 1)
                                        {
                                            if (primaryIndividual != null)
                                                primaryIndividual.IsPrimary = true;

                                            individualEntity.TFN = formEntiy.TFNSig1.RemoveWhiteSpacesStartEndInBetween();
                                        }
                                        else if (sigEnt.SigNo == 2)
                                            individualEntity.TFN = formEntiy.TFNSig2.RemoveWhiteSpacesStartEndInBetween();
                                        else if (sigEnt.SigNo == 3)
                                            individualEntity.TFN = formEntiy.TFNSig3.RemoveWhiteSpacesStartEndInBetween();
                                        else if (sigEnt.SigNo == 4)
                                            individualEntity.TFN = formEntiy.TFNSig4.RemoveWhiteSpacesStartEndInBetween();

                                        indOrg.Modified = true;
                                    }
                                }

                                iorg.Modified = true;
                            }
                        }
                    }
                }

                return ModifiedState.MODIFIED;
            }
            else if (data is ModelsDS)
            {
                ModelsDS modelsDS = (ModelsDS)data;

                if (modelsDS.ModelDataSetOperationType == ModelDataSetOperationType.BulkUpdate)
                {
                    foreach (ModelEntity modelEntity in this.Model)
                    {
                        ObservableCollection<AssetEntity> copiedAssets = new ObservableCollection<AssetEntity>();

                        foreach (AssetEntity assEntity in modelEntity.Assets)
                        {
                            var copiedAssEntity = ObjectCopier.Clone(assEntity);
                            copiedAssEntity.Products.Clear();
                            copiedAssEntity.ParentId = modelEntity.ID;

                            var orgAsset = this._Assets.Where(c => c.ID == copiedAssEntity.ID).FirstOrDefault();
                            if (orgAsset != null)
                            {
                                copiedAssEntity.Name = orgAsset.Name;
                                copiedAssEntity.Description = orgAsset.Description;
                            }

                            foreach (ProductEntity proEntity in assEntity.Products)
                            {
                                var copiedProduct = ObjectCopier.Clone(proEntity);
                                var orgPro = this._Products.Where(c => c.ID == copiedProduct.ID).FirstOrDefault();
                                if (orgPro != null)
                                {
                                    copiedProduct.Name = orgPro.Name;
                                    copiedProduct.Description = orgPro.Description;
                                    copiedProduct.ProductSecuritiesId = orgPro.ProductSecuritiesId;
                                    copiedProduct.IsDefaultProductSecurity = orgPro.IsDefaultProductSecurity;
                                }

                                copiedProduct.ParentId = copiedAssEntity.ID;
                                copiedAssEntity.Products.Add(copiedProduct);
                            }

                            copiedAssets.Add(copiedAssEntity);
                        }

                        modelEntity.Assets = copiedAssets;
                        modelEntity.CalculateAllocation();
                    }
                }

                if (modelsDS.ModelDataSetOperationType == ModelDataSetOperationType.NewAsset)
                {
                    var assetTemplate = this.Assets.Where(ass => ass.ID == modelsDS.AssetEntityID).FirstOrDefault();
                    var modelObject = this.Model.Where(mod => mod.ID == modelsDS.ModelEntityID).FirstOrDefault();
                    var existingAssets = modelObject.Assets.Where(ass => ass.ID == modelsDS.AssetEntityID).FirstOrDefault();

                    if (existingAssets == null)
                    {
                        AssetEntity assetEntity = ObjectCopier.Clone(assetTemplate);
                        assetEntity.ParentId = modelObject.ID;
                        assetEntity.Products.Clear();
                        modelObject.Assets.Add(assetEntity);
                        modelObject.CalculateAllocation();
                    }
                }

                else if (modelsDS.ModelDataSetOperationType == ModelDataSetOperationType.NewModel)
                {
                    foreach (DataRow row in modelsDS.Tables[ModelsDS.MODELTABLE].Rows)
                    {
                        ModelEntity entity = new ModelEntity();
                        var modelObject = this.Model.OrderByDescending(mod => mod.ProgramCode).FirstOrDefault();

                        int programcode = int.Parse(modelObject.ProgramCode);
                        int newCode = ++programcode;
                        entity.ProgramCode = newCode.ToString().PadLeft(4, '0');
                        entity.ServiceType = (ServiceTypes)row[ModelsDS.SERVICETYPE];
                        entity.Name = (string)row[ModelsDS.MODELNAME];
                        entity.IsPrivate = (bool)row[ModelsDS.ISPRIVATE];

                        this.Model.Add(entity);
                    }
                }
                else if (modelsDS.ModelDataSetOperationType == ModelDataSetOperationType.UpdateModel)
                {
                    foreach (DataRow row in modelsDS.Tables[ModelsDS.MODELTABLE].Rows)
                    {
                        Guid id = (Guid)row[ModelsDS.ID];

                        ModelEntity entity = this.Model.Where(mod => mod.ID == id).FirstOrDefault();

                        entity.ServiceType = (ServiceTypes)row[ModelsDS.SERVICETYPE];
                        entity.Name = (string)row[ModelsDS.MODELNAME];
                        entity.IsPrivate = (bool)row[ModelsDS.ISPRIVATE];
                    }
                }
                else if (modelsDS.ModelDataSetOperationType == ModelDataSetOperationType.DeleteModel)
                {
                    var modelObject = this.Model.Where(mod => mod.ID == modelsDS.ModelEntityID).FirstOrDefault();
                    if (modelObject != null)
                    {
                        this.Model.Remove(modelObject);
                    }
                }
                else if (modelsDS.ModelDataSetOperationType == ModelDataSetOperationType.NewProduct)
                {
                    var productTemplate = this.Products.Where(pro => pro.ID == modelsDS.ProductEntityID).FirstOrDefault();
                    var modelObject = this.Model.Where(mod => mod.ID == modelsDS.ModelEntityID).FirstOrDefault();
                    var existingAssets = modelObject.Assets.Where(ass => ass.ID == modelsDS.AssetEntityID).FirstOrDefault();
                    var existingProduct = existingAssets.Products.Where(pro => pro.ID == modelsDS.ProductEntityID).FirstOrDefault();

                    if (existingProduct == null)
                    {
                        ProductEntity productEntity = ObjectCopier.Clone(productTemplate);
                        productEntity.ParentId = existingAssets.ID;
                        productEntity.DynamicPercentage = modelsDS.ProDynamicPercentage;
                        productEntity.SharePercentage = modelsDS.ProNeutralPercentage;
                        productEntity.MinAllocation = modelsDS.ProMinAllocation;
                        productEntity.MaxAllocation = modelsDS.ProMaxAllocation;

                        existingAssets.Products.Add(productEntity);
                        modelObject.CalculateAllocation();
                    }
                }


                else if (modelsDS.ModelDataSetOperationType == ModelDataSetOperationType.UpdateProduct)
                {
                    var prodTemplate = this.Products.Where(pro => pro.ID == modelsDS.ProductEntityID).FirstOrDefault();
                    var modelObject = this.Model.Where(mod => mod.ID == modelsDS.ModelEntityID).FirstOrDefault();
                    var existingAssets = modelObject.Assets.Where(ass => ass.ID == modelsDS.AssetEntityID).FirstOrDefault();
                    var existingProduct = existingAssets.Products.Where(pro => pro.ID == modelsDS.ProductEntityID).FirstOrDefault();

                    if (existingProduct != null)
                    {
                        existingProduct.DynamicPercentage = modelsDS.ProDynamicPercentage;
                        existingProduct.SharePercentage = modelsDS.ProNeutralPercentage;
                        existingProduct.MinAllocation = modelsDS.ProMinAllocation;
                        existingProduct.MaxAllocation = modelsDS.ProMaxAllocation;
                        existingProduct.Description = prodTemplate.Description;
                        existingProduct.Name = prodTemplate.Name;

                        modelObject.CalculateAllocation();
                    }
                }

                else if (modelsDS.ModelDataSetOperationType == ModelDataSetOperationType.ReCalculationAllocation)
                {
                    foreach (var model in this.Model)
                    {
                        model.CalculateAllocation();
                    }
                }

                else if (modelsDS.ModelDataSetOperationType == ModelDataSetOperationType.DeleteProduct)
                {
                    var newAsset = this.Assets.Where(ass => ass.ID == modelsDS.AssetEntityID).FirstOrDefault();
                    var newProduct = this.Products.Where(pro => pro.ID == modelsDS.ProductEntityID).FirstOrDefault();
                    var modelObject = this.Model.Where(mod => mod.ID == modelsDS.ModelEntityID).FirstOrDefault();
                    var existingAssets = modelObject.Assets.Where(ass => ass.ID == modelsDS.AssetEntityID).FirstOrDefault();
                    var existingProduct = existingAssets.Products.Where(pro => pro.ID == modelsDS.ProductEntityID).FirstOrDefault();

                    if (existingProduct != null)
                    {
                        existingAssets.Products.Remove(existingProduct);
                        modelObject.CalculateAllocation();
                    }
                }

                else if (modelsDS.ModelDataSetOperationType == ModelDataSetOperationType.DeleteAsset)
                {
                    var modelObject = this.Model.Where(mod => mod.ID == modelsDS.ModelEntityID).FirstOrDefault();
                    var existingAssets = modelObject.Assets.Where(ass => ass.ID == modelsDS.AssetEntityID).FirstOrDefault();

                    if (existingAssets != null)
                    {
                        modelObject.Assets.Remove(existingAssets);
                        modelObject.CalculateAllocation();
                    }
                }
            }
            else if (data is SecurityDistributionsDetailsDS)
            {
                var DS = data as SecurityDistributionsDetailsDS;
                if (DS.CommandType == DatasetCommandTypes.ProcessAll)
                {
                    this.OnAddDataStream((int)OrganizationCommandType.ProcessAllSecurityDistributionsCommand, DS.SecID == Guid.Empty ? string.Empty : DS.SecID.ToString());
                }
                if (DS.CommandType == DatasetCommandTypes.RemoveInvalid)
                {
                    this.OnAddDataStream((int)OrganizationCommandType.RemoveAllInvalidDistributionsCommand, string.Empty);
                }
                else
                {
                    var security = this.Securities.Where(details => details.ID == DS.SecID).FirstOrDefault();
                    if (security != null)
                    {
                        if (security.DistributionCollection == null)
                            security.DistributionCollection = new ObservableCollection<DistributionEntity>();
                        var distributionentity = security.DistributionCollection.Where(ss => ss.ID == DS.ID).FirstOrDefault();

                        if (distributionentity == null && DS.CommandType == DatasetCommandTypes.Add)
                        {
                            DS.Entity.ID = DS.ID;
                            security.DistributionCollection.Add(DS.Entity);

                        }
                        else if (DS.CommandType == DatasetCommandTypes.Update)
                        {
                            security.DistributionCollection.Remove(distributionentity);
                            security.DistributionCollection.Add(DS.Entity);
                        }
                        else if (DS.CommandType == DatasetCommandTypes.Delete)
                        {
                            if (!distributionentity.ProcessFlag)
                                security.DistributionCollection.Remove(distributionentity);
                        }
                        else if (DS.CommandType == DatasetCommandTypes.Process)
                        {
                            List<Guid> ids = new List<Guid>();
                            ids.Add(DS.SecID);
                            ids.Add(DS.ID);
                            this.OnAddDataStream((int)OrganizationCommandType.ProcessSecuritiesDistributionstWeb, ids.ToXmlString());
                        }
                    }
                }
            }
            else if (data is SecurityDividendDetailsDS)
            {
                var DS = data as SecurityDividendDetailsDS;

                if (DS.CommandType == DatasetCommandTypes.ProcessAll)
                {

                    this.OnAddDataStream((int)OrganizationCommandType.ProcessAllSecurityDividendCommand, DS.SecID == Guid.Empty ? string.Empty : DS.SecID.ToString());
                }
                else
                {

                    var security = this.Securities.Where(details => details.ID == DS.SecID).FirstOrDefault();
                    if (security != null)
                    {
                        if (security.DividendCollection == null)
                            security.DividendCollection = new ObservableCollection<DividendEntity>();
                        var dividendEntity = security.DividendCollection.Where(ss => ss.ID == DS.ID).FirstOrDefault();

                        if (dividendEntity == null && DS.CommandType == DatasetCommandTypes.Add)
                        {
                            DS.Entity.ID = DS.ID;
                            security.DividendCollection.Add(DS.Entity);

                        }
                        else if (DS.CommandType == DatasetCommandTypes.Update)
                        {
                            if (!dividendEntity.ProcessFlag)
                            {
                                security.DividendCollection.Remove(dividendEntity);
                                security.DividendCollection.Add(DS.Entity);
                            }
                            else
                            {
                                dividendEntity.CentsPerShare = DS.Entity.CentsPerShare;
                                dividendEntity.Investor = DS.Entity.Investor;
                                dividendEntity.AdministrationSystem = DS.Entity.AdministrationSystem;
                                dividendEntity.TransactionType = DS.Entity.TransactionType;
                                dividendEntity.InvestmentCode = DS.Entity.InvestmentCode;
                                dividendEntity.ExternalReferenceID = DS.Entity.ExternalReferenceID;
                                dividendEntity.RecordDate = DS.Entity.RecordDate;
                                dividendEntity.PaymentDate = DS.Entity.PaymentDate;
                                dividendEntity.UnitsOnHand = DS.Entity.UnitsOnHand;
                                dividendEntity.FrankedAmount = DS.Entity.FrankedAmount;
                                dividendEntity.UnfrankedAmount = DS.Entity.UnfrankedAmount;
                                dividendEntity.TaxFree = DS.Entity.TaxFree;
                                dividendEntity.TaxDeferred = DS.Entity.TaxDeferred;
                                dividendEntity.ReturnOfCapital = DS.Entity.ReturnOfCapital;
                                dividendEntity.DomesticInterestAmount = DS.Entity.DomesticInterestAmount;
                                dividendEntity.ForeignInterestAmount = DS.Entity.ForeignInterestAmount;
                                dividendEntity.DomesticOtherIncome = DS.Entity.DomesticOtherIncome;
                                dividendEntity.ForeignOtherIncome = DS.Entity.ForeignOtherIncome;
                                dividendEntity.DomesticWithHoldingTax = DS.Entity.DomesticWithHoldingTax;
                                dividendEntity.ForeignWithHoldingTax = DS.Entity.ForeignWithHoldingTax;
                                dividendEntity.DomesticImputationTaxCredits = DS.Entity.DomesticImputationTaxCredits;
                                dividendEntity.ForeignImputationTaxCredits = DS.Entity.ForeignImputationTaxCredits;
                                dividendEntity.ForeignInvestmentFunds = DS.Entity.ForeignInvestmentFunds;
                                dividendEntity.DomesticIndexationCGT = DS.Entity.DomesticIndexationCGT;
                                dividendEntity.ForeignIndexationCGT = DS.Entity.ForeignIndexationCGT;
                                dividendEntity.DomesticDiscountedCGT = DS.Entity.DomesticDiscountedCGT;
                                dividendEntity.ForeignDiscountedCGT = DS.Entity.ForeignDiscountedCGT;
                                dividendEntity.DomesticOtherCGT = DS.Entity.DomesticOtherCGT;
                                dividendEntity.ForeignOtherCGT = DS.Entity.ForeignOtherCGT;
                                dividendEntity.DomesticGrossAmount = DS.Entity.DomesticGrossAmount;
                                dividendEntity.PercentFrankedAmount = DS.Entity.PercentFrankedAmount;
                                dividendEntity.PercentUnfrankedAmount = DS.Entity.PercentUnfrankedAmount;
                                dividendEntity.PercentTaxFree = DS.Entity.PercentTaxFree;
                                dividendEntity.PercentTaxDeferred = DS.Entity.PercentTaxDeferred;
                                dividendEntity.PercentReturnOfCapital = DS.Entity.PercentReturnOfCapital;
                                dividendEntity.PercentDomesticInterestAmount = DS.Entity.PercentDomesticInterestAmount;
                                dividendEntity.PercentForeignInterestAmount = DS.Entity.PercentForeignInterestAmount;
                                dividendEntity.PercentDomesticOtherIncome = DS.Entity.PercentDomesticOtherIncome;
                                dividendEntity.PercentForeignOtherIncome = DS.Entity.PercentForeignOtherIncome;
                                dividendEntity.PercentDomesticWithHoldingTax = DS.Entity.PercentDomesticWithHoldingTax;
                                dividendEntity.PercentForeignWithHoldingTax = DS.Entity.PercentForeignWithHoldingTax;
                                dividendEntity.PercentDomesticImputationTaxCredits = DS.Entity.PercentDomesticImputationTaxCredits;
                                dividendEntity.PercentForeignImputationTaxCredits = DS.Entity.PercentForeignImputationTaxCredits;
                                dividendEntity.PercentForeignInvestmentFunds = DS.Entity.PercentForeignInvestmentFunds;
                                dividendEntity.PercentDomesticIndexationCGT = DS.Entity.PercentDomesticIndexationCGT;
                                dividendEntity.PercentForeignIndexationCGT = DS.Entity.PercentForeignIndexationCGT;
                                dividendEntity.PercentDomesticDiscountedCGT = DS.Entity.PercentDomesticDiscountedCGT;
                                dividendEntity.PercentForeignDiscountedCGT = DS.Entity.PercentForeignDiscountedCGT;
                                dividendEntity.PercentDomesticOtherCGT = DS.Entity.PercentDomesticOtherCGT;
                                dividendEntity.PercentForeignOtherCGT = DS.Entity.PercentForeignOtherCGT;
                                dividendEntity.PercentDomesticGrossAmount = DS.Entity.PercentDomesticGrossAmount;
                                dividendEntity.Comment = DS.Entity.Comment;
                                dividendEntity.Account = DS.Entity.Account;
                                dividendEntity.CashManagementTransactionID = DS.Entity.CashManagementTransactionID;
                                dividendEntity.InterestRate = DS.Entity.InterestRate;
                                dividendEntity.InvestmentAmount = DS.Entity.InvestmentAmount;
                                dividendEntity.InterestPaid = DS.Entity.InterestPaid;
                                if (DS.Entity.BalanceDate.HasValue)
                                    dividendEntity.BalanceDate = DS.Entity.BalanceDate;
                                if (DS.Entity.BooksCloseDate.HasValue)
                                    dividendEntity.BooksCloseDate = DS.Entity.BooksCloseDate;
                                dividendEntity.Dividendtype = DS.Entity.Dividendtype;
                                dividendEntity.Currency = DS.Entity.Currency;
                            }

                        }
                        else if (DS.CommandType == DatasetCommandTypes.Delete)
                        {
                            if (!dividendEntity.ProcessFlag)
                                security.DividendCollection.Remove(dividendEntity);
                        }
                        else if (DS.CommandType == DatasetCommandTypes.Process)
                        {
                            List<Guid> ids = new List<Guid>();
                            ids.Add(DS.SecID);
                            ids.Add(DS.ID);
                            this.OnAddDataStream((int)OrganizationCommandType.ProcessSecuritiesDividendstWeb, ids.ToXmlString());
                        }
                    }
                }

            }
            else if (data is SecurityDetailsDS)
            {
                SecurityDetailsDS securitiesDS = (SecurityDetailsDS)data;

                var security = this.Securities.Where(details => details.ID == securitiesDS.DetailsGUID).FirstOrDefault();


                if (securitiesDS.RequiredDataType == SecurityDetailTypes.HistoricalPrices)
                {
                    if (security.ASXSecurity == null)
                        security.ASXSecurity = new ObservableCollection<ASXSecurityEntity>();
                    var priceEntity = security.ASXSecurity.FirstOrDefault(ss => ss.ID == Guid.Parse(securitiesDS.Tables[securitiesDS.PriceTable.TB_Name].Rows[0][securitiesDS.PriceTable.ID].ToString()));
                    DataRow row = securitiesDS.Tables[securitiesDS.PriceTable.TB_Name].Rows[0];
                    if (securitiesDS.CommandType == DatasetCommandTypes.Add)
                    {
                        priceEntity = security.ASXSecurity.FirstOrDefault(ss => ss.Date == (DateTime)securitiesDS.Tables[securitiesDS.PriceTable.TB_Name].Rows[0][securitiesDS.PriceTable.DATE]);
                        if (priceEntity != null)
                        {
                            securitiesDS.ExtendedProperties.Add("Result", OperationResults.Failed);
                            securitiesDS.ExtendedProperties.Add("Message", "For mentioned date price already exists.");
                        }
                        else
                        {
                            ASXSecurityEntity asxSecurityEntity = new ASXSecurityEntity();
                            var id = Guid.NewGuid();
                            row[securitiesDS.PriceTable.ID] = asxSecurityEntity.ID = id;
                            if (row[securitiesDS.PriceTable.ADJUSTEDPRICE] != DBNull.Value)
                                asxSecurityEntity.AdjustedPrice = double.Parse(row[securitiesDS.PriceTable.ADJUSTEDPRICE].ToString());

                            if (row[securitiesDS.PriceTable.DATE] != DBNull.Value)
                                asxSecurityEntity.Date = (DateTime)row[securitiesDS.PriceTable.DATE];

                            if (row[securitiesDS.PriceTable.CURRENCY] != DBNull.Value)
                                asxSecurityEntity.Currency = (string)row[securitiesDS.PriceTable.CURRENCY];

                            if (row[securitiesDS.PriceTable.PRICENAV] != DBNull.Value)
                                asxSecurityEntity.PriceNAV = (double?)row[securitiesDS.PriceTable.PRICENAV];
                            if (row[securitiesDS.PriceTable.PRICEPUR] != DBNull.Value)
                                asxSecurityEntity.PricePUR = (double?)row[securitiesDS.PriceTable.PRICEPUR];
                            if (row[securitiesDS.PriceTable.UNITPRICE] != DBNull.Value)
                                asxSecurityEntity.UnitPrice = (double)row[securitiesDS.PriceTable.UNITPRICE];
                            if (row[securitiesDS.PriceTable.INTERESTRATE] != DBNull.Value)
                                asxSecurityEntity.InterestRate = (double)row[securitiesDS.PriceTable.INTERESTRATE];

                            security.ASXSecurity.Add(asxSecurityEntity);

                            securitiesDS.ExtendedProperties.Add("Result", OperationResults.Successfull);
                            securitiesDS.ExtendedProperties.Add("Message", "Price added.");

                        }
                    }
                    else if (securitiesDS.CommandType == DatasetCommandTypes.Update)
                    {
                        if (priceEntity == null)
                        {
                            securitiesDS.ExtendedProperties.Add("Result", OperationResults.Failed);
                            securitiesDS.ExtendedProperties.Add("Message", "Price doesn't exists.");
                        }
                        else
                        {
                            if (row[securitiesDS.PriceTable.ADJUSTEDPRICE] != DBNull.Value)
                                priceEntity.AdjustedPrice = double.Parse(row[securitiesDS.PriceTable.ADJUSTEDPRICE].ToString());

                            if (row[securitiesDS.PriceTable.DATE] != DBNull.Value)
                                priceEntity.Date = (DateTime)row[securitiesDS.PriceTable.DATE];

                            if (row[securitiesDS.PriceTable.CURRENCY] != DBNull.Value)
                                priceEntity.Currency = (string)row[securitiesDS.PriceTable.CURRENCY];

                            if (row[securitiesDS.PriceTable.PRICENAV] != DBNull.Value)
                                priceEntity.PriceNAV = (double?)row[securitiesDS.PriceTable.PRICENAV];
                            if (row[securitiesDS.PriceTable.PRICEPUR] != DBNull.Value)
                                priceEntity.PricePUR = (double?)row[securitiesDS.PriceTable.PRICEPUR];
                            if (row[securitiesDS.PriceTable.UNITPRICE] != DBNull.Value)
                                priceEntity.UnitPrice = (double)row[securitiesDS.PriceTable.UNITPRICE];
                            if (row[securitiesDS.PriceTable.INTERESTRATE] != DBNull.Value)
                                priceEntity.InterestRate = (double)row[securitiesDS.PriceTable.INTERESTRATE];
                            securitiesDS.ExtendedProperties.Add("Result", OperationResults.Successfull);
                            securitiesDS.ExtendedProperties.Add("Message", "Price updated.");
                        }

                    }
                    else if (securitiesDS.CommandType == DatasetCommandTypes.Delete)
                    {
                        if (priceEntity == null)
                        {
                            securitiesDS.ExtendedProperties.Add("Result", OperationResults.Failed);
                            securitiesDS.ExtendedProperties.Add("Message", "Price doesn't exists.");
                        }
                        else
                        {
                            security.ASXSecurity.Remove(priceEntity);
                            securitiesDS.ExtendedProperties.Add("Result", OperationResults.Successfull);
                            securitiesDS.ExtendedProperties.Add("Message", "Price deleted.");
                        }
                    }
                }
                else if (securitiesDS.RequiredDataType == SecurityDetailTypes.DividendPushDownReport)
                {
                    if (securitiesDS.CommandType == DatasetCommandTypes.Delete && security.DividendCollection != null)
                    {
                        foreach (var divEntity in security.DividendCollection)
                        {
                            if (divEntity.ReportCollection != null)
                            {
                                divEntity.ReportCollection.Clear();
                            }

                        }
                    }
                }

            }
            else if (data is InvestmentDS)
            {
                InvestmentDS ds = (InvestmentDS)data;
                DataTable table = ds.Tables[InvestmentDS.INVESTMENTMANAGEMENT];
                InvestmentManagementEntity entity = this._InvestmentManagementDetails.Where(p => p.ID == ds.InvestmentID).FirstOrDefault();

                if (ds.CommandType == DatasetCommandTypes.Add || ds.CommandType == DatasetCommandTypes.Update)
                {
                    if (entity == null)
                    {
                        entity = new InvestmentManagementEntity();
                        entity.ID = ds.InvestmentID;
                        this._InvestmentManagementDetails.Add(entity);
                    }
                    if (table.Rows.Count > 0)
                    {
                        InvestmentManagementEntity.PopulateInvestmentManagementFromDataRow(entity, table.Rows[0]);
                        InvestmentManagementEntity.PopulateManagersAllocationFromDataTable(entity, ds.Tables[InvestmentDS.ALLOCATIONS]);
                        ds.ExtendedProperties.Add("Result", OperationResults.Successfull);
                        ds.ExtendedProperties.Add("Message", "Investment management details saved");
                    }
                }
            }
            else if (data is OrganizationalSettingsDS)
            {
                var ds = (OrganizationalSettingsDS)data;
                var table = ds.OrganizationalSettingTable;


                if (ds.CommandType == DatasetCommandTypes.Update)
                {
                    if (OrganizationalSettings == null)
                    {
                        OrganizationalSettings = new OrganizationalSettings();
                    }
                    if (table.Rows.Count > 0)
                    {
                        OrganizationalSettings.SendToSMAService = (bool)table.Rows[0][table.SENDORDERSTOSMA];
                        OrganizationalSettings.ReadFromSMAService = (bool)table.Rows[0][table.READFROMSMA];
                        ds.ExtendedProperties.Add("Result", OperationResults.Successfull);
                        ds.ExtendedProperties.Add("Message", "Organizational settings details saved");
                    }
                }
            }
            #region Product
            else if (data is ProductsDS)
            {
                ProductsDS productsDS = (ProductsDS)data;

                if (productsDS.CommandType == DatasetCommandTypes.Add)
                {
                    ProductEntity entity = this.Products.Where(p => p.Name == productsDS.Tables[ProductsDS.PRODUCTSTABLE].Rows[0][ProductsDS.NAME].ToString()).FirstOrDefault();
                    if (entity != null)
                    {
                        productsDS.ExtendedProperties.Add("Result", OperationResults.Failed);
                        productsDS.ExtendedProperties.Add("Message", "Product already exists with this Name.");
                    }
                    else
                    {
                        entity = new ProductEntity();
                        entity.ID = Guid.NewGuid();
                        entity.Name = productsDS.Tables[ProductsDS.PRODUCTSTABLE].Rows[0][ProductsDS.NAME].ToString();
                        entity.Description = productsDS.Tables[ProductsDS.PRODUCTSTABLE].Rows[0][ProductsDS.DESCRIPTION].ToString();
                        entity.EntityType = (OrganizationType)Convert.ToInt32(productsDS.Tables[ProductsDS.PRODUCTSTABLE].Rows[0][ProductsDS.EntityType].ToString());
                        entity.ProductType = productsDS.Tables[ProductsDS.PRODUCTSTABLE].Rows[0][ProductsDS.ProductType].ToString();
                        entity.ProductAPIR = productsDS.Tables[ProductsDS.PRODUCTSTABLE].Rows[0][ProductsDS.ProductAPIR].ToString();
                        entity.ProductISIN = productsDS.Tables[ProductsDS.PRODUCTSTABLE].Rows[0][ProductsDS.ProductISIN].ToString();
                        entity.ProductSecuritiesId = Guid.Parse(productsDS.Tables[ProductsDS.PRODUCTSTABLE].Rows[0][ProductsDS.ProductSecuritiesId].ToString());

                        entity.IsDefaultProductSecurity = false;
                        entity.BankAccountType = string.Empty;
                        entity.FundAccounts = new List<Guid>();
                        switch (entity.EntityType)
                        {
                            case OrganizationType.BankAccount:

                                entity.EntityId = new IdentityCM { Clid = Guid.Parse(productsDS.Tables[ProductsDS.PRODUCTSTABLE].Rows[0][ProductsDS.EntityId].ToString()) };

                                entity.BankAccountType = productsDS.Tables[ProductsDS.PRODUCTSTABLE].Rows[0][ProductsDS.BankAccountType].ToString();


                                break;
                            case OrganizationType.DesktopBrokerAccount:
                                entity.EntityId = new IdentityCM { Clid = Guid.Parse(productsDS.Tables[ProductsDS.PRODUCTSTABLE].Rows[0][ProductsDS.EntityId].ToString()) };

                                entity.IsDefaultProductSecurity = bool.Parse(productsDS.Tables[ProductsDS.PRODUCTSTABLE].Rows[0][ProductsDS.IsDefaultProductSecurity].ToString());
                                break;
                            case OrganizationType.TermDepositAccount:

                                entity.EntityId = new IdentityCM { Clid = Guid.Parse(productsDS.Tables[ProductsDS.PRODUCTSTABLE].Rows[0][ProductsDS.EntityId].ToString()), Csid = Guid.Parse(productsDS.Tables[ProductsDS.PRODUCTSTABLE].Rows[0][ProductsDS.EntityCsId].ToString()) };

                                entity.IncludedAccounts = new List<IdentityCM>();
                                break;
                            case OrganizationType.ManagedInvestmentSchemesAccount:

                                entity.EntityId = new IdentityCM { Clid = Guid.Parse(productsDS.Tables[ProductsDS.PRODUCTSTABLE].Rows[0][ProductsDS.EntityId].ToString()), Csid = Guid.Parse(productsDS.Tables[ProductsDS.PRODUCTSTABLE].Rows[0][ProductsDS.EntityCsId].ToString()) };
                                entity.FundAccounts = new List<Guid> { Guid.Parse(productsDS.Tables[ProductsDS.PRODUCTSTABLE].Rows[0][ProductsDS.FundAccounts].ToString()) };

                                break;
                        }
                        this.Products.Add(entity);
                        productsDS.ExtendedProperties.Add("Result", OperationResults.Successfull);
                        productsDS.ExtendedProperties.Add("Message", "Product added.");

                    }

                    UpdateProductTaskDescription(entity);
                }
                else if (productsDS.CommandType == DatasetCommandTypes.Update)
                {
                    var entity = this.Products.Where(p => p.ID == Guid.Parse(productsDS.Tables[ProductsDS.PRODUCTSTABLE].Rows[0][ProductsDS.ID].ToString())).FirstOrDefault();
                    if (entity != null)
                    {
                        entity.Name = productsDS.Tables[ProductsDS.PRODUCTSTABLE].Rows[0][ProductsDS.NAME].ToString();
                        entity.Description = productsDS.Tables[ProductsDS.PRODUCTSTABLE].Rows[0][ProductsDS.DESCRIPTION].ToString();
                        entity.EntityType = (OrganizationType)Convert.ToInt32(productsDS.Tables[ProductsDS.PRODUCTSTABLE].Rows[0][ProductsDS.EntityType].ToString());
                        entity.ProductType = productsDS.Tables[ProductsDS.PRODUCTSTABLE].Rows[0][ProductsDS.ProductType].ToString();
                        entity.ProductAPIR = productsDS.Tables[ProductsDS.PRODUCTSTABLE].Rows[0][ProductsDS.ProductAPIR].ToString();
                        entity.ProductISIN = productsDS.Tables[ProductsDS.PRODUCTSTABLE].Rows[0][ProductsDS.ProductISIN].ToString();
                        entity.ProductSecuritiesId = Guid.Parse(productsDS.Tables[ProductsDS.PRODUCTSTABLE].Rows[0][ProductsDS.ProductSecuritiesId].ToString());
                        entity.BankAccountType = string.Empty;
                        entity.FundAccounts = new List<Guid>();


                        entity.IsDefaultProductSecurity = false;

                        switch (entity.EntityType)
                        {
                            case OrganizationType.BankAccount:

                                entity.EntityId = new IdentityCM { Clid = Guid.Parse(productsDS.Tables[ProductsDS.PRODUCTSTABLE].Rows[0][ProductsDS.EntityId].ToString()) };

                                entity.BankAccountType = productsDS.Tables[ProductsDS.PRODUCTSTABLE].Rows[0][ProductsDS.BankAccountType].ToString();
                                break;
                            case OrganizationType.DesktopBrokerAccount:
                                entity.EntityId = new IdentityCM { Clid = Guid.Parse(productsDS.Tables[ProductsDS.PRODUCTSTABLE].Rows[0][ProductsDS.EntityId].ToString()) };

                                entity.IsDefaultProductSecurity = bool.Parse(productsDS.Tables[ProductsDS.PRODUCTSTABLE].Rows[0][ProductsDS.IsDefaultProductSecurity].ToString());
                                break;
                            case OrganizationType.TermDepositAccount:

                                entity.EntityId = new IdentityCM { Clid = Guid.Parse(productsDS.Tables[ProductsDS.PRODUCTSTABLE].Rows[0][ProductsDS.EntityId].ToString()), Csid = Guid.Parse(productsDS.Tables[ProductsDS.PRODUCTSTABLE].Rows[0][ProductsDS.EntityCsId].ToString()) };


                                break;
                            case OrganizationType.ManagedInvestmentSchemesAccount:

                                entity.EntityId = new IdentityCM { Clid = Guid.Parse(productsDS.Tables[ProductsDS.PRODUCTSTABLE].Rows[0][ProductsDS.EntityId].ToString()), Csid = Guid.Parse(productsDS.Tables[ProductsDS.PRODUCTSTABLE].Rows[0][ProductsDS.EntityCsId].ToString()) };
                                entity.FundAccounts = new List<Guid> { Guid.Parse(productsDS.Tables[ProductsDS.PRODUCTSTABLE].Rows[0][ProductsDS.FundAccounts].ToString()) };

                                break;
                        }

                        productsDS.ExtendedProperties.Add("Result", OperationResults.Successfull);
                        productsDS.ExtendedProperties.Add("Message", "Product updated.");

                        UpdateProductTaskDescription(entity);
                    }
                    else
                    {
                        productsDS.ExtendedProperties.Add("Result", OperationResults.Failed);
                        productsDS.ExtendedProperties.Add("Message", "Product doesn't exists.");
                    }


                }
                else if (productsDS.CommandType == DatasetCommandTypes.Delete)
                {
                    Guid id = Guid.Parse(productsDS.Tables[ProductsDS.PRODUCTSTABLE].Rows[0][ProductsDS.ID].ToString());
                    var entity = this.Products.Where(p => p.ID == id).FirstOrDefault();
                    var investmentManagement = this._InvestmentManagementDetails.Where(p => p.ID == id).FirstOrDefault();
                    if (investmentManagement != null)
                        this._InvestmentManagementDetails.Remove(investmentManagement);

                    if (entity != null)
                    {
                        this.Products.Remove(entity);
                        productsDS.ExtendedProperties.Add("Result", OperationResults.Successfull);
                        productsDS.ExtendedProperties.Add("Message", "Product deleted.");
                    }
                    else
                    {
                        productsDS.ExtendedProperties.Add("Result", OperationResults.Failed);
                        productsDS.ExtendedProperties.Add("Message", "Product doesn't exists.");
                    }
                }


            }
            #endregion
            #region Asset
            else if (data is AssetDS)
            {
                AssetDS DS = (AssetDS)data;

                if (DS.CommandType == DatasetCommandTypes.Add)
                {
                    var Entity = this.Assets.Where(p => p.Name == DS.Tables[AssetDS.TABLENAME].Rows[0][AssetDS.NAME].ToString()).FirstOrDefault();
                    if (Entity != null)
                    {
                        DS.ExtendedProperties.Add("Result", OperationResults.Failed);
                        DS.ExtendedProperties.Add("Message", "Asset already exists with this Name.");
                    }
                    else
                    {
                        AssetEntity entity = new AssetEntity();
                        entity.ID = Guid.NewGuid();
                        entity.Name = DS.Tables[AssetDS.TABLENAME].Rows[0][AssetDS.NAME].ToString();
                        entity.Description = DS.Tables[AssetDS.TABLENAME].Rows[0][AssetDS.DESCRIPTION].ToString();
                        this.Assets.Add(entity);
                        DS.ExtendedProperties.Add("Result", OperationResults.Successfull);
                        DS.ExtendedProperties.Add("Message", "Asset added.");

                    }
                }
                else if (DS.CommandType == DatasetCommandTypes.Update)
                {
                    var entity = this.Assets.Where(p => p.ID == Guid.Parse(DS.Tables[AssetDS.TABLENAME].Rows[0][AssetDS.ID].ToString())).FirstOrDefault();
                    if (entity != null)
                    {
                        entity.Name = DS.Tables[AssetDS.TABLENAME].Rows[0][AssetDS.NAME].ToString();
                        entity.Description = DS.Tables[AssetDS.TABLENAME].Rows[0][AssetDS.DESCRIPTION].ToString();
                        DS.ExtendedProperties.Add("Result", OperationResults.Successfull);
                        DS.ExtendedProperties.Add("Message", "Asset updated.");
                    }


                }
                else if (DS.CommandType == DatasetCommandTypes.Delete)
                {
                    var entity = this.Assets.Where(p => p.ID == Guid.Parse(DS.Tables[AssetDS.TABLENAME].Rows[0][AssetDS.ID].ToString())).FirstOrDefault();
                    if (entity != null)
                    {
                        this.Assets.Remove(entity);
                        DS.ExtendedProperties.Add("Result", OperationResults.Successfull);
                        DS.ExtendedProperties.Add("Message", "Asset deleted.");
                    }
                }
            }
            #endregion
            #region BusinessGroups
            else if (data is BusinessGroupsDS)
            {
                BusinessGroupsDS DS = (BusinessGroupsDS)data;

                if (DS.CommandType == DatasetCommandTypes.Add)
                {
                    var Entity = this.BusinessGroups.Where(p => p.Name == DS.Tables[BusinessGroupsDS.TABLENAME].Rows[0][BusinessGroupsDS.NAME].ToString()).FirstOrDefault();
                    if (Entity != null)
                    {
                        DS.ExtendedProperties.Add("Result", OperationResults.Failed);
                        DS.ExtendedProperties.Add("Message", "Business Group already exists with this Name.");
                    }
                    else
                    {
                        BusinessGroupEntity entity = new BusinessGroupEntity();
                        entity.ID = Guid.NewGuid();
                        entity.Name = DS.Tables[BusinessGroupsDS.TABLENAME].Rows[0][BusinessGroupsDS.NAME].ToString();
                        entity.Description = DS.Tables[BusinessGroupsDS.TABLENAME].Rows[0][BusinessGroupsDS.DESCRIPTION].ToString();
                        this.BusinessGroups.Add(entity);
                        DS.ExtendedProperties.Add("Result", OperationResults.Successfull);
                        DS.ExtendedProperties.Add("Message", "Business Group added.");

                    }
                }
                else if (DS.CommandType == DatasetCommandTypes.Update)
                {
                    var entity = this.BusinessGroups.Where(p => p.ID == Guid.Parse(DS.Tables[BusinessGroupsDS.TABLENAME].Rows[0][BusinessGroupsDS.ID].ToString())).FirstOrDefault();
                    if (entity != null)
                    {
                        entity.Name = DS.Tables[BusinessGroupsDS.TABLENAME].Rows[0][BusinessGroupsDS.NAME].ToString();
                        entity.Description = DS.Tables[BusinessGroupsDS.TABLENAME].Rows[0][BusinessGroupsDS.DESCRIPTION].ToString();
                        DS.ExtendedProperties.Add("Result", OperationResults.Successfull);
                        DS.ExtendedProperties.Add("Message", "Business Group updated.");
                    }
                }
                else if (DS.CommandType == DatasetCommandTypes.Delete)
                {
                    var entity = this.BusinessGroups.Where(p => p.ID == Guid.Parse(DS.Tables[BusinessGroupsDS.TABLENAME].Rows[0][BusinessGroupsDS.ID].ToString())).FirstOrDefault();
                    if (entity != null)
                    {
                        this.BusinessGroups.Remove(entity);
                        DS.ExtendedProperties.Add("Result", OperationResults.Successfull);
                        DS.ExtendedProperties.Add("Message", "Business Group deleted.");
                    }
                }
            }
            #endregion
            #region StatusFlag
            else if (data is StatusDS)
            {
                StatusDS DS = (StatusDS)data;

                if (DS.CommandType == DatasetCommandTypes.Add)
                {
                    var entity =
                        this.StatusList.Where(p =>
                        (p.Name == DS.Tables[StatusDS.TABLENAME].Rows[0][StatusDS.NAME].ToString()) &&
                        p.Type == DS.Tables[StatusDS.TABLENAME].Rows[0][StatusDS.TYPE].ToString()).FirstOrDefault();
                    if (entity != null)
                    {
                        DS.ExtendedProperties.Add("Result", OperationResults.Failed);
                        DS.ExtendedProperties.Add("Message", "Status already exists with this Name.");
                    }
                    else
                    {
                        Status statusEntity = new Status();
                        statusEntity.Name = DS.Tables[StatusDS.TABLENAME].Rows[0][StatusDS.NAME].ToString();
                        statusEntity.Type = DS.Tables[StatusDS.TABLENAME].Rows[0][StatusDS.TYPE].ToString();
                        this.StatusList.Add(statusEntity);
                        DS.ExtendedProperties.Add("Result", OperationResults.Successfull);
                        DS.ExtendedProperties.Add("Message", "Status added.");
                    }
                }
                else if (DS.CommandType == DatasetCommandTypes.Update)
                {
                    var entity =
                        this.StatusList.Where(p =>
                        (p.Name == DS.Tables[StatusDS.TABLENAME].Rows[0][StatusDS.NAME].ToString()) &&
                        p.Type == DS.Tables[StatusDS.TABLENAME].Rows[0][StatusDS.TYPE].ToString()).FirstOrDefault();
                    if (entity != null)
                    {
                        entity.Name = DS.Tables[StatusDS.TABLENAME].Rows[0][StatusDS.NAME].ToString();
                        entity.Type = DS.Tables[StatusDS.TABLENAME].Rows[0][StatusDS.TYPE].ToString();
                        DS.ExtendedProperties.Add("Result", OperationResults.Successfull);
                        DS.ExtendedProperties.Add("Message", "Status updated.");
                    }
                }
                else if (DS.CommandType == DatasetCommandTypes.Delete)
                {
                    var entity =
                        this.StatusList.Where(p =>
                        (p.Name == DS.Tables[StatusDS.TABLENAME].Rows[0][StatusDS.NAME].ToString()) &&
                        p.Type == DS.Tables[StatusDS.TABLENAME].Rows[0][StatusDS.TYPE].ToString()).FirstOrDefault();
                    if (entity != null)
                    {
                        this.StatusList.Remove(entity);
                        DS.ExtendedProperties.Add("Result", OperationResults.Successfull);
                        DS.ExtendedProperties.Add("Message", "Status deleted.");
                    }
                }
            }
            #endregion
            else if (data is SecuritiesDS)
            {
                SecuritiesDS securitiesDS = (SecuritiesDS)data;
                var security = this.Securities.Where(ss => ss.ID == securitiesDS.SecID).FirstOrDefault();

                if (security != null)
                {
                    if (securitiesDS.CommandType == DatasetCommandTypes.Delete)
                    {
                        security.ID = securitiesDS.SecID;
                        Securities.Remove(security);
                        securitiesDS.ExtendedProperties.Add("Result", OperationResults.Successfull);
                        securitiesDS.ExtendedProperties.Add("Message", "Security deleted.");
                    }
                    else if (securitiesDS.CommandType == DatasetCommandTypes.Update)
                    {
                        if (securitiesDS.Tables[SecuritiesDS.SECLIST].Rows.Count > 0)
                        {
                            DataRow row = securitiesDS.Tables[SecuritiesDS.SECLIST].Rows[0];
                            PopulateSecurityFromDataRow(security, row);
                        }


                        securitiesDS.ExtendedProperties.Add("Result", OperationResults.Successfull);
                        securitiesDS.ExtendedProperties.Add("Message", "Security updated.");
                    }

                }
                if (securitiesDS.CommandType == DatasetCommandTypes.Add)
                {
                    SecuritiesEntity securityentity = new SecuritiesEntity();
                    securityentity.ID = securitiesDS.SecID;
                    Securities.Add(securityentity);

                    if (securitiesDS.Tables[SecuritiesDS.SECLIST].Rows.Count > 0)
                    {
                        DataRow row = securitiesDS.Tables[SecuritiesDS.SECLIST].Rows[0];
                        PopulateSecurityFromDataRow(securityentity, row);
                    }

                    securitiesDS.ExtendedProperties.Add("Result", OperationResults.Successfull);
                    securitiesDS.ExtendedProperties.Add("Message", "Security added.");
                }
                else if (securitiesDS.CommandType == DatasetCommandTypes.UpdatedSMAProperties)
                {
                    foreach (DataRow row in securitiesDS.Tables[SecuritiesDS.SECLIST].Rows)
                    {
                        var sec = Securities.FirstOrDefault(ss => ss.ID == (Guid)row[SecuritiesDS.ID]);
                        if (sec != null)
                        {
                            sec.IsSMAApproved = (bool)row[SecuritiesDS.ISSMAAPPROVED];
                            sec.SMAHoldingLimit = (double)row[SecuritiesDS.SMAHOLDINGLIMIT];
                        }
                    }
                    securitiesDS.ExtendedProperties.Add("Result", OperationResults.Successfull);
                    securitiesDS.ExtendedProperties.Add("Message", "Securities updated.");
                }
            }
            else if (data is ManualAssetsDS)
            {
                var manualAssetDs = (ManualAssetsDS)data;
                if (manualAssetDs.IsDetails)
                {
                    var manual = ManualAsset.Where(details => details.ID == manualAssetDs.ItemID).FirstOrDefault();
                    var manualDetails = manual.ASXSecurity.Where(details => details.ID == manualAssetDs.DetailsGUID).FirstOrDefault();

                    if (manualDetails != null)
                    {
                        switch (manualAssetDs.CommandType)
                        {
                            case DatasetCommandTypes.Delete:
                                manualDetails.ID = manualAssetDs.DetailsGUID;
                                manual.ASXSecurity.Remove(manualDetails);
                                break;
                            case DatasetCommandTypes.Update:
                                {
                                    manualDetails.ID = manualAssetDs.DetailsGUID;
                                    manual.ASXSecurity.Remove(manualDetails);
                                    var manualASXSecurityEntity = new ManualAssetASXSecurityEntity();
                                    manualASXSecurityEntity.ID = manualAssetDs.DetailsGUID;
                                    manualASXSecurityEntity.Date = Convert.ToDateTime(manualAssetDs.Tables[ManualAssetsDS.MANPRICEHISTORY].Rows[0][ManualAssetsDS.DATE].ToString());
                                    manualASXSecurityEntity.Currency = manualAssetDs.Tables[ManualAssetsDS.MANPRICEHISTORY].Rows[0][ManualAssetsDS.CURRENCY].ToString();
                                    manualASXSecurityEntity.UnitPrice = Math.Round(Convert.ToDouble(manualAssetDs.Tables[ManualAssetsDS.MANPRICEHISTORY].Rows[0][ManualAssetsDS.UNITPRICE].ToString()), 6);
                                    manualASXSecurityEntity.NavValue = Math.Round(Convert.ToDouble(manualAssetDs.Tables[ManualAssetsDS.MANPRICEHISTORY].Rows[0][ManualAssetsDS.NAVPRICE].ToString()), 6);
                                    manualASXSecurityEntity.PurValue = Math.Round(Convert.ToDouble(manualAssetDs.Tables[ManualAssetsDS.MANPRICEHISTORY].Rows[0][ManualAssetsDS.PURPRICE].ToString()), 6);
                                    manualASXSecurityEntity.TotalUnits = manual.UnitSize;
                                    manualASXSecurityEntity.Value = manualASXSecurityEntity.UnitPrice * manualASXSecurityEntity.TotalUnits;
                                    manual.ASXSecurity.Add(manualASXSecurityEntity);
                                    break;
                                }
                        }

                    }
                    if (manualAssetDs.CommandType == DatasetCommandTypes.Add)
                    {
                        var manualASXSecurityEntity = new ManualAssetASXSecurityEntity();
                        manualASXSecurityEntity.ID = manualAssetDs.DetailsGUID;
                        manualASXSecurityEntity.Date = Convert.ToDateTime(manualAssetDs.Tables[ManualAssetsDS.MANPRICEHISTORY].Rows[0][ManualAssetsDS.DATE].ToString());
                        manualASXSecurityEntity.Currency = manualAssetDs.Tables[ManualAssetsDS.MANPRICEHISTORY].Rows[0][ManualAssetsDS.CURRENCY].ToString();
                        manualASXSecurityEntity.UnitPrice = Math.Round(Convert.ToDouble(manualAssetDs.Tables[ManualAssetsDS.MANPRICEHISTORY].Rows[0][ManualAssetsDS.UNITPRICE].ToString()), 6);
                        manualASXSecurityEntity.NavValue = Math.Round(Convert.ToDouble(manualAssetDs.Tables[ManualAssetsDS.MANPRICEHISTORY].Rows[0][ManualAssetsDS.NAVPRICE].ToString()), 6);
                        manualASXSecurityEntity.PurValue = Math.Round(Convert.ToDouble(manualAssetDs.Tables[ManualAssetsDS.MANPRICEHISTORY].Rows[0][ManualAssetsDS.PURPRICE].ToString()), 6);
                        manualASXSecurityEntity.TotalUnits = manual.UnitSize;
                        manualASXSecurityEntity.Value = manualASXSecurityEntity.UnitPrice * manualASXSecurityEntity.TotalUnits;
                        manual.ASXSecurity.Add(manualASXSecurityEntity);
                    }
                }
                else
                {
                    var manualAsset = ManualAsset.Where(item => item.ID == manualAssetDs.ItemID).FirstOrDefault();
                    if (manualAsset != null)
                    {
                        switch (manualAssetDs.CommandType)
                        {
                            case DatasetCommandTypes.Delete:
                                manualAsset.ID = manualAssetDs.ItemID;
                                ManualAsset.Remove(manualAsset);
                                break;
                            case DatasetCommandTypes.Update:
                                manualAsset.InvestmentType = manualAssetDs.Tables[ManualAssetsDS.MANIST].Rows[0][ManualAssetsDS.INVESTMENTTYPE].ToString();
                                manualAsset.AsxCode = manualAssetDs.Tables[ManualAssetsDS.MANIST].Rows[0][ManualAssetsDS.CODE].ToString();
                                manualAsset.Market = manualAssetDs.Tables[ManualAssetsDS.MANIST].Rows[0][ManualAssetsDS.MARKET].ToString();
                                manualAsset.Rating = manualAssetDs.Tables[ManualAssetsDS.MANIST].Rows[0][ManualAssetsDS.RATING].ToString();
                                manualAsset.Status = manualAssetDs.Tables[ManualAssetsDS.MANIST].Rows[0][ManualAssetsDS.STATUS].ToString();
                                manualAsset.CompanyName = manualAssetDs.Tables[ManualAssetsDS.MANIST].Rows[0][ManualAssetsDS.DESCRIPTION].ToString();
                                manualAsset.isUnitised = Convert.ToBoolean(manualAssetDs.Tables[ManualAssetsDS.MANIST].Rows[0][ManualAssetsDS.ISUNITISED]);
                                //manualAsset.UnitPriceBaseCurrency = Convert.ToDouble(manualAssetDs.Tables[ManualAssetsDS.MANIST].Rows[0][ManualAssetsDS.UNITPRICEBASECURRENCY].ToString());
                                manualAsset.UnitSize = Convert.ToDouble(manualAssetDs.Tables[ManualAssetsDS.MANIST].Rows[0][ManualAssetsDS.UNITSIZE].ToString());
                                manualAsset.MinSecurityBal = Convert.ToDouble(manualAssetDs.Tables[ManualAssetsDS.MANIST].Rows[0][ManualAssetsDS.MINSECURITYBAL].ToString());
                                manualAsset.IgnoreMinSecurityBal = Convert.ToBoolean(manualAssetDs.Tables[ManualAssetsDS.MANIST].Rows[0][ManualAssetsDS.IGNOREMINSECURITYBAL].ToString());
                                manualAsset.MinTradedLotSize = Convert.ToDouble(manualAssetDs.Tables[ManualAssetsDS.MANIST].Rows[0][ManualAssetsDS.MINTRADEDLOTSIZE].ToString());
                                manualAsset.IgnoreMinTradedLotSize = Convert.ToBoolean(manualAssetDs.Tables[ManualAssetsDS.MANIST].Rows[0][ManualAssetsDS.IGNOREMINTRADEDLOTSIZE].ToString());
                                string issueDate = manualAssetDs.Tables[ManualAssetsDS.MANIST].Rows[0][ManualAssetsDS.ISSUEDATE].ToString();
                                if (!string.IsNullOrEmpty(issueDate))
                                    manualAsset.IssueDate = Convert.ToDateTime(issueDate);
                                manualAsset.PriceAsAtDate = Convert.ToDouble(manualAssetDs.Tables[ManualAssetsDS.MANIST].Rows[0][ManualAssetsDS.PRICEASATDATE].ToString());
                                manualAsset.IsPrefferedInvestment = Convert.ToBoolean(manualAssetDs.Tables[ManualAssetsDS.MANIST].Rows[0][ManualAssetsDS.ISPREFFEREDINVESTMENT].ToString());
                                break;
                        }

                    }
                    if (manualAssetDs.CommandType == DatasetCommandTypes.Add)
                    {
                        var manualAssetEntity = new ManualAssetEntity();
                        manualAssetEntity.ID = manualAssetDs.ItemID;
                        manualAssetEntity.InvestmentType = manualAssetDs.Tables[ManualAssetsDS.MANIST].Rows[0][ManualAssetsDS.INVESTMENTTYPE].ToString();
                        manualAssetEntity.AsxCode = manualAssetDs.Tables[ManualAssetsDS.MANIST].Rows[0][ManualAssetsDS.CODE].ToString();
                        manualAssetEntity.Market = manualAssetDs.Tables[ManualAssetsDS.MANIST].Rows[0][ManualAssetsDS.MARKET].ToString();
                        manualAssetEntity.Rating = manualAssetDs.Tables[ManualAssetsDS.MANIST].Rows[0][ManualAssetsDS.RATING].ToString();
                        manualAssetEntity.Status = manualAssetDs.Tables[ManualAssetsDS.MANIST].Rows[0][ManualAssetsDS.STATUS].ToString();
                        manualAssetEntity.CompanyName = manualAssetDs.Tables[ManualAssetsDS.MANIST].Rows[0][ManualAssetsDS.DESCRIPTION].ToString();
                        manualAssetEntity.isUnitised = Convert.ToBoolean(manualAssetDs.Tables[ManualAssetsDS.MANIST].Rows[0][ManualAssetsDS.ISUNITISED]);
                        //manualAssetEntity.UnitPriceBaseCurrency = Convert.ToDouble(manualAssetDs.Tables[ManualAssetsDS.MANIST].Rows[0][ManualAssetsDS.UNITPRICEBASECURRENCY].ToString());
                        manualAssetEntity.UnitSize = Convert.ToDouble(manualAssetDs.Tables[ManualAssetsDS.MANIST].Rows[0][ManualAssetsDS.UNITSIZE].ToString());
                        manualAssetEntity.MinSecurityBal = Convert.ToDouble(manualAssetDs.Tables[ManualAssetsDS.MANIST].Rows[0][ManualAssetsDS.MINSECURITYBAL].ToString());
                        manualAssetEntity.IgnoreMinSecurityBal = Convert.ToBoolean(manualAssetDs.Tables[ManualAssetsDS.MANIST].Rows[0][ManualAssetsDS.IGNOREMINSECURITYBAL].ToString());
                        manualAssetEntity.MinTradedLotSize = Convert.ToDouble(manualAssetDs.Tables[ManualAssetsDS.MANIST].Rows[0][ManualAssetsDS.MINTRADEDLOTSIZE].ToString());
                        manualAssetEntity.IgnoreMinTradedLotSize = Convert.ToBoolean(manualAssetDs.Tables[ManualAssetsDS.MANIST].Rows[0][ManualAssetsDS.IGNOREMINTRADEDLOTSIZE].ToString());
                        string issueDate = manualAssetDs.Tables[ManualAssetsDS.MANIST].Rows[0][ManualAssetsDS.ISSUEDATE].ToString();
                        if (!string.IsNullOrEmpty(issueDate))
                            manualAssetEntity.IssueDate = Convert.ToDateTime(issueDate);
                        manualAssetEntity.PriceAsAtDate = Convert.ToDouble(manualAssetDs.Tables[ManualAssetsDS.MANIST].Rows[0][ManualAssetsDS.PRICEASATDATE].ToString());
                        manualAssetEntity.IsPrefferedInvestment = Convert.ToBoolean(manualAssetDs.Tables[ManualAssetsDS.MANIST].Rows[0][ManualAssetsDS.ISPREFFEREDINVESTMENT].ToString());
                        ManualAsset.Add(manualAssetEntity);
                    }
                }
            }
            else if (data is OrderPadAtCallRatesDS)
            {
                var ds = data as OrderPadAtCallRatesDS;

                var inst = Institution.FirstOrDefault(item => item.ID == ds.MainInstituteId);
                if (inst != null)
                {
                    foreach (DataRow row in ds.OrderPadAtCallRatesTable.Rows)
                    {
                        AtCallInstitutePriceEntity priceEntity = null;
                        if (inst.AtCallPrices != null && ds.CommandType != DatasetCommandTypes.Add)
                            priceEntity = inst.AtCallPrices.FirstOrDefault(item => item.ID == new Guid(row[ds.OrderPadAtCallRatesTable.ID].ToString()));

                        switch (ds.CommandType)
                        {
                            case DatasetCommandTypes.Delete:
                                if (priceEntity != null)
                                {
                                    inst.AtCallPrices.Remove(priceEntity);
                                }
                                break;
                            case DatasetCommandTypes.Update:
                                if (priceEntity != null)
                                {
                                    priceEntity.InstituteID = new Guid(row[ds.OrderPadAtCallRatesTable.INSTITUTEID].ToString());
                                    priceEntity.AccountTier = row[ds.OrderPadAtCallRatesTable.ACCOUNTTIER].ToString();
                                    priceEntity.IsEditable = Convert.ToBoolean(row[ds.OrderPadAtCallRatesTable.ISEDITABLE]);
                                    priceEntity.Max = row[ds.OrderPadAtCallRatesTable.MAX] as decimal?;
                                    priceEntity.MaximumBrokerage = row[ds.OrderPadAtCallRatesTable.MAXIMUMBROKERAGE] as decimal?;
                                    priceEntity.Min = row[ds.OrderPadAtCallRatesTable.MIN] as decimal?;
                                    priceEntity.Rate2 = row[ds.OrderPadAtCallRatesTable.RATE].ToString();
                                    priceEntity.Applicability = row[ds.OrderPadAtCallRatesTable.APPLICABILITY].ToString();
                                    priceEntity.HoneymoonPeriod =
                                        row[ds.OrderPadAtCallRatesTable.HONEYMOONPERIOD] as decimal?;
                                    var applicableDate = row[ds.OrderPadAtCallRatesTable.APPLICABLEFROM].ToString();
                                    if (!string.IsNullOrEmpty(applicableDate))
                                    {
                                        priceEntity.ApplicableFrom = Convert.ToDateTime(applicableDate);
                                    }
                                }
                                break;
                            case DatasetCommandTypes.Add:
                                if (priceEntity == null)
                                {
                                    priceEntity = new AtCallInstitutePriceEntity();
                                    priceEntity.InstituteID = new Guid(row[ds.OrderPadAtCallRatesTable.INSTITUTEID].ToString());
                                    priceEntity.AccountTier = row[ds.OrderPadAtCallRatesTable.ACCOUNTTIER].ToString();
                                    priceEntity.IsEditable = Convert.ToBoolean(row[ds.OrderPadAtCallRatesTable.ISEDITABLE]);
                                    priceEntity.Max = row[ds.OrderPadAtCallRatesTable.MAX] as decimal?;
                                    priceEntity.MaximumBrokerage = row[ds.OrderPadAtCallRatesTable.MAXIMUMBROKERAGE] as decimal?;
                                    priceEntity.Min = row[ds.OrderPadAtCallRatesTable.MIN] as decimal?;
                                    priceEntity.Rate2 = row[ds.OrderPadAtCallRatesTable.RATE].ToString();
                                    priceEntity.Type = row[ds.OrderPadAtCallRatesTable.TYPE].ToString();
                                    priceEntity.Applicability = row[ds.OrderPadAtCallRatesTable.APPLICABILITY].ToString();

                                    var date = row[ds.OrderPadAtCallRatesTable.DATE].ToString();
                                    if (!string.IsNullOrEmpty(date))
                                    {
                                        priceEntity.Date = Convert.ToDateTime(date);
                                    }

                                    date = row[ds.OrderPadAtCallRatesTable.APPLICABLEFROM].ToString();
                                    if (!string.IsNullOrEmpty(date))
                                    {
                                        priceEntity.ApplicableFrom = Convert.ToDateTime(date);
                                    }
                                    if (inst.AtCallPrices == null)
                                    {
                                        inst.AtCallPrices = new ObservableCollection<AtCallInstitutePriceEntity>();
                                    }
                                    inst.AtCallPrices.Add(priceEntity);
                                }
                                break;
                        }

                    }
                }

            }
            else if (data is OrderPadRatesDS)
            {
                var ds = data as OrderPadRatesDS;
                var inst = Institution.FirstOrDefault(item => item.ID == ds.InstituteId);
                if (inst != null)
                {
                    foreach (DataRow row in ds.Tables[OrderPadRatesDS.ORDERPADRATESTABLE].Rows)
                    {
                        TDInstitutePriceEntity priceEntity = null;
                        if (inst.TDPrices != null && ds.CommandType != DatasetCommandTypes.Add)
                            priceEntity = inst.TDPrices.FirstOrDefault(item => item.ID == new Guid(row[OrderPadRatesDS.ID].ToString()));

                        switch (ds.CommandType)
                        {
                            case DatasetCommandTypes.Delete:
                                if (priceEntity != null)
                                {
                                    inst.TDPrices.Remove(priceEntity);
                                }
                                break;
                            case DatasetCommandTypes.Update:
                                if (priceEntity != null)
                                {
                                    priceEntity.Ongoing = row[OrderPadRatesDS.ONGOING] as decimal?;
                                    priceEntity.Min = row[OrderPadRatesDS.MIN] as decimal?;
                                    priceEntity.Max = row[OrderPadRatesDS.MAX] as decimal?;
                                    priceEntity.MaximumBrokeage = row[OrderPadRatesDS.MAXIMUMBROKEAGE] as decimal?;
                                    priceEntity.Days30 = row[OrderPadRatesDS.DAYS30] as decimal?;
                                    priceEntity.Days60 = row[OrderPadRatesDS.DAYS60] as decimal?;
                                    priceEntity.Days90 = row[OrderPadRatesDS.DAYS90] as decimal?;
                                    priceEntity.Days120 = row[OrderPadRatesDS.DAYS120] as decimal?;
                                    priceEntity.Days150 = row[OrderPadRatesDS.DAYS150] as decimal?;
                                    priceEntity.Days180 = row[OrderPadRatesDS.DAYS180] as decimal?;
                                    priceEntity.Days270 = row[OrderPadRatesDS.DAYS270] as decimal?;
                                    priceEntity.Years1 = row[OrderPadRatesDS.YEARS1] as decimal?;
                                    priceEntity.Years2 = row[OrderPadRatesDS.YEARS2] as decimal?;
                                    priceEntity.Years3 = row[OrderPadRatesDS.YEARS3] as decimal?;
                                    priceEntity.Years4 = row[OrderPadRatesDS.YEARS4] as decimal?;
                                    priceEntity.Years5 = row[OrderPadRatesDS.YEARS5] as decimal?;
                                }
                                break;
                        }
                    }
                }
            }
            else if (data is InstitutesDS)
            {
                var dataset = data as InstitutesDS;
                switch (dataset.CommandType)
                {
                    case DatasetCommandTypes.Add:

                        foreach (DataRow row in dataset.InstitutionsTable.Rows)
                        {
                            var inst = new InstitutionEntity { ID = Guid.NewGuid() };

                            SetInstituteValues(dataset, row, inst);

                            this.Institution.Add(inst);
                        }


                        break;
                    case DatasetCommandTypes.Update:

                        foreach (DataRow row in dataset.InstitutionsTable.Rows)
                        {
                            var InsEntity = this.Institution.FirstOrDefault(inst => inst.ID == (Guid)row[dataset.InstitutionsTable.ID]);
                            SetInstituteValues(dataset, row, InsEntity);
                        }


                        break;
                    case DatasetCommandTypes.Delete:
                        {
                            foreach (DataRow row in dataset.InstitutionsTable.Rows)
                            {
                                var insEntity = this.Institution.FirstOrDefault(inst => inst.ID == (Guid)row[dataset.InstitutionsTable.ID]);
                                if (insEntity != null)
                                {
                                    if (insEntity.BankAccounts == null || insEntity.BankAccounts.Count == 0)
                                    {
                                        Institution.Remove(insEntity);
                                    }
                                    else
                                    {
                                        dataset.ExtendedProperties.Add("Message", "Cannot delete, associated with bank account");
                                    }
                                }
                            }


                            break;
                        }
                }

            }
            else if (data is InstitutionContactsDS)
            {
                var dataset = data as InstitutionContactsDS;
                var instituteEntity = this.Institution.FirstOrDefault(ss => ss.ID == dataset.Id);
                if (instituteEntity != null)
                {
                    dataset.InstituteName = instituteEntity.Name;
                    switch (dataset.CommandType)
                    {
                        case DatasetCommandTypes.Update:
                            {
                                if (instituteEntity.Signatories == null)
                                    instituteEntity.Signatories = new ObservableCollection<IndividualEntityCsidClidDetail>();
                                foreach (DataRow dr in dataset.IndividualTable.Rows)
                                {
                                    if (instituteEntity.Signatories.FirstOrDefault(ss => ss.Clid == new Guid(dr["Clid"].ToString()) && ss.Csid == new Guid(dr["Csid"].ToString())) == null)
                                    {
                                        instituteEntity.Signatories.Add(new IndividualEntityCsidClidDetail { Clid = new Guid(dr[dataset.IndividualTable.CLID].ToString()), Csid = new Guid(dr[dataset.IndividualTable.CSID].ToString()) });
                                    }
                                }
                                break;
                            }
                        case DatasetCommandTypes.Delete:
                            {
                                foreach (DataRow dr in dataset.IndividualTable.Rows)
                                {
                                    IndividualEntityCsidClidDetail deleteContact = new IndividualEntityCsidClidDetail { Clid = new Guid(dr[dataset.IndividualTable.CLID].ToString()), Csid = new Guid(dr[dataset.IndividualTable.CSID].ToString()) };
                                    var contact = instituteEntity.Signatories.Where(ss => ss.Clid == deleteContact.Clid && ss.Csid == deleteContact.Csid).ToList();
                                    if (contact != null)
                                    {
                                        foreach (var individualDetail in contact)
                                        {
                                            instituteEntity.Signatories.Remove(individualDetail);
                                        }
                                    }
                                }
                                break;
                            }
                    }
                }

            }
            else if (data is BusinessTitleDS)
            {
                BusinessTitleDS DS = (BusinessTitleDS)data;
                string businessTitle = DS.BTitleTable.Rows[0][DS.BTitleTable.TITLE].ToString();

                if (DS.CommandType == DatasetCommandTypes.Add)
                {
                    if (this.BussinessTitles.ContainsKey(businessTitle))
                    {
                        DS.ExtendedProperties.Add("Result", OperationResults.Failed);
                        DS.ExtendedProperties.Add("Message", "Title already exists with this Name.");
                    }
                    else
                    {
                        if (DS.BTitleTable.Rows.Count > 1)
                        {
                            foreach (DataRow dr in DS.BTitleTable.Rows)
                            {
                                businessTitle = dr[DS.BTitleTable.TITLE].ToString();
                                this.BussinessTitles.Add(businessTitle, businessTitle);
                            }
                        }
                        else
                        { this.BussinessTitles.Add(businessTitle, businessTitle); }
                        DS.ExtendedProperties.Add("Result", OperationResults.Successfull);
                        DS.ExtendedProperties.Add("Message", "Title added.");
                    }
                }
                else if (DS.CommandType == DatasetCommandTypes.Delete)
                {
                    if (this.BussinessTitles.ContainsKey(businessTitle))
                    {
                        this.BussinessTitles.Remove(businessTitle);
                        DS.ExtendedProperties.Add("Result", OperationResults.Successfull);
                        DS.ExtendedProperties.Add("Message", "Title deleted.");
                    }
                }
            }
            else if (data is CashBalanceDS)
            {
                var ds = data as CashBalanceDS;
                switch (ds.CommandType)
                {
                    case DatasetCommandTypes.Update:
                        {
                            var entity = CashBalances.FirstOrDefault(c => c.SuperAccountType.ToString() == ds.CashBalanceTable.Rows[0][ds.CashBalanceTable.SUPERACCOUNTTYPE].ToString());
                            if (entity != null)
                            {
                                entity.Percentage = Convert.ToDecimal(ds.CashBalanceTable.Rows[0][ds.CashBalanceTable.PERCENTAGE].ToString());
                                entity.MinValue = Convert.ToDecimal(ds.CashBalanceTable.Rows[0][ds.CashBalanceTable.MINVALUE].ToString());
                                entity.MaxValue = Convert.ToDecimal(ds.CashBalanceTable.Rows[0][ds.CashBalanceTable.MAXVALUE].ToString());

                                ds.ExtendedProperties.Add("Result", OperationResults.Successfull);
                                ds.ExtendedProperties.Add("Message", "Cash Balance updated.");
                            }
                            else
                            {
                                ds.ExtendedProperties.Add("Result", OperationResults.Failed);
                                ds.ExtendedProperties.Add("Message", "Cash Balance doesn't exists.");
                            }
                        }
                        break;
                }
            }
            else if (data is IHasOrganizationUnit)// this should be last if statement because it runs commands.
            {
                IHasOrganizationUnit ds = data as IHasOrganizationUnit;

                string log_message = string.Format("Executing  Command:{0} UserName:{1}", ds.Command,
                                                   ds.Unit.CurrentUser.CurrentUserName);

                if (data is IHasFileName)
                {
                    var fileds = data as IHasFileName;
                    if (fileds.FileName != null)
                    {
                        log_message += string.Format(" FileName:{0}", fileds.FileName);
                    }
                }

                Broker.LogEvent(EventType.FileImported, log_message, ds.Unit.CurrentUser.CurrentUserName, Guid.Empty);
                RunAddCommand(ds.Command, data);
            }

            return ModifiedState.MODIFIED;
        }

        private static void SetInstituteValues(InstitutesDS dataset, DataRow row, InstitutionEntity inst)
        {
            inst.Name = (string)row[dataset.InstitutionsTable.NAME];
            inst.Description = (string)row[dataset.InstitutionsTable.DESCRIPTION];
            inst.TradingName = (string)row[dataset.InstitutionsTable.TRADINGNAME];
            inst.LegalName = (string)row[dataset.InstitutionsTable.LEGALNAME];
            inst.ChkLegalName = (bool)row[dataset.InstitutionsTable.CHKLEGALNAME];
            inst.SPCreditRatingLong = (int)row[dataset.InstitutionsTable.SPCREDITRATINGLONG];
            inst.SPCreditRatingShort = (int)row[dataset.InstitutionsTable.SPCREDITRATINGSHORT];
            inst.Type = (int)row[dataset.InstitutionsTable.TYPE];
            inst.ABN = (string)row[dataset.InstitutionsTable.ABN];
            inst.ACN = (string)row[dataset.InstitutionsTable.ACN];
            inst.TFN = (string)row[dataset.InstitutionsTable.TFN];
            inst.TextInstitutionCode = (string)row[dataset.InstitutionsTable.INSTITUTIONCODE];
            inst.PhoneNumber = new PhoneNumberEntity { CountryCode = (string)row[dataset.InstitutionsTable.PHONENUMBERCOUNTYCODE], CityCode = (string)row[dataset.InstitutionsTable.PHONENUMBERCITYCODE], PhoneNumber = (string)row[dataset.InstitutionsTable.PHONENUMBER] };
            inst.Facsimile = new PhoneNumberEntity { CountryCode = (string)row[dataset.InstitutionsTable.FACSIMILECOUNTYCODE], CityCode = (string)row[dataset.InstitutionsTable.FACSIMILECITYCODE], PhoneNumber = (string)row[dataset.InstitutionsTable.FACSIMILE] };
            inst.Addresses.BusinessAddress = new AddressEntity { Addressline1 = (string)row[dataset.InstitutionsTable.ADDRESSLINE1], Addressline2 = (string)row[dataset.InstitutionsTable.ADDRESSLINE2], Suburb = (string)row[dataset.InstitutionsTable.SUBURB], PostCode = (string)row[dataset.InstitutionsTable.POSTCODE], State = (string)row[dataset.InstitutionsTable.STATE], Country = (string)row[dataset.InstitutionsTable.COUNTRY] };
        }

        private static void GetInstituteValues(InstitutesDS dataset, InstitutionEntity inst)
        {
            DataRow row = dataset.InstitutionsTable.NewRow();
            row[dataset.InstitutionsTable.NAME] = inst.Name;
            row[dataset.InstitutionsTable.DESCRIPTION] = inst.Description;
            row[dataset.InstitutionsTable.TRADINGNAME] = inst.TradingName;
            row[dataset.InstitutionsTable.LEGALNAME] = inst.LegalName;
            row[dataset.InstitutionsTable.CHKLEGALNAME] = inst.ChkLegalName;
            row[dataset.InstitutionsTable.SPCREDITRATINGLONG] = inst.SPCreditRatingLong;
            row[dataset.InstitutionsTable.SPCREDITRATINGSHORT] = inst.SPCreditRatingShort;
            row[dataset.InstitutionsTable.TYPE] = inst.Type;
            row[dataset.InstitutionsTable.ABN] = inst.ABN;
            row[dataset.InstitutionsTable.ACN] = inst.ACN;
            row[dataset.InstitutionsTable.TFN] = inst.TFN;
            row[dataset.InstitutionsTable.INSTITUTIONCODE] = inst.TextInstitutionCode;
            if (inst.PhoneNumber != null)
            {
                row[dataset.InstitutionsTable.PHONENUMBERCOUNTYCODE] = inst.PhoneNumber.CountryCode;
                row[dataset.InstitutionsTable.PHONENUMBERCITYCODE] = inst.PhoneNumber.CityCode;
                row[dataset.InstitutionsTable.PHONENUMBER] = inst.PhoneNumber.PhoneNumber;
            }

            if (inst.Facsimile != null)
            {
                row[dataset.InstitutionsTable.FACSIMILECOUNTYCODE] = inst.Facsimile.CountryCode;
                row[dataset.InstitutionsTable.FACSIMILECITYCODE] = inst.Facsimile.CityCode;
                row[dataset.InstitutionsTable.FACSIMILE] = inst.Facsimile.PhoneNumber;
            }

            if (inst.Addresses.BusinessAddress != null)
            {
                row[dataset.InstitutionsTable.ADDRESSLINE1] = inst.Addresses.BusinessAddress.Addressline1;
                row[dataset.InstitutionsTable.ADDRESSLINE2] = inst.Addresses.BusinessAddress.Addressline2;
                row[dataset.InstitutionsTable.SUBURB] = inst.Addresses.BusinessAddress.Suburb;
                row[dataset.InstitutionsTable.POSTCODE] = inst.Addresses.BusinessAddress.PostCode;
                row[dataset.InstitutionsTable.STATE] = inst.Addresses.BusinessAddress.State;
                row[dataset.InstitutionsTable.COUNTRY] = inst.Addresses.BusinessAddress.Country;
            }






            dataset.InstitutionsTable.Rows.Add(row);

        }

        private static void PopulateSecurityFromDataRow(SecuritiesEntity security, DataRow row)
        {
            security.CompanyName = GetNonNullStringValue(row, SecuritiesDS.DESCRIPTION);
            security.Rating = GetNonNullStringValue(row, SecuritiesDS.RATING);
            security.AssetID = new Guid(GetNonNullStringValue(row, SecuritiesDS.ASSET));
            security.InvestmentType = GetNonNullStringValue(row, SecuritiesDS.INVESTMENTTYPE);
            if (security.InvestmentType == "Term Depose")
                security.InvestmentType = "Term Deposit";
            security.ISINCode = GetNonNullStringValue(row, SecuritiesDS.ISINCODE);
            security.Market = GetNonNullStringValue(row, SecuritiesDS.MARKET);
            security.Status = GetNonNullStringValue(row, SecuritiesDS.STATUS);
            if (row[SecuritiesDS.UNITSHELD] != null && row[SecuritiesDS.UNITSHELD].ToString() != "")
                security.UnitSize = Convert.ToDouble(row[SecuritiesDS.UNITSHELD].ToString());
            if (row[SecuritiesDS.PRICEDATE] != null && row[SecuritiesDS.PRICEDATE].ToString() != "")
                security.IssueDate = Convert.ToDateTime(row[SecuritiesDS.PRICEDATE].ToString());
            security.AsxCode = GetNonNullStringValue(row, SecuritiesDS.CODE);
            security.PricingSource = (int)row[SecuritiesDS.PRICINGSOURCE];
            security.DistributionType = (int)row[SecuritiesDS.DISTRIBUTIONTYPE];
            security.DistributionFrequency = (int)row[SecuritiesDS.DISTRIBUTIONFREQUENCY];
            security.IsSMAApproved = (bool)row[SecuritiesDS.ISSMAAPPROVED];
            security.SMAHoldingLimit = (double)row[SecuritiesDS.SMAHOLDINGLIMIT];
        }

        public static string GetNonNullStringValue(DataRow row, string columnName)
        {
            if (row[columnName] is DBNull || ((string)row[columnName] == null))
                return "";
            else
                return row[columnName].ToString();
        }

        private static void ExtractSecirityData(SecuritiesEntity security, DataRow row)
        {
            row[SecuritiesDS.ID] = security.ID;
            row[SecuritiesDS.CODE] = security.AsxCode;
            row[SecuritiesDS.DESCRIPTION] = security.CompanyName;

            if (security.InvestmentType == "Term Depose")
                security.InvestmentType = "Term Deposit";

            row[SecuritiesDS.INVESTMENTTYPE] = security.InvestmentType;
            row[SecuritiesDS.ISINCODE] = security.ISINCode;
            row[SecuritiesDS.MARKET] = security.Market;
            row[SecuritiesDS.UNITSHELD] = security.UnitSize;
            row[SecuritiesDS.STATUS] = security.Status;
            row[SecuritiesDS.ASSET] = security.AssetID;
            row[SecuritiesDS.RATING] = security.Rating;
            row[SecuritiesDS.PRICINGSOURCE] = security.PricingSource;
            row[SecuritiesDS.DISTRIBUTIONTYPE] = security.DistributionType;
            row[SecuritiesDS.DISTRIBUTIONFREQUENCY] = security.DistributionFrequency;
            row[SecuritiesDS.ISSMAAPPROVED] = security.IsSMAApproved;
            row[SecuritiesDS.SMAHOLDINGLIMIT] = security.SMAHoldingLimit;
        }

        private void CreateCorporate(UMAFormEntity formEntiy)
        {
            bool create = false;
            IOrganizationUnit corporateObject = null;
            CorporateEntity corporateEntity = null;

            SetExstingClientID(formEntiy);

            if (formEntiy.ClientCID != Guid.Empty)
            {
                corporateObject = this.Broker.GetBMCInstance(formEntiy.ClientCID) as IOrganizationUnit;

                if (corporateObject != null)
                {
                    if (corporateObject.ClientEntity == null)
                        create = true;
                }
                else
                    create = true;

                if (!create)
                {
                    corporateEntity = corporateObject.ClientEntity as CorporateEntity;

                    foreach (SignatoryEntity signatoryEntity in formEntiy.SignatoryEntityList)
                    {
                        if (signatoryEntity.IndividualInstanceID != Guid.Empty)
                        {
                            IOrganizationUnit individualCM = this.Broker.GetBMCInstance(signatoryEntity.IndividualInstanceID) as IOrganizationUnit;
                            if (individualCM != null)
                                UpdateIndividualEntity(signatoryEntity, individualCM, formEntiy);
                            else
                            {
                                IOrganizationUnit newIndividualCM = CreateIndividual(signatoryEntity, formEntiy);
                                if (newIndividualCM != null)
                                {
                                    Oritax.TaxSimp.Common.IdentityCMDetail identityCMDetail = new Common.IdentityCMDetail();
                                    identityCMDetail.Clid = newIndividualCM.CLID;
                                    identityCMDetail.Csid = newIndividualCM.CSID;
                                    identityCMDetail.Cid = newIndividualCM.CID;

                                    if (signatoryEntity.SigNo == 1)
                                        identityCMDetail.IsPrimary = true;

                                    identityCMDetail.BusinessTitle = new TitleEntity();
                                    corporateEntity.Signatories.Add(identityCMDetail);
                                }
                            }
                        }
                        else
                        {
                            IOrganizationUnit newIndividualCM = CreateIndividual(signatoryEntity, formEntiy);
                            if (newIndividualCM != null)
                            {
                                Oritax.TaxSimp.Common.IdentityCMDetail identityCMDetail = new Common.IdentityCMDetail();
                                identityCMDetail.Clid = newIndividualCM.CLID;
                                identityCMDetail.Csid = newIndividualCM.CSID;
                                identityCMDetail.Cid = newIndividualCM.CID;

                                if (signatoryEntity.SigNo == 1)
                                    identityCMDetail.IsPrimary = true;

                                identityCMDetail.BusinessTitle = new TitleEntity();
                                corporateEntity.Signatories.Add(identityCMDetail);
                            }
                        }
                    }
                }

            }
            else
                create = true;

            if (create)
            {
                ILogicalModule created = null;
                ILogicalModule organization = null;
                created = this.Broker.CreateLogicalCM(TypeIDUMAForm(formEntiy.accountType), formEntiy.accountName, Guid.Empty, null);
                organization = this.Broker.GetLogicalCM(this.CLID);
                organization.AddChildLogicalCM(created);
                corporateObject = (IOrganizationUnit)created[created.CurrentScenario];
                corporateObject.CreateUniqueID();
                corporateObject.IsInvestableClient = true;

                formEntiy.ClientCID = corporateObject.CID;
                formEntiy.ClientID = corporateObject.ClientId;

                if (corporateObject.ClientEntity == null)
                    corporateObject.ClientEntity = new CorporateEntity();

                corporateEntity = corporateObject.ClientEntity as CorporateEntity;

                foreach (SignatoryEntity signatoryEntity in formEntiy.SignatoryEntityList)
                {
                    IOrganizationUnit individualCM = null;

                    if (signatoryEntity.SigNo == 1)
                        individualCM = CreateIndividual(signatoryEntity, formEntiy);
                    else if (signatoryEntity.SigNo == 2 && formEntiy.Sig2Required)
                        individualCM = CreateIndividual(signatoryEntity, formEntiy);
                    else if (signatoryEntity.SigNo == 3 && formEntiy.Sig3Required)
                        individualCM = CreateIndividual(signatoryEntity, formEntiy);
                    else if (signatoryEntity.SigNo == 4 && formEntiy.Sig4Required)
                        individualCM = CreateIndividual(signatoryEntity, formEntiy);

                    if (individualCM != null)
                    {
                        Oritax.TaxSimp.Common.IdentityCMDetail identityCMDetail = new Common.IdentityCMDetail();
                        identityCMDetail.Clid = individualCM.CLID;
                        identityCMDetail.Csid = individualCM.CSID;
                        identityCMDetail.Cid = individualCM.CID;

                        if (signatoryEntity.SigNo == 1)
                            identityCMDetail.IsPrimary = true;

                        identityCMDetail.BusinessTitle = new TitleEntity();
                        corporateEntity.Signatories.Add(identityCMDetail);
                    }
                }

            }

            corporateEntity.ABN = formEntiy.ABNCompany;
            corporateEntity.Name = formEntiy.accountName;
            corporateEntity.LegalName = formEntiy.accountName;
            if (formEntiy.accountType.Contains("Trust") || formEntiy.accountType.Contains("Trustee"))
                corporateEntity.LegalName = formEntiy.trustName + " ATF " + formEntiy.accountName;

            if (formEntiy.DoItWithMe)
            {
                corporateEntity.AccessFacilities.CHEQUEBOOK = formEntiy.DIWMChequeBook;
                corporateEntity.AccessFacilities.DEBITCARD = formEntiy.DIWMDebitCard;
                corporateEntity.AccessFacilities.DEPOSITBOOK = formEntiy.DIWMDepositBook;
                corporateEntity.AccessFacilities.ONLINEACCESS = formEntiy.DIWMOnlineAccess;
                corporateEntity.AccessFacilities.PHONEACCESS = formEntiy.DIWMPhoneAccess;
            }
            else if (formEntiy.DoItYourSelf)
            {
                corporateEntity.AccessFacilities.CHEQUEBOOK = formEntiy.DIYChequeBook;
                corporateEntity.AccessFacilities.DEBITCARD = formEntiy.DIYDebitCard;
                corporateEntity.AccessFacilities.DEPOSITBOOK = formEntiy.DIYDepositBook;
                corporateEntity.AccessFacilities.ONLINEACCESS = formEntiy.DIYOnlineAccess;
                corporateEntity.AccessFacilities.PHONEACCESS = formEntiy.DIYPhoneAccess;
            }

            corporateEntity.AccountDesignation = formEntiy.accountName;
            corporateEntity.ACN = formEntiy.ACN;

            corporateEntity.Address.MailingAddressSame = false;
            corporateEntity.Address.RegisteredAddressSame = false;
            corporateEntity.Address.DuplicateMailingAddressSame = false;

            corporateEntity.Address.BusinessAddress.Addressline1 = formEntiy.PrincipalAddressLine1;
            corporateEntity.Address.BusinessAddress.Addressline2 = formEntiy.PrincipalAddressLine2;
            corporateEntity.Address.BusinessAddress.Country = formEntiy.PrincipalCountry;
            corporateEntity.Address.BusinessAddress.PostCode = formEntiy.PrincipalPostCode;
            corporateEntity.Address.BusinessAddress.State = formEntiy.PrincipalState;
            corporateEntity.Address.BusinessAddress.Suburb = formEntiy.PrincipalSuburb;

            corporateEntity.Address.MailingAddress.Addressline1 = formEntiy.MialingAddressLine1;
            corporateEntity.Address.MailingAddress.Addressline2 = formEntiy.MialingAddressLine2;
            corporateEntity.Address.MailingAddress.Country = formEntiy.MialingCountry;
            corporateEntity.Address.MailingAddress.PostCode = formEntiy.MialingPostCode;
            corporateEntity.Address.MailingAddress.State = formEntiy.MialingState;
            corporateEntity.Address.MailingAddress.Suburb = formEntiy.MialingSuburb;

            corporateEntity.Address.DuplicateMailingAddress.Addressline1 = formEntiy.DupMialingAddressLine1;
            corporateEntity.Address.DuplicateMailingAddress.Addressline2 = formEntiy.DupMialingAddressLine2;
            corporateEntity.Address.DuplicateMailingAddress.Country = formEntiy.DupMialingCountry;
            corporateEntity.Address.DuplicateMailingAddress.PostCode = formEntiy.DupMialingPostCode;
            corporateEntity.Address.DuplicateMailingAddress.State = formEntiy.DupMialingState;
            corporateEntity.Address.DuplicateMailingAddress.Suburb = formEntiy.DupMialingSuburb;

            corporateEntity.Address.RegisteredAddress.Addressline1 = formEntiy.RegAddressLine1;
            corporateEntity.Address.RegisteredAddress.Addressline2 = formEntiy.RegAddressLine2;
            corporateEntity.Address.RegisteredAddress.Country = formEntiy.RegCountry;
            corporateEntity.Address.RegisteredAddress.PostCode = formEntiy.RegPostCode;
            corporateEntity.Address.RegisteredAddress.State = formEntiy.RegState;
            corporateEntity.Address.RegisteredAddress.Suburb = formEntiy.RegSuburb;

            corporateEntity.ClientName = formEntiy.accountName;

            corporateEntity.TFN = formEntiy.TFNTrustFund.RemoveWhiteSpacesStartEndInBetween();
            if (formEntiy.TFNTrustFund.RemoveWhiteSpacesStartEndInBetween() == string.Empty)
                corporateEntity.TFN = formEntiy.TFNSig1.RemoveWhiteSpacesStartEndInBetween();

            corporateEntity.TrustABN = formEntiy.ABNCompany;
            corporateObject.IsInvestableClient = true;
            corporateObject.CalculateToken(true);
        }

        private void SetExstingClientID(UMAFormEntity formEntiy)
        {
            if (formEntiy.ClientCID == Guid.Empty)
            {

                IBrokerManagedComponent user = this.Broker.GetBMCInstance("Administrator", "DBUser_1_1");
                object[] args = new Object[3] { this.CLID, user.CID, EnableSecurity.SecuritySetting };
                DataView view = new DataView(this.Broker.GetBMCDetailList(OrganizationInstall.ASSEMBLY_NAME, args).Tables[TableName]);
                view.RowFilter = OrganisationListingDS.GetRowFilter("");
                view.Sort = "ENTITYNAME_FIELD ASC";
                DataTable orgFiteredTable = view.ToTable();

                DataView entityView = new DataView(orgFiteredTable);
                entityView.RowFilter = "ENTITYNAME_FIELD='" + formEntiy.accountName + "'";
                DataTable entityTable = entityView.ToTable();
                if (entityTable.Rows.Count > 0)
                {
                    Guid cid = new Guid(entityTable.Rows[0]["ENTITYCIID_FIELD"].ToString());
                    IOrganizationUnit iorg = this.Broker.GetBMCInstance(cid) as IOrganizationUnit;
                    formEntiy.ClientCID = iorg.CID;
                    formEntiy.ClientID = iorg.ClientId;
                    formEntiy.UMAFormStatus = UMAFormStatus.Finalised;
                    this.Broker.ReleaseBrokerManagedComponent(iorg);
                }
                else
                {
                    formEntiy.ClientCID = Guid.Empty;
                    formEntiy.ClientID = String.Empty;
                }
            }
        }

        private void CreateIndividual(UMAFormEntity formEntiy)
        {
            bool create = false;

            IOrganizationUnit corporateObject = null;
            ClientIndividualEntity clientIndividualEntity = null;

            SetExstingClientID(formEntiy);

            if (formEntiy.ClientCID != Guid.Empty)
            {
                corporateObject = this.Broker.GetBMCInstance(formEntiy.ClientCID) as IOrganizationUnit;

                if (corporateObject != null)
                {
                    if (corporateObject.ClientEntity == null)
                        create = true;
                }
                else
                    create = true;

                if (!create)
                {
                    clientIndividualEntity = corporateObject.ClientEntity as ClientIndividualEntity;

                    foreach (SignatoryEntity signatoryEntity in formEntiy.SignatoryEntityList)
                    {

                        if (signatoryEntity.IndividualInstanceID != Guid.Empty)
                        {
                            IOrganizationUnit individualCM = this.Broker.GetBMCInstance(signatoryEntity.IndividualInstanceID) as IOrganizationUnit;
                            if (individualCM != null)
                                UpdateIndividualEntity(signatoryEntity, individualCM, formEntiy);
                            else
                            {
                                IOrganizationUnit newIndividualCM = CreateIndividual(signatoryEntity, formEntiy);
                                if (newIndividualCM != null)
                                {
                                    Oritax.TaxSimp.Common.IdentityCMDetail identityCMDetail = new Common.IdentityCMDetail();
                                    identityCMDetail.Clid = newIndividualCM.CLID;
                                    identityCMDetail.Csid = newIndividualCM.CSID;
                                    identityCMDetail.Cid = newIndividualCM.CID;

                                    if (signatoryEntity.SigNo == 1)
                                        identityCMDetail.IsPrimary = true;
                                    identityCMDetail.BusinessTitle = new TitleEntity();
                                    clientIndividualEntity.Applicants.Add(identityCMDetail);
                                }
                            }
                        }
                        else
                        {
                            IOrganizationUnit newIndividualCM = CreateIndividual(signatoryEntity, formEntiy);
                            if (newIndividualCM != null)
                            {
                                Oritax.TaxSimp.Common.IdentityCMDetail identityCMDetail = new Common.IdentityCMDetail();
                                identityCMDetail.Clid = newIndividualCM.CLID;
                                identityCMDetail.Csid = newIndividualCM.CSID;
                                identityCMDetail.Cid = newIndividualCM.CID;

                                if (signatoryEntity.SigNo == 1)
                                    identityCMDetail.IsPrimary = true;
                                identityCMDetail.BusinessTitle = new TitleEntity();
                                clientIndividualEntity.Applicants.Add(identityCMDetail);
                            }
                        }
                    }
                }

            }
            else
                create = true;

            if (create)
            {
                ILogicalModule created = null;
                ILogicalModule organization = null;

                created = this.Broker.CreateLogicalCM(TypeIDUMAForm(formEntiy.accountType), formEntiy.accountName, Guid.Empty, null);
                organization = this.Broker.GetLogicalCM(this.CLID);
                organization.AddChildLogicalCM(created);
                corporateObject = (IOrganizationUnit)created[created.CurrentScenario];
                corporateObject.CreateUniqueID();
                formEntiy.ClientCID = corporateObject.CID;
                formEntiy.ClientID = corporateObject.ClientId;

                if (corporateObject.ClientEntity == null)
                    corporateObject.ClientEntity = new ClientIndividualEntity();

                clientIndividualEntity = corporateObject.ClientEntity as ClientIndividualEntity;

                foreach (SignatoryEntity signatoryEntity in formEntiy.SignatoryEntityList)
                {
                    IOrganizationUnit individualCM = null;

                    if (signatoryEntity.SigNo == 1)
                        individualCM = CreateIndividual(signatoryEntity, formEntiy);
                    else if (signatoryEntity.SigNo == 2 && formEntiy.Sig2Required)
                        individualCM = CreateIndividual(signatoryEntity, formEntiy);
                    else if (signatoryEntity.SigNo == 3 && formEntiy.Sig3Required)
                        individualCM = CreateIndividual(signatoryEntity, formEntiy);
                    else if (signatoryEntity.SigNo == 4 && formEntiy.Sig4Required)
                        individualCM = CreateIndividual(signatoryEntity, formEntiy);

                    if (individualCM != null)
                    {
                        Oritax.TaxSimp.Common.IdentityCMDetail identityCMDetail = new Common.IdentityCMDetail();
                        identityCMDetail.Clid = individualCM.CLID;
                        identityCMDetail.Csid = individualCM.CSID;
                        identityCMDetail.Cid = individualCM.CID;

                        if (signatoryEntity.SigNo == 1)
                            identityCMDetail.IsPrimary = true;

                        identityCMDetail.BusinessTitle = new TitleEntity();
                        clientIndividualEntity.Applicants.Add(identityCMDetail);
                    }
                }

            }

            clientIndividualEntity.ABN = formEntiy.ABNCompany;
            clientIndividualEntity.Name = formEntiy.accountName;
            clientIndividualEntity.LegalName = formEntiy.accountName;

            if (formEntiy.accountType.Contains("Trust") || formEntiy.accountType.Contains("Trustee"))
            {
                clientIndividualEntity.TFN = formEntiy.TFNTrustFund;
                clientIndividualEntity.LegalName = formEntiy.trustName + " ATF " + formEntiy.accountName;
            }

            clientIndividualEntity.TFN = formEntiy.TFNSig1;

            if (formEntiy.DoItWithMe)
            {
                clientIndividualEntity.AccessFacilities.CHEQUEBOOK = formEntiy.DIWMChequeBook;
                clientIndividualEntity.AccessFacilities.DEBITCARD = formEntiy.DIWMDebitCard;
                clientIndividualEntity.AccessFacilities.DEPOSITBOOK = formEntiy.DIWMDepositBook;
                clientIndividualEntity.AccessFacilities.ONLINEACCESS = formEntiy.DIWMOnlineAccess;
                clientIndividualEntity.AccessFacilities.PHONEACCESS = formEntiy.DIWMPhoneAccess;
            }
            else if (formEntiy.DoItYourSelf)
            {
                clientIndividualEntity.AccessFacilities.CHEQUEBOOK = formEntiy.DIYChequeBook;
                clientIndividualEntity.AccessFacilities.DEBITCARD = formEntiy.DIYDebitCard;
                clientIndividualEntity.AccessFacilities.DEPOSITBOOK = formEntiy.DIYDepositBook;
                clientIndividualEntity.AccessFacilities.ONLINEACCESS = formEntiy.DIYOnlineAccess;
                clientIndividualEntity.AccessFacilities.PHONEACCESS = formEntiy.DIYPhoneAccess;
            }

            clientIndividualEntity.AccountDesignation = formEntiy.accountName;

            clientIndividualEntity.Address.MailingAddressSame = false;
            clientIndividualEntity.Address.RegisteredAddressSame = false;
            clientIndividualEntity.Address.DuplicateMailingAddressSame = false;

            clientIndividualEntity.Address.BusinessAddress.Addressline1 = formEntiy.PrincipalAddressLine1;
            clientIndividualEntity.Address.BusinessAddress.Addressline2 = formEntiy.PrincipalAddressLine2;
            clientIndividualEntity.Address.BusinessAddress.Country = formEntiy.PrincipalCountry;
            clientIndividualEntity.Address.BusinessAddress.PostCode = formEntiy.PrincipalPostCode;
            clientIndividualEntity.Address.BusinessAddress.State = formEntiy.PrincipalState;
            clientIndividualEntity.Address.BusinessAddress.Suburb = formEntiy.PrincipalSuburb;

            clientIndividualEntity.Address.MailingAddress.Addressline1 = formEntiy.MialingAddressLine1;
            clientIndividualEntity.Address.MailingAddress.Addressline2 = formEntiy.MialingAddressLine2;
            clientIndividualEntity.Address.MailingAddress.Country = formEntiy.MialingCountry;
            clientIndividualEntity.Address.MailingAddress.PostCode = formEntiy.MialingPostCode;
            clientIndividualEntity.Address.MailingAddress.State = formEntiy.MialingState;
            clientIndividualEntity.Address.MailingAddress.Suburb = formEntiy.MialingSuburb;

            clientIndividualEntity.Address.DuplicateMailingAddress.Addressline1 = formEntiy.DupMialingAddressLine1;
            clientIndividualEntity.Address.DuplicateMailingAddress.Addressline2 = formEntiy.DupMialingAddressLine2;
            clientIndividualEntity.Address.DuplicateMailingAddress.Country = formEntiy.DupMialingCountry;
            clientIndividualEntity.Address.DuplicateMailingAddress.PostCode = formEntiy.DupMialingPostCode;
            clientIndividualEntity.Address.DuplicateMailingAddress.State = formEntiy.DupMialingState;
            clientIndividualEntity.Address.DuplicateMailingAddress.Suburb = formEntiy.DupMialingSuburb;

            clientIndividualEntity.Address.RegisteredAddress.Addressline1 = formEntiy.RegAddressLine1;
            clientIndividualEntity.Address.RegisteredAddress.Addressline2 = formEntiy.RegAddressLine2;
            clientIndividualEntity.Address.RegisteredAddress.Country = formEntiy.RegCountry;
            clientIndividualEntity.Address.RegisteredAddress.PostCode = formEntiy.RegPostCode;
            clientIndividualEntity.Address.RegisteredAddress.State = formEntiy.RegState;
            clientIndividualEntity.Address.RegisteredAddress.Suburb = formEntiy.RegSuburb;

            clientIndividualEntity.Name = formEntiy.accountName;
            clientIndividualEntity.LegalName = formEntiy.accountName;
            corporateObject.IsInvestableClient = true;


            corporateObject.CalculateToken(true);
        }

        private IOrganizationUnit CreateIndividual(SignatoryEntity signatoryEntity, UMAFormEntity formEntiy)
        {
            string individualName = signatoryEntity.GivenName + signatoryEntity.MiddleName + signatoryEntity.FamilyName;

            ILogicalModule created = null;
            ILogicalModule organization = null;

            created = this.Broker.CreateLogicalCM(new Guid("9EC83666-D774-4BFE-895C-1E8D42325C50"), individualName, Guid.Empty, null);
            organization = this.Broker.GetLogicalCM(this.CLID);
            organization.AddChildLogicalCM(created);
            IOrganizationUnit individualCM = (IOrganizationUnit)created[created.CurrentScenario];
            individualCM.CreateUniqueID();
            signatoryEntity.IndividualInstanceID = individualCM.CID;
            return UpdateIndividualEntity(signatoryEntity, individualCM, formEntiy);
        }

        private static IOrganizationUnit UpdateIndividualEntity(SignatoryEntity signatoryEntity, IOrganizationUnit individualCM, UMAFormEntity formEntiy)
        {
            if (individualCM.ClientEntity == null)
                individualCM.ClientEntity = new Oritax.TaxSimp.Common.IndividualEntity();

            Oritax.TaxSimp.Common.IndividualEntity individualEntity = individualCM.ClientEntity as Oritax.TaxSimp.Common.IndividualEntity;

            individualEntity.Surname = signatoryEntity.FamilyName;
            individualEntity.Name = signatoryEntity.GivenName;
            individualEntity.MiddleName = signatoryEntity.MiddleName;
            individualEntity.MobilePhoneNumber = PhoneNumberEntity.MobileNumberConvertor(signatoryEntity.MobilePH);
            individualEntity.WorkPhoneNumber = PhoneNumberEntity.PhoneNumberConvertor(signatoryEntity.AlternativePH);
            individualEntity.HomePhoneNumber = PhoneNumberEntity.PhoneNumberConvertor(signatoryEntity.ContactPH);
            individualEntity.PersonalTitle.Title = signatoryEntity.Title;
            signatoryEntity.DateOfBirth = signatoryEntity.DateOfBirth.Trim();

            DateTime theDateTime = new DateTime();
            IFormatProvider theCultureInfo = new CultureInfo("en-AU", true);

            if (signatoryEntity.DateOfBirth != String.Empty)
                theDateTime = DateTime.ParseExact(signatoryEntity.DateOfBirth, "d/MM/yyyy", CultureInfo.InvariantCulture);

            if (theDateTime != null)
                individualEntity.DOB = theDateTime;

            if (signatoryEntity.DocumentNoPassport != string.Empty)
            {
                if (individualEntity.DocAustralianPassport == null)
                    individualEntity.DocAustralianPassport = new DocumentTypeEntity();

                individualEntity.HasDocAustralianPassport = true;
                individualEntity.DocAustralianPassport.Type = "Australian Passport (Current or < 2 Years Expired)";
                individualEntity.DocAustralianPassport.DocNumber = signatoryEntity.DocumentNoPassport;

                DateTime dateOfBirthPass;
                if (DateTime.TryParse(signatoryEntity.DateOfBirth, out dateOfBirthPass))
                    individualEntity.DocAustralianPassport.DOB = dateOfBirthPass;

                DateTime dateofIssuePass;
                if (DateTime.TryParse(signatoryEntity.PassportIssueDate, out dateofIssuePass))
                    individualEntity.DocAustralianPassport.IssueDate = dateofIssuePass;

                DateTime dateofExpPass;
                if (DateTime.TryParse(signatoryEntity.PassportExpiryDate, out dateofExpPass))
                    individualEntity.DocAustralianPassport.ExpiryDate = dateofExpPass;

                individualEntity.DocAustralianPassport.IssuePlace = signatoryEntity.PassportDocumentIssuer;
                individualEntity.DocAustralianPassport.IsAddressMatch = true;
                individualEntity.DocAustralianPassport.IsPhotoShown = true;
                individualEntity.DocAustralianPassport.CertifiedCopy = true;
            }
            else
            {
                individualEntity.HasDocAustralianPassport = false;
                individualEntity.DocAustralianPassport.Type = "";
                individualEntity.DocAustralianPassport.DocNumber = string.Empty;
                individualEntity.DocAustralianPassport.DOB = null;
                individualEntity.DocAustralianPassport.IssueDate = null;
                individualEntity.DocAustralianPassport.ExpiryDate = null;
                individualEntity.DocAustralianPassport.IssuePlace = string.Empty;
                individualEntity.DocAustralianPassport.IsAddressMatch = false;
                individualEntity.DocAustralianPassport.IsPhotoShown = false;
                individualEntity.DocAustralianPassport.CertifiedCopy = false;
            }

            if (signatoryEntity.DocumentNoLic != string.Empty)
            {
                if (individualEntity.DocDrivingLicence == null)
                    individualEntity.DocDrivingLicence = new DocumentTypeEntity();

                individualEntity.HasDocDrivingLicence = true;
                individualEntity.DocDrivingLicence.Type = "Certified Copy of Current Australian Drivers Licence";
                individualEntity.DocDrivingLicence.DocNumber = signatoryEntity.DocumentNoLic;

                DateTime dateOfBirthLic;
                if (DateTime.TryParse(signatoryEntity.DateOfBirth, out dateOfBirthLic))
                    individualEntity.DocDrivingLicence.DOB = dateOfBirthLic;

                DateTime dateofIssueLic;
                if (DateTime.TryParse(signatoryEntity.LicIssueDate, out dateofIssueLic))
                    individualEntity.DocDrivingLicence.IssueDate = dateofIssueLic;

                DateTime dateofExpLic;
                if (DateTime.TryParse(signatoryEntity.LicExpiryDate, out dateofExpLic))
                    individualEntity.DocDrivingLicence.ExpiryDate = dateofExpLic;

                individualEntity.DocDrivingLicence.IssuePlace = signatoryEntity.LicDocumentIssuer;
                individualEntity.DocDrivingLicence.IsAddressMatch = true;
                individualEntity.DocDrivingLicence.IsPhotoShown = true;
                individualEntity.DocDrivingLicence.CertifiedCopy = true;
            }
            else
            {
                individualEntity.HasDocDrivingLicence = false;
                individualEntity.DocDrivingLicence.Type = "";
                individualEntity.DocDrivingLicence.DocNumber = string.Empty;
                individualEntity.DocDrivingLicence.DOB = null;
                individualEntity.DocDrivingLicence.IssueDate = null;
                individualEntity.DocDrivingLicence.ExpiryDate = null;
                individualEntity.DocDrivingLicence.IssuePlace = string.Empty;
                individualEntity.DocDrivingLicence.IsAddressMatch = false;
                individualEntity.DocDrivingLicence.IsPhotoShown = false;
                individualEntity.DocDrivingLicence.CertifiedCopy = false;
            }

            individualEntity.EmailAddress = signatoryEntity.Email;
            individualEntity.Employer = signatoryEntity.Employer;
            individualEntity.Facsimile.PhoneNumber = signatoryEntity.Fax;

            individualEntity.MailingAddressSame = false;
            individualEntity.MailingAddress.Addressline1 = signatoryEntity.MailAddressLine1;
            individualEntity.MailingAddress.Addressline2 = signatoryEntity.MailAddressLine2;
            individualEntity.MailingAddress.Country = signatoryEntity.MailCountry;
            individualEntity.MailingAddress.PostCode = signatoryEntity.MailPostCode;
            individualEntity.MailingAddress.State = signatoryEntity.MailState;
            individualEntity.MailingAddress.Suburb = signatoryEntity.MailSuburb;

            individualEntity.ResidentialAddress.Addressline1 = signatoryEntity.ResiAddressLine1;
            individualEntity.ResidentialAddress.Addressline2 = signatoryEntity.ResiAddressLine2;
            individualEntity.ResidentialAddress.Country = signatoryEntity.ResiCountry;
            individualEntity.ResidentialAddress.PostCode = signatoryEntity.ResiPostCode;
            individualEntity.ResidentialAddress.State = signatoryEntity.ResiState;
            individualEntity.ResidentialAddress.Suburb = signatoryEntity.ResiSuburb;

            if (signatoryEntity.SigNo == 1)
                individualEntity.TFN = formEntiy.TFNSig1;
            else if (signatoryEntity.SigNo == 2)
                individualEntity.TFN = formEntiy.TFNSig2;
            else if (signatoryEntity.SigNo == 3)
                individualEntity.TFN = formEntiy.TFNSig3;
            else if (signatoryEntity.SigNo == 4)
                individualEntity.TFN = formEntiy.TFNSig4;


            individualCM.CalculateToken(true);

            return individualCM;
        }

        protected override void OnGetData(DataSet data)
        {
            base.OnGetData(data);

            if (data is UMAFeesDS)
            {
                UMAFeesDS umaFeesDS = (UMAFeesDS)data;
                DataTable umaFeeListTable = umaFeesDS.Tables[UMAFeesDS.FEELISTABLE];

                foreach (FeeEntity feeEntity in this._FeeEnttyList)
                {
                    DataRow row = umaFeeListTable.NewRow();
                    row[UMAFeesDS.ID] = feeEntity.ID;
                    row[UMAFeesDS.COMMENTS] = feeEntity.Comments;
                    row[UMAFeesDS.DESCRIPTION] = feeEntity.Description;
                    row[UMAFeesDS.YEAR] = feeEntity.Year;
                    row[UMAFeesDS.FEETYPE] = Enumeration.GetDescription(feeEntity.FeeType);
                    row[UMAFeesDS.FEETYPEENUM] = feeEntity.FeeType;
                    row[UMAFeesDS.SERVICESTYPE] = Enumeration.GetDescription(feeEntity.ServiceTypes);
                    row[UMAFeesDS.SERVICESTYPEENUM] = feeEntity.ServiceTypes;
                    row[UMAFeesDS.STARTDATE] = feeEntity.StartDate;
                    row[UMAFeesDS.ENDDATE] = feeEntity.EndDate;
                    row[UMAFeesDS.FEETYPEENUM] = feeEntity.FeeType;

                    umaFeeListTable.Rows.Add(row);
                }

                umaFeesDS.FeeEnttyList = this._FeeEnttyList;
            }
            else if (data is UMAFeesDetailsDS)
            {
                UMAFeesDetailsDS umaFeesDetailsDS = (UMAFeesDetailsDS)data;
                var selectedFeeEntity = this._FeeEnttyList.Where(fee => fee.ID == umaFeesDetailsDS.FeeEntityID).FirstOrDefault();
                umaFeesDetailsDS.FeeEntity = selectedFeeEntity;

                DataTable productsList = umaFeesDetailsDS.Tables[UMAFeesDetailsDS.PRODUCTSTABLENAME];
                if (selectedFeeEntity.ProductList != null)
                {
                    foreach (Guid productID in selectedFeeEntity.ProductList)
                    {
                        var product = this._Products.Where(pro => pro.ID == productID).FirstOrDefault();

                        if (product != null)
                        {
                            DataRow proRow = productsList.NewRow();
                            proRow[UMAFeesDetailsDS.PROID] = product.ID;
                            proRow[UMAFeesDetailsDS.PRONAME] = product.Description;
                            productsList.Rows.Add(proRow);
                        }
                    }
                }
            }
            else if (data is ProductsDS)
            {
                var ds = (data as ProductsDS);
                DataTable d = ds.Tables[ProductsDS.PRODUCTSTABLE];
                DataTable table = ds.Tables[InvestmentDS.INVESTMENTMANAGEMENT];
                foreach (ProductEntity pe in this.Products)
                {
                    DataRow r = d.NewRow();
                    ExtractProductEntityData(pe, r);

                    d.Rows.Add(r);
                }
            }
            else if (data is AssetDS)
            {
                var ds = (data as AssetDS);
                DataTable d = ds.Tables[AssetDS.TABLENAME];
                foreach (AssetEntity pe in this.Assets)
                {
                    DataRow r = d.NewRow();
                    r[ProductsDS.ID] = pe.ID;
                    r[ProductsDS.NAME] = pe.Name;
                    r[ProductsDS.DESCRIPTION] = pe.Description;
                    d.Rows.Add(r);
                }
            }
            else if (data is BusinessGroupsDS)
            {
                var ds = (data as BusinessGroupsDS);
                DataTable d = ds.Tables[BusinessGroupsDS.TABLENAME];
                foreach (BusinessGroupEntity bge in this.BusinessGroups)
                {
                    DataRow r = d.NewRow();
                    r[BusinessGroupsDS.ID] = bge.ID;
                    r[BusinessGroupsDS.NAME] = bge.Name;
                    r[BusinessGroupsDS.DESCRIPTION] = bge.Description;
                    d.Rows.Add(r);
                }
            }
            else if (data is StatusDS)
            {
                var ds = (data as StatusDS);
                DataTable d = ds.Tables[StatusDS.TABLENAME];

                List<Status> SortedList = this.StatusList.OrderBy(o => o.Type).ToList();

                if (ds.TypeFilter!=null && ds.TypeFilter.Trim()!=string.Empty)
                    SortedList = SortedList.Where(ss => ss.Type == ds.TypeFilter).ToList();

                foreach (Status st in SortedList)
                {
                    DataRow r = d.NewRow();
                    r[StatusDS.NAME] = st.Name;
                    r[StatusDS.TYPE] = st.Type;
                    d.Rows.Add(r);
                }
            }
            else if (data is BusinessTitleDS)
            {
                var ds = (data as BusinessTitleDS);

                foreach (var entry in this.BussinessTitles)
                {
                    DataRow r = ds.BTitleTable.NewRow();
                    r[ds.BTitleTable.TITLE] = entry.Key;
                    r[ds.BTitleTable.Value] = entry.Value;
                    ds.BTitleTable.Rows.Add(r);
                }
            }
            else if (data is OrganizationalSettingsDS)
            {
                var ds = (OrganizationalSettingsDS)data;
                var table = ds.OrganizationalSettingTable;


                if (ds.CommandType == DatasetCommandTypes.Get)
                {
                    if (OrganizationalSettings == null)
                    {
                        OrganizationalSettings = new OrganizationalSettings();
                    }
                    var row = table.NewRow();
                    row[table.SENDORDERSTOSMA] = OrganizationalSettings.SendToSMAService;
                    row[table.READFROMSMA] = OrganizationalSettings.ReadFromSMAService;
                    table.Rows.Add(row);
                }
            }
            else if (data is CSVExportDS)
            {
                ExportFinSimplicityHoldings exportFinSimplicityHoldings = new ExportFinSimplicityHoldings();
                exportFinSimplicityHoldings.Organization = this;

                CSVExportDS csvExportDS = (CSVExportDS)data;
                csvExportDS.CSVString = exportFinSimplicityHoldings.DoAction("");
            }
            else if (data is OrganisationListingDS)
            {
                OrganisationListingDS organisationListingDS = (OrganisationListingDS)data;

                string userName = "Administrator";

                if (this.Broker.UserContext != null)
                    userName = this.Broker.UserContext.Identity.Name;

                ExtractOrgnisationListingDS(organisationListingDS, userName);
            }
            else if (data is UMAFormsListingDS)
            {
                UMAFormsListingDS UMAFormsListingDS = (UMAFormsListingDS)data;

                foreach (UMAFormEntity UMAFormEntity in this._UMAFormEntityList)
                    UMAFormEntity.ExtractData(UMAFormsListingDS);
            }
            else if (data is ExportAppFormsDS)
            {
                ExportAppFormsDS exportAppFormsDS = (ExportAppFormsDS)data;

                if (exportAppFormsDS.SearchByCID)
                {
                    var formOrg = this._UMAFormEntityList.Where(forg => forg.ClientCID == exportAppFormsDS.ClientCID).FirstOrDefault();

                    if (formOrg != null)
                    {
                        exportAppFormsDS.Contents = ExportFormUtilities.GetFilledForms(formOrg, Broker, exportAppFormsDS.serviceType);
                        exportAppFormsDS.AppNo = formOrg.accountNO;
                    }
                }
                else
                {
                    var formOrg = this._UMAFormEntityList.Where(forg => forg.accountNO.ToLower() == exportAppFormsDS.AppNo.ToLower()).FirstOrDefault();

                    if (formOrg != null)
                        exportAppFormsDS.Contents = ExportFormUtilities.GetFilledForms(formOrg, Broker, exportAppFormsDS.serviceType);
                }
            }
            else if (data is UMAFormDS)
            {
                UMAFormDS UMAFormDS = (UMAFormDS)data;
                DataTable adviserFeeTable = UMAFormDS.Tables[UMAFormDS.ADVISERFEELISTING];
                if (UMAFormDS.UMAFormDSOperation == UMAFormDSOperation.ExportAdviserFees)
                {
                    foreach (var formEntiy in this._UMAFormEntityList)
                    {
                        DataRow row = adviserFeeTable.NewRow();
                        row[UMAFormDS.APPNOP] = formEntiy.accountNO.ToUpper();

                        row[UMAFormDS.CLIENTID] = formEntiy.ClientID.ToUpper();
                        row[UMAFormDS.ACCOUNTNAME] = formEntiy.accountName;
                        row[UMAFormDS.ADVISERNAME] = formEntiy.userName.ToUpper();
                        row[UMAFormDS.UPFRONTFEE] = formEntiy.UpFrontFee.ToString("C");
                        row[UMAFormDS.ONGOINGFEEVALUE] = formEntiy.OngoingFeeDollar.ToString("C");
                        row[UMAFormDS.ONGOINGFEEPERCENT] = formEntiy.OngoingFeePercentage.ToString("N4");
                        row[UMAFormDS.FROMTIERFEE1] = formEntiy.Tier1From.ToString("C");
                        row[UMAFormDS.TOTIERFEE1] = formEntiy.Tier1To.ToString("C");
                        row[UMAFormDS.FEEPERCENT1] = formEntiy.Tier1Percentage.ToString("N4");
                        row[UMAFormDS.FROMTIERFEE2] = formEntiy.Tier2From.ToString("C");
                        row[UMAFormDS.TOTIERFEE2] = formEntiy.Tier2To.ToString("C");
                        row[UMAFormDS.FEEPERCENT2] = formEntiy.Tier2Percentage.ToString("N4");
                        row[UMAFormDS.FROMTIERFEE3] = formEntiy.Tier3From.ToString("C");
                        row[UMAFormDS.TOTIERFEE3] = formEntiy.Tier3To.ToString("C");
                        row[UMAFormDS.FEEPERCENT3] = formEntiy.Tier3Percentage.ToString("N4");
                        row[UMAFormDS.FROMTIERFEE4] = formEntiy.Tier4From.ToString("C");
                        row[UMAFormDS.TOTIERFEE4] = formEntiy.Tier4To.ToString("C");
                        row[UMAFormDS.FEEPERCENT4] = formEntiy.Tier4Percentage.ToString("N4");
                        row[UMAFormDS.FROMTIERFEE5] = formEntiy.Tier5From.ToString("C");
                        row[UMAFormDS.TOTIERFEE5] = formEntiy.Tier5To.ToString("C");
                        row[UMAFormDS.FEEPERCENT5] = formEntiy.Tier5Percentage.ToString("N4");
                        row[UMAFormDS.STATUS] = Enumeration.GetDescription(formEntiy.UMAFormStatus);
                        adviserFeeTable.Rows.Add(row);
                    }
                }
                else
                {
                    var formOrg = this._UMAFormEntityList.Where(forg => forg.accountNO.ToLower() == UMAFormDS.AppNo.ToLower()).FirstOrDefault();
                    UMAFormDS.UMAFormEntity = formOrg;
                }
            }
            else if (data is ModelsDS)
            {
                ModelsDS modelsDS = (ModelsDS)data;

                if (modelsDS.ModelDataSetOperationType == ModelDataSetOperationType.ModelExport)
                {
                    foreach (ModelEntity modelEntity in Model)
                    {
                        foreach (AssetEntity assetEntity in modelEntity.Assets)
                        {
                            foreach (ProductEntity productEntity in assetEntity.Products)
                            {
                                DataTable modellistTable = modelsDS.Tables[ModelsDS.MODELLISTTABLEASSETPRODUCTS];
                                DataRow row = modellistTable.NewRow();
                                row[ModelsDS.ID] = modelEntity.ID;
                                row[ModelsDS.DESCRIPTION] = modelEntity.Description;
                                row[ModelsDS.DYNAMICPERCENTAGE] = modelEntity.DynamicPercentage;
                                row[ModelsDS.MAXALLOCATION] = modelEntity.MaxAllocation;
                                row[ModelsDS.MINALLOCATION] = modelEntity.MinAllocation;
                                row[ModelsDS.MODELNAME] = modelEntity.Name;
                                row[ModelsDS.PROGRAMCODE] = modelEntity.ProgramCode;
                                row[ModelsDS.SHAREPERCENTAGE] = modelEntity.SharePercentage;
                                row[ModelsDS.SERVICETYPE] = Enumeration.GetDescription(modelEntity.ServiceType);
                                row[ModelsDS.ISPRIVATE] = modelEntity.IsPrivate;
                                row[ModelsDS.ASSETID] = assetEntity.ID;
                                row[ModelsDS.ASSETDESCRIPTION] = assetEntity.Description;
                                row[ModelsDS.ASSETDYNAMICPERCENTAGE] = assetEntity.DynamicPercentage;
                                row[ModelsDS.ASSETSHAREPERCENTAGE] = assetEntity.SharePercentage;
                                row[ModelsDS.ASSETMAXALLOCATION] = assetEntity.MaxAllocation;
                                row[ModelsDS.ASSETMINALLOCATION] = assetEntity.MinAllocation;
                                row[ModelsDS.ASSETMODELNAME] = assetEntity.Name;
                                row[ModelsDS.ASSETID] = assetEntity.ID;
                                row[ModelsDS.PROID] = productEntity.ID;
                                row[ModelsDS.PRODESCRIPTION] = productEntity.Description;
                                row[ModelsDS.PRODYNAMICPERCENTAGE] = productEntity.DynamicPercentage;
                                row[ModelsDS.PROSHAREPERCENTAGE] = productEntity.SharePercentage;
                                row[ModelsDS.PROMAXALLOCATION] = productEntity.MaxAllocation;
                                row[ModelsDS.PROMINALLOCATION] = productEntity.MinAllocation;
                                row[ModelsDS.PROMODELNAME] = productEntity.Name;
                                modellistTable.Rows.Add(row);
                            }
                        }

                    }
                }
                else if (modelsDS.GetDetails)
                {
                    var model = this.Model.Where(details => details.ID == modelsDS.DetailsGUID).FirstOrDefault();

                    DataTable modelTable = modelsDS.Tables[ModelsDS.MODELTABLE];
                    DataRow modelRow = modelTable.NewRow();
                    modelRow[ModelsDS.ID] = model.ID;
                    modelRow[ModelsDS.PROGRAMCODE] = model.ProgramCode;
                    modelRow[ModelsDS.MODELNAME] = model.Name;
                    modelRow[ModelsDS.SERVICETYPE] = model.ServiceType;
                    modelRow[ModelsDS.ISPRIVATE] = model.IsPrivate;
                    modelTable.Rows.Add(modelRow);

                    foreach (AssetEntity assetEntity in model.Assets)
                    {
                        DataTable modelDetailTable = modelsDS.Tables[ModelsDS.MODELDETAILSLISTASSETS];
                        DataRow row = modelDetailTable.NewRow();
                        row[ModelsDS.ASSETID] = assetEntity.ID;
                        row[ModelsDS.ASSETDESCRIPTION] = assetEntity.Description;
                        row[ModelsDS.ASSETDYNAMICPERCENTAGE] = assetEntity.DynamicPercentage;
                        row[ModelsDS.ASSETSHAREPERCENTAGE] = assetEntity.SharePercentage;
                        row[ModelsDS.ASSETMAXALLOCATION] = assetEntity.MaxAllocation;
                        row[ModelsDS.ASSETMINALLOCATION] = assetEntity.MinAllocation;
                        row[ModelsDS.ASSETMODELNAME] = assetEntity.Name;

                        modelDetailTable.Rows.Add(row);

                        if (assetEntity.Products.Count > 0)
                        {
                            foreach (ProductEntity productEntity in assetEntity.Products)
                            {
                                DataTable modelDetailTablePro = modelsDS.Tables[ModelsDS.MODELDETAILSLISTPRO];
                                DataRow proRow = modelDetailTablePro.NewRow();
                                proRow[ModelsDS.ASSETID] = assetEntity.ID;
                                proRow[ModelsDS.PROID] = productEntity.ID;
                                proRow[ModelsDS.PRODESCRIPTION] = productEntity.Description;
                                proRow[ModelsDS.PRODYNAMICPERCENTAGE] = productEntity.DynamicPercentage;
                                proRow[ModelsDS.PROSHAREPERCENTAGE] = productEntity.SharePercentage;
                                proRow[ModelsDS.PROMAXALLOCATION] = productEntity.MaxAllocation;
                                proRow[ModelsDS.PROMINALLOCATION] = productEntity.MinAllocation;
                                proRow[ModelsDS.PROMODELNAME] = productEntity.Name;

                                modelDetailTablePro.Rows.Add(proRow);
                            }
                        }
                    }

                    foreach (AssetEntity assetEntity in model.Assets)
                    {
                        if (assetEntity.Products.Count > 0)
                        {
                            foreach (ProductEntity productEntity in assetEntity.Products)
                            {
                                DataTable modelDetailTable = modelsDS.Tables[ModelsDS.MODELDETAILSLIST];
                                DataRow row = modelDetailTable.NewRow();

                                row[ModelsDS.ASSETDESCRIPTION] = assetEntity.Description;
                                row[ModelsDS.ASSETDYNAMICPERCENTAGE] = assetEntity.DynamicPercentage;
                                row[ModelsDS.ASSETSHAREPERCENTAGE] = assetEntity.SharePercentage;
                                row[ModelsDS.ASSETMAXALLOCATION] = assetEntity.MaxAllocation;
                                row[ModelsDS.ASSETMINALLOCATION] = assetEntity.MinAllocation;
                                row[ModelsDS.ASSETMODELNAME] = assetEntity.Name;
                                row[ModelsDS.PRODESCRIPTION] = productEntity.Description;
                                row[ModelsDS.PRODYNAMICPERCENTAGE] = productEntity.DynamicPercentage;
                                row[ModelsDS.PROSHAREPERCENTAGE] = productEntity.SharePercentage;
                                row[ModelsDS.PROMAXALLOCATION] = productEntity.MaxAllocation;
                                row[ModelsDS.PROMINALLOCATION] = productEntity.MinAllocation;
                                row[ModelsDS.PROMODELNAME] = productEntity.Name;

                                modelDetailTable.Rows.Add(row);
                            }
                        }
                        else
                        {
                            DataTable modelDetailTable = modelsDS.Tables[ModelsDS.MODELDETAILSLIST];
                            DataRow row = modelDetailTable.NewRow();

                            row[ModelsDS.ASSETDESCRIPTION] = assetEntity.Description;
                            row[ModelsDS.ASSETDYNAMICPERCENTAGE] = assetEntity.DynamicPercentage;
                            row[ModelsDS.ASSETSHAREPERCENTAGE] = assetEntity.SharePercentage;
                            row[ModelsDS.ASSETMAXALLOCATION] = assetEntity.MaxAllocation;
                            row[ModelsDS.ASSETMINALLOCATION] = assetEntity.MinAllocation;
                            row[ModelsDS.ASSETMODELNAME] = assetEntity.Name;

                            modelDetailTable.Rows.Add(row);
                        }
                    }
                }
                else
                {
                    foreach (ModelEntity modelEntity in Model)
                    {
                        modelEntity.CalculateAllocation();
                        DataTable modellistTable = modelsDS.Tables[ModelsDS.MODELLIST];
                        DataRow row = modellistTable.NewRow();
                        row[ModelsDS.ID] = modelEntity.ID;
                        row[ModelsDS.DESCRIPTION] = modelEntity.Description;
                        row[ModelsDS.DYNAMICPERCENTAGE] = modelEntity.DynamicPercentage;
                        row[ModelsDS.MAXALLOCATION] = modelEntity.MaxAllocation;
                        row[ModelsDS.MINALLOCATION] = modelEntity.MinAllocation;
                        row[ModelsDS.MODELNAME] = modelEntity.Name;
                        row[ModelsDS.PROGRAMCODE] = modelEntity.ProgramCode;
                        row[ModelsDS.SHAREPERCENTAGE] = modelEntity.SharePercentage;
                        row[ModelsDS.SERVICETYPE] = Enumeration.GetDescription(modelEntity.ServiceType);
                        row[ModelsDS.ISPRIVATE] = modelEntity.IsPrivate;
                        row[ModelsDS.HASERRORS] =(Math.Round(modelEntity.DynamicPercentage,2) != 100.0 ||Math.Round(modelEntity.SharePercentage,2) != 100.0) ? "Error" : string.Empty;

                        modellistTable.Rows.Add(row);
                    }
                }

            }
            else if (data is InvestmentDS)
            {
                InvestmentDS ds = (InvestmentDS)data;
                DataTable table = ds.Tables[InvestmentDS.INVESTMENTMANAGEMENT];

                InvestmentManagementEntity entity = this._InvestmentManagementDetails.Where(r => r.ID == ds.InvestmentID).FirstOrDefault();
                if (entity != null)
                {
                    DataRow row = table.NewRow();
                    InvestmentManagementEntity.ExtractInvestmentManagementData(entity, row);
                    table.Rows.Add(row);

                    InvestmentManagementEntity.ExtractManagersAllocationToDataTable(entity, ds.Tables[InvestmentDS.ALLOCATIONS]);
                }
                DataTable secList = SecuritiesDS.CreateSecuritiesListTable();  //add data about securities on the system
                ExtractSecuritiesData(secList);
                ds.Merge(secList);

                PROSecuritiesDS proSecuritiesDS = new PROSecuritiesDS(); // add product securities list
                proSecuritiesDS.GetDetails = false;
                OnGetData(proSecuritiesDS);
                ds.Merge(proSecuritiesDS.Tables[PROSecuritiesDS.PROSECLISTTABLE]); //!!!

                DataTable mergedTable = CompleteManagersDataSource(ds.Tables[InvestmentDS.ALLOCATIONS], ds.Tables[PROSecuritiesDS.PROSECLISTTABLE]);
                ds.Tables[InvestmentDS.ALLOCATIONS].Clear();
                ds.Tables[InvestmentDS.ALLOCATIONS].Merge(mergedTable);


                DataTable productsTable = ds.Tables[ProductsDS.PRODUCTSTABLE];
                if (ds.InvestmentID != Guid.Empty && productsTable != null)
                {
                    ProductEntity productEntity = this.Products.Where(r => r.ID == ds.InvestmentID).FirstOrDefault();
                    if (productEntity != null)
                    {
                        DataRow r = productsTable.NewRow();
                        ExtractProductEntityData(productEntity, r);
                        productsTable.Rows.Add(r);

                        PerfReportDS dsPerf = new PerfReportDS();
                        dsPerf.EndDate = AccountingFinancialYear.LastDayOfMonthFromDateTime(DateTime.Now.AddMonths(-1));
                        dsPerf.StartDate = dsPerf.EndDate.AddMonths(-60);//5 yrs back

                        //SecurityPerformanceUtility util = new SecurityPerformanceUtility();
                        //util.SecurityPerformance((IOrganization)this, dsPerf, productEntity);

                        ds.Merge(dsPerf.Tables[PerfReportDS.PERFSUMMARYTABLE]);
                        ds.Merge(dsPerf.Tables[PerfReportDS.PERFSUMMARYINCOMEGROWTHTABLE]);
                    }
                }
            }
            else if (data is SecuritiesDS)
            {
                SecuritiesDS securitiesDS = (SecuritiesDS)data;
                DataTable secList = securitiesDS.Tables[SecuritiesDS.SECLIST];


                var security = this.Securities.Where(details => details.ID == securitiesDS.SecID).FirstOrDefault();

                if (security != null)
                {

                    DataRow row = secList.NewRow();
                    ExtractSecirityData(security, row);
                    row[SecuritiesDS.PRICEDATE] = security.IssueDate != null ? Convert.ToDateTime(security.IssueDate) : DateTime.Now;
                    //row[SecuritiesDS.UNITPRICE] = 0;
                    secList.Rows.Add(row);


                }
                else
                {
                    ExtractSecuritiesData(secList);
                }
            }
            else if (data is SecurityDetailsDS)
            {
                SecurityDetailsDS securitiesDS = (SecurityDetailsDS)data;

                var security = this.Securities.Where(details => details.ID == securitiesDS.DetailsGUID).FirstOrDefault();
                securitiesDS.ASXCode = security.AsxCode;
                securitiesDS.Description = security.CompanyName;
                //historical prices
                if (securitiesDS.RequiredDataType == SecurityDetailTypes.HistoricalPrices && security.ASXSecurity != null)
                {

                    foreach (ASXSecurityEntity asxSecurityEntity in security.ASXSecurity)
                    {

                        DataTable secHisDetails = securitiesDS.Tables[securitiesDS.PriceTable.TB_Name];
                        DataRow row = secHisDetails.NewRow();
                        row[securitiesDS.PriceTable.ID] = asxSecurityEntity.ID;
                        row[securitiesDS.PriceTable.SECID] = security.ID;
                        row[securitiesDS.PriceTable.ADJUSTEDPRICE] = asxSecurityEntity.AdjustedPrice;
                        row[securitiesDS.PriceTable.DATE] = asxSecurityEntity.Date;
                        row[securitiesDS.PriceTable.CURRENCY] = asxSecurityEntity.Currency;
                        row[securitiesDS.PriceTable.PRICENAV] = asxSecurityEntity.PriceNAV ?? 0;
                        row[securitiesDS.PriceTable.PRICEPUR] = asxSecurityEntity.PricePUR ?? 0;
                        row[securitiesDS.PriceTable.UNITPRICE] = asxSecurityEntity.UnitPrice;
                        row[securitiesDS.PriceTable.INTERESTRATE] = asxSecurityEntity.InterestRate;

                        secHisDetails.Rows.Add(row);
                    }

                }
                else if (securitiesDS.RequiredDataType == SecurityDetailTypes.Distiribution && security.DistributionCollection != null)
                {
                    foreach (DistributionEntity distributionEntity in security.DistributionCollection)
                    {
                        DataTable secHisDetails = securitiesDS.Tables[securitiesDS.DistributionTable.TB_Name];
                        DataRow row = secHisDetails.NewRow();
                        row[securitiesDS.DistributionTable.ID] = distributionEntity.ID;
                        row[securitiesDS.DistributionTable.SECID] = security.ID;
                        row[securitiesDS.DistributionTable.RECORDDATE] = distributionEntity.RecordDate;
                        row[securitiesDS.DistributionTable.PAYMENTDATE] = distributionEntity.PaymentDate;
                        row[securitiesDS.DistributionTable.INVESTMENTCODE] = security.AsxCode;
                        secHisDetails.Rows.Add(row);
                    }
                }
                else if (securitiesDS.RequiredDataType == SecurityDetailTypes.DividendPushDown & security.DividendCollection != null)
                {
                    foreach (DividendEntity dividendEntity in security.DividendCollection)
                    {

                        DataTable secHisDetails = securitiesDS.Tables[securitiesDS.DividendTable.TB_Name];
                        DataRow row = secHisDetails.NewRow();
                        row[securitiesDS.DividendTable.ID] = dividendEntity.ID;
                        row[securitiesDS.DividendTable.SECID] = security.ID;
                        row[securitiesDS.DividendTable.RECORDDATE] = dividendEntity.RecordDate;
                        row[securitiesDS.DividendTable.PAYMENTDATE] = dividendEntity.PaymentDate;
                        row[securitiesDS.DividendTable.TRANSACTIONTYPE] = dividendEntity.TransactionType;
                        row[securitiesDS.DividendTable.CENTSPERSHARE] = dividendEntity.CentsPerShare;
                        row[securitiesDS.DividendTable.INVESTMENTCODE] = security.AsxCode;

                        if (dividendEntity.BalanceDate.HasValue)
                            row[securitiesDS.DividendTable.BALANCEDATE] = dividendEntity.BalanceDate;
                        if (dividendEntity.BooksCloseDate.HasValue)
                            row[securitiesDS.DividendTable.BOOKSCLOSEDATE] = dividendEntity.BooksCloseDate;
                        row[securitiesDS.DividendTable.DIVIDENDTYPE] = dividendEntity.Dividendtype;
                        row[securitiesDS.DividendTable.CURRENCY] = dividendEntity.Currency;


                        secHisDetails.Rows.Add(row);
                    }
                }
                else if (securitiesDS.RequiredDataType == SecurityDetailTypes.DividendPushDownReport & security.DividendCollection != null)
                {
                    foreach (DividendEntity dividendEntity in security.DividendCollection)
                    {
                        if (dividendEntity.ReportCollection != null)
                        {
                            foreach (var report in dividendEntity.ReportCollection)
                            {
                                DataTable secHisDetails = securitiesDS.Tables[securitiesDS.SecDividendsReportTable.TB_Name];
                                DataRow row = secHisDetails.NewRow();
                                row[securitiesDS.SecDividendsReportTable.ID] = dividendEntity.ID;
                                row[securitiesDS.SecDividendsReportTable.SECID] = security.ID;
                                row[securitiesDS.SecDividendsReportTable.CLIENTIDENTIFICATION] = report.ClientIdentification;
                                row[securitiesDS.SecDividendsReportTable.CLIENTNAME] = report.Name;
                                row[securitiesDS.SecDividendsReportTable.ACCOUNTNAME] = report.acname;
                                row[securitiesDS.SecDividendsReportTable.ACCOUNTNUMBER] = report.acnumber;
                                row[securitiesDS.SecDividendsReportTable.RECORDDATE] = report.RecordDate;
                                row[securitiesDS.SecDividendsReportTable.PAYMENTDATE] = report.PaymentDate;
                                row[securitiesDS.SecDividendsReportTable.UNITSONHAND] = report.UnitsOnHand;
                                row[securitiesDS.SecDividendsReportTable.CENTSPERSHARE] = report.CentsPerShare;
                                row[securitiesDS.SecDividendsReportTable.PAIDDIVIDEND] = report.PaidDividend;
                                if (dividendEntity.BalanceDate.HasValue)
                                    row[securitiesDS.DividendTable.BALANCEDATE] = dividendEntity.BalanceDate;
                                if (dividendEntity.BooksCloseDate.HasValue)
                                    row[securitiesDS.DividendTable.BOOKSCLOSEDATE] = dividendEntity.BooksCloseDate;
                                row[securitiesDS.DividendTable.DIVIDENDTYPE] = dividendEntity.Dividendtype;
                                row[securitiesDS.DividendTable.CURRENCY] = dividendEntity.Currency;
                                secHisDetails.Rows.Add(row);
                            }

                        }

                    }
                }

            }
            else if (data is SecurityDistributionsDetailsDS)
            {
                var DS = data as SecurityDistributionsDetailsDS;
                var security = this.Securities.Where(details => details.ID == DS.SecID).FirstOrDefault();
                if (security != null && security.DistributionCollection != null)
                {

                    var distributionentity = security.DistributionCollection.Where(ss => ss.ID == DS.ID).FirstOrDefault();
                    if (DS.CommandType == DatasetCommandTypes.Get)
                    {
                        DS.Entity = distributionentity;
                    }

                }

            }
            else if (data is SecurityDividendDetailsDS)
            {
                var DS = data as SecurityDividendDetailsDS;
                var security = this.Securities.Where(details => details.ID == DS.SecID).FirstOrDefault();
                if (security != null && security.DividendCollection != null)
                {
                    var dividendEntity = security.DividendCollection.Where(ss => ss.ID == DS.ID).FirstOrDefault();
                    if (DS.CommandType == DatasetCommandTypes.Get)
                    {
                        DS.Entity = dividendEntity;
                    }

                }

            }
            else if (data is PROSecuritiesDS)
            {
                PROSecuritiesDS proSecuritiesDS = (PROSecuritiesDS)data;

                if (proSecuritiesDS.GetDetails)
                {
                    var proSecurity = this.ProductSecurities.Where(details => details.ID == proSecuritiesDS.DetailsGUID).FirstOrDefault();

                    foreach (ProductSecurityDetail productSecurityDetail in proSecurity.Details)
                    {
                        DataTable prosecDetails = proSecuritiesDS.Tables[PROSecuritiesDS.PROSECDETAILTABLE];
                        DataRow row = prosecDetails.NewRow();

                        var security = this.Securities.Where(sec => sec.ID == productSecurityDetail.SecurityCodeId).FirstOrDefault();

                        if (security != null)
                        {
                            row[PROSecuritiesDS.NAME] = security.AsxCode;
                            row[PROSecuritiesDS.DESCRIPTION] = security.CompanyName;
                        }

                        row[PROSecuritiesDS.ALLOCATION] = productSecurityDetail.Allocation;
                        row[PROSecuritiesDS.BUYPRICE] = productSecurityDetail.BuyPrice;
                        row[PROSecuritiesDS.CURRENCY] = productSecurityDetail.Currency;
                        row[PROSecuritiesDS.COMMENTS] = productSecurityDetail.Comments;
                        row[PROSecuritiesDS.ISDEACTIVE] = productSecurityDetail.IsDeactive;
                        row[PROSecuritiesDS.MARKETCODE] = productSecurityDetail.MarketCode;
                        row[PROSecuritiesDS.PERCENTALLOCATION] = productSecurityDetail.PercentageAllocation;
                        row[PROSecuritiesDS.RATING] = productSecurityDetail.Rating;
                        row[PROSecuritiesDS.RATINGOPTIONS] = productSecurityDetail.RatingOptions;
                        row[PROSecuritiesDS.SELLPRICE] = productSecurityDetail.SellPrice;

                        prosecDetails.Rows.Add(row);
                    }
                }
                else
                {
                    foreach (ProductSecuritiesEntity productSecuritiesEntity in ProductSecurities)
                    {
                        DataTable prosecList = proSecuritiesDS.Tables[PROSecuritiesDS.PROSECLISTTABLE];
                        DataRow row = prosecList.NewRow();

                        row[PROSecuritiesDS.ID] = productSecuritiesEntity.ID;
                        row[PROSecuritiesDS.NAME] = productSecuritiesEntity.Name;
                        row[PROSecuritiesDS.DESCRIPTION] = productSecuritiesEntity.Description;
                        prosecList.Rows.Add(row);
                    }
                }
            }
            #region ManualAssets
            else if (data is ManualAssetsDS)
            {
                var manualAssetsDS = (ManualAssetsDS)data;
                if (manualAssetsDS.IsDetails)
                {
                    var manual = ManualAsset.Where(item => item.ID == manualAssetsDS.ItemID).FirstOrDefault();

                    foreach (ManualAssetASXSecurityEntity asxSecurityEntity in manual.ASXSecurity)
                    {
                        DataTable secHisDetails = manualAssetsDS.Tables[ManualAssetsDS.MANPRICEHISTORY];
                        DataRow row = secHisDetails.NewRow();

                        row[ManualAssetsDS.MANHISID] = asxSecurityEntity.ID;
                        row[ManualAssetsDS.DATE] = asxSecurityEntity.Date;
                        row[ManualAssetsDS.CURRENCY] = asxSecurityEntity.Currency;
                        row[ManualAssetsDS.VALUE] = asxSecurityEntity.Value;
                        row[ManualAssetsDS.UNITPRICE] = asxSecurityEntity.UnitPrice;
                        row[ManualAssetsDS.NAVPRICE] = asxSecurityEntity.NavValue;
                        row[ManualAssetsDS.PURPRICE] = asxSecurityEntity.PurValue;
                        row[ManualAssetsDS.TOTALUNITS] = asxSecurityEntity.TotalUnits;

                        secHisDetails.Rows.Add(row);
                    }
                }
                else
                {
                    foreach (ManualAssetEntity manualAssetEntity in ManualAsset)
                    {
                        DataTable secList = manualAssetsDS.Tables[ManualAssetsDS.MANIST];
                        DataRow row = secList.NewRow();

                        row[ManualAssetsDS.ID] = manualAssetEntity.ID;
                        row[ManualAssetsDS.CODE] = manualAssetEntity.AsxCode;
                        row[ManualAssetsDS.DESCRIPTION] = manualAssetEntity.CompanyName;
                        row[ManualAssetsDS.INVESTMENTTYPE] = manualAssetEntity.InvestmentType;
                        row[ManualAssetsDS.MARKET] = manualAssetEntity.Market;
                        row[ManualAssetsDS.RATING] = manualAssetEntity.Rating;
                        row[ManualAssetsDS.ISPREFFEREDINVESTMENT] = manualAssetEntity.IsPrefferedInvestment;
                        row[ManualAssetsDS.UNITPRICEBASECURRENCY] = manualAssetEntity.UnitPriceBaseCurrency;
                        row[ManualAssetsDS.UNITSIZE] = manualAssetEntity.UnitSize;
                        row[ManualAssetsDS.MINSECURITYBAL] = manualAssetEntity.MinSecurityBal;
                        row[ManualAssetsDS.MINTRADEDLOTSIZE] = manualAssetEntity.MinTradedLotSize;
                        row[ManualAssetsDS.PRICEASATDATE] = manualAssetEntity.PriceAsAtDate;
                        row[ManualAssetsDS.IGNOREMINSECURITYBAL] = manualAssetEntity.IgnoreMinSecurityBal;
                        row[ManualAssetsDS.ISUNITISED] = manualAssetEntity.isUnitised;
                        row[ManualAssetsDS.IGNOREMINTRADEDLOTSIZE] = manualAssetEntity.IgnoreMinTradedLotSize;
                        if (manualAssetEntity.IssueDate.HasValue)
                            row[ManualAssetsDS.ISSUEDATE] = manualAssetEntity.IssueDate;
                        row[ManualAssetsDS.STATUS] = manualAssetEntity.Status;

                        secList.Rows.Add(row);
                    }
                }
            }
            #endregion
            else if (data is OrderPadRatesDS)
            {
                OrderPadRatesDS orderPadRatesds = (OrderPadRatesDS)data;
                DataTable ratesDetails = orderPadRatesds.Tables[OrderPadRatesDS.ORDERPADRATESTABLE];
                DataTable maxratesDetails = orderPadRatesds.Tables[OrderPadRatesDS.ORDERPADMAXRATESTABLE];

                if (orderPadRatesds.CommandType != DatasetCommandTypes.GetMax)
                {
                    foreach (var inst in this.Institution)
                    {
                        //show  prices  of latest dates 
                        if (inst.TDPrices != null)
                        {
                            //Get rates by ClientManangementType
                            var tdPrices = inst.TDPrices.Where(ss => ss.RateEffectiveFor == orderPadRatesds.ClientManagementType).ToList();
                            var topDate = tdPrices.OrderByDescending(or => or.Date).FirstOrDefault();
                            if (topDate != null)
                            {
                                foreach (var price in tdPrices.Where(ss => ss.Date == topDate.Date))
                                {
                                    DataRow row = ratesDetails.NewRow();

                                    row[OrderPadRatesDS.ID] = price.ID;
                                    row[OrderPadRatesDS.INSTITUTEID] = inst.ID;
                                    row[OrderPadRatesDS.INSTITUTENAME] = inst.Name;

                                    var ratings = InstituteCombos.SPShortTermRatings.Where(ss => ss.Value == inst.SPCreditRatingShort).FirstOrDefault();

                                    row[OrderPadRatesDS.INSTITUTESHORTRATING] = ratings.Key;
                                    row[OrderPadRatesDS.INSTITUTESHORTRATINGVALUE] = ratings.Value;
                                    ratings = InstituteCombos.SPLongTermRatings.Where(ss => ss.Value == inst.SPCreditRatingLong).FirstOrDefault();

                                    row[OrderPadRatesDS.INSTITUTELONGRATING] = ratings.Key;
                                    row[OrderPadRatesDS.INSTITUTELONGRATINGVALUE] = ratings.Value;
                                    row[OrderPadRatesDS.PRICEINSTITUTEID] = price.InstituteID;

                                    var inss = this.Institution.Where(ii => ii.ID == price.InstituteID).FirstOrDefault();
                                    if (inss != null)
                                    {
                                        row[OrderPadRatesDS.PRICEINSTITUTENAME] = inss.Name;
                                        ratings = InstituteCombos.SPShortTermRatings.Where(ss => ss.Value == inss.SPCreditRatingShort).FirstOrDefault();
                                        row[OrderPadRatesDS.PRICEINSTITUTESHORTRATING] = ratings.Key;
                                        row[OrderPadRatesDS.PRICEINSTITUTESHORTRATINGVALUE] = ratings.Value;
                                        ratings = InstituteCombos.SPLongTermRatings.Where(ss => ss.Value == inss.SPCreditRatingLong).FirstOrDefault();
                                        row[OrderPadRatesDS.PRICEINSTITUTELONGRATING] = ratings.Key;
                                        row[OrderPadRatesDS.PRICEINSTITUTELONGRATINGVALUE] = ratings.Value;
                                    }
                                    if (price.Min.HasValue)
                                        row[OrderPadRatesDS.MIN] = price.Min.Value;

                                    if (price.Max.HasValue)
                                        row[OrderPadRatesDS.MAX] = price.Max.Value;
                                    if (price.MaximumBrokeage.HasValue)
                                        row[OrderPadRatesDS.MAXIMUMBROKEAGE] = price.MaximumBrokeage.Value;
                                    if (price.Days30.HasValue)
                                        row[OrderPadRatesDS.DAYS30] = price.Days30.Value / 100;
                                    if (price.Days60.HasValue)
                                        row[OrderPadRatesDS.DAYS60] = price.Days60.Value / 100;
                                    if (price.Days90.HasValue)
                                        row[OrderPadRatesDS.DAYS90] = price.Days90.Value / 100;
                                    if (price.Days120.HasValue)
                                        row[OrderPadRatesDS.DAYS120] = price.Days120.Value / 100;
                                    if (price.Days150.HasValue)
                                        row[OrderPadRatesDS.DAYS150] = price.Days150.Value / 100;
                                    if (price.Days180.HasValue)
                                        row[OrderPadRatesDS.DAYS180] = price.Days180.Value / 100;
                                    if (price.Days270.HasValue)
                                        row[OrderPadRatesDS.DAYS270] = price.Days270.Value / 100;
                                    if (price.Years1.HasValue)
                                        row[OrderPadRatesDS.YEARS1] = price.Years1.Value / 100;
                                    if (price.Years2.HasValue)
                                        row[OrderPadRatesDS.YEARS2] = price.Years2.Value / 100;
                                    if (price.Years3.HasValue)
                                        row[OrderPadRatesDS.YEARS3] = price.Years3.Value / 100;
                                    if (price.Years4.HasValue)
                                        row[OrderPadRatesDS.YEARS4] = price.Years4.Value / 100;
                                    if (price.Years5.HasValue)
                                        row[OrderPadRatesDS.YEARS5] = price.Years5.Value / 100;

                                    if (!string.IsNullOrEmpty(price.Status))
                                        row[OrderPadRatesDS.STATUS] = price.Status;

                                    row[OrderPadRatesDS.DATE] = price.Date;
                                    if (price.Ongoing.HasValue)
                                        row[OrderPadRatesDS.ONGOING] = price.Ongoing.Value;

                                    row[OrderPadRatesDS.CLIENTMANAGEMENTTYPE] = price.RateEffectiveFor;
                                    //Setting Default Limit for SMA to 50%
                                    if (price.RateEffectiveFor == ClientManagementType.SMA)
                                        row[OrderPadRatesDS.SMAHOLDINGLIMIT] = 50;

                                    row[OrderPadRatesDS.RATEID] = price.ImportRateID;
                                    row[OrderPadRatesDS.PROVIDERID] = price.ProviderID;

                                    ratesDetails.Rows.Add(row);
                                }
                            }
                        }
                    }
                }

                DataRow maxrow = maxratesDetails.NewRow();

                maxrow[OrderPadRatesDS.MIN] = orderPadRatesds.GetMaxValue(OrderPadRatesDS.ORDERPADRATESTABLE, OrderPadRatesDS.MIN);
                maxrow[OrderPadRatesDS.MAX] = orderPadRatesds.GetMaxValue(OrderPadRatesDS.ORDERPADRATESTABLE, OrderPadRatesDS.MAX);
                maxrow[OrderPadRatesDS.MAXIMUMBROKEAGE] = orderPadRatesds.GetMaxValue(OrderPadRatesDS.ORDERPADRATESTABLE, OrderPadRatesDS.MAXIMUMBROKEAGE);
                maxrow[OrderPadRatesDS.DAYS30] = orderPadRatesds.GetMaxValue(OrderPadRatesDS.ORDERPADRATESTABLE, OrderPadRatesDS.DAYS30);
                maxrow[OrderPadRatesDS.DAYS60] = orderPadRatesds.GetMaxValue(OrderPadRatesDS.ORDERPADRATESTABLE, OrderPadRatesDS.DAYS60);
                maxrow[OrderPadRatesDS.DAYS90] = orderPadRatesds.GetMaxValue(OrderPadRatesDS.ORDERPADRATESTABLE, OrderPadRatesDS.DAYS90);
                maxrow[OrderPadRatesDS.DAYS120] = orderPadRatesds.GetMaxValue(OrderPadRatesDS.ORDERPADRATESTABLE, OrderPadRatesDS.DAYS120);
                maxrow[OrderPadRatesDS.DAYS150] = orderPadRatesds.GetMaxValue(OrderPadRatesDS.ORDERPADRATESTABLE, OrderPadRatesDS.DAYS150);
                maxrow[OrderPadRatesDS.DAYS180] = orderPadRatesds.GetMaxValue(OrderPadRatesDS.ORDERPADRATESTABLE, OrderPadRatesDS.DAYS180);
                maxrow[OrderPadRatesDS.DAYS270] = orderPadRatesds.GetMaxValue(OrderPadRatesDS.ORDERPADRATESTABLE, OrderPadRatesDS.DAYS270);
                maxrow[OrderPadRatesDS.YEARS1] = orderPadRatesds.GetMaxValue(OrderPadRatesDS.ORDERPADRATESTABLE, OrderPadRatesDS.YEARS1);
                maxrow[OrderPadRatesDS.YEARS2] = orderPadRatesds.GetMaxValue(OrderPadRatesDS.ORDERPADRATESTABLE, OrderPadRatesDS.YEARS2);
                maxrow[OrderPadRatesDS.YEARS3] = orderPadRatesds.GetMaxValue(OrderPadRatesDS.ORDERPADRATESTABLE, OrderPadRatesDS.YEARS3);
                maxrow[OrderPadRatesDS.YEARS4] = orderPadRatesds.GetMaxValue(OrderPadRatesDS.ORDERPADRATESTABLE, OrderPadRatesDS.YEARS4);
                maxrow[OrderPadRatesDS.YEARS5] = orderPadRatesds.GetMaxValue(OrderPadRatesDS.ORDERPADRATESTABLE, OrderPadRatesDS.YEARS5);
                maxrow[OrderPadRatesDS.DATE] = orderPadRatesds.GetMaxValue(OrderPadRatesDS.ORDERPADRATESTABLE, OrderPadRatesDS.DATE);
                maxrow[OrderPadRatesDS.ONGOING] = orderPadRatesds.GetMaxValue(OrderPadRatesDS.ORDERPADRATESTABLE, OrderPadRatesDS.ONGOING);

                maxratesDetails.Rows.Add(maxrow);


            }
            else if (data is InstitutesDS)
            {
                var dataset = data as InstitutesDS;

                foreach (var ins in this.Institution) { GetInstituteValues(dataset, ins); }

            }
            else if (data is InstitutionContactsDS)
            {
                var dataset = data as InstitutionContactsDS;
                var result = this.Institution.FirstOrDefault(ss =>
                {
                    if (dataset.Id == Guid.Empty) return true;
                    return (ss.ID == dataset.Id);
                });
                if (result != null)
                {
                    dataset.InstituteName = result.Name;
                    FillInstitutionContactsDataset(dataset, result.Signatories, Broker);
                }
            }
            else if (data is OrderPadAtCallRatesDS)
            {
                var ds = data as OrderPadAtCallRatesDS;
                switch (ds.CommandType)
                {
                    case DatasetCommandTypes.GetAll:
                        foreach (var inst in Institution)
                        {
                            if (inst.AtCallPrices != null)
                            {
                                foreach (var priceEntity in inst.AtCallPrices)
                                {
                                    GetAtCallRates(inst, ds, priceEntity);
                                }
                            }
                        }
                        break;
                    case DatasetCommandTypes.Get:
                        foreach (var inst in Institution)
                        {
                            if (inst.AtCallPrices != null)
                            {
                                //show  prices  of latest dates 
                                var topDate = inst.AtCallPrices.OrderByDescending(or => or.ApplicableFrom).FirstOrDefault();
                                if (topDate != null)
                                {
                                    foreach (var priceEntity in inst.AtCallPrices.Where(ss => ss.ApplicableFrom == topDate.ApplicableFrom))
                                    {
                                        GetAtCallRates(inst, ds, priceEntity);
                                    }
                                }
                            }
                        }
                        break;
                }
            }
            else if (data is CashBalanceDS)
            {
                var ds = data as CashBalanceDS;
                if (ds.CommandType == DatasetCommandTypes.Get)
                {
                    DataTable dt = ds.CashBalanceTable;
                    foreach (CashBalanceEntity entity in CashBalances)
                    {
                        DataRow dr = dt.NewRow();
                        dr[ds.CashBalanceTable.ID] = entity.ID;
                        dr[ds.CashBalanceTable.SUPERACCOUNTTYPE] = entity.SuperAccountType;
                        dr[ds.CashBalanceTable.PERCENTAGE] = entity.Percentage;
                        dr[ds.CashBalanceTable.MINVALUE] = entity.MinValue;
                        dr[ds.CashBalanceTable.MAXVALUE] = entity.MaxValue;
                        dt.Rows.Add(dr);
                    }
                }
            }
            else if (data is IHasOrganizationUnit)// this should be last if statement because it runs commands.
            {
                string userName = "Administrator";

                if (Broker.UserContext != null)
                    userName = Broker.UserContext.Identity.Name;

                IHasOrganizationUnit desktopBrokerAccountDs = data as IHasOrganizationUnit;
                Broker.LogEvent(EventType.UMAAccountChange, string.Format("Exceuting  Command:{0} UserName:{1}", desktopBrokerAccountDs.Command, userName), desktopBrokerAccountDs.Unit.CurrentUser.CurrentUserName, Guid.Empty);
                RunAddCommand(desktopBrokerAccountDs.Command, data);
            }
        }

        /// <summary>
        /// Get AtCallRates
        /// </summary>
        /// <param name="inst">InstitutionEntity</param>
        /// <param name="ds">OrderPadAtCallRatesDS</param>
        /// <param name="priceEntity">AtCallInstitutePriceEntity</param>
        private void GetAtCallRates(InstitutionEntity inst, OrderPadAtCallRatesDS ds, AtCallInstitutePriceEntity priceEntity)
        {
            var row = ds.OrderPadAtCallRatesTable.NewRow();

            row[ds.OrderPadAtCallRatesTable.ID] = priceEntity.ID;
            row[ds.OrderPadAtCallRatesTable.INSTITUTEID] = inst.ID;
            row[ds.OrderPadAtCallRatesTable.INSTITUTENAME] = inst.Name;
            row[ds.OrderPadAtCallRatesTable.PRICEINSTITUTEID] = priceEntity.InstituteID;

            var inss = Institution.Where(ii => ii.ID == priceEntity.InstituteID).FirstOrDefault();
            if (inss != null)
            {
                row[ds.OrderPadAtCallRatesTable.PRICEINSTITUTENAME] = inss.Name;
            }
            if (priceEntity.Min.HasValue)
            {
                row[ds.OrderPadAtCallRatesTable.MIN] = priceEntity.Min;
            }
            if (priceEntity.Max.HasValue)
            {
                row[ds.OrderPadAtCallRatesTable.MAX] = priceEntity.Max;
            }
            if (priceEntity.MaximumBrokerage.HasValue)
            {
                row[ds.OrderPadAtCallRatesTable.MAXIMUMBROKERAGE] = priceEntity.MaximumBrokerage;
            }
            if (!string.IsNullOrEmpty(priceEntity.Type))
            {
                row[ds.OrderPadAtCallRatesTable.TYPE] = priceEntity.Type;
            }
            row[ds.OrderPadAtCallRatesTable.ACCOUNTTIER] = priceEntity.AccountTier;
            row[ds.OrderPadAtCallRatesTable.RATE] = priceEntity.Rate2 ?? "";
            row[ds.OrderPadAtCallRatesTable.APPLICABLEFROM] = priceEntity.ApplicableFrom;
            row[ds.OrderPadAtCallRatesTable.DATE] = priceEntity.Date;
            row[ds.OrderPadAtCallRatesTable.ISEDITABLE] = priceEntity.IsEditable;
            row[ds.OrderPadAtCallRatesTable.APPLICABILITY] = priceEntity.Applicability;
            row[ds.OrderPadAtCallRatesTable.HONEYMOONPERIOD] = priceEntity.HoneymoonPeriod ?? 0;

            ds.OrderPadAtCallRatesTable.Rows.Add(row);
        }

        private DataTable CompleteManagersDataSource(DataTable allocation, DataTable productSec)
        {
            //List<Oritax.TaxSimp.Data.AllocationExtention> allocPercentage =
            //    (from alloc in allocation.AsEnumerable()
            //     join ps in productSec.AsEnumerable()
            //         on alloc.Field<Guid>(InvestmentDS.ID) equals ps.Field<Guid>(PROSecuritiesDS.ID)
            //     select new Oritax.TaxSimp.Data.AllocationExtention { ID = ps.Field<Guid>(PROSecuritiesDS.ID), Name = ps.Field<string>(PROSecuritiesDS.NAME), PercentageAllocation = alloc.Field<double>(InvestmentDS.PERCENTAGE) }).ToList();
            ////return allocPercentage;

            DataTable mergedTable =
                (from alloc in allocation.AsEnumerable()
                 join ps in productSec.AsEnumerable()
                     on alloc.Field<Guid>(InvestmentDS.ID) equals ps.Field<Guid>(PROSecuritiesDS.ID)
                 select new { ID = ps.Field<Guid>(PROSecuritiesDS.ID), Name = ps.Field<string>(PROSecuritiesDS.NAME), PercentageAllocation = alloc.Field<double>(InvestmentDS.PERCENTAGE) }).ToDataTable();

            return mergedTable;
        }

        private void ExtractSecuritiesData(DataTable secList)
        {
            foreach (SecuritiesEntity securitiesEntity in this.Securities)
            {

                DataRow row = secList.NewRow();

                ExtractSecirityData(securitiesEntity, row);
                if (securitiesEntity.ASXSecurity.Count > 0)
                {
                    var asxSecurity = securitiesEntity.ASXSecurity.OrderByDescending(sec => sec.Date).FirstOrDefault();
                    if (asxSecurity != null)
                    {
                        row[SecuritiesDS.PRICEDATE] = asxSecurity.Date;
                        row[SecuritiesDS.UNITPRICE] = asxSecurity.UnitPrice;
                        row[SecuritiesDS.CURRENCY] = asxSecurity.Currency;
                    }
                }
                else
                {
                    row[SecuritiesDS.PRICEDATE] = DateTime.Now;
                    row[SecuritiesDS.UNITPRICE] = 0;
                }
                secList.Rows.Add(row);
            }
        }

        private static void ExtractProductEntityData(ProductEntity pe, DataRow r)
        {
            r[ProductsDS.ID] = pe.ID;
            r[ProductsDS.NAME] = pe.Name;

            r[ProductsDS.DESCRIPTION] = pe.Description;

            r[ProductsDS.EntityId] = pe.EntityId == null ? Guid.Empty : pe.EntityId.Clid;
            r[ProductsDS.EntityCsId] = pe.EntityId == null ? Guid.Empty : pe.EntityId.Csid;
            r[ProductsDS.EntityType] = pe.EntityType;
            r[ProductsDS.BankAccountType] = pe.BankAccountType;
            r[ProductsDS.DynamicPercentage] = pe.DynamicPercentage;

            if (pe.FundAccounts != null && pe.FundAccounts.Count > 0)
                r[ProductsDS.FundAccounts] = pe.FundAccounts.FirstOrDefault();

            r[ProductsDS.IsDefaultProductSecurity] = pe.IsDefaultProductSecurity;
            r[ProductsDS.MaxAllocation] = pe.MaxAllocation;
            r[ProductsDS.MinAllocation] = pe.MinAllocation;
            r[ProductsDS.ProductAPIR] = pe.ProductAPIR;
            r[ProductsDS.ProductISIN] = pe.ProductISIN;
            r[ProductsDS.ProductSecuritiesId] = pe.ProductSecuritiesId;
            r[ProductsDS.ProductType] = pe.ProductType.ToLower();
            r[ProductsDS.SharePercentage] = pe.SharePercentage;
            r[ProductsDS.TaskDescription] = pe.TaskDescription;
        }

        public void ExtractOrgnisationListingDS(OrganisationListingDS organisationListingDS, string userName)
        {
            IBrokerManagedComponent user = this.Broker.GetBMCInstance(userName, "DBUser_1_1");
            object[] args = new Object[3] { this.CLID, user.CID, EnableSecurity.SecuritySetting };
            DataView view = new DataView(this.Broker.GetBMCDetailList(OrganizationInstall.ASSEMBLY_NAME, args).Tables[TableName]);

            if (organisationListingDS.OrganisationListingOperationType == OrganisationListingOperationType.ConsolidationEntities)
                view.RowFilter = organisationListingDS.GetRowFilterConsolidationEntities();
            else
                view.RowFilter = organisationListingDS.GetRowFilter();

            view.Sort = "ENTITYNAME_FIELD ASC";
            DataTable orgFiteredTable = view.ToTable();

            if (organisationListingDS.OrganisationListingOperationType == OrganisationListingOperationType.ClientsAndFeesAssociation)
            {
                ClientFeesAssocDS clientFeesAssocDS = new ClientFeesAssocDS();

                foreach (DataRow row in orgFiteredTable.Rows)
                {
                    Guid cid = new Guid(row["ENTITYCIID_FIELD"].ToString());
                    IOrganizationUnit organizationUnit = this.Broker.GetBMCInstance(cid) as IOrganizationUnit;

                    if (organizationUnit.IsInvestableClient)
                        organizationUnit.GetData(clientFeesAssocDS);

                    this.Broker.ReleaseBrokerManagedComponent(organizationUnit);
                }

                organisationListingDS.Merge(clientFeesAssocDS);
            }

            if (organisationListingDS.OrganisationListingOperationType == OrganisationListingOperationType.BankAccounts)
            {
                ExtractByBankAccount(organisationListingDS, args);
            }
            if (organisationListingDS.OrganisationListingOperationType == OrganisationListingOperationType.ClientDistributions)
            {
                ExtractClientsDIS(organisationListingDS, orgFiteredTable);
            }

            if (organisationListingDS.OrganisationListingOperationType == OrganisationListingOperationType.ASXAccounts)
            {
                ExtractByASXAccount(organisationListingDS, args);
            }

            if (organisationListingDS.OrganisationListingOperationType == OrganisationListingOperationType.FeeRuns)
            {
                this.ExtractByFeeRuns(organisationListingDS, args);
            }

            if (organisationListingDS.OrganisationListingOperationType == OrganisationListingOperationType.FeeRunsShort)
            {
                this.ExtractByFeeRunsWithoutCM(organisationListingDS, args);
            }

            if (organisationListingDS.OrganisationListingOperationType == OrganisationListingOperationType.UsersWithPasswords)
            {
                UsersWithPassword(organisationListingDS, args);
            }

            if (organisationListingDS.OrganisationListingOperationType == OrganisationListingOperationType.IFAAndClients || organisationListingDS.OrganisationListingOperationType == OrganisationListingOperationType.IFA)
            {
                DataView ifaView = new DataView(this.Broker.GetBMCDetailList(OrganizationInstall.ASSEMBLY_NAME, args).Tables[TableName]);
                ifaView.RowFilter = organisationListingDS.GetRowFilterIFA();
                ifaView.Sort = "ENTITYNAME_FIELD ASC";
                DataTable ifaViewTable = ifaView.ToTable();

                organisationListingDS.Tables.Add(ifaViewTable);

                return;
            }

            if (organisationListingDS.OrganisationListingOperationType == OrganisationListingOperationType.Clients)
            {
                orgFiteredTable.Columns.Add("ClientID", typeof(string));
                orgFiteredTable.Columns.Add("IsClient", typeof(bool));

                foreach (DataRow row in orgFiteredTable.Rows)
                {
                    Guid clid = new Guid(row["ENTITYCLID_FIELD"].ToString());
                    Guid csid = new Guid(row["ENTITYCSID_FIELD"].ToString());
                    IOrganizationUnit organizationUnit = this.Broker.GetCMImplementation(clid, csid) as IOrganizationUnit;

                    row["ClientID"] = organizationUnit.ClientId;
                    row["IsClient"] = organizationUnit.IsInvestableClient;

                    this.Broker.ReleaseBrokerManagedComponent(organizationUnit);
                }
            }

            if (organisationListingDS.OrganisationListingOperationType == OrganisationListingOperationType.ClientsWithFUM)
            {
                view = new DataView(orgFiteredTable);
                view.RowFilter = organisationListingDS.GetRowFilterAllClientsExceptEclipse();
                view.Sort = "ENTITYNAME_FIELD ASC";
                orgFiteredTable = view.ToTable();

                ExtractClientsFUM(orgFiteredTable, organisationListingDS.AsOfDate, organisationListingDS.ServiceTypes);
            }

            if (organisationListingDS.OrganisationListingOperationType == OrganisationListingOperationType.IndividualList)
            {
                ExtractIndividualList(organisationListingDS);
            }

            if (organisationListingDS.OrganisationListingOperationType == OrganisationListingOperationType.IndividualListOnlyNames)
            {
                this.ExtractIndividualListWithoutDetails(organisationListingDS);
            }

            if (organisationListingDS.OrganisationListingOperationType == OrganisationListingOperationType.EclipseSuperFUM)
            {
                DataView eclipseSuperView = new DataView(orgFiteredTable);
                view.RowFilter = OrganisationListingDS.GetRowFilterEclipseSuper();
                view.Sort = "ENTITYNAME_FIELD ASC";
                DataTable orgFiteredTableEclipseSuper = view.ToTable();
                this.ExtractClientsFUM(orgFiteredTableEclipseSuper, organisationListingDS.AsOfDate, organisationListingDS.ServiceTypes);
                orgFiteredTable = orgFiteredTableEclipseSuper;
                orgFiteredTable.TableName = OrganisationListingDS.ECLIPSESUPERFUMTABLENAME;

            }

            if (organisationListingDS.OrganisationListingOperationType == OrganisationListingOperationType.EclipseSuper)
            {
                DataView eclipseSuperView = new DataView(orgFiteredTable);
                view.RowFilter = OrganisationListingDS.GetRowFilterEclipseSuper();
                view.Sort = "ENTITYNAME_FIELD ASC";
                DataTable orgFiteredTableEclipseSuper = view.ToTable();
                orgFiteredTable = orgFiteredTableEclipseSuper;
                orgFiteredTable.TableName = OrganisationListingDS.ECLIPSESUPERTABLENAME;
            }

            if (organisationListingDS.OrganisationListingOperationType == OrganisationListingOperationType.BankAccountsListWithoutFUM)
            {
                DataView bankview = new DataView(this.Broker.GetBMCDetailList(OrganizationInstall.ASSEMBLY_NAME, args).Tables[TableName]);
                bankview.RowFilter = OrganisationListingDS.GetRowFilterBankAccounts();
                bankview.Sort = "ENTITYNAME_FIELD ASC";
                DataTable bankviewTable = bankview.ToTable(OrganisationListingDS.BANKACCOUNTLISTTABLENAME);
                organisationListingDS.Tables.Add(bankviewTable);

            }

            if (organisationListingDS.OrganisationListingOperationType == OrganisationListingOperationType.ClientsSECFUM)
            {
                DataView eclipseSuperView = new DataView(orgFiteredTable);
                view.RowFilter = new OrganisationListingDS().GetRowFilterAllClientsExceptEclipse();
                view.Sort = "ENTITYNAME_FIELD ASC";
                DataTable orgFiteredTableExceptEclipseSuper = view.ToTable();

                DataTable securityTable = ExtractClientsSECFUM(orgFiteredTableExceptEclipseSuper, organisationListingDS.AsOfDate, organisationListingDS.ServiceTypes);
                organisationListingDS.Tables.Add(securityTable.Copy());
            }

            if (organisationListingDS.OrganisationListingOperationType == OrganisationListingOperationType.ClientsWithAdvisers)
            {
                DataView allClientViewExceptSuper = new DataView(orgFiteredTable);
                allClientViewExceptSuper.RowFilter = organisationListingDS.GetRowFilter();
                allClientViewExceptSuper.Sort = "ENTITYNAME_FIELD ASC";
                DataTable allClientViewExceptSuperTable = allClientViewExceptSuper.ToTable();
                ExtractClientsWithAdviser(allClientViewExceptSuperTable);
                organisationListingDS.Tables.Add(ExtractAdviserListing());
                organisationListingDS.Tables.Add(allClientViewExceptSuperTable);
            }

            if (organisationListingDS.OrganisationListingOperationType == OrganisationListingOperationType.AdvisersBasicList)
            {
                organisationListingDS.Tables.Add(ExtractAdviserListing());
            }

            if (organisationListingDS.OrganisationListingOperationType == OrganisationListingOperationType.AssetSummaryOrg)
            {
                ExtractByAssetsSummaryOverall(organisationListingDS, orgFiteredTable);
            }

            if (organisationListingDS.OrganisationListingOperationType == OrganisationListingOperationType.DeleteMIS1004)
            {
                foreach (DataRow row in orgFiteredTable.Rows)
                {
                    Guid clid = new Guid(row["ENTITYCLID_FIELD"].ToString());
                    Guid csid = new Guid(row["ENTITYCSID_FIELD"].ToString());
                    IOrganizationUnit organizationUnit = this.Broker.GetCMImplementation(clid, csid) as IOrganizationUnit;
                    if (organizationUnit.IsInvestableClient)
                    {
                        DeleteMISTransactionDS DeleteMISTransactionDS = new DataSets.DeleteMISTransactionDS();
                        organizationUnit.SetData(DeleteMISTransactionDS);
                    }
                    this.Broker.ReleaseBrokerManagedComponent(organizationUnit);
                }
            }

            if (organisationListingDS.OrganisationListingOperationType == OrganisationListingOperationType.SetBankTransactions)
            {
                DataView bankview = new DataView(this.Broker.GetBMCDetailList(OrganizationInstall.ASSEMBLY_NAME, args).Tables[TableName]);
                bankview.RowFilter = OrganisationListingDS.GetRowFilterBankAccounts();
                bankview.Sort = "ENTITYNAME_FIELD ASC";
                DataTable bankviewTable = bankview.ToTable();

                foreach (DataRow row in bankviewTable.Rows)
                {
                    Guid ciid = new Guid(row["ENTITYCIID_FIELD"].ToString());
                    IOrganizationUnit organizationUnit = this.Broker.GetBMCInstance(ciid) as IOrganizationUnit;
                    BankTransactionDetailsFixDS bankTransactionDetailsFixDS = new DataSets.BankTransactionDetailsFixDS();
                    organizationUnit.SetData(bankTransactionDetailsFixDS);
                    this.Broker.ReleaseBrokerManagedComponent(organizationUnit);
                }
            }

            if (organisationListingDS.OrganisationListingOperationType == OrganisationListingOperationType.ClientsWithFUMDIFM)
            {
                ExtractByClientsWithFUMDIFM(orgFiteredTable);
            }

            if (organisationListingDS.OrganisationListingOperationType == OrganisationListingOperationType.DuplicatedAssociationClients)
            {
                DataTable dt = ExtractByClientsWithDuplicatedAssociation(orgFiteredTable);
                organisationListingDS.Tables.Add(dt.Copy());
            }

            if (organisationListingDS.OrganisationListingOperationType == OrganisationListingOperationType.ClientsSearch)
            {
                ExtractByClientSearch(organisationListingDS, user, orgFiteredTable);
                return;
            }

            if (organisationListingDS.OrganisationListingOperationType == OrganisationListingOperationType.AdvisersWithFUM)
            {
                orgFiteredTable.Columns.Add("IsClient", typeof(bool));
                orgFiteredTable.Columns.Add("Holding", typeof(decimal));

                foreach (DataRow row in orgFiteredTable.Rows)
                {
                    Guid clid = new Guid(row["ENTITYCLID_FIELD"].ToString());
                    Guid csid = new Guid(row["ENTITYCSID_FIELD"].ToString());
                    IOrganizationUnit organizationUnit = this.Broker.GetCMImplementation(clid, csid) as IOrganizationUnit;

                    HoldingRptDataSet holdingRptDataSet = new HoldingRptDataSet();
                    organizationUnit.GetData(holdingRptDataSet);

                    decimal holdingTotal = 0;

                    if (organizationUnit.IsInvestableClient)
                        holdingTotal = holdingRptDataSet.HoldingSummaryTable.Select().Sum(holdingRow => Convert.ToDecimal(holdingRow[holdingRptDataSet.HoldingSummaryTable.HOLDING]));

                    row["IsClient"] = organizationUnit.IsInvestableClient;
                    row["Holding"] = holdingTotal;

                    this.Broker.ReleaseBrokerManagedComponent(organizationUnit);
                }
            }

            DataView orgFiteredTableView = new DataView(orgFiteredTable);
            organisationListingDS.Tables.Add(orgFiteredTableView.ToTable());

            this.Broker.ReleaseBrokerManagedComponent(user);
        }

        private void ExtractByClientSearch(OrganisationListingDS organisationListingDS, IBrokerManagedComponent user, DataTable orgFiteredTable)
        {
            orgFiteredTable.Columns.Add("IsClient", typeof(bool));
            orgFiteredTable.Columns.Add("ClientType", typeof(string));
            orgFiteredTable.Columns.Add("ClientID", typeof(string));
            orgFiteredTable.Columns.Add("Holding", typeof(decimal));
            orgFiteredTable.Columns.Add("Unsettled", typeof(decimal));
            orgFiteredTable.Columns.Add("Total", typeof(decimal));
            orgFiteredTable.Columns.Add("AccountType", typeof(string));
            orgFiteredTable.Columns.Add("Status", typeof(string));
            orgFiteredTable.Columns.Add("OtherID", typeof(string));
            orgFiteredTable.Columns.Add("AdviserID", typeof(string));
            orgFiteredTable.Columns.Add("AdviserFullName", typeof(string));
            orgFiteredTable.Columns.Add("AdviserCID", typeof(Guid));

            DataView searchView = new DataView(orgFiteredTable);

            if (organisationListingDS.ClientNameSearchFilter.Length == 1)
                searchView.RowFilter = "ENTITYNAME_FIELD LIKE '%" + organisationListingDS.ClientNameSearchFilter + "*%'";
            else
                searchView.RowFilter = "ENTITYNAME_FIELD LIKE '%" + organisationListingDS.ClientNameSearchFilter + "%' OR OtherID LIKE '%" + organisationListingDS.ClientNameSearchFilter + "%' OR ENTITYECLIPSEID_FIELD LIKE '%" + organisationListingDS.ClientNameSearchFilter + "%'";

            DataTable searchedFilterTable = searchView.ToTable();

            foreach (DataRow row in searchedFilterTable.Rows)
            {
                Guid clid = new Guid(row["ENTITYCLID_FIELD"].ToString());
                Guid csid = new Guid(row["ENTITYCSID_FIELD"].ToString());
                IOrganizationUnit organizationUnit = this.Broker.GetCMImplementation(clid, csid) as IOrganizationUnit;

                decimal holdingTotal = 0;
                decimal unsettleTotal = 0;
                decimal totalTotal = 0;
                string clientType = string.Empty;

                if (organizationUnit.IsInvestableClient)
                {
                    HoldingRptDataSet holdingRptDataSet = new HoldingRptDataSet();
                    organizationUnit.GetData(holdingRptDataSet);

                    holdingTotal = holdingRptDataSet.HoldingSummaryTable.Select().Sum(holdingRow => Convert.ToDecimal(holdingRow[holdingRptDataSet.HoldingSummaryTable.HOLDING]));
                    unsettleTotal = holdingRptDataSet.HoldingSummaryTable.Select().Sum(holdingRow => Convert.ToDecimal(holdingRow[holdingRptDataSet.HoldingSummaryTable.UNSETTLED]));
                    totalTotal = holdingRptDataSet.HoldingSummaryTable.Select().Sum(holdingRow => Convert.ToDecimal(holdingRow[holdingRptDataSet.HoldingSummaryTable.TOTAL]));
                    clientType = (String)holdingRptDataSet.Tables[HoldingRptDataSet.CLIENTSUMMARYTABLE].Rows[0][HoldingRptDataSet.CLIENTTYPE];
                }

                row["ClientType"] = clientType;
                row["ClientID"] = organizationUnit.EclipseClientID;
                row["IsClient"] = organizationUnit.IsInvestableClient;
                row["Holding"] = holdingTotal;
                row["Unsettled"] = unsettleTotal;
                row["Total"] = totalTotal;
                row["OtherID"] = organizationUnit.OtherID;
                row["Status"] = Enumeration.GetDescription(organizationUnit.StatusType);
                var adviserInfo = ExtractAdviserInfo(organizationUnit);
                row["AdviserID"] = adviserInfo.First().Key;
                row["AdviserFullName"] = adviserInfo.First().Value;                
                row["AccountType"] = organizationUnit.ComponentInformation.DisplayName;
                ICmHasParent iCmHasParent = organizationUnit as ICmHasParent;
                row["AdviserCID"] = iCmHasParent.Parent.Cid == null ? Guid.Empty : iCmHasParent.Parent.Cid;

                this.Broker.ReleaseBrokerManagedComponent(organizationUnit);
            }
            if (organisationListingDS.OrganisationFilterType == OrganisationFilterType.ClientByID)
            {
                DataView viewByID = new DataView(searchedFilterTable);
                viewByID.RowFilter = "ClientID LIKE '%" + organisationListingDS.ClientNameSearchFilter + "%'";
                organisationListingDS.Tables.Add(viewByID.ToTable());
            }
            else
                organisationListingDS.Tables.Add(searchedFilterTable);

            this.Broker.ReleaseBrokerManagedComponent(user);
        }

        private void ExtractByClientsWithFUMDIFM(DataTable orgFiteredTable)
        {
            orgFiteredTable.Columns.Add("IsClient", typeof(bool));
            orgFiteredTable.Columns.Add("IsDIFM", typeof(bool));
            orgFiteredTable.Columns.Add("ClientType", typeof(string));
            orgFiteredTable.Columns.Add("ClientID", typeof(string));
            orgFiteredTable.Columns.Add("Holding", typeof(decimal));
            orgFiteredTable.Columns.Add("Unsettled", typeof(decimal));
            orgFiteredTable.Columns.Add("Total", typeof(decimal));

            foreach (DataRow row in orgFiteredTable.Rows)
            {
                Guid clid = new Guid(row["ENTITYCLID_FIELD"].ToString());
                Guid csid = new Guid(row["ENTITYCSID_FIELD"].ToString());
                IOrganizationUnit organizationUnit = this.Broker.GetCMImplementation(clid, csid) as IOrganizationUnit;

                decimal holdingTotal = 0;
                decimal unsettleTotal = 0;
                decimal totalTotal = 0;
                string clientType = string.Empty;

                if (organizationUnit.IsInvestableClient && organizationUnit.HasDIFM)
                {
                    HoldingRptDataSet holdingRptDataSet = new HoldingRptDataSet();
                    holdingRptDataSet.ServiceTypes = ServiceTypes.DoItForMe;
                    organizationUnit.GetData(holdingRptDataSet);

                    foreach (DataRow holdingRow in holdingRptDataSet.HoldingSummaryTable.Rows)
                    {
                        holdingTotal += Convert.ToDecimal(holdingRow[holdingRptDataSet.HoldingSummaryTable.HOLDING]);
                        unsettleTotal += Convert.ToDecimal(holdingRow[holdingRptDataSet.HoldingSummaryTable.UNSETTLED]);
                        totalTotal += Convert.ToDecimal(holdingRow[holdingRptDataSet.HoldingSummaryTable.TOTAL]);
                    }
                    clientType = (String)holdingRptDataSet.Tables[HoldingRptDataSet.CLIENTSUMMARYTABLE].Rows[0][HoldingRptDataSet.CLIENTTYPE];
                }

                row["IsDIFM"] = organizationUnit.HasDIFM;
                row["ClientType"] = clientType;
                row["ClientID"] = organizationUnit.ClientId;
                row["IsClient"] = organizationUnit.IsInvestableClient;
                row["Holding"] = holdingTotal;
                row["Unsettled"] = unsettleTotal;
                row["Total"] = totalTotal;

                this.Broker.ReleaseBrokerManagedComponent(organizationUnit);
            }
        }

        private DataTable ExtractByClientsWithDuplicatedAssociation(DataTable orgFiteredTable)
        {
            DuplicatedAssociationsClientsDS duplicatedAssociationsClientsDS = new DuplicatedAssociationsClientsDS();

            foreach (DataRow row in orgFiteredTable.Rows)
            {
                Guid ciid = new Guid(row["ENTITYCIID_FIELD"].ToString());
                IOrganizationUnit organizationUnit = this.Broker.GetBMCInstance(ciid) as IOrganizationUnit;

                if (organizationUnit.IsInvestableClient)
                    organizationUnit.GetData(duplicatedAssociationsClientsDS);

                foreach (DataRow duprow in duplicatedAssociationsClientsDS.Tables[DuplicatedAssociationsClientsDS.DUPASSOCIATIONTABLE].Rows)
                {
                    ProductEntity proEnt = this.Products.Where(pro => pro.ID == (Guid)duprow[DuplicatedAssociationsClientsDS.PRODUCTID]).FirstOrDefault();

                    if (proEnt != null)
                    {
                        duprow[DuplicatedAssociationsClientsDS.PRODUCTDESC] = proEnt.Description;
                        duprow[DuplicatedAssociationsClientsDS.PRODUCTNAME] = proEnt.Name;
                    }
                }

                this.Broker.ReleaseBrokerManagedComponent(organizationUnit);
            }

            return duplicatedAssociationsClientsDS.Tables[DuplicatedAssociationsClientsDS.DUPASSOCIATIONTABLE];
        }

        private void ExtractByAssetsSummaryOverall(OrganisationListingDS organisationListingDS, DataTable orgFiteredTable)
        {
            orgFiteredTable.Columns.Add("IsClient", typeof(bool));
            orgFiteredTable.Columns.Add("ClientType", typeof(string));
            orgFiteredTable.Columns.Add("ClientID", typeof(string));
            orgFiteredTable.Columns.Add("Holding", typeof(decimal));
            orgFiteredTable.Columns.Add("Unsettled", typeof(decimal));
            orgFiteredTable.Columns.Add("Total", typeof(decimal));

            HoldingRptDataSet holdingRptDataSetMain = new HoldingRptDataSet();
            holdingRptDataSetMain.ValuationDate = organisationListingDS.AsOfDate;
            foreach (DataRow row in orgFiteredTable.Rows)
            {
                Guid clid = new Guid(row["ENTITYCLID_FIELD"].ToString());
                Guid csid = new Guid(row["ENTITYCSID_FIELD"].ToString());
                IOrganizationUnit organizationUnit = this.Broker.GetCMImplementation(clid, csid) as IOrganizationUnit;

                decimal holdingTotal = 0;
                decimal unsettleTotal = 0;
                decimal totalTotal = 0;
                string clientType = string.Empty;

                if (organizationUnit.IsInvestableClient)
                {
                    HoldingRptDataSet holdingRptDataSet = new HoldingRptDataSet();
                    holdingRptDataSet.ValuationDate = organisationListingDS.AsOfDate;
                    organizationUnit.GetData(holdingRptDataSet);

                    holdingTotal = holdingRptDataSet.HoldingSummaryTable.Select().Sum(holdingRow => Convert.ToDecimal(holdingRow[holdingRptDataSet.HoldingSummaryTable.HOLDING]));
                    unsettleTotal = holdingRptDataSet.HoldingSummaryTable.Select().Sum(holdingRow => Convert.ToDecimal(holdingRow[holdingRptDataSet.HoldingSummaryTable.UNSETTLED]));
                    totalTotal = holdingRptDataSet.HoldingSummaryTable.Select().Sum(holdingRow => Convert.ToDecimal(holdingRow[holdingRptDataSet.HoldingSummaryTable.TOTAL]));
                    if (holdingRptDataSet.Tables[HoldingRptDataSet.CLIENTSUMMARYTABLE].Rows.Count > 0)
                        clientType = (String)holdingRptDataSet.Tables[HoldingRptDataSet.CLIENTSUMMARYTABLE].Rows[0][HoldingRptDataSet.CLIENTTYPE];

                    holdingRptDataSetMain.Merge(holdingRptDataSet);
                }

                row["ClientType"] = clientType;
                row["ClientID"] = organizationUnit.ClientId;
                row["IsClient"] = organizationUnit.IsInvestableClient;
                row["Holding"] = holdingTotal;
                row["Unsettled"] = unsettleTotal;
                row["Total"] = totalTotal;

                this.Broker.ReleaseBrokerManagedComponent(organizationUnit);
            }

            organisationListingDS.Merge(holdingRptDataSetMain);
        }

        private void ExtractClientsFUM(DataTable orgFiteredTable, DateTime selectedDate, ServiceTypes serviceType)
        {
            orgFiteredTable.Columns.Add("IsClient", typeof(bool));
            orgFiteredTable.Columns.Add("ClientType", typeof(string));
            orgFiteredTable.Columns.Add("AccountType", typeof(string));
            orgFiteredTable.Columns.Add("Status", typeof(string));
            orgFiteredTable.Columns.Add("OtherID", typeof(string));
            orgFiteredTable.Columns.Add("ClientID", typeof(string));
            orgFiteredTable.Columns.Add("Holding", typeof(decimal));
            orgFiteredTable.Columns.Add("Unsettled", typeof(decimal));
            orgFiteredTable.Columns.Add("Total", typeof(decimal));
            orgFiteredTable.Columns.Add("AdviserFullName", typeof(string));
            orgFiteredTable.Columns.Add("AdviserID", typeof(string));
            orgFiteredTable.Columns.Add("AddressLine1", typeof(string));
            orgFiteredTable.Columns.Add("AddressLine2", typeof(string));
            orgFiteredTable.Columns.Add("Suburb", typeof(string));
            orgFiteredTable.Columns.Add("PostCode", typeof(string));
            orgFiteredTable.Columns.Add("State", typeof(string));
            orgFiteredTable.Columns.Add("Country", typeof(string));
            orgFiteredTable.Columns.Add("TFN", typeof(string));
            orgFiteredTable.Columns.Add("ABN", typeof(string));
            orgFiteredTable.Columns.Add("MDAEXECUTIONDATE", typeof(DateTime));

            orgFiteredTable.Columns.Add("PcFirstName_1", typeof(string));
            orgFiteredTable.Columns.Add("PcSurname_1", typeof(string));
            orgFiteredTable.Columns.Add("PcMiddleName_1", typeof(string));
            orgFiteredTable.Columns.Add("PcEmail_1", typeof(string));

            orgFiteredTable.Columns.Add("PcFirstName_2", typeof(string));
            orgFiteredTable.Columns.Add("PcSurname_2", typeof(string));
            orgFiteredTable.Columns.Add("PcMiddleName_2", typeof(string));
            orgFiteredTable.Columns.Add("PcEmail_2", typeof(string));

            orgFiteredTable.Columns.Add("PcFirstName_3", typeof(string));
            orgFiteredTable.Columns.Add("PcSurname_3", typeof(string));
            orgFiteredTable.Columns.Add("PcMiddleName_3", typeof(string));
            orgFiteredTable.Columns.Add("PcEmail_3", typeof(string));

            orgFiteredTable.Columns.Add("PcFirstName_4", typeof(string));
            orgFiteredTable.Columns.Add("PcSurname_4", typeof(string));
            orgFiteredTable.Columns.Add("PcMiddleName_4", typeof(string));
            orgFiteredTable.Columns.Add("PcEmail_4", typeof(string));

            orgFiteredTable.Columns.Add("PcFirstName_5", typeof(string));
            orgFiteredTable.Columns.Add("PcSurname_5", typeof(string));
            orgFiteredTable.Columns.Add("PcMiddleName_5", typeof(string));
            orgFiteredTable.Columns.Add("PcEmail_5", typeof(string));

            orgFiteredTable.Columns.Add("PcFirstName_6", typeof(string));
            orgFiteredTable.Columns.Add("PcSurname_6", typeof(string));
            orgFiteredTable.Columns.Add("PcMiddleName_6", typeof(string));
            orgFiteredTable.Columns.Add("PcEmail_6", typeof(string));

            //1022 Add Service Type
            orgFiteredTable.Columns.Add("IsDIFM", typeof(string));
            orgFiteredTable.Columns.Add("DIFMModelName", typeof(string));
            orgFiteredTable.Columns.Add("IsDIWM", typeof(string));
            orgFiteredTable.Columns.Add("DIWMModelName", typeof(string));
            orgFiteredTable.Columns.Add("IsDIY", typeof(string));
            orgFiteredTable.Columns.Add("DIYModelName", typeof(string));

            //Added Accounts
            orgFiteredTable.Columns.Add("DesktopBrokerAccName", typeof(string));
            orgFiteredTable.Columns.Add("DesktopBrokerAccNo", typeof(string));
            orgFiteredTable.Columns.Add("OtherIDDesktopBroker", typeof(string));
            orgFiteredTable.Columns.Add("JPBankAccountBSB", typeof(string));
            orgFiteredTable.Columns.Add("JPBankAccountAccNo", typeof(string));
            orgFiteredTable.Columns.Add("JPBankAccountAccName", typeof(string));
            orgFiteredTable.Columns.Add("JPBankAccountAccOtherID", typeof(string));
            orgFiteredTable.Columns.Add("FIGGBankAccountBSB", typeof(string));
            orgFiteredTable.Columns.Add("FIGGBankAccountAccNo", typeof(string));
            orgFiteredTable.Columns.Add("FIGGBankAccountAccName", typeof(string));
            orgFiteredTable.Columns.Add("FIGGBankAccountAccOtherID", typeof(string));


            foreach (DataRow row in orgFiteredTable.Rows)
            {
                string ServiceTypes = string.Empty;
                string ServiceTypeDIFM = string.Empty;
                string ServiceTypeDIWM = string.Empty;
                string ServiceTypeDIYM = string.Empty;

                Guid clid = new Guid(row["ENTITYCLID_FIELD"].ToString());
                Guid csid = new Guid(row["ENTITYCSID_FIELD"].ToString());
                IOrganizationUnit organizationUnit = this.Broker.GetCMImplementation(clid, csid) as IOrganizationUnit;

                decimal holdingTotal = 0;
                decimal unsettleTotal = 0;
                decimal totalTotal = 0;
                string clientType = string.Empty;

                if (organizationUnit.IsInvestableClient)
                {
                    HoldingRptDataSet holdingRptDataSet = new HoldingRptDataSet();
                    holdingRptDataSet.ValuationDate = selectedDate;
                    holdingRptDataSet.ServiceTypes = serviceType;
                    organizationUnit.GetData(holdingRptDataSet);

                    holdingTotal = holdingRptDataSet.HoldingSummaryTable.Select().Sum(holdingRow => Convert.ToDecimal(holdingRow[holdingRptDataSet.HoldingSummaryTable.HOLDING]));
                    unsettleTotal = holdingRptDataSet.HoldingSummaryTable.Select().Sum(holdingRow => Convert.ToDecimal(holdingRow[holdingRptDataSet.HoldingSummaryTable.UNSETTLED]));
                    totalTotal = holdingRptDataSet.HoldingSummaryTable.Select().Sum(holdingRow => Convert.ToDecimal(holdingRow[holdingRptDataSet.HoldingSummaryTable.TOTAL]));
                    clientType = (String)holdingRptDataSet.Tables[HoldingRptDataSet.CLIENTSUMMARYTABLE].Rows[0][HoldingRptDataSet.CLIENTTYPE];

                    ICmHasParent iCmHasParent = organizationUnit as ICmHasParent;

                    var adviserInfo = ExtractAdviserInfo(organizationUnit);
                    row["AdviserID"] = adviserInfo.First().Key;
                    row["AdviserFullName"] = adviserInfo.First().Value;

                    IClientUMAData clientUMAData = organizationUnit.ClientEntity as IClientUMAData;
                    List<Oritax.TaxSimp.Common.IdentityCMDetail> contacts = new List<TaxSimp.Common.IdentityCMDetail>();
                    List<AccountProcessTaskEntity> accountProcessTaskEntityList = (clientUMAData as IClientEntityOrgUnits).ListAccountProcess;

                    List<ModelEntity> modelEntityList = new List<ModelEntity>();
                    var modelGroup = accountProcessTaskEntityList.GroupBy(lp => lp.ModelID);

                    foreach (var modelGroupItem in modelGroup)
                    {
                        var model = this.Model.Where(ss => ss.ID == modelGroupItem.Key).FirstOrDefault();
                        modelEntityList.Add(model);
                    }

                    SetAddressClientEntity(row, clientUMAData.Address);
                    row["TFN"] = clientUMAData.ABN;
                    row["ABN"] = clientUMAData.TFN;

                    if (clientUMAData.MDAExecutionDate.HasValue)
                        row["MDAEXECUTIONDATE"] = clientUMAData.MDAExecutionDate;

                    if (clientUMAData.Servicetype.DO_IT_FOR_ME == true)
                    {
                        row["IsDIFM"] = "1";
                        var model = modelEntityList.Where(m => m != null && m.ServiceType == TaxSimp.Data.ServiceTypes.DoItForMe).FirstOrDefault();
                        if (model != null)
                            row["DIFMModelName"] = model.ProgramCode + " - " + model.Name;
                    }

                    else
                        row["IsDIFM"] = "0";

                    if (clientUMAData.Servicetype.DO_IT_WITH_ME == true)
                    {
                        row["IsDIWM"] = "1";
                        var model = modelEntityList.Where(m => m != null && m.ServiceType == TaxSimp.Data.ServiceTypes.DoItWithMe).FirstOrDefault();
                        if (model != null)
                            row["DIWMModelName"] = model.ProgramCode + " - " + model.Name;
                    }
                    else
                        row["IsDIWM"] = "0";

                    if (clientUMAData.Servicetype.DO_IT_YOURSELF == true)
                    {
                        row["IsDIY"] = "1";
                        var model = modelEntityList.Where(m => m != null && m.ServiceType == TaxSimp.Data.ServiceTypes.DoItYourSelf).FirstOrDefault();
                        if (model != null)
                            row["DIYModelName"] = model.ProgramCode + " - " + model.Name;
                    }
                    else
                        row["IsDIY"] = "0";

                    int count = 1;

                    row["PcFirstName_1"] = "";
                    row["PcSurname_1"] = "";
                    row["PcMiddleName_1"] = "";
                    row["PcEmail_1"] = "";

                    row["PcFirstName_2"] = "";
                    row["PcSurname_2"] = "";
                    row["PcMiddleName_2"] = "";
                    row["PcEmail_2"] = "";

                    row["PcFirstName_3"] = "";
                    row["PcSurname_3"] = "";
                    row["PcMiddleName_3"] = "";
                    row["PcEmail_3"] = "";

                    row["PcFirstName_4"] = "";
                    row["PcSurname_4"] = "";
                    row["PcMiddleName_4"] = "";
                    row["PcEmail_4"] = "";

                    row["PcFirstName_5"] = "";
                    row["PcSurname_5"] = "";
                    row["PcMiddleName_5"] = "";
                    row["PcEmail_5"] = "";

                    row["PcFirstName_6"] = "";
                    row["PcSurname_6"] = "";
                    row["PcMiddleName_6"] = "";
                    row["PcEmail_6"] = "";


                    foreach (Common.IdentityCMDetail identityCMDetail in contacts)
                    {
                        IOrganizationUnit individualUnit = Broker.GetBMCInstance(identityCMDetail.Cid) as IOrganizationUnit;
                        if (individualUnit == null)
                            individualUnit = Broker.GetCMImplementation(identityCMDetail.Clid, identityCMDetail.Csid) as IOrganizationUnit;

                        if (individualUnit != null)
                        {
                            Oritax.TaxSimp.Common.IndividualEntity individualEntity = individualUnit.ClientEntity as Oritax.TaxSimp.Common.IndividualEntity;
                            if (individualEntity != null)
                            {
                                row["PcFirstName_" + count.ToString()] = individualEntity.Name;
                                row["PcSurname_" + count.ToString()] = individualEntity.Surname;
                                row["PcMiddleName_" + count.ToString()] = individualEntity.MiddleName;
                                row["PcEmail_" + count.ToString()] = individualEntity.EmailAddress;
                                count++;
                            }
                        }
                    }

                    row["ClientType"] = clientType;
                    row["ClientType"] = clientType;
                    row["ClientID"] = organizationUnit.ClientId;
                    row["IsClient"] = organizationUnit.IsInvestableClient;
                    row["Holding"] = holdingTotal;
                    row["Unsettled"] = unsettleTotal;
                    row["Total"] = totalTotal;
                    row["OtherID"] = organizationUnit.OtherID;
                    row["Status"] = Enumeration.GetDescription(organizationUnit.StatusType);
                    row["AccountType"] = organizationUnit.ComponentInformation.DisplayName;

                    ClientIndividualEntity clientIndividualEntity = organizationUnit.ClientEntity as ClientIndividualEntity;
                    if (clientIndividualEntity != null)
                    {
                        Common.IdentityCMDetail asxIdentityCM = clientIndividualEntity.DesktopBrokerAccounts.FirstOrDefault();
                        if (asxIdentityCM != null)
                        {
                            IOrganizationUnit asxOrgUnit = Broker.GetCMImplementation(asxIdentityCM.Clid, asxIdentityCM.Csid) as IOrganizationUnit;
                            if (asxOrgUnit != null)
                            {
                                DesktopBrokerAccountEntity asxEntity = asxOrgUnit.ClientEntity as DesktopBrokerAccountEntity;
                                if (asxEntity != null)
                                {
                                    row["DesktopBrokerAccName"] = asxOrgUnit.Name;
                                    row["DesktopBrokerAccNo"] = asxEntity.AccountNumber;
                                    row["OtherIDDesktopBroker"] = asxOrgUnit.OtherID;
                                }
                            }
                        }

                        foreach (var bankCMItem in clientIndividualEntity.BankAccounts)
                        {
                            if (bankCMItem != null)
                            {
                                IOrganizationUnit bankOrgUnit = Broker.GetCMImplementation(bankCMItem.Clid, bankCMItem.Csid) as IOrganizationUnit;
                                if (bankOrgUnit != null)
                                {
                                    BankAccountEntity bankAccountEntity = bankOrgUnit.ClientEntity as BankAccountEntity;
                                    if (bankAccountEntity != null)
                                    {
                                        if (bankAccountEntity.AccoutType.ToLower() == "cma")
                                        {
                                            row["JPBankAccountBSB"] = bankAccountEntity.BSB;
                                            row["JPBankAccountAccNo"] = bankAccountEntity.AccountNumber;
                                            row["JPBankAccountAccName"] = bankOrgUnit.Name;
                                            row["JPBankAccountAccOtherID"] = bankOrgUnit.OtherID;
                                        }

                                        if (bankAccountEntity.AccoutType.ToLower() == "term deposit")
                                        {
                                            row["FIGGBankAccountBSB"] = bankAccountEntity.BSB;
                                            row["FIGGBankAccountAccNo"] = bankAccountEntity.AccountNumber;
                                            row["FIGGBankAccountAccName"] = bankOrgUnit.Name;
                                            row["FIGGBankAccountAccOtherID"] = bankOrgUnit.OtherID;
                                        }
                                    }
                                }
                            }
                        }
                    }

                }

                this.Broker.ReleaseBrokerManagedComponent(organizationUnit);
            }
        }

        private void ExtractIndividualList(OrganisationListingDS organisationListingDS)
        {
            IBrokerManagedComponent user = this.Broker.GetBMCInstance(this.Broker.UserContext.Identity.Name, "DBUser_1_1");
            object[] args = new Object[3] { this.CLID, user.CID, EnableSecurity.SecuritySetting };
            DataView view = new DataView(this.Broker.GetBMCDetailList(OrganizationInstall.ASSEMBLY_NAME, args).Tables[TableName]);
            view.RowFilter = OrganisationListingDS.GetRowFilterIndividualList();
            view.Sort = "ENTITYNAME_FIELD ASC";
            DataTable orgFiteredTable = view.ToTable();

            IndividualDS indDS = new IndividualDS();

            foreach (DataRow row in orgFiteredTable.Rows)
            {
                Guid ciid = new Guid(row["ENTITYCIID_FIELD"].ToString());
                IOrganizationUnit organizationUnit = this.Broker.GetBMCInstance(ciid) as IOrganizationUnit;
                organizationUnit.GetData(indDS);
                this.Broker.ReleaseBrokerManagedComponent(organizationUnit);
            }

            organisationListingDS.Tables.Add(indDS.Tables[indDS.IndividualTable.TABLENAME].Copy());
        }

        private void ExtractIndividualListWithoutDetails(OrganisationListingDS organisationListingDS)
        {
            string userName = string.Empty;

            if (Broker.UserContext == null)
                userName = "Administrator";
            else
                userName = this.Broker.UserContext.Identity.Name; 

            IBrokerManagedComponent user = this.Broker.GetBMCInstance(userName, "DBUser_1_1");
            object[] args = new Object[3] { this.CLID, user.CID, EnableSecurity.SecuritySetting };
            DataView view = new DataView(this.Broker.GetBMCDetailList(OrganizationInstall.ASSEMBLY_NAME, args).Tables[TableName]);
            view.RowFilter = OrganisationListingDS.GetRowFilterIndividualList();
            view.Sort = "ENTITYNAME_FIELD ASC";
            DataTable orgFiteredTable = view.ToTable();
            orgFiteredTable.TableName = OrganisationListingDS.INDIVIDUALLISTWITHOUTDETAILSTABLENAME;
            organisationListingDS.Tables.Clear();
            organisationListingDS.Tables.Add(orgFiteredTable);
        }

        private Dictionary<string, string> ExtractAdviserInfo(IOrganizationUnit organizationUnit)
        {
            ICmHasParent iCmHasParent = organizationUnit as ICmHasParent;
            Dictionary<string, string> adviserInfo = new Dictionary<string, string>();

            if (iCmHasParent.Parent != null && iCmHasParent.Parent.Cid != Guid.Empty)
            {
                OrganizationUnitCM adviserCM = Broker.GetBMCInstance(iCmHasParent.Parent.Cid) as OrganizationUnitCM;
                adviserInfo.Add(adviserCM.ClientId, adviserCM.FullName());                
            }
            else if (iCmHasParent.Parent != null && iCmHasParent.Parent.Clid != Guid.Empty && iCmHasParent.Parent.Csid != Guid.Empty)
            {
                OrganizationUnitCM adviserCM = Broker.GetCMImplementation(iCmHasParent.Parent.Clid, iCmHasParent.Parent.Csid) as OrganizationUnitCM;
                adviserInfo.Add(adviserCM.ClientId, adviserCM.FullName());
            }
            else
                adviserInfo.Add(string.Empty, string.Empty);
            return adviserInfo;
        }

        private void ExtractClientseClipseSuper(DataTable orgFiteredTable, DateTime selectedDate, ServiceTypes serviceType)
        {
            orgFiteredTable.Columns.Add("IsClient", typeof(bool));
            orgFiteredTable.Columns.Add("ClientID", typeof(string));
            orgFiteredTable.Columns.Add("SuperMemberID", typeof(string));
            orgFiteredTable.Columns.Add("SuperClientID", typeof(string));

            foreach (DataRow row in orgFiteredTable.Rows)
            {
                Guid clid = new Guid(row["ENTITYCLID_FIELD"].ToString());
                Guid csid = new Guid(row["ENTITYCSID_FIELD"].ToString());
                IOrganizationUnit organizationUnit = this.Broker.GetCMImplementation(clid, csid) as IOrganizationUnit;

                if (organizationUnit.IsInvestableClient)
                {
                    row["ClientID"] = organizationUnit.ClientId;
                    row["IsClient"] = organizationUnit.IsInvestableClient;
                    row["SuperMemberID"] = organizationUnit.SuperMemberID;
                    row["SuperClientID"] = organizationUnit.SuperClientID;
                }

                this.Broker.ReleaseBrokerManagedComponent(organizationUnit);
            }
        }

        private DataTable ExtractClientsSECFUM(DataTable orgFiteredTable, DateTime selectedDate, ServiceTypes serviceType)
        {
            SecuritySummaryReportDS masterSecuritySummaryReportDS = new SecuritySummaryReportDS();

            foreach (DataRow row in orgFiteredTable.Rows)
            {
                Guid ciid = new Guid(row["ENTITYCIID_FIELD"].ToString());
                IOrganizationUnit organizationUnit = this.Broker.GetBMCInstance(ciid) as IOrganizationUnit;

                if (organizationUnit.IsInvestableClient)
                {
                    SecuritySummaryReportDS securitySummaryReportDS = new SecuritySummaryReportDS();
                    securitySummaryReportDS.ValuationDate = selectedDate;
                    securitySummaryReportDS.ServiceTypes = serviceType;
                    organizationUnit.GetData(securitySummaryReportDS);

                    masterSecuritySummaryReportDS.Tables[SecuritySummaryReportDS.SECURITYSUMMARYTABLE].Merge(securitySummaryReportDS.Tables[SecuritySummaryReportDS.SECURITYSUMMARYTABLE]);
                }
                this.Broker.ReleaseBrokerManagedComponent(organizationUnit);
            }

            return masterSecuritySummaryReportDS.Tables[SecuritySummaryReportDS.SECURITYSUMMARYTABLE];
        }

        private void ExtractClientsDIS(OrganisationListingDS organisationListingDS, DataTable orgFiteredTable)
        {
            ClientDistributionsDS clientDistributionsDSMaterDS = new ClientDistributionsDS();

            foreach (DataRow row in orgFiteredTable.Rows)
            {
                Guid ciid = new Guid(row["ENTITYCIID_FIELD"].ToString());
                IOrganizationUnit organizationUnit = this.Broker.GetBMCInstance(ciid) as IOrganizationUnit;

                if (organizationUnit.IsInvestableClient)
                {
                    ClientDistributionsDS clientDistributionsDS = new ClientDistributionsDS();
                    organizationUnit.GetData(clientDistributionsDS);
                    clientDistributionsDSMaterDS.Merge(clientDistributionsDS, true);
                }

                this.Broker.ReleaseBrokerManagedComponent(organizationUnit);
            }
            organisationListingDS.Merge(clientDistributionsDSMaterDS, true);
        }

        private static void SetAddressClientEntity(DataRow row, Oritax.TaxSimp.Common.DualAddressEntity Address)
        {
            if (Address.BusinessAddress.Addressline1 != string.Empty)
            {
                row["AddressLine1"] = Address.BusinessAddress.Addressline1;
                row["AddressLine2"] = Address.BusinessAddress.Addressline2;
                row["Suburb"] = Address.BusinessAddress.Suburb;
                row["State"] = Address.BusinessAddress.State;
                row["PostCode"] = Address.BusinessAddress.PostCode;
                row["Country"] = Address.BusinessAddress.Country;
            }
            else if (Address.RegisteredAddress.Addressline1 != string.Empty)
            {
                row["AddressLine1"] = Address.RegisteredAddress.Addressline1;
                row["AddressLine2"] = Address.RegisteredAddress.Addressline2;
                row["Suburb"] = Address.RegisteredAddress.Suburb;
                row["State"] = Address.RegisteredAddress.State;
                row["PostCode"] = Address.RegisteredAddress.PostCode;
                row["Country"] = Address.RegisteredAddress.Country;
            }
            else if (Address.ResidentialAddress.Addressline1 != string.Empty)
            {
                row["AddressLine1"] = Address.ResidentialAddress.Addressline1;
                row["AddressLine2"] = Address.ResidentialAddress.Addressline2;
                row["Suburb"] = Address.ResidentialAddress.Suburb;
                row["State"] = Address.ResidentialAddress.State;
                row["PostCode"] = Address.ResidentialAddress.PostCode;
                row["Country"] = Address.ResidentialAddress.Country;
            }
        }

        private DataTable ExtractAdviserListing()
        {
            IBrokerManagedComponent user = this.Broker.GetBMCInstance("Administrator", "DBUser_1_1");
            object[] args = new Object[3] { this.CLID, user.CID, EnableSecurity.SecuritySetting };

            DataView adviserView = new DataView(this.Broker.GetBMCDetailList(OrganizationInstall.ASSEMBLY_NAME, args).Tables[TableName]);
            adviserView.RowFilter = OrganisationListingDS.GetRowFilterAdvisers();
            adviserView.Sort = "ENTITYNAME_FIELD ASC";
            DataTable adviserListingTable = adviserView.ToTable("AdviserListing");
            adviserListingTable.TableName = OrganisationListingDS.ADVISERLISTINGTABLE;
            adviserListingTable.Columns.Add("AdviserID", typeof(string));
            adviserListingTable.Columns.Add("IFAName", typeof(string));
            return adviserListingTable;
        }

        private void ExtractClientsWithAdviser(DataTable orgFiteredTable)
        {
            orgFiteredTable.TableName = OrganisationListingDS.ADVISERANDCLIENTTABLE;
            orgFiteredTable.Columns.Add("IsClient", typeof(bool));
            orgFiteredTable.Columns.Add("ClientID", typeof(string));
            orgFiteredTable.Columns.Add("Holding", typeof(decimal));
            orgFiteredTable.Columns.Add("Unsettled", typeof(decimal));
            orgFiteredTable.Columns.Add("Total", typeof(decimal));
            orgFiteredTable.Columns.Add("AdviserID", typeof(string));
            orgFiteredTable.Columns.Add("AdviserName", typeof(string));
            orgFiteredTable.Columns.Add("AdviserCID", typeof(string));

            foreach (DataRow row in orgFiteredTable.Rows)
            {
                Guid clid = new Guid(row["ENTITYCLID_FIELD"].ToString());
                Guid csid = new Guid(row["ENTITYCSID_FIELD"].ToString());
                IOrganizationUnit organizationUnit = this.Broker.GetCMImplementation(clid, csid) as IOrganizationUnit;
                decimal holdingTotal = 0;
                decimal unsettleTotal = 0;
                decimal totalTotal = 0;
                string clientType = string.Empty;
                string adviserID = string.Empty;
                string adviserCID = string.Empty;
                string adviserName = string.Empty;

                if (organizationUnit.IsInvestableClient)
                {
                    if (organizationUnit.AdviserID != Guid.Empty)
                    {
                        OrganizationUnitCM adviserUnit = this.Broker.GetBMCInstance(organizationUnit.AdviserID) as OrganizationUnitCM;
                        adviserName = adviserUnit.FullName();
                        adviserID = adviserUnit.ClientId;
                        adviserCID = adviserUnit.CID.ToString();
                        Broker.ReleaseBrokerManagedComponent(adviserUnit);
                    }
                }
                row["AdviserID"] = adviserID;
                row["AdviserCID"] = adviserCID;
                row["AdviserName"] = adviserName;
                row["ClientID"] = organizationUnit.ClientId;
                row["IsClient"] = organizationUnit.IsInvestableClient;
                row["Holding"] = holdingTotal;
                row["Unsettled"] = unsettleTotal;
                row["Total"] = totalTotal;

                this.Broker.ReleaseBrokerManagedComponent(organizationUnit);
            }
        }

        private void ExtractGroupFUM(OrganisationListingDS organisationListingDS)
        {
            IBrokerManagedComponent user = this.Broker.GetBMCInstance(this.Broker.UserContext.Identity.Name, "DBUser_1_1");
            object[] args = new Object[3] { this.CLID, user.CID, EnableSecurity.SecuritySetting };

            DataView groupView = new DataView(this.Broker.GetBMCDetailList(OrganizationInstall.ASSEMBLY_NAME, args).Tables[TableName]);
            groupView.RowFilter = organisationListingDS.GetRowFilterGroup();
            groupView.Sort = "ENTITYNAME_FIELD ASC";
            DataTable orgFiteredTable = groupView.ToTable();

            orgFiteredTable.Columns.Add("IsClient", typeof(bool));
            orgFiteredTable.Columns.Add("ClientType", typeof(string));
            orgFiteredTable.Columns.Add("ClientID", typeof(string));
            orgFiteredTable.Columns.Add("Holding", typeof(decimal));
            orgFiteredTable.Columns.Add("Unsettled", typeof(decimal));
            orgFiteredTable.Columns.Add("Total", typeof(decimal));

            foreach (DataRow row in orgFiteredTable.Rows)
            {
                Guid clid = new Guid(row["ENTITYCLID_FIELD"].ToString());
                Guid csid = new Guid(row["ENTITYCSID_FIELD"].ToString());
                IOrganizationUnit organizationUnit = this.Broker.GetCMImplementation(clid, csid) as IOrganizationUnit;

                decimal holdingTotal = 0;
                decimal unsettleTotal = 0;
                decimal totalTotal = 0;
                string clientType = string.Empty;

                HoldingRptDataSet holdingRptDataSet = new HoldingRptDataSet();
                organizationUnit.GetData(holdingRptDataSet);

                holdingTotal = holdingRptDataSet.HoldingSummaryTable.Select().Sum(holdingRow => Convert.ToDecimal(holdingRow[holdingRptDataSet.HoldingSummaryTable.HOLDING]));
                unsettleTotal = holdingRptDataSet.HoldingSummaryTable.Select().Sum(holdingRow => Convert.ToDecimal(holdingRow[holdingRptDataSet.HoldingSummaryTable.UNSETTLED]));
                totalTotal = holdingRptDataSet.HoldingSummaryTable.Select().Sum(holdingRow => Convert.ToDecimal(holdingRow[holdingRptDataSet.HoldingSummaryTable.TOTAL]));
                clientType = (String)holdingRptDataSet.Tables[HoldingRptDataSet.CLIENTSUMMARYTABLE].Rows[0][HoldingRptDataSet.CLIENTTYPE];

                row["ClientType"] = clientType;
                row["ClientID"] = organizationUnit.ClientId;
                row["IsClient"] = organizationUnit.IsInvestableClient;
                row["Holding"] = holdingTotal;
                row["Unsettled"] = unsettleTotal;
                row["Total"] = totalTotal;

                this.Broker.ReleaseBrokerManagedComponent(organizationUnit);
            }
        }

        private void UsersWithPassword(OrganisationListingDS organisationListingDS, object[] args)
        {
            DataView userView = new DataView(this.Broker.GetBMCDetailList(OrganizationInstall.ASSEMBLY_NAME, args).Tables[TableName]);
            userView.RowFilter = organisationListingDS.GetRowFilterUsers();
            userView.Sort = "ENTITYNAME_FIELD ASC";
            DataTable userFiteredTable = userView.ToTable();
            userFiteredTable.Columns.Add("Password", typeof(string));
            userFiteredTable.TableName = "UsersWithPassword";

            foreach (DataRow row in userFiteredTable.Rows)
            {
                Guid clid = new Guid(row["ENTITYCLID_FIELD"].ToString());
                Guid csid = new Guid(row["ENTITYCSID_FIELD"].ToString());
                string password = string.Empty;
            }

            organisationListingDS.Tables.Add(userFiteredTable);
        }

        private void ExtractByFeeRuns(OrganisationListingDS organisationListingDS, object[] args)
        {
            DataView feeRunsView = new DataView(this.Broker.GetBMCDetailList(OrganizationInstall.ASSEMBLY_NAME, args).Tables[TableName]);
            feeRunsView.RowFilter = organisationListingDS.GetRowFilterFeeRuns();
            feeRunsView.Sort = "ENTITYNAME_FIELD ASC";

            DataTable feeRunTable = feeRunsView.ToTable(FeeRunListDS.FEERUNSTABLE);
            feeRunTable.Columns.Add(FeeRunListDS.ID, typeof(Guid));
            feeRunTable.Columns.Add(FeeRunListDS.DESCRIPTION, typeof(string));
            feeRunTable.Columns.Add(FeeRunListDS.YEAR, typeof(int));
            feeRunTable.Columns.Add(FeeRunListDS.MONTH, typeof(String));
            feeRunTable.Columns.Add(FeeRunListDS.SHORTNAME, typeof(String));
            feeRunTable.Columns.Add(FeeRunListDS.RUNTYPE, typeof(String));
            feeRunTable.Columns.Add(FeeRunListDS.RUNTYPEENUM, typeof(FeeRunType));
            feeRunTable.Columns.Add(FeeRunListDS.FEERUNDATE, typeof(DateTime));
            feeRunTable.Columns.Add(FeeRunListDS.TOTALFEES, typeof(decimal));

            foreach (DataRow row in feeRunTable.Rows)
            {
                Guid clid = new Guid(row["ENTITYCLID_FIELD"].ToString());
                Guid csid = new Guid(row["ENTITYCSID_FIELD"].ToString());
                IBrokerManagedComponent feeRumCM = this.Broker.GetCMImplementation(clid, csid) as IBrokerManagedComponent;
                if (feeRumCM != null)
                {
                    FeeRunDetailsDS feeRunDetailsDS = new FeeRunDetailsDS();
                    feeRumCM.GetData(feeRunDetailsDS);

                    if (feeRunDetailsDS.Tables[FeeRunDetailsDS.FEERUNSDETAILSTABLE].Rows.Count > 0)
                    {
                        DataRow feeDetailsRow = feeRunDetailsDS.Tables[FeeRunDetailsDS.FEERUNSDETAILSTABLE].Rows[0];
                        row[FeeRunDetailsDS.DESCRIPTION] = feeDetailsRow[FeeRunDetailsDS.DESCRIPTION];
                        row[FeeRunDetailsDS.YEAR] = feeDetailsRow[FeeRunDetailsDS.YEAR];
                        row[FeeRunDetailsDS.MONTH] = feeDetailsRow[FeeRunDetailsDS.MONTH];
                        row[FeeRunDetailsDS.SHORTNAME] = feeDetailsRow[FeeRunDetailsDS.SHORTNAME];
                        row[FeeRunDetailsDS.RUNTYPE] = feeDetailsRow[FeeRunDetailsDS.RUNTYPE];
                        row[FeeRunDetailsDS.RUNTYPEENUM] = feeDetailsRow[FeeRunDetailsDS.RUNTYPEENUM];
                        row[FeeRunDetailsDS.FEERUNDATE] = feeDetailsRow[FeeRunDetailsDS.FEERUNDATE];
                        row[FeeRunDetailsDS.TOTALFEES] = feeDetailsRow[FeeRunDetailsDS.TOTALFEES];
                    }

                    this.Broker.ReleaseBrokerManagedComponent(feeRumCM);
                }

                if (organisationListingDS.Tables.Contains(feeRunTable.TableName))
                    organisationListingDS.Tables.Remove(feeRunTable.TableName);

                organisationListingDS.Tables.Add(feeRunTable);

            }
        }

        private void ExtractByFeeRunsWithoutCM(OrganisationListingDS organisationListingDS, object[] args)
        {
            DataView feeRunsView = new DataView(this.Broker.GetBMCDetailList(OrganizationInstall.ASSEMBLY_NAME, args).Tables[TableName]);
            feeRunsView.RowFilter = organisationListingDS.GetRowFilterFeeRuns();
            feeRunsView.Sort = "ENTITYNAME_FIELD ASC";

            DataTable feeRunTable = feeRunsView.ToTable(FeeRunListDS.FEERUNSTABLE);

            if (organisationListingDS.Tables.Contains(feeRunTable.TableName))
                organisationListingDS.Tables.Remove(feeRunTable.TableName);

            organisationListingDS.Tables.Add(feeRunTable);

        }

        private void ExtractByBankAccount(OrganisationListingDS organisationListingDS, object[] args)
        {
            DataView bankView = new DataView(this.Broker.GetBMCDetailList(OrganizationInstall.ASSEMBLY_NAME, args).Tables[TableName]);
            bankView.RowFilter = OrganisationListingDS.GetRowFilterBankAccounts();
            bankView.Sort = "ENTITYNAME_FIELD ASC";
            DataTable bankViewTable = bankView.ToTable(BankDetailsDS.BANKDETAILSTABLELIST);

            DataView clientView = new DataView(this.Broker.GetBMCDetailList(OrganizationInstall.ASSEMBLY_NAME, args).Tables[TableName]);
            clientView.RowFilter = new OrganisationListingDS().GetRowFilter();
            clientView.Sort = "ENTITYNAME_FIELD ASC";
            DataTable clientViewTable = clientView.ToTable();

            bankViewTable.Columns.Add(BankDetailsDS.BSB, typeof(string));
            bankViewTable.Columns.Add(BankDetailsDS.ACCOUNTNUMBER, typeof(string));
            bankViewTable.Columns.Add(BankDetailsDS.ACCNAME, typeof(string));
            bankViewTable.Columns.Add(BankDetailsDS.ACCOUTTYPE, typeof(string));
            bankViewTable.Columns.Add(BankDetailsDS.CUSTNO, typeof(string));
            bankViewTable.Columns.Add(BankDetailsDS.HOLDING, typeof(decimal));
            bankViewTable.Columns.Add(BankDetailsDS.INSNAME, typeof(string));
            bankViewTable.Columns.Add(BankDetailsDS.INSID, typeof(Guid));
            bankViewTable.Columns.Add(BankDetailsDS.CLIENTID, typeof(string));
            bankViewTable.Columns.Add(BankDetailsDS.CLIENTCID, typeof(Guid));
            bankViewTable.Columns.Add(BankDetailsDS.CLIENTNAME, typeof(string));
            bankViewTable.Columns.Add(BankDetailsDS.TRANSACTION, typeof(decimal));
            bankViewTable.Columns.Add(BankDetailsDS.DIFF, typeof(decimal));

            bankViewTable.Columns.Add(BankDetailsDS.ISEXTERNALACCOUNT, typeof(bool));

            foreach (DataRow row in bankViewTable.Rows)
            {
                Guid ciid = new Guid(row["ENTITYCIID_FIELD"].ToString());
                IOrganizationUnit bankCM = this.Broker.GetBMCInstance(ciid) as IOrganizationUnit;

                BankDetailsDS bankDetailsDS = new BankDetailsDS();
                bankCM.GetData(bankDetailsDS);

                if (bankDetailsDS.Tables[BankDetailsDS.BANKDETAILSTABLE].Rows.Count > 0)
                {
                    DataRow bankDetailsRow = bankDetailsDS.Tables[BankDetailsDS.BANKDETAILSTABLE].Rows[0];
                    row[BankDetailsDS.BSB] = bankDetailsRow[BankDetailsDS.BSB];
                    row[BankDetailsDS.ACCOUNTNUMBER] = bankDetailsRow[BankDetailsDS.ACCOUNTNUMBER];
                    row[BankDetailsDS.ACCNAME] = bankDetailsRow[BankDetailsDS.ACCNAME];
                    row[BankDetailsDS.ACCOUTTYPE] = bankDetailsRow[BankDetailsDS.ACCOUTTYPE];
                    row[BankDetailsDS.CUSTNO] = bankDetailsRow[BankDetailsDS.CUSTNO];
                    row[BankDetailsDS.HOLDING] = bankDetailsRow[BankDetailsDS.HOLDING];
                    row[BankDetailsDS.TRANSACTION] = bankDetailsRow[BankDetailsDS.TRANSACTION];
                    row[BankDetailsDS.DIFF] = bankDetailsRow[BankDetailsDS.DIFF];

                    Guid parentID = (Guid)bankDetailsRow[BankDetailsDS.CLIENTCID];

                    if (parentID != Guid.Empty)
                    {
                        DataRow clientInstance = clientViewTable.Select().Where(clientRow => clientRow["ENTITYCIID_FIELD"].ToString() == parentID.ToString()).FirstOrDefault();

                        if (clientInstance != null)
                        {
                            row[BankDetailsDS.CLIENTCID] = clientInstance["ENTITYCIID_FIELD"];
                            row[BankDetailsDS.CLIENTNAME] = clientInstance["ENTITYNAME_FIELD"];
                            row[BankDetailsDS.CLIENTID] = clientInstance["ENTITYECLIPSEID_FIELD"];
                        }
                    }

                    string insName = string.Empty;
                    Guid insID = (Guid)bankDetailsRow[BankDetailsDS.INSID];
                    if (insID != null && insID != Guid.Empty)
                    {

                        var insObj = this.Institution.Where(ins => ins.ID == insID).FirstOrDefault();

                        if (insObj != null)
                            insName = insObj.Name;
                    }

                    row[BankDetailsDS.INSNAME] = insName;
                    row[BankDetailsDS.INSID] = (Guid)bankDetailsRow[BankDetailsDS.INSID];
                    row[BankDetailsDS.ISEXTERNALACCOUNT] = (bool)bankDetailsRow[BankDetailsDS.ISEXTERNALACCOUNT];
                }

                this.Broker.ReleaseBrokerManagedComponent(bankCM);
            }

            organisationListingDS.Tables.Add(bankViewTable);
        }

        private void ExtractByASXAccount(OrganisationListingDS organisationListingDS, object[] args)
        {
            DataView asxView = new DataView(this.Broker.GetBMCDetailList(OrganizationInstall.ASSEMBLY_NAME, args).Tables[TableName]);
            asxView.RowFilter = organisationListingDS.GetRowFilterASXAccounts();
            asxView.Sort = "ENTITYNAME_FIELD ASC";
            DataTable asxViewTable = asxView.ToTable(DesktopBrokerAccountDS.TABLENAME);
            asxViewTable.Columns.Add(DesktopBrokerAccountDS.ACCOUNTNAME, typeof(string));
            asxViewTable.Columns.Add(DesktopBrokerAccountDS.ACCOUNTNO, typeof(string));
            asxViewTable.Columns.Add(DesktopBrokerAccountDS.ACCOUNTDESIGNATION, typeof(string));
            asxViewTable.Columns.Add(DesktopBrokerAccountDS.HINNUMBER, typeof(string));
            asxViewTable.Columns.Add(DesktopBrokerAccountDS.STATUS, typeof(string));

            foreach (DataRow row in asxViewTable.Rows)
            {
                Guid clid = new Guid(row["ENTITYCLID_FIELD"].ToString());
                Guid csid = new Guid(row["ENTITYCSID_FIELD"].ToString());
                IBrokerManagedComponent asxCM = this.Broker.GetCMImplementation(clid, csid) as IBrokerManagedComponent;
                DesktopBrokerAccountDS desktopBrokerAccountDS = new DesktopBrokerAccountDS();
                asxCM.GetData(desktopBrokerAccountDS);

                if (desktopBrokerAccountDS.Tables[DesktopBrokerAccountDS.TABLENAME].Rows.Count > 0)
                {
                    DataRow asxDetailRow = desktopBrokerAccountDS.Tables[DesktopBrokerAccountDS.TABLENAME].Rows[0];
                    row[DesktopBrokerAccountDS.ACCOUNTNAME] = asxDetailRow[DesktopBrokerAccountDS.ACCOUNTNAME];
                    row[DesktopBrokerAccountDS.ACCOUNTNO] = asxDetailRow[DesktopBrokerAccountDS.ACCOUNTNO];
                    row[DesktopBrokerAccountDS.ACCOUNTDESIGNATION] = asxDetailRow[DesktopBrokerAccountDS.ACCOUNTDESIGNATION];
                    row[DesktopBrokerAccountDS.HINNUMBER] = asxDetailRow[DesktopBrokerAccountDS.HINNUMBER];
                    row[DesktopBrokerAccountDS.STATUS] = asxDetailRow[DesktopBrokerAccountDS.STATUS];

                }

                this.Broker.ReleaseBrokerManagedComponent(asxCM);
            }

            organisationListingDS.Tables.Add(asxViewTable);
        }

        public void AddRowToClientListingPresentationTable(Guid cid, string clientID, string name, string type, string status, DataTable ClientListingPresentationTable)
        {
            DataRow row = ClientListingPresentationTable.NewRow();

            row["CID"] = cid;
            row["ClientID"] = clientID;
            row["Name"] = name;
            row["Type"] = type;
            row["Status"] = status;

            ClientListingPresentationTable.Rows.Add(row);
        }

        public DataTable ClientListingPresentationTable()
        {
            DataTable clientListingPresentationTable = new DataTable("ClientListing");
            clientListingPresentationTable.Columns.Add("CID", typeof(Guid));
            clientListingPresentationTable.Columns.Add("ClientID", typeof(string));
            clientListingPresentationTable.Columns.Add("Name", typeof(string));
            clientListingPresentationTable.Columns.Add("Type", typeof(string));
            clientListingPresentationTable.Columns.Add("Status", typeof(string));

            return clientListingPresentationTable;
        }

        public void GetStatus(StatusDS statusDs)
        {
            SetDefaultValues("Organization");
            SetDefaultValues("Client");
            SetDefaultValues("Individual");
            SetDefaultValues("Bank");
            SetDefaultValues("DesktopBrokerAccount");
            SetDefaultValues("TermDepositAccount");
            SetDefaultValues("ManagedInvestmentSchemesAccount");

            DataTable dt = _StatusList.Where(s => s.Type.ToLower() == statusDs.TypeFilter.ToLower()).ToDataTable();
            dt.TableName = StatusDS.TABLENAME;
            statusDs.Tables.Add(dt);
        }

        private void SetDefaultValues(string statusType)
        {
            CheckForStatus("Pending", statusType);
            CheckForStatus("Active", statusType);
            CheckForStatus("Deactivated", statusType);
        }

        private void CheckForStatus(string statusName, string statusType)
        {
            var isStatusExist = StatusList.SingleOrDefault(e => e.Type.ToLower() == statusType.ToLower() && e.Name.ToLower() == statusName.ToLower());
            if (isStatusExist == null)
                StatusList.Add(new Status { Name = statusName, Type = statusType });
        }

        public static void FillInstitutionContactsDataset(InstitutionContactsDS ds, ObservableCollection<IndividualEntityCsidClidDetail> Contacts, ICMBroker Broker)
        {
            if (Contacts != null && Contacts.Count > 0)
            {
                foreach (var Contact in Contacts)
                {
                    var componenet = Broker.GetCMImplementation(Contact.Clid, Contact.Csid);
                    if (componenet != null)
                    {
                        componenet.GetData(ds);
                        Broker.ReleaseBrokerManagedComponent(componenet);
                    }
                }
            }

        }

        /// <summary>
        /// Setting Default Values for Cash Balances
        /// </summary>
        private void FillCashBalancesDefaultValues()
        {
            if (_CashBalances == null)
            {
                _CashBalances = new List<CashBalanceEntity>();
            }
            else if (_CashBalances.Count == 0)
            {
                var entity = new CashBalanceEntity
                    {
                        ID = Guid.NewGuid(),
                        SuperAccountType = SuperAccountType.Super,
                        Percentage = (decimal)0.03,
                        MinValue = 500,
                        MaxValue = 10000
                    };
                _CashBalances.Add(entity);
                entity = new CashBalanceEntity
                    {
                        ID = Guid.NewGuid(),
                        SuperAccountType = SuperAccountType.Pension,
                        Percentage = (decimal)0.05,
                        MinValue = 2000,
                        MaxValue = 10000
                    };
                _CashBalances.Add(entity);
            }
        }
    }
}
