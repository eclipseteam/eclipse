﻿using System.Collections.Generic;
using System.Text;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.CalculationInterface;
using System.Xml.Linq;
using Oritax.TaxSimp.Common;

using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Enum;
using Oritax.TaxSimp.Extensions;
using System.Linq;
using EPI.Data;
using System;

namespace Oritax.TaxSimp.CM.Organization
{
    public class SetClientBankAccountsUserCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string value)
        {
            StringBuilder output = new StringBuilder();
            output.Append("Client_Name,CLient_ID,Account_Id,Account_Number,Account_Name,Associated_User_Name,Is_Admin,User_CID" + Environment.NewLine);

            


            OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
                                                                              {
                                                                                  if ((bmc as OrganizationUnitCM).IsInvestableClient)
                                                                                  {



                                                                                      var data = bmc.GetDataStream((int)CmCommand.GetEntitySecurity, null);
                                                                                      var entity =data.ToData<EntitySecurity>();

                                                                                      if (entity != null && entity.IncludedUsers != null && entity.IncludedUsers.Count > 0)
                                                                                      {
                                                                                          var clientData = bmc.GetDataStream(-999, null);
                                                                                          var individualEntity = clientData.ToData<ClientIndividualEntity>();
                                                                                          var clientcop = clientData.ToData<CorporateEntity>();
                                                                                          List<Common.IdentityCMDetail> accounts;
                                                                                          string clientName, clientid;
                                                                                         
                                                                                          if(individualEntity!=null)
                                                                                          {
                                                                                              accounts =individualEntity.BankAccounts;
                                                                                              clientName = individualEntity.Name;
                                                                                              clientid = individualEntity.ClientId;
                                                                                          }else
                                                                                          {
                                                                                              accounts = clientcop.BankAccounts;
                                                                                              clientName = clientcop.Name;
                                                                                              clientid = clientcop.ClientId;
                                                                                          }
                                                                                          if (accounts != null && accounts.Count > 0)
                                                                                          foreach (var user in entity.IncludedUsers)
                                                                                          {
                                                                                              foreach(var acc in accounts)
                                                                                              {
                                                                                                  var comp = Organization.Broker.GetCMImplementation(acc.Clid, acc.Csid);
                                                                                                  if (comp != null)
                                                                                                  {
                                                                                                     
                                                                                                      ISecurityService security = comp as ISecurityService;
                                                                                                      if (security != null)
                                                                                                      {
                                                                                                          security.AddUser(user);
                                                                                                          var bankentity=comp.GetDataStream(-999, null).ToData<Common.BankAccountEntity>();
                                                                                                          output.Append(string.Format("{0},{1},{2},{3},{4},{5},{6},{7}{8}", clientName, clientid, (comp as OrganizationUnitCM).ClientId, bankentity.AccountNumber, bankentity.Name, user.CurrentUserName.Replace(",",""), user.IsAdmin, user.CID, Environment.NewLine));
                                                                                                      }
                                                                                                  }

                                                                                              }
                                                                                          }
                                                                                      }

                                                                                  }


                                                                              });

            return output.ToString();
        }
    }
}
