﻿using System;
using System.Data;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.CM.Organization
{

    public class NoneCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string value)
        {
            return "None";
        }
    }

    public static class EnableSecurity
    {
        public static readonly int SecuritySetting = 1;
    }

    public static class OrganizationListAction
    {
        private static readonly string DbUserAssemblyName = "DBUser_1_1";
        private static readonly string TableName = "Entities_Table";
        private static readonly string ColumnClid = "EntityClid_Field";
        private static readonly string ColumnCsid = "EntityCsid_Field";
        private static readonly string ColumnCiid = "EntityCiid_Field";
        private static readonly Guid Consolidation = new Guid("5A5BC2D6-50CB-412A-B151-BF8E16E17403");

        public static bool IsValidClientType(string typename)
        {
            switch (typename)
            {
                case "ClientSMSFCorporateTrustee":
                case "ClientCorporationPrivate":
                case "ClientCorporationPublic":
                case "ClientEClipseSuper":
                case "ClientIndividual":
                case "ClientOtherTrustsCorporate":
                case "ClientOtherTrustsIndividual":
                case "ClientSMSFIndividualTrustee":
                case "ClientSoleTrader":
                case "DesktopBrokerAccount":
                case "BankAccount":
                case "Individual":
                case "ManagedInvestmentSchemesAccount":
                case "Advisor":
                case "IFA":
                case "DealerGroup":
                case "TermDepositAccount":
                case "FeeRuns":
                case "Orders":
                case "SettledUnsettled":
                case "OBP":
                case "NonIndividual":
                    return true;
                default:
                    return false;
            }
        }

        public static bool IsValidClientAccountType(string typename)
        {
            switch (typename)
            {
                case "ClientSMSFCorporateTrustee":
                case "ClientCorporationPrivate":
                case "ClientCorporationPublic":
                case "ClientEClipseSuper":
                case "ClientIndividual":
                case "ClientOtherTrustsCorporate":
                case "ClientOtherTrustsIndividual":
                case "ClientSMSFIndividualTrustee":
                case "ClientSoleTrader":
                    return true;
                default:
                    return false;
            }
        }

        public static string GetRowFilter(OrganizationType type)
        {
            Guid id = OrganizationTypeList.GetEntity(type);
            string filter = string.Empty;
            filter = string.Format("ENTITYCTID_FIELD ='{0}'", id.ToString());
            return filter;
        }

        public static string GetClientFilter()
        {

            string filter =
                GetRowFilter(OrganizationType.ClientCorporationPrivate) + " OR "
                + GetRowFilter(OrganizationType.ClientCorporationPublic) + " OR "
                + GetRowFilter(OrganizationType.ClientIndividual) + " OR "
                + GetRowFilter(OrganizationType.ClientOtherTrustsCorporate) + " OR "
                + GetRowFilter(OrganizationType.ClientOtherTrustsIndividual) + " OR "
                + GetRowFilter(OrganizationType.ClientSMSFCorporateTrustee) + " OR "
                + GetRowFilter(OrganizationType.ClientSMSFIndividualTrustee) + " OR "
                + GetRowFilter(OrganizationType.ClientSoleTrader) + " OR "
                + GetRowFilter(OrganizationType.Corporate) + " OR "
                + GetRowFilter(OrganizationType.ConsoleClient) + " OR "
                + GetRowFilter(OrganizationType.ClientEClipseSuper);
            return filter;
        }





        private static string GetNameFilter(string name,bool appendAnd=true)
        {
            string filter = "";
            if (!string.IsNullOrEmpty(name))
            {
                filter = string.Format("{0}ENTITYNAME_FIELD LIKE '%{1}%'", (appendAnd ? " and " : "") , name);
            }
            return filter;
        }
        private static string GetCIDFilter(string cid)
        {
            string filter = "";
            if (!string.IsNullOrEmpty(cid))
            {
                filter = string.Format(" and ENTITYCIID_FIELD = '{0}'", cid);
            }
            return filter;
        }
        
        public static void ForEach(string userName, OrganizationType type, OrganizationCM organization, Action<Guid, ILogicalModule, IBrokerManagedComponent> action, string name = "", string cid = "")
        {
            IBrokerManagedComponent user = (IBrokerManagedComponent)organization.Broker.GetBMCInstance(userName, DbUserAssemblyName);
            object[] args = new Object[3] { organization.CLID, user.CID, EnableSecurity.SecuritySetting };
            DataView view = new DataView(organization.Broker.GetBMCDetailList(OrganizationInstall.ASSEMBLY_NAME, args).Tables[TableName]);
            view.RowFilter = GetRowFilter(type) + GetNameFilter(name) + GetCIDFilter(cid);
            view.Sort = "ENTITYNAME_FIELD ASC";
            ProcessDataview(organization, action, user, view);
        }

        public static DataTable GetBMCList(string userName, OrganizationType type, OrganizationCM organization,string name = "", string cid = "")
        {
            IBrokerManagedComponent user = (IBrokerManagedComponent)organization.Broker.GetBMCInstance(userName, DbUserAssemblyName);
            object[] args = new Object[3] { organization.CLID, user.CID, EnableSecurity.SecuritySetting };
            DataView view = new DataView(organization.Broker.GetBMCDetailList(OrganizationInstall.ASSEMBLY_NAME, args).Tables[TableName]);
            view.RowFilter = GetRowFilter(type) + GetNameFilter(name) + GetCIDFilter(cid);
            view.Sort = "ENTITYNAME_FIELD ASC";

            return view.ToTable();
        }

        public static int GetUnitsCount(string userName, OrganizationType type, OrganizationCM organization, string name = "", string cid = "")
        {
            IBrokerManagedComponent user =(IBrokerManagedComponent) organization.Broker.GetBMCInstance(userName, DbUserAssemblyName);
            object[] args = new Object[3] {organization.CLID, user.CID, EnableSecurity.SecuritySetting};
            DataView view =new DataView(organization.Broker.GetBMCDetailList(OrganizationInstall.ASSEMBLY_NAME, args).Tables[TableName]);
            view.RowFilter = GetRowFilter(type) + GetNameFilter(name) + GetCIDFilter(cid);
            view.Sort = "ENTITYNAME_FIELD ASC";
            return view.Count;
        }


        public static void ForEach(string userName, OrganizationCM organization, Action<Guid, ILogicalModule, IBrokerManagedComponent> action, string name = "")
        {
            IBrokerManagedComponent user = (IBrokerManagedComponent)organization.Broker.GetBMCInstance(userName, DbUserAssemblyName);
            object[] args = new Object[3] { organization.CLID, user.CID, EnableSecurity.SecuritySetting };
            DataView view = new DataView(organization.Broker.GetBMCDetailList(OrganizationInstall.ASSEMBLY_NAME, args).Tables[TableName]);
            view.RowFilter = GetNameFilter(name,false);
            ProcessDataview(organization, action, user, view);
        }
        public static void ForEachClientOnly(string userName, OrganizationCM organization, Action<Guid, ILogicalModule, IBrokerManagedComponent> action)
        {
            IBrokerManagedComponent user = (IBrokerManagedComponent)organization.Broker.GetBMCInstance(userName, DbUserAssemblyName);
            object[] args = new Object[3] { organization.CLID, user.CID, EnableSecurity.SecuritySetting };
            DataView view = new DataView(organization.Broker.GetBMCDetailList(OrganizationInstall.ASSEMBLY_NAME, args).Tables[TableName]);
            view.RowFilter = GetClientFilter();
            view.Sort = "ENTITYNAME_FIELD ASC";
            ProcessDataview(organization, action, user, view);
        }
        private static void ProcessDataview(OrganizationCM organization, Action<Guid, ILogicalModule, IBrokerManagedComponent> action, IBrokerManagedComponent user, DataView view)
        {
            foreach (DataRowView each in view)
            {
                Guid clid = new Guid(each[ColumnClid].ToString());
                Guid csid = new Guid(each[ColumnCsid].ToString());
                Guid ciid = new Guid(each[ColumnCiid].ToString());
                ILogicalModule logical = organization.Broker.GetLogicalCM(clid);
                if (logical != null && logical.CMType != Consolidation && IsValidClientType(logical.CMTypeName))
                {
                    IBrokerManagedComponent bmc =
                        organization.Broker.GetBMCInstance(ciid) as IBrokerManagedComponent;
                    action(csid, logical, bmc);
                    organization.Broker.ReleaseBrokerManagedComponent(bmc);
                }
                organization.Broker.ReleaseBrokerManagedComponent(logical);
            }
            organization.Broker.ReleaseBrokerManagedComponent(user);
        }

        public static string IsShownToAllUsers(OrganizationType type, string userName)
        {  
            //orderCm and unsettledCm should be shown to all users.
            string result = userName;
            if (type == OrganizationType.Order || type == OrganizationType.SettledUnsettled)
            {
                result = "Administrator";
            }
            return result;
        }
    }
}
