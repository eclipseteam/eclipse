using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.DSCalculation;
using Oritax.TaxSimp.CommonPersistence;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.CM.Organization
{
	/// <summary>
	/// Summary description for OrganisationPersister.
	/// </summary>
	public class OrganizationPersister : BlobPersister, IDisposable
	{
		internal const string ORGANIZATION_GET_ENTITY_LIST="ORGANIZATION_GET_ENTITY_LIST_1_3_0";
		private bool disposed = false;

		public OrganizationPersister(SqlConnection connection, SqlTransaction transaction) : base(connection, transaction)
		{
		}

		#region IDisposable Implementation ---------------------------------------

		/// <summary>
		///		Performs application-defined tasks associated with freeing, releasing, 
		///		or resetting unmanaged resources.
		/// </summary>
		public new void Dispose() 
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		/// <summary>
		///		Performs application-defined tasks associated with freeing, releasing, 
		///		or resetting unmanaged resources.
		/// </summary>
		/// <param name="disposing">
		///		True if called by the Dispose() method, false if called by the finalizer.
		/// </param>
		protected new void Dispose(bool disposing) 
		{
			if (!disposed)
			{
				if (disposing)
				{
					base.Dispose();
				}
				disposed = true;
			}
		}

		#endregion // IDisposable Implementation


		public override DataSet FindDetailsByTypeId(Guid typeId, object[] parameters)
		{
			EntityListDS ds=new EntityListDS();
			SqlDataAdapter sda=new SqlDataAdapter(ORGANIZATION_GET_ENTITY_LIST, this.Connection);
			sda.SelectCommand.Transaction = this.Transaction;
			sda.SelectCommand.CommandType=CommandType.StoredProcedure;
            sda.SelectCommand.Parameters.AddWithValue("@PARENTCLID", parameters[0].ToString());
            sda.SelectCommand.Parameters.AddWithValue("@USERID", parameters[1].ToString());
            sda.SelectCommand.Parameters.AddWithValue("@OVERRIDE", Convert.ToInt16(parameters[2]));
			sda.Fill(ds, EntityListDS.ENTITIES_TABLE);
			return ds;
		}

		public override void EstablishDatabase()
		{
			base.EstablishDatabase();

			SqlCommand adminCommand = DBCommandFactory.Instance.NewSqlCommand();
            adminCommand.Connection = this.Connection;
			adminCommand.Transaction = this.Transaction;

			adminCommand.CommandText="if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ORGANIZATION_GET_ENTITY_LIST_1_3_0]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) " +
				"drop procedure [dbo].[ORGANIZATION_GET_ENTITY_LIST_1_3_0] ";
			adminCommand.ExecuteNonQuery();

            string selectStr = "SELECT CINSTANCES.NAME AS ENTITYNAME_FIELD, CMLOGICAL.CLID AS ENTITYCLID_FIELD, CINSTANCES.OTHERID AS OTHERID_FIELD, CINSTANCES.STATUS AS STATUS_FIELD, CINSTANCES.ECLIPSECLIENTID AS ENTITYECLIPSEID_FIELD," +
                "CMSCENARIO.CSID AS ENTITYCSID_FIELD, CINSTANCES.CID AS ENTITYCIID_FIELD,		" +
                "CMLOGICAL.CTID AS ENTITYCTID_FIELD,		" +
                "SCENARIO.TYPE AS ENTITYSCTYPE_FIELD,		" +
                "0 AS ENTITYSORT_FIELD ,		" +
                "SCENARIO.STATUS AS ENTITYSCSTATUS_FIELD " +
                "FROM	CMLOGICAL INNER JOIN CMSCENARIO ON CMLOGICAL.CLID = CMSCENARIO.CLID AND 		" +
                "CMLOGICAL.CURRCSID = CMSCENARIO.CSID INNER JOIN " + BrokerManagedComponentDS.CINSTANCES_TABLE + " CINSTANCES ON CMSCENARIO.CIID = CINSTANCES.CID " +
                "INNER JOIN SCENARIO ON CMSCENARIO.CSID = SCENARIO.CSID ";

			adminCommand.CommandText=

				"CREATE PROCEDURE [dbo].[ORGANIZATION_GET_ENTITY_LIST_1_3_0] " +
				"@PARENTCLID UNIQUEIDENTIFIER, " +
				"@USERID UNIQUEIDENTIFIER, " +
				"@OVERRIDE INT " +
				"AS " +
				"DECLARE @ADMINID varchar(50) " +
				"IF( @USERID IN (SELECT CID FROM USERDETAIL WHERE USERDETAIL.ADMINISTRATOR = 1 OR USERDETAIL.USERNAME = 'Administrator') OR @OVERRIDE = 0) " +
				"BEGIN " +
                selectStr +
                "WHERE (CMLOGICAL.PLID = @PARENTCLID ) " +
				"END " +
				"ELSE " +
				"BEGIN " +
                selectStr +
				"WHERE CMLOGICAL.CLID IN (select CMLOGICAL.CLID from CMLOGICAL INNER JOIN " +BrokerManagedComponentDS.ACL_TABLE +" ACLENTRY ON ACLENTRY.BMCCID = CMLOGICAL.CLID " +
				"WHERE ACLENTRY.PARTYCID = @USERID AND CMLOGICAL.PLID  = @PARENTCLID)" +
				"END "; 

			adminCommand.ExecuteNonQuery();
		}
	}
}

