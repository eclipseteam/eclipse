﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oritax.TaxSimp.CM.Organization.HelperClass
{
    public enum AccountType
    {
        Individual,
        Joint,
        Company,
        Minor,
        Partnership,
        Other,
        SMSF,
        Trust,
        Default
    }

    public enum TrusteeType
    {
        Individual,
        Joint,
        Company,
        Default
    }

    public enum HonorificType
    {
        Mr,
        Mrs,
        Ms,
        Miss,
        Other,
        Default
    }

    public enum CorporateTrusteeTitleType
    {
        Director,
        Secretary,
        Other,
        Default
    }

    public enum InvestorType
    {
        Wholesale,
        Sophisticated,
        Professional,
        Retail,
        Default
    }

    public enum ServiceType
    {
        DoItYourSelf,
        DoItWithMe,
        DoItForMe,
        Default
    }

    public enum InvestmentProgram
    {
        InvestmentProgram1,
        InvestmentProgram2,
        InvestmentProgram3,
        Default
    }

    public enum HandlingOfUnmanagedInvestments
    {
        Sell,
        Retain,
        SellIfSellRated,
        Default
    }

    public enum HandlingType
    {
        AssignAllocatedValueToCash,
        DistributeAllocatedValueToOtherInvestments,
        Default
    }

    public enum ExclusionType
    {
        DontSell,
        DontBuy,
        DontBuyOrSell,
        Default
    }

    public enum OperateOption
    {
        AnyOneOfUs,
        AnyTwoOfUs,
        AllOfUs,
        Default
    }

    public enum TransferToService
    {
        DIY,
        DIWM,
        Default
    }

    public enum FormStatus
    {
        SaveAndExit,
        SubmitAndPrint,
        Default
    }

}