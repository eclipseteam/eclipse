﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oritax.TaxSimp.CM.Organization.HelperClass.Interface;

namespace Oritax.TaxSimp.CM.Organization.HelperClass.Controller
{
    [Serializable]
    public class UMAController
    {
        private IUMAForm iController = null;
        public UMAController(IUMAForm iForm)
        {
            iController = iForm;
        }
        public void Save()
        {
            iController.SaveUMAForm();
        }
    }
}