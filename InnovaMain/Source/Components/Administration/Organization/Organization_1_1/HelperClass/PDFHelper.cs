﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using iTextSharp.text;
//using iTextSharp.text.pdf;
//using System.IO;
//using System.Text;
//using Oritax.TaxSimp.CM.Organization.Model.Organization;
//using Oritax.TaxSimp.CM.Organization.Model.Entities;
//using Oritax.TaxSimp.CM.Organization.Model;
//namespace Oritax.TaxSimp.CM.Organization.HelperClass
//{
//    [Serializable]
//    public class PDFHelper
//    {
//        #region VARIABLES
//        public FormOrganization formOrganization { get; set; }
//        private FirstFormEntity Form1;
//        private SecondFormEntity Form2;
//        private ThirdFormEntity Form3;
//        private FourthFormEntity Form4;
//        private FifthFormEntity Form5;
//        private SixthFormEntity Form6;
//        private SeventhFormEntity Form7;
//        private EighthFormEntity Form8;
//        private NinethFormEntity Form9;
//        private TenthFormEntity Form10;

//        private string errMsg = string.Empty;
//        private string filePath = @"c:\UMAForm.pdf";//"~/PDF/UMAForm.pdf";

//        private const float TABLE_WIDTH = 100f;
//        private const float ROW_DETAIL_HEIGHT = 16f;
//        private const float SEPARATOR_HEIGHT = 10f;

//        private const string FONT = "Arial";
//        private const string IMG_UNCHECK_CHKBOX = "~/images/UmaForm/check-box-icon.png";
//        private const string IMG_CHECK_CHKBOX = "~/images/UmaForm/check-box-icon.png";
//        private const string IMG_UNCHECK_RADIO = "~/images/UmaForm/ui-radio-button-uncheck-icon.png";
//        private const string IMG_CHECK_RADIO = "~/images/UmaForm/ui-radio-button-icon.png";

//        private iTextSharp.text.Font TABLE_FONT_HEADER = iTextSharp.text.FontFactory.GetFont(FONT, 7f, iTextSharp.text.Font.BOLD, iTextSharp.text.BaseColor.WHITE);
//        private iTextSharp.text.Font TABLE_FONT_IDENTIFIER = iTextSharp.text.FontFactory.GetFont(FONT, 7f, iTextSharp.text.Font.BOLD, COLOR_DARK_GRAY);
//        private iTextSharp.text.Font TABLE_FONT_DETAILS = iTextSharp.text.FontFactory.GetFont(FONT, 6f, iTextSharp.text.Font.NORMAL, COLOR_DARK_GRAY);
//        private iTextSharp.text.Font TABLE_FONT_DETAILS_UNDERLINE = iTextSharp.text.FontFactory.GetFont(FONT, 6f, iTextSharp.text.Font.UNDERLINE, COLOR_DARK_GRAY);
//        private iTextSharp.text.Font TABLE_FONT_DETAILS_ITALIC = iTextSharp.text.FontFactory.GetFont(FONT, 6f, iTextSharp.text.Font.ITALIC, COLOR_DARK_GRAY);

//        private static readonly iTextSharp.text.BaseColor TABLE_HEADER_BGCOLOR = new BaseColor(61, 59, 106);
//        private static readonly iTextSharp.text.BaseColor TABLE_DETAIL_BGCOLOR = new BaseColor(255, 255, 255);
//        private static readonly iTextSharp.text.BaseColor TABLE_DETAIL_BORDER_COLOR = iTextSharp.text.BaseColor.WHITE;

//        private static readonly iTextSharp.text.BaseColor TEXTBOX_BGCOLOR = new BaseColor(255, 255, 255);
//        private static readonly iTextSharp.text.BaseColor TEXTBOX_BORDERCOLOR = new BaseColor(204, 204, 204);

//        private static readonly iTextSharp.text.BaseColor COLOR_DARK_GRAY = new BaseColor(112, 112, 112);
//        #endregion VARIABLES

//        #region CONSTRUCTOR
//        public PDFHelper(FormOrganization formOrganization)
//        {
//            this.formOrganization = formOrganization;
//            Form1 = this.formOrganization.firstFormEntity;
//            Form2 = this.formOrganization.secondFormEntity;
//            Form3 = this.formOrganization.thirdFormEntity;
//            Form4 = this.formOrganization.fourthFormEntity;
//            Form5 = this.formOrganization.fifthFormEntity;
//            Form6 = this.formOrganization.sixthFormEntity;
//            Form7 = this.formOrganization.seventhFormEntity;
//            Form8 = this.formOrganization.eighthFormEntity;
//            Form9 = this.formOrganization.ninethFormEntity;
//            Form10 = this.formOrganization.tenthFormEntity;

//            filePath = @"c:\UMAForm_" + this.formOrganization.appFormEntity.ClientName + ".pdf";
//        }
//        #endregion CONSTRUCTOR

//        #region PAGE 1 : CLIENT DETAILS
//        public PdfPTable Page_One_Client_Details()
//        {
//            float[] widths = new float[] { 2f, 3f, 13f, 27f, 2f, 3f, 18f, 2f, 3f, 18f }; /*Array should be equal to no. of Columns*/

//            /*Top, Right, Bottom, Left*/
//            float[] header_padding = new float[] { 1f, 0f, 1f, 3f };
//            float[] subheader_padding = new float[] { 4f, 0f, 4f, 3f };
//            float[] detail_padding = new float[] { 5f, 2f, 1f, 2f };
//            float[] chk_padding = new float[] { 2f, 2f, 2f, 2f };

//            int max_column = widths.Length;

//            PdfPTable tbl = CreateTable(max_column, widths);

//            tbl.AddCell(AddCell("CLIENT DETAILS", max_column, 12f, TABLE_FONT_HEADER, Element.ALIGN_LEFT, header_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_HEADER_BGCOLOR));
//            tbl.AddCell(AddCell("Type of Account:", max_column, 15f, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, subheader_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 1*/
//            tbl.AddCell(AddCell("1:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Form1.AccountType == "IND") ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Individual", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            tbl.AddCell(AddCell("7:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Form1.AccountType == "SMS") ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Self Managed Super Fund (SMSF) ", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            tbl.AddCell(AddCell("8:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Form1.AccountType == "TRS") ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Trust", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 2*/
//            tbl.AddCell(AddCell("2:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Form1.AccountType == "JNT") ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Joint", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Please specify the type of trustee:", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Please specify the type of trustee:", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 3*/
//            tbl.AddCell(AddCell("3:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Form1.AccountType == "CMP") ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Company", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Form1.SMSFType == "IND") ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Individual ", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Form1.TrusteeType == "IND") ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Individual ", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 4*/
//            tbl.AddCell(AddCell("4:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Form1.AccountType == "MNR") ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Minor", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Form1.SMSFType == "JNT") ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Joint ", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Form1.TrusteeType == "JNT") ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Joint ", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 5*/
//            tbl.AddCell(AddCell("5:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Form1.AccountType == "PTR") ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Partnership", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Form1.SMSFType == "CMP") ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Company ", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Form1.TrusteeType == "CMP") ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Company ", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 6*/
//            tbl.AddCell(AddCell("6:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Form1.AccountType == "OTH") ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Other - please provide details", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form1.OthersDetails, 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

//            tbl.AddCell(AddCell("", 6, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 7*/
//            tbl.AddCell(AddCell("Account Name:", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form1.AccountName, 4, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 8*/
//            tbl.AddCell(AddCell("Trustee Name/s:", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form1.TrusteeName, 4, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            return tbl;
//        }
//        #endregion PAGE 1 : CLIENT DETAILS

//        #region PAGE 1 : ADVISER
//        public PdfPTable Page_One_Adviser()
//        {
//            float[] widths = new float[] { 13f, 5f, 16f, 3f, 8f, 8f, 2f, 15f, 0.5f, 3f, 16f }; /*Array should be equal to no. of Columns*/

//            /*Top, Right, Bottom, Left*/
//            float[] header_padding = new float[] { 1f, 0f, 1f, 3f };
//            float[] subheader_padding = new float[] { 4f, 0f, 4f, 3f };
//            float[] detail_padding = new float[] { 5f, 2f, 1f, 5f };

//            int max_column = widths.Length;

//            PdfPTable tbl = CreateTable(max_column, widths);

//            tbl.AddCell(AddCell("ADVISER", max_column, 12f, TABLE_FONT_HEADER, Element.ALIGN_LEFT, header_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_HEADER_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 1*/
//            tbl.AddCell(AddCell("Unique ID:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form1.UniqueID, 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("Business Name:", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form1.BusinessName, 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("(if applicable)", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 2*/
//            tbl.AddCell(AddCell("Name:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form1.AdviserName, 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("", 8, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 3*/
//            tbl.AddCell(AddCell("Upfront fee:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form1.UpfrontFee.ToString(), 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("inc GST", 8, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 4*/
//            tbl.AddCell(AddCell("Ongoing fee:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form1.OngoingFeeAmount.ToString(), 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("p.a. inc GST   Or", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form1.OngoingFeePercent.ToString(), 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("p.a. inc GST", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 5*/
//            tbl.AddCell(AddCell("Or:", max_column, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 6*/
//            tbl.AddCell(AddCell("Tiered fee:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("From", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form1.TieredFeeFrom1.ToString(), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("To", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form1.TieredFeeTo1.ToString(), 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form1.TieredFeePA1.ToString(), 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("p.a. inc GST", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 7*/
//            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("From", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form1.TieredFeeFrom2.ToString(), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("To", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form1.TieredFeeTo2.ToString(), 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form1.TieredFeePA2.ToString(), 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("p.a. inc GST", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 8*/
//            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("From", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form1.TieredFeeFrom3.ToString(), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("To", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form1.TieredFeeTo3.ToString(), 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form1.TieredFeePA3.ToString(), 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("p.a. inc GST", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            return tbl;
//        }
//        #endregion PAGE 1 : ADVISERS

//        #region PAGE 2 : CLIENT TAX FILE NUMBER (Part 1)
//        public PdfPTable Page_Two_Client_Tax_File_Number_I()
//        {
//            float[] widths = new float[] { 8f, 1f, 0.5f, 1f, 0.5f, 1f, 0.5f, 1f, 0.5f, 1f, 0.5f, 1f, 0.5f, 1f, 0.5f, 1f, 0.5f, 1f, 0.5f, 1f, 0.5f, 1f, 0.5f }; /*Array should be equal to no. of Columns*/

//            /*Top, Right, Bottom, Left*/
//            float[] header_padding = new float[] { 1f, 0f, 1f, 3f };
//            float[] subheader_padding = new float[] { 4f, 0f, 4f, 3f };
//            float[] detail_padding = new float[] { 5f, 2f, 1f, 5f };
//            float[] highpen_padding = new float[] { 4.5f, 5f, 2f, 3f };

//            int max_column = widths.Length;

//            PdfPTable tbl = CreateTable(max_column, widths, 10f);

//            tbl.AddCell(AddCell("CLIENT TAX FILE NMBER/ABN", max_column, 12f, TABLE_FONT_HEADER, Element.ALIGN_LEFT, header_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_HEADER_BGCOLOR));
//            tbl.AddCell(AddCell("The TFN, ABN or ARBN must be that if the Trust / Superannuation fund / Deceased Estate", max_column, 15f, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, subheader_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 1*/
//            tbl.AddCell(AddCell("Client 1 TFN:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            for (int i = 0; i < Form2.Client1TFN.Length; i++)
//            {
//                switch (i)
//                {
//                    case 0:
//                        tbl.AddCell(AddCell(Form2.Client1TFN.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//                        break;
//                    case 1:
//                        tbl.AddCell(AddCell(Form2.Client1TFN.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//                        break;
//                    case 2:
//                        tbl.AddCell(AddCell(Form2.Client1TFN.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//                        tbl.AddCell(AddCell("-", 1, SEPARATOR_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, highpen_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//                        break;
//                    case 3:
//                        tbl.AddCell(AddCell(Form2.Client1TFN.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//                        break;
//                    case 4:
//                        tbl.AddCell(AddCell(Form2.Client1TFN.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//                        break;
//                    case 5:
//                        tbl.AddCell(AddCell(Form2.Client1TFN.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//                        tbl.AddCell(AddCell("-", 1, SEPARATOR_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, highpen_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//                        break;
//                    case 6:
//                        tbl.AddCell(AddCell(Form2.Client1TFN.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));         
//                        break;
//                    case 7:
//                        tbl.AddCell(AddCell(Form2.Client1TFN.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//                        break;
//                    case 8:
//                        tbl.AddCell(AddCell(Form2.Client1TFN.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//                        tbl.AddCell(AddCell("", 5, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//                        break;
//                }
//            }
            
//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 2*/
            
//            tbl.AddCell(AddCell("Client 2 TFN:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            for (int i = 0; i < Form2.Client2TFN.Length; i++)
//            {
//                switch (i)
//                {
//                    case 0:
//                        tbl.AddCell(AddCell(Form2.Client1TFN.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//                        break;
//                    case 1:
//                        tbl.AddCell(AddCell(Form2.Client1TFN.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//                        break;
//                    case 2:
//                        tbl.AddCell(AddCell(Form2.Client1TFN.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//                        tbl.AddCell(AddCell("-", 1, SEPARATOR_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, highpen_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//                        break;
//                    case 3:
//                        tbl.AddCell(AddCell(Form2.Client1TFN.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//                        break;
//                    case 4:
//                        tbl.AddCell(AddCell(Form2.Client1TFN.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//                        break;
//                    case 5:
//                        tbl.AddCell(AddCell(Form2.Client1TFN.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//                        tbl.AddCell(AddCell("-", 1, SEPARATOR_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, highpen_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//                        break;
//                    case 6:
//                        tbl.AddCell(AddCell(Form2.Client1TFN.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//                        break;
//                    case 7:
//                        tbl.AddCell(AddCell(Form2.Client1TFN.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//                        break;
//                    case 8:
//                        tbl.AddCell(AddCell(Form2.Client1TFN.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//                        tbl.AddCell(AddCell("", 5, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//                        break;
//                }
//            }

//            return tbl;
//        }
//        #endregion PAGE 2 : CLIENT TAX FILE NUMBER (Part 1)

//        #region PAGE 2 : CLIENT TAX FILE NUMBER (Part 2)
//        public PdfPTable Page_Two_Client_Tax_File_Number_II()
//        {
//            float[] widths = new float[] { 8f, 1f, 0.5f, 1f, 0.5f, 1f, 0.5f, 1f, 0.5f, 1f, 0.5f, 1f, 0.5f, 1f, 0.5f, 1f, 0.5f, 1f, 0.5f, 1f, 0.5f, 1f, 0.5f }; /*Array should be equal to no. of Columns*/

//            /*Top, Right, Bottom, Left*/
//            float[] header_padding = new float[] { 1f, 0f, 1f, 3f };
//            float[] subheader_padding = new float[] { 4f, 0f, 4f, 3f };
//            float[] detail_padding = new float[] { 5f, 2f, 1f, 5f };
//            float[] highpen_padding = new float[] { 4.5f, 1f, 2f, 1f };

//            int max_column = widths.Length;

//            PdfPTable tbl = CreateTable(max_column, widths);

//            /*ROW 1*/
//            tbl.AddCell(AddCell("Client ABN/ARBN:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            for (int i = 0; i < Form2.ClientARBN.Length; i++)
//            {
//                switch (i)
//                {
//                    case 0:
//                        tbl.AddCell(AddCell(Form2.ClientARBN.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//                        break;
//                    case 1:
//                        tbl.AddCell(AddCell(Form2.ClientARBN.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//                        tbl.AddCell(AddCell("-", 1, SEPARATOR_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_CENTER, highpen_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//                        break;
//                    case 2:
//                        tbl.AddCell(AddCell(Form2.ClientARBN.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//                        break;
//                    case 3:
//                        tbl.AddCell(AddCell(Form2.ClientARBN.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//                        break;
//                    case 4:
//                        tbl.AddCell(AddCell(Form2.ClientARBN.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//                        tbl.AddCell(AddCell("-", 1, SEPARATOR_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_CENTER, highpen_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//                        break;
//                    case 5:
//                        tbl.AddCell(AddCell(Form2.ClientARBN.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//                        break;
//                    case 6:
//                        tbl.AddCell(AddCell(Form2.ClientARBN.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//                        break;
//                    case 7:
//                        tbl.AddCell(AddCell(Form2.ClientARBN.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//                        tbl.AddCell(AddCell("-", 1, SEPARATOR_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_CENTER, highpen_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//                        break;
//                    case 8:
//                        tbl.AddCell(AddCell(Form2.ClientARBN.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//                        break;
//                    case 9:
//                        tbl.AddCell(AddCell(Form2.ClientARBN.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//                        break;
//                    case 10:
//                        tbl.AddCell(AddCell(Form2.ClientARBN.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//                        tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//                        break;
//                }
//            }
           
//            return tbl;
//        }
//        #endregion PAGE 2 : CLIENT TAX FILE NUMBER (Part 2)

//        #region PAGE 2 : CLIENT TAX FILE NUMBER (Part 3)
//        public PdfPTable Page_Two_Client_Tax_File_Number_III()
//        {
//            float[] widths = new float[] { 9.5f, 0.1f, 0.5f, 0.5f, 0.5f, 2f, 4f }; /*Array should be equal to no. of Columns*/

//            /*Top, Right, Bottom, Left*/
//            float[] header_padding = new float[] { 1f, 0f, 1f, 3f };
//            float[] subheader_padding = new float[] { 4f, 0f, 4f, 3f };
//            float[] detail_padding = new float[] { 5f, 2f, 1f, 2f };
//            float[] chk_padding = new float[] { 2f, 2f, 2f, 2f };

//            int max_column = widths.Length;

//            PdfPTable tbl = CreateTable(max_column, widths);

//            /*ROW 1*/
//            tbl.AddCell(AddCell("If you don't have an ABN or withholding tax payer number, have you applied for one?", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Form2.AppliedForWitholdingTaxNo) ? IMG_CHECK_RADIO : IMG_UNCHECK_RADIO, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Yes", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((!Form2.AppliedForWitholdingTaxNo) ? IMG_CHECK_RADIO : IMG_UNCHECK_RADIO, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("No", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 2*/
//            tbl.AddCell(AddCell("If you are a non-resident for tax purposes, please provide your country of residence.", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form2.CountryOfResidence, 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR)); tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            return tbl;
//        }
//        #endregion PAGE 2 : CLIENT TAX FILE NUMBER (Part 3)

//        #region PAGE 3 : INVESTOR STATUS
//        public PdfPTable Page_Three_Investor_Status()
//        {
//            float[] widths = new float[] { 0.15f, 1.2f, 0.15f, 4f }; /*Array should be equal to no. of Columns*/

//            /*Top, Right, Bottom, Left*/
//            float[] header_padding = new float[] { 1f, 0f, 1f, 3f };
//            float[] subheader_padding = new float[] { 4f, 0f, 4f, 3f };
//            float[] detail_padding = new float[] { 5f, 2f, 1f, 2f };
//            float[] chk_padding = new float[] { 2f, 2f, 2f, 2f };

//            int max_column = widths.Length;

//            PdfPTable tbl = CreateTable(max_column, widths);

//            tbl.AddCell(AddCell("INVESTOR STATUS", max_column, 12f, TABLE_FONT_HEADER, Element.ALIGN_LEFT, header_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_HEADER_BGCOLOR));
//            tbl.AddCell(AddCell("Please indicate if you are a wholesale or retail investor (see page 5 for the details).", max_column, 15f, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, subheader_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 1*/
//            tbl.AddCell(AddCellImage((Form3.InvestorType == "WI") ? IMG_CHECK_RADIO : IMG_UNCHECK_RADIO, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Wholesale Investor", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 2*/
//            tbl.AddCell(AddCellImage((Form3.InvestorType == "SI") ? IMG_CHECK_RADIO : IMG_UNCHECK_RADIO, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Sophisticated Investor", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS_UNDERLINE, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Form3.WithCertificateFrom) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Certificate from Accountant is attached ", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 3*/
//            tbl.AddCell(AddCellImage((Form3.InvestorType == "PI") ? IMG_CHECK_RADIO : IMG_UNCHECK_RADIO, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Professional Investor", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 4*/
//            tbl.AddCell(AddCellImage((Form3.InvestorType == "RI") ? IMG_CHECK_RADIO : IMG_UNCHECK_RADIO, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Retail Investor", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            return tbl;
//        }
//        #endregion PAGE 3 : INVESTOR STATUS

//        #region PAGE 3 : SERVICES
//        public PdfPTable Page_Three_Services()
//        {
//            float[] widths = new float[] { 0.4f, 0.5f, 0.4f, 3f, 1f, 2f, 4f, 4f }; /*Array should be equal to no. of Columns*/

//            /*Top, Right, Bottom, Left*/
//            float[] header_padding = new float[] { 1f, 0f, 1f, 3f };
//            float[] subheader_padding = new float[] { 4f, 0f, 4f, 3f };
//            float[] detail_padding = new float[] { 5f, 2f, 1f, 2f };
//            float[] chk_padding = new float[] { 2f, 2f, 2f, 2f };

//            int max_column = widths.Length;

//            PdfPTable tbl = CreateTable(max_column, widths);

//            tbl.AddCell(AddCell("SERVICES", max_column, 12f, TABLE_FONT_HEADER, Element.ALIGN_LEFT, header_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_HEADER_BGCOLOR));
//            tbl.AddCell(AddCell("Indicate the UMA Service and Accounts you require.", max_column, 15f, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, subheader_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 1*/
//            tbl.AddCell(AddCellImage((Form3.WithDoItYourself) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("DO IT YOURSELF (DIY)", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form3.DoItYourselfEstAmount.ToString(), 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 2*/
//            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Specify which Accounts you need opened", 7, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 3*/
//            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Form3.DoItForMeAccount.WithCMA) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("BWA CMA (for Bank Account)", 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 4*/
//            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Form3.DoItForMeAccount.WithDesktopBroker) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Desktop Broker (for HIN) ", 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 5*/
//            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Form3.DoItForMeAccount.WithFIIG) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("FIIG (for TDs & cash)", 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 6*/
//            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Form3.DoItForMeAccount.WithFIIG) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Money Market (for TDs & cash)", 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 7*/
//            tbl.AddCell(AddCellImage((Form3.WithDoItWithMe) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("DO IT WITH ME (DIWM)", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form3.DoItWithMeEstAmount.ToString(), 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 8*/
//            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Specify which Accounts you need opened", 7, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 9*/
//            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Form3.DoItWithMeAccount.WithCMA) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("BWA CMA (for Bank Account)", 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 10*/
//            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Form3.DoItWithMeAccount.WithDesktopBroker) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Desktop Broker (for HIN) ", 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 11*/
//            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Form3.DoItWithMeAccount.WithFIIG) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("FIIG (for TDs & cash)", 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 12*/
//            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Form3.DoItWithMeAccount.WithMMA) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Money Market (for TDs & cash)", 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 13*/
//            tbl.AddCell(AddCellImage((Form3.WithDoItForMe) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("DO IT FOR ME (DIFM)", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form3.DoItForMeEstAmount.ToString(), 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 14*/
//            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Investment Program Name:", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_CENTER, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form3.InvestmentProgram, 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("Program Code:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_CENTER, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form3.ProgramCode, 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 15*/
//            tbl.AddCell(AddCellImage((Form3.WithNominateInvestmentPreference) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Click on Nominated Investment Preferences to nominate your investment preferences", 7, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 16*/
//            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("The following Accounts will be opened for you. ", 7, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 17*/
//            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Form3.DoItForMeAccount.WithCMA) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("BWA CMA", 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 18*/
//            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Form3.DoItForMeAccount.WithDesktopBroker) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Desktop Broker (for HIN)", 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 19*/
//            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Form3.DoItForMeAccount.WithStateStreet) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("State Street (for Innova & P2)", 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 20*/
//            tbl.AddCell(AddCell("Note: When DIFM is the only service selected, the DIY BWA CMA will be automatically be ticked.", max_column, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            return tbl;
//        }
//        #endregion PAGE 3 : SERVICES

//        #region PAGE 3 : AUTHORITY TO ACT
//        public PdfPTable Page_Three_Authority_To_Act()
//        {
//            float[] widths = new float[] { 0.3f, 9.5f }; /*Array should be equal to no. of Columns*/

//            /*Top, Right, Bottom, Left*/
//            float[] header_padding = new float[] { 1f, 0f, 1f, 3f };
//            float[] subheader_padding = new float[] { 4f, 0f, 4f, 3f };
//            float[] detail_padding = new float[] { 5f, 2f, 1f, 2f };
//            float[] chk_padding = new float[] { 2f, 2f, 2f, 2f };

//            int max_column = widths.Length;

//            PdfPTable tbl = CreateTable(max_column, widths);

//            tbl.AddCell(AddCell("AUTHORITY TO ACT", max_column, 12f, TABLE_FONT_HEADER, Element.ALIGN_LEFT, header_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_HEADER_BGCOLOR));

//            /*ROW 1*/
//            tbl.AddCell(AddCellImage((Form3.WithLimitedPowerOfAttorney) ? IMG_CHECK_RADIO : IMG_UNCHECK_RADIO, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Client wishes to grant a limited power of attorney to e-Clipse Online Pty Limited (\"e-Clipse\") and Australian Money Market Pty Ltd. (AMM) to undertake and perform the agreed tasks on my/our behalf", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 2*/
//            tbl.AddCell(AddCellImage((!Form3.WithLimitedPowerOfAttorney) ? IMG_CHECK_RADIO : IMG_UNCHECK_RADIO, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Client does not wish to grant a limited power of attorney and will therefore issue all instructions and sign all forms directly.", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            return tbl;
//        }
//        #endregion PAGE 3 : AUTHORITY TO ACT

//        #region PAGE 4 :OTHER LINKED ACCOUNTS
//        public PdfPTable Page_Four_Other_Linked_Accounts()
//        {
//            float[] widths = new float[] { 4f, 1f, 0.3f, 1f, 0.3f, 1f, 0.3f, 1f, 0.3f, 1f, 0.3f, 1f, 9f }; /*Array should be equal to no. of Columns*/

//            /*Top, Right, Bottom, Left*/
//            float[] header_padding = new float[] { 1f, 0f, 1f, 3f };
//            float[] subheader_padding = new float[] { 4f, 0f, 4f, 3f };
//            float[] detail_padding = new float[] { 5f, 2f, 1f, 2f };
//            float[] chk_padding = new float[] { 2f, 2f, 2f, 2f };
//            float[] number_padding = new float[] { 0.2f, 0.2f, 0.2f, 0.2f };

//            int max_column = widths.Length;

//            PdfPTable tbl = CreateTable(max_column, widths);
           
//            tbl.AddCell(AddCell("OTHER LINKED ACCOUNTS", max_column, 12f, TABLE_FONT_HEADER, Element.ALIGN_LEFT, header_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_HEADER_BGCOLOR));
//            tbl.AddCell(AddCell("OTHER PLATFORMS", max_column, 15f, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, subheader_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 1*/
//            foreach (var Platform in Form4.PlatformCollection)
//            {
//                tbl.AddCell(AddCell("Name of the platform:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//                tbl.AddCell(AddCell((Platform.WithEclipseSuper) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 11, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//                tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//                /*SEPARATOR*/
//                tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//                tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//                tbl.AddCell(AddCell((Platform.WithBTWrap) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 11, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//                tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//                /*SEPARATOR*/
//                tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//                tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//                tbl.AddCell(AddCell((Platform.WithMarginLender) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 11, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//                tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
                
//                /*SEPARATOR*/
//                tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//                /*ROW 2*/
//                tbl.AddCell(AddCell("Investor No. / Account No.:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//                tbl.AddCell(AddCell(Platform.InvestorNo, 11, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//                tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//                /*SEPARATOR*/
//                tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//                /*ROW 3*/
//                tbl.AddCell(AddCell("Adviser No. / Code:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//                tbl.AddCell(AddCell(Platform.AdviserNo, 11, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//                tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//                /*SEPARATOR*/
//                tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//                /*ROW 4*/
//                tbl.AddCell(AddCell("Provider Name:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//                tbl.AddCell(AddCell(Platform.ProviderName, 11, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//                tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//                /*SEPARATOR*/
//                tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            }

//            /*ROW 6*/
//            tbl.AddCell(AddCell("MACQUARIE CMA", max_column, 15f, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, subheader_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 7*/
//            tbl.AddCell(AddCell("BSB:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            for (int i = 0; i < Form4.BSB.Length; i++)
//            {
//                switch (i)
//                {
//                    case 0:
//                        tbl.AddCell(AddCell(Form4.BSB.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//                        tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//                        break;
//                    case 1:
//                        tbl.AddCell(AddCell(Form4.BSB.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//                        tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//                        break;
//                    case 2:
//                        tbl.AddCell(AddCell(Form4.BSB.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//                        tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//                        break;
//                    case 3:
//                        tbl.AddCell(AddCell(Form4.BSB.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//                        tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//                        break;
//                    case 4:
//                        tbl.AddCell(AddCell(Form4.BSB.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//                        tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//                        break;
//                    case 5:
//                        tbl.AddCell(AddCell(Form4.BSB.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//                        tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//                        break;
//                }
//            }
            
//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 8*/
//            tbl.AddCell(AddCell("Account No:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form4.AccountNo, 11, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 9*/
//            tbl.AddCell(AddCell("Account Name:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form4.AccountName, 11, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            return tbl;
//        }
//        #endregion PAGE 4 : OTHER LINKED ACCOUNTS

//        #region PAGE 5 : INVESTOR/TRUSTEE/SIGNATORY DETAILS (PART 1)
//        public PdfPTable Page_Five_Authority_To_Act_I()
//        {
//            float[] widths = new float[] { 0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 0.4f, 0.3f, 0.5f, 0.7f, 2f, 5f }; /*Array should be equal to no. of Columns*/

//            /*Top, Right, Bottom, Left*/
//            float[] header_padding = new float[] { 1f, 0f, 1f, 3f };
//            float[] subheader_padding = new float[] { 4f, 0f, 4f, 3f };
//            float[] detail_padding = new float[] { 5f, 2f, 1f, 2f };
//            float[] chk_padding = new float[] { 2f, 2f, 2f, 2f };

//            int max_column = widths.Length;

//            PdfPTable tbl = CreateTable(max_column, widths);

//            tbl.AddCell(AddCell("INVESTOR/TRUSTEE/SIGNATORY DETAILS", max_column, 12f, TABLE_FONT_HEADER, Element.ALIGN_LEFT, header_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_HEADER_BGCOLOR));
//            tbl.AddCell(AddCell("INVESTOR/TRUSTEE/SIGNATORY 1", max_column, 15f, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, subheader_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 1*/
//            tbl.AddCell(AddCellImage((Form5.Honorific == "Mr") ? IMG_CHECK_RADIO : IMG_UNCHECK_RADIO, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Mr", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_RIGHT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Form5.Honorific == "Mrs") ? IMG_CHECK_RADIO : IMG_UNCHECK_RADIO, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Mrs", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_RIGHT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Form5.Honorific == "Ms") ? IMG_CHECK_RADIO : IMG_UNCHECK_RADIO, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Ms", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_RIGHT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Form5.Honorific == "Miss") ? IMG_CHECK_RADIO : IMG_UNCHECK_RADIO, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Miss", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_RIGHT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Form5.Honorific == "Other") ? IMG_CHECK_RADIO : IMG_UNCHECK_RADIO, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Other", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_RIGHT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form5.OtherHonorific, 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            return tbl;
//        }
//        #endregion PAGE 5 : INVESTOR/TRUSTEE/SIGNATORY DETAILS (PART 1)

//        #region PAGE 5 : INVESTOR/TRUSTEE/SIGNATORY DETAILS (PART 2)
//        public PdfPTable Page_Five_Authority_To_Act_II()
//        {
//            float[] widths = new float[] { 1f, 0.3f, 1f, 0.3f, 0.8f, 5f, 2f, 4f, 7f }; /*Array should be equal to no. of Columns*/

//            /*Top, Right, Bottom, Left*/
//            float[] header_padding = new float[] { 1f, 0f, 1f, 3f };
//            float[] subheader_padding = new float[] { 4f, 0f, 4f, 3f };
//            float[] detail_padding = new float[] { 5f, 2f, 1f, 2f };
//            float[] chk_padding = new float[] { 2f, 2f, 2f, 2f };

//            int max_column = widths.Length;

//            PdfPTable tbl = CreateTable(max_column, widths, 1f);

//            /*ROW 1*/
//            tbl.AddCell(AddCell("Family Name:", 5, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form5.FamilyName, 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("Given Name/s:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_CENTER, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form5.GivenName, 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 2*/
//            tbl.AddCell(AddCell("Date of Birth:", 5, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form5.DateOfBirth.ToString("MM/dd/yyyy"), 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 3*/
//            tbl.AddCell(AddCell("Title if Corporate Trustee", 9, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            return tbl;
//        }
//        #endregion PAGE 5 : INVESTOR/TRUSTEE/SIGNATORY DETAILS (PART 2)

//        #region PAGE 5 : INVESTOR/TRUSTEE/SIGNATORY DETAILS (PART 3)
//        public PdfPTable Page_Five_Authority_To_Act_III()
//        {
//            float[] widths = new float[] { 0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 0.4f, 0.3f, 0.5f, 0.7f, 2f, 5f }; /*Array should be equal to no. of Columns*/

//            /*Top, Right, Bottom, Left*/
//            float[] header_padding = new float[] { 1f, 0f, 1f, 3f };
//            float[] subheader_padding = new float[] { 4f, 0f, 4f, 3f };
//            float[] detail_padding = new float[] { 5f, 2f, 1f, 2f };
//            float[] chk_padding = new float[] { 2f, 2f, 2f, 2f };

//            int max_column = widths.Length;

//            PdfPTable tbl = CreateTable(max_column, widths, 10f);

//            /*ROW 1*/
//            tbl.AddCell(AddCellImage((Form5.CorporateTrusteeTitle == "Director") ? IMG_CHECK_RADIO : IMG_UNCHECK_RADIO, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Director", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_RIGHT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Form5.CorporateTrusteeTitle == "Secretary") ? IMG_CHECK_RADIO : IMG_UNCHECK_RADIO, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Secretary", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_RIGHT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Form5.CorporateTrusteeTitle == "Other") ? IMG_CHECK_RADIO : IMG_UNCHECK_RADIO, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Other", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_RIGHT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form5.OtherHonorific, 4, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            return tbl;
//        }
//        #endregion PAGE 5 : INVESTOR/TRUSTEE/SIGNATORY DETAILS (PART 3)

//        #region PAGE 5 : INVESTOR/TRUSTEE/SIGNATORY DETAILS (PART 4)
//        public PdfPTable Page_Five_Authority_To_Act_IV()
//        {
//            float[] widths = new float[] { 3f, 0.5f, 0.5f, 1f, 0.3f, 1f, 1f, 0.5f, 0.5f, 1f, 0.6f, 0.3f, 2f, 1f }; /*Array should be equal to no. of Columns*/

//            /*Top, Right, Bottom, Left*/
//            float[] header_padding = new float[] { 1f, 0f, 1f, 3f };
//            float[] subheader_padding = new float[] { 4f, 0f, 4f, 3f };
//            float[] detail_padding = new float[] { 5f, 2f, 1f, 2f };
//            float[] chk_padding = new float[] { 2f, 2f, 2f, 2f };

//            int max_column = widths.Length;

//            PdfPTable tbl = CreateTable(max_column, widths);

//            /*ROW 1*/
//            tbl.AddCell(AddCell("Residential Address (mandatory, a PO Box, RMB or c/ - is not sufficient):", 6, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Form5.WithDifferentMailingAddress) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Please tick ( √ ) if mailing address is different. ", 6, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 2*/
//            tbl.AddCell(AddCell("Address Line 1:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form5.ResidentialAddress.Addressline1, 13, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 3*/
//            tbl.AddCell(AddCell("Address Line 2:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form5.ResidentialAddress.Addressline2, 13, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 4*/
//            tbl.AddCell(AddCell("Suburb:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form5.ResidentialAddress.Suburb, 7, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("State:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form5.ResidentialAddress.State, 4, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 5*/
//            tbl.AddCell(AddCell("Postcode:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form5.ResidentialAddress.PostCode, 7, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Country:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form5.ResidentialAddress.Country, 4, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 6*/
//            tbl.AddCell(AddCell("Occupation:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form5.Occupation, 7, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Employer:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form5.Employer, 4, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 7*/
//            tbl.AddCell(AddCell("Main country of residence, if not Australia:", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form5.MainCountryOfResidence, 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("", 7, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 8*/
//            tbl.AddCell(AddCell("Contact Ph:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form5.ContactPh, 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Fax:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form5.FaxNo, 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 9*/
//            tbl.AddCell(AddCell("Alternate Ph:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form5.AlternatePh, 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Email:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form5.Email, 4, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 10*/


//            tbl.AddCell(AddCell("ID Type:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((!Form5.WithDriverLicense) ? IMG_UNCHECK_CHKBOX : IMG_CHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Drivers License:", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((!Form5.WithPassport) ? IMG_UNCHECK_CHKBOX : IMG_CHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Passport:", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("", 8, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 11*/
//            tbl.AddCell(AddCell("Document Issuer:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form5.DocumentIssuer, 7, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("", 6, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 12*/
//            tbl.AddCell(AddCell("Issue Date:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form5.IssueDate.ToString("MM/dd/yyyy"), 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Expiry Date:", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form5.ExpiryDate.ToString("MM/dd/yyyy"), 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 13*/
//            tbl.AddCell(AddCell("Document Number:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form5.DocumentNumber, 7, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Certified copy attached:", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Form5.WithCertifiedCopy) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            return tbl;
//        }
//        #endregion PAGE 5 : INVESTOR/TRUSTEE/SIGNATORY DETAILS (PART 4)

//        #region PAGE 6 : INVESTOR/TRUSTEE/SIGNATORY DETAILS (HEADER)
//        public PdfPTable Page_Six_Authority_To_Act(bool OneSignatoryOnly)
//        {
//            float[] widths = new float[] { 0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 0.4f, 0.3f, 0.5f, 0.7f, 2f, 5f }; /*Array should be equal to no. of Columns*/

//            /*Top, Right, Bottom, Left*/
//            float[] header_padding = new float[] { 1f, 0f, 1f, 3f };
//            float[] subheader_padding = new float[] { 4f, 0f, 4f, 3f };
//            float[] detail_padding = new float[] { 5f, 2f, 1f, 2f };
//            float[] chk_padding = new float[] { 2f, 2f, 2f, 2f };

//            int max_column = widths.Length;

//            PdfPTable tbl = CreateTable(max_column, widths);

//            tbl.AddCell(AddCell("INVESTOR/TRUSTEE/SIGNATORY DETAILS", max_column, 12f, TABLE_FONT_HEADER, Element.ALIGN_LEFT, header_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_HEADER_BGCOLOR));

//            tbl.AddCell(AddCellImage((OneSignatoryOnly) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Tick if only one Investor, Trustee or Signatory and this section is to be left blank", 11, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            
//            return tbl;
//        }
//        #endregion PAGE 6 : INVESTOR/TRUSTEE/SIGNATORY DETAILS (HEADER)

//        #region PAGE 6 : INVESTOR/TRUSTEE/SIGNATORY DETAILS (PART 1)
//        public PdfPTable Page_Six_Authority_To_Act_I(SixthFormEntity.Signatory Signatory, int Counter)
//        {
//            float[] widths = new float[] { 0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 0.4f, 0.3f, 0.5f, 0.7f, 2f, 5f }; /*Array should be equal to no. of Columns*/

//            /*Top, Right, Bottom, Left*/
//            float[] header_padding = new float[] { 1f, 0f, 1f, 3f };
//            float[] subheader_padding = new float[] { 4f, 0f, 4f, 3f };
//            float[] detail_padding = new float[] { 5f, 2f, 1f, 2f };
//            float[] chk_padding = new float[] { 2f, 2f, 2f, 2f };

//            int max_column = widths.Length;

//            PdfPTable tbl = CreateTable(max_column, widths);

//            tbl.AddCell(AddCell("INVESTOR/TRUSTEE/SIGNATORY " + Counter.ToString(), max_column, 15f, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, subheader_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            
//            /*ROW 1*/
//            tbl.AddCell(AddCellImage((Signatory.Honorific == "Mr") ? IMG_CHECK_RADIO : IMG_UNCHECK_RADIO, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Mr", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_RIGHT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Signatory.Honorific == "Mrs") ? IMG_CHECK_RADIO : IMG_UNCHECK_RADIO, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Mrs", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_RIGHT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Signatory.Honorific == "Ms") ? IMG_CHECK_RADIO : IMG_UNCHECK_RADIO, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Ms", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_RIGHT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Signatory.Honorific == "Miss") ? IMG_CHECK_RADIO : IMG_UNCHECK_RADIO, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Miss", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_RIGHT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Signatory.Honorific == "Other") ? IMG_CHECK_RADIO : IMG_UNCHECK_RADIO, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Other", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_RIGHT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Signatory.OtherHonorific, 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            return tbl;
//        }
//        #endregion PAGE 6 : INVESTOR/TRUSTEE/SIGNATORY DETAILS (PART 1)

//        #region PAGE 6 : INVESTOR/TRUSTEE/SIGNATORY DETAILS (PART 2)
//        public PdfPTable Page_Six_Authority_To_Act_II(SixthFormEntity.Signatory Signatory)
//        {
//            float[] widths = new float[] { 1f, 0.3f, 1f, 0.3f, 0.8f, 5f, 2f, 4f, 7f }; /*Array should be equal to no. of Columns*/

//            /*Top, Right, Bottom, Left*/
//            float[] header_padding = new float[] { 1f, 0f, 1f, 3f };
//            float[] subheader_padding = new float[] { 4f, 0f, 4f, 3f };
//            float[] detail_padding = new float[] { 5f, 2f, 1f, 2f };
//            float[] chk_padding = new float[] { 2f, 2f, 2f, 2f };

//            int max_column = widths.Length;

//            PdfPTable tbl = CreateTable(max_column, widths, 1f);

//            /*ROW 1*/
//            tbl.AddCell(AddCell("Family Name:", 5, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Signatory.FamilyName, 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("Given Name/s:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_CENTER, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Signatory.GivenName, 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 2*/
//            tbl.AddCell(AddCell("Date of Birth:", 5, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Signatory.DateOfBirth.ToString("MM/dd/yyyy"), 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 3*/
//            tbl.AddCell(AddCell("Title if Corporate Trustee", 9, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            return tbl;
//        }
//        #endregion PAGE 6 : INVESTOR/TRUSTEE/SIGNATORY DETAILS (PART 2)

//        #region PAGE 6 : INVESTOR/TRUSTEE/SIGNATORY DETAILS (PART 3)
//        public PdfPTable Page_Six_Authority_To_Act_III(SixthFormEntity.Signatory Signatory)
//        {
//            float[] widths = new float[] { 0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 0.4f, 0.3f, 0.5f, 0.7f, 2f, 5f }; /*Array should be equal to no. of Columns*/

//            /*Top, Right, Bottom, Left*/
//            float[] header_padding = new float[] { 1f, 0f, 1f, 3f };
//            float[] subheader_padding = new float[] { 4f, 0f, 4f, 3f };
//            float[] detail_padding = new float[] { 5f, 2f, 1f, 2f };
//            float[] chk_padding = new float[] { 2f, 2f, 2f, 2f };

//            int max_column = widths.Length;

//            PdfPTable tbl = CreateTable(max_column, widths, 10f);

//            /*ROW 1*/
//            tbl.AddCell(AddCellImage((Signatory.CorporateTrusteeTitle == "Director") ? IMG_CHECK_RADIO : IMG_UNCHECK_RADIO, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Director", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_RIGHT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Signatory.CorporateTrusteeTitle == "Secretary") ? IMG_CHECK_RADIO : IMG_UNCHECK_RADIO, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Secretary", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_RIGHT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Signatory.CorporateTrusteeTitle == "Other") ? IMG_CHECK_RADIO : IMG_UNCHECK_RADIO, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Other", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_RIGHT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Signatory.OtherCorporateTrusteeTitle, 4, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            return tbl;
//        }
//        #endregion PAGE 6 : INVESTOR/TRUSTEE/SIGNATORY DETAILS (PART 3)

//        #region PAGE 6 : INVESTOR/TRUSTEE/SIGNATORY DETAILS (PART 4)
//        public PdfPTable Page_Six_Authority_To_Act_IV(SixthFormEntity.Signatory Signatory)
//        {
//            float[] widths = new float[] { 3f, 0.5f, 0.5f, 1f, 0.3f, 1f, 1f, 0.5f, 0.5f, 1f, 0.6f, 0.3f, 2f, 1f }; /*Array should be equal to no. of Columns*/

//            /*Top, Right, Bottom, Left*/
//            float[] header_padding = new float[] { 1f, 0f, 1f, 3f };
//            float[] subheader_padding = new float[] { 4f, 0f, 4f, 3f };
//            float[] detail_padding = new float[] { 5f, 2f, 1f, 2f };
//            float[] chk_padding = new float[] { 2f, 2f, 2f, 2f };

//            int max_column = widths.Length;

//            PdfPTable tbl = CreateTable(max_column, widths);

//            /*ROW 1*/
//            tbl.AddCell(AddCell("Residential Address (mandatory, a PO Box, RMB or c/ - is not sufficient):", 6, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Signatory.WithDifferentMailingAddress) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Please tick ( √ ) if mailing address is different. ", 6, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 2*/
//            tbl.AddCell(AddCell("Address Line 1:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Signatory.ResidentialAddress.Addressline1, 13, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 3*/
//            tbl.AddCell(AddCell("Address Line 2:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Signatory.ResidentialAddress.Addressline2, 13, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 4*/
//            tbl.AddCell(AddCell("Suburb:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Signatory.ResidentialAddress.Suburb, 7, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("State:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Signatory.ResidentialAddress.State, 4, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 5*/
//            tbl.AddCell(AddCell("Postcode:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Signatory.ResidentialAddress.PostCode, 7, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Country:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Signatory.ResidentialAddress.Country, 4, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            tbl.AddCell(AddCell("Address Line 1:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Signatory.MailAddress.Addressline1, 13, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 3*/
//            tbl.AddCell(AddCell("Address Line 2:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Signatory.MailAddress.Addressline2, 13, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 4*/
//            tbl.AddCell(AddCell("Suburb:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Signatory.MailAddress.Suburb, 7, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("State:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Signatory.MailAddress.State, 4, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 5*/
//            tbl.AddCell(AddCell("Postcode:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Signatory.MailAddress.PostCode, 7, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Country:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Signatory.MailAddress.Country, 4, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));


//            /*ROW 6*/
//            tbl.AddCell(AddCell("Occupation:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Signatory.Occupation, 7, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Employer:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Signatory.Employer, 4, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 7*/
//            tbl.AddCell(AddCell("Main country of residence, if not Australia:", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Signatory.MainCountryOfResidence, 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("", 7, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 8*/
//            tbl.AddCell(AddCell("Contact Ph:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Signatory.ContactPh, 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Fax:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Signatory.FaxNo, 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 9*/
//            tbl.AddCell(AddCell("Alternate Ph:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Signatory.AlternatePh, 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Email:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Signatory.Email, 4, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 10*/
//            tbl.AddCell(AddCell("ID Type:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Signatory.WithDriverLicense) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Drivers License:", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Signatory.WithPassport) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Passport:", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("", 8, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 11*/
//            tbl.AddCell(AddCell("Document Issuer:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Signatory.DocumentIssuer, 7, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("", 6, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 12*/
//            tbl.AddCell(AddCell("Issue Date:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Signatory.IssuanceDate.ToString("MM/dd/yyyy"), 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Expiry Date:", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Signatory.ExpirationDate.ToString("MM/dd/yyyy"), 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 13*/
//            tbl.AddCell(AddCell("Document Number:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Signatory.DocumentNumber, 7, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Certified copy attached:", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Signatory.WithCertifiedCopy) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*Dyanmic Textbox*/

//            return tbl;
//        }
//        #endregion PAGE 6 : INVESTOR/TRUSTEE/SIGNATORY DETAILS (PART 4)

//        #region PAGE 7 : ACCOUNT CORRESPONDENCE DETAILS
//        public PdfPTable Page_Seven_Account_Correspondence_Details()
//        {
//            float[] widths = new float[] { 3f, 5f, 0.5f, 0.3f, 1f, 5f }; /*Array should be equal to no. of Columns*/

//            /*Top, Right, Bottom, Left*/
//            float[] header_padding = new float[] { 1f, 0f, 1f, 3f };
//            float[] subheader_padding = new float[] { 4f, 0f, 4f, 3f };
//            float[] detail_padding = new float[] { 5f, 2f, 1f, 2f };
//            float[] chk_padding = new float[] { 2f, 2f, 2f, 2f };

//            int max_column = widths.Length;

//            PdfPTable tbl = CreateTable(max_column, widths);

//            tbl.AddCell(AddCell("ACCOUNT CORRESPONDENCE DETAILS", max_column, 12f, TABLE_FONT_HEADER, Element.ALIGN_LEFT, header_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_HEADER_BGCOLOR));
//            tbl.AddCell(AddCell("Registered Address (mandatory, a P.O. Box, RMB or c/o is not sufficient)", max_column, 15f, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, subheader_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 1*/
//            tbl.AddCell(AddCell("Address Line 1:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form7.RegisteredAddress.Addressline1, 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 2*/
//            tbl.AddCell(AddCell("Address Line 2:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form7.RegisteredAddress.Addressline2, 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 3*/
//            tbl.AddCell(AddCell("Suburb:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form7.RegisteredAddress.Suburb, 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("State:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form7.RegisteredAddress.State, 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 4*/
//            tbl.AddCell(AddCell("Postcode:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form7.RegisteredAddress.PostCode, 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Country:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form7.RegisteredAddress.Country, 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 5*/
//            tbl.AddCell(AddCell("Mailing Address - Please tick ( √ ) if mailing address is same as Registered Address.", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((!Form7.SameasRegisteredAddress) ? IMG_UNCHECK_CHKBOX : IMG_CHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 6*/
//            tbl.AddCell(AddCell("Address Line 1:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form7.MailingAddress.Addressline1, 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 7*/
//            tbl.AddCell(AddCell("Address Line 2:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form7.MailingAddress.Addressline2, 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 8*/
//            tbl.AddCell(AddCell("Suburb:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form7.MailingAddress.Suburb, 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("State:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form7.MailingAddress.State, 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 9*/
//            tbl.AddCell(AddCell("Postcode:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form7.MailingAddress.PostCode, 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Country:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form7.MailingAddress.Country, 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 15*/
//            tbl.AddCell(AddCell("Duplicate Statement Mailing Address", max_column, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 16*/
//            tbl.AddCell(AddCell("Address Line 1:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form7.DuplicateStatementMailingAddress.Addressline1, 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 17*/
//            tbl.AddCell(AddCell("Address Line 2:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form7.DuplicateStatementMailingAddress.Addressline2, 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 18*/
//            tbl.AddCell(AddCell("Suburb:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form7.DuplicateStatementMailingAddress.Suburb, 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("State:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form7.DuplicateStatementMailingAddress.State, 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 19*/
//            tbl.AddCell(AddCell("Postcode:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form7.DuplicateStatementMailingAddress.PostCode, 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Country:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form7.DuplicateStatementMailingAddress.Country, 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

//            return tbl;
//        }
//        #endregion PAGE 7 : ACCOUNT CORRESPONDENCE DETAILS

//        #region PAGE 8 : DIY - BWA CMA OPTIONS (Part 1)
//        public PdfPTable Page_Eight_DIY_BWA_CMA_Options_I()
//        {
//            float[] widths = new float[] { 0.2f, 0.8f, 0.2f, 0.8f, 0.2f, 0.2f, 0.8f, 0.2f, 1.3f, 0.2f, 3f }; /*Array should be equal to no. of Columns*/

//            /*Top, Right, Bottom, Left*/
//            float[] header_padding = new float[] { 1f, 0f, 1f, 3f };
//            float[] subheader_padding = new float[] { 4f, 0f, 4f, 3f };
//            float[] detail_padding = new float[] { 5f, 2f, 1f, 2f };
//            float[] chk_padding = new float[] { 2f, 2f, 2f, 2f };

//            int max_column = widths.Length;

//            PdfPTable tbl = CreateTable(max_column, widths, 1f);

//            tbl.AddCell(AddCell("DIY - BWA CMA OPTIONS", max_column, 12f, TABLE_FONT_HEADER, Element.ALIGN_LEFT, header_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_HEADER_BGCOLOR));
//            tbl.AddCell(AddCell("The following selections will apply to my/our DIY BWA CMA", max_column, 15f, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, subheader_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            tbl.AddCell(AddCell("BWA ACCESS FACILITIES", max_column, 15f, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, subheader_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            tbl.AddCell(AddCell("Please tick ( √ ) the Access Facilities required:", max_column, 15f, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, subheader_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            tbl.AddCell(AddCellImage((Form8.DIYOption.WithPhoneAccess) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Phone Access", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Form8.DIYOption.WithOnlineAccess) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Online Access", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Form8.DIYOption.WithDebitCard) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Debit Card", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Form8.DIYOption.WithChequeBook) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Cheque Book (25 per book)", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Form8.DIYOption.WithDepositBook) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Deposit Book", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            return tbl;
//        }
//        #endregion PAGE 8 : DIY - BWA CMA OPTIONS (Part 1)

//        #region PAGE 8 : DIY - BWA CMA OPTIONS (Part 2)
//        public PdfPTable Page_Eight_DIY_BWA_CMA_Options_II()
//        {
//            float[] widths = new float[] { 0.2f, 0.8f, 0.2f, 0.8f, 0.2f, 0.2f, 0.8f, 3f }; /*Array should be equal to no. of Columns*/

//            /*Top, Right, Bottom, Left*/
//            float[] header_padding = new float[] { 1f, 0f, 1f, 3f };
//            float[] subheader_padding = new float[] { 4f, 0f, 4f, 3f };
//            float[] detail_padding = new float[] { 5f, 2f, 1f, 2f };
//            float[] chk_padding = new float[] { 2f, 2f, 2f, 2f };

//            int max_column = widths.Length;

//            PdfPTable tbl = CreateTable(max_column, widths);

//            tbl.AddCell(AddCell("BWA MANNER OF OPERATION", max_column, 15f, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, subheader_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            tbl.AddCell(AddCell("Please select how you wish to operate your CMA by ticking ( √ ) one of the following:", max_column, 15f, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, subheader_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
           
//            tbl.AddCell(AddCellImage((Form8.DIYOption.OperateOption == "ONE") ? IMG_CHECK_RADIO : IMG_UNCHECK_RADIO, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Any one of us to sign", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Form8.DIYOption.OperateOption == "TWO") ? IMG_CHECK_RADIO : IMG_UNCHECK_RADIO, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Any two of us to sign", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Form8.DIYOption.OperateOption == "ALL") ? IMG_CHECK_RADIO : IMG_UNCHECK_RADIO, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("All of us to sign", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            tbl.AddCell(AddCell("Note:", max_column, 15f, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, subheader_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("1. Where you do not elect a manner of operation, BWA will default to 'All of us to sign'.", max_column, 15f, TABLE_FONT_DETAILS_ITALIC, Element.ALIGN_LEFT, subheader_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("2. Phone Access, Online Access and a Debit Card cannot be selected unless the manner of operation is 'Any one of us to sign'.", max_column, 15f, TABLE_FONT_DETAILS_ITALIC, Element.ALIGN_LEFT, subheader_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            return tbl;
//        }
//        #endregion PAGE 8 : DIY - BWA CMA OPTIONS (Part 2)

//        #region PAGE 8 : DIWM - BWA CMA OPTIONS (Part 1)
//        public PdfPTable Page_Eight_DIWM_BWA_CMA_Options_I()
//        {
//            float[] widths = new float[] { 0.2f, 0.8f, 0.2f, 0.8f, 0.2f, 0.2f, 0.8f, 0.2f, 1.3f, 0.2f, 3f }; /*Array should be equal to no. of Columns*/

//            /*Top, Right, Bottom, Left*/
//            float[] header_padding = new float[] { 1f, 0f, 1f, 3f };
//            float[] subheader_padding = new float[] { 4f, 0f, 4f, 3f };
//            float[] detail_padding = new float[] { 5f, 2f, 1f, 2f };
//            float[] chk_padding = new float[] { 2f, 2f, 2f, 2f };

//            int max_column = widths.Length;

//            PdfPTable tbl = CreateTable(max_column, widths, 1f);

//            tbl.AddCell(AddCell("DIWM - BWA CMA OPTIONS", max_column, 12f, TABLE_FONT_HEADER, Element.ALIGN_LEFT, header_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_HEADER_BGCOLOR));
//            tbl.AddCell(AddCell("The following selections will apply to my/our DIWM BWA CMA", max_column, 15f, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, subheader_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            tbl.AddCell(AddCell("BWA ACCESS FACILITIES", max_column, 15f, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, subheader_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            tbl.AddCell(AddCell("Please tick ( √ ) the Access Facilities required:", max_column, 15f, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, subheader_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            tbl.AddCell(AddCellImage((Form8.DIWMOption.WithPhoneAccess) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Phone Access", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Form8.DIWMOption.WithOnlineAccess) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Online Access", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Form8.DIWMOption.WithDebitCard) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Debit Card", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Form8.DIWMOption.WithChequeBook) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Cheque Book (25 per book)", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Form8.DIWMOption.WithDepositBook) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Deposit Book", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            return tbl;
//        }
//        #endregion PAGE 8 : DIWM - BWA CMA OPTIONS (Part 1)

//        #region PAGE 8 : DIWM - BWA CMA OPTIONS (Part 2)
//        public PdfPTable Page_Eight_DIWM_BWA_CMA_Options_II()
//        {
//            float[] widths = new float[] { 0.2f, 0.8f, 0.2f, 0.8f, 0.2f, 0.2f, 0.8f, 3f }; /*Array should be equal to no. of Columns*/

//            /*Top, Right, Bottom, Left*/
//            float[] header_padding = new float[] { 1f, 0f, 1f, 3f };
//            float[] subheader_padding = new float[] { 4f, 0f, 4f, 3f };
//            float[] detail_padding = new float[] { 5f, 2f, 1f, 2f };
//            float[] chk_padding = new float[] { 2f, 2f, 2f, 2f };

//            int max_column = widths.Length;

//            PdfPTable tbl = CreateTable(max_column, widths);

//            tbl.AddCell(AddCell("BWA MANNER OF OPERATION", max_column, 15f, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, subheader_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            tbl.AddCell(AddCell("Please select how you wish to operate your CMA by ticking ( √ ) one of the following:", max_column, 15f, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, subheader_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            
//            tbl.AddCell(AddCellImage((Form8.DIWMOption.OperateOption == "ONE") ? IMG_CHECK_RADIO : IMG_UNCHECK_RADIO, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Any one of us to sign", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Form8.DIWMOption.OperateOption == "TWO") ? IMG_CHECK_RADIO : IMG_UNCHECK_RADIO, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Any two of us to sign", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Form8.DIWMOption.OperateOption == "ALL") ? IMG_CHECK_RADIO : IMG_UNCHECK_RADIO, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("All of us to sign", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            tbl.AddCell(AddCell("Note:", max_column, 15f, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, subheader_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("1. Where you do not elect a manner of operation, BWA will default to 'All of us to sign'.", max_column, 15f, TABLE_FONT_DETAILS_ITALIC, Element.ALIGN_LEFT, subheader_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("2. Phone Access, Online Access and a Debit Card cannot be selected unless the manner of operation is 'Any one of us to sign'.", max_column, 15f, TABLE_FONT_DETAILS_ITALIC, Element.ALIGN_LEFT, subheader_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            return tbl;
//        }
//        #endregion PAGE 8 : DIWM - BWA CMA OPTIONS (Part 2)

//        #region PAGE 9 : SECURITIES Part I
//        public PdfPTable Page_Nine_SecuritiesI()
//        {
//            float[] widths = new float[] { 3f, 3f, 3f, 3f, 5f, 5f, 3f, 3f, 3f, 3f, 3f, 3f, 2f, 5f,  0.3f, 1f, 1f, 0.3f, 0.3f, 0.3f, 0.4f, 0.3f, 0.5f, 0.7f, 2f, 5f, 5f,
//                                       1f, 1f, 1f, 0.3f, 0.3f, 0.3f, 0.4f, 0.3f, 0.5f, 0.7f, 2f, 5f,  0.3f, 1f, 1f, 0.3f, 0.3f, 0.3f, 0.4f, 0.3f, 0.5f, 0.7f, 2f, 5f, 5f,
//                                       1f, 1f};

//            float[] header_padding = new float[] { 1f, 0f, 1f, 3f };
//            float[] subheader_padding = new float[] { 4f, 0f, 4f, 3f };
//            float[] detail_padding = new float[] { 5f, 2f, 1f, 2f };
//            float[] chk_padding = new float[] { 2f, 2f, 2f, 2f };

//            int max_column = widths.Length;

//            PdfPTable tbl = CreateTable(max_column, widths);
//            /*ROW 1*/
//            tbl.AddCell(AddCell("CHESS TRANSFER OF SECURITIES", max_column, 12f, TABLE_FONT_HEADER, Element.ALIGN_LEFT, header_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_HEADER_BGCOLOR));
//            tbl.AddCell(AddCell("Do you want to transfer any existing listed securities to the new Desktop Broker account to be established in your name?", max_column, 15f, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, subheader_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 2*/
//            tbl.AddCell(AddCell("Yes", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_RIGHT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Form9.TransferExistingListedSecurities) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("No", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_RIGHT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((!Form9.TransferExistingListedSecurities) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 3*/
//            tbl.AddCell(AddCell("If Yes to which service (ie. DIY, DIWM):", 7, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form9.TransferToService, 4, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, chk_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            return tbl;
//        }
//        #endregion PAGE 9 : SECURITIES Part I

//        #region PAGE 9 : SECURITIES Part II
//        public PdfPTable Page_Nine_SecuritiesII()
//        {
//            float[] widths = new float[] { 
//            3f, 0.2f, 0.1f, 0.2f, 0.1f, 0.2f, 0.1f, 0.2f, 0.1f, 0.2f, 0.1f, 0.2f, 0.1f, 0.2f, 0.1f, 0.2f, 0.1f, 0.2f, 0.1f, 0.2f, 0.1f, 0.2f, 0.1f,
//                                        0.2f, 0.1f, 0.2f, 0.1f, 0.2f, 0.1f, 0.2f, 0.1f, 0.2f, 0.1f, 0.2f, 0.1f, 0.2f, 0.1f, 0.2f, 0.1f, 0.2f, 0.1f, 0.2f, 0.1f, 0.2f, 0.1f,
//                                          0.2f, 0.1f, 0.2f, 0.1f, 0.2f, 0.1f, 0.2f, 0.1f, 0.2f, 0.1f, 0.2f, 0.1f, 0.2f, 0.1f, 0.2f, 0.1f, 0.2f, 0.1f};

//            float[] header_padding = new float[] { 1f, 0f, 1f, 3f };
//            float[] subheader_padding = new float[] { 4f, 0f, 4f, 3f };
//            float[] detail_padding = new float[] { 5f, 2f, 1f, 5f };
//            float[] highpen_padding = new float[] { 4.5f, 5f, 2f, 3f };

//            int max_column = widths.Length;

//            PdfPTable tbl = CreateTable(max_column, widths);

//            tbl.AddCell(AddCell("CHESS REGISTRATION DETAILS ", max_column, 12f, TABLE_FONT_HEADER, Element.ALIGN_LEFT, header_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_HEADER_BGCOLOR));
//            tbl.AddCell(AddCell("Please note that if you are transfering securities, your registered address should be EXACTLY the same as on your latest holding statement.", max_column, 15f, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, subheader_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            tbl.AddCell(AddCell("Registered Account Name:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form9.RegisteredAccountName, 4, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 1*/
//            tbl.AddCell(AddCell("Account Designation:", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//           tbl.AddCell(AddCell("(Designation must not be more than 25 characters)", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
            
//            for (int i = 0; i < Form9.AccountDesignation.Length; i++)
//            {
//                tbl.AddCell(AddCell(Form9.AccountDesignation.Substring(i, 1), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//                tbl.AddCell(AddCell("", 1, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));              
//            }

//            return tbl;
//        }
//        #endregion PAGE 9 : SECURITIES Part II

//        #region PAGE 9 : SECURITIES Part III
//        public PdfPTable Page_Nine_SecuritiesIII()
//        {
//            float[] widths = new float[] { 2f, 3f, 13f, 27f, 2f, 3f, 18f, 2f, 3f, 18f };

//            float[] header_padding = new float[] { 1f, 0f, 1f, 3f };
//            float[] subheader_padding = new float[] { 4f, 0f, 4f, 3f };
//            float[] detail_padding = new float[] { 5f, 2f, 1f, 2f };
//            float[] chk_padding = new float[] { 2f, 2f, 2f, 2f };

//            int max_column = widths.Length;

//            PdfPTable tbl = CreateTable(max_column, widths);

//            tbl.AddCell(AddCell("CHESS Registration Address:", max_column, 15f, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, subheader_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            /*ROW 7*/
//            tbl.AddCell(AddCell("Address:", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form9.RegisteredAddress.Addressline1, 4, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 8*/
//            tbl.AddCell(AddCell("Line2", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form9.RegisteredAddress.Addressline2, 4, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 7*/
//            tbl.AddCell(AddCell("Suburb:", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form9.RegisteredAddress.Suburb, 4, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("State:", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form9.RegisteredAddress.State, 4, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 8*/
//            tbl.AddCell(AddCell("Postcode:", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form9.RegisteredAddress.PostCode, 4, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Country:", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form9.RegisteredAddress.Country, 4, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            return tbl;
//        }
//        #endregion PAGE 9 : SECURITIES Part III


//        #region PAGE 9 : SECURITIES Part IV
//        public PdfPTable Page_Nine_SecuritiesIV()
//        {
//            float[] widths = new float[] { 0.4f, 0.5f, 0.4f, 3f, 1f, 2f, 4f, 4f };

//            float[] header_padding = new float[] { 1f, 0f, 1f, 3f };
//            float[] subheader_padding = new float[] { 4f, 0f, 4f, 3f };
//            float[] detail_padding = new float[] { 5f, 2f, 1f, 2f };
//            float[] chk_padding = new float[] { 2f, 2f, 2f, 2f };

//            int max_column = widths.Length;

//            PdfPTable tbl = CreateTable(max_column, widths);

//            tbl.AddCell(AddCell("CHESS SPONSORSHIP TRANSFER  ", max_column, 12f, TABLE_FONT_HEADER, Element.ALIGN_LEFT, header_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_HEADER_BGCOLOR));
//            tbl.AddCell(AddCell("Are the securities to be transfered", max_column, 15f, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, subheader_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 1*/
//            tbl.AddCell(AddCellImage((Form9.IssuerSponsored) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(" Issuer Sponsored - please attach copies of your Issuer Sponsored Holding Statements", 10, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 3*/
//            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Form9.IssuerAddressMatch) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Exact Name/Address match as above", 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 4*/
//            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form9.IssuerCompanyHoldings.ToString(), 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("How many different company holdings", 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 5*/
//            tbl.AddCell(AddCell("i. Use an Issuer Sponsored Holdings to CHESS Sponsorship Conversion Form for all matching holdings ", 10, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 6*/
//            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Form9.IssuerAddressNotMatch) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Name/Address does not match the CHESS Registration Details above", 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 7*/
//            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(Form9.IssuerCompanyHoldingsChange.ToString(), 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//            tbl.AddCell(AddCell("How many different company holdings need to be changed?", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 8*/
//            tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("i. Use Change of client details ", 7, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("ii. Use an Issuer Sponsored Holdings to CHESS Sponsorship Conversion Form for all un-matched holdings", 7, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 9*/
//            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCellImage((Form9.BrokerSponsored) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell(" Broker Sponsored - please attach copies of your CHESS Registration", 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 10*/
//            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("i. Use the Broker to Broker Transfer Request, for each different Broker ", 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 11*/
//            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("ii. If names/addresses are not matched, use a Change of Details Form ", 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 12*/
//            tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("iii. If there is a change of Legal or Beneficial ownership, use an off market transfer form for each holding ", 5, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*SEPARATOR*/
//            tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 13*/
//            foreach (var Broker in Form9.ExistingBroker)
//            {
//                tbl.AddCell(AddCellImage(IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//                tbl.AddCell(AddCell("Name of existing Sponsoring Broker: ", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//                tbl.AddCell(AddCell(Broker.ExistingBrokerName, 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//                tbl.AddCell(AddCell("", 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//                /*SEPARATOR*/
//                tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//                /*ROW 14*/
//                tbl.AddCell(AddCell("", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//                tbl.AddCell(AddCell("Existing Broker PID:", 3, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_CENTER, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//                tbl.AddCell(AddCell(Broker.ExistingBrokerPID, 2, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));
//                tbl.AddCell(AddCell("Client HIN: ", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_CENTER, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//                tbl.AddCell(AddCell(Broker.ClientHIN, 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0.5f, TEXTBOX_BORDERCOLOR, TEXTBOX_BGCOLOR));

//                /*SEPARATOR*/
//                tbl.AddCell(AddCell("", max_column, SEPARATOR_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            }

//            return tbl;
//        }
//        #endregion PAGE 9 : SECURITIES Part IV

//        #region PAGE 10 : UMA DOCUMENT CHECKLIST
//        public PdfPTable Page_Ten_UMA_Document_Checklist()
//        {
//            float[] widths = new float[] { 0.3f, 9.5f }; /*Array should be equal to no. of Columns*/

//            /*Top, Right, Bottom, Left*/
//            float[] header_padding = new float[] { 1f, 0f, 1f, 3f };
//            float[] subheader_padding = new float[] { 4f, 0f, 4f, 3f };
//            float[] detail_padding = new float[] { 5f, 2f, 1f, 2f };
//            float[] chk_padding = new float[] { 2f, 2f, 2f, 2f };

//            int max_column = widths.Length;

//            PdfPTable tbl = CreateTable(max_column, widths);

//            tbl.AddCell(AddCell("UMA DOCUMENT CHECKLIST", max_column, 12f, TABLE_FONT_HEADER, Element.ALIGN_LEFT, header_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_HEADER_BGCOLOR));
//            tbl.AddCell(AddCell("The following documents are required to establish bank and broker accounts.", max_column, 15f, TABLE_FONT_IDENTIFIER, Element.ALIGN_LEFT, subheader_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 1*/
//            tbl.AddCell(AddCellImage((Form10.ProofOfID) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Proof of ID (Drivers Licence or passport) ", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 2*/
//            tbl.AddCell(AddCellImage((Form10.TrustDeed) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Trust Deed (If a Trust)", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 3*/
//            tbl.AddCell(AddCellImage((Form10.LetterFromYourAccountant) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Letter from your Accountant (If a sophisticated Investor)", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 4*/
//            tbl.AddCell(AddCellImage((Form10.CertificateOfRegistration) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Business Name Registration Certificate (If a business account) or Certificate of Registration ", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 5*/
//            tbl.AddCell(AddCellImage((Form10.BrokerToBRokerTransfer) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Broker to Broker Transfer", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 6*/
//            tbl.AddCell(AddCellImage((Form10.IssuerSponsoredHoldingsToChessSponsorshipConversion) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Issuer Sponsored Holdings to CHESS Sponsorship Conversion", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 7*/
//            tbl.AddCell(AddCellImage((Form10.IssuerSponsoredHoldingStatements) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Issuer Sponsored Holding Statements", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 8*/
//            tbl.AddCell(AddCellImage((Form10.OffMarketTransfer) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Off Market Transfer", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            /*ROW 9*/
//            tbl.AddCell(AddCellImage((Form10.ChangeOfClientDetailsForm) ? IMG_CHECK_CHKBOX : IMG_UNCHECK_CHKBOX, 1, Element.ALIGN_LEFT, chk_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));
//            tbl.AddCell(AddCell("Change of Client Details Form", 1, ROW_DETAIL_HEIGHT, TABLE_FONT_DETAILS, Element.ALIGN_LEFT, detail_padding, 0f, TABLE_DETAIL_BORDER_COLOR, TABLE_DETAIL_BGCOLOR));

//            return tbl;
//        }
//        #endregion PAGE 10 : UMA DOCUMENT CHECKLIST

//        #region CREATE PDF
//        public Boolean CreatePDF()
//        {
//            //Instantiate a document object set pagesize and margin left, right, top, bottom
//            Document document = new Document(PageSize.LETTER, 20, 20, 30, 30);

//            try
//            {
//                HttpContext Page = HttpContext.Current;
//                String pdfFilePath = Page.Server.MapPath(filePath);

//                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(pdfFilePath, FileMode.Create));
//                document.Open();

//                document.Add(Page_One_Client_Details());
//                document.Add(Page_One_Adviser());
//                document.Add(Page_Two_Client_Tax_File_Number_I());
//                document.Add(Page_Two_Client_Tax_File_Number_II());
//                document.Add(Page_Two_Client_Tax_File_Number_III());
//                document.Add(Page_Three_Investor_Status());
//                document.Add(Page_Three_Services());
//                document.Add(Page_Three_Authority_To_Act());
//                document.Add(Page_Four_Other_Linked_Accounts());
//                document.Add(Page_Five_Authority_To_Act_I());
//                document.Add(Page_Five_Authority_To_Act_II());
//                document.Add(Page_Five_Authority_To_Act_III());
//                document.Add(Page_Five_Authority_To_Act_IV());

//                document.Add(Page_Six_Authority_To_Act(Form6.WithOneInvestorTrusteeSignatory));

//                int Counter = 2;
//                foreach (var Form6Signatory in Form6.SignatoryCollection)
//                {
//                    document.Add(Page_Six_Authority_To_Act_I(Form6Signatory, Counter));
//                    document.Add(Page_Six_Authority_To_Act_II(Form6Signatory));
//                    document.Add(Page_Six_Authority_To_Act_III(Form6Signatory));
//                    document.Add(Page_Six_Authority_To_Act_IV(Form6Signatory));
//                    Counter += 1;
//                }

//                document.Add(Page_Seven_Account_Correspondence_Details());
//                document.Add(Page_Eight_DIY_BWA_CMA_Options_I());
//                document.Add(Page_Eight_DIY_BWA_CMA_Options_II());
//                document.Add(Page_Eight_DIWM_BWA_CMA_Options_I());
//                document.Add(Page_Eight_DIWM_BWA_CMA_Options_II());
//                document.Add(Page_Nine_SecuritiesI());
//                document.Add(Page_Nine_SecuritiesII());
//                document.Add(Page_Nine_SecuritiesIII());
//                document.Add(Page_Nine_SecuritiesIV());
//                document.Add(Page_Ten_UMA_Document_Checklist());

//                return true;
//            }
//            catch (DocumentException docEx)
//            {
//                errMsg = docEx.Message;
//            }
//            catch (IOException ioEx)
//            {
//                errMsg = ioEx.Message;
//            }
//            catch (Exception ex)
//            {
//                errMsg = ex.Message;
//            }
//            finally
//            {
//                document.Close();
//            }

//            return false;

//        }
//        #endregion CREATE PDF

//        #region CREATE TABLE
//        private PdfPTable CreateTable(int columns, float[] width, float after = 20f)
//        {
//            PdfPTable table = new PdfPTable(columns);
//            table.TotalWidth = TABLE_WIDTH;
//            table.WidthPercentage = TABLE_WIDTH;
//            table.SetWidths(width);
//            table.SpacingBefore = 0f;
//            table.SpacingAfter = after;
//            return table;
//        }
//        #endregion CREATE TABLE

//        #region ADD CELL
//        private PdfPCell AddCell(string label, int colSpan, float height, Font font, int alignment, float[] padding, float border, BaseColor bordercolor, BaseColor bgcolor)
//        {
//            /*label, colspan, size, fontstyle, color, alignment, padding, border, bordercolor, bgcolor*/
//            PdfPCell cell = new PdfPCell(new Phrase(label, font));

//            cell.BackgroundColor = bgcolor;
//            cell.BorderColor = bordercolor;
//            cell.Colspan = colSpan;
//            cell.BorderWidth = border;
//            cell.HorizontalAlignment = alignment;
//            cell.FixedHeight = height;
//            cell.PaddingTop = padding[0];
//            cell.PaddingRight = padding[1];
//            cell.PaddingBottom = padding[2];
//            cell.PaddingLeft = padding[3];

//            return cell;
//        }
//        #endregion ADD CELL

//        #region ADD CELL IMAGE
//        private PdfPCell AddCellImage(string path, int colSpan, int alignment, float[] padding, float border, BaseColor bordercolor, BaseColor bgcolor)
//        {
//            /*path, colspan, alignment, padding, border, bordercolor, bgcolor*/

//            HttpContext Page = HttpContext.Current;
//            Image image = Image.GetInstance(Page.Server.MapPath(path));

//            PdfPCell cell = new PdfPCell(image);

//            cell.BackgroundColor = bgcolor;
//            cell.BorderColor = bordercolor;
//            cell.Colspan = colSpan;
//            cell.BorderWidth = border;
//            cell.HorizontalAlignment = alignment;

//            return cell;
//        }
//        #endregion ADD CELL IMAGE

//    }
//}