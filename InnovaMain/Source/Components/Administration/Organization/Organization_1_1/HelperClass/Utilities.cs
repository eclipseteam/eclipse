﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Xml.Serialization;
using System.Security.Cryptography;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Xml;
using System.Configuration;
using System.Net.Mail;
using System.Net;
using Oritax.TaxSimp.CM.Organization.Model.Organization;
using Oritax.TaxSimp.CM.Organization.Model.Entities;
using Oritax.TaxSimp.CM.Organization.Model;

namespace Oritax.TaxSimp.CM.Organization.HelperClass
{
    [Serializable]
    public static class Utilities
    {
        public static String ToXML(this Object Obj)
        {
            XmlSerializer serializer = new XmlSerializer(Obj.GetType());
            StringWriter textWriter = new StringWriter();

            serializer.Serialize(textWriter, Obj);
            return textWriter.ToString();
        }

        public static bool ToPDF(this object form)
        {
            //PDFHelper pdf = new PDFHelper(form as FormOrganization);
            //if (pdf.CreatePDF())
            //{
            //    return true;
            //}
            //else
                return false;
        }
        public static ThirdFormEntity.AccountOptions ToAccountOption(this object obj)
        {
            ThirdFormEntity.AccountOptions model = obj as ThirdFormEntity.AccountOptions;
            return new ThirdFormEntity.AccountOptions
            {
                WithCMA = model.WithCMA,
                WithDesktopBroker = model.WithDesktopBroker,
                WithFIIG = model.WithFIIG,
                WithMMA = model.WithMMA,
                WithStateStreet = model.WithStateStreet
            };
        }
        public static List<ThirdFormEntity.InvestmentExclusions> ToInvestmentExclusions(this object obj)
        {
            List<ThirdFormEntity.InvestmentExclusions> modelList = new List<ThirdFormEntity.InvestmentExclusions>();
            List<ThirdFormEntity.InvestmentExclusions> objList = obj as List<ThirdFormEntity.InvestmentExclusions>;

            objList.ForEach(item =>
            {
                modelList.Add(new ThirdFormEntity.InvestmentExclusions
                {

                    ASXCode = item.ASXCode,
                    CompanyName = item.CompanyName,
                    ExclusionType = item.ExclusionType
                });
            });

            //foreach (var item in objList)
            //{
            //    modelList.Add(new ThirdFormEntity.InvestmentExclusions
            //    {

            //        ASXCode = item.ASXCode,
            //        CompanyName = item.CompanyName,
            //        ExclusionType = item.ExclusionType
            //    });


            //}


            return modelList;

        }

        public static AddressEntity ToAddressEntity(this object obj)
        {
            AddressEntity item = obj as AddressEntity;


            return (new AddressEntity
                 {

                     Addressline1 = item.Addressline1,
                     Addressline2 = item.Addressline2,
                     Country = item.Country,
                     PostCode = item.PostCode,
                     State = item.State,
                     Suburb = item.Suburb
                 });

        }
        public static List<SixthFormEntity.Signatory> ToSignatory(this object obj)
        {
            List<SixthFormEntity.Signatory> modelList = obj as List<SixthFormEntity.Signatory>;
            List<SixthFormEntity.Signatory> objList = obj as List<SixthFormEntity.Signatory>;

            objList.ForEach(item =>
            {
                modelList.Add(new SixthFormEntity.Signatory
                {
                    AlternatePh = item.AlternatePh,
                    ContactPh = item.ContactPh,
                    CorporateTrusteeTitle = item.CorporateTrusteeTitle,
                    DateOfBirth = item.DateOfBirth,
                    DocumentIssuer = item.DocumentIssuer,
                    DocumentNumber = item.DocumentNumber,
                    Email = item.Email,
                    Employer = item.Employer,
                    ExpirationDate = item.ExpirationDate,
                    FamilyName = item.FamilyName,
                    FaxNo = item.FaxNo,
                    GivenName = item.GivenName,
                    Honorific = item.Honorific,
                    IssuanceDate = item.IssuanceDate,
                    MailAddress = item.MailAddress,
                    MainCountryOfResidence = item.MainCountryOfResidence,
                    Occupation = item.Occupation,
                    OtherCorporateTrusteeTitle = item.OtherCorporateTrusteeTitle,
                    OtherHonorific = item.OtherHonorific,
                    ResidentialAddress = item.ResidentialAddress,
                    WithCertifiedCopy = item.WithCertifiedCopy,
                    WithDifferentMailingAddress = item.WithDifferentMailingAddress,
                    WithDriverLicense = item.WithDriverLicense,
                    WithPassport = item.WithPassport

                });
            });

            return modelList;

        }


        public static EighthFormEntity.ServiceOptions ToServiceOptions(this object obj)
        {
            EighthFormEntity.ServiceOptions item = obj as EighthFormEntity.ServiceOptions;

            return (new EighthFormEntity.ServiceOptions
            {
                OperateOption = item.OperateOption,
                WithChequeBook = item.WithChequeBook,
                WithDebitCard = item.WithDebitCard,
                WithDepositBook = item.WithDepositBook,
                WithOnlineAccess = item.WithOnlineAccess,
                WithPhoneAccess = item.WithPhoneAccess
            });

        }
        public static List<FourthFormEntity.Platform> ToPlatform(this object obj)
        {
            List<FourthFormEntity.Platform> modelList = new List<FourthFormEntity.Platform>();
            List<FourthFormEntity.Platform> objList = obj as List<FourthFormEntity.Platform>;

            objList.ForEach(item =>
            {
                modelList.Add(new FourthFormEntity.Platform
                {

                    AdviserNo = item.AdviserNo,
                    InvestorNo = item.InvestorNo,
                    ProviderName = item.ProviderName,
                    WithBTWrap = item.WithBTWrap,
                    WithEclipseSuper = item.WithEclipseSuper,
                    WithMarginLender = item.WithMarginLender
                });
            });

            return modelList;
        }

        public static List<NinethFormEntity.ExistingBrokerDetail> ToExistingBrokerDetail(this object obj)
        {
            List<NinethFormEntity.ExistingBrokerDetail> modelList = new List<NinethFormEntity.ExistingBrokerDetail>();
            List<NinethFormEntity.ExistingBrokerDetail> objList = obj as List<NinethFormEntity.ExistingBrokerDetail>;

            objList.ForEach(item =>
            {
                modelList.Add(new NinethFormEntity.ExistingBrokerDetail
                {
                    ClientHIN = item.ClientHIN,
                    ExistingBrokerName = item.ExistingBrokerName,
                    ExistingBrokerPID = item.ExistingBrokerPID

                });
            });

            return modelList;
        }

        public static DateTime ToDate(this object dt)
        {


            return Convert.ToDateTime(dt); ;

        }

        public static Decimal ToDecimal(this object st)
        {
            decimal final = decimal.Parse(st as string);
            return final;
        }
        public static Boolean ToBool(this object x)
        {
            bool final = Convert.ToBoolean(x);
            return final;
        }


        //public static UMAClientController ToObject(String XMLData)
        //{
        //    XmlSerializer serializer = new XmlSerializer(typeof(UMAClientController));
        //    StringReader textReader = new StringReader(XMLData);

        //    UMAClientController Obj = (UMAClientController)serializer.Deserialize(textReader);
        //    return Obj;
        //}

        public static void ToPDF()
        {

        }

        public static void GetObject(string ApplicationNumber)
        {
            // ConnectionString = String.Empty;
            ////ConnectionString = ConfigurationManager.AppSettings["DBConnectionString"];
            //ConnectionString = "server=localhost;database=Innova2011;uid=sa;pwd=$Admin#1";

            //using (SqlConnection con = new SqlConnection(ConnectionString))
            //{
            //    string Context = "SELECT * FROM dbo.UMA WHERE ApplicationNumber = '" + ApplicationNumber + "'";
            //    using (SqlCommand cmd = new SqlCommand(Context, con))
            //    {
            //        cmd.Connection.Open();

            //        using (SqlDataReader rdr = cmd.ExecuteReader())
            //        {
            //            UMAClientController Client = new UMAClientController();
            //            if (rdr.HasRows)
            //            {
            //                while (rdr.Read())
            //                {
            //                    string xmlData;
            //                    xmlData = (string)rdr[4];
            //                    Client = Utilities.ToObject(xmlData);
            //                } 
            //            }
            //        }
            //        cmd.Connection.Close();
            //    }
            //} 
        }
    }

    public static class Mailer
    {
        public static string Message { get; set; }
        public static string Recipient { get { return _recipient; } set { _recipient = value; } }
        public static string Status { get { return _status; } set { _status = value; } }

        private static string smtpHost = "";//ConfigurationManager.AppSettings["SmtpHost"];
        private static string smtpPort = "";//ConfigurationManager.AppSettings["SmtpPort"];
        private static bool smtpEnableSSL = true;
        private static bool smtpDefaultCredentials = true;
        private static string SenderAddress = "";//ConfigurationManager.AppSettings["SmtpUser"];
        private static string SenderName = "";//ConfigurationManager.AppSettings["SmtpDisplayName"];
        private static string SenderPassword = "";//ConfigurationManager.AppSettings["SmtpPassword"];
        private static string SaveBody = "This is the Body Message on Save";
        private static string SubmitBody = "This is the Body Message on Submit";
        public static string Subject = "Subject";
        private static string _recipient;
        private static string _status;

        public static void SendToEmail()
        {
            MailMessage message = new MailMessage();
            SmtpClient smtpClient = new SmtpClient();
            string msg = string.Empty;
            try
            {
                MailAddress fromAddress = new MailAddress(SenderAddress, SenderName);
                message.From = fromAddress;
                message.To.Add(_recipient);
                message.Subject = Subject;
                message.IsBodyHtml = true;
                if (_status == "SaveAndExit")
                {
                    message.Body = SaveBody;
                }
                else
                {
                    message.Body = SubmitBody;
                }
                smtpClient.Host = smtpHost;
                smtpClient.Port = Convert.ToInt32(smtpPort);
                smtpClient.EnableSsl = smtpEnableSSL;
                smtpClient.UseDefaultCredentials = smtpDefaultCredentials;
                smtpClient.Credentials = new System.Net.NetworkCredential(SenderAddress, SenderPassword);
                smtpClient.Send(message);
            }
            catch
            {

            }
        }
    }

    public static class ApplicationNumberGenerator
    {
        private static char[] allowedChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789`~!@#$%^&*()-_=+[]{}\\|;:'\",<.>/?".ToCharArray();
        private static RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
        private static bool IsRepeatable { get; set; }
        private static bool IsConsecutive { get; set; }
        private static bool HasSymbols { get; set; }
        private static string ExclusionSet { get; set; }
        private static int MinSize = 6;
        private static int MaxSize = 7;

        private const int DefaultMinimum = 6;
        private const int DefaultMaximum = 7;
        private const int UBoundDigit = 61;

        //public ApplicationNumberGenerator()
        //{
        //    IsRepeatable = false;
        //    IsConsecutive = false;
        //    HasSymbols = false;
        //    rng = new RNGCryptoServiceProvider();
        //}

        public static int GetCryptographicRandomNumber(int lBound, int uBound)
        {
            uint urndnum;
            byte[] rndnum = new Byte[4];

            if (lBound == uBound - 1)
            {
                return lBound;
            }

            uint xcludeRndBase = (uint.MaxValue - (uint.MaxValue % (uint)(uBound - lBound)));

            do
            {
                rng.GetBytes(rndnum);
                urndnum = System.BitConverter.ToUInt32(rndnum, 0);
            } while (urndnum >= xcludeRndBase);

            return (int)(urndnum % (uBound - lBound)) + lBound;
        }

        private static char GetRandomCharacter()
        {
            int upperBound = allowedChars.GetUpperBound(0);

            if (!HasSymbols)
            {
                upperBound = UBoundDigit;
            }

            int randomCharPosition = GetCryptographicRandomNumber(allowedChars.GetLowerBound(0), upperBound);

            char randomChar = allowedChars[randomCharPosition];

            return randomChar;
        }

        public static string Generate()
        {
            // Pick random length between minimum and maximum   
            int pwdLength = GetCryptographicRandomNumber(MinSize, MaxSize);

            StringBuilder pwdBuffer = new StringBuilder();
            pwdBuffer.Capacity = MaxSize;

            // Generate random characters
            char lastCharacter, nextCharacter;

            // Initial dummy character flag
            lastCharacter = nextCharacter = '\n';

            for (int i = 0; i < pwdLength; i++)
            {
                nextCharacter = GetRandomCharacter();

                if (!IsConsecutive)
                {
                    while (lastCharacter == nextCharacter)
                    {
                        nextCharacter = GetRandomCharacter();
                    }
                }

                if (!IsRepeatable)
                {
                    string temp = pwdBuffer.ToString();
                    int duplicateIndex = temp.IndexOf(nextCharacter);
                    while (-1 != duplicateIndex)
                    {
                        nextCharacter = GetRandomCharacter();
                        duplicateIndex = temp.IndexOf(nextCharacter);
                    }
                }

                if (ExclusionSet != null)
                {
                    while (-1 != ExclusionSet.IndexOf(nextCharacter))
                    {
                        nextCharacter = GetRandomCharacter();
                    }
                }

                pwdBuffer.Append(nextCharacter);
                lastCharacter = nextCharacter;
            }

            if (null != pwdBuffer)
            {
                return pwdBuffer.ToString();
            }
            else
            {
                return String.Empty;
            }
        }

    }
}