#region CHANGE_HISTORY
/*
 $Header: /2002/3. Implementation/NewConstruction/Components/BusinessStructure/Organization/Organization-1-0/EntityListDS.cs 3     22/08/03  $
 $History: EntityListDS.cs $
 * 
 * *****************  Version 3  *****************
 * User: Secampbell   Date: 22/08/03   Time: 9:02a
 * Updated in $/2002/3. Implementation/NewConstruction/Components/BusinessStructure/Organization/Organization-1-0
 * 
 * *****************  Version 2  *****************
 * User: Secampbell   Date: 3/06/03    Time: 2:22p
 * Updated in $/2002/3. Implementation/Construction/BusinessStructureCM/Organization/Organization-1-0
 * 
 * *****************  Version 3  *****************
 * User: Secampbell   Date: 16/05/03   Time: 2:58p
 * Updated in $/2002/3. Implementation/C2I3 Fixes/BusinessStructureCM/Organization/Organization-1-0
 * Updated OrganisationCM to include User based 'Current Entity'
 * 
 * *****************  Version 1  *****************
 * User: Sshrimpton   Date: 18/12/02   Time: 4:56p
 * Created in $/2002/3. Implementation/Construction/BusinessStructureCM/Organization/Organization-1-1-1
 * 
 * *****************  Version 3  *****************
 * User: Secampbell   Date: 7/11/02    Time: 11:59a
 * Updated in $/2002/3. Implementation/Elaboration4/CM/Organization
 * Added Change History Header
*/
#endregion

using System;
using System.Data;

namespace Oritax.TaxSimp.CM.Organization
{
	/// <summary>
	/// </summary>
	[Serializable] 
	public class EntityListDS : DataSet
	{
		private string strUsername;

        public const String ENTITIES_TABLE = "ENTITIES_TABLE";
        public const String ENTITYNAME_FIELD = "ENTITYNAME_FIELD";
        public const String ENTITYCLID_FIELD = "ENTITYCLID_FIELD";
        public const String ENTITYCSID_FIELD = "ENTITYCSID_FIELD";
        public const String ENTITYCIID_FIELD = "ENTITYCIID_FIELD";
        public const String ENTITYCTID_FIELD = "ENTITYCTID_FIELD";		// Business entity type ID
        public const String ENTITYSCTYPE_FIELD = "ENTITYSCTYPE_FIELD";		// Scenario type
        public const String ENTITYSCSTATUS_FIELD = "ENTITYSCSTATUS_FIELD";	// Scenario status
        public const String ENTITYSORT_FIELD = "ENTITYSORT_FIELD";	// Scenario status

		public string Username
		{
			get{ return( strUsername ); }
			set{ strUsername = value; }
		}

		public EntityListDS()
		{
			DataTable table;

            table = new DataTable(ENTITIES_TABLE);
            table.Columns.Add(ENTITYNAME_FIELD, typeof(System.String));
            table.Columns.Add(ENTITYCLID_FIELD, typeof(System.String));
            table.Columns.Add(ENTITYCSID_FIELD, typeof(System.String));
            table.Columns.Add(ENTITYCIID_FIELD, typeof(System.String));
            table.Columns.Add(ENTITYCTID_FIELD, typeof(System.String));
            table.Columns.Add(ENTITYSCTYPE_FIELD, typeof(System.String));
            table.Columns.Add(ENTITYSCSTATUS_FIELD, typeof(System.String));
            table.Columns.Add(ENTITYSORT_FIELD, typeof(System.Int32));

			this.Tables.Add(table);
		}
	}
}

