﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{
 
    public class GetOrganizationAllUnitDetails : OrganizationWebCommandBase
    {
        public override DataSet DoAction(DataSet value)
        {
            IHasOrganizationUnit ds = value as IHasOrganizationUnit;
            OrganizationListAction.ForEach(ds.Unit.CurrentUser.CurrentUserName, Organization, (csid, logical, bmc) =>
            {
                    bmc.GetData(value);
               
            }, ds.Unit.Name);
            return value;
        }
    }
}
