﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Enum;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{

    public class ValidateDesktopBrokerDataFeedCommand : OrganizationWebCommandBase
    {

        public override DataSet DoAction(DataSet value)
        {
            if (value.ExtendedProperties.Count == 0)
                return value;

            XElement root = XElement.Parse(value.ExtendedProperties["Xml"].ToString(), LoadOptions.None);

            List<string> asxAccounts = new List<string>();

            OrganizationListAction.ForEach("Administrator", OrganizationType.DesktopBrokerAccount, Organization, (csid, logical, bmc) =>
            {
                if (logical.CMType == OrganizationTypeList.GetEntity(OrganizationType.DesktopBrokerAccount))
                {
                    asxAccounts.Add(bmc.GetDataStream((int)CmCommand.AccountNumber, string.Empty));
                }
            });

            DataTable dt = GetValidateData(root, asxAccounts);
            dt.TableName = "ImportMessages";
            value.Tables.Add(dt);
            return value;
        }

        private DataTable GetValidateData(XElement root, List<string> asxAccounts)
        {
            DesktopBrokerMessagesTable messages = new DesktopBrokerMessagesTable();

            XElement[] elements = root.XPathSelectElements("//Accounts//Account").ToArray();

            foreach (var each in elements)
            {
                string accountNumber = each.XPathSelectElement("Id").Value;
                bool isValidAccountNumber = asxAccounts.Contains(accountNumber);
                XElement[] holdes = each.XPathSelectElements("InvestmentHoldings//InvestmentHolding").ToArray();
                foreach (var xElement in holdes)
                {
                    GetMessageData(xElement, messages, isValidAccountNumber, accountNumber);
                }
            }

            return messages;
        }

        private void GetMessageData(XElement investmentHolding, DesktopBrokerMessagesTable messages, bool isAccountValid, string accountnumber)
        {
           string investmentCode = investmentHolding.XPathSelectElement("InvestmentCode").Value;

            XElement[] elements = investmentHolding.XPathSelectElements("HoldingTransactions//Transactions//Transaction").ToArray();

            foreach (var xElement in elements)
            {
                DataRow dr = messages.NewRow();
                dr[DesktopBrokerMessagesTable.Message] = string.Empty;
                dr[DesktopBrokerMessagesTable.HasErrors] = false;
                dr[DesktopBrokerMessagesTable.IsMissingItem] = false;
                CreateMessage(xElement, investmentCode, isAccountValid, accountnumber, dr);
                messages.Rows.Add(dr);
            }
        }

        private void CreateMessage(XElement transaction, string investmentCode, bool isAccountValid, string accountNumber, DataRow dr)
        {
            DateTimeFormatInfo info = new DateTimeFormatInfo { ShortDatePattern = "dd/MM/yyyy" };
            try
            {
                dr[DesktopBrokerMessagesTable.InvestmentCode] = investmentCode;
                dr[DesktopBrokerMessagesTable.TransactionID] = transaction.XPathSelectElement("Id").Value;
                dr[DesktopBrokerMessagesTable.TransactionDate] = transaction.XPathSelectElement("TradeDate").Value;
                dr[DesktopBrokerMessagesTable.AccountNumber] = accountNumber;

                if (Organization.Securities.Count(sec => sec.AsxCode == investmentCode) == 0)
                {
                    dr[DesktopBrokerMessagesTable.Message] += "Investment code does not exist,";
                    dr[DesktopBrokerMessagesTable.IsMissingItem] = true;
                }
               

                if (!isAccountValid)
                {
                    dr[DesktopBrokerMessagesTable.Message] += "Account number does not exists,";
                    dr[DesktopBrokerMessagesTable.HasErrors] = true;
                }


                DateTime date;
                if (!DateTime.TryParse(transaction.XPathSelectElement("TradeDate").Value, info, DateTimeStyles.None, out date))
                {
                    dr[DesktopBrokerMessagesTable.Message] += "Invalid transaction date,";
                    dr[DesktopBrokerMessagesTable.HasErrors] = true;
                }
                else
                {
                    dr[DesktopBrokerMessagesTable.TransactionDate] = date.ToString("dd/MM/yyyy");
                }


                decimal utemp;
                if (!string.IsNullOrEmpty(transaction.XPathSelectElement("Units").Value))
                    if (!decimal.TryParse(transaction.XPathSelectElement("Units").Value, out utemp))
                    {
                        dr[DesktopBrokerMessagesTable.Message] += "Invalid units,";
                        dr[DesktopBrokerMessagesTable.HasErrors] = true;
                    }

                if (!string.IsNullOrEmpty(transaction.XPathSelectElement("GrossValue").Value))
                    if (!decimal.TryParse(transaction.XPathSelectElement("GrossValue").Value, out utemp))
                    {
                        dr[DesktopBrokerMessagesTable.Message] += "Invalid gross value,";
                        dr[DesktopBrokerMessagesTable.HasErrors] = true;
                    }

                if (!string.IsNullOrEmpty(transaction.XPathSelectElement("NetValue").Value))
                    if (!decimal.TryParse(transaction.XPathSelectElement("NetValue").Value, out utemp))
                    {
                        dr[DesktopBrokerMessagesTable.Message] += "Invalid net value,";
                        dr[DesktopBrokerMessagesTable.HasErrors] = true;
                    }

                if (!string.IsNullOrEmpty(transaction.XPathSelectElement("CostBase").Value))
                    if (!decimal.TryParse(transaction.XPathSelectElement("CostBase").Value, out utemp))
                    {
                        dr[DesktopBrokerMessagesTable.Message] += "Invalid cost base,";
                        dr[DesktopBrokerMessagesTable.HasErrors] = true;
                    }

                if (!DateTime.TryParse(transaction.XPathSelectElement("SettlementDate").Value, info, DateTimeStyles.None, out date))
                    if (!string.IsNullOrEmpty(transaction.XPathSelectElement("SettlementDate").Value))
                    {
                        dr[DesktopBrokerMessagesTable.Message] += "Invalid settlement date,";
                        dr[DesktopBrokerMessagesTable.HasErrors] = true;
                    }

                if (!string.IsNullOrEmpty(transaction.XPathSelectElement("Fees//Total").Value))
                    if (!decimal.TryParse(transaction.XPathSelectElement("Fees//Total").Value, out utemp))
                    {
                        dr[DesktopBrokerMessagesTable.Message] += "Invalid fees/total,";
                        dr[DesktopBrokerMessagesTable.HasErrors] = true;
                    }

                if (!string.IsNullOrEmpty(transaction.XPathSelectElement("Brokerage//Amount").Value))
                    if (!decimal.TryParse(transaction.XPathSelectElement("Brokerage//Amount").Value, out utemp))
                    {
                        dr[DesktopBrokerMessagesTable.Message] += "Invalid brokerage amount,";
                        dr[DesktopBrokerMessagesTable.HasErrors] = true;
                    }

                if (!string.IsNullOrEmpty(transaction.XPathSelectElement("Brokerage//GSTOnBrokerage").Value))
                    if (!decimal.TryParse(transaction.XPathSelectElement("Brokerage//GSTOnBrokerage").Value, out utemp))
                    {
                        dr[DesktopBrokerMessagesTable.Message] += "Invalid GST on brokerage,";
                        dr[DesktopBrokerMessagesTable.HasErrors] = true;
                    }

                if (!string.IsNullOrEmpty(dr[DesktopBrokerMessagesTable.Message].ToString()) && dr[DesktopBrokerMessagesTable.Message].ToString().Contains(','))
                {
                    dr[DesktopBrokerMessagesTable.Message] = dr[DesktopBrokerMessagesTable.Message].ToString().Remove(dr[DesktopBrokerMessagesTable.Message].ToString().LastIndexOf(','));
                }
            }
            catch
            {
            }
        }

      
    }

    public class DesktopBrokerMessagesTable : DataTable
    {
        public const string Name = "DesktopBrokerTable";
        public const string InvestmentCode = "InvestmentCode";
        public const string TransactionDate = "TransactionDate";
        public const string TransactionID = "TransactionID";
        public const string AccountNumber = "AccountNumber";
        public const string Message = "Message";
        public new const string HasErrors = "HasErrors";
        public const string IsMissingItem = "IsMissingItem";

        public DesktopBrokerMessagesTable()
        {
            TableName = Name;
            Columns.Add(InvestmentCode);
            Columns.Add(TransactionID);
            Columns.Add(TransactionDate);
            Columns.Add(AccountNumber);
            DataColumn messageCol = Columns.Add(Message);
            messageCol.DefaultValue = "Investment Code not found";
            DataColumn errorsCol = Columns.Add(HasErrors, typeof(bool));
            errorsCol.DefaultValue = true;
            DataColumn missingCol = Columns.Add(IsMissingItem, typeof(bool));
            missingCol.DefaultValue = true;
        }
    }
}
