﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{

    public class ImportClientDristibutionsCommand : OrganizationWebCommandBase
    {

        public override System.Data.DataSet DoAction(System.Data.DataSet value)
        {
           if(value.Tables.Count==0)
               return value;
            
            if(!value.Tables[0].Columns.Contains("Message"))
            {
                //set message to not found
                DataColumn dc=new DataColumn("Message");
                dc.DefaultValue = "Account Number Found";
                value.Tables[0].Columns.Add(dc);
            }

            Organization.Broker.SetStart();


            OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
            {

                if (bmc is OrganizationUnitCM && OrganizationListAction.IsValidClientType(logical.CMTypeName))
                {
                    var unit = (bmc as OrganizationUnitCM);
                    if (unit.ClientId != null)
                    {
                     DataRow[] drs=  value.Tables[0].Select("ACCOUNTNUMBER='" + unit.ClientId + "'");
                     if (drs != null)
                     {
                         if (drs.Length > 0)
                         {
                             //set message to found
                             foreach (var dr in drs)
                             {
                                 dr["Message"] = "";
                             }
                             unit.ImportDistributions(drs);
                         }
                     }
                     
                    }
                }


            });

            return value;
        }



        



     







       
    }
}
