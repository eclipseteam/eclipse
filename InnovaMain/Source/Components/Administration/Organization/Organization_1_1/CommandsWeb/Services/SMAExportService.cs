﻿using System;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using Oritax.TaxSimp.CM.Organization.CommandsWeb.Services.Encryption;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Utilities;
namespace Oritax.TaxSimp.CM.Organization.CommandsWeb.Services
{
    public class SMAExportService
    {
        private string LiveURL = "https://api.e-clipsesuper.com.au:10443/Trade/SubmitTradeRequest";
        private string TestURL = "https://api.e-clipsesuper.com.au:11443/Trade/SubmitTradeRequest";
        private static string UserName = "e-clipse";
        private static string Password = "Qx$j7x?9#;^|z342&i0sAtH0++";

        public string Url
        {
            get
            {
                return ((DBConnection.IsTestMode ? TestURL : LiveURL));
            }
        }

        public SubmitTradeResultViewModel SendData(string datatosubmit, string requestID)
        {

            var data = EcryptionUtil.EncryptData(datatosubmit);
            // ENCRYPTION NOTE!
            // You would PGP encrypt the dataToSubmit string, and then operate on the encrypted data from this point onwards.

            // Convert to a raw byte array and then encode as a Base64 string
            Byte[] byteData = Encoding.ASCII.GetBytes(data);
            String base64Data = Convert.ToBase64String(byteData);
            const string format = "xml";
            // Create a SHA1 object and hash the raw byte array
            SHA1 sha = new SHA1Managed();
            Byte[] rawHashValue = sha.ComputeHash(byteData);

            // Convert the hash array to a string
            String hashData = BitConverter.ToString(rawHashValue).Replace("-", String.Empty).ToUpper();

            // Construct the post data
            StringBuilder post = new StringBuilder();
            post.AppendFormat("requestID={0}", requestID);
            post.AppendFormat("&fileSize={0}", byteData.Length);
            post.AppendFormat("&fileData={0}", base64Data);
            post.AppendFormat("&fileHash={0}", hashData);
            post.AppendFormat("&format={0}", format);
            Byte[] postData = Encoding.ASCII.GetBytes(post.ToString());

            return SendDatatoWebService(this.Url, postData);
        }

        private static SubmitTradeResultViewModel SendDatatoWebService(string url, Byte[] postData)
        {

            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
            webRequest.Headers.Add("SOAPAction", "\"http://tempuri.org/Register\"");
            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.ContentLength = postData.Length;
            webRequest.Accept = "text/xml";
            webRequest.Method = "POST";
            webRequest.UseDefaultCredentials = true;
            webRequest.PreAuthenticate = true;
            webRequest.Credentials = new NetworkCredential(UserName, Password);
            webRequest.ContentLength = postData.Length;
            ServicePointManager.ServerCertificateValidationCallback += new RemoteCertificateValidationCallback(ValidateRemoteCertificate);
            using (Stream sendStream = webRequest.GetRequestStream())
            {
                sendStream.Write(postData, 0, postData.Length);
            }
            // Read The Response
            var webResponse = (HttpWebResponse)webRequest.GetResponse();
            SubmitTradeResultViewModel response = null;
            using (StreamReader recieveStream = new StreamReader(webResponse.GetResponseStream()))
            {
                var xml = recieveStream.ReadToEnd();
                response = xml.ToData<SubmitTradeResultViewModel>();
            }

            return response;
        }

        private static bool ValidateRemoteCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslpolicyerrors)
        {
            bool result = false;
            if (certificate.Subject.ToUpper().Contains("API.E-CLIPSESUPER.COM.AU"))
            {
                result = true;
            }
            return result;
        }
    }
}
