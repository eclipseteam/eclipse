﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb.Services.TradeRequestEntities
{
    public class TermDeposit
    {
        public String Payee { get; set; }
        public Decimal Rate { get; set; }
        public DateTime EffectiveDate { get; set; }
        public Int32 PeriodDays { get; set; }
        public Decimal TotalAmount { get; set; }
        public List<TermDepositMember> Members { get; private set; }

        public TermDeposit()
        {
            this.Members = new List<TermDepositMember>();
        }

        public void AddMember(String memberNumber, Decimal amount)
        {
            this.Members.Add(new TermDepositMember(memberNumber, amount));
        }

        public XElement ToXML()
        {
            XElement term = new XElement("TermDeposit");
            term.Add(new XElement("Payee", this.Payee));
            term.Add(new XElement("Rate", this.Rate));
            term.Add(new XElement("Effective", this.EffectiveDate.Date));
            term.Add(new XElement("PeriodDays", this.PeriodDays));
            term.Add(new XElement("TotalAmount", this.TotalAmount));
            term.Add(new XElement("MemberCount", this.Members.Count));

            XElement members = new XElement("Members");
            foreach (TermDepositMember curMember in this.Members)
            {
                XElement member = new XElement("Member");
                member.Add(new XElement("MemberNumber", curMember.MemberNumber));
                member.Add(new XElement("Amount", curMember.Amount));
                members.Add(member);
            }

            term.Add(members);

            return term;
        }

    }
}
