﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.IO;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb.Services.TradeRequestEntities
{
    public class TradeRequest
    {
        public String RequestID { get; set; }
        public List<ASXTrade> ASXTrades { get; private set; }
        public List<TermDeposit> TermDeposits { get; private set; }

        public TradeRequest()
        {
            this.ASXTrades = new List<ASXTrade>();
            this.TermDeposits = new List<TermDeposit>();
        }

        public XDocument ToXDocument()
        {
            XElement tradeRequest = new XElement("TradeRequest");

            XElement asxTrades = new XElement("AsxTrades");
            foreach (ASXTrade curTrade in this.ASXTrades)
            {
                asxTrades.Add(curTrade.ToXML());
            }

            XElement termDeposits = new XElement("TermDeposits");
            foreach (TermDeposit curTerm in this.TermDeposits)
            {
                termDeposits.Add(curTerm.ToXML());
            }

            tradeRequest.Add(new XElement("RequestID", this.RequestID));
            tradeRequest.Add(new XElement("AsxTradeCount", this.ASXTrades.Count));
            tradeRequest.Add(asxTrades);
            tradeRequest.Add(new XElement("TermDepositCount", this.TermDeposits.Count));
            tradeRequest.Add(termDeposits);

            XDocument result = new XDocument(new XDeclaration("1.0", "utf-8", "yes"), tradeRequest);
            return result;
        }

        public override string ToString()
        {
            XDocument xml = this.ToXDocument();
            StringBuilder xmlOutput = new StringBuilder();
            using (TextWriter xmlWriter = new StringWriter(xmlOutput))
            {
                xml.Save(xmlWriter);
            }

            return xmlOutput.ToString();
        }
    }
}
