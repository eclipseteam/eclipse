﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Net;
using System.IO;
using System.Web;
using System.Data;
using System.Xml;
using Oritax.TaxSimp.Common;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{
    public class MaquarieBankWebService
    {
        public string Url { get; set; }
        public string MethodName { get; set; }
        public Dictionary<string, string> Params = new Dictionary<string, string>();
        public XDocument ResultXML;
        public string ResultString;
        public bool IsSuccess = false;
        public string MessageNumber = String.Empty;
        public string MessageSummary = String.Empty;
        public string MessageDetail = String.Empty;
        public DataTable ResultTable = new DataTable();
        public string ResultXMLString = String.Empty;
        public DateTime EndDate = DateTime.Now;

        public MaquarieBankWebService()
        {

        }

        public MaquarieBankWebService(string url, string methodName)
        {
            Url = url;
            MethodName = methodName;
        }

        /// <summary>
        /// Invokes service
        /// </summary>
        public void InvokeMacquarieBankService()
        {
            InvokeMacquarieBankService(true);
        }

        /// <summary>
        /// InvokeMacquarieBankService
        /// </summary>
        /// <param name="encode">Added parameters will encode? (default: true)</param>
        public void InvokeMacquarieBankService(bool encode)
        {
            string endDate = EndDate.ToString("yyyy-MM-dd");
            string startDate = (EndDate.AddDays(-2)).ToString("yyyy-MM-dd");

            string soapStr = @"<SOAP-ENV:Envelope xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:SOAP-ENC=""http://schemas.xmlsoap.org/soap/encoding/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema""><SOAP-ENV:Body><m:GenerateXMLExtract xmlns:m=""wrapwebservices"" SOAP-ENV:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/""><string xsi:type=""xsd:string"">yEwNb23ORwGoNu39+fxukXiD3D4=</string><string0 xsi:type=""xsd:string"">E-CLIPSE V1.0</string0><string1 xsi:type=""xsd:string"">Q6n8SfSm9Y2tfPAe7RErDD6Tltc=</string1><string2 xsi:type=""xsd:string"">vpot5LSKsDTKHU4x4OQfCwprDJk=</string2><string3 xsi:type=""xsd:string"">your.clients Transactions</string3><string4 xsi:type=""xsd:string"">V1.3</string4><strings xsi:type=""SOAP-ENC:Array"" SOAP-ENC:arrayType=""xsd:string[4]""><item0></item0><item1>" + startDate + "</item1><item2>" + endDate + "</item2><item3>Y</item3></strings></m:GenerateXMLExtract></SOAP-ENV:Body></SOAP-ENV:Envelope>";

            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(Url);
            req.Headers.Add("SOAPAction", "\"http://tempuri.org/" + "GenerateXMLExtract" + "\"");
            req.ContentType = "text/xml;charset=\"utf-8\"";
            req.Accept = "text/xml";
            req.Method = "POST";
            using (Stream stm = req.GetRequestStream())
            {
                string postValues = "";
                foreach (var param in Params)
                {
                    if (encode)
                        postValues += string.Format("<{0}>{1}</{0}>", HttpUtility.UrlEncode(param.Key), HttpUtility.UrlEncode(param.Value));
                    else
                        postValues += string.Format("<{0}>{1}</{0}>", param.Key, param.Value);
                }
                using (StreamWriter stmw = new StreamWriter(stm))
                {
                    stmw.Write(soapStr);
                }
            }

            using (StreamReader responseReader = new StreamReader(req.GetResponse().GetResponseStream()))
            {
                string result = responseReader.ReadToEnd();
                ResultXML = XDocument.Parse(result);
                ResultString = result;

                XNamespace soapNameSpace = "wrapwebservices";
                XElement requestResult = (from xmlNodeRequestResult in ResultXML.Descendants("result") select xmlNodeRequestResult).FirstOrDefault();
                ResultXML = XDocument.Parse(requestResult.Value);
                DataSet resultDataSet = new DataSet();
                XmlReader reader = ResultXML.CreateReader();
                resultDataSet.ReadXml(reader);

                if (resultDataSet.Tables["RequestDetails"] != null && resultDataSet.Tables["RequestDetails"].Rows.Count > 0)
                {
                    string requestStatus = resultDataSet.Tables["RequestDetails"].Rows[0]["RequestStatus"].ToString();

                    if (requestStatus == "Success")
                    {
                        this.IsSuccess = true;
                        this.MessageNumber = "e-Clipse - Maq03";
                        this.MessageSummary = "Successful";
                        this.MessageDetail = "Successful and data found";
                        //Import Transaction
                        this.ResultTable = resultDataSet.Tables["yourclientsTransaction"];
                        this.ResultXMLString = requestResult.Value;
                    }
                    else
                    {
                        this.IsSuccess = false;

                        string requestErrorCode = resultDataSet.Tables["RequestDetails"].Rows[0]["RequestErrorNumber"].ToString();
                        var errorMessageObj = MacBankErrorMsgList.ErrorList.Where(errorObj => errorObj.ErrorCode == requestErrorCode).FirstOrDefault();

                        if (errorMessageObj != null)
                        {
                            this.MessageNumber = errorMessageObj.ErrorCode;
                            this.MessageSummary = errorMessageObj.ErrorMessage;
                            this.MessageDetail = errorMessageObj.ErrorMessageDetail;

                        }
                        else
                        {
                            this.MessageNumber = "e-Clipse - Maq02";
                            this.MessageSummary = "Response recieved with fatal results.";
                            this.MessageDetail = "Response recieved with fatal results.";
                        }
                    }
                }
                else
                {
                    this.IsSuccess = false;
                    this.MessageNumber = "e-Clipse - Maq01";
                    this.MessageSummary = "DataSet is Null.";
                    this.MessageDetail = "This means service did not return anything. Not even error a response";
                }
            }
        }
    }
}
