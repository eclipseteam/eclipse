﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace eclipse.SMA.Tools.TradeRequestWriter
{
    public class TradeRequest
    {
        public String RequestID { get; set; }
        public List<ASXTrade> ASXTrades { get; private set; }
        public List<TermDeposit> TermDeposits { get; private set; }

        public TradeRequest()
        {
            this.ASXTrades = new List<ASXTrade>();
            this.TermDeposits = new List<TermDeposit>();
        }

        public XDocument ToXDocument()
        {
            XElement tradeRequest = new XElement("TradeRequest");

            XElement asxTrades = new XElement("AsxTrades");
            foreach (ASXTrade curTrade in this.ASXTrades)
            {
                asxTrades.Add(curTrade.ToXML());
            }

            XElement termDeposits = new XElement("TermDeposits");
            foreach (TermDeposit curTerm in this.TermDeposits)
            {
                termDeposits.Add(curTerm.ToXML());
            }

            tradeRequest.Add(new XElement("RequestID", this.RequestID));
            tradeRequest.Add(new XElement("AsxTradeCount", this.ASXTrades.Count));
            tradeRequest.Add(asxTrades);
            tradeRequest.Add(new XElement("TermDepositCount", this.TermDeposits.Count));
            tradeRequest.Add(termDeposits);

            XDocument result = new XDocument(new XDeclaration("1.0", "utf-8", "yes"), tradeRequest);
            return result;
        }

        public override string ToString()
        {
            XDocument xml = this.ToXDocument();
            StringBuilder xmlOutput = new StringBuilder();
            using (TextWriter xmlWriter = new StringWriter(xmlOutput))
            {
                xml.Save(xmlWriter);
            }

            return xmlOutput.ToString();
        }
    }

    public class ASXTrade
    {
        public enum ActionType
        {
            Buy,
            Sell
        }

        private Decimal? _amount = null;
        private Decimal? _units = null;

        public String MemberNumber { get; set; }
        public DateTime EffectiveDate { get; set; }
        public ActionType Action { get; set; }
        public String Code { get; set; }
        public Decimal? Amount
        {
            get
            {
                return this._amount;
            }

            set
            {
                if (value != null)
                {
                    this._units = null;
                }

                this._amount = value;
            }
        }
        public Decimal? Units
        {
            get
            {
                return this._units;
            }

            set
            {
                if (value != null)
                {
                    this._amount = null;
                }

                this._units = value;
            }
        }

        public XElement ToXML()
        {
            XElement trade = new XElement("AsxTrade");
            trade.Add(new XElement("MemberNumber", this.MemberNumber));
            trade.Add(new XElement("Effective", this.EffectiveDate.Date));
            trade.Add(new XElement("Action", this.Action.ToString()));
            trade.Add(new XElement("Code", this.Code));
            trade.Add(new XElement("Amount", this.Amount));
            trade.Add(new XElement("Units", this.Units));

            return trade;
        }
    }

    public class TermDepositMember
    {
        public String MemberNumber { get; set; }
        public Decimal Amount { get; set; }

        public TermDepositMember()
        {
        }

        public TermDepositMember(String memberNumber, Decimal amount)
        {
            this.MemberNumber = memberNumber;
            this.Amount = amount;
        }
    }

    public class TermDeposit
    {
        public String Payee { get; set; }
        public Decimal Rate { get; set; }
        public DateTime EffectiveDate { get; set; }
        public Int32 PeriodDays { get; set; }
        public Decimal TotalAmount { get; set; }
        public List<TermDepositMember> Members { get; private set; }

        public TermDeposit()
        {
            this.Members = new List<TermDepositMember>();
        }

        public void AddMember(String memberNumber, Decimal amount)
        {
            this.Members.Add(new TermDepositMember(memberNumber, amount));
        }

        public XElement ToXML()
        {
            XElement term = new XElement("TermDeposit");
            term.Add(new XElement("Payee", this.Payee));
            term.Add(new XElement("Rate", this.Rate));
            term.Add(new XElement("Effective", this.EffectiveDate.Date));
            term.Add(new XElement("PeriodDays", this.PeriodDays));
            term.Add(new XElement("TotalAmount", this.TotalAmount));
            term.Add(new XElement("MemberCount", this.Members.Count));

            XElement members = new XElement("Members");
            foreach (TermDepositMember curMember in this.Members)
            {
                XElement member = new XElement("Member");
                member.Add(new XElement("MemberNumber", curMember.MemberNumber));
                member.Add(new XElement("Amount", curMember.Amount));
                members.Add(member);
            }

            term.Add(members);

            return term;
        }

    }
}
