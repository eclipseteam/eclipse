﻿using Org.BouncyCastle.Bcpg.OpenPgp;
using Org.BouncyCastle.Bcpg;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.Utilities.IO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace SMA.Tools.PGP
{
    public class PgpDecrypt
    {
        #region Private Variables
        private PgpEncryptionKeys mEncryptionKeys;
        private const int bufferSize = 0x10000; // should always be power of 2 

        #endregion

        #region Public Methods
        public PgpDecrypt(PgpEncryptionKeys encryptionKeys)
        {
            if (encryptionKeys == null)
            {
                throw new ArgumentNullException("encryptionKeys", "encryptionKeys is null.");
            }

            mEncryptionKeys = encryptionKeys;
        }

        public void DecryptAndVerify(Stream inputStream, string outputFile)
        {
            if (inputStream == null)
            {
                throw new ArgumentNullException("inputStream", "inputStream is null.");
            }
            if (outputFile == null)
            {
                throw new ArgumentNullException("outputFile", "outputFile is null.");
            }

            decryptAndVerify(inputStream, outputFile);
        }

        #endregion

        #region Private Methods
        private void decryptAndVerify(Stream inputStream, string outputFilePath)
        {
            using (Stream armouredIn = new ArmoredInputStream(inputStream))
            {
                PgpPublicKeyEncryptedData publicKeyED = extractPublicKeyEncryptedData(inputStream, mEncryptionKeys.PrivateKey.KeyId);
                PgpObject message = getClearCompressedMessage(publicKeyED);

                if (message is PgpCompressedData)
                {
                    message = processCompressedMessage(message);
                    PgpLiteralData literalData = (PgpLiteralData)message;
                    using (Stream outputFile = File.Create(outputFilePath))
                    {
                        using (Stream literalDataStream = literalData.GetInputStream())
                        {
                            Streams.PipeAll(literalDataStream, outputFile);
                        }
                    }
                }
            }

            return;
        }

        private static PgpObject processCompressedMessage(PgpObject message)
        {
            PgpCompressedData compressedData = (PgpCompressedData)message;
            Stream compressedDataStream = compressedData.GetDataStream();
            PgpObjectFactory compressedFactory = new PgpObjectFactory(compressedDataStream);
            message = checkforOnePassSignatureList(message, compressedFactory);
            return message;
        }

        private PgpObject getClearCompressedMessage(PgpPublicKeyEncryptedData publicKeyED)
        {
            PgpObjectFactory clearFactory = getClearDataStream(mEncryptionKeys.PrivateKey, publicKeyED);
            PgpObject message = clearFactory.NextPgpObject();
            return message;
        }

        private static PgpPublicKeyEncryptedData extractPublicKeyEncryptedData(Stream inputStream, Int64 privateKeyID)
        {
            Stream encodedFile = PgpUtilities.GetDecoderStream(inputStream);
            PgpEncryptedDataList encryptedDataList = getEncryptedDataList(encodedFile);
            PgpPublicKeyEncryptedData publicKeyED = extractPublicKey(encryptedDataList, privateKeyID);
            return publicKeyED;
        }

        private static PgpObject checkforOnePassSignatureList(PgpObject message, PgpObjectFactory compressedFactory)
        {
            message = compressedFactory.NextPgpObject();
            if (message is PgpOnePassSignatureList)
            {
                message = compressedFactory.NextPgpObject();
            }
            return message;
        }

        private static PgpObjectFactory getClearDataStream(PgpPrivateKey privateKey, PgpPublicKeyEncryptedData publicKeyED)
        {
            Stream clearStream = publicKeyED.GetDataStream(privateKey);
            PgpObjectFactory clearFactory = new PgpObjectFactory(clearStream);
            return clearFactory;
        }

        private static PgpPublicKeyEncryptedData extractPublicKey(PgpEncryptedDataList encryptedDataList, Int64 privateKeyID)
        {
            IEnumerable<PgpPublicKeyEncryptedData> publicKeyList = encryptedDataList.GetEncryptedDataObjects().Cast<PgpPublicKeyEncryptedData>();

            PgpPublicKeyEncryptedData publicKeyED = (from pk in publicKeyList
                                                     where pk.KeyId == privateKeyID
                                                     select pk).SingleOrDefault();

            return publicKeyED;
        }

        private static PgpEncryptedDataList getEncryptedDataList(Stream encodedFile)
        {
            PgpObjectFactory factory = new PgpObjectFactory(encodedFile);
            PgpObject pgpObject = factory.NextPgpObject();

            PgpEncryptedDataList encryptedDataList;

            if (pgpObject is PgpEncryptedDataList)
            {
                encryptedDataList = (PgpEncryptedDataList)pgpObject;
            }
            else
            {
                encryptedDataList = (PgpEncryptedDataList)factory.NextPgpObject();
            }
            return encryptedDataList;
        }
        #endregion

    }
}
