﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb.Services.TradeRequestEntities
{
    public class TermDepositMember
    {
        public String MemberNumber { get; set; }
        public Decimal Amount { get; set; }

        public TermDepositMember()
        {
        }

        public TermDepositMember(String memberNumber, Decimal amount)
        {
            this.MemberNumber = memberNumber;
            this.Amount = amount;
        }
    }
}
