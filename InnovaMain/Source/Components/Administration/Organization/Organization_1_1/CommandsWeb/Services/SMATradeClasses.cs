﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb.Services
{
    public enum TradeResultStatus
    {
        Success,
        SuccessWithErrors,
        Failed
    }
  

    [Serializable]
    public class SubmitTradeResultViewModel
    {
        public TradeResultStatus Status { get; set; }
        public Boolean TestMode { get; set; }
        public String RequestID { get; set; }

        public Int32 ErrorCount { get; set; }
        public List<ErrorInformation> Errors { get; set; }

        public Int32 TradeCount { get; set; }
        public List<TradeViewModel> Trades { get; set; }
    }
    public enum TradeType
    {
        Buy,
        Sell
    }

    [Serializable]
    public class TradeViewModel
    {
        public TradeType TradeType { get; set; }
        public String FundCode { get; set; }
        public String MemberNumber { get; set; }
        public DateTime EffectiveDate { get; set; }
        public String InvestmentManagerCode { get; set; }
        public Decimal? DollarAmount { get; set; }
        public Decimal? UnitsShares { get; set; }
        public String TransactionReference { get; set; }
        public String ShareTrade { get; set; }

        public TradeResultStatus Status { get; set; }
        public String ErrorMessage { get; set; }
    }
    [Serializable]
    public class ErrorInformation
    {
        public String ErrorCode { get; set; }
        public String ErrorMessage { get; set; }
        public Int32 LineNumber { get; set; }
    }

}
