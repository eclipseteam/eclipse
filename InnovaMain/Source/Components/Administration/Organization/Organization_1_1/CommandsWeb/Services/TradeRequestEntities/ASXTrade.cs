﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb.Services.TradeRequestEntities
{
    public class ASXTrade
    {
        public enum ActionType
        {
            Buy,
            Sell
        }

        private Decimal? _amount = null;
        private Decimal? _units = null;

        public String MemberNumber { get; set; }
        public DateTime EffectiveDate { get; set; }
        public ActionType Action { get; set; }
        public String Code { get; set; }
        public Decimal? Amount
        {
            get
            {
                return this._amount;
            }

            set
            {
                if (value != null)
                {
                    this._units = null;
                }

                this._amount = value;
            }
        }
        public Decimal? Units
        {
            get
            {
                return this._units;
            }

            set
            {
                if (value != null)
                {
                    this._amount = null;
                }

                this._units = value;
            }
        }

        public XElement ToXML()
        {
            XElement trade = new XElement("AsxTrade");
            trade.Add(new XElement("MemberNumber", this.MemberNumber));
            trade.Add(new XElement("Effective", this.EffectiveDate.Date));
            trade.Add(new XElement("Action", this.Action.ToString()));
            trade.Add(new XElement("Code", this.Code));
            trade.Add(new XElement("Amount", this.Amount));
            trade.Add(new XElement("Units", this.Units));

            return trade;
        }
    }
}
