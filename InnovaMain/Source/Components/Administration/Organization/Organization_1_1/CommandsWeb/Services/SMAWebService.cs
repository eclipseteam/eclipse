﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Net;
using System.IO;
using System.Data;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using Oritax.TaxSimp.Utilities;
using System.Linq;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{
    public class SMAWebService
    {
        private string LiveURL = "https://api.e-clipsesuper.com.au:10443";
        private string TestURL = "https://api.e-clipsesuper.com.au:11443";
        private static string UserName = "e-clipse";
        private static string Password = "Qx$j7x?9#;^|z342&i0sAtH0++";

        public string Url
        {
            get
            {
                return ((DBConnection.IsTestMode ? TestURL : LiveURL));
            }
        }

        public Dictionary<string, string> Params = new Dictionary<string, string>();
        public XDocument ResultXML;
        public string ResultString;
        public bool IsSuccess = false;
        public string MessageNumber = String.Empty;
        public string MessageSummary = String.Empty;
        public string MessageDetail = String.Empty;
        public DataSet ResultDataSet = new DataSet();
        public string ResultXMLString = String.Empty;

        public SMAWebService()
        {
        }

        /// <summary>
        /// Invokes service
        /// </summary>
        public void InvokeService()
        {
        }

        public void InvokeServiceMembersDetails(string memberCode)
        {
            string membersDetailsSoap = string.Empty;
            HttpWebRequest membersDetailsRequest = GetWebServiceRequest(string.Format("{0}/member/SearchMembersByID?memberid={1}&format=xml", this.Url, memberCode));
            membersDetailsRequest.Timeout = 10000000;

            using (Stream membersDetailsStm = membersDetailsRequest.GetRequestStream())
            {
                using (StreamWriter membersDetailsStmw = new StreamWriter(membersDetailsStm))
                {
                    membersDetailsStmw.Write(membersDetailsSoap);
                }
            }

            WebResponse response = membersDetailsRequest.GetResponse();
            Stream responseStream = response.GetResponseStream();
            DataSet membersDetailsDS = new DataSet();
            membersDetailsDS.ReadXml(responseStream);

            ResultDataSet.Merge(membersDetailsDS);
        }

        public DataSet InvokeServiceMemberList(bool encode)
        {
            ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(CertValidationUntrusted); ;

            string getMemberListSoap = string.Empty;
            var getMemberListRequest = GetWebServiceRequest(string.Format("{0}/Member/searchMembers?brokerID=&surname=&givenNames=&includeExited=true&format=xml", this.Url));

            using (Stream getMemberListStm = getMemberListRequest.GetRequestStream())
            {
                using (StreamWriter getMemberListStmw = new StreamWriter(getMemberListStm))
                {
                    getMemberListStmw.Write(getMemberListSoap);
                }
            }
            WebResponse getMemberListResponse = getMemberListRequest.GetResponse();
            Stream getMemberListResponseStream = getMemberListResponse.GetResponseStream();
            DataSet getMemberListDS = new DataSet();
            getMemberListDS.ReadXml(getMemberListResponseStream);

            return getMemberListDS;
        }

        public DataSet InvokeServiceMemberList(bool encode, bool includeExited)
        {
            ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(CertValidationUntrusted); ;

            string getMemberListSoap = string.Empty;
            var getMemberListRequest = GetWebServiceRequest(string.Format("{0}/Member/searchMembers?brokerID=&surname=&givenNames=&includeExited=" + includeExited .ToString()+ "&format=xml", this.Url));

            using (Stream getMemberListStm = getMemberListRequest.GetRequestStream())
            {
                using (StreamWriter getMemberListStmw = new StreamWriter(getMemberListStm))
                {
                    getMemberListStmw.Write(getMemberListSoap);
                }
            }
            WebResponse getMemberListResponse = getMemberListRequest.GetResponse();
            Stream getMemberListResponseStream = getMemberListResponse.GetResponseStream();
            DataSet getMemberListDS = new DataSet();
            getMemberListDS.ReadXml(getMemberListResponseStream);

            return getMemberListDS;
        }

        bool CertValidation(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslpolicyErrors)
        {
            bool result = false;
            // TODO: implement
            return result;
        }

        bool CertValidationUntrusted(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslpolicyErrors)
        {
            bool result = false;
            if ((chain != null) && (chain.ChainStatus.Length == 1) &&
                   (chain.ChainStatus[0].Status == X509ChainStatusFlags.UntrustedRoot))
                result = true;
            return result ? result : sslpolicyErrors == SslPolicyErrors.None;
        }

        public DataSet InvokeServiceAdviserList(bool encode)
        {
            ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(CertValidationUntrusted);

            string getAdviserListSoap = string.Empty;
            var getAdviserListRequest = GetWebServiceRequest(string.Format("{0}/Adviser/getAdviserList", this.Url));

            using (Stream getAdviserListStm = getAdviserListRequest.GetRequestStream())
            {
                using (StreamWriter getAdviserListStmw = new StreamWriter(getAdviserListStm))
                {
                    getAdviserListStmw.Write(getAdviserListSoap);
                }
            }
            WebResponse getAdviserListResponse = getAdviserListRequest.GetResponse();
            Stream getAdviserListResponseStream = getAdviserListResponse.GetResponseStream();
            DataSet getAdviserListDS = new DataSet();
            getAdviserListDS.ReadXml(getAdviserListResponseStream);

            return getAdviserListDS;
        }

        private static HttpWebRequest GetWebServiceRequest(string url)
        {
            HttpWebRequest getMemberListRequest = (HttpWebRequest)WebRequest.Create(url);
            getMemberListRequest.Headers.Add("SOAPAction", "\"http://tempuri.org/Register\"");
            getMemberListRequest.ContentType = "text/xml;charset=\"utf-8\"";
            getMemberListRequest.Accept = "text/xml";
            getMemberListRequest.Method = "POST";
            getMemberListRequest.UseDefaultCredentials = true;
            getMemberListRequest.PreAuthenticate = true;
            getMemberListRequest.Credentials = new NetworkCredential(UserName, Password);
            return getMemberListRequest;
        }

        public void InvokeServiceMembersDetails(string memberCode, DataTable resultTable)
        {
            string membersDetailsSoap = string.Empty;
            HttpWebRequest membersDetailsRequest = GetWebServiceRequest(string.Format("{0}/member/SearchMembersByID?memberid={1}&format=xml", this.Url, memberCode));
            membersDetailsRequest.Timeout = 10000000;

            using (Stream membersDetailsStm = membersDetailsRequest.GetRequestStream())
            {
                using (StreamWriter membersDetailsStmw = new StreamWriter(membersDetailsStm))
                {
                    membersDetailsStmw.Write(membersDetailsSoap);
                }
            }

            WebResponse response = membersDetailsRequest.GetResponse();
            Stream responseStream = response.GetResponseStream();
            DataSet membersDetailsDS = new DataSet();
            membersDetailsDS.ReadXml(responseStream);

            ResultDataSet.Merge(membersDetailsDS);
        }

        /// <summary>
        /// InvokeMacquarieBankService
        /// </summary>
        /// <param name="encode">Added parameters will encode? (default: true)</param>
        public void InvokeServiceMemberDataTransactions(string memberCode, DataTable resultTable, DateTime startDate, DateTime endDate)
        {
            ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(CertValidationUntrusted);

            string getMemberHoldingSoap = string.Empty;

            HttpWebRequest getMemberHoldingRequest = GetWebServiceRequest(string.Format("{0}/Member/getMemberPortfolioHoldings?memberid={1}&format=xml", this.Url, memberCode));

            using (Stream getMemberHoldingStm = getMemberHoldingRequest.GetRequestStream())
            {
                using (StreamWriter getMemberHoldingStmw = new StreamWriter(getMemberHoldingStm))
                {
                    getMemberHoldingStmw.Write(getMemberHoldingSoap);
                }
            }

            WebResponse getMemberHoldingResponse = getMemberHoldingRequest.GetResponse();
            Stream getMemberHoldingResponseStream = getMemberHoldingResponse.GetResponseStream();
            DataSet getMemberHoldingDS = new DataSet();
            getMemberHoldingDS.ReadXml(getMemberHoldingResponseStream);

            if (getMemberHoldingDS.Tables["InvestmentViewModel"] != null && getMemberHoldingDS.Tables["InvestmentViewModel"].Rows.Count > 0)
            {
                var equityRows = getMemberHoldingDS.Tables["InvestmentViewModel"].Select().Where(row => row["InvestmentClass"].ToString().ToLower() == "equity");
                foreach (DataRow equityRow in equityRows)
                {
                    string assetCode = equityRow["LongAssetCode"].ToString();
                    GetTransactionsByAssetCode(memberCode, startDate, endDate, assetCode, true);
                }
            }

            string getMemberCashTransactionsSoap = string.Empty;
            HttpWebRequest getMemberCashTransactionsRequest = GetWebServiceRequest(string.Format("{0}/member/GetMemberCashTransactions?memberid={1}&startDate={2}&endDate={3}&format=xml", this.Url, memberCode, DateTime.Now.AddYears(-5).ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd")));
            getMemberCashTransactionsRequest.Timeout = 10000000;

            using (Stream getMemberCashTransactionsStm = getMemberCashTransactionsRequest.GetRequestStream())
            {
                using (StreamWriter getMemberCashTransactionsStmw = new StreamWriter(getMemberCashTransactionsStm))
                {
                    getMemberCashTransactionsStmw.Write(getMemberCashTransactionsSoap);
                }
            }

            WebResponse getMemberCashTransactionsResponse = getMemberCashTransactionsRequest.GetResponse();
            Stream getMemberCashTransactionsResponseStream = getMemberCashTransactionsResponse.GetResponseStream();
            DataSet getMemberCashTransactionsDS = new DataSet();
            getMemberCashTransactionsDS.ReadXml(getMemberCashTransactionsResponseStream);

            string getMemberPortfolioHoldingsSoap = string.Empty;
            HttpWebRequest getMemberPortfolioHoldingsRequest = GetWebServiceRequest(string.Format("{0}/member/SearchMembersByID?memberid={1}&format=xml", this.Url, memberCode));
            getMemberPortfolioHoldingsRequest.Timeout = 10000000;

            using (Stream getMemberPortfolioHoldingStm = getMemberPortfolioHoldingsRequest.GetRequestStream())
            {
                using (StreamWriter getMemberPortfolioHoldingsStmw = new StreamWriter(getMemberPortfolioHoldingStm))
                {
                    getMemberPortfolioHoldingsStmw.Write(getMemberPortfolioHoldingsSoap);
                }
            }

            WebResponse response = getMemberPortfolioHoldingsRequest.GetResponse();
            Stream responseStream = response.GetResponseStream();
            DataSet getMemberPortfolioHoldingsDS = new DataSet();
            getMemberPortfolioHoldingsDS.ReadXml(responseStream);

            if (getMemberPortfolioHoldingsDS.Tables["InvestmentProfileAssetViewModel"] != null)
                MergeData(getMemberPortfolioHoldingsDS.Tables["InvestmentProfileAssetViewModel"],false , memberCode, startDate, endDate, getMemberHoldingDS, getMemberCashTransactionsDS, getMemberPortfolioHoldingsDS);
            else if (getMemberHoldingDS.Tables["InvestmentViewModel"] != null)
                MergeData(getMemberHoldingDS.Tables["InvestmentViewModel"],true, memberCode, startDate, endDate, getMemberHoldingDS, getMemberCashTransactionsDS, getMemberPortfolioHoldingsDS);
        }

        private void MergeData(DataTable investmentTable, bool isInvestmentViewModel, string memberCode, DateTime startDate, DateTime endDate, DataSet getMemberHoldingDS, DataSet getMemberCashTransactionsDS, DataSet getMemberPortfolioHoldingsDS)
        {
            foreach (DataRow assetCodeRow in investmentTable.Rows)
            {
                string assetCode = string.Empty;

                if (isInvestmentViewModel)
                {
                    if(assetCodeRow["InvestmentClass"].ToString().ToLower() == "managed fund")
                        assetCode = assetCodeRow["LongAssetCode"].ToString();
                }
                else
                    assetCode = assetCodeRow["AssetLongCode"].ToString();
            
                GetTransactionsByAssetCode(memberCode, startDate, endDate, assetCode, false);
            }

            ResultDataSet.Merge(getMemberCashTransactionsDS);
            ResultDataSet.Merge(getMemberPortfolioHoldingsDS);

            if (getMemberHoldingDS.Tables.Contains("InvestmentViewModel"))
                ResultDataSet.Merge(getMemberHoldingDS.Tables["InvestmentViewModel"]);
        }

        private void GetTransactionsByAssetCode(string memberCode, DateTime startDate, DateTime endDate, string assetCode, bool isEquity)
        {
            string getMemberAssetTransactionsSoap = string.Empty;
            HttpWebRequest getMemberAssetTransactionsRequest = GetWebServiceRequest(string.Format("{0}/member/GetAssetTransactionsForMember?memberid={1}&assetCode={2}&startDate={3}&endDate={4}&format=xml", this.Url, memberCode, assetCode, startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd")));
            getMemberAssetTransactionsRequest.Timeout = 10000000;

            using (Stream getMemberAssetTransactionsStm = getMemberAssetTransactionsRequest.GetRequestStream())
            {
                using (StreamWriter getMemberAssetTransactionsStmw = new StreamWriter(getMemberAssetTransactionsStm))
                {
                    getMemberAssetTransactionsStmw.Write(getMemberAssetTransactionsSoap);
                }
            }

            WebResponse getMemberAssetTransactionsResponse = getMemberAssetTransactionsRequest.GetResponse();
            Stream getMemberAssetTransactionsResponseStream = getMemberAssetTransactionsResponse.GetResponseStream();
            DataSet getMemberAssetTransactionsDS = new DataSet();
            getMemberAssetTransactionsDS.ReadXml(getMemberAssetTransactionsResponseStream);
            foreach (DataTable table in getMemberAssetTransactionsDS.Tables)
            {
                DataTable copiedTable = table.Copy();
                if (isEquity)
                    copiedTable.TableName = table.TableName + assetCode + "_ASX";
                else
                    copiedTable.TableName = table.TableName + assetCode;

                if (!copiedTable.Columns.Contains("Message"))
                {
                    //set message to not found
                    DataColumn dc = new DataColumn("Message");
                    copiedTable.Columns.Add(dc);
                }

                if (!copiedTable.Columns.Contains("HasErrors"))
                {
                    //set has error to true
                    DataColumn dc = new DataColumn("HasErrors", typeof(bool)) { DefaultValue = false };
                    copiedTable.Columns.Add(dc);
                }

                if (!copiedTable.Columns.Contains("IsMissingItem"))
                {
                    //set Is Missing Item to true
                    DataColumn dc = new DataColumn("IsMissingItem", typeof(bool)) { DefaultValue = false };
                    copiedTable.Columns.Add(dc);
                }

                if (isEquity && copiedTable != null)
                    ResultDataSet.Merge(copiedTable);
                else if (copiedTable != null)
                    ResultDataSet.Merge(copiedTable);
            }
        }

        /// <summary>
        /// InvokeMacquarieBankService
        /// </summary>
        /// <param name="encode">Added parameters will encode? (default: true)</param>
        public void InvokeServiceMemberDetails(string memberCode)
        {
            ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(CertValidationUntrusted);

            string getMemberDetailsSoap = string.Empty;
            HttpWebRequest getMemberDetailsRequest = GetWebServiceRequest(string.Format("{0}/member/SearchMembersByID?memberid={1}&format=xml", this.Url, memberCode));

            getMemberDetailsRequest.Timeout = 10000000;

            using (Stream getMemberDetailsStm = getMemberDetailsRequest.GetRequestStream())
            {
                using (StreamWriter getMemberDetailsStmw = new StreamWriter(getMemberDetailsStm))
                {
                    getMemberDetailsStmw.Write(getMemberDetailsSoap);
                }
            }

            WebResponse getMemberDetailsResponse = getMemberDetailsRequest.GetResponse();
            Stream getMemberDetailsResponseStream = getMemberDetailsResponse.GetResponseStream();
            DataSet getMemberDetailsDS = new DataSet();
            getMemberDetailsDS.ReadXml(getMemberDetailsResponseStream);
            ResultDataSet.Merge(getMemberDetailsDS);
        }
    }
}
