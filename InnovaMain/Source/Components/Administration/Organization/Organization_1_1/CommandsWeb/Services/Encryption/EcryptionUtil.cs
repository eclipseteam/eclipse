﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using SMA.Tools.PGP;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb.Services.Encryption
{
    public class EcryptionUtil
    {
        private const string PassPhrase = "aq1sw2de3fr4";
        private const string EclipsepublicKeyFilename = "\\KeyFiles\\ECLIPSE_PGP_Public_Test.asc";
        private const string PrivateKeyFilename = "\\KeyFiles\\ECLIPSE_PGP_Private_Test.asc";
        private const string SmaPublicKeyFilename = "\\KeyFiles\\SMA_PGP_Public_Test.asc";



        public static string  DecryptData(string data)
        {
            string resultdata = data;
            var tempFile = WriteToTempFile(data);
            DecryptFile(tempFile);
            resultdata = ReadFromFile(tempFile);
            return resultdata;
        }
        public static void DecryptFile(string filePath)
        {
            var file = new FileInfo(filePath);
            var parentDirectName = Directory.GetParent((new System.Uri(Assembly.GetExecutingAssembly().CodeBase)).AbsolutePath).FullName;
            var eclipsepubkey = parentDirectName + EclipsepublicKeyFilename;
            var priavteKeyFile = parentDirectName + PrivateKeyFilename;
            var keyStore = new PgpEncryptionKeys(new[] { eclipsepubkey }, priavteKeyFile, PassPhrase);
            var pgp = new PgpDecrypt(keyStore);
            using (var fileStream = new MemoryStream(File.ReadAllBytes(file.FullName)))
            {
                var tempFile = Path.GetTempFileName();
                pgp.DecryptAndVerify(fileStream, tempFile);

                File.Copy(tempFile, file.FullName, true);
                File.Delete(tempFile);
            }
        }
        public static string EncryptData(string data)
        {
            string resultdata = data;
            var tempFile = WriteToTempFile(data);
            EncryptFile(tempFile);
            resultdata = ReadFromFile(tempFile);
            return resultdata;
        }


        private static string ReadFromFile(string path)
        {
            var data = string.Empty;
            using (StreamReader file = new System.IO.StreamReader(path))
            {
                data = file.ReadToEnd();
                file.Close();
            }
            return data;
        }

        private static string WriteToTempFile(string data)
        {
            var tempFile = Path.GetTempFileName();
            using (StreamWriter file = new System.IO.StreamWriter(tempFile))
            {
                file.Write(data);
                file.Close();
            }
            return tempFile;
        }

        public static void EncryptFile(string filePath)
        {
            var parentDirectName = Directory.GetParent((new System.Uri(Assembly.GetExecutingAssembly().CodeBase)).AbsolutePath).FullName;
            var eclipsepubkey = parentDirectName+EclipsepublicKeyFilename;
            var smapubkey =parentDirectName+SmaPublicKeyFilename;
            var priavteKeyFile = parentDirectName  + PrivateKeyFilename;
           
            var publicKeyFilenameArray = new[] { eclipsepubkey, smapubkey };

            var keyStore = new PgpEncryptionKeys(publicKeyFilenameArray, priavteKeyFile, PassPhrase);
            var pgp = new PgpEncrypt(keyStore);
            var file = new FileInfo(filePath);
            using (var fileStream = new MemoryStream())
            {

                pgp.EncryptAndSign(fileStream, file);
                var tempFile = Path.GetTempFileName();

                using (var outFile = File.OpenWrite(tempFile))
                {
                    outFile.Write(fileStream.GetBuffer(), 0, (Int32)fileStream.Length);
                    outFile.Flush();
                    outFile.Close();
                }
                File.Copy(tempFile, file.FullName, true);
                File.Delete(tempFile);
            }

        }
    }
}
