﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Data;
using System.Data;
using Oritax.TaxSimp.CM.Organization.Commands;
using Oritax.TaxSimp.CM.Organization.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Calculation;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{
    public class ImportSMAAccountDeleteClosedAccounts : ImportSMA
    {
        public override void ImportFromService(ImportProcessDS importProcessDS)
        {
            base.ImportFromService(importProcessDS);

            SMAWebService smaWebService = new SMAWebService();
            SMAImportProcessDS smaImportProcessDS = (SMAImportProcessDS)importProcessDS;
            smaImportProcessDS.Tables.Clear();
            smaImportProcessDS.AddBasicResultTableToDataSet();
            smaWebService.ResultDataSet = smaImportProcessDS;

            OrganisationListingDS organisationListingDSEclipseClient = new Data.OrganisationListingDS();
            organisationListingDSEclipseClient.OrganisationListingOperationType = OrganisationListingOperationType.EclipseSuper;
            Organization.GetData(organisationListingDSEclipseClient);

            DataTable EclipseSuperTable = organisationListingDSEclipseClient.Tables[OrganisationListingDS.ECLIPSESUPERTABLENAME];
            
            foreach (DataRow row in EclipseSuperTable.Rows)
            {
                if (row["STATUS_FIELD"].ToString() == "7")
                {
                    IOrganizationUnit clientBMC = this.Organization.Broker.GetCMImplementation(new Guid(row["ENTITYCLID_FIELD"].ToString()), new Guid(row["ENTITYCSID_FIELD"].ToString())) as IOrganizationUnit;

                    if (clientBMC.StatusType == StatusType.Closed)
                    {
                        Organization.Broker.SaveOverride = true; 

                        DataRow resultRow = smaWebService.ResultDataSet.Tables[SMAImportProcessDS.RESULTTABLE].NewRow();
                        resultRow[SMAImportProcessDS.MESSAGE] = "Member Code: " + row["OTHERID_FIELD"].ToString() + " is removed";
                        resultRow[SMAImportProcessDS.ISMISSINGITEM] = "false";
                        resultRow[SMAImportProcessDS.HASERRORS] = "false";

                        IOrganizationUnit manageFundBMC = Organization.Broker.GetBMCInstance(new Guid("b354e928-a3c2-43d7-b61e-feb215cf1bd3")) as IOrganizationUnit;
                        MISTransactionDetailsDS misTranDS = new MISTransactionDetailsDS();
                        misTranDS.ClientID = clientBMC.EclipseClientID;
                        misTranDS.DataSetOperationType = DataSetOperationType.DeletBulk;
                        manageFundBMC.SetData(misTranDS);

                        IClientUMAData clientUMAData = clientBMC.ClientEntity as IClientUMAData;
                        foreach (var identity in clientUMAData.BankAccounts)
                        {
                            var bankBMC = Organization.Broker.GetBMCInstance(identity.Cid);

                            if (bankBMC == null)
                                bankBMC = Organization.Broker.GetCMImplementation(identity.Clid, identity.Csid);

                            if (bankBMC != null)
                                Organization.Broker.DeleteCMInstance(identity.Cid);
                        }
                        Organization.Broker.SaveOverride = true; 
                        Organization.Broker.DeleteCMInstance(clientBMC.CID);
                        this.Organization.Broker.SetComplete();
                        this.Organization.Broker.SetStart();
                        smaWebService.ResultDataSet.Tables[SMAImportProcessDS.RESULTTABLE].Rows.Add(resultRow);
                    }
                }
            }

            importProcessDS.MessageNumber = "SMA013";
            importProcessDS.MessageSummary = "Closed e-Clipse Super Accounts deleted successfully";
            importProcessDS.MessageDetail = "Closed e-Clipse Super Accounts deleted successfully";
        }
    }
}

