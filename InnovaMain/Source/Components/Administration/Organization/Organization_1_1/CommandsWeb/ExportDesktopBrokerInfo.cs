﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Data;
using System.Data;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{
    class ExportDesktopBrokerInfo: OrganizationWebCommandBase
    {       

        public override System.Data.DataSet DoAction(System.Data.DataSet value)
        {
            value.Tables.Clear();            
            int type=(int)CSVType.DesktopBroker;
            AllClientCSVCommand cmd = new AllClientCSVCommand();
            cmd.Organization = this.Organization;
            string fileData=cmd.DoAction(type.ToString());
            
            DataTable data = new DataTable();
            data.Columns.Add("data",typeof(string));
            DataRow r=data.NewRow();
            r[0] = fileData;
            data.Rows.Add(r);
            value.Tables.Add(data);
            return value;
        }
    }
}
