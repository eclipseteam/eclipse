﻿using System;
using System.Data;
using System.Diagnostics;
using System.Net;
using System.Net.Mail;
using System.Text;
using Oritax.TaxSimp.CM.Organization.Commands;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.CM.OrganizationUnit;
using System.Collections.Generic;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{

    public class OrganizationAddCommand : OrganizationWebCommandBase
    {
        const string DBUserModuleName = "DBUser_1_1";
        IBrokerManagedComponent _Component;

        IHasOrganizationUnit data;

        public override DataSet DoAction(DataSet value)
        {
            if (value.Tables.Count == 0)
                return value;

            AddOrganizationUnit(() => (IHasOrganizationUnit)value);
            return value;
        }

        public TaxSimp.Data.OrganizationUnit AddOrganizationUnit(Func<IHasOrganizationUnit> action)
        {
            ILogicalModule created = null;
            ILogicalModule organization = null;
            try
            {
                Organization.Broker.SetStart();
                data = action();

                Guid id = OrganizationTypeList.GetEntity((OrganizationType)data.Unit.Type.ToInt());
                created = Organization.Broker.CreateLogicalCM(id, data.Unit.Name, Guid.Empty, null);
                organization = Organization.Broker.GetLogicalCM(Organization.CLID);
                organization.AddChildLogicalCM(created);
                _Component = created[created.CurrentScenario];
                if(_Component is IOrganizationUnit)
                {
                    IOrganizationUnit orgUnit = _Component as IOrganizationUnit;
                    orgUnit.CreateUniqueID();
                }
                CreationPipeline();
                data.Unit.Type = created.CMTypeName;
                data.Unit.Clid = created.CLID;
            
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Organization.Broker.ReleaseBrokerManagedComponent(organization);
                Organization.Broker.ReleaseBrokerManagedComponent(created);
                Organization.Broker.ReleaseBrokerManagedComponent(_Component);
                _Component = null;
            }
            return data.Unit;
        }

        private void CreationPipeline()
        {
            ICalculationModule calculation = _Component as ICalculationModule;
            if (calculation != null)
            {
                if (_Component is OrganizationUnitCM)
                {
                    (_Component as OrganizationUnitCM).CreationDate = DateTime.Now;
                }
                ////Code Added by Samee to set cm client id
                //if (_Component is IOrganizationUnit)
                //{
                //    (_Component as IOrganizationUnit).ClientId = data.Unit.ClientId;
                //    (_Component as IOrganizationUnit).ApplicationID = data.Unit.ApplicationID;
                //}
                _Component.SetData((DataSet)data);
                data.Unit.Csid = calculation.CSID;
                data.Unit.Cid = calculation.CID;
            }

            ICanCreateDefaultUser creator = _Component as ICanCreateDefaultUser;
            if (creator != null)
            {
                IApplicationUserService service = Organization.Broker.GetCMImplementation(DBUserModuleName, DBUserModuleName) as IApplicationUserService;
                ApplicationUser user = creator.GetDefaultUser();
                service.AddUser(data.Unit.CurrentUser, user);
                SendEMail(user);
            }
            else
            {
                ISecurityService security = _Component as ISecurityService;
                if (security != null) security.AddUser(data.Unit.CurrentUser);
            }

        }

        private void SendEMail(ApplicationUser user)
        {
            try
            {
                SmtpClient client = new SmtpClient(Organization.SmtpSettings.HostName, Organization.SmtpSettings.PortNumber);
                client.Credentials = new NetworkCredential(Organization.SmtpSettings.UserName, Organization.SmtpSettings.Password);
                using (MailMessage mail = new MailMessage())
                {
                    mail.From = new MailAddress(Organization.SmtpSettings.UserName);
                    mail.To.Add(new MailAddress(user.Email));
                    mail.Subject = "Eclipse account details";
                    StringBuilder body = new StringBuilder();
                    body.AppendFormat(" Your new account has been automatically created at eclipse. Account details are give below. {0} {0} {0}", Environment.NewLine);
                    body.AppendFormat(" LoginId : {0}  {1} {1}", user.LoginId, Environment.NewLine);
                    body.AppendFormat(" Password : {0}  {1} {1}", user.Password, Environment.NewLine);
                    body.AppendFormat(" Regards,  {0} Eclipse Administrator", Environment.NewLine);
                    mail.Body = body.ToString();

                    if (Organization.SmtpSettings.UseSsl) client.EnableSsl = true;
                    client.Send(mail);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }
    }
}
