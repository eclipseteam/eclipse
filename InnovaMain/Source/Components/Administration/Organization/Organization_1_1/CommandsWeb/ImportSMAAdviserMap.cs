﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Data;
using System.Data;
using Oritax.TaxSimp.CM.Organization.Commands;
using Oritax.TaxSimp.CM.Organization.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Calculation;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{
    public class ImportSMAAdviserMap : ImportSMA
    {
        public override void ImportFromService(ImportProcessDS importProcessDS)
        {
            base.ImportFromService(importProcessDS);

            SMAWebService smaWebService = new SMAWebService();
            SMAImportProcessDS smaImportProcessDS = (SMAImportProcessDS)importProcessDS;
            smaImportProcessDS.Tables.Clear(); 
            smaImportProcessDS.AddBasicResultTableToDataSet(); 
            smaWebService.ResultDataSet = smaImportProcessDS;

            OrganisationListingDS organisationListingDS = new Data.OrganisationListingDS();
            organisationListingDS.OrganisationListingOperationType = OrganisationListingOperationType.AdvisersBasicList;
            Organization.GetData(organisationListingDS);

            OrganisationListingDS organisationListingDSEclipseClient = new Data.OrganisationListingDS();
            organisationListingDSEclipseClient.OrganisationListingOperationType = OrganisationListingOperationType.EclipseSuper;
            Organization.GetData(organisationListingDSEclipseClient);
            DataTable EclipseSuperTable = organisationListingDSEclipseClient.Tables[OrganisationListingDS.ECLIPSESUPERTABLENAME];

            DataSet memberList = smaWebService.InvokeServiceMemberList(false);

            foreach (DataRow row in memberList.Tables["MemberSearchViewModel"].Rows)
            {
                string clientNumber = row["ClientNumber"].ToString();
                string memberCode = row["MemberCode"].ToString();
                string adviserCode = row["AdviserCode"].ToString();
                string customerNo = clientNumber + "-" + memberCode;

                DataRow existingMember = EclipseSuperTable.Select().Where(crow => crow["OTHERID_FIELD"].ToString() == customerNo).FirstOrDefault();
                DataRow existingAdviser = organisationListingDS.Tables[OrganisationListingDS.ADVISERLISTINGTABLE].Select().Where(drow => drow["OTHERID_FIELD"].ToString().Contains(adviserCode)).FirstOrDefault();

                DataRow resultRow = smaWebService.ResultDataSet.Tables[SMAImportProcessDS.RESULTTABLE].NewRow();

                if (existingAdviser != null && existingMember != null)
                {
                    this.Organization.Broker.SaveOverride = true;

                    IOrganizationUnit advBMC = this.Organization.Broker.GetBMCInstance(new Guid(existingAdviser["ENTITYCIID_FIELD"].ToString())) as IOrganizationUnit;
                    MembershipDS membershipDs = new MembershipDS();
                    membershipDs.CommandType = DatasetCommandTypes.Add;
                    DataRow dr = membershipDs.membershipTable.NewRow();
                    dr[membershipDs.membershipTable.CID] = new Guid(existingMember["ENTITYCIID_FIELD"].ToString());
                    dr[membershipDs.membershipTable.CLID] = new Guid(existingMember["ENTITYCLID_FIELD"].ToString());
                    dr[membershipDs.membershipTable.CSID] = new Guid(existingMember["ENTITYCSID_FIELD"].ToString());
                    dr[membershipDs.membershipTable.NAME] = existingMember["ENTITYNAME_FIELD"].ToString();
                    membershipDs.membershipTable.Rows.Add(dr);
                    advBMC.SetData(membershipDs);

                    resultRow[SMAImportProcessDS.MESSAGE] = "Added e-Clipse Client: " + existingMember["ENTITYNAME_FIELD"] + "[" + customerNo + "] to Adviser: " + existingAdviser["ENTITYNAME_FIELD"];
                    resultRow[SMAImportProcessDS.ISMISSINGITEM] = "false";
                    resultRow[SMAImportProcessDS.HASERRORS] = "false";

                    IAdviserEntity adviserEntity = advBMC.ClientEntity as IAdviserEntity;
                    IEnumerable<DataRow> existingUsers = null;
                    DataRow selectedAdviser = smaImportProcessDS.UserDetail.Select().Where(drow => drow["EmailAddress"].ToString() == adviserEntity.Email && drow["UserType"].ToString() == "0").FirstOrDefault();
                    DataRow resultPartyRow = smaWebService.ResultDataSet.Tables[SMAImportProcessDS.RESULTTABLE].NewRow();

                    if (selectedAdviser != null)
                    {
                        Guid ifaID = new Guid(selectedAdviser["IFACID"].ToString());

                        if (ifaID != null && ifaID != Guid.Empty)
                            existingUsers = smaImportProcessDS.UserDetail.Select().Where(drow => drow["IFACID"].ToString() == ifaID.ToString() && drow["UserType"].ToString() == "0");
                        else
                            existingUsers = smaImportProcessDS.UserDetail.Select().Where(drow => drow["EmailAddress"].ToString() == adviserEntity.Email && drow["UserType"].ToString() == "0");

                        PartyDS partyDS = new PartyDS();
                        IOrganizationUnit clientBMC = this.Organization.Broker.GetBMCInstance(new Guid(existingMember["ENTITYCIID_FIELD"].ToString())) as IOrganizationUnit;
                        clientBMC.GetData(partyDS);
                        partyDS.AcceptChanges();
                        DataRow[] rowsToDelete = partyDS.Tables[PartyDS.INCLUDEDPARTY_TABLE].Select();
                        foreach (DataRow rowToDelete in rowsToDelete)
                            rowToDelete.Delete();

                        clientBMC.SetData(partyDS);
                        clientBMC = this.Organization.Broker.GetBMCInstance(new Guid(existingMember["ENTITYCIID_FIELD"].ToString())) as IOrganizationUnit;
                        partyDS = new PartyDS();

                        foreach (DataRow existingUser in existingUsers)
                        {
                            DataRow partyRow = partyDS.Tables[PartyDS.INCLUDEDPARTY_TABLE].NewRow();
                            partyRow[PartyDS.PARTYNAME_FIELD] = existingUser["Username"];
                            partyRow[PartyDS.PARTYCID_FIELD] = existingUser["CID"];
                            partyDS.Tables[PartyDS.INCLUDEDPARTY_TABLE].Rows.Add(partyRow);
                            resultPartyRow[SMAImportProcessDS.MESSAGE] = "Added Advser Access to e-Clipse Client: " + existingMember["ENTITYNAME_FIELD"] + "[" + customerNo + "] for Adviser ID: " + existingUser["Username"];
                            resultPartyRow[SMAImportProcessDS.ISMISSINGITEM] = "false";
                            resultPartyRow[SMAImportProcessDS.HASERRORS] = "false";
                        }

                        clientBMC.SetData(partyDS);
                    }

                    else
                    {
                        resultPartyRow[SMAImportProcessDS.MESSAGE] = "Failed to Add Advser Access to e-Clipse Client: " + existingMember["ENTITYNAME_FIELD"] + "[" + customerNo + "] for Adviser: " + existingAdviser["ENTITYNAME_FIELD"];
                        resultPartyRow[SMAImportProcessDS.ISMISSINGITEM] = "true";
                        resultPartyRow[SMAImportProcessDS.HASERRORS] = "true";
                    }

                    smaWebService.ResultDataSet.Tables[SMAImportProcessDS.RESULTTABLE].Rows.Add(resultPartyRow);

                    this.Organization.Broker.SetComplete();
                    this.Organization.Broker.SetStart();
                }
                else
                {
                    if (existingAdviser == null)
                    {
                        resultRow[SMAImportProcessDS.MESSAGE] = "Adviser Code: " + adviserCode + " not found, Associated with Member :" + customerNo;
                    }
                    else
                        resultRow[SMAImportProcessDS.MESSAGE] = "Member Code: " + customerNo + " not found";

                    resultRow[SMAImportProcessDS.ISMISSINGITEM] = "true";
                    resultRow[SMAImportProcessDS.HASERRORS] = "true";
                }

                smaWebService.ResultDataSet.Tables[SMAImportProcessDS.RESULTTABLE].Rows.Add(resultRow);
            }
            importProcessDS.MessageNumber = "SMA011";
            importProcessDS.MessageSummary = "Advisers Mapped Successfully";
            importProcessDS.MessageDetail = "Advisers Mapped Successfully";
        }
    }
}
