﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Enum;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Utilities;
using BankAccountEntity = Oritax.TaxSimp.Common.BankAccountEntity;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{

    public class ImportStateStreetPriceListCommand : OrganizationWebCommandBase
    {

        public override System.Data.DataSet DoAction(System.Data.DataSet value)
        {
            if (value.Tables.Count == 0)
                return value;

            if (!value.Tables[0].Columns.Contains("Message"))
            {
                //set message to not found
                DataColumn dc = new DataColumn("Message");
                dc.DefaultValue = "Fund code doesn't exists";
                value.Tables[0].Columns.Add(dc);
            }

            if (!value.Tables[0].Columns.Contains("HasErrors"))
            {
                //set has error to true
                DataColumn dc = new DataColumn("HasErrors", typeof(bool));
                dc.DefaultValue = true;
                value.Tables[0].Columns.Add(dc);
            }

            if (!value.Tables[0].Columns.Contains("IsMissingItem"))
            {
                //set Is Missing Item to true
                DataColumn dc = new DataColumn("IsMissingItem", typeof(bool));
                dc.DefaultValue = true;
                value.Tables[0].Columns.Add(dc);
            }


            foreach (DataRow dr in value.Tables[0].Rows)
            {
                if(!(IsSecurityExists(dr, Organization)))               
                ValidationRows(dr, Organization);
              
            }
                   

            return value;
        }

        private bool IsSecurityExists(DataRow dr, OrganizationCM Organization)
        {
            string InvestmentCode = dr["FundCode"].ToString();
            string CompanyName = dr["FundName"].ToString();
            SecuritiesEntity secuirtyEntity = Organization.Securities.Where(sec => sec.AsxCode == InvestmentCode).FirstOrDefault();
            if (secuirtyEntity == null)
            {
                dr["HasErrors"] = true;
                dr["isMissingItem"] = true;
            }
            else
            {
                dr["HasErrors"] = false;
                dr["isMissingItem"] = false;
                dr["Message"] = "";
            }
            return bool.Parse(dr["isMissingItem"].ToString()) ;
        }    

        private void ValidationRows(DataRow dr, OrganizationCM Organization)
        {
            DateTime date;
            DateTimeFormatInfo info = new DateTimeFormatInfo();
            decimal utemp;
            info.ShortDatePattern = "dd/MM/yyyy";
            ASXSecurityEntity asxprice = new ASXSecurityEntity();
            asxprice.ID = Guid.NewGuid();           
            if (string.IsNullOrEmpty(dr["Currency"].ToString()))
            {
                dr["Message"] += "Invalid Currency,";
                dr["HasErrors"] = true;
            }
            else
            {
                asxprice.Currency = dr["Currency"].ToString();
            }
            if (!DateTime.TryParse(dr["PriceDate"].ToString(), info, DateTimeStyles.None, out date))
            {
                dr["Message"] += "Invalid Price Date,";
                dr["HasErrors"] = true;
            }
            {
                asxprice.Date = date;
            }            
            if (!string.IsNullOrEmpty(dr["Price(NAV)"].ToString()))
                if (!decimal.TryParse(dr["Price(NAV)"].ToString(), NumberStyles.Currency, CultureInfo.CurrentCulture, out utemp))
                {
                    dr["Message"] += "Invalid Price (NAV),";
                    dr["HasErrors"] = true;
                }
                else
                {
                    asxprice.PriceNAV = Convert.ToDouble(utemp.ToString());
                }

            if (!string.IsNullOrEmpty(dr["Price(PUR)"].ToString()))
                if (!decimal.TryParse(dr["Price(PUR)"].ToString(), NumberStyles.Currency, CultureInfo.CurrentCulture, out utemp))
                {
                    dr["Message"] += "Invalid Price (PUR),";
                    dr["HasErrors"] = true;
                }
                else
                {
                    asxprice.PricePUR = Convert.ToDouble(utemp.ToString()); 
                }

            if (!string.IsNullOrEmpty(dr["Price(RDM)"].ToString()))
                if (!decimal.TryParse(dr["Price(RDM)"].ToString(), NumberStyles.Currency, CultureInfo.CurrentCulture, out utemp))
                {
                    dr["Message"] += "Invalid Price (RDM),";
                    dr["HasErrors"] = true;
                }
                else if (utemp == 0)
                { 
                    dr["Message"] += "Price (RDM) is zero,";
                    dr["HasErrors"] = true;      
                }
                else
                {
                    asxprice.UnitPrice = Convert.ToDouble(utemp.ToString()); 
                }
            var security = Organization.Securities.Where(sec => sec.AsxCode == dr["FundCode"].ToString()).FirstOrDefault();
            if (security != null && !bool.Parse(dr["HasErrors"].ToString()))
            {
                if (security.ASXSecurity == null)
                    security.ASXSecurity = new ObservableCollection<ASXSecurityEntity>();
                if (security.ASXSecurity.Count(s => s.Date == date) > 0)
                {
                    dr["Message"] += "Price for date already exists,";
                    dr["HasErrors"] = true;
                }
                else
                {
                    
                    security.ASXSecurity.Add(asxprice);
                }
            }
        }

       
    }
}
