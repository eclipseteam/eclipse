﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Enum;
using System.Collections.ObjectModel;

namespace Oritax.TaxSimp.CM.Organization.Commands
{
    public class ProcessSecurityDistributionCommandWeb : OrganizationCmCommandBase
    {
        public override string DoAction(string data)
        {
            List<Guid> ids = data.ToNewOrData<List<Guid>>();
            DistributionEntity distributionEntity = null;
            this.Organization.Broker.SetStart();
            var security = Organization.Securities.Where(sec => sec.ID == ids[0]).FirstOrDefault();

            if (ids[0] != Guid.Empty)
            {
                if (security != null && security.DistributionCollection != null)
                {
                    distributionEntity = security.DistributionCollection.Where(div => div.ID == ids[1]).FirstOrDefault();
                   
                    if (distributionEntity != null)
                    {
                        distributionEntity.Code = security.AsxCode; 
                        distributionEntity.ProcessFlag = true;
                    }
                    List<ProductEntity> products = new List<ProductEntity>();
                    foreach (var prodsec in Organization.ProductSecurities)
                    {
                        if (prodsec.Details != null)
                        {
                            var details = prodsec.Details.Where(detal => detal.SecurityCodeId == security.ID).FirstOrDefault();
                            if (details != null)
                            {
                                foreach (var product in Organization.Products.Where(produt => produt.ProductSecuritiesId == prodsec.ID))
                                {
                                    products.Add(product);
                                }
                            }
                        }
                    }

                    OrganizationListAction.ForEachClientOnly("Administrator", Organization, (csid, logical, bmc) =>
                    {
                        if ((bmc is IOrganizationUnit) && (bmc as IOrganizationUnit).IsInvestableClient)
                        {
                            (bmc as OrganizationUnitCM).Broker.SetStart();
                            (bmc as OrganizationUnitCM).ProcessDistribution(distributionEntity, products, security);
                            bmc.CalculateToken(true);
                        }
                    });

                }

            }








            if (distributionEntity != null)
                return distributionEntity.ToXmlString();

            return "Failed..";
        }
    }
}
