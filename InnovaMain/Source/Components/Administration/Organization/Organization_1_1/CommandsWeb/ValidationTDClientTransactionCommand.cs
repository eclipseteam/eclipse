﻿using System;
using System.Data;
using System.Globalization;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Enum;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{
    class ValidationTDClientTransactionCommand : OrganizationWebCommandBase
    {
        public override System.Data.DataSet DoAction(System.Data.DataSet value)
        {
            if (value.Tables.Count == 0)
                return value;

            #region cash transactions
            if (value.Tables.Contains("AtTDTransaction"))
            {
                if (!value.Tables["AtTDTransaction"].Columns.Contains("Message"))
                {
                    //set message to not found
                    DataColumn dc = new DataColumn("Message");
                    dc.DefaultValue = "Account Number not Found";
                    value.Tables["AtTDTransaction"].Columns.Add(dc);
                }

                if (!value.Tables["AtTDTransaction"].Columns.Contains("HasErrors"))
                {
                    //set has error to true
                    DataColumn dc = new DataColumn("HasErrors", typeof(bool));
                    dc.DefaultValue = true;
                    value.Tables["AtTDTransaction"].Columns.Add(dc);
                }

                if (!value.Tables["AtTDTransaction"].Columns.Contains("IsMissingItem"))
                {
                    //set Is Missing Item to true
                    DataColumn dc = new DataColumn("IsMissingItem", typeof(bool));
                    dc.DefaultValue = true;
                    value.Tables["AtTDTransaction"].Columns.Add(dc);
                }



            }

            #endregion



            OrganizationListAction.ForEach("Administrator", OrganizationType.BankAccount, Organization,
                                           (csid, logical, bmc) =>
                                           {
                                               if (logical.CMType == OrganizationTypeList.GetEntity(OrganizationType.BankAccount))
                                               {
                                                   string accountNumber = bmc.GetDataStream((int)CmCommand.AccountNumber, string.Empty);

                                                   string accounttype = bmc.GetDataStream((int)CmCommand.GetAccountType, string.Empty);
                                                   if (accounttype.ToLower() == "termdeposit")
                                                   {
                                                       var drs = value.Tables["AtTDTransaction"].Select(string.Format("AccountNumber='{0}'", accountNumber));

                                                       foreach (var dr in drs)
                                                       {
                                                           dr["Message"] = "";
                                                           dr["HasErrors"] = false;
                                                           dr["IsMissingItem"] = false;
                                                           ValidateCashTransaction(dr);
                                                       }
                                                   }
                                               }
                                           });



            return value;
        }

        private void ValidateCashTransaction(DataRow dr)
        {
            DateTimeFormatInfo info = new DateTimeFormatInfo();
            info.ShortDatePattern = "dd/MM/yyyy";

            var accountNo = dr["AccountNumber"].ToString();
            var dealId = dr["DealID"].ToString();
            var tdTran = dr["TDTRAN"].ToString();
            var contractId = dr["ContractID"].ToString();
            var dateOfTransaction = dr["TransactionDate"].ToString();
            var amount = dr["TransactionAmount"].ToString();
            var transcationType = dr["TransactionType"].ToString();

            DateTime date;
            decimal utemp;



            //if (string.IsNullOrEmpty(dealId))
            //{
            //    dr["Message"] += "Invalid Deal ID,";
            //    dr["HasErrors"] = true;
            //}

            if (string.IsNullOrEmpty(tdTran))
            {
                dr["Message"] += "Invalid TDTRAN,";
                dr["HasErrors"] = true;
            }

            if (string.IsNullOrEmpty(contractId))
            {
                dr["Message"] += "Invalid Contract ID,";
                dr["HasErrors"] = true;
            }

            if (!DateTime.TryParse(dateOfTransaction, info, DateTimeStyles.None, out date))
            {
                dr["Message"] += "Invalid Date Of Transaction,";
                dr["HasErrors"] = true;
            }


            if (!decimal.TryParse(amount, NumberStyles.Currency, CultureInfo.CurrentCulture, out utemp))
            {
                dr["Message"] += "Invalid Amount,";
                dr["HasErrors"] = true;
            }
            
            if (string.IsNullOrEmpty(transcationType))
            {
                dr["Message"] += "Invalid Transaction Type,";
                dr["HasErrors"] = true;
            }
            
        }
    }

}
