﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.Organization.Data;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{
 
    public class ExportBankwestAccountOpenings : OrganizationWebCommandBase
    {
        public override DataSet DoAction(DataSet value)
        {
            value.Tables.Clear();
            var objCSVColumns = new AccountOpeningBankWestCSVColumns();
            objCSVColumns.Table.Clear();
            value.Tables.Add(objCSVColumns.Table);
            AccountOpeningBankWestIDCSVColumns obje = new AccountOpeningBankWestIDCSVColumns();
            obje.Table.Clear();
            value.Tables.Add(obje.Table);
            this.Organization.Broker.SaveOverride = true; 
            
            #region Test Code
            //IOrganizationUnit melcot = this.Organization.Broker.GetBMCInstance(new Guid("2fef38e7-bc4f-41b3-a8cc-209f919af3fa")) as IOrganizationUnit;
           //IOrganizationUnit merdam = this.Organization.Broker.GetBMCInstance(new Guid("5b4720c2-9e77-4155-9fee-720c15aad94c")) as IOrganizationUnit;
           //IOrganizationUnit kuhlam = this.Organization.Broker.GetBMCInstance(new Guid("d3da7467-7454-415a-8427-11f775c60e69")) as IOrganizationUnit;
           //IOrganizationUnit decor = this.Organization.Broker.GetBMCInstance(new Guid("71b8d366-dc7c-40fb-85dc-4532408c1690")) as IOrganizationUnit;
           //IOrganizationUnit semple = this.Organization.Broker.GetBMCInstance(new Guid("3c70efdd-4190-4cac-8755-9faf7a2bbb1d")) as IOrganizationUnit;
           //IOrganizationUnit strathon = this.Organization.Broker.GetBMCInstance(new Guid("be7c277a-ed77-4267-a93e-349426dc961e")) as IOrganizationUnit;

           //melcot.GetData(value);
           //merdam.GetData(value);
           //kuhlam.GetData(value);
           //decor.GetData(value);
           //semple.GetData(value);
            //strathon.GetData(value);
            //IOrganizationUnit ZacharyPender = this.Organization.Broker.GetBMCInstance(new Guid("3c8d802c-c75f-4355-b61e-cf4af1c4ecce")) as IOrganizationUnit;
            //ZacharyPender.GetData(value);

            //IOrganizationUnit jane = this.Organization.Broker.GetBMCInstance(new Guid("a99c5bd7-e74b-4685-a215-b5b886badf5e")) as IOrganizationUnit;
            //IOrganizationUnit marie = this.Organization.Broker.GetBMCInstance(new Guid("a76d8f01-c779-4d85-8712-bc85d41064e7")) as IOrganizationUnit;
            //IOrganizationUnit nikitaras = this.Organization.Broker.GetBMCInstance(new Guid("21c628cb-98b8-4080-a9cd-9dcb5144080d")) as IOrganizationUnit;

            //jane.GetData(value);
            //marie.GetData(value);
            //nikitaras.GetData(value);

            #endregion

            OrganisationListingDS organisationListingDS = new OrganisationListingDS();
            IBrokerManagedComponent user = this.Organization.Broker.GetBMCInstance("Administrator", "DBUser_1_1");
            object[] args = new Object[3] { Organization.CLID, user.CID, EnableSecurity.SecuritySetting };
            DataView view = new DataView(Organization.Broker.GetBMCDetailList(OrganizationInstall.ASSEMBLY_NAME, args).Tables["Entities_Table"]);
            view.RowFilter = organisationListingDS.GetRowFilterAllClientsExceptEclipse();
            view.Sort = "ENTITYNAME_FIELD ASC";
            DataTable orgFiteredTable = view.ToTable();

            foreach (DataRow row in orgFiteredTable.Rows)
            {
                Guid cid = new Guid(row["ENTITYCIID_FIELD"].ToString());
                IOrganizationUnit organizationUnit = Organization.Broker.GetBMCInstance(cid) as IOrganizationUnit;
                if (organizationUnit.IsExported == false && organizationUnit.IsInvestableClient)
                {
                    //check validation errors INNOVA-1144
                    var clientValidationDS = new ClientValidationDS();
                    organizationUnit.GetData(clientValidationDS);
                    if (clientValidationDS.Tables[ClientValidationDS.ACCOUNTVALIDATIONTABLE].Rows.Count == 0) //if no errors found
                    {
                        organizationUnit.GetData(value);
                        organizationUnit.IsExported = true;
                        organizationUnit.Modified = true;
                    }
                }
            }

            Organization.Broker.SetComplete();
            Organization.Broker.SetStart(); 

            return value;
        }
    }
}
