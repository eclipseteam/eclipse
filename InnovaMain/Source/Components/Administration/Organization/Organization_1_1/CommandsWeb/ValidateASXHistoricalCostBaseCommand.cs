﻿using System;
using System.Data;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Enum;
using Oritax.TaxSimp.Extensions;
using System.Globalization;
using Oritax.TaxSimp.CM.Entity;
using System.Collections.Generic;
using System.Linq;
namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{
    public class ValidateASXHistoricalCostBaseCommand : OrganizationWebCommandBase
    {
        public override DataSet DoAction(DataSet value)
        {
            if (value.Tables.Count == 0)
                return value;

            if (!value.Tables[0].Columns.Contains("Message"))
            {
                //set message to not found
                DataColumn dc = new DataColumn("Message");
                dc.DefaultValue = "Client Account Not Found";
                value.Tables[0].Columns.Add(dc);
            }

            if (!value.Tables[0].Columns.Contains("HasErrors"))
            {
                //set has error to true
                DataColumn dc = new DataColumn("HasErrors", typeof(bool));
                dc.DefaultValue = true;
                value.Tables[0].Columns.Add(dc);
            }

            if (!value.Tables[0].Columns.Contains("IsMissingItem"))
            {
                //set Is Missing Item to true
                DataColumn dc = new DataColumn("IsMissingItem", typeof(bool));
                dc.DefaultValue = true;
                value.Tables[0].Columns.Add(dc);
            }

            #region Client Checks







            Guid Cid = Guid.Parse(value.ExtendedProperties["CID"].ToString());

            var unit = Organization.Broker.GetBMCInstance(Cid) as OrganizationUnitCM;

            DataRow[] drs = value.Tables[0].Select(string.Format("{0}='{1}'", "[Client Id]", unit.ClientId));

            foreach (DataRow dr in drs)
            {
                dr["Message"] = "";
                dr["HasErrors"] = false;
                dr["IsMissingItem"] = false;
                dr["Client Id"] = unit.ClientId;

                var invCode = Organization.Securities.Where(sec => sec.AsxCode == dr["Investment Code"].ToString()).FirstOrDefault();
                if (invCode != null)
                {
                    List<string> list = new List<string>();
                    list.Add(dr["Investment Code"].ToString());
                    list.Add("0");
                    string idslist = unit.GetDataStream((int)CmCommand.CheckASXInvestmentCode, list.ToXmlString());
                    SerializableDictionary<string, string> ids = idslist.ToNewOrData<SerializableDictionary<string, string>>();


                    if (string.IsNullOrEmpty(ids["ASXCID"]))
                    {
                        dr["Message"] = "Asx Account not found in Client";
                        dr["HasErrors"] = true;
                    }
                }
                else
                {
                    dr["Message"] = "Invalid Investment Code";
                    dr["HasErrors"] = true;
                }

                ValidationRows(dr);

            }

            Organization.Broker.ReleaseBrokerManagedComponent(unit);

            //OrganizationListAction.ForEachClientOnly("Administrator",
            //    Organization, (csid, logical, bmc) =>
            //    {
            //        if ((bmc is OrganizationUnitCM) &&
            //            !string.IsNullOrEmpty((bmc as OrganizationUnitCM).ClientId))
            //        {
            //            var unit = (bmc as OrganizationUnitCM);
            //            DataRow[] drs = value.Tables[0].Select(string.Format("{0}='{1}'", "[Client Id]", unit.ClientId));

            //            foreach (DataRow dr in drs)
            //            {
            //                dr["Message"] = "";
            //                dr["HasErrors"] = false;
            //                dr["IsMissingItem"] = false;
            //                dr["Client Id"] = unit.ClientId;
            //                List<string> list = new List<string>();
            //                list.Add(dr["Investment Code"].ToString());
            //                list.Add("0");
            //                string idslist = unit.GetDataStream((int)CmCommand.CheckASXInvestmentCode, list.ToXmlString());
            //                SerializableDictionary<string, string> ids = idslist.ToNewOrData<SerializableDictionary<string, string>>();

            //                if (string.IsNullOrEmpty(ids["ASXCID"]))
            //                {
            //                    dr["Message"] = "Asx Account not found in Client";
            //                    dr["HasErrors"] = true;
            //                }
            //                ValidationRows(dr);                                                     

            //            }
            //        }
            //    });

            #endregion Client Checks
            return value;
        }
        private void ValidationRows(DataRow dr)
        {
            decimal utemp;



            if (string.IsNullOrEmpty(dr["Client Id"].ToString()))
            {
                dr["Message"] += "Invalid Customer Number,";
                dr["HasErrors"] = true;
            }
            if (string.IsNullOrEmpty(dr["Client Name"].ToString()))
            {
                dr["Message"] += "Invalid Month,";
                dr["HasErrors"] = true;
            }
            if (string.IsNullOrEmpty(dr["Investment Code"].ToString()))
            {
                dr["Message"] += "Invalid Balance,";
                dr["HasErrors"] = true;
            }
            if (string.IsNullOrEmpty(dr["Transaction Date"].ToString()))
            {
                dr["Message"] += "Invalid amount,";
                dr["HasErrors"] = true;
            }
            if ((!decimal.TryParse(dr["Unit Price"].ToString(), NumberStyles.Any, CultureInfo.CurrentCulture, out utemp))
                && (!string.IsNullOrEmpty(dr["Unit Price"].ToString())))
            {
                dr["Message"] += "Invalid Balance,";
                dr["HasErrors"] = true;
            }
            if ((!decimal.TryParse(dr["Units "].ToString(), NumberStyles.Any, CultureInfo.CurrentCulture, out utemp))
                && (!string.IsNullOrEmpty(dr["Units "].ToString())))
            {
                dr["Message"] += "Invalid amount,";
                dr["HasErrors"] = true;
            }
            if (dr["Brokerage"].ToString().Contains(" "))
            {
                dr["Message"] += "Invalid amount,";
                dr["HasErrors"] = true;
            }
            if (dr["Net Value"].ToString().Contains(" "))
            {
                dr["Message"] += "Invalid amount,";
                dr["HasErrors"] = true;
            }
        }
    }
}
