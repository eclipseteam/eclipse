﻿using System;
using System.Data;
using System.Globalization;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.CalculationInterface;
namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{
    public class ValidateTDRatesCommand : OrganizationWebCommandBase
    {
        public override DataSet DoAction(DataSet value)
        {
            if (value.Tables.Count == 0)
                return value;

            if (!value.Tables[0].Columns.Contains("Message"))
            {
                //set message to not found
                DataColumn dc = new DataColumn("Message") { DefaultValue = "Client Account Not Found" };
                value.Tables[0].Columns.Add(dc);
            }

            if (!value.Tables[0].Columns.Contains("HasErrors"))
            {
                //set has error to true
                DataColumn dc = new DataColumn("HasErrors", typeof(bool)) { DefaultValue = true };
                value.Tables[0].Columns.Add(dc);
            }

            if (!value.Tables[0].Columns.Contains("IsMissingItem"))
            {
                //set Is Missing Item to true
                DataColumn dc = new DataColumn("IsMissingItem", typeof(bool)) { DefaultValue = true };
                value.Tables[0].Columns.Add(dc);
            }

            var unit = Organization.Broker.GetWellKnownBMC(WellKnownCM.Organization) as OrganizationUnitCM;

            DataRow[] drs = value.Tables[0].Select();

            foreach (DataRow dr in drs)
            {
                dr["Message"] = "";
                dr["HasErrors"] = false;
                dr["IsMissingItem"] = false;

                ValidationRows(dr);
            }

            Organization.Broker.ReleaseBrokerManagedComponent(unit);

            return value;
        }
        private void ValidationRows(DataRow dr)
        {
            double tempDouble;
            DateTime tempDate;
            DateTimeFormatInfo dtfInfo =
                new DateTimeFormatInfo { ShortDatePattern = "dd/MM/yyyy" };

            if (dr["Min"] != DBNull.Value)

                if (!double.TryParse(dr["Min"].ToString(), out tempDouble))
                {
                    dr["Message"] += "Invalid Min Price,";
                    dr["HasErrors"] = true;
                }
            if (dr["Max"] != DBNull.Value) 
            if (!double.TryParse(dr["Max"].ToString(), out tempDouble))
            {
                dr["Message"] += "Invalid Max Price,";
                dr["HasErrors"] = true;
            }
            if (dr["MaximumBrokeage"] != DBNull.Value) 
            if (!double.TryParse(dr["MaximumBrokeage"].ToString(), out tempDouble))
            {
                dr["Message"] += "Invalid Maximum Brokeage,";
                dr["HasErrors"] = true;
            }
            if (dr["Days30"] != DBNull.Value) 
            if (!double.TryParse(dr["Days30"].ToString(), out tempDouble))
            {
                dr["Message"] += "Invalid Days30,";
                dr["HasErrors"] = true;
            }
            if (dr["Days60"] != DBNull.Value) 
            if (!double.TryParse(dr["Days60"].ToString(), out tempDouble))
            {
                dr["Message"] += "Invalid Days60,";
                dr["HasErrors"] = true;
            }
            if (dr["Days90"] != DBNull.Value) 
            if (!double.TryParse(dr["Days90"].ToString(), out tempDouble))
            {
                dr["Message"] += "Invalid Days90,";
                dr["HasErrors"] = true;
            }
            if (dr["days120"] != DBNull.Value) 
            if (!double.TryParse(dr["days120"].ToString(), out tempDouble))
            {
                dr["Message"] += "Invalid days120,";
                dr["HasErrors"] = true;
            }
            if (dr["Days150"] != DBNull.Value) 
            if (!double.TryParse(dr["Days150"].ToString(), out tempDouble))
            {
                dr["Message"] += "Invalid Days150,";
                dr["HasErrors"] = true;
            }
            if (dr["Days180"] != DBNull.Value) 
            if (!double.TryParse(dr["Days180"].ToString(), out tempDouble))
            {
                dr["Message"] += "Invalid Days180,";
                dr["HasErrors"] = true;
            }
            if (dr["days270"] != DBNull.Value) 
            if (!double.TryParse(dr["days270"].ToString(), out tempDouble))
            {
                dr["Message"] += "Invalid days270,";
                dr["HasErrors"] = true;
            }
            if (dr["Years1"] != DBNull.Value) 
            if (!double.TryParse(dr["Years1"].ToString(), out tempDouble))
            {
                dr["Message"] += "Invalid Years1,";
                dr["HasErrors"] = true;
            }
            if (dr["Years2"] != DBNull.Value) 
            if (!double.TryParse(dr["Years2"].ToString(), out tempDouble))
            {
                dr["Message"] += "Invalid Years2,";
                dr["HasErrors"] = true;
            }
            if (dr["Years3"] != DBNull.Value) 
            if (!double.TryParse(dr["Years3"].ToString(), out tempDouble))
            {
                dr["Message"] += "Invalid Years3,";
                dr["HasErrors"] = true;
            }
            if (dr["Years4"] != DBNull.Value) 
            if (!double.TryParse(dr["Years4"].ToString(), out tempDouble))
            {
                dr["Message"] += "Invalid Years4,";
                dr["HasErrors"] = true;
            }
            if (dr["Years5"] != DBNull.Value) 
            if (!double.TryParse(dr["Years5"].ToString(), out tempDouble))
            {
                dr["Message"] += "Invalid Years5,";
                dr["HasErrors"] = true;
            }

            if (string.IsNullOrEmpty(dr["Status"].ToString()))
            {
                dr["Message"] += "Invalid Status,";
                dr["HasErrors"] = true;
            }

            if (string.IsNullOrEmpty(dr["Date"].ToString()) ||
                !DateTime.TryParse(dr["Date"].ToString(), dtfInfo, DateTimeStyles.None, out tempDate))
            {
                dr["Message"] += "Invalid Date,";
                dr["HasErrors"] = true;
            }
        }
    }
}
