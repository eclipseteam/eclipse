﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Data;
using System.Data;
using Oritax.TaxSimp.CM.Organization.Commands;
using Oritax.TaxSimp.CM.Organization.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.Commands;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{
    [Serializable]
    public class ImportSMAClientMembersTransactions : ImportSMA
    {

        public override void ImportFromService(ImportProcessDS importProcessDS)
        {
            base.ImportFromService(importProcessDS);
            SMAImportProcessDS smaImportProcessDS = (SMAImportProcessDS)importProcessDS;
            smaImportProcessDS.Tables.Clear();
            smaImportProcessDS.AddBasicResultTableToDataSet();
            OrganisationListingDS organisationListingDS = new Data.OrganisationListingDS();
            organisationListingDS.OrganisationListingOperationType = OrganisationListingOperationType.EclipseSuper;
            Organization.GetData(organisationListingDS);

            string userName = "Administrator";
            if(Organization.Broker.UserContext != null)
                userName = Organization.Broker.UserContext.Identity.Name;

            IBrokerManagedComponent user = this.Organization.Broker.GetBMCInstance(userName, "DBUser_1_1");
            object[] args = new Object[3] { Organization.CLID, user.CID, EnableSecurity.SecuritySetting };

            DataSet bmcListingDS = Organization.Broker.GetBMCDetailList(OrganizationInstall.ASSEMBLY_NAME, args);
            DataView bankview = new DataView(bmcListingDS.Tables[OrganisationListingDS.TableName]);
            bankview.RowFilter = OrganisationListingDS.GetRowFilterBankAccounts();
            bankview.Sort = "ENTITYNAME_FIELD ASC";
            DataTable bankFiteredTable = bankview.ToTable();

            SMAWebService smaWebService = new SMAWebService();

            DataSet memberList = smaWebService.InvokeServiceMemberList(false);
            int count = 1;

            foreach (DataRow row in memberList.Tables["MemberSearchViewModel"].Rows)
            {
                string memberCode = row["MemberCode"].ToString();
                smaWebService.ResultDataSet = new SMAImportProcessDS();
                smaWebService.InvokeServiceMemberDataTransactions(memberCode, importProcessDS.Tables[SMAImportProcessDS.RESULTTABLE], smaImportProcessDS.StartDate, smaImportProcessDS.EndDate);
                DataTable MemberViewModel = smaWebService.ResultDataSet.Tables["MemberViewModel"];

                if (MemberViewModel != null)
                {
                    SMASuperEntity smaSuperEntity = new SMASuperEntity();
                    smaSuperEntity.ImportSMAServiceEntityRow(MemberViewModel.Rows[0]);

                    DataRow[] entityRows = organisationListingDS.Tables[0].Select("OTHERID_FIELD LIKE'%" + smaSuperEntity.CustomerNO + "%'");
                    DataRow[] bankRows = bankFiteredTable.Select("OTHERID_FIELD LIKE '%" + smaSuperEntity.CustomerNO + "%'");

                    string eclipseClientID = string.Empty;
                    Guid bankCID = Guid.Empty;

                    if (bankRows.Count() > 0)
                    {
                        foreach (var bankRow in bankRows)
                        {
                            Guid bankCidToCheck = new Guid(bankRow["ENTITYCIID_FIELD"].ToString());
                            IOrganizationUnit orgUnitCM = Organization.Broker.GetBMCInstance(bankCidToCheck) as IOrganizationUnit;
                            BankAccountEntity bankEntity = orgUnitCM.ClientEntity as BankAccountEntity;
                            if (bankEntity.AccoutType.ToLower() == "cma")
                                bankCID = bankCidToCheck;
                        }
                    }

                    if (entityRows.Count() > 0)
                    {
                        eclipseClientID = entityRows[0]["ENTITYECLIPSEID_FIELD"].ToString();


                        if (smaWebService.ResultDataSet.Tables["MemberViewModel"] != null)
                        {
                            DataRow resultRow = importProcessDS.Tables[SMAImportProcessDS.RESULTTABLE].NewRow();

                            try
                            {
                                this.Organization.Broker.SaveOverride = true;
                                this.Organization.Broker.SetStart(); 
                                SMAImportProcessDS sMAImportProcessDS = (SMAImportProcessDS)smaWebService.ResultDataSet;
                                sMAImportProcessDS.ClientID = eclipseClientID;
                                sMAImportProcessDS.MemberID = smaSuperEntity.MemberCode;
                                sMAImportProcessDS.SuperID = smaSuperEntity.ClientNumber;
                                sMAImportProcessDS.Securities = this.Organization.Securities;
                                sMAImportProcessDS.Unit = importProcessDS.Unit;
                                CreateUpdateTransactionsObjects(Guid.Empty, this.Organization.Broker, eclipseClientID, bankCID,Guid.Empty, smaSuperEntity.EntityName, smaSuperEntity.MemberCode, smaSuperEntity.ClientNumber, sMAImportProcessDS, Organization, Organization.Securities);
                                resultRow[SMAImportProcessDS.MESSAGE] = "Imported MIS and Bank transactions successfully for : " + eclipseClientID + "-" + smaSuperEntity.EntityName + " [" + smaSuperEntity.CustomerNO + "]";
                                resultRow[SMAImportProcessDS.ISMISSINGITEM] = "false";
                                resultRow[SMAImportProcessDS.HASERRORS] = "false";
                            }
                            catch
                            {
                                resultRow[SMAImportProcessDS.MESSAGE] = "Import failed for : " + eclipseClientID + "-" + smaSuperEntity.EntityName + " [" + smaSuperEntity.CustomerNO + "]";
                                resultRow[SMAImportProcessDS.ISMISSINGITEM] = "false";
                                resultRow[SMAImportProcessDS.HASERRORS] = "false";
                            }

                            importProcessDS.Tables[SMAImportProcessDS.RESULTTABLE].Rows.Add(resultRow);
                        }
                    }
                    count++;
                }
            }

            importProcessDS.MessageNumber = "SMA002";
            importProcessDS.MessageSummary = "Uploaded Data. Last 5 Days MIS TRANSACTIONS and SINCE INCEPTION BANK TRANSACTIONS";
            importProcessDS.MessageDetail = "Uploaded Data Successfully, please see detailed results below.";
        }
    }
}
