﻿using System.Data;
using Oritax.TaxSimp.Common;
using System;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Extensions;
using eclipse.SMA.Tools.TradeRequestWriter;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{
    public class ExportSMABuyASXOrderCommand : OrganizationWebCommandBase
    {
        public override DataSet DoAction(DataSet value)
        {


            if (value is SMAOrderExportDS)
            {
                var ds = value as SMAOrderExportDS;
                if (ds.Data is OrderEntity)
                {
                    var order = ds.Data as OrderEntity;

                    string date = order.CreatedDate.ToString("dd/MM/yyyy");
                    int count = 0;
                    string CSV = "CONTROL:DBC=S1:REQ=BA:BTC=ECLS74:DIV=5:FND=ECLS:USR=WEB:EFD=" + date + Environment.NewLine;
                    count++;
                    CSV += "HDR:I2 FND MEM EFF IMG ABT SHT BPR" + Environment.NewLine;
                    count++;
                    CSV += "//Buy amount ASX" + Environment.NewLine;
                    count++;
                    string smaClientID = ExportOrderToSMACommand.GetClientSuperID(order.ClientCID, Organization);
                    foreach (var orderItemEntity in order.Items)
                    {
                        if (orderItemEntity is ASXOrderItem)
                        {
                            count++;
                            var item = orderItemEntity as ASXOrderItem;
                            CSV += string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",{4},{5},\"{6}\"{7}", "ECLS", smaClientID, date, item.InvestmentCode.PadLeft(9), item.SuggestedAmount, "Y", item.UnitPrice.Normalize(), Environment.NewLine);

                        }
                    }
                    CSV += "END-OF-FILE=" + (++count);
                    ds.OutputData = CSV;
                }

            }
            return value;
        }

        ///XML version
        ///
        //public override DataSet DoAction(DataSet value)
        //{
        //    if (value is SMAOrderExportDS)
        //    {
        //        var ds = value as SMAOrderExportDS;

        //        if (ds.Data is OrderEntity)
        //        {
        //            var order = ds.Data as OrderEntity;
                    
        //            TradeRequest trade = new TradeRequest();
        //            trade.RequestID = order.OrderID.ToString(); 
        //            string date = order.CreatedDate.ToString("dd/MM/yyyy");
                    
        //            string smaClientID = ExportOrderToSMACommand.GetClientSuperID(order.ClientCID, Organization);
                    
        //            foreach (var orderItemEntity in order.Items)
        //            {
        //                if (orderItemEntity is ASXOrderItem)
        //                {
        //                    var item = orderItemEntity as ASXOrderItem;

        //                    ASXTrade newTrade = new ASXTrade();
        //                    newTrade.Code = item.InvestmentCode;
        //                    newTrade.MemberNumber = smaClientID;
        //                    newTrade.Action = ASXTrade.ActionType.Buy;
        //                    newTrade.EffectiveDate = order.TradeDate;

        //                    if (order.OrderPreferedBy == OrderPreferedBy.Units)
        //                        newTrade.Amount = item.Units.Value;
        //                    else if (order.OrderPreferedBy == OrderPreferedBy.Amount)
        //                        newTrade.Amount = item.Amount.Value;

        //                    trade.ASXTrades.Add(newTrade);
        //                }
        //            }

        //            ds.OutputData = trade.ToXmlString();
        //        }
        //    }
        //    return value;
        //}
    }
}
