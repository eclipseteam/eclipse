﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using Oritax.TaxSimp.CM.Organization.CommandsWeb.Services;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.CM.Organization.Data;
using Oritax.TaxSimp.CalculationInterface;
using System;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Extensions;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{

    public class ExportSMASellAmountTDBulkOrderCommand : OrganizationWebCommandBase
    {
        public override DataSet DoAction(DataSet value)
        {
           
            if (value is SMAOrderExportDS)
            {
                var ds = value as SMAOrderExportDS;

                if (ds.Data is KeyValuePair<BulkOrderEntity, List<OrderEntity>>)
                {
                    var keyvalue = (KeyValuePair<BulkOrderEntity, List<OrderEntity>>)ds.Data;
                    var bulkorder = keyvalue.Key;
                    var orders = keyvalue.Value;
                    string dateApplicable = bulkorder.CreatedDate.ToString("dd/MM/yyyy");
                    int count = 0;
                    string csv = "CONTROL:DBC=S1:REQ=BA:BTC=ECLS75:DIV=5:FND=ECLS:USR=WEB:EFD=" + dateApplicable + Environment.NewLine;
                    count++;
                    csv += "HDR:R2 FND MEM EFF IMG ABT SHT CNO UBT" + Environment.NewLine;
                    count++;
                    csv += "//Sell amount TD Bulk" + Environment.NewLine;
                    count++;
                    foreach (var bulkOrderItemEntity in bulkorder.TDBulkOrderItems)
                    {
                        string code = (ExportOrderToSMACommand.GetInstituteName(bulkOrderItemEntity.InstituteID, Organization) + "_" + bulkOrderItemEntity.Term + "_" + (bulkOrderItemEntity.Rate * 100).Normalize()).PadLeft(9); 
                        foreach (var orderitem in bulkOrderItemEntity.OrderItems)
                        {
                            var order = orders.FirstOrDefault(ss => ss.ID == orderitem.OrderID);
                            string date = order.CreatedDate.ToString("dd/MM/yyyy");
                            var orderItemEntity = order.Items.FirstOrDefault(ss => ss.ID == orderitem.OrderItemID);
                            if (orderItemEntity is TermDepositOrderItem)
                            {
                                count++;
                                var item = orderItemEntity as TermDepositOrderItem;
                                csv += string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",{4},{5},{6},{7}{8}", "ECLS", ExportOrderToSMACommand.GetClientSuperID(order.ClientCID, Organization), date, code, item.Amount.ToPositiveString(), "N", orderItemEntity.SMATransactionID, item.Amount.ToPositiveString(), Environment.NewLine);

                            }

                        }
                        count++;
                        string date2 = bulkOrderItemEntity.CreatedDate.ToString("dd/MM/yyyy");
                        csv += string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",{4},{5},{6},{7}{8}", "ECLS", "-", date2, code, bulkOrderItemEntity.Amount.ToPositiveString(), "N", bulkOrderItemEntity.ContractNote, bulkOrderItemEntity.Amount.ToPositiveString(), Environment.NewLine);

                    }
                   
                   
                   csv +="END-OF-FILE=" + (++count);
                   ds.OutputData = csv;
                }

            }
            return value;
        }

       
    }
}
