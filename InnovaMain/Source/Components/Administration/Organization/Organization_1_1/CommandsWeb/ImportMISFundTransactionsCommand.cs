﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{
    public class ImportMISFundTransactionsCommand : OrganizationWebCommandBase
    {
        public override DataSet DoAction(DataSet value)
        {
            if (value.Tables.Count == 0)
                return value;

            if (!value.Tables[0].Columns.Contains("Message"))
            {
                //set message to not found
                var dc = new DataColumn("Message");
                value.Tables[0].Columns.Add(dc);
            }

            if (!value.Tables[0].Columns.Contains("HasErrors"))
            {
                //set has error to true
                var dc = new DataColumn("HasErrors", typeof(bool)) { DefaultValue = true };
                value.Tables[0].Columns.Add(dc);
            }

            if (!value.Tables[0].Columns.Contains("IsMissingItem"))
            {
                //set Is Missing Item to true
                var dc = new DataColumn("IsMissingItem", typeof(bool)) { DefaultValue = true };
                value.Tables[0].Columns.Add(dc);
            }

            if (!value.Tables[0].Columns.Contains("ClientCID"))
            {
                var dc = new DataColumn("ClientCID", typeof(Guid)) { DefaultValue = Guid.Empty };
                value.Tables[0].Columns.Add(dc);
            }

            var foundAccountNumbers = new List<string>();

            OrganizationListAction.ForEachClientOnly("Administrator", Organization, (csid, logical, bmc) =>
            {
                if (bmc is OrganizationUnitCM && OrganizationListAction.IsValidClientType(logical.CMTypeName))
                {
                    var unit = (bmc as OrganizationUnitCM);
                    if (!string.IsNullOrEmpty(unit.ClientId))
                    {
                        if (value.Tables[0] != null && value.Tables[0].Rows.Count > 0)
                        {
                            DataRow[] drs = value.Tables[0].Select("Account='" + unit.ClientId + "'");
                            if (drs.Length > 0)
                            {
                                foundAccountNumbers.Add(unit.ClientId.Trim());
                                foreach (var dr in drs)
                                {
                                    dr["Message"] = "";
                                    dr["HasErrors"] = false;
                                    dr["IsMissingItem"] = false;
                                    dr["ClientCID"] = unit.CID;
                                    ValidationRow(dr, unit);
                                }

                            }
                        }
                    }
                }
            });

            //cheking missing account/client
            foreach (DataRow dr in value.Tables[0].Rows)
            {
                if (!foundAccountNumbers.Contains(dr["Account"].ToString()))
                {
                    dr["Message"] += "Client with ID (" + dr["Account"] + ") does not exists";
                    dr["HasErrors"] = true;
                    dr["IsMissingItem"] = false;
                }
            }

            //import process 
            OrganizationListAction.ForEach("Administrator", OrganizationType.ManagedInvestmentSchemesAccount, Organization, (csid, logical, bmc) =>
            {
                if (logical.CMType == OrganizationTypeList.GetEntity(OrganizationType.ManagedInvestmentSchemesAccount))
                {
                    OrganizationUnitCM unit = (bmc as OrganizationUnitCM);
                    if (unit != null && unit.Name == "StateStreet")
                    {
                        if (value.Tables[0] != null && value.Tables[0].Rows.Count > 0)
                        {
                            unit.Broker.SetStart();
                            unit.SetData(value);
                            bmc.CalculateToken(true);
                        }
                    }
                }
            });
            if (value.Tables[0].Columns.Contains("ClientCID"))
            {
               value.Tables[0].Columns.Remove("ClientCID");
            }
            return value;
        }

        private void ValidationRow(DataRow dr, OrganizationUnitCM unit)
        {

            #region Validations

            string fundCode = dr["FundCode"].ToString();
            const string misName = "StateStreet";
            DateTimeFormatInfo info = new DateTimeFormatInfo();
            info.ShortDatePattern = "dd/MM/yyyy";

            bool isAttachedWithClient = unit.IsMISAttachedWithFund(fundCode, misName);

            if (!isAttachedWithClient)
            {
                dr["Message"] = "Fund Code (" + fundCode + ") either does not exist or is not linked to this client,";
                dr["HasErrors"] = true;
                dr["IsMissingItem"] = true;
            }

            #endregion


        }
    }
}
