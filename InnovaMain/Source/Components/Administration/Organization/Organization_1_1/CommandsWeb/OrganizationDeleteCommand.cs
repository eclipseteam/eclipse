﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{
    public class OrganizationDeleteCommand : OrganizationWebCommandBase
    {
        public override DataSet DoAction(DataSet value)
        {
            //UserEntity CurrentUser = new UserEntity();
            Organization.Broker.SetStart();
            TaxSimp.Data.OrganizationUnit unit =  ((IHasOrganizationUnit) value).Unit;
            DeleteOrganizationUnit(unit);
            return value;
        }

        public void DeleteOrganizationUnit(TaxSimp.Data.OrganizationUnit unit)
        {
            ILogicalModule logical = Organization.Broker.GetLogicalCM(Organization.CLID);
            ILogicalModule delete = Organization.Broker.GetLogicalCM(unit.Clid);
            logical.RemoveChildLogicalCM(delete);
            Organization.Broker.ReleaseBrokerManagedComponent(logical);
            Organization.Broker.ReleaseBrokerManagedComponent(delete);

            if (unit.ClientId == "" || unit.ClientId == null)
            {
                UpDateEventLog.UpdateLog(unit.Type, unit.Name, 4, Organization.Broker);
            }
            else
            {
                UpDateEventLog.UpdateLog(unit.Type, unit.Name + " (" + unit.ClientId + ")", 4, Organization.Broker);
            }


            //if (OrganizationTypeList.HasSharepoint(unit.Type))
            //{
            //    SharepointProxy.DeleteDocumentLibrary("Admin", unit.Name);
            //}
        }
    }

}
