﻿using System.Data;
using Oritax.TaxSimp.CM.Organization.CommandsWeb.Services;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.CM.Organization.Data;
using Oritax.TaxSimp.CalculationInterface;
using System;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Extensions;
namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{

    public class ExportSMASellTDOrderCommand : OrganizationWebCommandBase
    {
        public override DataSet DoAction(DataSet value)
        {
           
            if (value is SMAOrderExportDS)
            {
                var ds = value as SMAOrderExportDS;
                if(ds.Data is OrderEntity)
                {
                    var order = ds.Data as OrderEntity;
                    string date = order.CreatedDate.ToString("dd/MM/yyyy");
                    int count = 0;
                    string CSV = "CONTROL:DBC=S1:REQ=BA:BTC=ECLS75:DIV=5:FND=ECLS:USR=WEB:EFD=" + date + Environment.NewLine;
                    count++;
                    CSV += "HDR:R2 FND MEM EFF IMG ABT SHT" + Environment.NewLine;
                    count++;
                    CSV += "//Sell amount TD" + Environment.NewLine;
                    count++;
                    string smaClientID = ExportOrderToSMACommand.GetClientSuperID(order.ClientCID, Organization);
                    foreach (var orderItemEntity in order.Items)
                    {
                        if(orderItemEntity is TermDepositOrderItem)
                        {
                            count++;
                            var item = orderItemEntity as TermDepositOrderItem;
                            string code = (ExportOrderToSMACommand.GetInstituteName(item.BankID, Organization) + "_" + item.Duration + "_" + (item.Percentage * 100).Normalize()).PadLeft(9);
                            CSV += string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",{4},{5}{6}", "ECLS", smaClientID, date, code, item.Amount.ToPositiveString(), "N", Environment.NewLine);
                           
                        }
                    }
                  CSV +="END-OF-FILE=" + (++count);
                  ds.OutputData = CSV;
                }

            }
            return value;
        }

       
    }
}
