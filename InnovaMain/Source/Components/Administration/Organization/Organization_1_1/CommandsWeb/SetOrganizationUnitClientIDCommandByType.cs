﻿using System;
using System.Collections.Generic;
using System.Data;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{

    public class SetOrganizationUnitClientIDCommandByType : OrganizationWebCommandBase
    {
        public override DataSet DoAction(DataSet value)
        {
            IHasOrganizationUnit ds = value as IHasOrganizationUnit;
            OrganizationType type;
            OrganizationType.TryParse(ds.Unit.Type, true, out type);
            string userName = ds.Unit.CurrentUser.CurrentUserName;

            userName = OrganizationListAction.IsShownToAllUsers(type, ds.Unit.CurrentUser.CurrentUserName);
            // if instance requires ClientID and Application ID Then Create it. Else skip.
            if (OrganizationTypeList.HasClientIDApplicationID((OrganizationType)ds.Unit.Type.ToInt()))
            {
                List<string> clientIDs = new List<string>();
                List<string> bankwestIDs = new List<string>();
                OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
                    {
                        if ((bmc is IXmlData) && (bmc is IOrganizationUnit))
                        {
                            var cm = (bmc as IOrganizationUnit);
                            if (!string.IsNullOrEmpty(cm.ClientId))
                            {
                                if (!clientIDs.Contains(cm.ClientId))
                                {
                                    clientIDs.Add(cm.ClientId);
                                }
                            }
                            if (!string.IsNullOrEmpty(cm.ApplicationID))
                            {
                                if (!bankwestIDs.Contains(cm.ApplicationID))
                                {
                                    bankwestIDs.Add(cm.ApplicationID);
                                }

                            }

                        }

                    });
                OrganizationListAction.ForEach(userName, type, Organization, (csid, logical, bmc) =>
                    {
                        if (bmc is IOrganizationUnit)
                        {
                            var unit = bmc as IOrganizationUnit;
                            unit.Broker.SetStart();

                            if (string.IsNullOrEmpty(unit.ClientId))
                            {

                                unit.ClientId = "IV" + UniqueID.ReturnUniqueID(EUniqueIDFormat.Combined);
                                while (clientIDs.Contains(unit.ClientId))
                                    unit.ClientId = "IV" + UniqueID.ReturnUniqueID(EUniqueIDFormat.Combined);

                                clientIDs.Add(unit.ClientId);
                            }
                            else
                            {
                                unit.ClientId = unit.ClientId;
                            }
                            if (string.IsNullOrEmpty(unit.ApplicationID))
                            {
                                unit.ApplicationID = "BW" + UniqueID.Return9UniqueID();
                                while (bankwestIDs.Contains(unit.ApplicationID))
                                    unit.ApplicationID = "BW" + UniqueID.Return9UniqueID();

                                bankwestIDs.Add(unit.ClientId);
                            }
                            else
                            {
                                unit.ApplicationID = unit.ApplicationID;
                            }

                            unit.CalculateToken(true);
                        }



                    }, ds.Unit.Name);
            }
            return value;
        }
    }
}
