﻿using System.Data;
using Oritax.TaxSimp.Common;
using System;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Extensions;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{

    public class ExportSMABuyMISOrderCommand : OrganizationWebCommandBase
    {
        public override DataSet DoAction(DataSet value)
        {
          
           
            if (value is SMAOrderExportDS)
            {
                var ds = value as SMAOrderExportDS;
                if(ds.Data is OrderEntity)
                {
                    var order = ds.Data as OrderEntity;
                   
                    string date = order.CreatedDate.ToString("dd/MM/yyyy");
                    int count = 0;
                    string CSV = "CONTROL:DBC=S1:REQ=BA:BTC=ECLS74:DIV=5:FND=ECLS:USR=WEB:EFD=" + date + Environment.NewLine;
                    count++;
                    CSV += "HDR:I2 FND MEM EFF IMG ABT SHT BPR" + Environment.NewLine;
                    count++;
                    CSV += "//Buy amount MIS" + Environment.NewLine;
                    count++;
                    string smaClientID = ExportOrderToSMACommand.GetClientSuperID(order.ClientCID, Organization);

                    foreach (var orderItemEntity in order.Items)
                    {
                        if(orderItemEntity is StateStreetOrderItem)
                        {
                            count++;
                            var item = orderItemEntity as StateStreetOrderItem;
                            decimal? unitPrice = item.Amount.HasValue&&item.Units.HasValue?Math.Abs(item.Amount.Value / item.Units.Value):(decimal?) null;
                            CSV += string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",{4},{5},\"{6}\"{7}", "ECLS", smaClientID, date, item.FundCode.PadLeft(9), item.Amount, "Y", unitPrice.Normalize(), Environment.NewLine);
                           
                        }
                    }
                    CSV += "END-OF-FILE=" + (++count);
                    ds.OutputData = CSV;
                }

            }
            return value;
        }

       
    }
}
