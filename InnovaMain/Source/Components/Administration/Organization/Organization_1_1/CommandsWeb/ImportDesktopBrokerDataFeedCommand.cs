﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{

    public class ImportDesktopBrokerDataFeedCommand : OrganizationWebCommandBase
    {

        public override DataSet DoAction(DataSet value)
        {
            if (value.ExtendedProperties.Count == 0)
            {
                return value;
            }

        
                XElement root = XElement.Parse(value.ExtendedProperties["Xml"].ToString(), LoadOptions.None);

                DesktopBrokerMessagesTable messages = new DesktopBrokerMessagesTable();
                XElement[] elements = root.XPathSelectElements("//InvestmentCode").ToArray();


                foreach (var xElement in elements)
                {
                    string investmentCode = xElement.Value;

                    SecuritiesEntity secuirtyEntity = Organization.Securities.Where(sec => sec.AsxCode == investmentCode).FirstOrDefault();

                    if (secuirtyEntity == null)
                    {
                        secuirtyEntity = new SecuritiesEntity
                                             {
                                                 AsxCode = investmentCode,
                                                 CompanyName = "",
                                                 ID = Guid.NewGuid(),
                                                 ASXSecurity = new ObservableCollection<ASXSecurityEntity>()
                                             };

                        Organization.Securities.Add(secuirtyEntity);

                        DataRow dr = messages.NewRow();

                        dr[DesktopBrokerMessagesTable.Message] = "Investment code" + investmentCode + " has been added ,";
                        dr[DesktopBrokerMessagesTable.InvestmentCode] = investmentCode;
                        dr[DesktopBrokerMessagesTable.HasErrors] = false;
                        dr[DesktopBrokerMessagesTable.IsMissingItem] = true;
                        messages.Rows.Add(dr);
                    }

                }

                DataTable dt = new DataTable("ClientASXAccounts");
                dt.Columns.Add("ClientID");
                dt.Columns.Add("AccountNumber");
                dt.Columns.Add("ASXClid", typeof(Guid));
                dt.Columns.Add("ASXCsid", typeof(Guid));
                dt.Columns.Add("ClientCID",typeof(Guid));
                value.Tables.Add(dt);

                OrganizationListAction.ForEachClientOnly("Administrator", Organization,
                                          (csid, logical, bmc) =>
                                          {

                                              bmc.GetData(value);

                                          });

                var duplicates = dt.AsEnumerable()
                .GroupBy(r => new { a = r[0], b = r[1] })
                .Where(gr => gr.Count() > 1).ToList();

                OrganizationListAction.ForEach("Administrator", OrganizationType.DesktopBrokerAccount, Organization, (csid, logical, bmc) =>
                {
                    if (logical.CMType == OrganizationTypeList.GetEntity(OrganizationType.DesktopBrokerAccount))
                    {
                        IHasImportTransaction unit = (bmc as IHasImportTransaction);
                        if (unit != null)
                        {
                            bmc.Broker.SetStart();

                            foreach (var msg in unit.ImportTransactions(root, value))
                            {
                                var data = ((BellDirectMessages)msg);

                                DataRow dr = messages.NewRow();

                                dr[DesktopBrokerMessagesTable.Message] = data.Message;
                                dr[DesktopBrokerMessagesTable.InvestmentCode] = data.InvestmentCode;
                                dr[DesktopBrokerMessagesTable.HasErrors] = data.MessageType == ImportMessageType.Error;
                                dr[DesktopBrokerMessagesTable.IsMissingItem] = false;
                                dr[DesktopBrokerMessagesTable.AccountNumber] = data.AccountNumber;
                                dr[DesktopBrokerMessagesTable.TransactionDate] = data.TransactionDate;
                                dr[DesktopBrokerMessagesTable.TransactionID] = data.TransactionID;

                                messages.Rows.Add(dr);
                            }

                            //TODO: We Need to find a central place to calculate token
                            bmc.CalculateToken(true);
                        }
                    }
                });
                value.Tables.Add(messages);
                value.Tables.Remove(dt);
           
            return value;
        }
    }
}
