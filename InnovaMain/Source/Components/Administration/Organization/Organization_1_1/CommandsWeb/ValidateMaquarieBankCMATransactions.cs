﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Data;
using System.Data;
using Oritax.TaxSimp.CM.Organization.Commands;
using Oritax.TaxSimp.CM.Organization.Data;
using Oritax.TaxSimp.DataSets;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{
    public class ValidateMaquarieBankCMATransactions : OrganizationWebCommandBase
    {

        public override System.Data.DataSet DoAction(System.Data.DataSet value)
        {
            ImportProcessDS importProcessDS = (ImportProcessDS)value;
            if (importProcessDS.UseFile)
            { ImportFromFile(importProcessDS); }
            else
            {
                ImportFromService(importProcessDS);

            }
            return importProcessDS;
        }

        private void ImportFromFile(ImportProcessDS importProcessDs)
        {
            ValidateTransactions(importProcessDs);
        }

        private void ImportFromService(ImportProcessDS importProcessDS)
        {
            MaquarieBankWebService maqService = new MaquarieBankWebService("https://www.macquarie.com.au/ESI/ESIWebService/Extract", "");
            maqService.InvokeMacquarieBankService();

            if (maqService.IsSuccess)
            {
                if (maqService.ResultTable == null)
                {
                    importProcessDS.MessageNumber = maqService.MessageNumber;
                    importProcessDS.MessageSummary = maqService.MessageSummary;
                    importProcessDS.MessageDetail = "Successful however nothing to load";
                }
                else
                {
                    DataTable resultTable = maqService.ResultTable.Copy();
                    resultTable.TableName = MaquarieCMATransactionsDS.MACQUARIEBANKTRANSACTIONSTABLE;

                    importProcessDS.IsSuccess = maqService.IsSuccess;
                    importProcessDS.MessageNumber = maqService.MessageNumber;
                    importProcessDS.MessageSummary = maqService.MessageSummary;
                    importProcessDS.MessageDetail = maqService.MessageDetail;
                    importProcessDS.Tables.Add(resultTable.Copy());

                    if (importProcessDS.IsSuccess)
                    {
                        ValidateTransactions(importProcessDS);
                    }
                }
            }
            else
            {
                //if service failed
                importProcessDS.MessageNumber = maqService.MessageNumber;
                importProcessDS.MessageSummary = maqService.MessageSummary;
                importProcessDS.MessageDetail = "Failed to connect service";
            }
        }

        private void ValidateTransactions(ImportProcessDS importProcessDS)
        {
            DataTable resultTable = importProcessDS.Tables[MaquarieCMATransactionsDS.MACQUARIEBANKTRANSACTIONSTABLE];
            if (!resultTable.Columns.Contains(MaquarieCMATransactionsDS.MESSAGE))
            {
                var dc = new DataColumn(MaquarieCMATransactionsDS.MESSAGE, typeof(string));

                dc.DefaultValue = "Account Not fount";
                resultTable.Columns.Add(dc);
            
            
            }
            if (!resultTable.Columns.Contains(MaquarieCMATransactionsDS.HASERRORS))
            {
                var dc = new DataColumn(MaquarieCMATransactionsDS.HASERRORS, typeof(bool));

                dc.DefaultValue = true;
                resultTable.Columns.Add(dc);
                
                
            }
            if (!resultTable.Columns.Contains(MaquarieCMATransactionsDS.ISMISSINGITEM))
            {
                var dc = new DataColumn(MaquarieCMATransactionsDS.ISMISSINGITEM, typeof(bool));

                dc.DefaultValue = true;
                resultTable.Columns.Add(dc);


            }

            OrganisationListingDS organisationListingDS = new OrganisationListingDS();
            organisationListingDS.OrganisationListingOperationType = OrganisationListingOperationType.BankAccounts;
            this.Organization.GetData(organisationListingDS);

            DataRow[] filteredRows = organisationListingDS.Tables[BankDetailsDS.BANKDETAILSTABLELIST].Select("InstitutionID='f2ca2e28-f60d-452e-822f-d5dedc9d5eb1'");
            foreach (DataRow row in filteredRows)
            {
                var selectedTransaction = importProcessDS.Tables[MaquarieCMATransactionsDS.MACQUARIEBANKTRANSACTIONSTABLE].Select().Where(accrow => accrow[MaquarieCMATransactionsDS.ACCOUNTNUMBER].ToString() == row[BankDetailsDS.ACCOUNTNUMBER].ToString());
                if (selectedTransaction.Count() > 0)
                {

                    foreach (DataRow dataRow in selectedTransaction)
                    {
                        dataRow[MaquarieCMATransactionsDS.MESSAGE] = "";
                        dataRow[MaquarieCMATransactionsDS.HASERRORS] = false;
                        dataRow[MaquarieCMATransactionsDS.ISMISSINGITEM] = false;
                        ValidateRow(dataRow);
                    }
                    
                }
            }

        }

        private void ValidateRow(DataRow dr)
        {
        
            DateTime dateofTrans;
            DateTimeFormatInfo info = new DateTimeFormatInfo();
            info.ShortDatePattern = "dd/MM/yyyy";
            decimal amount;
          
            
            if (!decimal.TryParse(dr[MaquarieCMATransactionsDS.AMOUNT].ToString(), out amount))
                {
                    dr[MaquarieCMATransactionsDS.MESSAGE] += "Invalid amount,";
                    dr[MaquarieCMATransactionsDS.HASERRORS] = true;
                  
                }
                if (dr[MaquarieCMATransactionsDS.DEBITCREDIT] == DBNull.Value || string.IsNullOrEmpty(dr[MaquarieCMATransactionsDS.DEBITCREDIT].ToString()))
                {
                    dr[MaquarieCMATransactionsDS.MESSAGE] += "unable to recognize transaction type (Withdrawal/Deposit),";
                    dr[MaquarieCMATransactionsDS.HASERRORS] = true;
                }


                if (!DateTime.TryParse(dr[MaquarieCMATransactionsDS.TRANSACTIONDATE].ToString(), info, DateTimeStyles.None, out dateofTrans))
                {
                    dr[MaquarieCMATransactionsDS.MESSAGE] += "Invalid transaction date,";
                    dr[MaquarieCMATransactionsDS.HASERRORS] = true;
                   
                }

                if (dr[MaquarieCMATransactionsDS.TRANSACTIONTYPE] == DBNull.Value && string.IsNullOrEmpty(dr[MaquarieCMATransactionsDS.TRANSACTIONTYPE].ToString()))
                {

                    if (dr[MaquarieCMATransactionsDS.SYSTEMTRANSACTIONTYPE] == DBNull.Value && string.IsNullOrEmpty(dr[MaquarieCMATransactionsDS.SYSTEMTRANSACTIONTYPE].ToString()))
                    {
                        dr[MaquarieCMATransactionsDS.MESSAGE] += "Invalid system transaction type,";
                        dr[MaquarieCMATransactionsDS.HASERRORS] = true;
                     }


                    if (dr[MaquarieCMATransactionsDS.IMPORTTRANSACTIONTYPEDETAIL] == DBNull.Value && string.IsNullOrEmpty(dr[MaquarieCMATransactionsDS.IMPORTTRANSACTIONTYPEDETAIL].ToString()))
                    {
                        dr[MaquarieCMATransactionsDS.MESSAGE] += "Invalid import transaction detail,";
                        dr[MaquarieCMATransactionsDS.HASERRORS] = true;
                    }

                }
               
                   

               


               

        }

    }
}
