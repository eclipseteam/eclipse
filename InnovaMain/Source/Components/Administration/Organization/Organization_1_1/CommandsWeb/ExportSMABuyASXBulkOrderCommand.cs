﻿using System.Collections.Generic;
using System.Data;
using Oritax.TaxSimp.Common;
using System;
using Oritax.TaxSimp.DataSets;
using System.Linq;
using Oritax.TaxSimp.Extensions;


namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{

    public class ExportSMABuyASXBulkOrderCommand : OrganizationWebCommandBase
    {
        public override DataSet DoAction(DataSet value)
        {
            if (value is SMAOrderExportDS)
            {
                var ds = value as SMAOrderExportDS;

                if (ds.Data is KeyValuePair<BulkOrderEntity, List<OrderEntity>>)
                {
                    var keyvalue = (KeyValuePair<BulkOrderEntity, List<OrderEntity>>)ds.Data;
                    var bulkorder = keyvalue.Key;
                    var orders = keyvalue.Value;
                    string dateApplicable = bulkorder.CreatedDate.ToString("dd/MM/yyyy");
                    int count = 0;
                    string csv = "CONTROL:DBC=S1:REQ=BA:BTC=ECLS74:DIV=5:FND=ECLS:USR=WEB:EFD=" + dateApplicable + Environment.NewLine;
                    count++;
                    csv += "HDR:I2 FND MEM EFF IMG ABT SHT CNO UBT BPR" + Environment.NewLine;
                    count++;
                    csv += "//Buy ASX bulk" + Environment.NewLine;
                    count++;
                    foreach (var bulkOrderItemEntity in bulkorder.BulkOrderItems)
                    {
                        foreach(var orderitem in bulkOrderItemEntity.OrderItems)
                        {
                            var order = orders.FirstOrDefault(ss => ss.ID==orderitem.OrderID);
                            string date = order.CreatedDate.ToString("dd/MM/yyyy");
                            var orderItemEntity = order.Items.FirstOrDefault(ss => ss.ID == orderitem.OrderItemID);
                                if (orderItemEntity is ASXOrderItem)
                                {
                                    count++;
                                    var item = orderItemEntity as ASXOrderItem;
                                    csv += string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",{4},{5},{6},{7},\"{8}\"{9}", "ECLS", ExportOrderToSMACommand.GetClientSuperID(order.ClientCID, Organization), date, item.InvestmentCode.PadLeft(9), item.Amount, "Y", orderItemEntity.SMATransactionID, item.Units, item.UnitPrice.Normalize(), Environment.NewLine);

                                }
                            
                        }

                        count++;
                        string date2 = bulkOrderItemEntity.CreatedDate.ToString("dd/MM/yyyy");
                        csv += string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",{4},{5},{6},{7},\"{8}\"{9}", "ECLS", "-", date2, bulkOrderItemEntity.Code.PadLeft(9), bulkOrderItemEntity.ActualAmount, "Y", bulkOrderItemEntity.ContractNote, bulkOrderItemEntity.ActualUnits, bulkOrderItemEntity.ActualUnitPrice.Normalize(), Environment.NewLine);

                    }
                   
                   
                   csv += "END-OF-FILE=" + (++count);
                   ds.OutputData = csv;
                }

            }
            return value;
        }


    }
}
