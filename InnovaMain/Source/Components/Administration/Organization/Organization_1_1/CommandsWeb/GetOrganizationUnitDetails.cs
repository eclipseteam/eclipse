﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{
 
    public class GetOrganizationUnitDetails : OrganizationWebCommandBase
    {
        public override DataSet DoAction(DataSet value)
        {
            IHasOrganizationUnit ds = value as IHasOrganizationUnit;
            OrganizationType type;
            OrganizationType.TryParse(ds.Unit.Type, true, out type);
            OrganizationListAction.ForEach(ds.Unit.CurrentUser.CurrentUserName, type, Organization, (csid, logical, bmc) =>
            {
                if (logical.CMType == OrganizationTypeList.GetEntity(type))
                {
                    bmc.GetData(value);
                }
            }, ds.Unit.Name,ds.Unit.Cid.ToString());
            return value;
        }
    }
}
