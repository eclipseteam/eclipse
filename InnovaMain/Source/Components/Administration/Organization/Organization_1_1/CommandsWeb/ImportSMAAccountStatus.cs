﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Data;
using System.Data;
using Oritax.TaxSimp.CM.Organization.Commands;
using Oritax.TaxSimp.CM.Organization.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Calculation;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{
    public class ImportSMAAccountStatus : ImportSMA
    {
        public override void ImportFromService(ImportProcessDS importProcessDS)
        {
            base.ImportFromService(importProcessDS);

            SMAWebService smaWebService = new SMAWebService();
            SMAImportProcessDS smaImportProcessDS = (SMAImportProcessDS)importProcessDS;
            smaImportProcessDS.Tables.Clear();
            smaImportProcessDS.AddBasicResultTableToDataSet();
            smaWebService.ResultDataSet = smaImportProcessDS;

            DataTable resultTable = smaWebService.ResultDataSet.Tables[SMAImportProcessDS.RESULTTABLE].Copy(); 

            OrganisationListingDS organisationListingDSEclipseClient = new Data.OrganisationListingDS();
            organisationListingDSEclipseClient.OrganisationListingOperationType = OrganisationListingOperationType.EclipseSuper;
            Organization.GetData(organisationListingDSEclipseClient);
            DataTable EclipseSuperTable = organisationListingDSEclipseClient.Tables[OrganisationListingDS.ECLIPSESUPERTABLENAME];

            DataSet memberList = smaWebService.InvokeServiceMemberList(false);

           
            foreach (DataRow row in memberList.Tables["MemberSearchViewModel"].Rows)
            {
                string clientNumber = row["ClientNumber"].ToString();
                string memberCode = row["MemberCode"].ToString();
                string customerNo = clientNumber + "-" + memberCode;
                


                DataRow existingMember = EclipseSuperTable.Select().Where(crow => crow["OTHERID_FIELD"].ToString() == customerNo).FirstOrDefault();
                DataRow resultRow = resultTable.NewRow();
                smaWebService.ResultDataSet = new SMAImportProcessDS();
                smaWebService.InvokeServiceMembersDetails(memberCode, importProcessDS.Tables[ResultTableName]);
                if (smaWebService.ResultDataSet.Tables["MemberViewModel"] != null && existingMember != null)
                {
                    SMASuperEntity smaSuperEntity = new SMASuperEntity();
                    smaSuperEntity.ImportSMAServiceEntityRow(smaWebService.ResultDataSet.Tables["MemberViewModel"].Rows[0]);

                    this.Organization.Broker.SaveOverride = true;
                    IOrganizationUnit clientBMC = this.Organization.Broker.GetBMCInstance(new Guid(existingMember["ENTITYCIID_FIELD"].ToString())) as IOrganizationUnit;

                    if (row["AccountStatus"].ToString().ToLower() == "active")
                        clientBMC.StatusType = TaxSimp.Data.StatusType.Active;
                    else
                        clientBMC.StatusType = TaxSimp.Data.StatusType.Closed;


                    ILogicalModule created = null;
                    ILogicalModule organization = null;

                    created = this.Organization.Broker.GetLogicalCM(clientBMC.CLID);
                    organization = Organization.Broker.GetLogicalCM(Organization.CLID);

                    clientBMC.Name = smaSuperEntity.EntityName;
                    SetClientIndividualData(Organization as IOrganization, smaSuperEntity, clientBMC, (ClientIndividualEntity)clientBMC.ClientEntity, ref created, ref organization);

                    resultRow[SMAImportProcessDS.MESSAGE] = "Member Code: " + customerNo + " Status is updated to: " + Enumeration.GetDescription(clientBMC.StatusType);
                    resultRow[SMAImportProcessDS.ISMISSINGITEM] = "false";
                    resultRow[SMAImportProcessDS.HASERRORS] = "false";

                    if (clientBMC.StatusType == StatusType.Closed)
                    {
                        resultRow[SMAImportProcessDS.MESSAGE] = "Member Code: " + customerNo + " Status is updated to: " + Enumeration.GetDescription(clientBMC.StatusType) + " and removed";

                        IOrganizationUnit manageFundBMC = Organization.Broker.GetBMCInstance(new Guid("b354e928-a3c2-43d7-b61e-feb215cf1bd3")) as IOrganizationUnit;
                        MISTransactionDetailsDS misTranDS = new MISTransactionDetailsDS();
                        misTranDS.ClientID = clientBMC.EclipseClientID;
                        misTranDS.DataSetOperationType = DataSetOperationType.DeletBulk;
                        manageFundBMC.SetData(misTranDS);

                        IClientUMAData clientUMAData = clientBMC.ClientEntity as IClientUMAData;
                        foreach (var identity in clientUMAData.BankAccounts)
                        {
                            var bankBMC = Organization.Broker.GetBMCInstance(identity.Cid);

                            if (bankBMC == null)
                                bankBMC = Organization.Broker.GetCMImplementation(identity.Clid, identity.Csid);

                            if (bankBMC != null)
                                Organization.Broker.DeleteCMInstance(identity.Cid);
                        }

                        Organization.Broker.DeleteCMInstance(clientBMC.CID);
                    }

                    else
                        clientBMC.CalculateToken(true);

                    this.Organization.Broker.SetComplete();
                    this.Organization.Broker.SetStart();
                }
                else
                {
                    resultRow[SMAImportProcessDS.MESSAGE] = "Member Code: " + customerNo + " not found";
                    resultRow[SMAImportProcessDS.ISMISSINGITEM] = "true";
                    resultRow[SMAImportProcessDS.HASERRORS] = "true";
                }

                resultTable.Rows.Add(resultRow);
            }

            importProcessDS.Merge(resultTable); 
            importProcessDS.MessageNumber = "SMA012";
            importProcessDS.MessageSummary = "Account Statuses updated successfully";
            importProcessDS.MessageDetail = "Account Statuses updated successfully. Please see details below";
        }
    }
}
