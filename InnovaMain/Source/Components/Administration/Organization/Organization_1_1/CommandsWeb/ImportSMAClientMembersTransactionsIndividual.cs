﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Data;
using System.Data;
using Oritax.TaxSimp.CM.Organization.Commands;
using Oritax.TaxSimp.CM.Organization.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.Commands;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{
    [Serializable]
    public class ImportSMAClientMembersTransactionsIndividual : ImportSMA
    {

        public override void ImportFromService(ImportProcessDS importProcessDS)
        {
            base.ImportFromService(importProcessDS);
            SMAImportProcessDS smaImportProcessDS = (SMAImportProcessDS)importProcessDS;
            OrganisationListingDS organisationListingDS = new Data.OrganisationListingDS();
            organisationListingDS.OrganisationListingOperationType = OrganisationListingOperationType.EclipseSuper;
            Organization.GetData(organisationListingDS);

            string userName = "Administrator";
            IBrokerManagedComponent user = this.Organization.Broker.GetBMCInstance(userName, "DBUser_1_1");
            object[] args = new Object[3] { Organization.CLID, user.CID, EnableSecurity.SecuritySetting };

            DataSet bmcListingDS = Organization.Broker.GetBMCDetailList(OrganizationInstall.ASSEMBLY_NAME, args);
            DataView bankview = new DataView(bmcListingDS.Tables[OrganisationListingDS.TableName]);
            bankview.RowFilter = OrganisationListingDS.GetRowFilterBankAccounts();
            bankview.Sort = "ENTITYNAME_FIELD ASC";
            DataTable bankFiteredTable = bankview.ToTable();

            DataView asxView = new DataView(bmcListingDS.Tables[OrganisationListingDS.TableName]);
            asxView.RowFilter = new OrganisationListingDS().GetRowFilterASXAccounts();
            asxView.Sort = "ENTITYNAME_FIELD ASC";
            DataTable asxViewTable = asxView.ToTable();

            SMAWebService smaWebService = new SMAWebService();
            importProcessDS.Tables.Clear();
            importProcessDS.Tables.Add(new DataTable("ResultTable"));

            string memberCode = importProcessDS.Variables["MemberCode"].ToString();


            string eclipseID = "";
            if (importProcessDS.Variables.ContainsKey("EclipseID"))
                eclipseID = importProcessDS.Variables["EclipseID"].ToString();
            if (!string.IsNullOrWhiteSpace(eclipseID))
            {
                string OrgUnitCid = "";
                if (importProcessDS.Variables.ContainsKey("OrgCid"))
                    OrgUnitCid = importProcessDS.Variables["OrgCid"].ToString();
                if (!string.IsNullOrWhiteSpace(OrgUnitCid))
                {
                    IBrokerManagedComponent orgUnit = this.Organization.Broker.GetBMCInstance(new Guid(OrgUnitCid)) as IBrokerManagedComponent;
                    if (orgUnit != null)
                        eclipseID = orgUnit.EclipseClientID;
                    this.Organization.Broker.ReleaseBrokerManagedComponent(orgUnit);
                }

            }

            smaWebService.ResultDataSet = new SMAImportProcessDS();
            smaWebService.InvokeServiceMemberDataTransactions(memberCode, importProcessDS.Tables[ResultTableName], smaImportProcessDS.StartDate, smaImportProcessDS.EndDate);
            DataTable MemberViewModel = smaWebService.ResultDataSet.Tables["MemberViewModel"];

            if (MemberViewModel != null)
            {
                SMASuperEntity smaSuperEntity = new SMASuperEntity();
                smaSuperEntity.ImportSMAServiceEntityRow(MemberViewModel.Rows[0]);

                DataRow[] entityRows = organisationListingDS.Tables[0].Select("OTHERID_FIELD LIKE'%" + smaSuperEntity.CustomerNO + "%'");
                DataRow[] bankRows = bankFiteredTable.Select("OTHERID_FIELD LIKE '%" + smaSuperEntity.CustomerNO + "%'");
                DataRow[] asxAccountsRows = asxViewTable.Select("OTHERID_FIELD LIKE '%" + smaSuperEntity.CustomerNO + "%'");

                string eclipseClientID = string.Empty;
                Guid bankCID = Guid.Empty;
                Guid bankCIDTD = Guid.Empty;
                Guid asxCID = Guid.Empty;

                if (asxAccountsRows.Count() == 0)
                    asxAccountsRows = asxViewTable.Select("ENTITYECLIPSEID_FIELD = '" + eclipseID + "'");

                if (asxAccountsRows.Count() > 0)
                    asxCID = new Guid(asxAccountsRows[0]["ENTITYCIID_FIELD"].ToString());

                if (bankRows.Count() > 0)
                {
                    foreach (var row in bankRows)
                    {
                        Guid bankCidToCheck = new Guid(row["ENTITYCIID_FIELD"].ToString());
                        IOrganizationUnit orgUnitCM = Organization.Broker.GetBMCInstance(bankCidToCheck) as IOrganizationUnit;
                        BankAccountEntity bankEntity = orgUnitCM.ClientEntity as BankAccountEntity;
                        if (bankEntity.AccoutType.ToLower() == "cma")
                            bankCID = bankCidToCheck;
                        else
                            bankCIDTD = bankCidToCheck;
                    }
                }

                if (entityRows.Count() > 0)
                {
                    eclipseClientID = entityRows[0]["ENTITYECLIPSEID_FIELD"].ToString();

                    if (smaWebService.ResultDataSet.Tables["MemberViewModel"] != null)
                    {
                        this.Organization.Broker.SaveOverride = true;
                        SMAImportProcessDS sMAImportProcessDS = (SMAImportProcessDS)smaWebService.ResultDataSet;
                        sMAImportProcessDS.Unit = importProcessDS.Unit;
                        sMAImportProcessDS.ClientID = eclipseClientID;
                        sMAImportProcessDS.MemberID = smaSuperEntity.MemberCode;
                        sMAImportProcessDS.SuperID = smaSuperEntity.ClientNumber;
                        sMAImportProcessDS.Securities = this.Organization.Securities;
                        sMAImportProcessDS.ClientUMAID = eclipseClientID;

                        if (sMAImportProcessDS.Tables["InvestmentViewModel"] != null)
                        {
                            var rowSelected = sMAImportProcessDS.Tables["InvestmentViewModel"].Select().Where(row => row["LongAssetCode"].ToString().ToLower() == "accrual").FirstOrDefault();
                            if (rowSelected != null)
                            {
                                decimal holding = 0;
                                bool success = Decimal.TryParse(rowSelected["DollarBalanceIncludePending"].ToString(), out holding);
                                if (success)
                                    smaImportProcessDS.AccruedHolding = holding;

                                smaImportProcessDS.AccruedDescriptionProductCode = rowSelected["LongAssetCode"].ToString();
                                smaImportProcessDS.AccruedDescription = rowSelected["InvestmentName"].ToString();
                            }
                        }

                        CreateUpdateTransactionsObjects(asxCID, this.Organization.Broker, eclipseClientID, bankCID,bankCIDTD, smaSuperEntity.EntityName, smaSuperEntity.MemberCode, smaSuperEntity.ClientNumber, sMAImportProcessDS, Organization, Organization.Securities);
                        foreach (DataTable table in sMAImportProcessDS.Tables)
                        {
                            if (table.TableName.StartsWith("AssetTransactionViewModel"))
                                importProcessDS.Tables["ResultTable"].Merge(table, true, MissingSchemaAction.Add);
                        }


                    }
                }
            }

            importProcessDS.MessageNumber = "SMA002";
            importProcessDS.MessageSummary = "Uploaded Data";
            importProcessDS.MessageDetail = "Uploaded Data Successfully, please see detailed results below";
        }
    }
}
