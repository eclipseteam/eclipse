﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Data;
using System.Data;
using Oritax.TaxSimp.CM.Organization.Commands;
using Oritax.TaxSimp.CM.Organization.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.Commands;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{
    [Serializable]
    public class SMASuperEntity
    {
        public string ClientNumber = string.Empty;
        public string MemberCode = string.Empty;
        public string MemberTitle = string.Empty;
        public string MemberSurname = string.Empty;
        public string MemberGivenNames = string.Empty;
        public DateTime BirthDate = DateTime.Now;
        public string Gender = string.Empty;
        public string AccountDescription = string.Empty;
        public string BenefitCategoryDescription = string.Empty;
        public string AddressLine2 = string.Empty;
        public string AddressLine3 = string.Empty;
        public string AddressSuburb = string.Empty;
        public string AddressState = string.Empty;
        public string AddressPostcode = string.Empty;
        public string AddressCountry = string.Empty;
        public string CountryCode = string.Empty;
        public string AddressResLine2 = string.Empty;
        public string AddressResLine3 = string.Empty;
        public string AddressResSuburb = string.Empty;
        public string AddressResState = string.Empty;
        public string AddressResPostcode = string.Empty;
        public string AddressResCountry = string.Empty;
        public string MembeMemberHomePhonerCode = string.Empty;
        public string MemberWorkPhone = string.Empty;
        public string MemberFaxPhone = string.Empty;
        public string MemberMobilePhone = string.Empty;
        public string MemberEmail = string.Empty;
        public string OccupationDescription = string.Empty;
        public string AdviserCode = string.Empty;
        public StatusType StatusType = StatusType.None;

        public string FullName
        {
            get
            {
                string name = MemberGivenNames + " " + MemberSurname;
                return name.Replace("'", "''");
            }
        }

        public string CustomerNO
        {
            get
            {
                return ClientNumber + "-" + MemberCode;
            }
        }

        public string EntityName
        {
            get
            {

                string entityName = MemberTitle + " " + MemberGivenNames + " " + MemberSurname;
                if (BenefitCategoryDescription != null & BenefitCategoryDescription != string.Empty)
                    entityName = entityName + " (" + BenefitCategoryDescription + ")";

                return entityName.Replace("'", "''").Trim();
            }
        }

        public SMASuperEntity()
        {

        }

        public void ImportSMAServiceEntityRow(DataRow memberRow)
        {
            ClientNumber = memberRow["ClientNumber"].ToString();
            MemberCode = memberRow["MemberCode"].ToString();
            MemberTitle = memberRow["MemberTitle"].ToString();
            MemberSurname = memberRow["MemberSurname"].ToString();
            MemberGivenNames = memberRow["MemberGivenNames"].ToString();
            BirthDate = DateTime.Parse(memberRow["BirthDate"].ToString());
            Gender = memberRow["Gender"].ToString();
            AccountDescription = memberRow["AccountDescription"].ToString();
            BenefitCategoryDescription = memberRow["BenefitCategoryDescription"].ToString();
            AddressLine2 = memberRow["AddressLine2"].ToString();
            AddressLine3 = memberRow["AddressLine3"].ToString();
            AddressSuburb = memberRow["AddressSuburb"].ToString();
            AddressState = memberRow["AddressState"].ToString();
            AddressPostcode = memberRow["AddressPostcode"].ToString();
            AddressCountry = memberRow["AddressCountry"].ToString();
            CountryCode = memberRow["CountryCode"].ToString();
            AddressResLine2 = memberRow["AddressResLine2"].ToString();
            AddressResLine3 = memberRow["AddressResLine3"].ToString();
            AddressResSuburb = memberRow["AddressResSuburb"].ToString();
            AddressResState = memberRow["AddressResState"].ToString();
            AddressResPostcode = memberRow["AddressResPostcode"].ToString();
            AddressResCountry = memberRow["AddressResCountry"].ToString();
            MembeMemberHomePhonerCode = memberRow["MemberHomePhone"].ToString();
            MemberWorkPhone = memberRow["MemberWorkPhone"].ToString();
            MemberFaxPhone = memberRow["MemberFaxPhone"].ToString();
            MemberMobilePhone = memberRow["MemberMobilePhone"].ToString();
            MemberEmail = memberRow["MemberEmail"].ToString();
            OccupationDescription = memberRow["OccupationDescription"].ToString();
            AdviserCode = memberRow["AdviserCode"].ToString();
            if (memberRow["AccountStatus"].ToString().ToLower() == "active")
                StatusType = TaxSimp.Data.StatusType.Active;
            else
                StatusType = TaxSimp.Data.StatusType.Closed;

        }
    }

    public class ImportSMA : OrganizationWebCommandBase
    {
        public string EntityName = "EntityName";
        public string ClientCID = "EntityNClientCIDame";
        public string ClientID = "ClientID";
        public string SuperMemeberID = "MemeberID";
        public string SuperClientID = "SuperClientID";
        public string ResultTableName = "ResultTable";

        public override System.Data.DataSet DoAction(System.Data.DataSet value)
        {
            ImportProcessDS importProcessDS = (ImportProcessDS)value;
            ImportFromService(importProcessDS);
            return importProcessDS;
        }

        public DataTable ResultTable()
        {
            DataTable dt = new DataTable(ResultTableName);
            dt.Columns.Add(EntityName);
            dt.Columns.Add(ClientCID, typeof(Guid));
            dt.Columns.Add(ClientID);
            dt.Columns.Add(SuperMemeberID);
            dt.Columns.Add(SuperClientID);
            return dt;
        }

        public IOrganizationUnit CreateUpdateClientObjects(SMASuperEntity smaSuperEntity, DataSet resultData, Data.OrganisationListingDS organisationListingDS, ModelEntity modelEntity, ImportProcessDS importProcessDS, IOrganization orgCM, out Guid BankUnit)
        {
            DataRow importRow = importProcessDS.Tables[ResultTableName].NewRow();
            DataTable EclipseSuperTable = organisationListingDS.Tables[OrganisationListingDS.ECLIPSESUPERTABLENAME];

            DataRow existingMember = EclipseSuperTable.Select().Where(row => row["OTHERID_FIELD"].ToString() == smaSuperEntity.CustomerNO).FirstOrDefault();

            IOrganizationUnit orgUnit = null;
            ILogicalModule created = null;
            ILogicalModule organization = null;
            BankUnit = Guid.Empty;

            if (existingMember == null)
            {
                created = this.Organization.Broker.CreateLogicalCM(new Guid("E9D16670-D8F9-4A2D-844B-4EBE67E4629F"), smaSuperEntity.EntityName, Guid.Empty, null);
                organization = Organization.Broker.GetLogicalCM(Organization.CLID);
                organization.AddChildLogicalCM(created);
                orgUnit = (IOrganizationUnit)created[created.CurrentScenario];
                orgUnit.StatusType = smaSuperEntity.StatusType;

                orgUnit.CreateUniqueID();

                if (orgUnit.ClientEntity == null)
                    orgUnit.ClientEntity = new ClientIndividualEntity();

                importRow["Message"] = "New Account Added: " + orgUnit.ClientId + " - " + orgUnit.Name;
                importRow["HasErrors"] = false;
                importRow["IsMissingItem"] = false;
                Guid cid = orgUnit.CID;
                orgUnit.IsInvestableClient = true;
                orgUnit = Organization.Broker.GetBMCInstance(cid) as IOrganizationUnit;
                SetClientIndividualData(orgCM, smaSuperEntity, orgUnit, (ClientIndividualEntity)orgUnit.ClientEntity, ref created, ref organization);
                BankUnit = UpdateModel(smaSuperEntity.CustomerNO, modelEntity, importProcessDS, orgCM, orgUnit, (ClientIndividualEntity)orgUnit.ClientEntity);
                orgUnit.SuperClientID = smaSuperEntity.ClientNumber;
                orgUnit.SuperMemberID = smaSuperEntity.MemberCode;
                orgUnit.OtherID = smaSuperEntity.CustomerNO;
                orgUnit.CalculateToken(true);
                orgUnit.Broker.SetComplete();
                orgUnit.Broker.SetStart();
                importProcessDS.Tables[ResultTableName].Rows.Add(importRow);
            }

            return orgUnit;
        }

        public IOrganizationUnit UpdatedModel(SMASuperEntity smaSuperEntity, DataSet resultData, Data.OrganisationListingDS organisationListingDS, ModelEntity modelEntity, ImportProcessDS importProcessDS, IOrganization orgCM, out Guid BankUnit)
        {
            DataRow importRow = importProcessDS.Tables[ResultTableName].NewRow();
            DataTable EclipseSuperTable = organisationListingDS.Tables[OrganisationListingDS.ECLIPSESUPERTABLENAME];

            DataRow existingMember = EclipseSuperTable.Select().Where(row => row["ENTITYNAME_FIELD"].ToString() == smaSuperEntity.EntityName).FirstOrDefault();

            IOrganizationUnit orgUnit = null;
            BankUnit = Guid.Empty;
            orgUnit = Organization.Broker.GetBMCInstance(new Guid(existingMember["ENTITYCIID_FIELD"].ToString())) as IOrganizationUnit;
            orgUnit.IsInvestableClient = true;

            BankUnit = UpdateModel(smaSuperEntity.CustomerNO, modelEntity, importProcessDS, orgCM, orgUnit, (ClientIndividualEntity)orgUnit.ClientEntity);

            if (orgUnit.ClientEntity == null)
                orgUnit.ClientEntity = new ClientIndividualEntity();

            importRow["Message"] = "Account found and Updated: " + orgUnit.ClientId + " - " + orgUnit.Name;
            importRow["HasErrors"] = false;
            importRow["IsMissingItem"] = false;

            importProcessDS.Tables[ResultTableName].Rows.Add(importRow);
            return orgUnit;
        }

        protected void SetClientIndividualData(IOrganization orgCM, SMASuperEntity smaSuperEntity, IOrganizationUnit orgUnit, ClientIndividualEntity clientIndividualEntity, ref ILogicalModule created, ref ILogicalModule organization)
        {
            OrganisationListingDS organisationListingINDDS = new Data.OrganisationListingDS();
            organisationListingINDDS.OrganisationListingOperationType = OrganisationListingOperationType.IndividualListOnlyNames;
            orgCM.GetData(organisationListingINDDS);

            IndividualDS idividualDS = new IndividualDS();
            IOrganizationUnit individualCM = ProcessIndividualList(smaSuperEntity, ref created, ref organization, organisationListingINDDS, idividualDS);

            if (individualCM != null)
                SetMemberIndividualData(clientIndividualEntity, individualCM, smaSuperEntity);

            clientIndividualEntity.AccountDesignation = smaSuperEntity.AccountDescription;

            clientIndividualEntity.Address.MailingAddressSame = false;
            clientIndividualEntity.Address.RegisteredAddressSame = false;
            clientIndividualEntity.Address.DuplicateMailingAddressSame = false;

            SetClientEntityAddress(smaSuperEntity.AddressLine2, smaSuperEntity.AddressLine3, smaSuperEntity.AddressSuburb, smaSuperEntity.AddressState, smaSuperEntity.AddressPostcode, clientIndividualEntity.Address.BusinessAddress);
            SetClientEntityAddress(smaSuperEntity.AddressLine2, smaSuperEntity.AddressLine3, smaSuperEntity.AddressSuburb, smaSuperEntity.AddressState, smaSuperEntity.AddressPostcode, clientIndividualEntity.Address.RegisteredAddress);

            if (smaSuperEntity.AddressResLine2 != string.Empty)
            {
                SetClientEntityAddress(smaSuperEntity.AddressResLine2, smaSuperEntity.AddressResLine3, smaSuperEntity.AddressResSuburb, smaSuperEntity.AddressResState, smaSuperEntity.AddressResPostcode, clientIndividualEntity.Address.MailingAddress);
                SetClientEntityAddress(smaSuperEntity.AddressResLine2, smaSuperEntity.AddressResLine3, smaSuperEntity.AddressResSuburb, smaSuperEntity.AddressResState, smaSuperEntity.AddressResPostcode, clientIndividualEntity.Address.ResidentialAddress);
            }
            else
            {
                SetClientEntityAddress(smaSuperEntity.AddressLine2, smaSuperEntity.AddressLine3, smaSuperEntity.AddressSuburb, smaSuperEntity.AddressState, smaSuperEntity.AddressPostcode, clientIndividualEntity.Address.MailingAddress);
                SetClientEntityAddress(smaSuperEntity.AddressLine2, smaSuperEntity.AddressLine3, smaSuperEntity.AddressSuburb, smaSuperEntity.AddressState, smaSuperEntity.AddressPostcode, clientIndividualEntity.Address.ResidentialAddress);
            }

            clientIndividualEntity.Name = smaSuperEntity.EntityName;
            clientIndividualEntity.LegalName = smaSuperEntity.EntityName;
            orgUnit.IsInvestableClient = true;
        }

        protected IOrganizationUnit ProcessIndividualList(SMASuperEntity smaSuperEntity, ref ILogicalModule created, ref ILogicalModule organization, OrganisationListingDS organisationListingINDDS, IndividualDS idividualDS)
        {
            IEnumerable<DataRow> selectedIndRows = organisationListingINDDS.Tables[0].Select("ENTITYNAME_FIELD ='" + smaSuperEntity.FullName + "'");
            IOrganizationUnit individualCM = null;

            bool accountFound = false;

            foreach (DataRow row in selectedIndRows)
            {
                Guid ciid = new Guid(row["ENTITYCIID_FIELD"].ToString());
                individualCM = this.Organization.Broker.GetBMCInstance(ciid) as IOrganizationUnit;
                IndividualEntity individualEntity = individualCM.ClientEntity as IndividualEntity;
                if (individualEntity.EmailAddress == smaSuperEntity.MemberEmail)
                {
                    accountFound = true;
                    break;
                }
            }

            if (!accountFound)
            {
                created = Organization.Broker.CreateLogicalCM(new Guid("9EC83666-D774-4BFE-895C-1E8D42325C50"), smaSuperEntity.FullName, Guid.Empty, null);
                organization = Organization.Broker.GetLogicalCM(Organization.CLID);
                organization.AddChildLogicalCM(created);
                individualCM = (IOrganizationUnit)created[created.CurrentScenario];
                individualCM.ClientEntity = new IndividualEntity();
            }

            return individualCM;
        }

        protected void SetMemberIndividualData(ClientIndividualEntity clientIndividualEntity, IOrganizationUnit individualCM, SMASuperEntity smaSuperEntity)
        {
            Oritax.TaxSimp.Common.IndividualEntity individualEntity = individualCM.ClientEntity as Oritax.TaxSimp.Common.IndividualEntity;

            individualEntity.Surname = smaSuperEntity.MemberSurname;
            individualEntity.Name = smaSuperEntity.MemberGivenNames;
            individualEntity.MobilePhoneNumber = PhoneNumberEntity.MobileNumberConvertor(smaSuperEntity.MemberMobilePhone);
            individualEntity.WorkPhoneNumber = PhoneNumberEntity.PhoneNumberConvertor(smaSuperEntity.MemberWorkPhone);
            individualEntity.HomePhoneNumber = PhoneNumberEntity.PhoneNumberConvertor(smaSuperEntity.MembeMemberHomePhonerCode);
            individualEntity.PersonalTitle.Title = smaSuperEntity.MemberTitle;
            individualEntity.DOB = smaSuperEntity.BirthDate;

            individualEntity.EmailAddress = smaSuperEntity.MemberEmail;
            individualEntity.Occupation = smaSuperEntity.OccupationDescription;
            individualEntity.Facsimile = PhoneNumberEntity.PhoneNumberConvertor(smaSuperEntity.MemberFaxPhone);
            individualEntity.MailingAddressSame = false;

            if (smaSuperEntity.AddressResLine2 != string.Empty)
            {
                SetIndAddress(smaSuperEntity.AddressResLine2, smaSuperEntity.AddressResLine3, smaSuperEntity.AddressResSuburb, smaSuperEntity.AddressResState, smaSuperEntity.AddressResPostcode, individualEntity.MailingAddress);
                SetIndAddress(smaSuperEntity.AddressResLine2, smaSuperEntity.AddressResLine3, smaSuperEntity.AddressResSuburb, smaSuperEntity.AddressResState, smaSuperEntity.AddressResPostcode, individualEntity.ResidentialAddress);
            }
            else
            {
                SetIndAddress(smaSuperEntity.AddressLine2, smaSuperEntity.AddressLine3, smaSuperEntity.AddressSuburb, smaSuperEntity.AddressState, smaSuperEntity.AddressPostcode, individualEntity.MailingAddress);
                SetIndAddress(smaSuperEntity.AddressLine2, smaSuperEntity.AddressLine3, smaSuperEntity.AddressSuburb, smaSuperEntity.AddressState, smaSuperEntity.AddressPostcode, individualEntity.ResidentialAddress);
            }

            clientIndividualEntity.Applicants.Clear();
            Oritax.TaxSimp.Common.IdentityCMDetail identityCMDetail = new Common.IdentityCMDetail();
            identityCMDetail.Clid = individualCM.CLID;
            identityCMDetail.Csid = individualCM.CSID;
            identityCMDetail.Cid = individualCM.CID;
            identityCMDetail.IsPrimary = true;
            identityCMDetail.BusinessTitle = new TitleEntity();
            clientIndividualEntity.Applicants.Add(identityCMDetail);
        }

        protected Guid UpdateModel(string bankAccountNO, ModelEntity modelEntity, ImportProcessDS importProcessDS, IOrganization orgCM, IOrganizationUnit orgUnit, ClientIndividualEntity clientIndividualEntity)
        {
            Guid bankUnit = Guid.Empty;

            if (modelEntity != null && clientIndividualEntity.ListAccountProcess.Count == 0)
            {
                ClientModelsDetailsDS serviceTypes = new ClientModelsDetailsDS { CommandType = DatasetCommandTypes.Update };
                serviceTypes.DoItWithMe = true;

                DataRow dr;
                dr = serviceTypes.DoItWithMeTable.NewRow();
                dr[serviceTypes.DoItForMeTable.AUTHORIASTIONTYPE] = "D";
                dr[serviceTypes.DoItForMeTable.PROGRAMCODE] = modelEntity.ProgramCode;
                dr[serviceTypes.DoItForMeTable.PROGRAMNAME] = modelEntity.Name;
                serviceTypes.DoItWithMeTable.Rows.Add(dr);
                orgUnit.SetData(serviceTypes);
            }

            ClientIndividualEntity clientEntity = orgUnit.ClientEntity as ClientIndividualEntity;
            Guid csid = new Guid("b7ae4bb1-2143-4e56-b9ac-10cf24f7eae5");
            Guid clid = new Guid("7986b8ee-4509-44a5-ac8c-7ce59b59592d");

            foreach (AccountProcessTaskEntity accountProcessTaskEntity in clientEntity.ListAccountProcess)
            {
                if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.ManagedInvestmentSchemesAccount && accountProcessTaskEntity.LinkedEntity.Clid == Guid.Empty)
                {
                    accountProcessTaskEntity.LinkedEntity.Csid = csid;
                    accountProcessTaskEntity.LinkedEntity.Clid = clid;

                    var product = orgCM.Products.Where(pro => pro.ID == accountProcessTaskEntity.TaskID).FirstOrDefault();
                    if (product != null)
                    {
                        if (clientEntity.ManagedInvestmentSchemesAccounts.Count(ss => ss.Clid == accountProcessTaskEntity.LinkedEntity.Clid && ss.Csid == accountProcessTaskEntity.LinkedEntity.Csid && ss.FundID == product.FundAccounts.FirstOrDefault()) == 0)
                            clientEntity.ManagedInvestmentSchemesAccounts.Add(new Oritax.TaxSimp.Common.IdentityCMDetail { Clid = accountProcessTaskEntity.LinkedEntity.Clid, Csid = accountProcessTaskEntity.LinkedEntity.Csid, FundID = product.FundAccounts.FirstOrDefault() });
                    }
                }
                else if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.BankAccount)
                {
                    var product = orgCM.Products.Where(pro => pro.ID == accountProcessTaskEntity.TaskID).FirstOrDefault();
                    if (product != null && product.TaskDescription.ToLower().Contains("jp morgan"))
                    {
                        if (accountProcessTaskEntity.LinkedEntity.Clid == Guid.Empty && accountProcessTaskEntity.LinkedEntity.Cid == Guid.Empty)
                            AddNewCashAccount(bankAccountNO, orgUnit.SuperMemberID, orgUnit.SuperClientID, orgCM, orgUnit, importProcessDS, accountProcessTaskEntity, clientEntity.BankAccounts);


                        if (accountProcessTaskEntity.LinkedEntity.Clid != Guid.Empty && accountProcessTaskEntity.LinkedEntity.Cid == Guid.Empty)
                            bankUnit = Organization.Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid).CID;
                        else
                            bankUnit = accountProcessTaskEntity.LinkedEntity.Cid;
                    }
                }
            }

            return bankUnit;
        }

        protected void UpdateModelFiiG(string bankAccountNO, ImportProcessDS importProcessDS, IOrganization orgCM, IOrganizationUnit orgUnit, IClientEntityOrgUnits clientEntity)
        {
            foreach (AccountProcessTaskEntity accountProcessTaskEntity in clientEntity.ListAccountProcess)
            {
                if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.TermDepositAccount && accountProcessTaskEntity.LinkedEntity.Clid == Guid.Empty)
                {
                    var product = orgCM.Products.Where(pro => pro.ID == accountProcessTaskEntity.TaskID).FirstOrDefault();
                    if (product != null && product.TaskDescription.ToLower().Contains("fiig"))
                    {
                        if (accountProcessTaskEntity.LinkedEntity.Clid == Guid.Empty && accountProcessTaskEntity.LinkedEntity.Cid == Guid.Empty)
                            AddNewCashAccountTD(bankAccountNO, orgUnit.SuperMemberID, orgUnit.SuperClientID, orgCM, orgUnit, importProcessDS, accountProcessTaskEntity, (clientEntity as IClientUMAData).BankAccounts);

                    }
                }
            }
        }

        protected void UpdateModelDesktopBroker(string bankAccountNO, ImportProcessDS importProcessDS, IOrganization orgCM, IOrganizationUnit orgUnit, IClientEntityOrgUnits clientEntity)
        {
            foreach (AccountProcessTaskEntity accountProcessTaskEntity in clientEntity.ListAccountProcess)
            {
                if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.DesktopBrokerAccount)
                {
                    var product = orgCM.Products.Where(pro => pro.ID == accountProcessTaskEntity.TaskID).FirstOrDefault();
                    if (product != null && product.TaskDescription.ToLower().Contains("desktop"))
                    {
                        if (accountProcessTaskEntity.LinkedEntity.Clid == Guid.Empty && accountProcessTaskEntity.LinkedEntity.Cid == Guid.Empty)
                            this.AddNewDesktopBroker(bankAccountNO, orgUnit.SuperMemberID, orgUnit.SuperClientID, orgCM, orgUnit, importProcessDS, accountProcessTaskEntity, (clientEntity as IClientUMAData).DesktopBrokerAccounts);
                    }
                }
            }
        }

        protected void UpdateModelMIS(ModelEntity modelEntity, DataSet importProcessDS, IOrganization orgCM, IOrganizationUnit orgUnit, List<AccountProcessTaskEntity> ListAccountProcess, List<Oritax.TaxSimp.Common.IdentityCMDetail> ManagedInvestmentSchemesAccounts)
        {
            if (modelEntity != null && ListAccountProcess.Where(ap => ap.ModelID == modelEntity.ID).Count() == 0)
            {
                ClientModelsDetailsDS serviceTypes = new ClientModelsDetailsDS { CommandType = DatasetCommandTypes.Update };
                serviceTypes.DoItForMe = true;

                DataRow dr;
                dr = serviceTypes.DoItForMeTable.NewRow();
                dr[serviceTypes.DoItForMeTable.AUTHORIASTIONTYPE] = "D";
                dr[serviceTypes.DoItForMeTable.PROGRAMCODE] = modelEntity.ProgramCode;
                dr[serviceTypes.DoItForMeTable.PROGRAMNAME] = modelEntity.Name;
                serviceTypes.DoItForMeTable.Rows.Add(dr);
                orgUnit.SetData(serviceTypes);
            }

            Guid csid = new Guid("b7ae4bb1-2143-4e56-b9ac-10cf24f7eae5");
            Guid clid = new Guid("7986b8ee-4509-44a5-ac8c-7ce59b59592d");

            ManagedInvestmentSchemesAccounts.Clear();

            foreach (AccountProcessTaskEntity accountProcessTaskEntity in ListAccountProcess)
            {
                if (accountProcessTaskEntity.ModelID == modelEntity.ID && accountProcessTaskEntity.LinkedEntityType == OrganizationType.ManagedInvestmentSchemesAccount)
                {
                    accountProcessTaskEntity.IsCompleted = false;
                    accountProcessTaskEntity.LinkedEntity = new IdentityCM();
                    accountProcessTaskEntity.OldStatus = string.Empty;
                }
            }

            if (importProcessDS.Tables["InvestmentProfileAssetViewModel"] != null)
            {
                foreach (DataRow assetCodeRow in importProcessDS.Tables["InvestmentProfileAssetViewModel"].Rows)
                {
                    string assetCode = assetCodeRow["AssetLongCode"].ToString();

                    foreach (AccountProcessTaskEntity accountProcessTaskEntity in ListAccountProcess)
                    {
                        if (accountProcessTaskEntity.ModelID == modelEntity.ID && accountProcessTaskEntity.LinkedEntityType == OrganizationType.ManagedInvestmentSchemesAccount)
                        {
                            var product = orgCM.Products.Where(pro => pro.ID == accountProcessTaskEntity.TaskID).FirstOrDefault();

                            if (product != null && product.TaskDescription.Contains(assetCode))
                            {
                                var fundID = product.FundAccounts.FirstOrDefault();
                                var entiy = product.EntityId;

                                if (fundID != Guid.Empty && entiy != null && entiy != IdentityCM.Null && (accountProcessTaskEntity.LinkedEntity == null || accountProcessTaskEntity.LinkedEntity == IdentityCM.Null))
                                {
                                    accountProcessTaskEntity.LinkedEntity = new IdentityCM() { Clid = entiy.Clid, Csid = entiy.Csid, Cid = entiy.Cid };
                                    accountProcessTaskEntity.IsCompleted = true;
                                }

                                if (ManagedInvestmentSchemesAccounts != null && accountProcessTaskEntity.IsCompleted)
                                {
                                    if (ManagedInvestmentSchemesAccounts.Count(ss => ss.Clid == accountProcessTaskEntity.LinkedEntity.Clid && ss.Csid == accountProcessTaskEntity.LinkedEntity.Csid && ss.FundID == fundID) <= 0)
                                        ManagedInvestmentSchemesAccounts.Add(new Oritax.TaxSimp.Common.IdentityCMDetail { Clid = accountProcessTaskEntity.LinkedEntity.Clid, Csid = accountProcessTaskEntity.LinkedEntity.Csid, Cid = accountProcessTaskEntity.LinkedEntity.Cid, FundID = fundID });
                                }
                            }
                        }
                    }
                }
            }

        }

        protected void ClearMISForDIWMModel(ModelEntity modelEntity, IOrganizationUnit orgUnit, List<AccountProcessTaskEntity> ListAccountProcess, List<Oritax.TaxSimp.Common.IdentityCMDetail> ManagedInvestmentSchemesAccounts)
        {
            if (modelEntity != null && ListAccountProcess.Where(ap => ap.ModelID == modelEntity.ID).Count() == 0)
            {
                ClientModelsDetailsDS serviceTypes = new ClientModelsDetailsDS { CommandType = DatasetCommandTypes.Update };
                serviceTypes.DoItWithMe = true;

                DataRow dr;
                dr = serviceTypes.DoItWithMeTable.NewRow();
                dr[serviceTypes.DoItWithMeTable.AUTHORIASTIONTYPE] = "D";
                dr[serviceTypes.DoItWithMeTable.PROGRAMCODE] = modelEntity.ProgramCode;
                dr[serviceTypes.DoItWithMeTable.PROGRAMNAME] = modelEntity.Name;
                serviceTypes.DoItWithMeTable.Rows.Add(dr);
                orgUnit.SetData(serviceTypes);
            }

            Guid csid = new Guid("b7ae4bb1-2143-4e56-b9ac-10cf24f7eae5");
            Guid clid = new Guid("7986b8ee-4509-44a5-ac8c-7ce59b59592d");

            ManagedInvestmentSchemesAccounts.Clear();

            foreach (AccountProcessTaskEntity accountProcessTaskEntity in ListAccountProcess)
            {
                if (accountProcessTaskEntity.ModelID == modelEntity.ID && accountProcessTaskEntity.LinkedEntityType == OrganizationType.ManagedInvestmentSchemesAccount)
                {
                    accountProcessTaskEntity.IsCompleted = false;
                    accountProcessTaskEntity.LinkedEntity = new IdentityCM();
                    accountProcessTaskEntity.OldStatus = string.Empty;
                }
            }
        }

        protected void CreateUpdateTransactionsObjects(Guid asxCID, ICMBroker Broker, string clientID, Guid bankUnit, Guid bankUnitTD, string entityName, string memberID, string superClientID, SMAImportProcessDS resultData, OrganizationCM orgCM, List<SecuritiesEntity> Securities)
        {
            IBrokerManagedComponent manageFundBMC = Broker.GetBMCInstance(new Guid("b354e928-a3c2-43d7-b61e-feb215cf1bd3"));
            CreateUpdateManagedFunds(Broker, manageFundBMC, resultData, memberID, Securities, clientID);
            CreateUpdateCash(entityName, resultData, memberID, superClientID, bankUnit, Broker);
            CreateUpdateCashTD(entityName, resultData, memberID, superClientID, bankUnitTD, Broker);
            CreateUpdateASX(Broker, asxCID, resultData, memberID, clientID);
        }

        protected void CreateUpdateManagedFunds(ICMBroker Broker, IBrokerManagedComponent manageFundBMC, SMAImportProcessDS resultData, string memberCode, List<SecuritiesEntity> Securities, string clientID)
        {
            if (resultData.Tables[SMAImportProcessDS.MISTRANSACTIONTABLE] != null || resultData.Tables["InvestmentViewModel"] != null)
            {
                Broker.SaveOverride = true;
                manageFundBMC.SetData(resultData);
                Broker.SetComplete();
                Broker.SetStart();
            }
        }

        protected void CreateUpdateASX(ICMBroker Broker, Guid asxCID, SMAImportProcessDS resultData, string memberCode, string clientID)
        {
            IBrokerManagedComponent asxBMC = Broker.GetBMCInstance(asxCID);
            if (asxCID != Guid.Empty)
            {
                Broker.SaveOverride = true;
                asxBMC.SetData(resultData);
                Broker.SetComplete();
                Broker.SetStart();
            }
        }

        protected void CreateUpdateCash(string entityName, SMAImportProcessDS resultData, string memberID, string superClientID, Guid bankUnit, ICMBroker Broker)
        {
            if (resultData.Tables[SMAImportProcessDS.CASHTRANSACTIONTABLE] != null)
            {

                if (!resultData.Tables[SMAImportProcessDS.CASHTRANSACTIONTABLE].Columns.Contains("Message"))
                {
                    //set message to not found
                    DataColumn dc = new DataColumn("Message");
                    resultData.Tables[SMAImportProcessDS.CASHTRANSACTIONTABLE].Columns.Add(dc);
                }

                if (!resultData.Tables[SMAImportProcessDS.CASHTRANSACTIONTABLE].Columns.Contains("HasErrors"))
                {
                    //set has error to true
                    DataColumn dc = new DataColumn("HasErrors", typeof(bool)) { DefaultValue = false };
                    resultData.Tables[SMAImportProcessDS.CASHTRANSACTIONTABLE].Columns.Add(dc);
                }

                if (!resultData.Tables[SMAImportProcessDS.CASHTRANSACTIONTABLE].Columns.Contains("IsMissingItem"))
                {
                    //set Is Missing Item to true
                    DataColumn dc = new DataColumn("IsMissingItem", typeof(bool)) { DefaultValue = false };
                    resultData.Tables[SMAImportProcessDS.CASHTRANSACTIONTABLE].Columns.Add(dc);
                }

                IOrganizationUnit bankCM = Broker.GetBMCInstance(bankUnit) as IOrganizationUnit;

                if (bankCM != null)
                {
                    Broker.SaveOverride = true;
                    BankAccountEntity bankAccountEntity = bankCM.ClientEntity as BankAccountEntity;
                    bankCM.SetData(resultData);
                    Broker.SetComplete();
                    Broker.SetStart();
                }
            }
        }

        protected void CreateUpdateCashTD(string entityName, SMAImportProcessDS resultData, string memberID, string superClientID, Guid bankUnitTD, ICMBroker Broker)
        {
            if (resultData.Tables[SMAImportProcessDS.CASHTRANSACTIONTABLE] != null)
            {

                if (!resultData.Tables[SMAImportProcessDS.CASHTRANSACTIONTABLE].Columns.Contains("Message"))
                {
                    //set message to not found
                    DataColumn dc = new DataColumn("Message");
                    resultData.Tables[SMAImportProcessDS.CASHTRANSACTIONTABLE].Columns.Add(dc);
                }

                if (!resultData.Tables[SMAImportProcessDS.CASHTRANSACTIONTABLE].Columns.Contains("HasErrors"))
                {
                    //set has error to true
                    DataColumn dc = new DataColumn("HasErrors", typeof(bool)) { DefaultValue = false };
                    resultData.Tables[SMAImportProcessDS.CASHTRANSACTIONTABLE].Columns.Add(dc);
                }

                if (!resultData.Tables[SMAImportProcessDS.CASHTRANSACTIONTABLE].Columns.Contains("IsMissingItem"))
                {
                    //set Is Missing Item to true
                    DataColumn dc = new DataColumn("IsMissingItem", typeof(bool)) { DefaultValue = false };
                    resultData.Tables[SMAImportProcessDS.CASHTRANSACTIONTABLE].Columns.Add(dc);
                }

                IOrganizationUnit bankCM = Broker.GetBMCInstance(bankUnitTD) as IOrganizationUnit;

                if (bankCM != null)
                {
                    Broker.SaveOverride = true;
                    BankAccountEntity bankAccountEntity = bankCM.ClientEntity as BankAccountEntity;
                    bankCM.SetData(resultData);
                    Broker.SetComplete();
                    Broker.SetStart();
                }
            }
        }

        protected Guid AddNewCashAccount(string bankAccountNO, string memberID, string superClientID, IOrganization orgCM, IOrganizationUnit orgUnit, ImportProcessDS importProcessDS, AccountProcessTaskEntity accountProcessTaskEntity, List<Oritax.TaxSimp.Common.IdentityCMDetail> bankAccounts)
        {
            InstitutionEntity insEntity = orgCM.Institution.Where(ins => ins.Name.ToLower() == "jp morgan").FirstOrDefault();
            var bankaccountds = new BankAccountDS();
            bankaccountds.CommandType = DatasetCommandTypes.Add;
            var orgUnitEntity = new Oritax.TaxSimp.Data.OrganizationUnit
            {
                Name = EntityName,
                Type = ((int)OrganizationType.BankAccount).ToString(),
                CurrentUser = importProcessDS.Unit.CurrentUser
            };

            bankaccountds.Unit = orgUnitEntity;
            bankaccountds.Command = (int)WebCommands.AddNewOrganizationUnit;

            DataTable dataTable = bankaccountds.Tables[bankaccountds.BankAccountsTable.TABLENAME];
            DataRow dataRow = dataTable.NewRow();

            dataRow[bankaccountds.BankAccountsTable.CID] = Guid.Empty.ToString();
            dataRow[bankaccountds.BankAccountsTable.CLID] = Guid.Empty.ToString();
            dataRow[bankaccountds.BankAccountsTable.CSID] = Guid.Empty.ToString();
            dataRow[bankaccountds.BankAccountsTable.ACCOUNTNAME] = orgUnit.Name;
            dataRow[bankaccountds.BankAccountsTable.ACCOUNTNO] = "010113906";
            dataRow[bankaccountds.BankAccountsTable.ACCOUNTTYPE] = "CMA";
            dataRow[bankaccountds.BankAccountsTable.INSTITUTIONID] = insEntity.ID;
            dataRow[bankaccountds.BankAccountsTable.STATUS] = "ACTIVE";
            dataRow[bankaccountds.BankAccountsTable.BSBNO] = "212-200";
            dataRow[bankaccountds.BankAccountsTable.CUSTOMERNO] = bankAccountNO;
            dataRow[bankaccountds.BankAccountsTable.ISEXTERNALACCOUNT] = false;
            dataTable.Rows.Add(dataRow);
            bankaccountds.Tables.Remove(bankaccountds.BankAccountsTable.TABLENAME);
            bankaccountds.Tables.Add(dataTable);
            IBrokerManagedComponent ibmc = Organization.Broker.GetWellKnownBMC(WellKnownCM.Organization) as IBrokerManagedComponent;
            ibmc.SetData(bankaccountds);
            IBrokerManagedComponent bankBMC = this.Organization.Broker.GetBMCInstance(bankaccountds.Unit.Cid);
            bankBMC.SetData(bankaccountds);
            this.Organization.Broker.SetComplete();
            this.Organization.Broker.SetStart();
            this.Organization.Broker.SaveOverride = true;
            ibmc = Organization.Broker.GetBMCInstance(orgUnit.CID) as IBrokerManagedComponent;
            ibmc.SetData(bankaccountds);

            accountProcessTaskEntity.LinkedEntity.Cid = bankaccountds.Unit.Cid;
            accountProcessTaskEntity.LinkedEntity.Clid = bankaccountds.Unit.Clid;
            accountProcessTaskEntity.LinkedEntity.Csid = bankaccountds.Unit.Csid;

            if (bankAccounts.Count(ss => ss.Clid == accountProcessTaskEntity.LinkedEntity.Clid && ss.Csid == accountProcessTaskEntity.LinkedEntity.Csid) == 0)
            {
                Oritax.TaxSimp.Common.IdentityCMDetail bankIdentity = new Oritax.TaxSimp.Common.IdentityCMDetail();
                bankIdentity.Cid = accountProcessTaskEntity.LinkedEntity.Cid;
                bankIdentity.Clid = accountProcessTaskEntity.LinkedEntity.Clid;
                bankIdentity.Csid = accountProcessTaskEntity.LinkedEntity.Csid;
                bankAccounts.Add(bankIdentity);
            }

            return bankaccountds.Unit.Cid;
        }

        public Guid AddNewCashAccountTD(string bankAccountNO, string memberID, string superClientID, IOrganization orgCM, IOrganizationUnit orgUnit, ImportProcessDS importProcessDS, AccountProcessTaskEntity accountProcessTaskEntity, List<Oritax.TaxSimp.Common.IdentityCMDetail> bankAccounts)
        {
            InstitutionEntity insEntity = orgCM.Institution.Where(ins => ins.Name.ToLower() == "fiig").FirstOrDefault();
            var bankaccountds = new BankAccountDS();
            bankaccountds.CommandType = DatasetCommandTypes.Add;
            var orgUnitEntity = new Oritax.TaxSimp.Data.OrganizationUnit
            {
                Name = EntityName,
                Type = ((int)OrganizationType.BankAccount).ToString(),
                CurrentUser = importProcessDS.Unit.CurrentUser
            };

            bankaccountds.Unit = orgUnitEntity;
            bankaccountds.Command = (int)WebCommands.AddNewOrganizationUnit;

            DataTable dataTable = bankaccountds.Tables[bankaccountds.BankAccountsTable.TABLENAME];
            DataRow dataRow = dataTable.NewRow();
            this.Organization.Broker.SaveOverride = true;
            dataRow[bankaccountds.BankAccountsTable.CID] = Guid.Empty.ToString();
            dataRow[bankaccountds.BankAccountsTable.CLID] = Guid.Empty.ToString();
            dataRow[bankaccountds.BankAccountsTable.CSID] = Guid.Empty.ToString();
            dataRow[bankaccountds.BankAccountsTable.ACCOUNTNAME] = orgUnit.Name;
            dataRow[bankaccountds.BankAccountsTable.ACCOUNTNO] = superClientID + "-" + memberID;
            dataRow[bankaccountds.BankAccountsTable.ACCOUNTTYPE] = "Term Deposit";
            dataRow[bankaccountds.BankAccountsTable.INSTITUTIONID] = insEntity.ID;
            dataRow[bankaccountds.BankAccountsTable.STATUS] = "Active";
            dataRow[bankaccountds.BankAccountsTable.BSBNO] = "999-999";
            dataRow[bankaccountds.BankAccountsTable.CUSTOMERNO] = bankAccountNO;
            dataRow[bankaccountds.BankAccountsTable.ISEXTERNALACCOUNT] = false;
            dataTable.Rows.Add(dataRow);
            bankaccountds.Tables.Remove(bankaccountds.BankAccountsTable.TABLENAME);
            bankaccountds.Tables.Add(dataTable);
            IBrokerManagedComponent ibmc = Organization.Broker.GetWellKnownBMC(WellKnownCM.Organization) as IBrokerManagedComponent;
            ibmc.SetData(bankaccountds);
            IBrokerManagedComponent bankBMC = this.Organization.Broker.GetBMCInstance(bankaccountds.Unit.Cid);
            bankBMC.SetData(bankaccountds);
            ibmc = Organization.Broker.GetBMCInstance(orgUnit.CID) as IBrokerManagedComponent;
            ibmc.OtherID = superClientID + "-" + memberID;
            ibmc.SetData(bankaccountds);

            accountProcessTaskEntity.LinkedEntity.Cid = bankaccountds.Unit.Cid;
            accountProcessTaskEntity.LinkedEntity.Clid = bankaccountds.Unit.Clid;
            accountProcessTaskEntity.LinkedEntity.Csid = bankaccountds.Unit.Csid;

            if (bankAccounts.Count(ss => ss.Clid == accountProcessTaskEntity.LinkedEntity.Clid && ss.Csid == accountProcessTaskEntity.LinkedEntity.Csid) == 0)
            {
                Oritax.TaxSimp.Common.IdentityCMDetail bankIdentity = new Oritax.TaxSimp.Common.IdentityCMDetail();
                bankIdentity.Cid = accountProcessTaskEntity.LinkedEntity.Cid;
                bankIdentity.Clid = accountProcessTaskEntity.LinkedEntity.Clid;
                bankIdentity.Csid = accountProcessTaskEntity.LinkedEntity.Csid;
                bankAccounts.Add(bankIdentity);
            }

            return bankaccountds.Unit.Cid;
        }

        private DesktopBrokerAccountDS GetEntity(DesktopBrokerAccountDS desktopBrokerAccountDs, string accountName)
        {
            DataTable dataTable = desktopBrokerAccountDs.Tables[desktopBrokerAccountDs.DesktopBrokerAccountsTable.TABLENAME];
            DataRow dataRow = dataTable.NewRow();

            dataRow[desktopBrokerAccountDs.DesktopBrokerAccountsTable.CID] = Guid.Empty;
            dataRow[desktopBrokerAccountDs.DesktopBrokerAccountsTable.CLID] = Guid.Empty;
            dataRow[desktopBrokerAccountDs.DesktopBrokerAccountsTable.CSID] = Guid.Empty;
            dataRow[desktopBrokerAccountDs.DesktopBrokerAccountsTable.ACCOUNTNAME] = accountName;
            dataRow[desktopBrokerAccountDs.DesktopBrokerAccountsTable.ACCOUNTNO] = "173000";
            dataRow[desktopBrokerAccountDs.DesktopBrokerAccountsTable.ACCOUNTDESIGNATION] = "J P Morgan Nominees Australia Ltd";
            dataRow[desktopBrokerAccountDs.DesktopBrokerAccountsTable.HINNUMBER] = "X9999999999";
            dataRow[desktopBrokerAccountDs.DesktopBrokerAccountsTable.STATUS] = "Active";

            dataTable.Rows.Add(dataRow);
            desktopBrokerAccountDs.Tables.Remove(desktopBrokerAccountDs.DesktopBrokerAccountsTable.TABLENAME);
            desktopBrokerAccountDs.Tables.Add(dataTable);
            return desktopBrokerAccountDs;
        }

        public Guid AddNewDesktopBroker(string accountNo, string memberID, string superClientID, IOrganization orgCM, IOrganizationUnit orgUnit, ImportProcessDS importProcessDS, AccountProcessTaskEntity accountProcessTaskEntity, List<Oritax.TaxSimp.Common.IdentityCMDetail> desktopBrokerAccounts)
        {
            DesktopBrokerAccountDS desktopBrokerAccountDs = new DesktopBrokerAccountDS();
            desktopBrokerAccountDs.CommandType = DatasetCommandTypes.Add;
            desktopBrokerAccountDs.DesktopBrokerAccountsTable = GetEntity(desktopBrokerAccountDs, orgUnit.Name).DesktopBrokerAccountsTable;
            var unit = new Oritax.TaxSimp.Data.OrganizationUnit
            {
                Name = orgUnit.Name,
                Type = ((int)OrganizationType.DesktopBrokerAccount).ToString(),
                CurrentUser = importProcessDS.Unit.CurrentUser
            };

            desktopBrokerAccountDs.Unit = unit;
            desktopBrokerAccountDs.Command = (int)WebCommands.AddNewOrganizationUnit;
            desktopBrokerAccountDs.RepUnitCID = orgUnit.CID;
            desktopBrokerAccountDs.RepUnitID = orgUnit.ClientId;
            desktopBrokerAccountDs.RepUnitSuperID = memberID;

            //InstitutionEntity insEntity = orgCM.Institution.Where(ins => ins.Name.ToLower() == "fiig").FirstOrDefault();
            //var bankaccountds = new BankAccountDS();
            //bankaccountds.CommandType = DatasetCommandTypes.Add;
            //var orgUnitEntity = new Oritax.TaxSimp.Data.OrganizationUnit
            //{
            //    Name = EntityName,
            //    Type = ((int)OrganizationType.BankAccount).ToString(),
            //    CurrentUser = importProcessDS.Unit.CurrentUser
            //};
            this.Organization.Broker.SetStart();
            this.Organization.Broker.SaveOverride = true;
            orgCM.SetData(desktopBrokerAccountDs);

            accountProcessTaskEntity.LinkedEntity.Cid = desktopBrokerAccountDs.Unit.Cid;
            accountProcessTaskEntity.LinkedEntity.Clid = desktopBrokerAccountDs.Unit.Clid;
            accountProcessTaskEntity.LinkedEntity.Csid = desktopBrokerAccountDs.Unit.Csid;

            if (desktopBrokerAccounts.Count(ss => ss.Clid == accountProcessTaskEntity.LinkedEntity.Clid && ss.Csid == accountProcessTaskEntity.LinkedEntity.Csid) == 0)
            {
                Oritax.TaxSimp.Common.IdentityCMDetail asxEntity = new Oritax.TaxSimp.Common.IdentityCMDetail();
                asxEntity.Cid = accountProcessTaskEntity.LinkedEntity.Cid;
                asxEntity.Clid = accountProcessTaskEntity.LinkedEntity.Clid;
                asxEntity.Csid = accountProcessTaskEntity.LinkedEntity.Csid;
                desktopBrokerAccounts.Add(asxEntity);
            }

            if (desktopBrokerAccountDs.Unit != null)
                return desktopBrokerAccountDs.Unit.Cid;
            else
                return Guid.Empty;
        }

        protected static void SetClientEntityAddress(string AddressLine2, string AddressLine3, string AddressSuburb, string AddressState, string AddressPostcode, Oritax.TaxSimp.Common.AddressEntity addressEntity)
        {
            addressEntity.Addressline1 = AddressLine2; ;
            addressEntity.Addressline2 = AddressLine3;
            addressEntity.Country = "Australia";
            addressEntity.PostCode = AddressPostcode;
            addressEntity.State = AddressState;
            addressEntity.Suburb = AddressSuburb;
        }

        protected static void SetIndAddress(string AddressLine2, string AddressLine3, string AddressSuburb, string AddressState, string AddressPostcode, Oritax.TaxSimp.Common.AddressEntity addressEntity)
        {
            addressEntity.Addressline1 = AddressLine2; ;
            addressEntity.Addressline2 = AddressLine3;
            addressEntity.Country = "Australia";
            addressEntity.PostCode = AddressPostcode;
            addressEntity.State = AddressState;
            addressEntity.Suburb = AddressSuburb;
        }

        public virtual void ImportFromService(ImportProcessDS importProcessDS)
        {
            importProcessDS.Tables.Add(ResultTable());
            SetResults(importProcessDS);
        }

        protected void SetResults(ImportProcessDS importProcessDS)
        {
            DataTable resultTable = importProcessDS.Tables[this.ResultTableName];

            if (!resultTable.Columns.Contains(MaquarieCMATransactionsDS.MESSAGE))
            {
                var dc = new DataColumn(MaquarieCMATransactionsDS.MESSAGE, typeof(string));

                dc.DefaultValue = "Account Not found";
                resultTable.Columns.Add(dc);
            }

            if (!resultTable.Columns.Contains(MaquarieCMATransactionsDS.HASERRORS))
            {
                var dc = new DataColumn(MaquarieCMATransactionsDS.HASERRORS, typeof(bool));

                dc.DefaultValue = true;
                resultTable.Columns.Add(dc);
            }

            if (!resultTable.Columns.Contains(MaquarieCMATransactionsDS.ISMISSINGITEM))
            {
                var dc = new DataColumn(MaquarieCMATransactionsDS.ISMISSINGITEM, typeof(bool));

                dc.DefaultValue = true;
                resultTable.Columns.Add(dc);
            }
        }
    }
}
