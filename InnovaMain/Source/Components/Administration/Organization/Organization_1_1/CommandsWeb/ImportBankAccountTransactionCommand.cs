﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Enum;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Utilities;
using BankAccountEntity = Oritax.TaxSimp.Common.BankAccountEntity;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{
    public class ImportBankAccountTransactionCommand : OrganizationWebCommandBase
    {
        public override System.Data.DataSet DoAction(System.Data.DataSet value)
        {
            if (value.Tables.Count == 0)
                return value;
          
            #region cash transactions
            if (value.Tables.Contains("CashTransctions"))
            {
                if (!value.Tables["CashTransctions"].Columns.Contains("Message"))
                {
                    //set message to not found
                    DataColumn dc = new DataColumn("Message");
                    dc.DefaultValue = "Account Number not Found";
                    value.Tables["CashTransctions"].Columns.Add(dc);
                }

                if (!value.Tables["CashTransctions"].Columns.Contains("HasErrors"))
                {
                    //set has error to true
                    DataColumn dc = new DataColumn("HasErrors", typeof(bool));
                    dc.DefaultValue = true;
                    value.Tables["CashTransctions"].Columns.Add(dc);
                }

                if (!value.Tables["CashTransctions"].Columns.Contains("IsMissingItem"))
                {
                    //set Is Missing Item to true
                    DataColumn dc = new DataColumn("IsMissingItem", typeof(bool));
                    dc.DefaultValue = true;
                    value.Tables["CashTransctions"].Columns.Add(dc);
                }

                if (!value.Tables["CashTransctions"].Columns.Contains("ClientID"))
                {
                    //set Is Missing Item to true
                    DataColumn dc = new DataColumn("ClientID", typeof(string));
                    dc.DefaultValue = string.Empty;
                    value.Tables["CashTransctions"].Columns.Add(dc);
                }
            }

            #endregion

            #region Holding balance columns settings
            if (value.Tables.Contains("HoldingBalances"))
            {
                if (!value.Tables["HoldingBalances"].Columns.Contains("Message"))
                {
                    //set message to not found
                    DataColumn dc = new DataColumn("Message");
                    dc.DefaultValue = "Account Number not Found";
                    value.Tables["HoldingBalances"].Columns.Add(dc);
                }

                if (!value.Tables["HoldingBalances"].Columns.Contains("HasErrors"))
                {
                    //set has error to true
                    DataColumn dc = new DataColumn("HasErrors", typeof(bool));
                    dc.DefaultValue = true;
                    value.Tables["HoldingBalances"].Columns.Add(dc);
                }

                if (!value.Tables["HoldingBalances"].Columns.Contains("IsMissingItem"))
                {
                    //set Is Missing Item to true
                    DataColumn dc = new DataColumn("IsMissingItem", typeof(bool));
                    dc.DefaultValue = true;
                    value.Tables["HoldingBalances"].Columns.Add(dc);
                }
            }
            #endregion

            OrganizationListAction.ForEachClientOnly("Administrator",Organization,
                                           (csid, logical, bmc) =>
                                               {
                                                   bmc.GetData(value);
                                               });

            OrganizationListAction.ForEach("Administrator", OrganizationType.BankAccount, Organization,
                                           (csid, logical, bmc) =>
                                           {
                                               if (logical.CMType == OrganizationTypeList.GetEntity(OrganizationType.BankAccount))
                                               {
                                                   string accountNumber = bmc.GetDataStream((int)CmCommand.AccountNumber, string.Empty);
                                                   string bsb = bmc.GetDataStream((int)CmCommand.BSB, string.Empty);
                                                   if (!string.IsNullOrEmpty(bsb) && bsb.Contains('-'))
                                                       bsb = bsb.Split('-')[1];
                                                   string BsBNAccountNumber = bsb + accountNumber;
                                                   bool AccountFound = false;
                                                   if (value.Tables.Contains("CashTransctions"))
                                                   {
                                                       var drs = value.Tables["CashTransctions"].Select(string.Format("BSBNAccountNumber='{0}'", BsBNAccountNumber));
                                                       if (drs.Length > 0)
                                                       {
                                                           AccountFound = true;
                                                           foreach (var dr in drs)
                                                           {
                                                               dr["Message"] = "";
                                                               dr["HasErrors"] = false;
                                                               dr["IsMissingItem"] = false;

                                                           }
                                                       }

                                                   }

                                                   if (value.Tables.Contains("HoldingBalances"))
                                                   {
                                                       var drs = value.Tables["HoldingBalances"].Select(string.Format("BSBNAccountNumber='{0}'", BsBNAccountNumber));
                                                       if (drs.Length > 0)
                                                       {
                                                           AccountFound = true;
                                                           foreach (var dr in drs)
                                                           {
                                                               dr["Message"] = "";
                                                               dr["HasErrors"] = false;
                                                               dr["IsMissingItem"] = false;
                                                           }
                                                       }
                                                   }

                                                   if (AccountFound)
                                                   {
                                                       var unit = (bmc as OrganizationUnitCM);
                                                       unit.Broker.SetStart();
                                                       unit.SetData(value);
                                                       bmc.CalculateToken(true);
                                                   }
                                               }
                                           });

            //set dividends in Clients
            OrganizationListAction.ForEachClientOnly("Administrator", Organization,
                                           (csid, logical, bmc) =>
                                           {
                                               if ((bmc is OrganizationUnitCM) && (bmc as OrganizationUnitCM).IsInvestableClient)
                                               {
                                                   var unit = (bmc as OrganizationUnitCM);
                                                   unit.SetData(value);
                                               }

                                           });

            #region create Import message for import process
            
            if (!value.Tables.Contains(TransactionImportMessagesTable.Name))
            {
                TransactionImportMessagesTable resultTable = new TransactionImportMessagesTable();
                value.Tables.Add(resultTable);
            }
            else
            {
                value.Tables[TransactionImportMessagesTable.Name].Clear();
            }

            if (value.Tables.Contains("CashTransctions"))
            {
                foreach (DataRow datarow in value.Tables["CashTransctions"].Rows)
                {
                    DataRow dr = value.Tables[TransactionImportMessagesTable.Name].NewRow();
                    dr[TransactionImportMessagesTable.Message] = datarow[TransactionImportMessagesTable.Message];
                    dr[TransactionImportMessagesTable.HasErrors] = datarow[TransactionImportMessagesTable.HasErrors];
                    dr[TransactionImportMessagesTable.IsMissingItem] = datarow[TransactionImportMessagesTable.IsMissingItem];
                    dr[TransactionImportMessagesTable.InputMode] = "Transaction";
                    dr[TransactionImportMessagesTable.BankwestReferenceNumber] = datarow["BankwestReferenceNumber"];
                    dr[TransactionImportMessagesTable.BSBNAccountNumber] = datarow[TransactionImportMessagesTable.BSBNAccountNumber];
                    dr[TransactionImportMessagesTable.Date] = datarow["DateOfTransaction"];
                    dr[TransactionImportMessagesTable.Amount] = datarow["Amount"];
                    dr[TransactionImportMessagesTable.TransactionType] = datarow[TransactionImportMessagesTable.TransactionType];
                    dr[TransactionImportMessagesTable.TransactionDescription] = datarow[TransactionImportMessagesTable.TransactionDescription];
                    dr[TransactionImportMessagesTable.Type] = datarow["TransactionIndicator"];
                    value.Tables[TransactionImportMessagesTable.Name].Rows.Add(dr);
                }
            }

            if (value.Tables.Contains("HoldingBalances"))
            {
                foreach (DataRow datarow in value.Tables["HoldingBalances"].Rows)
                {
                    DataRow dr = value.Tables[TransactionImportMessagesTable.Name].NewRow();
                    dr[TransactionImportMessagesTable.Message] = datarow[TransactionImportMessagesTable.Message];
                    dr[TransactionImportMessagesTable.HasErrors] = datarow[TransactionImportMessagesTable.HasErrors];
                    dr[TransactionImportMessagesTable.IsMissingItem] = datarow[TransactionImportMessagesTable.IsMissingItem];
                    dr[TransactionImportMessagesTable.InputMode] = "Holding";
                    dr[TransactionImportMessagesTable.BSBNAccountNumber] = datarow[TransactionImportMessagesTable.BSBNAccountNumber];
                    dr[TransactionImportMessagesTable.TransactionDescription] = datarow["TransactionDescription"];
                    dr[TransactionImportMessagesTable.Date] = datarow["DateOfHolding"];
                    dr[TransactionImportMessagesTable.Amount] = datarow["HoldingAmount"];
                    dr[TransactionImportMessagesTable.UnsettledOrder] = datarow["UnsettledOrder"];
                    dr[TransactionImportMessagesTable.TransactionType] = datarow[TransactionImportMessagesTable.TransactionType];
                    dr[TransactionImportMessagesTable.BankwestReferenceNumber] = datarow[TransactionImportMessagesTable.BankwestReferenceNumber];
                    dr[TransactionImportMessagesTable.TransactionDescription] = datarow[TransactionImportMessagesTable.TransactionDescription];
                    dr[TransactionImportMessagesTable.Type] = datarow["TransactionIndicator"];
                    value.Tables[TransactionImportMessagesTable.Name].Rows.Add(dr);
                }
            }
            #endregion
            return value;
        }
        
    }
}
