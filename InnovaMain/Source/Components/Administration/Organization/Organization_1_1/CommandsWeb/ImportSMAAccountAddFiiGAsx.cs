﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Data;
using System.Data;
using Oritax.TaxSimp.CM.Organization.Commands;
using Oritax.TaxSimp.CM.Organization.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Calculation;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{
    public class ImportSMAAccountAddFiiGASX : ImportSMA
    {
        public override void ImportFromService(ImportProcessDS importProcessDS)
        {
            base.ImportFromService(importProcessDS);

            SMAWebService smaWebService = new SMAWebService();
            SMAImportProcessDS smaImportProcessDS = (SMAImportProcessDS)importProcessDS;
            smaImportProcessDS.Tables.Clear();
            smaImportProcessDS.AddBasicResultTableToDataSet();
            smaWebService.ResultDataSet = smaImportProcessDS;

            OrganisationListingDS organisationListingDSEclipseClient = new Data.OrganisationListingDS();
            organisationListingDSEclipseClient.OrganisationListingOperationType = OrganisationListingOperationType.EclipseSuper;
            Organization.GetData(organisationListingDSEclipseClient);
            DataTable EclipseSuperTable = organisationListingDSEclipseClient.Tables[OrganisationListingDS.ECLIPSESUPERTABLENAME];

            DataSet memberList = smaWebService.InvokeServiceMemberList(false);

            int count = 0;

            foreach (DataRow row in memberList.Tables["MemberSearchViewModel"].Rows)
            {
                string clientNumber = row["ClientNumber"].ToString();
                string memberCode = row["MemberCode"].ToString();
                string customerNo = clientNumber + "-" + memberCode;

                DataRow existingMember = EclipseSuperTable.Select().Where(crow => crow["OTHERID_FIELD"].ToString() == customerNo).FirstOrDefault();
                DataRow resultRow = smaWebService.ResultDataSet.Tables[SMAImportProcessDS.RESULTTABLE].NewRow();

                if (existingMember != null)
                {
                    this.Organization.Broker.SaveOverride = true;
                    IOrganizationUnit clientBMC = this.Organization.Broker.GetBMCInstance(new Guid(existingMember["ENTITYCIID_FIELD"].ToString())) as IOrganizationUnit;
                    if (clientBMC.StatusType == StatusType.Active)
                    {
                        UpdateModelFiiG(customerNo, importProcessDS, this.Organization as IOrganization, clientBMC, clientBMC.ClientEntity as IClientEntityOrgUnits);
                        UpdateModelDesktopBroker(customerNo, importProcessDS, this.Organization as IOrganization, clientBMC, clientBMC.ClientEntity as IClientEntityOrgUnits);
                        clientBMC.CalculateToken(true);
                        resultRow[SMAImportProcessDS.MESSAGE] = "Member Code: " + customerNo + " now has ASX and FiiG Accounts";
                        resultRow[SMAImportProcessDS.ISMISSINGITEM] = "false";
                        resultRow[SMAImportProcessDS.HASERRORS] = "false";

                        this.Organization.Broker.SetComplete();
                        this.Organization.Broker.SetStart();
                    }
                }
                else
                {
                    resultRow[SMAImportProcessDS.MESSAGE] = "Member Code: " + customerNo + " not found";
                    resultRow[SMAImportProcessDS.ISMISSINGITEM] = "true";
                    resultRow[SMAImportProcessDS.HASERRORS] = "true";
                }

                smaWebService.ResultDataSet.Tables[SMAImportProcessDS.RESULTTABLE].Rows.Add(resultRow);

                count++;
            }
            importProcessDS.MessageNumber = "SMA014";
            importProcessDS.MessageSummary = "Account Statuses updated successfully";
            importProcessDS.MessageDetail = "Account Statuses updated successfully. Please see details below";
        }
    }
}
