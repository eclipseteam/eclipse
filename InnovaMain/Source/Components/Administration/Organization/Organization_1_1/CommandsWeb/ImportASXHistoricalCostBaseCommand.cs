﻿using System;
using System.Data;
using System.Globalization;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using System.Collections.Generic;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Enum;
using System.Linq;
using Oritax.TaxSimp.CalculationInterface;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{
    public class ImportASXHistoricalCostBaseCommand : OrganizationWebCommandBase
    {
        public override DataSet DoAction(DataSet value)
        {
            if (value.Tables.Count == 0)
                return value;

            if (!value.Tables[0].Columns.Contains("Message"))
            {
                //set message to not found
                DataColumn dc = new DataColumn("Message");
                dc.DefaultValue = "Client Account Not Found";
                value.Tables[0].Columns.Add(dc);
            }

            if (!value.Tables[0].Columns.Contains("HasErrors"))
            {
                //set has error to true
                DataColumn dc = new DataColumn("HasErrors", typeof(bool));
                dc.DefaultValue = true;
                value.Tables[0].Columns.Add(dc);
            }

            if (!value.Tables[0].Columns.Contains("IsMissingItem"))
            {
                //set Is Missing Item to true
                DataColumn dc = new DataColumn("IsMissingItem", typeof(bool));
                dc.DefaultValue = true;
                value.Tables[0].Columns.Add(dc);
            }

            if (!value.Tables[0].Columns.Contains("Client Id"))
            {
                //set Is Missing Item to true
                DataColumn dc = new DataColumn("Client Id", typeof(Guid));

                value.Tables[0].Columns.Add(dc);
            }
            if (!value.Tables[0].Columns.Contains("Client Name"))
            {
                //set Is Missing Item to true
                DataColumn dc = new DataColumn("Client Name", typeof(Guid));

                value.Tables[0].Columns.Add(dc);
            }
            if (!value.Tables[0].Columns.Contains("Investment Code"))
            {
                //set Is Missing Item to true
                DataColumn dc = new DataColumn("Investment Code", typeof(Guid));

                value.Tables[0].Columns.Add(dc);
            }
            if (!value.Tables[0].Columns.Contains("Transaction Date"))
            {
                //set Is Missing Item to true
                DataColumn dc = new DataColumn("Transaction Date", typeof(Guid));

                value.Tables[0].Columns.Add(dc);
            }
            if (!value.Tables[0].Columns.Contains("Unit Price"))
            {
                //set Is Missing Item to true
                DataColumn dc = new DataColumn("Unit Price", typeof(string));

                value.Tables[0].Columns.Add(dc);
            }
            if (!value.Tables[0].Columns.Contains("Units "))
            {
                //set Is Missing Item to true
                DataColumn dc = new DataColumn("Units ", typeof(string));

                value.Tables[0].Columns.Add(dc);
            }
            if (!value.Tables[0].Columns.Contains("Brokerage"))
            {
                //set Is Missing Item to true
                DataColumn dc = new DataColumn("Brokerage", typeof(string));

                value.Tables[0].Columns.Add(dc);
            }
            if (!value.Tables[0].Columns.Contains("Net Value"))
            {
                //set Is Missing Item to true
                DataColumn dc = new DataColumn("Net Value", typeof(string));

                value.Tables[0].Columns.Add(dc);
            }

            if (!value.Tables[0].Columns.Contains("ASXCID"))
            {
                //set Is Missing Item to true
                DataColumn dc = new DataColumn("ASXCID", typeof(string));
                value.Tables[0].Columns.Add(dc);
            }




            #region Client Checks

            Guid Cid = Guid.Parse(value.ExtendedProperties["CID"].ToString());

            var unit = Organization.Broker.GetBMCInstance(Cid) as OrganizationUnitCM;

            DataRow[] drs = value.Tables[0].Select(string.Format("{0}='{1}'", "[Client Id]", unit.ClientId));

            string asxCID = string.Empty;
            foreach (DataRow dr in drs)
            {
                dr["Message"] = "";
                dr["HasErrors"] = false;
                dr["IsMissingItem"] = false;
                dr["Client Id"] = unit.ClientId;

                var invCode = Organization.Securities.Where(sec => sec.AsxCode == dr["Investment Code"].ToString()).FirstOrDefault();
                if (invCode != null)
                {
                    List<string> list = new List<string>();
                    list.Add(dr["Investment Code"].ToString());
                    list.Add("0");
                    string idslist = unit.GetDataStream((int)CmCommand.CheckASXInvestmentCode, list.ToXmlString());
                    SerializableDictionary<string, string> ids = idslist.ToNewOrData<SerializableDictionary<string, string>>();


                    if (string.IsNullOrEmpty(ids["ASXCID"]))
                    {
                        dr["Message"] = "Asx Account not found in Client";
                        dr["HasErrors"] = true;
                    }
                    else
                    {
                        asxCID = ids["ASXCID"];
                        dr["ASXCID"] = asxCID;
                    }
                }
                else
                {
                    dr["Message"] = "Invalid Investment Code";
                    dr["HasErrors"] = true;
                }
                ValidationRows(dr);

            }

            Organization.Broker.ReleaseBrokerManagedComponent(unit);

            #endregion Client Checks



            var unitO = Organization.Broker.GetBMCInstance(Guid.Parse(asxCID)) as OrganizationUnitCM;
            unitO.SetData(value);


            //OrganizationListAction.ForEach("Administrator", OrganizationType.DesktopBrokerAccount, Organization, (csid, logical, bmc) =>
            //{
            //    if (bmc is OrganizationUnitCM && OrganizationListAction.IsValidClientType(logical.CMTypeName))
            //    {
            //        var unitO = (bmc as OrganizationUnitCM);
            //        unitO.SetData(value);
            //    }
            //});


            //if (value.Tables[0].Columns.Contains("Client Id"))
            //{
            //    value.Tables[0].Columns.Remove("Client Id");

            //}
            //if (value.Tables[0].Columns.Contains("Client Name"))
            //{
            //    value.Tables[0].Columns.Remove("Client Name");

            //}
            //if (value.Tables[0].Columns.Contains("Investment Code"))
            //{
            //    value.Tables[0].Columns.Remove("Investment Code");

            //}

            //if (value.Tables[0].Columns.Contains("Transaction Date"))
            //{
            //    value.Tables[0].Columns.Remove("Transaction Date");
            //}
            //if (value.Tables[0].Columns.Contains("Unit Price"))
            //{
            //    value.Tables[0].Columns.Remove("Unit Price");

            //}
            //if (value.Tables[0].Columns.Contains("Units "))
            //{
            //    value.Tables[0].Columns.Remove("Units ");

            //}
            //if (value.Tables[0].Columns.Contains("Brokerage"))
            //{
            //    value.Tables[0].Columns.Remove("Brokerage");

            //}
            //if (value.Tables[0].Columns.Contains("Net Value"))
            //{
            //    value.Tables[0].Columns.Remove("Net Value");

            //}
            if (value.Tables[0].Columns.Contains("ASXCID"))
            {
                value.Tables[0].Columns.Remove("ASXCID");
            }

            return value;
        }



        private void ValidationRows(DataRow dr)
        {
            DateTime date;
            DateTimeFormatInfo info = new DateTimeFormatInfo();
            info.ShortDatePattern = "dd/MM/yyyy";

            string ClientId = dr["Client Id"].ToString();
            string ClientName = dr["Client Name"].ToString();
            string InvestmentCode = dr["Investment Code"].ToString();
            string TransactionDate = dr["Transaction Date"].ToString();
            string UnitPrice = dr["Unit Price"].ToString();
            string Units = dr["Units "].ToString();
            string Brokerage = dr["Brokerage"].ToString();
            string NetValue = dr["Net Value"].ToString();

            if (string.IsNullOrEmpty(ClientId))
            {
                dr["Message"] += "Invalid Client Id (" + ClientId + "), ";
                dr["HasErrors"] = true;
            }

            if (string.IsNullOrEmpty(ClientName))
            {
                dr["Message"] += "Client Name not found, ";
                dr["HasErrors"] = true;
            }

            if (string.IsNullOrEmpty(InvestmentCode))
            {
                dr["Message"] += "Invalid Investment Code, ";
                dr["HasErrors"] = true;
            }

            if (string.IsNullOrEmpty(TransactionDate) ||
                !DateTime.TryParse(TransactionDate, info, DateTimeStyles.None, out date))
            {
                dr["Message"] += "Invalid Transaction Date, ";
                dr["HasErrors"] = true;
            }

            if (string.IsNullOrEmpty(UnitPrice))
            {
                dr["Message"] += "Invalid Unit Price, ";
                dr["HasErrors"] = true;
            }

            if (string.IsNullOrEmpty(Units))
            {
                dr["Message"] += "Invalid Unit, ";
                dr["HasErrors"] = true;
            }

            if (string.IsNullOrEmpty(Brokerage))
            {
                dr["Message"] += "Invalid Brokerage, ";
                dr["HasErrors"] = true;
            }

            if (string.IsNullOrEmpty(NetValue))
            {
                dr["Message"] += "Invalid Net Value, ";
                dr["HasErrors"] = true;
            }
        }
    }
}
