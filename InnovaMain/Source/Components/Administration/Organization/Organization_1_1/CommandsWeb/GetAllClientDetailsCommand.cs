﻿using System.Data;
using System.Text.RegularExpressions;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.CM.Organization.Data;
using Oritax.TaxSimp.CalculationInterface;
using System;
using Oritax.TaxSimp.DataSets;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{

    public class GetAllClientDetailsCommand : OrganizationWebCommandBase
    {
        public override DataSet DoAction(DataSet value)
        {
            var ds = value as IHasOrganizationUnit;
            
            var organisationListingDS = new OrganisationListingDS();
            var user = this.Organization.Broker.GetBMCInstance(ds.Unit.CurrentUser.CurrentUserName, "DBUser_1_1");
            object[] args = new Object[3] { Organization.CLID, user.CID, EnableSecurity.SecuritySetting };
            var view = new DataView(Organization.Broker.GetBMCDetailList(OrganizationInstall.ASSEMBLY_NAME, args).Tables["Entities_Table"]);
            
            if (value is ExportStatusDS)
                view.RowFilter = organisationListingDS.GetRowFilterAllClientsExceptEclipse();
            else
                view.RowFilter = organisationListingDS.GetRowFilter();

            if (value is ISupportClientIDFilter)
            {
                var tempds = (value as ISupportClientIDFilter);
                if (!string.IsNullOrEmpty(tempds.ClientIDs))
                {
                    var r = new Regex("^[a-zA-Z0-9]*$");
                    string clientID = string.Empty;
                    var tempids=tempds.ClientIDs.Split(',');

                    foreach (var clientid in tempids)// for verification purpose
                    {
                        if (!string.IsNullOrEmpty(clientid) && r.IsMatch(clientid.Trim()))
                        clientID += string.Format("'{0}',",clientid.Trim());
                    }
                    

                    if(!string.IsNullOrEmpty(clientID))
                    {
                        clientID = clientID.Remove(clientID.Length - 1);

                        string filter = string.Format("ENTITYECLIPSEID_FIELD IN ({0})",clientID);
                        view.RowFilter = filter;
                    }
                }

            }
            view.Sort = "ENTITYNAME_FIELD ASC";
            var orgFiteredTable = view.ToTable();
            var count = orgFiteredTable.Rows.Count;

            if (value is ISupportRecordCount)
            {
                var tempds = (value as ISupportRecordCount);
                if (tempds.Count > 0 && tempds.Count <=count)
                {
                    count = tempds.Count;
                }

            }




            int i = 0;
            while(count>0 && i < orgFiteredTable.Rows.Count)
            {
                var row = orgFiteredTable.Rows[i];
                i++;
                var cid = new Guid(row["ENTITYCIID_FIELD"].ToString());
                var organizationUnit = Organization.Broker.GetBMCInstance(cid) as IOrganizationUnit;
                if (organizationUnit != null && organizationUnit.IsInvestableClient)
                {
                    count--;
                    organizationUnit.GetData(value);
                }
            }

            return value;
        }
    }
}
