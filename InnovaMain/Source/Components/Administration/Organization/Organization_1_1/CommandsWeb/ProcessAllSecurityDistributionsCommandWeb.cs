﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Enum;
using System.Collections.ObjectModel;
using System.Data;
using Oritax.TaxSimp.CM.Organization.Data;

namespace Oritax.TaxSimp.CM.Organization.Commands
{
    public class ProcessAllSecurityDistributionsCommandWeb : OrganizationCmCommandBase
    {
        public override string DoAction(string data)
        {
            IBrokerManagedComponent user = this.Organization.Broker.GetBMCInstance("Administrator", "DBUser_1_1");
            object[] args = new Object[3] { Organization.CLID, user.CID, EnableSecurity.SecuritySetting };
            DataView view = new DataView(this.Organization.Broker.GetBMCDetailList(OrganizationInstall.ASSEMBLY_NAME, args).Tables[OrganisationListingDS.TableName]);
            view.RowFilter = new OrganisationListingDS().GetRowFilter();
            view.Sort = "ENTITYNAME_FIELD ASC";
            DataTable orgFiteredTable = view.ToTable();

            foreach (DataRow clientRow in orgFiteredTable.Rows)
            {
        IOrganizationUnit orgUnit = Organization.Broker.GetBMCInstance(new Guid(clientRow["ENTITYCIID_FIELD"].ToString())) as IOrganizationUnit;

                if ((orgUnit is IOrganizationUnit) && (orgUnit as IOrganizationUnit).IsInvestableClient)
                {
                    var securities = Organization.Securities;

                    if (data != string.Empty)
                    {
                        securities = Organization.Securities.Where(ss => ss.ID == Guid.Parse(data)).ToList();
                    }

                    foreach (var security in securities)
                    {
                        var products = new List<ProductEntity>();
                        foreach (var prodsec in Organization.ProductSecurities)
                        {
                            if (prodsec.Details != null)
                            {
                                var details = prodsec.Details.Where(detal => detal.SecurityCodeId == security.ID).FirstOrDefault();
                                if (details != null)
                                {
                                    products.AddRange(Organization.Products.Where(produt => produt.ProductSecuritiesId == prodsec.ID));
                                }
                            }
                        }
                        if (security.DistributionCollection != null && security.DistributionCollection.Count > 0)
                        {
                            var organizationUnitCm = orgUnit as OrganizationUnitCM;
                            organizationUnitCm.Broker.SetStart();
                            foreach (var distributionEntity in security.DistributionCollection)
                            {
                                distributionEntity.Code = security.AsxCode;
                                distributionEntity.ProcessFlag = true;


                                if (organizationUnitCm != null) organizationUnitCm.ProcessDistribution(distributionEntity, products, security);
                            }
                            orgUnit.CalculateToken(true);
                        }

                    }

                }
            }

            return string.Empty;
        }
    }
}
