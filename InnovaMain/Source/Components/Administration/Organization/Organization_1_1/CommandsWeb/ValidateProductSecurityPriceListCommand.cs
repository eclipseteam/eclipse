﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Enum;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Utilities;
using BankAccountEntity = Oritax.TaxSimp.Common.BankAccountEntity;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{

    public class ValidateProductSecurityPriceListCommand : OrganizationWebCommandBase
    {

        public override System.Data.DataSet DoAction(System.Data.DataSet value)
        {
            if (value.Tables.Count == 0)
                return value;

            if (!value.Tables[0].Columns.Contains("Message"))
            {
                //set message to not found
                DataColumn dc = new DataColumn("Message");
                dc.DefaultValue = "";
                value.Tables[0].Columns.Add(dc);
            }

            if (!value.Tables[0].Columns.Contains("HasErrors"))
            {
                //set has error to true
                DataColumn dc = new DataColumn("HasErrors", typeof(bool));
                dc.DefaultValue = false;
                value.Tables[0].Columns.Add(dc);
            }

            if (!value.Tables[0].Columns.Contains("IsMissingItem"))
            {
                //set Is Missing Item to true
                DataColumn dc = new DataColumn("IsMissingItem", typeof(bool));
                dc.DefaultValue = true;
                value.Tables[0].Columns.Add(dc);
            }
            
            foreach (DataRow dr in value.Tables[0].Rows)
            {
                ValidationRow(dr, Guid.Parse(value.ExtendedProperties["ProductSecurityID"].ToString()));                
            }                     
                   

            return value;
        }
        private void ValidationRow(DataRow dr, Guid productSecuirtyID )
        {           
            DateTimeFormatInfo info = new DateTimeFormatInfo();
            double utemp;
            info.ShortDatePattern = "dd/MM/yyyy";

            if (Organization.Securities.Count(sec => sec.AsxCode == dr["StockCode"].ToString()) == 0)
            {
                dr["Message"] += "Stock code (" + dr["StockCode"].ToString() + ") doesn't exists,";
                //dr["HasErrors"] = false;
                dr["isMissingItem"] = true;
            }
            else
            {
                dr["isMissingItem"] = false;
            }
            if (string.IsNullOrEmpty(dr["Currency"].ToString()))
            {
                dr["Message"] += "Invalid Currency,";
                dr["HasErrors"] = true;
            }

            if (string.IsNullOrEmpty(dr["Market"].ToString()))
            {
                dr["Message"] += "Invalid market,";
                dr["HasErrors"] = true;
            }


            if (string.IsNullOrEmpty(dr["Recommendation"].ToString()))
            {
                dr["Message"] += "Invalid Recommendation,";
                dr["HasErrors"] = true;
            }
            if (string.IsNullOrEmpty(dr["DynamicRatingOption"].ToString()))
            {
                dr["Message"] += "Invalid dynamic rating option,";
                dr["HasErrors"] = true;
            }
            if (!double.TryParse(dr["Weighting"].ToString(), out utemp))
            {
                dr["Message"] += "Invalid weighting,";
                dr["HasErrors"] = true;
            }

            if (!double.TryParse(dr["BuyPrice"].ToString(), out utemp))
            {
                dr["Message"] += "Invalid buy price,";
                dr["HasErrors"] = true; 
            }

            if (!double.TryParse(dr["SellPrice"].ToString(), out utemp))
            {
                dr["Message"] += "Invalid sell price,";
                dr["HasErrors"] = true;
            }
            
            var productSec = Organization.ProductSecurities.Where(ps => ps.ID == productSecuirtyID).FirstOrDefault();
            if (productSec == null)
            {
                dr["Message"] += "Product security not found,";
                dr["HasErrors"] = true; 
            }

            var secuirtyEntity = Organization.Securities.Where(sec => sec.AsxCode == dr["StockCode"].ToString()).FirstOrDefault();
            if (secuirtyEntity != null)
            {
                if (productSec != null && productSec.Details != null && productSec.Details.Count(det => det.SecurityCodeId == secuirtyEntity.ID) > 0)
                {
                    if(!bool.Parse( dr["HasErrors"].ToString()))
                    dr["Message"] += "Already exists. Price for stock code will be updated,";
                    //dr["HasErrors"] = false;   
                }

            }
                     
        }     
       
    }
}
