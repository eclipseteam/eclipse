﻿using System.Data;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{

    public class UpdateAllClientDetailsCommand : OrganizationWebCommandBase
    {
        public override DataSet DoAction(DataSet value)
        {
            IHasOrganizationUnit ds = value as IHasOrganizationUnit;
            
            OrganizationListAction.ForEachClientOnly(ds.Unit.CurrentUser.CurrentUserName,  Organization, (csid, logical, bmc) =>
            {
                if ((bmc is OrganizationUnitCM) && (bmc as OrganizationUnitCM).IsInvestableClient)
                {
                    var unit = (bmc as OrganizationUnitCM);
                    unit.SetData(value);
                 }
            });
            return value;
        }
    }
}
