﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Data;
using System.Data;
using Oritax.TaxSimp.CM.Organization.Commands;
using Oritax.TaxSimp.CM.Organization.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.Commands;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{
    [Serializable]
    public class ImportSMAClientMembersSetOtherID : ImportSMA
    {
        public override void ImportFromService(ImportProcessDS importProcessDS)
        {
            base.ImportFromService(importProcessDS);
            SMAImportProcessDS smaImportProcessDS = (SMAImportProcessDS)importProcessDS;

            string userName = "Administrator";
            IBrokerManagedComponent user = this.Organization.Broker.GetBMCInstance(userName, "DBUser_1_1");
            object[] args = new Object[3] { Organization.CLID, user.CID, EnableSecurity.SecuritySetting };

            SMAWebService smaWebService = new SMAWebService();
            importProcessDS.Tables.Clear();
            importProcessDS.Tables.Add(new DataTable("ResultTable"));

            string memberCode = importProcessDS.Variables["MemberCode"].ToString();

            string eclipseID = "";
            
            if (importProcessDS.Variables.ContainsKey("EclipseID"))
                eclipseID = importProcessDS.Variables["EclipseID"].ToString();
          
            if (!string.IsNullOrWhiteSpace(eclipseID))
            {
                string OrgUnitCid = "";
                if (importProcessDS.Variables.ContainsKey("OrgCid"))
                    OrgUnitCid = importProcessDS.Variables["OrgCid"].ToString();
                if (!string.IsNullOrWhiteSpace(OrgUnitCid))
                {
                    IOrganizationUnit orgUnit = this.Organization.Broker.GetBMCInstance(new Guid(OrgUnitCid)) as IOrganizationUnit;
                    if (orgUnit != null)
                    {

                        IClientUMAData clientUMAData = orgUnit.ClientEntity as IClientUMAData;
                       
                        foreach (var item in clientUMAData.BankAccounts)
                        {
                            Organization.Broker.SaveOverride = true; 
                            Organization.Broker.SetStart(); 
                            IBrokerManagedComponent bankCM = Organization.Broker.GetCMImplementation(item.Clid, item.Csid);
                            bankCM.OtherID = orgUnit.OtherID;
                            bankCM.CalculateToken(true);
                            Organization.Broker.SetComplete();
                            Organization.Broker.SetStart(); 
                        }

                        foreach (var item in clientUMAData.DesktopBrokerAccounts)
                        {
                            Organization.Broker.SaveOverride = true;
                            Organization.Broker.SetStart();
                            IBrokerManagedComponent asxCM = Organization.Broker.GetCMImplementation(item.Clid, item.Csid);
                            asxCM.OtherID = orgUnit.OtherID;
                            asxCM.CalculateToken(true);
                            Organization.Broker.SetComplete();
                            Organization.Broker.SetStart();
                        }
                    }
                }
            }

            importProcessDS.MessageNumber = "SMA021";
            importProcessDS.MessageSummary = "Set Other ID Successfully";
            importProcessDS.MessageDetail = "Set Other ID Successfully";
        }
    }
}
