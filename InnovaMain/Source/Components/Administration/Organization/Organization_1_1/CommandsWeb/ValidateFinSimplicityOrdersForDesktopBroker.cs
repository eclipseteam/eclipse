﻿using System.Collections.Generic;
using System.Data;
using System.Globalization;
using Oritax.TaxSimp.Common;
using System;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Enum;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.Organization.Data;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{

    public class ValidateFinSimplicityOrdersForDesktopBroker : OrganizationWebCommandBase
    {

        public override DataSet DoAction(DataSet value)
        {
            if (value.Tables.Count == 0)
                return value;

            if (!value.Tables["Desktop Broker Rebookings"].Columns.Contains("Message"))
            {
                //set message to not found
                DataColumn dc = new DataColumn("Message");
                dc.DefaultValue = "Client Account  Not Found";
                value.Tables["Desktop Broker Rebookings"].Columns.Add(dc);
            }

            if (!value.Tables["Desktop Broker Rebookings"].Columns.Contains("HasErrors"))
            {
                //set has error to true
                DataColumn dc = new DataColumn("HasErrors", typeof(bool));
                dc.DefaultValue = true;
                value.Tables["Desktop Broker Rebookings"].Columns.Add(dc);
            }

            if (!value.Tables["Desktop Broker Rebookings"].Columns.Contains("IsMissingItem"))
            {
                //set Is Missing Item to true
                DataColumn dc = new DataColumn("IsMissingItem", typeof(bool));
                dc.DefaultValue = true;
                value.Tables["Desktop Broker Rebookings"].Columns.Add(dc);
            }
            if (!value.Tables["Desktop Broker Rebookings"].Columns.Contains("ClientCID"))
            {
                //set Is Missing Item to true
                DataColumn dc = new DataColumn("ClientCID", typeof(Guid));

                value.Tables["Desktop Broker Rebookings"].Columns.Add(dc);
            }
            if (!value.Tables["Desktop Broker Rebookings"].Columns.Contains("ProductID"))
            {
                //set Is Missing Item to true
                DataColumn dc = new DataColumn("ProductID", typeof(Guid));

                value.Tables["Desktop Broker Rebookings"].Columns.Add(dc);
            }
            if (!value.Tables["Desktop Broker Rebookings"].Columns.Contains("ASXCID"))
            {
                //set Is Missing Item to true
                DataColumn dc = new DataColumn("ASXCID", typeof(Guid));

                value.Tables["Desktop Broker Rebookings"].Columns.Add(dc);
            }
            if (!value.Tables["Desktop Broker Rebookings"].Columns.Contains("AccountCID"))
            {
                //set Is Missing Item to true
                DataColumn dc = new DataColumn("AccountCID", typeof(Guid));

                value.Tables["Desktop Broker Rebookings"].Columns.Add(dc);
            }
            if (!value.Tables["Desktop Broker Rebookings"].Columns.Contains("AccountNo"))
            {
                //set Is Missing Item to true
                DataColumn dc = new DataColumn("AccountNo", typeof(string));

                value.Tables["Desktop Broker Rebookings"].Columns.Add(dc);
            }
            if (!value.Tables["Desktop Broker Rebookings"].Columns.Contains("AccountBSB"))
            {
                //set Is Missing Item to true
                DataColumn dc = new DataColumn("AccountBSB", typeof(string));

                value.Tables["Desktop Broker Rebookings"].Columns.Add(dc);
            }
            if (!value.Tables["Desktop Broker Rebookings"].Columns.Contains("ASXAccountNo"))
            {
                //set Is Missing Item to true
                DataColumn dc = new DataColumn("ASXAccountNo", typeof(string));

                value.Tables["Desktop Broker Rebookings"].Columns.Add(dc);
            }

            if (!value.Tables["Desktop Broker Rebookings"].Columns.Contains("IsSecurityInSystem"))
            {
                //set Is Missing Item to true
                DataColumn dc = new DataColumn("IsSecurityInSystem", typeof(string));

                value.Tables["Desktop Broker Rebookings"].Columns.Add(dc);
            }

            OrganizationListAction.ForEach("Administrator", OrganizationType.Order, Organization, (csid, logical, bmc) =>
            {
                if (bool.Parse(bmc.GetDataStream((int)CmCommand.MatchFileName, (value as IHasFileName).FileName)))
                {
                    value.ExtendedProperties.Add("Result", OperationResults.Failed);
                    value.ExtendedProperties.Add("Message", "File already imported");
                }
            });

            IBrokerManagedComponent user = Organization.Broker.GetBMCInstance(this.Organization.Broker.UserContext.Identity.Name, "DBUser_1_1");
            object[] args = new Object[3] { Organization.CLID, user.CID, EnableSecurity.SecuritySetting };
            DataView entitiesView = new DataView(Organization.Broker.GetBMCDetailList(OrganizationInstall.ASSEMBLY_NAME, args).Tables[OrganisationListingDS.TableName]);
            entitiesView.RowFilter = new OrganisationListingDS().GetRowFilterAllClientsExceptEclipse();
            DataTable entitiesTable = entitiesView.ToTable();

            foreach (DataRow entityRow in entitiesTable.Rows)
            {
                DataRow[] drs = value.Tables["Desktop Broker Rebookings"].Select(string.Format("{0}='{1}'", "[BOOKED A/C]", entityRow["ENTITYECLIPSEID_FIELD"].ToString()));

                if (drs.Length > 0)
                {
                    foreach (var dr in drs)
                    {
                        IOrganizationUnit iorgUnit = this.Organization.Broker.GetBMCInstance(new Guid(entityRow["ENTITYCIID_FIELD"].ToString())) as IOrganizationUnit;

                        dr["Message"] = "";
                        dr["HasErrors"] = false;
                        dr["IsMissingItem"] = false;
                        dr["ClientCID"] = iorgUnit.CID;
                        List<string> list = new List<string>();
                        list.Add(dr["STOCK"].ToString());
                        list.Add(dr["PARENT A/C"].ToString());
                        string idslist = iorgUnit.GetDataStream((int)CmCommand.AccountProcessInvestmentCodeProductID, list.ToXmlString());
                        SerializableDictionary<string, string> ids = idslist.ToNewOrData<SerializableDictionary<string, string>>();
                        dr["ASXCID"] = string.IsNullOrEmpty(ids["ASXCID"].ToString()) ? Guid.Empty : Guid.Parse(ids["ASXCID"].ToString());
                        dr["ProductID"] = string.IsNullOrEmpty(ids["ProductID"].ToString()) ? Guid.Empty : Guid.Parse(ids["ProductID"].ToString());
                        dr["AccountCID"] = string.IsNullOrEmpty(ids["AccountCID"].ToString()) ? Guid.Empty : Guid.Parse(ids["AccountCID"].ToString());
                        dr["AccountNo"] = ids["AccountNo"];
                        dr["AccountBSB"] = ids["AccountBSB"];
                        dr["ASXAccountNo"] = ids["ASXAccountNo"];
                        dr["IsSecurityInSystem"] = ids["IsSecurityInSystem"];
                        ValidationRows(dr);
                    }
                }
            }

         
            if (value.Tables["Desktop Broker Rebookings"].Columns.Contains("ClientCID"))
            {
                value.Tables["Desktop Broker Rebookings"].Columns.Remove("ClientCID");

            }
            if (value.Tables["Desktop Broker Rebookings"].Columns.Contains("ProductID"))
            {
                value.Tables["Desktop Broker Rebookings"].Columns.Remove("ProductID");

            }
            if (value.Tables["Desktop Broker Rebookings"].Columns.Contains("ASXCID"))
            {
                value.Tables["Desktop Broker Rebookings"].Columns.Remove("ASXCID");

            }
            if (value.Tables["Desktop Broker Rebookings"].Columns.Contains("AccountCID"))
            {
                value.Tables["Desktop Broker Rebookings"].Columns.Remove("AccountCID");
            }
            if (value.Tables["Desktop Broker Rebookings"].Columns.Contains("AccountNo"))
            {
                value.Tables["Desktop Broker Rebookings"].Columns.Remove("AccountNo");
            }

            if (value.Tables["Desktop Broker Rebookings"].Columns.Contains("AccountBSB"))
            {
                value.Tables["Desktop Broker Rebookings"].Columns.Remove("AccountBSB");

            }
            if (value.Tables["Desktop Broker Rebookings"].Columns.Contains("ASXAccountNo"))
            {
                value.Tables["Desktop Broker Rebookings"].Columns.Remove("ASXAccountNo");

            }
            if (value.Tables["Desktop Broker Rebookings"].Columns.Contains("IsSecurityInSystem"))
            {
                value.Tables["Desktop Broker Rebookings"].Columns.Remove("IsSecurityInSystem");

            }
            return value;
        }



        private void ValidationRows(DataRow dr)
        {

            string parentACCID = dr["ASXAccountNo"].ToString();
            string BookedACC = dr["BOOKED A/C"].ToString();
            string BuySell = dr["B/S"].ToString();
            string QTY = dr["QTY"].ToString();
            string Stock = dr["STOCK"].ToString();
            string PriceAVG = dr["PRICE/AVG"].ToString();
            DateTime date;
            DateTimeFormatInfo info = new DateTimeFormatInfo();
            info.ShortDatePattern = "dd/MM/yyyy";
            string TradeAsAtDate = dr["Trade As At Date"].ToString();
            string ClientAccountName = dr["Client Account Name"].ToString();
            Guid productID = Guid.Parse(dr["ProductID"].ToString());
            Guid ASXCID = Guid.Parse(dr["ASXCID"].ToString());
            bool iSSecurityFound = bool.Parse(dr["IsSecurityInSystem"].ToString());

            if (string.IsNullOrEmpty(Stock))
            {
                dr["Message"] += "Invalid ASX Code (" + Stock + "), ";
                dr["HasErrors"] = true;
            }
            else if (!iSSecurityFound)
            {
                dr["Message"] += "ASX Code (" + Stock + ") not found in system, ";
                dr["HasErrors"] = true;
            }
            if (ASXCID == Guid.Empty)
            {
                dr["Message"] += "Client ASX account not found, ";
                dr["HasErrors"] = true;
            }
            //if (productID == Guid.Empty && (iSSecurityFound))
            //{
            //    dr["Message"] += "Client does not contain Investment code (" + Stock + ") in Account Process,";
            //    dr["HasErrors"] = true;
            //}

            //if (string.IsNullOrEmpty(parentACCID))
            //{
            //    dr["Message"] += "ASX account number is empty,";
            //    dr["HasErrors"] = true;
            //}


            if (string.IsNullOrEmpty(BookedACC))
            {
                dr["Message"] += "Invalid Client ID (Booked A/C), ";
                dr["HasErrors"] = true;
            }

            if (!DateTime.TryParse(TradeAsAtDate, info, DateTimeStyles.None, out date))
            {
                dr["Message"] += "Invalid Trade Date, ";
                dr["HasErrors"] = true;
            }

            if (string.IsNullOrEmpty(ClientAccountName))
            {
                dr["Message"] += "Invalid Client Account Name, ";
                dr["HasErrors"] = true;
            }

            OrderItemType ordertype = OrderItemType.Buy;
            bool hasOrderType = true;
            if (string.IsNullOrEmpty(BuySell) || !System.Enum.TryParse(BuySell, true, out ordertype))
            {
                dr["Message"] += "Invalid Order Type (" + ordertype + "), "; 
                dr["HasErrors"] = true;
                hasOrderType = false;
            }

            if (hasOrderType)
            {
                switch (ordertype)
                {
                    case OrderItemType.Buy:
                        {
                            decimal temp;
                            if (string.IsNullOrEmpty(PriceAVG) || !decimal.TryParse(PriceAVG, out temp))
                            {
                                dr["Message"] += "Invalid Price/Avg (" + PriceAVG + ") ";
                                dr["HasErrors"] = true;

                            }
                            if (!string.IsNullOrEmpty(QTY) && !decimal.TryParse(QTY, out temp))
                            {
                                dr["Message"] += "Invalid Qty (" + QTY + "), ";
                                dr["HasErrors"] = true;

                            }
                        }

                        break;
                    case OrderItemType.Sell:
                        {
                            decimal temp;
                            if (string.IsNullOrEmpty(QTY) || !decimal.TryParse(QTY, out temp))
                            {
                                dr["Message"] += "Invalid Qty (" + QTY + "), ";
                                dr["HasErrors"] = true;

                            }
                            if (!string.IsNullOrEmpty(PriceAVG) && !decimal.TryParse(PriceAVG, out temp))
                            {
                                dr["Message"] += "Invalid Price/Avg (" + PriceAVG + ") ";
                                dr["HasErrors"] = true;

                            }
                        }
                        break;


                }
            }
            else
            {
                decimal temp;
                if (!string.IsNullOrEmpty(PriceAVG) && !decimal.TryParse(PriceAVG, out temp))
                {
                    dr["Message"] += "Unit Price(" + PriceAVG + ") is empty or Invalid, ";
                    dr["HasErrors"] = true;

                }
                if (!string.IsNullOrEmpty(QTY) && !decimal.TryParse(QTY, out temp))
                {
                    dr["Message"] += "Units (" + PriceAVG + ") is empty or Invalid, ";
                    dr["HasErrors"] = true;

                }
            }







        }

        


      

    }
}
