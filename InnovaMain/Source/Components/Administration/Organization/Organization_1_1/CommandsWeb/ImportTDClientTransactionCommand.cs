﻿using System.Data;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Enum;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{
    public class ImportTDClientTransactionCommand : OrganizationWebCommandBase
    {
        public override System.Data.DataSet DoAction(System.Data.DataSet value)
        {
            if (value.Tables.Count == 0)
                return value;


            #region cash transactions
            if (value.Tables.Contains("AtTDTransaction"))
            {
                if (!value.Tables["AtTDTransaction"].Columns.Contains("Message"))
                {
                    //set message to not found
                    DataColumn dc = new DataColumn("Message");
                    dc.DefaultValue = "Account Number not Found";
                    value.Tables["AtTDTransaction"].Columns.Add(dc);
                }

                if (!value.Tables["AtTDTransaction"].Columns.Contains("HasErrors"))
                {
                    //set has error to true
                    DataColumn dc = new DataColumn("HasErrors", typeof(bool));
                    dc.DefaultValue = true;
                    value.Tables["AtTDTransaction"].Columns.Add(dc);
                }

                if (!value.Tables["AtTDTransaction"].Columns.Contains("IsMissingItem"))
                {
                    //set Is Missing Item to true
                    DataColumn dc = new DataColumn("IsMissingItem", typeof(bool));
                    dc.DefaultValue = true;
                    value.Tables["AtTDTransaction"].Columns.Add(dc);
                }

                if (!value.Tables["AtTDTransaction"].Columns.Contains("ClientID"))
                {
                    //set Is Missing Item to true
                    DataColumn dc = new DataColumn("ClientID", typeof(string));
                    dc.DefaultValue = string.Empty;
                    value.Tables["AtTDTransaction"].Columns.Add(dc);
                }
                if (!value.Tables["AtTDTransaction"].Columns.Contains("InstituteID"))
                {
                    DataColumn dc = new DataColumn("InstituteID", typeof(string));
                    dc.DefaultValue = string.Empty;
                    value.Tables["AtTDTransaction"].Columns.Add(dc);
                }

                if (!value.Tables["AtTDTransaction"].Columns.Contains("ProductID"))
                {
                    DataColumn dc = new DataColumn("ProductID", typeof(string));
                    dc.DefaultValue = string.Empty;
                    value.Tables["AtTDTransaction"].Columns.Add(dc);
                }

                if (!value.Tables["AtTDTransaction"].Columns.Contains("BrokerName"))
                {
                    DataColumn dc = new DataColumn("BrokerName", typeof(string));
                    dc.DefaultValue = string.Empty;
                    value.Tables["AtTDTransaction"].Columns.Add(dc);
                }

                if (!value.Tables["AtTDTransaction"].Columns.Contains("ServiceType"))
                {
                    DataColumn dc = new DataColumn("ServiceType", typeof(string));
                    dc.DefaultValue = string.Empty;
                    value.Tables["AtTDTransaction"].Columns.Add(dc);
                }


            }
            #endregion

            ////Get Client Id 
            OrganizationListAction.ForEachClientOnly("Administrator", Organization,
                                          (csid, logical, bmc) =>
                                          {
                                              bmc.GetData(value);
                                          });

            OrganizationListAction.ForEach("Administrator", OrganizationType.BankAccount, Organization,
                                         (csid, logical, bmc) =>
                                         {
                                             if (logical.CMType == OrganizationTypeList.GetEntity(OrganizationType.BankAccount))
                                             {
                                                 string accountNumber = bmc.GetDataStream((int)CmCommand.AccountNumber, string.Empty);
                                                 string accounttype = bmc.GetDataStream((int)CmCommand.GetAccountType, string.Empty);

                                                 bool AccountFound = false;
                                                 if (accounttype.ToLower() == "termdeposit" && value.Tables.Contains("AtTDTransaction"))
                                                 {
                                                     var drs = value.Tables["AtTDTransaction"].Select(string.Format("AccountNumber='{0}'", accountNumber)); // BsBNAccountNumber
                                                     if (drs.Length > 0)
                                                     {
                                                         AccountFound = true;
                                                         foreach (var dr in drs)
                                                         {
                                                             dr["Message"] = "";
                                                             dr["HasErrors"] = false;
                                                             dr["IsMissingItem"] = false;
                                                         }
                                                     }
                                                 }
                                                 if (AccountFound)
                                                 {
                                                     var unit = (bmc as OrganizationUnitCM);
                                                     unit.Broker.SetStart();
                                                     unit.SetData(value);
                                                     bmc.CalculateToken(true);
                                                 }
                                             }
                                         });
            return value;
        }
    }
}
