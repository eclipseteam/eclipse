﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{

    public class ImportProductSecurityPriceListCommand : OrganizationWebCommandBase
    {
        public override DataSet DoAction(System.Data.DataSet value)
        {
            if (value.Tables.Count == 0)
                return value;

            if (!value.Tables[0].Columns.Contains("Message"))
            {
                //set message to not found
                DataColumn dc = new DataColumn("Message") { DefaultValue = "" };
                value.Tables[0].Columns.Add(dc);
            }

            if (!value.Tables[0].Columns.Contains("HasErrors"))
            {
                //set has error to true
                DataColumn dc = new DataColumn("HasErrors", typeof(bool)) { DefaultValue = false };
                value.Tables[0].Columns.Add(dc);
            }

            if (!value.Tables[0].Columns.Contains("IsMissingItem"))
            {
                //set Is Missing Item to true
                DataColumn dc = new DataColumn("IsMissingItem", typeof(bool)) { DefaultValue = true };
                value.Tables[0].Columns.Add(dc);
            }

            foreach (DataRow dr in value.Tables[0].Rows)
            {
                dr["Message"] = "";
                SecuritiesEntity secuirtyEntity = Organization.Securities.Where(sec => sec.AsxCode == dr["StockCode"].ToString()).FirstOrDefault();
                if (secuirtyEntity == null)
                    secuirtyEntity = AddNewSecurity(dr);
                ValidationRow(dr, secuirtyEntity, Guid.Parse(value.ExtendedProperties["ProductSecurityID"].ToString()));
            }
            DeactivateProductSecurity(value.Tables[0], Guid.Parse(value.ExtendedProperties["ProductSecurityID"].ToString()));
            return value;
        }
        private SecuritiesEntity AddNewSecurity(DataRow dr)
        {
            var secuirtyEntity = new SecuritiesEntity
               {
                   AsxCode = dr["StockCode"].ToString(),
                   ID = Guid.NewGuid(),
                   ASXSecurity = new ObservableCollection<ASXSecurityEntity>()
               };
            Organization.Securities.Add(secuirtyEntity);
            dr["Message"] += "Stock code " + dr["StockCode"].ToString() + " has been added";
            dr["HasErrors"] = false;
            dr["IsMissingItem"] = true;
            return secuirtyEntity;
        }
        private void ValidationRow(DataRow dr, SecuritiesEntity secuirtyEntity, Guid productSecuirtyID)
        {
            double utemp;
            ProductSecurityDetail detail = new ProductSecurityDetail
            {
                DetailId = Guid.NewGuid(),
                Comments = dr["Comment"].ToString()
            };

            if (string.IsNullOrEmpty(dr["Currency"].ToString()))
            {
                dr["Message"] += "Invalid Currency,";
                dr["HasErrors"] = true;
            }
            else
            {
                detail.Currency = dr["Currency"].ToString();
            }

            if (string.IsNullOrEmpty(dr["Market"].ToString()))
            {
                dr["Message"] += "Invalid market,";
                dr["HasErrors"] = true;
            }
            else
            {
                detail.MarketCode = dr["Market"].ToString();
            }


            if (string.IsNullOrEmpty(dr["Recommendation"].ToString()))
            {
                dr["Message"] += "Invalid Recommendation,";
                dr["HasErrors"] = true;
            }
            else
            {
                detail.Rating = dr["Recommendation"].ToString();
            }

            if (string.IsNullOrEmpty(dr["DynamicRatingOption"].ToString()))
            {
                dr["Message"] += "Invalid dynamic rating option,";
                dr["HasErrors"] = true;
            }
            else
            {
                detail.RatingOptions = dr["DynamicRatingOption"].ToString();
            }

            if (!double.TryParse(dr["Weighting"].ToString(), out utemp))
            {
                dr["Message"] += "Invalid weighting,";
                dr["HasErrors"] = true;
            }
            else
            {
                detail.Allocation = utemp;
            }

            if (!double.TryParse(dr["BuyPrice"].ToString(), out utemp))
            {
                dr["Message"] += "Invalid buy price,";
                dr["HasErrors"] = true;
            }
            else
            {
                detail.BuyPrice = utemp;
            }

            if (!double.TryParse(dr["SellPrice"].ToString(), out utemp))
            {
                dr["Message"] += "Invalid sell price,";
                dr["HasErrors"] = true;
            }
            else
            {
                detail.SellPrice = utemp;
            }

            if (!bool.Parse(dr["HasErrors"].ToString()))
            {
                var productSec = Organization.ProductSecurities.Where(ps => ps.ID == productSecuirtyID).FirstOrDefault();
                if (productSec == null)
                {
                    dr["Message"] += "Product Security not found,";
                    dr["HasErrors"] = true;
                }
                else
                {
                    if (secuirtyEntity == null)
                    {
                        dr["Message"] += "Invalid Stock code, Ignored.,";
                        dr["HasErrors"] = true;
                    }
                    else
                    {
                        if (productSec.Details == null)
                        {
                            productSec.Details = new ObservableCollection<ProductSecurityDetail>();
                        }
                        var det = productSec.Details.Where(e => e.SecurityCodeId == secuirtyEntity.ID);
                        if (det != null)
                        {
                            if (productSec.Details.Count(dett => dett.SecurityCodeId == secuirtyEntity.ID) > 0)
                            {
                                dr["Message"] += "Already exists. Price for stock code updated,";
                            }
                            for (int i = det.Count() - 1; i >= 0; i--)
                            {
                                productSec.Details.Remove(det.ElementAt(i));
                            }

                        }

                        detail.SecurityCodeId = secuirtyEntity.ID;
                        productSec.Details.Add(detail);
                    }
                }
            }
        }
        private void DeactivateProductSecurity(DataTable dt, Guid productSecID)
        {
            var productSec = Organization.ProductSecurities.Where(ps => ps.ID == productSecID).FirstOrDefault();
            if (productSec != null)
            {
                foreach (var count in productSec.Details)
                {
                    var security = Organization.Securities.Where(sec => sec.ID == count.SecurityCodeId).FirstOrDefault();
                    if (security != null)
                    {
                        DataRow[] drs = dt.Select(string.Format("StockCode='{0}'", security.AsxCode));
                        /* if Product secuirty secuirty code is not in file deactivate it
                          if the secuirity code is not in file but it is attached with Product secuirty then it should be deactivated
                         */
                        if (drs.Length == 0)
                        {
                            if (!count.IsDeactive)
                            {
                                count.IsDeactive = true;
                                AddActivationMessage(dt, security.AsxCode, "Stock code " + security.AsxCode + " has been deactivated,");
                            }
                        }
                        else
                        {

                            if (count.IsDeactive)
                            {
                                count.IsDeactive = false;
                                AddActivationMessage(dt, security.AsxCode, "Stock code " + security.AsxCode + " has been activated,");
                            }
                        }
                    }
                    else
                    {
                        if (!count.IsDeactive)
                        {
                            count.IsDeactive = true;
                            AddActivationMessage(dt, security.AsxCode, "Stock code " + count.MarketCode + " has been deactivated,");

                        }
                    }
                }

            }
        }

        private static void AddActivationMessage(DataTable dt, string StockCode, string message)
        {
            DataRow dr = dt.NewRow();
            dr["StockCode"] = StockCode;
            dr["Message"] += message; ;
            dr["HasErrors"] = true;
            dr["IsMissingItem"] = false;
            dt.Rows.Add(dr);
        }
    }
}
