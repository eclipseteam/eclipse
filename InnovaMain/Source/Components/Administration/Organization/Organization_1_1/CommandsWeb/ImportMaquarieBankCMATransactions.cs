﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Data;
using System.Data;
using Oritax.TaxSimp.CM.Organization.Commands;
using Oritax.TaxSimp.CM.Organization.Data;
using Oritax.TaxSimp.DataSets;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{
    public class ImportMaquarieBankCMATransactions : OrganizationWebCommandBase
    {

        public override System.Data.DataSet DoAction(System.Data.DataSet value)
        {
            ImportProcessDS importProcessDS = (ImportProcessDS)value;
            if (importProcessDS.UseFile)
            { ImportFromFile(importProcessDS); }
            else
            {
                ImportFromService(importProcessDS);

            }
            return importProcessDS;
        }

        private void ImportFromFile(ImportProcessDS importProcessDs)
        {
            ImportTransactions(importProcessDs);
        }

        private void ImportFromService(ImportProcessDS importProcessDS)
        {
            MaquarieBankWebService maqService = new MaquarieBankWebService("https://www.macquarie.com.au/ESI/ESIWebService/Extract", "");
            maqService.EndDate = importProcessDS.EndDate; 
            maqService.InvokeMacquarieBankService();

            if (maqService.IsSuccess)
            {
                if (maqService.ResultTable == null)
                {
                    importProcessDS.MessageNumber = maqService.MessageNumber;
                    importProcessDS.MessageSummary = maqService.MessageSummary;
                    importProcessDS.MessageDetail = "Successful however nothing to load";
                }
                else
                {
                    DataTable resultTable = maqService.ResultTable.Copy();
                    resultTable.TableName = MaquarieCMATransactionsDS.MACQUARIEBANKTRANSACTIONSTABLE;

                    importProcessDS.IsSuccess = maqService.IsSuccess;
                    importProcessDS.MessageNumber = maqService.MessageNumber;
                    importProcessDS.MessageSummary = maqService.MessageSummary;
                    importProcessDS.MessageDetail = maqService.MessageDetail;
                    importProcessDS.Tables.Add(resultTable.Copy());

                    if (importProcessDS.IsSuccess)
                    {
                        ImportTransactions(importProcessDS);
                    }
                }
            }
            else
            {
                //if service failed
                importProcessDS.MessageNumber = maqService.MessageNumber;
                importProcessDS.MessageSummary = maqService.MessageSummary;
                importProcessDS.MessageDetail = "Failed to connect service";
            }
        }

        private void ImportTransactions(ImportProcessDS importProcessDS)
        {
            DataTable resultTable = importProcessDS.Tables[MaquarieCMATransactionsDS.MACQUARIEBANKTRANSACTIONSTABLE];
            if (!resultTable.Columns.Contains(MaquarieCMATransactionsDS.MESSAGE))
            {
                var dc = new DataColumn(MaquarieCMATransactionsDS.MESSAGE, typeof(string));
                dc.DefaultValue = "Account Not fount";
                resultTable.Columns.Add(dc);


            }
            if (!resultTable.Columns.Contains(MaquarieCMATransactionsDS.HASERRORS))
            {
                var dc = new DataColumn(MaquarieCMATransactionsDS.HASERRORS, typeof(bool));

                dc.DefaultValue = true;
                resultTable.Columns.Add(dc);
            }
            if (!resultTable.Columns.Contains(MaquarieCMATransactionsDS.ISMISSINGITEM))
            {
                var dc = new DataColumn(MaquarieCMATransactionsDS.ISMISSINGITEM, typeof(bool));

                dc.DefaultValue = true;
                resultTable.Columns.Add(dc);
            }
            if (!resultTable.Columns.Contains(MaquarieCMATransactionsDS.CLIENTID))
            {
                var dc = new DataColumn(MaquarieCMATransactionsDS.CLIENTID, typeof(string));

                dc.DefaultValue = string.Empty;
                resultTable.Columns.Add(dc);
            }
          
            OrganisationListingDS organisationListingDS = new OrganisationListingDS();
            organisationListingDS.OrganisationListingOperationType = OrganisationListingOperationType.BankAccounts;
            this.Organization.GetData(organisationListingDS);

            DataRow[] filteredRows = organisationListingDS.Tables[BankDetailsDS.BANKDETAILSTABLELIST].Select("InstitutionID='f2ca2e28-f60d-452e-822f-d5dedc9d5eb1'");
            foreach (DataRow row in filteredRows)
            {
                var selectedTransaction = importProcessDS.Tables[MaquarieCMATransactionsDS.MACQUARIEBANKTRANSACTIONSTABLE].Select().Where(accrow => accrow[MaquarieCMATransactionsDS.ACCOUNTNUMBER].ToString() == row[BankDetailsDS.ACCOUNTNUMBER].ToString());
                if (selectedTransaction.Count() > 0)
                {

                    foreach (DataRow dataRow in selectedTransaction)
                    {
                        dataRow[MaquarieCMATransactionsDS.MESSAGE] = "";
                        dataRow[MaquarieCMATransactionsDS.HASERRORS] = false;
                        dataRow[MaquarieCMATransactionsDS.ISMISSINGITEM] = false;
                        dataRow[MaquarieCMATransactionsDS.CLIENTID] = row[BankDetailsDS.CLIENTID];
                    }
                  
                    IOrganizationUnit bankAccount = this.Organization.Broker.GetBMCInstance(new Guid(row["ENTITYCIID_FIELD"].ToString())) as IOrganizationUnit;
                    bankAccount.SetData(importProcessDS);
                }
            }

            if (resultTable.Columns.Contains(MaquarieCMATransactionsDS.CLIENTID))
            {
                resultTable.Columns.Remove(MaquarieCMATransactionsDS.CLIENTID);
            }

        }
    }
}
