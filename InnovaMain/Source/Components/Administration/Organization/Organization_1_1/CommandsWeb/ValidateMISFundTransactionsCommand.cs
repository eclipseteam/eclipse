﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using Oritax.TaxSimp.CM.OrganizationUnit;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{
    public class ValidateMISFundTransactionsCommand : OrganizationWebCommandBase
    {
        public override DataSet DoAction(DataSet value)
        {
            if (value.Tables.Count == 0)
                return value;

            if (!value.Tables[0].Columns.Contains("Message"))
            {
                //set message to not found
                DataColumn dc = new DataColumn("Message");
                value.Tables[0].Columns.Add(dc);
            }

            if (!value.Tables[0].Columns.Contains("HasErrors"))
            {
                //set has error to true
                DataColumn dc = new DataColumn("HasErrors", typeof(bool)) { DefaultValue = true };
                value.Tables[0].Columns.Add(dc);
            }

            if (!value.Tables[0].Columns.Contains("IsMissingItem"))
            {
                //set Is Missing Item to true
                DataColumn dc = new DataColumn("IsMissingItem", typeof(bool)) { DefaultValue = true };
                value.Tables[0].Columns.Add(dc);
            }

            var foundAccountNumbers = new List<string>();

            OrganizationListAction.ForEachClientOnly("Administrator", Organization, (csid, logical, bmc) =>
            {
                if (bmc is OrganizationUnitCM && OrganizationListAction.IsValidClientType(logical.CMTypeName))
                {
                    var unit = (bmc as OrganizationUnitCM);
                    if (!string.IsNullOrEmpty(unit.ClientId))
                    {
                        if (value.Tables[0] != null && value.Tables[0].Rows.Count > 0)
                        {
                            DataRow[] drs = value.Tables[0].Select("Account='" + unit.ClientId.Trim() + "'");
                            if (drs.Length > 0)
                            {
                                foundAccountNumbers.Add(unit.ClientId.Trim());

                                foreach (var dr in drs)
                                {
                                    dr["Message"] = "";
                                    dr["HasErrors"] = false;
                                    dr["IsMissingItem"] = false;

                                    ValidationRow(dr, unit);
                                }

                            }
                        }
                    }
                }
            });

            //cheking missing account/client
            foreach (DataRow dr in value.Tables[0].Rows)
            {
                if (!foundAccountNumbers.Contains(dr["Account"].ToString()))
                {
                    dr["Message"] += "Client with ID (" + dr["Account"] + ") does not exists";
                    dr["HasErrors"] = true;
                    dr["IsMissingItem"] = false;
                }
            }

            return value;
        }

        private void ValidationRow(DataRow dr, OrganizationUnitCM unit)
        {

            #region Validations
            string fundCode = dr["FundCode"].ToString();
            const string misName = "StateStreet";

            DateTimeFormatInfo info = new DateTimeFormatInfo();
            info.ShortDatePattern = "dd/MM/yyyy";

            string tradeDate = dr["TradeDate"].ToString();
            string shares = dr["Shares"].ToString();
            string unitPrice = dr["UnitPrice"].ToString();
            string amount = dr["Amount"].ToString();

            DateTime date;
            decimal utemp;

            bool isAttachedWithClient = unit.IsMISAttachedWithFund(fundCode, misName);

            if (!isAttachedWithClient)
            {
                dr["Message"] += "Fund Code (" + fundCode + ") either does not exist or is not linked to this client,";
                dr["HasErrors"] = true;
                dr["IsMissingItem"] = true;
            }

            if (!DateTime.TryParse(tradeDate, info, DateTimeStyles.None, out date))
            {
                dr["Message"] += "Invalid Price Date,";
                dr["HasErrors"] = true;
            }

            if (!string.IsNullOrEmpty(unitPrice))
            {
                if (!decimal.TryParse(unitPrice, NumberStyles.Any, CultureInfo.CurrentCulture, out utemp))
                {
                    dr["Message"] += "Invalid Unit Price,";
                    dr["HasErrors"] = true;
                }
                else
                {
                   
                    dr["UnitPrice"] = utemp.ToString("N6");
                   
                }
            }

            if (!string.IsNullOrEmpty(amount))
            {
                if (!decimal.TryParse(amount, NumberStyles.Currency, CultureInfo.CurrentCulture, out utemp))
                {
                    dr["Message"] += "Invalid Amount,";
                    dr["HasErrors"] = true;
                }
                //else
                //{
                //    dr["Amount"] = utemp.ToString("C");
                //}
            }

            if (!string.IsNullOrEmpty(shares))
            {
                if (!decimal.TryParse(shares, NumberStyles.Any, CultureInfo.CurrentCulture, out utemp))
                {
                    dr["Message"] += "Invalid Shares,";
                    dr["HasErrors"] = true;
                }
                else
                {
                    dr["Shares"] = utemp.ToString("N4");
                }
            }

            #endregion


            if (!string.IsNullOrEmpty(dr["Message"].ToString()) && dr["Message"].ToString().Contains(','))
            {
                dr["Message"] = dr["Message"].ToString().Remove(dr["Message"].ToString().LastIndexOf(','));
            }


        }
    }
}
