﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Enum;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Utilities;
using BankAccountEntity = Oritax.TaxSimp.Common.BankAccountEntity;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{

    public class ImportTDPriceListCommand : OrganizationWebCommandBase
    {

        public override System.Data.DataSet DoAction(System.Data.DataSet value)
        {
            if (value.Tables.Count == 0)
                return value;

            if (!value.Tables[0].Columns.Contains("Message"))
            {
                //set message to not found
                var dc = new DataColumn("Message") { DefaultValue = "" };
                value.Tables[0].Columns.Add(dc);
            }

            if (!value.Tables[0].Columns.Contains("HasErrors"))
            {
                //set has error to true
                var dc = new DataColumn("HasErrors", typeof(bool)) { DefaultValue = false };
                value.Tables[0].Columns.Add(dc);
            }

            if (!value.Tables[0].Columns.Contains("IsMissingItem"))
            {
                //set Is Missing Item to true
                var dc = new DataColumn("IsMissingItem", typeof(bool)) { DefaultValue = true };
                value.Tables[0].Columns.Add(dc);
            }
            var inst = Organization.Institution.FirstOrDefault(ps => ps.ID == Guid.Parse(value.ExtendedProperties["InstituteID"].ToString()));
            var rateEffectiveFor = (ClientManagementType)System.Enum.Parse(typeof(ClientManagementType), value.ExtendedProperties["RateEffectiveFor"].ToString());
            if (inst != null)
                foreach (DataRow dr in value.Tables[0].Rows)
                {
                    dr["Message"] = "";
                    var childInstitute = Organization.Institution.FirstOrDefault(sec => sec.Name == dr["Provider"].ToString());
                    if (childInstitute == null)
                    {
                        childInstitute = AddNewInstitution(dr);
                    }
                    else
                    {
                        UpdateInstitution(childInstitute, dr);
                    }
                    ValidationRows(dr, inst, childInstitute.ID, rateEffectiveFor);
                }
            return value;
        }

        private void UpdateInstitution(InstitutionEntity childInstitue, DataRow dr)
        {
            childInstitue.SPCreditRatingLong = (!string.IsNullOrEmpty(dr["SPLongTermRating"].ToString()) && InstituteCombos.SPLongTermRatings.ContainsKey(dr["SPLongTermRating"].ToString())) ? InstituteCombos.SPLongTermRatings[dr["SPLongTermRating"].ToString()] : 22;//{"Unrated",22}
        }

        private InstitutionEntity AddNewInstitution(DataRow dr)
        {
            var childInstitute = Organization.Institution.Where(sec => sec.Name == dr["Provider"].ToString()).FirstOrDefault();
            if (childInstitute == null)
            {
                childInstitute = new InstitutionEntity()
                {
                    Name = dr["Provider"].ToString(),
                    ID = Guid.NewGuid(),
                    Type = (!string.IsNullOrEmpty(dr["ProviderType"].ToString()) && InstituteCombos.Types.ContainsKey(dr["ProviderType"].ToString())) ? InstituteCombos.Types[dr["ProviderType"].ToString()] : new int(),
                    SPCreditRatingLong = (!string.IsNullOrEmpty(dr["SPLongTermRating"].ToString()) && InstituteCombos.SPLongTermRatings.ContainsKey(dr["SPLongTermRating"].ToString())) ? InstituteCombos.SPLongTermRatings[dr["SPLongTermRating"].ToString()] : 22, //{"Unrated",22}

                };
                Organization.Institution.Add(childInstitute);
                dr["Message"] += "Institute/Provider" + dr["Provider"] + " has been added. ";
                dr["HasErrors"] = false;
                dr["IsMissingItem"] = true;
            }
            return childInstitute;
        }

        private void ValidationRows(DataRow dr, InstitutionEntity mainInstitution, Guid instId, ClientManagementType clientManagementType)
        {
            DateTime date;
            decimal utemp;
            var info = new DateTimeFormatInfo { ShortDatePattern = "dd/MM/yyyy" };
            var price = new TDInstitutePriceEntity { InstituteID = instId, Status = dr["Status"].ToString() };

            if (string.IsNullOrEmpty(dr["SPLongTermRating"].ToString().Trim()))
            {
                dr["Message"] += "Invalid SP Long Term Rating,";
                dr["HasErrors"] = true;
            }

            if (string.IsNullOrEmpty(dr["ProviderType"].ToString().Trim()))
            {
                dr["Message"] += "Invalid provider type,";
                dr["HasErrors"] = true;
            }

            if (!string.IsNullOrEmpty(dr["Min"].ToString().Trim()))
                if (!decimal.TryParse(dr["Min"].ToString(), out utemp))
                {
                    dr["Message"] += "Invalid min price,";
                    dr["HasErrors"] = true;
                }
                else
                {
                    price.Min = utemp;
                }

            if (!string.IsNullOrEmpty(dr["Max"].ToString().Trim()))
                if (!decimal.TryParse(dr["Max"].ToString(), out utemp))
                {
                    dr["Message"] += "Invalid max price,";
                    dr["HasErrors"] = true;
                }
                else
                {
                    price.Max = utemp;
                }

            if (!string.IsNullOrEmpty(dr["MaximumBrokeage"].ToString().Trim()))
                if (!decimal.TryParse(dr["MaximumBrokeage"].ToString(), out utemp))
                {
                    dr["Message"] += "Invalid maximumBrokeage price,";
                    dr["HasErrors"] = true;
                }
                else
                {
                    price.MaximumBrokeage = utemp;
                }

            if (!string.IsNullOrEmpty(dr["Days30"].ToString().Trim()))
                if (!decimal.TryParse(dr["Days30"].ToString(), out utemp))
                {
                    dr["Message"] += "Invalid 30-days price,";
                    dr["HasErrors"] = true;
                }
                else
                {
                    price.Days30 = utemp;
                }

            if (!string.IsNullOrEmpty(dr["Days60"].ToString().Trim()))
                if (!decimal.TryParse(dr["Days60"].ToString(), out utemp))
                {
                    dr["Message"] += "Invalid 60-days price,";
                    dr["HasErrors"] = true;
                }
                else
                {
                    price.Days60 = utemp;
                }

            if (!string.IsNullOrEmpty(dr["Days90"].ToString().Trim()))
                if (!decimal.TryParse(dr["Days90"].ToString(), out utemp))
                {
                    dr["Message"] += "Invalid 90-days price,";
                    dr["HasErrors"] = true;
                }
                else
                {
                    price.Days90 = utemp;
                }

            if (!string.IsNullOrEmpty(dr["Days120"].ToString().Trim()))
                if (!decimal.TryParse(dr["Days120"].ToString(), out utemp))
                {
                    dr["Message"] += "Invalid 120-days price,";
                    dr["HasErrors"] = true;
                }
                else
                {
                    price.Days120 = utemp;
                }



            if (!string.IsNullOrEmpty(dr["Days150"].ToString().Trim()))
                if (!decimal.TryParse(dr["Days150"].ToString(), out utemp))
                {
                    dr["Message"] += "Invalid 150-days price,";
                    dr["HasErrors"] = true;
                }
                else
                {
                    price.Days150 = utemp;
                }

            if (!string.IsNullOrEmpty(dr["Days180"].ToString().Trim()))
                if (!decimal.TryParse(dr["Days180"].ToString(), out utemp))
                {
                    dr["Message"] += "Invalid 180-days price,";
                    dr["HasErrors"] = true;
                }
                else
                {
                    price.Days180 = utemp;
                }

            if (!string.IsNullOrEmpty(dr["Days270"].ToString().Trim()))
                if (!decimal.TryParse(dr["Days270"].ToString(), out utemp))
                {
                    dr["Message"] += "Invalid 270-days price,";
                    dr["HasErrors"] = true;
                }
                else
                {
                    price.Days270 = utemp;
                }

            if (!string.IsNullOrEmpty(dr["Years1"].ToString().Trim()))
                if (!decimal.TryParse(dr["Years1"].ToString(), out utemp))
                {
                    dr["Message"] += "Invalid 1-Year price,";
                    dr["HasErrors"] = true;
                }
                else
                {
                    price.Years1 = utemp;
                }

            if (!string.IsNullOrEmpty(dr["Years2"].ToString().Trim()))
                if (!decimal.TryParse(dr["Years2"].ToString(), out utemp))
                {
                    dr["Message"] += "Invalid 2-Year price,";
                    dr["HasErrors"] = true;
                }
                else
                {
                    price.Years2 = utemp;
                }

            if (!string.IsNullOrEmpty(dr["Years3"].ToString().Trim()))
                if (!decimal.TryParse(dr["Years3"].ToString(), out utemp))
                {
                    dr["Message"] += "Invalid 3-Year price,";
                    dr["HasErrors"] = true;
                }
                else
                {
                    price.Years3 = utemp;
                }

            if (!string.IsNullOrEmpty(dr["Years4"].ToString().Trim()))
                if (!decimal.TryParse(dr["Years4"].ToString(), out utemp))
                {
                    dr["Message"] += "Invalid 4-Year price,";
                    dr["HasErrors"] = true;
                }
                else
                {
                    price.Years4 = utemp;
                }

            if (!string.IsNullOrEmpty(dr["Years5"].ToString().Trim()))
                if (!decimal.TryParse(dr["Years5"].ToString(), out utemp))
                {
                    dr["Message"] += "Invalid 5-Year price,";
                    dr["HasErrors"] = true;
                }
                else
                {
                    price.Years5 = utemp;
                }

            if (!DateTime.TryParseExact(dr["Date"].ToString(), "dd/MM/yyyy", info, DateTimeStyles.None, out date))
            {
                dr["Message"] += "Invalid Price Date,";
                dr["HasErrors"] = true;
            }
            else
            {
                price.Date = date;
            }   

            if (!bool.Parse(dr["HasErrors"].ToString()))
            {

                var ins = Organization.Institution.Where(sec => sec.ID == instId).FirstOrDefault();
                if (ins == null)
                {
                    dr["Message"] += "Invalid institution(Provider),";
                }
                else
                {
                    if (mainInstitution == null)
                    {
                        dr["Message"] += "Main Institution not found,";
                        dr["HasErrors"] = true;
                    }
                    else
                    {
                        if (mainInstitution.TDPrices == null)
                            mainInstitution.TDPrices = new ObservableCollection<TDInstitutePriceEntity>();
                        price.RateEffectiveFor = clientManagementType;
                        mainInstitution.TDPrices.Add(price);
                        dr["Message"] += "Successful";

                    }
                }
            }

        }
    }
}
