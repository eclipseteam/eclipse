﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{
    public class GetTDTransactionsAmongDividends : OrganizationWebCommandBase
    {
        public override DataSet DoAction(DataSet value)
        {
            OrganizationListAction.ForEachClientOnly("Administrator", Organization, (csid, logical, bmc) =>
            {

                if (bmc is OrganizationUnitCM && OrganizationListAction.IsValidClientType(logical.CMTypeName))
                {
                    var client = (bmc as OrganizationUnitCM);
                    client.GetData(value);
                }
            });

            return value;

           
        }




    }


}
