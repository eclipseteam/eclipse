﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Enum;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Utilities;
using BankAccountEntity = Oritax.TaxSimp.Common.BankAccountEntity;
using Oritax.TaxSimp.CM.Organization.Data;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{

    public class ImportBankWestAccountOpeningsCommand : OrganizationWebCommandBase
    {

        public override System.Data.DataSet DoAction(System.Data.DataSet value)
        {
            if (value.Tables.Count == 0)
                return value;

            if (!value.Tables[0].Columns.Contains("Message"))
            {
                //set message to not found
                DataColumn dc = new DataColumn("Message");
                dc.DefaultValue = "Client Account Number not Found";
                value.Tables[0].Columns.Add(dc);
            }

            if (!value.Tables[0].Columns.Contains("HasErrors"))
            {
                //set has error to true
                DataColumn dc = new DataColumn("HasErrors", typeof(bool));
                dc.DefaultValue = true;
                value.Tables[0].Columns.Add(dc);
            }

            if (!value.Tables[0].Columns.Contains("IsMissingItem"))
            {
                //set Is Missing Item to true
                DataColumn dc = new DataColumn("IsMissingItem", typeof(bool));
                dc.DefaultValue = true;
                value.Tables[0].Columns.Add(dc);
            }

            OrganisationListingDS organisationListingDS = new Data.OrganisationListingDS();
            organisationListingDS.OrganisationListingOperationType = OrganisationListingOperationType.BankAccounts;
            Organization.GetData(organisationListingDS);

            DataTable bankAccount = organisationListingDS.Tables[BankDetailsDS.BANKDETAILSTABLELIST];

            foreach (DataRow dr in value.Tables[0].Rows)
            {
                string AccountNumber = dr["AccountNo"].ToString();
                string AccountName = dr["AccountName"].ToString();
                string BSB = dr["BSB"].ToString();

                var existingAccount = bankAccount.Select().Where(row => row[BankDetailsDS.ACCOUNTNUMBER].ToString() == AccountNumber).FirstOrDefault();
                ValidateAccount(dr, AccountNumber, AccountName, BSB, existingAccount);

            }

            foreach (DataRow dr in value.Tables[0].Rows)
            {
                AddAccountsAndAttachWithClient(dr, (value as IHasOrganizationUnit).Unit.CurrentUser);
            }
            
            return value;
        }

        private void ValidateAccount(DataRow dr, string AccountNumber, string AccountName, string BSB, DataRow existingAccount)
        {
            bool isSuccess = true;

            if (existingAccount != null)
            {
                dr["Message"] += "Account Number (" + AccountNumber + ") and BSB  (" + BSB + ") already exists,";
                dr["HasErrors"] = true;
                isSuccess = false;
            }

            if (string.IsNullOrEmpty(AccountName))
            {
                dr["Message"] += "Account Name (" + AccountName + ") is empty,";
                dr["HasErrors"] = true;
                isSuccess = false;
            }

            if (string.IsNullOrEmpty(AccountNumber))
            {
                dr["Message"] += "Account Number (" + AccountNumber + ") is empty,";
                dr["HasErrors"] = true;
                isSuccess = false;
            }


            if (string.IsNullOrEmpty(BSB))
            {
                dr["Message"] += "Account BSB (" + BSB + ") is empty,";
                dr["HasErrors"] = true;
                isSuccess = false;
            }

            if (isSuccess)
            {
                dr["Message"] = "Success";
                dr["HasErrors"] = false;
                dr["IsMissingItem"] = false;
            }
        }

        private void AddAccountsAndAttachWithClient(DataRow account, UserEntity user)
        {
            OrganizationAddCommand command = new OrganizationAddCommand();
            command.Organization = this.Organization;
           
            var ClientBankAccouts = new List<IIdentityCM>();
           
                if (!bool.Parse(account["HasErrors"].ToString()))
                {
                    BankAccountDS bankaccountds = new BankAccountDS();
                    DataRow dataRow = bankaccountds.BankAccountsTable.NewRow();
                    dataRow[bankaccountds.BankAccountsTable.ACCOUNTNAME] = account["AccountName"].ToString();
                    dataRow[bankaccountds.BankAccountsTable.ACCOUNTNO] = account["AccountNo"].ToString();
                    dataRow[bankaccountds.BankAccountsTable.ACCOUNTTYPE] = "CMA";
                    var institute = Organization.Institution.FirstOrDefault(ss => ss.Name.ToLower() == "Bankwest".ToLower());

                    if (institute != null)
                        dataRow[bankaccountds.BankAccountsTable.INSTITUTIONID] = institute.ID;

                    dataRow[bankaccountds.BankAccountsTable.STATUS] = "Pending";
                    dataRow[bankaccountds.BankAccountsTable.BSBNO] = account["BSB"];

                    var unit = new Oritax.TaxSimp.Data.OrganizationUnit
                                   {
                                       Name = account["AccountName"].ToString(),
                                       Type = ((int)OrganizationType.BankAccount).ToString(),
                                       CurrentUser = user
                                   };
                    bankaccountds.BankAccountsTable.Rows.Add(dataRow);
                    bankaccountds.Unit = unit;

                    var accountunit = command.AddOrganizationUnit(() => bankaccountds);
                    ClientBankAccouts.Add(new IdentityCM(){Clid = accountunit.Clid,Csid = accountunit.Csid});
                   
                    account["Message"] = "Successfull";
            }
     
                  }

 
        private bool CheckAccountNumberAndBSBExists(string AccountNumber, string BSB)
        {

            bool result = false;

            OrganizationListAction.ForEach("Administrator", OrganizationType.BankAccount, Organization, (csid, logical, bmc) =>
            {
                string accountNumber = bmc.GetDataStream((int)CmCommand.AccountNumber, string.Empty);
                string bsb = bmc.GetDataStream((int)CmCommand.BSB, string.Empty);
                if (AccountNumber == accountNumber && BSB == bsb)
                {

                    result = true;

                }

            });
            return result;
        }


       

    }
}
