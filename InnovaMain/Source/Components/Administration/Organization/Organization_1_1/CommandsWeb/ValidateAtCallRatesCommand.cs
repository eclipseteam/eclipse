﻿using System;
using System.Data;
using System.Globalization;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.CalculationInterface;
namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{
    public class ValidateAtCallRatesCommand : OrganizationWebCommandBase
    {
        public override DataSet DoAction(DataSet value)
        {
            if (value.Tables.Count == 0)
                return value;

            if (!value.Tables[0].Columns.Contains("Message"))
            {
                //set message to not found
                var dc = new DataColumn("Message") {DefaultValue = "Client Account Not Found"};
                value.Tables[0].Columns.Add(dc);
            }

            if (!value.Tables[0].Columns.Contains("HasErrors"))
            {
                //set has error to true
                var dc = new DataColumn("HasErrors", typeof(bool)) {DefaultValue = true};
                value.Tables[0].Columns.Add(dc);
            }

            if (!value.Tables[0].Columns.Contains("IsMissingItem"))
            {
                //set Is Missing Item to true
                var dc = new DataColumn("IsMissingItem", typeof(bool)) {DefaultValue = true};
                value.Tables[0].Columns.Add(dc);
            }

            var unit = Organization.Broker.GetWellKnownBMC(WellKnownCM.Organization) as OrganizationUnitCM;

            DataRow[] drs = value.Tables[0].Select();

            foreach (DataRow dr in drs)
            {
                dr["Message"] = "";
                dr["HasErrors"] = false;
                dr["IsMissingItem"] = false;

                ValidationRows(dr);
            }

            Organization.Broker.ReleaseBrokerManagedComponent(unit);

            return value;
        }
        private void ValidationRows(DataRow dr)
        {
            double tempDouble;
            DateTime tempDate;
            DateTimeFormatInfo dtfInfo =
                new DateTimeFormatInfo { ShortDatePattern = "dd/MM/yyyy" };
            int rateId;
            if (!int.TryParse(dr["RateID"].ToString(), out rateId))
            {
                dr["Message"] += "Invalid Rate ID,";
                dr["HasErrors"] = true;
            }


            if (dr["Min"] != DBNull.Value && !double.TryParse(dr["Min"].ToString(), out tempDouble))
            {
                dr["Message"] += "Invalid Min Price,";
                dr["HasErrors"] = true;
            }

            if (dr["Max"] != DBNull.Value && !double.TryParse(dr["Max"].ToString(), out tempDouble))
            {
                dr["Message"] += "Invalid Max Price,";
                dr["HasErrors"] = true;
            }

            if (dr["Rate"] != DBNull.Value && !double.TryParse(dr["Rate"].ToString(), out tempDouble))
            {
                dr["Message"] += "Invalid Rate,";
                dr["HasErrors"] = true;
            }

            if (dr["MaximumBrokerage"] != DBNull.Value && !double.TryParse(dr["MaximumBrokerage"].ToString(), out tempDouble))
            {
                dr["Message"] += "Invalid Brokerage,";
                dr["HasErrors"] = true;
            }

            if (string.IsNullOrEmpty(dr["ApplicableFrom"].ToString()) ||
                !DateTime.TryParse(dr["ApplicableFrom"].ToString(), dtfInfo, DateTimeStyles.None, out tempDate))
            {
                dr["Message"] += "Invalid Applicable From Date, ";
                dr["HasErrors"] = true;
            }

            if (string.IsNullOrEmpty(dr["Date"].ToString()) ||
                !DateTime.TryParse(dr["Date"].ToString(), dtfInfo, DateTimeStyles.None, out tempDate))
            {
                dr["Message"] += "Invalid Date, ";
                dr["HasErrors"] = true;
            }

            if (string.IsNullOrEmpty(dr["Type"].ToString()))
            {
                dr["Message"] += "Invalid Type, ";
                dr["HasErrors"] = true;
            }
        }
    }
}
