﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Enum;
using System.Collections.ObjectModel;

namespace Oritax.TaxSimp.CM.Organization.Commands
{
    public class ProcessAllSecurityDividendCommandWeb : OrganizationCmCommandBase
    {
        public override string DoAction(string data)
        {          
            OrganizationListAction.ForEachClientOnly("Administrator", Organization, (csid, logical, bmc) =>
            {
               
                if ((bmc is IOrganizationUnit) && (bmc as IOrganizationUnit).IsInvestableClient)
                {
                    (bmc as OrganizationUnitCM).Broker.SetStart();
                    var securities = Organization.Securities;

                    if(data!=string.Empty)
                    {
                       securities= Organization.Securities.Where(ss => ss.ID == Guid.Parse(data)).ToList();
                    }


                    foreach (var security in securities)
                    {
                        List<ProductEntity> products = new List<ProductEntity>();
                        foreach (var prodsec in Organization.ProductSecurities)
                        {
                            if (prodsec.Details != null)
                            {
                                var details = prodsec.Details.Where(detal => detal.SecurityCodeId == security.ID).FirstOrDefault();
                                if (details != null)
                                {
                                    foreach (var product in Organization.Products.Where(produt => produt.ProductSecuritiesId == prodsec.ID))
                                    {
                                        products.Add(product);
                                    }
                                }
                            }
                        }
                        if (security.DividendCollection != null)
                        {
                            foreach (var div in security.DividendCollection)
                            {
                                if (div != null)
                                {
                                    div.ProcessFlag = true;
                                    div.InvestmentCode = security.AsxCode;
                                }
                              
                                (bmc as OrganizationUnitCM).ProcessDividend(div, products, security);                                                           
                            }
                           
                        }

                    }
                    
                }
                bmc.CalculateToken(true);
            });
           


            return string.Empty;
        }
      
    }
}
