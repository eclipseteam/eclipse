﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Globalization;
using Oritax.TaxSimp.Data;
using System.Linq;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{
    // ReSharper disable InconsistentNaming
    public class ImportTDRatesCommand : OrganizationWebCommandBase
    // ReSharper restore InconsistentNaming
    {
        public override DataSet DoAction(DataSet value)
        {
            if (value.Tables.Count == 0)
                return value;

            if (!value.Tables[0].Columns.Contains("Message"))
            {
                var dc = new DataColumn("Message", typeof(String))
                {
                    DefaultValue = "Client Account Not Found"
                };
                value.Tables[0].Columns.Add(dc);
            }
            if (!value.Tables[0].Columns.Contains("HasErrors"))
            {
                var dc = new DataColumn("HasErrors", typeof(bool))
                {
                    DefaultValue = true
                };
                value.Tables[0].Columns.Add(dc);
            }
            if (!value.Tables[0].Columns.Contains("IsMissingItem"))
            {
                var dc = new DataColumn("IsMissingItem", typeof(bool))
                {
                    DefaultValue = true
                };
                value.Tables[0].Columns.Add(dc);
            }

            #region Import Rates

            DataRow[] drs = value.Tables[0].Select();

            foreach (DataRow dr in drs)
            {
                dr["Message"] = "";
                dr["HasErrors"] = false;
                dr["IsMissingItem"] = false;
                ValidationRows(dr);
            }

            #endregion


            return value;
        }

        private void ValidationRows(DataRow dr)
        {
            double tempDouble;
            DateTime tempDate;
            DateTimeFormatInfo dtfInfo =
                new DateTimeFormatInfo { ShortDatePattern = "dd/MM/yyyy" };
            var InstituteID = Guid.Parse(dr["INSTITUTEID"].ToString());
            var bank = Organization.Institution.FirstOrDefault(ins => ins.Name == dr["Name"].ToString());

            if (bank == null)
            {
                dr["Message"] += "Bank Not Found,";
                dr["HasErrors"] = true;
            }


            TDInstitutePriceEntity priceEntity = new TDInstitutePriceEntity
                {
                    ID = Guid.Parse(dr["ID"].ToString()),
                    InstituteID = bank != null ? bank.ID : Guid.Empty,
                };
            int rateId;
            if (!int.TryParse(dr["RateID"].ToString(), out rateId))
            {
                dr["Message"] += "Invalid Rate ID,";
                dr["HasErrors"] = true;
            }
            else
            {
                priceEntity.ImportRateID = rateId;
            }
            int providerId;
            if (!int.TryParse(dr["ProviderID"].ToString(), out providerId))
            {
                dr["Message"] += "Invalid Provider ID,";
                dr["HasErrors"] = true;
            }
            else
            {
                priceEntity.ProviderID = providerId;
            }
            if (dr["Min"] != DBNull.Value)
                if (!double.TryParse(dr["Min"].ToString(), out tempDouble))
                {
                    dr["Message"] += "Invalid Min Price,";
                    dr["HasErrors"] = true;
                }
                else
                {
                    priceEntity.Min = (Decimal?)tempDouble;
                }
            if (dr["Max"] != DBNull.Value)
                if (!double.TryParse(dr["Max"].ToString(), out tempDouble))
                {
                    dr["Message"] += "Invalid Max Price,";
                    dr["HasErrors"] = true;
                }
                else
                {
                    priceEntity.Max = (Decimal?)tempDouble;
                }
            if (dr["MaximumBrokeage"] != DBNull.Value)
                if (!double.TryParse(dr["MaximumBrokeage"].ToString(), out tempDouble))
                {
                    dr["Message"] += "Invalid Brokeage,";
                    dr["HasErrors"] = true;
                }
                else
                {
                    priceEntity.MaximumBrokeage = (Decimal?)tempDouble;
                }
            if (dr["Days30"] != DBNull.Value)
                if (!double.TryParse(dr["Days30"].ToString(), out tempDouble))
                {
                    dr["Message"] += "Invalid Days30,";
                    dr["HasErrors"] = true;
                }
                else
                {
                    priceEntity.Days30 = (Decimal?)tempDouble;
                }
            if (dr["Days60"] != DBNull.Value)
                if (!double.TryParse(dr["Days60"].ToString(), out tempDouble))
                {
                    dr["Message"] += "Invalid Days60,";
                    dr["HasErrors"] = true;
                }
                else
                {
                    priceEntity.Days60 = (Decimal?)tempDouble;
                }
            if (dr["Days90"] != DBNull.Value)
                if (!double.TryParse(dr["Days90"].ToString(), out tempDouble))
                {
                    dr["Message"] += "Invalid Days90,";
                    dr["HasErrors"] = true;
                }
                else
                {
                    priceEntity.Days90 = (Decimal?)tempDouble;
                }
            if (dr["days120"] != DBNull.Value)
                if (!double.TryParse(dr["days120"].ToString(), out tempDouble))
                {
                    dr["Message"] += "Invalid days120,";
                    dr["HasErrors"] = true;
                }
                else
                {
                    priceEntity.Days120 = (Decimal?)tempDouble;
                }
            if (dr["Days150"] != DBNull.Value)
                if (!double.TryParse(dr["Days150"].ToString(), out tempDouble))
                {
                    dr["Message"] += "Invalid Days150,";
                    dr["HasErrors"] = true;
                }
                else
                {
                    priceEntity.Days150 = (Decimal?)tempDouble;
                }
            if (dr["Days180"] != DBNull.Value)
                if (!double.TryParse(dr["Days180"].ToString(), out tempDouble))
                {
                    dr["Message"] += "Invalid Days180,";
                    dr["HasErrors"] = true;
                }
                else
                {
                    priceEntity.Days180 = (Decimal?)tempDouble;
                }
            if (dr["days270"] != DBNull.Value)
                if (!double.TryParse(dr["days270"].ToString(), out tempDouble))
                {
                    dr["Message"] += "Invalid days270,";
                    dr["HasErrors"] = true;
                }
                else
                {
                    priceEntity.Days270 = (Decimal?)tempDouble;
                }
            if (dr["Years1"] != DBNull.Value)
                if (!double.TryParse(dr["Years1"].ToString(), out tempDouble))
                {
                    dr["Message"] += "Invalid Years1,";
                    dr["HasErrors"] = true;
                }
                else
                {
                    priceEntity.Years1 = (Decimal?)tempDouble;
                }

            if (dr["Years2"] != DBNull.Value)
                if (!double.TryParse(dr["Years2"].ToString(), out tempDouble))
                {
                    dr["Message"] += "Invalid Years2,";
                    dr["HasErrors"] = true;
                }
                else
                {
                    priceEntity.Years2 = (Decimal?)tempDouble;
                }

            if (dr["Years3"] != DBNull.Value)
                if (!double.TryParse(dr["Years3"].ToString(), out tempDouble))
                {
                    dr["Message"] += "Invalid Years3,";
                    dr["HasErrors"] = true;
                }
                else
                {
                    priceEntity.Years3 = (Decimal?)tempDouble;
                }

            if (dr["Years4"] != DBNull.Value)
                if (!double.TryParse(dr["Years4"].ToString(), out tempDouble))
                {
                    dr["Message"] += "Invalid Years4,";
                    dr["HasErrors"] = true;
                }
                else
                {
                    priceEntity.Years4 = (Decimal?)tempDouble;
                }
            if (dr["Years5"] != DBNull.Value)
                if (!double.TryParse(dr["Years5"].ToString(), out tempDouble))
                {
                    dr["Message"] += "Invalid Years5,";
                    dr["HasErrors"] = true;
                }
                else
                {
                    priceEntity.Years5 = (Decimal?)tempDouble;
                }

            if (string.IsNullOrEmpty(dr["Status"].ToString()))
            {
                dr["Message"] += "Invalid Status,";
                dr["HasErrors"] = true;
            }
            else
            {
                priceEntity.Status = dr["Status"].ToString();
            }

            if (string.IsNullOrEmpty(dr["Date"].ToString()) ||
                !DateTime.TryParse(dr["Date"].ToString(), dtfInfo, DateTimeStyles.None, out tempDate))
            {
                dr["Message"] += "Invalid Date,";
                dr["HasErrors"] = true;
            }
            else
            {
                priceEntity.Date = tempDate;
            }


            if (!double.TryParse(dr["ongoing"].ToString(), out tempDouble))
            {
                dr["Message"] += "Invalid ongoing,";
                dr["HasErrors"] = true;
            }
            else
            {
                priceEntity.Ongoing = (Decimal?)tempDouble;
            }


            if (!bool.Parse(dr["HasErrors"].ToString()))
            {
                var inst =
                    Organization.Institution.
                    FirstOrDefault(item => item.ID == InstituteID);

                if (inst != null)
                {
                    var objExist =
                        inst.TDPrices.
                        SingleOrDefault(ss => ss.ImportRateID == rateId
                            );
                    if (objExist != null)
                    {
                        dr["Message"] += "Rate with same ID already exists, ";
                        dr["HasErrors"] = true;
                    }
                    else
                    {
                        if (inst.TDPrices == null)
                            inst.TDPrices = new ObservableCollection<TDInstitutePriceEntity>();
                        inst.TDPrices.Add(priceEntity);
                    }
                }
            }
        }
    }
}