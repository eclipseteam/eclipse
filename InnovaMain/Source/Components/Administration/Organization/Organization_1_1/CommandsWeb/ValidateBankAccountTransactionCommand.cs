﻿using System;
using System.Data;
using System.Globalization;
using System.Linq;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Enum;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{
    public class ValidateBankAccountTransactionCommand : OrganizationWebCommandBase
    {
        public override System.Data.DataSet DoAction(System.Data.DataSet value)
        {
            if (value.Tables.Count == 0)
                return value;
           
            #region cash transactions
            if (value.Tables.Contains("CashTransctions"))
            {
                if (!value.Tables["CashTransctions"].Columns.Contains("Message"))
                {
                    //set message to not found
                    DataColumn dc = new DataColumn("Message");
                    dc.DefaultValue = "Account Number not Found";
                    value.Tables["CashTransctions"].Columns.Add(dc);
                }

                if (!value.Tables["CashTransctions"].Columns.Contains("HasErrors"))
                {
                    //set has error to true
                    DataColumn dc = new DataColumn("HasErrors", typeof(bool));
                    dc.DefaultValue = true;
                    value.Tables["CashTransctions"].Columns.Add(dc);
                }

                if (!value.Tables["CashTransctions"].Columns.Contains("IsMissingItem"))
                {
                    //set Is Missing Item to true
                    DataColumn dc = new DataColumn("IsMissingItem", typeof(bool));
                    dc.DefaultValue = true;
                    value.Tables["CashTransctions"].Columns.Add(dc);
                }
            }
            
            #endregion
            
            #region Holding balance columns settings
            if (value.Tables.Contains("HoldingBalances"))
            {
                if (!value.Tables["HoldingBalances"].Columns.Contains("Message"))
                {
                    //set message to not found
                    DataColumn dc = new DataColumn("Message");
                    dc.DefaultValue = "Account Number not Found";
                    value.Tables["HoldingBalances"].Columns.Add(dc);
                }

                if (!value.Tables["HoldingBalances"].Columns.Contains("HasErrors"))
                {
                    //set has error to true
                    DataColumn dc = new DataColumn("HasErrors", typeof(bool));
                    dc.DefaultValue = true;
                    value.Tables["HoldingBalances"].Columns.Add(dc);
                }

                if (!value.Tables["HoldingBalances"].Columns.Contains("IsMissingItem"))
                {
                    //set Is Missing Item to true
                    DataColumn dc = new DataColumn("IsMissingItem", typeof(bool));
                    dc.DefaultValue = true;
                    value.Tables["HoldingBalances"].Columns.Add(dc);
                }
            } 
            #endregion
           
            OrganizationListAction.ForEach("Administrator", OrganizationType.BankAccount, Organization,
                                           (csid, logical, bmc) =>
                                               {
                                                   if (logical.CMType == OrganizationTypeList.GetEntity(OrganizationType.BankAccount))
                                                   {
                                                       string accountNumber = bmc.GetDataStream((int)CmCommand.AccountNumber, string.Empty);
                                                       string bsb = bmc.GetDataStream((int)CmCommand.BSB, string.Empty);
                                                       if (!string.IsNullOrEmpty(bsb) && bsb.Contains('-'))
                                                           bsb = bsb.Split('-')[1];
                                                       string BsBNAccountNumber = bsb + accountNumber;

                                                       if (value.Tables.Contains("CashTransctions"))
                                                       {
                                                          var drs= value.Tables["CashTransctions"].Select(string.Format("BSBNAccountNumber='{0}'",BsBNAccountNumber));

                                                          foreach (var dr in drs)
                                                          {
                                                              dr["Message"] = "";
                                                              dr["HasErrors"] = false;
                                                              dr["IsMissingItem"] = false;

                                                              ValidateCashTransaction(dr);

                                                          }

                                                       }

                                                       if (value.Tables.Contains("HoldingBalances"))
                                                       {
                                                           var drs = value.Tables["HoldingBalances"].Select(string.Format("BSBNAccountNumber='{0}'", BsBNAccountNumber));
                                                           foreach (var dr in drs)
                                                           {
                                                               dr["Message"] = "";
                                                               dr["HasErrors"] = false;
                                                               dr["IsMissingItem"] = false;
                                                               ValidateHdlingBalances(dr);
                                                           }
                                                       }
                                                   }
                                               });


            #region create Merge message for validations
            if (!value.Tables.Contains(TransactionImportMessagesTable.Name))
            {
                TransactionImportMessagesTable resultTable = new TransactionImportMessagesTable();
                value.Tables.Add(resultTable);
            }
            else
            {
                value.Tables[TransactionImportMessagesTable.Name].Clear();
            }

            if (value.Tables.Contains("CashTransctions"))
            {
                foreach (DataRow datarow in value.Tables["CashTransctions"].Rows)
                {
                    DataRow dr = value.Tables[TransactionImportMessagesTable.Name].NewRow();
                    dr[TransactionImportMessagesTable.Message] = datarow[TransactionImportMessagesTable.Message];
                    dr[TransactionImportMessagesTable.HasErrors] = datarow[TransactionImportMessagesTable.HasErrors];
                    dr[TransactionImportMessagesTable.IsMissingItem] = datarow[TransactionImportMessagesTable.IsMissingItem];
                    dr[TransactionImportMessagesTable.InputMode] = "Transaction";
                    dr[TransactionImportMessagesTable.BankwestReferenceNumber] = datarow["BankwestReferenceNumber"];
                    dr[TransactionImportMessagesTable.BSBNAccountNumber] = datarow[TransactionImportMessagesTable.BSBNAccountNumber];
                    dr[TransactionImportMessagesTable.Date] = datarow["DateOfTransaction"];
                    dr[TransactionImportMessagesTable.Amount] = datarow["Amount"];
                    dr[TransactionImportMessagesTable.TransactionType] = datarow[TransactionImportMessagesTable.TransactionType];
                    dr[TransactionImportMessagesTable.TransactionDescription] = datarow[TransactionImportMessagesTable.TransactionDescription];
                    dr[TransactionImportMessagesTable.Type] = datarow["TransactionIndicator"];
                    value.Tables[TransactionImportMessagesTable.Name].Rows.Add(dr);
                }
            }

            if (value.Tables.Contains("HoldingBalances"))
            {
                foreach (DataRow datarow in value.Tables["HoldingBalances"].Rows)
                {
                    DataRow dr = value.Tables[TransactionImportMessagesTable.Name].NewRow();
                    dr[TransactionImportMessagesTable.Message] = datarow[TransactionImportMessagesTable.Message];
                    dr[TransactionImportMessagesTable.HasErrors] = datarow[TransactionImportMessagesTable.HasErrors];
                    dr[TransactionImportMessagesTable.IsMissingItem] = datarow[TransactionImportMessagesTable.IsMissingItem];
                    dr[TransactionImportMessagesTable.InputMode] = "Holding";
                    dr[TransactionImportMessagesTable.BSBNAccountNumber] = datarow[TransactionImportMessagesTable.BSBNAccountNumber];
                    dr[TransactionImportMessagesTable.TransactionDescription] = datarow["TransactionDescription"];
                    dr[TransactionImportMessagesTable.Date] = datarow["DateOfHolding"];
                    dr[TransactionImportMessagesTable.Amount] = datarow["HoldingAmount"];
                    dr[TransactionImportMessagesTable.UnsettledOrder] = datarow["UnsettledOrder"];
                    dr[TransactionImportMessagesTable.TransactionType] = datarow[TransactionImportMessagesTable.TransactionType];
                    dr[TransactionImportMessagesTable.BankwestReferenceNumber] = datarow[TransactionImportMessagesTable.BankwestReferenceNumber];
                    dr[TransactionImportMessagesTable.TransactionDescription] = datarow[TransactionImportMessagesTable.TransactionDescription];
                    dr[TransactionImportMessagesTable.Type] = datarow["TransactionIndicator"];
                    value.Tables[TransactionImportMessagesTable.Name].Rows.Add(dr);
                }
            } 
            #endregion



            return value;

        }

        private void ValidateHdlingBalances(DataRow dr)
        {
            DateTimeFormatInfo info = new DateTimeFormatInfo();
            info.ShortDatePattern = "dd/MM/yyyy";

            string Type = dr["TransactionIndicator"].ToString();
            string Amount = dr["HoldingAmount"].ToString();
            string DateOfHolding = dr["DateOfHolding"].ToString();
            string UnsettledOrder = dr["UnsettledOrder"].ToString();
            string TransactionType = dr["TransactionType"].ToString();
            string BankwestReferenceNumber = dr["BankwestReferenceNumber"].ToString();
            string TransactionDescription = dr["TransactionDescription"].ToString();
            DateTime date;
            decimal utemp;
           
            if (!DateTime.TryParse(DateOfHolding, info, DateTimeStyles.None, out date))
            {

                dr["Message"] += "Invalid Date Of Holding,";
                dr["HasErrors"] = true;
            }

            if (!string.IsNullOrEmpty(Amount))
                if (!decimal.TryParse(Amount, NumberStyles.Currency, CultureInfo.CurrentCulture, out utemp))
                {
                    dr["Message"] += "Invalid Holding Amount,";
                    dr["HasErrors"] = true;
                }

            if (string.IsNullOrEmpty(UnsettledOrder))
                if (!decimal.TryParse(UnsettledOrder, out utemp))
                {
                    dr["Message"] += "Invalid Unsettled Order,";
                    dr["HasErrors"] = true;
                }

        }

        private void ValidateCashTransaction(DataRow dr)
        {
            DateTimeFormatInfo info = new DateTimeFormatInfo();
            info.ShortDatePattern = "dd/MM/yyyy";

            string Type = dr["TransactionIndicator"].ToString();
            string BankwestReferenceNumber = dr["BankwestReferenceNumber"].ToString();
            string Amount = dr["Amount"].ToString();
            string DateOfTransaction = dr["DateOfTransaction"].ToString();
            string TransactionDescription = dr["TransactionDescription"].ToString();
            string TransactionType = dr["TransactionType"].ToString();


            DateTime date;
            decimal utemp;

            if (!DateTime.TryParse(DateOfTransaction, info, DateTimeStyles.None, out date))
            {

                dr["Message"] += "Invalid Date Of Transaction,";
                dr["HasErrors"] = true;
            }
           


            if (!string.IsNullOrEmpty(Amount))
                if (!decimal.TryParse(Amount, NumberStyles.Currency, CultureInfo.CurrentCulture, out utemp))
                {
                    dr["Message"] += "Invalid Amount,";
                    dr["HasErrors"] = true;
                }
              
            
        }

       
        

    }

   public class TransactionImportMessagesTable : DataTable
    {
        public const string Name = "ImportMessages";

        public const string BSBNAccountNumber = "BSBNAccountNumber";
        public const string Type = "Type";
        public const string InputMode = "InputMode";
        public const string BankwestReferenceNumber = "BankwestReferenceNumber";
        public const string Date = "Date";
        public const string Amount = "Amount";
        public const string TransactionDescription = "TransactionDescription";
        public const string TransactionType = "TransactionType";
        public const string UnsettledOrder = "UnsettledOrder";
        public const string Message = "Message";
        public new const string HasErrors = "HasErrors";
        public const string IsMissingItem = "IsMissingItem";

        public TransactionImportMessagesTable()
        {
            TableName = Name;
            Columns.Add(BSBNAccountNumber);
            Columns.Add(Type);
            Columns.Add(InputMode);
            Columns.Add(BankwestReferenceNumber);
            Columns.Add(Date);
            Columns.Add(Amount);
            Columns.Add(TransactionDescription);
            Columns.Add(TransactionType);
            Columns.Add(UnsettledOrder);
            DataColumn dc = new DataColumn(Message);
            dc.DefaultValue = "Account Number not Found";
            Columns.Add(dc);
            DataColumn dc1 = new DataColumn(HasErrors, typeof(bool));
            dc.DefaultValue = true;
            Columns.Add(dc1);


            DataColumn dc2 = new DataColumn(IsMissingItem, typeof(bool));
            dc2.DefaultValue = true;
            Columns.Add(dc2);

        }


    }


}
