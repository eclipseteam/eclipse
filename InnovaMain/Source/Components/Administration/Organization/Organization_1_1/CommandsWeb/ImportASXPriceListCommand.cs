﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Globalization;
using System.Linq;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{

    public class ImportASXPriceListCommand : OrganizationWebCommandBase
    {

        public override System.Data.DataSet DoAction(System.Data.DataSet value)
        {
            if (value.Tables.Count == 0)
                return value;

            if (!value.Tables[0].Columns.Contains("Message"))
            {
                //set message to not found
                DataColumn dc = new DataColumn("Message");
                dc.DefaultValue = "";
                value.Tables[0].Columns.Add(dc);
            }

            if (!value.Tables[0].Columns.Contains("HasErrors"))
            {
                //set has error to true
                DataColumn dc = new DataColumn("HasErrors", typeof(bool));
                dc.DefaultValue = false;
                value.Tables[0].Columns.Add(dc);
            }

            if (!value.Tables[0].Columns.Contains("IsMissingItem"))
            {
                //set Is Missing Item to true
                DataColumn dc = new DataColumn("IsMissingItem", typeof(bool));
                dc.DefaultValue = true;
                value.Tables[0].Columns.Add(dc);
            }
            
            foreach (DataRow dr in value.Tables[0].Rows)
            {
                dr["Message"] = "";
               AddNewSecurity(dr,Organization);
               ValidationRows(dr, Organization);                                
               
            }                      
            Organization.CalculateToken(true);
            return value;
        }


        private void AddNewSecurity(DataRow dr,OrganizationCM Organization)
        { 
                string InvestmentCode = dr["Code"].ToString();
                string CompanyName = dr["CompanyName"].ToString();
                SecuritiesEntity secuirtyEntity = Organization.Securities.Where(sec => sec.AsxCode == InvestmentCode).FirstOrDefault();
                if (secuirtyEntity == null)
                {
                    secuirtyEntity = new SecuritiesEntity
                                         {
                                             AsxCode = InvestmentCode,
                                             CompanyName = CompanyName,
                                             ID = Guid.NewGuid(),
                                             ASXSecurity = new ObservableCollection<ASXSecurityEntity>()
                                         };
                    dr["Message"] += "Investment code" + InvestmentCode + " has been added, ";
                    dr["IsMissingItem"] = true;
                    Organization.Securities.Add(secuirtyEntity);
                }  
        }        

        private void ValidationRows(DataRow dr, OrganizationCM Organization)
        {
            DateTime date;
            DateTimeFormatInfo info = new DateTimeFormatInfo();
            decimal utemp;
            info.ShortDatePattern = "dd/MM/yyyy";
            ASXSecurityEntity asxprice = new ASXSecurityEntity();
            asxprice.ID = Guid.NewGuid();
            if (string.IsNullOrEmpty(dr["Currency"].ToString()))
            {
                dr["Message"] += "Invalid Currency,";
                dr["HasErrors"] = true;
            }
            {
                asxprice.Currency = dr["Currency"].ToString();
            }
            if (!DateTime.TryParse(dr["PriceDate"].ToString(), info, DateTimeStyles.None, out date))
            {
                dr["Message"] += "Invalid Price Date,";
                dr["HasErrors"] = true;
            }
            else
            {
                asxprice.Date = date;
            }

            if (!string.IsNullOrEmpty(dr["Last"].ToString()))
                if (!decimal.TryParse(dr["Last"].ToString(), NumberStyles.Currency, CultureInfo.CurrentCulture, out utemp))
                {
                    dr["Message"] += "Invalid price,";
                    dr["HasErrors"] = true;
                }
                else if (utemp == 0)
                {
                    dr["Message"] += "\'Last\' (Unit Price) value is zero,";
                    dr["HasErrors"] = true;  
                }
                else
                {
                    asxprice.UnitPrice = Double.Parse(utemp.ToString());
                }
            if(!bool.Parse(dr["HasErrors"].ToString()))
            {
            var security = Organization.Securities.Where(sec => sec.AsxCode == dr["Code"].ToString()).FirstOrDefault();
            if (security != null)
            {
                if (security.ASXSecurity == null)
                    security.ASXSecurity = new ObservableCollection<ASXSecurityEntity>();
                if (security.ASXSecurity.Count(s => s.Date == date) > 0)
                {
                    dr["Message"] += "Price for date already exists,";
                    dr["HasErrors"] = true;
                }
                else
                {
                    security.ASXSecurity.Add(asxprice);
                     dr["Message"] +="Successfully added";
                }
            }
            else
            {
                dr["Message"] += "Invalid investment code,";
                dr["HasErrors"] = true;
            }
            }
        }
    }
}
