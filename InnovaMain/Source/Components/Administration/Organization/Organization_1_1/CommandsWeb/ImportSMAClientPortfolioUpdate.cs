﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Data;
using System.Data;
using Oritax.TaxSimp.CM.Organization.Commands;
using Oritax.TaxSimp.CM.Organization.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.Commands;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{
    public class ImportSMAClientPortfolioUpdate : ImportSMA
    {
        public override void ImportFromService(ImportProcessDS importProcessDS)
        {
            base.ImportFromService(importProcessDS);

            OrganisationListingDS organisationListingDS = new Data.OrganisationListingDS();
            organisationListingDS.OrganisationListingOperationType = OrganisationListingOperationType.EclipseSuper;
            Organization.GetData(organisationListingDS);

            ModelEntity modelEntityDIWM = Organization.Model.Where(model => model.ProgramCode == "0110").FirstOrDefault();
            ModelEntity modelEntityDIFM = Organization.Model.Where(model => model.ProgramCode == "0116").FirstOrDefault();

            SMAWebService smaWebService = new SMAWebService();
            if (importProcessDS.Variables.ContainsKey("MemberCode"))
            {
                string memberCode = importProcessDS.Variables["MemberCode"].ToString();
                Guid orgUnitGuid = new Guid(importProcessDS.Variables["OrgCid"].ToString());

                smaWebService.ResultDataSet = new SMAImportProcessDS();
                smaWebService.InvokeServiceMemberDataTransactions(memberCode, importProcessDS.Tables[ResultTableName], DateTime.Now.AddDays(1), DateTime.Now);
                if (smaWebService.ResultDataSet.Tables["MemberViewModel"] != null)
                {
                    IOrganizationUnit orgUnit = this.Organization.Broker.GetBMCInstance(orgUnitGuid) as IOrganizationUnit; 
                    this.Organization.Broker.SaveOverride = true;

                    ClientIndividualEntity clientIndividualEntity = orgUnit.ClientEntity as ClientIndividualEntity;
                    ClearMISForDIWMModel(modelEntityDIWM, orgUnit, clientIndividualEntity.ListAccountProcess, clientIndividualEntity.ManagedInvestmentSchemesAccounts);
                    UpdateModelMIS(modelEntityDIFM, smaWebService.ResultDataSet, (IOrganization)this.Organization, orgUnit, clientIndividualEntity.ListAccountProcess, clientIndividualEntity.ManagedInvestmentSchemesAccounts);
                    orgUnit.CalculateToken(true);
                    this.Organization.Broker.SetComplete();
                    this.Organization.Broker.SetStart();
                }

                importProcessDS.MessageNumber = "SMA006";
                importProcessDS.MessageSummary = "Updated Portfolio Structure";
                importProcessDS.MessageDetail = "Uploaded Portfolio Successfully";
            }
            else
            {
                importProcessDS.MessageNumber = "SMA006";
                importProcessDS.MessageSummary = "Cannot update Portfolio Structure";
                importProcessDS.MessageDetail = "Cannot update Portfolio";
            }
        }
    }
}
