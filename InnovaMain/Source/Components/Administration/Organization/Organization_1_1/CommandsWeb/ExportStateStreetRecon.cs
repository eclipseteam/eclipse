﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{
    class ExportStateStreetRecon : OrganizationWebCommandBase
    {       

        public override System.Data.DataSet DoAction(System.Data.DataSet value)
        {
            value.Tables.Clear();
            AOIDCSVColumnsRecon obj = new AOIDCSVColumnsRecon();
            obj.Table.Clear();
            value.Tables.Add(obj.Table);

            OrganizationListAction.ForEachClientOnly("Administrator", Organization, (csid, logical, bmc) =>
            {
                if ((bmc as OrganizationUnitCM).IsInvestableClient)
                {
                    (bmc as OrganizationUnitCM).GetData(value);
                }

            });
            return value;
        }
    }
}
