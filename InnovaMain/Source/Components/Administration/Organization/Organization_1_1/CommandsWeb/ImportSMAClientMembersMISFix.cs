﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Data;
using System.Data;
using Oritax.TaxSimp.CM.Organization.Commands;
using Oritax.TaxSimp.CM.Organization.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.Commands;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{
    [Serializable]
    public class ImportSMAClientMembersMISFix : ImportSMA
    {
       
        public override void ImportFromService(ImportProcessDS importProcessDS)
        {
            base.ImportFromService(importProcessDS);

            OrganisationListingDS organisationListingDS = new Data.OrganisationListingDS();
            organisationListingDS.OrganisationListingOperationType = OrganisationListingOperationType.EclipseSuper;
            Organization.GetData(organisationListingDS);

            IBrokerManagedComponent user = this.Organization.Broker.GetBMCInstance(Organization.Broker.UserContext.Identity.Name, "DBUser_1_1");
            object[] args = new Object[3] { Organization.CLID, user.CID, EnableSecurity.SecuritySetting };

            SMAWebService smaWebService = new SMAWebService();

            DataSet memberList = smaWebService.InvokeServiceMemberList(false);

            var duplicates = organisationListingDS.Tables[0].AsEnumerable().GroupBy(r => r["ENTITYECLIPSEID_FIELD"]).Where(gr => gr.Count() > 1);
           
            IOrganizationUnit manageFundBMC = null;
            List<string> membersToReImport = new List<string>();
            foreach (var duplicateItem in duplicates)
            {
                manageFundBMC = this.Organization.Broker.GetBMCInstance(new Guid("b354e928-a3c2-43d7-b61e-feb215cf1bd3")) as IOrganizationUnit;
                DataRow row = duplicateItem.FirstOrDefault();
                string eClipseID = row["ENTITYECLIPSEID_FIELD"].ToString();
                this.Organization.Broker.SaveOverride = true;
                ManagedInvestmentSchemesAccountEntity managedInvestmentSchemesAccountEntity = manageFundBMC.ClientEntity as ManagedInvestmentSchemesAccountEntity; 
                foreach(FundAccountEntity fundAccountEntity in managedInvestmentSchemesAccountEntity.FundAccounts)
                {
                    foreach (var x in fundAccountEntity.FundTransactions.ToList())
                    {
                        if (x.ClientID == eClipseID)
                            fundAccountEntity.FundTransactions.Remove(x);
                    }
                }
                manageFundBMC.CalculateToken(true); 
                this.Organization.Broker.SetComplete();
                
                this.Organization.Broker.SetStart();
                this.Organization.Broker.SaveOverride = true;

                foreach (DataRow dupRow in duplicateItem)
                {
                    string cid = dupRow["ENTITYCIID_FIELD"].ToString();
                    IOrganizationUnit orgUnit = this.Organization.Broker.GetBMCInstance(new Guid(cid)) as IOrganizationUnit;
                    membersToReImport.Add(orgUnit.SuperMemberID); 
                    orgUnit.EclipseClientID = string.Empty; 
                    orgUnit.CreateUniqueID();
                    orgUnit.CalculateToken(true); 
                }

                this.Organization.Broker.SetComplete();
                this.Organization.Broker.SetStart();
            }

            foreach (string memberCode in membersToReImport)
            {
                smaWebService.ResultDataSet = new SMAImportProcessDS();
                smaWebService.InvokeServiceMemberDataTransactions(memberCode, importProcessDS.Tables[ResultTableName], DateTime.Now.AddYears(-5), DateTime.Now);

                DataTable MemberViewModel = smaWebService.ResultDataSet.Tables["MemberViewModel"];
                if (MemberViewModel != null)
                {
                    SMASuperEntity smaSuperEntity = new SMASuperEntity();
                    smaSuperEntity.ImportSMAServiceEntityRow(MemberViewModel.Rows[0]);

                    DataRow[] entityRows = organisationListingDS.Tables[0].Select("ENTITYNAME_FIELD LIKE'%" + smaSuperEntity.EntityName + "%'");

                    string eclipseClientID = string.Empty;

                    if (entityRows.Count() > 0)
                    {
                        eclipseClientID = entityRows[0]["ENTITYECLIPSEID_FIELD"].ToString();

                        if (smaWebService.ResultDataSet.Tables["MemberViewModel"] != null)
                        {
                            this.Organization.Broker.SaveOverride = true;
                            SMAImportProcessDS sMAImportProcessDS = (SMAImportProcessDS)smaWebService.ResultDataSet;
                            sMAImportProcessDS.ClientID = eclipseClientID;
                            sMAImportProcessDS.MemberID = smaSuperEntity.MemberCode;
                            sMAImportProcessDS.SuperID = smaSuperEntity.ClientNumber;
                            sMAImportProcessDS.Securities = Organization.Securities;
                            manageFundBMC = this.Organization.Broker.GetBMCInstance(new Guid("b354e928-a3c2-43d7-b61e-feb215cf1bd3")) as IOrganizationUnit;
                            CreateUpdateManagedFunds(this.Organization.Broker, manageFundBMC, sMAImportProcessDS, smaSuperEntity.MemberCode, this.Organization.Securities, ClientID);
                        }
                    }
                }
            }

            importProcessDS.MessageNumber = "SMA004";
            importProcessDS.MessageSummary = "MIS Fix";
            importProcessDS.MessageDetail = "MIS Data Fixed Successfully";
        }
    }
}
