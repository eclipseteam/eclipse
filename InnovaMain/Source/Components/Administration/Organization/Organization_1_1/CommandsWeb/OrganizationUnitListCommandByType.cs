﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.CalculationInterface;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{

    public class OrganizationUnitListCommandByType : OrganizationWebCommandBase
    {
        public override DataSet DoAction(DataSet value)
        {
            IHasOrganizationUnit ds = value as IHasOrganizationUnit;
            OrganizationType type;
            OrganizationType.TryParse(ds.Unit.Type, true, out type);
            string userName = ds.Unit.CurrentUser.CurrentUserName;
            //ordercm and unsettledCm should be shown to all users.
            userName = OrganizationListAction.IsShownToAllUsers(type, ds.Unit.CurrentUser.CurrentUserName);
            DataTable bmcList = OrganizationListAction.GetBMCList(userName, type, Organization,ds.Unit.Name, string.Empty);
            IEnumerable<DataRow> rows = bmcList.Select();

           if(ds.Unit.Name != null && ds.Unit.Name != string.Empty)
               rows = bmcList.Select("ENTITYNAME_FIELD LIKE '%" + ds.Unit.Name + "%'");
           
            foreach (DataRow row in rows)
            {
                if (value is InstanceCMDS)
                {
                    var ds1 = value as InstanceCMDS;
                    DataRow dr = ds1.InstanceCMDetailsTable.NewRow();
                    dr[ds1.InstanceCMDetailsTable.CID] = new Guid(row["ENTITYCIID_FIELD"].ToString());
                    dr[ds1.InstanceCMDetailsTable.CLID] = new Guid(row["ENTITYCLID_FIELD"].ToString());
                    dr[ds1.InstanceCMDetailsTable.CSID] = new Guid(row["ENTITYCSID_FIELD"].ToString());
                    dr[ds1.InstanceCMDetailsTable.NAME] = row["ENTITYNAME_FIELD"].ToString();
                    dr[ds1.InstanceCMDetailsTable.TYPE] = Organization.Broker.GetComponentDictionary()[new Guid(row["ENTITYCTID_FIELD"].ToString())].Name;

                    //if ((bmc is OrganizationUnit))
                    //    dr[ds1.InstanceCMDetailsTable.STATUS] = (bmc as OrganizationUnit).OrganizationStatus;

                    ds1.InstanceCMDetailsTable.Rows.Add(dr);

                }
                else
                {
                    IBrokerManagedComponent bmc = Organization.Broker.GetBMCInstance(new Guid(row["ENTITYCIID_FIELD"].ToString()));
                    bmc.GetData(value);
                }
            }
            return value;
        }
    }
}
