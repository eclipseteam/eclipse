﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{
    public class GetUnlinkedAccount : OrganizationWebCommandBase
    {
        private IHasOrganizationUnit dataset = null;
        public override DataSet DoAction(DataSet value)
        {
            var unlinkedAccountDS = (UnlinkedAccountsDS)value;
            dataset = (IHasOrganizationUnit)value;
            var command = new OrganizationUnitListCommandByType { Organization = Organization };

            #region Get Desktop Broker


            var desktopBrokerAccountDs = new DesktopBrokerAccountDS
                                             {
                                                 CommandType = DatasetCommandTypes.Get,
                                                 Unit =new TaxSimp.Data.OrganizationUnit
                                                         {
                                                             CurrentUser = dataset.Unit.CurrentUser,
                                                             Type =((int) OrganizationType.DesktopBrokerAccount).ToString()
                                                         }
                                             };

            command.DoAction(desktopBrokerAccountDs);

            #endregion

            #region Get Bank Account


            var bankAccountDS = new BankAccountDS
                                    {
                                        CommandType = DatasetCommandTypes.Get,
                                        Unit =new TaxSimp.Data.OrganizationUnit
                                                {
                                                    CurrentUser = dataset.Unit.CurrentUser,
                                                    Type = ((int)OrganizationType.BankAccount).ToString()
                                                }
                                    };

            command.DoAction(bankAccountDS);

            #endregion

            #region Get MIS Account


            var managedInvestmentAccountDS = new ManagedInvestmentAccountDS
                                                 {
                                                     CommandType = DatasetCommandTypes.Get,
                                                     Unit =new TaxSimp.Data.OrganizationUnit
                                                             {
                                                                 CurrentUser = dataset.Unit.CurrentUser,
                                                                 Type =
                                                                     ((int)
                                                                      OrganizationType.ManagedInvestmentSchemesAccount).
                                                                     ToString()
                                                             }
                                                 };

            command.DoAction(managedInvestmentAccountDS);

            #endregion

            OrganizationListAction.ForEachClientOnly(dataset.Unit.CurrentUser.CurrentUserName, Organization, (csid, logical, bmc) =>
            {
                if ((bmc is OrganizationUnitCM) && (bmc as OrganizationUnitCM).IsInvestableClient)
                {
                    var unit = (bmc as OrganizationUnitCM);

                    #region Desktop Broker Remover

                    {


                        var newDS = new DesktopBrokerAccountDS();

                        unit.GetData(newDS);


                        var dt = desktopBrokerAccountDs.DesktopBrokerAccountsTable;
                        foreach (DataRow row in newDS.DesktopBrokerAccountsTable.Rows)
                        {
                            DataRow[] drs =desktopBrokerAccountDs.DesktopBrokerAccountsTable.Select(string.Format("{0} = '{1}'",dt.CID,row[dt.CID]));
                            if (drs.Length > 0)
                            {
                                for (var index = drs.Length - 1; index >= 0; index--)
                                {
                                    var dr = drs[index];
                                    dt.Rows.Remove(dr);
                                }
                            }
                        }

                       

                    }

                    #endregion

                    #region Bank Account Remover

                    {


                        var newDS = new BankAccountDS();

                        unit.GetData(newDS);

                        var dt = bankAccountDS.BankAccountsTable;
                        foreach (DataRow row in newDS.BankAccountsTable.Rows)
                        {
                            DataRow[] drs =
                                dt.Select(string.Format("{0} = '{1}'", dt.CID, row[dt.CID]));
                            if (drs.Length > 0)
                            {
                                for (var index = drs.Length - 1; index >= 0; index--)
                                {
                                    var dr = drs[index];
                                    dt.Rows.Remove(dr);
                                }
                            }
                        }
                        

                    }

                    #endregion

                    #region MIS Account Remover

                    {


                        var newDS = new ManagedInvestmentAccountDS();
                        unit.GetData(newDS);

                        var dt = managedInvestmentAccountDS.FundAccountsTable;
                        foreach (DataRow row in newDS.FundAccountsTable.Rows)
                        {
                            DataRow[] drs =
                                dt.Select(string.Format("{0} = '{1}' and {2} = '{3}'", dt.CID, row[dt.CID], dt.FUNDID,
                                                        row[dt.FUNDID]));
                            if (drs.Length > 0)
                            {
                                for (var index = drs.Length - 1; index >= 0; index--)
                                {
                                    var dr = drs[index];
                                    dt.Rows.Remove(dr);
                                }
                            }
                        }
                      

                    }
                    #endregion
                }

                

            });

            unlinkedAccountDS.bankAccounts = bankAccountDS.BankAccountsTable;
            unlinkedAccountDS.desktopBrokerAccounts = desktopBrokerAccountDs.DesktopBrokerAccountsTable;
            unlinkedAccountDS.fundAccountTable = managedInvestmentAccountDS.FundAccountsTable;
            return unlinkedAccountDS;
        }




    }


}
