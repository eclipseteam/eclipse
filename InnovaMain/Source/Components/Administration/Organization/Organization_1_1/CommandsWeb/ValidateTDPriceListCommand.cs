﻿using System;
using System.Data;
using System.Globalization;
using System.Linq;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{
    public class ValidateTDPriceListCommand : OrganizationWebCommandBase
    {
        public override DataSet DoAction(DataSet value)
        {
            if (value.Tables.Count == 0)
                return value;

            if (!value.Tables[0].Columns.Contains("Message"))
            {
                //set message to not found
                var dc = new DataColumn("Message") {DefaultValue = ""};
                value.Tables[0].Columns.Add(dc);
            }

            if (!value.Tables[0].Columns.Contains("HasErrors"))
            {
                //set has error to true
                var dc = new DataColumn("HasErrors", typeof (bool)) {DefaultValue = false};
                value.Tables[0].Columns.Add(dc);
            }

            if (!value.Tables[0].Columns.Contains("IsMissingItem"))
            {
                //set Is Missing Item to true
                var dc = new DataColumn("IsMissingItem", typeof (bool)) {DefaultValue = true};
                value.Tables[0].Columns.Add(dc);
            }

            foreach (DataRow dr in value.Tables[0].Rows)
            {
                ValidationRows(dr, Guid.Parse(value.ExtendedProperties["InstituteID"].ToString()));
            }


            return value;
        }
        private void ValidationRows(DataRow dr, Guid institutionId)
        {
            DateTime date;
            var info = new DateTimeFormatInfo();
            decimal utemp;
            info.ShortDatePattern = "dd/MM/yyyy";

            if (Organization.Institution.Count(sec => sec.Name == dr["Provider"].ToString()) == 0)
            {
                dr["Message"] += "Institute/Provider (" + dr["Provider"] + ") does not exist,";
                dr["HasErrors"] = false;
                dr["isMissingItem"] = true;
            }
            else
            {
                dr["isMissingItem"] = false;
            }

            if (string.IsNullOrEmpty(dr["SPLongTermRating"].ToString().Trim()))
            {
                dr["Message"] += "Invalid SP Long Term Rating,";
                dr["HasErrors"] = true;
            }

            if (string.IsNullOrEmpty(dr["ProviderType"].ToString().Trim()))
            {
                dr["Message"] += "Invalid Provider Type,";
                dr["HasErrors"] = true;
            }

            if (!string.IsNullOrEmpty(dr["Min"].ToString().Trim()))
                if (!decimal.TryParse(dr["Min"].ToString(), out utemp))
                {
                    dr["Message"] += "Invalid min price,";
                    dr["HasErrors"] = true;
                }

            if (!string.IsNullOrEmpty(dr["Max"].ToString().Trim()))
                if (!decimal.TryParse(dr["Max"].ToString(), out utemp))
                {
                    dr["Message"] += "Invalid max price,";
                    dr["HasErrors"] = true;
                }

            if (!string.IsNullOrEmpty(dr["MaximumBrokeage"].ToString().Trim()))
                if (!decimal.TryParse(dr["MaximumBrokeage"].ToString(), out utemp))
                {
                    dr["Message"] += "Invalid maximumBrokeage price,";
                    dr["HasErrors"] = true;
                }

            if (!string.IsNullOrEmpty(dr["Days30"].ToString().Trim()))
                if (!decimal.TryParse(dr["Days30"].ToString(), out utemp))
                {
                    dr["Message"] += "Invalid 30-days price,";
                    dr["HasErrors"] = true;
                }

            if (!string.IsNullOrEmpty(dr["Days60"].ToString().Trim()))
                if (!decimal.TryParse(dr["Days60"].ToString(), out utemp))
                {
                    dr["Message"] += "Invalid 60-days price,";
                    dr["HasErrors"] = true;
                }

            if (!string.IsNullOrEmpty(dr["Days90"].ToString().Trim()))
                if (!decimal.TryParse(dr["Days90"].ToString(), out utemp))
                {
                    dr["Message"] += "Invalid 90-days price,";
                    dr["HasErrors"] = true;
                }

            if (!string.IsNullOrEmpty(dr["Days120"].ToString().Trim()))
                if (!decimal.TryParse(dr["Days120"].ToString(), out utemp))
                {
                    dr["Message"] += "Invalid 120-days price,";
                    dr["HasErrors"] = true;
                }



            if (!string.IsNullOrEmpty(dr["Days150"].ToString().Trim()))
                if (!decimal.TryParse(dr["Days150"].ToString(), out utemp))
                {
                    dr["Message"] += "Invalid 150-days price,";
                    dr["HasErrors"] = true;
                }

            if (!string.IsNullOrEmpty(dr["Days180"].ToString().Trim()))
                if (!decimal.TryParse(dr["Days180"].ToString(), out utemp))
                {
                    dr["Message"] += "Invalid 180-days price,";
                    dr["HasErrors"] = true;
                }

            if (!string.IsNullOrEmpty(dr["Days270"].ToString().Trim()))
                if (!decimal.TryParse(dr["Days270"].ToString(), out utemp))
                {
                    dr["Message"] += "Invalid 270-days price,";
                    dr["HasErrors"] = true;
                }


            if (!string.IsNullOrEmpty(dr["Years1"].ToString().Trim()))
                if (!decimal.TryParse(dr["Years1"].ToString(), out utemp))
                {
                    dr["Message"] += "Invalid 1-Year price,";
                    dr["HasErrors"] = true;
                }

            if (!string.IsNullOrEmpty(dr["Years2"].ToString().Trim()))
                if (!decimal.TryParse(dr["Years2"].ToString(), out utemp))
                {
                    dr["Message"] += "Invalid 2-Year price,";
                    dr["HasErrors"] = true;
                }
            if (!string.IsNullOrEmpty(dr["Years3"].ToString().Trim()))
                if (!decimal.TryParse(dr["Years3"].ToString(), out utemp))
                {
                    dr["Message"] += "Invalid 3-Year price,";
                    dr["HasErrors"] = true;
                }
            if (!string.IsNullOrEmpty(dr["Years4"].ToString().Trim()))
                if (!decimal.TryParse(dr["Years4"].ToString(), out utemp))
                {
                    dr["Message"] += "Invalid 4-Year price,";
                    dr["HasErrors"] = true;
                }
            if (!string.IsNullOrEmpty(dr["Years5"].ToString().Trim()))
                if (!decimal.TryParse(dr["Years5"].ToString(), out utemp))
                {
                    dr["Message"] += "Invalid 5-Year price,";
                    dr["HasErrors"] = true;
                }

            if (!DateTime.TryParseExact(dr["Date"].ToString(), "dd/MM/yyyy", info, DateTimeStyles.None, out date))
            {
                dr["Message"] += "Invalid Price Date,";
                dr["HasErrors"] = true;
            }

            var productSec = Organization.Institution.Where(ps => ps.ID == institutionId).FirstOrDefault();
            if (productSec == null)
            {
                dr["Message"] += "Main Institution not found,";
                dr["HasErrors"] = true;
            }
        }


    }
}