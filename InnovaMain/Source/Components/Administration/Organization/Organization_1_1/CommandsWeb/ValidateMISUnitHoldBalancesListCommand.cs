﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using Oritax.TaxSimp.CM.OrganizationUnit;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{

    public class ValidateMISUnitHoldBalancesListCommand : OrganizationWebCommandBase
    {

        public override DataSet DoAction(DataSet value)
        {
            if (value.Tables.Count == 0)
                return value;

            if (!value.Tables[0].Columns.Contains("Message"))
            {
                //set message to not found
                DataColumn dc = new DataColumn("Message");
                dc.DefaultValue = "";
                value.Tables[0].Columns.Add(dc);
            }

            if (!value.Tables[0].Columns.Contains("HasErrors"))
            {
                //set has error to true
                DataColumn dc = new DataColumn("HasErrors", typeof(bool));
                dc.DefaultValue = false;
                value.Tables[0].Columns.Add(dc);
            }

            if (!value.Tables[0].Columns.Contains("IsMissingItem"))
            {
                //set Is Missing Item to true
                DataColumn dc = new DataColumn("IsMissingItem", typeof(bool));
                dc.DefaultValue = true;
                value.Tables[0].Columns.Add(dc);
            }
            string misName = value.ExtendedProperties["misName"].ToString();
            List<string> foundAccounts = new List<string>();
            OrganizationListAction.ForEachClientOnly("Administrator", Organization, (csid, logical, bmc) =>
            {
                if (bmc is OrganizationUnitCM)
                {
                    var unit = bmc as OrganizationUnitCM;
                    if (!string.IsNullOrEmpty(unit.ClientId))
                    {

                        DataRow[] drs = value.Tables[0].Select(string.Format("Account='{0}'", unit.ClientId));

                        if (drs.Length > 0)
                        {
                            foundAccounts.Add(unit.ClientId);
                            foreach (DataRow dr in drs)
                            {
                                dr["Message"] = "";
                                dr["HasErrors"] = false;
                                ValidationRow(dr, misName, unit);
                            }
                        }
                    }
                }
            });
            foreach (DataRow dr in value.Tables[0].Rows)
            {
                if (!foundAccounts.Contains(dr["Account"].ToString().Trim()))
                {
                    dr["Message"] += "Client with ID (" + dr["Account"].ToString().Trim() + ") does not exist.Skipped.";
                    dr["HasErrors"] = true;
                }
            }

            return value;
        }
        private void ValidationRow(DataRow dr, string misName, OrganizationUnitCM client)
        {



            bool isAttachedWithClient = client.IsMISAttachedWithFund(dr["FundCode"].ToString().Trim(), misName);
            if (!isAttachedWithClient)
            {
                dr["Message"] += "Fund Code (" + dr["FundCode"].ToString().Trim() + ") does not exist.Or Fund code is not attached with any client,";
                dr["HasErrors"] = true;
                dr["isMissingItem"] = true;
            }
            else
            {
                dr["isMissingItem"] = false;
            }

            DateTimeFormatInfo info = new DateTimeFormatInfo();
            info.ShortDatePattern = "dd/MM/yyyy";
            DateTime date;
            decimal utemp;
            if (!DateTime.TryParse(dr["AsOfDate"].ToString().Trim(), info, DateTimeStyles.None, out date))
            {
                dr["Message"] += "Invalid Price Date,";
                dr["HasErrors"] = true;
            }

            if (!string.IsNullOrEmpty(dr["RedemptionPrice"].ToString().Trim()))
                if (!decimal.TryParse(dr["RedemptionPrice"].ToString().Trim(), NumberStyles.Currency, CultureInfo.CurrentCulture, out utemp))
                {
                    dr["Message"] += "Invalid Unit Price,";
                    dr["HasErrors"] = true;
                }

            if (!string.IsNullOrEmpty(dr["CurrentValue"].ToString().Trim()))
                if (!decimal.TryParse(dr["CurrentValue"].ToString().Trim(), NumberStyles.Currency, CultureInfo.CurrentCulture, out utemp))
                {
                    dr["Message"] += "Invalid Current Value(Amount),";
                    dr["HasErrors"] = true;
                }
            if (!string.IsNullOrEmpty(dr["TotalShares"].ToString().Trim()))
                if (!decimal.TryParse(dr["TotalShares"].ToString().Trim(), NumberStyles.Currency, CultureInfo.CurrentCulture, out utemp))
                {
                    dr["Message"] += "Invalid Total Shares,";
                    dr["HasErrors"] = true;
                }
        }

    }
}
