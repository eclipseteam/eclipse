﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Enum;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Utilities;
using BankAccountEntity = Oritax.TaxSimp.Common.BankAccountEntity;
using Oritax.TaxSimp.CM.Organization.Data;
using Oritax.TaxSimp.DataSets;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{

    public class ValidateBankWestAccountOpeningsCommand : OrganizationWebCommandBase
    {

        public override System.Data.DataSet DoAction(System.Data.DataSet value)
        {
           if(value.Tables.Count==0)
               return value;
            
            if(!value.Tables[0].Columns.Contains("Message"))
            {
                //set message to not found
                DataColumn dc=new DataColumn("Message");
                dc.DefaultValue = "Client Account Number not Found";
                value.Tables[0].Columns.Add(dc);
            }

            if (!value.Tables[0].Columns.Contains("HasErrors"))
            {
                //set has error to true
                DataColumn dc = new DataColumn("HasErrors",typeof(bool));
                dc.DefaultValue = true;
                value.Tables[0].Columns.Add(dc);
            }

            if (!value.Tables[0].Columns.Contains("IsMissingItem"))
            {
                //set Is Missing Item to true
                DataColumn dc = new DataColumn("IsMissingItem", typeof(bool));
                dc.DefaultValue = true;
                value.Tables[0].Columns.Add(dc);
            }


            OrganisationListingDS organisationListingDS = new Data.OrganisationListingDS();
            organisationListingDS.OrganisationListingOperationType = OrganisationListingOperationType.BankAccounts;
            Organization.GetData(organisationListingDS);

            DataTable bankAccount = organisationListingDS.Tables[BankDetailsDS.BANKDETAILSTABLELIST]; 

            foreach(DataRow dr in  value.Tables[0].Rows)
            {
                string AccountNumber = dr["AccountNo"].ToString();
                string AccountName = dr["AccountName"].ToString();
                string BSB = dr["BSB"].ToString();

                var existingAccount = bankAccount.Select().Where(row => row[BankDetailsDS.ACCOUNTNUMBER].ToString() == AccountNumber).FirstOrDefault();
                ValidateAccount(dr, AccountNumber, AccountName, BSB, existingAccount);

            }

            return value;
        }

        private void ValidateAccount(DataRow dr, string AccountNumber, string AccountName, string BSB, DataRow existingAccount)
        {
            bool isSuccess = true;

            if (existingAccount != null)
            {
                dr["Message"] += "Account Number (" + AccountNumber + ") and BSB  (" + BSB + ") already exists,";
                dr["HasErrors"] = true;
                isSuccess = false;
            }

            if (string.IsNullOrEmpty(AccountName))
            {
                dr["Message"] += "Account Name (" + AccountName + ") is empty,";
                dr["HasErrors"] = true;
                isSuccess = false;
            }

            if (string.IsNullOrEmpty(AccountNumber))
            {
                dr["Message"] += "Account Number (" + AccountNumber + ") is empty,";
                dr["HasErrors"] = true;
                isSuccess = false;
            }


            if (string.IsNullOrEmpty(BSB))
            {
                dr["Message"] += "Account BSB (" + BSB + ") is empty,";
                dr["HasErrors"] = true;
                isSuccess = false;
            }

            if (isSuccess)
            {
                dr["Message"] = "Success";
                dr["HasErrors"] = false;
                dr["IsMissingItem"] = false;
            }
        }


        public string GetData(IBrokerManagedComponent bmc)
        {
            return bmc.GetDataStream(-911, string.Empty);// run default Command From CM Class
        }
       
    }
}
