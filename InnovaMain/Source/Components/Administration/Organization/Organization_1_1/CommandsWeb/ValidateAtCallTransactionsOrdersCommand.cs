﻿using System;
using System.Data;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Enum;
using Oritax.TaxSimp.Extensions;
using System.Globalization;
namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{
    public class ValidateAtCallTransactionsCommand : OrganizationWebCommandBase
    {
        public override DataSet DoAction(DataSet value)
        {
            if (value.Tables.Count == 0)
                return value;

            if (!value.Tables[0].Columns.Contains("Message"))
            {
                //set message to not found
                DataColumn dc = new DataColumn("Message");
                dc.DefaultValue = "";
                value.Tables[0].Columns.Add(dc);
            }

            if (!value.Tables[0].Columns.Contains("HasErrors"))
            {
                //set has error to true
                DataColumn dc = new DataColumn("HasErrors", typeof(bool));
                dc.DefaultValue = false;
                value.Tables[0].Columns.Add(dc);
            }

            if (!value.Tables[0].Columns.Contains("IsMissingItem"))
            {
                //set Is Missing Item to true
                DataColumn dc = new DataColumn("IsMissingItem", typeof(bool));
                dc.DefaultValue = false;
                value.Tables[0].Columns.Add(dc);
            }
            foreach (DataRow dr in value.Tables[0].Rows)
            {
                ValidationRows(dr);
            }   
            return value;
        }
        private void ValidationRows(DataRow dr)
        {
            decimal utemp;
            long temp;           
            if (string.IsNullOrEmpty(dr["CustomerNumber"].ToString()))
            {
                dr["Message"] += "Invalid Customer Number,";
                dr["HasErrors"] = true;
            }            
            if (string.IsNullOrEmpty(dr["Month"].ToString()))
            {
                dr["Message"] += "Invalid Month,";
                dr["HasErrors"] = true;
            }
            if (string.IsNullOrEmpty(dr["Balance"].ToString()))
            {
                dr["Message"] += "Invalid Balance,";
                dr["HasErrors"] = true;
            }           
            if (string.IsNullOrEmpty(dr["Amount"].ToString()))
            {
                dr["Message"] += "Invalid amount,";
                dr["HasErrors"] = true;
            }
            if ((!decimal.TryParse(dr["Balance"].ToString(), NumberStyles.Any, CultureInfo.CurrentCulture, out utemp)) && (!string.IsNullOrEmpty(dr["Balance"].ToString())))
            {
                dr["Message"] += "Invalid Balance,";
                dr["HasErrors"] = true;
            }
           
            if ((!decimal.TryParse(dr["Amount"].ToString(), NumberStyles.Any, CultureInfo.CurrentCulture, out utemp)) && (!string.IsNullOrEmpty(dr["Amount"].ToString())))
            {
                dr["Message"] += "Invalid amount,";
                dr["HasErrors"] = true;
            }
            string CustNumber = string.Empty;
            if (dr["CustomerNumber"].ToString().Contains(" "))
            {
                CustNumber = dr["CustomerNumber"].ToString().Replace(" ", "");
            }
            else
            {
                CustNumber = dr["CustomerNumber"].ToString();
            }
            string AccNumber = string.Empty;
            if (dr["AccNumber"].ToString().Contains(" "))
            {
                AccNumber = dr["AccNumber"].ToString().Replace(" ", "");
            }
            else
            {
              AccNumber = dr["AccNumber"].ToString();  
            }

            if ((!long.TryParse(CustNumber.Trim(), NumberStyles.Any, CultureInfo.CurrentCulture, out temp)) && (!string.IsNullOrEmpty(CustNumber.ToString())))
            {
                dr["Message"] += "Invalid Customer Number,";
                dr["HasErrors"] = true;
            }           

        }
    }
}
