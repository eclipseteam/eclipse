﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Data;
using System.Data;
using Oritax.TaxSimp.CM.Organization.Commands;
using Oritax.TaxSimp.CM.Organization.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.Commands;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{
    public class ImportSMAAdviserList : ImportSMA
    {
        public override void ImportFromService(ImportProcessDS importProcessDS)
        {
            base.ImportFromService(importProcessDS);

            SMAWebService smaWebService = new SMAWebService();
            smaWebService.ResultDataSet = new SMAImportProcessDS();
            smaWebService.ResultDataSet  = smaWebService.InvokeServiceAdviserList(false);
            importProcessDS.Tables.Clear();
            importProcessDS.Merge(smaWebService.ResultDataSet); 
            importProcessDS.MessageNumber = "SMA010";
            importProcessDS.MessageSummary = "Advisers Listing Downloaded Successfully";
            importProcessDS.MessageDetail = "Advisers Listing Downloaded Successfully";
        }
    }
}
