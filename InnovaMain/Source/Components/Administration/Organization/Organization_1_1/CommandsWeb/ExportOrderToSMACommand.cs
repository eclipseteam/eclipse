﻿using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Web;
using Oritax.TaxSimp.CM.Organization.CommandsWeb.Services;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using System;
using Oritax.TaxSimp.DataSets;
using System.Linq;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{
    public class ExportOrderToSMACommand : OrganizationWebCommandBase
    {
        public override DataSet DoAction(DataSet value)
        {
            if (value is SMAOrderExportDS)
            {
                var ds = value as SMAOrderExportDS;
                OrganizationWebCommandBase command = null;
                var requestId = string.Empty;
                string zipFileName = string.Empty;

                string filename = string.Empty;
                var output = new SerializableDictionary<string, byte[]>();
                var sendtoService = true;
                if (Organization.OrganizationalSettings != null)
                {
                    sendtoService = Organization.OrganizationalSettings.SendToSMAService;
                }
                switch (ds.Operation)
                {
                    case SMAOperationType.SingleOrder:
                        {
                            var order = ds.Data as OrderEntity;

                            if (order.ClientManagementType == ClientManagementType.SMA)
                            {
                                requestId = "Sing" + order.OrderID.ToString() + "-" + order.SMARequestCount++;
                                filename = string.Format("{0}_{1}", order.OrderID, order.ClientID);
                                zipFileName = "SMA_InstructionsExport_" + filename;
                                order.SMACallStatus = !sendtoService ? SMACallStatus.NotSent : SMACallStatus.Failed;
                                switch (order.OrderAccountType)
                                {
                                    case OrderAccountType.TermDeposit:
                                        switch (order.OrderItemType)
                                        {
                                            case OrderItemType.Buy:
                                                command = new ExportSMABuyTDOrderCommand();
                                                break;
                                            case OrderItemType.Sell:
                                                command = new ExportSMASellTDOrderCommand();
                                                break;
                                        }
                                        break;

                                    case OrderAccountType.ASX:
                                        if (order.OrderItemType == OrderItemType.Buy)
                                        {
                                            command = new ExportSMABuyASXOrderCommand();
                                        }
                                        else if (order.OrderItemType == OrderItemType.Sell)
                                        {
                                            command = order.OrderPreferedBy == OrderPreferedBy.Units ? (OrganizationWebCommandBase)new ExportSMASellUnitsASXOrderCommand() : new ExportSMASellAmountASXOrderCommand();
                                        }
                                        break;
                                    case OrderAccountType.StateStreet:
                                        if (order.OrderItemType == OrderItemType.Buy)
                                        {
                                            command = new ExportSMABuyMISOrderCommand();
                                        }
                                        else if (order.OrderItemType == OrderItemType.Sell)
                                        {
                                            command = order.OrderPreferedBy == OrderPreferedBy.Units ? (OrganizationWebCommandBase)new ExportSMASellUnitsMISOrderCommand() : new ExportSMASellAmountMISOrderCommand();
                                        }
                                        break;
                                }
                            }
                        }
                        break;
                    case SMAOperationType.BulkOrders:
                        {
                            var keyvalue = (KeyValuePair<BulkOrderEntity, List<OrderEntity>>)ds.Data;
                            var bulkorder = keyvalue.Key;
                            var orders = keyvalue.Value;
                            if (bulkorder.ClientManagementType == ClientManagementType.SMA)
                            {
                                requestId = "Bulk" + bulkorder.BulkOrderID.ToString() + "-" + bulkorder.SMARequestCount++;
                                filename = string.Format("{0}_", bulkorder.BulkOrderID);
                                zipFileName = "SMA_BulkInstructionsExport_" + filename;
                                bulkorder.SMACallStatus = !sendtoService ? SMACallStatus.NotSent : SMACallStatus.Failed;

                                switch (bulkorder.OrderAccountType)
                                {
                                    case OrderAccountType.ASX:
                                        if (bulkorder.OrderItemType == OrderItemType.Buy)
                                        {
                                            command = new ExportSMABuyASXBulkOrderCommand();
                                        }
                                        else if (bulkorder.OrderItemType == OrderItemType.Sell)
                                        {
                                            command = bulkorder.OrderPreferedBy == OrderPreferedBy.Units ? (OrganizationWebCommandBase)new ExportSMASellUnitsASXBulkOrderCommand() : new ExportSMASellAmountASXBulkOrderCommand();
                                        }
                                        break;
                                    case OrderAccountType.TermDeposit:
                                        if (bulkorder.OrderItemType == OrderItemType.Buy)
                                        {
                                            command = new ExportSMABuyTDBulkOrderCommand();
                                        }
                                        else if (bulkorder.OrderItemType == OrderItemType.Sell)
                                        {
                                            command = new ExportSMASellAmountTDBulkOrderCommand();
                                        }
                                        break;
                                }
                            }
                        }
                        break;
                }

                if (command != null)
                {
                    try
                    {
                        command.Organization = this.Organization;
                        command.DoAction(ds);
                        SubmitTradeResultViewModel trans;
                        string messages;
                        output.Add(filename + ".csv", StrToByteArray(ds.OutputData));
                        if (SendData(requestId, ds.OutputData, out trans, out  messages, sendtoService) && trans != null)
                        {
                            switch (ds.Operation)
                            {
                                case SMAOperationType.SingleOrder:
                                    {
                                        var order = ds.Data as OrderEntity;
                                        order.SMARequestID = requestId;
                                        order.SMACallStatus = SMACallStatus.Successful;
                                        foreach (var item in order.Items)
                                        {
                                            if (item is ASXOrderItem)
                                            {
                                                var asxitem = item as ASXOrderItem;
                                                var trade = trans.Trades.FirstOrDefault(ss => ss.InvestmentManagerCode == asxitem.InvestmentCode);
                                                if (trade != null)
                                                    item.SMATransactionID = trade.TransactionReference;
                                            }
                                            else if (item is StateStreetOrderItem)
                                            {
                                                var misitem = item as StateStreetOrderItem;
                                                var trade = trans.Trades.FirstOrDefault(ss => ss.InvestmentManagerCode == misitem.FundCode);
                                                if (trade != null)
                                                    item.SMATransactionID = trade.TransactionReference;
                                            }
                                            else if (item is TermDepositOrderItem)
                                            {
                                                var misitem = item as TermDepositOrderItem;
                                                var trade = trans.Trades.FirstOrDefault(ss => ss.DollarAmount == misitem.Amount);
                                                if (trade != null)
                                                    item.SMATransactionID = trade.TransactionReference;
                                            }
                                        }
                                    }
                                    break;
                                case SMAOperationType.BulkOrders:
                                    {
                                        var keyvalue = (KeyValuePair<BulkOrderEntity, List<OrderEntity>>)ds.Data;
                                        var bulkorder = keyvalue.Key;
                                        bulkorder.SMARequestID = requestId;
                                        bulkorder.SMACallStatus = SMACallStatus.Successful;
                                        //  bulkorders.SMATransactionID = trans;
                                    }
                                    break;
                                default:
                                    throw new Exception("Invalid Command");
                            }
                        }
                        else if (!string.IsNullOrEmpty(messages))
                        {
                            // service is configured but there  are errors
                            value.ExtendedProperties.Add("SMAExportMessage", messages);
                        }
                    }
                    catch (Exception ex)
                    {
                        value.ExtendedProperties.Add("SMAExportMessage", ex.Message);
                    }

                    string message = (sendtoService ? "SuccessFully sent:" : "Service option disabled by Admin.") + DateTime.Now;
                    var haserrors = "";
                    if (value.ExtendedProperties.Contains("SMAExportMessage"))
                    {
                        message = (string)value.ExtendedProperties["SMAExportMessage"];
                        haserrors = "_withErrors_";
                    }
                    output.Add(filename + "_log.txt", StrToByteArray(message));
                    var bytes = new byte[] { };
                    if (output.Count > 0)
                    {
                        bytes = Zipper.CreateZip(output);
                    }
                    string dirPath = HttpContext.Current.ApplicationInstance.Server.MapPath("~/ExportedOrdersFiles") + "/SMA/";
                    if (!Directory.Exists(dirPath))
                    {
                        Directory.CreateDirectory(dirPath);
                    }
                    string path = dirPath + zipFileName + haserrors + DateTime.Now.ToString("dd_MM_yyyy_hhmmssFFF") + ".zip";

                    File.WriteAllBytes(path, bytes);
                }
            }
            return value;
        }

        public static string GetClientSuperID(Guid clientCID, IOrganization org)
        {
            var clientdta = org.Broker.GetBMCInstance(clientCID) as IOrganizationUnit;
            string clientSuperID = string.Empty;
            if (clientdta != null)
            {
                clientSuperID = clientdta.SuperMemberID;

                org.Broker.ReleaseBrokerManagedComponent(clientdta);
            }
            return clientSuperID;
        }
        public static string GetInstituteName(Guid id, IOrganization org)
        {
            var clientdta = org.Institution.FirstOrDefault(ii => ii.ID == id);
            string instCode = string.Empty;
            if (clientdta != null)
            {
                instCode = clientdta.TextInstitutionCode;
            }
            return instCode;
        }

        private bool SendData(string requestId, string outputData, out SubmitTradeResultViewModel transaction, out string messages, bool sendtoService)
        {
            bool isSuccess = false;
            transaction = null;
            messages = string.Empty;
            if (sendtoService)
            {
                if (!string.IsNullOrEmpty(outputData))
                {
                    var smaTradeService = new SMAExportService();
                    //return if organizational settings says don't send to sma.
                    SubmitTradeResultViewModel result = smaTradeService.SendData(outputData, requestId);

                    if (result != null)
                    {
                        isSuccess = result.Status == TradeResultStatus.Success;

                        if (isSuccess)
                        {
                            transaction = result;
                        }
                        else
                        {
                            messages = "Data:" + Environment.NewLine + outputData + Environment.NewLine + "Messages:" + Environment.NewLine;
                            foreach (var error in result.Errors)
                            {
                                messages += string.Format("{0} Code:{1} LineNumber:{2} {3}", error.ErrorMessage, error.ErrorCode, error.LineNumber, Environment.NewLine);
                            }
                        }
                    }
                    else
                    {
                        messages = "Data:" + Environment.NewLine + outputData + Environment.NewLine + "Messages:" + Environment.NewLine;
                        messages += string.Format("UNKNOWN Error  while sending to service{0}", Environment.NewLine);
                    }
                }
                else
                {
                    messages = "Data:" + Environment.NewLine + outputData + Environment.NewLine + "Messages:" + Environment.NewLine;
                    messages += string.Format("No Data Found to send to service{0}", Environment.NewLine);
                }
            }
            return isSuccess;
        }

        public static byte[] StrToByteArray(string str)
        {
            UnicodeEncoding encoding = new UnicodeEncoding();
            return encoding.GetBytes(str);
        }
    }
}
