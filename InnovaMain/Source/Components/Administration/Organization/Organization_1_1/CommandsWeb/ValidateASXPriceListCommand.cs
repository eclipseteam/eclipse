﻿using System;
using System.Data;
using System.Globalization;
using System.Linq;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{

    public class ValidateASXPriceListCommand : OrganizationWebCommandBase
    {

        public override System.Data.DataSet DoAction(System.Data.DataSet value)
        {
           if(value.Tables.Count==0)
               return value;
            
            if(!value.Tables[0].Columns.Contains("Message"))
            {
                //set message to not found
                DataColumn dc=new DataColumn("Message");
                dc.DefaultValue = "";
                value.Tables[0].Columns.Add(dc);
            }

            if (!value.Tables[0].Columns.Contains("HasErrors"))
            {
                //set has error to true
                DataColumn dc = new DataColumn("HasErrors",typeof(bool));
                dc.DefaultValue = false;
                value.Tables[0].Columns.Add(dc);
            }

            if (!value.Tables[0].Columns.Contains("IsMissingItem"))
            {
                //set Is Missing Item to true
                DataColumn dc = new DataColumn("IsMissingItem", typeof(bool));
                dc.DefaultValue = true;
                value.Tables[0].Columns.Add(dc);
            }
            
            foreach (DataRow dr in value.Tables[0].Rows)
            {
                ValidationRows(dr, Organization);                
            }                     
                   

            return value;
        }
        private void ValidationRows(DataRow dr, OrganizationCM Organization)
        {
            DateTime date;
            DateTimeFormatInfo info = new DateTimeFormatInfo();
            decimal utemp;
            info.ShortDatePattern = "dd/MM/yyyy";

            if (Organization.Securities.Count(sec => sec.AsxCode == dr["Code"].ToString()) == 0)
            {
                dr["Message"] += "Investment code (" + dr["Code"].ToString() + ") doesn't exists,";
                dr["HasErrors"] = false;
                dr["isMissingItem"] = true;
            }
            else
            {
                dr["isMissingItem"] = false;
            }
            if (string.IsNullOrEmpty(dr["Currency"].ToString()))
            {
                dr["Message"] += "Invalid Currency,";
                dr["HasErrors"] = true;
            }
            if (!DateTime.TryParse(dr["PriceDate"].ToString(), info, DateTimeStyles.None, out date))
            { 
                dr["Message"] += "Invalid Price Date,";
                dr["HasErrors"] = true;
            }
                       
            if (!string.IsNullOrEmpty(dr["Last"].ToString()))
                if (!decimal.TryParse(dr["Last"].ToString(), NumberStyles.Currency, CultureInfo.CurrentCulture, out utemp))
                {
                    dr["Message"] += "Invalid price,";
                    dr["HasErrors"] = true;                       
                }
                else if (utemp == 0)
                {
                    dr["Message"] += "\'Last\' (Unit Price) value is zero,";
                    dr["HasErrors"] = true;
                }


            var security = Organization.Securities.Where(sec => sec.AsxCode == dr["Code"].ToString()).FirstOrDefault();
            if (security != null)
            {
                if (security.ASXSecurity != null && security.ASXSecurity.Count(s => s.Date == date) > 0)
                {
                    dr["Message"] += "Price for date already exists,";
                    dr["HasErrors"] = true;
                }
            }
        }
       
       
    }
}
