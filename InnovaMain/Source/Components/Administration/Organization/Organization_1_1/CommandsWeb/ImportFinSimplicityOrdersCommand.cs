﻿using System;
using System.Data;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Enum;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.Organization.Data;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{
    public class ImportFinSimplicityCommand : OrganizationWebCommandBase
    {
        public override DataSet DoAction(DataSet value)
        {
            if (value.ExtendedProperties["GetSquanceNumber"] != null)
            {
                OrganizationListAction.ForEach("Administrator", OrganizationType.Order, Organization, (csid, logical, bmc) =>
                {
                    if (bmc is OrganizationUnitCM && OrganizationListAction.IsValidClientType(logical.CMTypeName))
                    {
                        var unit = (bmc as OrganizationUnitCM);
                        unit.SetData(value);
                    }
                });
                return value;
            }
            if (value.Tables.Count == 0)
                return value;
            if (!value.Tables["State Street"].Columns.Contains("Message"))
            {
                //set message to not found
                DataColumn dc = new DataColumn("Message");
                dc.DefaultValue = "Client Account  Not Found";
                value.Tables["State Street"].Columns.Add(dc);
            }
            if (!value.Tables["State Street"].Columns.Contains("HasErrors"))
            {
                //set has error to true
                DataColumn dc = new DataColumn("HasErrors", typeof(bool));
                dc.DefaultValue = true;
                value.Tables["State Street"].Columns.Add(dc);
            }
            if (!value.Tables["State Street"].Columns.Contains("IsMissingItem"))
            {
                //set Is Missing Item to true
                DataColumn dc = new DataColumn("IsMissingItem", typeof(bool));
                dc.DefaultValue = true;
                value.Tables["State Street"].Columns.Add(dc);
            }
            if (!value.Tables["State Street"].Columns.Contains("ClientCID"))
            {
                //set Is Missing Item to true
                DataColumn dc = new DataColumn("ClientCID", typeof(Guid));

                value.Tables["State Street"].Columns.Add(dc);
            }
            if (!value.Tables["State Street"].Columns.Contains("ProductID"))
            {
                //set Is Missing Item to true
                DataColumn dc = new DataColumn("ProductID", typeof(Guid));

                value.Tables["State Street"].Columns.Add(dc);
            }
            if (!value.Tables["State Street"].Columns.Contains("AccountCID"))
            {
                //set Is Missing Item to true
                DataColumn dc = new DataColumn("AccountCID", typeof(Guid));

                value.Tables["State Street"].Columns.Add(dc);
            }
            if (!value.Tables["State Street"].Columns.Contains("AccountNo"))
            {
                //set Is Missing Item to true
                DataColumn dc = new DataColumn("AccountNo", typeof(string));

                value.Tables["State Street"].Columns.Add(dc);
            }
            if (!value.Tables["State Street"].Columns.Contains("AccountBSB"))
            {
                //set Is Missing Item to true
                DataColumn dc = new DataColumn("AccountBSB", typeof(string));

                value.Tables["State Street"].Columns.Add(dc);
            }
        
             IBrokerManagedComponent user = Organization.Broker.GetBMCInstance(this.Organization.Broker.UserContext.Identity.Name, "DBUser_1_1");
            object[] args = new Object[3] { Organization.CLID, user.CID, EnableSecurity.SecuritySetting };
            DataView entitiesView = new DataView(Organization.Broker.GetBMCDetailList(OrganizationInstall.ASSEMBLY_NAME, args).Tables[OrganisationListingDS.TableName]);
            entitiesView.RowFilter = new OrganisationListingDS().GetRowFilterAllClientsExceptEclipse();
            DataTable entitiesTable = entitiesView.ToTable();

            foreach (DataRow entityRow in entitiesTable.Rows)
            {
                DataRow[] drs = value.Tables["State Street"].Select(string.Format("{0}='{1}'", "[Oritax Id]", entityRow["ENTITYECLIPSEID_FIELD"].ToString()));

                if (drs.Length > 0)
                {
                    foreach (var dr in drs)
                    {
                        IOrganizationUnit iorgUnit = this.Organization.Broker.GetBMCInstance(new Guid(entityRow["ENTITYCIID_FIELD"].ToString())) as IOrganizationUnit;
                      
                        dr["Message"] = "";
                        dr["HasErrors"] = false;
                        dr["IsMissingItem"] = false;
                        dr["ClientCID"] = iorgUnit.CID;
                        string idslist = iorgUnit.GetDataStream((int)CmCommand.AccountProcessFundCodeProductID, dr["Investment Code"].ToString());
                        SerializableDictionary<string, string> ids = idslist.ToNewOrData<SerializableDictionary<string, string>>();
                        dr["ProductID"] = string.IsNullOrEmpty(ids["ProductID"].ToString()) ? Guid.Empty : Guid.Parse(ids["ProductID"].ToString());
                        dr["AccountCID"] = string.IsNullOrEmpty(ids["AccountCID"].ToString()) ? Guid.Empty : Guid.Parse(ids["AccountCID"].ToString());
                        dr["AccountNo"] = ids["AccountNo"];
                        dr["AccountBSB"] = ids["AccountBSB"];
                        ValidationRows(dr);
                    }
                }
            }
           
            #region Client Checks

            if (OrganizationListAction.GetUnitsCount("Administrator", OrganizationType.Order, Organization) <= 0)
            {
                CreateOrderCMObject(value);
            }
            else
            {
                OrganizationListAction.ForEach("Administrator", OrganizationType.Order, Organization, (csid, logical, bmc) =>
                {
                    if (bmc is OrganizationUnitCM && OrganizationListAction.IsValidClientType(logical.CMTypeName))
                    {
                        var unit = (bmc as OrganizationUnitCM);
                        unit.SetData(value);
                    }
                });
            }

            if (value.Tables["State Street"].Columns.Contains("ClientCID"))
            {
                value.Tables["State Street"].Columns.Remove("ClientCID");
               
            }
            if (value.Tables["State Street"].Columns.Contains("ProductID"))
            {
                value.Tables["State Street"].Columns.Remove("ProductID");
               
            }
            if (value.Tables["State Street"].Columns.Contains("AccountCID"))
            {
                value.Tables["State Street"].Columns.Remove("AccountCID");
            }
            if (value.Tables["State Street"].Columns.Contains("AccountNo"))
            {
                value.Tables["State Street"].Columns.Remove("AccountNo");

            }
            if (value.Tables["State Street"].Columns.Contains("AccountBSB"))
            {
                value.Tables["State Street"].Columns.Remove("AccountBSB");

            }
            #endregion

            return value;
        }

        private void CreateOrderCMObject(DataSet ds)
        {
            OrganizationAddCommand addCommand = new OrganizationAddCommand();
            addCommand.Organization = this.Organization;
            addCommand.DoAction(ds);
        }
        
        private void ValidationRows(DataRow dr)
        {

            string clientId = dr["Oritax Id"].ToString();
            string investmentCode = dr["Investment Code"].ToString();
            string orderType = dr["Order Type"].ToString();
            string amount = dr["Order Amount"].ToString();
            string units = dr["Rebalancing Units"].ToString();

            Guid productID = Guid.Parse(dr["ProductID"].ToString());

            if (productID == Guid.Empty)
            {
                dr["Message"] += "Client does not contains investment code(" + investmentCode + ") in account process,";
                dr["HasErrors"] = true;
            }

            if (string.IsNullOrEmpty(clientId))
            {
                dr["Message"] += "Client ID (" + clientId + ") is empty,";
                dr["HasErrors"] = true;
            }


            if (string.IsNullOrEmpty(investmentCode))
            {
                dr["Message"] += "Investment Code (" + investmentCode + ") is empty,";
                dr["HasErrors"] = true;
            }
            OrderItemType ordertype = OrderItemType.Buy;
            bool hasOrderType = true;
            if (string.IsNullOrEmpty(orderType) || !System.Enum.TryParse(orderType, true, out ordertype))
            {
                dr["Message"] += "Order Type(" + orderType + ") is empty or Invalid,";
                dr["HasErrors"] = true;
                hasOrderType = false;
            }

            if (hasOrderType)
            {
                switch (ordertype)
                {
                    case OrderItemType.Buy:
                        {
                            decimal temp;
                            if (string.IsNullOrEmpty(amount) || !decimal.TryParse(amount, out temp))
                            {
                                dr["Message"] += "Amount(" + amount + ") is empty or Invalid,";
                                dr["HasErrors"] = true;

                            }
                            if (!string.IsNullOrEmpty(units) && !decimal.TryParse(units, out temp))
                            {
                                dr["Message"] += "Units(" + amount + ") is  Invalid,";
                                dr["HasErrors"] = true;

                            }
                        }

                        break;
                    case OrderItemType.Sell:
                        {
                            decimal temp;
                            if (string.IsNullOrEmpty(units) || !decimal.TryParse(units, out temp))
                            {
                                dr["Message"] += "Units(" + units + ") is empty or Invalid,";
                                dr["HasErrors"] = true;

                            }
                            if (!string.IsNullOrEmpty(amount) && !decimal.TryParse(amount, out temp))
                            {
                                dr["Message"] += "Amount(" + amount + ") is  Invalid,";
                                dr["HasErrors"] = true;

                            }
                        }
                        break;


                }
            }
            else
            {
                decimal temp;
                if (!string.IsNullOrEmpty(amount) && !decimal.TryParse(amount, out temp))
                {
                    dr["Message"] += "Amount(" + amount + ") is  Invalid,";
                    dr["HasErrors"] = true;

                }
                if (!string.IsNullOrEmpty(units) && !decimal.TryParse(units, out temp))
                {
                    dr["Message"] += "Units(" + amount + ") is  Invalid,";
                    dr["HasErrors"] = true;

                }
            }







        }
        
    }
}
