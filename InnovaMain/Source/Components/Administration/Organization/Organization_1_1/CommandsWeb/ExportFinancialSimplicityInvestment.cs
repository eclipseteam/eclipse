﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Data;
using System.Data;
using Oritax.TaxSimp.CM.Organization.Commands;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{
    class ExportFinancialSimplicityInvestment : OrganizationWebCommandBase
    {
        string TableName = "FinInvestment";
        string InvestmentCode = "investment_code";
        string MarketCode = "market_code";
        string CurrencyCode = "currency_code";
        string InvestmentType = "investment_type";
        string InvestmentName = "investment_name";
        string UnitPrice = "unit_price";
        string AssetClass = "asset_class";
        public override System.Data.DataSet DoAction(System.Data.DataSet value)
        {
            value.Tables.Clear();
            value.Tables.Add(CreateFinSimplicityInvestTable());
            foreach (var record in CreateExportFinSimplicityObjects())
            {
                DataRow dr = value.Tables[TableName].NewRow();
                dr[InvestmentCode] = record.InvestmentCode;
                dr[MarketCode] = record.MarketCode;
                dr[CurrencyCode] = record.CurrencyCode;
                dr[InvestmentType] = record.InvestmentType;
                dr[InvestmentName] = record.InvestmentName;
                dr[UnitPrice] = record.UnitPrice;
                //changes made on babar request 
                dr[AssetClass] = string.Empty;//record.AssetClass;
                value.Tables[TableName].Rows.Add(dr);
            }
            return value;
        }

        private List<FinSimplicityInvestment> CreateExportFinSimplicityObjects()
        {
            List<FinSimplicityInvestment> records = new List<FinSimplicityInvestment>();

            foreach (var security in Organization.Securities)
            {
                if (!security.AsxCode.Contains("test"))
                {
                    FinSimplicityInvestment record = new FinSimplicityInvestment();
                    record.InvestmentCode = security.AsxCode;
                    record.MarketCode = (security.InvestmentType.Trim() == "Managed Fund /Unit Trust") ? String.Empty : "ASX";
                    record.CurrencyCode = "AUD";
                    record.InvestmentType = "" + GetInvestmentTypeNumber(security.InvestmentType);
                    record.InvestmentName = security.CompanyName;
                    var asset = Organization.Assets.Where(ss => ss.ID == security.AssetID).FirstOrDefault();

                    if (asset != null)
                        record.AssetClass = asset.Name;
                    else
                        record.AssetClass = string.Empty;

                    if (security.ASXSecurity != null)
                    {

                        var priceEntity = security.ASXSecurity.OrderByDescending(ss => ss.Date).FirstOrDefault();
                        if (priceEntity != null)
                            record.UnitPrice = (decimal?)priceEntity.UnitPrice;
                    }
                    records.Add(record);
                }

            }


            return records;
        }
        private int? GetInvestmentTypeNumber(string type)
        {
            int? result = null;

            Dictionary<string, int> InvestmentTypes = new Dictionary<string, int>{
                                              {"Bank Bill",18} ,
                                              {"Bond",13},
                                              {"Cash",1},
                                              {"Debenture",17} ,
                                              {"Fixed Interest",15} ,
                                              {"Foreign Exchange",1},
                                              {"Index",7},
                                              {"Investment Property",9},
                                              {"Managed Fund /Unit Trust",2},
                                              {"Option",3},
                                              {"Other CGT Assessable",11} ,
                                              {"Other CGT Exempt",12} ,
                                              {"Residential Property",8},
                                              {"Rightslssue",4},
                                              {"Share",0},
                                              {"Term Depose",16} ,
                                              {"Term Deposit",16} ,
                                              {"Warrant",5}
                                          };


            if (InvestmentTypes.ContainsKey(type))
            {
                result = InvestmentTypes[type];
            }




            return result;
        }

        #region Create data table for Financial Simplicity Investment
            private DataTable CreateFinSimplicityInvestTable()
        {
            DataTable dt = new DataTable(TableName);
            dt.Columns.Add(InvestmentCode);
            dt.Columns.Add(MarketCode);
            dt.Columns.Add(CurrencyCode);
            dt.Columns.Add(InvestmentType);
            dt.Columns.Add(InvestmentName);
            dt.Columns.Add(UnitPrice);
            dt.Columns.Add(AssetClass);
            return dt;
        }
        #endregion
    }
}
