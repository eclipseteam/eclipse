﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Data;
using System.Data;
using Oritax.TaxSimp.CM.Organization.Commands;
using Oritax.TaxSimp.CM.Organization.Data;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.Commands;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{
    public class ImportSMAClientMembersData : ImportSMA
    {
        public override void ImportFromService(ImportProcessDS importProcessDS)
        {
            base.ImportFromService(importProcessDS);
            string memberIDToImport = importProcessDS.ByIDPara;
            ModelEntity modelEntity = Organization.Model.Where(model => model.ID == new Guid("a5f09a9a-f539-4f0d-9d2a-484622b795ab")).FirstOrDefault();
            SMAWebService smaWebService = new SMAWebService();
            Guid bankUnit = Guid.Empty;

            OrganisationListingDS organisationListingDS = new Data.OrganisationListingDS();
            organisationListingDS.OrganisationListingOperationType = OrganisationListingOperationType.EclipseSuper;
            Organization.GetData(organisationListingDS);

            if (importProcessDS.ImportByID)
                bankUnit = SetNewMember(importProcessDS, organisationListingDS, modelEntity, smaWebService, bankUnit, memberIDToImport);
            else
            {
                DataSet memberList = smaWebService.InvokeServiceMemberList(false);
                foreach (DataRow row in memberList.Tables["MemberSearchViewModel"].Rows)
                {
                    string memberCode = row["MemberCode"].ToString();
                    bankUnit = SetNewMember(importProcessDS, organisationListingDS, modelEntity, smaWebService, bankUnit, memberCode);
                }
            }

            importProcessDS.MessageNumber = "SMA001";
            importProcessDS.MessageSummary = "Uploaded Data";
            importProcessDS.MessageDetail = "Uploaded Data Successfully, please see detailed results below";
        }

        private Guid SetNewMember(ImportProcessDS importProcessDS, OrganisationListingDS organisationListingDS, ModelEntity modelEntity, SMAWebService smaWebService, Guid bankUnit, string memberCode)
        {
            smaWebService.ResultDataSet = new SMAImportProcessDS();
            smaWebService.InvokeServiceMemberDataTransactions(memberCode, importProcessDS.Tables[ResultTableName], DateTime.Now.AddYears(-5), DateTime.Now);
            if (smaWebService.ResultDataSet.Tables["MemberViewModel"] != null)
            {
                DataTable MemberViewModel = smaWebService.ResultDataSet.Tables["MemberViewModel"];
                SMASuperEntity smaSuperEntity = new SMASuperEntity();
                smaSuperEntity.ImportSMAServiceEntityRow(MemberViewModel.Rows[0]);
                this.Organization.Broker.SaveOverride = true;
                IOrganizationUnit orgUnit = CreateUpdateClientObjects(smaSuperEntity, smaWebService.ResultDataSet, organisationListingDS, modelEntity, importProcessDS, Organization, out bankUnit);
                if (orgUnit != null)
                {
                    this.Organization.Broker.SaveOverride = true;
                    SMAImportProcessDS sMAImportProcessDS = (SMAImportProcessDS)smaWebService.ResultDataSet;
                    sMAImportProcessDS.ClientID = orgUnit.EclipseClientID;
                    sMAImportProcessDS.MemberID = smaSuperEntity.MemberCode;
                    sMAImportProcessDS.SuperID = smaSuperEntity.ClientNumber;
                    sMAImportProcessDS.Securities = Organization.Securities;
                    sMAImportProcessDS.Unit = importProcessDS.Unit;
                    CreateUpdateTransactionsObjects(Guid.Empty, this.Organization.Broker, orgUnit.EclipseClientID, bankUnit, Guid.Empty, smaSuperEntity.EntityName, smaSuperEntity.MemberCode, smaSuperEntity.ClientNumber, sMAImportProcessDS, Organization, Organization.Securities);
                }
            }
            return bankUnit;
        }
    }
}
