﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{
    class ExportAdvisorDetailCommand: OrganizationWebCommandBase
    {       

        public override System.Data.DataSet DoAction(System.Data.DataSet value)
        {
            value.Tables.Clear();
            AdviserDetailsCSVColumns obj = new AdviserDetailsCSVColumns();
            obj.Table.Clear();
            value.Tables.Add(obj.Table);

            OrganizationListAction.ForEach("Administrator", OrganizationType.Adviser, Organization,
                (csid, logical, bmc) =>
                {
                    if (logical.CMType == OrganizationTypeList.GetEntity(OrganizationType.Adviser))
                    {
                        (bmc as OrganizationUnitCM).GetData(value);
                    }
                });

            return value;
        }
    }
}
