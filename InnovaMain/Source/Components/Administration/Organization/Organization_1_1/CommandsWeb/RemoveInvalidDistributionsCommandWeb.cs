﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Enum;
using System.Collections.ObjectModel;

namespace Oritax.TaxSimp.CM.Organization.Commands
{
    public class RemoveInvalidDistributionsCommandWeb : OrganizationCmCommandBase
    {
        public override string DoAction(string data)
        {
            OrganizationListAction.ForEachClientOnly("Administrator", Organization, (csid, logical, bmc) =>
                {

                    if ((bmc is IOrganizationUnit) && (bmc as IOrganizationUnit).IsInvestableClient)
                    {
                        var organizationUnitCm = bmc as OrganizationUnitCM;
                        organizationUnitCm.Broker.SetStart();

                        if (organizationUnitCm.IsCorporateType())
                        {
                            var entity = organizationUnitCm.ClientEntity as CorporateEntity;

                            if (entity != null)
                                RemoveInvalidDistributions(entity.DistributionIncomes);
                        }
                        else
                        {
                            var entity = organizationUnitCm.ClientEntity as ClientIndividualEntity;
                            if (entity != null)
                                RemoveInvalidDistributions(entity.DistributionIncomes);
                        }

                        bmc.CalculateToken(true);


                    }


                });



            return string.Empty;
        }

        private void RemoveInvalidDistributions(ObservableCollection<DistributionIncomeEntity> distributionIncomes)
        {
            if (distributionIncomes != null && distributionIncomes.Count > 0)
            {
                var unvalidDist = distributionIncomes.Where(ss => ss.NetCashDistribution == 0 && ss.NetGrossDistribution == 0&&ss.RecodDate_Shares==0);
                var count = unvalidDist.Count();
                for (int i = count - 1; i >= 0; i--)
                {
                    distributionIncomes.Remove(unvalidDist.ElementAt(i));
                }
            }


        }
    }
}
