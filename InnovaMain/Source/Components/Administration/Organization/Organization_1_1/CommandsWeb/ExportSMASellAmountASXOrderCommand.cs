﻿using System.Data;
using Oritax.TaxSimp.CM.Organization.CommandsWeb.Services;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.CM.Organization.Data;
using Oritax.TaxSimp.CalculationInterface;
using System;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Extensions;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{
    public class ExportSMASellAmountASXOrderCommand : OrganizationWebCommandBase
    {
        public override DataSet DoAction(DataSet value)
        {
          
           
            if (value is SMAOrderExportDS)
            {
                var ds = value as SMAOrderExportDS;
                if(ds.Data is OrderEntity)
                {
                    var order = ds.Data as OrderEntity;
                    string date = order.CreatedDate.ToString("dd/MM/yyyy");
                    int count = 0;
                    string csv = "CONTROL:DBC=S1:REQ=BA:BTC=ECLS75:DIV=5:FND=ECLS:USR=WEB:EFD=" + date + Environment.NewLine;
                    count++;
                    csv += "HDR:R2 FND MEM EFF IMG ABT SHT BPR" + Environment.NewLine;
                    count++;
                    csv += "//Sell amount ASX" + Environment.NewLine;
                    count++;

                    string smaClientID = ExportOrderToSMACommand.GetClientSuperID(order.ClientCID, Organization);
                    foreach (var orderItemEntity in order.Items)
                    {
                        if(orderItemEntity is ASXOrderItem)
                        {
                            count++;
                            var item = orderItemEntity as ASXOrderItem;

                            csv += string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",{4},{5},\"{6}\"{7}", "ECLS", smaClientID, date, item.InvestmentCode.PadLeft(9), item.SuggestedAmount.ToPositiveString(), "Y", item.UnitPrice.Normalize(), Environment.NewLine);
                           
                        }
                    }
                csv +="END-OF-FILE=" + (++count);
                ds.OutputData = csv;
                }

            }
            return value;
        }

       
    }
}
