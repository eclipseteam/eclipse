﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.CM.Organization.Data;
using System.Data;
using Oritax.TaxSimp.CalculationInterface;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{
    class ExportStateStreet : OrganizationWebCommandBase
    {

        public override System.Data.DataSet DoAction(System.Data.DataSet value)
        {
            value.Tables.Clear();
            AOIDCSVColumns obj = new AOIDCSVColumns();
            obj.Table.Clear();
            value.Tables.Add(obj.Table);
            this.Organization.Broker.SaveOverride = true; 

            OrganisationListingDS organisationListingDS = new OrganisationListingDS();
            IBrokerManagedComponent user = this.Organization.Broker.GetBMCInstance("Administrator", "DBUser_1_1");
            object[] args = new Object[3] { Organization.CLID, user.CID, EnableSecurity.SecuritySetting };
            DataView view = new DataView(Organization.Broker.GetBMCDetailList(OrganizationInstall.ASSEMBLY_NAME, args).Tables["Entities_Table"]);
            view.RowFilter = organisationListingDS.GetRowFilterAllClientsExceptEclipse();
            view.Sort = "ENTITYNAME_FIELD ASC";
            DataTable orgFiteredTable = view.ToTable();

            foreach (DataRow row in orgFiteredTable.Rows)
            {
                Guid cid = new Guid(row["ENTITYCIID_FIELD"].ToString());
                IOrganizationUnit organizationUnit = Organization.Broker.GetBMCInstance(cid) as IOrganizationUnit;
                if ((organizationUnit as OrganizationUnitCM).IsExportedSS == false && (organizationUnit as OrganizationUnitCM).IsInvestableClient)
                {
                    organizationUnit.GetData(value);
                    organizationUnit.IsExported = true;
                    organizationUnit.Modified = true;
                }
            }

            Organization.Broker.SetComplete();
            Organization.Broker.SetStart();
           
            return value;
        }
    }
}