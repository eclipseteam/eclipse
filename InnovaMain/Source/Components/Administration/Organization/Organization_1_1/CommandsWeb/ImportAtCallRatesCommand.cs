﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Globalization;
using Oritax.TaxSimp.Data;
using System.Linq;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{
    public class ImportAtCallRatesCommand : OrganizationWebCommandBase
    {
        public override DataSet DoAction(DataSet value)
        {
            if (value.Tables.Count == 0)
                return value;

            if (!value.Tables[0].Columns.Contains("Message"))
            {
                var dc = new DataColumn("Message", typeof(String)) { DefaultValue = "Client Account Not Found" };
                value.Tables[0].Columns.Add(dc);
            }
            if (!value.Tables[0].Columns.Contains("HasErrors"))
            {
                var dc = new DataColumn("HasErrors", typeof(bool)) { DefaultValue = true };
                value.Tables[0].Columns.Add(dc);
            }
            if (!value.Tables[0].Columns.Contains("IsMissingItem"))
            {
                var dc = new DataColumn("IsMissingItem", typeof(bool)) { DefaultValue = true };
                value.Tables[0].Columns.Add(dc);
            }

            #region Import Rates

            DataRow[] drs = value.Tables[0].Select();

            foreach (DataRow dr in drs)
            {
                dr["Message"] = "";
                dr["HasErrors"] = false;
                dr["IsMissingItem"] = false;

                ValidationRows(dr);
            }
            #endregion 

            return value;
        }

        private void ValidationRows(DataRow dr)
        {
            double tempDouble;
            int rateId;
            DateTime tempDate;
            DateTimeFormatInfo dtfInfo =
                new DateTimeFormatInfo { ShortDatePattern = "dd/MM/yyyy" };

            AtCallInstitutePriceEntity priceEntity = new AtCallInstitutePriceEntity
            {
                ID = Guid.Parse(dr["ID"].ToString()),
                InstituteID = Guid.Parse(dr["INSTITUTEID"].ToString()),
            };


            if (!int.TryParse(dr["RateID"].ToString(), out rateId))
            {
                dr["Message"] += "Invalid Rate ID,";
                dr["HasErrors"] = true;
            }
            else
            {
                priceEntity.ImportRateID = rateId;
            }

            if (dr["Min"] != DBNull.Value) 
            if (!double.TryParse(dr["Min"].ToString(), out tempDouble))
            {
                dr["Message"] += "Invalid Min Price,";
                dr["HasErrors"] = true;
            }
            else
            {
                priceEntity.Min = (Decimal?)tempDouble;
            }
            if (dr["Max"] != DBNull.Value) 
            if (!double.TryParse(dr["Max"].ToString(), out tempDouble))
            {
                dr["Message"] += "Invalid Max Price,";
                dr["HasErrors"] = true;
            }
            else
            {
                priceEntity.Max = (Decimal?)tempDouble;
            }
            if (dr["Rate"] != DBNull.Value)
            if (!double.TryParse(dr["Rate"].ToString(), out tempDouble))
            {
                priceEntity.Rate2 = dr["Rate"].ToString();
            }
            else
            {
                priceEntity.Rate2 = (tempDouble / 100).ToString("P");
            }
            if (dr["MaximumBrokerage"] != DBNull.Value) 
            if (!double.TryParse(dr["MaximumBrokerage"].ToString(), out tempDouble))
            {
                dr["Message"] += "Invalid Maximum Brokerage,";
                dr["HasErrors"] = true;
            }
            else
            {
                priceEntity.MaximumBrokerage = (Decimal?)tempDouble;
            }

            if (string.IsNullOrEmpty(dr["ApplicableFrom"].ToString()) ||
                !DateTime.TryParse(dr["ApplicableFrom"].ToString(), dtfInfo, DateTimeStyles.None, out tempDate))
            {
                dr["Message"] += "Invalid Applicable From Date, ";
                dr["HasErrors"] = true;
            }
            else
            {
                priceEntity.ApplicableFrom = tempDate;
            }

            if (string.IsNullOrEmpty(dr["Date"].ToString()) ||
                !DateTime.TryParse(dr["Date"].ToString(), dtfInfo, DateTimeStyles.None, out tempDate))
            {
                dr["Message"] += "Invalid Date, ";
                dr["HasErrors"] = true;
            }
            else
            {
                priceEntity.Date = tempDate;
            }

            if (string.IsNullOrEmpty(dr["Type"].ToString()))
            {
                dr["Message"] += "Invalid Type, ";
                dr["HasErrors"] = true;
            }
            else
            {
                priceEntity.Type = dr["Type"].ToString();
            }

            priceEntity.Applicability = dr["APPLICABILITY"].ToString();

            if (!bool.Parse(dr["HasErrors"].ToString()))
            {
                var inst =
                    Organization.Institution.
                    FirstOrDefault(item => item.ID == priceEntity.InstituteID);

                if (inst != null)
                {
                    var objExist =
                        inst.AtCallPrices.
                        SingleOrDefault(ss => ss.ImportRateID==rateId);

                    if (objExist != null)
                    {
                        dr["Message"]  += "Rate with same ID already exists, ";
                        dr["HasErrors"] = true;
                    }
                    else
                    {
                        if (inst.AtCallPrices == null)
                            inst.AtCallPrices = new ObservableCollection<AtCallInstitutePriceEntity>();
                        inst.AtCallPrices.Add(priceEntity);
                    }
                }
            }
        }
    }
}
