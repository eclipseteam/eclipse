﻿using System;
using System.Data;
using System.Globalization;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Enum;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{
    public class ValidateAtCallClientTransactionCommand : OrganizationWebCommandBase
    {
        public override System.Data.DataSet DoAction(System.Data.DataSet value)
        {
            #region cash transactions
            if (value.Tables.Contains("AtCallAmm"))
            {
                if (!value.Tables["AtCallAmm"].Columns.Contains("Message"))
                {
                    //set message to not found
                    DataColumn dc = new DataColumn("Message");
                    dc.DefaultValue = "Account Number not Found";
                    value.Tables["AtCallAmm"].Columns.Add(dc);
                }

                if (!value.Tables["AtCallAmm"].Columns.Contains("HasErrors"))
                {
                    //set has error to true
                    DataColumn dc = new DataColumn("HasErrors", typeof(bool));
                    dc.DefaultValue = true;
                    value.Tables["AtCallAmm"].Columns.Add(dc);
                }

                if (!value.Tables["AtCallAmm"].Columns.Contains("IsMissingItem"))
                {
                    //set Is Missing Item to true
                    DataColumn dc = new DataColumn("IsMissingItem", typeof(bool));
                    dc.DefaultValue = true;
                    value.Tables["AtCallAmm"].Columns.Add(dc);
                }
            }

            #endregion



            OrganizationListAction.ForEach("Administrator", OrganizationType.BankAccount, Organization,
                                           (csid, logical, bmc) =>
                                           {
                                               if (logical.CMType == OrganizationTypeList.GetEntity(OrganizationType.BankAccount))
                                               {
                                                   string accountNumber = bmc.GetDataStream((int)CmCommand.AccountNumber, string.Empty);

                                                   string accounttype = bmc.GetDataStream((int)CmCommand.GetAccountType, string.Empty);
                                                   if (accounttype.ToLower() == "termdeposit")
                                                   {
                                                       var drs = value.Tables["AtCallAmm"].Select(string.Format("AccountNumber='{0}'", accountNumber));

                                                       foreach (var dr in drs)
                                                       {
                                                           dr["Message"] = "";
                                                           dr["HasErrors"] = false;
                                                           dr["IsMissingItem"] = false;
                                                           ValidateCashTransaction(dr);
                                                       }
                                                   }
                                               }
                                           });


            return value;

        }



        private void ValidateCashTransaction(DataRow dr)
        {
            DateTimeFormatInfo info = new DateTimeFormatInfo();
            info.ShortDatePattern = "dd/MM/yyyy";

            string amount = dr["TransactionAmount"].ToString();
            string dateOfTransaction = dr["TransactionDate"].ToString();
            string transcationRate = dr["TransactionRate"].ToString();
            string transcationType = dr["TransactionType"].ToString();
            string contractId = dr["ContractID"].ToString();
            string tdtran = dr["TDTRAN"].ToString();

            DateTime date;
            decimal utemp;


            if (string.IsNullOrEmpty(tdtran))
            {
                dr["Message"] += "Invalid TDTRAN,";
                dr["HasErrors"] = true;
            }
            if (!DateTime.TryParse(dateOfTransaction, info, DateTimeStyles.None, out date))
            {
                dr["Message"] += "Invalid Date Of Transaction,";
                dr["HasErrors"] = true;
            }

            if (!decimal.TryParse(amount, NumberStyles.Currency, CultureInfo.CurrentCulture, out utemp))
            {
                dr["Message"] += "Invalid Amount,";
                dr["HasErrors"] = true;
            }
            
            if (!decimal.TryParse(transcationRate, NumberStyles.Currency, CultureInfo.CurrentCulture, out utemp))
            {
                dr["Message"] += "Invalid Transaction Rate,";
                dr["HasErrors"] = true;
            }

            if (string.IsNullOrEmpty(transcationType))
            {
                dr["Message"] += "Invalid Transaction Type,";
                dr["HasErrors"] = true;
            }

            if (string.IsNullOrEmpty(contractId))
            {
                dr["Message"] += "Invalid Contract ID,";
                dr["HasErrors"] = true;
            }

            if (string.IsNullOrEmpty(tdtran))
            {
                dr["Message"] += "Invalid transaction,";
                dr["HasErrors"] = true;
            }

        }
    }

}
