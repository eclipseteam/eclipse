﻿using System;
using System.Data;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Enum;
using Oritax.TaxSimp.Extensions;
using System.Globalization;
using System.Collections.Generic;
namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{
    public class ImportAtCallTransactionsCommand : OrganizationWebCommandBase
    {
        public override DataSet DoAction(DataSet value)
        {
            if (value.Tables.Count == 0)
                return value;
            if (value.Tables["AtCallTransactions"].Columns.Contains("Message"))
            {
                value.Tables["AtCallTransactions"].Columns.Remove("Message");
            }
            if (!value.Tables["AtCallTransactions"].Columns.Contains("Message"))
            {
                //set message to not found
                DataColumn dc = new DataColumn("Message");
                dc.DefaultValue = "Customer number not found";
                value.Tables["AtCallTransactions"].Columns.Add(dc);
            }

            if (!value.Tables["AtCallTransactions"].Columns.Contains("HasErrors"))
            {
                //set has error to true
                DataColumn dc = new DataColumn("HasErrors", typeof(bool));
                dc.DefaultValue = true;
                value.Tables["AtCallTransactions"].Columns.Add(dc);
            }

            if (!value.Tables["AtCallTransactions"].Columns.Contains("IsMissingItem"))
            {
                //set Is Missing Item to true
                DataColumn dc = new DataColumn("IsMissingItem", typeof(bool));
                dc.DefaultValue = true;
                value.Tables["AtCallTransactions"].Columns.Add(dc);
            }
            if (!value.Tables["AtCallTransactions"].Columns.Contains("ProductID"))
            {
                //set Is Missing Item to true
                DataColumn dc = new DataColumn("ProductID", typeof(Guid));
                value.Tables["AtCallTransactions"].Columns.Add(dc);
            }
            if (!value.Tables["AtCallTransactions"].Columns.Contains("BrokerName"))
            {
                //set Is Missing Item to true
                DataColumn dc = new DataColumn("BrokerName", typeof(string));
                value.Tables["AtCallTransactions"].Columns.Add(dc);
            }        
            OrganizationListAction.ForEachClientOnly("Administrator", Organization,
                                           (csid, logical, bmc) =>
                                           {

                                               if ((bmc is OrganizationUnitCM) && !string.IsNullOrEmpty((bmc as OrganizationUnitCM).ClientId))
                                               {
                                                   var unit = (bmc as OrganizationUnitCM);                                                   
                                                   unit.SetData(value);
                                               }

                                           });

            if (value.Tables["AtCallTransactions"].Columns.Contains("ProductID"))
            {
                value.Tables["AtCallTransactions"].Columns.Remove("ProductID");
            }

            if (value.Tables["AtCallTransactions"].Columns.Contains("InstituteID"))
            {
                value.Tables["AtCallTransactions"].Columns.Remove("InstituteID");
            }
            if (value.Tables["AtCallTransactions"].Columns.Contains("TranDate"))
            {
                value.Tables["AtCallTransactions"].Columns.Remove("TranDate");
            }
            if (value.Tables["AtCallTransactions"].Columns.Contains("SysTranType"))
            {
                value.Tables["AtCallTransactions"].Columns.Remove("SysTranType");
            }
            if (value.Tables["AtCallTransactions"].Columns.Contains("ImportTranType"))
            {
                value.Tables["AtCallTransactions"].Columns.Remove("ImportTranType");
            }
            if (value.Tables["AtCallTransactions"].Columns.Contains("Broker"))
            {
                value.Tables["AtCallTransactions"].Columns.Remove("Broker");
            }
            if (value.Tables["AtCallTransactions"].Columns.Contains("BrokerName"))
            {
                value.Tables["AtCallTransactions"].Columns.Remove("BrokerName");
            }
            return value;
        }
     
    }
}
