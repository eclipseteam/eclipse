﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{
    public abstract class OrganizationWebCommandBase : IOrganizationCmWebCommand
    {
      public OrganizationCM Organization { get; set; }
      public abstract System.Data.DataSet DoAction(System.Data.DataSet value);
    }
}
