﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Xml.Linq;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.CM.Organization.Commands;
using Oritax.TaxSimp.DataSets;

namespace Oritax.TaxSimp.CM.Organization
{
    public interface IOrganizationCmCommand
    {
        OrganizationCM Organization { get; set; }
        string DoAction(string value);
    }


    public abstract class OrganizationCmCommandBase : IOrganizationCmCommand
    {
        public OrganizationCM Organization { get; set; }
        public abstract string DoAction(string value);

        public XElement GetClientMainInfo(IBrokerManagedComponent bmc, XDocument document)
        {
            var rootElement = GetClientEntity(document);
            XElement clientElement = null;
            if (rootElement != null)
            {
                clientElement = GetClientData(rootElement);
                if (clientElement != null)
                    clientElement.Add(new XElement("ClientID", (bmc as IOrganizationUnit).ClientId));
            }
            return clientElement;
        }

        public static XElement GetClientEntity(XDocument document)
        {
            var rootElement = document.Element("ClientIndividualEntity") ?? document.Element("CorporateEntity");
            return rootElement;
        }

        public XElement GetClientData(XElement root)
        {
            if (root == null) return null;
            var client = new XElement("Client");
            
            var adviser = root.Element("AdviserID");
            if (adviser != null) client.Add(adviser);
            
            var clientName = root.Element("Name");
            if (clientName != null) client.Add(clientName);
            
            var legalName = root.Element("LegalName");
            if (legalName != null) client.Add(legalName);

            

            var serviceType = root.Element("Servicetype");
            if (serviceType != null)
            {
                var _serviceType = new XElement("Servicetype");

                var doityourself = serviceType.Element("DO_IT_YOURSELF");
                doityourself.Name = "DIY";
                if (doityourself != null) _serviceType.Add(doityourself);

                var doitwithme = serviceType.Element("DO_IT_WITH_ME");
                doitwithme.Name = "DIWM";
                if (doitwithme != null) _serviceType.Add(doitwithme);

                var doitforme = serviceType.Element("DO_IT_FOR_ME");
                doitforme.Name = "DIFM";
                if (doitforme != null) _serviceType.Add(doitforme);

                if (doityourself != null && doityourself.Value == "true")
                    AddServiceType(_serviceType, serviceType.Element("DoItYourSelf"));

                if (doitwithme != null && doitwithme.Value == "true")
                    AddServiceType(_serviceType, serviceType.Element("DoItWithMe"));

                if (doitforme != null && doitforme.Value == "true")
                    AddServiceType(_serviceType, serviceType.Element("DoItForMe"));
                client.Add(_serviceType);
            }
            return client;

        }

        public void AddServiceType(XElement serviceType, XElement service)
        {
            if (serviceType == null || service == null) return;
            var model = service.Element("Model");
            if (model == null) return;
            var _model = new XElement("Model");

            var programCode = model.Element("ProgramCode");
            if (programCode != null) _model.Add(programCode);

            //var id = model.Element("ID");
            //if (id != null) _model.Add(id);
            var _service = new XElement(service.Name);
            _service.Add(_model);
            serviceType.Add(_service);
        }
    }

    public static class OrganizationCmCommandFactory
    {
        public static IOrganizationCmCommand GetCommand(this OrganizationCM organiztion, int type)
        {
            Type value = _Commands[type];
            if (value == null) return null;

            IOrganizationCmCommand command = Activator.CreateInstance(value) as IOrganizationCmCommand;
            command.Organization = organiztion;
            return command;
        }
       



        private static readonly Dictionary<int, Type> _Commands = new Dictionary<int, Type>()
		{
			{ (int)WellKnownRestCommand.AllClient,  typeof(AllClientCommand) },
			{ (int)WellKnownRestCommand.AllClientByType,  typeof(AllClientByTypeCommand) },
			{ (int)WellKnownRestCommand.ClientByName,  typeof(ClientByNameCommand) },
			{ (int)WellKnownRestCommand.AllClientsMainData,  typeof(AllClientsMainInfoCommand) },
			{ (int)WellKnownRestCommand.ClientFinancialDetails,  typeof(ClientFinancialsCommand) },
            { (int)WellKnownRestCommand.AllClientsByAdviserID,  typeof(AllClientByAdviserID) },
            { (int)WellKnownRestCommand.AllClientsByIFAID,  typeof(AllClientByIFA) },
            { (int)WellKnownRestCommand.SuperManagersEclipseSuperBasic,  typeof(EclipseSuperAllAccountsBasic) },
            { (int)WellKnownRestCommand.AllModels,  typeof(AllModels) },
            { (int)WellKnownRestCommand.ModelDetailsByID,  typeof(ModelDetailsByID) },
            { (int)WellKnownRestCommand.EPIDataReposneAllSecurities,  typeof(EPIDataAllSecuritiesCommand) },
            { (int)WellKnownRestCommand.EPIDataReposneAllClient,  typeof(EPIDataAllClientCommand) },
            { (int)WellKnownRestCommand.EPIASXDataReposneAllClient,  typeof(EPIASXDataAllClientCommand) },
			{ (int)WellKnownRestCommand.EPIDataReposneAllClientByType,  typeof(EPIDataRepsonseAllClientByTypeCommand) },
			{ (int)WellKnownRestCommand.EPIDataReposneClientByName,  typeof(EPIDataResponseClientByNameCommand) },
            
			{ (int)OrganizationCommandType.AddOrganization,  typeof(OrganizationAddCommand) },
			{ (int)OrganizationCommandType.UpdateOrganization,  typeof(OrganizationAddCommand) },
			{ (int)OrganizationCommandType.DeleteOrganization,  typeof(OrganizationDeleteCommand) },
			{ (int)OrganizationCommandType.DeleteMISAccount,  typeof(MISAccountDeleteCommand) },
			{ (int)OrganizationCommandType.CheckFundCodeAssociation,  typeof(CheckMISAccountFundCodeDeleteCommand) },
			{ (int)OrganizationCommandType.SetClientBankAccountsUsers,  typeof(SetClientBankAccountsUserCommand) },
			{ (int)OrganizationCommandType.GetDefaultFormsList,  typeof(OrganizationGetDefaultFormsListCommand) },
            { (int)OrganizationCommandType.GetEntityListDataByClidCsid,  typeof(OrganizationListCommandByClidCsid) },
			{ (int)OrganizationCommandType.GetAllOrganizationInstances,  typeof(OrganizationInstanceListCommand) },
			{ (int)OrganizationCommandType.GetAllOrganizationInstancesWithDetail,  typeof(OrganizationInstanceListCommandWithDetail) },
			{ (int)OrganizationCommandType.GetOrganzationsHasParentNull,  typeof(OrganzationsHasParentNullCommand)},
			{ (int)OrganizationCommandType.GetOrganizationHasClient,  typeof(OrganzationsHasClientCommand)},
			{ (int)OrganizationCommandType.GetOrganizationInstancesByCMType,  typeof(OrganizationInstancesByCMTypeCommand)},
			{ (int)OrganizationCommandType.GetAllOUCMTypeByName,  typeof(AllOUCMTypeByNameCommand)},
			{ (int)OrganizationCommandType.GetOrganzationsHasSMSFCorporateTrusteeNull,  typeof(OrganzationsHasSMSFCorporateTrusteeNullCommand)},
			{ (int)OrganizationCommandType.GetStatusList,  typeof(GetStatusListCommand)},
			{ (int)OrganizationCommandType.AddStatusList,  typeof(StatusListAddCommand)},
			{ (int)OrganizationCommandType.UpdateStatusList,  typeof(StatusListUpdateCommand)},
			{ (int)OrganizationCommandType.DeleteStatusList,  typeof(StatusListDeleteCommand)},
			{ (int)OrganizationCommandType.GetAllOrganizationInstancesByType,  typeof(OrganizationInstanceListCommandByType)},
			{ (int)OrganizationCommandType.GetAllOrganizationInstancesByTypeWithDetail,  typeof(OrganizationInstanceListCommandByTypeWithDetail)},
			{ (int)OrganizationCommandType.GetSmtpSettings,  typeof(GetSmtpSettingsCommand)},
			{ (int)OrganizationCommandType.UpdateSmtpSettings,  typeof(UpdateSmtpSettingsCommand)},
			{ (int)OrganizationCommandType.GetOrganizationChart,  typeof(GetOrganizationChartCommand) },
			{ (int)OrganizationCommandType.GetAssociation,  typeof(GetAssociationCommand) },
			{ (int)OrganizationCommandType.GetAllAssociation,  typeof(GetAllAssociationCommand) },
            { (int)OrganizationCommandType.GetAllClientsNoAdviser,  typeof(GetAllClientWithoutAdviser) },
			{ (int)OrganizationCommandType.GetProductList,  typeof(GetProductListCommand) },
			{ (int)OrganizationCommandType.AddProductsList,  typeof(ProductListAddCommand) },
			{ (int)OrganizationCommandType.UpdateProductsList,  typeof(ProductListUpdateCommand) },
			{ (int)OrganizationCommandType.DeleteProductsList,  typeof(ProductListDeleteCommand) },
			{ (int)OrganizationCommandType.GetAssetList,  typeof(GetAssetListCommand ) },
			{ (int)OrganizationCommandType.AddAssetsList,  typeof(AssetListAddCommand) },
			{ (int)OrganizationCommandType.UpdateAssetsList,  typeof(AssetListUpdateCommand) },
			{ (int)OrganizationCommandType.DeleteAssetsList,  typeof(AssetListDeleteCommand) },
			{ (int)OrganizationCommandType.GetSecuritiesList,  typeof(GetSecuritiesListCommand) },
			{ (int)OrganizationCommandType.AddSecuritiesList,  typeof(SecuritiesListAddCommand) },
            { (int)OrganizationCommandType.ProcessSecuritiesList,  typeof(SecuritiesListProcessCommand) },
             { (int)OrganizationCommandType.ProcessSecuritiesDividendstWeb,  typeof(ProcessSecurityDividendCommandWeb) },
               { (int)OrganizationCommandType.ProcessSecuritiesDistributionstWeb,  typeof(ProcessSecurityDistributionCommandWeb) },
			{ (int)OrganizationCommandType.UpdateSecuritiesList,  typeof(SecuritiesListUpdateCommand) },
			{ (int)OrganizationCommandType.DeleteSecuritiesList,  typeof(SecuritiesListDeleteCommand) },
			{ (int)OrganizationCommandType.GetManualAssetList,  typeof(GetManualAssetListCommand) },
			{ (int)OrganizationCommandType.AddManualAssetList,  typeof(ManualAssetListAddCommand) },
			{ (int)OrganizationCommandType.UpdateManualAssetList,  typeof(ManualAssetListUpdateCommand) },
			{ (int)OrganizationCommandType.DeleteManualAssetList,  typeof(ManualAssetListDeleteCommand) },
			{ (int)OrganizationCommandType.GetProductSecuritiesList,  typeof(GetProductSecuritiesListCommand) },
			{ (int)OrganizationCommandType.AddProductSecurities,  typeof(ProductSecuritiesListAddCommand) },
			{ (int)OrganizationCommandType.UpdateProductSecurities,  typeof(ProductSecuritiesListUpdateCommand) },
			{ (int)OrganizationCommandType.DeleteProductSecurities,  typeof(ProductSecuritiesListDeleteCommand) },
			{ (int)OrganizationCommandType.GetModelList,  typeof(GetModelListCommand ) },
			{ (int)OrganizationCommandType.AddModelList,  typeof(ModelListAddCommand) },
			{ (int)OrganizationCommandType.UpdateModelList,  typeof(ModelListUpdateCommand) },
			{ (int)OrganizationCommandType.DeleteModelList,  typeof(ModelListDeleteCommand) },
			{ (int)OrganizationCommandType.GetInstitutionList,  typeof(GetInstitutionListCommand ) },
			{ (int)OrganizationCommandType.AddInstitutionList,  typeof(InstitutionListAddCommand) },
			{ (int)OrganizationCommandType.UpdateInstitutionList,  typeof(InstitutionListUpdateCommand) },
			{ (int)OrganizationCommandType.DeleteInstitutionList,  typeof(InstitutionListDeleteCommand) },
			{ (int)OrganizationCommandType.GetAdminAccontRelation,  typeof(GetAdminAccontRelationCommand) },
			{ (int)OrganizationCommandType.GetAdminAccontListByType,  typeof(GetAdminAccontListByTypeCommand) },
			{ (int)OrganizationCommandType.FillModelTaskDescription,  typeof(FillModelTaskDescriptionCommand) },
			{ (int)OrganizationCommandType.FillTaskStatus,  typeof(FillTaskStatusCommand) },
			{ (int)OrganizationCommandType.GetBusinessGroupList,  typeof(GetBusinessGroupListCommand ) },
			{ (int)OrganizationCommandType.AddBusinessGroup,  typeof(BusinessGroupListAddCommand) },
			{ (int)OrganizationCommandType.UpdateBusinessGroup,  typeof(BusinessGroupListUpdateCommand) },
			{ (int)OrganizationCommandType.DeleteBusinessGroup,  typeof(BusinessGroupListDeleteCommand) },
			{ (int)OrganizationCommandType.GetAllOrganizationExportInstances,  typeof(OrganizationInstanceExportListCommand) },            
			{ (int)OrganizationCommandType.GetDefaultModelList,  typeof(GetDefaultModelListCommand) },
			{ (int)OrganizationCommandType.GetAllOrganizationExportInstancesSS,  typeof(OrganizationInstanceExportSSListCommand) },
			{ (int)OrganizationCommandType.GetAllOrganizationExportDBInstances,  typeof(OrganizationInstanceExportDBListCommand) },
            {(int)OrganizationCommandType.AttachBankAccountInClient,typeof(AttachBankAccountInClientCommand)},
			{ (int)OrganizationCommandCSVType.GetAllClients,  typeof(AllClientCSVCommand) },
			{ (int)MigrationCommand.Export,  typeof(ExportAllCommand) },
			{ (int)MigrationCommand.Import,  typeof(ImportAllCommand) },
			{ (int)MigrationCommand.ImportBankWestAccounts,  typeof(ImportBankWestAccountsCommand) },
			{ (int)MigrationCommand.ImportBankAccountTransactions,  typeof(ImportBankAccountTransactionCommand) },
			{ (int)MigrationCommand.ImportBellDirectDataFeed,  typeof(ImportBellDirectDataFeedCommand) },
			{ (int)MigrationCommand.ValidateBellDirectDataFeed,  typeof(ValidateBellDirectDataFeedCommand) },
			{ (int)MigrationCommand.ValidateAsxPriceList,  typeof(ValidateASXPriceListCommand) },
			{ (int)MigrationCommand.ImportASXPriceList,  typeof(ImportASXPriceListCommand) },
			{ (int)MigrationCommand.ValidateMISFundTransactions,  typeof(ValidateMISFundTransactionsCommand) },
			{ (int)MigrationCommand.ImportMISFundTransactions,  typeof(ImportMISFundTransactionsCommand) },
	 	    { (int)MigrationCommand.ValidateStatestreetPriceList,  typeof(ValidateStatestreetPriceListCommand) },
            { (int)MigrationCommand.ImportStatestreetPriceList,  typeof(ImportStatestreetPriceListCommand) },
            { (int)MigrationCommand.ValidateBankAccountTransactions,  typeof(ValidateBankAccountTransactionCommand) },
            { (int)MigrationCommand.ValidateBankWestAccounts,  typeof(ValidateBankWestAccountsCommand) },
            { (int)OrganizationCommandType.GetModelAssociation,  typeof(GetModelAssociationCommand) },
            { (int)MigrationCommand.ValidateProductSecuirtiesPriceList,  typeof(ValidateProductSecuritiestPriceListCommand) },
            { (int)MigrationCommand.ImportProductSecuirtiesPriceList,  typeof(ImportProductSecuirtiesPriceListCommand) },
            { (int)OrganizationCommandType.GetSelectedOrganizationInstancesByTypeWithDetail,  typeof(OrganizationInstanceCommandByTypeWithDetail)},
            //{ (int)MigrationCommand.Import,  typeof(ValidatetUnitHolderBalanceTransactionsCommand) },
            { (int)MigrationCommand.ValidateUnitHolderBalanceTransactions,  typeof(ValidateUnitHolderBalanceTransactionsCommand) },
            { (int)MigrationCommand.ImportUnitHolderTransactions,  typeof(ImportUnitHolderTransactionsCommand) },
            { (int)OrganizationCommandType.DeleteAllImportTransaction,  typeof(DeleteAllImportTransactionCommand) },

            { (int)MigrationCommand.ResetUnitHolderBalanceFunds,  typeof(ResetUnitHolderFundsCommand) },

             { (int)MigrationCommand.ValidateBankAccountPaidOrders,  typeof(ValidateBankAccountPaidOrdersCommand) },
            { (int)MigrationCommand.ImportBankAccountPaidOrders,  typeof(ImportBankAccountOrdersCommand) },
              { (int)MigrationCommand.ValidateMISPaidOrders,  typeof(ValidateMISPaidOrdersCommand) },
            { (int)MigrationCommand.ImportMISPaidOrders,  typeof(ImportMISPaidOrdersCommand) },
			 { (int)MigrationCommand.ValidateASXPaidOrders,  typeof(ValidateASXPaidOrdersCommand) },
            { (int)MigrationCommand.ImportASXPaidOrders,  typeof(ImportASXPaidOrdersCommand) },
            { (int)MigrationCommand.ValidateTDPriceList,  typeof(ValidateTDtPriceListCommand) },
            { (int)MigrationCommand.ImportTDPriceList,  typeof(ImportTDPriceListCommand) },
            { (int)MigrationCommand.ExportFinSimplicityHoldings,  typeof(ExportFinSimplicityHoldings) },
             { (int)MigrationCommand.ExportFinSimplicityInvestments,  typeof(ExportFinSimplicityInvestments) },
             { (int)OrganizationCommandType.ProcessAllSecurityDividendCommand,  typeof(ProcessAllSecurityDividendCommandWeb) },
             { (int)OrganizationCommandType.ProcessAllSecurityDistributionsCommand,  typeof(ProcessAllSecurityDistributionsCommandWeb) },
              { (int)OrganizationCommandType.RemoveAllInvalidDistributionsCommand,  typeof(RemoveInvalidDistributionsCommandWeb) },
		};
    }

   
}
