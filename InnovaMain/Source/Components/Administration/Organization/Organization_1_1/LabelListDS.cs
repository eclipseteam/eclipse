using System;
using System.Data;
using Oritax.TaxSimp.Calculation;

namespace Oritax.TaxSimp.CM.Organization
{
	/// <summary>
	/// Presentation dataset for Reporting Units
	/// </summary>
    public class LabelListDS : BusinessStructureWorkpaperDS
    {
     
#region Debugger Code 

        public string testlink = string.Empty;
    

#endregion 
        public LabelListDS()
            : base()
        {
        }

    }
}
