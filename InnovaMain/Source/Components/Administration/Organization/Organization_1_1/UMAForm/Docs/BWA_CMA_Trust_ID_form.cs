﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;

namespace Oritax.TaxSimp.CM.Organization.UMAForm
{
    public class BWA_CMA_Trust_ID_form : ITagFile
    {
        public CalculationInterface.ICMBroker Broker { get; set; }
        public Dictionary<string, string> GenerateTags(UMAFormEntity entity, ServiceTypes serviceType)
        {
            var dic = new Dictionary<string, string>()
                                                   { 
                                    
                            {"trust.fundname.line1",entity.accountName},
                            {"trust.businessname.line1",entity.accountName},
                            
                            {"trust.countryofestablishment",entity.RegCountry},
                            {"trust.charity.yes",""},
                            {"trust.regulatedscheme.yes",""},
                            {"trust.regulatedscheme.number",""},
                            {"trust.regulated.licence",entity.ABNCompany},
                            {"trust.govtsuper.yes",""},
                            {"trust.govtsuper.legislationname",""},
                            {"trust.othertype.yes",""},
                            {"trust.othertype.description",""},
                            {"trust.termben.yes",""},
                            {"trust.termben.no",""},
                            {"trust.beneficiaries.count",""},
                            {"trust.beneficiaries.1.givenname",""},
                            {"trust.beneficiaries.1.surname",""},
                            {"trust.beneficiaries.2.givenname",""},
                            {"trust.beneficiaries.2.surname",""},
                            {"trust.beneficiaries.3.givenname",""},
                            {"trust.beneficiaries.3.surname",""},
                            {"trust.beneficiaries.4.givenname",""},
                            {"trust.beneficiaries.4.surname",""},
                            {"trust.beneficiaries.5.givenname",""},
                            {"trust.beneficiaries.5.surname",""},
                            {"trust.beneficiaries.6.givenname",""},
                            {"trust.beneficiaries.6.surname",""},
                            {"NA.trust.trustee.count",""},
                            {"NA.trust.trustee.1.givenname",""},
                            {"NA.trust.trustee.1.surname",""},
                            {"NA.trust.trustee.1.streetaddress.line1",""},
                            {"NA.trust.trustee.1.streetaddress.suburb",""},
                            {"NA.trust.trustee.1.streetaddress.state",""},
                            {"NA.trust.trustee.1.streetaddress.postcode",""},
                            {"NA.trust.trustee.1.streetaddress.country",""},
                            {"NA.trust.trustee.2.givenname",""},
                            {"NA.trust.trustee.2.surname",""},
                            {"NA.trust.trustee.2.streetaddress.line1",""},
                            {"NA.trust.trustee.2.streetaddress.suburb",""},
                            {"NA.trust.trustee.2.streetaddress.state",""},
                            {"NA.trust.trustee.2.streetaddress.postcode",""},
                            {"NA.trust.trustee.2.streetaddress.country",""},
                            {"trust.trustee.3.givenname",""},
                            {"trust.trustee.3.surname",""},
                            {"trust.trustee.3.streetaddress.line1",""},
                            {"trust.trustee.3.streetaddress.suburb",""},
                            {"trust.trustee.3.streetaddress.state",""},
                            {"trust.trustee.3.streetaddress.postcode",""},
                            {"trust.trustee.3.streetaddress.country",""},
                            {"trust.trustee.4.givenname",""},
                            {"trust.trustee.4.surname",""},
                            {"trust.trustee.4.streetaddress.line1",""},
                            {"trust.trustee.4.streetaddress.suburb",""},
                            {"trust.trustee.4.streetaddress.state",""},
                            {"trust.trustee.4.streetaddress.postcode",""},
                            {"trust.trustee.4.streetaddress.country",""},
                            {"trust.trustee.5.givenname",""},
                            {"trust.trustee.5.surname",""},
                            {"trust.trustee.5.streetaddress.line1",""},
                            {"trust.trustee.5.streetaddress.suburb",""},
                            {"trust.trustee.5.streetaddress.state",""},
                            {"trust.trustee.5.streetaddress.postcode",""},
                            {"trust.trustee.5.streetaddress.country",""},
                            {"trust.trustee.6.givenname",""},
                            {"trust.trustee.6.surname",""},
                            {"trust.trustee.6.streetaddress.line1",""},
                            {"trust.trustee.6.streetaddress.suburb",""},
                            {"trust.trustee.6.streetaddress.state",""},
                            {"trust.trustee.6.streetaddress.postcode",""},
                            {"trust.trustee.6.streetaddress.country",""},
                            {"trust.trustee.1.givenname",""},
                            {"trust.trustee.1.surname",""},
                           
                            {"trust.trustee.1.streetaddress.line1",""},
                            {"trust.trustee.1.streetaddress.suburb",""},
                            {"trust.trustee.1.streetaddress.state",""},
                            {"trust.trustee.1.streetaddress.postcode",""},
                            {"trust.trustee.1.streetaddress.country",""},
                            {"business.acn/arbn",entity.accountNO},
                         
                            {"trust.trustee.1.streetaddress.line2",entity.PrincipalAddressLine2},
                            {"trust.trustee.1.middlename",entity.trustName},
                           
                            {"business.regulated.licence",""},
                            {"business.listed.yes",""},
                            {"business.listed.market/exchange",""},
                            {"business.majorityowned.yes",""},
                            {"business.majorityowned.listedname",""},
                            {"business.majorityowned.market/exchange",""},
                            {"business.type.public",""},
                            
                          
                        
                                        
                                                   
                                                   };




            #region IFA


            if (entity.IFACID != Guid.Empty)
            {
                OrganizationUnitCM IFA = Broker.GetBMCInstance(entity.IFACID) as OrganizationUnitCM;
                IFADS ds = new IFADS();
                IFA.GetData(ds);

                dic.Add("financial.planners.name", ds.ifaTable.Rows[0][ds.ifaTable.TRADINGNAME].ToString()+ "  ( " + ds.ifaTable.Rows[0][ds.ifaTable.SKCODE].ToString() + " )");
                
                
                
                dic.Add("financial.planner.phoneno", ds.ifaTable.Rows[0][ds.ifaTable.PHONENUMBERCOUNTRYCODE].ToString()+ds.ifaTable.Rows[0][ds.ifaTable.PHONENUMBERCITYCODE].ToString()+ds.ifaTable.Rows[0][ds.ifaTable.PHONENUBER].ToString());

                if (ds.ifaTable.Rows[0][ds.ifaTable.TRADINGNAME].ToString().Contains("Fortnum Private Wealth Pty Ltd"))
                    dic.Add("financial.planner.afs.no", "357306");
                else if (ds.ifaTable.Rows[0][ds.ifaTable.TRADINGNAME].ToString().Contains("RI"))
                    dic.Add("financial.planner.afs.no", "238429");
                else
                    dic.Add("financial.planner.afs.no", "357306");

                 var parent = (IFA as ICmHasParent);

                 if (parent != null && parent.Parent != IdentityCM.Null)
                 {
                     OrganizationUnitCM dealorGroup =
                         Broker.GetCMImplementation(parent.Parent.Clid, parent.Parent.Csid) as OrganizationUnitCM;

                     DealerGroupDS DGds = new DealerGroupDS();
                     dealorGroup.GetData(DGds);


                     dic.Add("financial.planner.afs.name", DGds.DealerGroupTable.Rows[0][DGds.DealerGroupTable.TRADINGNAME].ToString());
                     //dic.Add("financial.planner.afs.no", DGds.DealerGroupTable.Rows[0][DGds.DealerGroupTable.AFSLNO].ToString());
                     Broker.ReleaseBrokerManagedComponent(dealorGroup);

                 }


                Broker.ReleaseBrokerManagedComponent(IFA);
            }
           
            #endregion



            DateTimeFormatInfo info = new DateTimeFormatInfo();
            info.ShortDatePattern = "dd/MM/yyyy";

            dic.Add("issue.date.day", DateTime.Now.Day.ToString());
            dic.Add("issue.date.month", DateTime.Now.Month.ToString());
            dic.Add("issue.date.year", DateTime.Now.Year.ToString());
            dic.Add("id.advisor.dateverified.day", DateTime.Now.Day.ToString());
            dic.Add("id.advisor.dateverified.month", DateTime.Now.Month.ToString());
            dic.Add("id.advisor.dateverified.year", DateTime.Now.Year.ToString());
            
            dic.Add("trust.businessname.line.blank", "");

            //section 3 applicable if account type = trust - individual trustee
            if (entity.accountType == "Trust - Individual Trustee"
                || entity.accountType == "Self Managed Super Fund (SMSF) - Joint Trustee"
                || entity.accountType == "Self Managed Super Fund (SMSF) - Individual Trustee"
                || entity.accountType == "Trust - Joint Trustee"
                )
            {
                dic.Add("trust.individual.givenname", entity.SignatoryEntityList[0].GivenName + " " + entity.SignatoryEntityList[0].MiddleName);
                dic.Add("trust.individual.surname", entity.SignatoryEntityList[0].FamilyName);
                dic.Add("trust.individual.streetaddress.line1", entity.SignatoryEntityList[0].ResiAddressLine1 + " " + entity.SignatoryEntityList[0].ResiAddressLine2);
                dic.Add("trust.individual.streetaddress.suburb", entity.SignatoryEntityList[0].ResiSuburb);
                dic.Add("trust.individual.streetaddress.state", State.GetStateCode(entity.SignatoryEntityList[0].ResiState));
                dic.Add("trust.individual.streetaddress.postcode", entity.SignatoryEntityList[0].ResiPostCode);
                dic.Add("trust.individual.streetaddress.country", entity.SignatoryEntityList[0].ResiCountry);
                DateTime date;
                if (DateTime.TryParse(entity.SignatoryEntityList[0].DateOfBirth, new DateTimeFormatInfo() { ShortDatePattern = "dd/MM/yyyy" }, DateTimeStyles.None, out date))
                {
                    dic.Add("trust.individual.dob.day", date.Day > 9 ? date.Day.ToString() : "0" + date.Day);
                    dic.Add("trust.individual.dob.month", date.Month > 9 ? date.Month.ToString() : "0" + date.Month);
                    dic.Add("trust.individual.dob.year", date.Year.ToString());
                }
                if (entity.Sig2Required)
                {
                    dic.Add("trust.individual1.givenname",entity.SignatoryEntityList[1].GivenName + " " + entity.SignatoryEntityList[1].MiddleName);
                    dic.Add("trust.individual1.surname", entity.SignatoryEntityList[1].FamilyName);
                    dic.Add("trust.individual1.streetaddress.line1",entity.SignatoryEntityList[1].ResiAddressLine1 + " " +entity.SignatoryEntityList[1].ResiAddressLine2);
                    dic.Add("trust.individual1.streetaddress.suburb", entity.SignatoryEntityList[1].ResiSuburb);
                    dic.Add("trust.individual1.streetaddress.state",State.GetStateCode(entity.SignatoryEntityList[1].ResiState));
                    dic.Add("trust.individual1.streetaddress.postcode", entity.SignatoryEntityList[1].ResiPostCode);
                    dic.Add("trust.individual1.streetaddress.country", entity.SignatoryEntityList[1].ResiCountry);
                  
                    if (DateTime.TryParse(entity.SignatoryEntityList[1].DateOfBirth,new DateTimeFormatInfo() {ShortDatePattern = "dd/MM/yyyy"},DateTimeStyles.None, out date))
                    {
                        dic.Add("trust.individual1.dob.day", date.Day > 9 ? date.Day.ToString() : "0" + date.Day);
                        dic.Add("trust.individual1.dob.month", date.Month > 9 ? date.Month.ToString() : "0" + date.Month);
                        dic.Add("trust.individual1.dob.year", date.Year.ToString());
                    }
                }

                if (entity.Sig3Required)
                {
                    dic.Add("trust.individual2.givenname", entity.SignatoryEntityList[2].GivenName + " " + entity.SignatoryEntityList[2].MiddleName);
                    dic.Add("trust.individual2.surname", entity.SignatoryEntityList[2].FamilyName);
                    dic.Add("trust.individual2.streetaddress.line1", entity.SignatoryEntityList[2].ResiAddressLine1 + " " + entity.SignatoryEntityList[2].ResiAddressLine2);
                    dic.Add("trust.individual2.streetaddress.suburb", entity.SignatoryEntityList[2].ResiSuburb);
                    dic.Add("trust.individual2.streetaddress.state", State.GetStateCode(entity.SignatoryEntityList[2].ResiState));
                    dic.Add("trust.individual2.streetaddress.postcode", entity.SignatoryEntityList[2].ResiPostCode);
                    dic.Add("trust.individual2.streetaddress.country", entity.SignatoryEntityList[2].ResiCountry);

                    if (DateTime.TryParse(entity.SignatoryEntityList[2].DateOfBirth, new DateTimeFormatInfo() { ShortDatePattern = "dd/MM/yyyy" }, DateTimeStyles.None, out date))
                    {
                        dic.Add("trust.individual2.dob.day", date.Day > 9 ? date.Day.ToString() : "0" + date.Day);
                        dic.Add("trust.individual2.dob.month", date.Month > 9 ? date.Month.ToString() : "0" + date.Month);
                        dic.Add("trust.individual2.dob.year", date.Year.ToString());
                    }
                }
                if (entity.Sig4Required)
                {
                    dic.Add("trust.individual3.givenname", entity.SignatoryEntityList[3].GivenName + " " + entity.SignatoryEntityList[3].MiddleName);
                    dic.Add("trust.individual3.surname", entity.SignatoryEntityList[3].FamilyName);
                    dic.Add("trust.individual3.streetaddress.line1", entity.SignatoryEntityList[3].ResiAddressLine1 + " " + entity.SignatoryEntityList[3].ResiAddressLine2);
                    dic.Add("trust.individual3.streetaddress.suburb", entity.SignatoryEntityList[3].ResiSuburb);
                    dic.Add("trust.individual3.streetaddress.state", State.GetStateCode(entity.SignatoryEntityList[3].ResiState));
                    dic.Add("trust.individual3.streetaddress.postcode", entity.SignatoryEntityList[3].ResiPostCode);
                    dic.Add("trust.individual3.streetaddress.country", entity.SignatoryEntityList[3].ResiCountry);

                    if (DateTime.TryParse(entity.SignatoryEntityList[3].DateOfBirth, new DateTimeFormatInfo() { ShortDatePattern = "dd/MM/yyyy" }, DateTimeStyles.None, out date))
                    {
                        dic.Add("trust.individual3.dob.day", date.Day > 9 ? date.Day.ToString() : "0" + date.Day);
                        dic.Add("trust.individual3.dob.month", date.Month > 9 ? date.Month.ToString() : "0" + date.Month);
                        dic.Add("trust.individual3.dob.year", date.Year.ToString());
                    }
                }

            }
            //section 4.1 applicable if account type= trust - corporate trustee
            if (entity.accountType == "Trust - Corporate Trustee" || entity.accountType == "Self Managed Super Fund (SMSF) - Corporate Trustee")
            {
                int count = 1;
                if (entity.Sig2Required) count++;
                if (entity.Sig3Required) count++;
                if (entity.Sig4Required) count++;

                dic.Add("business.directors.count", count.ToString());
                dic.Add("business.principalactivity", entity.accountType);
                dic.Add("business.entityname.line1", entity.trustName);
                dic.Add("Corporate1.ACN", entity.ACN);
                dic.Add("business.charity.yes", "0");
                dic.Add("business.correspondence.streetaddress.line1", entity.RegAddressLine1);
                dic.Add("business.correspondence.streetaddress.line2", entity.RegAddressLine2);
                dic.Add("business.correspondence.streetaddress.suburb", entity.RegSuburb);
                dic.Add("business.correspondence.streetaddress.state", State.GetStateCode(entity.RegState));
                dic.Add("business.correspondence.streetaddress.postcode", entity.RegPostCode);
                dic.Add("business.correspondence.streetaddress.country", entity.RegCountry);
                dic.Add("business.correspondence.mailingaddress.line1", entity.PrincipalAddressLine1);
                dic.Add("business.correspondence.mailingaddress.line2", entity.PrincipalAddressLine2);
                dic.Add("business.correspondence.mailingaddress.suburb", entity.PrincipalSuburb);
                dic.Add("business.correspondence.mailingaddress.state", State.GetStateCode(entity.PrincipalState));
                dic.Add("business.correspondence.mailingaddress.postcode", entity.PrincipalPostCode);
                dic.Add("business.correspondence.mailingaddress.country", entity.PrincipalCountry);
                dic.Add("business.regulated.yes", "1");
                dic.Add("business.regulated.regulatorname", "ASIC");
                dic.Add("business.type.proprietary", "1");


                dic.Add("business.directors.1.givenname", entity.SignatoryEntityList[0].GivenName + " " + entity.SignatoryEntityList[0].MiddleName);
                dic.Add("business.directors.1.surname", entity.SignatoryEntityList[0].FamilyName);
                dic.Add("business.directors.1.streetaddress.line1", entity.SignatoryEntityList[0].ResiAddressLine1 + " " + entity.SignatoryEntityList[0].ResiAddressLine2);
                dic.Add("business.directors.1.streetaddress.suburb", entity.SignatoryEntityList[0].ResiSuburb);
                dic.Add("business.directors.1.streetaddress.state", State.GetStateCode(entity.SignatoryEntityList[0].ResiState));
                dic.Add("business.directors.1.streetaddress.postcode", entity.SignatoryEntityList[0].ResiPostCode);
                dic.Add("business.directors.1.streetaddress.country", entity.SignatoryEntityList[0].ResiCountry);
                if (entity.Sig2Required)
                {
                    dic.Add("business.directors.2.givenname", entity.SignatoryEntityList[1].GivenName + " " + entity.SignatoryEntityList[1].MiddleName);
                    dic.Add("business.directors.2.surname", entity.SignatoryEntityList[1].FamilyName);
                    dic.Add("business.directors.2.streetaddress.line1", entity.SignatoryEntityList[1].ResiAddressLine1 + " " + entity.SignatoryEntityList[1].ResiAddressLine2);
                    dic.Add("business.directors.2.streetaddress.suburb", entity.SignatoryEntityList[1].ResiSuburb);
                    dic.Add("business.directors.2.streetaddress.state", State.GetStateCode(entity.SignatoryEntityList[1].ResiState));
                    dic.Add("business.directors.2.streetaddress.postcode", entity.SignatoryEntityList[1].ResiPostCode);
                    dic.Add("business.directors.2.streetaddress.country", entity.SignatoryEntityList[1].ResiCountry);
                }

                if (entity.Sig3Required)
                {
                    dic.Add("business.directors.3.givenname", entity.SignatoryEntityList[2].GivenName + " " + entity.SignatoryEntityList[2].MiddleName);
                    dic.Add("business.directors.3.surname", entity.SignatoryEntityList[2].FamilyName);
                    dic.Add("business.directors.3.streetaddress.line1", entity.SignatoryEntityList[2].ResiAddressLine1 + " " + entity.SignatoryEntityList[2].ResiAddressLine2);
                    dic.Add("business.directors.3.streetaddress.suburb", entity.SignatoryEntityList[2].ResiSuburb);
                    dic.Add("business.directors.3.streetaddress.state", State.GetStateCode(entity.SignatoryEntityList[2].ResiState));
                    dic.Add("business.directors.3.streetaddress.postcode", entity.SignatoryEntityList[2].ResiPostCode);
                    dic.Add("business.directors.3.streetaddress.country", entity.SignatoryEntityList[2].ResiCountry);
                }
                if (entity.Sig4Required)
                {
                    dic.Add("business.directors.4.givenname", entity.SignatoryEntityList[3].GivenName + " " + entity.SignatoryEntityList[3].MiddleName);
                    dic.Add("business.directors.4.surname", entity.SignatoryEntityList[3].FamilyName);
                    dic.Add("business.directors.4.streetaddress.line1", entity.SignatoryEntityList[3].ResiAddressLine1 + " " + entity.SignatoryEntityList[3].ResiAddressLine2);
                    dic.Add("business.directors.4.streetaddress.suburb", entity.SignatoryEntityList[3].ResiSuburb);
                    dic.Add("business.directors.4.streetaddress.state", State.GetStateCode(entity.SignatoryEntityList[3].ResiState));
                    dic.Add("business.directors.4.streetaddress.postcode", entity.SignatoryEntityList[3].ResiPostCode);
                    dic.Add("business.directors.4.streetaddress.country", entity.SignatoryEntityList[3].ResiCountry);
                }
            }
            else
            {
                dic.Add("business.type.proprietary", "0");
                dic.Add("business.principalactivity", "");
            }


           
          

            return dic;
        }
    }
}
