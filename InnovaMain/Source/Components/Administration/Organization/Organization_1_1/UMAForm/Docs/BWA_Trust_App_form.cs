﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.CM.Organization.UMAForm
{
    public class BWA_Trust_App_form : ITagFile
    {
        public CalculationInterface.ICMBroker Broker { get; set; }
        public Dictionary<string, string> GenerateTags(UMAFormEntity entity, ServiceTypes serviceType)
        {
            string bsb = string.Empty;
            string accountNo = string.Empty;

            if (entity.ClientCID != null && entity.ClientCID != Guid.Empty)
            {
                IOrganizationUnit orgUnit = Broker.GetBMCInstance(entity.ClientCID) as IOrganizationUnit;
                Common.BankAccountEntity bankAccountEntity = orgUnit.GetBankAccountDetails(bsb, accountNo, serviceType);
                if (bankAccountEntity != null)
                {
                    bsb = bankAccountEntity.BSB;
                    accountNo = bankAccountEntity.AccountNumber;
                }
            }

            var dic = new Dictionary<string, string>()
            {
               //todo:fields are not in our system its purpose of investment
                {"InvestmentDetails",entity.accountType},
                //2:Trust Detail
                {"trust.fundname.line1",entity.accountName},
              
                //todo:fields are not in our system
                {"trust.super",""},

                //4:Coresponding Details
               
                {"trust.correspondence.streetaddress.line1",entity.SignatoryEntityList[0].MailAddressLine1},
                {"trust.correspondence.streetaddress.line2",entity.SignatoryEntityList[0].MailAddressLine2},
                {"trust.correspondence.streetaddress.suburb",entity.SignatoryEntityList[0].MailSuburb},
                {"trust.correspondence.streetaddress.state",State.GetStateCode( entity.SignatoryEntityList[0].MailState )},
                {"trust.correspondence.streetaddress.postcode", entity.SignatoryEntityList[0].MailPostCode},
                {"trust.correspondence.streetaddress.country",((entity.MialingCountry=="Australia")?"":entity.MialingCountry)},
                {"trust.correspondence.mailingaddress.line2",entity.MialingAddressLine2},
                {"trust.correspondence.mailingaddress.suburb",string.IsNullOrEmpty(entity.MialingSuburb)?"Neutral Bay":entity.MialingSuburb},
               
              
               
                {"trust.correspondence.dupstatement.address.line1",entity.DupMialingAddressLine1},
                {"trust.correspondence.dupstatement.address.line2",entity.DupMialingAddressLine2},
                {"trust.correspondence.dupstatement.suburb",entity.DupMialingSuburb},
                {"trust.correspondence.dupstatement.state",State.GetStateCode(entity.DupMialingState)},
                {"trust.correspondence.dupstatement.postcode",entity.DupMialingPostCode},
                {"trust.correspondence.dupstatement.country",((entity.DupMialingCountry=="Australia")?" ":entity.DupMialingCountry)},
                
               
                //6:Maner of operation
                {"trust.mannerofoperation.anyone",(entity.DIYMannerOfOperation=="Any one of us to sign"||entity.DIWMMannerOfOperation=="Any one of us to sign")?"Yes":"0"},
                {"trust.mannerofoperation.anytwo",(entity.DIYMannerOfOperation=="Any two of us to sign"||entity.DIWMMannerOfOperation=="Any two of us to sign")?"Yes":"0"},
                {"trust.mannerofoperation.all",(entity.DIYMannerOfOperation=="All of us to sign"||entity.DIWMMannerOfOperation=="All of us to sign")?"Yes":"0"},
             
                 //7:Tax File Number/Non Resident Declaration/Tax Identification Number
                {"trust.acn/abn",entity.ABNCompany},
                {"trust.trustee.acn",entity.ACN},
                {"trust.tfn/abn",(entity.TFNTrustFund==""?entity.ABNCompany:entity.TFNTrustFund)},
                {"trust.tin",""},
                {"trust.nonres.country",entity.NonRedidentCountry},
                 //todo:fields are not in our system
                {"trust.tfnexempt.1","0"},
                {"trust.tfnexempt.2","0"},
                {"trust.tfnexempt.3","0"},
              
                //8:document check list
                //todo:document checks are not provided in checklist please add
                 {"trust.documentchecklist","0"},

                 //9:advisor Firm Access
                 //todo:advisoraccess are not provided 
                {"trust.advisoraccess.fee",""},
                {"trust.advisoraccess.general",""},
                {"trust.advisoraccess.no",""},

                //todo:Approved Distributor Use only are not provided 
                //{"trust.approveddistributor.groupname",""},
                //{"trust.approveddistributor.dealername",""},
               
                //{"trust.approveddistributor.clientaccount",""},
                //{"trust.cmtbsb",""},
                //{"trust.cmtaccountnumber",""},
                //{"trust.platformname",""},
                //{"trust.platformreference",""},
                //{"txtreceiptnumber",""},
                //{"receiptnumber",""},
                
                {"bsb",bsb},
                {"account",accountNo},

                {"trust.correspondence.contact.contactname",entity.trustName},
                {"trust.regulated.licence",entity.ABNCompany},
                
                {"trust.correspondence.mailingaddress.line1", EclipseMailingAddress.AddresLine},
                {"trust.correspondence.mailingaddress.subrub",EclipseMailingAddress.Subrub},
                {"trust.correspondence.mailingaddress.state", EclipseMailingAddress.State},
                {"trust.correspondence.mailingaddress.country",EclipseMailingAddress.Country},
                {"trust.correspondence.mailingaddress.postcode", EclipseMailingAddress.PCode}

            };




         if (entity.accountType == "Trust - Corporate Trustee" || entity.accountType == "Self Managed Super Fund (SMSF) - Corporate Trustee")
         {

             dic.Add("trust.trustee.entityname.line1", entity.trustName);
         }
         else
         {
             dic.Add("trust.trustee.entityname.line1", string.Empty);
         }


            #region IFA


            if (entity.IFACID != Guid.Empty)
            {
                OrganizationUnitCM IFA = Broker.GetBMCInstance(entity.IFACID) as OrganizationUnitCM;
                IFADS ds = new IFADS();
                IFA.GetData(ds);

                dic.Add("trust.approveddistributor.advisorname", ds.ifaTable.Rows[0][ds.ifaTable.TRADINGNAME].ToString() + "  ( " + ds.ifaTable.Rows[0][ds.ifaTable.SKCODE].ToString() + " )");
                dic.Add("trust.approveddistributor.dealername", ds.ifaTable.Rows[0][ds.ifaTable.TRADINGNAME].ToString());
              
                dic.Add("trust.approveddistributor.advisorcode", ds.ifaTable.Rows[0][ds.ifaTable.SKCODE].ToString());
                Broker.ReleaseBrokerManagedComponent(IFA);
            }

            #endregion

            #region Access Facilities

            if (entity.DoItForhMe)
            {
                dic.Add("trust.investorplus.telephoneaccess", (entity.DIYPhoneAccess || entity.DIWMPhoneAccess) ? "Yes" : "0");
                dic.Add("trust.investorplus.onlineaccess", (entity.DIYOnlineAccess || entity.DIWMOnlineAccess) ? "Yes" : "0");
                dic.Add("trust.investorplus.debitcard", (entity.DIYDebitCard || entity.DIWMDebitCard) ? "Yes" : "0");
                dic.Add("trust.investorplus.chequebook", (entity.DIYChequeBook || entity.DIWMChequeBook) ? "Yes" : "0");
                dic.Add("trust.investorplus.depositbook", (entity.DIYDepositBook || entity.DIWMDepositBook) ? "Yes" : "0");
            }
            else
            {
                dic.Add("trust.investorplus.telephoneaccess",  "0");
                dic.Add("trust.investorplus.onlineaccess", "0");
                dic.Add("trust.investorplus.debitcard", "0");
                dic.Add("trust.investorplus.chequebook", "0");
                dic.Add("trust.investorplus.depositbook",  "0");
            }
            #endregion 
            #region Investment Detail

             if (entity.DoItYourSelf)
            {
               
                dic.Add("Text1", ("SMSF CASH ACCOUNT"));
               
            }
             else if (entity.DoItWithMe)
                 dic.Add("Text1", ("DIWM CASH ACCOUNT"));
             else if (entity.DoItForhMe)
                dic.Add("Text1", ("DIFM CASH ACCOUNT"));
            else
                dic.Add("Text1", ("CASH ACCOUNT"));
            #endregion 
            #region Signatories
            //3:Trustee and CMA Account Signatory Details
            if (entity.SignatoryEntityList != null && entity.SignatoryEntityList.Count > 0)
            {

                dic.Add("trust.trustee.accountsig1.title.mr", entity.SignatoryEntityList[0].Title.ToLower() == "mr" ? "Yes" : "0");
                dic.Add("trust.trustee.accountsig1.title.mrs", entity.SignatoryEntityList[0].Title.ToLower() == "mrs" ? "Yes" : "0");
                dic.Add("trust.trustee.accountsig1.title.miss", entity.SignatoryEntityList[0].Title.ToLower() == "miss" ? "Yes" : "0");
                dic.Add("trust.trustee.accountsig1.title.ms", entity.SignatoryEntityList[0].Title.ToLower() == "ms" ? "Yes" : "0");
                dic.Add("trust.trustee.accountsig1.other", (entity.SignatoryEntityList[0].Title.ToLower() != "mr" && entity.SignatoryEntityList[0].Title.ToLower() != "mrs" && entity.SignatoryEntityList[0].Title.ToLower() != "miss" && entity.SignatoryEntityList[0].Title.ToLower() != "ms") ? entity.SignatoryEntityList[0].Title : "");
                dic.Add("trust.trustee.accountsig1.givenname", entity.SignatoryEntityList[0].GivenName);
                dic.Add("trust.trustee.accountsig1.middlename", entity.SignatoryEntityList[0].MiddleName);
                dic.Add("trust.trustee.accountsig1.surname", entity.SignatoryEntityList[0].FamilyName);
                dic.Add("trust.accountsig1.address.line1", entity.SignatoryEntityList[0].ResiAddressLine1);
                dic.Add("trust.accountsig1.address.line2", entity.SignatoryEntityList[0].ResiAddressLine2);
                dic.Add("trust.accountsig1.suburb", entity.SignatoryEntityList[0].ResiSuburb);
                dic.Add("trust.accountsig1.state", State.GetStateCode( entity.SignatoryEntityList[0].ResiState));
                dic.Add("trust.accountsig1.postcode", entity.SignatoryEntityList[0].ResiPostCode);
                dic.Add("trust.accountsig1.country", (entity.SignatoryEntityList[0].ResiCountry == "Australia"?"":entity.SignatoryEntityList[0].ResiCountry));
                DateTime date;
                if (DateTime.TryParse(entity.SignatoryEntityList[0].DateOfBirth, new DateTimeFormatInfo() { ShortDatePattern = "dd/MM/yyyy" }, DateTimeStyles.None, out date))
                {
                    dic.Add("trust.trustee.accountsig1.dob.day", date.Day.ToString());
                    dic.Add("trust.trustee.accountsig1.dob.month", date.Month.ToString());
                    dic.Add("trust.trustee.accountsig1.dob.year", date.Year.ToString());
                }
                //todo:fields are not in our system also set for second signatory
                dic.Add("trust.trustee.accountsig1.password", "");
                dic.Add("trust.accountsig1.occupation", entity.SignatoryEntityList[0].Occupation);
                dic.Add("trust.accountsig1.res.country", entity.SignatoryEntityList[0].MainCountryOFAddressSig);
                dic.Add("trust.accountsig1.employer", entity.SignatoryEntityList[0].Employer);
                //todo:fields are not in our system also set for second signatory
                dic.Add("Signatory1.Fullname", entity.SignatoryEntityList[0].GivenName + " " + entity.SignatoryEntityList[0].MiddleName +" " +entity.SignatoryEntityList[0].FamilyName);
                dic.Add("trust.correspondence.contact.areacode", "");
                dic.Add("trust.accountsig1.phone.prefix", "");
                if(entity.SignatoryEntityList[0].ContactPH != string.Empty)
                    dic.Add("trust.accountsig1.phone.number", entity.SignatoryEntityList[0].ContactPH);
                else
                    dic.Add("trust.accountsig1.phone.number", entity.SignatoryEntityList[0].AlternativePH);

                dic.Add("trust.trustee.accountsig1.email", entity.SignatoryEntityList[0].Email);
                if (entity.Sig2Required)
                {
                    dic.Add("trust.trustee.accountsig2.title.mr", entity.SignatoryEntityList[1].Title.ToLower() == "mr" ? "Yes" : "0");
                    dic.Add("trust.trustee.accountsig2.title.mrs", entity.SignatoryEntityList[1].Title.ToLower() == "mrs" ? "Yes" : "0");
                    dic.Add("trust.trustee.accountsig2.title.miss", entity.SignatoryEntityList[1].Title.ToLower() == "miss" ? "Yes" : "0");
                    dic.Add("trust.trustee.accountsig2.title.ms", entity.SignatoryEntityList[1].Title.ToLower() == "ms" ? "Yes" : "0");
                    dic.Add("trust.trustee.accountsig2.other", (entity.SignatoryEntityList[1].Title.ToLower() != "mr" && entity.SignatoryEntityList[1].Title.ToLower() != "mrs" && entity.SignatoryEntityList[1].Title.ToLower() != "miss" && entity.SignatoryEntityList[1].Title.ToLower() != "ms") ? entity.SignatoryEntityList[1].Title : "");
                    dic.Add("trust.trustee.accountsig2.givenname", entity.SignatoryEntityList[1].GivenName);
                    dic.Add("trust.trustee.accountsig2.middlename", entity.SignatoryEntityList[1].MiddleName);
                    dic.Add("trust.trustee.accountsig2.surname", entity.SignatoryEntityList[1].FamilyName);
                    dic.Add("trust.accountsig2.address.line1", entity.SignatoryEntityList[1].ResiAddressLine1);
                    dic.Add("trust.accountsig2.address.line2", entity.SignatoryEntityList[1].ResiAddressLine2);
                    dic.Add("trust.accountsig2.suburb", entity.SignatoryEntityList[1].ResiSuburb);
                    dic.Add("trust.accountsig2.state",State.GetStateCode( entity.SignatoryEntityList[1].ResiState));
                    dic.Add("trust.accountsig2.postcode", entity.SignatoryEntityList[1].ResiPostCode);
                    dic.Add("trust.accountsig2.country", (entity.SignatoryEntityList[1].ResiCountry == "Australia"?"":entity.SignatoryEntityList[1].ResiCountry));
                    if (DateTime.TryParse(entity.SignatoryEntityList[1].DateOfBirth, new DateTimeFormatInfo() { ShortDatePattern = "dd/MM/yyyy" }, DateTimeStyles.None, out date))
                    {
                        dic.Add("trust.trustee.accountsig2.dob.day", date.Day.ToString());
                        dic.Add("trust.trustee.accountsig2.dob.month", date.Month.ToString());
                        dic.Add("trust.trustee.accountsig2.dob.year", date.Year.ToString());
                    }
                    dic.Add("trust.trustee.accountsig2.password", "");
                    dic.Add("trust.accountsig2.occupation", entity.SignatoryEntityList[1].Occupation);
                    dic.Add("trust.accountsig2.res.country", entity.SignatoryEntityList[1].MainCountryOFAddressSig);
                    dic.Add("trust.accountsig2.employer", entity.SignatoryEntityList[1].Employer);
                    dic.Add("trust.accountsig2.phone.prefix", "");
                    //We have 3 Objects, Home, Work and Mobile. Check Home then Work then Mobile
                    if (entity.SignatoryEntityList[0].ContactPH != string.Empty)
                        dic.Add("trust.accountsig2.phone.number", entity.SignatoryEntityList[1].ContactPH);
                    else
                        dic.Add("trust.accountsig2.phone.number", entity.SignatoryEntityList[1].AlternativePH);

                    dic.Add("trust.trustee.accountsig2.email", entity.SignatoryEntityList[1].Email);
                }

            }
            #endregion



            return dic;
        }
    }
}
