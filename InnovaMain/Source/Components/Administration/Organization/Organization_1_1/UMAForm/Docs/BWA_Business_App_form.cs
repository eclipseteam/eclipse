﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.CM.Organization.UMAForm
{


    public class BWA_Business_App_form : ITagFile
    {
        public CalculationInterface.ICMBroker Broker { get; set; }
        public Dictionary<string, string> GenerateTags(UMAFormEntity entity, ServiceTypes serviceType)
        {
            string bsb = string.Empty;
            string accountNo = string.Empty;

            if (entity.ClientCID != null && entity.ClientCID != Guid.Empty)
            {
                IOrganizationUnit orgUnit = Broker.GetBMCInstance(entity.ClientCID) as IOrganizationUnit;
                Common.BankAccountEntity bankAccountEntity = orgUnit.GetBankAccountDetails(bsb, accountNo, serviceType);
                if (bankAccountEntity != null)
                {
                    bsb = bankAccountEntity.BSB;
                    accountNo = bankAccountEntity.AccountNumber;
                }
            }

            // todo: please read whole to todo list.its has some important data to be implemented."APPROVED DISTRIBUTOR USE ONLY" section is not implemented.
            var dic = new Dictionary<string, string>()
            {
                //todo:fields are not in our system its purpose of investment
                {"Text1","Cash Account"},
                {"business.entityname.line1",entity.accountName},
                {"business.acn/arbn",entity.ACN},
                //todo:fields are not in our system
                {"business.abn",entity.ABNCompany},
                {"business.password",""},
                {"business.countryofestablishment",entity.ACN},
                {"business.acccount.designation",""},//entity.accountType

                {"business.correspondence.streetaddress.line1",entity.PrincipalAddressLine1 +" "+entity.PrincipalAddressLine2},
                {"business.correspondence.streetaddress.suburb",entity.PrincipalSuburb},
                {"business.correspondence.streetaddress.state",State.GetStateCode( entity.PrincipalState)},
                {"business.correspondence.streetaddress.postcode",entity.PrincipalPostCode},
                {"business.correspondence.streetaddress.country",entity.PrincipalCountry},
               
                {"business.correspondence.mailingasstreet",EclipseMailingAddress.AddresLine},
                {"business.correspondence.mailingaddress.line1",EclipseMailingAddress.AddresLine},
                {"business.correspondence.mailingaddress.suburb",EclipseMailingAddress.Subrub},
                {"business.correspondence.mailingaddress.state",EclipseMailingAddress.State},
                {"business.correspondence.mailingaddress.postcode",EclipseMailingAddress.PCode},
                {"business.correspondence.mailingaddress.country",EclipseMailingAddress.Country},
               
                 {"bsb",bsb},
                {"account",accountNo},

                {"business.correspondence.dupstatement.address.line1",entity.DupMialingAddressLine1 + " " +entity.DupMialingAddressLine1},
                {"business.correspondence.dupstatement.suburb",entity.DupMialingSuburb}, 
                {"business.correspondence.dupstatement.state",State.GetStateCode( entity.DupMialingState)},
                {"business.correspondence.dupstatement.postcode",entity.DupMialingPostCode},
                {"business.correspondence.dupstatement.country",entity.DupMialingCountry},

                {"business.correspondence.contact.contactname",entity.SignatoryEntityList[0].FullName},//--
                {"business.correspondence.contact.areacode.contactnumber","61"},
                {"business.correspondence.contact.contactnumber",entity.SignatoryEntityList[0].ContactPH},
                {"business.correspondence.contact.contactemail",entity.SignatoryEntityList[0].Email},


                {"business.investorplus.telephoneaccess",(entity.DIYPhoneAccess||entity.DIWMPhoneAccess)?"Yes":"0"},
                {"business.investorplus.onlineaccess",(entity.DIYOnlineAccess||entity.DIWMOnlineAccess)?"Yes":"0"},
                {"business.investorplus.debitcard",(entity.DIYDebitCard||entity.DIWMDebitCard)?"Yes":"0"},
                {"business.investorplus.chequebook",(entity.DIYChequeBook||entity.DIWMChequeBook)?"Yes":"0"},
                {"business.investorplus.depositbook",(entity.DIYDepositBook||entity.DIWMDepositBook)?"Yes":"0"},
                //todo:fields in the pdf are same needs to be different
                {"trust.mannerofoperations.oneofus",(entity.DIYMannerOfOperation=="Any one of us to sign"||entity.DIWMMannerOfOperation=="Any one of us to sign")?"Yes":"0"},
                {"trust.mannerofoperations.twoofus",(entity.DIYMannerOfOperation=="Any two of us to sign"||entity.DIWMMannerOfOperation=="Any two of us to sign")?"Yes":"0"},
                {"trust.mannerofoperations.allofus",(entity.DIYMannerOfOperation=="All of us to sign"||entity.DIWMMannerOfOperation=="All of us to sign")?"Yes":"0"},
                
              
                //todo:document checks are not provided in checklist please add
               {"business.documentchecklist","0"},
               //todo: advisor firm access is not defined in form and in pdf field name are not correct for Advisor access,Equiry access and no access please correct
                {"business.adviserfirmaccess","0"},
                //{"business.adviserfirmaccess","0"},
                //{"business.adviserfirmaccess","0"},
                  

                 {"business.tfn/abn",(!string.IsNullOrEmpty(entity.TFNTrustFund))?entity.TFNTrustFund:entity.ABNCompany},
                {"business.tin",""},
                {"business.nonres.country",entity.NonRedidentCountry},
                 //todo:fields are not in our system
                {"business.tfnexempt.1","0"},
                {"business.tfnexempt.2","0"},
                {"business.tfnexempt.3","0"},

                
                 //todo:Approved Distributor Use only are not provided 
                //{"business.approveddistributor.groupname",""},
                //{"business.approveddistributor.dealername",""},
                
               
                //{"business.approveddistributor.clientaccount",""},
                //{"business.cmtbsb",""},
                //{"business.cmtaccountnumber",""},
                //{"business.platformname",""},
                //{"business.platformreference",""},
                //{"txtreceiptnumber",""},
                //{"receiptnumber",""},
            };

            #region IFA


            if (entity.IFACID != Guid.Empty)
            {
                OrganizationUnitCM IFA = Broker.GetBMCInstance(entity.IFACID) as OrganizationUnitCM;
                IFADS ds = new IFADS();
                IFA.GetData(ds);

                dic.Add("business.approveddistributor.advisorname", ds.ifaTable.Rows[0][ds.ifaTable.TRADINGNAME].ToString());
                dic.Add("business.approveddistributor.advisorcode", ds.ifaTable.Rows[0][ds.ifaTable.SKCODE].ToString());
                Broker.ReleaseBrokerManagedComponent(IFA);
            }

            #endregion


            #region Signatories
            if (entity.SignatoryEntityList != null && entity.SignatoryEntityList.Count > 0)
            {

                dic.Add("business.accountsig1.title.mr", entity.SignatoryEntityList[0].Title.ToLower() == "mr" ? "Mr" : "0");
                dic.Add("business.accountsig1.title.mrs", entity.SignatoryEntityList[0].Title.ToLower() == "mrs" ? "Mrs" : "0");
                dic.Add("business.accountsig1.title.miss", entity.SignatoryEntityList[0].Title.ToLower() == "miss" ? "Miss" : "0");
                dic.Add("business.accountsig1.title.ms", entity.SignatoryEntityList[0].Title.ToLower() == "ms" ? "Ms" : "0");
                dic.Add("business.accountsig1.other", (entity.SignatoryEntityList[0].Title.ToLower() != "mr" && entity.SignatoryEntityList[0].Title.ToLower() != "mrs" && entity.SignatoryEntityList[0].Title.ToLower() != "miss" && entity.SignatoryEntityList[0].Title.ToLower() != "ms") ? entity.SignatoryEntityList[0].Title : "");
                dic.Add("business.accountsig1.givenname", entity.SignatoryEntityList[0].GivenName);
                dic.Add("business.accountsig1.middlename", entity.SignatoryEntityList[0].MiddleName);
                dic.Add("business.accountsig1.surname", entity.SignatoryEntityList[0].FamilyName);
                dic.Add("business.accountsig1.address.line1", entity.SignatoryEntityList[0].ResiAddressLine1);
                dic.Add("business.accountsig1.address.line2", entity.SignatoryEntityList[0].ResiAddressLine2);
                dic.Add("business.accountsig1.suburb", entity.SignatoryEntityList[0].ResiSuburb);
                dic.Add("business.accountsig1.state", State.GetStateCode(entity.SignatoryEntityList[0].ResiState));
                dic.Add("business.accountsig1.postcode", entity.SignatoryEntityList[0].ResiPostCode);
                dic.Add("business.accountsig1.country", entity.SignatoryEntityList[0].ResiCountry);
                DateTime date;
                if (DateTime.TryParse(entity.SignatoryEntityList[0].DateOfBirth, new DateTimeFormatInfo() { ShortDatePattern = "dd/MM/yyyy" }, DateTimeStyles.None, out date))
                {
                    dic.Add("business.accountsig1.dob.day", date.Day.ToString());
                    dic.Add("business.accountsig1.dob.month", date.Month.ToString());
                    dic.Add("business.accountsig1.dob.year", date.Year.ToString());
                }
                //todo:fields are not in our system also set for second signatory
                dic.Add("business.accountsig1.password", "");
                dic.Add("business.accountsig1.occupation", entity.SignatoryEntityList[0].Occupation);
                dic.Add("business.accountsig1.res.country", entity.SignatoryEntityList[0].MainCountryOFAddressSig);
                dic.Add("business.accountsig1.employer", entity.SignatoryEntityList[0].Employer);
                //todo:fields are not in our system also set for second signatory
                dic.Add("business.accountsig1.phone.prefix", "");
                dic.Add("business.accountsig1.phone.number", entity.SignatoryEntityList[0].ContactPH);
                dic.Add("business.accountsig1.email", entity.SignatoryEntityList[0].Email);


                if (entity.Sig2Required)
                {
                    dic.Add("business.accountsig2.title.mr", entity.SignatoryEntityList[1].Title.ToLower() == "mr" ? "Mr" : "0");
                    dic.Add("business.accountsig2.title.mrs", entity.SignatoryEntityList[1].Title.ToLower() == "mrs" ? "Mrs" : "0");
                    dic.Add("business.accountsig2.title.miss", entity.SignatoryEntityList[1].Title.ToLower() == "miss" ? "Miss" : "0");
                    dic.Add("business.accountsig2.title.ms", entity.SignatoryEntityList[1].Title.ToLower() == "ms" ? "Ms" : "0");
                    dic.Add("business.accountsig2.other", (entity.SignatoryEntityList[1].Title.ToLower() != "mr" && entity.SignatoryEntityList[1].Title.ToLower() != "mrs" && entity.SignatoryEntityList[1].Title.ToLower() != "miss" && entity.SignatoryEntityList[1].Title.ToLower() != "ms") ? entity.SignatoryEntityList[1].Title : "");
                    dic.Add("business.accountsig2.givenname", entity.SignatoryEntityList[1].GivenName);
                    dic.Add("business.accountsig2.middlename", entity.SignatoryEntityList[1].MiddleName);
                    dic.Add("business.accountsig2.surname", entity.SignatoryEntityList[1].FamilyName);
                    dic.Add("business.accountsig2.address.line1", entity.SignatoryEntityList[1].ResiAddressLine1);
                    dic.Add("business.accountsig2.address.line2", entity.SignatoryEntityList[1].ResiAddressLine2);
                    dic.Add("business.accountsig2.suburb", entity.SignatoryEntityList[1].ResiSuburb);
                    dic.Add("business.accountsig2.state", State.GetStateCode(entity.SignatoryEntityList[1].ResiState));
                    dic.Add("business.accountsig2.postcode", entity.SignatoryEntityList[1].ResiPostCode);
                    dic.Add("business.accountsig2.country", entity.SignatoryEntityList[1].ResiCountry);
                    DateTime date1;
                    if (DateTime.TryParse(entity.SignatoryEntityList[1].DateOfBirth, new DateTimeFormatInfo() { ShortDatePattern = "dd/MM/yyyy" }, DateTimeStyles.None, out date1))
                    {
                        dic.Add("business.accountsig2.dob.day", date1.Day.ToString());
                        dic.Add("business.accountsig2.dob.month", date1.Month.ToString());
                        dic.Add("business.accountsig2.dob.year", date1.Year.ToString());
                    }
                    //todo:fields are not in our system also set for second signatory
                    dic.Add("business.accountsig2.password", "");
                    dic.Add("business.accountsig2.occupation", entity.SignatoryEntityList[1].Occupation);
                    dic.Add("business.accountsig2.res.country", entity.SignatoryEntityList[1].MainCountryOFAddressSig);
                    dic.Add("business.accountsig2.employer", entity.SignatoryEntityList[1].Employer);
                    //todo:fields are not in our system also set for second signatory
                    dic.Add("business.accountsig2.phone.prefix", "");
                    dic.Add("business.accountsig2.phone.number", entity.SignatoryEntityList[1].ContactPH);
                    dic.Add("business.accountsig2.email", entity.SignatoryEntityList[1].Email);

                }

                dic.Add("business.addapplicants", "");

            }
            #endregion

            return dic;
        }
    }
}
