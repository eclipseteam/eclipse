﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.CM.Organization.UMAForm
{
    public class BWA_CMA_Company_ID_form : ITagFile
    {
        public CalculationInterface.ICMBroker Broker { get; set; }
        public Dictionary<string, string> GenerateTags(UMAFormEntity entity, ServiceTypes serviceType)
        {
            int count = 1;
            if (entity.Sig2Required) count++;
            if (entity.Sig3Required) count++;
            if (entity.Sig4Required) count++;

            var dic= new Dictionary<string, string>()
            {
                        {"business.entityname.line1",entity.accountName},
                        {"business.acn/arbn",entity.accountNO},
                        {"business.charity.yes",""},
                        {"business.charity.no",""},
                        {"business.principalactivity",""},
                        {"business.correspondence.streetaddress.line1",entity.RegAddressLine1},
                        {"business.correspondence.streetaddress.suburb",entity.RegSuburb},
                        {"business.correspondence.streetaddress.state",State.GetStateCode( entity.RegState)},
                        {"business.correspondence.streetaddress.postcode",entity.RegPostCode},
                        {"business.correspondence.streetaddress.country",entity.RegCountry},
                        {"business.regulated.yes",""},
                        {"business.regulated.regulatorname",""},
                        {"business.regulated.licence",""},
                        {"business.listed.yes",""},
                        {"business.listed.market/exchange",""},
                        {"business.majorityowned.yes",""},
                        {"business.majorityowned.listedname",""},
                        {"business.majorityowned.market/exchange",""},
                        {"business.type.public","Yes"},
                        {"business.type.proprietary",""},
                        {"business.directors.count",count.ToString()},
                        {"business.directors.1.givenname",entity.SignatoryEntityList[0].GivenName},
                        {"business.directors.1.surname",entity.SignatoryEntityList[0].MiddleName},
                        {"business.shareholders.1.givenname",entity.SignatoryEntityList[0].GivenName},
                        {"business.shareholders.1.surname",entity.SignatoryEntityList[0].MiddleName},
                        {"business.shareholders.1.streetaddress.line1",entity.SignatoryEntityList[0].ResiAddressLine1},
                        {"business.shareholders.1.streetaddress.suburb",entity.SignatoryEntityList[0].ResiSuburb},
                        {"business.shareholders.1.streetaddress.state",State.GetStateCode( entity.SignatoryEntityList[0].ResiState)},
                        {"business.shareholders.1.streetaddress.postcode",entity.SignatoryEntityList[0].ResiPostCode},
                        {"business.shareholders.1.streetaddress.country",entity.SignatoryEntityList[0].ResiCountry},

            };
            if(entity.Sig2Required)
            {
                dic.Add("business.directors.2.givenname", entity.SignatoryEntityList[1].GivenName);
                dic.Add("business.directors.2.surname", entity.SignatoryEntityList[1].MiddleName);
                dic.Add("business.shareholders.2.streetaddress.line1",entity.SignatoryEntityList[1].ResiAddressLine1);
                dic.Add("business.shareholders.2.streetaddress.suburb",entity.SignatoryEntityList[1].ResiSuburb);
                dic.Add("business.shareholders.2.streetaddress.state",State.GetStateCode( entity.SignatoryEntityList[1].ResiState));
                dic.Add("business.shareholders.2.streetaddress.postcode",entity.SignatoryEntityList[1].ResiPostCode);
                dic.Add("business.shareholders.2.streetaddress.country",entity.SignatoryEntityList[1].ResiCountry);
                dic.Add("business.shareholders.2.givenname",entity.SignatoryEntityList[1].GivenName);
                dic.Add("business.shareholders.2.surname",entity.SignatoryEntityList[1].MiddleName);
            }
             if(entity.Sig3Required)
            {
                dic.Add("business.directors.3.givenname",entity.SignatoryEntityList[2].GivenName);
                dic.Add("business.directors.3.surname",entity.SignatoryEntityList[2].MiddleName);
                dic.Add("business.shareholders.3.givenname",entity.SignatoryEntityList[2].GivenName);
                dic.Add("business.shareholders.3.surname",entity.SignatoryEntityList[2].MiddleName);
                dic.Add("business.shareholders.3.streetaddress.line1",entity.SignatoryEntityList[2].ResiAddressLine1);
                dic.Add("business.shareholders.3.streetaddress.suburb",entity.SignatoryEntityList[2].ResiSuburb);
                dic.Add("business.shareholders.3.streetaddress.state",State.GetStateCode(entity.SignatoryEntityList[2].ResiState));
                dic.Add("business.shareholders.3.streetaddress.postcode", entity.SignatoryEntityList[2].ResiPostCode);
                dic.Add("business.shareholders.3.streetaddress.country", entity.SignatoryEntityList[2].ResiCountry);
            }
            if(entity.Sig4Required)
            {
                dic.Add("business.directors.4.givenname",entity.SignatoryEntityList[3].GivenName);
                dic.Add("business.directors.4.surname", entity.SignatoryEntityList[3].GivenName);
            }
            return dic;
        }
    }
}
