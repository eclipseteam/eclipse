﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.CM.Organization.UMAForm
{
    public class BWA_Business_Additional_Sigs_App_form : ITagFile
    {
        public CalculationInterface.ICMBroker Broker { get; set; }
        public Dictionary<string, string> GenerateTags(UMAFormEntity entity, ServiceTypes serviceType)
        {
            // todo: please read whole to todo list.its has some important data to be implemented."APPROVED DISTRIBUTOR USE ONLY" section is not implemented.
            var dic = new Dictionary<string, string>()
            {
                //todo:fields are not in our system its purpose of investment
                {"Text1",""},
                {"business.entityname.line1",entity.accountName},
                {"business.acn/arbn",entity.ACN},
                {"business.abn",entity.ABNCompany},
                //todo:fields are not in our system
                {"business.password",""},
                {"business.countryofestablishment",entity.RegCountry},
                {"business.acccount.designation",entity.accountType},

                {"business.correspondence.streetaddress.line1",entity.RegAddressLine1},
                {"business.correspondence.streetaddress.suburb",entity.RegSuburb},
                {"business.correspondence.streetaddress.state",State.GetStateCode( entity.RegState)},
                {"business.correspondence.streetaddress.postcode",entity.RegPostCode},
                {"business.correspondence.streetaddress.country",entity.RegCountry},
                
                {"business.correspondence.mailingaddress.line1",entity.MialingAddressLine1},
                {"business.correspondence.mailingaddress.suburb",entity.MialingSuburb},
                {"business.correspondence.mailingaddress.state",State.GetStateCode(entity.MialingState)},
                {"business.correspondence.mailingaddress.postcode",entity.MialingPostCode},
                {"business.correspondence.mailingaddress.country",entity.MialingCountry},
               
                {"business.correspondence.dupstatement.address.line1",entity.DupMialingAddressLine1},
                {"business.correspondence.dupstatement.suburb",entity.DupMialingSuburb},
                {"business.correspondence.dupstatement.state",State.GetStateCode(entity.DupMialingState)},
                {"business.correspondence.dupstatement.postcode",entity.DupMialingPostCode},
                {"business.correspondence.dupstatement.country",entity.DupMialingCountry},
                      //todo:fields are not in our system
                {"business.correspondence.contact.contactname",""},
                {"business.correspondence.primarycontact.contactnumber.prefix",""},
                {"business.correspondence.primarycontact.contactnumber.number",""},
                {"business.correspondence.contact.contactemail",""},
                
                {"business.investorplus.telephoneaccess",(entity.DIYPhoneAccess||entity.DIWMPhoneAccess)?"Yes":"0"},
                {"business.investorplus.onlineaccess",(entity.DIYOnlineAccess||entity.DIWMOnlineAccess)?"Yes":"0"},
                {"business.investorplus.debitcard",(entity.DIYDebitCard||entity.DIWMDebitCard)?"Yes":"0"},
                {"business.investorplus.chequebook",(entity.DIYChequeBook||entity.DIWMChequeBook)?"Yes":"0"},
                {"business.investorplus.depositbook",(entity.DIYDepositBook||entity.DIWMDepositBook)?"Yes":"0"},
                //todo:fields in the pdf are same needs to be different
                {"business.mannerofoperations.oneofus",(entity.DIYMannerOfOperation=="Any one of us to sign"||entity.DIWMMannerOfOperation=="Any one of us to sign")?"Yes":"0"},
                {"business.mannerofoperations.twoofus",(entity.DIYMannerOfOperation=="Any two of us to sign"||entity.DIWMMannerOfOperation=="Any two of us to sign")?"Yes":"0"},
                {"business.mannerofoperations.allofus",(entity.DIYMannerOfOperation=="All of us to sign"||entity.DIWMMannerOfOperation=="All of us to sign")?"Yes":"0"},
                
                {"business.tfn/abn",(!string.IsNullOrEmpty(entity.TFNTrustFund))?entity.TFNTrustFund:entity.ABNCompany},
                {"business.tin",""},
                {"business.nonres.country",entity.NonRedidentCountry},
                 //todo:fields are not in our system
                {"business.tfnexempt.1","0"},
                {"business.tfnexempt.2","0"},
                {"business.tfnexempt.3","0"},
                //todo:document checks are not provided in checklist please add
               {"business.documentchecklist","0"},
               //todo: advisor firm access is not defined in form and in pdf field name are not correct for Advisor access,Equiry access and no access please correct
                {"business.adviserfirmaccess","0"},
                {"business.equiryfirmaccess","0"},
                {"business.noaccess","0"},
            };
            #region Signatories
            if (entity.SignatoryEntityList != null && entity.SignatoryEntityList.Count > 0)
            {

                dic.Add("business.accountsig1.title.mr", entity.SignatoryEntityList[0].Title.ToLower() == "mr" ? "Yes" : "0");
                dic.Add("business.accountsig1.title.mrs", entity.SignatoryEntityList[0].Title.ToLower() == "mrs" ? "Yes" : "0");
                dic.Add("business.accountsig1.title.miss", entity.SignatoryEntityList[0].Title.ToLower() == "miss" ? "Yes" : "0");
                dic.Add("business.accountsig1.title.ms", entity.SignatoryEntityList[0].Title.ToLower() == "ms" ? "Yes" : "0");
                dic.Add("business.accountsig1.other", (entity.SignatoryEntityList[0].Title.ToLower() != "mr" && entity.SignatoryEntityList[0].Title.ToLower() != "mrs" && entity.SignatoryEntityList[0].Title.ToLower() != "miss" && entity.SignatoryEntityList[0].Title.ToLower() != "ms") ? entity.SignatoryEntityList[0].Title : "");
                dic.Add("business.accountsig1.givenname", entity.SignatoryEntityList[0].GivenName);
                dic.Add("business.accountsig1.surname", entity.SignatoryEntityList[0].FamilyName);
                dic.Add("business.accountsig1.address.line1", entity.SignatoryEntityList[0].ResiAddressLine1);
                dic.Add("business.accountsig1.address.line2", entity.SignatoryEntityList[0].ResiAddressLine2);
                dic.Add("business.accountsig1.suburb", entity.SignatoryEntityList[0].ResiSuburb);
                dic.Add("business.accountsig1.state",State.GetStateCode( entity.SignatoryEntityList[0].ResiState));
                dic.Add("business.accountsig1.postcode", entity.SignatoryEntityList[0].ResiPostCode);
                dic.Add("business.accountsig1.country", entity.SignatoryEntityList[0].ResiCountry);
                DateTime date;
                if (DateTime.TryParse(entity.SignatoryEntityList[0].DateOfBirth, new DateTimeFormatInfo() { ShortDatePattern = "dd/MM/yyyy" }, DateTimeStyles.None, out date))
                {
                    dic.Add("business.accountsig1.dob.day", date.Day.ToString());
                    dic.Add("business.accountsig1.dob.month", date.Month.ToString());
                    dic.Add("business.accountsig1.dob.year", date.Year.ToString());
                }
                //todo:fields are not in our system also set for second signatory
                dic.Add("business.accountsig1.password", "");
                dic.Add("business.accountsig1.occupation", entity.SignatoryEntityList[0].Occupation);
                dic.Add("business.accountsig1.res.country", entity.SignatoryEntityList[0].MainCountryOFAddressSig);
                dic.Add("business.accountsig1.employer", entity.SignatoryEntityList[0].Employer);
                //todo:fields are not in our system also set for second signatory
                dic.Add("business.accountsig1.phone.prefix", "");
                dic.Add("business.accountsig1.phone.number", entity.SignatoryEntityList[0].ContactPH);
                dic.Add("business.accountsig1.email", entity.SignatoryEntityList[0].Email);


                if (entity.Sig2Required)
                {
                    dic.Add("business.accountsig2.title.mr", entity.SignatoryEntityList[1].Title.ToLower() == "mr" ? "Yes" : "0");
                    dic.Add("business.accountsig2.title.mrs", entity.SignatoryEntityList[1].Title.ToLower() == "mrs" ? "Yes" : "0");
                    dic.Add("business.accountsig2.title.miss", entity.SignatoryEntityList[1].Title.ToLower() == "miss" ? "Yes" : "0");
                    dic.Add("business.accountsig2.title.ms", entity.SignatoryEntityList[1].Title.ToLower() == "ms" ? "Yes" : "0");
                    dic.Add("business.accountsig2.other", (entity.SignatoryEntityList[1].Title.ToLower() != "mr" && entity.SignatoryEntityList[1].Title.ToLower() != "mrs" && entity.SignatoryEntityList[1].Title.ToLower() != "miss" && entity.SignatoryEntityList[1].Title.ToLower() != "ms") ? entity.SignatoryEntityList[1].Title : "");
                    dic.Add("business.accountsig2.givenname", entity.SignatoryEntityList[1].GivenName);
                    dic.Add("business.accountsig2.surname", entity.SignatoryEntityList[1].FamilyName);
                    dic.Add("business.accountsig2.address.line1", entity.SignatoryEntityList[1].ResiAddressLine1);
                    dic.Add("business.accountsig2.address.line2", entity.SignatoryEntityList[1].ResiAddressLine2);
                    dic.Add("business.accountsig2.suburb", entity.SignatoryEntityList[1].ResiSuburb);
                    dic.Add("business.accountsig2.state", State.GetStateCode(entity.SignatoryEntityList[1].ResiState));
                    dic.Add("business.accountsig2.postcode", entity.SignatoryEntityList[1].ResiPostCode);
                    dic.Add("business.accountsig2.country", entity.SignatoryEntityList[1].ResiCountry);
                    if (DateTime.TryParse(entity.SignatoryEntityList[0].DateOfBirth, new DateTimeFormatInfo() { ShortDatePattern = "dd/MM/yyyy" }, DateTimeStyles.None, out date))
                    {
                        dic.Add("business.accountsig2.dob.day", date.Day.ToString());
                        dic.Add("business.accountsig2.dob.month", date.Month.ToString());
                        dic.Add("business.accountsig2.dob.year", date.Year.ToString());
                    }
                    dic.Add("business.accountsig2.password", "");
                    dic.Add("business.accountsig2.occupation", entity.SignatoryEntityList[1].Occupation);
                    dic.Add("business.accountsig2.res.country", entity.SignatoryEntityList[1].MainCountryOFAddressSig);
                    dic.Add("business.accountsig2.employer", entity.SignatoryEntityList[1].Employer);
                    dic.Add("business.accountsig2.phone.prefix", "");
                    dic.Add("business.accountsig2.phone.number", entity.SignatoryEntityList[1].ContactPH);
                    dic.Add("business.accountsig2.email", entity.SignatoryEntityList[1].Email);
                }



            }
            #endregion






            return dic;
        }


        
    }
}
