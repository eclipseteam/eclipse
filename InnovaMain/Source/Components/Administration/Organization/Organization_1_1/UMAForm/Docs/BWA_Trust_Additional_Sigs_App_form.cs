﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.CM.Organization.UMAForm
{
    public class BWA_Trust_Additional_Sigs_App_form : ITagFile
    {
        public CalculationInterface.ICMBroker Broker { get; set; }
        public Dictionary<string, string> GenerateTags(UMAFormEntity entity, ServiceTypes serviceType)
        {

            // todo: please read whole to todo list.its has some important data to be implemented."APPROVED DISTRIBUTOR USE ONLY" section is not implemented.
            var dic= new Dictionary<string, string>()
            {
                //todo:fields are not in our system its purpose of investment
              
                {"trust.fundname.line1",entity.accountName},
                {"trust.trustee.acn",entity.ABNCompany},
                //todo:fields are not in our system
                {"trust.super",""},
               
                {"trust.trustee.acn/arbn",entity.ACN},
                {"trust.trustee.abn",entity.ABNCompany},
                {"trust.trustee.password",""},
                {"trust.trustee.countryofestablishment",(entity.RegCountry == "Australia"?"":entity.RegCountry)},

                {"trust.correspondence.streetaddress.line1",entity.SignatoryEntityList[0].ResiAddressLine1},
                {"trust.correspondence.streetaddress.suburb",entity.RegSuburb},
                {"trust.correspondence.streetaddress.state",State.GetStateCode(entity.RegState)},
                {"trust.correspondence.streetaddress.postcode",entity.RegPostCode},
                {"trust.correspondence.streetaddress.country",(entity.RegCountry == "Australia"?"":entity.RegCountry)},
                {"trust.trustee.entityname.line1", entity.trustName},
              
               
                {"trust.correspondence.dupstatement.address.line1",entity.DupMialingAddressLine1},
                {"trust.correspondence.dupstatement.suburb",entity.DupMialingSuburb},
                {"trust.correspondence.dupstatement.state",State.GetStateCode( entity.DupMialingState)},
                {"trust.correspondence.dupstatement.postcode",entity.DupMialingPostCode},
                {"trust.correspondence.dupstatement.country",(entity.DupMialingCountry == "Australia"?"":entity.DupMialingCountry)},
                
                 //todo:fields are not in our system
                {"trust.correspondence.contact.contactname",entity.SignatoryEntityList[0].GivenName+ " "+ entity.SignatoryEntityList[0].MiddleName+ " "+ entity.SignatoryEntityList[0].FamilyName},
                {"trust.correspondence.contactnumber.prefix",""},
                {"trust.correspondence.contactnumber.number",entity.SignatoryEntityList[0].ContactPH!=String.Empty?entity.SignatoryEntityList[0].ContactPH:entity.SignatoryEntityList[0].AlternativePH},
                {"trust.correspondence.contact.contactemail",entity.SignatoryEntityList[0].Email},
                {"trust.platformname","e-Clipse Online , XPlan"},

                {"trust.investorplus.telephoneaccess",(entity.DIYPhoneAccess||entity.DIWMPhoneAccess)?"Yes":"0"},
                {"trust.investorplus.onlineaccess",(entity.DIYOnlineAccess||entity.DIWMOnlineAccess)?"Yes":"0"},
                {"trust.investorplus.debitcard",(entity.DIYDebitCard||entity.DIWMDebitCard)?"Yes":"0"},
                {"trust.investorplus.chequebook",(entity.DIYChequeBook||entity.DIWMChequeBook)?"Yes":"0"},
                {"trust.investorplus.depositbook",(entity.DIYDepositBook||entity.DIWMDepositBook)?"Yes":"0"},
                //todo:fields in the pdf are same needs to be different
                {"trust.mannerofoperations.oneofus",(entity.DIYMannerOfOperation=="Any one of us to sign"||entity.DIWMMannerOfOperation=="Any one of us to sign")?"Yes":"0"},
                {"trust.mannerofoperations.twoofus",(entity.DIYMannerOfOperation=="Any two of us to sign"||entity.DIWMMannerOfOperation=="Any two of us to sign")?"Yes":"0"},
                {"trust.mannerofoperations.allofus",(entity.DIYMannerOfOperation=="All of us to sign"||entity.DIWMMannerOfOperation=="All of us to sign")?"Yes":"0"},
                
                {"trust.tfn/abn",(!string.IsNullOrEmpty(entity.TFNTrustFund))?entity.TFNTrustFund:entity.ABNCompany},
                {"trust.tin",""},
                {"trust.nonres.country",entity.NonRedidentCountry},
                 //todo:fields are not in our system
                {"trust.tfnexempt.1","0"},
                {"trust.tfnexempt.2","0"},
                {"trust.tfnexempt.3","0"},
                //todo:document checks are not provided in checklist please add
               {"trust.documentchecklist","0"},
               //todo: advisor firm access is not defined in form and in pdf field name are not correct for Advisor access,Equiry access and no access please correct
                {"trust.adviserfirmaccess","0"},
                {"trust.equiryfirmaccess","0"},
                {"trust.noaccess","0"},
                {"trust.correspondence.mailingaddress.line1", EclipseMailingAddress.AddresLine},
                {"trust.correspondence.mailingaddress.suburb", EclipseMailingAddress.Subrub},
                {"trust.correspondence.mailingaddress.state", EclipseMailingAddress.State},
                {"trust.correspondence.mailingaddress.country", EclipseMailingAddress.Country},
                {"trust.correspondence.mailingaddress.postcode", EclipseMailingAddress.PCode},

                //todo:Approved Distributor Use only are not provided 
                //{"trust.approveddistributor.groupname",""},
                //{"trust.approveddistributor.dealername",""},
                
                //{"trust.approveddistributor.clientaccount",""},
                //{"trust.cmtbsb",""},
                //{"trust.cmtaccountnumber",""},
                //{"trust.platformname",""},
                //{"trust.platformreference",""},
                //{"txtreceiptnumber",""},
                //{"receiptnumber",""},
                
            };

            #region IFA


            if (entity.IFACID != Guid.Empty)
            {
                OrganizationUnitCM IFA = Broker.GetBMCInstance(entity.IFACID) as OrganizationUnitCM;
                IFADS ds = new IFADS();
                IFA.GetData(ds);

                dic.Add("trust.approveddistributor.advisorname", ds.ifaTable.Rows[0][ds.ifaTable.TRADINGNAME].ToString());
                dic.Add("trust.approveddistributor.advisorcode", ds.ifaTable.Rows[0][ds.ifaTable.SKCODE].ToString());
                Broker.ReleaseBrokerManagedComponent(IFA);
            }

            #endregion

            #region Signatories
            if (entity.SignatoryEntityList != null && entity.SignatoryEntityList.Count > 2)
            {

                DateTime date;

                if (entity.Sig3Required)
                {
                    dic.Add("trust.trustee.accountsig1.title.mr", entity.SignatoryEntityList[2].Title.ToLower() == "mr" ? "Yes" : "0");
                    dic.Add("trust.trustee.accountsig1.title.mrs", entity.SignatoryEntityList[2].Title.ToLower() == "mrs" ? "Yes" : "0");
                    dic.Add("trust.trustee.accountsig1.title.miss", entity.SignatoryEntityList[2].Title.ToLower() == "miss" ? "Yes" : "0");
                    dic.Add("trust.trustee.accountsig1.title.ms", entity.SignatoryEntityList[2].Title.ToLower() == "ms" ? "Yes" : "0");
                    dic.Add("trust.trustee.accountsig1.other", (entity.SignatoryEntityList[2].Title.ToLower() != "mr" && entity.SignatoryEntityList[3].Title.ToLower() != "mrs" && entity.SignatoryEntityList[3].Title.ToLower() != "miss" && entity.SignatoryEntityList[3].Title.ToLower() != "ms") ? entity.SignatoryEntityList[3].Title : "");
                    dic.Add("trust.trustee.accountsig1.givenname", entity.SignatoryEntityList[2].GivenName + " " + entity.SignatoryEntityList[3].MiddleName);
                    dic.Add("trust.trustee.accountsig1.surname", entity.SignatoryEntityList[2].FamilyName);
                    dic.Add("trust.accountsig1.address.line1", entity.SignatoryEntityList[2].ResiAddressLine1);
                    dic.Add("trust.accountsig1.address.line2", entity.SignatoryEntityList[2].ResiAddressLine2);
                    dic.Add("trust.accountsig1.suburb", entity.SignatoryEntityList[2].ResiSuburb);
                    dic.Add("trust.accountsig1.state", State.GetStateCode(entity.SignatoryEntityList[2].ResiState));
                    dic.Add("trust.accountsig1.postcode", entity.SignatoryEntityList[2].ResiPostCode);
                    dic.Add("trust.accountsig1.country", entity.SignatoryEntityList[2].ResiCountry);
                    
                    if (DateTime.TryParse(entity.SignatoryEntityList[3].DateOfBirth, new DateTimeFormatInfo() { ShortDatePattern = "dd/MM/yyyy" }, DateTimeStyles.None, out date))
                    {
                        dic.Add("trust.trustee.accountsig1.dob.day", date.Day.ToString());
                        dic.Add("trust.trustee.accountsig1.dob.month", date.Month.ToString());
                        dic.Add("trust.trustee.accountsig1.dob.year", date.Year.ToString());
                    }
                    //todo:fields are not in our system also set for second signatory
                    dic.Add("trust.trustee.accountsig1.password", "");
                    dic.Add("trust.accountsig1.occupation", entity.SignatoryEntityList[2].Occupation);
                    dic.Add("trust.accountsig1.res.country", entity.SignatoryEntityList[2].MainCountryOFAddressSig);
                    dic.Add("trust.accountsig1.employer", entity.SignatoryEntityList[2].Employer);
                    //todo:fields are not in our system also set for second signatory
                    dic.Add("trust.accountsig1.phone.prefix", "");
                    dic.Add("trust.accountsig1.phone.number", entity.SignatoryEntityList[2].ContactPH);
                    dic.Add("trust.trustee.accountsig1.email", entity.SignatoryEntityList[2].Email);

                }

                if (entity.Sig4Required)
                {
                    dic.Add("trust.trustee.accountsig2.title.mr", entity.SignatoryEntityList[3].Title.ToLower() == "mr" ? "Yes" : "0");
                    dic.Add("trust.trustee.accountsig2.title.mrs", entity.SignatoryEntityList[3].Title.ToLower() == "mrs" ? "Yes" : "0");
                    dic.Add("trust.trustee.accountsig2.title.miss", entity.SignatoryEntityList[3].Title.ToLower() == "miss" ? "Yes" : "0");
                    dic.Add("trust.trustee.accountsig2.title.ms", entity.SignatoryEntityList[3].Title.ToLower() == "ms" ? "Yes" : "0");
                    dic.Add("trust.trustee.accountsig2.other", (entity.SignatoryEntityList[3].Title.ToLower() != "mr" && entity.SignatoryEntityList[3].Title.ToLower() != "mrs" && entity.SignatoryEntityList[3].Title.ToLower() != "miss" && entity.SignatoryEntityList[3].Title.ToLower() != "ms") ? entity.SignatoryEntityList[3].Title : "");
                    dic.Add("trust.trustee.accountsig2.givenname", entity.SignatoryEntityList[3].GivenName + " " + entity.SignatoryEntityList[3].MiddleName);
                    dic.Add("trust.trustee.accountsig2.surname", entity.SignatoryEntityList[3].FamilyName);
                    dic.Add("trust.accountsig2.address.line1", entity.SignatoryEntityList[3].ResiAddressLine1);
                    dic.Add("trust.accountsig2.address.line2", entity.SignatoryEntityList[3].ResiAddressLine2);
                    dic.Add("trust.accountsig2.suburb", entity.SignatoryEntityList[3].ResiSuburb);
                    dic.Add("trust.accountsig2.state",State.GetStateCode(entity.SignatoryEntityList[3].ResiState));
                    dic.Add("trust.accountsig2.postcode", entity.SignatoryEntityList[3].ResiPostCode);
                    dic.Add("trust.accountsig2.country", entity.SignatoryEntityList[3].ResiCountry);
                    if (DateTime.TryParse(entity.SignatoryEntityList[3].DateOfBirth, new DateTimeFormatInfo() { ShortDatePattern = "dd/MM/yyyy" }, DateTimeStyles.None, out date))
                    {
                        dic.Add("trust.trustee.accountsig2.dob.day", date.Day.ToString());
                        dic.Add("trust.trustee.accountsig2.dob.month", date.Month.ToString());
                        dic.Add("trust.trustee.accountsig2.dob.year", date.Year.ToString());
                    }
                    dic.Add("trust.trustee.accountsig2.password", "");
                    dic.Add("trust.accountsig2.occupation", entity.SignatoryEntityList[3].Occupation);
                    dic.Add("trust.accountsig2.res.country", entity.SignatoryEntityList[3].MainCountryOFAddressSig);
                    dic.Add("trust.accountsig2.employer", entity.SignatoryEntityList[3].Employer);
                    dic.Add("trust.accountsig2.phone.prefix", "");
                    dic.Add("trust.accountsig2.phone.number", entity.SignatoryEntityList[3].ContactPH);
                    dic.Add("trust.trustee.accountsig2.email", entity.SignatoryEntityList[3].Email);
                }

            } 
            #endregion
            #region Investment Detail
             if (entity.DoItYourSelf)
            {
              
                dic.Add("Text1", ("SMSF CASH ACCOUNT"));
                
            }
             else if (entity.DoItWithMe)
             {
                 //dic.Add("trust.correspondence.mailingaddress.postcode", "2089");
                 dic.Add("Text1", ("DIWM CASH ACCOUNT"));
             }
             else if (entity.DoItForhMe)
                dic.Add("Text1", ("DIFM CASH ACCOUNT"));
            
           
            else
            {
                dic.Add("Text1", ("CASH ACCOUNT"));
            }
            #endregion 

            return dic;
        }
    }
}
