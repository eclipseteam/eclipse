﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Exceptions;

namespace Oritax.TaxSimp.CM.Organization.UMAForm
{
    public interface ITagFile
    {
        Dictionary<string, string> GenerateTags(UMAFormEntity entity, ServiceTypes serviceType);
        CalculationInterface.ICMBroker Broker { get; set; }
    }

    public interface ISignatoyFile : ITagFile
    {
         SignatoryEntity Signatory { get; set; }
    }


    public class FileTagFactory
    {
        public static Dictionary<string, Type> Files = new Dictionary<string, Type>()
                                                       {
                                                        {"BWA Business & Additional Sigs App form", typeof(BWA_Business_Additional_Sigs_App_form)},  
                                                        {"BWA Trust & Additional Sigs App form", typeof(BWA_Trust_Additional_Sigs_App_form)},
                                                        {"BWA Business App form", typeof(BWA_Business_App_form)},
                                                        {"BWA Trust App form", typeof(BWA_Trust_App_form)},
                                                        {"BWAMI CIP", typeof(BWAMI_CIP)},
                                                        {"BWA CMA Trust ID form", typeof(BWA_CMA_Trust_ID_form)},
                                                        {"BWA CMA Company ID form", typeof(BWA_CMA_Company_ID_form)}, 
                                                        {"BWA Personal App form", typeof(BWA_Ind_App_form)}, 
                                                       };

        public static ITagFile GetFileTagCommand(string fileName)
        {
            if (!Files.ContainsKey(fileName)) return null;
            Type value = Files[fileName];
            return Activator.CreateInstance(value) as ITagFile; ;
        }
    }
}
