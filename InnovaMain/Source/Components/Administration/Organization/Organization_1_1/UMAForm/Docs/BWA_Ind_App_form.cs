﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.CM.Organization.UMAForm
{
    public class BWA_Ind_App_form : ITagFile
    {
        public CalculationInterface.ICMBroker Broker { get; set; }
        public Dictionary<string, string> GenerateTags(UMAFormEntity entity, ServiceTypes serviceType)
        {
            string bsb = string.Empty;
            string accountNo = string.Empty;

            if (entity.ClientCID != null && entity.ClientCID != Guid.Empty)
            {
                IOrganizationUnit orgUnit = Broker.GetBMCInstance(entity.ClientCID) as IOrganizationUnit;
                Common.BankAccountEntity bankAccountEntity = orgUnit.GetBankAccountDetails(bsb, accountNo, serviceType);
                if (bankAccountEntity != null)
                {
                    bsb = bankAccountEntity.BSB;
                    accountNo = bankAccountEntity.AccountNumber;
                }
            }

            var dic = new Dictionary<string, string>();
              
                dic.Add("personal.applicant1.tfn",entity.TFNSig1);
                dic.Add("personal.applicant2.tfn",entity.TFNSig2);

                 dic.Add("personal.dupstatement.address.line1",entity.DupMialingAddressLine1);
                dic.Add("personal.dupstatement.address.line2",entity.DupMialingAddressLine2);
                dic.Add("personal.dupstatement.suburb",entity.DupMialingSuburb);
                dic.Add("personal.dupstatement.state",State.GetStateCode(entity.DupMialingState));
                dic.Add("personal.dupstatement.postcode",entity.DupMialingPostCode);
                dic.Add("personal.dupstatement.country",((entity.DupMialingCountry=="Australia")?" ":entity.DupMialingCountry));
                
               
                //6:Maner of operation
                dic.Add("OperationManner.ANY_ONE_OF_US_TO_SIGN",(entity.DIYMannerOfOperation=="Any one of us to sign"||entity.DIWMMannerOfOperation=="Any one of us to sign")?"Yes":"0");
                dic.Add("OperationManner.ANY_TWO_OF_US_TO_SIGN",(entity.DIYMannerOfOperation=="Any two of us to sign"||entity.DIWMMannerOfOperation=="Any two of us to sign")?"Yes":"0");
                dic.Add("OperationManner.ALL_OF_US_TO_SIGN",(entity.DIYMannerOfOperation=="All of us to sign"||entity.DIWMMannerOfOperation=="All of us to sign")?"Yes":"0");
            
             
                  dic.Add("bsb",bsb);
                dic.Add("account",accountNo);


                dic.Add("trust.correspondence.contact.contactname",entity.trustName);
                dic.Add("trust.regulated.licence",entity.ABNCompany);
                
                dic.Add("personal.applicant1.mailingaddress.line1", EclipseMailingAddress.AddresLine);
                dic.Add("personal.applicant1.mailingaddress.suburb", EclipseMailingAddress.Subrub);
                dic.Add("personal.applicant1.mailingaddress.state", EclipseMailingAddress.State);
                dic.Add("personal.applicant1.mailingaddress.country",EclipseMailingAddress.Country);
                dic.Add("personal.applicant1.mailingaddress.postcode", EclipseMailingAddress.PCode);

            #region IFA


            if (entity.IFACID != Guid.Empty)
            {
                OrganizationUnitCM IFA = Broker.GetBMCInstance(entity.IFACID) as OrganizationUnitCM;
                IFADS ds = new IFADS();
                IFA.GetData(ds);

                dic.Add("personal.approveddistributor.advisorname", ds.ifaTable.Rows[0][ds.ifaTable.TRADINGNAME].ToString() + "  ( " + ds.ifaTable.Rows[0][ds.ifaTable.SKCODE].ToString() + " )");
                dic.Add("personal.approveddistributor.groupname", ds.ifaTable.Rows[0][ds.ifaTable.TRADINGNAME].ToString());

                dic.Add("personal.approveddistributor.advisorcode", ds.ifaTable.Rows[0][ds.ifaTable.SKCODE].ToString());
                Broker.ReleaseBrokerManagedComponent(IFA);
            }

            #endregion

            #region Access Facilities

            if (entity.DoItForhMe)
            {
                dic.Add("personal.investorplus.telephoneaccess", (entity.DIYPhoneAccess || entity.DIWMPhoneAccess) ? "Yes" : "0");
                dic.Add("personal.investorplus.onlineaccess", (entity.DIYOnlineAccess || entity.DIWMOnlineAccess) ? "Yes" : "0");
                dic.Add("personal.investorplus.debitcard", (entity.DIYDebitCard || entity.DIWMDebitCard) ? "Yes" : "0");
                dic.Add("personal.investorplus.chequebook", (entity.DIYChequeBook || entity.DIWMChequeBook) ? "Yes" : "0");
                dic.Add("personal.investorplus.depositbook", (entity.DIYDepositBook || entity.DIWMDepositBook) ? "Yes" : "0");
            }
            else
            {
                dic.Add("personal.investorplus.telephoneaccess",  "0");
                dic.Add("personal.investorplus.onlineaccess", "0");
                dic.Add("personal.investorplus.debitcard", "0");
                dic.Add("personal.investorplus.chequebook", "0");
                dic.Add("personal.investorplus.depositbook",  "0");
            }
            #endregion 
            #region Investment Detail

             if (entity.DoItYourSelf)
            {
               
                dic.Add("Text1", ("SMSF CASH ACCOUNT"));
               
            }
             else if (entity.DoItWithMe)
                 dic.Add("Text1", ("DIWM CASH ACCOUNT"));
             else if (entity.DoItForhMe)
                dic.Add("Text1", ("DIFM CASH ACCOUNT"));
            else
                dic.Add("Text1", ("CASH ACCOUNT"));
            #endregion 
            #region Signatories
            //3:Trustee and CMA Account Signatory Details
            if (entity.SignatoryEntityList != null && entity.SignatoryEntityList.Count > 0)
            {

                dic.Add("personal.applicant1.title.mr", entity.SignatoryEntityList[0].Title.ToLower() == "mr" ? "Yes" : "0");
                dic.Add("personal.applicant1.title.mrs", entity.SignatoryEntityList[0].Title.ToLower() == "mrs" ? "Yes" : "0");
                dic.Add("personal.applicant1.title.miss", entity.SignatoryEntityList[0].Title.ToLower() == "miss" ? "Yes" : "0");
                dic.Add("personal.applicant1.title.ms", entity.SignatoryEntityList[0].Title.ToLower() == "ms" ? "Yes" : "0");
                dic.Add("personal.applicant1.other", (entity.SignatoryEntityList[0].Title.ToLower() != "mr" && entity.SignatoryEntityList[0].Title.ToLower() != "mrs" && entity.SignatoryEntityList[0].Title.ToLower() != "miss" && entity.SignatoryEntityList[0].Title.ToLower() != "ms") ? entity.SignatoryEntityList[0].Title : "");
                dic.Add("personal.applicant1.givenname", entity.SignatoryEntityList[0].GivenName);
                dic.Add("personal.applicant1.middlename", entity.SignatoryEntityList[0].MiddleName);
                dic.Add("personal.applicant1.surname", entity.SignatoryEntityList[0].FamilyName);
                dic.Add("personal.applicant1.resaddress.line1", entity.SignatoryEntityList[0].ResiAddressLine1);
                dic.Add("personal.applicant1.resaddress.line2", entity.SignatoryEntityList[0].ResiAddressLine2);
                dic.Add("personal.applicant1.resaddress.suburb", entity.SignatoryEntityList[0].ResiSuburb);
                dic.Add("personal.applicant1.resaddress.state", State.GetStateCode( entity.SignatoryEntityList[0].ResiState));
                dic.Add("personal.applicant1.resaddress.postcode", entity.SignatoryEntityList[0].ResiPostCode);
                dic.Add("personal.applicant1.resaddress.country", (entity.SignatoryEntityList[0].ResiCountry == "Australia"?"":entity.SignatoryEntityList[0].ResiCountry));
                DateTime date;
                if (DateTime.TryParse(entity.SignatoryEntityList[0].DateOfBirth, new DateTimeFormatInfo() { ShortDatePattern = "dd/MM/yyyy" }, DateTimeStyles.None, out date))
                {
                    dic.Add("personal.applicant1.dob.day", date.Day.ToString());
                    dic.Add("personal.applicant1.dob.month", date.Month.ToString());
                    dic.Add("personal.applicant1.dob.year", date.Year.ToString());
                }
                //todo:fields are not in our system also set for second signatory
                dic.Add("personal.applicant1.password", "");
                dic.Add("personal.applicant1.occupation", entity.SignatoryEntityList[0].Occupation);
                dic.Add("personal.applicant1.res.country", entity.SignatoryEntityList[0].MainCountryOFAddressSig);
                dic.Add("personal.applicant1.employer", entity.SignatoryEntityList[0].Employer);
                //todo:fields are not in our system also set for second signatory
                dic.Add("Signatory1.Fullname", entity.SignatoryEntityList[0].GivenName + " " + entity.SignatoryEntityList[0].MiddleName +" " +entity.SignatoryEntityList[0].FamilyName);
                dic.Add("personal.applicant1.phone.prefix", "");
                if(entity.SignatoryEntityList[0].ContactPH != string.Empty)
                    dic.Add("personal.applicant1.contact.home.number", entity.SignatoryEntityList[0].ContactPH);
                if (entity.SignatoryEntityList[0].AlternativePH != string.Empty)
                    dic.Add("personal.applicant1.contact.work.number", entity.SignatoryEntityList[0].AlternativePH);
                if (entity.SignatoryEntityList[0].MobilePH != string.Empty)
                    dic.Add("personal.applicant1.contact.mobile.number", entity.SignatoryEntityList[0].AlternativePH);

                dic.Add("personal.applicant1.contact.email", entity.SignatoryEntityList[0].Email);
                if (entity.Sig2Required)
                {
                    dic.Add("personal.applicant2.title.mr", entity.SignatoryEntityList[1].Title.ToLower() == "mr" ? "Yes" : "0");
                    dic.Add("personal.applicant2.title.mrs", entity.SignatoryEntityList[1].Title.ToLower() == "mrs" ? "Yes" : "0");
                    dic.Add("personal.applicant2.title.miss", entity.SignatoryEntityList[1].Title.ToLower() == "miss" ? "Yes" : "0");
                    dic.Add("personal.applicant2.title.ms", entity.SignatoryEntityList[1].Title.ToLower() == "ms" ? "Yes" : "0");
                    dic.Add("personal.applicant2.other", (entity.SignatoryEntityList[1].Title.ToLower() != "mr" && entity.SignatoryEntityList[1].Title.ToLower() != "mrs" && entity.SignatoryEntityList[1].Title.ToLower() != "miss" && entity.SignatoryEntityList[1].Title.ToLower() != "ms") ? entity.SignatoryEntityList[1].Title : "");
                    dic.Add("personal.applicant2.givenname", entity.SignatoryEntityList[1].GivenName);
                    dic.Add("personal.applicant2.middlename", entity.SignatoryEntityList[1].MiddleName);
                    dic.Add("personal.applicant2.surname", entity.SignatoryEntityList[1].FamilyName);
                    dic.Add("personal.applicant2.resaddress.line1", entity.SignatoryEntityList[1].ResiAddressLine1);
                    dic.Add("personal.applicant2.resaddress.line2", entity.SignatoryEntityList[1].ResiAddressLine2);
                    dic.Add("personal.applicant2.resaddress.suburb", entity.SignatoryEntityList[1].ResiSuburb);
                    dic.Add("personal.applicant2.resaddress.state",State.GetStateCode( entity.SignatoryEntityList[1].ResiState));
                    dic.Add("personal.applicant2.resaddress.postcode", entity.SignatoryEntityList[1].ResiPostCode);
                    dic.Add("personal.applicant2.resaddress.country", (entity.SignatoryEntityList[1].ResiCountry == "Australia"?"":entity.SignatoryEntityList[1].ResiCountry));
                    if (DateTime.TryParse(entity.SignatoryEntityList[1].DateOfBirth, new DateTimeFormatInfo() { ShortDatePattern = "dd/MM/yyyy" }, DateTimeStyles.None, out date))
                    {
                        dic.Add("personal.applicant2.dob.day", date.Day.ToString());
                        dic.Add("personal.applicant2.dob.month", date.Month.ToString());
                        dic.Add("personal.applicant2.dob.year", date.Year.ToString());
                    }
                    dic.Add("personal.applicant2.password", "");
                    dic.Add("personal.applicant2.occupation", entity.SignatoryEntityList[1].Occupation);
                    dic.Add("personal.applicant2.res.country", entity.SignatoryEntityList[1].MainCountryOFAddressSig);
                    dic.Add("personal.applicant2.employer", entity.SignatoryEntityList[1].Employer);
                    dic.Add("personal.applicant2.phone.prefix", "");
                    if (entity.SignatoryEntityList[1].ContactPH != string.Empty)
                        dic.Add("personal.applicant2.contact.home.number", entity.SignatoryEntityList[0].ContactPH);
                    if (entity.SignatoryEntityList[1].AlternativePH != string.Empty)
                        dic.Add("personal.applicant2.contact.work.number", entity.SignatoryEntityList[0].AlternativePH);
                    if (entity.SignatoryEntityList[1].MobilePH != string.Empty)
                        dic.Add("personal.applicant2.contact.mobile.number", entity.SignatoryEntityList[0].AlternativePH);

                    dic.Add("personal.applicant2.contact.email", entity.SignatoryEntityList[1].Email);
                }

            }
            #endregion



            return dic;
        }
    }
}
