﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DataSets;

namespace Oritax.TaxSimp.CM.Organization.UMAForm
{
    public class BWAMI_CIP : ISignatoyFile
    {
        public CalculationInterface.ICMBroker Broker { get; set; }
        public SignatoryEntity Signatory { get; set; }
        public Dictionary<string, string> GenerateTags(UMAFormEntity entity, ServiceTypes serviceType)
        {
            var fields= new Dictionary<string, string>()
                                                   {
                                                       {"id.customer.full",Signatory.GivenName+" "+Signatory.MiddleName+" "+Signatory.FamilyName},
                                                       {"id.primarywithphoto.currentaustraliandriverslicence",Signatory.DriverLicSig?"Yes":"0"},
                                                       {"id.primarywithphoto.currentaustralianpassport",Signatory.PassportSig?"Yes":"0"},
                                                       {"id.primarywithphoto.proofofagecard","0"},
                                                       {"id.primarywithphoto.currentforeignpassport","0"},
                                                       {"id.primarywithoutphoto.birthcertificate","0"},
                                                       {"id.primarywithoutphoto.australiancitizenshipcertificate","0"},
                                                       {"id.primarywithoutphoto.pensioncard","0"},
                                                       {"id.primarywithoutphoto.healthcard","0"},
                                                       {"id.secondary.ratingauthoritybillsightedt","0"},
                                                       {"id.secondary.atonotice","0"},
                                                       {"id.secondary.publicutilityrecordbillsighted","0"},
                                                       {"id.secondary.school","0"}
                                                      
                                                     };


            
           #region IFA


           if (entity.IFACID != Guid.Empty)
           {
               OrganizationUnitCM IFA = Broker.GetBMCInstance(entity.IFACID) as OrganizationUnitCM;
               IFADS ds = new IFADS();
               IFA.GetData(ds);

               fields.Add("id.planners.name", ds.ifaTable.Rows[0][ds.ifaTable.TRADINGNAME].ToString() + "  ( " + ds.ifaTable.Rows[0][ds.ifaTable.SKCODE].ToString() + " )");
               fields.Add("id.planner.phoneno", ds.ifaTable.Rows[0][ds.ifaTable.PHONENUBER].ToString());
               decimal cityCode = 0;
               if (ds.ifaTable.Rows[0][ds.ifaTable.PHONENUMBERCITYCODE] != null && ds.ifaTable.Rows[0][ds.ifaTable.PHONENUMBERCITYCODE].ToString() != String.Empty)
                   cityCode = Convert.ToDecimal(ds.ifaTable.Rows[0][ds.ifaTable.PHONENUMBERCITYCODE]);
               string countryCityCode = string.Empty;
               if (ds.ifaTable.Rows[0][ds.ifaTable.PHONENUMBERCOUNTRYCODE] != null && ds.ifaTable.Rows[0][ds.ifaTable.PHONENUMBERCOUNTRYCODE].ToString() != String.Empty)
                    countryCityCode = ds.ifaTable.Rows[0][ds.ifaTable.PHONENUMBERCOUNTRYCODE].ToString() + cityCode.ToString();

               string phonePrefix = String.Empty;
               if(countryCityCode != null && countryCityCode != string.Empty)   
                   phonePrefix = Convert.ToDecimal(countryCityCode).ToString("{0:+##-#}");

               fields.Add("id.advisor.full", ds.ifaTable.Rows[0][ds.ifaTable.TRADINGNAME].ToString());
               fields.Add("id.advisor.phone.prefix", phonePrefix);
               fields.Add("id.advisor.phone.number", ds.ifaTable.Rows[0][ds.ifaTable.PHONENUBER].ToString());
               string afslNo = string.Empty; 

               if(ds.ifaTable.Rows[0][ds.ifaTable.TRADINGNAME].ToString().Contains("Fortnum Private Wealth Pty Ltd"))
                   fields.Add("id.planner.afs.no", "357306");
               else if(ds.ifaTable.Rows[0][ds.ifaTable.TRADINGNAME].ToString().Contains("RI"))
                   fields.Add("id.planner.afs.no", "238429");
               else
                   fields.Add("id.planner.afs.no", "357306");
             
               var parent = (IFA as ICmHasParent);

               if (parent != null && parent.Parent != IdentityCM.Null)
               {
                   OrganizationUnitCM dealorGroup =Broker.GetCMImplementation(parent.Parent.Clid, parent.Parent.Csid) as OrganizationUnitCM;
                   
                       DealerGroupDS DGds = new DealerGroupDS();
                       dealorGroup.GetData(DGds);
                       fields.Add("id.planner.afs.name",
                                  DGds.DealerGroupTable.Rows[0][DGds.DealerGroupTable.TRADINGNAME].ToString());
                       //fields.Add("id.planner.afs.no",
                       //           DGds.DealerGroupTable.Rows[0][DGds.DealerGroupTable.AFSLNO].ToString());
                       fields.Add("id.advisor.afs.name",
                                  DGds.DealerGroupTable.Rows[0][DGds.DealerGroupTable.TRADINGNAME].ToString());
                       fields.Add("id.advisor.afs.number",
                                  DGds.DealerGroupTable.Rows[0][DGds.DealerGroupTable.AFSLNO].ToString());
                       Broker.ReleaseBrokerManagedComponent(dealorGroup);
               }

               Broker.ReleaseBrokerManagedComponent(IFA);
           }
        
           #region Advisor

           DateTimeFormatInfo info = new DateTimeFormatInfo();
           info.ShortDatePattern = "dd/MM/yyyy";
           
               fields.Add("id.advisor.dateverified.day", DateTime.Now.Day.ToString());
               fields.Add("id.advisor.dateverified.month", DateTime.Now.Month.ToString());
               fields.Add("id.advisor.dateverified.year", DateTime.Now.Year.ToString());
           
           #endregion
           #endregion

            #region Documents
            string doc = "primary";
            string number = "1";
            if (!string.IsNullOrEmpty(Signatory.LicDocumentIssuer.Trim()) || !string.IsNullOrEmpty(Signatory.LicIssueDate.Trim()) || !string.IsNullOrEmpty(Signatory.LicExpiryDate.Trim()))
            {
                fields.Add(string.Format("id.{0}.original", doc), "0");
                fields.Add(string.Format("id.{0}.certified", doc), Signatory.LicCertifiedCopyAttached ? "Yes" : "0");
                if (entity.DoItYourSelf)
                { fields.Add(string.Format("id.document.{0}.issuedby", number), "QLD"); }
                else
                {
                    fields.Add(string.Format("id.document.{0}.issuedby", number), Signatory.LicDocumentIssuer); 
                }
               
                DateTime date;
                if (DateTime.TryParse(Signatory.LicIssueDate, new DateTimeFormatInfo() { ShortDatePattern = "dd/MM/yyyy" }, DateTimeStyles.None, out date))
                {
                    fields.Add(string.Format("id.document.{0}.dateofissue.day", number), date.Day.ToString());
                    fields.Add(string.Format("id.document.{0}.dateofissue.month", number), date.Month.ToString());
                    fields.Add(string.Format("id.document.{0}.dateofissue.year", number), date.Year.ToString());
                }
                if (DateTime.TryParse(Signatory.LicExpiryDate, new DateTimeFormatInfo() { ShortDatePattern = "dd/MM/yyyy" }, DateTimeStyles.None, out date))
                {
                    fields.Add(string.Format("id.document.{0}.expirydate.day", number), date.Day.ToString());
                    fields.Add(string.Format("id.document.{0}.expirydate.month", number), date.Month.ToString());
                    fields.Add(string.Format("id.document.{0}.expirydate.year", number), date.Year.ToString());
                }
                fields.Add(string.Format("id.document.{0}.docno", number), Signatory.DocumentNoLic);
                fields.Add(string.Format("id.document.{0}.translation.na", number), "");
                fields.Add(string.Format("id.document.{0}.translation.yes", number), "");

                //change documnet to secondary and number to 3
                doc = "secondary";
                number = "3";
            }

            if (!string.IsNullOrEmpty(Signatory.PassportDocumentIssuer.Trim()) || !string.IsNullOrEmpty(Signatory.PassportIssueDate.Trim()) || !string.IsNullOrEmpty(Signatory.PassportExpiryDate.Trim()))
            {
                fields.Add(string.Format("id.{0}.original", doc), "0");
                fields.Add(string.Format("id.{0}.certified", doc), Signatory.PassportCertifiedCopyAttached ? "Yes" : "0");
                fields.Add(string.Format("id.document.{0}.issuedby", number), Signatory.PassportDocumentIssuer);

                DateTime date;
                if (DateTime.TryParse(Signatory.PassportIssueDate, new DateTimeFormatInfo() { ShortDatePattern = "dd/MM/yyyy" }, DateTimeStyles.None, out date))
                {
                    fields.Add(string.Format("id.document.{0}.dateofissue.day", number), date.Day.ToString());
                    fields.Add(string.Format("id.document.{0}.dateofissue.month", number), date.Month.ToString());
                    fields.Add(string.Format("id.document.{0}.dateofissue.year", number), date.Year.ToString());
                }
                if (DateTime.TryParse(Signatory.PassportExpiryDate, new DateTimeFormatInfo() { ShortDatePattern = "dd/MM/yyyy" }, DateTimeStyles.None, out date))
                {
                    fields.Add(string.Format("id.document.{0}.expirydate.day", number), date.Day.ToString());
                    fields.Add(string.Format("id.document.{0}.expirydate.month", number), date.Month.ToString());
                    fields.Add(string.Format("id.document.{0}.expirydate.year", number), date.Year.ToString());
                }
                fields.Add(string.Format("id.document.{0}.docno", number), Signatory.DocumentNoPassport);
                fields.Add(string.Format("id.document.{0}.translation.na", number), "");
                fields.Add(string.Format("id.document.{0}.translation.yes", number), "");

                //change documnet to secondary and number to 3
                doc = "secondary";
                number = "3";
            } 
            #endregion

            return fields;
        }

       
    }
}
