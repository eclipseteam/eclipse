﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.CM.Organization.Data;
using System.Data;
using System.ComponentModel;

namespace Oritax.TaxSimp.CM.Organization.UMAForm
{
    public class OneFormVaidationEntity
    {
        public const string WARNING = "Warning";
        public const string ERROR = "Error";
        public const string MANDATORY ="Mandatory field not filled";

        public string ShortDesc = string.Empty;
        public string Description = string.Empty;
        public string ErrorType = string.Empty;
    }
    [Serializable]
    public enum UMAFormStatus
    {
        [Description("Initialised")]
        Intialised,
        [Description("Under review")]
        UnderReview,
        [Description("Finalised")]
        Finalised
    }

    [Serializable]
    public class UMAFormEntity
    {
        [NonSerialized]
        public List<OneFormVaidationEntity> OneFormVaidationEntityList = new List<OneFormVaidationEntity>();

        public bool Sig2Required = false;
        public bool Sig3Required = false;
        public bool Sig4Required = false;

        #region 1. Account Details

        public UMAFormStatus UMAFormStatus = UMAFormStatus.Intialised; 

        public string userName = string.Empty;
        public DateTime lastModifiedDate;
        public DateTime CreatedOnDate;
        public string lastModifiedDateForm;
        public string CreatedOnDateForm;

        public string modifiedByUser = string.Empty;

        public string accountType = string.Empty;
        public string ClientID = string.Empty;
        public Guid ClientCID = Guid.Empty;
        public string accountNO = string.Empty;
        public string accountName = string.Empty;
        public string trustName = string.Empty;
        public Guid IFACID = Guid.Empty;
     

        #endregion

        #region Fee

        public bool TierFee = false;
        public bool OnGoingFee = false;

        public decimal UpFrontFee = 0;

        public double OngoingFeeDollar = 0;

        public double OngoingFeePercentage = 0;

        public decimal Tier1To = 0;
        public decimal Tier1From = 0;
        public decimal Tier1Percentage = 0;

        public decimal Tier2To = 0;
        public decimal Tier2From = 0;
        public decimal Tier2Percentage = 0;

        public decimal Tier3To = 0;
        public decimal Tier3From = 0;
        public decimal Tier3Percentage = 0;

        public decimal Tier4To = 0;
        public decimal Tier4From = 0;
        public decimal Tier4Percentage = 0;

        public decimal Tier5To = 0;
        public decimal Tier5From = 0;
        public decimal Tier5Percentage = 0;

        #endregion

        #region TFN ABN ABRN

        public string TFNSig1 = string.Empty;
        public string TFNSig2 = string.Empty;
        public string TFNSig3 = string.Empty;
        public string TFNSig4 = string.Empty;

        public string TFNTrustFund = string.Empty;
        public string ABNCompany = string.Empty;
        public string ACN = string.Empty;

        public bool AppliedForABN = false;
        public string NonRedidentCountry = string.Empty;

        #endregion

        #region Authority to Act

        public bool EclipseOnline = false;
        public bool AustralianMoneyMarket = false;
        public bool MacquarieCMA = false;

        public bool InvestorStatusWholesaleInvestor = false;
        public bool InvestorStatusSophisticatedInvestor = false;
        public bool InvestorStatusProfessionalInvestor = false;
        public bool InvestorStatusRetailInvestor = false;

        public bool CertificateFromAccountantIsAttached = false;

        #endregion

        #region Services

        public bool DoItYourSelf = false;
        public bool DIYBWA = false;
        public bool DIYDesktopBroker = false;
        public bool DIYFIIG = false;
        public bool DIYAMM = false;
        public decimal DIYAmount = 0;

        public bool DoItWithMe = false;
        public bool DIWNMBWA = false;
        public bool DIWNMDesktopBroker = false;
        public bool DIWNMFIIG = false;
        public bool DIWNMAMM = false;
        public bool DIWNMSS = false;
        public decimal DIWMAmount = 0;
        public List<StateStreetEntity> StateStreetEntityList = new List<StateStreetEntity>();
        public bool DIWMServiceManagedViaMDA = false;

        public string InvestmentProgram = string.Empty;
        public string InvestmentProgramCode = string.Empty;
        public bool DoItForhMe = false;
        public bool DIFNMBWA = false;
        public bool DIFNMDesktopBroker = false;
        public bool DIFNMSS = false;
        public decimal DIFMAmount = 0;
        public bool DIFMNominatePreferences = false;

        public string MinBalance = "2500";
        public string MinBalancePercentage = "0";
        public string MinTrade = "2500";
        public string MinTradePercentage = "0";

        public bool AssignedAllocatedValueHoldSell = true;
        public bool DistributedAllocatedValueHoldSell = false;
        public bool AssignedAllocatedValueExclusion = true;
        public bool DistributedAllocatedValueExclusion = false;
        public bool AssignedAllocatedValueMinTradeHolding = true;
        public bool DistributedAllocatedValueMinTradeHolding = false; 

        public List<SecuritiesExclusionEntity> SecuritiesExclusionEntityList = new List<SecuritiesExclusionEntity>();

        #endregion

        #region Investment Preference  Data

        public string HandlingDueToHoldOrSell()
        {
            if (AssignedAllocatedValueHoldSell)
                return "Assign allocated value to cash (Default)";
            else
                return "Distribute allocated value to other investments";
        }

        public string HandlingDueToExclusion()
        {
            if (AssignedAllocatedValueExclusion)
                return "Assign allocated value to cash (Default)";
            else
                return "Distribute allocated value to other investments";
        }

        public string HandlingDueToMinTradeOrHoldingConstraint()
        {
            if (AssignedAllocatedValueMinTradeHolding)
                return "Assign allocated value to cash (Default)";
            else
                return "Distribute allocated value to other investments";
        }

        #endregion 

        #region Platforms

        public string NameOfPlatformsBT = string.Empty;
        public string InvestorNoBT = string.Empty;
        public string AdviserNoBT = string.Empty;
        public string ProviderNoBT = string.Empty;

        public string NameOfPlatformsEclipse = string.Empty;
        public string InvestorNoEclipse = string.Empty;
        public string AdviserNoEclipse = string.Empty;
        public string ProviderNoEclipse = string.Empty;

        public bool MaqCMALinkBWA = false;
        public string MaqCMADIYDIWM = string.Empty;
        public string MaqCMABSB = string.Empty;
        public string MaqCMAAccountNo = string.Empty;
        public string MaqCMAAccountName = string.Empty;

        #endregion

        #region Sigs

        public List<SignatoryEntity> SignatoryEntityList = new List<SignatoryEntity>();

        #endregion


        #region BWA Options

        public bool DIYPhoneAccess = false;
        public bool DIYOnlineAccess = false;
        public bool DIYDebitCard = false;
        public bool DIYChequeBook = false;
        public bool DIYDepositBook = false;

        public string DIYMannerOfOperation = string.Empty;

        public bool DIWMPhoneAccess = false;
        public bool DIWMOnlineAccess = false;
        public bool DIWMDebitCard = false;
        public bool DIWMChequeBook = false;
        public bool DIWMDepositBook = false;

        public string DIWMMannerOfOperation = string.Empty;

        #endregion

        #region Chess Transfer Of Secrities

        public bool TransferListedSecuritiesToDesktopBroker = false;
        public string ChessDIYDIWM = string.Empty;

        public string ChessRegisteredAccountName = string.Empty;
        public string ChessAccountDesignation = string.Empty;

        public string ChessAddressLine1 = string.Empty;
        public string ChessAddressLine2 = string.Empty;
        public string ChessSuburb = string.Empty;
        public string ChessState = string.Empty;
        public string ChessPostCode = string.Empty;
        public string ChessCountry = string.Empty;


        public bool IssuedSponsor = false;
        public bool ExactNameMatch = false;
        public string ExactNameMatchAmount = string.Empty;
        public bool ExactNameNoMatch = false;
        public string ExactNameNoMatchAmount = string.Empty;
        public bool BrokerSponsored = false;

        public string ExistingBroker1 = string.Empty;
        public string ClientNo1 = string.Empty;
        public string ClientHin1 = string.Empty;

        public string ExistingBroker2 = string.Empty;
        public string ClientNo2 = string.Empty;
        public string ClientHin2 = string.Empty;

        #endregion

        #region Checklist

        public bool ProofIDCheckListItem = false;
        public bool TrustDeedChecListItem = false;
        public bool LetterOfAccountantChecListItem = false;
        public bool BusinessNameRegoChecListItem = false;
        public bool IssueSponsorChessSponsorChecListItem = false;
        public bool IssueSponsorChessSponsorHoldsingStatements = false;
        public bool OffMarketTransferChecListItem = false;
        public bool ChangeOFClientChecListItem = false;
        public bool BrokerToBrokerChecListItem = false;
        public bool ProofOfAddressChecListItem = false;

        #endregion

        #region Addresses

        public string MialingAddressLine1 = string.Empty;
        public string MialingAddressLine2 = string.Empty;
        public string MialingSuburb = string.Empty;
        public string MialingState = string.Empty;
        public string MialingPostCode = string.Empty;
        public string MialingCountry = string.Empty;

        public string DupMialingAddressLine1 = string.Empty;
        public string DupMialingAddressLine2 = string.Empty;
        public string DupMialingSuburb = string.Empty;
        public string DupMialingState = string.Empty;
        public string DupMialingPostCode = string.Empty;
        public string DupMialingCountry = string.Empty;

        public string RegAddressLine1 = string.Empty;
        public string RegAddressLine2 = string.Empty;
        public string RegSuburb = string.Empty;
        public string RegState = string.Empty;
        public string RegPostCode = string.Empty;
        public string RegCountry = string.Empty;

        public string PrincipalAddressLine1 = string.Empty;
        public string PrincipalAddressLine2 = string.Empty;
        public string PrincipalSuburb = string.Empty;
        public string PrincipalState = string.Empty;
        public string PrincipalPostCode = string.Empty;
        public string PrincipalCountry = string.Empty;

        #endregion

        #region IncludedUsers
      public  List<Guid> IncludedUsers {get;set;}


        #endregion


        public UMAFormEntity()
        {
            SignatoryEntity signatoryEntity1 = new SignatoryEntity();
            signatoryEntity1.SigNo = 1;
            this.SignatoryEntityList.Add(signatoryEntity1);

            SignatoryEntity signatoryEntity2 = new SignatoryEntity();
            signatoryEntity2.SigNo = 2;
            this.SignatoryEntityList.Add(signatoryEntity2);

            SignatoryEntity signatoryEntity3 = new SignatoryEntity();
            signatoryEntity3.SigNo = 3;
            this.SignatoryEntityList.Add(signatoryEntity3);

            SignatoryEntity signatoryEntity4 = new SignatoryEntity();
            signatoryEntity4.SigNo = 4;
            this.SignatoryEntityList.Add(signatoryEntity4);


            IncludedUsers = new List<Guid>(); 

        }

        public void ExtractData(UMAFormsListingDS data)
        {
            DataRow formRow = data.Tables[UMAFormsListingDS.ACCOUNTSLISTTABLE].NewRow();
            formRow[UMAFormsListingDS.APPLICATIONNO] = this.accountNO.ToUpper();
            formRow[UMAFormsListingDS.ACCOUNTNAME] = this.accountName;
            formRow[UMAFormsListingDS.ACCOUNTTYPE] = this.accountType;
            formRow[UMAFormsListingDS.CLIENTCID] = this.ClientCID;

            formRow[UMAFormsListingDS.SALESDIFM] = this.DIFMAmount;
            formRow[UMAFormsListingDS.SALESDIWM] = this.DIWMAmount;
            formRow[UMAFormsListingDS.SALESDIY] = this.DIYAmount;

            if(this.ClientID == null)
                formRow[UMAFormsListingDS.CLIENTID] = string.Empty;
            else
                formRow[UMAFormsListingDS.CLIENTID] = this.ClientID;

            formRow[UMAFormsListingDS.MODIFIEDBY] = this.modifiedByUser;
            formRow[UMAFormsListingDS.MODIFIEDDATE] = this.lastModifiedDateForm;
            formRow[UMAFormsListingDS.CREATEDBY] = this.userName;
            formRow[UMAFormsListingDS.CREATEDDATE] = this.CreatedOnDateForm;

            string status = "Intialised";

            if (this.UMAFormStatus == UMAFormStatus.Finalised)
                status = "Finalised";
            if (this.UMAFormStatus == UMAFormStatus.Intialised)
                status = "Intialised";
            if (this.UMAFormStatus == UMAFormStatus.UnderReview)
                status = "Under Review";

            formRow[UMAFormsListingDS.FORMSTATUS] = status;

            data.Tables[UMAFormsListingDS.ACCOUNTSLISTTABLE].Rows.Add(formRow);
            if(IncludedUsers!=null)
            foreach (var usercid in IncludedUsers)
            {
                data.userTable.AddRow(this.accountNO.ToUpper(), usercid);
            }
        }

        public DataTable Validate()
        {
            #region Account

            if (this.accountType == string.Empty)
            {
                OneFormVaidationEntity validEntity = new OneFormVaidationEntity();
                validEntity.ErrorType = OneFormVaidationEntity.ERROR;
                validEntity.ShortDesc = OneFormVaidationEntity.MANDATORY;
                validEntity.Description = "Account Type field is compulsory. Please go to “1. Account” tab and select Account Type. ";

                this.OneFormVaidationEntityList.Add(validEntity);
            }

            #endregion 

            #region TFN ABN

            if (this.accountType != string.Empty)
            {
                bool HasTFN = false;

                if (this.TFNSig1 != string.Empty || this.TFNSig2 != string.Empty || this.TFNSig3 != string.Empty || this.TFNSig4 != string.Empty)
                    HasTFN = true;

                bool HasABNorACN = false;

                if (this.ACN != string.Empty || this.ABNCompany != string.Empty)
                    HasABNorACN = true;

                if (!HasTFN)
                {
                    OneFormVaidationEntity validEntityTFN = new OneFormVaidationEntity();
                    validEntityTFN.ErrorType = OneFormVaidationEntity.ERROR;
                    validEntityTFN.ShortDesc = OneFormVaidationEntity.MANDATORY;
                    validEntityTFN.Description = "TFN for Signatories / Investors is compulsory. Please go to “3. TFN / ABN” tab and fill in details.";
                    this.OneFormVaidationEntityList.Add(validEntityTFN);
                }

                if (this.accountType == "Trust - Company Trustee" || this.accountType == "Trust - Joint Trustee" || this.accountType == "Trust - Individual Trustee" ||
                   this.accountType == "Self Managed Super Fund (SMSF) - Company Trustee" || this.accountType == "Self Managed Super Fund (SMSF) - Individual Trustee" || this.accountType == "Self Managed Super Fund (SMSF) - Joint Trustee")
                {
                    if (this.TFNTrustFund == string.Empty)
                    {
                        OneFormVaidationEntity validEntity = new OneFormVaidationEntity();
                        validEntity.ErrorType = OneFormVaidationEntity.ERROR;
                        validEntity.ShortDesc = OneFormVaidationEntity.MANDATORY;
                        validEntity.Description = "TFN (Trust/Superfund) is compulsory. Please go to “3. TFN / ABN ” tab and fill in details.";
                        this.OneFormVaidationEntityList.Add(validEntity);
                    }
                }

                if (this.accountType == "Trust - Company Trustee" || this.accountType == "Self Managed Super Fund (SMSF) - Company Trustee")

                {
                    if (!HasABNorACN)
                    {
                        OneFormVaidationEntity validEntity = new OneFormVaidationEntity();
                        validEntity.ErrorType = OneFormVaidationEntity.ERROR;
                        validEntity.ShortDesc = OneFormVaidationEntity.MANDATORY;
                        validEntity.Description = "ABN (Company/Superfund) OR ACN (Company) is compulsory. Please go to “3. TFN / ABN” tab and fill in details.";
                        this.OneFormVaidationEntityList.Add(validEntity);
                    }
                }
            }

            #endregion 

            #region Investor & Service 

            if (this.GetINvestorStatus() == string.Empty)
            {
                OneFormVaidationEntity validEntity = new OneFormVaidationEntity();
                validEntity.ErrorType = OneFormVaidationEntity.ERROR;
                validEntity.ShortDesc = OneFormVaidationEntity.MANDATORY;
                validEntity.Description = "Investor Status field is compulsory. Please go to “4. Authority to Act” tab and select Account Type. ";

                this.OneFormVaidationEntityList.Add(validEntity);
            }

            #endregion 

            #region Services

            if (!this.DoItForhMe && !this.DoItYourSelf && !this.DoItWithMe)
            {
                OneFormVaidationEntity validEntity = new OneFormVaidationEntity();
                validEntity.ErrorType = OneFormVaidationEntity.ERROR;
                validEntity.ShortDesc = OneFormVaidationEntity.MANDATORY;
                validEntity.Description = "Services Type field is compulsory. Please go to “5. DIY / DIWM” and / or “6. DIFM” tab and select Services Type. ";

                this.OneFormVaidationEntityList.Add(validEntity);
            }

            if (this.DoItForhMe)
            {
                if (InvestmentProgram == string.Empty)
                {
                    OneFormVaidationEntity validEntity = new OneFormVaidationEntity();
                    validEntity.ErrorType = OneFormVaidationEntity.ERROR;
                    validEntity.ShortDesc = OneFormVaidationEntity.MANDATORY;
                    validEntity.Description = "Investment Program field is compulsory. Please go to “6. DIFM” tab and select Investment Program. ";

                    this.OneFormVaidationEntityList.Add(validEntity);
                }
            }

            #endregion 

            #region Signatories

            SignatoryEntity signatoryEntity1 = SignatoryEntityList.Where(sig => sig.SigNo == 1).FirstOrDefault();
            SignatoryEntity signatoryEntity2 = SignatoryEntityList.Where(sig => sig.SigNo == 2).FirstOrDefault();
            SignatoryEntity signatoryEntity3 = SignatoryEntityList.Where(sig => sig.SigNo == 3).FirstOrDefault();
            SignatoryEntity signatoryEntity4 = SignatoryEntityList.Where(sig => sig.SigNo == 4).FirstOrDefault();

            #region SIG 1

            if (signatoryEntity1.FamilyName == string.Empty)
            {
                OneFormVaidationEntity validEntity = new OneFormVaidationEntity();
                validEntity.ErrorType = OneFormVaidationEntity.ERROR;
                validEntity.ShortDesc = OneFormVaidationEntity.MANDATORY;
                validEntity.Description = "FamilyName is compulsory. Please go to “8. Sig1” tab and select Account Type. ";

                this.OneFormVaidationEntityList.Add(validEntity);
            }

            if (signatoryEntity1.GivenName == string.Empty)
            {
                OneFormVaidationEntity validEntity = new OneFormVaidationEntity();
                validEntity.ErrorType = OneFormVaidationEntity.ERROR;
                validEntity.ShortDesc = OneFormVaidationEntity.MANDATORY;
                validEntity.Description = "GivenName field is compulsory. Please go to “8. Sig1” tab and select Account Type. ";

                this.OneFormVaidationEntityList.Add(validEntity);
            }

            if (signatoryEntity1.DateOfBirth == string.Empty)
            {
                OneFormVaidationEntity validEntity = new OneFormVaidationEntity();
                validEntity.ErrorType = OneFormVaidationEntity.ERROR;
                validEntity.ShortDesc = OneFormVaidationEntity.MANDATORY;
                validEntity.Description = "DateOfBirth field is compulsory. Please go to “8. Sig1” tab and select Account Type. ";

                this.OneFormVaidationEntityList.Add(validEntity);
            }

            if (signatoryEntity1.ContactPH == string.Empty)
            {
                OneFormVaidationEntity validEntity = new OneFormVaidationEntity();
                validEntity.ErrorType = OneFormVaidationEntity.ERROR;
                validEntity.ShortDesc = OneFormVaidationEntity.MANDATORY;
                validEntity.Description = "ContactPH field is compulsory. Please go to “8. Sig1” tab and select Account Type. ";

                this.OneFormVaidationEntityList.Add(validEntity);
            }

            if (signatoryEntity1.Email == string.Empty)
            {
                OneFormVaidationEntity validEntity = new OneFormVaidationEntity();
                validEntity.ErrorType = OneFormVaidationEntity.ERROR;
                validEntity.ShortDesc = OneFormVaidationEntity.MANDATORY;
                validEntity.Description = "Email field is compulsory. Please go to “8. Sig1” tab and select Account Type. ";

                this.OneFormVaidationEntityList.Add(validEntity);
            }

            #endregion 

            #region SIG 2

            if (Sig2Required)
            {

                if (signatoryEntity2.FamilyName == string.Empty)
                {
                    OneFormVaidationEntity validEntity = new OneFormVaidationEntity();
                    validEntity.ErrorType = OneFormVaidationEntity.ERROR;
                    validEntity.ShortDesc = OneFormVaidationEntity.MANDATORY;
                    validEntity.Description = "FamilyName is compulsory. Please go to “9. Sig2” tab and select Account Type. ";

                    this.OneFormVaidationEntityList.Add(validEntity);
                }

                if (signatoryEntity2.GivenName == string.Empty)
                {
                    OneFormVaidationEntity validEntity = new OneFormVaidationEntity();
                    validEntity.ErrorType = OneFormVaidationEntity.ERROR;
                    validEntity.ShortDesc = OneFormVaidationEntity.MANDATORY;
                    validEntity.Description = "GivenName field is compulsory. Please go to “9. Sig2” tab and select Account Type. ";

                    this.OneFormVaidationEntityList.Add(validEntity);
                }

                if (signatoryEntity2.DateOfBirth == string.Empty)
                {
                    OneFormVaidationEntity validEntity = new OneFormVaidationEntity();
                    validEntity.ErrorType = OneFormVaidationEntity.ERROR;
                    validEntity.ShortDesc = OneFormVaidationEntity.MANDATORY;
                    validEntity.Description = "DateOfBirth field is compulsory. Please go to “9. Sig2” tab and select Account Type. ";

                    this.OneFormVaidationEntityList.Add(validEntity);
                }

                if (signatoryEntity2.ContactPH == string.Empty)
                {
                    OneFormVaidationEntity validEntity = new OneFormVaidationEntity();
                    validEntity.ErrorType = OneFormVaidationEntity.ERROR;
                    validEntity.ShortDesc = OneFormVaidationEntity.MANDATORY;
                    validEntity.Description = "ContactPH field is compulsory. Please go to “9. Sig2” tab and select Account Type. ";

                    this.OneFormVaidationEntityList.Add(validEntity);
                }

                if (signatoryEntity2.Email == string.Empty)
                {
                    OneFormVaidationEntity validEntity = new OneFormVaidationEntity();
                    validEntity.ErrorType = OneFormVaidationEntity.ERROR;
                    validEntity.ShortDesc = OneFormVaidationEntity.MANDATORY;
                    validEntity.Description = "Email field is compulsory. Please go to “9. Sig2” tab and select Account Type. ";

                    this.OneFormVaidationEntityList.Add(validEntity);
                }
            }

            #endregion 

            #region SIG 3

            if (Sig3Required)
            {

                if (signatoryEntity3.FamilyName == string.Empty)
                {
                    OneFormVaidationEntity validEntity = new OneFormVaidationEntity();
                    validEntity.ErrorType = OneFormVaidationEntity.ERROR;
                    validEntity.ShortDesc = OneFormVaidationEntity.MANDATORY;
                    validEntity.Description = "FamilyName is compulsory. Please go to “10. Sig3” tab and select Account Type. ";

                    this.OneFormVaidationEntityList.Add(validEntity);
                }

                if (signatoryEntity3.GivenName == string.Empty)
                {
                    OneFormVaidationEntity validEntity = new OneFormVaidationEntity();
                    validEntity.ErrorType = OneFormVaidationEntity.ERROR;
                    validEntity.ShortDesc = OneFormVaidationEntity.MANDATORY;
                    validEntity.Description = "GivenName field is compulsory. Please go to “10. Sig3” tab and select Account Type. ";

                    this.OneFormVaidationEntityList.Add(validEntity);
                }

                if (signatoryEntity3.DateOfBirth == string.Empty)
                {
                    OneFormVaidationEntity validEntity = new OneFormVaidationEntity();
                    validEntity.ErrorType = OneFormVaidationEntity.ERROR;
                    validEntity.ShortDesc = OneFormVaidationEntity.MANDATORY;
                    validEntity.Description = "DateOfBirth field is compulsory. Please go to “10. Sig3” tab and select Account Type. ";

                    this.OneFormVaidationEntityList.Add(validEntity);
                }

                if (signatoryEntity3.ContactPH == string.Empty)
                {
                    OneFormVaidationEntity validEntity = new OneFormVaidationEntity();
                    validEntity.ErrorType = OneFormVaidationEntity.ERROR;
                    validEntity.ShortDesc = OneFormVaidationEntity.MANDATORY;
                    validEntity.Description = "ContactPH field is compulsory. Please go to “10. Sig3” tab and select Account Type. ";

                    this.OneFormVaidationEntityList.Add(validEntity);
                }

                if (signatoryEntity3.Email == string.Empty)
                {
                    OneFormVaidationEntity validEntity = new OneFormVaidationEntity();
                    validEntity.ErrorType = OneFormVaidationEntity.ERROR;
                    validEntity.ShortDesc = OneFormVaidationEntity.MANDATORY;
                    validEntity.Description = "Email field is compulsory. Please go to “10. Sig3” tab and select Account Type. ";

                    this.OneFormVaidationEntityList.Add(validEntity);
                }
            }

            #endregion 

            #region SIG 4

            if (Sig3Required)
            {

                if (signatoryEntity4.FamilyName == string.Empty)
                {
                    OneFormVaidationEntity validEntity = new OneFormVaidationEntity();
                    validEntity.ErrorType = OneFormVaidationEntity.ERROR;
                    validEntity.ShortDesc = OneFormVaidationEntity.MANDATORY;
                    validEntity.Description = "FamilyName is compulsory. Please go to “11. Sig4” tab and select Account Type. ";

                    this.OneFormVaidationEntityList.Add(validEntity);
                }

                if (signatoryEntity4.GivenName == string.Empty)
                {
                    OneFormVaidationEntity validEntity = new OneFormVaidationEntity();
                    validEntity.ErrorType = OneFormVaidationEntity.ERROR;
                    validEntity.ShortDesc = OneFormVaidationEntity.MANDATORY;
                    validEntity.Description = "GivenName field is compulsory. Please go to “11. Sig4” tab and select Account Type. ";

                    this.OneFormVaidationEntityList.Add(validEntity);
                }

                if (signatoryEntity4.DateOfBirth == string.Empty)
                {
                    OneFormVaidationEntity validEntity = new OneFormVaidationEntity();
                    validEntity.ErrorType = OneFormVaidationEntity.ERROR;
                    validEntity.ShortDesc = OneFormVaidationEntity.MANDATORY;
                    validEntity.Description = "DateOfBirth field is compulsory. Please go to “11. Sig4” tab and select Account Type. ";

                    this.OneFormVaidationEntityList.Add(validEntity);
                }

                if (signatoryEntity4.ContactPH == string.Empty)
                {
                    OneFormVaidationEntity validEntity = new OneFormVaidationEntity();
                    validEntity.ErrorType = OneFormVaidationEntity.ERROR;
                    validEntity.ShortDesc = OneFormVaidationEntity.MANDATORY;
                    validEntity.Description = "ContactPH field is compulsory. Please go to “11. Sig4” tab and select Account Type. ";

                    this.OneFormVaidationEntityList.Add(validEntity);
                }

                if (signatoryEntity4.Email == string.Empty)
                {
                    OneFormVaidationEntity validEntity = new OneFormVaidationEntity();
                    validEntity.ErrorType = OneFormVaidationEntity.ERROR;
                    validEntity.ShortDesc = OneFormVaidationEntity.MANDATORY;
                    validEntity.Description = "Email field is compulsory. Please go to “11. Sig4” tab and select Account Type. ";

                    this.OneFormVaidationEntityList.Add(validEntity);
                }
            }

            #endregion 

            #endregion

            DataTable dt = new DataTable();
            dt.Columns.Add("ErrorType");
            dt.Columns.Add("ShortDesc");
            dt.Columns.Add("Description");

            foreach (OneFormVaidationEntity oneFormVaidationEntity in OneFormVaidationEntityList)
            {
                DataRow row =  dt.NewRow();
                row["ErrorType"] = oneFormVaidationEntity.ErrorType;
                row["ShortDesc"] = oneFormVaidationEntity.ShortDesc;
                row["Description"] = oneFormVaidationEntity.Description;

                dt.Rows.Add(row);
            }

            return dt;
        }

        public string GetINvestorStatus()
        {
            string investorStatus = string.Empty;

            if (InvestorStatusWholesaleInvestor)
                return "Wholesale Investor";
            else if (InvestorStatusSophisticatedInvestor)
                return "Sophisticated Investor";

            else if (InvestorStatusProfessionalInvestor)
                return "Professional Investor";
            else if (InvestorStatusRetailInvestor)
                return "Retail Investor";

            return investorStatus;

        }

    }

    [Serializable]
    public class SignatoryEntity
    {
        public int SigNo = 0;
        public Guid IndividualInstanceID = Guid.Empty; 
        public string Title = string.Empty;
        public string FamilyName = string.Empty;
        public string GivenName = string.Empty;
        public string MiddleName = string.Empty;
        public string OtherName = string.Empty;
        public string DateOfBirth;
        public string CorporateTitel = string.Empty;

        public string ResiAddressLine1 = string.Empty;
        public string ResiAddressLine2 = string.Empty;
        public string ResiSuburb = string.Empty;
        public string ResiState = string.Empty;
        public string ResiPostCode = string.Empty;
        public string ResiCountry = string.Empty;

        public string MailAddressLine1 = string.Empty;
        public string MailAddressLine2 = string.Empty;
        public string MailSuburb = string.Empty;
        public string MailState = string.Empty;
        public string MailPostCode = string.Empty;
        public string MailCountry = string.Empty;

        public string Occupation = string.Empty;
        public string Employer = string.Empty;
        public string MainCountryOFAddressSig = string.Empty;
        public string ContactPH = string.Empty;
        public string AlternativePH = string.Empty;
        public string MobilePH = string.Empty;
        public string Fax = string.Empty;
        public string Email = string.Empty;

        public bool DriverLicSig = false;
        public bool PassportSig = false;

        public string DocumentNoPassport = string.Empty;
        public string DocumentNoLic = string.Empty;

        public string LicDocumentIssuer = string.Empty;
        public bool LicCertifiedCopyAttached = false;
        public string LicIssueDate;
        public string LicExpiryDate;

        public string PassportDocumentIssuer = string.Empty;
        public bool PassportCertifiedCopyAttached = false;
        public string PassportIssueDate;
        public string PassportExpiryDate;

        public string FullName { get
        {
            
            return GivenName + " " + MiddleName + " " + FamilyName;
        }
        }

    }

    [Serializable]
    public class SecuritiesExclusionEntity
    {
        public SecuritiesExclusionEntity(string code, string description, string investmentAction, Guid ID)
        {
            this.InvestmentAction = investmentAction;
            this.Code = code;
            this.Description = description;
            this.ID = ID;
        }
        public string Code { get; set; }
        public string InvestmentAction { get; set; }
        public string Description { get; set; }
        public Guid ID { get; set; }
    }

    [Serializable]
    public class StateStreetEntity
    {
        public StateStreetEntity(string code, string description)
        {
            this.Code = code;
            this.Description = description;
        }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
