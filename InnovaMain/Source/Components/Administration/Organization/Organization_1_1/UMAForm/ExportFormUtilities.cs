﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.FormGenerators;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.Exceptions;
namespace Oritax.TaxSimp.CM.Organization.UMAForm
{
    public class ExportFormUtilities
    {
        static public string AssemblyDirectory
        {
            get
            {
                string codeBase = System.Reflection.Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                var dir = System.IO.Path.GetDirectoryName(path);
                return dir.Substring(0, dir.LastIndexOf("Source\\"));
            }
        }
        public static string GetFilledForms(UMAFormEntity formOrg, ICMBroker broker, ServiceTypes serviceType)
        {
            string bytes = string.Empty;
           
            SerializableDictionary<string, byte[]> output = new SerializableDictionary<string, byte[]>();
            List<DefaultFormsFile> inputfiles = new List<DefaultFormsFile>();
            
            string AssemblyDirectoryName = AssemblyDirectory+@"Source\Assembly";
            string docFolder = AssemblyDirectoryName + @"\Documents\Forms\PDF\";
            if (!string.IsNullOrEmpty(formOrg.accountType))
            {
                docFolder += GetClientFolder(formOrg.accountType);
            }
            else
            { throw new TransactionNotRunningException("Account Type cannot be null"); }

            docFolder += "\\";

            FileProcessCommadnType type = FileProcessCommadnType.NormarlTagged;
            string commtwoFileName = "BWAMI CIP.pdf";


            foreach (var file in GetFiles(docFolder, "*.PDF|*.Doc|*.docx"))
            {
                if (!Path.GetFileName(file).StartsWith("BWA"))
                {
                    continue;
                }

                if (Path.GetFileName(file) == commtwoFileName)
                {
                    type = FileProcessCommadnType.PartialTagged;
                }
                else
                {

                    if (Path.GetExtension(file).ToUpper() == ".PDF")
                        type = FileProcessCommadnType.NormarlTagged;
                    else
                    {
                        type = FileProcessCommadnType.DocFile;
                    }

                }

                inputfiles.Add(new DefaultFormsFile() { FileName = Path.GetFileName(file), FilePath = file, MapID = Guid.NewGuid(), Type = type });

            }

            foreach (var inputfile in inputfiles)
            {
                byte[] original = File.ReadAllBytes(inputfile.FilePath);
            var fileinfo=    new FileInfo(inputfile.FileName);
            IFormGenerator generator = FormGeneratorFactory.GetInstance(fileinfo.Extension.ToGeneratorType());
                Dictionary<string, string> tagedData = new Dictionary<string, string>();

                ITagFile tags = FileTagFactory.GetFileTagCommand(fileinfo.Name.Replace(fileinfo.Extension,""));
               
               if (tags != null)
               {
                   tags.Broker = broker;
                   if (tags is ISignatoyFile)
                   {
                       //do it for each signatory
                       var signatoryfile = (tags as ISignatoyFile);
                       int i = 1;
                       bool required = false;
                       foreach (var signtry in formOrg.SignatoryEntityList)
                       {
                           required = false;
                           if(i==1)
                           {
                               required = true;
                           }
                           else if(i==2)
                           {
                               required = formOrg.Sig2Required;
                           }
                           else if(i==3)
                           {
                               required = formOrg.Sig3Required;
                           }
                           else if(i==4)
                           {
                               required = formOrg.Sig4Required;
                           }
                           

                           if(required)
                           {
                               signatoryfile.Signatory = signtry;
                               byte[] content = generator.Generate(original, tags.GenerateTags(formOrg, serviceType));
                               output.Add(
                                   fileinfo.Name.Replace(fileinfo.Extension, "") + "_" + (i++) + fileinfo.Extension,
                                   content);
                           }
                       }

                   }
                   else
                   {
                   byte[] content = generator.Generate(original, tags.GenerateTags(formOrg, serviceType));
                   output.Add(inputfile.FileName, content);
                   }


                  
               }

            }

            bytes = Convert.ToBase64String(Zipper.CreateZip(output));

            return bytes;
        }
        private static string[] GetFiles(string sourceFolder, string filters)
        {
            return filters.Split('|').SelectMany(filter => System.IO.Directory.GetFiles(sourceFolder, filter)).ToArray();
        }
        private static string GetClientFolder(string accountType)
        {
            string docFolder = "";
            switch (accountType)
            {
                case "Partnership":
                    break;
                case "Company - Private":
                    docFolder += "ClientCorporationPrivate";
                    break;
                case "Company - Public":
                    docFolder += "ClientCorporationPublic";
                    break;
                case "Individual":
                case "Joint":
                    docFolder += "ClientIndividual";
                    break;
                case "EclipseSuper":
                    docFolder += "ClientEClipseSuper";
                    break;
                case "Trust - Corporate Trustee":
                    docFolder += "ClientOtherTrustsCorporate";
                    break;
                case "Trust - Individual Trustee":
                case "Trust - Joint Trustee":
                    docFolder += "ClientOtherTrustsIndividual";
                    break;
                case "Self Managed Super Fund (SMSF) - Corporate Trustee":
                    docFolder += "ClientSMSFCorporateTrustee";
                    break;
                case "Self Managed Super Fund (SMSF) - Individual Trustee":
                case "Self Managed Super Fund (SMSF) - Joint Trustee":
                    docFolder += "ClientSMSFIndividualTrustee";
                    break;
                case "SoleTrader":
                    docFolder += "ClientSoleTrader";
                    break;
                default:
                    throw new ArgumentException("Unknown Account Type");
            }
            return docFolder;

        }
    }
}
