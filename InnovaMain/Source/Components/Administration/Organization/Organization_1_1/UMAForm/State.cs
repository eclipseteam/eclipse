﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.CM.Organization.UMAForm
{
    public static class State
    {

        public static string GetStateCode(string Fullname)
        {
            var dic = new Dictionary<string, string>();
            dic.Add("Australian Capital Territory", "ACT");
            dic.Add("New South Wales", "NSW");
            dic.Add("Northern Territory", "NT");
            dic.Add("Queensland", "QLD");
            dic.Add("South Australia", "SA");
            dic.Add("Tasmania", "TAS");
            dic.Add("Victoria", "VIC");
            dic.Add("Western Australia", "WA");
            if (dic.ContainsKey(Fullname))
            {


                return dic[Fullname].ToString();
            }
            else
            {
                return Fullname;
            }

        }

    }
    
}
