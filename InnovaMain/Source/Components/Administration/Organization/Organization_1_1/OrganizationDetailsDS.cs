using System;
using System.Data;

namespace Oritax.TaxSimp.CM.Organization
{
	/// <summary>
	/// DSCalculationModule is a DataSet that contains all of the persistent data for a CM instance.
	/// </summary>
	[Serializable] 
	public class OrganizationDetailsDS : DataSet
	{
		private string organizationName;
		private string breadCrumb;
		private Guid currentScenario;

		public OrganizationDetailsDS()
		{
			organizationName="";
			breadCrumb="";
			currentScenario=Guid.Empty;
		}

		public string OrganizationName{get{return organizationName;}set{organizationName=value;}}
		public string BreadCrumb{get{return breadCrumb;}set{breadCrumb=value;}}
		public Guid CurrentScenario{get{return currentScenario;}set{currentScenario=value;}}
	}
}

