﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Oritax.TaxSimp.Calculation;

namespace Oritax.TaxSimp.CM.Organization
{
    public class OrganisationDetailsDS : DataSet
    {
        public Guid OrganisationId { get; set; }
        public string OrganisationName { get; set; }
    }
}
