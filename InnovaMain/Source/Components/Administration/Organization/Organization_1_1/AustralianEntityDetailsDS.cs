﻿using System;
using System.Data;
using Oritax.TaxSimp.CM.Group;
using Oritax.TaxSimp.BusinessStructureUtilities;

namespace Oritax.TaxSimp.CM.Organization
{
    /// <summary>
    /// DSCalculationModule is a DataSet that contains all of the persistent data for a CM instance.
    /// </summary>
    [Serializable]
    public class AustralianEntityDetailsDS : EntityDetailsDS
    {
        public const String SAP_TABLE = "SAP_TABLE";
        private TaxStatusType m_objTaxStatus;

        public AustralianEntityDetailsDS()
            : base()
        {
            m_objTaxStatus = TaxStatusType.Default;
            DataTable table = new DataTable(SAP_TABLE);
            this.Tables.Add(table);
        }

        public TaxStatusType TaxStatus
        {
            get
            {
                return this.m_objTaxStatus;
            }
            set
            {
                this.m_objTaxStatus = value;
            }
        }
    }
}

