﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using System.Security.Cryptography;
using System.Text;
using Oritax.TaxSimp.CM.Organization.HelperClass;

namespace Oritax.TaxSimp.CM.Organization.Model
{
    [Serializable]
    public class AddressEntity
    {
        public string Addressline1 { get; set; }
        public string Addressline2 { get; set; }
        public string Suburb { get; set; }
        public string State { get; set; }
        public string PostCode { get; set; }
        public string Country { get; set; }

        public AddressEntity()
        {
            Addressline1 = string.Empty;
            Addressline2 = string.Empty;
            Suburb = string.Empty;
            State = string.Empty;
            PostCode = string.Empty;
            Country = string.Empty;
        }
    }

    #region Hide
    //#region Page1
    //public class ClientDetail
    //{
    //    //Client
    //    public string AccountName { get; set; }
    //    public string TrusteeName { get; set; }
    //    public AccountType AccountType { get; set; }
    //    public TrusteeType TrusteeType { get; set; }
    //    public string OthersDetails { get; set; }
    //    //Adviser       
    //    public string UniqueID { get; set; }
    //    public string BusinessName { get; set; }
    //    public string AdviserName { get; set; }
    //    public decimal UpfrontFee { get; set; }
    //    public decimal OngoingFeeAmount { get; set; }
    //    public decimal OngoingFeePercent { get; set; }
    //    public List<TieredFee> TieredFeeCollection { get; set; }

    //    public ClientDetail()
    //    {
    //        AccountName =
    //        TrusteeName =
    //        OthersDetails =
    //        UniqueID =
    //        BusinessName =
    //        AdviserName = string.Empty;
    //        UpfrontFee =
    //        OngoingFeeAmount =
    //        OngoingFeePercent = 0;
    //    }
    //}

    //public class TieredFee
    //{
    //    public decimal FromAmount { get; set; }
    //    public decimal ToAmount { get; set; }
    //    public decimal PAAmount { get; set; }

    //    public TieredFee()
    //    {
    //        FromAmount =
    //        ToAmount =
    //        PAAmount = 0;
    //    }
    //}
    //#endregion

    //#region Page2
    //public class TaxDetail
    //{
    //    public string Client1TFN { get; set; }
    //    public string Client2TFN { get; set; }
    //    public string ClientABN { get; set; }
    //    public bool AppliedForABN { get; set; }
    //    public string CountryOfResidence { get; set; }

    //    public TaxDetail()
    //    {
    //        Client1TFN =
    //        Client2TFN =
    //        ClientABN =
    //        CountryOfResidence = string.Empty;
    //        AppliedForABN = false;
    //    }
    //}
    //#endregion

    //#region Page3
    //public class ServiceDetail
    //{
    //    public InvestorType InvestorType { get; set; }
    //    public bool IsSophisticatedInvestor { get; set; }
    //    public bool WithNetAssetsOf { get; set; }
    //    public bool WithGrossIncomeOf { get; set; }
    //    public bool WithCertificateFrom { get; set; }
    //    public bool WithDoItYourself { get; set; }
    //    public decimal DoItYourselfEstAmount { get; set; }
    //    public AccountOptions DoItYourselfAccount { get; set; }
    //    public bool WithDoItWithMe { get; set; }
    //    public decimal DoItWithMeEstAmount { get; set; }
    //    public AccountOptions DoItWithMeAccount { get; set; }
    //    public bool WithDoItForMe { get; set; }
    //    public decimal DoItForMeEstAmount { get; set; }
    //    public AccountOptions DoItForMeAccount { get; set; }
    //    public InvestmentProgram InvestmentProgram { get; set; }
    //    public string ProgramCode { get; set; }
    //    public InvestmentPreference NominateInvestmentPreference { get; set; }
    //    public bool WithLimitedPowerOfAttorney { get; set; }

    //    public ServiceDetail()
    //    {
    //        IsSophisticatedInvestor =
    //        WithNetAssetsOf =
    //        WithGrossIncomeOf =
    //        WithCertificateFrom =
    //        WithLimitedPowerOfAttorney = false;
    //        ProgramCode = string.Empty;
    //    }
    //}

    //public class AccountOptions
    //{
    //    public bool WithCMA { get; set; }
    //    public bool WithDesktopBroker { get; set; }
    //    public bool WithFIIG { get; set; }
    //    public bool WithMMA { get; set; }
    //    public bool WithStateStreet { get; set; }

    //    public AccountOptions()
    //    {
    //        WithCMA =
    //        WithDesktopBroker =
    //        WithFIIG =
    //        WithMMA =
    //        WithStateStreet = false;
    //    }
    //}

    //public class InvestmentPreference
    //{
    //    public HandlingOfUnmanagedInvestments HandlingOfUnmanagedInvestments { get; set; }
    //    public HandlingType HandlingDueToHoldOrSell { get; set; }
    //    public HandlingType HandlingDueToExclusion { get; set; }
    //    public HandlingType HandlingDueToMinTradeOrHoldingConstraint { get; set; }
    //    public decimal MinBalancePerInvestment { get; set; }
    //    public decimal MinBalancePercentage { get; set; }
    //    public decimal MinTradeAmount { get; set; }
    //    public decimal MinTradePercentage { get; set; }
    //    public List<InvestmentExclusions> InvestmentExclusionCollection { get; set; }

    //    public InvestmentPreference()
    //    {
    //        MinBalancePerInvestment =
    //        MinBalancePercentage =
    //        MinTradeAmount =
    //        MinTradePercentage = 0;
    //    }
    //}

    //public class InvestmentExclusions
    //{
    //    public string ASXCode { get; set; }
    //    public string CompanyName { get; set; }
    //    public ExclusionType ExclusionType { get; set; }

    //    public InvestmentExclusions()
    //    {
    //        ASXCode = string.Empty;
    //        CompanyName = string.Empty;
    //    }
    //}
    //#endregion

    //#region Page4
    //public class LinkedAccountDetail
    //{
    //    public List<Platform> PlatformCollection { get; set; }
    //    public string BSB { get; set; }
    //    public string AccountNO { get; set; }
    //    public string AccountName { get; set; }

    //    public LinkedAccountDetail()
    //    {
    //        BSB =
    //        AccountNO =
    //        AccountName = string.Empty;
    //    }
    //}

    //public class Platform
    //{
    //    public string PlatformName { get; set; }
    //    public string InvestorNo { get; set; }
    //    public string AdviserNo { get; set; }
    //    public string ProviderName { get; set; }

    //    public Platform()
    //    {
    //        PlatformName =
    //        InvestorNo =
    //        AdviserNo =
    //        ProviderName = string.Empty;
    //    }
    //}
    //#endregion

    //#region Page5and6
    //public class InvestorTrusteeSignatoryDetail
    //{
    //    public List<InvestorTrusteeSignatory> InvestorTrusteeSignatoryCollection { get; set; }

    //    public InvestorTrusteeSignatoryDetail()
    //    {
    //    }
    //}

    //public class InvestorTrusteeSignatory
    //{
    //    public HonorificType Honorific { get; set; }
    //    public string OtherHonorific { get; set; }
    //    public string FamilyName { get; set; }
    //    public string GivenName { get; set; }
    //    public DateTime DateOfBirth { get; set; }
    //    public CorporateTrusteeTitleType CorporateTrusteeTitle { get; set; }
    //    public string OtherCorporateTrusteeTitle { get; set; }
    //    public AddressEntity ResidentialAddress { get; set; }
    //    public bool WithDifferentMailingAddress { get; set; }
    //    public string Occupation { get; set; }
    //    public string Employer { get; set; }
    //    public string MainCountryOfResidence { get; set; }
    //    public string ContactPh { get; set; }
    //    public string AlternatePh { get; set; }
    //    public string FaxNo { get; set; }
    //    public string Email { get; set; }
    //    public bool WithDriverLicense { get; set; }
    //    public bool WithPassport { get; set; }
    //    public string DocumentIssuer { get; set; }
    //    public DateTime IssueDate { get; set; }
    //    public DateTime ExpiryDate { get; set; }
    //    public string DocumentNumber { get; set; }
    //    public bool WithCertifiedCopy { get; set; }
    //    public bool WithOneInvestorTrusteeSignatory { get; set; }

    //    public InvestorTrusteeSignatory()
    //    {
    //        FamilyName =
    //        GivenName =
    //        OtherHonorific =
    //        OtherCorporateTrusteeTitle =
    //        Occupation =
    //        Employer =
    //        MainCountryOfResidence =
    //        ContactPh =
    //        AlternatePh =
    //        FaxNo =
    //        Email =
    //        DocumentIssuer =
    //        DocumentNumber = String.Empty;
    //        DateOfBirth =
    //        IssueDate =
    //        ExpiryDate = DateTime.Now;
    //        ResidentialAddress = null;
    //        WithCertifiedCopy =
    //        WithDifferentMailingAddress =
    //        WithDriverLicense =
    //        WithPassport =
    //        WithOneInvestorTrusteeSignatory = false;
    //    }
    //}
    //#endregion

    //#region Page7
    //public class AccountCorrespondenceDetail
    //{
    //    public AddressEntity RegisteredAddress { get; set; }
    //    public AddressEntity MailingAddress { get; set; }
    //    public AddressEntity DuplicateStatementMailingAddress { get; set; }
    //    public bool SameasRegisteredAddress { get; set; }

    //    public AccountCorrespondenceDetail()
    //    {
    //        SameasRegisteredAddress = false;
    //    }
    //}
    //#endregion

    //#region Page8
    //public class ServiceOptionsDetail
    //{
    //    public ServiceOptions DIYOption { get; set; }
    //    public ServiceOptions DIWMOption { get; set; }

    //    public ServiceOptionsDetail()
    //    {
    //    }
    //}

    //public class ServiceOptions
    //{
    //    public bool WithPhoneAccess { get; set; }
    //    public bool WithOnlineAccess { get; set; }
    //    public bool WithDebitCard { get; set; }
    //    public bool WithChequeBook { get; set; }
    //    public bool WithDepositBook { get; set; }
    //    public OperateOption OperateOption { get; set; }

    //    public ServiceOptions()
    //    {
    //        WithPhoneAccess =
    //        WithOnlineAccess =
    //        WithDebitCard =
    //        WithChequeBook =
    //        WithDepositBook = false;
    //    }
    //}
    //#endregion

    //#region Page9
    //public class TransfeOfSharesDetail
    //{
    //    public bool TransferExistingListedSecurities { get; set; }
    //    public TransferToService TransferToService { get; set; }
    //    public string RegisteredAccountName { get; set; }
    //    public string AccountDesignation { get; set; }
    //    public AddressEntity RegisteredAddress { get; set; }
    //    public bool IssuerSponsored { get; set; }
    //    public bool IssuerAddressMatch { get; set; }
    //    public decimal IssuerCompanyHoldings { get; set; }
    //    public bool IssuerAddressNotMatch { get; set; }
    //    public decimal IssuerDifferentCompanyHoldings { get; set; }
    //    public bool BrokerSponsored { get; set; }
    //    public List<ExistingBrokerDetail> ExistingBrokerDetail { get; set; }

    //    public TransfeOfSharesDetail()
    //    {
    //        TransferExistingListedSecurities =
    //        IssuerSponsored =
    //        IssuerAddressMatch =
    //        IssuerAddressNotMatch =
    //        BrokerSponsored = false;
    //        RegisteredAccountName =
    //        AccountDesignation = string.Empty;
    //        IssuerCompanyHoldings =
    //        IssuerDifferentCompanyHoldings = 0;
    //    }
    //}

    //public class ExistingBrokerDetail
    //{
    //    public string ExistingBrokerName { get; set; }
    //    public string ExistingBrokerPID { get; set; }
    //    public string ClientHIN { get; set; }

    //    public ExistingBrokerDetail()
    //    {
    //        ExistingBrokerName =
    //        ExistingBrokerPID =
    //        ClientHIN = string.Empty;
    //    }
    //}
    //#endregion

    //#region Page10
    //public class DocumentDetail
    //{
    //    public bool ProofOfID { get; set; }
    //    public bool TrustDeed { get; set; }
    //    public bool LetterFromYourAccountant { get; set; }
    //    public bool CertificateOfRegistration { get; set; }
    //    public bool BrokerToBRokerTransfer { get; set; }
    //    public bool IssuerSponsoredHoldingsToChessSponsorshipConversion { get; set; }
    //    public bool IssuerSponsoredHoldingStatements { get; set; }
    //    public bool OffMarketTransfer { get; set; }
    //    public bool ChangeOfClientDetailsForm { get; set; }

    //    public DocumentDetail()
    //    {
    //        ProofOfID =
    //        TrustDeed =
    //        LetterFromYourAccountant =
    //        CertificateOfRegistration =
    //        BrokerToBRokerTransfer =
    //        IssuerSponsoredHoldingsToChessSponsorshipConversion =
    //        IssuerSponsoredHoldingStatements =
    //        OffMarketTransfer =
    //        ChangeOfClientDetailsForm = false;
    //    }
    //}
    //#endregion
    #endregion
}