﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oritax.TaxSimp.CM.Organization.Model.Entities;
using Oritax.TaxSimp.CM.Organization.Model.Interface;
using Oritax.TaxSimp.CM.Organization.Model.Organization;

namespace Oritax.TaxSimp.CM.Organization.Model
{
    [Serializable]
    public class AppFormModel : IAppFormModel
    {
        
        public AppFormEntity GetClientInfo()
        {
            return new AppFormEntity()
            {
                ApplicationNo = "ApplicationNo",
                ClientName = "ClientName",
                Email = "ClientEmail"
            };
        }
    }
}