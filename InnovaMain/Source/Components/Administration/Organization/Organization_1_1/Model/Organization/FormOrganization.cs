﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oritax.TaxSimp.CM.Organization.Model.Entities;
using Oritax.TaxSimp.CM.Organization.Data;
using System.Data;

namespace Oritax.TaxSimp.CM.Organization.Model.Organization
{
    [Serializable]
    public class FormOrganization
    {
        public string AdviserID { get; set; }
        public Guid GUID { get; set; }
        public String SessionID { get; set; }
        public bool Status { get; set; }
        public AppFormEntity appFormEntity { get; set; }
        public FirstFormEntity firstFormEntity { get; set; }
        public SecondFormEntity secondFormEntity { get; set; }
        public ThirdFormEntity thirdFormEntity { get; set; }
        public FourthFormEntity fourthFormEntity { get; set; }
        public FifthFormEntity fifthFormEntity { get; set; }
        public SixthFormEntity sixthFormEntity { get; set; }
        public SeventhFormEntity seventhFormEntity { get; set; }
        public EighthFormEntity eighthFormEntity { get; set; }
        public NinethFormEntity ninethFormEntity { get; set; }
        public TenthFormEntity tenthFormEntity { get; set; }

        public void ExtractData(UMAFormsListingDS data)
        {
            DataRow formRow = data.Tables[UMAFormsListingDS.ACCOUNTSLISTTABLE].NewRow();
            formRow[UMAFormsListingDS.APPLICATIONNO] = appFormEntity.ApplicationNo.ToUpper();
            formRow[UMAFormsListingDS.ACCOUNTNAME] = appFormEntity.ClientName;
            formRow[UMAFormsListingDS.ACCOUNTTYPE] = firstFormEntity.AccountType + " " + firstFormEntity.SMSFType + firstFormEntity.TrusteeType;

            data.Tables[UMAFormsListingDS.ACCOUNTSLISTTABLE].Rows.Add(formRow);
        }
    }
}