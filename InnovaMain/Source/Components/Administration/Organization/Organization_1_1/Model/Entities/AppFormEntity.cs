﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oritax.TaxSimp.CM.Organization.Model.Entities
{
    [Serializable]
    public class AppFormEntity
    {
        public string ClientName { get; set; }
        public string Email { get; set; }
        public string ApplicationNo { get; set; }
    }
}