﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oritax.TaxSimp.CM.Organization.HelperClass;

namespace Oritax.TaxSimp.CM.Organization.Model.Entities
{
    [Serializable]
    public class FifthFormEntity
    {
        #region Page5
        public string Honorific { get; set; }
        public string OtherHonorific { get; set; }
        public string FamilyName { get; set; }
        public string GivenName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string CorporateTrusteeTitle { get; set; }
        public string OtherCorporateTrusteeTitle { get; set; }
        public AddressEntity ResidentialAddress { get; set; }
        public bool WithDifferentMailingAddress { get; set; }
        public string Occupation { get; set; }
        public string Employer { get; set; }
        public string MainCountryOfResidence { get; set; }
        public string ContactPh { get; set; }
        public string AlternatePh { get; set; }
        public string FaxNo { get; set; }
        public string Email { get; set; }
        public bool WithDriverLicense { get; set; }
        public bool WithPassport { get; set; }
        public string DocumentIssuer { get; set; }
        public DateTime IssueDate { get; set; }
        public DateTime ExpiryDate { get; set; }
        public string DocumentNumber { get; set; }
        public bool WithCertifiedCopy { get; set; }



        //public class AddressEntity
        //{
        //    public string Addressline1 { get; set; }
        //    public string Addressline2 { get; set; }
        //    public string Suburb { get; set; }
        //    public string State { get; set; }
        //    public string PostCode { get; set; }
        //    public string Country { get; set; }

        //    public AddressEntity()
        //    {
        //        Addressline1 = string.Empty;
        //        Addressline2 = string.Empty;
        //        Suburb = string.Empty;
        //        State = string.Empty;
        //        PostCode = string.Empty;
        //        Country = string.Empty;
        //    }
        //}
        //public class InvestorTrusteeSignatory
        //{
        //    public HonorificType Honorific { get; set; }
        //    public string OtherHonorific { get; set; }
        //    public string FamilyName { get; set; }
        //    public string GivenName { get; set; }
        //    public DateTime DateOfBirth { get; set; }
        //    public CorporateTrusteeTitleType CorporateTrusteeTitle { get; set; }
        //    public string OtherCorporateTrusteeTitle { get; set; }
        //    public AddressEntity ResidentialAddress { get; set; }
        //    public bool WithDifferentMailingAddress { get; set; }
        //    public string Occupation { get; set; }
        //    public string Employer { get; set; }
        //    public string MainCountryOfResidence { get; set; }
        //    public string ContactPh { get; set; }
        //    public string AlternatePh { get; set; }
        //    public string FaxNo { get; set; }
        //    public string Email { get; set; }
        //    public bool WithDriverLicense { get; set; }
        //    public bool WithPassport { get; set; }
        //    public string DocumentIssuer { get; set; }
        //    public DateTime IssueDate { get; set; }
        //    public DateTime ExpiryDate { get; set; }
        //    public string DocumentNumber { get; set; }
        //    public bool WithCertifiedCopy { get; set; }
        //    public bool WithOneInvestorTrusteeSignatory { get; set; }

        //    public InvestorTrusteeSignatory()
        //    {
        //        FamilyName =
        //        GivenName =
        //        OtherHonorific =
        //        OtherCorporateTrusteeTitle =
        //        Occupation =
        //        Employer =
        //        MainCountryOfResidence =
        //        ContactPh =
        //        AlternatePh =
        //        FaxNo =
        //        Email =
        //        DocumentIssuer =
        //        DocumentNumber = String.Empty;
        //        DateOfBirth =
        //        IssueDate =
        //        ExpiryDate = DateTime.Now;
        //        ResidentialAddress = null;
        //        WithCertifiedCopy =
        //        WithDifferentMailingAddress =
        //        WithDriverLicense =
        //        WithPassport =
        //        WithOneInvestorTrusteeSignatory = false;
        //    }
        //}
        #endregion
    }
}