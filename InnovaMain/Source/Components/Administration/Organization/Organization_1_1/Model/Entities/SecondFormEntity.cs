﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oritax.TaxSimp.CM.Organization.HelperClass;

namespace Oritax.TaxSimp.CM.Organization.Model.Entities
{
    [Serializable]
    public class SecondFormEntity
    {
        public string Client1TFN { get; set; }
        public string Client2TFN { get; set; }
        public string Client3TFN { get; set; }
        public string Client4TFN { get; set; }
        public string TrustSuperTFN { get; set; }
        public string ClientARBN { get; set; }
        public string ClientACN { get; set; }
        public bool AppliedForWitholdingTaxNo { get; set; }
        public string CountryOfResidence { get; set; }
        
    }
}