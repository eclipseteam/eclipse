﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oritax.TaxSimp.CM.Organization.HelperClass;

namespace Oritax.TaxSimp.CM.Organization.Model.Entities
{
    [Serializable]
    public class EighthFormEntity
    {
        #region page8
        public ServiceOptions DIYOption { get; set; }
        public ServiceOptions DIWMOption { get; set; }

        public EighthFormEntity()
        {
            DIYOption = null;
            DIWMOption = null;
        }
        [Serializable]
        public class ServiceOptions
        {
            public bool WithPhoneAccess { get; set; }
            public bool WithOnlineAccess { get; set; }
            public bool WithDebitCard { get; set; }
            public bool WithChequeBook { get; set; }
            public bool WithDepositBook { get; set; }
            public string OperateOption { get; set; }

            public ServiceOptions()
            {
                WithPhoneAccess =
                WithOnlineAccess =
                WithDebitCard =
                WithChequeBook =
                WithDepositBook = false;
            }
        }
        #endregion 
    }
}