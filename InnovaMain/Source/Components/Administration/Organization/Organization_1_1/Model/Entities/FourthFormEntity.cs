﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oritax.TaxSimp.CM.Organization.Model.Entities
{
    [Serializable]
    public class FourthFormEntity
    {
        #region Page4
        public List<Platform> PlatformCollection { get; set; }
        public string BSB { get; set; }
        public string AccountNo { get; set; }
        public string AccountName { get; set; }

        public FourthFormEntity()
        {
            BSB =
            AccountNo =
            AccountName = string.Empty;
        }

        [Serializable]
        public class Platform
        {
            public bool WithEclipseSuper { get; set; }
            public bool WithBTWrap { get; set; }
            public bool WithMarginLender { get; set; }
            public string InvestorNo { get; set; }
            public string AdviserNo { get; set; }
            public string ProviderName { get; set; }

            public Platform()
            {
                WithEclipseSuper =
                WithBTWrap =
                WithMarginLender = false;
                InvestorNo =
                AdviserNo =
                ProviderName = string.Empty;
            }
        }
        #endregion
    }
}