﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oritax.TaxSimp.CM.Organization.HelperClass;

namespace Oritax.TaxSimp.CM.Organization.Model.Entities
{
    [Serializable]
    public class FirstFormEntity
    {
        #region Page1
        //Client
        public string AccountName { get; set; }
        public string TrusteeName { get; set; }
        public string AccountType { get; set; }
        public string SMSFType { get; set; }
        public string TrusteeType { get; set; }
        public string OthersDetails { get; set; }
        //Adviser       
        public string UniqueID { get; set; }
        public string BusinessName { get; set; }
        public string AdviserName { get; set; }
        public decimal UpfrontFee { get; set; }
        public decimal OngoingFeeAmount { get; set; }
        public decimal OngoingFeePercent { get; set; }
        public decimal TieredFeeFrom1 { get; set; }
        public decimal TieredFeeTo1 { get; set; }
        public decimal TieredFeePA1 { get; set; }
        public decimal TieredFeeFrom2 { get; set; }
        public decimal TieredFeeTo2 { get; set; }
        public decimal TieredFeePA2 { get; set; }
        public decimal TieredFeeFrom3 { get; set; }
        public decimal TieredFeeTo3 { get; set; }
        public decimal TieredFeePA3 { get; set; }

        public FirstFormEntity()
        {
            AccountName =
            TrusteeName =
            OthersDetails =
            UniqueID =
            BusinessName =
            AdviserName = string.Empty;
            UpfrontFee =
            OngoingFeeAmount =
            OngoingFeePercent =
            TieredFeeFrom1 =
            TieredFeeTo1 =
            TieredFeePA1 =
            TieredFeeFrom2 =
            TieredFeeTo2 =
            TieredFeePA2 =
            TieredFeeFrom2 =
            TieredFeeTo3 =
            TieredFeePA3 = 0;
        }

        #endregion
    }
}