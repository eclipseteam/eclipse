﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oritax.TaxSimp.CM.Organization.Model.Entities
{
    [Serializable]
    public class SeventhFormEntity
    {
        #region page7
        public AddressEntity RegisteredAddress { get; set; }
        public AddressEntity MailingAddress { get; set; }
        public AddressEntity DuplicateStatementMailingAddress { get; set; }
        public bool SameasRegisteredAddress { get; set; }

        public SeventhFormEntity()
        {
            SameasRegisteredAddress = false;
        }
        #endregion
    }
}