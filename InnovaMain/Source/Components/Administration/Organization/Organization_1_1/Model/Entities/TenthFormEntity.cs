﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oritax.TaxSimp.CM.Organization.Model.Entities
{
    [Serializable]
    public class TenthFormEntity
    {
        #region page10
        public bool ProofOfID { get; set; }
        public bool TrustDeed { get; set; }
        public bool LetterFromYourAccountant { get; set; }
        public bool CertificateOfRegistration { get; set; }
        public bool BrokerToBRokerTransfer { get; set; }
        public bool IssuerSponsoredHoldingsToChessSponsorshipConversion { get; set; }
        public bool IssuerSponsoredHoldingStatements { get; set; }
        public bool OffMarketTransfer { get; set; }
        public bool ChangeOfClientDetailsForm { get; set; }

        public TenthFormEntity()
        {
            ProofOfID =
            TrustDeed =
            LetterFromYourAccountant =
            CertificateOfRegistration =
            BrokerToBRokerTransfer =
            IssuerSponsoredHoldingsToChessSponsorshipConversion =
            IssuerSponsoredHoldingStatements =
            OffMarketTransfer =
            ChangeOfClientDetailsForm = false;
        }
        #endregion
    }
}