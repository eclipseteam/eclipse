﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oritax.TaxSimp.CM.Organization.HelperClass;

namespace Oritax.TaxSimp.CM.Organization.Model.Entities
{
    [Serializable]
    public class ThirdFormEntity
    {
        #region Page3
        public string InvestorType { get; set; }
        //public bool IsSophisticatedInvestor { get; set; }
        public bool WithNetAssetsOf { get; set; }
        public bool WithGrossIncomeOf { get; set; }
        public bool WithCertificateFrom { get; set; }
        public bool WithDoItYourself { get; set; }
        public decimal DoItYourselfEstAmount { get; set; }
        public AccountOptions DoItYourselfAccount { get; set; }
        public bool WithDoItWithMe { get; set; }
        public decimal DoItWithMeEstAmount { get; set; }
        public AccountOptions DoItWithMeAccount { get; set; }
        public bool WithDoItForMe { get; set; }
        public decimal DoItForMeEstAmount { get; set; }
        public AccountOptions DoItForMeAccount { get; set; }
        public string InvestmentProgram { get; set; }
        public string ProgramCode { get; set; }
        public bool WithNominateInvestmentPreference { get; set; }
        public string HandlingOfUnmanagedInvestments { get; set; }
        public string HandlingDueToHoldOrSell { get; set; }
        public string HandlingDueToExclusion { get; set; }
        public string HandlingDueToMinTradeOrHoldingConstraint { get; set; }
        public decimal MinBalancePerInvestment { get; set; }
        public decimal MinBalancePercentage { get; set; }
        public decimal MinTradeAmount { get; set; }
        public decimal MinTradePercentage { get; set; }
        public bool WithMinBalancePerInvestment { get; set; }
        public bool WithMinTradePerInvestment { get; set; } 
        public List<InvestmentExclusions> ExclusionList { get; set; }
        public bool WithLimitedPowerOfAttorney { get; set; }
        public bool WithNOLimitedPowerOfAttorney { get; set; }
        public bool WithLimitedPowerOfAttorneyAMM { get; set; }

        public ThirdFormEntity()
        {
            //IsSophisticatedInvestor =
            WithNetAssetsOf =
            WithGrossIncomeOf =
            WithCertificateFrom =
            WithLimitedPowerOfAttorney =
            WithLimitedPowerOfAttorneyAMM =
            WithNOLimitedPowerOfAttorney = 
            WithNominateInvestmentPreference = false;
            ProgramCode = string.Empty;
        }
        [Serializable]
        public class AccountOptions
        {
            public bool WithCMA { get; set; }
            public bool WithDesktopBroker { get; set; }
            public bool WithFIIG { get; set; }
            public bool WithMMA { get; set; }
            public bool WithStateStreet { get; set; }

            public AccountOptions()
            {
                WithCMA =
                WithDesktopBroker =
                WithFIIG =
                WithMMA =
                WithStateStreet = false;
            }
        }
        [Serializable]
        public class InvestmentExclusions
        {
            public string ASXCode { get; set; }
            public string CompanyName { get; set; }
            public string ExclusionType { get; set; }

            public InvestmentExclusions()
            {
                ASXCode = string.Empty;
                CompanyName = string.Empty;
                ExclusionType = string.Empty;
            }
        }
        #endregion
    }
}