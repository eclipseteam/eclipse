﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oritax.TaxSimp.CM.Organization.HelperClass;

namespace Oritax.TaxSimp.CM.Organization.Model.Entities
{
    [Serializable]
    public class NinethFormEntity
    {
        #region page9
        public bool TransferExistingListedSecurities { get; set; }
        public string TransferToService { get; set; }
        public string RegisteredAccountName { get; set; }
        public string AccountDesignation { get; set; }
        public AddressEntity RegisteredAddress { get; set; }
        public bool IssuerSponsored { get; set; }
        public bool IssuerAddressMatch { get; set; }
        public decimal IssuerCompanyHoldings { get; set; }
        public bool IssuerAddressNotMatch { get; set; }
        public decimal IssuerCompanyHoldingsChange { get; set; }
        public bool BrokerSponsored { get; set; }
        public List<ExistingBrokerDetail> ExistingBroker { get; set; }

        public NinethFormEntity()
        {
            TransferExistingListedSecurities =
            IssuerSponsored =
            IssuerAddressMatch =
            IssuerAddressNotMatch =
            BrokerSponsored = false;
            RegisteredAccountName =
            AccountDesignation = string.Empty;
            IssuerCompanyHoldings =
            IssuerCompanyHoldingsChange = 0;
        }
        [Serializable]
        public class ExistingBrokerDetail
        {
            public string ExistingBrokerName { get; set; }
            public string ExistingBrokerPID { get; set; }
            public string ClientHIN { get; set; }

            public ExistingBrokerDetail()
            {
                ExistingBrokerName =
                ExistingBrokerPID =
                ClientHIN = string.Empty;
            }
        }
        #endregion
    }
}