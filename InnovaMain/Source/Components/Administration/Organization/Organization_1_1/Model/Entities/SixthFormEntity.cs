﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oritax.TaxSimp.CM.Organization.HelperClass;
using Oritax.TaxSimp.CM.Organization.Model.Entities;

namespace Oritax.TaxSimp.CM.Organization.Model.Entities
{
    [Serializable]
    public class SixthFormEntity
    {
        public bool WithOneInvestorTrusteeSignatory { get; set; }
        public List<Signatory> SignatoryCollection { get; set; }
        [Serializable]
        public class Signatory
        {
            #region Page6
            public string Honorific { get; set;} 
            public string OtherHonorific { get; set; }
            public string FamilyName { get; set; }
            public string GivenName { get; set; }
            public DateTime DateOfBirth { get; set; }
            public string CorporateTrusteeTitle { get; set; }
            public string OtherCorporateTrusteeTitle { get; set; }
            public AddressEntity ResidentialAddress { get; set; }
            public AddressEntity MailAddress { get; set; }
            public bool WithDifferentMailingAddress { get; set; }
            public string Occupation { get; set; }
            public string Employer { get; set; }
            public string MainCountryOfResidence { get; set; }
            public string ContactPh { get; set; }
            public string AlternatePh { get; set; }
            public string FaxNo { get; set; }
            public string Email { get; set; }
            public bool WithDriverLicense { get; set; }
            public bool WithPassport { get; set; }
            public string DocumentIssuer { get; set; }
            public DateTime IssuanceDate { get; set; } 
            public DateTime ExpirationDate { get; set; }
            public string DocumentNumber { get; set; }
            public bool WithCertifiedCopy { get; set; }
            #endregion
        }
    }
}