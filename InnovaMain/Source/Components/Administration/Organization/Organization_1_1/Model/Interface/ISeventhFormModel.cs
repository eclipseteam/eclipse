﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oritax.TaxSimp.CM.Organization.Model.Entities;

namespace Oritax.TaxSimp.CM.Organization.Model.Interface
{
    
    public interface ISeventhFormModel
    {
        SeventhFormEntity BindSeventhForm();
    }
}