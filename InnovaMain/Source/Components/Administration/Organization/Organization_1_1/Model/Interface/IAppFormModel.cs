﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.CM.Organization.Model.Entities;
using Oritax.TaxSimp.CM.Organization.Model.Organization;

namespace Oritax.TaxSimp.CM.Organization.Model.Interface
{
    
    public interface IAppFormModel
    {
        AppFormEntity GetClientInfo();
    }
}
