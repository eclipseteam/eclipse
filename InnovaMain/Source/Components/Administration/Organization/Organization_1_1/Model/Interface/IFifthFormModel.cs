﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oritax.TaxSimp.CM.Organization.Model.Entities;
using Oritax.TaxSimp.CM.Organization.Model.Models;

namespace Oritax.TaxSimp.CM.Organization.Model.Interface
{
    
    public interface IFifthFormModel
    {
        FifthFormEntity BindFifthForm();
    }
}