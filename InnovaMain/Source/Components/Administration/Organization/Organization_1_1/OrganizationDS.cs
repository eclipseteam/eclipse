using System;
using System.Data;
using System.Data.SqlClient;

using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.CM.Organization
{
	/// <summary>
	/// Summary description for OrganizationDS.
	/// </summary>
	[Serializable]
	public class OrganizationDS :  BusinessStructureBaseDS
	{
		public const String ORGANIZATION_TABLE				= "ORGANIZATION";
		public const String SECURITYENABLED_FIELD			= "SECURITYENABLED";
		public const string DENYNEWLOGINS_FIELD				= "DENYNEWLOGINS";
		public const string LOGINADMINMESSAGE_FIELD			= "LOGINADMINMESSAGE";
		public const string SYSTEMWIDEMESSAGE_FIELD			= "SYSTEMWIDEMESSAGE";
		public const string LOGOFFALLUSERS_FIELD			= "LOGOFFALLUSERS";
		public const string LICENCEMESSAGE_FIELD			= "LICENCEMESSAGE";

		public const String TARGETS_TABLE				= "TARGETS_TABLE";
		public const String TARGETS_DESCRIPTION_FIELD	= "Description";
		public const String TARGETS_COMPONENTID_FIELD	= "ComponentID";
		public const String TARGETS_PRIMARYKEY_FIELD	= "Key";
		public const String TARGETS_LOCKED_FIELD		= "Locked";
		
		public OrganizationDS() : base()
		{
			DataTable dt;

			dt = new DataTable( ORGANIZATION_TABLE );
			dt.Columns.Add(SECURITYENABLED_FIELD, typeof( int ) );
			dt.Columns.Add(DENYNEWLOGINS_FIELD, typeof(bool));
			dt.Columns.Add(LOGINADMINMESSAGE_FIELD, typeof(string));
			dt.Columns.Add(SYSTEMWIDEMESSAGE_FIELD, typeof(string));
			dt.Columns.Add(LOGOFFALLUSERS_FIELD, typeof(bool));
			dt.Columns.Add(LICENCEMESSAGE_FIELD, typeof(string));
			this.Tables.Add( dt );
			BrokerManagedComponentDS.AddVersionStamp(dt,typeof(OrganizationDS));

			dt = new DataTable(TARGETS_TABLE);
			dt.Columns.Add(TARGETS_DESCRIPTION_FIELD,typeof(System.String)); 
			dt.Columns.Add(TARGETS_COMPONENTID_FIELD,typeof(System.Guid)); 
			dt.Columns.Add(TARGETS_PRIMARYKEY_FIELD,typeof(System.Guid)); 
			dt.Columns.Add(TARGETS_LOCKED_FIELD,typeof(System.Boolean)); 
			
			Tables.Add(dt);
			BrokerManagedComponentDS.AddVersionStamp(dt,typeof(OrganizationDS));
		
		}

		
		/// <summary>
		/// Convert a V1.1.0.0 dataset to a V1.1.1.1 dataset
		/// 
		/// Fortunately they are identical at this level
		/// </summary>
		/// <param name="data">The orginal untampered dataset</param>
		/// <param name="convData">A converted dataset passed in from a previous conversion function</param>
		/// <returns>A converted dataset</returns>
		public static DataSet ConvertV_1_1_0_0toV_1_1_1_1DataSet(DataSet data, DataSet convData)
		{
		
			if(convData == null)
				return null;

			//In complete contradiction to normal software development standards we DO want to use
			//Literals and not constants here. This hard codes the software to the old version.
			string fn = BrokerManagedComponentDS.GetFullName(convData.Tables["ORGANIZATION"]);
			AssemblyFullName afn = new AssemblyFullName(fn);

			//if not the appropriate verion for correction then just return back
			if(afn.Version.ToString() != "1.1.0.0")
			{
				return convData;
			}
			//the conversion is merely changing the full name to the correct version number.
			afn.Version = new Version("1.1.1.1");

			DataSet ds = new DataSet();
			DataTable dt;

			dt = new DataTable( "ORGANIZATION" );
			dt.Columns.Add( "SECURITYENABLED", typeof( int ) );
			dt.Columns.Add( "DENYNEWLOGINS", typeof( bool ) );
			dt.Columns.Add( "LOGINADMINMESSAGE", typeof( string ) );
			dt.Columns.Add( "SYSTEMWIDEMESSAGE", typeof( string ) );
			dt.Columns.Add( "LOGOFFALLUSERS", typeof( bool ) );

			ds.Tables.Add( dt );
			
			dt = new DataTable("TARGETS_TABLE");
			dt.Columns.Add("Description",typeof(System.String)); 
			dt.Columns.Add("ComponentID",typeof(System.Guid)); 
			dt.Columns.Add("Key",typeof(System.Guid)); 
			dt.Columns.Add("Locked",typeof(System.Boolean)); 
			ds.Tables.Add(dt);
				
			ds.EnforceConstraints = true;
			ds.Merge(convData, true, MissingSchemaAction.Ignore);

			ds.Tables["ORGANIZATION"].Rows[0]["DENYNEWLOGINS"] = false;
			ds.Tables["ORGANIZATION"].Rows[0]["LOGINADMINMESSAGE"] = string.Empty;
			ds.Tables["ORGANIZATION"].Rows[0]["SYSTEMWIDEMESSAGE"] = string.Empty;
			ds.Tables["ORGANIZATION"].Rows[0]["LOGOFFALLUSERS"] = false;
		
			ds.Tables["ORGANIZATION"].ExtendedProperties["InformationalVersion"] = afn.Version.ToString();
			ds.Tables["TARGETS_TABLE"].ExtendedProperties["InformationalVersion"] = afn.Version.ToString();
			
			return ds;
		}
	}
}
