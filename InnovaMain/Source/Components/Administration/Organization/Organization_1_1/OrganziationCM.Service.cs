﻿using System;
using System.Collections.Generic;
using Oritax.TaxSimp.CM.Organization.CommandsWeb;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;

namespace Oritax.TaxSimp.CM.Organization
{

    public partial class OrganizationCM 
    {

        public override string GetDataStream(int action, string data)
        {
            var command = this.GetCommand(action);
            return command == null ? string.Format("Not valid") : command.DoAction(data);
        }

        public override string OnAddDataStream(int action, string data)
        {
            var command = this.GetCommand(action);
            return command == null ? string.Format("Not valid") : command.DoAction(data);
        }

        public  DataSet RunGetCommand(int action, DataSet data)
        {
            var command = this.GetWebCommand(action);
            return command == null ? null : command.DoAction(data);
        }

        public DataSet RunAddCommand(int action, DataSet data)
        {
            var command = this.GetWebCommand(action);
            return command == null ? null : command.DoAction(data);
        }

        
        //This was added by Samee and Shamil just for test purpose.It has pros and crons.please don't uncomment this.else client creation stops specially SMSFCooperated trustee.




        //public override string OnUpdateDataStream(int action, string data)
        //{
        //try
        //{
        //    var command = this.GetCommand(action);
        //    return command == null ? string.Format("Not valid") : command.DoAction(data);
        //}catch
        //{
        //return string.Empty;
        //}
        //}
    }

}
