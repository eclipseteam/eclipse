﻿using System.Collections.Generic;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.CalculationInterface;
using System.Xml.Linq;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.CM.Organization.Data;
using System.Data;
using Oritax.TaxSimp.DataSets;
using System.Xml;
using System;
using System.Linq;
using Oritax.TaxSimp.CM.Organization.CommandsWeb;
using System.IO;

namespace Oritax.TaxSimp.CM.Organization
{
    public class AllClientCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string value)
        {
            List<XNode> nodes = new List<XNode>();
            OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
            {
                if ((bmc is IXmlData) && (bmc is IOrganizationUnit) && (bmc as IOrganizationUnit).IsInvestableClient && !(bmc as IOrganizationUnit).IsConsolCM)
                {
                    string xml = (bmc as IXmlData).GetXmlData();
                    xml.Replace("&", "&amp;");
                    XDocument document = XDocument.Parse(xml);
                    document.Root.RemoveAttributes();
                    document.Root.Name = "Client";
                    nodes.Add(document.FirstNode);
                }
            });

            XElement root = new XElement("e-Clipse_Online_Services");
            XElement element = new XElement("Clients", nodes);


            nodes = new List<XNode>();
            OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
            {
                if ((bmc is IXmlData) && (bmc is IOrganizationUnit) && ("advisor" == logical.CMTypeName.ToLower()))
                {
                    XDocument document = XDocument.Parse((bmc as IXmlData).GetXmlData());
                    document.Root.RemoveAttributes();
                    document.Root.Name = "AdviserDetail";
                    nodes.Add(document.FirstNode);
                }
            });

            XElement element2 = new XElement("Advisers", nodes);
            root.Add(element);
            root.Add(element2);
            return root.ToString();
        }
    }

    public class EclipseSuperAllAccountsBasic : OrganizationCmCommandBase
    {
        public override string DoAction(string value)
        {
            SMAWebService smaWebService = new SMAWebService();
            SMAImportProcessDS smaImportProcessDS = new SMAImportProcessDS();
            smaImportProcessDS.Tables.Clear(); 
            smaImportProcessDS.AddBasicResultTableToDataSet(); 
            smaWebService.ResultDataSet = smaImportProcessDS;

            DataTable resultTable = smaWebService.ResultDataSet.Tables[SMAImportProcessDS.RESULTTABLE].Copy();

            DataSet memberList = smaWebService.InvokeServiceMemberList(false, false);
            DataTable memberDetails = new DataTable("MemberDetails");
            DataTable outputTable = memberList.Tables["MemberSearchViewModel"].Copy(); 
            outputTable.Columns.Add("InvestmentModelTemplate");

            foreach (DataRow row in outputTable.Rows)
            {
                string clientNumber = row["ClientNumber"].ToString();
                string memberCode = row["MemberCode"].ToString();
                string customerNo = clientNumber + "-" + memberCode;
                smaWebService.ResultDataSet = new SMAImportProcessDS();
                smaWebService.InvokeServiceMembersDetails(memberCode);
                if (smaWebService.ResultDataSet.Tables["MemberViewModel"] != null)
                {
                    string model = string.Empty; 

                    if(smaWebService.ResultDataSet.Tables["MemberViewModel"].Columns.Contains("InvestmentModelTemplate"))
                        model = smaWebService.ResultDataSet.Tables["MemberViewModel"].Rows[0]["InvestmentModelTemplate"].ToString();

                    row["InvestmentModelTemplate"] = model;
                }
            }

            string result;
            using (StringWriter sw = new StringWriter())
            {
                memberDetails.WriteXml(sw);
                result = sw.ToString();
            }

            return result;
        }
    }

    public class AllModels : OrganizationCmCommandBase
    {
        public override string DoAction(string value)
        {
            var nodes = new List<XNode>();

            ModelsDS modelsDS = new ModelsDS();
            Organization.GetData(modelsDS);
            DataView entityView = new DataView(modelsDS.Tables[ModelsDS.MODELLIST]);
            entityView.Sort = ModelsDS.MODELNAME;
            DataTable entityTable = entityView.ToTable();
            XElement root = new XElement("e-Clipse_Online_Services");
            entityTable.TableName = "Model";
            DataSet clientsDS = new DataSet("Models");
            clientsDS.Tables.Add(entityTable);

            using (XmlWriter w = root.CreateWriter()) { clientsDS.WriteXml(w); }

            return root.ToString();
        }
    }

    public class ModelDetailsByID : OrganizationCmCommandBase
    {
        public override string DoAction(string value)
        {
            var nodes = new List<XNode>();

            ModelsDS modelsDS = new ModelsDS();
            modelsDS.GetDetails = true;
            modelsDS.DetailsGUID = new Guid(value);
            Organization.GetData(modelsDS);

            DataView assetEntity = new DataView(modelsDS.Tables[ModelsDS.MODELDETAILSLISTASSETS]);
            assetEntity.Sort = ModelsDS.ASSETMODELNAME;
            DataTable entityTable = assetEntity.ToTable();

            DataView proEntity = new DataView(modelsDS.Tables[ModelsDS.MODELDETAILSLISTPRO]);
            proEntity.Sort = ModelsDS.PRODESCRIPTION;
            DataTable proTable = proEntity.ToTable();
            
            XElement root = new XElement("e-Clipse_Online_Services");
            
            entityTable.TableName = "Assets";
            proTable.TableName = "Products";

            DataSet clientsDS = new DataSet("ModelDetails");
            clientsDS.Tables.Add(entityTable);
            clientsDS.Tables.Add(proTable);

            using (XmlWriter w = root.CreateWriter()) { clientsDS.WriteXml(w); }

            return root.ToString();
        }
    }

    public class AllClientsMainInfoCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string value)
        {
            var nodes = new List<XNode>();

            OrganisationListingDS organisationListingDS = new OrganisationListingDS();
            organisationListingDS.OrganisationListingOperationType = OrganisationListingOperationType.ClientsWithFUM;
            Organization.ExtractOrgnisationListingDS(organisationListingDS, "Administrator");
            DataView entityView = new DataView(organisationListingDS.Tables["Entities_Table"]);
            entityView.Sort = "ENTITYNAME_FIELD ASC";
            entityView.RowFilter = "IsClient='true'";
            DataTable entityTable = entityView.ToTable();
            entityTable.Columns.Remove("ENTITYCLID_FIELD");
            entityTable.Columns.Remove("ENTITYCSID_FIELD");
            entityTable.Columns.Remove("ENTITYCTID_FIELD");
            entityTable.Columns["ENTITYCIID_FIELD"].ColumnName = "SystemID";
            entityTable.Columns.Remove("ENTITYSCTYPE_FIELD");
            entityTable.Columns.Remove("ENTITYSCSTATUS_FIELD");
            entityTable.Columns.Remove("ENTITYSORT_FIELD");
            entityTable.Columns["ENTITYNAME_FIELD"].ColumnName = "ClientName";
           
            XElement root = new XElement("e-Clipse_Online_Services");
            entityTable.TableName = "Client";
            DataSet clientsDS = new DataSet("Clients");
            clientsDS.Tables.Add(entityTable);

            using (XmlWriter w = root.CreateWriter()) { clientsDS.WriteXml(w); }
            string xml = root.ToString();
    
            if(xml.Contains("&"))
                xml = xml.Replace("&", "&amp;");

            return xml.ToString();
        }
    }

    public class AllSecuritiesAndFunds : OrganizationCmCommandBase
    {
        public override string DoAction(string value)
        {
            var nodes = new List<XNode>();

            SecuritiesDS securitiesDS = new DataSets.SecuritiesDS();
            Organization.GetData(securitiesDS);

            //DataView entityView = new DataView(organisationListingDS.Tables["Entities_Table"]);
            //entityView.Sort = "ENTITYNAME_FIELD ASC";
            //entityView.RowFilter = "IsClient='true' AND IsDIFM='true'";
            //DataTable entityTable = entityView.ToTable();
            //entityTable.Columns.Remove("ENTITYCLID_FIELD");
            //entityTable.Columns.Remove("ENTITYCSID_FIELD");
            //entityTable.Columns.Remove("ENTITYCIID_FIELD");
            //entityTable.Columns.Remove("ENTITYCTID_FIELD");
            //entityTable.Columns.Remove("ENTITYSCTYPE_FIELD");
            //entityTable.Columns.Remove("ENTITYSCSTATUS_FIELD");
            //entityTable.Columns.Remove("ENTITYSORT_FIELD");
            //entityTable.Columns["ENTITYNAME_FIELD"].ColumnName = "ClientName";

            
            //entityTable.TableName = "Client";
            //DataSet clientsDS = new DataSet("Clients");
            //clientsDS.Tables.Add(entityTable);

            XElement root = new XElement("e-Clipse_Online_Services");
            using (XmlWriter w = root.CreateWriter()) { securitiesDS.WriteXml(w); }
            string xml = root.ToString();
           
            return xml.ToString();
        }
    }

    public class ClientFinancialsCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string value)
        {
            var nodes = new List<XNode>();


            IBrokerManagedComponent clientData = Organization.Broker.GetBMCInstance(new Guid(value));
            HoldingSummaryReportDS holdingRptDataSet = new DataSets.HoldingSummaryReportDS();
            holdingRptDataSet.EndDate = DateTime.Now;
            BankTransactionDS banktransacationDS = new BankTransactionDS();
            MISTransactionDS mISTransactionDS = new Oritax.TaxSimp.DataSets.MISTransactionDS();
            ASXTransactionDS aSXTransactionDS = new Oritax.TaxSimp.DataSets.ASXTransactionDS();
            DIVTransactionDS dIVTransactionDS = new Oritax.TaxSimp.DataSets.DIVTransactionDS();
            TDTransactionDS tdTransactionDS = new Oritax.TaxSimp.DataSets.TDTransactionDS();
            ManualTransactionDS manualTransactionDS = new Oritax.TaxSimp.DataSets.ManualTransactionDS();
            ClientDistributionsDS clientDistributionsDS = new DataSets.ClientDistributionsDS(); 

            clientData.GetData(holdingRptDataSet);
            clientData.GetData(banktransacationDS);
            clientData.GetData(clientDistributionsDS);

            if (clientDistributionsDS.Tables.Contains(HoldingRptDataSet.CLIENTSUMMARYTABLE))
            {
                clientDistributionsDS.Tables.Remove(clientDistributionsDS.Tables[HoldingRptDataSet.CLIENTSUMMARYTABLE]);
                clientDistributionsDS.Tables.Remove(clientDistributionsDS.Tables[HoldingRptDataSet.CONTACTTABLE]);
            }

            if (banktransacationDS.Tables.Contains(HoldingRptDataSet.CLIENTSUMMARYTABLE))
            {
                banktransacationDS.Tables.Remove(banktransacationDS.Tables[HoldingRptDataSet.CLIENTSUMMARYTABLE]);
                banktransacationDS.Tables.Remove(banktransacationDS.Tables[HoldingRptDataSet.CONTACTTABLE]);
            }
            clientData.GetData(mISTransactionDS);

            if (mISTransactionDS.Tables.Contains(HoldingRptDataSet.CLIENTSUMMARYTABLE))
            {
                mISTransactionDS.Tables.Remove(mISTransactionDS.Tables[HoldingRptDataSet.CLIENTSUMMARYTABLE]);
                mISTransactionDS.Tables.Remove(mISTransactionDS.Tables[HoldingRptDataSet.CONTACTTABLE]);
            }
            
            clientData.GetData(aSXTransactionDS);

            if (aSXTransactionDS.Tables.Contains(HoldingRptDataSet.CLIENTSUMMARYTABLE))
            {
                aSXTransactionDS.Tables.Remove(aSXTransactionDS.Tables[HoldingRptDataSet.CLIENTSUMMARYTABLE]);
                aSXTransactionDS.Tables.Remove(aSXTransactionDS.Tables[HoldingRptDataSet.CONTACTTABLE]);
            }
            clientData.GetData(dIVTransactionDS);

            if (dIVTransactionDS.Tables.Contains(HoldingRptDataSet.CLIENTSUMMARYTABLE))
            {
                dIVTransactionDS.Tables.Remove(dIVTransactionDS.Tables[HoldingRptDataSet.CLIENTSUMMARYTABLE]);
                dIVTransactionDS.Tables.Remove(dIVTransactionDS.Tables[HoldingRptDataSet.CONTACTTABLE]);
            }
            
            clientData.GetData(tdTransactionDS);

            if (tdTransactionDS.Tables.Contains(HoldingRptDataSet.CLIENTSUMMARYTABLE))
            {
                tdTransactionDS.Tables.Remove(tdTransactionDS.Tables[HoldingRptDataSet.CLIENTSUMMARYTABLE]);
                tdTransactionDS.Tables.Remove(tdTransactionDS.Tables[HoldingRptDataSet.CONTACTTABLE]);
            }
            
            clientData.GetData(manualTransactionDS);

            if (manualTransactionDS.Tables.Contains(HoldingRptDataSet.CLIENTSUMMARYTABLE))
            {
                manualTransactionDS.Tables.Remove(manualTransactionDS.Tables[HoldingRptDataSet.CLIENTSUMMARYTABLE]);
                manualTransactionDS.Tables.Remove(manualTransactionDS.Tables[HoldingRptDataSet.CONTACTTABLE]);
            }
            
            DataSet financialDatasets = new DataSet();
            DataTable table = new DataTable("Product");
            table.Columns.Add("Code", typeof(string));
            table.Columns.Add("Description", typeof(string));
            table.Columns.Add("Type", typeof(string));
            table.Columns.Add("CustomerNO", typeof(string));
            table.Columns.Add("AccountNO", typeof(string));
            table.Columns.Add("BSB", typeof(string));
            table.Columns.Add("ClosingUnits", typeof(decimal));
            table.Columns.Add("ClosingPrice", typeof(decimal));
            table.Columns.Add("ClosingBalance", typeof(decimal));
            table.Columns.Add("BalanceDate", typeof(DateTime));

            foreach (DataRow row in holdingRptDataSet.securitySummaryTable.Rows)
            {
                DataRow holdingRow = table.NewRow();
                holdingRow["Code"] = row[holdingRptDataSet.securitySummaryTable.SECNAME].ToString();
                holdingRow["Description"] = row[holdingRptDataSet.securitySummaryTable.DESCRIPTION].ToString();
                holdingRow["Type"] = row[holdingRptDataSet.securitySummaryTable.TYPE].ToString();

                holdingRow["CustomerNO"] = row[holdingRptDataSet.securitySummaryTable.CUSTNO].ToString();
                holdingRow["AccountNO"] = row[holdingRptDataSet.securitySummaryTable.ACCOUTNO].ToString();
                holdingRow["BSB"] = row[holdingRptDataSet.securitySummaryTable.BSB].ToString();

                holdingRow["ClosingUnits"] = 0.0000;
                holdingRow["ClosingPrice"] = 0.000000;
                holdingRow["ClosingBalance"] = 0.00;
                holdingRow["BalanceDate"] = DateTime.Now.AddDays(-1);

                if(row[holdingRptDataSet.securitySummaryTable.CLOSINGUNITS] != null && row[holdingRptDataSet.securitySummaryTable.CLOSINGUNITS] != DBNull.Value)
                    holdingRow["ClosingUnits"] = Math.Round(Convert.ToDecimal(row[holdingRptDataSet.securitySummaryTable.CLOSINGUNITS]), 4);

                if (row[holdingRptDataSet.securitySummaryTable.CLOSINGUNITPRICE] != null && row[holdingRptDataSet.securitySummaryTable.CLOSINGUNITPRICE] != DBNull.Value)
                    holdingRow["ClosingPrice"] = Math.Round(Convert.ToDecimal(row[holdingRptDataSet.securitySummaryTable.CLOSINGUNITPRICE]), 6);

                if (row[holdingRptDataSet.securitySummaryTable.CLOSINGBAL] != null && row[holdingRptDataSet.securitySummaryTable.CLOSINGBAL] != DBNull.Value)
                    holdingRow["ClosingBalance"] = Math.Round(Convert.ToDecimal(row[holdingRptDataSet.securitySummaryTable.CLOSINGBAL]), 2);
                
                table.Rows.Add(holdingRow);
            }

            financialDatasets.Tables.Add(table);
            
            //    financialDatasets.Merge(holdingRptDataSet, true, MissingSchemaAction.Add);
            
            //if (financialDatasets.Tables.Contains("HoldingSummary"))
            //{
            //    financialDatasets.Tables["HoldingSummary"].Columns.Remove("TDID");
            //    financialDatasets.Tables["HoldingSummary"].Columns.Remove("ProductID");
            //    financialDatasets.Tables["HoldingSummary"].Columns.Remove("ServiceType");
            //    financialDatasets.Tables["HoldingSummary"].Columns.Remove("Holding");
            //    financialDatasets.Tables["HoldingSummary"].Columns.Remove("Unsettled");
            //    financialDatasets.Tables["HoldingSummary"].TableName = "Product";
            //}

            if (banktransacationDS.Tables.Contains("Cash Transactions"))
            {
                banktransacationDS.Tables["Cash Transactions"].Columns.Remove("InstitutionID");
                banktransacationDS.Tables["Cash Transactions"].Columns.Remove("ServiceType");
                banktransacationDS.Tables["Cash Transactions"].Columns.Remove("DividendID");
                banktransacationDS.Tables["Cash Transactions"].Columns.Remove("BankCid");
                banktransacationDS.Tables["Cash Transactions"].Columns.Remove("SystemTransactionType");
                banktransacationDS.Tables["Cash Transactions"].Columns.Remove("Category");
                banktransacationDS.Tables["Cash Transactions"].Columns.Remove("ContractNote");
                banktransacationDS.Tables["Cash Transactions"].Columns.Remove("MaturityDate");
                banktransacationDS.Tables["Cash Transactions"].Columns.Remove("InterestRate");
                banktransacationDS.Tables["Cash Transactions"].Columns.Remove("IsContractNote");
                banktransacationDS.Tables["Cash Transactions"].Columns.Remove("InvestmentCode");
                banktransacationDS.Tables["Cash Transactions"].Columns.Remove("Status");
                banktransacationDS.Tables["Cash Transactions"].Columns.Remove("InvestmentName");
                banktransacationDS.Tables["Cash Transactions"].Columns.Remove("AdministrationSystem");
                banktransacationDS.Tables["Cash Transactions"].Columns.Remove("Amount");
                banktransacationDS.Tables["Cash Transactions"].Columns.Remove("Balance");
                banktransacationDS.Tables["Cash Transactions"].Columns.Remove("Adjustment");
                banktransacationDS.Tables["Cash Transactions"].Columns.Remove("DividendStatus");
                banktransacationDS.Tables["Cash Transactions"].Columns.Remove("AccountName");
            }



            if (clientDistributionsDS.Tables.Contains(ClientDistributionsDS.CLIENTDISTRIBUTIONTABLE))
            {
                clientDistributionsDS.Tables[ClientDistributionsDS.CLIENTDISTRIBUTIONTABLE].Columns["RecodDate_Shares"].ColumnName = "Units";
            }

            if (tdTransactionDS.Tables.Contains("TDs Transactions"))
            {
                tdTransactionDS.Tables["TDs Transactions"].Columns.Remove("DividendID");
                tdTransactionDS.Tables["TDs Transactions"].Columns.Remove("InstitutionID");
                tdTransactionDS.Tables["TDs Transactions"].Columns.Remove("InvestmentCode");
                tdTransactionDS.Tables["TDs Transactions"].Columns.Remove("Status");
                tdTransactionDS.Tables["TDs Transactions"].Columns.Remove("Currency");
                tdTransactionDS.Tables["TDs Transactions"].Columns.Remove("InvestmentName");
                tdTransactionDS.Tables["TDs Transactions"].Columns.Remove("SystemTransactionType");
                tdTransactionDS.Tables["TDs Transactions"].Columns.Remove("Category");
                tdTransactionDS.Tables["TDs Transactions"].Columns.Remove("ExternalReferenceID");
                tdTransactionDS.Tables["TDs Transactions"].Columns.Remove("AccountName");
                tdTransactionDS.Tables["TDs Transactions"].Columns.Remove("DividendStatus");
                tdTransactionDS.Tables["TDs Transactions"].Columns.Remove("AccountType");
                tdTransactionDS.Tables["TDs Transactions"].Columns.Remove("IsContractNote");
                tdTransactionDS.Tables["TDs Transactions"].Columns.Remove("ServiceType");
                tdTransactionDS.Tables["TDs Transactions"].Columns.Remove("Product");
                tdTransactionDS.Tables["TDs Transactions"].Columns.Remove("AmountTotal");
                tdTransactionDS.Tables["TDs Transactions"].Columns.Remove("BSB");
                tdTransactionDS.Tables["TDs Transactions"].Columns.Remove("BankAmount");
                tdTransactionDS.Tables["TDs Transactions"].Columns.Remove("BankAdjustment");
                tdTransactionDS.Tables["TDs Transactions"].Columns.Remove("BankTransactionDate");
                tdTransactionDS.Tables["TDs Transactions"].Columns.Remove("BankMaturityDate");
                tdTransactionDS.Tables["TDs Transactions"].Columns.Remove("BankCid");
            }

            if (aSXTransactionDS.Tables[0] != null)
            {
                aSXTransactionDS.Tables[0].Columns.Remove("ASXCID");
            }

            if (manualTransactionDS.Tables[0] != null)
            {
                manualTransactionDS.Tables[0].Columns.Remove("PriceType");
                manualTransactionDS.Tables[0].Columns.Remove("InvestmentID");
            }

            if ((dIVTransactionDS.Tables[0] != null))
            {
                dIVTransactionDS.Tables[0].Columns.Remove("InterestRate");
                dIVTransactionDS.Tables[0].Columns.Remove("InterestPaid");
                dIVTransactionDS.Tables[0].Columns.Remove("DividendType");
                dIVTransactionDS.Tables[0].Columns.Remove("Status");
                dIVTransactionDS.Tables[0].Columns.Remove("InvestmentAmount");
            }

            if (financialDatasets.Tables.Contains("ContactTable"))
                financialDatasets.Tables.Remove("ContactTable");

            financialDatasets.DataSetName = "PortfolioSummary";

            if(banktransacationDS.Tables["BankAccountList"] != null)
                banktransacationDS.Tables.Remove("BankAccountList");

            if (banktransacationDS.Tables["Cash Transactions"] != null)
            {
                banktransacationDS.Tables["Cash Transactions"].TableName = "CashTransaction";
                banktransacationDS.DataSetName = "CashTransactions";
            }
            tdTransactionDS.DataSetName = "TermDepositTransactions";
            
            if(tdTransactionDS.Tables["TDs Transactions"] != null)
                tdTransactionDS.Tables["TDs Transactions"].TableName = "TermDepositTransaction";
            
            tdTransactionDS.Tables.Remove("PRODUCTLIST");
            tdTransactionDS.Tables.Remove("TDAccountList");

            dIVTransactionDS.DataSetName = "DividendIncomeTransactions";
            if(dIVTransactionDS.Tables[0] != null)
                dIVTransactionDS.Tables[0].TableName = "DividendIncomeTransaction";

            clientDistributionsDS.DataSetName = "DistributionIncomeTransactions";
            if (clientDistributionsDS.Tables[ClientDistributionsDS.CLIENTDISTRIBUTIONTABLE] != null)
                clientDistributionsDS.Tables[ClientDistributionsDS.CLIENTDISTRIBUTIONTABLE].TableName = "DistributionIncomeTransaction";

             manualTransactionDS.DataSetName = "ManualAssetsTransactions";
            if(manualTransactionDS.Tables[0] != null)
                manualTransactionDS.Tables[0].TableName = "ManualAssetsTransaction";

            aSXTransactionDS.DataSetName = "DesktopBrokerTransactions";
            if(aSXTransactionDS.Tables[0] != null)
                aSXTransactionDS.Tables[0].TableName = "DesktopBrokerTransaction";

            mISTransactionDS.DataSetName = "ManagedFundTransactions";
            if (mISTransactionDS.Tables[0] != null)
            {
                mISTransactionDS.Tables[0].TableName = "ManagedFundTransaction";
                mISTransactionDS.Tables[0].Columns.Remove("MISCID");
                mISTransactionDS.Tables[0].Columns.Remove("CLIENTID");
                mISTransactionDS.Tables[0].Columns.Remove("PriceType");
                mISTransactionDS.Tables[0].Columns.Remove("Status");
            }
            var root = new XElement("e-Clipse_Online_Services");
           
            using (XmlWriter w = root.CreateWriter()) {
                DataSet clientSummary = new DataSet("AccountInformation");
                holdingRptDataSet.Tables[HoldingRptDataSet.CLIENTSUMMARYTABLE].Columns.Remove("AdviserCID");
                clientSummary.Merge(holdingRptDataSet.Tables[HoldingRptDataSet.CLIENTSUMMARYTABLE], true, MissingSchemaAction.Add);
                clientSummary.WriteXml(w);
                financialDatasets.Tables["Product"].WriteXml(w);
                banktransacationDS.WriteXml(w);
                tdTransactionDS.WriteXml(w);
                dIVTransactionDS.WriteXml(w);
                manualTransactionDS.WriteXml(w);
                aSXTransactionDS.WriteXml(w);
                mISTransactionDS.WriteXml(w);
                clientDistributionsDS.WriteXml(w);
            }

            root.Add(nodes);
            return root.ToString();
        }
    }

    public class AllClientByTypeCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string type)
        {
            List<XNode> nodes = new List<XNode>();
            OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
            {
                if ((bmc is IXmlData) && (bmc is IOrganizationUnit) && (bmc as IOrganizationUnit).IsInvestableClient && (type == logical.CMTypeName) && !(bmc as IOrganizationUnit).IsConsolCM)
                {
                    XDocument document = XDocument.Parse((bmc as IXmlData).GetXmlData());
                    document.Root.RemoveAttributes();
                    document.Root.Name = "Client";
                    nodes.Add(document.FirstNode);
                }
            });
            XElement element = new XElement("Clients", nodes);
            return element.ToString();
        }
    }

    public class AllClientByAdviserID : OrganizationCmCommandBase
    {
        public override string DoAction(string adviserID)
        {
            List<XNode> nodes = new List<XNode>();
            //OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
            //{
            //    if ((bmc is IXmlData) && (bmc is IOrganizationUnit) && (bmc as IOrganizationUnit).IsInvestableClient && (bmc as IOrganizationUnit).HasAdviser && !(bmc as IOrganizationUnit).IsConsolCM)
            //    {
            //        XDocument document = XDocument.Parse((bmc as IXmlData).GetXmlData());
            //        document.Root.RemoveAttributes();
            //        document.Root.Name = "Client";
            //        nodes.Add(document.FirstNode);
            //    }
            //});
            XElement element = new XElement("Clients", nodes);
            return element.ToString();
        }
    }

    public class AllClientByIFA : OrganizationCmCommandBase
    {
        public override string DoAction(string ifaID)
        {
            var nodes = new List<XNode>();

            OrganisationListingDS organisationListingDS = new OrganisationListingDS();
            organisationListingDS.OrganisationListingOperationType = OrganisationListingOperationType.IFAAndClients;
            Organization.ExtractOrgnisationListingDS(organisationListingDS, "Administrator");
            DataView entityView = new DataView(organisationListingDS.Tables["Entities_Table"]);
            entityView.Sort = "ENTITYNAME_FIELD ASC";
            DataTable entityTable = entityView.ToTable();
            entityTable.Columns.Remove("ENTITYCLID_FIELD");
            entityTable.Columns.Remove("ENTITYCSID_FIELD");
            entityTable.Columns.Remove("ENTITYCTID_FIELD");
            entityTable.Columns.Remove("ENTITYSCTYPE_FIELD");
            entityTable.Columns.Remove("ENTITYSCSTATUS_FIELD");
            entityTable.Columns.Remove("ENTITYSORT_FIELD");
            entityTable.Columns["ENTITYNAME_FIELD"].ColumnName = "IFAName";
            entityTable.Columns["ENTITYCIID_FIELD"].ColumnName = "IFAID";
            ClientsByIFADS clientsByIFADS = new DataSets.ClientsByIFADS();
            clientsByIFADS.DataSetName = "Clients";


            string laneMossesIFAID = "4217bb03-d9df-4a51-b9b9-24fee00e11af";
            string dycryptedIFAID = string.Empty;

            if (ifaID == laneMossesIFAID)
                dycryptedIFAID = ifaID;
            else
            {
                ifaID = ifaID.Replace("FWSLASH", "/");
                ifaID = ifaID.Replace("BKSLASH", "\""); 
                dycryptedIFAID = Utilities.Encryption.DecryptData(ifaID);
            }
            DataRow[] rows = entityTable.Select("IFAID='" + dycryptedIFAID + "'");
            if (rows.Length > 0)
            {
                IBrokerManagedComponent ibmc = Organization.Broker.GetBMCInstance(new System.Guid(rows[0]["IFAID"].ToString()));
                ibmc.GetData(clientsByIFADS);
            }
            XElement root = new XElement("e-Clipse_Online_Services");

            using (XmlWriter w = root.CreateWriter()) { clientsByIFADS.WriteXml(w); }

            return root.ToString();
        }
    }

    public class ClientByNameCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string name)
        {
            string xml = new XElement("Error",
                            new XElement("Message", name + " not found")
                ).ToString();
            OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
            {
                if (bmc is IXmlData && bmc.Name == name)
                {
                    xml = (bmc as IXmlData).GetXmlData();
                }
            });
            return xml;
        }

    }
}
