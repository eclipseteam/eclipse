﻿using System;
using System.Data;
using Oritax.TaxSimp.CM.Group;

namespace Oritax.TaxSimp.CM.Organization
{
    /// <summary>
    /// The Information about the company
    /// </summary>
    [Serializable]
    public class AustralianEntityCompanyDetailsDS : AustralianEntityDetailsDS
    {
        string m_strTFN;
        string m_strABN;
        string m_strACN;

        public AustralianEntityCompanyDetailsDS()
            : base()
        {
            this.m_strTFN = String.Empty;
            this.m_strABN = String.Empty;
            this.m_strACN = String.Empty;
        }

        /// <summary>
        /// The Tax File Number of the company
        /// </summary>
        public string TaxFileNumber
        {
            get
            {
                return this.m_strTFN;
            }
            set
            {
                this.m_strTFN = value;
            }
        }

        /// <summary>
        /// The company's ABN
        /// </summary>
        public string ABN
        {
            get
            {
                return this.m_strABN;
            }
            set
            {
                this.m_strABN = value;
            }
        }

        /// <summary>
        /// The company's ACN
        /// </summary>
        public string ACN
        {
            get
            {
                return this.m_strACN;
            }
            set
            {
                this.m_strACN = value;
            }
        }
    }
}

