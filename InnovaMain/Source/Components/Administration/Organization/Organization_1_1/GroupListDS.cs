#region CHANGE_HISTORY
/*
 $Header: /2002/3. Implementation/Construction/BusinessStructureCM/Organization/Organization-1-1-1/GroupListDS.cs 2     12/02/03 11:15a Paub $
 $History: GroupListDS.cs $
 * 
 * *****************  Version 2  *****************
 * User: Paubailey    Date: 12/02/03   Time: 11:15a
 * Updated in $/2002/3. Implementation/Construction/BusinessStructureCM/Organization/Organization-1-1-1
 * 
 * *****************  Version 1  *****************
 * User: Sshrimpton   Date: 18/12/02   Time: 4:56p
 * Created in $/2002/3. Implementation/Construction/BusinessStructureCM/Organization/Organization-1-1-1
 * 
 * *****************  Version 2  *****************
 * User: Secampbell   Date: 7/11/02    Time: 11:59a
 * Updated in $/2002/3. Implementation/Elaboration4/CM/Organization
 * Added Change History Header
*/
#endregion

using System;
using System.Data;

namespace Oritax.TaxSimp.CM.Organization
{
	/// <summary>
	/// </summary>
	[Serializable] 
	public class GroupListDS : DataSet
	{
		public const String GROUPS_TABLE		= "GROUPS_TABLE";
		public const String GROUPNAME_FIELD		= "GROUPNAME_FIELD";
		public const String GROUPCLID_FIELD		= "GROUPCLID_FIELD";
		public const String GROUPCSID_FIELD		= "GROUPCSID_FIELD";
		public const String GROUPCIID_FIELD		= "GROUPCIID_FIELD";
		public const String GROUPCTID_FIELD		= "GROUPCTID_FIELD";		// Business entity type ID
		public const String GROUPSCTYPE_FIELD	= "GROUPSCTYPE_FIELD";
		public const String GROUPSCSTATUS_FIELD	= "GROUPSCSTATUS_FIELD";

		public GroupListDS()
		{
			DataTable table;

			table = new DataTable(GROUPS_TABLE);
			table.Columns.Add(GROUPNAME_FIELD, typeof(System.String));
			table.Columns.Add(GROUPCLID_FIELD, typeof(System.String));
			table.Columns.Add(GROUPCSID_FIELD, typeof(System.String));
			table.Columns.Add(GROUPCIID_FIELD, typeof(System.String));
			table.Columns.Add(GROUPCTID_FIELD, typeof(System.String));
			table.Columns.Add(GROUPSCTYPE_FIELD, typeof(System.String));
			table.Columns.Add(GROUPSCSTATUS_FIELD, typeof(System.String));
			this.Tables.Add(table);
		}
	}
}

