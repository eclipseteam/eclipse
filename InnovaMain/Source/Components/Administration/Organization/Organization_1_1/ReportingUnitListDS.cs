using System;
using System.Data;

using Oritax.TaxSimp.WPDatasetBase;

namespace Oritax.TaxSimp.CM.Organization
{
	/// <summary>
	/// 
	/// </summary>
	public class ReportingUnitListDS : WPDatasetBaseDS
	{
		public ReportingUnitListDS()
		{
			DataTable reportingUnitListTable = new DataTable(REPORTINGUNITLIST_TABLE);
			reportingUnitListTable.Columns.Add(REPORTINGUNITNAME);
			reportingUnitListTable.Columns.Add(REPORTINGUNITCLID,typeof(Guid));
			reportingUnitListTable.Columns.Add(REPORTINGUNITTYPENAME);
			reportingUnitListTable.Columns.Add(REPORTINGUNITTYPEID,typeof(Guid));

			this.Tables.Add(reportingUnitListTable);
		}

		public const string REPORTINGUNITLIST_TABLE = "ReportingUnitListTable";
		public const string REPORTINGUNITNAME = "ReportingUnitListName";
		public const string REPORTINGUNITCLID = "ReportingUnitListCLID";
		public const string REPORTINGUNITTYPENAME = "ReportingUnitListTypeName";
		public const string REPORTINGUNITTYPEID = "ReportingUnitListTypeID";
	}
}
