﻿using System;
using System.Collections.Generic;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.CM.Organization
{

    public class IdentityCMType
    {
        public OrganizationType Otype { get; set; }
        public string Username { get; set; }
        public bool IsLightData { get; set; }
    }

    public class OUNameCMType : IdentityCMType
    {
        public string Name { get; set; }
    }

    public class HaveAssociation : IdentityCMType, IIdentityCM
    {
        public Guid Csid { get; set; }
        public Guid Clid { get; set; }
    }

    public static class OrganizationTypeList
    {
        public static bool HasSharepoint(string value)
        {
            if (value == "Advisor")
                value = "Adviser";
            OrganizationType type = (OrganizationType)System.Enum.Parse(typeof(OrganizationType), value);
            return HasSharepoint(type);
        }

        public static bool HasSharepoint(Guid guid)
        {
            OrganizationType? type = null;
            foreach (var each in _Entities)
            {
                if (each.Value != guid) continue;
                type = each.Key;
                break;
            }
            return type == null ? false : HasSharepoint(type.Value);
        }

        public static bool HasClientIDApplicationID(OrganizationType type)
        {
            return type == OrganizationType.ClientCorporationPrivate ||
                   type == OrganizationType.ClientCorporationPublic ||
                   type == OrganizationType.ClientOtherTrustsCorporate ||
                   type == OrganizationType.ClientOtherTrustsIndividual ||
                   type == OrganizationType.ClientSMSFCorporateTrustee ||
                   type == OrganizationType.ClientSMSFIndividualTrustee ||
                   type == OrganizationType.ClientSoleTrader ||
                   type == OrganizationType.ConsoleClient ||
                   type == OrganizationType.ClientEClipseSuper ||
                   type == OrganizationType.Corporate ||
                   type == OrganizationType.Trust ||
                   type == OrganizationType.ClientIndividual ||
                   type == OrganizationType.ClientEClipseSuper ||
                   type == OrganizationType.DealerGroup ||
                   type == OrganizationType.Adviser ||
                   type == OrganizationType.IFA ||
                   type == OrganizationType.Accountant ||
                   type == OrganizationType.Individual||
                   type == OrganizationType.MemberFirm||
                     type == OrganizationType.NonIndividual;
        }

        public static bool HasSharepoint(OrganizationType type)
        {
            return type == OrganizationType.ClientCorporationPrivate ||
                    type == OrganizationType.ClientCorporationPublic ||
                    type == OrganizationType.ClientOtherTrustsCorporate ||
                    type == OrganizationType.ClientOtherTrustsIndividual ||
                    type == OrganizationType.ClientSMSFCorporateTrustee ||
                    type == OrganizationType.ClientSMSFIndividualTrustee ||
                    type == OrganizationType.Corporate ||
                    type == OrganizationType.Trust ||
                    type == OrganizationType.ClientIndividual ||
                    type == OrganizationType.ClientEClipseSuper||
                    type == OrganizationType.DealerGroup ||
                    type == OrganizationType.Adviser ||
                    type == OrganizationType.IFA ||
                    type == OrganizationType.Accountant ||
                     type == OrganizationType.Individual;

        }

        private static readonly Dictionary<OrganizationType, Guid> _Entities = new Dictionary<OrganizationType, Guid>()
        {
            { OrganizationType.Corporate, new Guid("A2E9B3AC-15C1-4FC8-AD52-4AEB6DA26572")},
            { OrganizationType.Trust, new Guid("6E865686-A6BA-4233-9D3E-41ECC31621F6")},
            { OrganizationType.ClientIndividual, new Guid("2A2A95AC-7933-40A6-B6BA-1597CB18014F")},
            { OrganizationType.ClientEClipseSuper, new Guid("E9D16670-D8F9-4A2D-844B-4EBE67E4629F")},
            { OrganizationType.DealerGroup, new Guid("B384E97F-A4B5-4A88-AE07-96D59A993837")},
            { OrganizationType.MemberFirm, new Guid("AF418E2D-349C-49A3-9BC6-F43035DA99F8")},
            { OrganizationType.Adviser, new Guid("6F5F1CAA-E770-4022-918D-A435CB896DE6")},
            { OrganizationType.Individual, new Guid("9EC83666-D774-4BFE-895C-1E8D42325C50")},
            { OrganizationType.ClientCorporationPrivate, new Guid("B95E5A7B-8C82-4C05-934D-F6AB7D95AE41")},
            { OrganizationType.ClientCorporationPublic, new Guid("96C93D8F-9D3D-4989-89CF-CFF47D377B43")},
            { OrganizationType.ClientOtherTrustsCorporate, new Guid("815051A3-D82D-41B6-8536-147D8A2055D2")},
            { OrganizationType.ClientOtherTrustsIndividual, new Guid("6660CEAD-AE7C-485E-AF8E-622FA54F7AF2")},
            { OrganizationType.ClientSMSFCorporateTrustee, new Guid("8256F2AA-3CCC-43B3-A683-90C8B603934A")},
            { OrganizationType.ClientSMSFIndividualTrustee, new Guid("7485432C-A44B-4E8B-AD73-9778E071DE2D")},
            { OrganizationType.BankAccount, new Guid("D851B5FF-AE5F-4BF6-A4D6-AB48743D1752")},
            { OrganizationType.PrincipalPractice, new Guid("62137E6D-E021-4412-8E87-779B32EFEE8E")},
            { OrganizationType.IFA, new Guid("0BB7F593-84D0-430C-AB49-5F7E9979628E")},
            { OrganizationType.ClientSoleTrader, new Guid("ABA693E9-BE2F-4228-A300-92100D418F9D")},
            { OrganizationType.DesktopBrokerAccount, new Guid("8AA0A349-B5EC-4A32-9E39-7538606D2F70")},
            { OrganizationType.TermDepositAccount, new Guid("C55FC882-0480-49EF-B844-71B77E454357")},
            { OrganizationType.ManagedInvestmentSchemesAccount, new Guid("CD9D3D21-8199-40E7-A55E-0659A3384B34")},
            { OrganizationType.Accountant, new Guid("FF8E26E5-1A3E-4AAC-8455-4B80707F38E9")},
            { OrganizationType.Period_1_1, new Guid("C54AF37A-709E-4FBD-9592-CCBBE16259DE")},
            { OrganizationType.ConsoleClient, new Guid("D565CDB2-D71D-46f6-8ADB-080E981EBA7E")},
            { OrganizationType.FeeRuns, new Guid("AE8DCE8C-0343-461C-8152-7FA4F4E22427")},
            { OrganizationType.Order, new Guid("6F78B54B-C4D4-4C4B-8711-26FDDBD53072")},
            { OrganizationType.SettledUnsettled, new Guid("83F89990-796E-4FA8-BA8F-83658BA0B12D")},
            { OrganizationType.NonIndividual, new Guid("10E95F10-0C46-4C35-A1E2-182285C7E7CD")},
            
        };

        public static Guid GetEntity(OrganizationType type)
        {
            return _Entities[type];
        }

       
    }
}
