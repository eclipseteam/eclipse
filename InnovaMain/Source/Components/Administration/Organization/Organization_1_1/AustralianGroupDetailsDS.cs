﻿using System;
using System.Data;
using Oritax.TaxSimp.CM.Group;

namespace Oritax.TaxSimp.CM.Organization
{
    /// <summary>
    /// DSCalculationModule is a DataSet that contains all of the persistent data for a CM instance.
    /// </summary>
    [Serializable]
    public class AustralianGroupDetailsDS : GroupDetailsDS
    {
        public const String SAP_TABLE = "SAP_TABLE";

        public AustralianGroupDetailsDS()
            : base()
        {
            DataTable table;
            table = new DataTable(SAP_TABLE);
            this.Tables.Add(table);
        }
    }
}

