using System;
using System.Data;
using Oritax.TaxSimp.Calculation;
using System.Xml;

namespace Oritax.TaxSimp.CM.Organization
{
	/// <summary>
	/// Presentation dataset for Reporting Units
	/// </summary>
	public class ReportingUnitPresentationDS : BusinessStructureWorkpaperDS
	{
        public const string ORG_TABLE = "ORG_TABLE";
		public const string REPORTINGUNITSTABLE = "REPORTINGUNITS";
        public const string PERIODSTABLE = "PERIODSTABLE";
        public const string REPORTINGUNITPIDFIELD = "PID";
        public const string REPORTINGUNITNAMEFIELD = "REPORTINGUNITNAME";
        public const string REPORTINGUNITSCENARIONAME = "REPORTINGUNITSCENARIONAME";
        public const string REPORTINGUNITCLIDFIELD = "REPORTINGUNITCLID";
        public const string REPORTINGUNITCSIDFIELD = "REPORTINGUNITCSID";
		public const string REPORTINGUNITTYPENAMEFIELD = "REPORTINGUNITTYPENAMEFIELD";
		public const string REPORTINGUNITORGANISATIONUNITTYPEFIELD = "REPORTINGUNITORGANISATIONUNITTYPE";
		public const string REPORTINGUNITSORTFIELD = "REPORTINGUNITSORTFIELD";
		public const string REPORTINGUNITWORKPAPERNAME = "REPORTINGUNITWORKPAPERNAME";
        public const string REPORTINGUNITWORKPAPERNAMEURLMAIN = "REPORTINGUNITWORKPAPERNAMEURLMAIN";
        public string testlink = string.Empty;
        public const string VISITEDWORKPAPERSTABLE = "VISITEDWORKPAPERS";
        public const string VISITEDWORKPAPERSDETAIL = "DETAIL";
        public string SearchString = String.Empty;
        public const string FAVOURITEWORKPAPERSTABLE = "FAVOURITEWORKPAPERS";

        public const string FAVOURITEWORKPAPERUSERNAME = "Username";
        public const string FAVOURITEWORKPAPERNAME = "WorkpaperName";
        public const string FAVOURITEWORKPAPERADDRESS = "WorkpaperAddress";
        public const string FAVOURITEWORKPAPERENTITY = "Entity";
        public const string FAVOURITEWORKPAPERPERIOD = "Period";
        public const string FAVOURITEWORKPAPERSCENARIO = "Scenario";

        public bool isTEALicenced = false;

        public XmlDocument TreeSource = null;

		public ReportingUnitPresentationDS():base()
		{
            DataTable objReportingUnitTable = CreateTable(REPORTINGUNITSTABLE);
			this.Tables.Add( objReportingUnitTable );
            objReportingUnitTable = CreateTable(ORG_TABLE);
			this.Tables.Add( objReportingUnitTable );

            DataTable periodTable = new DataTable(PERIODSTABLE);
            periodTable.Columns.Add(REPORTINGUNITCLID_FIELD, typeof(System.Guid));
            periodTable.Columns.Add(SCENARIONAME_FIELD, typeof(string));
            periodTable.Columns.Add(SCENARIOCSID_FIELD, typeof(System.Guid));
            periodTable.Columns.Add(PERIODCLID_FIELD, typeof(System.Guid));
            periodTable.Columns.Add(PERIODNAME_FIELD, typeof(System.String));


            //DataTable searchLevelNode = new DataTable("SearchLevelNode");
            //searchLevelNode.Columns.Add("PCLID", typeof(System.Guid));
            //searchLevelNode.Columns.Add("WPNAME", typeof(System.String));
            //searchLevelNode.Columns.Add("WPURL", typeof(System.String));
            //searchLevelNode.Columns.Add("LevelName", typeof(System.String));
            //this.Tables.Add(searchLevelNode);

            this.Tables.Add(periodTable);
		}

        private static DataTable CreateTable(string tableName)
        {
            DataTable objReportingUnitTable = new DataTable(tableName);
            objReportingUnitTable.Columns.Add(REPORTINGUNITNAMEFIELD, typeof(string));
            objReportingUnitTable.Columns.Add(REPORTINGUNITCLIDFIELD, typeof(Guid));
            objReportingUnitTable.Columns.Add(REPORTINGUNITCSIDFIELD, typeof(Guid));
            objReportingUnitTable.Columns.Add(REPORTINGUNITTYPENAMEFIELD, typeof(string));
            objReportingUnitTable.Columns.Add(REPORTINGUNITORGANISATIONUNITTYPEFIELD, typeof(string));
            objReportingUnitTable.Columns.Add(REPORTINGUNITSORTFIELD, typeof(string));
            objReportingUnitTable.Columns.Add(REPORTINGUNITWORKPAPERNAME, typeof(string));
            objReportingUnitTable.Columns.Add(REPORTINGUNITWORKPAPERNAMEURLMAIN, typeof(string));
            objReportingUnitTable.Columns.Add(REPORTINGUNITPIDFIELD, typeof(Guid));
            return objReportingUnitTable;
        }

        //public void MergeAndCreateSearchTable(string searchString)
        //{ 
        //    this.Tables["SearchLevelNode"].Merge(this.Tables["SecondLevelNode"],false,MissingSchemaAction.Ignore);
        //    this.Tables["SearchLevelNode"].Merge(this.Tables["ThirdLevelNode"], false, MissingSchemaAction.Ignore);
        //    this.Tables["SearchLevelNode"].Merge(this.Tables["FourthlevelNode"], false, MissingSchemaAction.Ignore);
        //    this.Tables["SearchLevelNode"].Merge(this.Tables["FifthlevelNode"], false, MissingSchemaAction.Ignore);
        //    this.Tables["SearchLevelNode"].Merge(this.Tables["SixlevelNode"], false, MissingSchemaAction.Ignore);

        //        DataView filteredSearchView = new DataView(this.Tables["SearchLevelNode"].Copy());
        //        filteredSearchView.RowFilter = "LevelName LIKE '%" + searchString + "%'";
        //        DataTable tableFiltered = filteredSearchView.ToTable();
        //        tableFiltered.TableName = "FilteredSearchLevelNode";
        //        this.Tables.Add(tableFiltered);
        //}

        public DataTable periodTable
        {
            get
            {
                return this.Tables["PeriodTableTree"];
            }
        }


        //public DataTable sixlevelNode
        //{
        //    get
        //    {
        //        return this.Tables["SixlevelNode"];
        //    }
        //}

        //public DataTable fifthlevelNode
        //{
        //    get
        //    {
        //        return this.Tables["FifthlevelNode"];
        //    }
        //}

        //public DataTable fourthLevelNode
        //{
        //    get
        //    {
        //        return this.Tables["FourthlevelNode"];
        //    }
        //}


        //public DataTable thirdLevelNode
        //{
        //    get
        //    {
        //        return this.Tables["ThirdLevelNode"];
        //    }
        //}

        //public DataTable secondLevelNode
        //{
        //    get
        //    {
        //        return this.Tables["SecondLevelNode"];
        //    }
        //}

        //public DataTable firstLevelNode
        //{
        //    get
        //    {
        //        return this.Tables["FirstLevelNode"];
        //    }
        //}

	}
}
