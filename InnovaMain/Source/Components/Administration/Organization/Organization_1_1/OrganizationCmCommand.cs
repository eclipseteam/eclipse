﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Enum;

namespace Oritax.TaxSimp.CM.Organization
{
    public enum OrganizationCommandType : int
    {
        GetAllOrganizationInstances = 1,
        AddOrganization,
        UpdateOrganization,
        DeleteOrganization,
        GetOrganzationsHasParentNull,
        GetOrganizationHasClient,
        GetOrganizationInstancesByCMType,
        GetOrganzationsHasSMSFCorporateTrusteeNull,
        GetStatusList,
        AddStatusList,
        UpdateStatusList,
        DeleteStatusList,
        GetFormMapsList,
        AddFormMapsList,
        CopyFormMapsList,
        UpdateFormMapsList,
        DeleteFormMapsList,
        ExportAllFormMapsList,
        ImportAllFormMapsList,
        GetAllOrganizationInstancesByType,
        GetDefaultFormsList,
        GetSmtpSettings,
        UpdateSmtpSettings,
        GetAllOUCMTypeByName,
        GetOrganizationChart,
        GetAssociation,
        GetAllAssociation,
        GetProductList,
        GetAssetList,
        AddProductsList,
        AddAssetsList,
        UpdateProductsList,
        UpdateAssetsList,
        DeleteProductsList,
        DeleteAssetsList,
        DeleteModelList,
        AddModelList,
        UpdateModelList,
        GetModelList,
        DeleteInstitutionList,
        AddInstitutionList,
        UpdateInstitutionList,
        GetInstitutionList,
        AddSecuritiesList,
        UpdateSecuritiesList,
        GetSecuritiesList,
        DeleteSecuritiesList,
        AddProductSecurities,
        UpdateProductSecurities,
        GetProductSecuritiesList,
        DeleteProductSecurities,
        GetAdminAccontRelation = 1033,
        GetAdminAccontListByType = 1034,
        FillModelTaskDescription = 1035,
        FillTaskStatus = 1036,
        AttachBankAccountInClient = 1037,
        GetBusinessGroupList,
        AddBusinessGroup,
        DeleteBusinessGroup,
        UpdateBusinessGroup,
        GetAllOrganizationExportInstances = 12000,
        GetAllOrganizationExportInstancesSS = 12001,
        GetAllOrganizationExportDBInstances = 12002,
        GetDefaultModelList,
        GetAllOrganizationInstancesByTypeWithDetail,
        GetAllOrganizationInstancesWithDetail,
        DeleteMISAccount,
        CheckFundCodeAssociation,
        ProcessSecuritiesList,
        GetModelAssociation,
        GetSelectedOrganizationInstancesByTypeWithDetail,
        AddManualAssetList,
        UpdateManualAssetList,
        GetManualAssetList,
        DeleteManualAssetList,
        DeleteAllImportTransaction,
        GetAllClientsNoAdviser,
        GetEntityListDataByClidCsid,
        SetClientBankAccountsUsers,
        ProcessSecuritiesDividendstWeb,
        ProcessSecuritiesDistributionstWeb,
        ProcessAllSecurityDividendCommand,
        ProcessAllSecurityDistributionsCommand,
        RemoveAllInvalidDistributionsCommand
    }

    public class  OrganizationListCommandByClidCsid:OrganizationCmCommandBase
    {

        public override string DoAction(string value)
        {
            var identies  = value.ToNewOrData<List<IdentityCM>>();
            List<OrganizationUnit> units = new List<OrganizationUnit>();
            foreach (var identy in identies)
            {
               var compnent= this.Organization.Broker.GetCMImplementation(identy.Clid, identy.Csid);
                if(compnent!=null)
                    units.Add(new OrganizationUnit() { Clid = identy.Clid, Csid = identy.Csid, Data = compnent.GetAllDataStream(1) });

            }
            return units.ToXmlString();
        }
    }

    public class OrganizationInstanceListCommandByTypeWithDetail : OrganizationCmCommandBase
    {
        public override string DoAction(string value)
        {
            IdentityCMType identity = value.ToNewOrData<IdentityCMType>();
            List<OrganizationUnit> units = new List<OrganizationUnit>();
            OrganizationUnit unit = null;
            OrganizationListAction.ForEach(identity.Username, Organization, (csid, logical, bmc) =>
            {
                if (logical.CMType == OrganizationTypeList.GetEntity(identity.Otype))
                {

                string accountprocessstatus = string.Empty;

                if (bmc is IHaveAccountProcess)
                    accountprocessstatus = (bmc as IHaveAccountProcess).GetAccountProcessStatus();

                    unit = new OrganizationUnit
                    {
                        Clid = logical.CLID,
                        Csid = csid,
                        Name = GetTypeName(identity.Otype, bmc),//bmc.Name,
                        Type = logical.CMTypeName,
                        Data = identity.IsLightData ? bmc.GetDataStream((int)CmCommand.GetLightEntity, string.Empty) : GetData(bmc),
                        IsInvestableClient = (bmc as OrganizationUnitCM).IsInvestableClient,
                        OrganizationStatus = (bmc as OrganizationUnitCM).OrganizationStatus,
                        ClientId = (bmc as OrganizationUnitCM).ClientId,
                        AccountProcessStatus = accountprocessstatus
                    };
                    units.Add(unit);
                }
            });
            return units.ToXmlString();
        }





        private string GetTypeName(OrganizationType type, IBrokerManagedComponent bmc)
        {
            string xml = string.Empty;
            switch (type)
            {
                case OrganizationType.Corporate:
                    xml = bmc.Name;
                    break;
                case OrganizationType.Trust:
                    xml = bmc.Name;
                    break;
                case OrganizationType.ClientIndividual:
                    xml = bmc.Name;
                    break;
                case OrganizationType.ClientEClipseSuper:
                    xml = bmc.Name;
                    break;
                case OrganizationType.DealerGroup:
                    xml = bmc.Name;
                    break;
                case OrganizationType.MemberFirm:
                    xml = bmc.Name;
                    break;
                case OrganizationType.Adviser:
                    xml = bmc.GetDataStream(999, string.Empty);
                    break;
                case OrganizationType.Individual:
                    xml = bmc.GetDataStream(999, string.Empty);
                    break;
                case OrganizationType.ClientCorporationPrivate:
                    xml = bmc.Name;
                    break;
                case OrganizationType.ClientCorporationPublic:
                    xml = bmc.Name;
                    break;
                case OrganizationType.ClientOtherTrustsCorporate:
                    xml = bmc.Name;
                    break;
                case OrganizationType.ClientOtherTrustsIndividual:
                    xml = bmc.Name;
                    break;
                case OrganizationType.ClientSMSFCorporateTrustee:
                    xml = bmc.Name;
                    break;
                case OrganizationType.ClientSMSFIndividualTrustee:
                    xml = bmc.Name;
                    break;
                case OrganizationType.BankAccount:
                    xml = bmc.Name;
                    break;
                case OrganizationType.PrincipalPractice:
                    xml = bmc.Name;
                    break;
                default:
                    xml = bmc.Name;
                    break;
            }
            return xml;
        }

        public string GetData(IBrokerManagedComponent bmc)
        {
            return bmc.GetDataStream(-911, string.Empty);// run default Command From CM Class
        }


    }



    public class OrganizationInstanceCommandByTypeWithDetail : OrganizationCmCommandBase
    {
        public override string DoAction(string value)
        {

            List<string> args=value.ToNewOrData<List<string>>();

            List<IdentityCM> unitsIdentityCms = args[0].ToNewOrData<List<IdentityCM>>();
            UserEntity user = args[1].ToNewOrData<UserEntity>();


            List<OrganizationUnit> units = new List<OrganizationUnit>();
            OrganizationUnit unit = null;
            OrganizationListAction.ForEach(user.CurrentUserName, Organization, (csid, logical, bmc) =>
            {
                if (bmc is OrganizationUnitCM && unitsIdentityCms.Count(tempUnit => tempUnit.Clid == (bmc as OrganizationUnitCM).Clid && tempUnit.Csid == (bmc as OrganizationUnitCM).Csid) > 0 )
                {
                    unit = new OrganizationUnit
                    {
                        Clid = logical.CLID,
                        Csid = csid,
                        //Name = GetTypeName((bmc as OrganizationUnitCM)., bmc),//bmc.Name,
                        Type = logical.CMTypeName,
                        Data =  GetData(bmc),
                        IsInvestableClient = (bmc as OrganizationUnitCM).IsInvestableClient,
                        OrganizationStatus = (bmc as OrganizationUnitCM).OrganizationStatus,
                        ClientId = (bmc as OrganizationUnitCM).ClientId
                    };
                    units.Add(unit);
                }
            });
            return units.ToXmlString();
        }





        private string GetTypeName(OrganizationType type, IBrokerManagedComponent bmc)
        {
            string xml = string.Empty;
            switch (type)
            {
                case OrganizationType.Corporate:
                    xml = bmc.Name;
                    break;
                case OrganizationType.Trust:
                    xml = bmc.Name;
                    break;
                case OrganizationType.ClientIndividual:
                    xml = bmc.Name;
                    break;
                case OrganizationType.ClientEClipseSuper:
                    xml = bmc.Name;
                    break;
                case OrganizationType.DealerGroup:
                    xml = bmc.Name;
                    break;
                case OrganizationType.MemberFirm:
                    xml = bmc.Name;
                    break;
                case OrganizationType.Adviser:
                    xml = bmc.GetDataStream(999, string.Empty);
                    break;
                case OrganizationType.Individual:
                    xml = bmc.GetDataStream(999, string.Empty);
                    break;
                case OrganizationType.ClientCorporationPrivate:
                    xml = bmc.Name;
                    break;
                case OrganizationType.ClientCorporationPublic:
                    xml = bmc.Name;
                    break;
                case OrganizationType.ClientOtherTrustsCorporate:
                    xml = bmc.Name;
                    break;
                case OrganizationType.ClientOtherTrustsIndividual:
                    xml = bmc.Name;
                    break;
                case OrganizationType.ClientSMSFCorporateTrustee:
                    xml = bmc.Name;
                    break;
                case OrganizationType.ClientSMSFIndividualTrustee:
                    xml = bmc.Name;
                    break;
                case OrganizationType.BankAccount:
                    xml = bmc.Name;
                    break;
                case OrganizationType.PrincipalPractice:
                    xml = bmc.Name;
                    break;
                default:
                    xml = bmc.Name;
                    break;
            }
            return xml;
        }

        public string GetData(IBrokerManagedComponent bmc)
        {
            return bmc.GetDataStream(-911, string.Empty);// run default Command From CM Class
        }


    }

    public class OrganizationInstanceListCommandByType : OrganizationCmCommandBase
    {
        public override string DoAction(string value)
        {
            IdentityCMType identity = value.ToNewOrData<IdentityCMType>();
            List<OrganizationUnit> units = new List<OrganizationUnit>();
            OrganizationUnit unit = null;
            OrganizationListAction.ForEach(identity.Username, Organization, (csid, logical, bmc) =>
                {
                    if (logical.CMType == OrganizationTypeList.GetEntity(identity.Otype))
                    {
                        unit = new OrganizationUnit
                        {
                            Clid = logical.CLID,
                            Csid = csid,
                            Name = GetTypeName(identity.Otype, bmc),//bmc.Name,
                            Type = logical.CMTypeName,
                            Data = string.Empty,
                            IsInvestableClient = (bmc as OrganizationUnitCM).IsInvestableClient,
                            OrganizationStatus = (bmc as OrganizationUnitCM).OrganizationStatus,
                            ClientId = (bmc as OrganizationUnitCM).ClientId
                        };
                        units.Add(unit);
                    }
                });
            return units.ToXmlString();
        }

        private string GetTypeName(OrganizationType type, IBrokerManagedComponent bmc)
        {
            string xml = string.Empty;
            switch (type)
            {
                case OrganizationType.Corporate:
                    xml = bmc.Name;
                    break;
                case OrganizationType.Trust:
                    xml = bmc.Name;
                    break;
                case OrganizationType.ClientIndividual:
                    xml = bmc.Name;
                    break;
                case OrganizationType.ClientEClipseSuper:
                    xml = bmc.Name;
                    break;
                case OrganizationType.DealerGroup:
                    xml = bmc.Name;
                    break;
                case OrganizationType.MemberFirm:
                    xml = bmc.Name;
                    break;
                case OrganizationType.Adviser:
                    xml = bmc.GetDataStream(999, string.Empty);
                    break;
                case OrganizationType.Individual:
                    xml = bmc.GetDataStream(999, string.Empty);
                    break;
                case OrganizationType.ClientCorporationPrivate:
                    xml = bmc.Name;
                    break;
                case OrganizationType.ClientCorporationPublic:
                    xml = bmc.Name;
                    break;
                case OrganizationType.ClientOtherTrustsCorporate:
                    xml = bmc.Name;
                    break;
                case OrganizationType.ClientOtherTrustsIndividual:
                    xml = bmc.Name;
                    break;
                case OrganizationType.ClientSMSFCorporateTrustee:
                    xml = bmc.Name;
                    break;
                case OrganizationType.ClientSMSFIndividualTrustee:
                    xml = bmc.Name;
                    break;
                case OrganizationType.BankAccount:
                    xml = bmc.Name;
                    break;
                case OrganizationType.PrincipalPractice:
                    xml = bmc.Name;
                    break;
                default:
                    xml = bmc.Name;
                    break;
            }
            return xml;
        }

    }

    public class OrganizationInstanceListCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string value)
        {
            List<OrganizationUnit> units = new List<OrganizationUnit>();
            OrganizationUnit unit = null;
            OrganizationListAction.ForEach(value, Organization, (csid, logical, bmc) =>
            {
                string accountprocessstatus = string.Empty;

                if (bmc is IHaveAccountProcess)
                    accountprocessstatus = (bmc as IHaveAccountProcess).GetAccountProcessStatus();

                unit = new OrganizationUnit
                {
                    Clid = logical.CLID,
                    Csid = csid,
                    Name = GetTypeName(logical.CMTypeName, bmc),//bmc.Name,
                    Type = logical.CMTypeName,
                    Data = string.Empty,
                    IsInvestableClient = (bmc as OrganizationUnitCM).IsInvestableClient,
                    OrganizationStatus = (bmc as OrganizationUnitCM).OrganizationStatus,
                    ClientId = (bmc as OrganizationUnitCM).ClientId,
                    AccountProcessStatus = accountprocessstatus
                };

                units.Add(unit);
            });

            bool createLibrary = false;
            if (createLibrary)
            {
                CreateSharepointLibrary(value);
            }

            return units.ToXmlString();
        }

        private void CreateSharepointLibrary(string value)
        {
            List<OrganizationUnit> units = new List<OrganizationUnit>();
            OrganizationUnit unit = null;
            OrganizationListAction.ForEach(value, Organization, (csid, logical, bmc) =>
            {
                unit = new OrganizationUnit
                {
                    Clid = logical.CLID,
                    Csid = csid,
                    Name = GetTypeName(logical.CMTypeName, bmc),//bmc.Name,
                    Type = logical.CMTypeName,
                    Data = string.Empty,
                    IsInvestableClient = (bmc as OrganizationUnitCM).IsInvestableClient,
                    OrganizationStatus = (bmc as OrganizationUnitCM).OrganizationStatus,
                    ClientId = (bmc as OrganizationUnitCM).ClientId
                };
                units.Add(unit);
            });
        }

        private string GetTypeName(string type, IBrokerManagedComponent bmc)
        {
            string fullname = string.Empty;

            if (type.ToLower() == "advisor" || type.ToLower() == "adviser")
                fullname = bmc.GetDataStream(999, string.Empty);
            else if (type.ToLower() == "individual")
                fullname = bmc.GetDataStream(999, string.Empty);
            else
                fullname = bmc.Name;

            return fullname;
        }
    }


    public class OrganizationInstanceListCommandWithDetail : OrganizationCmCommandBase
    {
        public override string DoAction(string value)
        {
            List<OrganizationUnit> units = new List<OrganizationUnit>();
            OrganizationUnit unit = null;
            try
            {
                OrganizationListAction.ForEach(value, Organization, (csid, logical, bmc) =>
                {
                    string accountprocessstatus = string.Empty;

                    if (bmc is IHaveAccountProcess)
                        accountprocessstatus = (bmc as IHaveAccountProcess).GetAccountProcessStatus();

                    unit = new OrganizationUnit
                    {
                        Clid = logical.CLID,
                        Csid = csid,
                        Name = GetTypeName(logical.CMTypeName, bmc),//bmc.Name,
                        Type = logical.CMTypeName,
                        Data = GetData(bmc),
                        IsInvestableClient = (bmc as OrganizationUnitCM).IsInvestableClient,
                        OrganizationStatus = (bmc as OrganizationUnitCM).OrganizationStatus,
                        ClientId = (bmc as OrganizationUnitCM).ClientId,
                        AccountProcessStatus = accountprocessstatus
                    };
                    units.Add(unit);
                });
            }
            catch (Exception ex)
            {
                throw ex;
            }
            bool createLibrary = false;
            if (createLibrary)
            {
                CreateSharepointLibrary(value);
            }

            return units.ToXmlString();
        }

        private void CreateSharepointLibrary(string value)
        {
            List<OrganizationUnit> units = new List<OrganizationUnit>();
            OrganizationUnit unit = null;
            OrganizationListAction.ForEach(value, Organization, (csid, logical, bmc) =>
            {
                unit = new OrganizationUnit
                {
                    Clid = logical.CLID,
                    Csid = csid,
                    Name = GetTypeName(logical.CMTypeName, bmc),//bmc.Name,
                    Type = logical.CMTypeName,
                    Data = string.Empty,
                    IsInvestableClient = (bmc as OrganizationUnitCM).IsInvestableClient,
                    OrganizationStatus = (bmc as OrganizationUnitCM).OrganizationStatus,
                    ClientId = (bmc as OrganizationUnitCM).ClientId
                };
                units.Add(unit);
            });
        }

        private string GetTypeName(string type, IBrokerManagedComponent bmc)
        {
            string fullname = string.Empty;

            if (type.ToLower() == "advisor" || type.ToLower() == "adviser")
                fullname = bmc.GetDataStream(999, string.Empty);
            else if (type.ToLower() == "individual")
                fullname = bmc.GetDataStream(999, string.Empty);
            else
                fullname = bmc.Name;

            return fullname;
        }

        public string GetData(IBrokerManagedComponent bmc)
        {
            return bmc.GetDataStream(-911, string.Empty);// run default Command From CM Class
        }
    }

    public class OrganizationInstanceExportListCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string value)
        {
            List<OrganizationUnitExport> units = new List<OrganizationUnitExport>();
            OrganizationUnitExport unit = null;
            OrganizationListAction.ForEach(value, Organization, (csid, logical, bmc) =>
            {
                if ((bmc as OrganizationUnitCM).IsInvestableClient)
                {
                    unit = new OrganizationUnitExport
                               {
                                   Clid = logical.CLID,
                                   Csid = csid,
                                   Name = GetTypeName(logical.CMTypeName, bmc),
                                   Type = logical.CMTypeName,
                                   Data = string.Empty,
                                   IsInvestableClient = (bmc as OrganizationUnitCM).IsInvestableClient,
                                   OrganizationStatus = (bmc as OrganizationUnitCM).OrganizationStatus,
                                   ClientId = (bmc as OrganizationUnitCM).ClientId,
                                   IsExport = (bmc as OrganizationUnitCM).IsExported.ToString(),
                                   ApplicationDate = (bmc as OrganizationUnitCM).ApplicationDate,
                                   ApplicationID = (bmc as OrganizationUnitCM).ApplicationID
                               };
                    units.Add(unit);
                }
            });

            return units.ToXmlString();
        }

        private string GetTypeName(string type, IBrokerManagedComponent bmc)
        {
            string fullname = string.Empty;

            if (type.ToLower() == "advisor" || type.ToLower() == "adviser")
                fullname = bmc.GetDataStream(999, string.Empty);
            else if (type.ToLower() == "individual")
                fullname = bmc.GetDataStream(999, string.Empty);
            else
                fullname = bmc.Name;

            return fullname;
        }
    }

    public class OrganizationInstanceExportSSListCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string value)
        {
            List<OrganizationUnitExport> units = new List<OrganizationUnitExport>();
            OrganizationUnitExport unit = null;
            OrganizationListAction.ForEach(value, Organization, (csid, logical, bmc) =>
            {
                
                //if ((bmc as OrganizationUnitCM).IsInvestableClient && ((bmc as OrganizationUnitCM).ServiceType.DO_IT_FOR_ME || (bmc as OrganizationUnitCM).ServiceType.DO_IT_WITH_ME))
                //if ((bmc as OrganizationUnitCM).IsInvestableClient && ((bmc as OrganizationUnitCM).ServiceType.DO_IT_FOR_ME))
                if (((bmc as OrganizationUnitCM).IsInvestableClient && ((bmc as OrganizationUnitCM).ServiceType.DO_IT_FOR_ME)) || ((bmc as OrganizationUnitCM).IsInvestableClient && ((bmc as OrganizationUnitCM).ServiceType.DO_IT_WITH_ME)))
                {
                   
                    unit = new OrganizationUnitExport
                               {
                                   Clid = logical.CLID,
                                   Csid = csid,
                                   Name = GetTypeName(logical.CMTypeName, bmc),
                                   Type = logical.CMTypeName,
                                   Data = string.Empty,
                                   IsInvestableClient = (bmc as OrganizationUnitCM).IsInvestableClient,
                                   OrganizationStatus = (bmc as OrganizationUnitCM).OrganizationStatus,
                                   ClientId = (bmc as OrganizationUnitCM).ClientId,
                                   IsExport = (bmc as OrganizationUnitCM).IsExportedSS.ToString(),
                                   ApplicationDate = (bmc as OrganizationUnitCM).ApplicationDate,
                                   ApplicationID = (bmc as OrganizationUnitCM).ApplicationID
                                   
                               };
                    units.Add(unit);
                }
            });

            return units.ToXmlString();
        }

        private string GetTypeName(string type, IBrokerManagedComponent bmc)
        {
            string fullname = string.Empty;

            if (type.ToLower() == "advisor" || type.ToLower() == "adviser")
                fullname = bmc.GetDataStream(999, string.Empty);
            else if (type.ToLower() == "individual")
                fullname = bmc.GetDataStream(999, string.Empty);
            else
                fullname = bmc.Name;

            return fullname;
        }
    }


    public class OrganizationInstanceExportDBListCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string value)
        {
            List<OrganizationUnitExport> units = new List<OrganizationUnitExport>();
            OrganizationUnitExport unit = null;
            OrganizationListAction.ForEach(value, Organization, (csid, logical, bmc) =>
            {
                if ((bmc as OrganizationUnitCM).IsInvestableClient)
                {
                unit = new OrganizationUnitExport
                {
                    Clid = logical.CLID,
                    Csid = csid,
                    Name = GetTypeName(logical.CMTypeName, bmc),
                    Type = logical.CMTypeName,
                    Data = string.Empty,

                    IsInvestableClient = (bmc as OrganizationUnitCM).IsInvestableClient,
                    OrganizationStatus = (bmc as OrganizationUnitCM).OrganizationStatus,
                    ClientId = (bmc as OrganizationUnitCM).ClientId,
                    IsExport = (bmc as OrganizationUnitCM).IsExportedDB.ToString(),
                    ApplicationDate = (bmc as OrganizationUnitCM).ApplicationDate,
                    ApplicationID = (bmc as OrganizationUnitCM).ApplicationID
                };
                units.Add(unit);}
            });

            return units.ToXmlString();
        }

        private string GetTypeName(string type, IBrokerManagedComponent bmc)
        {
            string fullname = string.Empty;

            if (type.ToLower() == "advisor" || type.ToLower() == "adviser")
                fullname = bmc.GetDataStream(999, string.Empty);
            else if (type.ToLower() == "individual")
                fullname = bmc.GetDataStream(999, string.Empty);
            else
                fullname = bmc.Name;

            return fullname;
        }
    }




    public class OrganzationsHasParentNullCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string value)
        {
            IdentityCMType identity = value.ToNewOrData<IdentityCMType>();
            List<OrganizationUnit> units = new List<OrganizationUnit>();
            OrganizationUnit unit = null;
            OrganizationListAction.ForEach(identity.Username, Organization, (csid, logical, bmc) =>
            {
                ICmHasParent parent = bmc as ICmHasParent;
                if (parent != null && parent.Parent == IdentityCM.Null)
                {
                    if (logical.CMType == OrganizationTypeList.GetEntity(identity.Otype))
                    {
                        unit = new OrganizationUnit
                        {
                            Clid = logical.CLID,
                            Csid = csid,
                            Name = GetTypeName(identity.Otype, bmc),//bmc.Name
                            Type = logical.CMTypeName,
                            Data = string.Empty
                        };
                        units.Add(unit);
                    }
                }
            });
            return units.ToXmlString();
        }

        private string GetTypeName(OrganizationType type, IBrokerManagedComponent bmc)
        {
            string typename = string.Empty;
            switch (type)
            {
                case OrganizationType.Adviser:
                    typename = bmc.GetDataStream(999, string.Empty);
                    break;
                case OrganizationType.IFA:
                case OrganizationType.Corporate:
                case OrganizationType.Trust:
                case OrganizationType.ClientIndividual:
                case OrganizationType.ClientEClipseSuper:
                case OrganizationType.DealerGroup:
                case OrganizationType.MemberFirm:
                case OrganizationType.Individual:
                case OrganizationType.ClientCorporationPrivate:
                case OrganizationType.ClientCorporationPublic:
                case OrganizationType.ClientOtherTrustsCorporate:
                case OrganizationType.ClientOtherTrustsIndividual:
                case OrganizationType.ClientSMSFCorporateTrustee:
                case OrganizationType.ClientSMSFIndividualTrustee:
                case OrganizationType.BankAccount:
                case OrganizationType.PrincipalPractice:
                case OrganizationType.ClientSoleTrader:
                default:
                    typename = bmc.Name;
                    break;
            }
            return typename;
        }

    }

    public class OrganzationsHasSMSFCorporateTrusteeNullCommand : OrganizationCmCommandBase
    {

        public override string DoAction(string value)
        {
            IdentityCMType identity = value.ToNewOrData<IdentityCMType>();
            List<OrganizationUnit> units = new List<OrganizationUnit>();
            OrganizationUnit unit = null;
            OrganizationListAction.ForEach(identity.Username, Organization, (csid, logical, bmc) =>
            {
                IIdentitySMSFCorporateTrustee entity = bmc as IIdentitySMSFCorporateTrustee;
                if (entity != null && entity.IdentitySMSFCorporateTrustee == IdentityCM.Null)
                {
                    if (logical.CMType == OrganizationTypeList.GetEntity(identity.Otype))
                    {
                        unit = new OrganizationUnit { Clid = logical.CLID, Csid = csid, Name = bmc.Name, Type = logical.CMTypeName, Data = string.Empty };
                        units.Add(unit);
                    }
                }
            });
            return units.ToXmlString();
        }
    }

    public class OrganzationsHasClientCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string value)
        {
            IdentityCMType identity = value.ToNewOrData<IdentityCMType>();
            List<OrganizationUnit> units = new List<OrganizationUnit>();
            OrganizationUnit unit = null;
            OrganizationListAction.ForEach(identity.Username, Organization, (csid, logical, bmc) =>
            {
                ICmHasClient client = bmc as ICmHasClient;
                if (client != null && client.IsClient == true)
                {
                    ICmHasParent parent = bmc as ICmHasParent;
                    if (parent != null && parent.Parent == IdentityCM.Null)
                    {
                        unit = new OrganizationUnit { Clid = logical.CLID, Csid = csid, Name = bmc.Name, Type = logical.CMTypeName, Data = string.Empty };
                        units.Add(unit);
                    }
                }
            });
            return units.ToXmlString();
        }
    }

    public class OrganizationInstancesByCMTypeCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string value)
        {
            IdentityCMType identity = value.ToNewOrData<IdentityCMType>();
            List<OrganizationUnit> units = new List<OrganizationUnit>();
            OrganizationUnit unit = null;
            OrganizationListAction.ForEach(identity.Username, Organization, (csid, logical, bmc) =>
            {
                if (logical.CMType == OrganizationTypeList.GetEntity(identity.Otype))
                {
                    unit = new OrganizationUnit { Clid = logical.CLID, Csid = csid, Name = bmc.Name, Type = logical.CMTypeName, Data = string.Empty };
                    units.Add(unit);
                }
            });
            return units.ToXmlString();
        }
    }

    public class AllOUCMTypeByNameCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string value)
        {
            OUNameCMType identity = value.ToNewOrData<OUNameCMType>();
            List<OrganizationUnit> units = new List<OrganizationUnit>();
            OrganizationUnit unit = null;
            OrganizationListAction.ForEach(identity.Username, Organization, (csid, logical, bmc) =>
            {
                if (logical.CMType == OrganizationTypeList.GetEntity(identity.Otype))
                {
                    if (bmc.Name.ToLower() == identity.Name.ToLower())
                    {
                        unit = new OrganizationUnit
                        {
                            Clid = logical.CLID,
                            Csid = csid,
                            Name = bmc.Name,
                            Type = logical.CMTypeName,
                            Data = GetData(bmc)
                        };
                        units.Add(unit);
                    }
                }
            });
            return units.ToXmlString();
        }

        public string GetData(IBrokerManagedComponent bmc)
        {
            return bmc.GetDataStream(-911, string.Empty);
        }
    }

    public class OrganizationUpdateCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string value)
        {
            Organization.Broker.SetWriteStart();
            OrganizationUnit unit = value.ToNewOrData<OrganizationUnit>();
            ILogicalModule logical = Organization.Broker.GetLogicalCM(Organization.CLID);
            ILogicalModule delete = Organization.Broker.GetLogicalCM(unit.Clid);
            logical.RemoveChildLogicalCM(delete);
            Organization.Broker.ReleaseBrokerManagedComponent(logical);
            Organization.Broker.ReleaseBrokerManagedComponent(delete);
            return value;
        }
    }

    public class OrganizationDeleteCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string value)
        {
            //UserEntity CurrentUser = new UserEntity();
            Organization.Broker.SetWriteStart();
            OrganizationUnit unit = value.ToNewOrData<OrganizationUnit>();
            DeleteOrganizationUnit(unit);
            return value;
        }

        public void DeleteOrganizationUnit(OrganizationUnit unit)
        {
            ILogicalModule logical = Organization.Broker.GetLogicalCM(Organization.CLID);
            ILogicalModule delete = Organization.Broker.GetLogicalCM(unit.Clid);
            logical.RemoveChildLogicalCM(delete);
            Organization.Broker.ReleaseBrokerManagedComponent(logical);
            Organization.Broker.ReleaseBrokerManagedComponent(delete);

            if (unit.ClientId == "" || unit.ClientId == null)
            {
                UpDateEventLog.UpdateLog(unit.Type, unit.Name, 4, Organization.Broker);
            }
            else
            {
                UpDateEventLog.UpdateLog(unit.Type, unit.Name + " (" + unit.ClientId + ")", 4, Organization.Broker);
            }
        }
    }


    public class MISAccountDeleteCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string value)
        {
            //UserEntity CurrentUser = new UserEntity();
            Organization.Broker.SetWriteStart();
            OrganizationUnit unit = value.ToNewOrData<OrganizationUnit>();


            bool isAttachedToProduct = CheckIfAttachedToProduct(unit);
            bool isAttchedToClient = CheckedIfAttachedToClient(unit);

            if (!isAttachedToProduct && !isAttchedToClient)
            {
              
                
                
                OrganizationDeleteCommand deleteCommand = new OrganizationDeleteCommand();
                deleteCommand.Organization = this.Organization;
                
                deleteCommand.DeleteOrganizationUnit(unit);
            }
            else
            {
                if (isAttachedToProduct && isAttchedToClient)
                {
                    value = ((int)AssociatedOrganizationStatus.AttachedToCientAndProduct).ToString();//MIs is attached to  Client and Product
                }else if (isAttachedToProduct)
                {
                    value = ((int)AssociatedOrganizationStatus.AttachedToProduct).ToString();//MIs is attached with product
                }
                else if (isAttchedToClient)
                {
                    value = ((int)AssociatedOrganizationStatus.AttachedToClient).ToString(); ;//MIs is attached with Client
                }

              
            }

            return value;
        }

        private bool CheckIfAttachedToProduct(OrganizationUnit unit)
        {
            bool result = false;
            if (Organization.Products != null)
             result=     Organization.Products.Count
                 (product => 
                     product.EntityId!=null && 
                     product.EntityId.Clid == unit.Clid && 
                     product.EntityId.Csid == unit.Csid
                  ) > 0;
            return result;
        }

        private bool CheckedIfAttachedToClient(OrganizationUnit unit)
        {

            bool result = false;


            OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
            {
                if ((bmc is OrganizationUnitCM) && (bmc as IOrganizationUnit).IsInvestableClient)
                {
                    result = ((bmc as OrganizationUnitCM).IsMISAttached(unit.Clid, unit.Csid))?true:result;


                }
            });



            return result;
        }
    }

    public class CheckMISAccountFundCodeDeleteCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string value)
        {
            //UserEntity CurrentUser = new UserEntity();
            Organization.Broker.SetWriteStart();
            FundAccountEntity unit = value.ToNewOrData<FundAccountEntity>();


            bool isAttachedToProduct = CheckIfAttachedToProduct(unit);
            bool isAttchedToClient = CheckedIfAttachedToClient(unit);

           
                if (isAttachedToProduct && isAttchedToClient)
                {
                    value = ((int)AssociatedOrganizationStatus.AttachedToCientAndProduct).ToString();//MIs is attached to  Client and Product
                }
                else if (isAttachedToProduct)
                {
                    value = ((int)AssociatedOrganizationStatus.AttachedToProduct).ToString();//MIs is attached with product
                }
                else if (isAttchedToClient)
                {
                    value = ((int)AssociatedOrganizationStatus.AttachedToClient).ToString(); ;//MIs is attached with Client
                }


          

            return value;
        }

        private bool CheckIfAttachedToProduct(FundAccountEntity unit)
        {
            bool result = false;
            if (Organization.Products != null)
                result = Organization.Products.Count
                    (product =>
                        product.FundAccounts != null &&
                        product.FundAccounts.Count(aa=>aa==unit.ID) >0
                        
                     ) > 0;
            return result;
        }

        private bool CheckedIfAttachedToClient(FundAccountEntity unit)
        {

            bool result = false;


            OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
            {
                if ((bmc is OrganizationUnitCM) && (bmc as IOrganizationUnit).IsInvestableClient)
                {
                    result = ((bmc as OrganizationUnitCM).IsMISFundCodeAttached(unit.ID)) ? true : result;


                }
            });



            return result;
        }
    }

    public class OrganizationGetDefaultFormsListCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string value)
        {
            string AssemblyDirectoryName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string docFolder = AssemblyDirectoryName + @"\Documents\Forms\PDF\";

            switch (value.ToInt())
            {
                case (int)OrganizationType.ClientCorporationPrivate:
                    docFolder += "ClientCorporationPrivate";
                    break;
                case (int)OrganizationType.ClientCorporationPublic:
                    docFolder += "ClientCorporationPublic";
                    break;
                case (int)OrganizationType.ClientIndividual:
                    docFolder += "ClientIndividual";
                    break;
                case (int)OrganizationType.ClientEClipseSuper:
                    docFolder += "ClientEClipseSuper";
                    break;
                case (int)OrganizationType.ClientOtherTrustsCorporate:
                    docFolder += "ClientOtherTrustsCorporate";
                    break;
                case (int)OrganizationType.ClientOtherTrustsIndividual:
                    docFolder += "ClientOtherTrustsIndividual";
                    break;
                case (int)OrganizationType.ClientSMSFCorporateTrustee:
                    docFolder += "ClientSMSFCorporateTrustee";
                    break;
                case (int)OrganizationType.ClientSMSFIndividualTrustee:
                    docFolder += "ClientSMSFIndividualTrustee";
                    break;
                case (int)OrganizationType.ClientSoleTrader:
                    docFolder += "ClientSoleTrader";
                    break;
                default:
                    throw new ArgumentException("Unknown enum value found.");
            }

            docFolder += "\\";


            List<DefaultFormsFile> files = new List<DefaultFormsFile>();

            FileProcessCommadnType type = FileProcessCommadnType.NormarlTagged;
            string commtwoFileName = "BWAMI CIP.pdf";


            foreach (var file in GetFiles(docFolder, "*.PDF|*.Doc|*.docx"))
            {

                if (Path.GetFileName(file) == commtwoFileName)
                {
                    type = FileProcessCommadnType.PartialTagged;
                }
                else
                {

                    if (Path.GetExtension(file).ToUpper() == ".PDF")
                        type = FileProcessCommadnType.NormarlTagged;
                    else
                    {
                        type = FileProcessCommadnType.DocFile;
                    }

                }

                files.Add(new DefaultFormsFile() { FileName = Path.GetFileName(file), FilePath = file, MapID = Guid.NewGuid(), Type = type });
            }

            return files.ToXmlString();
        }

        private static string[] GetFiles(string sourceFolder, string filters)
        {
            return filters.Split('|').SelectMany(filter => System.IO.Directory.GetFiles(sourceFolder, filter)).ToArray();
        }
    }

    #region *** Status List Commands ***

    public class GetStatusListCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string value)
        {
            Organization.Broker.SetWriteStart();

            SetDefaultValues("Organization");
            SetDefaultValues("Client");
            SetDefaultValues("Individual");
            SetDefaultValues("Bank");
            SetDefaultValues("DesktopBrokerAccount");
            SetDefaultValues("TermDepositAccount");
            SetDefaultValues("ManagedInvestmentSchemesAccount");

            return Organization.StatusList.ToXmlString();
        }


        private void SetDefaultValues(string StatusType)
        {
            CheckForStatus("Pending", StatusType);
            CheckForStatus("Active", StatusType);
            CheckForStatus("Deactivated", StatusType);
        }

        private void CheckForStatus(string Status, string StatusType)
        {
            var IsStatusExist = Organization.StatusList.Where(e => e.Type.ToLower() == StatusType && e.Name.ToLower() == Status.ToLower()).SingleOrDefault();
            if (IsStatusExist == null)
                Organization.StatusList.Add(new Status() { Name = Status, Type = StatusType });
        }
    }


    public class StatusListAddCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string data)
        {
            Status value = data.ToNewOrData<Status>();
            var selected = (from e in Organization.StatusList where e.Name.ToLower() == value.Name.Trim().ToLower() && e.Type.ToLower() == value.Type.Trim().ToLower() select e).SingleOrDefault();

            if (selected != null)
            {
                return "false";
            }
            else
            {
                Organization.Broker.SetWriteStart();
                Organization.StatusList.Add(value);
                return Organization.StatusList.ToXmlString();
            }
        }
    }

    public class StatusListUpdateCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string data)
        {
            List<Status> item = data.ToNewOrData<List<Status>>();
            var OldValue = (from e in Organization.StatusList where e.Name.ToLower() == item[0].Name.Trim().ToLower() && e.Type.ToLower() == item[0].Type.Trim().ToLower() select e).SingleOrDefault();
            Organization.StatusList.Remove(OldValue);

            var NewValue = (from e in Organization.StatusList where e.Name.ToLower() == item[1].Name.Trim().ToLower() && e.Type.ToLower() == item[1].Type.Trim().ToLower() select e).SingleOrDefault();
            if (Organization.StatusList.Contains(NewValue))
            {
                Organization.StatusList.Add(item[0]);
                return "false";
            }
            else
            {
                Organization.Broker.SetWriteStart();
                Organization.StatusList.Add(item[1]);
                return Organization.StatusList.ToXmlString();
            }
        }
    }

    public class StatusListDeleteCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string data)
        {
            Status value = data.ToNewOrData<Status>();
            var OldValue = (from e in Organization.StatusList where e.Name.ToLower() == value.Name.Trim().ToLower() && e.Type.ToLower() == value.Type.Trim().ToLower() select e).SingleOrDefault();
            Organization.StatusList.Remove(OldValue);

            return Organization.StatusList.ToXmlString();
        }
    }

    #endregion

    #region *** Product List Commands ***

    public class GetProductListCommand : OrganizationCmCommandBase
    {
        private void ProcessProducts()
        {
            foreach (var product in Organization.Products)
            {
                UpdateProductTaskDescription(product);
            }
        }

        private void UpdateProductTaskDescription(ProductEntity product)
        {
            string taskDescription = string.Empty;
            string organizationUnit = string.Empty;
            string accountName = string.Empty;
            string fundCode = string.Empty;

            switch (product.EntityType)
            {
                case OrganizationType.BankAccount:
                    var bankInstitute = Organization.Institution.Where(e => e.ID == product.EntityId.Clid).FirstOrDefault();
                    taskDescription = string.Format("{0} - {1} - {2}", product.EntityType.GetDescription(), bankInstitute.Name, product.BankAccountType.ToUpper());
                    break;
                case OrganizationType.DesktopBrokerAccount:
                    var brokerInstitute = Organization.Institution.Where(e => e.ID == product.EntityId.Clid).FirstOrDefault();
                    taskDescription = string.Format("{0} - {1}", product.EntityType.GetDescription(), brokerInstitute.Name);
                    break;

                case OrganizationType.TermDepositAccount:

                    Oritax.TaxSimp.Common.TermDepositAccountEntity termDepositAccount = null;
                    IBrokerManagedComponent bmc = Organization.Broker.GetCMImplementation(product.EntityId.Clid, product.EntityId.Csid);
                    if (bmc != null)
                    {
                        termDepositAccount = (bmc as OrganizationUnitCM).GetDataStream(0, string.Empty).ToNewOrData<Oritax.TaxSimp.Common.TermDepositAccountEntity>();
                        organizationUnit = (bmc as OrganizationUnitCM).Name;
                    }

                    if (product.IncludedAccounts != null && product.IncludedAccounts.Count > 0)
                    {
                        bmc = Organization.Broker.GetCMImplementation(product.IncludedAccounts[0].Clid, product.IncludedAccounts[0].Csid);
                        if (bmc != null)
                        {
                            accountName = (bmc as OrganizationUnitCM).Name;
                        }
                    }

                    if (organizationUnit == null || organizationUnit == "")
                    {
                        organizationUnit = "Default";
                    }
                    //taskDescription = string.Format("{0} - {1} - {2}", product.EntityType.GetDescription(), organizationUnit, accountName);
                    taskDescription = string.Format("{0} - {1}", product.EntityType.GetDescription(), organizationUnit);
                    break;

                case OrganizationType.ManagedInvestmentSchemesAccount:

                    Guid fundId = new Guid();
                    if (product.FundAccounts != null && product.FundAccounts.Count > 0)
                        fundId = product.FundAccounts[0];
                    bmc = Organization.Broker.GetCMImplementation(product.EntityId.Clid, product.EntityId.Csid);
                    var miaAccount = (bmc as OrganizationUnitCM).GetDataStream(0, string.Empty).ToNewOrData<ManagedInvestmentSchemesAccountEntity>();
                    var fund = miaAccount.FundAccounts.FirstOrDefault(e => e.ID.ToString() == fundId.ToString());
                    if (fund != null)
                        fundCode = fund.Code + " " + fund.Description;
                    taskDescription = string.Format("{0} - {1} - {2}", product.EntityType.GetDescription(), miaAccount.Name, fundCode);
                    break;
            }

            product.TaskDescription = taskDescription;

        }

        public override string DoAction(string value)
        {
            ProcessProducts();
            return Organization.Products.ToXmlString();
        }
    }

    public class ProductListAddCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string data)
        {
            ProductEntity value = data.ToNewOrData<ProductEntity>();
            var selected = (from e in Organization.Products where e.Name.ToLower() == value.Name.Trim().ToLower() select e).SingleOrDefault();

            if (selected != null)
            {
                return "false";
            }
            else
            {
                Organization.Broker.SetWriteStart();
                Organization.Products.Add(value);
                return Organization.Products.ToXmlString();
            }
        }
    }

    public class ProductListUpdateCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string data)
        {
            List<ProductEntity> item = data.ToNewOrData<List<ProductEntity>>();
            var OldValue = (from e in Organization.Products where e.ID == item[0].ID select e).SingleOrDefault();
            Organization.Products.Remove(OldValue);

            var NewValue = (from e in Organization.Products where e.Name.ToLower() == item[1].Name.Trim().ToLower() select e).SingleOrDefault();
            if (Organization.Products.Contains(NewValue))
            {
                Organization.Products.Add(item[0]);
                return "false";
            }
            else
            {
                Organization.Broker.SetWriteStart();
                Organization.Products.Add(item[1]);
                return Organization.Products.ToXmlString();
            }
        }
    }

    public class ProductListDeleteCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string data)
        {
            ProductEntity value = data.ToNewOrData<ProductEntity>();
            var OldValue = (from e in Organization.Products where e.ID == value.ID select e).SingleOrDefault();
            Organization.Products.Remove(OldValue);

            return Organization.Products.ToXmlString();
        }
    }

    #endregion

    #region *** Asset List Commands ***

    public class GetAssetListCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string value)
        {
            return Organization.Assets.ToXmlString();
        }
    }

    public class AssetListAddCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string data)
        {
            AssetEntity value = data.ToNewOrData<AssetEntity>();
            var selected = (from e in Organization.Assets where e.Name.ToLower() == value.Name.Trim().ToLower() select e).SingleOrDefault();

            if (selected != null)
            {
                return "false";
            }
            else
            {
                Organization.Broker.SetWriteStart();
                Organization.Assets.Add(value);
                return Organization.Assets.ToXmlString();
            }
        }
    }

    public class AssetListUpdateCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string data)
        {
            List<AssetEntity> item = data.ToNewOrData<List<AssetEntity>>();
            var OldValue = (from e in Organization.Assets where e.ID == item[0].ID select e).SingleOrDefault();
            Organization.Assets.Remove(OldValue);

            var NewValue = (from e in Organization.Assets where e.Name.ToLower() == item[1].Name.Trim().ToLower() select e).SingleOrDefault();
            if (Organization.Assets.Contains(NewValue))
            {
                Organization.Assets.Add(item[0]);
                return "false";
            }
            else
            {
                Organization.Broker.SetWriteStart();
                Organization.Assets.Add(item[1]);
                return Organization.Assets.ToXmlString();
            }
        }
    }

    public class AssetListDeleteCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string data)
        {
            AssetEntity value = data.ToNewOrData<AssetEntity>();
            var OldValue = (from e in Organization.Assets where e.ID == value.ID select e).SingleOrDefault();
            Organization.Assets.Remove(OldValue);

            return Organization.Assets.ToXmlString();
        }
    }

    #endregion

    #region *** Business Group List Commands ***

    public class GetBusinessGroupListCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string value)
        {
            return Organization.BusinessGroups.ToXmlString();
        }
    }

    public class BusinessGroupListAddCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string data)
        {
            BusinessGroupEntity value = data.ToNewOrData<BusinessGroupEntity>();
            var selected = (from e in Organization.BusinessGroups where e.Name.ToLower() == value.Name.Trim().ToLower() select e).SingleOrDefault();

            if (selected != null)
            {
                return "false";
            }
            else
            {
                Organization.Broker.SetWriteStart();
                Organization.BusinessGroups.Add(value);
                return Organization.BusinessGroups.ToXmlString();
            }
        }
    }

    public class BusinessGroupListUpdateCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string data)
        {
            List<BusinessGroupEntity> item = data.ToNewOrData<List<BusinessGroupEntity>>();
            var OldValue = (from e in Organization.BusinessGroups where e.ID == item[0].ID select e).SingleOrDefault();
            Organization.BusinessGroups.Remove(OldValue);

            var NewValue = (from e in Organization.BusinessGroups where e.Name.ToLower() == item[1].Name.Trim().ToLower() select e).SingleOrDefault();
            if (Organization.BusinessGroups.Contains(NewValue))
            {
                Organization.BusinessGroups.Add(item[0]);
                return "false";
            }
            else
            {
                Organization.Broker.SetWriteStart();
                Organization.BusinessGroups.Add(item[1]);
                return Organization.BusinessGroups.ToXmlString();
            }
        }
    }

    public class BusinessGroupListDeleteCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string data)
        {
            BusinessGroupEntity value = data.ToNewOrData<BusinessGroupEntity>();
            var OldValue = (from e in Organization.BusinessGroups where e.ID == value.ID select e).SingleOrDefault();
            Organization.BusinessGroups.Remove(OldValue);

            return Organization.BusinessGroups.ToXmlString();
        }
    }

    #endregion

    #region *** Model List Commands ***

    public class GetModelListCommand : OrganizationCmCommandBase
    {
        private void ProcessModels(List<ModelEntity> models)
        {
            foreach (var model in models)
            {
                foreach (var asset in model.Assets)
                {
                    foreach (var product in asset.Products)
                    {
                        UpdateProductTaskDescription(product);
                    }
                }
            }
        }

        private void UpdateProductTaskDescription(ProductEntity product)
        {

            var produ=     Organization.Products.Where(prod => prod.ID == product.ID).FirstOrDefault();
            if (produ != null)
            {
                product.ProductSecuritiesId = produ.ProductSecuritiesId;
                product.IsDefaultProductSecurity = produ.IsDefaultProductSecurity;
            }


            string taskDescription = string.Empty;
            string organizationUnit = string.Empty;
            string accountName = string.Empty;
            string fundCode = string.Empty;

            switch (product.EntityType)
            {
                case OrganizationType.BankAccount:
                    var bankInstitute = Organization.Institution.Where(e => e.ID == product.EntityId.Clid).FirstOrDefault();
                    taskDescription = string.Format("{0} - {1} - {2}", product.EntityType.GetDescription(), bankInstitute.Name, product.BankAccountType.ToUpper());
                    break;
                case OrganizationType.DesktopBrokerAccount:
                    var brokerInstitute = Organization.Institution.Where(e => e.ID == product.EntityId.Clid).FirstOrDefault();
                    taskDescription = string.Format("{0} - {1}", product.EntityType.GetDescription(), brokerInstitute.Name);
                    break;

                case OrganizationType.TermDepositAccount:

                    Oritax.TaxSimp.Common.TermDepositAccountEntity termDepositAccount = null;
                    IBrokerManagedComponent bmc = Organization.Broker.GetCMImplementation(product.EntityId.Clid, product.EntityId.Csid);
                    if (bmc != null)
                    {
                        termDepositAccount = (bmc as OrganizationUnitCM).GetDataStream(0, string.Empty).ToNewOrData<Oritax.TaxSimp.Common.TermDepositAccountEntity>();
                        organizationUnit = (bmc as OrganizationUnitCM).Name;
                    }

                    if (product.IncludedAccounts != null && product.IncludedAccounts.Count > 0)
                    {
                        bmc = Organization.Broker.GetCMImplementation(product.IncludedAccounts[0].Clid, product.IncludedAccounts[0].Csid);
                        if (bmc != null)
                        {
                            accountName = (bmc as OrganizationUnitCM).Name;
                        }
                    }

                    //taskDescription = string.Format("{0} - {1} - {2}", product.EntityType.GetDescription(), organizationUnit, accountName);
                    
                    if (organizationUnit == null || organizationUnit == "")
                    {
                        organizationUnit = "Default";
                    }
                    taskDescription = string.Format("{0} - {1}", product.EntityType.GetDescription(), organizationUnit);
                    
                    break;

                case OrganizationType.ManagedInvestmentSchemesAccount:

                    Guid fundId = new Guid();
                    if (product.FundAccounts != null && product.FundAccounts.Count > 0)
                        fundId = product.FundAccounts[0];
                    bmc = Organization.Broker.GetCMImplementation(product.EntityId.Clid, product.EntityId.Csid);
                    var miaAccount = (bmc as OrganizationUnitCM).GetDataStream(0, string.Empty).ToNewOrData<ManagedInvestmentSchemesAccountEntity>();
                    var fund = miaAccount.FundAccounts.FirstOrDefault(e => e.ID.ToString() == fundId.ToString());
                    if (fund != null)
                        fundCode = fund.Code + " " + fund.Description;
                    taskDescription = string.Format("{0} - {1} - {2}", product.EntityType.GetDescription(), miaAccount.Name, fundCode);
                    break;
            }

            product.TaskDescription = taskDescription;

        }

        public override string DoAction(string value)
        {
            ProcessModels(Organization.Model);

            return Organization.Model.ToXmlString();
        }
    }

    public class ModelListAddCommand : OrganizationCmCommandBase
    {
        private string GetMaxProgramCode()
        {
            int newCode = Organization.MaxProgramCode.Code;
            newCode++;
            return newCode.ToString().PadLeft(4, '0');
        }

        public override string DoAction(string data)
        {
            ModelEntity value = data.ToNewOrData<ModelEntity>();
            var selected = (from e in Organization.Model where e.Name.ToLower() == value.Name.Trim().ToLower() select e).SingleOrDefault();
            if (selected != null) { return "false"; }

            value.ProgramCode = GetMaxProgramCode();
            Organization.Broker.SetWriteStart();
            Organization.Model.Add(value);
            Organization.MaxProgramCode = new ProgramCode(Convert.ToInt32(value.ProgramCode));
            return Organization.Model.ToXmlString();
        }
    }

    public class ModelListUpdateCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string data)
        {
            List<ModelEntity> item = data.ToNewOrData<List<ModelEntity>>();
            var OldValue = (from e in Organization.Model where e.ID == item[0].ID select e).SingleOrDefault();
            Organization.Model.Remove(OldValue);
            var NewValue = (from e in Organization.Model where e.Name.ToLower() == item[1].Name.Trim().ToLower() select e).SingleOrDefault();

            if (Organization.Model.Contains(NewValue)) { return "false"; }

            Organization.Broker.SetWriteStart();
            Organization.Model.Add(item[1]);
            return Organization.Model.ToXmlString();

        }
    }

    public class ModelListDeleteCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string data)
        {
            ModelEntity value = data.ToNewOrData<ModelEntity>();
            var OldValue = (from e in Organization.Model where e.ID == value.ID select e).SingleOrDefault();
            Organization.Model.Remove(OldValue);

            return Organization.Model.ToXmlString();
        }
    }

    public class FillModelTaskDescriptionCommand : OrganizationCmCommandBase
    {
        private string GetTaskDesc_BankAccount(ProductEntity product)
        {
            var institute = Organization.Institution.Where(e => e.ID == product.EntityId.Clid).FirstOrDefault();
            return String.Format("{0} - {1} - {2}", product.EntityType.GetDescription(), institute.Name, product.BankAccountType);
        }
        private string GetTaskDesc_DesktopBrokerAccount(ProductEntity product)
        {
            return String.Format("{0}", product.EntityType.GetDescription());
        }
        private string GetTaskDesc_TermDepositAccount(ProductEntity product)
        {
            return String.Format("{0}", product.EntityType.GetDescription());
        }
        private string GetTaskDesc_ManagedInvestmentSchemesAccount(ProductEntity product)
        {
            return String.Format("{0}", product.EntityType.GetDescription());
        }

        public override string DoAction(string data)
        {
            ModelEntity model = data.ToNewOrData<ModelEntity>();
            if (model == null)
            {
                model = Organization.Model.Where(e => e.ProgramCode.ToLower() == data.ToLower()).FirstOrDefault(); ;
            }

            foreach (var asset in model.Assets)
            {
                foreach (var product in asset.Products)
                {
                    switch (product.EntityType)
                    {
                        case OrganizationType.BankAccount:
                            product.TaskDescription = GetTaskDesc_BankAccount(product);
                            break;
                        case OrganizationType.DesktopBrokerAccount:
                            product.TaskDescription = GetTaskDesc_DesktopBrokerAccount(product);
                            break;
                        case OrganizationType.ManagedInvestmentSchemesAccount:
                            product.TaskDescription = GetTaskDesc_ManagedInvestmentSchemesAccount(product);
                            break;
                        case OrganizationType.TermDepositAccount:
                            product.TaskDescription = GetTaskDesc_TermDepositAccount(product);
                            break;
                        default:
                            break;
                    }
                }
            }

            return model.ToXmlString();
        }
    }

    public class FillTaskStatusCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string data)
        {
            List<TaskEntity> list = data.ToNewOrData<List<TaskEntity>>();

            foreach (var task in list)
            {
                IBrokerManagedComponent bmc = Organization.Broker.GetCMImplementation(task.LinkedEntity.Clid, task.LinkedEntity.Csid);
                if (bmc != null)
                    task.Status = (bmc as OrganizationUnitCM).OrganizationStatus;
            }
            return list.ToXmlString();
        }
    }


    public class GetDefaultModelListCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string data)
        {
            List<ModelEntity> list = new List<ModelEntity>();

            list.Add((from e in Organization.Model where e.ServiceType == ServiceTypes.DoItWithMe select e).SingleOrDefault());
            list.Add((from e in Organization.Model where e.ServiceType == ServiceTypes.DoItYourSelf select e).SingleOrDefault());
            list.Add((from e in Organization.Model where e.ServiceType == ServiceTypes.DoItForMe select e).SingleOrDefault());

            return list.ToXmlString();
        }
    }

    #endregion

    #region *** Institution List Commands ***

    public class GetInstitutionListCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string value)
        {
            return Organization.Institution.ToXmlString();
        }
    }

    public class InstitutionListAddCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string data)
        {
            InstitutionEntity value = data.ToNewOrData<InstitutionEntity>();
            var selected = (from e in Organization.Institution where e.Name.ToLower() == value.Name.Trim().ToLower() select e).SingleOrDefault();

            if (selected != null)
            {
                return "false";
            }
            else
            {
                Organization.Broker.SetWriteStart();
                Organization.Institution.Add(value);
                return Organization.Institution.ToXmlString();
            }
        }
    }

    public class InstitutionListUpdateCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string data)
        {
            List<InstitutionEntity> item = data.ToNewOrData<List<InstitutionEntity>>();
            var OldValue = (from e in Organization.Institution where e.ID == item[0].ID select e).SingleOrDefault();
            Organization.Institution.Remove(OldValue);

            var NewValue = (from e in Organization.Institution where e.Name.ToLower() == item[1].Name.Trim().ToLower() select e).SingleOrDefault();
            if (Organization.Institution.Contains(NewValue))
            {
                Organization.Institution.Add(item[0]);
                return "false";
            }
            else
            {
                Organization.Broker.SetWriteStart();
                Organization.Institution.Add(item[1]);
                return Organization.Institution.ToXmlString();
            }
        }
    }

    public class InstitutionListDeleteCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string data)
        {
            InstitutionEntity value = data.ToNewOrData<InstitutionEntity>();
            var OldValue = (from e in Organization.Institution where e.ID == value.ID select e).SingleOrDefault();
            Organization.Institution.Remove(OldValue);

            return Organization.Institution.ToXmlString();
        }
    }

    #endregion

    #region *** SMTP Settings Commands ***

    public class GetSmtpSettingsCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string value)
        {
            return Organization.SmtpSettings.ToXmlString();
        }
    }

    public class UpdateSmtpSettingsCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string data)
        {
            Organization.Broker.SetWriteStart();
            Organization.SmtpSettings = data.ToNewOrData<SmtpServerEntity>();
            return Organization.SmtpSettings.ToXmlString();
        }
    }

    #endregion

    #region *** Organization Chart Commands ***

    public class GetOrganizationChartCommand : OrganizationCmCommandBase
    {
        #region OrganizationUnits

        private string GetTypeName(string type, IBrokerManagedComponent bmc)
        {
            string fullname = string.Empty;

            if (type.ToLower() == "advisor" || type.ToLower() == "adviser")
                fullname = bmc.GetDataStream(999, string.Empty);
            else if (type.ToLower() == "individual")
                fullname = bmc.GetDataStream(999, string.Empty);
            else
                fullname = bmc.Name;

            return fullname;
        }

        private List<OrganizationUnit> GetAllUnits(string username)
        {

            List<OrganizationUnit> units = new List<OrganizationUnit>();
            OrganizationUnit unit = null;
            OrganizationListAction.ForEach(username, Organization, (csid, logical, bmc) =>
            {
                unit = new OrganizationUnit
                {
                    Clid = logical.CLID,
                    Csid = csid,
                    Name = GetTypeName(logical.CMTypeName, bmc),//bmc.Name,
                    Type = logical.CMTypeName,
                    Data = string.Empty,
                    IsInvestableClient = (bmc as OrganizationUnitCM).IsInvestableClient,
                    OrganizationStatus = (bmc as OrganizationUnitCM).OrganizationStatus,
                    ClientId = (bmc as OrganizationUnitCM).ClientId
                };
                units.Add(unit);
            });

            return units;
        }

        #endregion

        public override string DoAction(string username)
        {
            List<OrganizationChart> chart = new List<OrganizationChart>();
            List<OrganizationUnit> units = GetAllUnits(username);
            OrganizationChart clientparent = new OrganizationChart();
            List<OrganizationChart> clients = new List<OrganizationChart>();

            IOrganizationChartData component = null;
            ICalculationModule module = null;
            foreach (var each in units.Where(e => e.Type.ToLower().StartsWith("client")))
            {
                module = Organization.Broker.GetCMImplementation(each.Clid, each.Csid);
                if (module != null && module is IOrganizationChartData)
                {
                    component = module as IOrganizationChartData;
                    clients.Add(component.GetOrganizationChart());
                }
            }
            clientparent.NodeName = "Clients";
            clientparent.Subordinates.AddRange(clients);
            chart.Add(clientparent);

            return chart.ToXmlString();
        }
    }

    #endregion


    #region *** Model Chart Commands ***
    //public class GetChartForModelCommand : OrganizationCmCommandBase
    //{
    //    #region OrganizationUnits

    //    private string GetTypeName(string type, IBrokerManagedComponent bmc)
    //    {
    //        string fullname = string.Empty;

    //        if (type.ToLower() == "advisor" || type.ToLower() == "adviser")
    //            fullname = bmc.GetDataStream(999, string.Empty);
    //        else if (type.ToLower() == "individual")
    //            fullname = bmc.GetDataStream(999, string.Empty);
    //        else
    //            fullname = bmc.Name;

    //        return fullname;
    //    }

    //    private List<OrganizationUnit> GetAllUnits(string username)
    //    {

    //        List<OrganizationUnit> units = new List<OrganizationUnit>();
    //        OrganizationUnit unit = null;
    //        OrganizationListAction.ForEach(username, Organization, (csid, logical, bmc) =>
    //        {
    //            unit = new OrganizationUnit
    //            {
    //                Clid = logical.CLID,
    //                Csid = csid,
    //                Name = GetTypeName(logical.CMTypeName, bmc),//bmc.Name,
    //                Type = logical.CMTypeName,
    //                Data = string.Empty,
    //                IsInvestableClient = (bmc as OrganizationUnitCM).IsInvestableClient,
    //                OrganizationStatus = (bmc as OrganizationUnitCM).OrganizationStatus,
    //                ClientId = (bmc as OrganizationUnitCM).ClientId
    //            };
    //            units.Add(unit);
    //        });

    //        return units;
    //    }

    //    #endregion

    //    public override string DoAction(List<OrganizationUnit> units)
    //    {

    //        List<OrganizationChart> chart = new List<OrganizationChart>();
    //        //List<OrganizationUnit> units = GetAllUnits(username);
    //        OrganizationChart clientparent = new OrganizationChart();
    //        List<OrganizationChart> clients = new List<OrganizationChart>();

    //        IOrganizationChartData component = null;
    //        ICalculationModule module = null;
    //        foreach (var each in units.Where(e => e.Type.ToLower().StartsWith("client")))
    //        {
    //            module = Organization.Broker.GetCMImplementation(each.Clid, each.Csid);
    //            if (module != null && module is IOrganizationChartData)
    //            {
    //                component = module as IOrganizationChartData;
    //                clients.Add(component.GetOrganizationChart());
    //            }
    //        }
    //        clientparent.NodeName = "Clients";
    //        clientparent.Subordinates.AddRange(clients);
    //        chart.Add(clientparent);

    //        return chart.ToXmlString();
    //    }
    //}
    #endregion

    #region *** Association Commands ***

    public class GetAssociationCommand : OrganizationCmCommandBase
    {
        private string GetTypeName(string type, IBrokerManagedComponent bmc)
        {
            string fullname = string.Empty;

            if (type.ToLower() == "advisor" || type.ToLower() == "adviser")
                fullname = bmc.GetDataStream(999, string.Empty);
            else if (type.ToLower() == "individual")
                fullname = bmc.GetDataStream(999, string.Empty);
            else
                fullname = bmc.Name;

            return fullname;
        }

        private List<OrganizationUnit> GetAllUnits(string username)
        {

            List<OrganizationUnit> units = new List<OrganizationUnit>();
            OrganizationUnit unit = null;
            OrganizationListAction.ForEach(username, Organization, (csid, logical, bmc) =>
            {
                unit = new OrganizationUnit
                {
                    Clid = logical.CLID,
                    Csid = csid,
                    Name = GetTypeName(logical.CMTypeName, bmc),//bmc.Name,
                    Type = logical.CMTypeName,
                    Data = string.Empty,
                    IsInvestableClient = (bmc as OrganizationUnitCM).IsInvestableClient,
                    OrganizationStatus = (bmc as OrganizationUnitCM).OrganizationStatus,
                    ClientId = (bmc as OrganizationUnitCM).ClientId
                };
                units.Add(unit);
            });

            return units;
        }

        public override string DoAction(string value)
        {
            HaveAssociation identity = value.ToNewOrData<HaveAssociation>();
            List<OrganizationChart> chart = new List<OrganizationChart>();
            //OrganizationChart Assosiatee = new OrganizationChart();
            //Assosiatee.NodeName = GetAssosiateName(identity);
            //Assosiatee.TypeName = GetDisplayNameByEnum(identity.Otype);
            List<OrganizationUnit> units = GetAllUnits(identity.Username);
            ICalculationModule module = null;
            foreach (var each in units)
            {
                module = Organization.Broker.GetCMImplementation(each.Clid, each.Csid);
                if (module != null && module is IHaveAssociation)
                {
                    OrganizationChart association = (module as IHaveAssociation).GetAssociation(identity, identity.Otype);
                    if (association != null && association.NodeName != string.Empty)
                    {
                        //association.TypeName = GetDisplayNameByEnum(GetEnumByName(each.Type));
                        //if (Assosiatee.NodeName == association.NodeName && Assosiatee.TypeName == association.TypeName)
                        //{
                        //  Assosiatee = association;
                        //}
                        //else
                        //{
                        //  Assosiatee.Subordinates.Add(association);
                        //}
                        chart.Add(association);
                    }
                }
            }
            return chart.ToXmlString();
        }

        private string GetAssosiateName(IIdentityCM identity)
        {
            IBrokerManagedComponent bmc = Organization.Broker.GetCMImplementation(identity.Clid, identity.Csid);
            return GetTypeName(bmc.TypeName, bmc);
        }

        public OrganizationType GetEnumByName(string value)
        {
            OrganizationType result = OrganizationType.Trust;
            switch (value.ToLower())
            {
                case "corporate":
                    result = OrganizationType.Corporate;
                    break;
                case "trust":
                    result = OrganizationType.Trust;
                    break;
                case "clientindividual":
                    result = OrganizationType.ClientIndividual;
                    break;
                case "clienteclipsesuper":
                    result = OrganizationType.ClientEClipseSuper;
                    break;
                case "dealergroup":
                    result = OrganizationType.DealerGroup;
                    break;
                case "memberfirm":
                    result = OrganizationType.MemberFirm;
                    break;
                case "principalpractice":
                    result = OrganizationType.PrincipalPractice;
                    break;
                case "advisor":
                case "adviser":
                    result = OrganizationType.Adviser;
                    break;
                case "individual":
                    result = OrganizationType.Individual;
                    break;
                case "clientcorporationprivate":
                    result = OrganizationType.ClientCorporationPrivate;
                    break;
                case "clientcorporationpublic":
                    result = OrganizationType.ClientCorporationPublic;
                    break;
                case "clientothertrustscorporate":
                    result = OrganizationType.ClientOtherTrustsCorporate;
                    break;
                case "clientothertrustsindividual":
                    result = OrganizationType.ClientOtherTrustsIndividual;
                    break;
                case "clientsmsfcorporatetrustee":
                    result = OrganizationType.ClientSMSFCorporateTrustee;
                    break;
                case "clientsmsfindividualtrustee":
                    result = OrganizationType.ClientSMSFIndividualTrustee;
                    break;
                case "bankaccount":
                    result = OrganizationType.BankAccount;
                    break;
                case "ifa":
                    result = OrganizationType.IFA;
                    break;
                case "clientsoletrader":
                    result = OrganizationType.ClientSoleTrader;
                    break;
                default:
                    break;
            }
            return result;
        }

        public string GetDisplayNameByEnum(OrganizationType oType)
        {
            string xml = string.Empty;
            switch (oType)
            {
                case OrganizationType.Corporate:
                    xml = "Corporate";
                    break;
                case OrganizationType.Trust:
                    xml = "Trust";
                    break;
                case OrganizationType.ClientIndividual:
                    xml = "Individuals Client";
                    break;
                case OrganizationType.ClientEClipseSuper:
                    xml = "e-Clipse Super";
                    break;
                case OrganizationType.DealerGroup:
                    xml = "Dealer Group";
                    break;
                case OrganizationType.MemberFirm:
                    xml = "Member Firm";
                    break;
                case OrganizationType.Adviser:
                    xml = "Adviser";
                    break;
                case OrganizationType.Individual:
                    xml = "Individual";
                    break;
                case OrganizationType.ClientCorporationPrivate:
                    xml = "Corporation - Private";
                    break;
                case OrganizationType.ClientCorporationPublic:
                    xml = "Corporation - Public";
                    break;
                case OrganizationType.ClientOtherTrustsCorporate:
                    xml = "Other Trusts - Corporate";
                    break;
                case OrganizationType.ClientOtherTrustsIndividual:
                    xml = "Other Trusts - Individual";
                    break;
                case OrganizationType.ClientSMSFCorporateTrustee:
                    xml = "SMSF – Corporate Trustee";
                    break;
                case OrganizationType.ClientSMSFIndividualTrustee:
                    xml = "SMSF – Individual Trustee/s";
                    break;
                case OrganizationType.BankAccount:
                    xml = "Bank Account";
                    break;
                case OrganizationType.PrincipalPractice:
                    xml = "Principal Practice";
                    break;
                case OrganizationType.IFA:
                    xml = "IFA";
                    break;
                case OrganizationType.ClientSoleTrader:
                    xml = "Sole Trader";
                    break;
                default:
                    break;
            }
            return xml;
        }
    }

    public class GetModelAssociationCommand : OrganizationCmCommandBase
    {
        private string GetTypeName(string type, IBrokerManagedComponent bmc)
        {
            string fullname = string.Empty;

            if (type.ToLower() == "advisor" || type.ToLower() == "adviser")
                fullname = bmc.GetDataStream(999, string.Empty);
            else if (type.ToLower() == "individual")
                fullname = bmc.GetDataStream(999, string.Empty);
            else
                fullname = bmc.Name;

            return fullname;
        }

        private List<OrganizationUnit> GetAllUnits(string username)
        {

            List<OrganizationUnit> units = new List<OrganizationUnit>();
            OrganizationUnit unit = null;
            OrganizationListAction.ForEach(username, Organization, (csid, logical, bmc) =>
            {
                unit = new OrganizationUnit
                {
                    Clid = logical.CLID,
                    Csid = csid,
                    Name = GetTypeName(logical.CMTypeName, bmc),//bmc.Name,
                    Type = logical.CMTypeName,
                    Data = string.Empty,
                    IsInvestableClient = (bmc as OrganizationUnitCM).IsInvestableClient,
                    OrganizationStatus = (bmc as OrganizationUnitCM).OrganizationStatus,
                    ClientId = (bmc as OrganizationUnitCM).ClientId
                };
                units.Add(unit);
            });

            return units;
        }

        public override string DoAction(string value)
        {

            HaveAssociation identity = value.ToNewOrData<HaveAssociation>();
            var model = Organization.Model.Where(w => w.ID == identity.Clid).FirstOrDefault();
            List<OrganizationChart> chart = new List<OrganizationChart>();
            List<OrganizationChart> clients = new List<OrganizationChart>();
           
             OrganizationChart clientparent = new OrganizationChart();
            List<OrganizationUnit> units = GetAllUnits(identity.Username);

            IOrganizationChartData component = null;            
            ICalculationModule module = null;
            foreach (var each in units.Where(e => e.Type.ToLower().StartsWith("client")))
            {
                module = Organization.Broker.GetCMImplementation(each.Clid, each.Csid);
                //module is IHaveAssociation &&
                //&& module is IOrganizationChartData
                

                if (module != null &&  each.IsInvestableClient)
                {

                    string entity=module.GetDataStream(-999, null);
                    component = module as IOrganizationChartData;

                    ClientIndividualEntity CIE = null;
                    CorporateEntity ce = null;


                    if (module.TypeName.ToLower().Contains("corporate") == false && module.TypeName.ToLower().Contains("corporation") == false && module.TypeName.ToLower().Contains("sole") == false)
                    {
                        CIE = entity.ToNewOrData<ClientIndividualEntity>();
                        foreach (var count in CIE.ListAccountProcess)
                        {
                            if (count.ModelID == model.ID)
                            {
                                OrganizationChart association = (module as IHaveAssociation).GetAssociation(new IdentityCM { Clid = each.Clid, Csid = each.Csid }, GetEnumByName(each.Type));
                                //OrganizationChart association = (module as IHaveAssociation);
                                if (clients.Where(c => c.Clid == each.Clid && c.Csid == each.Csid).FirstOrDefault() == null)
                                    {
                                        //OrganizationChart item;
                                        //item = component.GetOrganizationChart();
                                        //item.Subordinates = null;
                                        //clients.Add(item);
                                        association.Subordinates = null;
                                        //clients.Add(association);
                                        if (GetEnumByName(association.TypeName).ToString().ToLower().StartsWith("client"))
                                        {
                                            clients.Add(association);
                                        }
                                    }
                                
                            }
                        }
                    }
                    else
                    {
                        ce = entity.ToNewOrData<CorporateEntity>();
                        foreach (var count in ce.ListAccountProcess)
                        {
                            if (count.ModelID == model.ID)
                            {
                                OrganizationChart association = (module as IHaveAssociation).GetAssociation(new IdentityCM { Clid = each.Clid, Csid = each.Csid }, GetEnumByName(each.Type));

                                if (clients.Where(c => c.Clid == each.Clid && c.Csid == each.Csid).FirstOrDefault() == null)
                                    {
                                        //clients.Add(component.GetOrganizationChart());
                                        //OrganizationChart item;
                                        //item = component.GetOrganizationChart();
                                        //item.Subordinates = null;
                                        //clients.Add(item);
                                        association.Subordinates = null;
                                        if (GetEnumByName(association.TypeName).ToString().ToLower().StartsWith("client"))
                                        {
                                            clients.Add(association);
                                        }
                                    }
                                
                            }
                        }
                    }
                }
            }
            clientparent.NodeName = model.Name;
            clientparent.Subordinates.AddRange(clients);
            clientparent.Clid = model.ID;
            clientparent.TypeName = "Model";
            chart.Add(clientparent);
            return chart.ToXmlString();
        }

        private string GetAssosiateName(IIdentityCM identity)
        {
            IBrokerManagedComponent bmc = Organization.Broker.GetCMImplementation(identity.Clid, identity.Csid);
            return GetTypeName(bmc.TypeName, bmc);
        }

        public OrganizationType GetEnumByName(string value)
        {
            OrganizationType result = OrganizationType.Trust;
            switch (value.ToLower())
            {
                case "corporate":
                    result = OrganizationType.Corporate;
                    break;
                case "trust":
                    result = OrganizationType.Trust;
                    break;
                case "clientindividual":
                    result = OrganizationType.ClientIndividual;
                    break;
                case "clienteclipsesuper":
                    result = OrganizationType.ClientEClipseSuper;
                    break;
                case "dealergroup":
                    result = OrganizationType.DealerGroup;
                    break;
                case "memberfirm":
                    result = OrganizationType.MemberFirm;
                    break;
                case "principalpractice":
                    result = OrganizationType.PrincipalPractice;
                    break;
                case "advisor":
                case "adviser":
                    result = OrganizationType.Adviser;
                    break;
                case "individual":
                    result = OrganizationType.Individual;
                    break;
                case "clientcorporationprivate":
                    result = OrganizationType.ClientCorporationPrivate;
                    break;
                case "clientcorporationpublic":
                    result = OrganizationType.ClientCorporationPublic;
                    break;
                case "clientothertrustscorporate":
                    result = OrganizationType.ClientOtherTrustsCorporate;
                    break;
                case "clientothertrustsindividual":
                    result = OrganizationType.ClientOtherTrustsIndividual;
                    break;
                case "clientsmsfcorporatetrustee":
                    result = OrganizationType.ClientSMSFCorporateTrustee;
                    break;
                case "clientsmsfindividualtrustee":
                    result = OrganizationType.ClientSMSFIndividualTrustee;
                    break;
                case "bankaccount":
                    result = OrganizationType.BankAccount;
                    break;
                case "ifa":
                    result = OrganizationType.IFA;
                    break;
                case "clientsoletrader":
                    result = OrganizationType.ClientSoleTrader;
                    break;
                case "model":
                    result = OrganizationType.Model;
                    break;
                default:
                    break;
            }
            return result;
        }

        public string GetDisplayNameByEnum(OrganizationType oType)
        {
            string xml = string.Empty;
            switch (oType)
            {
                case OrganizationType.Corporate:
                    xml = "Corporate";
                    break;
                case OrganizationType.Trust:
                    xml = "Trust";
                    break;
                case OrganizationType.ClientIndividual:
                    xml = "Individuals Client";
                    break;
                case OrganizationType.ClientEClipseSuper:
                    xml = "e-Clipse Super";
                    break;
                case OrganizationType.DealerGroup:
                    xml = "Dealer Group";
                    break;
                case OrganizationType.MemberFirm:
                    xml = "Member Firm";
                    break;
                case OrganizationType.Adviser:
                    xml = "Adviser";
                    break;
                case OrganizationType.Individual:
                    xml = "Individual";
                    break;
                case OrganizationType.ClientCorporationPrivate:
                    xml = "Corporation - Private";
                    break;
                case OrganizationType.ClientCorporationPublic:
                    xml = "Corporation - Public";
                    break;
                case OrganizationType.ClientOtherTrustsCorporate:
                    xml = "Other Trusts - Corporate";
                    break;
                case OrganizationType.ClientOtherTrustsIndividual:
                    xml = "Other Trusts - Individual";
                    break;
                case OrganizationType.ClientSMSFCorporateTrustee:
                    xml = "SMSF – Corporate Trustee";
                    break;
                case OrganizationType.ClientSMSFIndividualTrustee:
                    xml = "SMSF – Individual Trustee/s";
                    break;
                case OrganizationType.BankAccount:
                    xml = "Bank Account";
                    break;
                case OrganizationType.PrincipalPractice:
                    xml = "Principal Practice";
                    break;
                case OrganizationType.IFA:
                    xml = "IFA";
                    break;
                case OrganizationType.ClientSoleTrader:
                    xml = "Sole Trader";
                    break;
                case OrganizationType.Model:
                    xml = "Model";
                    break;
                default:
                    break;
            }
            return xml;
        }
    }

    public class GetAllAssociationCommand : OrganizationCmCommandBase
    {
        private string GetTypeName(string type, IBrokerManagedComponent bmc)
        {
            string fullname = string.Empty;

            if (type.ToLower() == "advisor" || type.ToLower() == "adviser")
                fullname = bmc.GetDataStream(999, string.Empty);
            else if (type.ToLower() == "individual")
                fullname = bmc.GetDataStream(999, string.Empty);
            else
                fullname = bmc.Name;

            return fullname;
        }

        private List<OrganizationUnit> GetAllUnits(string username)
        {

            List<OrganizationUnit> units = new List<OrganizationUnit>();
            OrganizationUnit unit = null;
            OrganizationListAction.ForEach(username, Organization, (csid, logical, bmc) =>
            {
                unit = new OrganizationUnit
                {
                    Clid = logical.CLID,
                    Csid = csid,
                    Name = GetTypeName(logical.CMTypeName, bmc),//bmc.Name,
                    Type = logical.CMTypeName,
                    Data = string.Empty,
                    IsInvestableClient = (bmc as OrganizationUnitCM).IsInvestableClient,
                    OrganizationStatus = (bmc as OrganizationUnitCM).OrganizationStatus,
                    ClientId = (bmc as OrganizationUnitCM).ClientId
                };
                units.Add(unit);
            });

            return units;
        }

        public override string DoAction(string value)
        {
            HaveAssociation identity = value.ToNewOrData<HaveAssociation>();
            List<OrganizationChart> chart = new List<OrganizationChart>();
            OrganizationChart Assosiatee = new OrganizationChart();
            Assosiatee.NodeName = GetAssosiateName(identity);
            Assosiatee.TypeName = GetDisplayNameByEnum(identity.Otype);
            List<OrganizationUnit> units = GetAllUnits(identity.Username);
            ICalculationModule module = null;
            foreach (var each in units)
            {
                module = Organization.Broker.GetCMImplementation(each.Clid, each.Csid);
                if (module != null && module is IHaveAssociation)
                {
                    OrganizationChart association = (module as IHaveAssociation).GetAssociation(identity, identity.Otype);
                    if (association != null && association.NodeName != string.Empty)
                    {
                        association.TypeName = GetDisplayNameByEnum(GetEnumByName(each.Type));
                        Assosiatee.Subordinates.Add(association);
                    }
                }
            }
            chart.Add(Assosiatee);
            return chart.ToXmlString();
        }

        private string GetAssosiateName(IIdentityCM identity)
        {
            IBrokerManagedComponent bmc = Organization.Broker.GetCMImplementation(identity.Clid, identity.Csid);
            return GetTypeName(bmc.TypeName, bmc);
        }

        public OrganizationType GetEnumByName(string value)
        {
            OrganizationType result = OrganizationType.Trust;
            switch (value.ToLower())
            {
                case "corporate":
                    result = OrganizationType.Corporate;
                    break;
                case "trust":
                    result = OrganizationType.Trust;
                    break;
                case "clientindividual":
                    result = OrganizationType.ClientIndividual;
                    break;
                case "clienteclipsesuper":
                    result = OrganizationType.ClientEClipseSuper;
                    break;
                case "dealergroup":
                    result = OrganizationType.DealerGroup;
                    break;
                case "memberfirm":
                    result = OrganizationType.MemberFirm;
                    break;
                case "principalpractice":
                    result = OrganizationType.PrincipalPractice;
                    break;
                case "advisor":
                case "adviser":
                    result = OrganizationType.Adviser;
                    break;
                case "individual":
                    result = OrganizationType.Individual;
                    break;
                case "clientcorporationprivate":
                    result = OrganizationType.ClientCorporationPrivate;
                    break;
                case "clientcorporationpublic":
                    result = OrganizationType.ClientCorporationPublic;
                    break;
                case "clientothertrustscorporate":
                    result = OrganizationType.ClientOtherTrustsCorporate;
                    break;
                case "clientothertrustsindividual":
                    result = OrganizationType.ClientOtherTrustsIndividual;
                    break;
                case "clientsmsfcorporatetrustee":
                    result = OrganizationType.ClientSMSFCorporateTrustee;
                    break;
                case "clientsmsfindividualtrustee":
                    result = OrganizationType.ClientSMSFIndividualTrustee;
                    break;
                case "bankaccount":
                    result = OrganizationType.BankAccount;
                    break;
                case "ifa":
                    result = OrganizationType.IFA;
                    break;
                case "clientsoletrader":
                    result = OrganizationType.ClientSoleTrader;
                    break;
                default:
                    break;
            }
            return result;
        }

        public string GetDisplayNameByEnum(OrganizationType oType)
        {
            string xml = string.Empty;
            switch (oType)
            {
                case OrganizationType.Corporate:
                    xml = "Corporate";
                    break;
                case OrganizationType.Trust:
                    xml = "Trust";
                    break;
                case OrganizationType.ClientIndividual:
                    xml = "Individuals Client";
                    break;
                case OrganizationType.ClientEClipseSuper:
                    xml = "e-Clipse Super";
                    break;
                case OrganizationType.DealerGroup:
                    xml = "Dealer Group";
                    break;
                case OrganizationType.MemberFirm:
                    xml = "Member Firm";
                    break;
                case OrganizationType.Adviser:
                    xml = "Adviser";
                    break;
                case OrganizationType.Individual:
                    xml = "Individual";
                    break;
                case OrganizationType.ClientCorporationPrivate:
                    xml = "Corporation - Private";
                    break;
                case OrganizationType.ClientCorporationPublic:
                    xml = "Corporation - Public";
                    break;
                case OrganizationType.ClientOtherTrustsCorporate:
                    xml = "Other Trusts - Corporate";
                    break;
                case OrganizationType.ClientOtherTrustsIndividual:
                    xml = "Other Trusts - Individual";
                    break;
                case OrganizationType.ClientSMSFCorporateTrustee:
                    xml = "SMSF – Corporate Trustee";
                    break;
                case OrganizationType.ClientSMSFIndividualTrustee:
                    xml = "SMSF – Individual Trustee/s";
                    break;
                case OrganizationType.BankAccount:
                    xml = "Bank Account";
                    break;
                case OrganizationType.PrincipalPractice:
                    xml = "Principal Practice";
                    break;
                case OrganizationType.IFA:
                    xml = "IFA";
                    break;
                case OrganizationType.ClientSoleTrader:
                    xml = "Sole Trader";
                    break;
                default:
                    break;
            }
            return xml;
        }
    }

    public class GetAllClientWithoutAdviser : OrganizationCmCommandBase
    {
        private string GetTypeName(string type, IBrokerManagedComponent bmc)
        {
            string fullname = string.Empty;

            if (type.ToLower() == "advisor" || type.ToLower() == "adviser")
                fullname = bmc.GetDataStream(999, string.Empty);
            else if (type.ToLower() == "individual")
                fullname = bmc.GetDataStream(999, string.Empty);
            else
                fullname = bmc.Name;

            return fullname;
        }

        private List<OrganizationUnit> GetAllUnits(string username)
        {

            List<OrganizationUnit> units = new List<OrganizationUnit>();
            OrganizationUnit unit = null;
            OrganizationListAction.ForEach(username, Organization, (csid, logical, bmc) =>
            {
                unit = new OrganizationUnit
                {
                    Clid = logical.CLID,
                    Csid = csid,
                    Name = GetTypeName(logical.CMTypeName, bmc),//bmc.Name,
                    Type = logical.CMTypeName,
                    Data = string.Empty,
                    IsInvestableClient = (bmc as OrganizationUnitCM).IsInvestableClient,
                    OrganizationStatus = (bmc as OrganizationUnitCM).OrganizationStatus,
                    ClientId = (bmc as OrganizationUnitCM).ClientId,
                    HasAdviser = (bmc as OrganizationUnitCM).HasAdviser
                };
                if(unit.IsInvestableClient && !unit.HasAdviser)
                    units.Add(unit);
            });

            return units;
        }

        public override string DoAction(string value)
        {
            HaveAssociation identity = value.ToNewOrData<HaveAssociation>();
            List<OrganizationUnit> units = GetAllUnits(identity.Username);

            return units.ToXmlString();
        }

        private string GetAssosiateName(IIdentityCM identity)
        {
            IBrokerManagedComponent bmc = Organization.Broker.GetCMImplementation(identity.Clid, identity.Csid);
            return GetTypeName(bmc.TypeName, bmc);
        }

        public OrganizationType GetEnumByName(string value)
        {
            OrganizationType result = OrganizationType.Trust;
            switch (value.ToLower())
            {
                case "corporate":
                    result = OrganizationType.Corporate;
                    break;
                case "trust":
                    result = OrganizationType.Trust;
                    break;
                case "clientindividual":
                    result = OrganizationType.ClientIndividual;
                    break;
                case "clienteclipsesuper":
                    result = OrganizationType.ClientEClipseSuper;
                    break;
                case "dealergroup":
                    result = OrganizationType.DealerGroup;
                    break;
                case "memberfirm":
                    result = OrganizationType.MemberFirm;
                    break;
                case "principalpractice":
                    result = OrganizationType.PrincipalPractice;
                    break;
                case "advisor":
                case "adviser":
                    result = OrganizationType.Adviser;
                    break;
                case "individual":
                    result = OrganizationType.Individual;
                    break;
                case "clientcorporationprivate":
                    result = OrganizationType.ClientCorporationPrivate;
                    break;
                case "clientcorporationpublic":
                    result = OrganizationType.ClientCorporationPublic;
                    break;
                case "clientothertrustscorporate":
                    result = OrganizationType.ClientOtherTrustsCorporate;
                    break;
                case "clientothertrustsindividual":
                    result = OrganizationType.ClientOtherTrustsIndividual;
                    break;
                case "clientsmsfcorporatetrustee":
                    result = OrganizationType.ClientSMSFCorporateTrustee;
                    break;
                case "clientsmsfindividualtrustee":
                    result = OrganizationType.ClientSMSFIndividualTrustee;
                    break;
                case "bankaccount":
                    result = OrganizationType.BankAccount;
                    break;
                case "ifa":
                    result = OrganizationType.IFA;
                    break;
                case "clientsoletrader":
                    result = OrganizationType.ClientSoleTrader;
                    break;
                default:
                    break;
            }
            return result;
        }

        public string GetDisplayNameByEnum(OrganizationType oType)
        {
            string xml = string.Empty;
            switch (oType)
            {
                case OrganizationType.Corporate:
                    xml = "Corporate";
                    break;
                case OrganizationType.Trust:
                    xml = "Trust";
                    break;
                case OrganizationType.ClientIndividual:
                    xml = "Individuals Client";
                    break;
                case OrganizationType.ClientEClipseSuper:
                    xml = "e-Clipse Super";
                    break;
                case OrganizationType.DealerGroup:
                    xml = "Dealer Group";
                    break;
                case OrganizationType.MemberFirm:
                    xml = "Member Firm";
                    break;
                case OrganizationType.Adviser:
                    xml = "Adviser";
                    break;
                case OrganizationType.Individual:
                    xml = "Individual";
                    break;
                case OrganizationType.ClientCorporationPrivate:
                    xml = "Corporation - Private";
                    break;
                case OrganizationType.ClientCorporationPublic:
                    xml = "Corporation - Public";
                    break;
                case OrganizationType.ClientOtherTrustsCorporate:
                    xml = "Other Trusts - Corporate";
                    break;
                case OrganizationType.ClientOtherTrustsIndividual:
                    xml = "Other Trusts - Individual";
                    break;
                case OrganizationType.ClientSMSFCorporateTrustee:
                    xml = "SMSF – Corporate Trustee";
                    break;
                case OrganizationType.ClientSMSFIndividualTrustee:
                    xml = "SMSF – Individual Trustee/s";
                    break;
                case OrganizationType.BankAccount:
                    xml = "Bank Account";
                    break;
                case OrganizationType.PrincipalPractice:
                    xml = "Principal Practice";
                    break;
                case OrganizationType.IFA:
                    xml = "IFA";
                    break;
                case OrganizationType.ClientSoleTrader:
                    xml = "Sole Trader";
                    break;
                default:
                    break;
            }
            return xml;
        }
    }

    #endregion

    #region ***Admin Accont Relation ***

    public class GetAdminAccontRelationCommand : OrganizationCmCommandBase
    {
        private List<OrganizationUnit> GetAllUnits(string username)
        {
            List<OrganizationUnit> units = new List<OrganizationUnit>();
            OrganizationUnit unit = null;
            OrganizationListAction.ForEach(username, Organization, (csid, logical, bmc) =>
            {
                unit = new OrganizationUnit
                {
                    Clid = logical.CLID,
                    Csid = csid,
                    Name = bmc.Name,
                    Type = logical.CMTypeName,
                    Data = string.Empty,
                    IsInvestableClient = (bmc as OrganizationUnitCM).IsInvestableClient,
                    OrganizationStatus = (bmc as OrganizationUnitCM).OrganizationStatus,
                    ClientId = (bmc as OrganizationUnitCM).ClientId
                };
                units.Add(unit);
            });

            return units;
        }

        public override string DoAction(string value)
        {
            string Username = value;
            List<OrganizationUnit> units = GetAllUnits(Username);
            List<AdminSectionEntity> returnvalue = new List<AdminSectionEntity>();

            foreach (var each in units)
            {
                if (each.Type.ToLower() != "BankAccount".ToLower() &&
                    each.Type.ToLower() != "DesktopBrokerAccount".ToLower() &&
                    each.Type.ToLower() != "ManagedInvestmentSchemesAccount".ToLower() &&
                    each.Type.ToLower() != "TermDepositAccount".ToLower()
                    ) continue;

                var module = Organization.Broker.GetCMImplementation(each.Clid, each.Csid);
                IBrokerManagedComponent component = Organization.Broker.GetCMImplementation(each.Clid, each.Csid) as IBrokerManagedComponent;
                List<AccountRelation> data = component.GetDataStream(1025, "").ToNewOrData<List<AccountRelation>>();

                foreach (var account in data)
                {
                    AdminSectionEntity entity = new AdminSectionEntity();
                    AccountRelation leftaccount = component.GetDataStream(1026, "").ToNewOrData<AccountRelation>();
                    entity.LeftAccount = leftaccount;

                    module = Organization.Broker.GetCMImplementation(account.Clid, account.Csid);
                    IBrokerManagedComponent component1 = Organization.Broker.GetCMImplementation(account.Clid, account.Csid) as IBrokerManagedComponent;
                    if (component1 == null) continue;
                    AccountRelation rightaccount = component1.GetDataStream(1026, "").ToNewOrData<AccountRelation>();
                    entity.RightAccount = rightaccount;

                    entity.ServiceType = account.ServiceType;

                    if (CanAdd(returnvalue, leftaccount, rightaccount))
                        returnvalue.Add(entity);
                }

            }
            return returnvalue.ToXmlString();
        }

        private bool CanAdd(List<AdminSectionEntity> list, AccountRelation leftaccount, AccountRelation rightaccount)
        {
            var condition1 = list.Where(e => (e.LeftAccount == leftaccount && e.RightAccount == rightaccount)).ToList();
            if (condition1.Count > 0) return false;

            var condition2 = list.Where(e => (e.LeftAccount == rightaccount && e.RightAccount == leftaccount)).ToList();
            if (condition2.Count > 0) return false;

            return true;
        }
    }

    public class GetAdminAccontListByTypeCommand : OrganizationCmCommandBase
    {
        private List<OrganizationUnit> GetUnitsByType(string value)
        {
            IdentityCMType identity = value.ToNewOrData<IdentityCMType>();
            List<OrganizationUnit> units = new List<OrganizationUnit>();
            OrganizationUnit unit = null;
            OrganizationListAction.ForEach(identity.Username, Organization, (csid, logical, bmc) =>
            {
                if (logical.CMType == OrganizationTypeList.GetEntity(identity.Otype))
                {
                    unit = new OrganizationUnit
                    {
                        Clid = logical.CLID,
                        Csid = csid,
                        Name = bmc.Name,
                        Type = logical.CMTypeName,
                        Data = string.Empty,
                        IsInvestableClient = (bmc as OrganizationUnitCM).IsInvestableClient,
                        OrganizationStatus = (bmc as OrganizationUnitCM).OrganizationStatus,
                        ClientId = (bmc as OrganizationUnitCM).ClientId
                    };
                    units.Add(unit);
                }
            });
            return units;
        }

        public override string DoAction(string value)
        {
            List<OrganizationUnit> units = GetUnitsByType(value);
            List<AccountRelation> returnvalue = new List<AccountRelation>();

            foreach (var each in units)
            {
                var module = Organization.Broker.GetCMImplementation(each.Clid, each.Csid);
                IBrokerManagedComponent component = Organization.Broker.GetCMImplementation(each.Clid, each.Csid) as IBrokerManagedComponent;
                returnvalue.Add(component.GetDataStream(1026, "").ToNewOrData<AccountRelation>());
            }
            return returnvalue.ToXmlString();
        }


    }

    #endregion

    #region *** Securities List Commands ***

    public class GetSecuritiesListCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string value)
        {
            return Organization.Securities.ToXmlString();
        }
    }

    public class SecuritiesListAddCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string data)
        {
            SecuritiesEntity value = data.ToNewOrData<SecuritiesEntity>();
            var selected = (from e in Organization.Securities where e.AsxCode.ToLower() == value.AsxCode.Trim().ToLower() select e).SingleOrDefault();

            if (selected != null)
            {
                return "false";
            }
            else
            {
                Organization.Broker.SetWriteStart();
                Organization.Securities.Add(value);
                return Organization.Securities.ToXmlString();
            }
        }
    }

    public class SecuritiesListUpdateCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string data)
        {
            List<SecuritiesEntity> item = data.ToNewOrData<List<SecuritiesEntity>>();
            var OldValue = (from e in Organization.Securities where e.ID == item[0].ID select e).SingleOrDefault();
            Organization.Securities.Remove(OldValue);

            var NewValue = (from e in Organization.Securities where e.AsxCode.ToLower() == item[1].AsxCode.Trim().ToLower() select e).SingleOrDefault();
            if (Organization.Securities.Contains(NewValue))
            {
                Organization.Securities.Add(item[0]);
                return "false";
            }
            else
            {
                Organization.Broker.SetWriteStart();
                Organization.Securities.Add(item[1]);
                return Organization.Securities.ToXmlString();
            }
        }
    }

    public class SecuritiesListDeleteCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string data)
        {
            SecuritiesEntity value = data.ToNewOrData<SecuritiesEntity>();
            var OldValue = (from e in Organization.Securities where e.ID == value.ID select e).SingleOrDefault();
            Organization.Securities.Remove(OldValue);

            return Organization.Securities.ToXmlString();
        }
    }

    #endregion


    #region *** ManualAsset List Commands ***

    public class GetManualAssetListCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string value)
        {
            return Organization.ManualAsset.ToXmlString();
        }
    }

    public class ManualAssetListAddCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string data)
        {
            ManualAssetEntity value = data.ToNewOrData<ManualAssetEntity>();
            var selected = (from e in Organization.ManualAsset where e.AsxCode.ToLower() == value.AsxCode.Trim().ToLower() select e).SingleOrDefault();

            if (selected != null)
            {
                return "false";
            }
            else
            {
                Organization.Broker.SetWriteStart();
                Organization.ManualAsset.Add(value);
                return Organization.ManualAsset.ToXmlString();
            }
        }
    }

    public class ManualAssetListUpdateCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string data)
        {
            List<ManualAssetEntity> item = data.ToNewOrData<List<ManualAssetEntity>>();
            var OldValue = (from e in Organization.ManualAsset where e.ID == item[0].ID select e).SingleOrDefault();
            Organization.ManualAsset.Remove(OldValue);

            var NewValue = (from e in Organization.ManualAsset where e.AsxCode.ToLower() == item[1].AsxCode.Trim().ToLower() select e).SingleOrDefault();
            if (Organization.ManualAsset.Contains(NewValue))
            {
                Organization.ManualAsset.Add(item[0]);
                return "false";
            }
            else
            {
                Organization.Broker.SetWriteStart();
                Organization.ManualAsset.Add(item[1]);
                return Organization.ManualAsset.ToXmlString();
            }
        }
    }

    public class ManualAssetListDeleteCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string data)
        {
            ManualAssetEntity value = data.ToNewOrData<ManualAssetEntity>();
            var OldValue = (from e in Organization.ManualAsset where e.ID == value.ID select e).SingleOrDefault();
            Organization.ManualAsset.Remove(OldValue);
            return Organization.ManualAsset.ToXmlString();
        }
    }

    #endregion

    #region *** Product Securities List Commands ***

    public class GetProductSecuritiesListCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string value)
        {
            return Organization.ProductSecurities.ToXmlString();
        }
    }

    public class ProductSecuritiesListAddCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string data)
        {
            ProductSecuritiesEntity value = data.ToNewOrData<ProductSecuritiesEntity>();
            var selected = (from e in Organization.ProductSecurities where e.Name.ToLower() == value.Name.Trim().ToLower() select e).SingleOrDefault();

            if (selected != null)
            {
                return "false";
            }
            else
            {
                Organization.Broker.SetWriteStart();
                Organization.ProductSecurities.Add(value);
                return Organization.ProductSecurities.ToXmlString();
            }
        }
    }

    public class ProductSecuritiesListUpdateCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string data)
        {
            List<ProductSecuritiesEntity> item = data.ToNewOrData<List<ProductSecuritiesEntity>>();
            var OldValue = (from e in Organization.ProductSecurities where e.ID == item[0].ID select e).SingleOrDefault();
            Organization.ProductSecurities.Remove(OldValue);

            var NewValue = (from e in Organization.ProductSecurities where e.Name.ToLower() == item[1].Name.Trim().ToLower() select e).SingleOrDefault();
            if (Organization.ProductSecurities.Contains(NewValue))
            {
                Organization.ProductSecurities.Add(item[0]);
                return "false";
            }
            else
            {
                Organization.Broker.SetWriteStart();
                Organization.ProductSecurities.Add(item[1]);
                return Organization.ProductSecurities.ToXmlString();
            }
        }
    }

    public class ProductSecuritiesListDeleteCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string data)
        {
            ProductSecuritiesEntity value = data.ToNewOrData<ProductSecuritiesEntity>();
            var OldValue = (from e in Organization.ProductSecurities where e.ID == value.ID select e).SingleOrDefault();
            Organization.ProductSecurities.Remove(OldValue);
            return Organization.ProductSecurities.ToXmlString();
        }
    }

    #endregion

}
