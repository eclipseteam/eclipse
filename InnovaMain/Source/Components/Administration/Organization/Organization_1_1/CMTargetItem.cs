using System;
using System.Data;
using System.Security.Permissions;
using System.Runtime.Serialization;

using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.CM.Organization
{
	/// <summary>
	/// Summary description for CMTargetsItem.
	/// </summary>
	[Serializable]
	public class CMTargetItem: ISerializable
	{
		#region FIELD VARIABLES -------------------------------------------------------------------
	
		/// <summary>
		/// Unique Identifier for the Target Item
		/// </summary>
		public Guid PrimaryKey = Guid.NewGuid();

		/// <summary>
		/// Owning component family ID
		/// </summary>
		public Guid ComponentID;

		/// <summary>
		/// Description Field for the target
		/// </summary>
		public String Description = String.Empty;

		#endregion
		#region CONSTRUCTORS ----------------------------------------------------------------------
		public CMTargetItem()
		{
		}
		public CMTargetItem(DataRow row)
		{
			this.OnSetData(row);
		}
		protected CMTargetItem(SerializationInfo si, StreamingContext context)
		{
			PrimaryKey=Serialize.GetSerializedGuid(si,"cmti_PrimaryKey");
			ComponentID=Serialize.GetSerializedGuid(si,"cmti_ComponentID");
			Description=Serialize.GetSerializedString(si,"cmti_Description");
		}
		//Only allow the .NET Serialization core code to call this function
		[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]
		public virtual void GetObjectData(SerializationInfo si, StreamingContext context)
		{
			Serialize.AddSerializedValue(si,"cmti_PrimaryKey",PrimaryKey);
			Serialize.AddSerializedValue(si,"cmti_ComponentID",ComponentID);
			Serialize.AddSerializedValue(si,"cmti_Description",Description);
		}
		#endregion
		#region PUBLIC METHODS --------------------------------------------------------------------
		
		public void OnGetData(DataRow row )
		{
			row[CMTargetsDS.TARGETS_PRIMARYKEY_FIELD]	= this.PrimaryKey;
			row[CMTargetsDS.TARGETS_COMPONENTID_FIELD]	= this.ComponentID;
			row[CMTargetsDS.TARGETS_DESCRIPTION_FIELD]	= this.Description;
		}

		public void OnSetData(DataRow row)
		{
			this.PrimaryKey = (Guid)row[CMTargetsDS.TARGETS_PRIMARYKEY_FIELD];
			this.ComponentID = (Guid)row[CMTargetsDS.TARGETS_COMPONENTID_FIELD];
			this.Description = row[CMTargetsDS.TARGETS_DESCRIPTION_FIELD].ToString();
		}


		#endregion
	}
}
