﻿using System;
using System.Text;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.CalculationInterface;
namespace Oritax.TaxSimp.CM.Organization
{
    public partial class AllClientCSVCommand
    {
        private string StateStreet()
        {
            StringBuilder csvstring = new StringBuilder();
            var objAOIDCSVColumns = new AOIDCSVColumns();
            csvstring.Append(objAOIDCSVColumns.GetColumnCSV());

            OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
            {
                if ((bmc is ICsvData) && (bmc is IOrganizationUnit))
                {
                    if (((bmc as OrganizationUnitCM).IsInvestableClient && (bmc as OrganizationUnitCM).ServiceType.DO_IT_FOR_ME) || ((bmc as OrganizationUnitCM).IsInvestableClient && (bmc as OrganizationUnitCM).ServiceType.DO_IT_WITH_ME))
                        csvstring.Append((bmc as ICsvData).GetCsvData((CSVType)1));
                }
            });

            return csvstring.ToString();
        }



    }
}
