﻿using System;
using System.Text;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.CalculationInterface;
namespace Oritax.TaxSimp.CM.Organization
{
    public partial class AllClientCSVCommand
    {
        /// <summary>
        /// Extracting Adviser Details to CSV 
        /// </summary>
        /// <returns></returns>
        private string AdviserDetails()
        {
            StringBuilder csvstring = new StringBuilder();
            var objCSVColumns = new AdviserDetailsCSVColumns();
            csvstring.Append(objCSVColumns.GetColumnCSV());

            OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
            {
                if ((bmc is ICsvData) && (bmc is IOrganizationUnit))
                {
                    if (bmc.TypeName == "Advisor")
                    {
                        string returncsv = (bmc as ICsvData).GetCsvData((CSVType)2);
                        if (returncsv != null && !string.IsNullOrEmpty(returncsv))
                        {
                            csvstring.Append(returncsv);
                        }
                    }
                }
            });

            return csvstring.ToString();
        }

    }
}
