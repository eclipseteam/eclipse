﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Utilities;
using SpreadsheetGear;
using System.Data;
namespace Oritax.TaxSimp.CM.Organization
{
    public partial class AllClientCSVCommand
    {
        private int startColumn = 0;
        private int headeStartRowIndex = 1;
        private int IndividualJointRowCounter = 0;
        private int IndividualBSRowCounter = 0;
        private int ClientSMSFIndividualTrusteerowCounter = 0;
        private int ClientSMSFCorporateTrusteerowCounter = 0;
        private int ClientSMSFCorporateTrusteeRowCounter = 0;
        private int ClientOtherTrustsCorporateRowCounter = 0;
        private int ClientCorporationPublic_ClientCorporationPrivate_RowCounter = 0;


        private string DesktopBroker()
        {
            ClearAllCounters();
            SpreadsheetGear.IWorkbook workBook = SpreadsheetGear.Factory.GetWorkbook();

            OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
            {
                if ((bmc is IDesktopBrokerData) && (bmc is IOrganizationUnit))
                {
                    if ((bmc as OrganizationUnitCM).IsExportedDB == false && (bmc as OrganizationUnitCM).IsInvestableClient)
                    {
                        var data = (bmc as IDesktopBrokerData).GetDesktopBrokerData();
                        if (data != null)
                            Process(workBook, data);
                    }
                }
            });
            if (workBook.Worksheets.Count > 1)
                workBook.Worksheets[0].Delete();//remove default Sheet
            SerializableDictionary<string, byte[]> output = new SerializableDictionary<string, byte[]>();
            output.Add(string.Format("DesktopBrokerExport_" + DateTime.Now.ToString("dd-MM-yy") + "_.xls", DateTime.Now.ToString("yyyyMMdd")), workBook.SaveToMemory(FileFormat.Excel8));
            workBook.Close();
            return Convert.ToBase64String(Zipper.CreateZip(output));
        }

        private void ClearAllCounters()
        {
            IndividualJointRowCounter = headeStartRowIndex;
            ClientSMSFIndividualTrusteerowCounter = headeStartRowIndex;
            IndividualBSRowCounter = headeStartRowIndex;
            ClientSMSFCorporateTrusteerowCounter = headeStartRowIndex;
            ClientSMSFCorporateTrusteeRowCounter = headeStartRowIndex;
            ClientOtherTrustsCorporateRowCounter = headeStartRowIndex;
            ClientCorporationPublic_ClientCorporationPrivate_RowCounter = headeStartRowIndex;
        }

        private void Process(IWorkbook workBook, DesktopBrokerDataResponse response)
        {
            switch (response.Type)
            {
                case OrganizationType.ClientSMSFIndividualTrustee:
                    ClientSMSFIndividualTrusteerowCounter = AppendFile("Joint trustees of superfund BS", response.Data, workBook, ClientSMSFIndividualTrusteerowCounter);
                    //ClientSMSFIndividualTrusteerowCounter++;
                    break;
                case OrganizationType.ClientSMSFCorporateTrustee:
                    ClientSMSFCorporateTrusteerowCounter = AppendFile("Corporate trustee of sf BS ", response.Data, workBook, ClientSMSFCorporateTrusteerowCounter);
                    // ClientSMSFCorporateTrusteerowCounter++;
                    break;
                case OrganizationType.ClientOtherTrustsCorporate:
                    ClientOtherTrustsCorporateRowCounter = AppendFile("Corporate trustee of trust BS", response.Data, workBook, ClientOtherTrustsCorporateRowCounter);
                    //ClientOtherTrustsCorporateRowCounter++;
                    break;
                case OrganizationType.ClientIndividual:
                    if (response.Data.ContainsKey(DesktopBrokerExportKeys.Applicant_2.ToString()))
                    {
                        IndividualJointRowCounter = AppendFile("Joint BS", response.Data, workBook, IndividualJointRowCounter);
                        //   IndividualJointRowCounter++;     
                    }
                    else
                    {
                        IndividualBSRowCounter = AppendFile("Individual BS", response.Data, workBook, IndividualBSRowCounter);
                        //IndividualBSRowCounter++;  
                    }
                    break;
                case OrganizationType.ClientCorporationPublic:
                case OrganizationType.ClientCorporationPrivate:
                    ClientCorporationPublic_ClientCorporationPrivate_RowCounter = AppendFile("Corporation BS", response.Data, workBook, ClientCorporationPublic_ClientCorporationPrivate_RowCounter);
                    //  ClientCorporationPublic_ClientCorporationPrivate_RowCounter++;
                    break;
            }

        }

        private int AppendFile(string sheetName, Dictionary<string, DataTable> data, IWorkbook workBook, int row)
        {
            SpreadsheetGear.IWorksheet worksheet = workBook.Worksheets[sheetName];
            if (worksheet == null)
            {
                worksheet = workBook.Worksheets.Add();
                // = workBook.Worksheets[workBook.Worksheets.Count - 1];



            }
            worksheet.Name = sheetName;
            SpreadsheetGear.IRange range;
            int currentColumn = startColumn;
            int newCurrentRow = row;
            foreach (var item in data)
            {
                DataTable table = data[item.Key];

                #region Commented code
                //switch (item.Key)
                //{
                //    case "Client":
                //        range = worksheet.Cells[0, currentColumn];
                //        break;
                //    case "Applicant_1":
                //        range = worksheet.Cells[0, currentColumn];
                //        break;
                //    case "Applicant_2":
                //        range = worksheet.Cells[0, currentColumn];
                //        break;
                //    case "BankAccount":
                //        range = worksheet.Cells[0, currentColumn];
                //        break;
                //    case "TransferingStock":
                //        range = worksheet.Cells[0, currentColumn];
                //        break;
                //    case "TrustAccountDetails":
                //        range = worksheet.Cells[0, currentColumn];
                //        break;
                //    case "AccountHolder":
                //        range = worksheet.Cells[0, currentColumn];
                //        break;
                //} 
                #endregion

                range = worksheet.Cells[row, currentColumn];
                SpreadsheetGear.Data.SetDataFlags Flag;
                if (row == headeStartRowIndex)
                {
                    Flag = SpreadsheetGear.Data.SetDataFlags.WrapNoColumnHeaders;
                    newCurrentRow = row + 1;    // if header are added than increament row number

                    string Heading = GetHeadeing(item.Key);
                    SpreadsheetGear.IRange headrange = worksheet.Cells[headeStartRowIndex - 1, currentColumn, headeStartRowIndex - 1, currentColumn + table.Columns.Count - 1];
                    headrange.MergeCells = true;
                    headrange.Value = Heading;

                }
                else
                {
                    Flag = SpreadsheetGear.Data.SetDataFlags.NoColumnHeaders;
                    newCurrentRow = row;    // if  header are  not added than don't add in row numbers increament row number



                };


                range.CopyFromDataTable(table, Flag);
                worksheet.UsedRange.Columns.AutoFit();
                currentColumn += table.Columns.Count;
                newCurrentRow += (table.Rows.Count);//add extra table rows in  row count
            }


            return newCurrentRow;
        }

        private string GetHeadeing(string key)
        {
            string heading = "";
            DesktopBrokerExportKeys typekey = (DesktopBrokerExportKeys)System.Enum.Parse(typeof(DesktopBrokerExportKeys), key);
            switch (typekey)
            {
                case DesktopBrokerExportKeys.Applicant_1:
                    heading = "Account holder 1";
                    break;
                case DesktopBrokerExportKeys.Applicant_2:
                    heading = "Account holder 2";
                    break;
                case DesktopBrokerExportKeys.AccountHolder:
                    heading = "";
                    break;
                case DesktopBrokerExportKeys.BankAccount:
                    heading = "Bank Account details";
                    break;
                case DesktopBrokerExportKeys.Client:
                    heading = "";
                    break;
                case DesktopBrokerExportKeys.TransferingStock:
                    heading = "Transferring stock from another broker?";
                    break;

                case DesktopBrokerExportKeys.TrustAccountDetails:
                    heading = "Trust Account details";
                    break;


            }
            return heading;
        }
    }
}
