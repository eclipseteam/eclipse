﻿using System;
using System.Text;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Utilities;
using System.Collections.Generic;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Calculation;
using System.Diagnostics;

namespace Oritax.TaxSimp.CM.Organization
{
    public partial class AllClientCSVCommand
    {
        private string BankWestZip2()
        {
            string sbBankwestDownload = string.Empty;
            string sbBankwest2Download = string.Empty;

            sbBankwestDownload = BankWest();
            sbBankwest2Download = BankWest2();

            SerializableDictionary<string, byte[]> output = new SerializableDictionary<string, byte[]>();
            output.Add(string.Format("Multi_account_opening_account_details_"+ DateTime.Now.ToString("dd-MM-yy") + "_.csv", DateTime.Now.ToString("yyyyMMdd")), StrToByteArray(sbBankwestDownload.ToString()));
            output.Add(string.Format("Multi_account_opening_Bankwest_ID_" + DateTime.Now.ToString("dd-MM-yy") + "_.csv", DateTime.Now.ToString("yyyyMMdd")), StrToByteArray(sbBankwest2Download.ToString()));

            string bytes = Convert.ToBase64String(Zipper.CreateZip(output));
            return bytes;
        }

        private string BankWest()
        {
            StringBuilder csvstring = new StringBuilder();
            var objCSVColumns = new AccountOpeningBankWestCSVColumns();
            csvstring.Append(objCSVColumns.GetColumnCSV());

            OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
            {
                if ((bmc is ICsvData) && (bmc is IOrganizationUnit))
                {
                    if ((bmc as OrganizationUnitCM).IsExported == false && (bmc as OrganizationUnitCM).IsInvestableClient)
                    {
                        string returncsv = (bmc as ICsvData).GetCsvData((CSVType)2);
                        if (returncsv != null && !string.IsNullOrEmpty(returncsv))
                        {
                            csvstring.Append(returncsv);
                        }
                    }
                }
            });

            //UpdateAllExportedClients();

            return csvstring.ToString();
        }

        private string BankWest2()
        {
            StringBuilder csvstring = new StringBuilder();
            var objCSVColumns = new AccountOpeningBankWestIDCSVColumns();
            csvstring.Append(objCSVColumns.GetColumnCSV());

            OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
            {
                if ((bmc is ICsvData) && (bmc is IOrganizationUnit))
                {
                    if ((bmc as OrganizationUnitCM).IsExported == false && (bmc as OrganizationUnitCM).IsInvestableClient)
                    {
                        string returncsv = (bmc as ICsvData).GetCsvData((CSVType)4);
                        if (returncsv != null && !string.IsNullOrEmpty(returncsv))
                        {
                            csvstring.Append(returncsv);
                        }
                    }
                }
            });

            //UpdateAllExportedClients();

            return csvstring.ToString();
        }

        private void MarkCMCSVExported(IBrokerManagedComponent _bmc)
        {
            if (_bmc != null)
            {
                _bmc.UpdateDataStream(11000, bool.TrueString);
            }
        }

        private string UpdateAllExportedClients()
        {
            IBrokerManagedComponent organization = null;
            IBrokerManagedComponent component = null;
            try
            {
                Organization.Broker.SetWriteStart();
                organization = Organization.Broker.GetCMImplementation(Organization.CLID, Organization.CSID);
                string xml = organization.GetDataStream((int)OrganizationCommandType.GetAllOrganizationInstances, "Administrator");                
                var ou = xml.ToData<List<OrganizationUnit>>();
                foreach (var each in ou)
                {
                    if (each.Type.ToLower().StartsWith("client"))
                    {
                        component = GetComponent(new IdentityCM() { Clid = each.Clid, Csid = each.Csid });
                        component.UpdateDataStream(11000, bool.TrueString);
                    }
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);                
            }
            finally
            {
                Organization.Broker.ReleaseBrokerManagedComponent(organization);
                Organization.Broker.ReleaseBrokerManagedComponent(component);
            }
            return string.Empty;
        }

        public IBrokerManagedComponent GetComponent(IdentityCM identity)
        {
            return Organization.Broker.GetCMImplementation(identity.Clid, identity.Csid) as IBrokerManagedComponent;
        }

        private string BankWestZip()
        {
            bool TrusteeContentDownload = false;
            bool BusinessContentDownload = false;
            bool PersonalContentDownload = false;

            StringBuilder TrusteeContent = new StringBuilder();
            StringBuilder BusinessContent = new StringBuilder();
            StringBuilder PersonalContent = new StringBuilder();

            TrusteeContent.Append(new AOTrustCSVColumns().GetColumnCSV());
            BusinessContent.Append(new AOBusinessCSVColumns().GetColumnCSV());
            PersonalContent.Append(new AOPersonalCSVColumns().GetColumnCSV());


            OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
            {
                switch (logical.CMTypeName.ToLower())
                {
                    case "clientsmsfcorporatetrustee":
                    case "clientsmsfindividualtrustee":
                    case "clientothertrustscorporate":
                    case "clientothertrustsindividual":
                        FillContent(TrusteeContent, bmc);
                        TrusteeContentDownload = true;
                        break;
                    case "clientcorporationprivate":
                    case "clientcorporationpublic":
                        FillContent(BusinessContent, bmc);
                        BusinessContentDownload = true;
                        break;
                    case "clientindividual":
                        FillContent(PersonalContent, bmc);
                        PersonalContentDownload = true;
                        break;
                    default:
                        break;
                }
            });

            SerializableDictionary<string, byte[]> output = new SerializableDictionary<string, byte[]>();
            if (TrusteeContentDownload)
                output.Add(string.Format("AccountOpening{0}_bankwest_trust.csv", DateTime.Now.ToString("yyyyMMdd")), StrToByteArray(TrusteeContent.ToString()));

            if (BusinessContentDownload)
                output.Add(string.Format("AccountOpening{0}_bankwest_business.csv", DateTime.Now.ToString("yyyyMMdd")), StrToByteArray(BusinessContent.ToString()));

            if (PersonalContentDownload)
                output.Add(string.Format("AccountOpening{0}_bankwest_personal.csv", DateTime.Now.ToString("yyyyMMdd")), StrToByteArray(PersonalContent.ToString()));

            string bytes = Convert.ToBase64String(Zipper.CreateZip(output));
            return bytes;
        }

        private void FillContent(StringBuilder content, IBrokerManagedComponent bmc)
        {
            if ((bmc is ICsvData) && (bmc is IOrganizationUnit))
                content.Append((bmc as ICsvData).GetCsvData((CSVType)2));
        }

        private byte[] StrToByteArray(string str)
        {
            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            return encoding.GetBytes(str);
        }

    }
}
