﻿using System;
using System.Text;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.CalculationInterface;
namespace Oritax.TaxSimp.CM.Organization
{
    public partial class AllClientCSVCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string value)
        {
            int typeEnum = Convert.ToInt32(value);

            string result = string.Empty;
            switch ((CSVType)typeEnum)
            {
                case CSVType.BankWest:
                    result= BankWestZip2();
                    break;
                case CSVType.StateStreet:
                    result = StateStreet();
                    break;
                case CSVType.BankWestMarkCSVExport:
                    result= UpdateAllExportedClients();
                    break;
                case CSVType.DesktopBroker:
                    result= DesktopBroker();
                    break;
                case CSVType.AdviserDetails:
                    result = AdviserDetails();
                    break;

                //break;
            }
            return result;
        }
    }
}
