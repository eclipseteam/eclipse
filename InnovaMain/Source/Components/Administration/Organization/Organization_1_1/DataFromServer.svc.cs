﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;
using System.Xml.Linq;
using Oritax.TaxSimp.CM.Period;
using Oritax.TaxSimp.CM.Group;
using Oritax.TaxSimp.Calculation;
using System.Threading;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Services.CMBroker;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.BusinessStructureUtilities;
using Oritax.TaxSimp.CM.OrganizationUnit;
using System.Collections;
using System.Web;
using System.Diagnostics;

namespace Oritax.TaxSimp.CM.Organization
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class DataFromServer : BrokerManagedBase
    {
        [OperationContract]
        public string BulkPrint(Guid periodClid, Guid periodCsid, List<string> ruClidList, List<string> ruCsidList, List<string> wpReferences,
            bool includeNotes, bool includeAttachments)
        {
            string url = "";
            PeriodCM periodCm = null;
            InitialiseBroker();
            try
            {
                WorkpaperPrintingManager manager = new WorkpaperPrintingManager();
                manager.PrintNotes = includeNotes;
                manager.PrintAttachments = includeAttachments;
                manager.WpRefList = wpReferences;
                url = WorkpaperPrintingManager.GetPrintingPageUrl(CmBroker, periodClid, periodCsid);

                periodCm = (PeriodCM)CmBroker.GetCMImplementation(periodClid, periodCsid);
                ArrayList buildList = periodCm.PushDownBulkPrintingSelectedWorkpapers(manager, ruClidList, ruCsidList);
                // add the url list to the session
                HttpContext.Current.Session["PrintWorkpapers"] = buildList;
            }
            catch (Exception ex)
            {
                throw new Exception("Exception occured in BulkPrint service", ex);
            }
            finally
            {
                if (periodCm != null)
                    CmBroker.ReleaseBrokerManagedComponent(periodCm);
                CmBroker.SetAbort();
            }
            return url;
        }

        [OperationContract]
        public DateTime GetServerTime()
        {
            return DateTime.Now;
        }

        [OperationContract]
        private string GetPeriodConfiguration()
        {
            XmlElement element = PeriodConfiguration.ConfigTemplate.DocumentElement;
            string xmlString = element.OuterXml;
            return xmlString;
        }
        [OperationContract]
        private string GetReportingUnits(string strClid, string strCsid)
        {
            string xmlString = "";
            InitialiseBroker();
            IPeriod periodCm = null;
            try
            {
                if (!string.IsNullOrEmpty(strClid) && !string.IsNullOrEmpty(strCsid))
                {
                    Guid gClid = new Guid(strClid);
                    Guid gCsid = new Guid(strCsid);
                    periodCm = (IPeriod)CmBroker.GetCMImplementation(gClid, gCsid);
                    OrganizationUnitCM ruCm = (OrganizationUnitCM)periodCm.GetParentOrganisationUnit();

                    XmlDocument doc = new XmlDocument();
                    XmlElement el = doc.CreateElement("GroupStructure");
                    doc.AppendChild(el);

                    ruCm.AddGroupStructureToXmlElement(el);
                    xmlString = el.OuterXml;

                 }
            }
            catch (Exception ex)
            {
                throw new Exception("Exception occured in GetReportingUnits service", ex);
            }
            finally
            {
                if (periodCm != null)
                    CmBroker.ReleaseBrokerManagedComponent(periodCm);

                CmBroker.SetAbort();
            }
            return xmlString;
        }

        [OperationContract]
        private string GetEntityWorkpaperStructure()
        {
            InitialiseBroker();
            string userName = HttpContext.Current.User.Identity.Name;
            OrganizationCM1 organisationCm = (OrganizationCM1)this.CmBroker.GetWellKnownBMC(WellKnownCM.Organization);
            string entityWorkpaperStructure = organisationCm.GetEntityWorkpaperStructure(userName);            
            return entityWorkpaperStructure;
        }

        [OperationContract]
        private string GetXmlDocument()
        {
            string uri = @"C:\Documents and Settings\u0107352\My Documents\Visual Studio 2008\Projects\SilverlightApplication2\SilverlightApplication2.Web\App_Data\PeriodConfiguration2.xml";

            StreamReader sReader = new StreamReader(uri);
            string xmlString = sReader.ReadToEnd();
            sReader.Close();

            return xmlString;
        }
    }
}
