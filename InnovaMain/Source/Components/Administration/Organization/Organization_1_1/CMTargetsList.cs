using System;
using System.Collections;
using System.Data;
using System.Security.Permissions;
using System.Runtime.Serialization;

using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.CM.Organization
{
	/// <summary>
	/// Summary description for CMTargetsList.
	/// </summary>
	[Serializable]
	public class CMTargetsList : SerializableList, ISerializable
	{
		#region FIELD VARIABLES -------------------------------------------------------------------
		#endregion
		#region CONSTRUCTORS ----------------------------------------------------------------------
		public CMTargetsList()
		{
		}

		protected CMTargetsList(SerializationInfo si, StreamingContext context): base(si,context)
		{
		}
		
		//Only allow the .NET Serialization core code to call this function
		[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]
		public override void GetObjectData(SerializationInfo si, StreamingContext context)
		{
			base.GetObjectData(si,context);
		}
		#endregion
		#region PUBLIC METHODS --------------------------------------------------------------------
		public void OnGetData(CMTargetsDS cMTargetDS)
		{
			foreach(CMTargetItem cMTargetItem in this)
			{
				if(cMTargetItem.ComponentID == cMTargetDS.ComponentID)
				{
					DataRow row = cMTargetDS.Tables[CMTargetsDS.TARGETS_TABLE].NewRow();
					cMTargetItem.OnGetData(row);
					cMTargetDS.Tables[CMTargetsDS.TARGETS_TABLE].Rows.Add(row);
				}
			}
		}

		public void OnGetData(OrganizationDS orgDS)
		{
			foreach(CMTargetItem cMTargetItem in this)
			{
				DataRow row = orgDS.Tables[OrganizationDS.TARGETS_TABLE].NewRow();
				cMTargetItem.OnGetData(row);
				orgDS.Tables[OrganizationDS.TARGETS_TABLE].Rows.Add(row);
			}
		}

		private void RemoveComponentIDItems(Guid componentID)
		{
			IEnumerator en = this.GetEnumerator();
			while(en.MoveNext())
			{
				CMTargetItem cMTargetItem = (CMTargetItem)en.Current;
				if(cMTargetItem.ComponentID == componentID)
				{
					this.Remove(cMTargetItem);
					en = this.GetEnumerator();
				}
			}
		}

		public void OnSetData(CMTargetsDS cMTargetDS)
		{
			//Remove all targets of the component ID before delivering
			//them from the dataset
			RemoveComponentIDItems(cMTargetDS.ComponentID);

			foreach(DataRow row in cMTargetDS.Tables[CMTargetsDS.TARGETS_TABLE].Rows)
			{
				if(row.RowState!=DataRowState.Deleted)
				{
					CMTargetItem targetItem = new CMTargetItem(row);
					this.Add(targetItem);
				}
			}
		}

		public void OnSetData(OrganizationDS orgDS)
		{
			foreach(DataRow row in orgDS.Tables[OrganizationDS.TARGETS_TABLE].Rows)
			{
				CMTargetItem targetItem = new CMTargetItem(row);
				this.Add(targetItem);
			}
		}
		#endregion
	}
}
