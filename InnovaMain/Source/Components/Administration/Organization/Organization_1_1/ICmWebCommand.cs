﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Xml.Linq;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.CM.Organization.CommandsWeb;
using Oritax.TaxSimp.DataSets;

namespace Oritax.TaxSimp.CM.Organization.CommandsWeb
{

    public interface IOrganizationCmWebCommand
    {
        OrganizationCM Organization { get; set; }
        DataSet DoAction(DataSet value);
    }

    public static class OrganizationCmWebCommandFactory
    {
        public static IOrganizationCmWebCommand GetWebCommand(this OrganizationCM organiztion, int type)
        {
            Type value = _Commands[type];
            if (value == null) return null;

            IOrganizationCmWebCommand command = Activator.CreateInstance(value) as IOrganizationCmWebCommand;
            command.Organization = organiztion;
            return command;
        }

        private static readonly Dictionary<int, Type> _Commands =
            new Dictionary<int, Type>()
            {
                { (int)WebCommands.AddNewOrganizationUnit, typeof(OrganizationAddCommand) },
                { (int)WebCommands.DeleteOrganizationUnit,  typeof(OrganizationDeleteCommand) },
                { (int)WebCommands.GetOrganizationUnitsByType,  typeof(OrganizationUnitListCommandByType) },
                { (int)WebCommands.SetOrganizationUnitClientIDsByType,  typeof(SetOrganizationUnitClientIDCommandByType) },
                { (int)WebCommands.GetOrganizationUnitDetails,  typeof(GetOrganizationUnitDetails) },
                { (int)WebCommands.GetOrganizationAllUnitDetails,  typeof(GetOrganizationAllUnitDetails) },
                { (int)WebCommands.GetAllClientDetails,  typeof(GetAllClientDetailsCommand) },
                { (int)WebCommands.UpdateAllClientDetails,  typeof(UpdateAllClientDetailsCommand) },
                { (int)WebCommands.ExportBankwestAccountOpennings,  typeof(ExportBankwestAccountOpenings) },
                { (int)WebCommands.ExportStateStreet,  typeof(ExportStateStreet) },
                { (int)WebCommands.ExportStateStreetRecon,  typeof(ExportStateStreetRecon) },
                { (int)WebCommands.ExportAdvisorDetails,  typeof(ExportAdvisorDetailCommand) },
                { (int)WebCommands.ExportFinSimplicityInvestment,  typeof(ExportFinancialSimplicityInvestment) },                                                                          
                { (int)WebCommands.ExportDesktopBrokerInfo,  typeof(ExportDesktopBrokerInfo) },
                { (int)WebCommands.ImportClientsDistiributions, typeof(ImportClientDristibutionsCommand) }, 
                { (int)WebCommands.ValidateAccountOpenningImportFile,  typeof(ValidateBankWestAccountOpeningsCommand) },
                { (int)WebCommands.ImportAccountOpenningImportFile,  typeof(ImportBankWestAccountOpeningsCommand) },
                { (int)WebCommands.ValidateASXPriceImportFile,  typeof(ValidateASXPriceListCommand) },
                { (int)WebCommands.ImportASXPriceImportFile,  typeof(ImportASXPriceListCommand) },
                { (int)WebCommands.ValidateStateStreetPriceImportFile,  typeof(ValidateStateStreetPriceListCommand) },
                { (int)WebCommands.ImportStateStreetImportFile,  typeof(ImportStateStreetPriceListCommand) }, 
                { (int)WebCommands.ImportDesktopBrokerDataFeedImportFile,  typeof(ImportDesktopBrokerDataFeedCommand) },
                { (int)WebCommands.ValidateDesktopBrokerDataFeedImportFile,  typeof(ValidateDesktopBrokerDataFeedCommand) },
                { (int)WebCommands.ValidateBankwestDatafeedImportFile,  typeof(ValidateBankAccountTransactionCommand) } ,
                { (int)WebCommands.ImportBankwestDatafeedImportFile,  typeof(ImportBankAccountTransactionCommand) },
                { (int)WebCommands.ValidateAtCallClientTransactionImportFile ,typeof(ValidateAtCallClientTransactionCommand) },
                { (int)WebCommands.ImportAtCallClientTransactionImportFile,  typeof(ImportAtCallClientTransactionCommand) },
                { (int)WebCommands.ValidationTDClientTransactionImportFile,typeof(ValidationTDClientTransactionCommand)},
                { (int)WebCommands.ImportTDClientTransactionImportFile,typeof(ImportTDClientTransactionCommand)},
               
                { (int)WebCommands.ValidateProductSecurityPriceListDataFeedImportFile,  typeof(ValidateProductSecurityPriceListCommand) },
                { (int)WebCommands.ImportProductSecurityPriceListFeedImportFile,  typeof(ImportProductSecurityPriceListCommand) },
                { (int)WebCommands.ValidateTDPriceListDataFeedImportFile,  typeof(ValidateTDPriceListCommand) },
                { (int)WebCommands.ImportTDPriceListFeedImportFile,  typeof(ImportTDPriceListCommand) },
                { (int)WebCommands.ValidateMISFundTransactionsImportFile,  typeof(ValidateMISFundTransactionsCommand) },
                { (int)WebCommands.ImportMISFundTransactionsImportFile,  typeof(ImportMISFundTransactionsCommand) },
                { (int)WebCommands.ValidateMISUnitHoldImportFile,  typeof(ValidateMISUnitHoldBalancesListCommand) },
                { (int)WebCommands.ImportMISUnitHoldImportFile,  typeof(ImportMISUnitHoldBalancesListCommand) },
                { (int)WebCommands.GetUnlinkedAccount,  typeof(GetUnlinkedAccount) },
                { (int)WebCommands.ValidateFinSimplcityOrders,  typeof(ValidateFinSimplicityCommand) },
                { (int)WebCommands.ImportFinSimplcityOrders,  typeof(ImportFinSimplicityCommand) },
                { (int)WebCommands.ValidateFinSimplcityOrdersForDesktopBroker,  typeof(ValidateFinSimplicityOrdersForDesktopBroker) },
                { (int)WebCommands.ImportFinSimplcityOrdersForDesktopBroker,  typeof(ImportFinSimplicityOrdersForDesktopBroker) },
                { (int)WebCommands.GetTransactionAmongDividents,  typeof(GetTDTransactionsAmongDividends) },
                { (int)WebCommands.ValidatieAtCallTransactionsImportFiles,  typeof(ValidateAtCallTransactionsCommand) },
                { (int)WebCommands.ImportAtCallTransactionsImportFiles,  typeof(ImportAtCallTransactionsCommand) }, 
                { (int)WebCommands.ValidateASXHistoricalCostBaseOfTransferredAssets,  typeof(ValidateASXHistoricalCostBaseCommand) },
                { (int)WebCommands.ImportASXHistoricalCostBaseOfTransferredAssets,  typeof(ImportASXHistoricalCostBaseCommand) },
                { (int)WebCommands.MaquarieBankTransactionsImport,  typeof(ImportMaquarieBankCMATransactions) },
                { (int)WebCommands.ValidateMaquarieBankTransactions,  typeof(ValidateMaquarieBankCMATransactions) },
                { (int)WebCommands.ImportSMATransactions,  typeof(ImportSMAClientMembersTransactions) },
                { (int)WebCommands.ImportSMAClientMembersWithFULLData,  typeof(ImportSMAClientMembersData) },
                { (int)WebCommands.ValidateAtCallRates,  typeof(ValidateAtCallRatesCommand) },
                { (int)WebCommands.ImportAtCallRates,  typeof(ImportAtCallRatesCommand) },
                { (int)WebCommands.ValidateTDRates,  typeof(ValidateTDRatesCommand) },
                { (int)WebCommands.ImportTDRates,  typeof(ImportTDRatesCommand) },
                { (int)WebCommands.ImportSMARefreshMISTransactions,  typeof(ImportSMAClientMembersMISFix) },
                { (int)WebCommands.ImportSMAClientPortfolioUpdate,  typeof(ImportSMAClientPortfolioUpdate) },
                { (int)WebCommands.ImportSMAAdviserList,  typeof(ImportSMAAdviserList) },
                { (int)WebCommands.ImportSMATransactionsIndividual,  typeof(ImportSMAClientMembersTransactionsIndividual) },
                { (int)WebCommands.ExportToSMA,  typeof(ExportOrderToSMACommand) },
                { (int)WebCommands.ImportSMAAdviserMAP,  typeof(ImportSMAAdviserMap) },
                { (int)WebCommands.ImportSMAAccountStatus,  typeof(ImportSMAAccountStatus)},
                { (int)WebCommands.ImportSMAAccountAddFiiGASX,  typeof(ImportSMAAccountAddFiiGASX) },
                { (int)WebCommands.ImportSMAAccountDeleteClosedAccounts,  typeof(ImportSMAAccountDeleteClosedAccounts)},
                { (int)WebCommands.ImportSMASetOtherID,  typeof(ImportSMAClientMembersSetOtherID)},
            };
    }
}
