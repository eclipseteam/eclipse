﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EPI.Data;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;
using AccountType = EPI.Data.AccountType;
using IdentityCMDetail = Oritax.TaxSimp.Common.IdentityCMDetail;
using ServiceType = Oritax.TaxSimp.Common.ServiceType;
using System.Data;
using System.Globalization;

namespace Oritax.TaxSimp.CM.Organization
{
    public class EPIUtilDataUtil
    {

        public static EPIDataResponse GetBasicReponse()
        {
            var response = new EPIDataResponse();
            response.Version = "4.2";
            response.Date = DateTime.Now;
            response.ExtractMethod = new EPIDataResponseExtractMethod { FPSRequest = EPIDataResponseExtractMethodFPSRequest.Yes };
            response.Provider = new Platform
            {
                EPIId = "INOV",
                Name = "E-Clipse Online",
                HeadOffice = new PlatformHeadOffice
                {
                    ContactDetails = new ContactDetails
                    {

                        Addresses = new ContactDetailsAddressesAddress[]
                       {
                           new ContactDetailsAddressesAddress {Addressee="E-Clipse",Country=Country.AU,Line1="PO Box 1899",Line2="Neutral Bay NSW  2089",PostCode="2089",Preferred=false,State="NSW",Suburb="Neutral Bay",Type=AddressType.Business}
                       }
   ,
                        PhoneNumbers = new ContactDetailsPhoneNumbersPhoneNumber[] { new ContactDetailsPhoneNumbersPhoneNumber { AreaCode = "02", CountryCode = "61", Number = "9346 4686", Preferred = false, Type = PhoneNumberType.Business } },
                        EmailAddresses = new ContactDetailsEmailAddressesEmailAddress[] { new ContactDetailsEmailAddressesEmailAddress { Address = "inquiries@e-clipse.com.au", Preferred = true, Type = EmailAddressType.Business } },
                        WebAddresses = new ContactDetailsWebAddressesWebAddress[] { new ContactDetailsWebAddressesWebAddress { URL = "http://e-clipse.com.au", Preferred = false, Type = WebAddressType.Business } },



                        PreferredContactMethod = ContactMethod.Email

                    }
                },
                Support = new PlatformSupport
                {
                    ContactDetails = new ContactDetails
                    {

                        Addresses = new ContactDetailsAddressesAddress[]
                       {
                           new ContactDetailsAddressesAddress {Addressee="E-Clipse",Country=Country.AU,Line1="PO Box 1899",Line2="Neutral Bay NSW  2089",PostCode="2089",Preferred=false,State="NSW",Suburb="Neutral Bay",Type=AddressType.Business}
                       },
                        PhoneNumbers = new ContactDetailsPhoneNumbersPhoneNumber[] { new ContactDetailsPhoneNumbersPhoneNumber { AreaCode = "02", CountryCode = "61", Number = "9346 4686", Preferred = false, Type = PhoneNumberType.Business } },
                        EmailAddresses = new ContactDetailsEmailAddressesEmailAddress[] { new ContactDetailsEmailAddressesEmailAddress { Address = "inquiries@e-clipse.com.au", Preferred = true, Type = EmailAddressType.Business } },

                        WebAddresses = new ContactDetailsWebAddressesWebAddress[] { new ContactDetailsWebAddressesWebAddress { URL = "http://e-clipse.com.au", Preferred = false, Type = WebAddressType.Business } },



                        PreferredContactMethod = ContactMethod.Email

                    }
                },

            };


            return response;
        }
        public static void AddClientModels(ServiceType serviceType, List<ModelEntity> models, OrganizationCM Organization)
        {
            ModelEntity model = null;

            if (serviceType.DO_IT_FOR_ME)
            {
                if (serviceType.DoItForMe.ProgramCode != null)
                    model = Organization.Model.Where(ss => ss.ProgramCode == serviceType.DoItForMe.ProgramCode).FirstOrDefault();
                if (model != null)
                {
                    models.Add(model);
                }
            }


            if (serviceType.DO_IT_WITH_ME)
            {
                if (serviceType.DoItWithMe.Model != null)
                    model = Organization.Model.Where(ss => ss.ID == serviceType.DoItWithMe.Model.ID).FirstOrDefault();
                else if (serviceType.DoItWithMe.ProgramCode!=null)
                    model = Organization.Model.Where(ss => ss.ProgramCode == serviceType.DoItWithMe.ProgramCode).FirstOrDefault();


                if (model != null)
                {
                    models.Add(model);
                }
            }

            if (serviceType.DO_IT_YOURSELF)
            {
                if (serviceType.DoItYourSelf.Model != null)
                    model = Organization.Model.Where(ss => ss.ID == serviceType.DoItYourSelf.Model.ID).FirstOrDefault();
                else if (serviceType.DoItYourSelf.ProgramCode != null)
                    model = Organization.Model.Where(ss => ss.ProgramCode == serviceType.DoItYourSelf.ProgramCode).FirstOrDefault();


                if (model != null)
                {
                    models.Add(model);
                }
            }


        }

        public static void AddClientModels(ServiceType serviceType, List<ModelEntity> models, OrganizationCM Organization, List<AccountProcessTaskEntity> accountProcessTaskEntityList)
        {
            ModelEntity model = null;
            var modelGroup = accountProcessTaskEntityList.GroupBy( lp => lp.ModelID);

            foreach (var modelGroupItem in modelGroup)
            { 
                model = Organization.Model.Where(ss => ss.ID == modelGroupItem.Key).FirstOrDefault();
                if (model != null)
                {
                    if(model.ServiceType == ServiceTypes.DoItForMe && serviceType.DO_IT_FOR_ME)
                         models.Add(model);
                    else if(model.ServiceType == ServiceTypes.DoItYourSelf && serviceType.DO_IT_YOURSELF)
                        models.Add(model);
                    else if(model.ServiceType == ServiceTypes.DoItWithMe && serviceType.DO_IT_WITH_ME)
                        models.Add(model);
                 }
            }
        }
     
        public static void SetClientAccount(AccountType type, string name, string shortName, AccountStatusStatus status, CurrencyCode currency, EPIDataResponseAccount clientAccount)
        {
            clientAccount.Type = type;
            clientAccount.Name = name.ReplaceEmptyWithNull();
            if (!String.IsNullOrEmpty(shortName))
                clientAccount.ShortName = shortName.ReplaceEmptyWithNull();
            clientAccount.Status = new AccountStatus { Status = status };
            clientAccount.ReportingCurrency = currency;

        }
        public static ClientBankDetails GetBankDetails(IdentityCM account, OrganizationCM organization)
        {

            var comp = organization.Broker.GetCMImplementation(account.Clid, account.Csid);
            if (comp != null)
            {

                var bankacount = (comp as OrganizationUnitCM).ClientEntity as BankAccountEntity;

                if (bankacount != null)
                {

                    var debtDetail = new DirectCredit
                    {
                        AccountName = bankacount.Name.ReplaceEmptyWithNull() != null ? bankacount.Name.Substring(0, (bankacount.Name.Length > 85) ? 85 : bankacount.Name.Length) : null,
                        AccountNumber = bankacount.AccountNumber.ReplaceEmptyWithNull(),
                        BSB = bankacount.BSB.ReplaceEmptyWithNull() != null ? bankacount.BSB.Replace("-", "") : null
                    };

                    var institute =  organization.Institution.Where(ss => ss.ID == bankacount.InstitutionID).FirstOrDefault();

                    if (institute != null)
                    {
                        debtDetail.FinancialInstitutionName = institute.Name.ReplaceEmptyWithNull();

                    }

                    return new ClientBankDetails
                    {
                        DirectCredit = debtDetail

                    }; ;
                }
                organization.Broker.ReleaseBrokerManagedComponent(comp);
            }

            return null;
        }
        public static TaxationDetails GetTaxationDetails(string abn, string tfn)
        {
            return new TaxationDetails { ABN = abn.ReplaceEmptyWithNull(), TFN = tfn.ReplaceEmptyWithNull() };
        }
        public static EntitySuperfund GetEntitySuperfund(string Name, List<IdentityCMDetail> trustees, OrganizationCM organization)
        {
            var entity = new EntitySuperfund
            {
                Name = Name.ReplaceEmptyWithNull(),
            };

            List<EntitySuperfundTrustee> tt = new List<EntitySuperfundTrustee>();
            foreach (var applicant in trustees)
            {
                var trustee = new EntitySuperfundTrustee { Person = GetPersonEntity(applicant,organization) };

                if (trustee.Person != null && trustee.Person.FirstName != String.Empty && trustee.Person.LastName != String.Empty)
                {
                    var addedTrustee = tt.Where(t => t.Person.FirstName == trustee.Person.FirstName && t.Person.LastName == trustee.Person.LastName).FirstOrDefault();
                    if (addedTrustee == null)
                        tt.Add(trustee);
                }

            }
            if (tt.Count > 0)
                entity.Trustees = tt.ToArray();
            return entity;
        }
        public static EntityTrust GetTrust(string ABN, string Name, List<IdentityCMDetail> trustees, OrganizationCM organization)
        {
            var entity = new EntityTrust
            {
                ABN = ABN.ReplaceEmptyWithNull(),
                Name = Name.ReplaceEmptyWithNull(),

            };

            List<EntityTrustTrustee> tt = new List<EntityTrustTrustee>();
            foreach (var applicant in trustees)
            {
                var trustee = new EntityTrustTrustee { Person = GetPersonEntity(applicant,organization) };

                if (trustee.Person != null && trustee.Person.FirstName != null && trustee.Person.FirstName != String.Empty && trustee.Person.LastName != String.Empty && trustee.Person.LastName != null)
                {
                    var addedTrustee = tt.Where(t => t.Person.FirstName == trustee.Person.FirstName && t.Person.LastName == trustee.Person.LastName).FirstOrDefault();

                    if (addedTrustee == null)
                        tt.Add(trustee);
                }
            }
            if (tt.Count > 0)
                entity.Trustees = tt.ToArray();
            return entity;
        }
        public static EntityOrganisation GetEntityOrganization(CorporateEntity clientcop)
        {
            var entity = new EntityOrganisation
            {
                ABN = clientcop.ABN.ReplaceEmptyWithNull(),
                ACNorARBN = clientcop.ACN.ReplaceEmptyWithNull(),
                Name = clientcop.Name.ReplaceEmptyWithNull(),
                TradingName = clientcop.Name.ReplaceEmptyWithNull(),

                ContactDetails = new ContactDetails
                {
                    Addresses = new[]{
                                    new ContactDetailsAddressesAddress
                                        {
                                            Addressee =clientcop.Name.ReplaceEmptyWithNull(),
                                            Country =Country.AU,
                                            Line1 =clientcop.Address.BusinessAddress.Addressline1.ReplaceEmptyWithNull()??"line1",
                                            Line2 =clientcop.Address.BusinessAddress.Addressline2.ReplaceEmptyWithNull(),
                                            PostCode =clientcop.Address.BusinessAddress.PostCode.ReplaceEmptyWithNull()??"123",
                                            Preferred =true,
                                            State =clientcop.Address.BusinessAddress.State.ReplaceEmptyWithNull()??"NSW",
                                            Suburb =clientcop.Address.BusinessAddress.Suburb.ReplaceEmptyWithNull()??"subrub",
                                            Type =AddressType.Postal
                                        }
                                    }
                    ,
                    PreferredContactMethod = ContactMethod.Address,
                }
            };

            return entity;
        }
        public static EntityPerson GetPersonEntityFromPrimaryCMDetail(List<IdentityCMDetail> list, OrganizationCM organization)
        {
            var ind = list.Where(ss => ss.IsPrimary == true).FirstOrDefault();

            if (ind == null && list.Count > 0)
                ind = list.First();

            if (ind != null)
                return GetPersonEntity(ind,organization);

            return null;
        }
        public static EntityPerson GetPersonEntity(IdentityCMDetail ind, OrganizationCM organization)
        {
            var comp =organization.Broker.GetCMImplementation(ind.Clid, ind.Csid);
            if (comp != null)
            {

                var entity = (comp as OrganizationUnitCM).ClientEntity as IndividualEntity;

                if (entity != null && entity.Name != null && entity.Name != String.Empty && entity.Surname != null && entity.Surname != String.Empty)
                {

                    var person = new EntityPerson
                    {
                        FirstName = entity.Name.ReplaceEmptyWithNull(),
                        LastName = entity.Surname.ReplaceEmptyWithNull(),
                        MiddleName = entity.MiddleName.ReplaceEmptyWithNull(),
                        MaritalStatus = MaritalStatus.Unknown,
                        Gender = Gender.Unknown,
                        //EmploymentDetails = new Employment[] { new Employment() { Employer = new EmploymentEmployer() { Name = entity.Employer.ReplaceEmptyWithNull() } } },
                        CountryOfResidence = Country.AU,
                    };


                    if (entity.HasAddress)
                    {
                        var ContactDetails = new ContactDetails
                        {

                            Addresses =
                                new[]{
                                       new ContactDetailsAddressesAddress
                                       {
                                               Addressee =entity.Fullname.ReplaceEmptyWithNull(),
                                               Country =Country.AU,
                                               Line1 =entity.ResidentialAddress.Addressline1.ReplaceEmptyWithNull()??"line1",
                                               Line2 =entity.ResidentialAddress.Addressline2.ReplaceEmptyWithNull(),
                                                PostCode =entity.ResidentialAddress.PostCode.ReplaceEmptyWithNull()??"123",
                                               Preferred =false,
                                               State =entity.ResidentialAddress.State.ReplaceEmptyWithNull()??"NSW",
                                               Suburb =entity.ResidentialAddress.Suburb.ReplaceEmptyWithNull()??"subrub",
                                               Type =AddressType.Postal
                                        }
                                     },


                            PreferredContactMethod = ContactMethod.Address
                        };
                        if (entity.HomePhoneNumber.PhoneNumber.ReplaceEmptyWithNull() != null)
                        {
                            ContactDetails.PhoneNumbers =
                                new ContactDetailsPhoneNumbersPhoneNumber
                                    []
                                    {
                                        new ContactDetailsPhoneNumbersPhoneNumber
                                            {
                                                AreaCode = entity.HomePhoneNumber.CityCode.ReplaceEmptyWithNull(),
                                                CountryCode = entity.HomePhoneNumber.CountryCode.ReplaceEmptyWithNull(),
                                                Number = entity.HomePhoneNumber.PhoneNumber.ReplaceEmptyWithNull(),
                                                Preferred = false,
                                                Type = PhoneNumberType.Home
                                            }
                                    };
                            ContactDetails.PreferredContactMethod = ContactMethod.Phone;
                        }
                        if (entity.EmailAddress.ReplaceEmptyWithNull() != null)
                        {
                            ContactDetails.EmailAddresses = new[]
                                                                {
                                                                    new ContactDetailsEmailAddressesEmailAddress
                                                                        {
                                                                            Address =
                                                                                entity.EmailAddress.ReplaceEmptyWithNull
                                                                                (),
                                                                            Preferred = true,
                                                                            Type = EmailAddressType.Business
                                                                        }
                                                                };
                            ContactDetails.PreferredContactMethod = ContactMethod.Email;
                        }

                        person.ContactDetails = ContactDetails;
                    }

                    Title title;
                    if (System.Enum.TryParse(entity.PersonalTitle.Title, true, out title))
                    {
                        person.Title = title;
                    }


                    if (entity.DOB.HasValue)
                    {
                        person.DOB = entity.DOB.Value.Date;
                    }

                    return person;
                }
            }

            return null;
        }

        public static EPIDataResponseAdviser GetAdvisorId(IdentityCM identityCM, OrganizationCM organization)
        {

            EPIDataResponseAdviser adviser = null;
            IOrganizationUnit orgUnit = GetIOrgUnitEntity(identityCM, organization);

            adviser = GetAdviserEPIEntity(adviser, orgUnit);

            return adviser;
        }

        public static EPIDataResponseAdviser GetAdviserEPIEntity(EPIDataResponseAdviser adviser, IOrganizationUnit orgUnit)
        {
            if (orgUnit != null)
            {
                adviser = new EPIDataResponseAdviser { Id = orgUnit.EclipseClientID };
                (orgUnit as OrganizationUnitCM).FillEntityDetailsInObjects(adviser, 1);
            }

            return adviser;
        }

        public static IOrganizationUnit GetIOrgUnitEntity(IdentityCM identityCM, OrganizationCM organization)
        {
            IOrganizationUnit orgUnit = null;

            if (identityCM != null && identityCM != IdentityCM.Null)
            {
                if (identityCM.Cid != Guid.Empty)
                    orgUnit = organization.Broker.GetBMCInstance(identityCM.Cid) as IOrganizationUnit;
                else
                    orgUnit = organization.Broker.GetCMImplementation(identityCM.Clid, identityCM.Csid) as IOrganizationUnit;
            }

            return orgUnit;
        }

        public static bool HasClientASXAccountAttached(List<AccountProcessTaskEntity> listAccountProcess)
        {

            return listAccountProcess.Count( ss =>ss.LinkedEntityType == OrganizationType.DesktopBrokerAccount &&ss.LinkedEntity.Csid != Guid.Empty && ss.LinkedEntity.Clid != Guid.Empty) > 0;

        }

        public static bool HasClientExportableCashAccountAttached(List<AccountProcessTaskEntity> listAccountProcess, OrganizationCM organization)
        {

            return listAccountProcess.Count(
                ss =>
                ss.LinkedEntityType == OrganizationType.BankAccount &&
                ss.LinkedEntity.Csid != Guid.Empty && ss.LinkedEntity.Clid != Guid.Empty) > 0;
        }
       

        public static bool HasClientMISAccountAttached(List<AccountProcessTaskEntity> listAccountProcess)
        {

            return
                listAccountProcess.Count(
                    ss =>
                    (ss.LinkedEntityType == OrganizationType.ManagedInvestmentSchemesAccount &&
                    ss.LinkedEntity.Csid != Guid.Empty && ss.LinkedEntity.Clid != Guid.Empty) || 
                    (ss.LinkedEntityType == OrganizationType.BankAccount &&
                    ss.LinkedEntity.Csid != Guid.Empty && ss.LinkedEntity.Clid != Guid.Empty) ||
                    (ss.LinkedEntityType == OrganizationType.DesktopBrokerAccount &&
                    ss.LinkedEntity.Csid != Guid.Empty && ss.LinkedEntity.Clid != Guid.Empty)
                    ) > 0;

        }

       public static  bool ShouldChangeAmountToZero(string investmentCode,string transType)
       {
           return (IsP2Fund(investmentCode) && IsAdjustmentTrans(transType));
       }

        public static bool IsAdjustmentTrans(string transType)
        {
            return (!String.IsNullOrEmpty(transType))&& (transType.ToLower() == "Adjustment Up".ToLower() || transType.ToLower() == "Adjustment Down".ToLower());
        }

        public static bool IsP2Fund(string investmentCode)
        {
            return (!String.IsNullOrEmpty(investmentCode)) && (investmentCode.ToLower() == "IV004".ToLower() || investmentCode.ToLower() == "IV014".ToLower());
        }

        public static EPIDataResponseInvestmentHoldingMovementTransaction GenerateASXTransaction(HoldingTransactions tran, string accountID, string investmentCode, bool isDeleted,IOrganization organization)
        {
            var typ = InvestmentHoldingTransactionTypeMapping.GetTransactionType(tran.TransactionType.Trim());
            //Swapped gross value and net value value be because they were not coming properly.After discussion with Babar and Farooq (Dated:4 July 2013):Samee


            var unitPrice = tran.Units == 0 ? 0 : Math.Abs( tran.UnitPrice);
            var grossValue = typ.ChangeValueAccordingToPolarity((typ.Type == InvestmentHoldingTransactionType.T00111)? tran.GrossValue : tran.NetValue);
            var netValue=   typ.ChangeValueAccordingToPolarity(tran.GrossValue);
            var costBase = typ.ChangeValueAccordingToPolarity(tran.CostBase);
            var expenses = tran.BrokerageAmount + tran.FeesTotal;
            
            if (typ.Type != InvestmentHoldingTransactionType.T00111 && grossValue < 0 && Math.Abs(tran.GrossValue) > Math.Abs(tran.NetValue))
            {

                costBase = grossValue = typ.ChangeValueAccordingToPolarity(tran.GrossValue);
                netValue = typ.ChangeValueAccordingToPolarity(tran.NetValue);

            }

            if (typ.Type == InvestmentHoldingTransactionType.T00051 && typ.TransactionDesction.ToLower() == "reversal - buy / purchase / deposit")
            {
                grossValue = unitPrice * tran.Units;
                netValue = grossValue + tran.BrokerageAmount;
            }

            if ((typ.Type == InvestmentHoldingTransactionType.T00111 || typ.Type == InvestmentHoldingTransactionType.T00112) && tran.Units!=0 && tran.UnitPrice == 0&&tran.GrossValue==0)
            {
                var security=organization.Securities.FirstOrDefault(ss=>ss.AsxCode==investmentCode && ss.ASXSecurity!=null&& ss.ASXSecurity.Count>0);
                if(security!=null)
                {
                    var price = security.ASXSecurity.OrderByDescending(ss=>ss.Date).FirstOrDefault(ss=>ss.Date.Date<=tran.TradeDate.Date);

                    if(price!=null)
                    {
                        unitPrice = (decimal) Math.Abs(price.UnitPrice);
                        costBase =grossValue = netValue = typ.ChangeValueAccordingToPolarity(tran.Units*unitPrice);
                    }
                }
            }


            return new EPIDataResponseInvestmentHoldingMovementTransaction
                {
                    AccountId = accountID,
                    InvestmentCode = investmentCode.ReplaceEmptyWithNull(),
                    Exchange = "ASX",
                    //  HoldingId = brokerAccountEntity.HINNumber,
                    Id = tran.TranstactionUniqueID.ToString(),
                    EPITransactionType = typ.Type,
                    PlatformDescriptionForTransaction = tran.TransactionType.ReplaceEmptyWithNull(),
                    Units = tran.Units,
                    UnitPrice = new MonetaryAmount { Currency = CurrencyCode.AUD, Value = tran.Units == 0 ? 0 : Decimal.Round(unitPrice, 6) },
                    GrossValue = new MonetaryAmount { Currency = CurrencyCode.AUD, Value = Decimal.Round(grossValue, 6) },
                    NetValue = new MonetaryAmount { Currency = CurrencyCode.AUD, Value = Decimal.Round(netValue, 6) },
                    CostBase = new MonetaryAmount { Currency = CurrencyCode.AUD, Value = Decimal.Round(costBase, 6) },
                    TransactionDate = tran.TradeDate,
                    SettlementDate = tran.SettlementDate.HasValue ? tran.SettlementDate.Value : tran.TradeDate,
                    SettlementStatus = InvestmentHoldingTransactionSettlementStatus.Settled,
                    TaxDate = tran.TradeDate,
                    Fees = new InvestmentHoldingTransactionFees { Total = new MonetaryAmount { Currency = CurrencyCode.AUD, Value = Decimal.Round(tran.FeesTotal, 6) } },
                    //BrokerageAndCommision = (tran.Brokerage != 0 || tran.BrokerageGST != 0) ? new InvestmentHoldingTransactionBrokerageAndCommision
                    //    {
                    //        BrokerageAmount = tran.Brokerage != 0 ? new MonetaryAmount { Currency = CurrencyCode.AUD, Value = Math.Round(tran.Brokerage, 6) } : null,
                    //        TaxOnBrokerageAmount = tran.BrokerageGST != 0 ? new MonetaryAmountPositive { Value = Math.Round(Math.Abs(tran.BrokerageGST), 6) } : null,
                    //        CommisionAmount = new MonetaryAmount { Currency = CurrencyCode.AUD, Value = 0 },

                    //    } : null,
                    BrokerageAndCommision = (tran.BrokerageAmount != 0 ) ? new InvestmentHoldingTransactionBrokerageAndCommision
                    {
                        BrokerageAmount = tran.BrokerageAmount != 0 ? new MonetaryAmount { Currency = CurrencyCode.AUD, Value = Math.Round(tran.BrokerageAmount, 6) } : null,

                        CommisionAmount = new MonetaryAmount { Currency = CurrencyCode.AUD, Value = 0 },

                    } : null,

                    Delete = isDeleted,
                    LastModified = DateTime.Now
                };
        }

        public static EPIDataResponseInvestmentHoldingMovementTransaction GenerateMISTransation(MISFundTransactionEntity tran, string Account_ID, string fundCode, bool IsDeleted)
        {
            var typ = InvestmentHoldingTransactionTypeMapping.GetTransactionType(tran.TransactionType.Trim());
            bool amountZero = EPIUtilDataUtil.ShouldChangeAmountToZero(fundCode, tran.TransactionType);
            var amount = amountZero ? 0 : tran.Amount;
            return new EPIDataResponseInvestmentHoldingMovementTransaction
                {
                    Exchange = "FND",
                    AccountId = Account_ID,
                    InvestmentCode = fundCode.ReplaceEmptyWithNull(),
                    Id = tran.ID.ToString(),
                    PlatformDescriptionForTransaction = tran.TransactionType.ReplaceEmptyWithNull(),
                    EPITransactionType = typ.Type,
                    Units = Math.Round(tran.Shares, 4),
                    UnitPrice = new MonetaryAmount
                        {
                            Currency = CurrencyCode.AUD,
                            Value = Decimal.Round(tran.UnitPrice, 6)
                        },
                    GrossValue = new MonetaryAmount
                        {
                            Currency = CurrencyCode.AUD,
                            Value = Decimal.Round(amount, 6)
                        },
                    NetValue = new MonetaryAmount
                        {
                            Currency = CurrencyCode.AUD,
                            Value = Decimal.Round(amount, 6)
                        },
                    TransactionDate = tran.TradeDate,
                    SettlementDate = tran.TradeDate,
                    SettlementStatus = InvestmentHoldingTransactionSettlementStatus.Settled,
                    TaxDate = tran.TradeDate,
                    Delete = IsDeleted,
                    LastModified = DateTime.Now
                };
        }

        public static EPIDataResponseInvestmentHoldingMovementTransaction GenerateBankTransaction(Common.CashManagementEntity tran,string investmentCode, string Account_ID, bool IsDeleted)
        {
            InvestmentHoldingTransactionTypeMapping typ = InvestmentHoldingTransactionTypeMapping.GetTransactionType(tran.SystemTransactionType.Trim());

            if (tran.SystemTransactionType == "Interest" && tran.Category == "Income")
                typ = InvestmentHoldingTransactionTypeMapping.GetTransactionType("Distribution Effect"); 
            
            bool amountZero = EPIUtilDataUtil.ShouldChangeAmountToZero("CASH", tran.TransactionType);
            var amount = amountZero ? 0 : tran.TotalAmount;
            return new EPIDataResponseInvestmentHoldingMovementTransaction
            {
                Exchange = "CASH",
                AccountId = Account_ID,
                InvestmentCode = investmentCode,
                Id = tran.ID.ToString(),
                PlatformDescriptionForTransaction = tran.TransactionType.ReplaceEmptyWithNull(),
                EPITransactionType = typ.Type,
                Units = 0,
                UnitPrice = new MonetaryAmount
                {
                    Currency = CurrencyCode.AUD,
                    Value = Decimal.Round(0, 6)
                },
                GrossValue = new MonetaryAmount
                {
                    Currency = CurrencyCode.AUD,
                    Value = Decimal.Round(amount, 6)
                },
                NetValue = new MonetaryAmount
                {
                    Currency = CurrencyCode.AUD,
                    Value = Decimal.Round(amount, 6)
                },
                TransactionDate = tran.TransactionDate,
                SettlementDate = tran.TransactionDate,
                SettlementStatus = InvestmentHoldingTransactionSettlementStatus.Settled,
                TaxDate = tran.TransactionDate,
                Delete = IsDeleted,
                LastModified = DateTime.Now
            }; 
        }

        public static void GenerateBankTransaction(List<EPIDataResponseInvestmentHoldingMovementTransaction> trans, string fileNameToLoad, string investmentCode, bool IsDeleted)
        {
            SpreadsheetGear.IWorkbook workBook = SpreadsheetGear.Factory.GetWorkbook(fileNameToLoad);
            DataSet ds = workBook.GetDataSet(SpreadsheetGear.Data.GetDataFlags.FormattedText);

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                try
                {
                    string Account_ID = row["AccountId"].ToString();

                    string tranID = row["TradeId"].ToString();
                    decimal amount = 0;
                    decimal.TryParse(row["Units"].ToString(), out amount);
                    var existingMapping = InvestmentHoldingTransactionTypeMapping.Types.Where(t => t.Value.Type.ToString().ToLower() == row["PlatformTradeType"].ToString().ToLower()).FirstOrDefault();
                    DateTime transactionDate = DateTime.Now;
                    
                    bool dateResult = DateTime.TryParse(row["TradeDate"].ToString(), out transactionDate);
                    if(!dateResult)
                        transactionDate = DateTime.Now;

                    var epiDataResponseInvestmentHoldingMovementTransactionObj = new EPIDataResponseInvestmentHoldingMovementTransaction
                    {
                        Exchange = "CASH",
                        AccountId = Account_ID,
                        InvestmentCode = investmentCode,
                        Id = tranID,
                        PlatformDescriptionForTransaction = existingMapping.Key.ReplaceEmptyWithNull(),
                        EPITransactionType = existingMapping.Value.Type,
                        Units = 0,
                        UnitPrice = new MonetaryAmount
                        {
                            Currency = CurrencyCode.AUD,
                            Value = Decimal.Round(0, 6)
                        },
                        GrossValue = new MonetaryAmount
                        {
                            Currency = CurrencyCode.AUD,
                            Value = Decimal.Round(amount, 6)
                        },
                        NetValue = new MonetaryAmount
                        {
                            Currency = CurrencyCode.AUD,
                            Value = Decimal.Round(amount, 6)
                        },
                        TransactionDate = transactionDate,
                        SettlementDate = transactionDate,
                        SettlementStatus = InvestmentHoldingTransactionSettlementStatus.Settled,
                        TaxDate = transactionDate,
                        Delete = IsDeleted,
                        LastModified = DateTime.Now
                    };
                    trans.Add(epiDataResponseInvestmentHoldingMovementTransactionObj);
                }

                catch (Exception ex)
                { }
            }
        }
        public static bool IsExportableBankAccount(OrganizationCM organization, BankAccountEntity bankacount)
        {
            bool result = false;
            var institute = organization.Institution.Where(ss => ss.ID == bankacount.InstitutionID).FirstOrDefault();

            if (institute != null && institute.Name == "JP Morgan")
            {
                result = true;

            }
            return result;
        }
      
    }
}
