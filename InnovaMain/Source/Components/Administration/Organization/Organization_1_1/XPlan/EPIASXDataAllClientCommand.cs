﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.CalculationInterface;
using System.Xml.Linq;
using Oritax.TaxSimp.Common;

using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;
using System.Linq;
using EPI.Data;
using System;
using AccountType = EPI.Data.AccountType;

namespace Oritax.TaxSimp.CM.Organization
{
    public class EPIASXDataAllClientCommand : OrganizationCmCommandBase
    {


        public override string DoAction(string value)
        {
            string xml = string.Empty;
            try
            {
                bool addClient = true;
                var response = EPIUtilDataUtil.GetBasicReponse();
                var clients = new List<EPIDataResponseClient>();

                var clientAccounts = new List<EPIDataResponseAccount>();

                var advisors = new List<EPIDataResponseAdviser>();
                var clientHoldings = new List<EPIDataResponseInvestmentHolding>();
                var clientsHodlingTransactions = new List<EPIDataResponseInvestmentHoldingMovementTransaction>();
                var clientsInvestmentHodlingBalances = new List<EPIDataResponseHoldingBalance>();
                var ClientInvestmentHoldingIncomeEntitlements = new List<EPIDataResponseIncomeEntitlement>();
                OrganizationListAction.ForEachClientOnly("Administrator", Organization, (csid, logical, bmc) =>
                {
                    if ((bmc as OrganizationUnitCM).IsInvestableClient)
                    {

                        CorporateEntity clientcop = null;
                        ClientIndividualEntity individualEntity = null;
                        if ((bmc as OrganizationUnitCM).IsCorporateType())
                            clientcop = (bmc as OrganizationUnitCM).ClientEntity as CorporateEntity;
                        else
                            individualEntity = (bmc as OrganizationUnitCM).ClientEntity as ClientIndividualEntity;


                        List<AccountProcessTaskEntity> ListAccountProcess;
                        ObservableCollection<DividendEntity> dividendIncomes;
                        string clientID = (bmc as OrganizationUnitCM).ClientId;
                        List<ModelEntity> models = new List<ModelEntity>();


                        if (individualEntity != null)
                        {
                            dividendIncomes = individualEntity.DividendCollection;
                            ListAccountProcess = individualEntity.ListAccountProcess;
                            EPIUtilDataUtil.AddClientModels(individualEntity.Servicetype, models, Organization);
                        }
                        else
                        {
                            ListAccountProcess = clientcop.ListAccountProcess;
                            dividendIncomes = clientcop.DividendCollection;
                            EPIUtilDataUtil.AddClientModels(clientcop.Servicetype, models, Organization);
                        }

                        if (EPIUtilDataUtil.HasClientASXAccountAttached(ListAccountProcess))
                        {
                            var client = new EPIDataResponseClient();
                            client.Id = clientID;
                            string advisorid = "";
                            EPIDataResponseAdviser adviser = null;
                            if (bmc is ICmHasParent)
                            {

                                adviser = EPIUtilDataUtil.GetAdvisorId((bmc as ICmHasParent).Parent, Organization);
                            }
                            if (adviser != null)
                            {
                                advisorid = adviser.Id;
                                client.Advisers = new[]
                                                  {
                                                      new EPIDataResponseClientAdviser {Id = advisorid, PrimaryAdviser = true}
                                                  };

                                //if already not added advisor
                                if (advisors.Count(ss => ss.Id == advisorid) == 0)
                                {
                                    advisors.Add(adviser);
                                }
                                client.LastModified = DateTime.Now;

                                switch (logical.CMTypeName.ToLower())
                                {
                                    case "clientsmsfcorporatetrustee":
                                        client.Superfund = EPIUtilDataUtil.GetEntitySuperfund(clientcop.Name, clientcop.Signatories, Organization);

                                        client.TaxationDetails = EPIUtilDataUtil.GetTaxationDetails(clientcop.ABN, clientcop.TFN);
                                        if (clientcop.BankAccounts.Count > 0)
                                            client.BankDetails = EPIUtilDataUtil.GetBankDetails(clientcop.BankAccounts[0], Organization);
                                        AddClientHoldings(clientAccounts, clientcop.Name, AccountType.Super, clientID, advisorid, clientHoldings, clientsHodlingTransactions, clientsInvestmentHodlingBalances, ClientInvestmentHoldingIncomeEntitlements, ListAccountProcess, models, dividendIncomes);
                                        break;
                                    case "clientsmsfindividualtrustee":
                                    case "clienteclipsesuper":

                                        client.Superfund = EPIUtilDataUtil.GetEntitySuperfund(individualEntity.Name, individualEntity.Applicants, Organization);
                                        client.TaxationDetails = EPIUtilDataUtil.GetTaxationDetails(individualEntity.ABN, individualEntity.TFN);
                                        if (individualEntity.BankAccounts.Count > 0)
                                        {

                                            client.BankDetails = EPIUtilDataUtil.GetBankDetails(individualEntity.BankAccounts[0], Organization);

                                        }

                                        AddClientHoldings(clientAccounts, individualEntity.Name, AccountType.Super, clientID, advisorid, clientHoldings, clientsHodlingTransactions, clientsInvestmentHodlingBalances, ClientInvestmentHoldingIncomeEntitlements, ListAccountProcess, models, dividendIncomes);
                                        break;
                                    case "clientothertrustscorporate":
                                        client.Trust = EPIUtilDataUtil.GetTrust(clientcop.ABN, clientcop.Name, clientcop.Signatories, Organization);
                                        client.TaxationDetails = EPIUtilDataUtil.GetTaxationDetails(clientcop.ABN, clientcop.TFN);
                                        if (clientcop.BankAccounts.Count > 0)
                                            client.BankDetails = EPIUtilDataUtil.GetBankDetails(clientcop.BankAccounts[0], Organization);

                                        AddClientHoldings(clientAccounts, clientcop.Name, AccountType.Investment, clientID, advisorid, clientHoldings, clientsHodlingTransactions, clientsInvestmentHodlingBalances, ClientInvestmentHoldingIncomeEntitlements, ListAccountProcess, models, dividendIncomes);
                                        break;
                                    case "clientothertrustsindividual":
                                        client.Trust = EPIUtilDataUtil.GetTrust(individualEntity.ABN, individualEntity.Name, individualEntity.Applicants, Organization);
                                        client.TaxationDetails = EPIUtilDataUtil.GetTaxationDetails(individualEntity.ABN, individualEntity.TFN);
                                        if (individualEntity.BankAccounts.Count > 0)
                                            client.BankDetails = EPIUtilDataUtil.GetBankDetails(individualEntity.BankAccounts[0], Organization);

                                        AddClientHoldings(clientAccounts, individualEntity.Name, AccountType.Investment, clientID, advisorid, clientHoldings, clientsHodlingTransactions, clientsInvestmentHodlingBalances, ClientInvestmentHoldingIncomeEntitlements, ListAccountProcess, models, dividendIncomes);
                                        break;
                                    case "clientcorporationprivate":
                                    case "clientcorporationpublic":
                                    case "soletrader":
                                        client.Organisation = EPIUtilDataUtil.GetEntityOrganization(clientcop);
                                        client.TaxationDetails = EPIUtilDataUtil.GetTaxationDetails(clientcop.ABN, clientcop.TFN);
                                        if (clientcop.BankAccounts.Count > 0)
                                            client.BankDetails = EPIUtilDataUtil.GetBankDetails(clientcop.BankAccounts[0], Organization);

                                        AddClientHoldings(clientAccounts, clientcop.Name, AccountType.Investment, clientID, advisorid, clientHoldings, clientsHodlingTransactions, clientsInvestmentHodlingBalances, ClientInvestmentHoldingIncomeEntitlements, ListAccountProcess, models, dividendIncomes);
                                        break;
                                    case "clientindividual":
                                        client.Person = EPIUtilDataUtil.GetPersonEntityFromPrimaryCMDetail(individualEntity.Applicants, Organization);
                                        if (client.Person != null)
                                        {
                                            client.TaxationDetails = EPIUtilDataUtil.GetTaxationDetails(individualEntity.ABN, individualEntity.TFN);
                                            if (individualEntity.BankAccounts.Count > 0)
                                                client.BankDetails = EPIUtilDataUtil.GetBankDetails(individualEntity.BankAccounts[0], Organization);
                                            AddClientHoldings(clientAccounts, individualEntity.Name, AccountType.Investment, clientID, advisorid, clientHoldings, clientsHodlingTransactions, clientsInvestmentHodlingBalances, ClientInvestmentHoldingIncomeEntitlements, ListAccountProcess, models, dividendIncomes);
                                        }
                                        else
                                            addClient = false;
                                        break;
                                    default:
                                        break;
                                }
                                if (addClient)
                                    clients.Add(client);
                            }

                        }
                    }
                });



                var InvestmentProducts = new List<EPIDataResponseInvestmentProductsInvestmentProduct>();
                foreach (var productsKey in InnovaProducts.ProductEPICodes.Keys)
                {
                    var innovaProduct = InnovaProducts.ProductEPICodes[productsKey];
                    if (innovaProduct.InnovaProductType == InnovaProductType.ASX)
                    {
                        var InvestmentProduct = new EPIDataResponseInvestmentProductsInvestmentProduct
                            {
                                InvestmentCode = innovaProduct.InnovaProductCode,
                                Exchange = innovaProduct.Exchange,
                                Name = innovaProduct.Desc,
                                Type = innovaProduct.Type,
                                Status = InvestmentProductStatus.Open,
                                Unitised = true,
                                LastModified = DateTime.Now,

                            };
#region ASX prices skipped 
                        //var security = Organization.Securities.FirstOrDefault(ss => ss.AsxCode == productsKey);
                        //if (security != null && security.ASXSecurity != null && security.ASXSecurity.Count > 0)
                        //{

                        //    var UnitPrices = new List<InvestmentProductUnitPrice>();

                        //    foreach (var price in security.ASXSecurity)
                        //    {
                        //        CurrencyCode currency;
                        //        if (!System.Enum.TryParse(price.Currency, true, out currency))
                        //        {
                        //            currency = CurrencyCode.AUD;
                        //        }

                        //        UnitPrices.Add(new InvestmentProductUnitPrice { AsAtDate = price.Date, LastModified = price.Date, Price = new UnitPricePrice { Currency = currency, Value = decimal.Round((decimal)price.UnitPrice, 6) } });
                        //    }

                        //    InvestmentProduct.UnitPrices = UnitPrices.ToArray();

                        //}
#endregion ASX prices skipped
                        var assetAllocations = new List<InvestmentProductAssetAllocation>();


                        assetAllocations.Add(new InvestmentProductAssetAllocation
                            {
                                AsAtDate = innovaProduct.AssestAllocation.AsAtDate,
                                LastModified = innovaProduct.AssestAllocation.LastModified,
                                DomesticCash = innovaProduct.AssestAllocation.DomesticCash,
                                DomesticDirectProperty = innovaProduct.AssestAllocation.DomesticDirectProperty,
                                DomesticFixedInterest = innovaProduct.AssestAllocation.DomesticFixedInterest,
                                DomesticProperty = innovaProduct.AssestAllocation.DomesticProperty,
                                DomesticShares = innovaProduct.AssestAllocation.DomesticShares,
                                ForeignCash = innovaProduct.AssestAllocation.ForeignCash,
                                ForeignDirectProperty = innovaProduct.AssestAllocation.ForeignDirectProperty,
                                ForeignFixedInterest = innovaProduct.AssestAllocation.ForeignFixedInterest,
                                ForeignProperty = innovaProduct.AssestAllocation.ForeignProperty,
                                ForeignShares = innovaProduct.AssestAllocation.ForeignShares,
                                Other = innovaProduct.AssestAllocation.Other,
                            });
                        InvestmentProduct.AssetAllocations = assetAllocations.ToArray();


                        InvestmentProducts.Add(InvestmentProduct);
                    }
                }

                response.ProductsAndSuperfunds = new ProductsAndSuperFunds { InvestmentProducts = InvestmentProducts.ToArray() };
                response.Clients = clients.ToArray();
                response.Accounts = clientAccounts.ToArray();
                response.InvestmentHoldings = clientHoldings.ToArray();
                response.InvestmentHoldingMovementTransactions = clientsHodlingTransactions.ToArray();
                response.InvestmentHoldingBalances = clientsInvestmentHodlingBalances.ToArray();
                if (ClientInvestmentHoldingIncomeEntitlements.Count > 0)
                    response.InvestmentHoldingIncomeEntitlements = ClientInvestmentHoldingIncomeEntitlements.ToArray();
                response.Advisers = advisors.ToArray();

                xml = response.ToXmlString();
                //todo : should be replaced with wildcard replace method
                for (int i = 1; i < 10; i++)
                {
                    for (int j = 1; j < 5; j++)
                    {
                        xml = xml.Replace("<TempItems xmlns:d" + i + "p" + j + "=\"http://schemas.datacontract.org/2004/07/System.Xml.Linq\">", "");
                        xml = xml.Replace("<d" + i + "p" + j + ":XElement>", "");
                        xml = xml.Replace("</d" + i + "p" + j + ":XElement>", "");
                        xml = xml.Replace("xmlns:d" + i + "p" + j + "=\"http://schemas.datacontract.org/2004/07/System.Xml.Linq\"", "");

                    }
                }

                xml = xml.Replace("</TempItems>", "");
                xml = xml.Replace("<TempTag>", "");
                xml = xml.Replace("</TempTag>", "");

                for (int i = 1; i <= 20; i++)
                {
                    xml = xml.Replace("<TempTag" + i + ">", "");
                    xml = xml.Replace("</TempTag" + i + ">", "");
                }

                xml = xml.Replace(" xmlns=\"\"", "");
                xml = xml.Replace("i:nil=\"true\"", "");
            }
            catch (Exception ex)
            {
                xml = "<Error>" + ex.Message + "</Error>";
            }
            return xml;
        }

        private void AddClientHoldings(List<EPIDataResponseAccount> Clientaccounts, string ClientName, AccountType typeofacttount, string clientID, string advisorId, List<EPIDataResponseInvestmentHolding> holdings, List<EPIDataResponseInvestmentHoldingMovementTransaction> trans, List<EPIDataResponseHoldingBalance> holdingBalances, List<EPIDataResponseIncomeEntitlement> incomeEntitlements, List<AccountProcessTaskEntity> ListAccountProcess, List<ModelEntity> models, ObservableCollection<DividendEntity> dividendIncomes)
        {
            try
            {
                var taskEntities = ListAccountProcess.Where(
                       ss =>
                       ss.LinkedEntityType == OrganizationType.DesktopBrokerAccount &&
                       ss.LinkedEntity.Csid != Guid.Empty && ss.LinkedEntity.Clid != Guid.Empty).GroupBy(p => p.LinkedEntity.Csid).Select(grp => grp.First()); ;

                foreach (var accountProcessTaskEntity in taskEntities)
                {
                    var md = models.Where(ss => ss.ID == accountProcessTaskEntity.ModelID).FirstOrDefault();
                    if (md != null)
                    {
                        var asst = md.Assets.Where(ss => ss.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                        if (asst != null)
                        {
                            var prd = asst.Products.Where(ss => ss.ID == accountProcessTaskEntity.TaskID).FirstOrDefault();
                            if (prd != null)
                            {
                                var comp = this.Organization.Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid);
                                if (comp != null)
                                {

                                    var brokerAccountEntity = (comp as OrganizationUnitCM).ClientEntity as DesktopBrokerAccountEntity;

                                    if (brokerAccountEntity != null)
                                    {
                                        if (brokerAccountEntity.holdingTransactions != null)
                                        {
                                            var investmentCodes = (from p in brokerAccountEntity.holdingTransactions select p.InvestmentCode).Distinct();
                                            foreach (string InvestmentCode in investmentCodes)
                                            {
                                                string EPICode = InnovaProducts.GetEpiCode(InvestmentCode);
                                                string Account_ID = clientID + "_" + InvestmentCode + "_" + EPICode;

                                                if (Clientaccounts.Count(ss => ss.Id == Account_ID) == 0)
                                                {

                                                    var clientAccount = new EPIDataResponseAccount();
                                                    if (!string.IsNullOrEmpty(advisorId))
                                                        clientAccount.Advisers = new AccountAdviser[]
                                                    {
                                                        new AccountAdviser {Id = advisorId, PrimaryAdviser = true}
                                                    };
                                                    clientAccount.Id = Account_ID;

                                                    EPIUtilDataUtil.SetClientAccount(typeofacttount, ClientName, "", AccountStatusStatus.Open, CurrencyCode.AUD, clientAccount);

                                                    clientAccount.EPIProductCode = EPICode.ReplaceEmptyWithNull();
                                                    clientAccount.Owners = new AccountOwner[] { new AccountOwner { Id = clientID, PercentageOfOwnership = 100 } };

                                                    // clientAccount.FPSId = "xxx";
                                                    clientAccount.Delete = false;
                                                    clientAccount.LastModified = DateTime.Now;

                                                    Clientaccounts.Add(clientAccount);
                                                }
                                                if (holdings.Count(ss => ss.AccountId == Account_ID && ss.InvestmentCode == InvestmentCode) == 0)
                                                {

                                                    holdings.Add(new EPIDataResponseInvestmentHolding { Exchange = "ASX", AccountId = Account_ID, Deleted = false, InvestmentCode = InvestmentCode.ReplaceEmptyWithNull(), IsSubjectToCGT = true, IsCashAccount = false, }); //, HoldingId = fundacc.Code.ReplaceEmptyWithNull()


                                                    if (brokerAccountEntity.holdingTransactions != null)
                                                    {
                                                        var tt = brokerAccountEntity.holdingTransactions.Where(ss => ss.InvestmentCode == InvestmentCode);
                                                        var ttordered = tt.OrderByDescending(ss => ss.TradeDate).FirstOrDefault();
                                                        var ut = brokerAccountEntity.UnSettledOrders.Where(ss => ss.InvestmentCode == InvestmentCode);
                                                        decimal settled = tt.Sum(ss => ss.Units);
                                                        decimal unsettled = ut.Sum(ss => ss.UnsettledOrders);

                                                        DateTime? date = null;
                                                        if (ttordered != null)
                                                        {
                                                            date = ttordered.TradeDate;
                                                            var balac = new EPIDataResponseHoldingBalance { LastModified = DateTime.Now, AccountId = Account_ID, InvestmentCode = InvestmentCode.ReplaceEmptyWithNull(), Exchange = "ASX" }; //, HoldingId = fundacc.Code.ReplaceEmptyWithNull()
                                                            balac.UnitBalance = new InvestmentHoldingBalanceUnitBalance { AsAtDate = date.Value, Settled = settled, Pending = unsettled };
                                                            holdingBalances.Add(balac);
                                                        }

                                                        foreach (var tran in tt)
                                                        {
                                                            try
                                                            {

                                                                trans.Add(EPIUtilDataUtil.GenerateASXTransaction(tran, Account_ID, InvestmentCode, false,Organization));
                                                            }
                                                            catch (Exception hh)
                                                            {
                                                                throw hh;
                                                            }
                                                        }

                                                    }
                                                    if (brokerAccountEntity.DeletedHoldingTransactions != null)
                                                    {

                                                        foreach (var tran in brokerAccountEntity.DeletedHoldingTransactions.Where(ss => ss.InvestmentCode == InvestmentCode))
                                                        {
                                                            try
                                                            {

                                                                trans.Add(EPIUtilDataUtil.GenerateASXTransaction(tran, Account_ID, InvestmentCode, true, Organization));
                                                            }
                                                            catch (Exception eh)
                                                            {
                                                                throw eh;
                                                            }
                                                        }
                                                    }

                                                }
                                                if (dividendIncomes.Count(ss => ss.InvestmentCode == InvestmentCode) > 0)
                                                {
                                                    var distributions = dividendIncomes.Where(ss => ss.InvestmentCode == InvestmentCode);
                                                    foreach (var distributionIncomeEntity in distributions)
                                                    {
                                                        var incomeEntitlement = new EPIDataResponseIncomeEntitlement
                                                            {
                                                                EPIIncomeType = InvestmentHoldingIncomeEntitlementEPIIncomeType.I00002,
                                                                Id = distributionIncomeEntity.ID.ToString(),
                                                                LastModified = DateTime.Now,
                                                                AccountId = Account_ID,
                                                                //  HoldingId =fundacc.Code.ReplaceEmptyWithNull(),
                                                                InvestmentCode = InvestmentCode.ReplaceEmptyWithNull(),
                                                                Exchange = "ASX",
                                                                Units = (decimal)distributionIncomeEntity.UnitsOnHand,
                                                                AmountPerUnit = new MonetaryAmount { Currency = CurrencyCode.AUD, Value = distributionIncomeEntity.UnitsOnHand != 0.0 ? decimal.Round((decimal)(distributionIncomeEntity.CentsPerShare), 6) : 0 },
                                                                Status = InvestmentHoldingIncomeEntitlementStatus.Final,
                                                            };

                                                        if (distributionIncomeEntity.RecordDate.HasValue)
                                                            incomeEntitlement.AccrualDate = distributionIncomeEntity.RecordDate.Value;
                                                        if (distributionIncomeEntity.PaymentDate.HasValue)
                                                        {
                                                            incomeEntitlement.PaymentDate = distributionIncomeEntity.PaymentDate.Value;
                                                            incomeEntitlement.TaxDate = distributionIncomeEntity.PaymentDate.Value;
                                                        }


                                                        incomeEntitlement.GrossAmount = new MonetaryAmount { Currency = CurrencyCode.AUD, Value = decimal.Round((decimal)distributionIncomeEntity.PaidDividend, 6) };
                                                        incomeEntitlement.NetAmount = new MonetaryAmount { Currency = CurrencyCode.AUD, Value = decimal.Round((decimal)distributionIncomeEntity.PaidDividend, 6) };


                                                        incomeEntitlements.Add(incomeEntitlement);
                                                    }

                                                }


                                            }
                                        }





                                    }

                                    this.Organization.Broker.ReleaseBrokerManagedComponent(comp);
                                }
                            }

                        }


                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }


    }
}
