﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.CalculationInterface;
using System.Xml.Linq;
using Oritax.TaxSimp.Common;

using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;
using System.Linq;
using EPI.Data;
using System;
using AccountType = EPI.Data.AccountType;
using Oritax.TaxSimp.Calculation;
using System.Data;
using Oritax.TaxSimp.CM.Organization.Data;

namespace Oritax.TaxSimp.CM.Organization
{
    public class EPIDataAllClientCommand : OrganizationCmCommandBase
    {
      public override string DoAction(string value)
        {
            string xml = string.Empty;
            try
            {
                bool addClient = true;
                var response = EPIUtilDataUtil.GetBasicReponse();
                var clients = new List<EPIDataResponseClient>();

                var clientAccounts = new List<EPIDataResponseAccount>();

                var advisors = new List<EPIDataResponseAdviser>();
                var clientHoldings = new List<EPIDataResponseInvestmentHolding>();
                var clientsHodlingTransactions = new List<EPIDataResponseInvestmentHoldingMovementTransaction>();
                var clientsInvestmentHodlingBalances = new List<EPIDataResponseHoldingBalance>();
                var ClientInvestmentHoldingIncomeEntitlements = new List<EPIDataResponseIncomeEntitlement>();


                IOrganizationUnit manageFundBMC = this.Organization.Broker.GetBMCInstance(new Guid("b354e928-a3c2-43d7-b61e-feb215cf1bd3")) as IOrganizationUnit;
                ManagedInvestmentSchemesAccountEntity managedInvestmentSchemesAccountEntity = manageFundBMC.ClientEntity as ManagedInvestmentSchemesAccountEntity;
                IBrokerManagedComponent user = this.Organization.Broker.GetBMCInstance("Administrator", "DBUser_1_1");
                object[] args = new Object[3] { Organization.CLID, user.CID, EnableSecurity.SecuritySetting };
                DataView view = new DataView(this.Organization.Broker.GetBMCDetailList(OrganizationInstall.ASSEMBLY_NAME, args).Tables[OrganisationListingDS.TableName]);
                view.RowFilter = new OrganisationListingDS().GetRowFilter();
                view.Sort = "ENTITYNAME_FIELD ASC";
                DataTable orgFiteredTable = view.ToTable();

                foreach (DataRow row in orgFiteredTable.Rows)
                {
                    Guid cid = new Guid(row["ENTITYCIID_FIELD"].ToString());
                    IOrganizationUnit bmc = Organization.Broker.GetBMCInstance(cid) as IOrganizationUnit;

                    if (bmc.IsInvestableClient)
                    {
                        CorporateEntity clientcop = null;
                        ClientIndividualEntity individualEntity = null;
                        if ((bmc as OrganizationUnitCM).IsCorporateType())
                            clientcop = (bmc as OrganizationUnitCM).ClientEntity as CorporateEntity;
                        else
                            individualEntity = (bmc as OrganizationUnitCM).ClientEntity as ClientIndividualEntity;

                        IClientUMAData clientEntity = bmc.ClientEntity as IClientUMAData;

                        List<AccountProcessTaskEntity> ListAccountProcess;
                        List<DistributionIncomeEntity> Distributions;
                        ObservableCollection<DividendEntity> dividends;
                        string clientID = bmc.EclipseClientID;
                        List<ModelEntity> models = new List<ModelEntity>();
                        dividends = clientEntity.DividendCollection;

                        if (individualEntity != null)
                        {
                            Distributions = individualEntity.DistributionIncomes.Where(ss => ss.NetCashDistribution != 0 && ss.NetGrossDistribution != 0).ToList();
                            ListAccountProcess = individualEntity.ListAccountProcess;
                            EPIUtilDataUtil.AddClientModels(individualEntity.Servicetype, models, Organization, ListAccountProcess);
                        }
                        else
                        {
                            ListAccountProcess = clientcop.ListAccountProcess;
                            Distributions = clientcop.DistributionIncomes.Where(ss => ss.NetCashDistribution != 0 && ss.NetGrossDistribution != 0).ToList();
                            EPIUtilDataUtil.AddClientModels(clientcop.Servicetype, models, Organization, ListAccountProcess);
                        }

                        if (EPIUtilDataUtil.HasClientMISAccountAttached(ListAccountProcess))
                        {
                            var client = new EPIDataResponseClient();
                            client.Id = clientID;
                            string advisorid = "";
                            EPIDataResponseAdviser adviser = null;
                            IOrganizationUnit ifaOrgUnit = null;

                            if (bmc is ICmHasParent)
                            {
                                IOrganizationUnit advOrgUnit = EPIUtilDataUtil.GetIOrgUnitEntity((bmc as ICmHasParent).Parent, Organization);
                                adviser = EPIUtilDataUtil.GetAdviserEPIEntity(adviser, advOrgUnit);
                                if (advOrgUnit is ICmHasParent)
                                {
                                    ifaOrgUnit = EPIUtilDataUtil.GetIOrgUnitEntity((advOrgUnit as ICmHasParent).Parent, Organization);
                                }
                            }
                            if (adviser != null)
                            {
                                advisorid = adviser.Id;
                                client.Advisers = new EPIDataResponseClientAdviser[]
                                                  {
                                                      new EPIDataResponseClientAdviser {Id = advisorid, PrimaryAdviser = true}
                                                  };

                                //if already not added advisor
                                if (advisors.Count(ss => ss.Id == advisorid) == 0)
                                {
                                    advisors.Add(adviser);
                                }

                                client.LastModified = DateTime.Now;

                                switch (bmc.TypeName.ToLower())
                                {
                                    case "clientsmsfcorporatetrustee":
                                        client.Superfund = EPIUtilDataUtil.GetEntitySuperfund(clientcop.Name, clientcop.Signatories, Organization);

                                        client.TaxationDetails = EPIUtilDataUtil.GetTaxationDetails(clientcop.ABN, clientcop.TFN);
                                        if (clientcop.BankAccounts.Count > 0)
                                            client.BankDetails = EPIUtilDataUtil.GetBankDetails(clientcop.BankAccounts[0], Organization);

                                        AddClientHoldings(managedInvestmentSchemesAccountEntity, clientAccounts, clientcop.Name, AccountType.Super, clientID, advisorid, clientHoldings, clientsHodlingTransactions, clientsInvestmentHodlingBalances, ClientInvestmentHoldingIncomeEntitlements, ListAccountProcess, models, Distributions);
                                        AddClientHoldingsASX(clientAccounts, clientEntity.Name, AccountType.DirectEquity, bmc.EclipseClientID, advisorid, clientHoldings, clientsHodlingTransactions, clientsInvestmentHodlingBalances, ClientInvestmentHoldingIncomeEntitlements, ListAccountProcess, models, Distributions, dividends);
                                        break;
                                    case "clientsmsfindividualtrustee":
                                    case "clienteclipsesuper":

                                        client.Superfund = EPIUtilDataUtil.GetEntitySuperfund(individualEntity.Name, individualEntity.Applicants, Organization);

                                        client.TaxationDetails = EPIUtilDataUtil.GetTaxationDetails(individualEntity.ABN, individualEntity.TFN);
                                        if (individualEntity.BankAccounts.Count > 0)
                                            client.BankDetails = EPIUtilDataUtil.GetBankDetails(individualEntity.BankAccounts[0], Organization);

                                        if (bmc.TypeName.ToLower() == "clienteclipsesuper")
                                        {
                                            if (models.Where(m => m.ProgramCode == "0110").FirstOrDefault() == null)
                                            {
                                                var model = Organization.Model.Where(m => m.ProgramCode == "0110").FirstOrDefault();
                                                models.Add(model);
                                            }

                                            if (models.Where(m => m.ProgramCode == "0116").FirstOrDefault() == null)
                                            {
                                                var model = Organization.Model.Where(m => m.ProgramCode == "0116").FirstOrDefault();
                                                models.Add(model);
                                            }

                                            this.AddECLIPSESUPERJPMHoldings(clientAccounts, bmc.Name, AccountType.Super, bmc.EclipseClientID, advisorid, clientHoldings, clientsHodlingTransactions, clientsInvestmentHodlingBalances, ClientInvestmentHoldingIncomeEntitlements, ListAccountProcess, models, Distributions, dividends);
                                        }

                                        AddClientHoldings(managedInvestmentSchemesAccountEntity, clientAccounts, individualEntity.Name, AccountType.Super, clientID, advisorid, clientHoldings, clientsHodlingTransactions, clientsInvestmentHodlingBalances, ClientInvestmentHoldingIncomeEntitlements, ListAccountProcess, models, Distributions);
                                        AddClientHoldingsASX(clientAccounts, clientEntity.Name, AccountType.DirectEquity, bmc.EclipseClientID, advisorid, clientHoldings, clientsHodlingTransactions, clientsInvestmentHodlingBalances, ClientInvestmentHoldingIncomeEntitlements, ListAccountProcess, models, Distributions, dividends);
                                        break;
                                    case "clientothertrustscorporate":
                                        client.Trust = EPIUtilDataUtil.GetTrust(clientcop.ABN, clientcop.Name, clientcop.Signatories, Organization);
                                        client.TaxationDetails = EPIUtilDataUtil.GetTaxationDetails(clientcop.ABN, clientcop.TFN);
                                        if (clientcop.BankAccounts.Count > 0)
                                            client.BankDetails = EPIUtilDataUtil.GetBankDetails(clientcop.BankAccounts[0], Organization);

                                        AddClientHoldings(managedInvestmentSchemesAccountEntity, clientAccounts, clientcop.Name, AccountType.Investment, clientID, advisorid, clientHoldings, clientsHodlingTransactions, clientsInvestmentHodlingBalances, ClientInvestmentHoldingIncomeEntitlements, ListAccountProcess, models, Distributions);
                                        AddClientHoldingsASX(clientAccounts, clientEntity.Name, AccountType.DirectEquity, bmc.EclipseClientID, advisorid, clientHoldings, clientsHodlingTransactions, clientsInvestmentHodlingBalances, ClientInvestmentHoldingIncomeEntitlements, ListAccountProcess, models, Distributions, dividends);
                                        break;
                                    case "clientothertrustsindividual":
                                        client.Trust = EPIUtilDataUtil.GetTrust(individualEntity.ABN, individualEntity.Name, individualEntity.Applicants, Organization);
                                        client.TaxationDetails = EPIUtilDataUtil.GetTaxationDetails(individualEntity.ABN, individualEntity.TFN);
                                        if (individualEntity.BankAccounts.Count > 0)
                                            client.BankDetails = EPIUtilDataUtil.GetBankDetails(individualEntity.BankAccounts[0], Organization);

                                        AddClientHoldings(managedInvestmentSchemesAccountEntity, clientAccounts, individualEntity.Name, AccountType.Investment, clientID, advisorid, clientHoldings, clientsHodlingTransactions, clientsInvestmentHodlingBalances, ClientInvestmentHoldingIncomeEntitlements, ListAccountProcess, models, Distributions);
                                        AddClientHoldingsASX(clientAccounts, clientEntity.Name, AccountType.DirectEquity, bmc.EclipseClientID, advisorid, clientHoldings, clientsHodlingTransactions, clientsInvestmentHodlingBalances, ClientInvestmentHoldingIncomeEntitlements, ListAccountProcess, models, Distributions, dividends);
                                        break;
                                    case "clientcorporationprivate":
                                    case "clientcorporationpublic":
                                    case "soletrader":
                                        client.Organisation = EPIUtilDataUtil.GetEntityOrganization(clientcop);
                                        client.TaxationDetails = EPIUtilDataUtil.GetTaxationDetails(clientcop.ABN, clientcop.TFN);
                                        if (clientcop.BankAccounts.Count > 0)
                                            client.BankDetails = EPIUtilDataUtil.GetBankDetails(clientcop.BankAccounts[0], Organization);

                                        AddClientHoldings(managedInvestmentSchemesAccountEntity, clientAccounts, clientcop.Name, AccountType.Investment, clientID, advisorid, clientHoldings, clientsHodlingTransactions, clientsInvestmentHodlingBalances, ClientInvestmentHoldingIncomeEntitlements, ListAccountProcess, models, Distributions);
                                        AddClientHoldingsASX(clientAccounts, clientEntity.Name, AccountType.DirectEquity, bmc.EclipseClientID, advisorid, clientHoldings, clientsHodlingTransactions, clientsInvestmentHodlingBalances, ClientInvestmentHoldingIncomeEntitlements, ListAccountProcess, models, Distributions, dividends);
                                        break;
                                    case "clientindividual":
                                        client.Person = EPIUtilDataUtil.GetPersonEntityFromPrimaryCMDetail(individualEntity.Applicants, Organization);
                                        if (client.Person != null)
                                        {
                                            client.TaxationDetails = EPIUtilDataUtil.GetTaxationDetails(individualEntity.ABN, individualEntity.TFN);
                                            if (individualEntity.BankAccounts.Count > 0)
                                                client.BankDetails = EPIUtilDataUtil.GetBankDetails(individualEntity.BankAccounts[0], Organization);
                                            AddClientHoldings(managedInvestmentSchemesAccountEntity, clientAccounts, individualEntity.Name, AccountType.Investment, clientID, advisorid, clientHoldings, clientsHodlingTransactions, clientsInvestmentHodlingBalances, ClientInvestmentHoldingIncomeEntitlements, ListAccountProcess, models, Distributions);
                                            AddClientHoldingsASX(clientAccounts, clientEntity.Name, AccountType.DirectEquity, bmc.EclipseClientID, advisorid, clientHoldings, clientsHodlingTransactions, clientsInvestmentHodlingBalances, ClientInvestmentHoldingIncomeEntitlements, ListAccountProcess, models, Distributions, dividends);
                                        }
                                        else
                                            addClient = false;
                                        break;
                                    default:
                                        break;
                                }
                                if (addClient)
                                    clients.Add(client);
                            }

                        }
                    }
                }

                //EPIUtilDataUtil.GenerateBankTransaction( clientsHodlingTransactions, @"C:\temp\INOVTradesNotDeleted", "JPMCASH", true);

                var InvestmentProducts = new List<EPIDataResponseInvestmentProductsInvestmentProduct>();
                foreach (var productsKey in InnovaProducts.ProductEPICodes.Keys)
                {
                    var innovaProduct = InnovaProducts.ProductEPICodes[productsKey];
                    if (innovaProduct.InnovaProductType == InnovaProductType.MIS || innovaProduct.InnovaProductType == InnovaProductType.CASH)
                    {
                        var InvestmentProduct = new EPIDataResponseInvestmentProductsInvestmentProduct
                            {
                                InvestmentCode = innovaProduct.InnovaProductCode,
                                Exchange = innovaProduct.Exchange,
                                Name = innovaProduct.Desc,
                                Type = innovaProduct.Type,
                                Status = InvestmentProductStatus.Open,
                                Unitised = true,
                                LastModified = DateTime.Now,

                            };
                        var security = Organization.Securities.FirstOrDefault(ss => ss.AsxCode == productsKey);
                        if (security != null && security.ASXSecurity != null && security.ASXSecurity.Count > 0)
                        {

                            var UnitPrices = new List<InvestmentProductUnitPrice>();

                            foreach (var price in security.ASXSecurity)
                            {
                                CurrencyCode currency;
                                if (!System.Enum.TryParse(price.Currency, true, out currency))
                                {
                                    currency = CurrencyCode.AUD;
                                }

                                UnitPrices.Add(new InvestmentProductUnitPrice {AsAtDate = price.Date, LastModified = price.Date, Price = new UnitPricePrice {Currency = currency, Value = (decimal) price.UnitPrice}});
                            }

                            InvestmentProduct.UnitPrices = UnitPrices.ToArray();

                        }
                        var assetAllocations = new List<InvestmentProductAssetAllocation>();


                        assetAllocations.Add(new InvestmentProductAssetAllocation
                            {
                                AsAtDate = innovaProduct.AssestAllocation.AsAtDate,
                                LastModified = innovaProduct.AssestAllocation.LastModified,
                                DomesticCash = innovaProduct.AssestAllocation.DomesticCash,
                                DomesticDirectProperty = innovaProduct.AssestAllocation.DomesticDirectProperty,
                                DomesticFixedInterest = innovaProduct.AssestAllocation.DomesticFixedInterest,
                                DomesticProperty = innovaProduct.AssestAllocation.DomesticProperty,
                                DomesticShares = innovaProduct.AssestAllocation.DomesticShares,
                                ForeignCash = innovaProduct.AssestAllocation.ForeignCash,
                                ForeignDirectProperty = innovaProduct.AssestAllocation.ForeignDirectProperty,
                                ForeignFixedInterest = innovaProduct.AssestAllocation.ForeignFixedInterest,
                                ForeignProperty = innovaProduct.AssestAllocation.ForeignProperty,
                                ForeignShares = innovaProduct.AssestAllocation.ForeignShares,
                                Other = innovaProduct.AssestAllocation.Other,
                            });
                        InvestmentProduct.AssetAllocations = assetAllocations.ToArray();


                        InvestmentProducts.Add(InvestmentProduct);
                    }
                }

                response.ProductsAndSuperfunds = new ProductsAndSuperFunds { InvestmentProducts = InvestmentProducts.ToArray() };
                response.Clients = clients.ToArray();
                response.Accounts = clientAccounts.ToArray();
                response.InvestmentHoldings = clientHoldings.ToArray();
                response.InvestmentHoldingMovementTransactions = clientsHodlingTransactions.ToArray();
                response.InvestmentHoldingBalances = clientsInvestmentHodlingBalances.ToArray();
                response.InvestmentHoldingIncomeEntitlements = ClientInvestmentHoldingIncomeEntitlements.ToArray();
                response.Advisers = advisors.ToArray();

                xml = response.ToXmlString();
                //todo : should be replaced with wildcard replace method
                for (int i = 1; i < 10; i++)
                {
                    for (int j = 1; j < 5; j++)
                    {
                        xml = xml.Replace("<TempItems xmlns:d" + i + "p" + j + "=\"http://schemas.datacontract.org/2004/07/System.Xml.Linq\">", "");
                        xml = xml.Replace("<d" + i + "p" + j + ":XElement>", "");
                        xml = xml.Replace("</d" + i + "p" + j + ":XElement>", "");
                        xml = xml.Replace("xmlns:d" + i + "p" + j + "=\"http://schemas.datacontract.org/2004/07/System.Xml.Linq\"", "");

                    }
                }



                xml = xml.Replace("</TempItems>", "");
                xml = xml.Replace("<TempTag>", "");
                xml = xml.Replace("</TempTag>", "");

                for (int i = 1; i <= 20; i++)
                {
                    xml = xml.Replace("<TempTag" + i + ">", "");
                    xml = xml.Replace("</TempTag" + i + ">", "");
                }


                xml = xml.Replace(" xmlns=\"\"", "");


                xml = xml.Replace("i:nil=\"true\"", "");

            }
            catch (Exception ex)
            {
                xml = "<Error>" + ex.Message + "</Error>";
            }
            return xml;
        }

      private static bool IsValidIFA(IOrganizationUnit ifaOrgUnit)
      {
          return (ifaOrgUnit != null && (ifaOrgUnit.CID.ToString() == "271da8ff-c8a0-4801-b292-8ea4c8d20d07" || ifaOrgUnit.CID.ToString() == "26bc00ff-a578-4758-bfc9-94f0c267d3ec" || ifaOrgUnit.CID.ToString() == "5083ef2e-b189-4470-844c-d6f8cdc63d7d" || ifaOrgUnit.CID.ToString() == "ad09db4e-3923-48d7-8884-aba53dcfd88d" || ifaOrgUnit.CID.ToString() == "ee508b33-412e-488f-a6c9-a605b5d785a8" || ifaOrgUnit.CID.ToString() == "054b042d-fdc9-401d-9c3b-66487b9c36f0" || ifaOrgUnit.CID.ToString() == "4217bb03-d9df-4a51-b9b9-24fee00e11af"));
      }

      private void AddECLIPSESUPERJPMHoldings(List<EPIDataResponseAccount> Clientaccounts, string ClientName, AccountType typeofacttount, string clientID, string advisorId, List<EPIDataResponseInvestmentHolding> holdings, List<EPIDataResponseInvestmentHoldingMovementTransaction> trans, List<EPIDataResponseHoldingBalance> holdingBalances, List<EPIDataResponseIncomeEntitlement> incomeEntitlements, List<AccountProcessTaskEntity> listAccountProcess, List<ModelEntity> models, List<DistributionIncomeEntity> distributionIncomes, ObservableCollection<DividendEntity> dividendIncomes)
      {
          #region JMP CASH DATA

          try
          {
              var taskEntities = listAccountProcess.Where(ss => ss.LinkedEntityType == OrganizationType.BankAccount && ss.LinkedEntity.Csid != Guid.Empty && ss.LinkedEntity.Clid != Guid.Empty).GroupBy(p => p.LinkedEntity.Csid).Select(grp => grp.First());

              foreach (var accountProcessTaskEntity in taskEntities)
              {
                  var md = models.Where(ss => ss.ID == accountProcessTaskEntity.ModelID).FirstOrDefault();

                  if (md != null)
                  {
                      var asst = md.Assets.Where(ss => ss.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                      if (asst != null)
                      {
                          var prd = asst.Products.Where(ss => ss.ID == accountProcessTaskEntity.TaskID).FirstOrDefault();
                          if (prd != null)
                          {
                              var comp = Organization.Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid);
                              if (comp != null)
                              {
                                  var bankAccountEntity = (comp as OrganizationUnitCM).ClientEntity as BankAccountEntity;
                                  if (bankAccountEntity != null && EPIUtilDataUtil.IsExportableBankAccount(Organization, bankAccountEntity))
                                  {
                                      if (bankAccountEntity.CashManagementTransactions != null)
                                      {
                                          string investmentCode = "JPMCASH";

                                          string EPICode = "INOV000001"; //GetEpiCode(InvestmentCode);
                                          string Account_ID = clientID; //+ "_" + InvestmentCode + "_" + EPICode;

                                          if (Clientaccounts.Count(ss => ss.Id == Account_ID) == 0)
                                          {

                                              var clientAccount = new EPIDataResponseAccount();
                                              if (!string.IsNullOrEmpty(advisorId))
                                                  clientAccount.Advisers = new AccountAdviser[]
                                                                {
                                                                    new AccountAdviser {Id = advisorId, PrimaryAdviser = true}
                                                                };
                                              clientAccount.Id = Account_ID;

                                              EPIUtilDataUtil.SetClientAccount(typeofacttount, ClientName, "", AccountStatusStatus.Open, CurrencyCode.AUD, clientAccount);

                                              clientAccount.EPIProductCode = EPICode.ReplaceEmptyWithNull();
                                              clientAccount.Owners = new AccountOwner[] { new AccountOwner { Id = clientID, PercentageOfOwnership = 100 } };

                                              // clientAccount.FPSId = "xxx";
                                              clientAccount.Delete = false;
                                              clientAccount.LastModified = DateTime.Now;

                                              Clientaccounts.Add(clientAccount);
                                          }
                                          if (holdings.Count(ss => ss.AccountId == Account_ID && ss.InvestmentCode == investmentCode) == 0)
                                          {

                                              holdings.Add(new EPIDataResponseInvestmentHolding { Exchange = "CASH", AccountId = Account_ID, Deleted = true, InvestmentCode = investmentCode.ReplaceEmptyWithNull(), IsSubjectToCGT = false, IsCashAccount = true, }); //, HoldingId = fundacc.Code.ReplaceEmptyWithNull()


                                              if (bankAccountEntity.CashManagementTransactions != null)
                                              {

                                                  var ttordered = bankAccountEntity.CashManagementTransactions.OrderByDescending(ss => ss.TransactionDate).FirstOrDefault();
                                                  DateTime? date = null;
                                                  //if (ttordered != null)
                                                  //{
                                                  //    decimal holding = bankAccountEntity.CashManagementTransactions.Sum(t => t.TotalAmount);
                                                  //    date = ttordered.TransactionDate;
                                                  //    var balac = new EPIDataResponseHoldingBalance { LastModified = DateTime.Now, AccountId = Account_ID, InvestmentCode = investmentCode.ReplaceEmptyWithNull(), Exchange = "CASH" }; //, HoldingId = fundacc.Code.ReplaceEmptyWithNull()
                                                  //    balac.UnitBalance = new InvestmentHoldingBalanceUnitBalance { AsAtDate = DateTime.Now, Settled = holding, Pending = 0 };
                                                  //    holdingBalances.Add(balac);
                                                  //}
                                                  foreach (var tran in bankAccountEntity.CashManagementTransactions)
                                                  {
                                                      try
                                                      {
                                                          trans.Add(EPIUtilDataUtil.GenerateBankTransaction(tran, investmentCode, Account_ID, false)); //HoldingId = fundacc.Code.ReplaceEmptyWithNull(),
                                                      }
                                                      catch (Exception hh)
                                                      {
                                                          throw hh;
                                                      }
                                                  }

                                                  foreach (var tran in bankAccountEntity.CashManagementTransactions)
                                                  {
                                                      if (tran.SystemTransactionType == "Interest" && tran.Category == "Income")
                                                      {
                                                          var incomeEntitlement = new EPIDataResponseIncomeEntitlement
                                                          {
                                                              EPIIncomeType = InvestmentHoldingIncomeEntitlementEPIIncomeType.I00003,
                                                              Id = tran.ID.ToString(),
                                                              LastModified = DateTime.Now,
                                                              AccountId = Account_ID,
                                                              InvestmentCode = investmentCode,
                                                              Exchange = "CASH",
                                                              Units = 0,
                                                              AmountPerUnit = new MonetaryAmount
                                                              {
                                                                  Currency = CurrencyCode.AUD,
                                                                  Value = Decimal.Round(0, 6)
                                                              },
                                                              GrossAmount = new MonetaryAmount
                                                              {
                                                                  Currency = CurrencyCode.AUD,
                                                                  Value = Decimal.Round(tran.TotalAmount, 6)
                                                              },
                                                              NetAmount = new MonetaryAmount
                                                              {
                                                                  Currency = CurrencyCode.AUD,
                                                                  Value = Decimal.Round(tran.TotalAmount, 6)
                                                              },
                                                              Status = InvestmentHoldingIncomeEntitlementStatus.Final,
                                                              PaymentDate = tran.TransactionDate.Date,
                                                              TaxDate = tran.TransactionDate.Date,
                                                              AccrualDate = tran.TransactionDate.Date,
                                                          };

                                                          incomeEntitlements.Add(incomeEntitlement);
                                                      }
                                                  }
                                              }

                                              if (bankAccountEntity.DeletedCashManagementTransactions != null)
                                              {
                                                  foreach (var tran in bankAccountEntity.DeletedCashManagementTransactions)
                                                  {
                                                      try
                                                      {
                                                          trans.Add(EPIUtilDataUtil.GenerateBankTransaction(tran, investmentCode, Account_ID, true)); //HoldingId = fundacc.Code.ReplaceEmptyWithNull(),
                                                      }
                                                      catch (Exception eh)
                                                      {
                                                          throw eh;
                                                      }
                                                  }
                                              }
                                          }
                                      }
                                  }
                                  Organization.Broker.ReleaseBrokerManagedComponent(comp);
                              }
                          }
                      }
                  }
              }
          }
          catch (Exception ex)
          {

              throw ex;
          }

          #endregion JMP CASH DATA
      }

      //private void AddECLIPSESUPERJPMHoldings(List<EPIDataResponseAccount> Clientaccounts, string ClientName, AccountType typeofacttount, string clientID, string advisorId, List<EPIDataResponseInvestmentHolding> holdings, List<EPIDataResponseInvestmentHoldingMovementTransaction> trans, List<EPIDataResponseHoldingBalance> holdingBalances, List<EPIDataResponseIncomeEntitlement> incomeEntitlements, List<AccountProcessTaskEntity> listAccountProcess, List<ModelEntity> models, List<DistributionIncomeEntity> distributionIncomes, ObservableCollection<DividendEntity> dividendIncomes)
      //{
      //    #region JMP CASH DATA

      //    try
      //    {
      //        var taskEntities = listAccountProcess.Where(ss => ss.LinkedEntityType == OrganizationType.BankAccount && ss.LinkedEntity.Csid != Guid.Empty && ss.LinkedEntity.Clid != Guid.Empty).GroupBy(p => p.LinkedEntity.Csid).Select(grp => grp.First());

      //        foreach (var accountProcessTaskEntity in taskEntities)
      //        {
      //            var md = models.Where(ss => ss.ID == accountProcessTaskEntity.ModelID).FirstOrDefault();

      //            if (md != null)
      //            {
      //                var asst = md.Assets.Where(ss => ss.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
      //                if (asst != null)
      //                {
      //                    var prd = asst.Products.Where(ss => ss.ID == accountProcessTaskEntity.TaskID).FirstOrDefault();
      //                    if (prd != null)
      //                    {
      //                        var comp = Organization.Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid);
      //                        if (comp != null)
      //                        {
      //                            var bankAccountEntity = (comp as OrganizationUnitCM).ClientEntity as BankAccountEntity;
      //                            if (bankAccountEntity != null && EPIUtilDataUtil.IsExportableBankAccount(Organization, bankAccountEntity))
      //                            {
      //                                if (bankAccountEntity.CashManagementTransactions != null)
      //                                {
      //                                    string investmentCode = "JPMCASH";

      //                                    string EPICode = "INOV000001"; //GetEpiCode(InvestmentCode);
      //                                    string Account_ID = clientID; //+ "_" + InvestmentCode + "_" + EPICode;

      //                                    if (Clientaccounts.Count(ss => ss.Id == Account_ID) == 0)
      //                                    {

      //                                        var clientAccount = new EPIDataResponseAccount();
      //                                        if (!string.IsNullOrEmpty(advisorId))
      //                                            clientAccount.Advisers = new AccountAdviser[]
      //                                                          {
      //                                                              new AccountAdviser {Id = advisorId, PrimaryAdviser = true}
      //                                                          };
      //                                        clientAccount.Id = Account_ID;

      //                                        EPIUtilDataUtil.SetClientAccount(typeofacttount, ClientName, "", AccountStatusStatus.Open, CurrencyCode.AUD, clientAccount);

      //                                        clientAccount.EPIProductCode = EPICode.ReplaceEmptyWithNull();
      //                                        clientAccount.Owners = new AccountOwner[] { new AccountOwner { Id = clientID, PercentageOfOwnership = 100 } };

      //                                        // clientAccount.FPSId = "xxx";
      //                                        clientAccount.Delete = false;
      //                                        clientAccount.LastModified = DateTime.Now;

      //                                        Clientaccounts.Add(clientAccount);
      //                                    }
      //                                    if (holdings.Count(ss => ss.AccountId == Account_ID && ss.InvestmentCode == investmentCode) == 0)
      //                                    {

      //                                        holdings.Add(new EPIDataResponseInvestmentHolding { Exchange = "CASH", AccountId = Account_ID, Deleted = true, InvestmentCode = investmentCode.ReplaceEmptyWithNull(), IsSubjectToCGT = false, IsCashAccount = true, }); //, HoldingId = fundacc.Code.ReplaceEmptyWithNull()


      //                                        if (bankAccountEntity.CashManagementTransactions != null)
      //                                        {

      //                                            var ttordered = bankAccountEntity.CashManagementTransactions.OrderByDescending(ss => ss.TransactionDate).FirstOrDefault();
      //                                            DateTime? date = null;
      //                                            //if (ttordered != null)
      //                                            //{
      //                                            //   // decimal holding = bankAccountEntity.CashManagementTransactions.Sum(t => t.TotalAmount);
      //                                            //    date = ttordered.TransactionDate;
      //                                            //    var balac = new EPIDataResponseHoldingBalance { LastModified = DateTime.Now, AccountId = Account_ID, InvestmentCode = investmentCode.ReplaceEmptyWithNull(), Exchange = "CASH" }; //, HoldingId = fundacc.Code.ReplaceEmptyWithNull()
      //                                            //    balac.UnitBalance = new InvestmentHoldingBalanceUnitBalance { AsAtDate = DateTime.Now, Settled = 0, Pending = 0 };
      //                                            //    holdingBalances.Add(balac);
      //                                            //}
      //                                            //foreach (var tran in bankAccountEntity.CashManagementTransactions)
      //                                            //{
      //                                            //    try
      //                                            //    {
      //                                            //        trans.Add(EPIUtilDataUtil.GenerateBankTransaction(tran, investmentCode, Account_ID, false)); //HoldingId = fundacc.Code.ReplaceEmptyWithNull(),
      //                                            //    }
      //                                            //    catch (Exception hh)
      //                                            //    {
      //                                            //        throw hh;
      //                                            //    }
      //                                            //}

      //                                            //foreach (var tran in bankAccountEntity.CashManagementTransactions)
      //                                            //{
      //                                            //    if (tran.SystemTransactionType == "Interest" && tran.Category == "Income")
      //                                            //    {
      //                                            //        var incomeEntitlement = new EPIDataResponseIncomeEntitlement
      //                                            //        {
      //                                            //            EPIIncomeType = InvestmentHoldingIncomeEntitlementEPIIncomeType.I00003,
      //                                            //            Id = tran.ID.ToString(),
      //                                            //            LastModified = DateTime.Now,
      //                                            //            AccountId = Account_ID,
      //                                            //            InvestmentCode = investmentCode,
      //                                            //            Exchange = "CASH",
      //                                            //            Units = 0,
      //                                            //            AmountPerUnit = new MonetaryAmount
      //                                            //            {
      //                                            //                Currency = CurrencyCode.AUD,
      //                                            //                Value = Decimal.Round(0, 6)
      //                                            //            },
      //                                            //            GrossAmount = new MonetaryAmount
      //                                            //            {
      //                                            //                Currency = CurrencyCode.AUD,
      //                                            //                Value = Decimal.Round(tran.TotalAmount, 6)
      //                                            //            },
      //                                            //            NetAmount = new MonetaryAmount
      //                                            //            {
      //                                            //                Currency = CurrencyCode.AUD,
      //                                            //                Value = Decimal.Round(tran.TotalAmount, 6)
      //                                            //            },
      //                                            //            Status = InvestmentHoldingIncomeEntitlementStatus.Final,
      //                                            //            PaymentDate = tran.TransactionDate.Date,
      //                                            //            TaxDate = tran.TransactionDate.Date,
      //                                            //            AccrualDate = tran.TransactionDate.Date,
      //                                            //        };

      //                                            //        incomeEntitlements.Add(incomeEntitlement);
      //                                            //    }
      //                                            //}
      //                                        }

      //                                        if (bankAccountEntity.DeletedCashManagementTransactions != null)
      //                                        {
      //                                            foreach (var tran in bankAccountEntity.DeletedCashManagementTransactions)
      //                                            {
      //                                                try
      //                                                {
      //                                                    trans.Add(EPIUtilDataUtil.GenerateBankTransaction(tran, investmentCode, Account_ID, true)); //HoldingId = fundacc.Code.ReplaceEmptyWithNull(),
      //                                                }
      //                                                catch (Exception eh)
      //                                                {
      //                                                    throw eh;
      //                                                }
      //                                            }
      //                                            foreach (var tran in bankAccountEntity.CashManagementTransactions)
      //                                            {
      //                                                try
      //                                                {
      //                                                    trans.Add(EPIUtilDataUtil.GenerateBankTransaction(tran, investmentCode, Account_ID, true)); //HoldingId = fundacc.Code.ReplaceEmptyWithNull(),
      //                                                }
      //                                                catch (Exception eh)
      //                                                {
      //                                                    throw eh;
      //                                                }
      //                                            }
      //                                        }
      //                                    }
      //                                }
      //                            }
      //                            Organization.Broker.ReleaseBrokerManagedComponent(comp);
      //                        }
      //                    }
      //                }
      //            }
      //        }
      //    }
      //    catch (Exception ex)
      //    {

      //        throw ex;
      //    }

      //    #endregion JMP CASH DATA
      //}
  
      private void AddClientHoldings(ManagedInvestmentSchemesAccountEntity misentity, List<EPIDataResponseAccount> Clientaccounts, string ClientName, AccountType typeofacttount, string clientID, string advisorId, List<EPIDataResponseInvestmentHolding> holdings, List<EPIDataResponseInvestmentHoldingMovementTransaction> trans, List<EPIDataResponseHoldingBalance> holdingBalances, List<EPIDataResponseIncomeEntitlement> incomeEntitlements, List<AccountProcessTaskEntity> ListAccountProcess, List<ModelEntity> models, List<DistributionIncomeEntity> DistributionIncomes)
        {
            var taskEntities = ListAccountProcess.Where(
                   ss =>
                   ss.LinkedEntityType == OrganizationType.ManagedInvestmentSchemesAccount &&
                   ss.LinkedEntity.Csid != Guid.Empty && ss.LinkedEntity.Clid != Guid.Empty);


            foreach (var accountProcessTaskEntity in taskEntities)
            {
                var md = models.Where(ss => ss.ID == accountProcessTaskEntity.ModelID).FirstOrDefault();

                if (md != null)
                {

                    var asst = md.Assets.Where(ss => ss.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                    if (asst != null)
                    {
                        var prd = asst.Products.Where(ss => ss.ID == accountProcessTaskEntity.TaskID).FirstOrDefault();
                        if (prd != null)
                        {
                            var fundid = prd.FundAccounts.FirstOrDefault();
                            if (fundid != null)
                            {
                                if (misentity != null)
                                {
                                    var fundacc = misentity.FundAccounts.Where(ss => ss.ID == fundid).FirstOrDefault();
                                    if (fundacc != null)
                                    {
                                        if (fundacc.FundTransactions != null)
                                        {
                                            var tt = fundacc.FundTransactions.Where(ss => ss.ClientID == clientID);
                                            var ut = fundacc.UnsettledOrders.Where(ss => ss.ClientID == clientID);

                                            if (tt.Count() > 0 || ut.Count() > 0)
                                            {

                                                string EPICode = InnovaProducts.GetEpiCode(fundacc.Code);
                                                string Account_ID = clientID + "_" + fundacc.Code + "_" + EPICode;
                                                if (Clientaccounts.Count(ss => ss.Id == Account_ID) == 0)
                                                {

                                                    var clientAccount = new EPIDataResponseAccount();
                                                    if (!string.IsNullOrEmpty(advisorId))
                                                        clientAccount.Advisers = new AccountAdviser[]
                                                  {
                                                      new AccountAdviser {Id = advisorId, PrimaryAdviser = true}
                                                  };
                                                    clientAccount.Id = Account_ID;

                                                    EPIUtilDataUtil.SetClientAccount(typeofacttount, ClientName, "", AccountStatusStatus.Open, CurrencyCode.AUD, clientAccount);

                                                    clientAccount.EPIProductCode = EPICode.ReplaceEmptyWithNull();
                                                    clientAccount.Owners = new AccountOwner[] { new AccountOwner { Id = clientID, PercentageOfOwnership = 100 } };
                                                    clientAccount.Delete = false;
                                                    clientAccount.LastModified = DateTime.Now;
                                                    Clientaccounts.Add(clientAccount);
                                                }


                                                if (holdings.Count(ss => ss.AccountId == Account_ID && ss.InvestmentCode == fundacc.Code) == 0)
                                                {

                                                    holdings.Add(new EPIDataResponseInvestmentHolding { Exchange = "FND", AccountId = Account_ID, Deleted = false, InvestmentCode = fundacc.Code.ReplaceEmptyWithNull(), IsSubjectToCGT = true, IsCashAccount = false, });//, HoldingId = fundacc.Code.ReplaceEmptyWithNull()

                                                    var ttordered = tt.OrderByDescending(ss => ss.TradeDate).FirstOrDefault();

                                                    decimal settled = tt.Sum(ss => ss.Shares);
                                                    decimal unsettled = ut.Sum(ss => ss.UnsettledUnits);

                                                    DateTime? date = null;
                                                    if (ttordered != null)
                                                    {
                                                        date = ttordered.TradeDate;
                                                        var balac = new EPIDataResponseHoldingBalance { LastModified = DateTime.Now, AccountId = Account_ID, InvestmentCode = fundacc.Code.ReplaceEmptyWithNull(), Exchange = "FND" };//, HoldingId = fundacc.Code.ReplaceEmptyWithNull()
                                                        balac.UnitBalance = new InvestmentHoldingBalanceUnitBalance { AsAtDate = date.Value, Settled = settled, Pending = unsettled };
                                                        holdingBalances.Add(balac);
                                                    }

                                                    foreach (var tran in tt)
                                                    {
                                                        trans.Add(EPIUtilDataUtil.GenerateMISTransation(tran, Account_ID, fundacc.Code, false));
                                                    }

                                                    if (fundacc.DeletedFundTransactions != null)
                                                    {
                                                        var deletett = fundacc.DeletedFundTransactions.Where(ss => ss.ClientID == clientID);
                                                        foreach (var tran in deletett)
                                                        {
                                                            trans.Add(EPIUtilDataUtil.GenerateMISTransation(tran, Account_ID, fundacc.Code, true));
                                                        }
                                                    }

                                                }
                                                if (DistributionIncomes.Count(ss => ss.FundCode == fundacc.Code) > 0)
                                                {
                                                    var distributions = DistributionIncomes.Where(ss => ss.FundCode == fundacc.Code);
                                                    foreach (var distributionIncomeEntity in distributions)
                                                    {
                                                        var incomeEntitlement = new EPIDataResponseIncomeEntitlement
                                                                                    {
                                                                                        EPIIncomeType = InvestmentHoldingIncomeEntitlementEPIIncomeType.I00002,
                                                                                        Id = distributionIncomeEntity.ID.ToString(),
                                                                                        LastModified = DateTime.Now,
                                                                                        AccountId = Account_ID,
                                                                                        //  HoldingId =fundacc.Code.ReplaceEmptyWithNull(),
                                                                                        InvestmentCode = fundacc.Code.ReplaceEmptyWithNull(),
                                                                                        Exchange = "FND",
                                                                                        Units = (decimal)distributionIncomeEntity.RecodDate_Shares,
                                                                                        AmountPerUnit = new MonetaryAmount { Currency = CurrencyCode.AUD, Value = distributionIncomeEntity.RecodDate_Shares != 0.0 ? decimal.Round((decimal)(distributionIncomeEntity.Components.Sum(ss => ss.Unit_Amount) / distributionIncomeEntity.RecodDate_Shares), 6) : 0 },
                                                                                        Status = InvestmentHoldingIncomeEntitlementStatus.Final,
                                                                                    };

                                                        if (distributionIncomeEntity.RecordDate.HasValue)
                                                            incomeEntitlement.AccrualDate = distributionIncomeEntity.RecordDate.Value;
                                                        if (distributionIncomeEntity.PaymentDate.HasValue)
                                                        {
                                                            incomeEntitlement.PaymentDate = distributionIncomeEntity.PaymentDate.Value;
                                                            incomeEntitlement.TaxDate = distributionIncomeEntity.PaymentDate.Value;
                                                        }

                                                        if (distributionIncomeEntity.Components != null)
                                                        {
                                                            incomeEntitlement.GrossAmount = new MonetaryAmount { Currency = CurrencyCode.AUD, Value = decimal.Round((decimal)distributionIncomeEntity.Components.Sum(ss => ss.GrossDistribution), 6) };
                                                            incomeEntitlement.NetAmount = new MonetaryAmount { Currency = CurrencyCode.AUD, Value = decimal.Round((decimal)distributionIncomeEntity.Components.Sum(ss => ss.Unit_Amount), 6) };
                                                        }

                                                        incomeEntitlements.Add(incomeEntitlement);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

      private void AddClientHoldingsASX(List<EPIDataResponseAccount> Clientaccounts, string ClientName, AccountType typeofacttount, string clientID, string advisorId, List<EPIDataResponseInvestmentHolding> holdings, List<EPIDataResponseInvestmentHoldingMovementTransaction> trans, List<EPIDataResponseHoldingBalance> holdingBalances, List<EPIDataResponseIncomeEntitlement> incomeEntitlements, List<AccountProcessTaskEntity> listAccountProcess, List<ModelEntity> models, List<DistributionIncomeEntity> distributionIncomes, ObservableCollection<DividendEntity> dividendIncomes)
      {
          #region ASX DATA

          try
          {
              var taskEntities = listAccountProcess.Where(ss => ss.LinkedEntityType == OrganizationType.DesktopBrokerAccount && ss.LinkedEntity.Csid != Guid.Empty && ss.LinkedEntity.Clid != Guid.Empty).GroupBy(p => p.LinkedEntity.Csid).Select(grp => grp.First());

              foreach (var accountProcessTaskEntity in taskEntities)
              {
                  var md = models.Where(ss => ss.ID == accountProcessTaskEntity.ModelID).FirstOrDefault();
                  if (md != null && md.ServiceType == ServiceTypes.DoItForMe)
                  {
                      var asst = md.Assets.Where(ss => ss.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                      if (asst != null)
                      {
                          var prd = asst.Products.Where(ss => ss.ID == accountProcessTaskEntity.TaskID).FirstOrDefault();
                          if (prd != null)
                          {
                              var comp = this.Organization.Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid);
                              if (comp != null)
                              {

                                  DesktopBrokerAccountEntity brokerAccountEntity = (comp as OrganizationUnitCM).ClientEntity as DesktopBrokerAccountEntity;

                                  if (brokerAccountEntity != null)
                                  {
                                      if (brokerAccountEntity.holdingTransactions != null)
                                      {
                                          var investmentCodes = (from p in brokerAccountEntity.holdingTransactions select p.InvestmentCode).Distinct();

                                          foreach (string InvestmentCode in investmentCodes)
                                          {
                                              string EPICode = "INOV000001"; //GetEpiCode(InvestmentCode);
                                              string Account_ID = brokerAccountEntity.AccountNumber; //+ "_" + InvestmentCode + "_" + EPICode;

                                              if (Clientaccounts.Count(ss => ss.Id == Account_ID) == 0)
                                              {

                                                  var clientAccount = new EPIDataResponseAccount();
                                                  if (!string.IsNullOrEmpty(advisorId))
                                                      clientAccount.Advisers = new AccountAdviser[]
                                                            {
                                                                new AccountAdviser {Id = advisorId, PrimaryAdviser = true}
                                                            };
                                                  
                                                  clientAccount.Id = Account_ID;
                                                  EPIUtilDataUtil.SetClientAccount(typeofacttount, ClientName + " (Desktop Broker)", "", AccountStatusStatus.Open, CurrencyCode.AUD, clientAccount);

                                                  clientAccount.EPIProductCode = EPICode.ReplaceEmptyWithNull();
                                                  clientAccount.Owners = new AccountOwner[] { new AccountOwner { Id = clientID, PercentageOfOwnership = 100 } };
                                                   clientAccount.Delete = false;
                                                  clientAccount.LastModified = DateTime.Now;

                                                  Clientaccounts.Add(clientAccount);
                                              }
                                              if (holdings.Count(ss => ss.AccountId == Account_ID && ss.InvestmentCode == InvestmentCode) == 0)
                                              {

                                                  holdings.Add(new EPIDataResponseInvestmentHolding { Exchange = "ASX", AccountId = Account_ID, Deleted = false, InvestmentCode = InvestmentCode.ReplaceEmptyWithNull(), IsSubjectToCGT = true, IsCashAccount = false, }); //, HoldingId = fundacc.Code.ReplaceEmptyWithNull()


                                                  if (brokerAccountEntity.holdingTransactions != null)
                                                  {
                                                      var tt = brokerAccountEntity.holdingTransactions.Where(ss => ss.InvestmentCode == InvestmentCode);
                                                      var ttordered = tt.OrderByDescending(ss => ss.TradeDate).FirstOrDefault();
                                                      var ut = brokerAccountEntity.UnSettledOrders.Where(ss => ss.InvestmentCode == InvestmentCode);
                                                      decimal settled = tt.Sum(ss => ss.Units);
                                                      decimal unsettled = ut.Sum(ss => ss.UnsettledOrders);

                                                      DateTime? date = null;
                                                      if (ttordered != null)
                                                      {
                                                          date = ttordered.TradeDate;
                                                          var balac = new EPIDataResponseHoldingBalance { LastModified = DateTime.Now, AccountId = Account_ID, InvestmentCode = InvestmentCode.ReplaceEmptyWithNull(), Exchange = "ASX" }; //, HoldingId = fundacc.Code.ReplaceEmptyWithNull()
                                                          balac.UnitBalance = new InvestmentHoldingBalanceUnitBalance { AsAtDate = date.Value, Settled = settled, Pending = 0 };
                                                          holdingBalances.Add(balac);
                                                      }

                                                      foreach (var tran in tt)
                                                      {
                                                          try
                                                          {

                                                              trans.Add(EPIUtilDataUtil.GenerateASXTransaction(tran, Account_ID, InvestmentCode, false, Organization)); //HoldingId = fundacc.Code.ReplaceEmptyWithNull(),
                                                          }
                                                          catch (Exception hh)
                                                          {
                                                              throw hh;
                                                          }
                                                      }

                                                  }
                                                  if (brokerAccountEntity.DeletedHoldingTransactions != null)
                                                  {

                                                      foreach (var tran in brokerAccountEntity.DeletedHoldingTransactions.Where(ss => ss.InvestmentCode == InvestmentCode))
                                                      {
                                                          try
                                                          {

                                                              trans.Add(EPIUtilDataUtil.GenerateASXTransaction(tran, Account_ID, InvestmentCode, true, Organization)); //HoldingId = fundacc.Code.ReplaceEmptyWithNull(),
                                                          }
                                                          catch (Exception eh)
                                                          {
                                                              throw eh;
                                                          }
                                                      }
                                                  }

                                              }
                                              if (dividendIncomes.Count(ss => ss.InvestmentCode == InvestmentCode) > 0)
                                              {
                                                  var distributions = dividendIncomes.Where(ss => ss.InvestmentCode == InvestmentCode);
                                                  foreach (var dividendEntity in distributions)
                                                  {
                                                      var incomeEntitlement = new EPIDataResponseIncomeEntitlement
                                                      {
                                                          EPIIncomeType = InvestmentHoldingIncomeEntitlementEPIIncomeType.I00002,
                                                          Id = dividendEntity.ID.ToString(),
                                                          LastModified = DateTime.Now,
                                                          AccountId = Account_ID,
                                                          //  HoldingId =fundacc.Code.ReplaceEmptyWithNull(),
                                                          InvestmentCode = InvestmentCode.ReplaceEmptyWithNull(),
                                                          Exchange = "ASX",
                                                          Units = (decimal)dividendEntity.UnitsOnHand,
                                                          AmountPerUnit = new MonetaryAmount { Currency = CurrencyCode.AUD, Value = dividendEntity.UnitsOnHand != 0.0 ? decimal.Round((decimal)(dividendEntity.CentsPerShare), 6) : 0 },
                                                          Status = InvestmentHoldingIncomeEntitlementStatus.Final,
                                                      };

                                                      if (dividendEntity.RecordDate.HasValue)
                                                          incomeEntitlement.AccrualDate = dividendEntity.RecordDate.Value;
                                                      if (dividendEntity.PaymentDate.HasValue)
                                                      {
                                                          incomeEntitlement.PaymentDate = dividendEntity.PaymentDate.Value;
                                                          incomeEntitlement.TaxDate = dividendEntity.PaymentDate.Value;
                                                      }


                                                      incomeEntitlement.GrossAmount = new MonetaryAmount { Currency = CurrencyCode.AUD, Value = decimal.Round((decimal)dividendEntity.PaidDividend, 6) };
                                                      incomeEntitlement.NetAmount = new MonetaryAmount { Currency = CurrencyCode.AUD, Value = decimal.Round((decimal)dividendEntity.PaidDividend, 6) };


                                                      incomeEntitlements.Add(incomeEntitlement);
                                                  }

                                              }


                                          }
                                      }
                                  }
                                  Organization.Broker.ReleaseBrokerManagedComponent(comp);
                              }

                          }

                      }
                  }


              }
          }
          catch (Exception ex)
          {

              throw ex;
          }

          #endregion ASX Data
      }
    
    }

    public class EPIDataRepsonseAllClientByTypeCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string type)
        {

            var response = EPIUtilDataUtil.GetBasicReponse();
            return response.ToXmlString();
        }
    }

    public class EPIDataResponseClientByNameCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string name)
        {
            var response = EPIUtilDataUtil.GetBasicReponse();
            return response.ToXmlString();
        }
    }
}
