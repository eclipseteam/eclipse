﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.CM.Organization
{
    public class InnovaProductsAssestAllocation
    {
        public decimal DomesticCash { get; set; }
        public decimal ForeignCash { get; set; }
        public decimal DomesticShares { get; set; }
        public decimal ForeignShares { get; set; }
        public decimal DomesticFixedInterest { get; set; }
        public decimal ForeignFixedInterest { get; set; }
        public decimal DomesticProperty { get; set; }
        public decimal ForeignProperty { get; set; }
        public decimal DomesticDirectProperty { get; set; }
        public decimal ForeignDirectProperty { get; set; }
        public decimal Other { get; set; }
        public DateTime AsAtDate = new DateTime(2012, 07, 1);
        public DateTime LastModified = new DateTime(2012, 07, 1);
    }
}
