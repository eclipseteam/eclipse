﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Xml;
using Oritax.TaxSimp.CM.Organization.UMAForm;

namespace Oritax.TaxSimp.DataSets
{
    public enum UMAFormDSOperation
    {
        Delete,
        Add,
        Update,
        CreateClient,
        AssociateAllClients,
        ExportAdviserFees,
        UpdateKeyClientRecords
    }

    public partial class UMAFormDS : DataSet
    {
        public UMAFormEntity UMAFormEntity;
        public UMAFormDSOperation UMAFormDSOperation;
        public String AppNo = string.Empty;

        public const string ADVISERFEELISTING = "ADVISERFEELISTING";

        public const string APPNOP = "APPNOP";
        public const string CLIENTID = "CLIENTID";
        public const string ACCOUNTNAME = "ACCOUNTNAME";
        public const string ADVISERNAME = "ADVISERNAME";
        public const string STATUS = "STATUS";

        public const string UPFRONTFEE = "UPFRONTFEE";

        public const string ONGOINGFEEVALUE = "ONGOINGFEEVALUE";
        public const string ONGOINGFEEPERCENT = "ONGOINGFEEPERCENT";

        public const string FROMTIERFEE1 = "FROMTIERFEE1";
        public const string TOTIERFEE1 = "TOTIERFEE1";
        public const string FEEPERCENT1 = "FEEPERCENT1";

        public const string FROMTIERFEE2 = "FROMTIERFEE2";
        public const string TOTIERFEE2 = "TOTIERFEE2";
        public const string FEEPERCENT2 = "FEEPERCENT2";

        public const string FROMTIERFEE3 = "FROMTIERFEE3";
        public const string TOTIERFEE3 = "TOTIERFEE3";
        public const string FEEPERCENT3 = "FEEPERCENT3";

        public const string FROMTIERFEE4 = "FROMTIERFEE4";
        public const string TOTIERFEE4 = "TOTIERFEE4";
        public const string FEEPERCENT4 = "FEEPERCENT4";

        public const string FROMTIERFEE5 = "FROMTIERFEE5";
        public const string TOTIERFEE5 = "TOTIERFEE5";
        public const string FEEPERCENT5 = "FEEPERCENT5";



        public UMAFormDS()
        {
            DataTable dt = new DataTable(ADVISERFEELISTING);

            dt.Columns.Add(APPNOP, typeof(string));
            dt.Columns.Add(CLIENTID, typeof(string));
            dt.Columns.Add(ACCOUNTNAME, typeof(string));
            dt.Columns.Add(ADVISERNAME, typeof(string));
            dt.Columns.Add(STATUS, typeof(string));
            dt.Columns.Add(UPFRONTFEE, typeof(string));
            dt.Columns.Add(ONGOINGFEEVALUE, typeof(string));
            dt.Columns.Add(ONGOINGFEEPERCENT, typeof(string));

            dt.Columns.Add(FROMTIERFEE1, typeof(string));
            dt.Columns.Add(TOTIERFEE1, typeof(string));
            dt.Columns.Add(FEEPERCENT1, typeof(string));

            dt.Columns.Add(FROMTIERFEE2, typeof(string));
            dt.Columns.Add(TOTIERFEE2, typeof(string));
            dt.Columns.Add(FEEPERCENT2, typeof(string));

            dt.Columns.Add(FROMTIERFEE3, typeof(string));
            dt.Columns.Add(TOTIERFEE3, typeof(string));
            dt.Columns.Add(FEEPERCENT3, typeof(string));

            dt.Columns.Add(FROMTIERFEE4, typeof(string));
            dt.Columns.Add(TOTIERFEE4, typeof(string));
            dt.Columns.Add(FEEPERCENT4, typeof(string));

            dt.Columns.Add(FROMTIERFEE5, typeof(string));
            dt.Columns.Add(TOTIERFEE5, typeof(string));
            dt.Columns.Add(FEEPERCENT5, typeof(string));

            this.Tables.Add(dt); 
        }
    }
}
