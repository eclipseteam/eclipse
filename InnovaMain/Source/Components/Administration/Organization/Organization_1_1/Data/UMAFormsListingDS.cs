﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Oritax.TaxSimp.CM.Organization.Data
{
    public class UMAFormsListingDS : DataSet
    {
        public const string ACCOUNTSLISTTABLE = "AccountsListingTable";
        public const string APPLICATIONNO = "ApplicationNo";
        public const string ACCOUNTNAME = "AccountName";
        public const string ACCOUNTTYPE = "AccountType";
        public const string CLIENTID = "ClientID";
        public const string CLIENTCID = "ClientCID";
        public const string SALESDIY = "SALESDIY";
        public const string SALESDIFM = "SALESDIFM";
        public const string SALESDIWM = "SALESDIWM";
        public const string CREATEDBY = "CreatedBy";
        public const string CREATEDDATE = "CreatedDate";

        public const string MODIFIEDBY = "ModifiedBy";
        public const string MODIFIEDDATE = "ModifiedDate";

        public const string FORMSTATUS = "Status";
        public UserTable userTable = new UserTable();
        public UMAFormsListingDS()
        {
            this.UMAFormsListingTable();
            Tables.Add(userTable);
        }

        public void UMAFormsListingTable()
        {
            DataTable dt = new DataTable(ACCOUNTSLISTTABLE);
            dt.Columns.Add(APPLICATIONNO, typeof(string));
            dt.Columns.Add(ACCOUNTNAME, typeof(string));
            dt.Columns.Add(CLIENTID, typeof(string));
            dt.Columns.Add(CLIENTCID, typeof(Guid));
            dt.Columns.Add(ACCOUNTTYPE, typeof(string));
            dt.Columns.Add(SALESDIY, typeof(decimal));
            dt.Columns.Add(SALESDIFM, typeof(decimal));
            dt.Columns.Add(SALESDIWM, typeof(decimal));
            dt.Columns.Add(CREATEDBY, typeof(string));
            dt.Columns.Add(CREATEDDATE, typeof(string));
            dt.Columns.Add(MODIFIEDBY, typeof(string));
            dt.Columns.Add(MODIFIEDDATE, typeof(string));
            dt.Columns.Add(FORMSTATUS, typeof(string));

            this.Tables.Add(dt);
        }
        
    }
    public class UserTable : DataTable
    {
        private readonly string USERSTABLE = "UsersTable";
        public readonly string UMAFORMENTITYID = "UMAFormEntityID";
        public readonly string USERCID = "UserCid";
        internal UserTable()
        {

            this.TableName = USERSTABLE;
            Columns.Add(UMAFORMENTITYID, typeof(string));
            Columns.Add(USERCID, typeof(Guid));

        }

        public void AddRow(string AppNo, Guid usercid)
        {
            var dr = this.NewRow();
            dr[UMAFORMENTITYID] = AppNo;
            dr[USERCID] = usercid;
            Rows.Add(dr);
        }
    }
}
