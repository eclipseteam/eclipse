﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.Data
{
    [Serializable]
    public class SmtpServerEntity
    {
        #region Public Properties

        public string HostName { get; set; }
        public int PortNumber { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool UseSsl { get; set; }

        #endregion Public Properties

        #region Constructors

        public SmtpServerEntity()
        {
            HostName = string.Empty;
            PortNumber = 25;
            UserName = string.Empty;
            Password = string.Empty;
            UseSsl = false;
        }

        #endregion Constructos
    }
}
