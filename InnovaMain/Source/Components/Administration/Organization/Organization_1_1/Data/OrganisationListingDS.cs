﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.CM.Organization.Data
{
    [Serializable]
    public enum OrganisationListingOperationType
    {
        AdvisersWithFUM,
        Advisers,
        DealerGroupWithFUM,
        DealerGroup,
        ClientsWithFUM,
        ClientsWithFUMDIFM,
        Clients,
        ClientsSearch,
        IFAWithFUM,
        IFA,
        DeleteMIS1004,
        SetBankTransactions,
        UsersWithPasswords,
        BankAccounts,
        IFAAndClients,
        AssetSummaryOrg,
        FeeRuns,
        FeeRunsShort,
        GroupWithFUM,
        ClientsWithAdvisers,
        AdvisersBasicList,
        ASXAccounts,
        ClientDistributions,
        DuplicatedAssociationClients,
        ClientsAndFeesAssociation,
        ClientsPhoneFix,
        ClientsSECFUM,
        EclipseSuper,
        EclipseSuperFUM,
        IndividualList,
        IndividualListOnlyNames,
        BankAccountsListWithoutFUM,
        ConsolidationEntities
    }
    [Serializable]
    public enum OrganisationFilterType
    {
        ClientByName,
        ClientByID,
        ClientByOtherID
    }
    [Serializable]
    public class OrganisationListingDS : DataSet
    {
        public const string TableName = "Entities_Table";

        public const string ADVISERANDCLIENTTABLE = "ADVISERANDCLIENTTABLE";
        public const string ECLIPSESUPERFUMTABLENAME = "ECLIPSESUPERFUMTABLE";
        public const string ECLIPSESUPERTABLENAME = "ECLIPSESUPERTABLE";
        public const string BANKACCOUNTLISTTABLENAME = "BANKACCOUNTLISTTABLE";
        public const string INDIVIDUALLISTWITHOUTDETAILSTABLENAME = "INDIVIDUALLISTWITHOUTDETAILSTABLE";

        public const string ADVISERCID = "ADVISERCID";
        public const string ADVISERFULLNAME = "ADVISERFULLNAME";
        public const string ADVISERLISTINGTABLE = "ADVISERLISTINGTABLE";

        public string DateInfo()
        {
            return "AS OF " + this.AsOfDate.ToString("dd/MM/yyyy");
        }

        public string ClientNameSearchFilter = String.Empty;
        public DateTime AsOfDate = DateTime.Now;
        public DateTime StartDate = DateTime.Now.AddYears(-1);
        public DateTime EndDate = DateTime.Now;

        public ServiceTypes ServiceTypes = ServiceTypes.None;

        public OrganisationListingOperationType OrganisationListingOperationType;

        public OrganisationFilterType OrganisationFilterType;

        public string GetRowFilter()
        {
            string filter = string.Empty;
            filter = "ENTITYCTID_FIELD ='b95e5a7b-8c82-4c05-934d-f6ab7d95ae41' OR " +
                                                "ENTITYCTID_FIELD ='96c93d8f-9d3d-4989-89cf-cff47d377b43' OR " +
                                                "ENTITYCTID_FIELD ='e9d16670-d8f9-4a2d-844b-4ebe67e4629f' OR " +
                                                "ENTITYCTID_FIELD ='2a2a95ac-7933-40a6-b6ba-1597cb18014f' OR " +
                                                "ENTITYCTID_FIELD ='815051a3-d82d-41b6-8536-147d8a2055d2' OR " +
                                                "ENTITYCTID_FIELD ='6660cead-ae7c-485e-af8e-622fa54f7af2' OR " +
                                                "ENTITYCTID_FIELD ='8256f2aa-3ccc-43b3-a683-90c8b603934a' OR " +
                                                "ENTITYCTID_FIELD ='aba693e9-be2f-4228-a300-92100d418f9d' OR " +
                                                "ENTITYCTID_FIELD ='7485432c-a44b-4e8b-ad73-9778e071de2d'";
            return filter;
        }

        public string GetRowFilterConsolidationEntities()
        {
            string filter = string.Empty;
            filter = "ENTITYCTID_FIELD ='D565CDB2-D71D-46f6-8ADB-080E981EBA7E'";
            return filter;
        }

        public string GetRowFilterAllClientsExceptEclipse()
        {
            string filter = string.Empty;
            filter = "ENTITYCTID_FIELD ='b95e5a7b-8c82-4c05-934d-f6ab7d95ae41' OR " +
                                                "ENTITYCTID_FIELD ='96c93d8f-9d3d-4989-89cf-cff47d377b43' OR " +
                                                "ENTITYCTID_FIELD ='2a2a95ac-7933-40a6-b6ba-1597cb18014f' OR " +
                                                "ENTITYCTID_FIELD ='815051a3-d82d-41b6-8536-147d8a2055d2' OR " +
                                                "ENTITYCTID_FIELD ='6660cead-ae7c-485e-af8e-622fa54f7af2' OR " +
                                                "ENTITYCTID_FIELD ='8256f2aa-3ccc-43b3-a683-90c8b603934a' OR " +
                                                "ENTITYCTID_FIELD ='aba693e9-be2f-4228-a300-92100d418f9d' OR " +
                                                "ENTITYCTID_FIELD ='7485432c-a44b-4e8b-ad73-9778e071de2d'";

            return filter;
        }

        public static string GetRowFilter(string accountName)
        {
            string filter = string.Empty;

            filter = "ENTITYCTID_FIELD ='b95e5a7b-8c82-4c05-934d-f6ab7d95ae41' OR " +
                                                "ENTITYCTID_FIELD ='96c93d8f-9d3d-4989-89cf-cff47d377b43' OR " +
                                                "ENTITYCTID_FIELD ='e9d16670-d8f9-4a2d-844b-4ebe67e4629f' OR " +
                                                "ENTITYCTID_FIELD ='2a2a95ac-7933-40a6-b6ba-1597cb18014f' OR " +
                                                "ENTITYCTID_FIELD ='815051a3-d82d-41b6-8536-147d8a2055d2' OR " +
                                                "ENTITYCTID_FIELD ='6660cead-ae7c-485e-af8e-622fa54f7af2' OR " +
                                                "ENTITYCTID_FIELD ='8256f2aa-3ccc-43b3-a683-90c8b603934a' OR " +
                                                "ENTITYCTID_FIELD ='aba693e9-be2f-4228-a300-92100d418f9d' OR " +
                                                "ENTITYCTID_FIELD ='7485432c-a44b-4e8b-ad73-9778e071de2d'";

            return filter;
        }

        public static string GetRowFilterEclipseSuper()
        {
            string filter = string.Empty;

            filter = "ENTITYCTID_FIELD ='E9D16670-D8F9-4A2D-844B-4EBE67E4629F'";

            return filter;
        }

        public static string GetRowFilterBankAccounts()
        {
            string filter = string.Empty;

            filter = "ENTITYCTID_FIELD ='D851B5FF-AE5F-4BF6-A4D6-AB48743D1752'";

            return filter;
        }

        public OrganisationListingDS()
        {

        }

     


        public string GetRowFilterUsers()
        {
            string filter = string.Empty;
            if (this.OrganisationListingOperationType == Data.OrganisationListingOperationType.UsersWithPasswords)
            {
                filter = "ENTITYCTID_FIELD ='3D98A779-ADF0-4BF5-940D-220EE57ACBD9'";
            }

            return filter;
        }

        public string GetRowFilterGroup()
        {
            string filter = string.Empty;
            filter = "ENTITYCTID_FIELD ='D565CDB2-D71D-46f6-8ADB-080E981EBA7E'";
            return filter;
        }

        public static string GetRowFilterMISAccounts()
        {
            string filter = string.Empty;
            filter = "ENTITYCTID_FIELD ='CD9D3D21-8199-40E7-A55E-0659A3384B34'";

            return filter;
        }

        public static string GetRowFilterIndividualList()
        {
            string filter = string.Empty;
            filter = "ENTITYCTID_FIELD ='9EC83666-D774-4BFE-895C-1E8D42325C50'";

            return filter;
        }

        public string GetRowFilterASXAccounts()
        {
            string filter = "ENTITYCTID_FIELD ='8AA0A349-B5EC-4A32-9E39-7538606D2F70'";
            return filter;
        }


        public string GetRowFilterFeeRuns()
        {
            string filter = "ENTITYCTID_FIELD ='AE8DCE8C-0343-461C-8152-7FA4F4E22427'";
            return filter;
        }

        public static string GetRowFilterAdvisers()
        {
            string filter = "ENTITYCTID_FIELD ='6F5F1CAA-E770-4022-918D-A435CB896DE6'";
            return filter;
        }

        public string GetRowFilterIFA()
        {
            string filter = string.Empty;
            filter = "ENTITYCTID_FIELD ='0bb7f593-84d0-430c-ab49-5f7e9979628e'";
            return filter;
        }
    }
}
