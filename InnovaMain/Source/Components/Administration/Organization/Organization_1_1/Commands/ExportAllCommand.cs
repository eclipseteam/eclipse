﻿using System.Xml.Linq;
using Oritax.TaxSimp.Extensions;
using System;

namespace Oritax.TaxSimp.CM.Organization.Commands
{
    public class ExportAllCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string value)
        {
            string[] outa = null;
            try
            {
                XElement root = new XElement("Eclipse");
                Organization.Export(root);
                XElement units = new XElement("OrganizationUnits");
                root.Add(units);
                OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
                {
                    if (bmc != null && bmc is ISupportMigration)
                    {
                        XElement unit = new XElement("OraganizationUnit");
                        unit.Add(
                            new XAttribute("Name", bmc.Name),
                            new XAttribute("Clid", logical.CLID),
                            new XAttribute("Csid", csid),
                            new XAttribute("AssemblyId", bmc.ComponentVersionInformation.ID));
                        unit.Add(bmc.ComponentInformation.ToXElement("ComponentInformation"));
                        unit.Add(bmc.ComponentVersionInformation.ToXElement("ComponentVersionInformation"));
                        (bmc as ISupportMigration).Export(unit);
                        units.Add(unit);
                    }
                });
                outa = new string[] { "Exported completed successfully", root.ToString() };
            }
            catch (Exception ex)
            {
                outa = new string[] { ex.Message };
            }

            return outa.ToXmlString();
        }
    }
}
