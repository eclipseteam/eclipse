﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.CM.Organization.Commands
{





    public class ImportMISPaidOrdersCommand : OrganizationCmCommandBase
    {


        public override string DoAction(string value)
        {
            string outa = string.Empty;
            try
            {
                string[] values = value.ToDataArray<string>();
                UserEntity user = values[0].ToData<UserEntity>();
                XElement root = XElement.Parse(values[1], LoadOptions.None);

                string filename = values[2];
                string misName = values[3];
                this.Organization.Broker.LogEvent(EventType.FileImported, "Imported MIS Order Pad File. File Name: " + filename, user.CurrentUserName, Guid.Empty);
                outa = GetValidateData(root, misName).ToXmlString();
            }
            catch (Exception ex)
            {
                outa = "Exception : " + ex.Message;
            }

            return outa;
        }



        public string GetData(IBrokerManagedComponent bmc)
        {
            return bmc.GetDataStream(-911, string.Empty);// run default Command From CM Class
        }

        private List<MISPaidOrderMessage> GetValidateData(XElement root, string misName)
        {
        List<MISPaidOrderMessage> messages = new List<MISPaidOrderMessage>();
            //<PaidOrder> \n" +
            //                                "<UnitholderCode>{0}</UnitholderCode> \n" +
            //                                "<UnitholderSubCode>{1}</UnitholderSubCode> \n" +
            //                                "<FundCode>{2}</FundCode>\n " +
            //                                "<MCHCode>{3}</MCHCode> \n" +
            //                                 "<TradeDate>{4}</TradeDate> \n" +
            //                                "<Status>{5}</Status> \n" +
            //                                "<ClearDate>{6}</ClearDate> \n" +
            //                                "<Amount>{7}</Amount> \n" +
            //                                "<TransactionType>{8}</TransactionType> \n" +
            //                                "<CurrencyOfTrade>{9}</CurrencyOfTrade> \n" +
            //                                      "<PurchaseSource>{10}</PurchaseSource> \n" +
            //                                "<ProcessAtNAV>{11}</ProcessAtNAV> \n" +


            //                                "</PaidOrder> 
            XElement[] elements = root.XPathSelectElements("//PaidOrder").ToArray();

            foreach (var each in elements)
            {
            string fundCode = each.XPathSelectElement("FundCode").Value;
                string clientAccount = each.XPathSelectElement("UnitholderCode").Value;

                OrganizationUnitCM client = null;
                OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
                {

                    if ((bmc is OrganizationUnitCM) && (bmc as OrganizationUnitCM).ClientId != null && (bmc as OrganizationUnitCM).ClientId.Trim() == clientAccount.Trim())
                    {
                        client = (bmc as OrganizationUnitCM);

                    }

                });


                if (client == null)
                {
                MISPaidOrderMessage message = new MISPaidOrderMessage();

                    message.Message += "Client with id (" + clientAccount + ") doesn't exists.Skipped.";
                    message.UnitholderCode = clientAccount;
                    message.MessageType = ImportMessageType.Warning;
                    //message.Status = ImportMessageStatus.Failed;
                    message.Status = ImportMessageStatus.AccountNotFound;
                    //message.MessageType = ImportMessageType.Error;
                    messages.Add(message);

                }
                else
                {
                    bool isAttachedWithClient = client.IsMISAttachedWithFund(fundCode, misName);
                    if (!isAttachedWithClient)
                    {
                    MISPaidOrderMessage message = new MISPaidOrderMessage();

                        message.Message += "Fund code (" + fundCode + ") doesn't exists.Skipped.";
                        message.FundCode = fundCode;
                        message.MessageType = ImportMessageType.MissingInvestmentCode;
                        message.Status = ImportMessageStatus.Failed;
                        messages.Add(message);
                    }
                    else
                    {

                        OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
                           {
                               if (logical.CMType == OrganizationTypeList.GetEntity(OrganizationType.ManagedInvestmentSchemesAccount))
                               {
                                   OrganizationUnitCM unit = (bmc as OrganizationUnitCM);
                                   unit.Broker.SetWriteStart();
                                   if (unit.Name == misName)
                                   {
                                       foreach (var msg in unit.ImportPaidOrders(each))
                                       {
                                       messages.Add((MISPaidOrderMessage)msg);
                                       }
                                   }

                                   bmc.CalculateToken(true);
                               }


                           });
                    }
                }
            }

            return messages;
        }









    }
}
