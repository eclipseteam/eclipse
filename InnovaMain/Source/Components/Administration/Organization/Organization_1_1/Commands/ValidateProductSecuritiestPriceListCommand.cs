﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;

namespace Oritax.TaxSimp.CM.Organization.Commands
{





    public class ValidateProductSecuritiestPriceListCommand : OrganizationCmCommandBase
    {
      

        public override string DoAction(string value)
        {
            string outa = string.Empty;
            try
            {
                string[] values = value.ToDataArray<string>();
                UserEntity user = values[0].ToData<UserEntity>();
                XElement root = XElement.Parse(values[1], LoadOptions.None);
                Guid ProductSecurityId = new Guid(values[2]);

                outa = GetValidateData(root,ProductSecurityId).ToXmlString();
            }
            catch (Exception ex)
            {
                outa = "Exception : " + ex.Message;
            }

            return outa;
        }



        public string GetData(IBrokerManagedComponent bmc)
        {
            return bmc.GetDataStream(-911, string.Empty);// run default Command From CM Class
        }

        private List<ProductSecuirtyPriceMessage> GetValidateData(XElement root,Guid productSecuirtyID)
        {
            List<ProductSecuirtyPriceMessage> messages = new List<ProductSecuirtyPriceMessage>();
            XElement[] elements = root.XPathSelectElements("//ProductSecurityPrice").ToArray();
            
            foreach (var each in elements)
            {
                string StockCode = each.XPathSelectElement("StockCode").Value;
               
              
                if (Organization.Securities.Count(sec => sec.AsxCode == StockCode) == 0)
                {
                    ProductSecuirtyPriceMessage message = new ProductSecuirtyPriceMessage();

                    message.Message += "Stock code (" + StockCode + ") doesn't exists";
                    message.StockCode = StockCode;
                    message.MessageType = ImportMessageType.MissingInvestmentCode;
                    messages.Add(message);
                }


                messages.Add(CreateMessage(each, productSecuirtyID));



            }



            return messages;
        }



        private ProductSecuirtyPriceMessage CreateMessage(XElement price, Guid productSecuirtyID)
        {
            ProductSecuirtyPriceMessage message = new ProductSecuirtyPriceMessage();

           
            DateTimeFormatInfo info = new DateTimeFormatInfo();
            info.ShortDatePattern = "dd/MM/yyyy";

            string stockCode = price.XPathSelectElement("StockCode").Value;
            string market = price.XPathSelectElement("Market").Value;
            string currency = price.XPathSelectElement("Currency").Value;
            string recommendation = price.XPathSelectElement("Recommendation").Value;
            string weighting = price.XPathSelectElement("Weighting").Value;
            string comment = price.XPathSelectElement("Comment").Value;
            string buyPrice = price.XPathSelectElement("BuyPrice").Value;
            string sellPrice = price.XPathSelectElement("SellPrice").Value;
            string dynamicRatingOption = price.XPathSelectElement("DynamicRatingOption").Value;
          
            decimal utemp;
            try
            {

                message.Status = ImportMessageStatus.Sucess;
                message.MessageType = ImportMessageType.Sucess;
                message.StockCode = stockCode;
                message.Market = market;
                message.Currency = currency;
                message.Recommendation = recommendation;
                message.Weighting = weighting;
                message.Comment = comment;
                message.BuyPrice = buyPrice;
                message.SellPrice = sellPrice;
                message.DynamicRatingOption = dynamicRatingOption;
             

             

                if (string.IsNullOrEmpty(currency))
                {
                    message.Message += "Invalid currency,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                }
                if (string.IsNullOrEmpty(market))
                {
                    message.Message += "Invalid market,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                }
               
                if (string.IsNullOrEmpty(recommendation))
                {
                    message.Message += "Invalid Recommendation,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                }

                if (string.IsNullOrEmpty(dynamicRatingOption))
                {
                    message.Message += "Invalid dynamic rating option,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                }
                if (!decimal.TryParse(weighting,  out utemp))
                {
                    message.Message += "Invalid weighting,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                }
                else
                {

                    message.Weighting = utemp.ToString();
                }
              


                if (!decimal.TryParse(buyPrice, out utemp))
                {
                    message.Message += "Invalid buy price,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                }
                else
                {

                    message.BuyPrice = utemp.ToString();
                }
               
                if (!decimal.TryParse(sellPrice, out utemp))
                {
                    message.Message += "Invalid sell price,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                }
                else
                {

                    //message.BuyPrice = utemp.ToString();
                    message.SellPrice = utemp.ToString();
                }


                var productSec = Organization.ProductSecurities.Where(ps => ps.ID == productSecuirtyID).FirstOrDefault();

                if (productSec == null)
                {
                    message.Message += "Product security not found,";
                    message.Status = ImportMessageStatus.AlreayExists;
                    message.MessageType = ImportMessageType.Error;
                }
               
                  var secuirtyEntity=  Organization.Securities.Where(sec => sec.AsxCode == stockCode).FirstOrDefault();
                  if (secuirtyEntity==null)
                    {
                        message.Message += "Invalid stock code,";
                        message.Status = ImportMessageStatus.Failed;
                       // message.MessageType = ImportMessageType.Error;

                    }
                    else
                    {


                        if (productSec!=null && productSec.Details != null && productSec.Details.Count(det => det.SecurityCodeId == secuirtyEntity.ID) > 0)
                        {
                            //message.Message += "Skipped.Price for stock code already exists,";
                            //message.Status = ImportMessageStatus.AlreayExists;
                            //message.MessageType = ImportMessageType.Error;
                            message.Message += "Already exists.Price for stock code will be updated,";
                            message.Status = ImportMessageStatus.AlreayExists;
                            message.MessageType = ImportMessageType.Sucess;
                            
                        }
                       
                    }
               
              

                if (!string.IsNullOrEmpty(message.Message) && message.Message.Contains(','))
                {
                    message.Message = message.Message.Remove(message.Message.LastIndexOf(','));
                }
                else
                {
                    message.Message = "";
                }

            }
            catch
            {

                message = null;
            }
            return message;
        }

      

       

      
        
    }
}
