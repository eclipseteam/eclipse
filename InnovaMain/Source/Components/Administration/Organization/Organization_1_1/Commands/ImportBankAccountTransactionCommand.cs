﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Utilities;
using BankAccountEntity = Oritax.TaxSimp.Common.BankAccountEntity;

namespace Oritax.TaxSimp.CM.Organization.Commands
{
    public class ImportBankAccountTransactionCommand : OrganizationCmCommandBase
    {

        public override string DoAction(string value)
        {
            string outa = string.Empty;
            try
            {
                string[] values = value.ToDataArray<string>();
                UserEntity user = values[0].ToData<UserEntity>();
                XElement root = XElement.Parse(values[1], LoadOptions.None);
                string filename = values[2];
                this.Organization.Broker.LogEvent(EventType.FileImported, "Imported Bankwest transactions. File Name: " + filename, user.CurrentUserName, Guid.Empty);
                List<BankAccountTransactionMessage> messages = new List<BankAccountTransactionMessage>();
                
                XElement[] elements =root.Elements().ToArray();
             
                List<string> addedAccountsNumbers = new List<string>();
                OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
                {
                    OrganizationUnitCM unit = (bmc as OrganizationUnitCM);
                    if (logical.CMType == OrganizationTypeList.GetEntity(OrganizationType.BankAccount))
                    {
                        BankAccountEntity bank = this.GetData(bmc).ToNewOrData<BankAccountEntity>();
                        string bsb = "";
                        if (!string.IsNullOrEmpty(bank.BSB) && bank.BSB.Contains('-'))
                            bsb = bank.BSB.Split('-')[1];
                        string BsBNAccountNumber = bsb + bank.AccountNumber;
                        var each = elements.Where(ss => ss.Attribute("BSBNAccountNumber").Value == BsBNAccountNumber).FirstOrDefault();

                      
                        if (each != null)
                        {
                            unit.Broker.SetWriteStart();
                            addedAccountsNumbers.Add(BsBNAccountNumber);
                            foreach (var msg in unit.ImportTransactions(root))
                            {
                                messages.Add((BankAccountTransactionMessage)msg);
                            }
                            bmc.CalculateToken(true);
                        }

                      
                      
                       
                       
                    }
                    else if ((bmc is IOrganizationUnit) && (bmc as IOrganizationUnit).IsInvestableClient)
                    {
                     

                        unit.Broker.SetWriteStart();
                        unit.AddClientDividend(root, unit);
                        //foreach (var msg in unit.ImportTransactions(root))
                        //{
                        //messages.Add((BankAccountTransactionMessage)msg);
                        //}
                        bmc.CalculateToken(true);
                    }
                   

                });
                //create missing elements
                foreach (var each in elements)
                {
                    XAttribute att = each.Attribute("BSBNAccountNumber");
                    if (!addedAccountsNumbers.Contains(att.Value))
                    {
                        messages.Add(CreateMissingMessage(att.Value));
                    }

                }


                #region old code replaced because of performances issues
                //foreach (var each in elements)
                //{
                //    XAttribute att = each.Attribute("BSBNAccountNumber");


                //    bankAccount = null;
                //    OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
                //    {
                //        if (logical.CMType == OrganizationTypeList.GetEntity(OrganizationType.BankAccount))
                //        {

                //            BankAccountEntity bank = this.GetData(bmc).ToNewOrData<BankAccountEntity>();
                //            string bsb = "";
                //            if (!string.IsNullOrEmpty(bank.BSB) && bank.BSB.Contains('-'))
                //                bsb = bank.BSB.Split('-')[1];
                //            string BsBNAccountNumber = bsb + bank.AccountNumber;
                //            if (att.Value.Trim() == BsBNAccountNumber)
                //            {
                //                bankAccount = bank;
                //            }
                //        }

                //    });

                //    if (bankAccount == null)
                //    {
                //        messages.Add(CreateMissingMessage(att.Value));
                //    }
                //} 




                //OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
                //{
                //    if (logical.CMType == OrganizationTypeList.GetEntity(OrganizationType.BankAccount))
                //    {
                //        OrganizationUnitCM unit = (bmc as OrganizationUnitCM);

                //        unit.Broker.SetWriteStart();
                //        foreach (var msg in unit.ImportTransactions(root))
                //        {
                //            messages.Add((BankAccountTransactionMessage)msg);
                //        }
                //        unit.AddClientDividend(root, unit);
                //        bmc.CalculateToken(true);
                //    }
                //});

                //OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
                //{
                //    OrganizationUnitCM unit = (bmc as OrganizationUnitCM);
                //    unit.Broker.SetWriteStart();
                //    unit.AddClientDividend(root, unit);
                //    //foreach (var msg in unit.ImportTransactions(root))
                //    //{
                //    //messages.Add((BankAccountTransactionMessage)msg);
                //    //}
                //    bmc.CalculateToken(true);
                //});


                #endregion
             

                outa = messages.ToXmlString(); 
            }
            catch (Exception ex)
            {
                outa = "Exception : " + ex.Message;
            }

            return outa;
        }

        public string GetData(IBrokerManagedComponent bmc)
        {
            return bmc.GetDataStream(-911, string.Empty);// run default Command From CM Class
        }

        private BankAccountTransactionMessage CreateMissingMessage(string BSBNAccountNumber)
        {
            BankAccountTransactionMessage message = new BankAccountTransactionMessage();



            message.Message += "Account Number (" + BSBNAccountNumber + ") doesn't exists";
            message.BSBNAccountNumber = BSBNAccountNumber;
            message.MessageType = ImportMessageType.InvestmentCodeAdded;
            //message.Status = ImportMessageStatus.Failed;




            return message;

        }




    }
}
