﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;

namespace Oritax.TaxSimp.CM.Organization.Commands
{





    public class ValidateStatestreetPriceListCommand : OrganizationCmCommandBase
    {
      

        public override string DoAction(string value)
        {
            string outa = string.Empty;
            try
            {
                string[] values = value.ToDataArray<string>();
                UserEntity user = values[0].ToData<UserEntity>();
                XElement root = XElement.Parse(values[1], LoadOptions.None);

                outa = GetValidateData(root).ToXmlString();
            }
            catch (Exception ex)
            {
                outa = "Exception : " + ex.Message;
            }

            return outa;
        }



        public string GetData(IBrokerManagedComponent bmc)
        {
            return bmc.GetDataStream(-911, string.Empty);// run default Command From CM Class
        }

        private List<StatestreetPriceMessage> GetValidateData(XElement root)
        {
            List<StatestreetPriceMessage> messages = new List<StatestreetPriceMessage>();
            XElement[] elements = root.XPathSelectElements("//StateStreetPrice").ToArray();

            foreach (var each in elements)
            {
                string InvestmentCode = each.XPathSelectElement("SecurityCode").Value;
               

                if (Organization.Securities.Count(sec => sec.AsxCode == InvestmentCode) == 0)
                {
                    StatestreetPriceMessage message = new StatestreetPriceMessage();

                    message.Message += "Fund code (" + InvestmentCode + ") doesn't exists";
                    message.InvestmentCode = InvestmentCode;
                    message.MessageType = ImportMessageType.MissingInvestmentCode;
                    messages.Add(message);
                }


                messages.Add(CreateMessage(each));



            }



            return messages;
        }



        private StatestreetPriceMessage CreateMessage(XElement price)
        {
            StatestreetPriceMessage message = new StatestreetPriceMessage();

           
            DateTimeFormatInfo info = new DateTimeFormatInfo();
            info.ShortDatePattern = "dd/MM/yyyy";

            string InvestmentCode = price.XPathSelectElement("SecurityCode").Value;
            string CompanyName = price.XPathSelectElement("CompanyName").Value;
            string PriceDate = price.XPathSelectElement("PriceDate").Value;
            string Price_NAV = price.XPathSelectElement("Price_NAV").Value;
            string Price_PUR = price.XPathSelectElement("Price_PUR").Value;
            string Price_RDM = price.XPathSelectElement("Price_RDM").Value;
            string Currency = price.XPathSelectElement("Currency").Value;
            DateTime date;
            decimal utemp;
            try
            {

                message.Status = ImportMessageStatus.Sucess;
                message.MessageType = ImportMessageType.Sucess;
                message.InvestmentCode = InvestmentCode;
                message.CompanyName = CompanyName;
                message.PriceDate = PriceDate;

                message.Price_NAV = Price_NAV;
                message.Price_PUR = Price_PUR;
                message.Price_RDM = Price_RDM;
                message.Currency = Currency;

                if (Organization.Securities.Count(sec => sec.AsxCode == InvestmentCode) == 0)
                {


                    message.Message += "Invalid Fund code,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;

                }





                if (!DateTime.TryParse(price.XPathSelectElement("PriceDate").Value, info, DateTimeStyles.None, out date))
                {

                    message.Message += "Invalid Price Date,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                }
               else
               {
                   message.PriceDate = date.ToString("dd/MM/yyyy");
               }



            var security=    Organization.Securities.Where(sec =>sec.AsxCode==InvestmentCode).FirstOrDefault();


                if(security!=null)
                {
                    if (security.ASXSecurity!=null && security.ASXSecurity.Count(s => s.Date == date) > 0)
                    {
                        message.Message += "Price for date already exists,";
                        message.Status = ImportMessageStatus.AlreayExists;
                        message.MessageType = ImportMessageType.Error;
                        
                    }
                }


                if (!string.IsNullOrEmpty(price.XPathSelectElement("Price_NAV").Value))
                    if (!decimal.TryParse(price.XPathSelectElement("Price_NAV").Value, NumberStyles.Currency, CultureInfo.CurrentCulture, out utemp))
                    {
                        message.Message += "Invalid Price (NAV),";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                    }else
                   {

                    message.Price_NAV = utemp.ToString();
                   }

                if (!string.IsNullOrEmpty(price.XPathSelectElement("Price_PUR").Value))
                    if (!decimal.TryParse(price.XPathSelectElement("Price_PUR").Value, NumberStyles.Currency, CultureInfo.CurrentCulture, out utemp))
                    {
                        message.Message += "Invalid Price (PUR),";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                    }
                    else
                    {

                       
                        message.Price_PUR = utemp.ToString();
                    }
                if (!string.IsNullOrEmpty(price.XPathSelectElement("Price_RDM").Value))
                    if (!decimal.TryParse(price.XPathSelectElement("Price_RDM").Value, NumberStyles.Currency, CultureInfo.CurrentCulture, out utemp))
                    {
                        message.Message += "Invalid Price (RDM),";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                    }
                    else
                    {

                        // string format = "$#,##0.00;-$#,##0.00;$0";

                        message.Price_RDM = utemp.ToString();
                    }

                if (string.IsNullOrEmpty(price.XPathSelectElement("Currency").Value))
                {
                    message.Message += "Invalid Currency";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                }

                if (!string.IsNullOrEmpty(message.Message) && message.Message.Contains(','))
                {
                    message.Message = message.Message.Remove(message.Message.LastIndexOf(','));
                }
                else
                {
                    message.Message = "";
                }

            }
            catch
            {

                message = null;
            }
            return message;
        }

      

       

      
        
    }
}
