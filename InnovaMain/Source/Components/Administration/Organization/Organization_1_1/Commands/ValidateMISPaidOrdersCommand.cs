﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;

namespace Oritax.TaxSimp.CM.Organization.Commands
{





    public class ValidateMISPaidOrdersCommand : OrganizationCmCommandBase
    {
      

        public override string DoAction(string value)
        {
            string outa = string.Empty;
            try
            {
                string[] values = value.ToDataArray<string>();
                UserEntity user = values[0].ToData<UserEntity>();
                XElement root = XElement.Parse(values[1], LoadOptions.None);
               string misName = values[2];

                outa = GetValidateData(root, misName).ToXmlString();
            }
            catch (Exception ex)
            {
                outa = "Exception : " + ex.Message;
            }

            return outa;
        }



        public string GetData(IBrokerManagedComponent bmc)
        {
            return bmc.GetDataStream(-911, string.Empty);// run default Command From CM Class
        }

        private List<MISPaidOrderMessage> GetValidateData(XElement root, string misName)
        {
        List<MISPaidOrderMessage> messages = new List<MISPaidOrderMessage>();


            //<PaidOrder> \n" +
            //                                "<UnitholderCode>{0}</UnitholderCode> \n" +
            //                                "<UnitholderSubCode>{1}</UnitholderSubCode> \n" +
            //                                "<FundCode>{2}</FundCode>\n " +
            //                                "<MCHCode>{3}</MCHCode> \n" +
            //                                 "<TradeDate>{4}</TradeDate> \n" +
            //                                "<Status>{5}</Status> \n" +
            //                                "<ClearDate>{6}</ClearDate> \n" +
            //                                "<Amount>{7}</Amount> \n" +
            //                                "<TransactionType>{8}</TransactionType> \n" +
            //                                "<CurrencyOfTrade>{9}</CurrencyOfTrade> \n" +
            //                                      "<PurchaseSource>{10}</PurchaseSource> \n" +
            //                                "<ProcessAtNAV>{11}</ProcessAtNAV> \n" +


            //                                "</PaidOrder> 
            XElement[] elements = root.XPathSelectElements("//PaidOrder").ToArray();

            foreach (var each in elements)
            {
            string fundCode = each.XPathSelectElement("FundCode").Value;
                string ClientAccount = each.XPathSelectElement("UnitholderCode").Value;

                OrganizationUnitCM client = null;
                OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
                {

                    if ((bmc is OrganizationUnitCM) && (bmc as OrganizationUnitCM).ClientId != null && (bmc as OrganizationUnitCM).ClientId.Trim() == ClientAccount.Trim())
                    {
                        client = (bmc as OrganizationUnitCM);
                     
                    }
                    

                
                });


                if(client==null)
                {
                MISPaidOrderMessage message = new MISPaidOrderMessage();

                    message.Message += "Client with id (" + ClientAccount + ") doesn't exists";
                    message.UnitholderCode = ClientAccount;
                    message.MessageType = ImportMessageType.Warning;
                    //message.Status = ImportMessageStatus.Failed;
                    message.Status = ImportMessageStatus.AccountNotFound;
                    //message.MessageType = ImportMessageType.Error;
                    messages.Add(message);

                }
                else
                {

                    bool isAttachedWithClient = client.IsMISAttachedWithFund(fundCode, misName);


                    if (!isAttachedWithClient)
                    {
                    MISPaidOrderMessage message = new MISPaidOrderMessage();

                        message.Message += "Fund code (" + fundCode + ") doesn't exists.Or Fund code is not attached with any client";
                        message.FundCode = fundCode;
                        message.MessageType = ImportMessageType.MissingInvestmentCode;
                        //message.Status = ImportMessageStatus.Failed;
                        message.Status = ImportMessageStatus.Failed;
                        //message.MessageType = ImportMessageType.Error;



                        messages.Add(message);
                    }


                    messages.Add(CreateMessage(each, isAttachedWithClient));

                   

                }


                


              



            }



            return messages;
        }



        private MISPaidOrderMessage CreateMessage(XElement asxPrice, bool isAttached)
        {
        MISPaidOrderMessage message = new MISPaidOrderMessage();

            DateTimeFormatInfo info = new DateTimeFormatInfo();
            info.ShortDatePattern = "dd/MM/yyyy";
            
            string UnitholderCode =  asxPrice.XPathSelectElement("UnitholderCode").Value;
            string UnitholderSubCode= asxPrice.XPathSelectElement("UnitholderSubCode").Value;
            string FundCode = asxPrice.XPathSelectElement("FundCode").Value;
            string MCHCode =  asxPrice.XPathSelectElement("MCHCode").Value;
            string TradeDate = asxPrice.XPathSelectElement("TradeDate").Value;
            string  ClearDate =  asxPrice.XPathSelectElement("ClearDate").Value;
            string Amount =  asxPrice.XPathSelectElement("Amount").Value;
            string TransactionType =asxPrice.XPathSelectElement("TransactionType").Value;
            string CurrencyOfTrade  = asxPrice.XPathSelectElement("CurrencyOfTrade").Value;
            string PurchaseSource = asxPrice.XPathSelectElement("PurchaseSource").Value;
            string ProcessAtNAV =  asxPrice.XPathSelectElement("ProcessAtNAV").Value;


            string RedemptionOrExchangeOptions = asxPrice.XPathSelectElement("RedemptionOrExchangeOptions").Value;
            string NumberofSharesNotCorrect = asxPrice.XPathSelectElement("NumberofShares").Value;
            string ExchangeToFund = asxPrice.XPathSelectElement("ExchangeToFund").Value;
            string DealerCommissionRate = asxPrice.XPathSelectElement("DealerCommissionRate").Value;
          
            //Need to remove this code
            string NumberofShares = string.Empty;

            if (TransactionType == "690")
            {
                NumberofShares = String.Concat(NumberofSharesNotCorrect.Remove(0, 1), ExchangeToFund.Remove(ExchangeToFund.Count() - 1, 1));
                ExchangeToFund = string.Empty;
            }
            string ExternalID = asxPrice.XPathSelectElement("ExternalID").Value;
            string RedemptionSource = asxPrice.XPathSelectElement("RedemptionSource").Value;
            string OperatorEnteredBy = asxPrice.XPathSelectElement("OperatorEnteredBy").Value;
            
            string Generator = asxPrice.XPathSelectElement("Generator").Value;
            string EffectiveDate = asxPrice.XPathSelectElement("EffectiveDate").Value;
            string AUDEquivalentAmount = asxPrice.XPathSelectElement("AUDEquivalentAmount").Value;


            DateTime date;
            decimal utemp;
            try
            {

                message.Status = ImportMessageStatus.Sucess;
                message.MessageType = ImportMessageType.Sucess;

               

                message.UnitholderCode = UnitholderCode;
                message.UnitholderSubCode = UnitholderSubCode;
                message.FundCode = FundCode;
                message.MCHCode = MCHCode;
                message.TradeDate = TradeDate;
                message.ClearDate = ClearDate;
                message.Amount = Amount;
                message.TransactionType = TransactionType;
                message.CurrencyOfTrade = CurrencyOfTrade;
                message.PurchaseSource = PurchaseSource;
                message.ProcessAtNAV = ProcessAtNAV;

                message.RedemptionOrExchangeOptions = RedemptionOrExchangeOptions;
                message.NumberofShares = NumberofShares;
                message.ExchangeToFund = ExchangeToFund;
                message.DealerCommissionRate = DealerCommissionRate;
               
                message.ExternalID = ExternalID;
                message.RedemptionSource = RedemptionSource;
                message.OperatorEnteredBy = OperatorEnteredBy;
                
                message.Generator = Generator;
                message.EffectiveDate = EffectiveDate;
                message.AUDEquivalentAmount = AUDEquivalentAmount;
                


                if (!isAttached)
                {
                    message.Message += "Fund code (" + FundCode + ") doesn't exists.Or fund code is not attached with any client,";

                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                }
                
                else
                {

                    //message.Message += "Fund code (" + FundCode + ") doesn't exists.Or fund code is not attached with any client,";
                    //message.Status = ImportMessageStatus.Failed;
                    //message.MessageType = ImportMessageType.Error;
                }

                
                    if (!DateTime.TryParseExact(TradeDate, "yyyyMMdd", info, DateTimeStyles.None, out date))
                {

                    message.Message += "Invalid Price Date,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                }
               else
               {
                   message.TradeDate = date.ToString("dd/MM/yyyy");
               }

                    if (!DateTime.TryParseExact(ClearDate,"yyyyMMdd", info, DateTimeStyles.None, out date))
                    {

                    message.Message += "Invalid Clear Date,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                    }
                else
                    {
                    message.ClearDate = date.ToString("dd/MM/yyyy");
                    }


                    if (!string.IsNullOrEmpty(EffectiveDate))
                        if (!DateTime.TryParseExact(EffectiveDate, "yyyyMMdd", info, DateTimeStyles.None, out date))
                            {

                            message.Message += "Invalid Effective Date,";
                            message.Status = ImportMessageStatus.Failed;
                            message.MessageType = ImportMessageType.Error;
                            }
                        else
                            {
                            message.EffectiveDate = date.ToString("dd/MM/yyyy");
                           
                            }
               
                if (!string.IsNullOrEmpty(Amount))
                    if (!decimal.TryParse(Amount, NumberStyles.Currency, CultureInfo.CurrentCulture, out utemp))
                    {
                        message.Message += "Invalid Amount,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                    }
                    else
                    {

                        // string format = "$#,##0.00;-$#,##0.00;$0";

                        message.Amount = utemp.ToString("C");
                    }


                if (!string.IsNullOrEmpty(NumberofShares))
                    if (!decimal.TryParse(NumberofShares, NumberStyles.Currency, CultureInfo.CurrentCulture, out utemp))
                        {
                        message.Message += "Invalid Number of Shares,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                        }
                    else
                        {

                        // string format = "$#,##0.00;-$#,##0.00;$0";

                        message.NumberofShares = utemp.ToString("C");
                        }

                if (!string.IsNullOrEmpty(message.Message) && message.Message.Contains(','))
                {
                    message.Message = message.Message.Remove(message.Message.LastIndexOf(','));
                }
                else
                {
                    message.Message = "";
                }

            }
            catch
            {

                message = null;
            }
            return message;
        }

      

       

      
        
    }
}
