﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;

namespace Oritax.TaxSimp.CM.Organization.Commands
{
    public class ImportAllCommand : OrganizationCmCommandBase
    {
        const string DBUserModuleName = "DBUser_1_1";
        ILogicalModule _OrganizationLM;
        List<ILogicalModule> _Modules;

        public override string DoAction(string value)
        {
            string outa = string.Empty;
            Dictionary<IBrokerManagedComponent, XElement> components = new Dictionary<IBrokerManagedComponent, XElement>();
            IBrokerManagedComponent component = null;
            _Modules = new List<ILogicalModule>();
            try
            {
                String[] values = value.ToDataArray<String>();
                UserEntity user = values[0].ToData<UserEntity>();
                XElement element = XElement.Parse(values[1]);
                XElement[] elements = element.XPathSelectElements("OrganizationUnits/OraganizationUnit").ToArray();
                IdentityCmMap map = new IdentityCmMap();

                Organization.Broker.SetWriteStart();
                Organization.Import(element.XPathSelectElement("Organization"), map);
                _OrganizationLM = Organization.Broker.GetLogicalCM(Organization.CLID);
                foreach (XElement each in elements)
                {
                    component = AddOrganizationUnit(each.Attribute("AssemblyId").Value.ToGuid(), each.Attribute("Name").Value);
                    components.Add(component, each);
                    map.Add(ToIdentityCM(each), ToIdentityCM(component));
                }

                foreach (var each in components)
                {
                    ImportFromXElement(each.Key as ICalculationModule, each.Value, map);
                    CreateDefaultUser(each.Key as ICanCreateDefaultUser, user);
                    AddToSecurity(each.Key as ISecurityService, user);
                }
                outa = "Imported completed successfully.";
            }
            catch (Exception ex)
            {
                outa = "Exception : " + ex.Message;
            }
            finally
            {
                Organization.Broker.ReleaseBrokerManagedComponent(_OrganizationLM);
                foreach (var each in _Modules) Organization.Broker.ReleaseBrokerManagedComponent(each);
                foreach (var each in components.Keys) Organization.Broker.ReleaseBrokerManagedComponent(each);
                _OrganizationLM = null;
                _Modules = null;
            }
            return outa;
        }

        private IBrokerManagedComponent AddOrganizationUnit(Guid assemblyId, string name)
        {
            ILogicalModule createdLM = Organization.Broker.CreateLogicalCM(assemblyId, name, Guid.Empty, null);
            _OrganizationLM.AddChildLogicalCM(createdLM);
            _Modules.Add(createdLM);
            return (IBrokerManagedComponent)createdLM[createdLM.CurrentScenario];
        }

        private void ImportFromXElement(ICalculationModule module, XElement root, IdentityCmMap map)
        {
            if (module == null || !(module is ISupportMigration)) return;
            (module as ISupportMigration).Import(root, map);
        }

        private void CreateDefaultUser(ICanCreateDefaultUser creator, UserEntity user)
        {
            if (creator == null) return;
            IApplicationUserService service = Organization.Broker.GetCMImplementation(DBUserModuleName, DBUserModuleName) as IApplicationUserService;
            ApplicationUser appUser = creator.GetDefaultUser();
            service.AddUser(user, appUser);
            SendEMail(appUser);
        }

        private void AddToSecurity(ISecurityService security, UserEntity user)
        {
            if (security == null && !(security is ICanCreateDefaultUser)) return;
            security.AddUser(user);
        }

        private void SendEMail(ApplicationUser user)
        {
            try
            {
                SmtpClient client = new SmtpClient(Organization.SmtpSettings.HostName, Organization.SmtpSettings.PortNumber);
                client.Credentials = new NetworkCredential(Organization.SmtpSettings.UserName, Organization.SmtpSettings.Password);
                using (MailMessage mail = new MailMessage())
                {
                    mail.From = new MailAddress(Organization.SmtpSettings.UserName);
                    mail.To.Add(new MailAddress(user.Email));
                    mail.Subject = "Eclipse account details";
                    StringBuilder body = new StringBuilder();
                    body.AppendFormat(" Your new account has been automatically created at eclipse. Account details are give below. {0} {0} {0}", Environment.NewLine);
                    body.AppendFormat(" LoginId : {0}  {1} {1}", user.LoginId, Environment.NewLine);
                    body.AppendFormat(" Password : {0}  {1} {1}", user.Password, Environment.NewLine);
                    body.AppendFormat(" Regards,  {0} Eclipse Administrator", Environment.NewLine);
                    mail.Body = body.ToString();

                    if (Organization.SmtpSettings.UseSsl) client.EnableSsl = true;
                    client.Send(mail);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        private static IIdentityCM ToIdentityCM(XElement element)
        {
            return new IdentityCM(element.Attribute("Clid").Value, element.Attribute("Csid").Value);
        }

        private static IIdentityCM ToIdentityCM(IBrokerManagedComponent component)
        {
            if (component == null || !(component is IIdentityCM)) return IdentityCM.Null;
            IIdentityCM identity = component as IIdentityCM;
            return new IdentityCM { Clid = identity.Clid, Csid = identity.Csid };
        }

    }
}
