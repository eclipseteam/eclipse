﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.CM.Organization.Commands
{





    public class ImportProductSecuirtiesPriceListCommand : OrganizationCmCommandBase
    {
      

        public override string DoAction(string value)
        {
            string outa = string.Empty;
            try
            {
                string[] values = value.ToDataArray<string>();
                UserEntity user = values[0].ToData<UserEntity>();
                XElement root = XElement.Parse(values[1], LoadOptions.None);
                string filename = values[2];
                Guid ProductSecurityId = new Guid(values[3]);
                this.Organization.Broker.LogEvent(EventType.FileImported, "Imported Product Secuirities Price List. File Name: " + filename, user.CurrentUserName, Guid.Empty);
                outa = GetValidateData(root, ProductSecurityId).ToXmlString();
            }
            catch (Exception ex)
            {
                outa = "Exception : " + ex.Message;
            }

            return outa;
        }



        public string GetData(IBrokerManagedComponent bmc)
        {
            return bmc.GetDataStream(-911, string.Empty);// run default Command From CM Class
        }

        private List<ProductSecuirtyPriceMessage> GetValidateData(XElement root,Guid productSecuirtyID)
        {
            List<ProductSecuirtyPriceMessage> messages = new List<ProductSecuirtyPriceMessage>();
            ObservableCollection<ProductSecurityDetail> lst = new ObservableCollection<ProductSecurityDetail>();
            var productSec = Organization.ProductSecurities.Where(ps => ps.ID == productSecuirtyID).FirstOrDefault();
            XElement[] elements = root.XPathSelectElements("//ProductSecurityPrice").ToArray();

            Organization.Broker.SetWriteStart();
            foreach (var each in elements)
            {
                string InvestmentCode = each.XPathSelectElement("StockCode").Value;
               
            
               SecuritiesEntity secuirtyEntity = Organization.Securities.Where(sec => sec.AsxCode == InvestmentCode).FirstOrDefault();

                 if (secuirtyEntity == null)
                {
                    secuirtyEntity = new SecuritiesEntity
                    {
                        AsxCode = InvestmentCode,
                       // CompanyName = CompanyName,
                        ID = Guid.NewGuid(),
                        ASXSecurity = new ObservableCollection<ASXSecurityEntity>()
                    };

                    Organization.Securities.Add(secuirtyEntity);

                    ProductSecuirtyPriceMessage message = new ProductSecuirtyPriceMessage();
                    message.StockCode = InvestmentCode;
                    message.MessageType = ImportMessageType.InvestmentCodeAdded;
                    message.Message += "Stock code" + InvestmentCode + " has been added";
                    messages.Add(message);
                }
                 messages.Add(CreateMessage(each, secuirtyEntity, productSecuirtyID));

                
            }

           
           
            // code changes from here
                 if (productSec != null)
                 {
                     foreach (var count in productSec.Details)
                     {
                         var security = Organization.Securities.Where(sec => sec.ID == count.SecurityCodeId).FirstOrDefault();
                         if (security != null)
                         {
                             var xmlelement = elements.Where(e => e.XPathSelectElement("StockCode").Value == security.AsxCode).FirstOrDefault();
                             if (xmlelement == null)
                             {
                                 lst.Add(count);
                                 ProductSecuirtyPriceMessage message = new ProductSecuirtyPriceMessage();
                                 message.Status = ImportMessageStatus.SuccessFullyDeleted;
                                 message.StockCode = security.AsxCode;
                                 message.MessageType = ImportMessageType.InvestmentCodeDeleted;
                                 message.Message += "Stock code " + security.AsxCode + " has been deactivated";
                                 messages.Add(message);
                             }
                         }
                         else
                         {
                             lst.Add(count);
                         }
                             //ProductSecuirtyPriceMessage message = new ProductSecuirtyPriceMessage();
                             //message.StockCode = count.MarketCode;
                             //message.MessageType = ImportMessageType.InvestmentCodeAdded;
                             //message.Message += "Stock code" + count.MarketCode + " has been removed";
                             //messages.Add(message);
                     }

                     foreach (var count in lst)
                     {
                         var remove = productSec.Details.Where(e => e.DetailId == count.DetailId).FirstOrDefault();
                         if (remove != null)
                         {
                         remove.IsDeactive = true;

                         }
                     }
                 }
                // code changes till here

            


            //List<SecuritiesEntity> lst = new List<SecuritiesEntity>();
            //foreach (var count in Organization.Securities)
            //{
                
            //    var xmlelement = elements.Where(e => e.XPathSelectElement("StockCode").Value == count.AsxCode).FirstOrDefault();
            //    if (xmlelement == null)
            //    {
            //        lst.Add(count);
            //    }
            //}

            //foreach (var count in lst)
            //{
            //    var securityentity = Organization.Securities.Where(e => e.AsxCode == count.AsxCode).FirstOrDefault();
            //    if (securityentity != null)
            //    {
            //        Organization.Securities.Remove(securityentity);
            //    }
            //}

            return messages;
        }



        private ProductSecuirtyPriceMessage CreateMessage(XElement price, SecuritiesEntity secuirtyEntity,Guid productSecuirtyID)
        {
            ProductSecuirtyPriceMessage message = new ProductSecuirtyPriceMessage();


            DateTimeFormatInfo info = new DateTimeFormatInfo();
            info.ShortDatePattern = "dd/MM/yyyy";

            string stockCode = price.XPathSelectElement("StockCode").Value;
            string market = price.XPathSelectElement("Market").Value;
            string currency = price.XPathSelectElement("Currency").Value;
            string recommendation = price.XPathSelectElement("Recommendation").Value;
            string weighting = price.XPathSelectElement("Weighting").Value;
            string comment = price.XPathSelectElement("Comment").Value;
            string buyPrice = price.XPathSelectElement("BuyPrice").Value;
            string sellPrice = price.XPathSelectElement("SellPrice").Value;
            string dynamicRatingOption = price.XPathSelectElement("DynamicRatingOption").Value;

            double utemp;
            try
            {

                message.Status = ImportMessageStatus.Sucess;
                message.MessageType = ImportMessageType.Sucess;
                message.StockCode = stockCode;
                message.Market = market;
                message.Currency = currency;
                message.Recommendation = recommendation;
                message.Weighting = weighting;
                message.Comment = comment;
                message.BuyPrice = buyPrice;
                message.SellPrice = sellPrice;
                message.DynamicRatingOption = dynamicRatingOption;
                
             
                 
                ProductSecurityDetail detail=new ProductSecurityDetail();
                detail.DetailId = Guid.NewGuid();
                detail.Comments = comment;
                //if (Organization.Securities.Count(sec => sec.AsxCode == stockCode) == 0)
                //{
                //    message.Message += "Invalid stock code,";
                //    message.Status = ImportMessageStatus.Failed;
                //    message.MessageType = ImportMessageType.Error;

                //}

                if (string.IsNullOrEmpty(currency))
                {
                    message.Message += "Invalid currency,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                }else
                {
                    detail.Currency = currency;

                }
                if (string.IsNullOrEmpty(market))
                {
                    message.Message += "Invalid market,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                }else
                {
                    detail.MarketCode = market;
                }

                if (string.IsNullOrEmpty(recommendation))
                {
                    message.Message += "Invalid Recommendation,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                }else
                {
                    detail.Rating = recommendation;
                }

                if (string.IsNullOrEmpty(dynamicRatingOption))
                {
                    message.Message += "Invalid dynamic rating option,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                }else
                {
                    detail.RatingOptions = dynamicRatingOption;
                }



                if (!double.TryParse(weighting, out utemp))
                {
                    message.Message += "Invalid weighting,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                }
                else
                {

                    message.Weighting = utemp.ToString();
                    detail.Allocation = utemp;
                }



                if (!double.TryParse(buyPrice, out utemp))
                {
                    message.Message += "Invalid buy price,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                }
                else
                {

                    message.BuyPrice = utemp.ToString();
                    detail.BuyPrice = utemp;
                }

                if (!double.TryParse(sellPrice, out utemp))
                {
                    message.Message += "Invalid sell price,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                }
                else
                {

                    //message.BuyPrice = utemp.ToString();
                    message.SellPrice = utemp.ToString();
                    detail.SellPrice = utemp;
                }




             




               




                if (message.MessageType != ImportMessageType.Error)
                {


                    
                    


                        var productSec =Organization.ProductSecurities.Where(ps => ps.ID == productSecuirtyID).FirstOrDefault();

                        if (productSec == null)
                        {
                            message.Message += "Product Security not found,";
                            message.Status = ImportMessageStatus.AlreayExists;
                            message.MessageType = ImportMessageType.Error;
                        }
                        else
                        {
                            if (secuirtyEntity == null)
                            {
                                message.Message += "Invalid Stock code,Ignored.,";
                                message.Status = ImportMessageStatus.Failed;
                                message.MessageType = ImportMessageType.Error;

                            }
                            else
                            {
                               if( productSec.Details==null)
                               {
                                   productSec.Details=new ObservableCollection<ProductSecurityDetail>();
                               }

                                if (productSec.Details.Count(det=>det.SecurityCodeId==secuirtyEntity.ID)>0)
                                {
                                    //message.Message += "Skipped.Price for stock code already exists,";
                                    //message.Status = ImportMessageStatus.AlreayExists;
                                    //message.MessageType = ImportMessageType.Error;
                                    message.Message += "Already exists.Price for stock code updated,";
                                    message.Status = ImportMessageStatus.AlreayExists;
                                    message.MessageType = ImportMessageType.Sucess;
                                    var det = productSec.Details.Where(e => e.SecurityCodeId == secuirtyEntity.ID).FirstOrDefault();
                                    if (det != null)
                                    {
                                        productSec.Details.Remove(det);
                                    }
                                    detail.SecurityCodeId = secuirtyEntity.ID;
                                    productSec.Details.Add(detail);
                                }
                                else
                                {
                                    detail.SecurityCodeId = secuirtyEntity.ID;

                                    productSec.Details.Add(detail);
                                }


                            }
                        }




                }


                if (!string.IsNullOrEmpty(message.Message) && message.Message.Contains(','))
                {
                    message.Message = message.Message.Remove(message.Message.LastIndexOf(','));
                }
                else
                {
                    message.Message = "";
                }

            }
            catch
            {

                message = null;
            }
            return message;
        }

      

       

      
        
    }
}
