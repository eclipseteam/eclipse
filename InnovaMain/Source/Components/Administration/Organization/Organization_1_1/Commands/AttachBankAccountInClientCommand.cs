﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;

namespace Oritax.TaxSimp.CM.Organization.Commands
    {
    public class AttachBankAccountInClientCommand : OrganizationCmCommandBase
        {

        public override string DoAction(string value)
            {
            string outa = string.Empty;
            try
                {
                var values = value.ToDataArray<string>();
                //var user = values[0].ToData<UserEntity>();
                var clientId = values[0];
                var unit = values[1].ToData<OrganizationUnit>();
                //var entity = values[2].ToData<BankAccountEntity>();
                //var account = values[3].ToData<IdentityCM>();
                //var unit = new OrganizationUnit
                //{
                //    CurrentUser = user,
                //    Name = entity.Name,
                //    Type = ((int)OrganizationType.BankAccount).ToString(),
                //    Data = entity.ToXmlString(),
                //    Clid=account.Clid,
                //    Csid = account.Csid
                //};
                var units = new List<OrganizationUnit>() { unit };

                OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
                {
                    if (bmc is OrganizationUnitCM && (bmc as OrganizationUnitCM).ClientId == clientId)
                        {
                        var client = (bmc as OrganizationUnitCM);
                        client.Broker.SetWriteStart();
                        client.ImportBankwestData(units.ConvertAll<IIdentityCM>(u => u));
                        bmc.CalculateToken(true);
                        outa = unit.ToXmlString();
                        }
                });

                }
            catch (Exception ex)
                {
                outa = "Exception : " + ex.Message;
                }

            return outa;
            }



        private void GetValidationMessage(XElement value, List<BankAccountImportMessage> messages, string clientID, bool HasFoundClient)
            {
            XElement[] elements = value.XPathSelectElements("Account").ToArray();


            foreach (var each in elements)
                {
                BankAccountImportMessage message = new BankAccountImportMessage();
                message.Status = ImportMessageStatus.Sucess;
                message.MessageType = ImportMessageType.Sucess;
                message.AccountName = each.XPathSelectElement("AccountName").Value;
                message.Type = each.XPathSelectElement("Type").Value;
                message.OrganizationStatus = each.XPathSelectElement("OrganizationStatus").Value;
                message.BSB = each.XPathSelectElement("BSB").Value;
                message.BCH = each.XPathSelectElement("BCH").Value;
                message.AccountNumber = each.XPathSelectElement("AccountNumber").Value;
                message.ClientID = clientID;





                if (!HasFoundClient)
                    {

                    message.Message += "Client with ID (" + clientID + ") doesn't exists,";
                    message.Status = ImportMessageStatus.AccountNotFound;
                    message.MessageType = ImportMessageType.Error;

                    }

                if (string.IsNullOrEmpty(message.AccountName) || message.AccountName.Trim() == string.Empty)
                    {
                    message.Message += "Account Name (" + message.AccountName + ") is empty,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                    }


                if (string.IsNullOrEmpty(message.AccountNumber) || message.AccountNumber.Trim() == string.Empty)
                    {
                    message.Message += "Account Number (" + message.AccountNumber + ") is empty,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                    }


                if (string.IsNullOrEmpty(message.BSB) || message.BSB.Trim() == string.Empty)
                    {
                    message.Message += "Account BSB (" + message.BSB + ") is empty,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                    }

                if (string.IsNullOrEmpty(message.BCH) || message.BCH.Trim() == string.Empty)
                    {
                    message.Message += "Account BCH (" + message.BCH + ") is empty,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                    }


                if (CheckAccountNumberAndBSBExists(message.AccountNumber, message.BSB + "-" + message.BCH))
                    {

                    message.Message += "Account Number (" + message.AccountNumber + ") and BSB  (" + message.BSB + "-" + message.BCH + ") already exists,";
                    message.Status = ImportMessageStatus.AlreayExists;
                    message.MessageType = ImportMessageType.Error;

                    }





                if (!string.IsNullOrEmpty(message.Message) && message.Message.Contains(','))
                    {
                    message.Message = message.Message.Remove(message.Message.LastIndexOf(','));
                    }
                else
                    {
                    message.Message = "";
                    }

                messages.Add(message);
                }

            }

        private bool CheckAccountNumberAndBSBExists(string AccountNumber, string BSB)
            {

            bool result = false;

            OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
            {
                if (logical.CMType == OrganizationTypeList.GetEntity(OrganizationType.BankAccount))
                    {



                    Common.BankAccountEntity bank = this.GetData(bmc).ToNewOrData<Common.BankAccountEntity>();




                    if (AccountNumber == bank.AccountNumber && BSB == bank.BSB)
                        {

                        result = true;

                        }




                    }

            });


            return result;

            }


        public string GetData(IBrokerManagedComponent bmc)
            {
            return bmc.GetDataStream(-911, string.Empty);// run default Command From CM Class
            }




        }
    }
