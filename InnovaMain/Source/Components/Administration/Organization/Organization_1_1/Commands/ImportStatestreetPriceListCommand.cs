﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.CM.Organization.Commands
{





    public class ImportStatestreetPriceListCommand : OrganizationCmCommandBase
    {
      

        public override string DoAction(string value)
        {
            string outa = string.Empty;
            try
            {
                string[] values = value.ToDataArray<string>();
                UserEntity user = values[0].ToData<UserEntity>();
                XElement root = XElement.Parse(values[1], LoadOptions.None);
                string filename = values[2];
                this.Organization.Broker.LogEvent(EventType.FileImported, "Imported State Street Price List. File Name: " + filename, user.CurrentUserName, Guid.Empty);
                 outa = GetValidateData(root).ToXmlString();
            }
            catch (Exception ex)
            {
                outa = "Exception : " + ex.Message;
            }

            return outa;
        }



        public string GetData(IBrokerManagedComponent bmc)
        {
            return bmc.GetDataStream(-911, string.Empty);// run default Command From CM Class
        }

        private List<StatestreetPriceMessage> GetValidateData(XElement root)
        {
            List<StatestreetPriceMessage> messages = new List<StatestreetPriceMessage>();



            XElement[] elements = root.XPathSelectElements("//StateStreetPrice").ToArray();

            Organization.Broker.SetWriteStart();
            foreach (var each in elements)
            {
                string InvestmentCode = each.XPathSelectElement("SecurityCode").Value;
                string CompanyName = each.XPathSelectElement("CompanyName").Value;
            
               SecuritiesEntity secuirtyEntity = Organization.Securities.Where(sec => sec.AsxCode == InvestmentCode).FirstOrDefault();

                 if (secuirtyEntity == null)
                {



                    StatestreetPriceMessage message = new StatestreetPriceMessage();
                    message.InvestmentCode = InvestmentCode;
                    message.CompanyName = CompanyName;
                    message.MessageType = ImportMessageType.InvestmentCodeAdded;
                    message.Message += "Investment code" + InvestmentCode + " has been Ignored";
                    messages.Add(message);
                }
             
                
                messages.Add(CreateMessage(each, secuirtyEntity));

            }

           




           



            return messages;
        }



        private StatestreetPriceMessage CreateMessage(XElement price, SecuritiesEntity secuirtyEntity)
        {
            StatestreetPriceMessage message = new StatestreetPriceMessage();


            DateTimeFormatInfo info = new DateTimeFormatInfo();
            info.ShortDatePattern = "dd/MM/yyyy";

            string InvestmentCode = price.XPathSelectElement("SecurityCode").Value;
            string CompanyName = price.XPathSelectElement("CompanyName").Value;
            string PriceDate = price.XPathSelectElement("PriceDate").Value;
            string Price_NAV = price.XPathSelectElement("Price_NAV").Value;
            string Price_PUR = price.XPathSelectElement("Price_PUR").Value;
            string Price_RDM = price.XPathSelectElement("Price_RDM").Value;
            string Currency = price.XPathSelectElement("Currency").Value;
           
           
            DateTime date;
            double utemp;
            try
            {

                message.Status = ImportMessageStatus.SuccessFullyAdded;
                message.MessageType = ImportMessageType.Sucess;
                message.InvestmentCode = InvestmentCode;
                message.CompanyName = CompanyName;
               
                message.PriceDate = PriceDate;
                message.Price_NAV = Price_NAV;
                message.Price_PUR = Price_PUR;
                message.Price_RDM = Price_RDM;
                message.Currency = Currency;
                
               
                
                ASXSecurityEntity asxprice=new ASXSecurityEntity();
                asxprice.ID = Guid.NewGuid();
                if (!DateTime.TryParse(price.XPathSelectElement("PriceDate").Value, info, DateTimeStyles.None, out date))
                {

                    message.Message += "Invalid price date,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                }else
               {
                   asxprice.Date = date;
                   message.PriceDate = date.ToString("dd/MM/yyyy");
               }




                if (!string.IsNullOrEmpty(price.XPathSelectElement("Price_RDM").Value))
                    if (!double.TryParse(price.XPathSelectElement("Price_RDM").Value, NumberStyles.Currency, CultureInfo.CurrentCulture, out utemp))
                    {
                        message.Message += "Invalid Price (RDM),";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                    }else
                   {
                       asxprice.UnitPrice = utemp;
                     //  string format = "$#,##0.00;-$#,##0.00;$0";

                       message.Price_RDM = utemp.ToString();
                  
                   }



                if (!string.IsNullOrEmpty(price.XPathSelectElement("Price_NAV").Value))
                    if (!double.TryParse(price.XPathSelectElement("Price_NAV").Value, NumberStyles.Currency, CultureInfo.CurrentCulture, out utemp))
                    {
                        message.Message += "Invalid Price (NAV),";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                    }
                    else
                    {
                        asxprice.PriceNAV = utemp;
                        //  string format = "$#,##0.00;-$#,##0.00;$0";

                        message.Price_NAV = utemp.ToString();

                    }

                if (!string.IsNullOrEmpty(price.XPathSelectElement("Price_PUR").Value))
                    if (!double.TryParse(price.XPathSelectElement("Price_PUR").Value, NumberStyles.Currency, CultureInfo.CurrentCulture, out utemp))
                    {
                        message.Message += "Invalid Price (PUR),";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                    }
                    else
                    {
                        asxprice.PricePUR = utemp;
                        //  string format = "$#,##0.00;-$#,##0.00;$0";

                        message.Price_PUR = utemp.ToString();

                    }

                if (string.IsNullOrEmpty(price.XPathSelectElement("Currency").Value))
                {
                    message.Message += "Invalid currency,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                }
                else
                {
                    asxprice.Currency = Currency;
                    message.Currency = Currency;
                }





                if (message.MessageType != ImportMessageType.Error)
                {


                    if (secuirtyEntity==null)
                    {
                        message.Message += "Invalid fund code,Ignored.,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;

                    }else
                    {
                    

                    if(secuirtyEntity.ASXSecurity==null)
                        secuirtyEntity.ASXSecurity=new ObservableCollection<ASXSecurityEntity>();

                        


                    if (secuirtyEntity.ASXSecurity != null && secuirtyEntity.ASXSecurity.Count(s => s.Date == date) > 0)
                        {
                            message.Message += "Skipped.Price already exists for date,";
                            message.Status = ImportMessageStatus.AlreayExists;
                            message.MessageType = ImportMessageType.Error;
                        }
                        else
                        {
                        secuirtyEntity.ASXSecurity.Add(asxprice);
                        }


                    }

                }


                if (!string.IsNullOrEmpty(message.Message) && message.Message.Contains(','))
                {
                    message.Message = message.Message.Remove(message.Message.LastIndexOf(','));
                }
                else
                {
                    message.Message = "";
                }

            }
            catch
            {

                message = null;
            }
            return message;
        }

      

       

      
        
    }
}
