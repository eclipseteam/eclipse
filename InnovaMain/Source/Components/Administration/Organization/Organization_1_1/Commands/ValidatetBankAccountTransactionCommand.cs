﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;
using BankAccountEntity = Oritax.TaxSimp.Common.BankAccountEntity;

namespace Oritax.TaxSimp.CM.Organization.Commands
{
    public class ValidateBankAccountTransactionCommand : OrganizationCmCommandBase
    {

        public override string DoAction(string value)
        {
            string outa = string.Empty;
            try
            {
                string[] values = value.ToDataArray<string>();
                UserEntity user = values[0].ToData<UserEntity>();
                XElement root = XElement.Parse(values[1], LoadOptions.None);



             XElement[] elements =root.Elements().ToArray();//XPathSelectElements("//AccountTransactions").ToArray();
                BankAccountEntity bankAccount = null;




                List<BankAccountTransactionMessage> messages = new List<BankAccountTransactionMessage>();
                 foreach (var each in elements)
                 {
                     XAttribute att = each.Attribute("BSBNAccountNumber");


                     bankAccount = null;
                     OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
                     {
                         if (logical.CMType == OrganizationTypeList.GetEntity(OrganizationType.BankAccount))
                         {
                             BankAccountEntity bank = this.GetData(bmc).ToNewOrData<BankAccountEntity>();
                              string bsb = "";
                             if (!string.IsNullOrEmpty(bank.BSB) && bank.BSB.Contains('-'))
                              bsb =bank.BSB.Split('-')[1];
                             string BsBNAccountNumber = bsb + bank.AccountNumber;
                             if(att.Value.Trim()==BsBNAccountNumber)
                             {
                                 bankAccount = bank;
                             }
                             
                         }
                            
                     });

                     if(bankAccount==null)
                     {
                        messages.Add(CreateMissingMessage(att.Value));
                     }
                     XElement[] trans = each.XPathSelectElements("Transaction").ToArray();
                     foreach (var eachchild in trans)
                     {
                         messages.Add(CreateBankValidationMessage(eachchild,att.Value, (bankAccount != null)));
                     }
                     XElement[] holdings = each.XPathSelectElements("Holding").ToArray();
                     foreach (var eachchild in holdings)
                     {
                         messages.Add(CreateHoldingValidationMessage(eachchild, att.Value, (bankAccount != null)));
                     }



                 }






                 outa = messages.ToXmlString(); 
            }
            catch (Exception ex)
            {
                outa = "Exception : " + ex.Message;
            }

            return outa;
        }

        private BankAccountTransactionMessage CreateHoldingValidationMessage(XElement transaction, string BSBNAccountNumber, bool isAccountFound)
        {
            BankAccountTransactionMessage message = new BankAccountTransactionMessage();

            DateTimeFormatInfo info = new DateTimeFormatInfo();
            info.ShortDatePattern = "dd/MM/yyyy";

            string Type = transaction.XPathSelectElement("Type").Value;
            string Amount = transaction.XPathSelectElement("HoldingAmount").Value;
            string DateOfHolding = transaction.XPathSelectElement("DateOfHolding").Value;
            string UnsettledOrder = transaction.XPathSelectElement("UnsettledOrder").Value;
            string TransactionType = transaction.XPathSelectElement("TransactionType").Value;
            string BankwestReferenceNumber = transaction.XPathSelectElement("BankwestReferenceNumber").Value;
            string TransactionDescription = transaction.XPathSelectElement("TransactionDescription").Value;


            DateTime date;
            decimal utemp;
            try
            {

                message.Status = ImportMessageStatus.Sucess;
                message.MessageType = ImportMessageType.Sucess;
                message.BSBNAccountNumber = BSBNAccountNumber;

                message.Type = Type;
                message.InputMode = "Holding";
                message.Amount = Amount;

                message.Date = DateOfHolding;
                message.UnsettledOrder = UnsettledOrder;
                message.TransactionType = TransactionType;
                message.BankwestReferenceNumber = BankwestReferenceNumber;
                message.TransactionDescription = TransactionDescription;
                if (!isAccountFound)
                {

                    message.Message += "Account Number (" + BSBNAccountNumber + ") doesn't exists,";
                    message.Status = ImportMessageStatus.AccountNotFound;
                    message.MessageType = ImportMessageType.Error;

                }





                if (!DateTime.TryParse(DateOfHolding, info, DateTimeStyles.None, out date))
                {

                    message.Message += "Invalid Date Of Holding,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                }
                else
                {
                    message.Date = date.ToString("dd/MM/yyyy");
                }


                if (!string.IsNullOrEmpty(Amount))
                    if (!decimal.TryParse(Amount, NumberStyles.Currency, CultureInfo.CurrentCulture, out utemp))
                    {
                        message.Message += "Invalid Holding Amount,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                    }
                    else
                    {

                        // string format = "$#,##0.00;-$#,##0.00;$0";

                        message.Amount = utemp.ToString();
                    }


                if (string.IsNullOrEmpty(UnsettledOrder))
                    if (!decimal.TryParse(UnsettledOrder, out utemp))
                    {
                        message.Message += "Invalid Unsettled Order,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                    }

                if (!string.IsNullOrEmpty(message.Message) && message.Message.Contains(','))
                {
                    message.Message = message.Message.Remove(message.Message.LastIndexOf(','));
                }
                else
                {
                    message.Message = "";
                }

            }
            catch
            {

                message = null;
            }
            return message;



        }

      

        private BankAccountTransactionMessage CreateBankValidationMessage(XElement transaction,string BSBNAccountNumber, bool isAccountFound)
        {
            BankAccountTransactionMessage message = new BankAccountTransactionMessage();

            DateTimeFormatInfo info = new DateTimeFormatInfo();
            info.ShortDatePattern = "dd/MM/yyyy";

            string Type = transaction.XPathSelectElement("Type").Value;
            string BankwestReferenceNumber = transaction.XPathSelectElement("BankwestReferenceNumber").Value;
            string Amount = transaction.XPathSelectElement("Amount").Value;
            string DateOfTransaction = transaction.XPathSelectElement("DateOfTransaction").Value;
            string TransactionDescription = transaction.XPathSelectElement("TransactionDescription").Value;
            string TransactionType = transaction.XPathSelectElement("TransactionType").Value;


            DateTime date;
            decimal utemp;
            try
            {

                message.Status = ImportMessageStatus.Sucess;
                message.MessageType = ImportMessageType.Sucess;
                message.BSBNAccountNumber = BSBNAccountNumber;
                
                message.Type = Type;
                message.InputMode = "Transaction";
                message.BankwestReferenceNumber = BankwestReferenceNumber;
                message.Amount = Amount;
                message.Date = DateOfTransaction;
                message.TransactionDescription = TransactionDescription;
                message.TransactionType = TransactionType;

                if (!isAccountFound)
                {

                    message.Message += "Account Number (" + BSBNAccountNumber + ") doesn't exists,"; 
                    message.Status = ImportMessageStatus.AccountNotFound;
                    message.MessageType = ImportMessageType.Error;

                }





                if (!DateTime.TryParse(DateOfTransaction, info, DateTimeStyles.None, out date))
                {

                    message.Message += "Invalid Date Of Transaction,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                }
               else
               {
                   message.Date = date.ToString("dd/MM/yyyy");
               }


                if (!string.IsNullOrEmpty(Amount))
                    if (!decimal.TryParse(Amount, NumberStyles.Currency, CultureInfo.CurrentCulture, out utemp))
                    {
                        message.Message += "Invalid Amount,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                    }
                    else
                    {

                        // string format = "$#,##0.00;-$#,##0.00;$0";

                        message.Amount = utemp.ToString();
                    }

          
                
                
                if (!string.IsNullOrEmpty(message.Message) && message.Message.Contains(','))
                {
                    message.Message = message.Message.Remove(message.Message.LastIndexOf(','));
                }
                else
                {
                    message.Message = "";
                }

            }
            catch
            {

                message = null;
            }




            return message;
        }

        private BankAccountTransactionMessage CreateMissingMessage(string BSBNAccountNumber)
        {
            BankAccountTransactionMessage message = new BankAccountTransactionMessage();



            message.Message += "Account Number (" + BSBNAccountNumber + ") doesn't exists";
            message.BSBNAccountNumber = BSBNAccountNumber;
            message.MessageType = ImportMessageType.MissingInvestmentCode;
            //message.Status = ImportMessageStatus.Failed;
         



            return message;

        }



        public string GetData(IBrokerManagedComponent bmc)
        {
            return bmc.GetDataStream(-911, string.Empty);// run default Command From CM Class
        }

       



    }
}
