﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.CM.Organization.Commands
{
    public class ImportBellDirectDataFeedCommand : OrganizationCmCommandBase
    {

        public override string DoAction(string value)
        {
            string outa = string.Empty;
            try
            {
                string[] values = value.ToDataArray<string>();
                UserEntity user = values[0].ToData<UserEntity>();
                XElement root = XElement.Parse(values[1], LoadOptions.None);
                string filename = values[2];
                this.Organization.Broker.LogEvent(EventType.FileImported, "Imported Bell Direct Data Feed. File Name: " + filename, user.CurrentUserName, Guid.Empty);

                List<BellDirectMessages> Messages = new List<BellDirectMessages>();
                XElement[] elements = root.XPathSelectElements("//InvestmentCode").ToArray();


                foreach (var xElement in elements)
                {
                    string InvestmentCode = xElement.Value;


                    SecuritiesEntity secuirtyEntity = Organization.Securities.Where(sec => sec.AsxCode == InvestmentCode).FirstOrDefault();

                    if (secuirtyEntity == null)
                    {
                       // Organization.Broker.SetWriteStart();
                        BellDirectMessages message = new BellDirectMessages();
                        secuirtyEntity = new SecuritiesEntity();
                        secuirtyEntity.AsxCode = InvestmentCode;
                        secuirtyEntity.CompanyName = "";
                        secuirtyEntity.ID = Guid.NewGuid();
                        secuirtyEntity.ASXSecurity = new ObservableCollection<ASXSecurityEntity>();

                        Organization.Securities.Add(secuirtyEntity);
                        message.Message += "Investment code" + InvestmentCode + " has been added ,";
                        message.Status = ImportMessageStatus.SuccessFullyAdded;
                        message.TransactionID = "";
                        message.TransactionDate = "";
                        message.InvestmentCode = InvestmentCode;
                        message.MessageType = ImportMessageType.InvestmentCodeAdded;
                        Messages.Add(message);
                    }

                }

                OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
                {
                    if (logical.CMType == OrganizationTypeList.GetEntity(OrganizationType.DesktopBrokerAccount))
                    {

                        OrganizationUnitCM unit = (bmc as OrganizationUnitCM);
                        
                        bmc.Broker.SetWriteStart();

                        foreach (var msg in unit.ImportTransactions(root))
                        {
                            Messages.Add((BellDirectMessages)msg);
                        }

                        //TODO: We Need to find a central place to calculate token
                        bmc.CalculateToken(true);
                    }
                });

                outa = Messages.ToXmlString();
            }
            catch (Exception ex)
            {
                outa = "Exception : " + ex.Message;
            }

            return outa;
        }


        //SecuritiesEntity secuirtyEntity = orga.Securities.Where(sec => sec.AsxCode == InvestmentCode).FirstOrDefault();
        //   if (!CheckinvestmentCodeExist(InvestmentCode))
        //   {
        //       AddInvestmentCode(InvestmentCode);
        //       message.Message += "Investment code" + InvestmentCode + " has been added ,";
        //   }


        #region BackUp

        //const string DBUserModuleName = "DBUser_1_1";
        //ILogicalModule _OrganizationLM;
        //List<ILogicalModule> _Modules;


        public string DoAction_BackUp(string value)
        {
            string outa = string.Empty;
            return outa;
        }
        
        #endregion





    }
}
