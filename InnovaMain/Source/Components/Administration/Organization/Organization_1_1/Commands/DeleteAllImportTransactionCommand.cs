﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Enum;
using System.Collections.ObjectModel;

namespace Oritax.TaxSimp.CM.Organization.Commands
{
    public class DeleteAllImportTransactionCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string data)
        {
            ObservableCollection<OrganizationUnit> ids = data.ToNewOrData<ObservableCollection<OrganizationUnit>>();
            //List<DividendEntity> ids = data.ToNewOrData<List<DividendEntity>>();
             DividendEntity dividend =null;
            this.Organization.Broker.SetStart();

            //OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
            //            {
            //                if (logical.CMType == OrganizationTypeList.GetEntity(OrganizationType.BankAccount))
            //                {
            //                    (bmc as OrganizationUnitCM).Broker.SetWriteStart();
            //                    (bmc as OrganizationUnitCM).DeleteAllCashTransaction_Bank();
            //                    bmc.CalculateToken(true);
            //                }
            //            });

            //OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
            //{
            //    if (logical.CMType == OrganizationTypeList.GetEntity(OrganizationType.DesktopBrokerAccount))
            //    {
            //        (bmc as OrganizationUnitCM).Broker.SetWriteStart();
            //        (bmc as OrganizationUnitCM).DeleteAllHoldingBalances_DesktopBroker();
            //        (bmc as OrganizationUnitCM).DeleteAllHoldingTransaction_DesktopBroker();
            //        bmc.CalculateToken(true);
            //    }
            //});

            //OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
            //{
            //    if (logical.CMType == OrganizationTypeList.GetEntity(OrganizationType.ManagedInvestmentSchemesAccount))
            //    {
            //        (bmc as OrganizationUnitCM).Broker.SetWriteStart();
            //        (bmc as OrganizationUnitCM).DeleteAllSecurityHolding_ManagedInvestmentSchemes();
            //        (bmc as OrganizationUnitCM).DeleteAllFundTransaction_ManagedInvestmentSchemes();
            //        bmc.CalculateToken(true);
            //    }
            //});


            OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
            {
                if ((bmc is IOrganizationUnit) && (bmc as IOrganizationUnit).IsInvestableClient)
                {
                    OrganizationUnitCM oucm = bmc as OrganizationUnitCM;
                    (bmc as OrganizationUnitCM).Broker.SetStart();
                    if (ids.Where(ee => ee.Clid == oucm.Clid && ee.Csid == oucm.Csid).Count() > 0)
                    {
                        (bmc as OrganizationUnitCM).DeletAllTransaction();
                    }
                    bmc.CalculateToken(true);
                }
            });


            if(dividend!=null)
             return dividend.ToXmlString();

             return "Failed..";
        }
    }
}
