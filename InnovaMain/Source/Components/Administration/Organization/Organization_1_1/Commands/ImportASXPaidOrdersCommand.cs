﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.CM.Organization.Commands
{
    public class ImportASXPaidOrdersCommand : OrganizationCmCommandBase
    {

    public override string DoAction(string value)
        {
        string outa = string.Empty;
        try
            {
            string[] values = value.ToDataArray<string>();
            UserEntity user = values[0].ToData<UserEntity>();
            XElement root = XElement.Parse(values[1], LoadOptions.None);
            string filename = values[2];
            this.Organization.Broker.LogEvent(EventType.FileImported, "Imported ASX Order Paid File. File Name: " + filename, user.CurrentUserName, Guid.Empty);
            List<ASXPaidOrderMessage> messages = new List<ASXPaidOrderMessage>();


            XElement[] elements = root.Elements().ToArray();//XPathSelectElements("//AccountTransactions").ToArray();
            DesktopBrokerAccountEntity bankAccount = null;

            foreach (var each in elements)
                {
                string stock = each.XPathSelectElement("Stock").Value;


                string att = each.XPathSelectElement("BookedACC").Value;


                bankAccount = null;
                OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
                {
                if (logical.CMType == OrganizationTypeList.GetEntity(OrganizationType.DesktopBrokerAccount))
                        {

                        DesktopBrokerAccountEntity bank = this.GetData(bmc).ToNewOrData<DesktopBrokerAccountEntity>();

                        if (att.Trim() == bank.AccountNumber)
                            {
                            bankAccount = bank;
                            }

                        }

                });

                //if (Organization.Securities.Count(sec => sec.AsxCode == stock) == 0)
                //    {
                //    ASXPaidOrderMessage message = new ASXPaidOrderMessage();

                //    message.Message += "Investment code (" + stock + ") doesn't exists";
                //    message.Stock = stock;
                //    message.MessageType = ImportMessageType.MissingInvestmentCode;
                //    //message.Status = ImportMessageStatus.Failed;
                //    messages.Add(message);
                //    }


                if (bankAccount == null)
                    {
                    messages.Add(CreateMissingMessage(att.Trim()));
                    }
                }



            OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
            {
                if (logical.CMType == OrganizationTypeList.GetEntity(OrganizationType.DesktopBrokerAccount))
                    {
                    OrganizationUnitCM unit = (bmc as OrganizationUnitCM);

                    unit.Broker.SetWriteStart();
                    foreach (var msg in unit.ImportPaidOrders(root))
                        {
                        messages.Add((ASXPaidOrderMessage)msg);
                        }
                    bmc.CalculateToken(true);
                    }
            });

           


            outa = messages.ToXmlString();
            }
        catch (Exception ex)
            {
            outa = "Exception : " + ex.Message;
            }

        return outa;
        }

    public string GetData(IBrokerManagedComponent bmc)
        {
        return bmc.GetDataStream(-911, string.Empty);// run default Command From CM Class
        }

    private ASXPaidOrderMessage CreateMissingMessage(string BSBNAccountNumber)
        {
        ASXPaidOrderMessage message = new ASXPaidOrderMessage();



        message.Message += "Account Number (" + BSBNAccountNumber + ") doesn't exists";
        message.BookedACC = BSBNAccountNumber;
        message.MessageType = ImportMessageType.InvestmentCodeAdded;
        //message.Status = ImportMessageStatus.Failed;




        return message;

        }



       

    }
}
