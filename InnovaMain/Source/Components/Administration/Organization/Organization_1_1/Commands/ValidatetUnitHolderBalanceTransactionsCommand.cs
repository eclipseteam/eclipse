﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;

namespace Oritax.TaxSimp.CM.Organization.Commands
{
    public class ValidatetUnitHolderBalanceTransactionsCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string value)
        {
            string outa = string.Empty;
            try
            {
                string[] values = value.ToDataArray<string>();
                UserEntity user = values[0].ToData<UserEntity>();
                XElement root = XElement.Parse(values[1], LoadOptions.None);
                string misName = values[2];

                outa = GetValidateData(root, misName).ToXmlString();
            }
            catch (Exception ex)
            {
                outa = "Exception : " + ex.Message;
            }

            return outa;
        }

        public string GetData(IBrokerManagedComponent bmc)
        {
            return bmc.GetDataStream(-911, string.Empty);// run default Command From CM Class
        }

        private List<UnitHolderBalanceTransactionMessage> GetValidateData(XElement root, string misName)
        {
            List<UnitHolderBalanceTransactionMessage> messages = new List<UnitHolderBalanceTransactionMessage>();
            XElement[] elements = root.XPathSelectElements("//UnitHolderTransaction").ToArray();
            foreach (var each in elements)
            {
                string fundCode = each.XPathSelectElement("FundCode").Value;
                string Account = each.XPathSelectElement("Account").Value;
                OrganizationUnitCM client = null;
                OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
                {
                    if ((bmc is OrganizationUnitCM) && (bmc as OrganizationUnitCM).ClientId != null && (bmc as OrganizationUnitCM).ClientId.Trim() == Account.Trim())
                    {
                        client = (bmc as OrganizationUnitCM);
                    }
                });
                if (client == null)
                {
                    UnitHolderBalanceTransactionMessage message = new UnitHolderBalanceTransactionMessage();
                    message.Message += "Client with id (" + Account + ") doesn't exists";
                    message.Account = Account;
                    message.MessageType = ImportMessageType.Warning;
                    message.Status = ImportMessageStatus.AccountNotFound;
                    messages.Add(message);
                }
                else
                {
                    bool isAttachedWithClient = client.IsMISAttachedWithFund(fundCode, misName);
                    if (!isAttachedWithClient)
                    {
                        UnitHolderBalanceTransactionMessage message = new UnitHolderBalanceTransactionMessage();
                        message.Message += "Fund code (" + fundCode + ") doesn't exists.Or Fund code is not attached with any client";
                        message.FundCode = fundCode;
                        message.MessageType = ImportMessageType.MissingInvestmentCode;
                        message.Status = ImportMessageStatus.Failed;
                        messages.Add(message);
                    }
                    messages.Add(CreateMessage(each, isAttachedWithClient));
                }
            }
            return messages;
        }

        private UnitHolderBalanceTransactionMessage CreateMessage(XElement docElement, bool isAttached)
        {
            UnitHolderBalanceTransactionMessage message = new UnitHolderBalanceTransactionMessage();

            DateTimeFormatInfo info = new DateTimeFormatInfo();
            info.ShortDatePattern = "dd/MM/yyyy";

            string Account = docElement.XPathSelectElement("Account").Value;
            string SubAccount = docElement.XPathSelectElement("SubAccount").Value;
            string FundCode = docElement.XPathSelectElement("FundCode").Value;
            string FundName = docElement.XPathSelectElement("FundName").Value;
            string TotalShares = docElement.XPathSelectElement("TotalShares").Value;
            string RedemptionPrice = docElement.XPathSelectElement("RedemptionPrice").Value;
            string CurrentValue = docElement.XPathSelectElement("CurrentValue").Value;
            string AsofDate = docElement.XPathSelectElement("AsofDate").Value;
            string InvestorCategory = docElement.XPathSelectElement("InvestorCategory").Value;
            DateTime date;
            decimal utemp;
            try
            {
                message.Status = ImportMessageStatus.Sucess;
                message.MessageType = ImportMessageType.Sucess;
                message.Account = Account;
                message.SubAccount = SubAccount;
                message.FundCode = FundCode;
                message.FundName = FundName;
                message.TotalShares = TotalShares;
                message.RedemptionPrice = RedemptionPrice;
                message.CurrentValue = CurrentValue;
                DateTime asOfDate;
                if (DateTime.TryParse(AsofDate, new CultureInfo("en-AU"), DateTimeStyles.None, out asOfDate))
                    message.AsofDate = asOfDate;
                message.InvestorCategory = InvestorCategory;
                if (!isAttached)
                {
                    message.Message += "Fund code (" + FundCode + ") doesn't exists.Or fund code is not attached with any client,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                }
                if (!DateTime.TryParse(AsofDate, info, DateTimeStyles.None, out date))
                {
                    message.Message += "Invalid Price Date,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                }
                else
                {
                    message.AsofDate = date;
                }
                if (!string.IsNullOrEmpty(RedemptionPrice))
                    if (!decimal.TryParse(RedemptionPrice, NumberStyles.Currency, CultureInfo.CurrentCulture, out utemp))
                    {
                        message.Message += "Invalid Unit Price,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                    }
                    else
                    {
                        message.RedemptionPrice = utemp.ToString("C");
                    }

                if (!string.IsNullOrEmpty(CurrentValue))
                    if (!decimal.TryParse(CurrentValue, NumberStyles.Currency, CultureInfo.CurrentCulture, out utemp))
                    {
                        message.Message += "Invalid Amount,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                    }
                    else
                    {
                        message.CurrentValue = utemp.ToString("C");
                    }
                if (!string.IsNullOrEmpty(TotalShares))
                    if (!decimal.TryParse(TotalShares, NumberStyles.Currency, CultureInfo.CurrentCulture, out utemp))
                    {
                        message.Message += "Invalid Shares,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                    }
                    else
                    {
                        message.TotalShares = utemp.ToString();
                    }
                if (!string.IsNullOrEmpty(message.Message) && message.Message.Contains(','))
                {
                    message.Message = message.Message.Remove(message.Message.LastIndexOf(','));
                }
                else
                {
                    message.Message = "";
                }
            }
            catch
            {
                message = null;
            }
            return message;
        }
    }
}
