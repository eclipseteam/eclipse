﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.CM.Organization.Commands
    {
    public class ExportFinSimplicityInvestments : OrganizationCmCommandBase
        {
        
        public override string DoAction(string value)
            {
            StringBuilder csvstring = new StringBuilder();
           

            csvstring.Append("investment_code");
            csvstring.Append(",market_code");
            csvstring.Append(",currency_code");
            csvstring.Append(",investment_type");
            csvstring.Append(",investment_name");
            csvstring.Append(",unit_price");
            //csvstring.Append(",projected_income");
            //csvstring.Append(",projected_growth");
            //csvstring.Append(",isin_code");
            //csvstring.Append(",issuer_code");
            //csvstring.Append(",investment_term");
            //csvstring.Append(",maturity_date");
            //csvstring.Append(",interest_rate");
            //csvstring.Append(",coupons_per_year");
            //csvstring.Append(",interest_type");
            //csvstring.Append(",sector_code");
            //csvstring.Append(",option_type");
            //csvstring.Append(",expiry_price");
            //csvstring.Append(",exercise_type");
            //csvstring.Append(",shares_per_contract");
            //csvstring.Append(",underlying_investment_code");
            csvstring.Append(",asset_class");
            csvstring.Append("\n");
            foreach (var record in CreateExportFinSimplicityObjects())
                {


               
                csvstring.Append( record.InvestmentCode);
                csvstring.Append("," + record.MarketCode);
                csvstring.Append("," + record.CurrencyCode);
                csvstring.Append("," + record.InvestmentType);
                csvstring.Append("," + record.InvestmentName);
                csvstring.Append("," + record.UnitPrice);
              
                //csvstring.Append(",");
                //csvstring.Append(",");
                //csvstring.Append(",");
                //csvstring.Append(",");
                //csvstring.Append(",");
                //csvstring.Append(",");
                //csvstring.Append(",");
                //csvstring.Append(",");
                //csvstring.Append(",");
                //csvstring.Append(",");
                //csvstring.Append(",");
                //csvstring.Append(",");
                //csvstring.Append(",");
                //csvstring.Append(",");
                //csvstring.Append(",");
            //    csvstring.Append("," + record.AssetClass);
                csvstring.Append("," + string.Empty);
                csvstring.Append("\n");

                }


            return csvstring.ToString();
        }



        private List<FinSimplicityInvestment> CreateExportFinSimplicityObjects()
            {
            List<FinSimplicityInvestment> records = new List<FinSimplicityInvestment>();

            foreach (var security in Organization.Securities)
            {
                if (!security.AsxCode.Contains("test"))
                {
                    FinSimplicityInvestment record = new FinSimplicityInvestment();
                    record.InvestmentCode = security.AsxCode;
                    record.MarketCode = (security.InvestmentType.Trim() == "Managed Fund /Unit Trust") ? String.Empty : "ASX";
                    record.CurrencyCode = "AUD";
                    record.InvestmentType = "" + GetInvestmentTypeNumber(security.InvestmentType);
                    record.InvestmentName = security.CompanyName;
                    var asset = Organization.Assets.Where(ss => ss.ID == security.AssetID).FirstOrDefault();

                    if (asset != null)
                        record.AssetClass = asset.Name;
                    else
                        record.AssetClass = string.Empty;
                    
                    if (security.ASXSecurity != null)
                    {

                        var priceEntity = security.ASXSecurity.OrderByDescending(ss => ss.Date).FirstOrDefault();
                        if (priceEntity != null)
                            record.UnitPrice = (decimal?)priceEntity.UnitPrice;
                    }
                    records.Add(record);
                }
              
            }


            return records;
            }

        private int?  GetInvestmentTypeNumber(string type)
        {
            int? result=null;

            Dictionary<string, int> InvestmentTypes = new Dictionary<string, int>{
                                              {"Bank Bill",18} ,
                                              {"Bond",13},
                                              {"Cash",1},
                                              {"Debenture",17} ,
                                              {"Fixed Interest",15} ,
                                              {"Foreign Exchange",1},
                                              {"Index",7},
                                              {"Investment Property",9},
                                              {"Managed Fund /Unit Trust",2},
                                              {"Option",3},
                                              {"Other CGT Assessable",11} ,
                                              {"Other CGT Exempt",12} ,
                                              {"Residential Property",8},
                                              {"Rightslssue",4},
                                              {"Share",0},
                                              {"Term Depose",16} ,
                                              {"Term Deposit",16} ,
                                              {"Warrant",5}
                                          };


            if (InvestmentTypes.ContainsKey(type))
            {
                result = InvestmentTypes[type];
            }




            return result;
        }




        }// end of class

    [Serializable]
    public class FinSimplicityInvestment
        {
        // mandatory fields
       public string InvestmentCode { get; set; }
       public string MarketCode { get; set; }
       public string CurrencyCode { get; set; }
       public string InvestmentType { get; set; }
       public string InvestmentName { get; set; }
       public decimal? UnitPrice { get; set; }

        //optional fields
       public string ProjectedIncome { get; set; }
       public string ProjectedGrowth { get; set; }
       public string IsinCode { get; set; }
       public string IssuerCode { get; set; }
       public string InvestmentTerm { get; set; }
       public string MaturityDate { get; set; }
       public string InterestRate { get; set; }
       public string CouponsPerYear { get; set; }
       public string InterestType { get; set; }
       public string SectorCode { get; set; }
       public string OptionType { get; set; }
       public string ExpiryPrice { get; set; }
       public string ExerciseType { get; set; }
       public string SharesPerContract { get; set; }
       public string UnderlyingInvestmentCode { get; set; }
       public string AssetClass { get; set; }

        public FinSimplicityInvestment()
            {

            






            }



       
        }



    }
