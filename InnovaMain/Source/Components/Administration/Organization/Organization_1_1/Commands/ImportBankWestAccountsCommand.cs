﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;
using BankAccountEntity = Oritax.TaxSimp.Common.BankAccountEntity;

namespace Oritax.TaxSimp.CM.Organization.Commands
{
    public class ImportBankWestAccountsCommand : OrganizationCmCommandBase
    {

        public override string DoAction(string value)
        {
            string outa = string.Empty;
            try
            {
                string[] values = value.ToDataArray<string>();
                UserEntity user = values[0].ToData<UserEntity>();
                XElement root = XElement.Parse(values[1], LoadOptions.None);
                List<BankAccountImportMessage> messages = new List<BankAccountImportMessage>();
                foreach (var each in root.Elements("ClientAccounts"))
                {
                    string applicationId = each.Attribute("ApplicationID").Value;
                   
                    OrganizationUnitCM client = null;
                    OrganizationListAction.ForEach("Administrator", Organization,
                                                   (csid, logical, bmc) =>
                                                   client =
                                                   ((client == null) && (bmc is IXmlData) && ((bmc as OrganizationUnitCM).ApplicationID != null) && (bmc is OrganizationUnitCM) &&
                                                    ((bmc as OrganizationUnitCM).ApplicationID.Trim() == applicationId.Trim()))
                                                       ? bmc as OrganizationUnitCM
                                                       : client);

                    List<OrganizationUnit> units = GetOrganizationUnits(each, user, messages, applicationId, (client != null));
                    AddOrganizationUnits(units);

                    if (client != null)
                    {
                        OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
                        {
                            if (bmc is OrganizationUnitCM && (bmc as OrganizationUnitCM) == client)
                            {

                                client.Broker.SetWriteStart();
                                client.ImportBankwestData(units.ConvertAll<IIdentityCM>(u => u));
                                bmc.CalculateToken(true);
                            }
                        });
                    }
                    else
                    {
                        messages.Add(CreateMissingMessage(applicationId));

                    }
                }
                outa = messages.ToXmlString();
            }
            catch (Exception ex)
            {
                outa = "Exception : " + ex.Message;
            }

            return outa;
        }
        private BankAccountImportMessage CreateMissingMessage(string clientid)
        {
            BankAccountImportMessage message = new BankAccountImportMessage();



            message.Message += "Client with iD (" + clientid + ") doesn't exists";
            message.ClientID = clientid;
            message.MessageType = ImportMessageType.InvestmentCodeAdded;
            //message.Status = ImportMessageStatus.Failed;




            return message;

        }
        #region BackUp

        //const string DBUserModuleName = "DBUser_1_1";
        //ILogicalModule _OrganizationLM;
        //List<ILogicalModule> _Modules;


        public string DoAction_BackUp(string value)
        {
            string outa = string.Empty;
            return outa;
        }
        
        #endregion

        private  List<OrganizationUnit> GetOrganizationUnits(XElement value, UserEntity user, List<BankAccountImportMessage> messages, string clientID, bool HasFoundClient)
        {
            XElement[] elements = value.XPathSelectElements("Account").ToArray();

            List<OrganizationUnit> units = new List<OrganizationUnit>();
            foreach (var each in elements)
            {
                Oritax.TaxSimp.Common.BankAccountEntity entity = new Oritax.TaxSimp.Common.BankAccountEntity();
                entity.Name = each.XPathSelectElement("AccountName").Value;
                entity.AccoutType = each.XPathSelectElement("Type").Value;
                entity.OrganizationStatus = each.XPathSelectElement("OrganizationStatus").Value;
                entity.BSB = each.XPathSelectElement("BSB").Value +"-"+ each.XPathSelectElement("BCH").Value;
                entity.AccountNumber = each.XPathSelectElement("AccountNumber").Value;
                entity.InstitutionID = new Guid(each.XPathSelectElement("InstitutionID").Value);

                BankAccountImportMessage message = new BankAccountImportMessage();
                message.Status = ImportMessageStatus.Sucess;
                message.MessageType = ImportMessageType.Sucess;
                message.AccountName = each.XPathSelectElement("AccountName").Value;
                message.Type = each.XPathSelectElement("Type").Value;
                message.OrganizationStatus = each.XPathSelectElement("OrganizationStatus").Value;
                message.BSB = each.XPathSelectElement("BSB").Value;
                message.BCH = each.XPathSelectElement("BCH").Value;
                message.AccountNumber = each.XPathSelectElement("AccountNumber").Value;
                message.ClientID = clientID;


                if (!HasFoundClient)
                {

                    message.Message += "Client with ID (" + clientID + ") doesn't exists,";
                    message.Status = ImportMessageStatus.AccountNotFound;
                    message.MessageType = ImportMessageType.Error;

                }


                if (string.IsNullOrEmpty(message.AccountName) || message.AccountName.Trim()==string.Empty)
                {
                    message.Message += "Account Name (" + message.AccountName + ") is empty,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                }


                if (string.IsNullOrEmpty(message.AccountNumber) || message.AccountNumber.Trim() == string.Empty)
                {
                    message.Message += "Account Number (" + message.AccountNumber + ") is empty,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                }


                if (string.IsNullOrEmpty(message.BSB) || message.BSB.Trim() == string.Empty)
                {
                    message.Message += "Account BSB (" + message.BSB + ") is empty,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                }

                if (string.IsNullOrEmpty(message.BCH) || message.BCH.Trim() == string.Empty)
                {
                    message.Message += "Account BCH (" + message.BCH + ") is empty,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                }


                if (CheckAccountNumberAndBSBExists(message.AccountNumber, message.BSB + "-" + message.BCH))
                {

                    message.Message += "Account Number (" + message.AccountNumber + ") and BSB  (" + message.BSB + "-" + message.BCH + ") already exists,";
                    message.Status = ImportMessageStatus.AlreayExists;
                    message.MessageType = ImportMessageType.Error;

                }





                if (!string.IsNullOrEmpty(message.Message) && message.Message.Contains(','))
                {
                    message.Message = message.Message.Remove(message.Message.LastIndexOf(','));
                }
                else
                {
                    message.Message = "";
                }

                messages.Add(message);


                if (message.MessageType != ImportMessageType.Error)
                {
                 OrganizationUnit unit = new OrganizationUnit
                {
                    CurrentUser =  user,
                    Name = entity.Name,
                    Type = ((int)OrganizationType.BankAccount).ToString(),
                    Data = entity.ToXmlString()
                };

                units.Add(unit);
                }
            }
            return units;
        }

        private void AddOrganizationUnits(List<OrganizationUnit> units)
        {
            OrganizationAddCommand command = new OrganizationAddCommand();
            command.Organization = this.Organization;
            foreach (var each in units)
            {
                command.AddOrganizationUnit(() => each);
            }
        }

        private bool CheckAccountNumberAndBSBExists(string AccountNumber, string BSB)
        {

            bool result = false;

            OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
            {
                if (logical.CMType == OrganizationTypeList.GetEntity(OrganizationType.BankAccount))
                {



                    BankAccountEntity bank = this.GetData(bmc).ToNewOrData<BankAccountEntity>();




                    if (AccountNumber == bank.AccountNumber && BSB == bank.BSB)
                    {

                        result = true;

                    }




                }

            });


            return result;

        }
        public string GetData(IBrokerManagedComponent bmc)
        {
            return bmc.GetDataStream(-911, string.Empty);// run default Command From CM Class
        }

        
    }
}
