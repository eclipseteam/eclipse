﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Utilities;
using BankAccountEntity = Oritax.TaxSimp.Common.BankAccountEntity;

namespace Oritax.TaxSimp.CM.Organization.Commands
{
    public class ImportBankAccountOrdersCommand : OrganizationCmCommandBase
    {

        public override string DoAction(string value)
        {
            string outa = string.Empty;
            try
            {
                string[] values = value.ToDataArray<string>();
                UserEntity user = values[0].ToData<UserEntity>();
                XElement root = XElement.Parse(values[1], LoadOptions.None);
                string filename = values[2];
                this.Organization.Broker.LogEvent(EventType.FileImported, "Imported Bank Account Order Paid . File Name: " + filename, user.CurrentUserName, Guid.Empty);
                List<BankAccountOrdersMessage> messages = new List<BankAccountOrdersMessage>();
                
                
                 XElement[] elements =root.Elements().ToArray();
                BankAccountEntity bankAccount = null;
                
                foreach (var each in elements)
                {
              
                XElement accs = each.XPathSelectElement("Account");

                    
                    bankAccount = null;
                    OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
                    {
                           if (logical.CMType ==OrganizationTypeList.GetEntity(OrganizationType.BankAccount))
                            {
                                
                                BankAccountEntity bank =this.GetData(bmc).ToNewOrData<BankAccountEntity>();
                               

                                string BsBNAccountNumber = bank.BSB+"-" + bank.AccountNumber;
                                if (accs.Value.Trim() == BsBNAccountNumber)
                                {
                                bankAccount = bank;
                              

                                }
                            }

                     });

                    if (bankAccount == null)
                    {
                    messages.Add(CreateMissingMessage(accs.Value.Trim()));
                    }
                }



                OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
                {
                    if (logical.CMType == OrganizationTypeList.GetEntity(OrganizationType.BankAccount))
                    {
                        OrganizationUnitCM unit = (bmc as OrganizationUnitCM);

                        unit.Broker.SetWriteStart();
                        foreach (var msg in unit.ImportPaidOrders(root))
                        {
                        messages.Add((BankAccountOrdersMessage)msg);
                        }
                        bmc.CalculateToken(true);
                    }
                });

               


                outa = messages.ToXmlString(); 
            }
            catch (Exception ex)
            {
                outa = "Exception : " + ex.Message;
            }

            return outa;
        }

        public string GetData(IBrokerManagedComponent bmc)
        {
            return bmc.GetDataStream(-911, string.Empty);// run default Command From CM Class
        }

        private BankAccountOrdersMessage CreateMissingMessage(string Account)
        {
        BankAccountOrdersMessage message = new BankAccountOrdersMessage();



            message.Message += "Account Number (" + Account + ") doesn't exists";
            
            message.Account = Account;
            message.MessageType = ImportMessageType.InvestmentCodeAdded;
            //message.Status = ImportMessageStatus.Failed;




            return message;

        }




    }
}
