﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.CM.Organization.Commands
{
    public class ImportUnitHolderTransactionsCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string value)
        {
            string outa = string.Empty;
            try
            {
                string[] values = value.ToDataArray<string>();
                UserEntity user = values[0].ToData<UserEntity>();
                XElement root = XElement.Parse(values[1], LoadOptions.None);
                string filename = values[2];
                string misName = values[3];
                this.Organization.Broker.LogEvent(EventType.FileImported, "Imported Unit Holder Balance transaction File. File Name: " + filename, user.CurrentUserName, Guid.Empty);
                outa = GetValidateData(root, misName).ToXmlString();
            }
            catch (Exception ex)
            {
                outa = "Exception : " + ex.Message;
            }
            return outa;
        }

        public string GetData(IBrokerManagedComponent bmc)
        {
            return bmc.GetDataStream(-911, string.Empty);// run default Command From CM Class
        }

        private List<UnitHolderBalanceTransactionMessage> GetValidateData(XElement root, string misName)
        {
            List<UnitHolderBalanceTransactionMessage> messages = new List<UnitHolderBalanceTransactionMessage>();
            XElement[] elements = root.XPathSelectElements("//UnitHolderTransaction").ToArray();
            List<ClientFundsDetails> fundDetails = new List<ClientFundsDetails>();

            foreach (var each in elements)
            {
                string fundCode = each.XPathSelectElement("FundCode").Value;
                string Account = each.XPathSelectElement("Account").Value;
                OrganizationUnitCM client = null;
                OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
                {
                    if ((bmc is OrganizationUnitCM) && (bmc as OrganizationUnitCM).ClientId != null && (bmc as OrganizationUnitCM).ClientId.Trim() == Account.Trim())
                    {
                        client = (bmc as OrganizationUnitCM);
                    }
                });

                if (client == null)
                {
                    UnitHolderBalanceTransactionMessage message = new UnitHolderBalanceTransactionMessage();
                    message.Message += "Client with id (" + Account + ") doesn't exists.Skipped.";
                    message.Account = Account;
                    message.MessageType = ImportMessageType.Warning;
                    message.Status = ImportMessageStatus.AccountNotFound;
                    messages.Add(message);
                }
                else
                {
                    bool isAttachedWithClient = client.IsMISAttachedWithFund(fundCode, misName);
                    if (!isAttachedWithClient)
                    {
                        var message = new UnitHolderBalanceTransactionMessage();
                        message.Message += "Fund code (" + fundCode + ") doesn't exists.Skipped.";
                        message.FundCode = fundCode;
                        message.MessageType = ImportMessageType.MissingInvestmentCode;
                        message.Status = ImportMessageStatus.Failed;
                        messages.Add(message);
                        messages.Add(CreateMessage(each, isAttachedWithClient));
                    }
                    else
                    {
                        var fundDetail = fundDetails.Where(f => f.Client.CLID == client.CLID && f.Client.Csid == client.Csid).FirstOrDefault() ??
                                         new ClientFundsDetails() { Client = client };

                        OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
                           {
                               if (logical.CMType == OrganizationTypeList.GetEntity(OrganizationType.ManagedInvestmentSchemesAccount))
                               {
                                   OrganizationUnitCM unit = (bmc as OrganizationUnitCM);
                                   unit.Broker.SetWriteStart();
                                   if (unit.Name == misName)
                                   {
                                       foreach (var msg in unit.ImportTransactionsUnitHolderBalance(each))
                                       {
                                           //if(msg.MessageType == ImportMessageType.Sucess)
                                           messages.Add((UnitHolderBalanceTransactionMessage)msg);
                                       }
                                   }
                                   bmc.CalculateToken(true);
                               }
                           });
                    }

                }
            }
           // var successMessages = messages.Where(m => m.MessageType == ImportMessageType.Sucess).ToList();
         //   RemoveUnuploadedTransactions(successMessages, misName);

            return messages;
        }

        private void RemoveUnuploadedTransactions(List<UnitHolderBalanceTransactionMessage> messages, string misName)
        {
            List<string> clients = messages.Select(m => m.Account).Distinct().ToList();

            foreach (var clientId in clients)
            {
                OrganizationUnitCM client = null;
                OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
                {
                    if ((bmc is OrganizationUnitCM) && (bmc as OrganizationUnitCM).ClientId != null && (bmc as OrganizationUnitCM).ClientId.Trim() == clientId.Trim())
                        client = (bmc as OrganizationUnitCM);
                });

                if (client == null) continue;

                //OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
                //{
                //    if (logical.CMType == OrganizationTypeList.GetEntity(OrganizationType.ManagedInvestmentSchemesAccount))
                //    {
                //        var unit = (bmc as OrganizationUnitCM);
                //        unit.Broker.SetWriteStart();
                //        if (unit.Name == misName)
                //        {
                var fundCodes = messages.Where(m=>m.Account==client.ClientId).Select(m => m.FundCode).ToList();
              //  client.Broker.SetWriteStart();
                client.SetUnuploadedUnitHoldTransactions(fundCodes, misName);

                //  }
              //  client.CalculateToken(true);
                // }
                // });

            }


        }


        private UnitHolderBalanceTransactionMessage CreateMessage(XElement docElement, bool isAttached)
        {
            UnitHolderBalanceTransactionMessage message = new UnitHolderBalanceTransactionMessage();

            DateTimeFormatInfo info = new DateTimeFormatInfo();
            info.ShortDatePattern = "dd/MM/yyyy";

            string Account = docElement.XPathSelectElement("Account").Value;
            string SubAccount = docElement.XPathSelectElement("SubAccount").Value;
            string FundCode = docElement.XPathSelectElement("FundCode").Value;
            string FundName = docElement.XPathSelectElement("FundName").Value;
            string TotalShares = docElement.XPathSelectElement("TotalShares").Value;
            string RedemptionPrice = docElement.XPathSelectElement("RedemptionPrice").Value;
            string CurrentValue = docElement.XPathSelectElement("CurrentValue").Value;
            string AsofDate = docElement.XPathSelectElement("AsofDate").Value;
            string InvestorCategory = docElement.XPathSelectElement("InvestorCategory").Value;
            DateTime date;
            decimal utemp;
            try
            {
                message.Status = ImportMessageStatus.Sucess;
                message.MessageType = ImportMessageType.Sucess;
                message.Account = Account;
                message.SubAccount = SubAccount;
                message.FundCode = FundCode;
                message.FundName = FundName;
                message.TotalShares = TotalShares;
                message.RedemptionPrice = RedemptionPrice;
                message.CurrentValue = CurrentValue;
                DateTime asOfDate;
                if (DateTime.TryParse(AsofDate, new CultureInfo("en-AU"), DateTimeStyles.None, out asOfDate))
                    message.AsofDate = asOfDate;
                //message.AsofDate = Convert.ToDateTime(AsofDate);
                message.InvestorCategory = InvestorCategory;
                if (!isAttached)
                {
                    message.Message += "Fund code (" + FundCode + ") doesn't exists.Or fund code is not attached with any client,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                }
                if (!DateTime.TryParse(AsofDate, info, DateTimeStyles.None, out date))
                {
                    message.Message += "Invalid Price Date,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                }
                else
                {
                    message.AsofDate = date;
                }
                if (!string.IsNullOrEmpty(RedemptionPrice))
                    if (!decimal.TryParse(RedemptionPrice, NumberStyles.Currency, CultureInfo.CurrentCulture, out utemp))
                    {
                        message.Message += "Invalid Unit Price,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                    }
                    else
                    {
                        message.RedemptionPrice = utemp.ToString("C");
                    }

                if (!string.IsNullOrEmpty(CurrentValue))
                    if (!decimal.TryParse(CurrentValue, NumberStyles.Currency, CultureInfo.CurrentCulture, out utemp))
                    {
                        message.Message += "Invalid Amount,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                    }
                    else
                    {
                        message.CurrentValue = utemp.ToString("C");
                    }
                if (!string.IsNullOrEmpty(TotalShares))
                    if (!decimal.TryParse(TotalShares, NumberStyles.Currency, CultureInfo.CurrentCulture, out utemp))
                    {
                        message.Message += "Invalid Shares,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                    }
                    else
                    {
                        message.TotalShares = utemp.ToString();
                    }
                if (!string.IsNullOrEmpty(message.Message) && message.Message.Contains(','))
                {
                    message.Message = message.Message.Remove(message.Message.LastIndexOf(','));
                }
                else
                {
                    message.Message = "";
                }
            }
            catch
            {
                message = null;
            }
            return message;
        }
    }

    public class ResetUnitHolderFundsCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string value)
        {
            string outa = string.Empty;
            try
            {
                var values = value.ToDataArray<string>();
                var messages =values[0].ToDataArray<UnitHolderBalanceTransactionMessage>();
                var misName = values[1];
                //this.Organization.Broker.LogEvent(EventType.FileImported, "Imported Unit Holder Balance transaction File. File Name: " + filename, user.CurrentUserName);
                //outa = GetValidateData(root, misName).ToXmlString();

                RemoveUnuploadedTransactions(messages.ToList(),misName);
            }
            catch (Exception ex)
            {
                outa = "Exception : " + ex.Message;
            }
            return outa;
        }

        public string GetData(IBrokerManagedComponent bmc)
        {
            return bmc.GetDataStream(-911, string.Empty);// run default Command From CM Class
        }

        private List<UnitHolderBalanceTransactionMessage> GetValidateData(XElement root, string misName)
        {
            List<UnitHolderBalanceTransactionMessage> messages = new List<UnitHolderBalanceTransactionMessage>();
            XElement[] elements = root.XPathSelectElements("//UnitHolderTransaction").ToArray();
            List<ClientFundsDetails> fundDetails = new List<ClientFundsDetails>();

            foreach (var each in elements)
            {
                string fundCode = each.XPathSelectElement("FundCode").Value;
                string Account = each.XPathSelectElement("Account").Value;
                OrganizationUnitCM client = null;
                OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
                {
                    if ((bmc is OrganizationUnitCM) && (bmc as OrganizationUnitCM).ClientId != null && (bmc as OrganizationUnitCM).ClientId.Trim() == Account.Trim())
                    {
                        client = (bmc as OrganizationUnitCM);
                    }
                });

                if (client == null)
                {
                    UnitHolderBalanceTransactionMessage message = new UnitHolderBalanceTransactionMessage();
                    message.Message += "Client with id (" + Account + ") doesn't exists.Skipped.";
                    message.Account = Account;
                    message.MessageType = ImportMessageType.Warning;
                    message.Status = ImportMessageStatus.AccountNotFound;
                    messages.Add(message);
                }
                else
                {
                    bool isAttachedWithClient = client.IsMISAttachedWithFund(fundCode, misName);
                    if (!isAttachedWithClient)
                    {
                        var message = new UnitHolderBalanceTransactionMessage();
                        message.Message += "Fund code (" + fundCode + ") doesn't exists.Skipped.";
                        message.FundCode = fundCode;
                        message.MessageType = ImportMessageType.MissingInvestmentCode;
                        message.Status = ImportMessageStatus.Failed;
                        messages.Add(message);
                        messages.Add(CreateMessage(each, isAttachedWithClient));
                    }
                    else
                    {
                        var fundDetail = fundDetails.Where(f => f.Client.CLID == client.CLID && f.Client.Csid == client.Csid).FirstOrDefault() ??
                                         new ClientFundsDetails() { Client = client };

                        OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
                        {
                            if (logical.CMType == OrganizationTypeList.GetEntity(OrganizationType.ManagedInvestmentSchemesAccount))
                            {
                                OrganizationUnitCM unit = (bmc as OrganizationUnitCM);
                                unit.Broker.SetWriteStart();
                                if (unit.Name == misName)
                                {
                                    foreach (var msg in unit.ImportTransactionsUnitHolderBalance(each))
                                    {
                                        messages.Add((UnitHolderBalanceTransactionMessage)msg);
                                    }
                                }
                                bmc.CalculateToken(true);
                            }
                        });
                    }

                }
            }
         

            return messages;
        }

        private void RemoveUnuploadedTransactions(List<UnitHolderBalanceTransactionMessage> messages, string misName)
        {
            var clients = messages.Select(m => m.Account).Distinct().ToList();

            foreach (var clientId in clients)
            {
                OrganizationUnitCM client = null;
                OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
                {
                    if ((bmc is OrganizationUnitCM) && (bmc as OrganizationUnitCM).ClientId != null && (bmc as OrganizationUnitCM).ClientId.Trim() == clientId.Trim())
                        client = (bmc as OrganizationUnitCM);
                });

                if (client == null) continue;
                var fundCodes = messages.Where(m => m.Account == client.ClientId).Select(m => m.FundCode).ToList();
                client.SetUnuploadedUnitHoldTransactions(fundCodes, misName);
            }


        }


        private UnitHolderBalanceTransactionMessage CreateMessage(XElement docElement, bool isAttached)
        {
            UnitHolderBalanceTransactionMessage message = new UnitHolderBalanceTransactionMessage();

            DateTimeFormatInfo info = new DateTimeFormatInfo();
            info.ShortDatePattern = "dd/MM/yyyy";

            string Account = docElement.XPathSelectElement("Account").Value;
            string SubAccount = docElement.XPathSelectElement("SubAccount").Value;
            string FundCode = docElement.XPathSelectElement("FundCode").Value;
            string FundName = docElement.XPathSelectElement("FundName").Value;
            string TotalShares = docElement.XPathSelectElement("TotalShares").Value;
            string RedemptionPrice = docElement.XPathSelectElement("RedemptionPrice").Value;
            string CurrentValue = docElement.XPathSelectElement("CurrentValue").Value;
            string AsofDate = docElement.XPathSelectElement("AsofDate").Value;
            string InvestorCategory = docElement.XPathSelectElement("InvestorCategory").Value;
            DateTime date;
            decimal utemp;
            try
            {
                message.Status = ImportMessageStatus.Sucess;
                message.MessageType = ImportMessageType.Sucess;
                message.Account = Account;
                message.SubAccount = SubAccount;
                message.FundCode = FundCode;
                message.FundName = FundName;
                message.TotalShares = TotalShares;
                message.RedemptionPrice = RedemptionPrice;
                message.CurrentValue = CurrentValue;
                DateTime asOfDate;
                if (DateTime.TryParse(AsofDate, new CultureInfo("en-AU"), DateTimeStyles.None, out asOfDate))
                    message.AsofDate = asOfDate;
                //message.AsofDate = Convert.ToDateTime(AsofDate);
                message.InvestorCategory = InvestorCategory;
                if (!isAttached)
                {
                    message.Message += "Fund code (" + FundCode + ") doesn't exists.Or fund code is not attached with any client,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                }
                if (!DateTime.TryParse(AsofDate, info, DateTimeStyles.None, out date))
                {
                    message.Message += "Invalid Price Date,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                }
                else
                {
                    message.AsofDate = date;
                }
                if (!string.IsNullOrEmpty(RedemptionPrice))
                    if (!decimal.TryParse(RedemptionPrice, NumberStyles.Currency, CultureInfo.CurrentCulture, out utemp))
                    {
                        message.Message += "Invalid Unit Price,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                    }
                    else
                    {
                        message.RedemptionPrice = utemp.ToString("C");
                    }

                if (!string.IsNullOrEmpty(CurrentValue))
                    if (!decimal.TryParse(CurrentValue, NumberStyles.Currency, CultureInfo.CurrentCulture, out utemp))
                    {
                        message.Message += "Invalid Amount,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                    }
                    else
                    {
                        message.CurrentValue = utemp.ToString("C");
                    }
                if (!string.IsNullOrEmpty(TotalShares))
                    if (!decimal.TryParse(TotalShares, NumberStyles.Currency, CultureInfo.CurrentCulture, out utemp))
                    {
                        message.Message += "Invalid Shares,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                    }
                    else
                    {
                        message.TotalShares = utemp.ToString();
                    }
                if (!string.IsNullOrEmpty(message.Message) && message.Message.Contains(','))
                {
                    message.Message = message.Message.Remove(message.Message.LastIndexOf(','));
                }
                else
                {
                    message.Message = "";
                }
            }
            catch
            {
                message = null;
            }
            return message;
        }
    }





    public class ClientFundsDetails
    {
        public ClientFundsDetails()
        {
            FundHoldings = new List<FundCodeId>();
        }

        public OrganizationUnitCM Client { get; set; }
        public List<FundCodeId> FundHoldings { get; set; }
    }

    public class FundCodeId
    {
        public string Account { get; set; }
        public string FundId { get; set; }
        public string FundCode { get; set; }
    }

}
