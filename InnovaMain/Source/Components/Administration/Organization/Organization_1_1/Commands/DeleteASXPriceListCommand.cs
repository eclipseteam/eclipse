﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.CM.Organization.Commands
{
    public class DeleteASXPriceListCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string value)
        {
            string outa = string.Empty;
            try
            {
                string[] values = value.ToDataArray<string>();
                UserEntity user = values[0].ToData<UserEntity>();
                XElement root = XElement.Parse(values[1], LoadOptions.None);
                string filename = values[2];
                //this.Organization.Broker.LogEvent(EventType.FileImported, "Imported ASX Price List. File Name: " + filename, user.CurrentUserName);
                //outa = GetValidateData(root).ToXmlString();
                DeleteAllASXSecurity_Securities();
            }
            catch (Exception ex)
            {
                outa = "Exception : " + ex.Message;
            }
            return outa;
        }

        private void DeleteAllASXSecurity_Securities()
        {
            Organization.Broker.SetWriteStart();
            foreach (var count in Organization.Securities)
            {
                count.ASXSecurity = new ObservableCollection<ASXSecurityEntity>();
            }
            foreach (var count in Organization.ProductSecurities)
            {
                count.Details = new ObservableCollection<ProductSecurityDetail>();
            }
            Organization.CalculateToken(true);
        }


    }
}
