﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.CM.Organization.Commands
{
    public class ImportASXPriceListCommand : OrganizationCmCommandBase
    {

        public override string DoAction(string value)
        {
            string outa = string.Empty;
            try
            {
                string[] values = value.ToDataArray<string>();
                UserEntity user = values[0].ToData<UserEntity>();
                XElement root = XElement.Parse(values[1], LoadOptions.None);
                string filename = values[2];
                this.Organization.Broker.LogEvent(EventType.FileImported, "Imported ASX Price List. File Name: " + filename, user.CurrentUserName, Guid.Empty);
                outa = GetValidateData(root).ToXmlString();
            }
            catch (Exception ex)
            {
                outa = "Exception : " + ex.Message;
            }

            return outa;
        }



        public string GetData(IBrokerManagedComponent bmc)
        {
            return bmc.GetDataStream(-911, string.Empty);// run default Command From CM Class
        }

        private List<ASXPriceMessage> GetValidateData(XElement root)
        {
            List<ASXPriceMessage> messages = new List<ASXPriceMessage>();

            XElement[] elements = root.XPathSelectElements("//AsxPrice").ToArray();

            Organization.Broker.SetWriteStart();
            foreach (var each in elements)
            {
                string InvestmentCode = each.XPathSelectElement("InvestmentCode").Value;
                string CompanyName = each.XPathSelectElement("CompanyName").Value;

                SecuritiesEntity secuirtyEntity = Organization.Securities.Where(sec => sec.AsxCode == InvestmentCode).FirstOrDefault();

                if (secuirtyEntity == null)
                {
                    secuirtyEntity = new SecuritiesEntity
                                         {
                                             AsxCode = InvestmentCode,
                                             CompanyName = CompanyName,
                                             ID = Guid.NewGuid(),
                                             ASXSecurity = new ObservableCollection<ASXSecurityEntity>()
                                         };

                    Organization.Securities.Add(secuirtyEntity);

                    ASXPriceMessage message = new ASXPriceMessage();
                    message.InvestmentCode = InvestmentCode;
                    message.CompanyName = CompanyName;
                    message.MessageType = ImportMessageType.InvestmentCodeAdded;
                    message.Message += "Investment code" + InvestmentCode + " has been added";
                    messages.Add(message);
                }
                Organization.CalculateToken(true);

                messages.Add(CreateMessage(each, secuirtyEntity));

            }

            return messages;
        }



        private ASXPriceMessage CreateMessage(XElement asxPrice, SecuritiesEntity secuirtyEntity)
        {
            ASXPriceMessage message = new ASXPriceMessage();
            DateTimeFormatInfo info = new DateTimeFormatInfo();
            info.ShortDatePattern = "dd/MM/yyyy";

            string InvestmentCode = asxPrice.XPathSelectElement("InvestmentCode").Value;
            string CompanyName = asxPrice.XPathSelectElement("CompanyName").Value;
            string Price = asxPrice.XPathSelectElement("Price").Value;
            string PriceDate = asxPrice.XPathSelectElement("PriceDate").Value;


            string currency = asxPrice.XPathSelectElement("Currency").Value;
            DateTime date;
            double utemp;
            try
            {
                message.Status = ImportMessageStatus.SuccessFullyAdded;
                message.InvestmentCode = InvestmentCode;
                message.CompanyName = CompanyName;
                message.Price = Price;
                message.PriceDate = PriceDate;
                message.Currency = currency;

                
               
                
                ASXSecurityEntity asxprice=new ASXSecurityEntity();
                asxprice.ID = Guid.NewGuid();
                if (!DateTime.TryParse(asxPrice.XPathSelectElement("PriceDate").Value, info, DateTimeStyles.None, out date))
                {

                    message.Message += "Invalid Price Date,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                }
                else
                {
                    asxprice.Date = date;
                    message.PriceDate = date.ToString("dd/MM/yyyy");
                }

               if (string.IsNullOrEmpty(asxPrice.XPathSelectElement("Currency").Value))
                {

                    message.Message += "Invalid Currency,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;

                }else
               {
                   asxprice.Currency = currency;
                   message.Currency = currency;


               }






                if (!string.IsNullOrEmpty(asxPrice.XPathSelectElement("Price").Value))
                    if (!double.TryParse(asxPrice.XPathSelectElement("Price").Value, NumberStyles.Currency, CultureInfo.CurrentCulture, out utemp))
                    {
                        message.Message += "Invalid Price,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                    }
                    else
                    {
                        asxprice.UnitPrice = utemp;
                        //  string format = "$#,##0.00;-$#,##0.00;$0";

                        message.Price = utemp.ToString("C");

                    }

                if (message.MessageType != ImportMessageType.Error)
                {
                    if (secuirtyEntity.ASXSecurity == null)
                        secuirtyEntity.ASXSecurity = new ObservableCollection<ASXSecurityEntity>();

                    if (secuirtyEntity.ASXSecurity != null && secuirtyEntity.ASXSecurity.Count(s => s.Date == date) > 0)
                    {
                        message.Message += "Skipped.Aleady Exists,";
                        message.Status = ImportMessageStatus.AlreayExists;
                    }
                    else
                    {
                        secuirtyEntity.ASXSecurity.Add(asxprice);
                    }

                }

                if (!string.IsNullOrEmpty(message.Message) && message.Message.Contains(','))
                {
                    message.Message = message.Message.Remove(message.Message.LastIndexOf(','));
                }
                else
                {
                    message.Message = "";
                }
            }
            catch
            {

                message = null;
            }
            return message;
        }

    }
}
