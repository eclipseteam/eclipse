﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.CM.Organization.Commands
{





    public class ImportTDPriceListCommand : OrganizationCmCommandBase
    {
      

        public override string DoAction(string value)
        {
            string outa = string.Empty;
            try
            {
            string[] values = value.ToDataArray<string>();
            UserEntity user = values[0].ToData<UserEntity>();
            XElement root = XElement.Parse(values[1], LoadOptions.None);
            Guid instituteId = new Guid(values[2]);
            string filename = values[3];

            this.Organization.Broker.LogEvent(EventType.FileImported, "Imported TD Price List. File Name: " + filename, user.CurrentUserName, Guid.Empty);
                outa = GetValidateData(root, instituteId).ToXmlString();
            }
            catch (Exception ex)
            {
                outa = "Exception : " + ex.Message;
            }

            return outa;
        }



        public string GetData(IBrokerManagedComponent bmc)
        {
            return bmc.GetDataStream(-911, string.Empty);// run default Command From CM Class
        }

        private List<TDPriceListMessage> GetValidateData(XElement root, Guid instituteId)
        {
        List<TDPriceListMessage> messages = new List<TDPriceListMessage>();

        var inst = Organization.Institution.Where(ps => ps.ID == instituteId).FirstOrDefault();
            XElement[] elements = root.XPathSelectElements("//TDPrice").ToArray();

            Organization.Broker.SetWriteStart();
            foreach (var each in elements)
            {
            string provider = each.XPathSelectElement("Provider").Value;

            string providerType = each.XPathSelectElement("ProviderType").Value;
            string SPLongTermRating = each.XPathSelectElement("SPLongTermRating").Value;
            var childInstitute = Organization.Institution.Where(sec => sec.Name == provider).FirstOrDefault();

                 if (childInstitute == null)
                {
                    childInstitute = new InstitutionEntity()
                    {
                        Name = provider,
                       // CompanyName = CompanyName,
                        ID = Guid.NewGuid(),
                        Type = (!string.IsNullOrEmpty(providerType) && InstituteCombos.Types.ContainsKey(providerType)) ? InstituteCombos.Types[providerType] : new int(),
                        SPCreditRatingLong = (!string.IsNullOrEmpty(SPLongTermRating)&&InstituteCombos.SPLongTermRatings.ContainsKey(SPLongTermRating))  ? InstituteCombos.SPLongTermRatings[SPLongTermRating] : new int(),
                        
                    };

                    Organization.Institution.Add(childInstitute);

                    TDPriceListMessage message = new TDPriceListMessage();
                    message.Provider = provider;
                    message.MessageType = ImportMessageType.InvestmentCodeAdded;
                    message.Message += "Institute (Provider)" + provider + " has been added";
                    messages.Add(message);
                }
                 messages.Add(CreateMessage(each, inst , childInstitute.ID));

                
            }

           
           
           

            


          

            return messages;
        }



        private TDPriceListMessage CreateMessage(XElement element, InstitutionEntity mainInstitution,Guid instID)
        {

        TDInstitutePriceEntity price = new TDInstitutePriceEntity();
        price.InstituteID = instID;
    

        TDPriceListMessage message = new TDPriceListMessage();


        DateTimeFormatInfo info = new DateTimeFormatInfo();
        info.ShortDatePattern = "dd/MM/yyyy";

        string provider = element.XPathSelectElement("Provider").Value;
        string providerType = element.XPathSelectElement("ProviderType").Value;
        string SPLongTermRating = element.XPathSelectElement("SPLongTermRating").Value;
        string min = element.XPathSelectElement("Min").Value;
        string max = element.XPathSelectElement("Max").Value;
        string maximumBrokeage = element.XPathSelectElement("MaximumBrokeage").Value;
        string days30 = element.XPathSelectElement("Days30").Value;
        string days60 = element.XPathSelectElement("Days60").Value;
        string days90 = element.XPathSelectElement("Days90").Value;
        string days120 = element.XPathSelectElement("Days120").Value;
        string days150 = element.XPathSelectElement("Days150").Value;
        string days180 = element.XPathSelectElement("Days180").Value;
        string days270 = element.XPathSelectElement("Days270").Value;
        string years1 = element.XPathSelectElement("Years1").Value;
        string years2 = element.XPathSelectElement("Years2").Value;
        string years3 = element.XPathSelectElement("Years3").Value;
        string years4 = element.XPathSelectElement("Years4").Value;
        string years5 = element.XPathSelectElement("Years5").Value;
        string pricedate = element.XPathSelectElement("Date").Value;
        string status = element.XPathSelectElement("Status").Value;
        DateTime Pdate;

            decimal utemp;
            try
            {

                message.Status = ImportMessageStatus.Sucess;
                message.MessageType = ImportMessageType.Sucess;
                message.Provider = provider;
                message.ProviderType = providerType;
                message.SPLongTermRating = SPLongTermRating;
                message.Min = min;
                message.Max = max;
                message.MaximumBrokeage = maximumBrokeage;
                message.Days30 = days30;
                message.Days60 = days60;
                message.Days90 = days90;
                message.Days120 = days120;
                message.Days150 = days150;
                message.Days180 = days180;
                message.Days270 = days270;
                message.Years1 = years1;
                message.Years2 = years2;
                message.Years3 = years3;
                message.Years4 = years4;
                message.Years5 = years5;

                price.Status = status;


                if (string.IsNullOrEmpty(SPLongTermRating))
                    {
                    message.Message += "Invalid SP Long Term Rating,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                    }
                if (string.IsNullOrEmpty(providerType))
                    {
                    message.Message += "Invalid Provider Type,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                    }




                if (!string.IsNullOrEmpty(min))
                    if (!decimal.TryParse(min, out utemp))
                        {
                        message.Message += "Invalid min price,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                        }
                    else
                        {

                        message.Min = utemp.ToString();
                        price.Min = utemp;
                        }


                if (!string.IsNullOrEmpty(max))
                    if (!decimal.TryParse(max, out utemp))
                        {
                        message.Message += "Invalid max price,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                        }
                    else
                        {
                        message.Max = utemp.ToString();
                        price.Max = utemp;
                        }



                if (!string.IsNullOrEmpty(maximumBrokeage))
                    if (!decimal.TryParse(maximumBrokeage, out utemp))
                        {
                        message.Message += "Invalid maximumBrokeage price,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                        }
                    else
                        {

                        message.MaximumBrokeage = utemp.ToString();
                        price.MaximumBrokeage = utemp;
                        }


                if (!string.IsNullOrEmpty(days30))
                    if (!decimal.TryParse(days30, out utemp))
                        {
                        message.Message += "Invalid 30-days price,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                        }
                    else
                        {

                        message.Days30 = utemp.ToString();
                        price.Days30 = utemp;
                        }
                if (!string.IsNullOrEmpty(days60))
                    if (!decimal.TryParse(days60, out utemp))
                        {
                        message.Message += "Invalid 60-days price,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                        }
                    else
                        {

                        message.Days60 = utemp.ToString();
                        price.Days60 = utemp;
                        }

                if (!string.IsNullOrEmpty(days90))
                    if (!decimal.TryParse(days90, out utemp))
                        {
                        message.Message += "Invalid 90-days price,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                        }
                    else
                        {

                        message.Days90 = utemp.ToString();
                        price.Days90 = utemp;
                        }


                if (!string.IsNullOrEmpty(days120))
                    if (!decimal.TryParse(days120, out utemp))
                        {
                        message.Message += "Invalid 120-days price,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                        }
                    else
                        {

                        message.Days120 = utemp.ToString();
                        price.Days120 = utemp;
                        }

                if (!string.IsNullOrEmpty(days150))
                    if (!decimal.TryParse(days150, out utemp))
                        {
                        message.Message += "Invalid 150-days price,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                        }
                    else
                        {

                        message.Days150 = utemp.ToString();
                        price.Days150 = utemp;
                        }


                if (!string.IsNullOrEmpty(days180))
                    if (!decimal.TryParse(days180, out utemp))
                        {
                        message.Message += "Invalid 180-days price,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                        }
                    else
                        {

                        message.Days180 = utemp.ToString();
                        price.Days180 = utemp;
                        }





                if (!string.IsNullOrEmpty(days270))
                    if (!decimal.TryParse(days270, out utemp))
                        {
                        message.Message += "Invalid 270-days price,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                        }
                    else
                        {

                        message.Days270 = utemp.ToString();
                        price.Days270 = utemp;
                        }


                if (!string.IsNullOrEmpty(years1))
                    if (!decimal.TryParse(years1, out utemp))
                        {
                        message.Message += "Invalid 1-Year price,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                        }
                    else
                        {

                        message.Years1 = utemp.ToString();
                        price.Years1 = utemp;
                        }




                if (!string.IsNullOrEmpty(years2))
                    if (!decimal.TryParse(years2, out utemp))
                        {
                        message.Message += "Invalid 2-Year price,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                        }
                    else
                        {

                        message.Years2 = utemp.ToString();
                        price.Years2 = utemp;
                        }


                if (!string.IsNullOrEmpty(years3))
                    if (!decimal.TryParse(years3, out utemp))
                        {
                        message.Message += "Invalid 3-Year price,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                        }
                    else
                        {

                        message.Years3 = utemp.ToString();
                        price.Years3 = utemp;
                        }


                if (!string.IsNullOrEmpty(years4))
                    if (!decimal.TryParse(years4, out utemp))
                        {
                        message.Message += "Invalid 4-Year price,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                        }
                    else
                        {

                        message.Years4 = utemp.ToString();
                        price.Years4 = utemp;
                        }


                if (!string.IsNullOrEmpty(years5))
                    if (!decimal.TryParse(years5, out utemp))
                        {
                        message.Message += "Invalid 5-Year price,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                        }
                    else
                        {

                        message.Years5 = utemp.ToString();
                        price.Years5 = utemp;
                        }
                if (!DateTime.TryParseExact(pricedate, "dd/MM/yyyy", info, DateTimeStyles.None, out Pdate))
                    {

                    message.Message += "Invalid Price Date,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                    }
                else
                    {
                    message.Date = Pdate.ToString("dd/MM/yyyy");
                    price.Date = Pdate;
                    }
            



             




               




                if (message.MessageType != ImportMessageType.Error)
                {



                

                if (mainInstitution == null)
                    {
                    message.Message += "Main Institution not found,";
                    message.Status = ImportMessageStatus.AlreayExists;
                    message.MessageType = ImportMessageType.Error;
                    }

                var ins = Organization.Institution.Where(sec => sec.ID == instID).FirstOrDefault();
                if (ins == null)
                    {
                    message.Message += "Invalid institution(Provider),";
                    message.Status = ImportMessageStatus.Failed;
                   

                    }
                else
                    {


                    if (mainInstitution != null)
                        {
                            if(mainInstitution.TDPrices == null)
                             mainInstitution.TDPrices = new ObservableCollection<TDInstitutePriceEntity>();

                            //var oldPrices =mainInstitution.TDPrices.Where(det => det.InstituteID == ins.ID && det.Date == Pdate);

                            //if (oldPrices!=null && oldPrices.Count() > 0)
                            //{

                            // for (int i = oldPrices.Count() - 1; i >= 0;i-- )
                            //    {
                            //    mainInstitution.TDPrices.Remove(oldPrices.ElementAt(i));
                            //    }
                            //     message.Message += "TD Price updated,";
                            // }
                        
                        mainInstitution.TDPrices.Add(price);



                        }

                    }


                       




                }


                if (!string.IsNullOrEmpty(message.Message) && message.Message.Contains(','))
                {
                    message.Message = message.Message.Remove(message.Message.LastIndexOf(','));
                }
                else
                {
                    message.Message = "";
                }

            }
            catch
            {

                message = null;
            }
            return message;
        }

      

       

      
        
    }
}
