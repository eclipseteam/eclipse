﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;

namespace Oritax.TaxSimp.CM.Organization.Commands
    {





    public class ValidateASXPaidOrdersCommand : OrganizationCmCommandBase
        {


        public override string DoAction(string value)
            {
            string outa = string.Empty;
            try
                {
                string[] values = value.ToDataArray<string>();
                UserEntity user = values[0].ToData<UserEntity>();
                XElement root = XElement.Parse(values[1], LoadOptions.None);


                outa = GetValidateData(root).ToXmlString();
                }
            catch (Exception ex)
                {
                outa = "Exception : " + ex.Message;
                }

            return outa;
            }



        public string GetData(IBrokerManagedComponent bmc)
            {
            return bmc.GetDataStream(-911, string.Empty);// run default Command From CM Class
            }

        private List<ASXPaidOrderMessage> GetValidateData(XElement root)
            {
            List<ASXPaidOrderMessage> messages = new List<ASXPaidOrderMessage>();


            XElement[] elements = root.XPathSelectElements("//PaidOrder").ToArray();

            foreach (var each in elements)
                {
                string InvestmentCode = each.XPathSelectElement("Stock").Value;


                string att = each.XPathSelectElement("BookedACC").Value;


                DesktopBrokerAccountEntity bankAccount = null;
               
                OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
                {
                    if (logical.CMType == OrganizationTypeList.GetEntity(OrganizationType.DesktopBrokerAccount))
                        {
                        DesktopBrokerAccountEntity bank = this.GetData(bmc).ToNewOrData<DesktopBrokerAccountEntity>();

                        if (att.Trim() == bank.AccountNumber)
                            {
                            bankAccount = bank;
                            }

                        }

                });



                if (Organization.Securities.Count(sec => sec.AsxCode == InvestmentCode) == 0)
                    {
                    ASXPaidOrderMessage message = new ASXPaidOrderMessage();

                    message.Message += "Investment code (" + InvestmentCode + ") doesn't exists";
                    message.Stock = InvestmentCode;
                    message.MessageType = ImportMessageType.MissingInvestmentCode;
                    //message.Status = ImportMessageStatus.Failed;
                    messages.Add(message);
                    }


                messages.Add(CreateMessage(each, (bankAccount != null)));



                }



          
               

                
                
           



            

            return messages;
            }



        private ASXPaidOrderMessage CreateMessage(XElement asxPrice, bool isAccountFound)
            {
            ASXPaidOrderMessage message = new ASXPaidOrderMessage();


            DateTimeFormatInfo info = new DateTimeFormatInfo();
            info.ShortDatePattern = "dd/MM/yyyy";

            string ParentAcc = asxPrice.XPathSelectElement("ParentAcc").Value;
            string BookedACC = asxPrice.XPathSelectElement("BookedACC").Value;
            string BuySale = asxPrice.XPathSelectElement("BuySale").Value;
            string QTY = asxPrice.XPathSelectElement("QTY").Value;
            string Stock = asxPrice.XPathSelectElement("Stock").Value;
            string PriceAVG = asxPrice.XPathSelectElement("PriceAVG").Value;
            string TradeDate = asxPrice.XPathSelectElement("TradeDate").Value;
            string ClientAccountName = asxPrice.XPathSelectElement("ClientAccountName").Value;

            


            DateTime date;
            decimal utemp;
            int itemp;
            try
                {

                message.Status = ImportMessageStatus.Sucess;
                message.MessageType = ImportMessageType.Sucess;

                message.ParentAcc = ParentAcc;
                message.BookedACC = BookedACC;
                message.BuySale = BuySale;
                message.QTY = QTY;
                message.Stock = Stock;
                message.PriceAVG = PriceAVG;
                message.TradeDate = TradeDate;
                message.ClientAccountName = ClientAccountName;



              




                if (!DateTime.TryParse(TradeDate, info, DateTimeStyles.None, out date))
                    {

                    message.Message += "Invalid Trade Date,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                    }
                else
                    {
                    message.TradeDate = date.ToString("dd/MM/yyyy");
                    }



                if (!string.IsNullOrEmpty(PriceAVG))
                    if (!decimal.TryParse(PriceAVG, NumberStyles.Currency, CultureInfo.CurrentCulture, out utemp))
                        {
                        message.Message += "Invalid price,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                        }
                    else
                        {

                        // string format = "$#,##0.00;-$#,##0.00;$0";

                        message.PriceAVG = utemp.ToString();
                        }


                if (!string.IsNullOrEmpty(QTY))
                    if (!int.TryParse(QTY, NumberStyles.Integer, CultureInfo.CurrentCulture, out itemp))
                        {
                        message.Message += "Invalid Quantity,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                        }
                    else
                        {

                        // string format = "$#,##0.00;-$#,##0.00;$0";

                        message.QTY = itemp.ToString();
                        }



                var security = Organization.Securities.Where(sec => sec.AsxCode == Stock).FirstOrDefault();


                if (security == null)
                    {
                    message.Message += "Invalid investment code,";
                    message.Status = ImportMessageStatus.Failed;
                    }

                if (!isAccountFound)
                    {

                    message.Message += "Account Number (" + BookedACC + ") doesn't exists,";
                    message.Status = ImportMessageStatus.AccountNotFound;
                    message.MessageType = ImportMessageType.Error;

                    }

                if (!string.IsNullOrEmpty(message.Message) && message.Message.Contains(','))
                    {
                    message.Message = message.Message.Remove(message.Message.LastIndexOf(','));
                    }
                else
                    {
                    message.Message = "";
                    }

                }
            catch
                {

                message = null;
                }
            return message;
            }







        }
    }
