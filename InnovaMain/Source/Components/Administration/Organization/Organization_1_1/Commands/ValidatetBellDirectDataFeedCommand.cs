﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;

namespace Oritax.TaxSimp.CM.Organization.Commands
{





    public class ValidatetBellDirectDataFeedCommand : OrganizationCmCommandBase
    {


        public override string DoAction(string value)
        {
            string outa = string.Empty;
            try
            {
                string[] values = value.ToDataArray<string>();
                UserEntity user = values[0].ToData<UserEntity>();
                XElement root = XElement.Parse(values[1], LoadOptions.None);


                List<DesktopBrokerAccountEntity> asxAccounts = new List<DesktopBrokerAccountEntity>();

                OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
                {
                    if (logical.CMType == OrganizationTypeList.GetEntity(OrganizationType.DesktopBrokerAccount))
                    {


                        asxAccounts.Add(GetData(bmc).ToData<DesktopBrokerAccountEntity>());

                    }
                });


                outa = GetValidateData(root, asxAccounts).ToXmlString();
            }
            catch (Exception ex)
            {
                outa = "Exception : " + ex.Message;
            }

            return outa;
        }



        public string GetData(IBrokerManagedComponent bmc)
        {
            return bmc.GetDataStream(-911, string.Empty);// run default Command From CM Class
        }

        private List<BellDirectMessages> GetValidateData(XElement root, List<DesktopBrokerAccountEntity> asxAccounts)
        {
            List<BellDirectMessages> messages = new List<BellDirectMessages>();



            XElement[] elements = root.XPathSelectElements("//Accounts//Account").ToArray();

            foreach (var each in elements)
            {
                string accountNumber = each.XPathSelectElement("Id").Value;
                //bool isValidAccountNumber = (asxAccounts.Count(acc => acc.AccountNumber == accountNumber) > 0);
                bool isValidAccountNumber = (asxAccounts.Find(acc => acc.AccountNumber.Equals(accountNumber)) != null ? true : false);
                XElement[] holdes = each.XPathSelectElements("InvestmentHoldings//InvestmentHolding").ToArray();
                foreach (var xElement in holdes)
                {
                    GetMessageData(xElement, messages, isValidAccountNumber, accountNumber);
                }
            }



            return messages;
        }
        private void GetMessageData(XElement InvestmentHolding, List<BellDirectMessages> messages, bool isAccountValid, string accountnumber)
        {

            string InvestmentCode = InvestmentHolding.XPathSelectElement("InvestmentCode").Value;



            if (Organization.Securities.Count(sec => sec.AsxCode == InvestmentCode) == 0)
            {
                BellDirectMessages message = new BellDirectMessages();
                message.InvestmentCode = InvestmentCode;
                message.Message += "Investment code doesn't exists,";
                message.MessageType = ImportMessageType.MissingInvestmentCode;
                messages.Add(message);

            }


            XElement[] elements = InvestmentHolding.XPathSelectElements("HoldingTransactions//Transactions//Transaction").ToArray();


            foreach (var xElement in elements)
            {

                BellDirectMessages trans = CreateMessage(xElement, InvestmentCode, isAccountValid, accountnumber);
                if (trans != null)
                    messages.Add(trans);


            }


        }


        private BellDirectMessages CreateMessage(XElement transaction, string InvestmentCode, bool isAccountValid, string accountNumber)
        {
            BellDirectMessages message = new BellDirectMessages();


            DateTimeFormatInfo info = new DateTimeFormatInfo();
            info.ShortDatePattern = "dd/MM/yyyy";
            DateTime date;
            decimal utemp;
            try
            {

                message.Status = ImportMessageStatus.Sucess;
                message.MessageType = ImportMessageType.Sucess;
                message.InvestmentCode = InvestmentCode;
                message.TransactionID = transaction.XPathSelectElement("Id").Value;
                message.TransactionDate = transaction.XPathSelectElement("TradeDate").Value;
                message.AccountNumber = accountNumber;


                if (Organization.Securities.Count(sec => sec.AsxCode == InvestmentCode) == 0)
                {
                    message.Message += "Investment code doesn't exists,";
                    message.Status = ImportMessageStatus.Failed;
                    // message.MessageType = ImportMessageType.Warning;

                }

                if (!isAccountValid)
                {
                    message.Message += "Account number doesn't exists,";
                    message.Status = ImportMessageStatus.AccountNotFound;
                    message.MessageType = ImportMessageType.Error;
                }




                if (!DateTime.TryParse(transaction.XPathSelectElement("TradeDate").Value, info, DateTimeStyles.None, out date))
                {

                    message.Message += "Invalid transaction date,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                }
                else
                {
                    message.TransactionDate = date.ToString("dd/MM/yyyy");
                }



                if (!string.IsNullOrEmpty(transaction.XPathSelectElement("Units").Value))
                    if (!decimal.TryParse(transaction.XPathSelectElement("Units").Value, out utemp))
                    {
                        message.Message += "Invalid units,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                    }

                if (!string.IsNullOrEmpty(transaction.XPathSelectElement("GrossValue").Value))
                    if (!decimal.TryParse(transaction.XPathSelectElement("GrossValue").Value, out utemp))
                    {
                        message.Message += "Invalid gross value,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                    }

                if (!string.IsNullOrEmpty(transaction.XPathSelectElement("NetValue").Value))
                    if (!decimal.TryParse(transaction.XPathSelectElement("NetValue").Value, out utemp))
                    {
                        message.Message += "Invalid net value,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                    }

                if (!string.IsNullOrEmpty(transaction.XPathSelectElement("CostBase").Value))
                    if (!decimal.TryParse(transaction.XPathSelectElement("CostBase").Value, out utemp))
                    {
                        message.Message += "Invalid cost base,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                    }




                if (!DateTime.TryParse(transaction.XPathSelectElement("SettlementDate").Value, info, DateTimeStyles.None, out date))
                    if (!string.IsNullOrEmpty(transaction.XPathSelectElement("SettlementDate").Value))
                    {

                        message.Message += "Invalid settlement date,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                    }



                if (!string.IsNullOrEmpty(transaction.XPathSelectElement("Fees//Total").Value))
                    if (!decimal.TryParse(transaction.XPathSelectElement("Fees//Total").Value, out utemp))
                    {
                        message.Message += "Invalid fees/total,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;

                    }

                if (!string.IsNullOrEmpty(transaction.XPathSelectElement("Brokerage//Amount").Value))
                    if (!decimal.TryParse(transaction.XPathSelectElement("Brokerage//Amount").Value, out utemp))
                    {
                        message.Message += "Invalid brokerage amount,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                    }

                if (!string.IsNullOrEmpty(transaction.XPathSelectElement("Brokerage//GSTOnBrokerage").Value))
                    if (!decimal.TryParse(transaction.XPathSelectElement("Brokerage//GSTOnBrokerage").Value, out utemp))
                    {
                        message.Message += "Invalid GST on brokerage,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                    }



                if (!string.IsNullOrEmpty(message.Message) && message.Message.Contains(','))
                {
                    message.Message = message.Message.Remove(message.Message.LastIndexOf(','));
                }
                else
                {
                    message.Message = "";
                }

            }
            catch
            {

                message = null;
            }
            return message;
        }







    }
}
