﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.CalculationInterface;
using System.Data;
using Oritax.TaxSimp.CM.Organization.Data;


namespace Oritax.TaxSimp.CM.Organization.Commands
{
    public class ExportFinSimplicityHoldings : OrganizationCmCommandBase
    {
        public override string DoAction(string value)
        {
            StringBuilder csvstring = new StringBuilder();
            csvstring.Append("ClientID");
            csvstring.Append(",InvestmentCode");
            csvstring.Append(",MarketCode");
            csvstring.Append(",CurrencyCode");
            csvstring.Append(",CashAccountName");
            csvstring.Append(",PurchaseDate");
            csvstring.Append(",PurchasePrice");
            csvstring.Append(",Units");
            csvstring.Append(",ReservedUnits");
            csvstring.Append(",CostBase");
            csvstring.Append(",RebalanceUnits");
            csvstring.Append(",AdvisorID");
            csvstring.Append("\n");

            foreach (var record in CreateExportFinSimplicityObjects())
            {
                csvstring.Append(record.ClientID);
                csvstring.Append("," + record.InvestmentCode);
                csvstring.Append("," + record.MarketCode);
                csvstring.Append("," + record.CurrencyCode);
                csvstring.Append("," + record.CashAccountName);
                csvstring.Append("," + (record.PurchaseDate.HasValue ? record.PurchaseDate.Value.ToString("dd/MM/yyyy") : ""));
                csvstring.Append("," + record.PurchasePrice);
                csvstring.Append("," + record.Units);
                csvstring.Append("," + record.ReservedUnits);
                csvstring.Append("," + record.CostBase);
                csvstring.Append("," + record.RebalanceUnits);
                csvstring.Append("," + record.AdvisorID);
                csvstring.Append("\n");
            }

            return csvstring.ToString();
        }

        private List<FinSimplicityHolding> CreateExportFinSimplicityObjects()
        {
            List<FinSimplicityHolding> records = new List<FinSimplicityHolding>();

            IBrokerManagedComponent user = Organization.Broker.GetBMCInstance(Organization.Broker.UserContext.Identity.Name, "DBUser_1_1");
            object[] args = new Object[3] { Organization.CLID, user.CID, EnableSecurity.SecuritySetting };
            DataView view = new DataView(Organization.Broker.GetBMCDetailList(OrganizationInstall.ASSEMBLY_NAME, args).Tables["Entities_Table"]);
            view.RowFilter = new OrganisationListingDS().GetRowFilterAllClientsExceptEclipse();
            view.Sort = "ENTITYNAME_FIELD ASC";
            DataTable orgFiteredTable = view.ToTable();

            foreach (DataRow row in orgFiteredTable.Rows)
            {
                Guid clid = new Guid(row["ENTITYCLID_FIELD"].ToString());
                Guid csid = new Guid(row["ENTITYCSID_FIELD"].ToString());

                IOrganizationUnit organizationUnit = Organization.Broker.GetCMImplementation(clid, csid) as IOrganizationUnit;

                if (((organizationUnit as OrganizationUnitCM).IsInvestableClient && (organizationUnit as OrganizationUnitCM).ServiceType.DO_IT_FOR_ME))
                {
                    OrganizationUnitCM clientUnit = (OrganizationUnitCM)organizationUnit;

                    CorporateEntity cEntity = null;
                    ClientIndividualEntity iEntity = null;
                    if (clientUnit.IsCorporateType())
                        cEntity = clientUnit.ClientEntity as CorporateEntity;
                    else
                        iEntity = clientUnit.ClientEntity as ClientIndividualEntity;

                    Common.ServiceType service;
                    List<AccountProcessTaskEntity> accountProcess;
                    List<Oritax.TaxSimp.Common.IdentityCMDetail> misAccounts;
                    string clientID = (organizationUnit as OrganizationUnitCM).ClientId;
                    string AdvisorId = "";
                    bool isDoItForMe = false;

                    if (cEntity != null)//coparate entity 
                    {
                        service = cEntity.Servicetype;
                        accountProcess = cEntity.ListAccountProcess;
                        misAccounts = cEntity.ManagedInvestmentSchemesAccounts;
                        isDoItForMe = cEntity.Servicetype.DO_IT_FOR_ME;
                    }
                    else// invidual entity
                    {
                        service = iEntity.Servicetype;
                        accountProcess = iEntity.ListAccountProcess;
                        misAccounts = iEntity.ManagedInvestmentSchemesAccounts;
                        isDoItForMe = iEntity.Servicetype.DO_IT_FOR_ME;
                    }

                    if (isDoItForMe)
                    {
                        if (organizationUnit is ICmHasParent)
                            AdvisorId = GetAdvisorId((organizationUnit as ICmHasParent).Parent);
                 
                        AddHoldingsInList(clientID, AdvisorId, service, service.DoItForMe.ProgramCode, accountProcess, records, misAccounts);
                    }
                }
                
                Organization.Broker.ReleaseBrokerManagedComponent(organizationUnit);
            }
            return records;
        }

        private string GetAdvisorId(IdentityCM identityCM)
        {
            string result = "";


            if (identityCM != null)
            {
                var comp = this.Organization.Broker.GetCMImplementation(identityCM.Clid, identityCM.Csid);
                if (comp != null && comp is OrganizationUnitCM)
                    result = (comp as OrganizationUnitCM).ClientId;
            }
            return result;
        }

        private void AddHoldingsInList(string clientId, string AdvisorId, Common.ServiceType service, string programCode, List<AccountProcessTaskEntity> accountProcess, List<FinSimplicityHolding> records, List<Oritax.TaxSimp.Common.IdentityCMDetail> misAccounts)
        {
            IOrganizationUnit misData = Organization.Broker.GetCMImplementation(new Guid("7986b8ee-4509-44a5-ac8c-7ce59b59592d"),
                                                                  new Guid("b7ae4bb1-2143-4e56-b9ac-10cf24f7eae5")) as IOrganizationUnit;

            ManagedInvestmentSchemesAccountEntity misentity =
               misData.ClientEntity as ManagedInvestmentSchemesAccountEntity;

            bool desktopBrokerIsProcessed = false;
            var model = Organization.Model.Where(ee => ee.ProgramCode == programCode).FirstOrDefault();
            if (model != null)
            {
                var filteredAccountProcess = accountProcess.Where(accpro => accpro.ModelID == model.ID);

                foreach (var task in filteredAccountProcess)
                {
                    var product = this.Organization.Products.Where(pro => pro.ID == task.TaskID).FirstOrDefault();

                    if (task.LinkedEntity.Clid != Guid.Empty && task.LinkedEntity.Csid != Guid.Empty)
                    {
                        IOrganizationUnit component = this.Organization.Broker.GetCMImplementation(task.LinkedEntity.Clid, task.LinkedEntity.Csid) as IOrganizationUnit;
                        IClientEntity data = component.ClientEntity;

                        if (task.LinkedEntityType == OrganizationType.DesktopBrokerAccount)
                        {
                            if (!desktopBrokerIsProcessed)
                            {
                                if (data != null)
                                {
                                    DesktopBrokerAccountEntity asxEntity = data as DesktopBrokerAccountEntity;
                                    if (asxEntity != null)
                                    {
                                        desktopBrokerIsProcessed = true;
                                        var groupedSecurities = asxEntity.holdingTransactions.GroupBy(holdTran => holdTran.InvestmentCode);
                                        foreach (var security in groupedSecurities)
                                        {
                                            decimal totalSecHoldingUnits = security.Sum(sec => sec.Units);
                                            var asxBroker = Organization.Securities.Where(sec => sec.AsxCode == security.Key).FirstOrDefault();
                                            if (asxBroker != null)
                                            {
                                                var hisSecurity = asxBroker.ASXSecurity.OrderByDescending(secpri => secpri.Date).FirstOrDefault();
                                                decimal unitPrice = 0;

                                                if (hisSecurity != null)
                                                    unitPrice = Convert.ToDecimal(hisSecurity.UnitPrice);

                                                FinSimplicityHolding holdingASX = new FinSimplicityHolding();
                                                holdingASX.ClientID = clientId;
                                                holdingASX.InvestmentCode = asxBroker.AsxCode;
                                                holdingASX.MarketCode = "ASX";
                                                holdingASX.CurrencyCode = "AUD";
                                                holdingASX.CashAccountName = "";
                                                holdingASX.PurchasePrice = 0;
                                                holdingASX.Units = totalSecHoldingUnits;
                                                records.Add(holdingASX);
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        else if (task.LinkedEntityType == OrganizationType.BankAccount)
                        {
                            if (data != null)
                            {
                                Common.BankAccountEntity bankAccountEntity = data as BankAccountEntity;
                                if (bankAccountEntity != null)
                                {
                                    decimal totalholding = bankAccountEntity.CashManagementTransactions.Sum(tran => tran.TotalAmount);
                                    decimal unsettledHolding = bankAccountEntity.UnsettledOrder;
                                    FinSimplicityHolding holding = new FinSimplicityHolding();

                                    holding.ClientID = clientId;
                                    holding.InvestmentCode = "CASH";
                                    holding.MarketCode = "";
                                    holding.CurrencyCode = "AUD";
                                    holding.CashAccountName = clientId;
                                    holding.PurchasePrice = 1;
                                    holding.AdvisorID = string.Empty;
                                    holding.Units = totalholding + unsettledHolding;
                                    records.Add(holding);
                                }
                            }
                        }

                        if (task.LinkedEntityType == OrganizationType.ManagedInvestmentSchemesAccount)
                        {
                            if (data != null)
                            {
                                if (misentity != null)
                                {
                                    Guid fundId = product.FundAccounts.FirstOrDefault();

                                    var secr = Organization.Securities.FirstOrDefault(secs => product.TaskDescription.Contains(secs.AsxCode));

                                    if (secr != null)
                                    {
                                        double totalshares = 0;

                                        var fundAccountEntity = misentity.FundAccounts.FirstOrDefault(ee => ee.ID == fundId);

                                        if (fundAccountEntity != null && product.TaskDescription.Contains(fundAccountEntity.Code))
                                        {
                                            var securityHolding = fundAccountEntity.SecurityHoldings.Where(ee => ee.Account == clientId && ee.FundCode == fundAccountEntity.Code).FirstOrDefault();
                                            if (securityHolding != null)
                                                totalshares = securityHolding.TotalShares;
                                        }

                                        var holding = records.FirstOrDefault(ii => ii.InvestmentCode == secr.AsxCode && ii.ClientID == clientId);
                                        decimal unsettledUnits = 0;
                                        var unitHoldings = fundAccountEntity.UnsettledOrders.Where(ss => ss.ClientID == clientId);
                                        if (unitHoldings != null)
                                            unsettledUnits = unitHoldings.Sum(ss => ss.UnsettledUnits);

                                        if (holding == null)
                                        {
                                            holding = new FinSimplicityHolding();
                                            holding.ClientID = clientId;
                                            holding.InvestmentCode = secr.AsxCode;
                                            holding.MarketCode = "";
                                            holding.CurrencyCode = "AUD";
                                            holding.CashAccountName = "";
                                            holding.PurchasePrice = 0;
                                            holding.AdvisorID = "";
                                            holding.Units = Decimal.Round(Convert.ToDecimal(totalshares), 0) + unsettledUnits;
                                            records.Add(holding);
                                        }
                                        else
                                            holding.Units += (Decimal.Round(Convert.ToDecimal(totalshares), 0) + unsettledUnits);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void SetHoldingRecords(string clientId, string AdvisorId, List<FinSimplicityHolding> records, DesktopBrokerAccountEntity enty,
                                              string code, SecuritiesEntity secr)
        {
            HoldingBalance bankAccountEntityHoldingBalancesWhere =
                enty.HoldingBalances.Where(
                    sss => sss.InvestmentCode == code).
                    FirstOrDefault();
            decimal hodling = 0;

            decimal unsettledUnits = 0;
            var unitHoldings =
                enty.PaidOrdersTranactions.Where(ss => ss.IsSettled == false && ss.InvestmentCode == code);

            if (unitHoldings != null)
                unsettledUnits = unitHoldings.Sum(ss => ss.Units);

            if (bankAccountEntityHoldingBalancesWhere != null)
                hodling = bankAccountEntityHoldingBalancesWhere.Actual - unsettledUnits;

            var holding =
                records.Where(ii => ii.InvestmentCode == code && ii.ClientID == clientId).FirstOrDefault();


            if (holding == null)
            {
                holding = new FinSimplicityHolding();
                holding.ClientID = clientId;
                holding.InvestmentCode = secr.AsxCode;
                holding.MarketCode = secr.Market;
                holding.CurrencyCode = "AUD";
                holding.CashAccountName = "";
                holding.PurchasePrice = 0;
                holding.Units = hodling;
                records.Add(holding);
            }
            else
                holding.Units += hodling;
        }
    }//end of classs

    [Serializable]
    public class FinSimplicityHolding
    {

        public string ClientID { get; set; }
        public string InvestmentCode { get; set; }
        public string MarketCode { get; set; }
        public string CurrencyCode { get; set; }
        public string CashAccountName { get; set; }
        public DateTime? PurchaseDate { get; set; }
        public decimal? PurchasePrice { get; set; }
        public decimal Units { get; set; }
        public decimal? ReservedUnits { get; set; }
        public decimal? CostBase { get; set; }
        public decimal? RebalanceUnits { get; set; }
        public string AdvisorID { get; set; }

        public FinSimplicityHolding()
        {
            PurchaseDate = new DateTime(1900, 1, 1);
            ClientID = string.Empty;
            InvestmentCode = string.Empty;
            MarketCode = string.Empty;
            CurrencyCode = string.Empty;
            CashAccountName = string.Empty;
            AdvisorID = string.Empty;
        }

    }
}
