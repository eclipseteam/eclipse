﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Mail;
using System.Text;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.CM.OrganizationUnit;
using System.Collections.Generic;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.CM.Organization.Commands
{
    public class OrganizationAddCommand : OrganizationCmCommandBase
    {
        const string DBUserModuleName = "DBUser_1_1";
        IBrokerManagedComponent _Component;
        OrganizationUnit _Unit;

        public override string DoAction(string value)
        {
            var unit = AddOrganizationUnit(() => value.ToNewOrData<OrganizationUnit>());
            return unit.ToXmlString();
        }

        public OrganizationUnit AddOrganizationUnit(Func<OrganizationUnit> action)
        {
            ILogicalModule created = null;
            ILogicalModule organization = null;
            try
            {
                Organization.Broker.SetWriteStart();
                _Unit = action();

                //Set Unique ID..........

                //1. Prepare IDs Collection. In Future given we will have more unique IDs we will need to prepare a special 
                //collection class which can hold 4 or more IDs Combination
                Dictionary<string, string> clientIDs = new Dictionary<string, string>();
                Dictionary<string, string> bankwestIDs = new Dictionary<string, string>();

                OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
                {
                    if ((bmc is IXmlData) && (bmc is IOrganizationUnit))
                        if (((IOrganizationUnit)bmc).ClientId != String.Empty)
                        {
                            if (clientIDs.ContainsKey(((IOrganizationUnit)bmc).ClientId))
                            {
                                if (((IOrganizationUnit)bmc).ApplicationID != String.Empty && ((IOrganizationUnit)bmc).ApplicationID != null)
                                {
                                    if (bankwestIDs.ContainsKey(((IOrganizationUnit)bmc).ApplicationID))
                                    {
                                        string bankwestID = "BW" + UniqueID.Return9UniqueID();

                                        while (bankwestIDs.ContainsValue(_Unit.ApplicationID))
                                            ((IOrganizationUnit)bmc).ApplicationID = "BW" + UniqueID.Return9UniqueID();
                                        ((IOrganizationUnit)bmc).ApplicationID = bankwestID;
                                        bankwestIDs.Add(((IOrganizationUnit)bmc).ApplicationID, ((IOrganizationUnit)bmc).ApplicationID);
                                    }
                                    else
                                        bankwestIDs.Add(((IOrganizationUnit)bmc).ApplicationID, ((IOrganizationUnit)bmc).ApplicationID);
                                }

                                string clientId = "IV" + UniqueID.ReturnUniqueID(EUniqueIDFormat.Combined);

                                while (clientIDs.ContainsKey(clientId))
                                    clientId = "IV" + UniqueID.ReturnUniqueID(EUniqueIDFormat.Combined);
                                ((IOrganizationUnit)bmc).ClientId = clientId;
                                clientIDs.Add(((IOrganizationUnit)bmc).ClientId, ((IOrganizationUnit)bmc).ClientId);
                            }
                            else
                                clientIDs.Add(((IOrganizationUnit)bmc).ClientId, ((IOrganizationUnit)bmc).ClientId);
                        }
                });

                _Unit.ClientId = "IV" + UniqueID.ReturnUniqueID(EUniqueIDFormat.Combined);

                while (clientIDs.ContainsKey(_Unit.ClientId))
                    _Unit.ClientId = "IV" + UniqueID.ReturnUniqueID(EUniqueIDFormat.Combined);

                _Unit.ApplicationID = "BW" + UniqueID.Return9UniqueID();

                while (bankwestIDs.ContainsKey(_Unit.ApplicationID))
                    _Unit.ApplicationID = "BW" + UniqueID.Return9UniqueID();

                Guid id = OrganizationTypeList.GetEntity((OrganizationType)_Unit.Type.ToInt());
                created = Organization.Broker.CreateLogicalCM(id, _Unit.Name, Guid.Empty, null);
                organization = Organization.Broker.GetLogicalCM(Organization.CLID);
                organization.AddChildLogicalCM(created);
                _Component = (IBrokerManagedComponent)created[created.CurrentScenario];

                CreationPipeline();

                _Unit.Type = created.CMTypeName;
                _Unit.Clid = created.CLID;
            }
            catch (Exception ex)
            {
                throw ex;

            }
            finally
            {
                Organization.Broker.ReleaseBrokerManagedComponent(organization);
                Organization.Broker.ReleaseBrokerManagedComponent(created);
                Organization.Broker.ReleaseBrokerManagedComponent(_Component);
                _Component = null;
            }
            return _Unit;
        }

        private void CreationPipeline()
        {
            ICalculationModule calculation = _Component as ICalculationModule;
            if (calculation != null)
            {
                _Unit.Data = _Component.UpdateDataStream(-1, _Unit.Data);
                _Unit.Csid = calculation.CSID;
            }

            ICanCreateDefaultUser creator = _Component as ICanCreateDefaultUser;
            if (creator != null)
            {
                IApplicationUserService service = Organization.Broker.GetCMImplementation(DBUserModuleName, DBUserModuleName) as IApplicationUserService;
                ApplicationUser user = creator.GetDefaultUser();
                service.AddUser(_Unit.CurrentUser, user);
                SendEMail(user);
            }
            else
            {
                ISecurityService security = _Component as ISecurityService;
                if (security != null) security.AddUser(_Unit.CurrentUser);
            }

        }

        private void SendEMail(ApplicationUser user)
        {
            try
            {
                SmtpClient client = new SmtpClient(Organization.SmtpSettings.HostName, Organization.SmtpSettings.PortNumber);
                client.Credentials = new NetworkCredential(Organization.SmtpSettings.UserName, Organization.SmtpSettings.Password);
                using (MailMessage mail = new MailMessage())
                {
                    mail.From = new MailAddress(Organization.SmtpSettings.UserName);
                    mail.To.Add(new MailAddress(user.Email));
                    mail.Subject = "Eclipse account details";
                    StringBuilder body = new StringBuilder();
                    body.AppendFormat(" Your new account has been automatically created at eclipse. Account details are give below. {0} {0} {0}", Environment.NewLine);
                    body.AppendFormat(" LoginId : {0}  {1} {1}", user.LoginId, Environment.NewLine);
                    body.AppendFormat(" Password : {0}  {1} {1}", user.Password, Environment.NewLine);
                    body.AppendFormat(" Regards,  {0} Eclipse Administrator", Environment.NewLine);
                    mail.Body = body.ToString();

                    if (Organization.SmtpSettings.UseSsl) client.EnableSsl = true;
                    client.Send(mail);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }
    }
}
