﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;
using BankAccountEntity = Oritax.TaxSimp.Common.BankAccountEntity;

namespace Oritax.TaxSimp.CM.Organization.Commands
{
    public class ValidateBankAccountPaidOrdersCommand : OrganizationCmCommandBase
    {

        public override string DoAction(string value)
        {
            string outa = string.Empty;
            try
            {
                string[] values = value.ToDataArray<string>();
                UserEntity user = values[0].ToData<UserEntity>();
                XElement root = XElement.Parse(values[1], LoadOptions.None);



             XElement[] elements =root.Elements().ToArray();//XPathSelectElements("//AccountTransactions").ToArray();
                BankAccountEntity bankAccount = null;


                

                List<BankAccountOrdersMessage> messages = new List<BankAccountOrdersMessage>();
                 foreach (var each in elements)
                 {
                
                 XElement accs = each.XPathSelectElement("Account");

                     bankAccount = null;
                     OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
                     {
                         if (logical.CMType == OrganizationTypeList.GetEntity(OrganizationType.BankAccount))
                         {
                             BankAccountEntity bank = this.GetData(bmc).ToNewOrData<BankAccountEntity>();
                             string bsb = bank.BSB;
                             //if (!string.IsNullOrEmpty(bank.BSB) && bank.BSB.Contains('-'))
                             // bsb =bank.BSB.Split('-')[1];
                             string BsBNAccountNumber = bsb + "-"+bank.AccountNumber;
                             if ( accs.Value.Trim() == BsBNAccountNumber)
                             {
                                 bankAccount = bank;
                             }
                             
                         }
                            
                     });

                     if(bankAccount==null)
                     {
                     messages.Add(CreateMissingMessage(accs.Value.Trim()));
                     }

                     messages.Add(CreateHoldingValidationMessage(each, (bankAccount != null)));
                 
                   



                 }






                 outa = messages.ToXmlString(); 
            }
            catch (Exception ex)
            {
                outa = "Exception : " + ex.Message;
            }

            return outa;
        }

        private BankAccountOrdersMessage CreateHoldingValidationMessage(XElement transaction, bool isAccountFound)
        {
        BankAccountOrdersMessage message = new BankAccountOrdersMessage();

            DateTimeFormatInfo info = new DateTimeFormatInfo();
            info.ShortDatePattern = "dd/MM/yyyy";



           
            string Account = transaction.XPathSelectElement("Account").Value;
            string Amount = transaction.XPathSelectElement("Amount").Value;
            string ReferenceNarration = transaction.XPathSelectElement("ReferenceNarration").Value;
            string DateRequired = transaction.XPathSelectElement("DateRequired").Value;

            if(DateRequired.Length==7)
            {
                DateRequired = "0" + DateRequired;
            }


            DateTime date;
            decimal utemp;
            try
            {

                message.Status = ImportMessageStatus.Sucess;
                message.MessageType = ImportMessageType.Sucess;
              

                message.Account = Account;
                message.Amount = Amount;

                message.DateRequired = DateRequired;
               
                message.ReferenceNarration = ReferenceNarration;
               
                if (!isAccountFound)
                {

                    message.Message += "Account Number (" + Account + ") doesn't exists,";
                    message.Status = ImportMessageStatus.AccountNotFound;
                    message.MessageType = ImportMessageType.Error;

                }




                if (!DateTime.TryParseExact(DateRequired, "dMMyyyy", info, DateTimeStyles.None, out date))
                {

                    message.Message += "Invalid required Date,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                }
                else
                {
                message.DateRequired = date.ToString("dd/MM/yyyy");
                }


                if (!string.IsNullOrEmpty(Amount))
                    if (!decimal.TryParse(Amount, NumberStyles.Currency, CultureInfo.CurrentCulture, out utemp))
                    {
                        message.Message += "Invalid  Amount,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                    }
                    else
                    {

                        // string format = "$#,##0.00;-$#,##0.00;$0";

                        message.Amount = utemp.ToString();
                    }



                if (!string.IsNullOrEmpty(message.Message) && message.Message.Contains(','))
                {
                    message.Message = message.Message.Remove(message.Message.LastIndexOf(','));
                }
                else
                {
                    message.Message = "";
                }

            }
            catch
            {

                message = null;
            }
            return message;



        }



        private BankAccountOrdersMessage CreateMissingMessage(string AccountNumber)
        {
           BankAccountOrdersMessage message = new BankAccountOrdersMessage();



            message.Message += "Account Number (" + AccountNumber + ") doesn't exists";
          
            message.Account = AccountNumber;
            message.MessageType = ImportMessageType.MissingInvestmentCode;
            //message.Status = ImportMessageStatus.Failed;
         



            return message;

        }



        public string GetData(IBrokerManagedComponent bmc)
        {
            return bmc.GetDataStream(-911, string.Empty);// run default Command From CM Class
        }

       



    }
}
