﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Enum;
using System.Collections.ObjectModel;

namespace Oritax.TaxSimp.CM.Organization.Commands
{
    public class SecuritiesListProcessCommand : OrganizationCmCommandBase
    {
        public override string DoAction(string data)
        {
            List<Guid> ids = data.ToNewOrData<List<Guid>>();
            //List<DividendEntity> ids = data.ToNewOrData<List<DividendEntity>>();
             DividendEntity dividend1 =null;
            this.Organization.Broker.SetStart();
            var security=Organization.Securities.Where(sec => sec.ID == ids[0]).FirstOrDefault();

            if (ids[0] != Guid.Empty)
            {
                if (security != null && security.DividendCollection != null)
                {
                    var dividend = security.DividendCollection.Where(div => div.ID == ids[1]).FirstOrDefault();
                    if (dividend != null)
                    {
                        dividend1 = dividend;
                        //dividend = ids[1];
                        dividend.ProcessFlag = true;
                    }
                    List<ProductEntity> products = new List<ProductEntity>();
                    foreach (var prodsec in Organization.ProductSecurities)
                    {
                        if (prodsec.Details != null)
                        {
                            var details = prodsec.Details.Where(detal => detal.SecurityCodeId == security.ID).FirstOrDefault();
                            if (details != null)
                            {
                                foreach (var product in Organization.Products.Where(produt => produt.ProductSecuritiesId == prodsec.ID))
                                {
                                    products.Add(product);
                                }
                            }
                        }
                    }
                        OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
                        {
                            if ((bmc is IOrganizationUnit) && (bmc as IOrganizationUnit).IsInvestableClient)
                            {
                                (bmc as OrganizationUnitCM).Broker.SetStart();
                                (bmc as OrganizationUnitCM).IncomeTransactionProcess(dividend, products, security);
                                bmc.CalculateToken(true);
                            }
                        });
                }

            }
            else
            {
                OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
                {
                    if ((bmc is IOrganizationUnit) && (bmc as IOrganizationUnit).IsInvestableClient)
                    {
                        (bmc as OrganizationUnitCM).Broker.SetStart();
                        (bmc as OrganizationUnitCM).IncomeTransactionProcessTD(ids[1]);
                        bmc.CalculateToken(true);
                    }
                });
                
            }







            if (dividend1 != null)
                return dividend1.ToXmlString();

             return "Failed..";
        }
    }
}
