﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;

namespace Oritax.TaxSimp.CM.Organization.Commands
{





    public class ValidatetMISFundTransactionsCommand : OrganizationCmCommandBase
    {
      

        public override string DoAction(string value)
        {
            string outa = string.Empty;
            try
            {
                string[] values = value.ToDataArray<string>();
                UserEntity user = values[0].ToData<UserEntity>();
                XElement root = XElement.Parse(values[1], LoadOptions.None);
                string misName = values[2];

                outa = GetValidateData(root, misName).ToXmlString();
            }
            catch (Exception ex)
            {
                outa = "Exception : " + ex.Message;
            }

            return outa;
        }



        public string GetData(IBrokerManagedComponent bmc)
        {
            return bmc.GetDataStream(-911, string.Empty);// run default Command From CM Class
        }

        private List<MISFundTransactionMessage> GetValidateData(XElement root, string misName)
        {
            List<MISFundTransactionMessage> messages = new List<MISFundTransactionMessage>();

            XElement[] elements = root.XPathSelectElements("//MISFundTransaction").ToArray();

            foreach (var each in elements)
            {
                string fundCode = each.XPathSelectElement("FundCode").Value;
                string Account = each.XPathSelectElement("Account").Value;

                OrganizationUnitCM client = null;
                OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
                {

                    if ((bmc is OrganizationUnitCM) && (bmc as OrganizationUnitCM).ClientId != null && (bmc as OrganizationUnitCM).ClientId.Trim() == Account.Trim())
                    {
                        client = (bmc as OrganizationUnitCM);
                     
                    }
                    

                
                });


                if(client==null)
                {
                    MISFundTransactionMessage message = new MISFundTransactionMessage();

                    message.Message += "Client with id (" + Account + ") doesn't exists";
                    message.Account = Account;
                    message.MessageType = ImportMessageType.Warning;
                    //message.Status = ImportMessageStatus.Failed;
                    message.Status = ImportMessageStatus.AccountNotFound;
                    //message.MessageType = ImportMessageType.Error;
                    messages.Add(message);

                }
                else
                {

                    bool isAttachedWithClient = client.IsMISAttachedWithFund(fundCode, misName);


                    if (!isAttachedWithClient)
                    {
                        MISFundTransactionMessage message = new MISFundTransactionMessage();

                        message.Message += "Fund code (" + fundCode + ") doesn't exists.Or Fund code is not attached with any client";
                        message.FundCode = fundCode;
                        message.MessageType = ImportMessageType.MissingInvestmentCode;
                        //message.Status = ImportMessageStatus.Failed;
                        message.Status = ImportMessageStatus.Failed;
                        //message.MessageType = ImportMessageType.Error;



                        messages.Add(message);
                    }


                    messages.Add(CreateMessage(each, isAttachedWithClient));

                   

                }


                


              



            }



            return messages;
        }



        private MISFundTransactionMessage CreateMessage(XElement asxPrice, bool isAttached )
        {
            MISFundTransactionMessage message = new MISFundTransactionMessage();

            DateTimeFormatInfo info = new DateTimeFormatInfo();
            info.ShortDatePattern = "dd/MM/yyyy";
            
            string TradeDate = asxPrice.XPathSelectElement("TradeDate").Value;
            string FundCode = asxPrice.XPathSelectElement("FundCode").Value;
            string FundName = asxPrice.XPathSelectElement("FundName").Value;
            string Account = asxPrice.XPathSelectElement("Account").Value;
            string SubAccount = asxPrice.XPathSelectElement("SubAccount").Value;
           
            string TransactionType = asxPrice.XPathSelectElement("TransactionType").Value;
            string Shares = asxPrice.XPathSelectElement("Shares").Value;
            string UnitPrice = asxPrice.XPathSelectElement("UnitPrice").Value;
            string Amount = asxPrice.XPathSelectElement("Amount").Value;
            string InvestorCategory = asxPrice.XPathSelectElement("InvestorCategory").Value;


            DateTime date;
            decimal utemp;
            try
            {

                message.Status = ImportMessageStatus.Sucess;
                message.MessageType = ImportMessageType.Sucess;

                message.TradeDate = TradeDate;
                message.FundCode = FundCode;
                message.FundName = FundName;
                message.Account = Account;
                message.SubAccount = SubAccount;
                message.TransactionType = TransactionType;
                message.Shares = Shares;
                message.UnitPrice = UnitPrice;
                message.Amount = Amount;
                message.InvestorCategory = InvestorCategory;





                if (!isAttached)
                {
                    message.Message += "Fund code (" + FundCode + ") doesn't exists.Or fund code is not attached with any client,";

                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                }
                
                else
                {

                    //message.Message += "Fund code (" + FundCode + ") doesn't exists.Or fund code is not attached with any client,";
                    //message.Status = ImportMessageStatus.Failed;
                    //message.MessageType = ImportMessageType.Error;
                }
                

                if (!DateTime.TryParse(TradeDate, info, DateTimeStyles.None, out date))
                {

                    message.Message += "Invalid Price Date,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                }
               else
               {
                   message.TradeDate = date.ToString("dd/MM/yyyy");
               }



      

                if (!string.IsNullOrEmpty(UnitPrice))
                    if (!decimal.TryParse(UnitPrice, NumberStyles.Currency, CultureInfo.CurrentCulture, out utemp))
                    {
                        message.Message += "Invalid Unit Price,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                    }else
                   {

                      // string format = "$#,##0.00;-$#,##0.00;$0";

                       message.UnitPrice = utemp.ToString("C");
                   }

                if (!string.IsNullOrEmpty(Amount))
                    if (!decimal.TryParse(Amount, NumberStyles.Currency, CultureInfo.CurrentCulture, out utemp))
                    {
                        message.Message += "Invalid Amount,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                    }
                    else
                    {

                        // string format = "$#,##0.00;-$#,##0.00;$0";

                        message.Amount = utemp.ToString("C");
                    }

                
                if (!string.IsNullOrEmpty(Shares))
                    if (!decimal.TryParse(Shares, NumberStyles.Currency, CultureInfo.CurrentCulture, out utemp))
                    {
                        message.Message += "Invalid Shares,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                    }
                    else
                    {

                        // string format = "$#,##0.00;-$#,##0.00;$0";

                        message.Shares = utemp.ToString();
                    }



                if (!string.IsNullOrEmpty(message.Message) && message.Message.Contains(','))
                {
                    message.Message = message.Message.Remove(message.Message.LastIndexOf(','));
                }
                else
                {
                    message.Message = "";
                }

            }
            catch
            {

                message = null;
            }
            return message;
        }

      

       

      
        
    }
}
