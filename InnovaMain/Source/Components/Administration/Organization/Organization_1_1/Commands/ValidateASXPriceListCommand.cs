﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;

namespace Oritax.TaxSimp.CM.Organization.Commands
{





    public class ValidateASXPriceListCommand : OrganizationCmCommandBase
    {
      

        public override string DoAction(string value)
        {
            string outa = string.Empty;
            try
            {
                string[] values = value.ToDataArray<string>();
                UserEntity user = values[0].ToData<UserEntity>();
                XElement root = XElement.Parse(values[1], LoadOptions.None);


             outa = GetValidateData(root).ToXmlString();
            }
            catch (Exception ex)
            {
                outa = "Exception : " + ex.Message;
            }

            return outa;
        }



        public string GetData(IBrokerManagedComponent bmc)
        {
            return bmc.GetDataStream(-911, string.Empty);// run default Command From CM Class
        }

        private List<ASXPriceMessage> GetValidateData(XElement root)
        {
            List<ASXPriceMessage> messages = new List<ASXPriceMessage>();

          
            XElement[] elements = root.XPathSelectElements("//AsxPrice").ToArray();

            foreach (var each in elements)
            {
                string InvestmentCode = each.XPathSelectElement("InvestmentCode").Value;
               

                if (Organization.Securities.Count(sec => sec.AsxCode == InvestmentCode) == 0)
                {
                    ASXPriceMessage message = new ASXPriceMessage();

                    message.Message += "Investment code (" + InvestmentCode + ") doesn't exists";
                    message.InvestmentCode = InvestmentCode;
                    message.MessageType = ImportMessageType.MissingInvestmentCode;
                    //message.Status = ImportMessageStatus.Failed;
                    messages.Add(message);
                }


                messages.Add(CreateMessage(each));



            }



            return messages;
        }



        private ASXPriceMessage CreateMessage(XElement asxPrice)
        {
            ASXPriceMessage message = new ASXPriceMessage();

           
            DateTimeFormatInfo info = new DateTimeFormatInfo();
            info.ShortDatePattern = "dd/MM/yyyy";

            string InvestmentCode = asxPrice.XPathSelectElement("InvestmentCode").Value;
            string CompanyName = asxPrice.XPathSelectElement("CompanyName").Value;
            string Price = asxPrice.XPathSelectElement("Price").Value;
            string PriceDate = asxPrice.XPathSelectElement("PriceDate").Value;
            string currency = asxPrice.XPathSelectElement("Currency").Value;
            DateTime date;
            decimal utemp;
            try
            {

                message.Status = ImportMessageStatus.Sucess;
                message.MessageType = ImportMessageType.Sucess;
                message.InvestmentCode = InvestmentCode;
                message.CompanyName = CompanyName;
                message.Price = Price;
                message.PriceDate = PriceDate;
                message.Currency = currency;


                if (string.IsNullOrEmpty(asxPrice.XPathSelectElement("Currency").Value))
                {

                    message.Message += "Invalid Currency,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;

                }

               
              
               

               if (!DateTime.TryParse(asxPrice.XPathSelectElement("PriceDate").Value, info, DateTimeStyles.None, out date))
                {

                    message.Message += "Invalid Price Date,";
                    message.Status = ImportMessageStatus.Failed;
                    message.MessageType = ImportMessageType.Error;
                }
               else
               {
                   message.PriceDate = date.ToString("dd/MM/yyyy");
               }

               

            var security=    Organization.Securities.Where(sec =>sec.AsxCode==InvestmentCode).FirstOrDefault();


                if(security!=null)
                {
                    if (security.ASXSecurity!=null && security.ASXSecurity.Count(s => s.Date == date) > 0)
                    {
                        message.Message += "Price for date already exists,";
                        message.Status = ImportMessageStatus.AlreayExists;
                        message.MessageType = ImportMessageType.Error;
                    }
                }else
                {
                    message.Message += "Invalid investment code,";
                    message.Status = ImportMessageStatus.Failed;
                    //message.MessageType = ImportMessageType.Warning;

                }


                if (!string.IsNullOrEmpty(asxPrice.XPathSelectElement("Price").Value))
                   if (!decimal.TryParse(asxPrice.XPathSelectElement("Price").Value, NumberStyles.Currency, CultureInfo.CurrentCulture, out utemp))
                    {
                        message.Message += "Invalid price,";
                        message.Status = ImportMessageStatus.Failed;
                        message.MessageType = ImportMessageType.Error;
                    }else
                   {

                      // string format = "$#,##0.00;-$#,##0.00;$0";

                       message.Price = utemp.ToString();
                   }




                if (!string.IsNullOrEmpty(message.Message) && message.Message.Contains(','))
                {
                    message.Message = message.Message.Remove(message.Message.LastIndexOf(','));
                }
                else
                {
                    message.Message = "";
                }

            }
            catch
            {

                message = null;
            }
            return message;
        }

      

       

      
        
    }
}
