﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.CM.Organization.Commands
{





    public class ImportMISFundTransactionsCommand : OrganizationCmCommandBase
    {


        public override string DoAction(string value)
        {
            string outa = string.Empty;
            try
            {
                string[] values = value.ToDataArray<string>();
                UserEntity user = values[0].ToData<UserEntity>();
                XElement root = XElement.Parse(values[1], LoadOptions.None);

                string filename = values[2];
                string misName = values[3];
                this.Organization.Broker.LogEvent(EventType.FileImported, "Imported MIS Fund transaction File. File Name: " + filename, user.CurrentUserName, Guid.Empty);
                outa = GetValidateData(root, misName).ToXmlString();
            }
            catch (Exception ex)
            {
                outa = "Exception : " + ex.Message;
            }

            return outa;
        }



        public string GetData(IBrokerManagedComponent bmc)
        {
            return bmc.GetDataStream(-911, string.Empty);// run default Command From CM Class
        }

        private List<MISFundTransactionMessage> GetValidateData(XElement root, string misName)
        {
            List<MISFundTransactionMessage> messages = new List<MISFundTransactionMessage>();

            List<XElement> elements = root.XPathSelectElements("//MISFundTransaction").ToList();

            List<string> addedAccountsNumbers = new List<string>();
            List<XElement> notAttached = new List<XElement>();
            OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
            {

                if (bmc is OrganizationUnitCM && OrganizationListAction.IsValidClientType(logical.CMTypeName))
                {
                    var unit = (bmc as OrganizationUnitCM);
                    if (unit.ClientId != null)
                    {
                        Process(misName, messages, elements, addedAccountsNumbers, unit, notAttached);
                    }
                }


            });

            //create missing elements
            for (int index = elements.Count-1; index >=0; index--)
            {
                bool remove = false;
                var each = elements[index];
                XElement att = each.XPathSelectElement("Account");
                if (!addedAccountsNumbers.Contains(att.Value))
                {
                    MISFundTransactionMessage message = new MISFundTransactionMessage();

                    message.Message += "Client with id (" + att.Value + ") doesn't exists";
                    message.Account = att.Value;
                    message.MessageType = ImportMessageType.Warning;
                    //message.Status = ImportMessageStatus.Failed;
                    message.Status = ImportMessageStatus.AccountNotFound;
                    //message.MessageType = ImportMessageType.Error;
                    messages.Add(message);
                    remove = true;
                }


                if(notAttached.Contains(each))
                {
                    remove = true;
                }
                if(remove)
                {
                    elements.Remove(each);
                }
            }


            //import process 
            OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
                           {
                               if (logical.CMType == OrganizationTypeList.GetEntity(OrganizationType.ManagedInvestmentSchemesAccount))
                               {
                                   OrganizationUnitCM unit = (bmc as OrganizationUnitCM);
                                  
                                   if (unit.Name == misName)
                                   {
                                       unit.Broker.SetWriteStart();
                                       foreach (var each in elements)
                                       {
                                       foreach (var msg in unit.ImportTransactions(each))
                                       {
                                           messages.Add((MISFundTransactionMessage)msg);
                                       }
                                       }

                                       bmc.CalculateToken(true);
                                       return;
                                   }

                                  
                               }


                           });


            #region old code
            //foreach (var each in elements)
            //{
            //    string fundCode = each.XPathSelectElement("FundCode").Value;
            //    string Account = each.XPathSelectElement("Account").Value;

            //    OrganizationUnitCM client = null;
            //    OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
            //    {

            //        if ((bmc is OrganizationUnitCM) && (bmc as OrganizationUnitCM).ClientId != null && (bmc as OrganizationUnitCM).ClientId.Trim() == Account.Trim())
            //        {
            //            client = (bmc as OrganizationUnitCM);

            //        }

            //    });


            //    if (client == null)
            //    {
            //        MISFundTransactionMessage message = new MISFundTransactionMessage();

            //        message.Message += "Client with id (" + Account + ") doesn't exists.Skipped.";
            //        message.Account = Account;
            //        message.MessageType = ImportMessageType.Warning;
            //        //message.Status = ImportMessageStatus.Failed;
            //        message.Status = ImportMessageStatus.AccountNotFound;
            //        //message.MessageType = ImportMessageType.Error;
            //        messages.Add(message);

            //    }
            //    else
            //    {
            //        bool isAttachedWithClient = client.IsMISAttachedWithFund(fundCode, misName);
            //        if (!isAttachedWithClient)
            //        {
            //            MISFundTransactionMessage message = new MISFundTransactionMessage();

            //            message.Message += "Fund code (" + fundCode + ") doesn't exists.Skipped.";
            //            message.FundCode = fundCode;
            //            message.MessageType = ImportMessageType.MissingInvestmentCode;
            //            message.Status = ImportMessageStatus.Failed;
            //            messages.Add(message);
            //        }
            //        else
            //        {

            //            OrganizationListAction.ForEach("Administrator", Organization, (csid, logical, bmc) =>
            //               {
            //                   if (logical.CMType == OrganizationTypeList.GetEntity(OrganizationType.ManagedInvestmentSchemesAccount))
            //                   {
            //                       OrganizationUnitCM unit = (bmc as OrganizationUnitCM);
            //                       unit.Broker.SetWriteStart();
            //                       if (unit.Name == misName)
            //                       {
            //                           foreach (var msg in unit.ImportTransactions(each))
            //                           {
            //                               messages.Add((MISFundTransactionMessage)msg);
            //                           }
            //                       }

            //                       bmc.CalculateToken(true);
            //                   }


            //               });
            //        }
            //    }
            //} 
            #endregion

            return messages;
        }


        private void Process(string misName, List<MISFundTransactionMessage> messages, List<XElement> elements, List<string> addedAccountsNumbers,
                             OrganizationUnitCM unit,List<XElement> notAttached )
        {

            var clientelements = elements.Where(ss => ss.XPathSelectElement("Account").Value.Trim() == unit.ClientId.Trim());

            if (clientelements != null && clientelements.Count() > 0)
            {

                addedAccountsNumbers.Add(unit.ClientId.Trim());

                foreach (var each in clientelements)
                {
                    string fundCode = each.XPathSelectElement("FundCode").Value;
                    bool isAttachedWithClient = unit.IsMISAttachedWithFund(fundCode, misName);


                    if (!isAttachedWithClient)
                    {
                        notAttached.Add(each);
                        MISFundTransactionMessage message = new MISFundTransactionMessage();

                        message.Message += "Fund code (" + fundCode + ") doesn't exists.Or Fund code is not attached with any client";
                        message.FundCode = fundCode;
                        message.MessageType = ImportMessageType.MissingInvestmentCode;
                        message.Status = ImportMessageStatus.Failed;
                        messages.Add(message);
                    }


                   
                }

            }
        }







    }
}
