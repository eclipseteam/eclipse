using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Permissions;

using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.MapTarget;
using Oritax.TaxSimp.DataSets;

namespace Oritax.TaxSimp.EventLog
{
    [Serializable]
    public partial class EventLogCM : CalculationModuleBase
    {
        #region CONSTRUCTOR
        public EventLogCM()
            : base()
        {

        }

        public EventLogCM(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {

        }

        #endregion

        #region OVERRIDE METHODS
        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
        protected override void OnGetData(DataSet data)
        {
            if (data is EventLogDS)
            {
                EventLogDS eventLogDS = (EventLogDS)data;
                EventLogAllCollectionSpecifier collectionSpecifier = new EventLogAllCollectionSpecifier(eventLogDS.EventLogFilter, eventLogDS.CIDFilter);
                DataTable dataTable = Broker.ListBMCCollection(collectionSpecifier);
                if (dataTable != null)
                {
                    dataTable.TableName = "EventLog";
                    data.Tables.Add(dataTable.Copy());
                }
            }
        }

        public override string GetDataStream(int type, string data)
        {

            EventLogAllCollectionSpecifier collectionSpecifier = new EventLogAllCollectionSpecifier();
            DataTable dataTable = Broker.ListBMCCollection(collectionSpecifier);
            string xml = string.Empty;
            if (dataTable != null)
            {
                dataTable.TableName = "EventLog";
                List<EventLogEntity> eventLogs = new List<EventLogEntity>();

                foreach (DataRow row in dataTable.Rows)
                {
                    EventLogEntity eventLog = new EventLogEntity();
                    eventLog.Name = row["Name"].ToString();
                    eventLog.UserName = row["UserName"].ToString();
                    eventLog.Type = row["Type"].ToString();
                    eventLog.Detail = row["Detail"].ToString();
                    eventLog.DateTime = row["DateTime"]==null?DateTime.Now  as DateTime?: row["DateTime"] as DateTime?;

                    eventLogs.Add(eventLog);
                }

                xml= eventLogs.ToXmlString();
               
            }
            return xml;
        }

        public override string OnUpdateDataStream(int type, string data)
        {
            string xml = string.Empty;


            return xml;
        }


        #endregion
    }
}
