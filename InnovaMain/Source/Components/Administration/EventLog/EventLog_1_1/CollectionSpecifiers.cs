using System;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.MapTarget
{
	public class EventLogAllCollectionSpecifier : CalculationInterface.CollectionSpecifier
	{
        private int eventTypFilter;
        public int EventTypFilter { get { return eventTypFilter; } }

        private Guid cidFilter = Guid.Empty;
        public Guid CidFilter { get { return cidFilter; } }

        private static Guid TypeId = new Guid("D7FE4F30-D06F-11E0-9572-0800200C9A66");
		public EventLogAllCollectionSpecifier(int eventType, Guid cid) : base(TypeId)
		{
            eventTypFilter = eventType;
            cidFilter = cid; 
		}

        public EventLogAllCollectionSpecifier()
            : base(TypeId)
		{
		}
	}
}
