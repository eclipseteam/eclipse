﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;
using System.Xml.Serialization;

namespace Oritax.TaxSimp.Data
{
    [Serializable]
    public class EventLogEntity
    {
        [XmlElement("EventId")]
        public int? EventId { get; set; }
        [XmlElement("Name")]
        public string Name { get; set; }
        [XmlElement("UserName")]
        public string UserName { get; set; }
        [XmlElement("Type")]
        public string Type { get; set; }
        [XmlElement("DateTime")]
        public DateTime? DateTime { get; set; }
        [XmlElement("Detail")]
        public string Detail { get; set; }
    }


}
