using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;

using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Persistence;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.MapTarget;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.EventLog
{
    public class EventLogPersister : BrokerManagedPersister
    {
        public EventLogPersister(SqlConnection connection, SqlTransaction transaction) : base(connection, transaction) { }

        public virtual List<EventLogEntity> ListEventLogCollection()
        {
            List<EventLogEntity> eventLogEntities = new List<EventLogEntity>();
            return null;
        }

        public override DataTable ListBMCCollection(CollectionSpecifier bMCCollectionSpecifier)
        {
            EventLogAllCollectionSpecifier eventLogAllCollectionSpecifier = (EventLogAllCollectionSpecifier)bMCCollectionSpecifier;

            string selectCommand = "SELECT TOP 50000 [EVENTID] ,[EVENTCID], [DATETIME] ,[USERNAME] ,[NAME] ,[TYPE] ,[DETAIL] FROM [EVENTLOG] where Detail <> '' order by EventLog.DATETIME DESC";

            DataTable results = new DataTable();

            if (eventLogAllCollectionSpecifier.CidFilter != Guid.Empty)
                selectCommand = "SELECT * from [dbo].[EVENTLOG] where EVENTCID = '" + eventLogAllCollectionSpecifier.CidFilter.ToString() + "' AND DETAIL <> '' ORDER BY DATETIME ASC";
           
            SqlDataAdapter sda = new SqlDataAdapter(selectCommand, this.Connection);
            sda.SelectCommand.Transaction = this.Transaction;
            sda.SelectCommand.CommandType = CommandType.Text;
            sda.Fill(results);
            return results;
        }
    }
}
