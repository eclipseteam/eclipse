using System;

namespace Oritax.TaxSimp.EventLog
{
	/// <summary>
	/// Summary description for Install.
	/// </summary>
	public class EventLogInstall
	{
		#region INSTALLATION PROPERTIES

		// Assembly Installation Properties
        public const string ASSEMBLY_ID = "D7FE4F30-D06F-11E0-9572-0800200C9A66";
		public const string ASSEMBLY_NAME="EventLog_1_1";
		public const string ASSEMBLY_DISPLAYNAME="Event Log";
		public const string ASSEMBLY_MAJORVERSION="1";
		public const string ASSEMBLY_MINORVERSION="1";
		public const string ASSEMBLY_DATAFORMAT="0";

		public const string ASSEMBLY_REVISION="3";		//2005.1


		public const string ASSEMBLY_PERSISTERASSEMBLY="EventLog_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";
		public const string ASSEMBLY_PERSISTERCLASS="Oritax.TaxSimp.EventLog.EventLogPersister";

		// Component Installation Properties
        public const string COMPONENT_ID = "19DE52B0-D070-11E0-9572-0800200C9A66";
		public const string COMPONENT_NAME="EventLog";
		public const string COMPONENT_DISPLAYNAME="Event Log";
		public const string COMPONENT_CATEGORY="Framework";

		// Component Version Installation Properties
		public const string COMPONENTVERSION_STARTDATE="01/01/1990 00:00:00 AM";
		public const string COMPONENTVERSION_ENDDATE="31/12/2050 12:59:59 AM";
		public const string COMPONENTVERSION_PERSISTERASSEMBLY="EventLog_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";
        public const string COMPONENTVERSION_PERSISTERCLASS = "Oritax.TaxSimp.EventLog.EventLogPersister";
		public const string COMPONENTVERSION_IMPLEMENTATIONCLASS="Oritax.TaxSimp.EventLog.EventLogCM";

		#endregion
	}
}
