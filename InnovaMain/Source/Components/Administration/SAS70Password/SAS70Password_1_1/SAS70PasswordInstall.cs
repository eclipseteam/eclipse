using System;

namespace Oritax.TaxSimp.SAS70Password
{
	/// <summary>
	/// Summary description for SAS70PasswordInstall.
	/// </summary>
	public class SAS70PasswordInstall
	{
		#region Installation properties
		
		public const string ASSEMBLY_ID = "6D0500F5-B1F9-4053-88BC-FF4004D9E9D0";
		public const string ASSEMBLY_NAME = "SAS70Password_1_1";
		public const string ASSEMBLY_DISPLAYNAME = "SAS70 Password Policy V1.1";
		public const string ASSEMBLY_MAJORVERSION = "1";
		public const string ASSEMBLY_MINORVERSION = "1";
		public const string ASSEMBLY_DATAFORMAT = "0";
		public const string ASSEMBLY_REVISION = "0";	

		// Component Installation Properties
		public const string COMPONENT_ID = "7FD11578-35EF-4c42-A385-46EA23E1FEA2";
		public const string COMPONENT_NAME = "SAS70Password";
		public const string COMPONENT_DISPLAYNAME = "SAS70 Password Policy";
		public const string COMPONENT_CATEGORY = "Security";

		// Component Version Installation Properties
		public const string COMPONENTVERSION_STARTDATE = "01/01/1990 00:00:00 AM";
		public const string COMPONENTVERSION_ENDDATE = "31/12/2050 12:59:59 AM";
		public const string COMPONENTVERSION_PERSISTERASSEMBLY = "SAS70Password_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";
		public const string COMPONENTVERSION_PERSISTERCLASS = "Oritax.TaxSimp.SAS70Password.SAS70PasswordPersister";
		public const string COMPONENTVERSION_IMPLEMENTATIONCLASS = "Oritax.TaxSimp.SAS70Password.SAS70PasswordSM";

		// Data Model
		public const string ASSEMBLY_PERSISTERASSEMBLY="SAS70Password_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";
		public const string ASSEMBLY_PERSISTERCLASS="Oritax.TaxSimp.SAS70Password.SAS70PasswordPersister";
		
	    #endregion
		
	}
}
