using System;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.Serialization;
using System.Globalization;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.SAS70Password
{
	/// <summary>
	/// Summary description for PasswordHistoryDS.
	/// </summary>
	[Serializable]
	public class PasswordHistoryDS : DataSet
	{
		public const string PasswordHistoryTable			= "PasswordHistory";
		public const string PasswordUserIdField				= "CID";
		public const string PasswordUserNameField			= "UserName";
		public const string PasswordPasswordField			= "Password";
		public const string PasswordCreationDateTimeField	= "CreationDateTime";
		public const string PasswordIsCurrentField			= "IsCurrent";

		public const string PasswordKeyField				= "Key";
		public const string PasswordDescriptionField		= "Description";
		public const string PasswordResultField				= "Result";

		public const string PasswordLengthKey				= "PwdLength";
		public const string PasswordUpperCharsKey			= "PwdUpperChar";
		public const string PasswordLowerCharsKey			= "PwdLowerChar";
		public const string PasswordDigitCharsKey			= "PwdDigitChar";
		public const string PasswordNonAlphaCharsKey			= "PwdNonAlaphChar";
		public const string PasswordNoUserNameKey			= "PwdNoUserName";
		public const string PasswordNoHistoricalKey			= "PwdNoHistorical";
		public const string PasswordNoRepeatingCharsKey		= "PwdNoRepeatingChar";

		public const string PasswordLength					= "{0}.&nbsp;Between {1} and {2} characters long";
		public const string PasswordUpperChars				= "{0}.&nbsp;Contain at least one alphabetic uppercase character(A through Z)";
		public const string PasswordLowerChars				= "{0}.&nbsp;Contain at least one alphabetic lowercase character(a through z)";
		public const string PasswordDigitChars				= "{0}.&nbsp;Contain at least one number(0 through 9)";
		public const string PasswordNonAlphaChars			= "{0}.&nbsp;Contain at least one non alphanumeric characters('~!@#$%^&*()_-+=[]{1}|\\:;\"<>,.?/)";
		public const string PasswordNoUserName				= "{0}.&nbsp;Not contain the UserName";
		public const string PasswordNoHistorical			= "{0}.&nbsp;{1} unique new passwords must be used before an old password can be reused.";
		public const string PasswordNoRepeatingChars		= "{0}.&nbsp;Cannot repeat the same character in sequence more than {1} times<br/>&nbsp;&nbsp;&nbsp;&nbsp;Example: If the same character cannot repeated in sequence more than 2 times, then \"helllloo\",\"AAA\",\"hellooo\" would be invalid,<br/>&nbsp;&nbsp;&nbsp;&nbsp;\"hello\",\"helloo\",\"AA\" would be valid";		

		public PasswordHistoryDS() 
		{
			DataTable dtHistory = new DataTable( PasswordHistoryTable );
			dtHistory.Locale = CultureInfo.InvariantCulture;
			dtHistory.Columns.Add(new DataColumn( PasswordUserIdField, typeof( System.Guid ) ) );
			dtHistory.Columns.Add(new DataColumn( PasswordUserNameField, typeof( System.String ) ) );
			dtHistory.Columns.Add(new DataColumn( PasswordPasswordField, typeof( System.String ) ) );
			dtHistory.Columns.Add(new DataColumn( PasswordCreationDateTimeField, typeof( System.DateTime ) ) );
			dtHistory.Columns.Add(new DataColumn( PasswordIsCurrentField, typeof( System.Boolean ) ) );
			
			this.Tables.Add( dtHistory );
			BrokerManagedComponentDS.AddVersionStamp( dtHistory, typeof( PasswordHistoryDS ) );	
		}
		
		protected PasswordHistoryDS(SerializationInfo si, StreamingContext context) : base(si, context) {}
    }
}
