using System;
using System.Text.RegularExpressions;
using System.Text;
using System.Reflection;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Data;
using System.Collections;
using System.Globalization;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.CM;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Utilities;

[assembly: System.Runtime.InteropServices.ComVisible(false)]
namespace Oritax.TaxSimp.SAS70Password
{
	/// <summary>
	/// Summary description for SAS70PasswordSM.
	/// </summary>
	[Serializable]
	public class SAS70PasswordSM : SystemModuleBase, ISerializable, IDisposable
	{
		#region Public constant members
		public const int MinLengthAllow = 7;
		public const int MaxLengthAllow = 50;
		public const int MinHistoryAllow = 3;
		public const int MaxHistoryAllow	= 10;
		public const int MinRepeatingCharsAllow = 2;
		public const int MaxRepeatingCharsAllow = 6;
		public const int MinExpiryDaysAllow = 1;
		public const int MaxExpiryDaysAllow = 90;
		public const int MinReminderDaysAllow = 1;
		public const int MaxReminderDaysAllow = 10;
		public const int MinFailureAttemptsAllow = 1;
		public const int MaxFailureAttemptsAllow = 3;

		public const int DefaultMinLength = 7;
		public const int DefaultMaxLength = 50;
		public const int DefaultHistory = 3;
		public const int DefaultRepeatingChars = 2;
		public const int DefaultExpiryDays = 90;
		public const int DefaultReminderDays = 10;
		public const int DefaultFailureAttempts = 3;
		#endregion

		#region Private properties

		private int passwordMinLength = DefaultMinLength;
		private int passwordMaxLength = DefaultMaxLength;
		private int passwordHistory = DefaultHistory;
		private int maxRepeatingChars = DefaultRepeatingChars;
		private int validTimeSpan = DefaultExpiryDays;
		private int reminderTimeSpan = DefaultReminderDays;
		private int maxFailureAttempts = DefaultFailureAttempts;
		private PasswordHistoryDS dsHitory = new PasswordHistoryDS();
		[NonSerialized] private bool disposed;		

		#endregion

		#region Public property
		public int PasswordMinLength
		{
			get
			{
				return this.passwordMinLength;
			}
			set
			{
				this.passwordMinLength = value;
			}
		}
		
		public int PasswordMaxLength
		{
			get
			{
				return this.passwordMaxLength;	
			}
			set
			{
				this.passwordMaxLength = value;
			}
		}

		public int PasswordHistory
		{
			get
			{
				return this.passwordHistory;
			}
			set
			{
				this.passwordHistory = value;
			}
		}

		public int MaxRepeatingChars
		{
			get
			{
				return this.maxRepeatingChars;
			}
			set
			{
				this.maxRepeatingChars = value;
			}
		}

		public int ValidTimeSpan
		{
			get
			{
				return this.validTimeSpan;
			}
			set
			{
				this.validTimeSpan = value;
			}
		}

		public int ReminderTimeSpan
		{
			get
			{
				return this.reminderTimeSpan;
			}
			set
			{
				this.reminderTimeSpan = value;	
			}
		}

		public int MaxFailureAttempts
		{
			get
			{
				return this.maxFailureAttempts;
			}
			set
			{
				this.maxFailureAttempts = value;
			}
		}
        #endregion

		#region IDisposable Implementation ---------------------------------------

		/// <summary>
		///		Performs application-defined tasks associated with freeing, releasing, 
		///		or resetting unmanaged resources.
		/// </summary>
		public void Dispose() 
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		/// <summary>
		///		Performs application-defined tasks associated with freeing, releasing, 
		///		or resetting unmanaged resources.
		/// </summary>
		/// <param name="disposing">
		///		True if called by the Dispose() method, false if called by the finalizer.
		/// </param>
		protected virtual void Dispose(bool disposing) 
		{
			if (!disposed)
			{
				if (disposing)
				{
					this.dsHitory.Dispose();
				}
				disposed = true;
			}
		}

		#endregion // IDisposable Implementation
        		
		#region Overriden members
				
		protected override void OnGetData(DataSet data)
		{
			base.OnGetData(data);
			if ( data is PasswordPolicyDS )
			{
				SetDataToDS(data, PasswordPolicyDS.SettingMinLengthKey, this.PasswordMinLength);
				SetDataToDS(data, PasswordPolicyDS.SettingMaxLengthKey, this.PasswordMaxLength);
				SetDataToDS(data, PasswordPolicyDS.SettingHistoryKey, this.PasswordHistory);
				SetDataToDS(data, PasswordPolicyDS.SettingRepeatingCharsKey, this.MaxRepeatingChars);
				SetDataToDS(data, PasswordPolicyDS.SettingExpirySpanKey, this.ValidTimeSpan);
				SetDataToDS(data, PasswordPolicyDS.SettingReminderSpanKey, this.ReminderTimeSpan);
				SetDataToDS(data, PasswordPolicyDS.SettingLockoutAttemptsKey, this.MaxFailureAttempts);
			}
		}

		protected override ModifiedState OnSetData(DataSet data)
		{
			base.OnSetData (data);
			if ( data is PasswordPolicyDS )
			{
				this.PasswordMinLength = Int32.Parse(ExtractDataFromDS(data, PasswordPolicyDS.SettingMinLengthKey),NumberFormatInfo.InvariantInfo);
				this.PasswordMaxLength = Int32.Parse(ExtractDataFromDS(data, PasswordPolicyDS.SettingMaxLengthKey),NumberFormatInfo.InvariantInfo);
				this.PasswordHistory = Int32.Parse(ExtractDataFromDS(data, PasswordPolicyDS.SettingHistoryKey),NumberFormatInfo.InvariantInfo);
				this.MaxRepeatingChars = Int32.Parse(ExtractDataFromDS(data, PasswordPolicyDS.SettingRepeatingCharsKey),NumberFormatInfo.InvariantInfo);
				this.ValidTimeSpan = Int32.Parse(ExtractDataFromDS(data, PasswordPolicyDS.SettingExpirySpanKey),NumberFormatInfo.InvariantInfo);
				this.ReminderTimeSpan = Int32.Parse(ExtractDataFromDS(data, PasswordPolicyDS.SettingReminderSpanKey),NumberFormatInfo.InvariantInfo);
				this.MaxFailureAttempts = Int32.Parse(ExtractDataFromDS(data, PasswordPolicyDS.SettingLockoutAttemptsKey),NumberFormatInfo.InvariantInfo);	
				
				return ModifiedState.MODIFIED;
			}
			return ModifiedState.UNCHANGED;
		}

		public override DataSet PrimaryDataSet
		{
			get
			{
				return new PasswordPolicyDS();
			}
		}

		private string ExtractDataFromDS(DataSet ds, string key)
		{
			DataRow[] rows = ds.Tables[PasswordPolicyDS.PolicySettingsTable].Select(PasswordPolicyDS.SettingNameField + " ='" + key + "'", String.Empty, DataViewRowState.CurrentRows);
			if ( rows.Length > 0 )
				return rows[0][PasswordPolicyDS.SettingValueField].ToString();
			return null;
		}

		private void SetDataToDS(DataSet ds, string key, int newValue)
		{
			DataRow[] rows = ds.Tables[PasswordPolicyDS.PolicySettingsTable].Select(PasswordPolicyDS.SettingNameField + " ='" + key + "'", String.Empty, DataViewRowState.CurrentRows);
			if ( rows.Length > 0 )
			{
				rows[0][PasswordPolicyDS.SettingValueField] = newValue;
			}	
		}

		#endregion
		
		#region Public Constructors
		public SAS70PasswordSM() : base() {}
		
		protected SAS70PasswordSM(SerializationInfo si, StreamingContext context)  : base(si, context)
		{
			this.PasswordMinLength = Serialize.GetSerializedInt32(si, "sas70_password_minLength");
			this.PasswordMaxLength = Serialize.GetSerializedInt32(si, "sas70_password_maxLength");
			this.PasswordHistory = Serialize.GetSerializedInt32(si, "sas70_password_history");
			this.MaxRepeatingChars = Serialize.GetSerializedInt32(si, "sas70_password_maxRepeatingChars");
			this.ValidTimeSpan = Serialize.GetSerializedInt32(si, "sas70_password_validTimeSpan");
			this.ReminderTimeSpan = Serialize.GetSerializedInt32(si, "sas70_password_reminderTimeSpan");
			this.MaxFailureAttempts = Serialize.GetSerializedInt32(si, "sas70_password_maxFailureAttempts");
			this.dsHitory = ( PasswordHistoryDS) Serialize.GetSerializedValue(si, "sas70_password_historyDS", typeof(PasswordHistoryDS), new PasswordHistoryDS() );
		}
		
		//Only allow the .NET Serialization core code to call this function
		[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]	
		public override void GetObjectData(System.Runtime.Serialization.SerializationInfo si, System.Runtime.Serialization.StreamingContext context)
		{
			base.GetObjectData (si, context);
			Serialize.AddSerializedValue(si, "sas70_password_minLength", this.PasswordMinLength);
			Serialize.AddSerializedValue(si, "sas70_password_maxLength", this.PasswordMaxLength);
			Serialize.AddSerializedValue(si, "sas70_password_history", this.PasswordHistory);
			Serialize.AddSerializedValue(si, "sas70_password_maxRepeatingChars", this.MaxRepeatingChars);
			Serialize.AddSerializedValue(si, "sas70_password_validTimeSpan", this.ValidTimeSpan);
			Serialize.AddSerializedValue(si, "sas70_password_reminderTimeSpan", this.ReminderTimeSpan);
			Serialize.AddSerializedValue(si, "sas70_password_maxFailureAttempts", this.MaxFailureAttempts);
			Serialize.AddSerializedValue(si, "sas70_password_historyDS", this.dsHitory);
		}
        
		#endregion

		#region Member methods
		public bool IsPasswordExpired(Guid userId, DateTime currentDate)
		{
			DataRow row = this.GetUserCurrentRow(userId);
			if ( row != null )	
			{
				DateTime creationDate = (DateTime) row[PasswordHistoryDS.PasswordCreationDateTimeField];
				int existingDays = currentDate.Subtract(creationDate).Days;
				return existingDays > this.ValidTimeSpan;
			}
			if ( this.GetWholePasswordHistoryByUser(userId).Length == 0 )
			{
				return false;
			}
			else
			{
				return true;
			}				
		}

		public bool IsInReminderTimeSpan(Guid userId, DateTime currentDate)
		{
			//Retrieve the creation date time and compare with the current system time
			DataRow row = this.GetUserCurrentRow(userId);
			if ( row != null )
			{
				DateTime pwdCreationDate = (DateTime) row[PasswordHistoryDS.PasswordCreationDateTimeField];
				int existingDays = (currentDate - pwdCreationDate).Days;
				return existingDays >= (this.ValidTimeSpan - this.ReminderTimeSpan);
			}
			return false;
		}
    	
		private bool CheckPasswordLength(string pwd, DataTable table)
		{
			bool result = (pwd.Length >= this.PasswordMinLength && pwd.Length <= this.PasswordMaxLength);
			SetResultInTable(table, PasswordHistoryDS.PasswordLengthKey, !result);
			return result;
		}

		private bool CheckCharCombination(string pwd, DataTable table)
		{
			bool result = false;
			string patternLl = @"\p{Ll}";
			string patternLu = @"\p{Lu}";
			string patternNd = @"\p{Nd}";
			string patternNonAlpha = "`~!@#$%^&*()_-+{}[]|\\'\"<>,.?/;=: ";
			Regex ex = new Regex(patternLl, RegexOptions.Singleline | RegexOptions.Compiled );
			bool isLlComp = ex.IsMatch(pwd);
			SetResultInTable(table, PasswordHistoryDS.PasswordLowerCharsKey, !isLlComp);
			ex = new Regex(patternLu, RegexOptions.Singleline | RegexOptions.Compiled );
			bool isLuComp = ex.IsMatch(pwd);
			SetResultInTable(table, PasswordHistoryDS.PasswordUpperCharsKey, !isLuComp);
			ex = new Regex(patternNd, RegexOptions.IgnoreCase | RegexOptions.Singleline | RegexOptions.Compiled );
			bool isNdComp = ex.IsMatch(pwd);
			SetResultInTable(table, PasswordHistoryDS.PasswordDigitCharsKey, !isNdComp);
			bool hasNonAlphaChars = ContainNonAlphaChars(pwd,patternNonAlpha);
			SetResultInTable(table, PasswordHistoryDS.PasswordNonAlphaCharsKey, !hasNonAlphaChars);
			result = isLlComp && isLuComp && isNdComp && hasNonAlphaChars;
			
			return result;
		}

		private bool ContainNonAlphaChars(string pwd, string pattern)
		{
			bool result = false;
			for( int index = 0; index < pattern.Length; index++ )
			{
				if ( pwd.IndexOf(pattern[index].ToString()) != -1 )
				{
					result = true;
					break;
				} 
			}
			return result;
		}

		private bool CheckPasswordExpired(Guid userId, DateTime currentDate)
		{
			return this.IsPasswordExpired(userId, currentDate);					
		}

		private bool CheckIdentialToUserName(string UserName, string pwd, DataTable table)
		{
			bool result = false;
			if ( UserName.Length != 0 )
			{
				result = (pwd.ToLower(CultureInfo.InvariantCulture).IndexOf(UserName.ToLower(CultureInfo.InvariantCulture)) != -1);
			}
			SetResultInTable(table,PasswordHistoryDS.PasswordNoUserNameKey, result);
			return result;
		}
		
		private bool CheckRepeatingChars(string pwd, DataTable table)
		{
			bool result = true;
			int slash = pwd.IndexOf(@"\");

			char[] charArray = ExtractChars(pwd);

			if ( charArray != null )
			{
				foreach( char c in charArray )
				{
					if (((int) c != 0) && ((int) c !=92) && ((int) c !=94))
					{
						string patternString = @"[" + c.ToString(CultureInfo.InvariantCulture) + "]{" + (this.MaxRepeatingChars+1).ToString(NumberFormatInfo.InvariantInfo) + ",}";
						Regex ex = new Regex(patternString, RegexOptions.IgnoreCase | RegexOptions.Singleline | RegexOptions.Compiled );
						if ( ex.IsMatch(pwd) )
						{
							result = false;
							break;
						}
					}
					else
					{
						break;
					}
				}
			}
			else
				result = false;
			SetResultInTable(table,PasswordHistoryDS.PasswordNoRepeatingCharsKey, !result);
			return result;
		}
        
		private bool CheckIdenticalToPreviousPassword(Guid userId, string pwd, DataTable table, bool skipCurrent)
		{
            DataRow row = null;
            string pastPassword = string.Empty;
			bool result = false;
			DataRow[] rows = this.GetLimitedPasswordHistoryByUser(userId, this.PasswordHistory);

            for (int i = 0; i < rows.Length; i++)
            {
                row = rows[i];

                pastPassword = Encryption.DecryptData(row[PasswordHistoryDS.PasswordPasswordField].ToString());
     
                //for some reason the first password stored is encrypted twice so in order to compare it must
                // be decrypted twice.
                try
                {
                    pastPassword = Encryption.DecryptData(pastPassword);
                }
                catch
                { 
                
                }


                if (pastPassword.ToLower(CultureInfo.InvariantCulture).CompareTo(pwd.ToLower(CultureInfo.InvariantCulture)) == 0 && (skipCurrent || !Convert.ToBoolean(row[PasswordHistoryDS.PasswordIsCurrentField].ToString(), CultureInfo.InvariantCulture)))
                {
                    result = true;
                    break;
                }
            }

			SetResultInTable(table,PasswordHistoryDS.PasswordNoHistoricalKey, result);			
			return result;
		}

		public bool CheckAllRulesAtLogin(Guid userId,string userName,string password, DataTable table, DateTime currentDate, bool skipCurrent)
		{
			return ( this.CheckPasswordLength(password,table) & 
				CheckCharCombination(password, table) & 
				this.CheckRepeatingChars(password, table) & 
				!CheckIdentialToUserName(userName, password, table) );
		}

		public bool CheckAllRules(Guid userId,string userName,string password, DataTable table, DateTime currentDate, bool skipCurrent)
		{
			return ( this.CheckPasswordLength(password,table) & 
					 CheckCharCombination(password, table) & 
					 this.CheckRepeatingChars(password, table) & 
					 !this.CheckIdenticalToPreviousPassword(userId, password, table, skipCurrent) & 
					 !CheckIdentialToUserName(userName, password, table) );
		}

		//Check the password for new user. Only apply the capible rules
		public bool CheckRulesForNewUser(string userName, string password, DataTable table)
		{
			return ( this.CheckPasswordLength(password,table) & 
					 CheckCharCombination(password, table) & 
					 this.CheckRepeatingChars(password, table) & 
					 !CheckIdentialToUserName(userName, password, table) );	
		}

		private char[] ExtractChars(string s)
		{
			if ( s.Length > 0 )
			{
				char[] charArray = s.ToCharArray();
				Array.Sort(charArray);
				char[] returnArray = new char[s.Length];
				returnArray[0] = charArray[0];
				int returnIndex = 0;
				for ( int index = 1; index < s.Length; index++ )
				{
					if ( charArray[index] != charArray[index - 1] )
					{
						returnIndex++;
						returnArray[returnIndex] = charArray[index];						
					}
				}
				Array.Clear(returnArray, returnIndex+1,s.Length - (returnIndex+1));
				return returnArray;
			}
			else
				return null;
		}
        		
		public DataRow[] GetWholePasswordHistoryByUser(Guid userId)
		{
			DataRow[] history = this.dsHitory.Tables[PasswordHistoryDS.PasswordHistoryTable].Select(PasswordHistoryDS.PasswordUserIdField + " = '" + userId.ToString() + "'", PasswordHistoryDS.PasswordCreationDateTimeField + " DESC", DataViewRowState.CurrentRows);
			return history;
		}

		public DataRow[] GetLimitedPasswordHistoryByUser(Guid userId, int limitation)
		{
			DataRow[] wholeHistory = this.GetWholePasswordHistoryByUser(userId);
			if ( wholeHistory.Length <= limitation )
			{
				return wholeHistory;
			}
			DataRow[] limitedHistory = new DataRow[limitation];
			for ( int index = 0; index < limitation; index++ )
			{
				limitedHistory[index] = wholeHistory[index];		
			}
			return limitedHistory;
		}

		public DataRow GetUserCurrentRow(Guid userId)
		{
			string filter = String.Format(CultureInfo.InvariantCulture,"{0} = 'True' AND {1} = '{2}'", PasswordHistoryDS.PasswordIsCurrentField, PasswordHistoryDS.PasswordUserIdField, userId.ToString("D",CultureInfo.InvariantCulture));
			DataRow[] rows = this.dsHitory.Tables[PasswordHistoryDS.PasswordHistoryTable].Select(filter, String.Empty, DataViewRowState.CurrentRows);
			if ( rows.Length == 1 )
			{
				return rows[0];
			}
			return null;
		}

		public void InsertCurrentPassword(Guid userId, string userName, string password)
		{
			this.UpdateHistoryPassword(userId);
			DataTable table = this.dsHitory.Tables[PasswordHistoryDS.PasswordHistoryTable];
			DataRow row = table.NewRow();
			row[PasswordHistoryDS.PasswordUserIdField] = userId;
			row[PasswordHistoryDS.PasswordUserNameField] = userName;
			row[PasswordHistoryDS.PasswordPasswordField] = Encryption.EncryptData(password);
			row[PasswordHistoryDS.PasswordCreationDateTimeField] = DateTime.Now;
			row[PasswordHistoryDS.PasswordIsCurrentField] = true;
			table.Rows.Add(row);
			table.AcceptChanges();
			//if the number of history entries is bigger than the MAXHISTORY, then trunc the oldest entry
			DataRow[] rows = this.GetWholePasswordHistoryByUser(userId);
			if ( rows.Length > SAS70PasswordSM.MaxHistoryAllow )
			{
				rows[rows.Length-1].Delete();
				table.AcceptChanges();	
			}		

			this.CalculateToken(true);
		}

		public void UpdateHistoryPassword(Guid userId)
		{
			DataRow row = this.GetUserCurrentRow(userId);
			if ( row != null )
			{
				row[PasswordHistoryDS.PasswordIsCurrentField] = false;
				DataTable table = this.dsHitory.Tables[PasswordHistoryDS.PasswordHistoryTable];
				table.AcceptChanges();
			}
		}

		private void SetResultInTable(DataTable table,string key,bool result)
		{
			DataRow targetRow = FindRowByKey(table, key);
			if ( targetRow != null )
			{
				targetRow[PasswordHistoryDS.PasswordResultField] = result;	
			}
		}
		
		private DataRow FindRowByKey(DataTable table,string key)
		{
			DataRow[] rows = table.Select(PasswordHistoryDS.PasswordKeyField + " = '" + key + "'", String.Empty, DataViewRowState.CurrentRows);
			if ( rows.Length == 1 )
			{
				return rows[0];
			}
			return null;
		}
		
		public int GetExpirySpan(Guid userId, DateTime date)
		{
			//Get the days remaining before current password expired.
			DataRow row = this.GetUserCurrentRow(userId);
			if ( row != null )
			{
				DateTime creationDate = (DateTime) row[PasswordHistoryDS.PasswordCreationDateTimeField];
				DateTime expiryDate = creationDate.Date.AddDays(this.ValidTimeSpan);
				return expiryDate.Subtract(date.Date).Days; 
			}
			return Int32.MinValue;
		
		}
        
		public bool IsUserWithHistory(Guid userId)
		{
			DataRow[] rows = this.GetWholePasswordHistoryByUser(userId);
			return (rows.Length > 0);
		}

		public void TruncateHistory(Guid userId)
		{
			DataRow[] rows = this.GetWholePasswordHistoryByUser(userId);
			foreach( DataRow row in rows )
			{
				row.Delete();
			}
			this.dsHitory.Tables[PasswordHistoryDS.PasswordHistoryTable].AcceptChanges();	
		}
				       
        #endregion
	}
}
