using System;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Runtime.Serialization;
using System.Security.Permissions;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.SAS70Password
{
	/// <summary>
	/// Summary description for PasswordPolicyDS.
	/// </summary>
	[Serializable]
	public class PasswordPolicyDS : DataSet, ISerializable
	{
		public const string PolicySettingsTable				= "PolicySettings";
		public const string SettingNameField				= "SettingName";
		public const string SettingValueField				= "SettingValue";

		public const string SettingMinLengthKey				= "PasswordMinLength";
		public const string SettingMaxLengthKey 			= "PasswordMaxLength";
		public const string SettingHistoryKey				= "NoRepeatingHistory";
		public const string SettingRepeatingCharsKey		= "RepeatingChars";
		public const string SettingExpirySpanKey			= "ExpiryTimeSpan";
		public const string SettingReminderSpanKey			= "ReminderTimeSpan";
		public const string SettingLockoutAttemptsKey		= "MaxFailureAttemps";	
	
		public PasswordPolicyDS()  : base()
		{
			DataTable dt = new DataTable(PolicySettingsTable);
			dt.Locale = CultureInfo.InvariantCulture;
			dt.Columns.Add(new DataColumn(SettingNameField, typeof(System.String)));
			dt.Columns.Add(new DataColumn(SettingValueField, typeof(System.Int32)));
			this.Tables.Add(dt);
			BrokerManagedComponentDS.AddVersionStamp(dt, typeof(PasswordPolicyDS));
			
			DataRow row = dt.NewRow();
			row[SettingNameField] = SettingMinLengthKey;
			row[SettingValueField] = 0;
			dt.Rows.Add(row);
			row = dt.NewRow();
			row[SettingNameField] = SettingMaxLengthKey;
			row[SettingValueField] = 0;
			dt.Rows.Add(row);
			row = dt.NewRow();
			row[SettingNameField] = SettingHistoryKey;
			row[SettingValueField] = 0;
			dt.Rows.Add(row);
			row = dt.NewRow();
			row[SettingNameField] = SettingRepeatingCharsKey;
			row[SettingValueField] = 0;
			dt.Rows.Add(row);
			row = dt.NewRow();
			row[SettingNameField] = SettingExpirySpanKey;
			row[SettingValueField] = 0;
			dt.Rows.Add(row);
			row = dt.NewRow();
			row[SettingNameField] = SettingReminderSpanKey;
			row[SettingValueField] = 0;
			dt.Rows.Add(row);
			row = dt.NewRow();
			row[SettingNameField] = SettingLockoutAttemptsKey;
			row[SettingValueField] = 0;
			dt.Rows.Add(row);
		}
	
		protected PasswordPolicyDS(SerializationInfo info,StreamingContext context) : base(info, context) {}
			
    }
}
