using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Persistence;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.CommonPersistence;


namespace Oritax.TaxSimp.SAS70Password
{
	/// <summary>
	/// Summary description for SAS70PasswordPersister.
	/// </summary>
	public class SAS70PasswordPersister : BlobPersister, IDisposable
	{
		private SqlDataAdapter dsCommand;
		private bool disposed;

		public SAS70PasswordPersister(SqlConnection connection, SqlTransaction transaction) : base(connection, transaction)
		{
			//
			// Create the adapter
			//
			dsCommand = new SqlDataAdapter();

			//
			// Create the DataSetCommand SelectCommand
			//
			dsCommand.SelectCommand = DBCommandFactory.Instance.NewSqlCommand("SELECT "+BrokerManagedComponentDS.CINSTANCES_TABLE+".CID,"+BrokerManagedComponentDS.CINSTANCES_TABLE+".NAME,"+BrokerManagedComponentDS.CINSTANCES_TABLE+".CTID,"+BrokerManagedComponentDS.CINSTANCES_TABLE+".UPDATETOKEN FROM "+BrokerManagedComponentDS.CINSTANCES_TABLE+" WHERE "+BrokerManagedComponentDS.CINSTANCES_TABLE+".CID = @CID" );
			dsCommand.SelectCommand.CommandType = CommandType.Text;
			dsCommand.SelectCommand.Connection=connection;
			dsCommand.SelectCommand.Transaction=transaction;

			dsCommand.InsertCommand = DBCommandFactory.Instance.NewSqlCommand();
			dsCommand.InsertCommand.Connection=connection;
			dsCommand.InsertCommand.Transaction=transaction;

			dsCommand.UpdateCommand = DBCommandFactory.Instance.NewSqlCommand();
			dsCommand.UpdateCommand.Connection=connection;
			dsCommand.UpdateCommand.Transaction=transaction;

			dsCommand.DeleteCommand = DBCommandFactory.Instance.NewSqlCommand();
			dsCommand.DeleteCommand.Connection=connection;
			dsCommand.DeleteCommand.Transaction=transaction;
		}

		#region IDisposable Implementation ---------------------------------------

		/// <summary>
		///		Performs application-defined tasks associated with freeing, releasing, 
		///		or resetting unmanaged resources.
		/// </summary>
		public new void Dispose() 
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		/// <summary>
		///		Performs application-defined tasks associated with freeing, releasing, 
		///		or resetting unmanaged resources.
		/// </summary>
		/// <param name="disposing">
		///		True if called by the Dispose() method, false if called by the finalizer.
		/// </param>
		protected virtual new void Dispose(bool disposing) 
		{
			base.Dispose();
			if (!disposed)
			{
				if (disposing)
				{
					dsCommand.Dispose();
				}
				disposed = true;
			}
		}

		#endregion // IDisposable Implementation

		public override void EstablishDatabase()
		{
			base.EstablishDatabase();
		}

		public override SqlConnection Connection
		{
			set
			{
				base.Connection=value;

				dsCommand.InsertCommand.Connection=value;
				dsCommand.SelectCommand.Connection=value;
				dsCommand.UpdateCommand.Connection=value;
				dsCommand.DeleteCommand.Connection=value;
			}
		}

		public override SqlTransaction Transaction
		{
			set
			{
				base.Transaction=value;

				dsCommand.InsertCommand.Transaction=value;
				dsCommand.SelectCommand.Transaction=value;
				dsCommand.UpdateCommand.Transaction=value;
				dsCommand.DeleteCommand.Transaction=value;
			}
		}

		public override void Save(IBrokerManagedComponent iBMC, ref DataSet persistedPrimaryDS)
		{
			base.Save(iBMC, ref persistedPrimaryDS);

			this.dsCommand.UpdateCommand.CommandText = "UPDATE "+BrokerManagedComponentDS.CINSTANCES_TABLE+" SET CTID=@CTID, NAME=@NAME, UPDATETOKEN=@UPDATETOKEN WHERE CID=@CID";
			this.dsCommand.UpdateCommand.CommandType = CommandType.Text;
			this.dsCommand.UpdateCommand.Parameters.Add(new SqlParameter("@CID", SqlDbType.UniqueIdentifier,16,DSBMCInfo.CMID_FIELD));
			this.dsCommand.UpdateCommand.Parameters.Add(new SqlParameter("@NAME", SqlDbType.VarChar,128,DSBMCInfo.CMNAME_FIELD));
			this.dsCommand.UpdateCommand.Parameters.Add(new SqlParameter("@CTID", SqlDbType.UniqueIdentifier,16,DSBMCInfo.CMTYPEID_FIELD));
			this.dsCommand.UpdateCommand.Parameters.Add(new SqlParameter("@UPDATETOKEN", SqlDbType.Image,16,DSBMCInfo.CMUPDATETOKEN_FIELD));

			this.dsCommand.InsertCommand.CommandText = "INSERT INTO "+BrokerManagedComponentDS.CINSTANCES_TABLE+" (CID,NAME,CTID,UPDATETOKEN) VALUES(@CID,@NAME,@CTID,@UPDATETOKEN)";
			this.dsCommand.InsertCommand.CommandType = CommandType.Text;
			this.dsCommand.InsertCommand.Parameters.Add(new SqlParameter("@CID", SqlDbType.UniqueIdentifier,16,DSBMCInfo.CMID_FIELD));
			this.dsCommand.InsertCommand.Parameters.Add(new SqlParameter("@NAME", SqlDbType.VarChar,128,DSBMCInfo.CMNAME_FIELD));
			this.dsCommand.InsertCommand.Parameters.Add(new SqlParameter("@CTID", SqlDbType.UniqueIdentifier,16,DSBMCInfo.CMTYPEID_FIELD));
			this.dsCommand.InsertCommand.Parameters.Add(new SqlParameter("@UPDATETOKEN", SqlDbType.Image,16,DSBMCInfo.CMUPDATETOKEN_FIELD));

			DSBMCInfo data = new DSBMCInfo();
			if ( iBMC != null )
			{
				dsCommand.SelectCommand.Parameters.Add(new SqlParameter("@CID", SqlDbType.UniqueIdentifier,16,DSBMCInfo.CMID_FIELD)).Value=iBMC.CID;
			
				dsCommand.Fill(data,DSBMCInfo.CMLIST_TABLE);

				if(data.Tables[DSBMCInfo.CMLIST_TABLE].Rows.Count==0)
					data = new DSBMCInfo(iBMC.CID,iBMC.Name,iBMC.TypeID,iBMC.UpdateToken.Hash);

				data.Tables[DSBMCInfo.CMLIST_TABLE].Rows[0][DSBMCInfo.CMID_FIELD]=iBMC.CID;
				data.Tables[DSBMCInfo.CMLIST_TABLE].Rows[0][DSBMCInfo.CMNAME_FIELD]=iBMC.Name;
				data.Tables[DSBMCInfo.CMLIST_TABLE].Rows[0][DSBMCInfo.CMTYPEID_FIELD]=iBMC.TypeID;
				data.Tables[DSBMCInfo.CMLIST_TABLE].Rows[0][DSBMCInfo.CMUPDATETOKEN_FIELD]=iBMC.UpdateToken.Hash;

				dsCommand.TableMappings.Add("Table", DSBMCInfo.CMLIST_TABLE);

				dsCommand.Update(data);
			}
		}

		public override void Delete(IBrokerManagedComponent iBMC)
		{
			base.Delete(iBMC,new BrokerManagedComponentDS());
			
			SqlParameter cid = new SqlParameter("@CID", SqlDbType.UniqueIdentifier,16,DSBMCInfo.CMID_FIELD);

			this.dsCommand.DeleteCommand.CommandText = "DELETE FROM "+BrokerManagedComponentDS.CINSTANCES_TABLE+" WHERE CID";
			this.dsCommand.DeleteCommand.CommandType = CommandType.Text;
			this.dsCommand.DeleteCommand.Parameters.Add(cid);

			DSBMCInfo data=new DSBMCInfo();
			dsCommand.Fill(data);

			foreach(DataRow row in data.Tables[DSBMCInfo.CMID_FIELD].Rows)
				row.Delete();

			dsCommand.Update(data);
		}
	}
}
