using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Calculation;

using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.Security
{
	/// <summary>
	/// Summary description for Customer SecurityGroupPersister.
	/// </summary>

	public class SecurityGroupPersister : BrokerManagedComponentPersister, ICPersister, IDisposable
	{
		public const String SECGRP_INSERT_SP	= "SECURITY_SECURITYGRP_INSERT_1_0_0";
		public const String SECGRP_UPDATE_SP	= "SECURITY_SECURITYGRP_UPDATE_1_0_0";
		public const String SECGRP_DELETE_SP	= "SECURITY_SECURITYGRP_DELETE_1_0_0";
		public const String SECGRP_SELECT_SP	= "SECURITY_SECURITYGRP_SELECT_1_0_0";
		public const String SECGRP_SELECTALL_SP = "SECURITY_SECURITYGRP_SELECTALL_1_0_0";

		private const String USER_SELECTALL_SP	= "SECURITY_USER_SELECTALL_1_0_0";
		private Guid USER_CTID					= new Guid("{3D98A779-ADF0-4BF5-940D-220EE57ACBD9}");

		public const String USERS_AVAILABLE_SELECT_SP	= "SECURITY_AVAILABLEUSERS_SELECT_1_0_0";
		public const String USERS_SELECTED_SELECT_SP	= "SECURITY_SELECTEDUSERS_SELECT_1_0_0";
		public const String USERS_SELECTED_DELETE_SP	= "SECURITY_SELECTEDUSERS_DELETE_1_0_0";
		public const String USERS_SELECTED_INSERT_SP	= "SECURITY_SELECTEDUSERS_INSERT_1_0_0";

		private SqlDataAdapter sdaSecurityGroup;
		private SqlDataAdapter sdaAvailableUsers;
		private SqlDataAdapter sdaSelectedUsers;

		private string cinstancesTableName = BrokerManagedComponentDS.CINSTANCES_TABLE;

		private bool disposed = false;

		public SecurityGroupPersister(SqlConnection connection, SqlTransaction transaction) : base(connection, transaction)
		{
			sdaSecurityGroup = new SqlDataAdapter();	
			sdaAvailableUsers = new SqlDataAdapter();
			sdaSelectedUsers = new SqlDataAdapter();

			/* Security Group table adapter setup */
			sdaSecurityGroup.InsertCommand = DBCommandFactory.Instance.NewSqlCommand(SECGRP_INSERT_SP);
			sdaSecurityGroup.InsertCommand.Connection = this.Connection;
			sdaSecurityGroup.InsertCommand.Transaction = this.Transaction;
			sdaSecurityGroup.InsertCommand.CommandType = CommandType.StoredProcedure;
			sdaSecurityGroup.InsertCommand.Parameters.Add( "@CID", SqlDbType.UniqueIdentifier, 16, DBSecurityGroupDetailsDS.SECGRP_SECURITYID_FIELD );
			sdaSecurityGroup.InsertCommand.Parameters.Add( "@Groupname", SqlDbType.VarChar, 50, DBSecurityGroupDetailsDS.SECGRP_SECURITYNAME_FIELD );

			sdaSecurityGroup.UpdateCommand = DBCommandFactory.Instance.NewSqlCommand(SECGRP_UPDATE_SP);
			sdaSecurityGroup.UpdateCommand.Connection = this.Connection;
			sdaSecurityGroup.UpdateCommand.Transaction = this.Transaction;
			sdaSecurityGroup.UpdateCommand.CommandType = CommandType.StoredProcedure;
			sdaSecurityGroup.UpdateCommand.Parameters.Add( "@CID", SqlDbType.UniqueIdentifier, 16, DBSecurityGroupDetailsDS.SECGRP_SECURITYID_FIELD );
			sdaSecurityGroup.UpdateCommand.Parameters.Add( "@Groupname", SqlDbType.VarChar, 50, DBSecurityGroupDetailsDS.SECGRP_SECURITYNAME_FIELD );

			sdaSecurityGroup.SelectCommand = DBCommandFactory.Instance.NewSqlCommand(SECGRP_SELECT_SP);
			sdaSecurityGroup.SelectCommand.Connection = this.Connection;
			sdaSecurityGroup.SelectCommand.Transaction = this.Transaction;
			sdaSecurityGroup.SelectCommand.CommandType = CommandType.StoredProcedure;
			sdaSecurityGroup.SelectCommand.Parameters.Add( "@CID", SqlDbType.UniqueIdentifier, 16, DBSecurityGroupDetailsDS.SECGRP_SECURITYID_FIELD );

			sdaSecurityGroup.DeleteCommand = DBCommandFactory.Instance.NewSqlCommand(SECGRP_DELETE_SP);
			sdaSecurityGroup.DeleteCommand.Connection = this.Connection;
			sdaSecurityGroup.DeleteCommand.Transaction = this.Transaction;
			sdaSecurityGroup.DeleteCommand.CommandType = CommandType.StoredProcedure;
			sdaSecurityGroup.DeleteCommand.Parameters.Add( "@CID", SqlDbType.UniqueIdentifier, 16, DBSecurityGroupDetailsDS.SECGRP_SECURITYID_FIELD );

			/* Available Users adapter setup */
			sdaAvailableUsers.SelectCommand = DBCommandFactory.Instance.NewSqlCommand(USERS_AVAILABLE_SELECT_SP);
			sdaAvailableUsers.SelectCommand.Connection = this.Connection;
			sdaAvailableUsers.SelectCommand.Transaction = this.Transaction;
			sdaAvailableUsers.SelectCommand.CommandType = CommandType.StoredProcedure;

			/* Selected Users adapter setup */
			sdaSelectedUsers.SelectCommand = DBCommandFactory.Instance.NewSqlCommand(USERS_SELECTED_SELECT_SP);
			sdaSelectedUsers.SelectCommand.Connection = this.Connection;
			sdaSelectedUsers.SelectCommand.Transaction = this.Transaction;
			sdaSelectedUsers.SelectCommand.CommandType = CommandType.StoredProcedure;
			sdaSelectedUsers.SelectCommand.Parameters.Add( "@GroupID", SqlDbType.UniqueIdentifier, 16, DBSecurityGroupDetailsDS.SECGRPUSER_GROUPID_FIELD );

			sdaSelectedUsers.InsertCommand = DBCommandFactory.Instance.NewSqlCommand(USERS_SELECTED_INSERT_SP);
			sdaSelectedUsers.InsertCommand.Connection = this.Connection;
			sdaSelectedUsers.InsertCommand.Transaction = this.Transaction;
			sdaSelectedUsers.InsertCommand.CommandType = CommandType.StoredProcedure;
			sdaSelectedUsers.InsertCommand.Parameters.Add( "@UserID", SqlDbType.UniqueIdentifier, 16, DBSecurityGroupDetailsDS.SECGRPUSER_USERID_FIELD );
			sdaSelectedUsers.InsertCommand.Parameters.Add( "@GroupID", SqlDbType.UniqueIdentifier, 16, DBSecurityGroupDetailsDS.SECGRPUSER_GROUPID_FIELD );

			sdaSelectedUsers.DeleteCommand = DBCommandFactory.Instance.NewSqlCommand(USERS_SELECTED_DELETE_SP);
			sdaSelectedUsers.DeleteCommand.Connection = this.Connection;
			sdaSelectedUsers.DeleteCommand.Transaction = this.Transaction;
			sdaSelectedUsers.DeleteCommand.CommandType = CommandType.StoredProcedure;
			sdaSelectedUsers.DeleteCommand.Parameters.Add( "@GroupID", SqlDbType.UniqueIdentifier, 16, DBSecurityGroupDetailsDS.SECGRPUSER_GROUPID_FIELD );
			sdaSelectedUsers.DeleteCommand.Parameters.Add( "@UserID", SqlDbType.UniqueIdentifier, 16, DBSecurityGroupDetailsDS.SECGRPUSER_USERID_FIELD );
		}

		#region IDisposable Implementation ---------------------------------------

		/// <summary>
		///		Performs application-defined tasks associated with freeing, releasing, 
		///		or resetting unmanaged resources.
		/// </summary>
		public new void Dispose() 
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		/// <summary>
		///		Performs application-defined tasks associated with freeing, releasing, 
		///		or resetting unmanaged resources.
		/// </summary>
		/// <param name="disposing">
		///		True if called by the Dispose() method, false if called by the finalizer.
		/// </param>
		protected void Dispose(bool disposing) 
		{
			if (!disposed)
			{
				if (disposing)
				{
					base.Dispose();
					sdaSecurityGroup.Dispose();
					sdaAvailableUsers.Dispose();
					sdaSelectedUsers.Dispose();
				}
				disposed = true;
			}
		}

		#endregion // IDisposable Implementation

		public SqlDataAdapter DataAdapter 
		{
			get{ return( sdaSecurityGroup ); }
		}

		public DataSet Update(DataSet data)
		{
			return( null );
		}

		public override IBrokerManagedComponent Get(Guid instanceId, ref DataSet primaryDS)
		{
			DBSecurityGroup objSecurityGroup = null;
			try
			{
				if( instanceId != Guid.Empty )
				{
					objSecurityGroup = new DBSecurityGroup();
					objSecurityGroup.CID = instanceId;

					DBSecurityGroupDetailsDS objSecurityGroupDS = new DBSecurityGroupDetailsDS();
					sdaSecurityGroup.SelectCommand.Parameters["@CID"].Value = instanceId;
					sdaSelectedUsers.SelectCommand.Parameters["@GroupID"].Value = instanceId;

					sdaSecurityGroup.Fill( objSecurityGroupDS.Tables[ DBSecurityGroupDetailsDS.SECGRP_DETAIL_TABLE ] );
					sdaAvailableUsers.Fill( objSecurityGroupDS.Tables[ DBSecurityGroupDetailsDS.SECGRP_AVAILABLEUSERS_TABLE ] );
					sdaSelectedUsers.Fill( objSecurityGroupDS.Tables[ DBSecurityGroupDetailsDS.SECGRP_SELECTEDUSERS_TABLE ] );

					objSecurityGroupDS.AcceptChanges();

					objSecurityGroup.CalculateToken( false );
					base.Hydrate( objSecurityGroup, instanceId, new BrokerManagedComponentDS(), ref primaryDS );

					objSecurityGroup.DeliverData( objSecurityGroupDS );

					// S.C - 13MAY03 - Setting modified flag should not be necessary - however an issue exists
					//				   wherein Modified flag was set to true after DeliverData ( it should always be false on loading the CM )
					//				   caused exceptions to be thrown.
					objSecurityGroup.Modified = false;
				}
			}
			catch( Exception e )
			{
				throw new Exception( "DBUser.UserPersister.GetByInstanceID : " + e.Message );
			}
			return( (IBrokerManagedComponent) objSecurityGroup );
		}

		public override void Save(IBrokerManagedComponent iBMC, ref DataSet persistedDS)
		{
			try
			{
				base.Save(iBMC, new BrokerManagedComponentDS(), ref persistedDS );

				/* Save the Security Group */
				
				DBSecurityGroupDetailsDS objTargetGroupDS = new DBSecurityGroupDetailsDS();
				sdaSecurityGroup.SelectCommand.Parameters["@CID"].Value = iBMC.CID;
				sdaSecurityGroup.Fill( objTargetGroupDS.Tables[ DBSecurityGroupDetailsDS.SECGRP_DETAIL_TABLE ]);

				/* Save Users associated with this Security Group */

				sdaSelectedUsers.SelectCommand.Parameters["@GroupID"].Value = iBMC.CID;
				sdaSelectedUsers.Fill( objTargetGroupDS.Tables[ DBSecurityGroupDetailsDS.SECGRP_SELECTEDUSERS_TABLE ] );

				DBSecurityGroupDetailsDS objSourceGroupDS = new DBSecurityGroupDetailsDS();
				iBMC.ExtractData( objSourceGroupDS );

				objTargetGroupDS.Merge( objSourceGroupDS, false, MissingSchemaAction.Ignore );
				sdaSecurityGroup.Update( objTargetGroupDS, DBSecurityGroupDetailsDS.SECGRP_DETAIL_TABLE );

				sdaSelectedUsers.Update( objTargetGroupDS, DBSecurityGroupDetailsDS.SECGRP_SELECTEDUSERS_TABLE );
			}
			catch( Exception e )
			{
				throw new Exception( "SecurityGroupPersister.Save : " + e.Message );
			}
		}

		public override void Delete(IBrokerManagedComponent iBMC)
		{
			base.Delete(iBMC);
		}

		public override DataSet FindDetailsByTypeId(Guid typeId, object[] parameters)
		{
			// List of Available Security Groups.

			SqlDataAdapter sdaSecurityGroupDetails = new SqlDataAdapter();
			sdaSecurityGroupDetails.SelectCommand = DBCommandFactory.Instance.NewSqlCommand( SECGRP_SELECTALL_SP );
			sdaSecurityGroupDetails.SelectCommand.Connection = sdaSecurityGroup.SelectCommand.Connection;
			sdaSecurityGroupDetails.SelectCommand.Transaction = this.Transaction;
			sdaSecurityGroupDetails.SelectCommand.CommandType = CommandType.StoredProcedure;
			sdaSecurityGroupDetails.SelectCommand.Parameters.AddWithValue( "@CTID", typeId );

			DBSecurityGroupDetailsDS objSecurityGroupDS=new DBSecurityGroupDetailsDS();
			sdaSecurityGroupDetails.Fill(objSecurityGroupDS.Tables[DBSecurityGroupDetailsDS.SECGRP_DETAIL_TABLE]);

			SqlDataAdapter sdaUserDetails=new SqlDataAdapter();
			sdaUserDetails.SelectCommand = DBCommandFactory.Instance.NewSqlCommand( USER_SELECTALL_SP );
			sdaUserDetails.SelectCommand.Connection = sdaSecurityGroupDetails.SelectCommand.Connection;
			sdaUserDetails.SelectCommand.Transaction = this.Transaction;
			sdaUserDetails.SelectCommand.CommandType = CommandType.StoredProcedure;
            sdaUserDetails.SelectCommand.Parameters.AddWithValue("@CTID", USER_CTID);

			sdaUserDetails.Fill(objSecurityGroupDS.Tables[DBSecurityGroupDetailsDS.SECGRP_AVAILABLEUSERS_TABLE]);

			return objSecurityGroupDS;
		}

		public override SqlConnection Connection
		{
			set
			{
				base.Connection = value;
				sdaSecurityGroup.InsertCommand.Connection=value;
				sdaSecurityGroup.SelectCommand.Connection=value;
				sdaSecurityGroup.UpdateCommand.Connection=value;
				sdaSecurityGroup.DeleteCommand.Connection=value;

				sdaAvailableUsers.SelectCommand.Connection=value;

				sdaSelectedUsers.InsertCommand.Connection=value;
				sdaSelectedUsers.SelectCommand.Connection=value;
				sdaSelectedUsers.DeleteCommand.Connection=value;
			}
		}

		public override SqlTransaction Transaction
		{
			set
			{
				base.Transaction = value;
				sdaSecurityGroup.InsertCommand.Transaction=value;
				sdaSecurityGroup.SelectCommand.Transaction=value;
				sdaSecurityGroup.UpdateCommand.Transaction=value;
				sdaSecurityGroup.DeleteCommand.Transaction=value;

				sdaAvailableUsers.SelectCommand.Transaction=value;

				sdaSelectedUsers.InsertCommand.Transaction=value;
				sdaSelectedUsers.SelectCommand.Transaction=value;
				sdaSelectedUsers.DeleteCommand.Transaction=value;
			}
		}
		public override void EstablishDatabase()
		{
			SqlCommand adminCommand = DBCommandFactory.Instance.NewSqlCommand();
            adminCommand.Connection = this.Connection;
			adminCommand.Transaction = this.Transaction;

			adminCommand.CommandText="if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SecurityGroup]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)"
			+"CREATE TABLE [dbo].[SecurityGroup] ("
			+"[CID] [uniqueidentifier] NULL ,"
			+"[Groupname] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL "
			+") ON [PRIMARY]";
			adminCommand.ExecuteNonQuery();

			adminCommand.CommandText="if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[UserDetail_SecurityGroup]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)"
				+"CREATE TABLE [dbo].[UserDetail_SecurityGroup] ("
				+"[UserID] [uniqueidentifier] NULL ,"
				+"[GroupID] [uniqueidentifier] NULL "
				+") ON [PRIMARY]";
			adminCommand.ExecuteNonQuery();

			adminCommand.CommandText="if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SECURITY_AVAILABLEUSERS_SELECT_1_0_0]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) " +
				"drop procedure [dbo].[SECURITY_AVAILABLEUSERS_SELECT_1_0_0] ";
			adminCommand.ExecuteNonQuery();

			adminCommand.CommandText="CREATE PROCEDURE [dbo].[SECURITY_AVAILABLEUSERS_SELECT_1_0_0] "
			+" AS "
			+"SELECT CID, LASTNAME, FIRSTNAME FROM UserDetail";
			adminCommand.ExecuteNonQuery();

			adminCommand.CommandText="if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SECURITY_SECURITYGRP_DELETE_1_0_0]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) " +
				"drop procedure [dbo].[SECURITY_SECURITYGRP_DELETE_1_0_0] ";
			adminCommand.ExecuteNonQuery();

			adminCommand.CommandText="CREATE PROCEDURE [dbo].[SECURITY_SECURITYGRP_DELETE_1_0_0] " 
			+"@CID uniqueidentifier"
			+" AS "
			+"DELETE SecurityGroup WHERE CID = @CID";
			adminCommand.ExecuteNonQuery();

			adminCommand.CommandText="if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SECURITY_SECURITYGRP_INSERT_1_0_0]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) " +
				"drop procedure [dbo].[SECURITY_SECURITYGRP_INSERT_1_0_0] ";
			adminCommand.ExecuteNonQuery();

			adminCommand.CommandText="CREATE PROCEDURE [dbo].[SECURITY_SECURITYGRP_INSERT_1_0_0] "
			+"@CID uniqueidentifier,"
			+"@Groupname varchar(50)"
			+" AS "
			+"INSERT INTO SecurityGroup( CID, Groupname ) VALUES( @CID, @Groupname )";
			adminCommand.ExecuteNonQuery();

			adminCommand.CommandText="if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SECURITY_SECURITYGRP_SELECTALL_1_0_0]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) " +
				"drop procedure [dbo].[SECURITY_SECURITYGRP_SELECTALL_1_0_0] ";
			adminCommand.ExecuteNonQuery();

			adminCommand.CommandText="CREATE PROCEDURE [dbo].[SECURITY_SECURITYGRP_SELECTALL_1_0_0] "
			+"@CTID uniqueidentifier"
			+" AS "
			+ string.Format("SELECT {0}.CID, {0}.NAME, SECURITYGROUP.GROUPNAME", cinstancesTableName)
			+" FROM "
			+ string.Format("{0}, SECURITYGROUP", cinstancesTableName)
			+" WHERE "
			+ string.Format("{0}.CID=SECURITYGROUP.CID AND {0}.CTID = @CTID", cinstancesTableName);
			adminCommand.ExecuteNonQuery();

			adminCommand.CommandText="if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SECURITY_SECURITYGRP_SELECT_1_0_0]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) " +
				"drop procedure [dbo].[SECURITY_SECURITYGRP_SELECT_1_0_0] ";
			adminCommand.ExecuteNonQuery();

			adminCommand.CommandText="CREATE PROCEDURE [dbo].[SECURITY_SECURITYGRP_SELECT_1_0_0] "
			+"@CID uniqueidentifier"
			+" AS "
			+"SELECT * FROM SecurityGroup WHERE CID = @CID";
			adminCommand.ExecuteNonQuery();


			adminCommand.CommandText="if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SECURITY_SECURITYGRP_UPDATE_1_0_0]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) " +
				"drop procedure [dbo].[SECURITY_SECURITYGRP_UPDATE_1_0_0] ";
			adminCommand.ExecuteNonQuery();

			adminCommand.CommandText="CREATE PROCEDURE [dbo].[SECURITY_SECURITYGRP_UPDATE_1_0_0] "
			+"@CID uniqueidentifier,"
			+"@Groupname varchar(50)"
			+" AS "
			+"UPDATE SecurityGroup SET Groupname = @Groupname WHERE CID = @CID";
			adminCommand.ExecuteNonQuery();

			adminCommand.CommandText="if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SECURITY_SELECTEDUSERS_INSERT_1_0_0]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) " +
				"drop procedure [dbo].[SECURITY_SELECTEDUSERS_INSERT_1_0_0] ";
			adminCommand.ExecuteNonQuery();

			adminCommand.CommandText="CREATE PROCEDURE [dbo].[SECURITY_SELECTEDUSERS_INSERT_1_0_0] "
			+"@GroupID uniqueidentifier,"
			+"@UserID uniqueidentifier"
			+" AS "
			+" INSERT INTO UserDetail_SecurityGroup ( GroupID, UserID ) VALUES (@GroupID, @UserID )";
			adminCommand.ExecuteNonQuery();

			adminCommand.CommandText="if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SECURITY_SELECTEDUSERS_SELECT_1_0_0]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) " +
				"drop procedure [dbo].[SECURITY_SELECTEDUSERS_SELECT_1_0_0] ";
			adminCommand.ExecuteNonQuery();

			adminCommand.CommandText="CREATE PROCEDURE [dbo].[SECURITY_SELECTEDUSERS_SELECT_1_0_0] "
			+"@GroupID uniqueidentifier"
			+" AS "
			+"SELECT DISTINCT dbo.SecurityGroup.CID AS GroupID, dbo.UserDetail.CID AS UserID, dbo.UserDetail.Username, dbo.UserDetail.Password, dbo.UserDetail.Lastname, dbo.UserDetail.Firstname"
			+" FROM "
			+" dbo.UserDetail_SecurityGroup INNER JOIN dbo.UserDetail ON dbo.UserDetail_SecurityGroup.UserID = dbo.UserDetail.CID INNER JOIN dbo.SecurityGroup ON dbo.UserDetail_SecurityGroup.GroupID = dbo.SecurityGroup.CID"
			+" WHERE "
			+"UserDetail_SecurityGroup.GroupID = @GroupID";
			adminCommand.ExecuteNonQuery();

			adminCommand.CommandText="if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SECURITY_SG_SELECT_1_0_0]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) " +
				"drop procedure [dbo].[SECURITY_SG_SELECT_1_0_0] ";
			adminCommand.ExecuteNonQuery();

			adminCommand.CommandText="CREATE PROCEDURE [dbo].[SECURITY_SG_SELECT_1_0_0] "
			+"@CID uniqueidentifier"
			+" AS "
			+"SELECT CID, Groupname from SecurityGroup WHERE SecurityGroup.CID in ( SELECT UserDetail_SecurityGroup.GroupID FROM UserDetail_SecurityGroup WHERE UserDetail_SecurityGroup.UserID = @CID )";
			adminCommand.ExecuteNonQuery();

            adminCommand.CommandText = "if exists (" +
                                       "select * from dbo.sysobjects" +
                                       "where id = object_id(N'[dbo].[MESSAGE_INBOXUSER_SELECT_1_0_0]') and " +
                                       "OBJECTPROPERTY(id, N'IsProcedure') = 1 " +
                                       ") " +
                                       "drop procedure [dbo].[MESSAGE_INBOXUSER_SELECT_1_0_0] " +
                                       "GO " +
                                       "CREATE PROCEDURE [dbo].[MESSAGE_INBOXUSER_SELECT_1_0_0] " +
                                       "@ToUserCID uniqueidentifier " +
                                       "AS " +
                                       "SELECT * FROM MessageTable WHERE ToUserCID = @ToUserCID " +
                                       "GO ";
            adminCommand.ExecuteNonQuery();
		}

		public override void ClearDatabase()
		{
			SqlCommand adminCommand = DBCommandFactory.Instance.NewSqlCommand();
            adminCommand.Connection = this.Connection;
			adminCommand.Transaction = this.Transaction;

			adminCommand.CommandText="if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SecurityGroup]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)"
				+"DELETE SecurityGroup";
			adminCommand.ExecuteNonQuery();

			adminCommand.CommandText="if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[UserDetail_SecurityGroup]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)"
				+"DELETE UserDetail_SecurityGroup";
			adminCommand.ExecuteNonQuery();
		}

		public override void RemoveDatabase()
		{

		}
	}
}
