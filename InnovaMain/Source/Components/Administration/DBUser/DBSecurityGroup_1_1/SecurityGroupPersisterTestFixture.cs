using System;
using System.Diagnostics;
using System.Configuration;
using System.Data.SqlClient;
using System.Reflection;
using System.Data;

using NUnit.Framework;



namespace Oritax.TaxSimp.Security
{
	/// <summary>
	/// Summary description for SecurityGroupPersisterTestFixture.
	/// </summary>
	/// 
	[TestFixture]
	public class SecurityGroupPersisterTestFixture
	{
		SqlConnection sqlCon = null;

		[Conditional("DEBUG")] [SetUp] public void PersisterTestSetup()
		{
			string sConn = ConfigurationSettings.AppSettings["TX360ENTERPRISE_DBCONNECTIONSTRING"];
			sqlCon = new SqlConnection(sConn);
			sqlCon.Open();
		}

		[Conditional("DEBUG")] [TearDown] public void PersisterTestTearDown()
		{
			sqlCon.Close();
			sqlCon.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterConstructionTest()
		{
			SecurityGroupPersister sgPersister = new SecurityGroupPersister(sqlCon);

			FieldInfo dataAdaptInfo = sgPersister.GetType().GetField("sdaSecurityGroup", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlDataAdapter dsCommand = (SqlDataAdapter)dataAdaptInfo.GetValue(sgPersister);
			Assertion.AssertNotNull("SqlDataAdapter dsBMCCommand is Null",dsCommand);
			Assertion.AssertNotNull("sdaSecurityGroup.SelectCommand is Null",dsCommand.SelectCommand);
			Assertion.AssertNotNull("sdaSecurityGroup.InsertCommand is Null",dsCommand.InsertCommand);
			Assertion.AssertNotNull("sdaSecurityGroup.UpdateCommand is Null",dsCommand.UpdateCommand);
			Assertion.AssertNotNull("sdaSecurityGroup.DeleteCommand is Null",dsCommand.DeleteCommand);

			dataAdaptInfo = sgPersister.GetType().GetField("sdaAvailableUsers", BindingFlags.NonPublic | BindingFlags.Instance);
			dsCommand = (SqlDataAdapter)dataAdaptInfo.GetValue(sgPersister);
			Assertion.AssertNotNull("SqlDataAdapter dsBMCCommand is Null",dsCommand);
			Assertion.AssertNotNull("sdaAvailableUsers.SelectCommand is Null",dsCommand.SelectCommand);
			Assertion.AssertNull("sdaAvailableUsers.InsertCommand is not Null",dsCommand.InsertCommand);
			Assertion.AssertNull("sdaAvailableUsers.UpdateCommand is not Null",dsCommand.UpdateCommand);
			Assertion.AssertNull("sdaAvailableUsers.DeleteCommand is not Null",dsCommand.DeleteCommand);

			dataAdaptInfo = sgPersister.GetType().GetField("sdaSelectedUsers", BindingFlags.NonPublic | BindingFlags.Instance);
			dsCommand = (SqlDataAdapter)dataAdaptInfo.GetValue(sgPersister);
			Assertion.AssertNotNull("SqlDataAdapter dsBMCCommand is Null",dsCommand);
			Assertion.AssertNotNull("sdaSelectedUsers.SelectCommand is Null",dsCommand.SelectCommand);
			Assertion.AssertNotNull("sdaSelectedUsers.InsertCommand is Null",dsCommand.InsertCommand);
			Assertion.AssertNull("sdaSelectedUsers.UpdateCommand is not Null",dsCommand.UpdateCommand);
			Assertion.AssertNotNull("sdaSelectedUsers.DeleteCommand is Null",dsCommand.DeleteCommand);

			FieldInfo sqlConnectionInfo = sgPersister.GetType().GetField("sqlConnection", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlConnection sqlConnection = (SqlConnection)sqlConnectionInfo.GetValue(sgPersister);
			Assertion.AssertEquals("Connection has not been set", sqlConnection,sqlCon);

			FieldInfo sqlTransactionInfo = sgPersister.GetType().GetField("sqlTransaction", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlTransaction sqlTransaction = (SqlTransaction)sqlTransactionInfo.GetValue(sgPersister);
			Assertion.AssertNull("sqlTransaction is not null", sqlTransaction);

			sgPersister.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterConnectionTest()
		{
			SecurityGroupPersister sgPersister = new SecurityGroupPersister(sqlCon);
			sgPersister.Connection = sqlCon;

			FieldInfo sqlConnectionInfo = sgPersister.GetType().GetField("sqlConnection", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlConnection sqlConnection = (SqlConnection)sqlConnectionInfo.GetValue(sgPersister);
			Assertion.AssertEquals("Connection has not been set", sqlConnection,sqlCon);

			sgPersister.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterTransactionTest()
		{
			SecurityGroupPersister sgPersister = new SecurityGroupPersister(sqlCon);
			SqlTransaction sqlTran = sqlCon.BeginTransaction();
			sgPersister.Transaction = sqlTran;
			FieldInfo sqlTransactionInfo = sgPersister.GetType().GetField("sqlTransaction", BindingFlags.NonPublic | BindingFlags.Instance);
			SqlTransaction sqlTransaction = (SqlTransaction)sqlTransactionInfo.GetValue(sgPersister);
			Assertion.AssertEquals("sqlTransaction has not been set", sqlTransaction, sqlTran);
			sqlTran.Commit();
			sqlTran.Dispose();
			sgPersister.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterEstablishDBTest()
		{
			SecurityGroupPersister sgPersister = new SecurityGroupPersister(sqlCon);

			SqlTransaction sqlTran = sqlCon.BeginTransaction();
			sgPersister.Transaction = sqlTran;
			try
			{
				sgPersister.EstablishDatabase();
			}
			finally
			{
				sqlTran.Rollback();	//DO NOT CORRUPT THE DATABASE
			}
			sgPersister.Dispose();
		}

		[Conditional("DEBUG")] [Test] public void PersisterClearDBTest()
		{
			SecurityGroupPersister sgPersister = new SecurityGroupPersister(sqlCon);

			SqlTransaction sqlTran = sqlCon.BeginTransaction();
			sgPersister.Transaction = sqlTran;
			try
			{
				sgPersister.ClearDatabase();
			}
			finally
			{
				sqlTran.Rollback();	//DO NOT CORRUPT THE DATABASE
			}
			sgPersister.Dispose();
		}
	}
}
