using System;

namespace Oritax.TaxSimp.Security
{
	/// <summary>
	/// Summary description for Install.
	/// </summary>
	public class DBSecurityGroupInstall
	{
		#region INSTALLATION PROPERTIES

		// Assembly Installation Properties
		public const string ASSEMBLY_ID="ED088FAD-B668-4070-A7D4-97DA5C98297F";
		public const string ASSEMBLY_NAME="DBSecurityGroup_1_1";
		public const string ASSEMBLY_DISPLAYNAME="DB Security Group V1.1";
		public const string ASSEMBLY_MAJORVERSION="1";
		public const string ASSEMBLY_MINORVERSION="1";
		public const string ASSEMBLY_DATAFORMAT="0";
		//public const string ASSEMBLY_REVISION="0";	//2004.3.1
		//public const string ASSEMBLY_REVISION="1";	//2004.3.2
		public const string ASSEMBLY_REVISION="2";		//2005.1

		// Component Installation Properties
		public const string COMPONENT_ID="2B4FFB46-E5C1-4b99-BD9D-EAD2B4A666ED";
		public const string COMPONENT_NAME="DBSecurityGroup";
		public const string COMPONENT_DISPLAYNAME="DB Security Group";
		public const string COMPONENT_CATEGORY="Security";

		// Component Version Installation Properties
		public const string COMPONENTVERSION_STARTDATE="01/01/1990 00:00:00 AM";
		public const string COMPONENTVERSION_ENDDATE="31/12/2050 12:59:59 AM";
		public const string COMPONENTVERSION_PERSISTERASSEMBLY="DBSecurityGroup_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";
		public const string COMPONENTVERSION_PERSISTERCLASS="Oritax.TaxSimp.Security.SecurityGroupPersister";
		public const string COMPONENTVERSION_IMPLEMENTATIONCLASS="Oritax.TaxSimp.Security.DBSecurityGroup";

		
		// Data Model
		public const string ASSEMBLY_PERSISTERASSEMBLY="DBSecurityGroup_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";
		public const string ASSEMBLY_PERSISTERCLASS="Oritax.TaxSimp.Security.SecurityGroupPersister";

		#endregion
	}
}
