#if DEBUG

using System;
using System.Data;
using System.Configuration;
using System.Diagnostics;
using System.Data.SqlClient;
using NUnit.Framework;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.Services.CMBroker;

namespace Oritax.TaxSimp.Security
{
	/// <summary>
	/// Summary description for DBUserTestFixture.
	/// </summary>
	/// 
	[TestFixture]
	public class DBSecurityGroupFixture
	{
		public bool bAddUserRun = false;
		CMBroker broker;

		[SetUp]
		public void Setup()
		{
			broker = new CMBroker();
		}

		[TearDown]
		public void TearDown()
		{
		}

		[Test]
		public void testAddGroup()
		{
			try
			{
				broker.SetStart();
				DBSecurityGroup objSecurityGroup = (DBSecurityGroup) broker.CreateBMCInstance( "DBSecurityGroup", "" );

                DataSet dsGroupDetails = new DBSecurityGroupDS();
				DataRow drGroupDetails = dsGroupDetails.Tables[ DBSecurityGroupDS.SECGRP_DETAIL_TABLE ].NewRow();

				drGroupDetails[ DBSecurityGroupDS.SECGRP_SECURITYID_FIELD ]   = Guid.NewGuid();
				drGroupDetails[ DBSecurityGroupDS.SECGRP_SECURITYNAME_FIELD ] = "NUnit Group Test";
				dsGroupDetails.Tables[ DBSecurityGroupDS.SECGRP_DETAIL_TABLE ].Rows.Add( drGroupDetails );

				objSecurityGroup.DeliverData( dsGroupDetails );

				broker.SetComplete();
			}
			catch
			{
				broker.SetAbort();
			}
		}

		[Test]
		public void testDeleteGroup()
		{
		}
			
	}
}
#endif