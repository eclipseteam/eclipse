using System;
using System.Data;
using System.Data.SqlClient;

using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.WPDatasetBase;

namespace Oritax.TaxSimp.Security
{
	[Serializable]
	public class DBSecurityGroupDetailsDS : WPDatasetBaseDS
	{
		DataTable dtSecurityGroup;
		DataTable dtAvailableUsers;
		DataTable dtSelectedUsers;

		public const String SECGRP_DETAIL_TABLE			= "SecurityGroupDetails";
		public const String SECGRP_SELECTEDUSERS_TABLE	= "SelectedUsers";
		public const String SECGRP_AVAILABLEUSERS_TABLE	= "AvailableUsers";

		public const String REGEX_ALPHANUMERIC			= @"^[a-zA-Z0-9\s.\-\']+$";

		public const String MSG_GROUPNAME_INVALID		= "A group name may contain alphabetic characters, apostrophe (') " +
														  ", and hyphen(-), Please re-enter the group name";

		public const String USER_USERID_FIELD			= "CID";
		public const String USER_FIRSTNAME_FIELD		= "Firstname";
		public const String USER_LASTNAME_FIELD			= "Lastname";

		public const String SECGRPUSER_USERID_FIELD		= "UserID";
		public const String SECGRPUSER_GROUPID_FIELD	= "GroupID";

		public const String SECGRP_SECURITYID_FIELD		= "CID";
		public const String SECGRP_SECURITYNAME_FIELD	= "Groupname";
		
		public const int ADDUSER_OPERATION				= 10;
		public const int REMOVEUSER_OPERATION			= 20;
		public const int LISTGROUPS_OPERATION			= 30;

		public bool GroupsList = false;

		public DBSecurityGroupDetailsDS(): base()
		{
			this.AddSecurityGroupDetailTable();

			/* Construct Available Users table */
			dtAvailableUsers = new DataTable( SECGRP_AVAILABLEUSERS_TABLE );

			dtAvailableUsers.Columns.Add( USER_USERID_FIELD, typeof( System.Guid) );
			dtAvailableUsers.Columns.Add( USER_FIRSTNAME_FIELD, typeof( System.String ) );
			dtAvailableUsers.Columns.Add( USER_LASTNAME_FIELD, typeof( System.String ) );

			DataColumn[] dcAUPrimaryKey = {dtAvailableUsers.Columns[ USER_USERID_FIELD ]};
			this.Tables.Add( dtAvailableUsers);
			BrokerManagedComponentDS.AddVersionStamp(dtAvailableUsers,typeof(DBSecurityGroupDetailsDS));

			/* Construct Selected Users table */
			dtSelectedUsers = new DataTable( SECGRP_SELECTEDUSERS_TABLE );

			dtSelectedUsers.Columns.Add( SECGRPUSER_USERID_FIELD, typeof( System.Guid) );
			dtSelectedUsers.Columns.Add( SECGRPUSER_GROUPID_FIELD, typeof( System.Guid) );
			dtSelectedUsers.Columns.Add( USER_FIRSTNAME_FIELD, typeof( System.String ) );
			dtSelectedUsers.Columns.Add( USER_LASTNAME_FIELD, typeof( System.String ) );

			DataColumn[] dcSUPrimaryKey = {dtSelectedUsers.Columns[ USER_USERID_FIELD ]};
			dtSelectedUsers.PrimaryKey = dcSUPrimaryKey;
			this.Tables.Add( dtSelectedUsers );
			BrokerManagedComponentDS.AddVersionStamp(dtSelectedUsers,typeof(DBSecurityGroupDetailsDS));

		}

		public void AddSecurityGroupDetailTable()
		{
			//* Construct Security Group table */
			if(!this.Tables.Contains(SECGRP_DETAIL_TABLE))
			{
				dtSecurityGroup = new DataTable( SECGRP_DETAIL_TABLE );

				dtSecurityGroup.Columns.Add( SECGRP_SECURITYID_FIELD, typeof( System.Guid) );
				dtSecurityGroup.Columns.Add( SECGRP_SECURITYNAME_FIELD, typeof( System.String ) );

				DataColumn[] dcSGPrimaryKey = {dtSecurityGroup.Columns[ SECGRP_SECURITYID_FIELD ]};
				dtSecurityGroup.PrimaryKey = dcSGPrimaryKey;

				this.Tables.Add( dtSecurityGroup );

				BrokerManagedComponentDS.AddVersionStamp(dtSecurityGroup,typeof(DBSecurityGroupDetailsDS));
			}
		}
	}
}
