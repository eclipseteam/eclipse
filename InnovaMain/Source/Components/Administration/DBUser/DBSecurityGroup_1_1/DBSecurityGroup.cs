using System;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Reflection;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.Exceptions;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.CalculationInterface;

namespace Oritax.TaxSimp.Security
{
	/// <summary>
	/// Summary description for DBSecurityGroup.
	/// </summary>
	/// 
	[Serializable]
	public class DBSecurityGroup : SystemModuleBase, ISerializable
	{
		#region CONSTRUCTORS
		[NonSerialized]
		DBSecurityGroupDetailsDS dsSecurityGroup = null;

		public DBSecurityGroup()
			:base()
		{
			dsSecurityGroup = new DBSecurityGroupDetailsDS();
		}

		protected DBSecurityGroup(SerializationInfo si, StreamingContext context)
			: base(si,context)
		{
		}
	
		//Only allow the .NET Serialization core code to call this function
		[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]
		public override void GetObjectData(SerializationInfo si, StreamingContext context)
		{
			base.GetObjectData(si,context);
		}
		#endregion
		#region OVERRIDE MEMBERS FOR CM BEHAVIOR

		public override DataSet PrimaryDataSet{get{return new DBSecurityGroupDS();}}


		#region OnGetData methods
		protected void OnGetData( DBSecurityGroupDetailsDS data )
		{
			if(data.GroupsList)
			{
				DBSecurityGroupDetailsDS objListDS = (DBSecurityGroupDetailsDS) Broker.GetBMCDetailList( DBSecurityGroupInstall.ASSEMBLY_NAME );
				
				if( data.Tables.Contains( DBSecurityGroupDetailsDS.SECGRP_DETAIL_TABLE ))
					data.Tables.Remove( DBSecurityGroupDetailsDS.SECGRP_DETAIL_TABLE );
				
				data.Tables.Add(objListDS.Tables[DBSecurityGroupDetailsDS.SECGRP_DETAIL_TABLE].Copy());

				if( data.Tables.Contains( DBSecurityGroupDetailsDS.SECGRP_AVAILABLEUSERS_TABLE ) )
					data.Tables.Remove( DBSecurityGroupDetailsDS.SECGRP_AVAILABLEUSERS_TABLE );
			
				data.Tables.Add( objListDS.Tables[ DBSecurityGroupDetailsDS.SECGRP_AVAILABLEUSERS_TABLE ].Copy() );

			}
			else
			{
				if( data.Tables.Contains( DBSecurityGroupDetailsDS.SECGRP_DETAIL_TABLE ))
					data.Tables.Remove( DBSecurityGroupDetailsDS.SECGRP_DETAIL_TABLE );

				if( dsSecurityGroup.Tables[ DBSecurityGroupDetailsDS.SECGRP_DETAIL_TABLE ].Rows.Count > 0 )
				{
					DataRow drSecurityGroup = dsSecurityGroup.Tables[ DBSecurityGroupDetailsDS.SECGRP_DETAIL_TABLE ].Rows[ 0 ];
					drSecurityGroup[ DBSecurityGroupDetailsDS.SECGRP_SECURITYID_FIELD ] = this.CID;
					data.Tables.Add( dsSecurityGroup.Tables[ DBSecurityGroupDetailsDS.SECGRP_DETAIL_TABLE ].Copy() );
				}

				/* Copy Selected Users table from data */
				if( data.Tables.Contains( DBSecurityGroupDetailsDS.SECGRP_SELECTEDUSERS_TABLE  ))
					data.Tables.Remove( DBSecurityGroupDetailsDS.SECGRP_SELECTEDUSERS_TABLE );

				foreach( DataRow drGroup in dsSecurityGroup.Tables[ DBSecurityGroupDetailsDS.SECGRP_SELECTEDUSERS_TABLE ].Rows )
					drGroup[ DBSecurityGroupDetailsDS.SECGRPUSER_GROUPID_FIELD ] = this.CID;
				data.Tables.Add( dsSecurityGroup.Tables[ DBSecurityGroupDetailsDS.SECGRP_SELECTEDUSERS_TABLE ].Copy() );

				DataSet dsAvailableUsers = new DataSet();
				dsAvailableUsers = Broker.GetBMCDetailList( DBSecurityGroupInstall.ASSEMBLY_NAME );

				if( data.Tables.Contains( DBSecurityGroupDetailsDS.SECGRP_AVAILABLEUSERS_TABLE ) )
					data.Tables.Remove( DBSecurityGroupDetailsDS.SECGRP_AVAILABLEUSERS_TABLE );
				data.Tables.Add( dsAvailableUsers.Tables[ DBSecurityGroupDetailsDS.SECGRP_AVAILABLEUSERS_TABLE ].Copy() );

				if( dsSecurityGroup.Tables[ DBSecurityGroupDetailsDS.SECGRP_DETAIL_TABLE ].Rows.Count > 0 )
				{
					DataRow drSecurityGroup = dsSecurityGroup.Tables[ DBSecurityGroupDetailsDS.SECGRP_DETAIL_TABLE ].Rows[ 0 ];
					data.Breadcrumb = drSecurityGroup[DBSecurityGroupDetailsDS.SECGRP_SECURITYNAME_FIELD].ToString();
				}
				else
					data.Breadcrumb = String.Empty;
	
			}
		}

		protected override void OnGetData(DataSet data )
		{
			if (data is DBSecurityGroupDS)
			{
				base.OnGetData((SystemModuleBaseDS)data);
				DBSecurityGroupDS securityData = data as DBSecurityGroupDS;
			
				if( securityData.Tables.Contains( DBSecurityGroupDS.SECGRP_DETAIL_TABLE ))
					securityData.Tables.Remove( DBSecurityGroupDS.SECGRP_DETAIL_TABLE );
				securityData.Tables.Add( dsSecurityGroup.Tables[ DBSecurityGroupDS.SECGRP_DETAIL_TABLE ].Copy() );

				/* Copy Selected Users table from data */
				if( securityData.Tables.Contains( DBSecurityGroupDetailsDS.SECGRP_SELECTEDUSERS_TABLE  ))
					securityData.Tables.Remove( DBSecurityGroupDetailsDS.SECGRP_SELECTEDUSERS_TABLE );

				foreach( DataRow drGroup in dsSecurityGroup.Tables[ DBSecurityGroupDetailsDS.SECGRP_SELECTEDUSERS_TABLE ].Rows )
					drGroup[ DBSecurityGroupDetailsDS.SECGRPUSER_GROUPID_FIELD ] = this.CID;
				securityData.Tables.Add( dsSecurityGroup.Tables[ DBSecurityGroupDetailsDS.SECGRP_SELECTEDUSERS_TABLE ].Copy() );
			}
			else if (data is DBSecurityGroupDetailsDS)
			{
				this.OnGetData((DBSecurityGroupDetailsDS)data);
			}
		}

		#endregion
		
		#region OnSetData methods
		
		protected override ModifiedState OnSetData(DataSet data)
		{
			base.OnSetData(data);
			if (data is DBSecurityGroupDetailsDS)
			{
				DBSecurityGroupDetailsDS objData = data as DBSecurityGroupDetailsDS;
				if(objData.GroupsList)
				{
					foreach(DataRow dRow in  objData.Tables[ DBSecurityGroupDetailsDS.SECGRP_DETAIL_TABLE ].Rows)
					{
						if (dRow.RowState == DataRowState.Deleted)
						{
							dRow.RejectChanges();
							IOrganization orgCM = (IOrganization)this.Broker.GetWellKnownCM(WellKnownCM.Organization);
							ILogicalModule orgLM = this.Broker.GetLogicalCM(orgCM.CLID);

							this.Broker.ReleaseBrokerManagedComponent(orgCM);

							foreach(Guid childCLID in orgLM.ChildCLIDs)
							{
								ILogicalModule childLM = this.Broker.GetLogicalCM(childCLID);

								PartyDS partyDS = new PartyDS();
								childLM.GetData(partyDS);
								partyDS.AcceptChanges();
								DataRow[] drCollection = partyDS.Tables[PartyDS.INCLUDEDPARTY_TABLE].Select(PartyDS.PARTYCID_FIELD + " = '"+dRow[DBSecurityGroupDetailsDS.SECGRP_SECURITYID_FIELD].ToString()+"'"); 

								if(drCollection.Length > 0)
								{
									drCollection[0].Delete();
									childLM.SetData(partyDS);
								}

								this.Broker.ReleaseBrokerManagedComponent(childLM);
							}

							this.Broker.ReleaseBrokerManagedComponent(orgLM);

							this.Broker.DeleteCMInstance( (Guid)dRow[DBSecurityGroupDetailsDS.SECGRP_SECURITYID_FIELD] );
						}
					}
				}
				else
				{
					if( dsSecurityGroup.Tables.Contains( DBSecurityGroupDetailsDS.SECGRP_DETAIL_TABLE ))
						dsSecurityGroup.Tables.Remove( DBSecurityGroupDetailsDS.SECGRP_DETAIL_TABLE );
					dsSecurityGroup.Tables.Add( objData.Tables[ DBSecurityGroupDetailsDS.SECGRP_DETAIL_TABLE ].Copy() );

					/* Copy Available Users table from data */
					if( dsSecurityGroup.Tables.Contains( DBSecurityGroupDetailsDS.SECGRP_AVAILABLEUSERS_TABLE ))
						dsSecurityGroup.Tables.Remove( DBSecurityGroupDetailsDS.SECGRP_AVAILABLEUSERS_TABLE );
					dsSecurityGroup.Tables.Add( objData.Tables[ DBSecurityGroupDetailsDS.SECGRP_AVAILABLEUSERS_TABLE ].Copy());

					/* Copy Selected Users table from data */
					if( dsSecurityGroup.Tables.Contains( DBSecurityGroupDetailsDS.SECGRP_SELECTEDUSERS_TABLE ))
						dsSecurityGroup.Tables.Remove( DBSecurityGroupDetailsDS.SECGRP_SELECTEDUSERS_TABLE );
					dsSecurityGroup.Tables.Add( objData.Tables[ DBSecurityGroupDetailsDS.SECGRP_SELECTEDUSERS_TABLE ].Copy());

					if( dsSecurityGroup.Tables[ DBSecurityGroupDetailsDS.SECGRP_DETAIL_TABLE ].Rows.Count > 0 )
					{
						DataRow drSecurityGroup = dsSecurityGroup.Tables[ DBSecurityGroupDetailsDS.SECGRP_DETAIL_TABLE ].Rows[ 0 ];
						string strGroupName = drSecurityGroup[ DBSecurityGroupDetailsDS.SECGRP_SECURITYNAME_FIELD ].ToString();
						if(Broker != null)
						{
							IBrokerManagedComponent objBMC = Broker.GetBMCInstance( strGroupName , DBSecurityGroupInstall.ASSEMBLY_NAME );

							try
							{
								if( (strGroupName == String.Empty) || (objBMC != null && objBMC.CID != this.CID))
									throw new DisplayUserMessageException(new MessageBoxDefinition(MessageBoxDefinition.MSG_USER_GROUP_UNIQUE),false,MessageDisplayMethod.MainWindowError);
							}
							finally
							{
								this.Broker.ReleaseBrokerManagedComponent(objBMC);
							}
						}
						this.Name = drSecurityGroup[ DBSecurityGroupDetailsDS.SECGRP_SECURITYNAME_FIELD ].ToString();
					}
				}
			}
			else if (data is DBSecurityGroupDS)
			{
				OnSetData((DBSecurityGroupDS)data);
			}
			return ModifiedState.UNKNOWN;
		}
		
		protected ModifiedState OnSetData( DBSecurityGroupDS objData )
		{
			this.DeliverPrimaryData( objData );
			return ModifiedState.UNKNOWN;
		}
		#endregion

		private new void DeliverPrimaryData(DataSet data)
		{
			if( dsSecurityGroup.Tables.Contains( DBSecurityGroupDS.SECGRP_DETAIL_TABLE ))
				dsSecurityGroup.Tables.Remove( DBSecurityGroupDS.SECGRP_DETAIL_TABLE );
			dsSecurityGroup.Tables.Add( data.Tables[ DBSecurityGroupDS.SECGRP_DETAIL_TABLE ].Copy() );

			if( dsSecurityGroup.Tables.Contains( DBSecurityGroupDS.SECGRP_SELECTEDUSERS_TABLE ))
				dsSecurityGroup.Tables.Remove( DBSecurityGroupDS.SECGRP_SELECTEDUSERS_TABLE );
			dsSecurityGroup.Tables.Add( data.Tables[ DBSecurityGroupDS.SECGRP_SELECTEDUSERS_TABLE ].Copy() );
		}

		public override DataSet MigrateDataSet(DataSet data)
		{
			DataSet baseDataSet = base.MigrateDataSet(data);

			DBSecurityGroupDS ds = new DBSecurityGroupDS();

			ds.Merge(data.Tables[ DBSecurityGroupDS.SECGRP_DETAIL_TABLE ]);
			ds.Merge(data.Tables[ DBSecurityGroupDS.SECGRP_SELECTEDUSERS_TABLE ]);

			ds.Merge(baseDataSet);
			return ds;
		}

		public override void MigrationCompleted()
		{
			base.MigrationCompleted ();
			this.Broker.DeleteCMInstance(this.CID);
		}

		public override void Initialize( String name )
		{
			base.Initialize( name );

			if( this.CID == Guid.Empty )
				this.CID = Guid.NewGuid();
		}
		#endregion
	}
}
