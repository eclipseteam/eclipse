using System;
using System.Data;
using System.Data.SqlClient;

using Oritax.TaxSimp.Calculation;

namespace Oritax.TaxSimp.Security
{
	[Serializable]
	public class DBSecurityGroupDS : SystemModuleBaseDS
	{
		DataTable dtSecurityGroup;

		public const String SECGRP_DETAIL_TABLE			= "SecurityGroupDetails";
		public const String SECGRP_SELECTEDUSERS_TABLE	= "SelectedUsers";

		public const String SECGRP_SECURITYID_FIELD		= "CID";
		public const String SECGRP_SECURITYNAME_FIELD	= "Groupname";

		public DBSecurityGroupDS(): base()
		{
			/* Construct Security Group table */
			dtSecurityGroup = new DataTable( SECGRP_DETAIL_TABLE );

			dtSecurityGroup.Columns.Add( SECGRP_SECURITYID_FIELD, typeof( System.Guid) );
			dtSecurityGroup.Columns.Add( SECGRP_SECURITYNAME_FIELD, typeof( System.String ) );

			DataColumn[] dcSGPrimaryKey = {dtSecurityGroup.Columns[ SECGRP_SECURITYID_FIELD ]};
			dtSecurityGroup.PrimaryKey = dcSGPrimaryKey;

			this.Tables.Add( dtSecurityGroup );

			BrokerManagedComponentDS.AddVersionStamp(dtSecurityGroup,typeof(DBSecurityGroupDetailsDS));
		}
	}
}
