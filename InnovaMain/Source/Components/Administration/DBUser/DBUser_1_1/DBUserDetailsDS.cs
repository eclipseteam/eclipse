using System;
using System.Data;
using System.Data.SqlClient;

using Oritax.TaxSimp.Calculation;

namespace Oritax.TaxSimp.Security
{
    /// <summary>
    /// Summary description for DBUserDetailsDS.
    /// </summary>
    [Serializable]
    public class DBUserDetailsDS : DataSet
    {
        // Regular expressions
        public bool MessageUpdate = false;
        public bool MessageInsert = false;

        public const String REGEX_EMAIL = @"^[\w-]+(?:\.[\w-]+)*@(?:[\w-]+\.)+[a-zA-Z]{2,7}$";
        public const String REGEX_ALPHANUMERIC_PUNC = @"^[a-zA-Z0-9\s.\-\']+$";
        public const String REGEX_ALPHABETIC = @"^[a-zA-Z\s.\-\']+$";
        public const String REGEX_ALPHANUMERIC = @"^[a-zA-Z0-9]+$";

        // Dataset constants
        public const string MESSAGE_TABLE = "MESSAGETABLE";
        public const string MESSAGE_ID = "MessageID";
        public const string MESSAGE_FROMCID = "FromCID";
        public const string MESSAGE_TOCID = "ToUserCID";
        public const string MESSAGE_MESSAGE = "Message";
        public const string MESSAGE_MESSAGETITLE = "MessageTitle";
        public const string MESSAGE_CREATEDATE = "CreateDate";
        public const string MESSAGE_STATE = "State";
        public const string MESSAGE_MESSAGESOURCETYPE = "MessageSourceType";
        public const string MESSAGE_ATTACHMENTBMCID = "AttachmentBMCID";
       
        public const string ADVISERID = "AdviserID";
        public const string ADVISERCID = "AdviserCID";
        public const string IFANAME = "IFANAME";
        public const string IFACID = "IFACID"; 
       
        public const String USER_DETAIL_TABLE = "UserDetail";
        public const String SECURITYGROUP_DETAIL_TABLE = "SecurityGroup";

        public const String USER_USERID_FIELD = "CID";
        public const String USER_USERNAME_FIELD = "Username";
        public const String USER_PASSWORD_FIELD = "Password";
        public const String USER_ACTIVE_FIELD = "Active";
        public const String USER_LASTLOGGEDIN_FIELD = "LastLoggedIn";
        public const String USER_EMAILADDRESS_FIELD = "EmailAddress";
        public const String USER_FAILEDATTEMPTS_FIELD = "FailedAttempts";
        public const String USER_LOCKED_FIELD = "Locked";
        public const String USER_ADMINISTRATOR_FIELD = "Administrator";
        public const String USER_FIRSTNAME_FIELD = "Firstname";
        public const String USER_LASTNAME_FIELD = "Lastname";
        public const String USER_PRIVTEMPLATECID_FIELD = "PRIVTEMPLATECID";

        public const String USER_REPORTINGUNITNAME_FIELD = "ReportingUnitName";
        public const String USER_REPORTINGUNITCLID_FIELD = "ReportingUnitCLID";

        public const String USER_SCENARIONAME_FIELD = "ScenarioName";
        public const String USER_SCENARIOCSID_FIELD = "ScenarioCSID";

        public const String USER_PERIODNAME_FIELD = "PeriodName";
        public const String USER_PERIODCLID_FIELD = "PeriodCLID";

        public const String USER_DISPLAY_ZERO_PREFERENCE_FIELD = "DisplayZeroPreference";

        public const String USER_ORGANISATION_CID_FIELD = "OrganisationCID";
        public const String USER_ORGANISATION_NAME_FIELD = "OrganisationName";

        public const String USER_ADMINISTRATOR_TYPE_FIELD = "AdministratorType";
        public const String USER_USER_TYPE_FIELD = "UserType";
        public const String USER_USER_TYPE_ENUM_FIELD = "UserTypeEnum";

        public const String SECURITYGROUP_USERID_FIELD = "UserCID";
        public const String SECURITYGROUP_NAME_FIELD = "GroupName";
        public string DBUserCID = String.Empty; 
        string strGroupName;

        //Flag to tell if the details for all users should be retrieved or only for a specific user
        public bool UserList = false;

        //Flag to set if the details are pertaining to a new user being created.
        public bool IsNew = false;

        public String GroupName
        {
            get { return strGroupName; }
            set { strGroupName = value; }
        }

        public DataTable OrganisationalDetailsTable
        {
            get
            {
                return this.Tables[OrganisationsDetailsDS.ORGANISATIONAL_DETAILS_TABLE];
            }
        }

        public DBUserDetailsDS()
        {
            DataTable dtMessage;

            dtMessage = new DataTable(MESSAGE_TABLE);

            dtMessage.Columns.Add(MESSAGE_ID, typeof(System.Guid));
            dtMessage.Columns.Add(MESSAGE_FROMCID, typeof(System.Guid));
            dtMessage.Columns.Add(MESSAGE_TOCID, typeof(System.Guid));
            dtMessage.Columns.Add(MESSAGE_MESSAGETITLE, typeof(System.String));
            dtMessage.Columns.Add(MESSAGE_MESSAGE, typeof(System.String));
            dtMessage.Columns.Add(MESSAGE_STATE, typeof(System.Int32));
            dtMessage.Columns.Add(MESSAGE_MESSAGESOURCETYPE, typeof(System.Int32));
            dtMessage.Columns.Add(MESSAGE_ATTACHMENTBMCID, typeof(System.Guid));
            dtMessage.Columns.Add(MESSAGE_CREATEDATE, typeof(System.DateTime));
            DataColumn[] dcPrimaryKeyMessage = { dtMessage.Columns[USER_USERID_FIELD] };
            dtMessage.PrimaryKey = dcPrimaryKeyMessage;
            this.Tables.Add(dtMessage);

            DataTable dtUser;

            dtUser = new DataTable(USER_DETAIL_TABLE);

            dtUser.Columns.Add(USER_USERID_FIELD, typeof(System.Guid));
            dtUser.Columns.Add(USER_USERNAME_FIELD, typeof(System.String));
            dtUser.Columns.Add(USER_PASSWORD_FIELD, typeof(System.String));
            dtUser.Columns.Add(USER_ACTIVE_FIELD, typeof(System.Int32));
            dtUser.Columns.Add(USER_ADMINISTRATOR_FIELD, typeof(System.Int32));
            dtUser.Columns.Add(USER_LASTLOGGEDIN_FIELD, typeof(System.DateTime));
            dtUser.Columns.Add(USER_EMAILADDRESS_FIELD, typeof(System.String));
            dtUser.Columns.Add(USER_FAILEDATTEMPTS_FIELD, typeof(System.String));
            dtUser.Columns.Add(USER_LOCKED_FIELD, typeof(System.Int32));
            dtUser.Columns.Add(USER_FIRSTNAME_FIELD, typeof(System.String));
            dtUser.Columns.Add(USER_LASTNAME_FIELD, typeof(System.String));
            dtUser.Columns.Add(USER_PRIVTEMPLATECID_FIELD, typeof(System.Guid));
            dtUser.Columns.Add(USER_REPORTINGUNITNAME_FIELD, typeof(System.String));
            dtUser.Columns.Add(USER_REPORTINGUNITCLID_FIELD, typeof(System.Guid));
            dtUser.Columns.Add(USER_SCENARIONAME_FIELD, typeof(System.String));
            dtUser.Columns.Add(USER_SCENARIOCSID_FIELD, typeof(System.Guid));
            dtUser.Columns.Add(USER_PERIODNAME_FIELD, typeof(System.String));
            dtUser.Columns.Add(USER_PERIODCLID_FIELD, typeof(System.Guid));
            dtUser.Columns.Add(USER_DISPLAY_ZERO_PREFERENCE_FIELD, typeof(System.Boolean));
            dtUser.Columns.Add(USER_ORGANISATION_CID_FIELD, typeof(System.Guid));
            dtUser.Columns.Add(USER_ADMINISTRATOR_TYPE_FIELD, typeof(System.Int32));
            dtUser.Columns.Add(USER_USER_TYPE_FIELD, typeof(System.Int32));
            dtUser.Columns.Add(USER_ORGANISATION_NAME_FIELD, typeof(System.String));
            dtUser.Columns.Add(USER_USER_TYPE_ENUM_FIELD, typeof(UserType));
            dtUser.Columns.Add(ADVISERCID, typeof(System.Guid));
            dtUser.Columns.Add(ADVISERID, typeof(System.String));
            dtUser.Columns.Add(IFANAME, typeof(System.String));
            dtUser.Columns.Add(IFACID, typeof(System.Guid));


            DataColumn[] dcPrimaryKey = { dtUser.Columns[USER_USERID_FIELD] };
            dtUser.PrimaryKey = dcPrimaryKey;

            this.Tables.Add(dtUser);
            BrokerManagedComponentDS.AddVersionStamp(dtUser, typeof(DBUserDetailsDS));

            DataTable dtGroup;

            dtGroup = new DataTable(SECURITYGROUP_DETAIL_TABLE);
            dtGroup.Columns.Add(SECURITYGROUP_USERID_FIELD, typeof(System.Guid));
            dtGroup.Columns.Add(SECURITYGROUP_NAME_FIELD, typeof(System.String));

            this.Tables.Add(dtGroup);
            BrokerManagedComponentDS.AddVersionStamp(dtGroup, typeof(DBUserDetailsDS));

            this.CreateOrganisationalDetailsTable();
        }

        private void CreateOrganisationalDetailsTable()
        {
            DataTable organisationalDetailsTable = OrganisationsDetailsDS.BuildOrganisationalDetailsTable();
            this.Tables.Add(organisationalDetailsTable);
            BrokerManagedComponentDS.AddVersionStamp(organisationalDetailsTable, typeof(DBUserDetailsDS));
        }
    }
}
