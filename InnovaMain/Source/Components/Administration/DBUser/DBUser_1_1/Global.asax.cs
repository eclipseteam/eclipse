using Oritax.TaxSimp.Services.CMBroker;
using System;
using System.Collections;
using System.ComponentModel;
using System.Web;
using System.Web.SessionState;
using System.Web.Security;
using System.Security.Principal;
using Oritax.TaxSimp.Security;
using System.Threading;
using System.Globalization;
using System.IO;
using Oritax.TaxSimp.Utilities;
using System.Configuration;
using Oritax.TaxSimp.Services;

namespace TX360Enterprise
{
	/// <summary>
	/// Summary description for Global.
	/// </summary>
	public class Global : System.Web.HttpApplication
	{
		public Global()
		{
			InitializeComponent();
		}	
		
		protected void Application_Start(Object sender, EventArgs e)
		{

		}
 
		protected void Session_Start(Object sender, EventArgs e)
		{
		}

		protected void Application_BeginRequest(Object sender, EventArgs e)
		{
			try 
			{
				Thread.CurrentThread.CurrentCulture =CultureInfo.CreateSpecificCulture("en-au");
			}
			catch (ArgumentException) 
			{
				// Do nothing if CreateSpecificCulture fails
			}
		}

		protected void Application_EndRequest(Object sender, EventArgs e)
		{
		}

		protected void Application_AuthenticateRequest(Object sender, EventArgs e)
		{
			String cookieName = FormsAuthentication.FormsCookieName;
			HttpCookie authCookie = Context.Request.Cookies[cookieName];
			
			if( authCookie == null )
			{
				return;
			}	
		
			FormsAuthenticationTicket authTicket = null;
			try
			{
				authTicket = FormsAuthentication.Decrypt(authCookie.Value);
			}
			catch
			{
				return;
			}
	
			if(authTicket == null) 
			{
				return;		
			}	

			if (authTicket.Expired)
			{
                UserAuthentication.LogUserOut(authTicket.Name);
				return;
			}

            if (!UserAuthentication.IsUserLoggedIn(authTicket.Name))
			{
				FormsAuthentication.SignOut();
				Response.Redirect("Login_1_1.aspx?ReturnURL=" + HttpUtility.UrlEncode(Request.Url.ToString()));
				return;
			}
	
			FormsAuthenticationTicket newAuthTicket = new FormsAuthenticationTicket(authTicket.Version, authTicket.Name, authTicket.IssueDate, DateTime.Now.AddMinutes(UserAuthentication.AuthenticationTimeout), false, authTicket.UserData);
			string encryptedTicket = FormsAuthentication.Encrypt(newAuthTicket);
			authCookie.Value= encryptedTicket;
			
			Context.Response.Cookies.Set(authCookie);

            UserAuthentication.UserRefreshedSession(authTicket.Name);
	
			//When the ticket was created, the UserData property was assigned a
			//pipe-delimited string of group names.
			String[] groups = newAuthTicket.UserData.Split(new char[]{'|'});

			//Create an Identity.
			GenericIdentity id = new GenericIdentity(newAuthTicket.Name, "LdapAuthentication");
	
			//This principal flows throughout the request.
			TXPrincipal principal = new TXPrincipal(id, groups);

			Context.User = principal;
		}

		protected void Application_Error(Object sender, EventArgs e)
		{
			#if(!DEBUG)
			// get the setting which specifies the error handling to be off
			string strErrorTrapSetting = System.Configuration.ConfigurationManager.AppSettings[ "GlobalErrorTrap" ];

			if ( strErrorTrapSetting == null || strErrorTrapSetting != "OFF" )
			{
				// don't want to recurse if there is an error on the error page
				if ( Request.Url.PathAndQuery.IndexOf( "globalerror" ) < 0 )
				{
					string strErrorLogFile = "";
					try
					{
						string strBaseFolder = this.Request.PhysicalApplicationPath;
						if ( !strBaseFolder.EndsWith( "\\" ) )
							strBaseFolder += "\\";

						strBaseFolder += "ErrorLog\\";
				
						if ( !Directory.Exists( strBaseFolder ) )
							Directory.CreateDirectory( strBaseFolder );
			
						strErrorLogFile = "Error_" + DateTime.Now.Date.Day.ToString( ) +
							DateTime.Now.Date.Month.ToString( ) + 
							DateTime.Now.Date.Year.ToString( ) + "_" +
							DateTime.Now.TimeOfDay.Hours.ToString( ) +
							DateTime.Now.TimeOfDay.Minutes.ToString( ) +
							DateTime.Now.TimeOfDay.Seconds.ToString( ) + "_" +
							DateTime.Now.Millisecond.ToString( ) + ".xml";

						// we assume that the file does not exist (due to the randomness of the file name)
						// if it does happen, the the user will go to the catch block and exit (without the log being written)
						ErrorCollector.WriteErrorAsXML( this.Context, Context.Error.GetBaseException( ), strBaseFolder + strErrorLogFile ); 
					}
					catch
					{
						// if there is an error, ignore it, otherwise the system will
						// end up in a loop on the error handler.
					}
					finally
					{
						string strHelpLink = Context.Error.GetBaseException().HelpLink;
						this.Response.Redirect(string.Format("nonauthenticated/globalerror_1_1.aspx?ErrorLog={0}",strErrorLogFile), true );
					}
				}
			}
#endif
        }

		protected void Session_End(Object sender, EventArgs e)
		{
		}

		protected void Application_End(Object sender, EventArgs e)
		{
		}
			
		#region Web Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
		}
		#endregion
	}
}

