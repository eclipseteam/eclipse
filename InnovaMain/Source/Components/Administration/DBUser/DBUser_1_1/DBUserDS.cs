using System;
using System.Data;
using System.Data.SqlClient;

using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.Utilities;


namespace Oritax.TaxSimp.Security
{
	/// <summary>
	/// Summary description for DBUserDS.
	/// </summary>
	[Serializable]
	public class DBUserDS  : SystemModuleBaseDS
	{
		public const String DBUSER_TABLE				= "DBUSERCM";

		public const String DBUSERLASTLOGGED_FIELD		= "LASTLOGGED";
		public const String DBUSERFAILEDATTEMPTS_FIELD	= "FAILEDATTEMPTS";
		public const String DBUSERLOCKED_FIELD			= "LOCKED";

		public DBUserDS() : base()
		{
			DataTable dt;

			dt = new DataTable( DBUSER_TABLE );
			dt.Columns.Add( DBUSERLASTLOGGED_FIELD, typeof( System.DateTime ) );
			dt.Columns.Add( DBUSERFAILEDATTEMPTS_FIELD, typeof( int ) );
			dt.Columns.Add( DBUSERLOCKED_FIELD, typeof( int ) );
			this.Tables.Add( dt );
			BrokerManagedComponentDS.AddVersionStamp(dt,typeof(DBUserDS));
		}

		/// <summary>
		/// Convert a V1.0.3.0 dataset to a V1.1.0.0 dataset
		/// 
		/// Fortunately they are identical at this level
		/// </summary>
		/// <param name="data">The orginal untampered dataset</param>
		/// <param name="convData">A converted dataset passed in from a previous conversion function</param>
		/// <returns>A converted dataset</returns>
		public static DataSet ConvertV_1_0_1_0toV_1_1_0_0DataSet(DataSet data, DataSet convData)
		{
			//Swallowing errors
			//The dataset passed in may not be a 1_0_3_0 dataset
			//The whole shape of the dataset may have changed
			//We want to carry on regardless even if there are errors
			//so that other convert functions can have a chance of converting.

			if(convData == null)
				return null;

			//In complete contradiction to normal software development standards we DO want to use
			//Literals and not constants here. This hard codes the software to the old version.
			string fn = BrokerManagedComponentDS.GetFullName(convData.Tables["DBUSERCM"]);
			AssemblyFullName afn = new AssemblyFullName(fn);

			//if not the appropriate verion for correction then just return back
			if(afn.Version.ToString() != "1.0.1.0")
				return convData;
			
			//the conversion is merely changing the full name to the correct version number.
			afn.Version = new Version("1.1.0.0");

			DataSet ds = new DataSet();
			DataTable table;

			table = new DataTable( "DBUSERCM" );
			table.Columns.Add( "LASTLOGGED", typeof( System.DateTime ) );
			table.Columns.Add( "FAILEDATTEMPTS", typeof( int ) );
			table.Columns.Add( "LOCKED", typeof( int ) );
			ds.Tables.Add( table );
			
			table = new DataTable( "UserDetail" );
			table.Columns.Add( "CID", typeof( System.Guid) );
			table.Columns.Add( "Username", typeof( System.String ) );
			table.Columns.Add( "Password", typeof( System.String ) );
			table.Columns.Add( "Active", typeof( System.Int32 ) );
			table.Columns.Add( "LastLoggedIn", typeof( System.DateTime ) );
			table.Columns.Add( "EmailAddress", typeof( System.String ) );
			table.Columns.Add( "FailedAttempts", typeof( System.String ) );
			table.Columns.Add( "Locked", typeof( System.Int32) );
			table.Columns.Add( "Administrator", typeof( System.Int32) );
			table.Columns.Add( "Firstname", typeof( System.String ) );
			table.Columns.Add( "Lastname", typeof( System.String ) );
			table.Columns.Add( "PRIVTEMPLATECID", typeof( System.Guid ) );
			table.Columns.Add( "ReportingUnitName", typeof( System.String ) );
			table.Columns.Add( "ReportingUnitCLID", typeof( System.Guid ) );
			table.Columns.Add( "ScenarioName", typeof( System.String ) );
			table.Columns.Add( "ScenarioCSID", typeof( System.Guid ) );
			table.Columns.Add( "PeriodName", typeof( System.String ) );
			table.Columns.Add( "PeriodCLID", typeof( System.Guid ) );
			ds.Tables.Add( table );

			table = new DataTable( "SecurityGroupDetails" );
			table.Columns.Add( "CID", typeof( System.Guid) );
			table.Columns.Add( "Groupname", typeof( System.String ) );
			ds.Tables.Add( table );

			table = new DataTable( "PasswordDetail" );
			table.Columns.Add( "CID", typeof( System.Guid) );
			table.Columns.Add( "Username", typeof( System.String ) );
			table.Columns.Add( "Password", typeof( System.String ) );
			ds.Tables.Add( table );

			ds.EnforceConstraints = false;
			ds.Merge(convData, true, MissingSchemaAction.Ignore);

			//Perform Conversions
			//None
			ds.EnforceConstraints = true;

			ds.Tables["DBUSERCM"].ExtendedProperties["FullName"] = afn.ToString();
			ds.Tables["UserDetail"].ExtendedProperties["FullName"] = afn.ToString();
			ds.Tables["SecurityGroupDetails"].ExtendedProperties["FullName"] = afn.ToString();
			ds.Tables["PasswordDetail"].ExtendedProperties["FullName"] = afn.ToString();
			
			return ds;
		}
	}
}
