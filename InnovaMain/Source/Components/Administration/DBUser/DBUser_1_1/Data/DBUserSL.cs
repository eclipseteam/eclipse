﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;
using System.Xml.Serialization;

namespace Oritax.TaxSimp.Data
{
    [Serializable]
    public class DBUserSL : UserBaseClass
    {
        [XmlElement("USER_USERID_FIELD")]
        public String USER_USERID_FIELD { get; set; } // "CID";

        [XmlElement("USER_USERNAME_FIELD")]
        public String USER_USERNAME_FIELD { get; set; } // "Username";

        [XmlElement("USER_PASSWORD_FIELD")]
        public String USER_PASSWORD_FIELD { get; set; } // "Password";

        [XmlElement("USER_ACTIVE_FIELD")]
        public String USER_ACTIVE_FIELD { get; set; } // "Active";

        [XmlElement("USER_LASTLOGGEDIN_FIELD")]
        public String USER_LASTLOGGEDIN_FIELD { get; set; } // "LastLoggedIn";

        [XmlElement("USER_EMAILADDRESS_FIELD")]
        public String USER_EMAILADDRESS_FIELD { get; set; } // "EmailAddress";

        [XmlElement("USER_FAILEDATTEMPTS_FIELD")]
        public String USER_FAILEDATTEMPTS_FIELD { get; set; } // "FailedAttempts";

        [XmlElement("USER_LOCKED_FIELD")]
        public String USER_LOCKED_FIELD { get; set; } // "Locked";

        [XmlElement("USER_ADMINISTRATOR_FIELD")]
        public String USER_ADMINISTRATOR_FIELD { get; set; } // "Administrator";

        [XmlElement("USER_FIRSTNAME_FIELD")]
        public String USER_FIRSTNAME_FIELD { get; set; } // "Firstname";

        [XmlElement("USER_LASTNAME_FIELD")]
        public String USER_LASTNAME_FIELD { get; set; } // "Lastname";

        [XmlElement("USER_PRIVTEMPLATECID_FIELD")]
        public String USER_PRIVTEMPLATECID_FIELD { get; set; } // "PRIVTEMPLATECID";

        [XmlElement("USER_REPORTINGUNITNAME_FIELD")]
        public String USER_REPORTINGUNITNAME_FIELD { get; set; } // "ReportingUnitName";

        [XmlElement("USER_REPORTINGUNITCLID_FIELD")]
        public String USER_REPORTINGUNITCLID_FIELD { get; set; } // "ReportingUnitCLID";

        [XmlElement("USER_SCENARIONAME_FIELD")]
        public String USER_SCENARIONAME_FIELD { get; set; } // "ScenarioName";

        [XmlElement("USER_SCENARIOCSID_FIELD")]
        public String USER_SCENARIOCSID_FIELD { get; set; } // "ScenarioCSID";

        [XmlElement("USER_PERIODNAME_FIELD")]
        public String USER_PERIODNAME_FIELD { get; set; } // "PeriodName";

        [XmlElement("USER_PERIODCLID_FIELD")]
        public String USER_PERIODCLID_FIELD { get; set; } // "PeriodCLID";

        [XmlElement("USER_DISPLAY_ZERO_PREFERENCE_FIELD")]
        public String USER_DISPLAY_ZERO_PREFERENCE_FIELD { get; set; } // "DisplayZeroPreference";

        [XmlElement("USER_ORGANISATION_CID_FIELD")]
        public String USER_ORGANISATION_CID_FIELD { get; set; } // "OrganisationCID";

        [XmlElement("USER_ORGANISATION_NAME_FIELD")]
        public String USER_ORGANISATION_NAME_FIELD { get; set; } // "OrganisationName";

        [XmlElement("USER_ADMINISTRATOR_TYPE_FIELD")]
        public String USER_ADMINISTRATOR_TYPE_FIELD { get; set; } // "AdministratorType";

        [XmlElement("USER_USER_TYPE_FIELD")]
        public String USER_USER_TYPE_FIELD { get; set; } // "UserType";

        [XmlElement("SECURITYGROUP_USERID_FIELD")]
        public String SECURITYGROUP_USERID_FIELD { get; set; } // "UserCID";

        [XmlElement("SECURITYGROUP_NAME_FIELD")]
        public String SECURITYGROUP_NAME_FIELD { get; set; } // "GroupName";

        [XmlElement("USER_CURRENTLYLOGGEDIN")]
        public string USER_CURRENTLYLOGGEDIN { get; set; }
    }
}