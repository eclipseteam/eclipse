﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;
using System.Xml.Serialization;

namespace Oritax.TaxSimp.Data
{
    [Serializable]
    public class DBUserOrganisationalDetailsSL
    {
        [XmlElement("ORGANISATIONAL_ID_FIELD")]
        public string ORGANISATIONAL_ID_FIELD { get; set; } // "ORGANISATIONAL_ID_FIELD";

        [XmlElement("ORGANISATIONAL_NAME_FIELD")]
        public string ORGANISATIONAL_NAME_FIELD { get; set; } // "ORGANISATIONAL_NAME_FIELD";
    }
}
