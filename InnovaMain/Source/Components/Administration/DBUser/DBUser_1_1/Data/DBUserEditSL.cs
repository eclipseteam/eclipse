﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;
using System.Xml.Serialization;

namespace Oritax.TaxSimp.Data
{
    [Serializable]
    public class DBUserEditSL : UserBaseClass
    {
        [XmlElement("OrganisationalDetailsList")]
        public List<DBUserOrganisationalDetailsSL> OrganisationalDetailsList { get; set; }

        [XmlElement("DBUserSLDetail")]
        public DBUserSL DBUserSLDetail { get; set; }

        [XmlElement("LoginName")]
        public String LoginName { get; set; }

        [XmlElement("FirstName")]
        public String FirstName { get; set; }

        [XmlElement("LastName")]
        public String LastName { get; set; }

        [XmlElement("Email")]
        public String Email { get; set; }

        [XmlElement("Password")]
        public String Password { get; set; }

        [XmlElement("ConfirmPassword")]
        public String ConfirmPassword { get; set; }

        [XmlElement("IsAdministrator")]
        public bool IsAdministrator { get; set; }

        [XmlElement("OrganisationGuid")]
        public String OrganisationGuid { get; set; }

        [XmlElement("IsActive")]
        public bool IsActive { get; set; }

        [XmlElement("IsLocked")]
        public bool IsLocked { get; set; }

        [XmlElement("UserType")]
        public int UserType { get; set; }
    }
}

