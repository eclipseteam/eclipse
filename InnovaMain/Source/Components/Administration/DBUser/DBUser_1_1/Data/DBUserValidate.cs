﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;
using System.Xml.Serialization;

namespace Oritax.TaxSimp.Data
{
    [Serializable]
    public class DBUserValidate
    {
        [XmlElement("IsValid")]
        public bool IsValid { get; set; }

        [XmlElement("ErrorMessage")]
        public String ErrorMessage { get; set; }

        [XmlElement("ShowError")]
        public bool ShowError { get; set; }

        [XmlElement("PasswordRulesList")]
        public List<DBUserPasswordRulesSL> PasswordRulesList { get; set; }
    }
}
