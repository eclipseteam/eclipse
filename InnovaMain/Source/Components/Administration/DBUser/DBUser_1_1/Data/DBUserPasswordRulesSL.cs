﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;
using System.Xml.Serialization;

namespace Oritax.TaxSimp.Data
{
    [Serializable]
    public class DBUserPasswordRulesSL
    {
        [XmlElement("PasswordKeyField")]
        public string PasswordKeyField { get; set; } // "Key";

        [XmlElement("PasswordDescriptionField")]
        public string PasswordDescriptionField { get; set; } // "Description";

        [XmlElement("PasswordResultField")]
        public string PasswordResultField { get; set; } // "Result";
    }
}
