using System;
using System.Configuration;
using System.Globalization;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Security;

using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.TaxSimpLicence;
using PresentationControls;
using Oritax.TaxSimp.SAS70Password;
using Oritax.TaxSimp.Services;

using Oritax.TaxSimp.BusinessStructureUtilities;
using Oritax.TaxSimp.CM.Group;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.CM.Period;
using Oritax.TaxSimp.Services.CMBroker;



namespace Oritax.TaxSimp.Security
{
    /// <summary>
    /// 
    /// </summary>
    public class Login : SystemModuleWorkpaper
    {
        protected System.Web.UI.WebControls.Button btnlogin;
        protected System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidatorPassword;
        protected System.Web.UI.WebControls.TextBox txtLoginName;
        protected System.Web.UI.WebControls.TextBox txtPassword;
        protected System.Web.UI.WebControls.Label lblError;
        protected System.Web.UI.WebControls.Label lblLoginMessage;
        protected System.Web.UI.WebControls.Label lblLoginTitle;
        protected System.Web.UI.WebControls.Label lblLicenceMessage;
        protected System.Web.UI.WebControls.Label lblBuild;
        protected System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator_loginname;
        private const string expireWarning = "Your current password has expired.<br/>Please change your password to continue or contact your system administrator for assistance.";
        private const string invalidWarning = "Your current password does not conform with the current password restrictions in N-ABLE.<br/>Please change your password to continue or contact your system administrator for assistance.";
        private const string inReminderWarning = "Your password is due to expire in {0} days.<br/>Do you wish to change your password now?";

        #region Fields

        //public const int versionMajorField = 2;
        //public const int versionMinorField = 0;
        public const int versionMajorField = 4;
        public const int versionMinorField = 0;

        #endregion // Fields

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            this.btnlogin.Click += new System.EventHandler(this.btnlogin_Click);
            // Attach the customized event handler to handler message box event					
            this.MessageBoxButtonClicked += new ButtonPressedEventHandler(this.ChangePassword_MessageBox_ButtonClicked);
        }
        #endregion

        #region Overriden Properties
        protected override string BMCTypeName { get { return OrganizationInstall.ASSEMBLY_NAME; } }
        protected override Guid BMCTypeID { get { return new Guid(OrganizationInstall.ASSEMBLY_ID); } }
        protected override string TransactionCommitErrorActionDescription { get { return "Login page"; } }

        protected override Stylesheet[] WorkpaperSpecificStylesheets
        {
            get
            {
                string strStyles = "BODY { FONT-SIZE: 12px; MARGIN: 0px; COLOR: #fff; FONT-FAMILY: Tahoma; BACKGROUND-COLOR: #696969 }"
                + "A {FONT-WEIGHT: normal; COLOR: #9fc513 }"
                + "P { FONT-WEIGHT: bold; FONT-SIZE: 12px }"
                + "#btnlogin { FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: #fff; BACKGROUND-COLOR: #3b8593 }"
                + "#startbanner { MARGIN-BOTTOM: 25px; WIDTH: 100%; HEIGHT: 73px; BACKGROUND-COLOR: #9fc513 }"
                + "#bannerimage { BACKGROUND-IMAGE: url(images/banner_1_1.gif); WIDTH: 166px; HEIGHT: 73px }"
                + "#loginarea { BORDER-RIGHT: 0px; BORDER-TOP: 0px; MARGIN-LEFT: 166px; BORDER-LEFT: 0px; WIDTH: 500px; BORDER-BOTTOM: 0px }"
                + "#build { MARGIN-LEFT: 166px }"
                + "#version { MARGIN-LEFT: 166px }"
                + ".loginmessage { MARGIN-LEFT: 166px }"
                + "#loginnablelogo { DISPLAY: inline; BACKGROUND-IMAGE: url(images/logo_1_1.gif); VERTICAL-ALIGN: middle; WIDTH: 169px; BACKGROUND-REPEAT: no-repeat; HEIGHT: 65px }"
                + ".arrow{background-image: url(images/greenArrow_1_1.gif); background-repeat:no-repeat; width:23px; background-position: center center; )";

                Stylesheet objSS = new Stylesheet(StylesheetMedia.Screen, StylesheetType.Inline, String.Empty);
                objSS.StylesheetInline = strStyles;
                return new Stylesheet[] { objSS };
            }
        }

        public override Guid WorkpaperID { get { return new Guid("{7340F44D-2A53-4229-87A4-ABF55475E942}"); } }
        protected override HeaderType WorkpaperHeaderType { get { return HeaderType.None; } }
        protected override string WorkpaperName { get { return String.Empty; } }

        #endregion

        #region Overriden Methods
        public override DataSet CreateDataSet()
        {
            OrganizationDS orgDS = new OrganizationDS();
            return orgDS;
        }

        protected override void InitialiseIDs(Guid objInstanceID)
        {
            ICalculationModule organisation = this.Broker.GetWellKnownCM(WellKnownCM.Organization);
            base.InitialiseIDs(organisation.CIID);
            this.Broker.ReleaseBrokerManagedComponent(organisation);
        }


        public override void PopulateWorkpaper(DataSet data)
        {
            string adminMessage = string.Empty;
            adminMessage = "";
            adminMessage = adminMessage.Trim();
            if (adminMessage != "")
            {
                lblLoginTitle.Text = "";
                lblLoginMessage.Text = adminMessage.Replace("\n", "<br>");
            }

            //string licenceMessage = (string)data.Tables[OrganizationDS.ORGANIZATION_TABLE].Rows[0][OrganizationDS.LICENCEMESSAGE_FIELD];
            string licenceMessage = string.Empty;
            licenceMessage = licenceMessage.Trim();
            if (licenceMessage != "")
            {
                lblLicenceMessage.Text = licenceMessage.Replace("\n", "<br>");
            }

            lblBuild.Text = "(Build " + this.Broker.ProductVersion + ")";
        }

        public override void DepopulateWorkpaper(DataSet objData)
        {
        }

        #endregion

        protected void Page_Load(object sender, System.EventArgs e)
        {
            Version version = Environment.Version;
            if (version.Major != versionMajorField || version.Minor != versionMinorField)
            {
                try
                {
                    Response.Redirect("versionerror_1_1.aspx");
                    base.StopFurtherOutput();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        private DataSet ConstructAuthenticationDataSet()
        {
            AuthenticateUserDS authenticateDS = new AuthenticateUserDS();
            DataRow drUser;

            if (authenticateDS.Tables.Contains(AuthenticateUserDS.AUTHENTICATION_DETAIL_TABLE))
            {
                drUser = authenticateDS.Tables[AuthenticateUserDS.AUTHENTICATION_DETAIL_TABLE].NewRow();
                drUser[AuthenticateUserDS.AUTHENTICATION_USERNAME_FIELD] = txtLoginName.Text;
                drUser[AuthenticateUserDS.AUTHENTICATION_PASSWORD_FIELD] = txtPassword.Text;

                authenticateDS.Tables[AuthenticateUserDS.AUTHENTICATION_DETAIL_TABLE].Rows.Add(drUser);

                return authenticateDS;
            }
            else
            {
                throw new Exception("Login.ConstructAuthenticationDataSet - Authentication table not found.");
            }
        }

        private bool licenceExpired()
        {
            ILicence licence = (ILicence)this.Broker.GetWellKnownBMC(WellKnownCM.Licencing);
            try
            {
                return !licence.IsLicenceValid();
            }
            finally
            {
                this.Broker.ReleaseBrokerManagedComponent(licence);
            }
        }

        private void btnlogin_Click(object sender, System.EventArgs e)
        {
            /* 01MAR03 - SC - Whereas previously calling AuthenticateUser( username, password ). This was percieved as
             * a problematic way of performing Authentication. Methodology now consists of constructing an AuthenticateUserDS
             * DataSet, passing this to the DBUser (via ExtractData) and performing an analysis of the returned DataSet.
             */
            FormsAuthenticationTicket authTicket = null;
            HttpCookie authCookie = null;

            DBUser objUser;
            SAS70PasswordSM pwdSM;
            AuthenticateUserDS dsAuthenticate;
            DBSecurityGroupDetailsDS dsSecurityGroup;
            PasswordDS dsPassword = new PasswordDS();
            DataRow drUser = null;
            //string redirectWorkpaper = "Entities_1_1.aspx";
            //string redirectWorkpaper = "TaxSimpSLDefault.aspx";
            string redirectWorkpaper = "Dashboard.aspx";

            //bool denyLogins = (bool)this.PostbackDataSet.Tables[OrganizationDS.ORGANIZATION_TABLE].Rows[0][OrganizationDS.DENYNEWLOGINS_FIELD];
            bool denyLogins = false;

            if (licenceExpired())
            {
                if (txtLoginName.Text.ToLower() != "administrator")
                {
                    this.lblError.Text = "Unable to login as the licence has expired";
                    return;
                }
                else
                {
                    redirectWorkpaper = "ViewLicence_1_1.aspx";
                }
            }

            if (denyLogins && txtLoginName.Text.ToLower() != "administrator")
            {
                this.lblError.Text = new MessageBoxDefinition(336).ToString();
                return;
            }

            try
            {
                //Construct a DBUser object and pass username/password details to this object via a AuthenticateUserDS dataset.
                // see: DBUser.ExtractData()
                this.Broker.SetComplete();
                this.Broker.SetWriteStart();
                //IIS cachce has little no value going forward  hence clearing cache has no impact
                this.Broker.ClearCache();

                this.InitialiseIDs(Guid.Empty);
                this.InitialiseObjects();

                objUser = (DBUser)Broker.GetBMCInstance(txtLoginName.Text, "DBUser_1_1");
                pwdSM = (SAS70PasswordSM)this.Broker.GetWellKnownBMC(WellKnownCM.SecurityProvider);

                if (objUser != null && pwdSM != null)
                {
                    // User name is valid, so ExtractData to perform password comparison.
                    dsAuthenticate = (AuthenticateUserDS)this.ConstructAuthenticationDataSet();
                    objUser.GetData(dsAuthenticate);
                    objUser.GetData(dsPassword);

                    // Analyize returned dataset ( from objUser.ExtractData ), 
                    drUser = dsAuthenticate.Tables[AuthenticateUserDS.AUTHENTICATION_DETAIL_TABLE].Rows[0];
                    if (Convert.ToInt32(drUser[AuthenticateUserDS.AUTHENTICATION_AUTHENTICATE_FIELD].ToString()) == AuthenticateUserDS.AUTHENTICATE_SUCCESS)
                    {
                        // Load and add Security Groups to the Users cookie.
                        dsSecurityGroup = new DBSecurityGroupDetailsDS();
                        objUser.GetData(dsSecurityGroup);

                        string userdata = "";
                        userdata += objUser.CID.ToString() + "|";
                        DataTable dtSecurityGroup = dsSecurityGroup.Tables[DBSecurityGroupDetailsDS.SECGRP_DETAIL_TABLE];
                        foreach (DataRow drSecurityGroup in dtSecurityGroup.Rows)
                            userdata += drSecurityGroup[DBSecurityGroupDetailsDS.SECGRP_SECURITYNAME_FIELD] + "|";

                        int timeout = int.Parse(ConfigurationManager.AppSettings["AuthenticationTimeout"]);
                        authTicket = new FormsAuthenticationTicket(1, txtLoginName.Text, DateTime.Now, DateTime.Now.AddMinutes(timeout), false, userdata);
                        string encryptedTicket = FormsAuthentication.Encrypt(authTicket);
                        authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                        Response.Cookies.Add(authCookie);

                        UserAuthentication.LogUserIn(txtLoginName.Text);
                    }
                    else
                    {
                        DataRow drAuthenticate = dsAuthenticate.Tables[AuthenticateUserDS.AUTHENTICATION_DETAIL_TABLE].Rows[0];
                        int failedAttempts = Convert.ToInt32(drAuthenticate[AuthenticateUserDS.AUTHENTICATION_FAILEDATTEMPTS_FIELD], NumberFormatInfo.InvariantInfo);
                        switch (Convert.ToInt32(drUser[AuthenticateUserDS.AUTHENTICATION_AUTHENTICATE_FIELD].ToString()))
                        {

                            case AuthenticateUserDS.AUTHENTICATE_BADUSER:
                                this.lblError.Text = AuthenticateUserDS.MSG_BADPASSWORD;
                                this.Broker.LogEvent(EventType.LoginUnsuccessful, String.Format("'{0} unsuccessful login attempts {1}: Invalid login name", txtLoginName.Text, failedAttempts.ToString(CultureInfo.InvariantCulture)), EventType.SYSTEM_EVENT_TYPE, Guid.Empty);
                                break;

                            case AuthenticateUserDS.AUTHENTICATE_BADPASSWORD:
                                this.lblError.Text = AuthenticateUserDS.MSG_BADPASSWORD;
                                this.Broker.LogEvent(EventType.LoginUnsuccessful, String.Format("{0} unsuccessful login attempts {1}: Incorrect password", txtLoginName.Text, failedAttempts.ToString(CultureInfo.InvariantCulture)), EventType.SYSTEM_EVENT_TYPE, Guid.Empty);
                                if (failedAttempts >= pwdSM.MaxFailureAttempts)
                                {
                                    this.Broker.LogEvent(EventType.UserLocked, "User account locked: " + this.txtLoginName.Text.Trim(), EventType.SYSTEM_EVENT_TYPE, Guid.Empty);
                                }
                                break;

                            case AuthenticateUserDS.AUTHENTICATE_DISABLED:
                                this.lblError.Text = AuthenticateUserDS.MSG_DISABLED;
                                this.Broker.LogEvent(EventType.LoginUnsuccessful, String.Format("{0} unsuccessful login: Account is disabled", txtLoginName.Text), EventType.SYSTEM_EVENT_TYPE, Guid.Empty);
                                break;

                            case AuthenticateUserDS.AUTHENTICATE_LOCKED:
                                this.lblError.Text = AuthenticateUserDS.MSG_LOCKED;
                                this.Broker.LogEvent(EventType.LoginUnsuccessful, String.Format("{0} unsuccessful login: Account is locked", txtLoginName.Text), EventType.SYSTEM_EVENT_TYPE, Guid.Empty);
                                break;
                        }
                        objUser.SetData(dsAuthenticate);
                    }
                }
                else
                {
                    this.lblError.Text = AuthenticateUserDS.MSG_BADPASSWORD;
                    this.Broker.LogEvent(EventType.LoginUnsuccessful, String.Format("{0} unsuccessful login: Invalid login name", this.txtLoginName.Text), EventType.SYSTEM_EVENT_TYPE, Guid.Empty);
                }

                if (authCookie != null)
                {
                    //MYL 10/10/2005 - After authenticate the user, determine if the user's password needs to be updated.
                    if (objUser != null && pwdSM != null && drUser != null)
                    {
                        bool isPwdInReminder = pwdSM.IsInReminderTimeSpan(objUser.CID, DateTime.Now);
                       // bool isPwdExpired = pwdSM.IsPasswordExpired(objUser.CID, DateTime.Now);
                        bool isPwdValid = pwdSM.CheckAllRulesAtLogin(objUser.CID, objUser.Name, this.txtPassword.Text, dsPassword.Tables[PasswordDS.PasswordRulesTable], DateTime.Now, false);

                        //if (isPwdValid && !isPwdInReminder)  // override password expiry reminder for now
                        if (isPwdValid)
                        {
                            this.Broker.LogEvent(EventType.LoginSuccessful, "Login successful '" + txtLoginName.Text + "'", txtLoginName.Text, Guid.Empty);
                            this.GetOrganizationInformation();
                            directToEntities(string.Format("{0}?CLID={1}&CSID={2}", redirectWorkpaper, Organization_CLID, Organization_CSID));
                        }
                        else
                        {
                            // if the password is not Valid
                            // For NON-SAS70 based passwords.
                            if (!isPwdValid)
                            {
                                string warningString = String.Empty;
                                //if (isPwdExpired)
                                //{
                                //    warningString = expireWarning;
                                //}
                                //else
                                //{
                                //    warningString = invalidWarning;
                                //}
                                warningString = invalidWarning;
                                this.RegisterMessageBoxOnStartup("Warning", "Warning", warningString,
                                    new MessageBoxButton[] { new MessageBoxButton(MessageBoxButtonType.OK, true) },
                                    "PasswordForceToChange", PromptType.Warning);
                            }
                            //Second to check if the password is near to expire
                            //else if (isPwdExpired)
                            //{
                            //    this.RegisterMessageBoxOnStartup("Warning", "Warning", expireWarning,
                            //        new MessageBoxButton[] { new MessageBoxButton(MessageBoxButtonType.OK, true) },
                            //        "PasswordForceToChange", PromptType.Warning);
                            //}
                            //Third to check if the password is near to expire

                            /* Disable password reminder for now 
                            else if (isPwdInReminder)
                            {
                                //Raise a dialog box to ask user if he wants to update the new password
                                this.RegisterMessageBoxOnStartup("Warning", "Warning",
                                    String.Format(inReminderWarning, pwdSM.GetExpirySpan(objUser.CID, DateTime.Now).ToString()),
                                    new MessageBoxButton[] { new MessageBoxButton(MessageBoxButtonType.Yes, true), new MessageBoxButton(MessageBoxButtonType.No, true) }, "PasswordInReminder",
                                    PromptType.Warning);
                            }
                             */
                        }
                    }
                }

                if (objUser != null)
                {
                    this.Session[DBUser.sessionUserNameKey] = objUser.Name;
                    this.Session[DBUser.SessionUserPasswordKey] = txtPassword.Text;

                    if (objUser.Name.ToLower() == "administrator".ToLower())
                        this.Session[DBUser.sessionIsAdminKey] = true;
                    else
                        this.Session[DBUser.sessionIsAdminKey] = objUser.Administrator;

                    this.Session[DBUser.sessionUserCIDKey] = objUser.CID.ToString();
                    this.Session["UserType"] = objUser.UserType;
                }

                this.Broker.ReleaseBrokerManagedComponent(objUser);
                this.Broker.ReleaseBrokerManagedComponent(pwdSM);
            }
            catch (Exception ex)
            {
                lblError.Text = "Error authenticating. " + ex.Message;
            }
        }

        private DBUser RetrieveDBUser(string username)
        {
            return (DBUser)Broker.GetBMCInstance(txtLoginName.Text, "DBUser_1_1");
        }

        private void ChangePassword_MessageBox_ButtonClicked(object sender, MessageBoxEventArgs args)
        {
            string entityPage = "TaxSimpSLDefault.aspx";
            string pwdChangePage = "ChangeUserPassword_1_1.aspx";

            if (args.EventArgs == "PasswordForceToChange" && args.ButtonPressed == MessageBoxButtonType.OK)
            {
                //Direct to change password page
                DBUser user = this.RetrieveDBUser(this.txtLoginName.Text);
                this.directToWorkpaper(pwdChangePage, user, true);
                this.Broker.ReleaseBrokerManagedComponent(user);
            }
            else if (args.EventArgs == "PasswordInReminder" && args.ButtonPressed == MessageBoxButtonType.Yes)
            {
                DBUser user = this.RetrieveDBUser(this.txtLoginName.Text);
                this.directToWorkpaper(pwdChangePage, user, true);
                this.Broker.ReleaseBrokerManagedComponent(user);
            }
            else if (args.EventArgs == "PasswordInReminder" && args.ButtonPressed == MessageBoxButtonType.No)
            {
                DBUser user = this.RetrieveDBUser(this.txtLoginName.Text);
                this.directToWorkpaper(entityPage, user, true);
                this.Broker.ReleaseBrokerManagedComponent(user);
            }
            else
            {
                this.directToEntities(entityPage);
            }
        }

        private void directToEntities(string redirectWorkpaper)
        {
            Response.Clear();

            StringBuilder script = new StringBuilder("<script type=\"text/javascript\">");
            script.Append("strSplit = '/';");
            script.Append("mylocation = unescape(location.pathname);");
            script.Append("strInstance = mylocation.split(strSplit);");
            script.Append("myhost = unescape(location.host);");
            script.AppendFormat("fullpath = myhost + strInstance[1] + '{0}';", redirectWorkpaper);
            script.Append("sRemoveWPType  = fullpath.replace(/&WPTYPE=\\d/g,\"\");");
            script.Append("strQueryStringParse = /\\?|\\&/g;");
            script.Append("strQueryStringComponents = sRemoveWPType.split(strQueryStringParse);");
            script.Append("strQueryStringComponents.sort();");
            script.Append("sWorkpaperName = strQueryStringComponents.join(\"\");");
            script.Append(@"re = /\.|\?|\&|\/|\:|\=|\-/g;");
            script.Append("sWorkpaperName = sWorkpaperName.replace(re, \"_\");");
            //script.Append("mywindow=window.open('',sWorkpaperName,'menubar=no,status=no,toolbar=no,resizable=yes,scrollbars=no,left=0,top=0,width='+screen.availWidth+',height='+screen.availHeight);");
            script.Append("mywindow=window.open(sWorkpaperName,'_self','menubar=no,status=no,toolbar=no,resizable=yes,scrollbars=no,left=0,top=0,width='+screen.availWidth+',height='+screen.availHeight);");
            script.Append("if(mywindow.document.title == '')");
            script.Append("{");
            //script.AppendFormat("mywindow=window.open('{0}',sWorkpaperName,'menubar=no,status=no,toolbar=no,resizable=yes,scrollbars=no,left=0,top=0');", redirectWorkpaper);
            script.AppendFormat("mywindow=window.open(sWorkpaperName,'_self','menubar=no,status=no,toolbar=no,resizable=yes,scrollbars=no,left=0,top=0');", redirectWorkpaper);
            script.Append("}");
            script.Append("if (window!=mywindow) window.close();");
            script.Append("mywindow.resizeTo(screen.availWidth,screen.availHeight);");
            script.Append("mywindow.focus();");

            // KGM - ensure the window is loaded from the server and NOT
            // the clients local cache.
            script.AppendFormat("mywindow.location.replace( '{0}' );", redirectWorkpaper);

            script.Append("</script>");
            Response.Write(script);
            Response.Flush();
        }

        private void directToWorkpaper(string redirectWorkpaper, DBUser objUser, bool forceToLogout)
        {
            Response.Clear();

            StringBuilder script = new StringBuilder("<script type=\"text/javascript\">");
            script.Append("strSplit = '/';");
            script.Append("mylocation = unescape(location.pathname);");
            script.Append("strInstance = mylocation.split(strSplit);");
            script.Append("myhost = unescape(location.host);");
            script.AppendFormat("fullpath = myhost + strInstance[1] + '{0}';", redirectWorkpaper);
            script.Append("sRemoveWPType  = fullpath.replace(/&WPTYPE=\\d/g,\"\");");
            script.Append("strQueryStringParse = /\\?|\\&/g;");
            script.Append("strQueryStringComponents = sRemoveWPType.split(strQueryStringParse);");
            script.Append("strQueryStringComponents.sort();");
            script.Append("sWorkpaperName = strQueryStringComponents.join(\"\");");
            script.AppendFormat("sWorkpaperName = sWorkpaperName + '\\CID={0}';", objUser.CID.ToString());
            script.Append(@"re = /\.|\?|\&|\/|\:|\=|\-/g;");
            script.Append("sWorkpaperName = sWorkpaperName.replace(re, \"_\");");
            script.Append("mywindow=window.open('',sWorkpaperName,'menubar=no,status=no,toolbar=no,resizable=yes,scrollbars=no,left=0,top=0,width='+screen.availWidth+',height='+screen.availHeight);");
            script.Append("if(mywindow.document.title == '')");
            script.Append("{");
            script.AppendFormat("mywindow=window.open('{0}',sWorkpaperName,'menubar=no,status=no,toolbar=no,resizable=yes,scrollbars=no,left=0,top=0');", redirectWorkpaper);
            script.Append("}");
            script.Append("if (window!=mywindow) window.close();");
            script.Append("mywindow.resizeTo(screen.availWidth,screen.availHeight);");
            script.Append("mywindow.focus();");

            // KGM - ensure the window is loaded from the server and NOT
            // the clients local cache.
            string replaceUrl = redirectWorkpaper + "?CID=" + objUser.CID.ToString() + "&ForceToChange=";
            if (forceToLogout)
            {
                replaceUrl += "1";
            }
            else
            {
                replaceUrl += "0";
            }
            script.AppendFormat("mywindow.location.replace( '{0}' );", replaceUrl);

            script.Append("</script>");
            Response.Write(script);
            Response.Flush();
        }

        private string Organization_CLID { get; set; }
        private string Organization_CSID { get; set; }

        private void GetOrganizationInformation()
        {
            //InitialiseBroker();
            //string userName = HttpContext.Current.User.Identity.Name;
            OrganizationCM organization = (OrganizationCM)this.Broker.GetWellKnownBMC(WellKnownCM.Organization);
            Organization_CLID = organization.CLID.ToString();
            Organization_CSID = organization.CSID.ToString();
            //string entityWorkpaperStructure = organisationCm.GetEntityWorkpaperStructure(userName);            
            //return entityWorkpaperStructure;
        }

    }
}
