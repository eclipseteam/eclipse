<%@ Assembly Name="DBUser_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215" %>

<%@ Page Language="c#" Codebehind="login_1_1.aspx.cs" AutoEventWireup="false" Inherits="Oritax.TaxSimp.Security.Login" %>

<div id="wrap" class="clearfix">
    <div id="header">
        <div class="logo">
            <img src="images/logo2.png" alt="TaxSimplifier" /></div>
        <div class="TRlogo">
           
    </div>
    <!-- end header -->
    <div id="contentLogin">llll     
        <img  src="images/nextgenbanner.jpg"/>
        <table>
            <tr>
            
            <td Width=20px>
                    <div class="description">
                        <p style="color:#362c66">
                            <asp:label visible=false Font-Size="7pt" id="lblLoginTitle" runat="server"></asp:label>
                        </p>
                        <p >
                            <asp:label visible=false Font-Size="7pt"  ForeColor="#362c66" id="lblLoginMessage" runat="server"></asp:label>
                        </p>
                        <p >
                            <asp:label visible=false Font-Size="7pt"  ForeColor="#696969" id="lblLicenceMessage" runat="server"></asp:label>
                        </p>
                        <p id="P1" style="color:#696969; font-weight:bold; Font-Size="7pt" ">
                            </p>
                        <p id="P2">
                            <asp:label Font-Size="7pt" visible=false   ForeColor="#696969" Font-Bold=true id="lblBuild" runat="server"></asp:label>
                        </p>
                    </div>
                </td>
                <td>
                    <p style="color:#362c66">
                        e-clipse online Login</p>
                    <p>
                        <label for="fname" style="color:#362c66">
                            Username</label>
                        <asp:textbox id="txtLoginName" runat="server" maxlength="50" cssclass="large"></asp:textbox>
                        &nbsp;<asp:requiredfieldvalidator id="RequiredFieldValidator_loginname" runat="server"
                            cssclass="LoginError" controltovalidate="txtLoginName" errormessage="User name must be completed"
                            forecolor="#666666"> Login name must be completed.</asp:requiredfieldvalidator>
                    </p>
                      <p>
                        <label for="fname" style="color:#362c66">
                            Password</label>
                        <asp:textbox id="txtPassword" runat="server" maxlength="32" cssclass="large" textmode="password"></asp:textbox>
                        &nbsp;<asp:requiredfieldvalidator id="RequiredFieldValidatorPassword" runat="server"
                            cssclass="LoginError" controltovalidate="txtPassword" errormessage="Password must be completed"
                            forecolor="#666666"> Password must be completed.</asp:requiredfieldvalidator>
                    </p>
   <asp:button id="btnlogin" BackColor="#362c66" Width=100px ForeColor=White runat="server" text="Log in"></asp:button>
                       <asp:label id="lblError" runat="server" style="color:#362c66" cssclass="LoginError"></asp:label>
                </td>
            </tr>
        </table>
        <div id="footer-wrap">
            <div id="footer">
                <address>
                    � 2010 - 2011 e-clipse online
                </address>
                <ul>
                    <li><a href="#"></a></li>
                </ul>
            </div>
            <!-- end footer -->
        </div>
        <!-- end footer-wrap -->
    </div>
</div>
<!-- end content-->
</div>
<!-- end wrap-->
