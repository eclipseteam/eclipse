using Oritax.TaxSimp.CalculationInterface;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Web;

namespace Oritax.TaxSimp.Security
{
    public static class CurrentUser
    {
        #region Public Properties

        public static bool DisplayZeroPreference
        {
            get
            {
                return getUser().DisplayZeroPreference;
            }

            set
            {
                DBUser user = getUser();
                user.DisplayZeroPreference = value;
                user.CalculateToken(true);
            }
        }

        #endregion // Public Properties

        #region Private Properties

        private static DBUser getUser()
        {
            HttpContext context = HttpContext.Current;
            string userName = HttpContext.Current.User.Identity.Name;
            LocalDataStoreSlot brokerSlot = Thread.GetNamedDataSlot("Broker");
            ICMBroker broker = (ICMBroker)Thread.GetData(brokerSlot);
            DBUser dbUser = (DBUser)broker.GetBMCInstance(userName, "DBUser_1_1");
            return dbUser;
        }

        #endregion // Protected and Internal Properties
    }
}
