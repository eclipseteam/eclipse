﻿using System;
using System.Data;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.SAS70Password;
using System.Diagnostics;

namespace Oritax.TaxSimp.Security
{
    public partial class DBUser
    {
        private void SetPassword(string _userid,string _newpassword)
        {
            PasswordDS objDS = new PasswordDS();
            this.OnGetData(objDS);
            this.InitiateVariables();
            DataRow[] drCollection = objDS.Tables[PasswordDS.PasswordDetailsTable].Select(PasswordDS.PasswordUserIdField + " = '" + _userid + "'", String.Empty, DataViewRowState.CurrentRows);
            Guid userId = new Guid(_userid);
            string userName = String.Empty;
            if (drCollection.Length == 1)
            {
                drCollection[0][PasswordDS.PasswordUserIdField] = userId;
                drCollection[0][PasswordDS.PasswordPasswordField] = Utilities.Encryption.EncryptData(_newpassword);
                userName = drCollection[0][PasswordDS.PasswordUsernameField].ToString();
                //Log event
                //this.LogEvent(EventType.ChangePassword, "Password of user '" + drCollection[0][PasswordDS.PasswordUsernameField].ToString() + "' Updated");
            }
            else
            {
                DataRow drPassword = objDS.Tables[PasswordDS.PasswordDetailsTable].NewRow();

                drPassword[PasswordDS.PasswordUserIdField] = userId;
                drPassword[PasswordDS.PasswordPasswordField] = Utilities.Encryption.EncryptData(_newpassword);
                userName = drPassword[PasswordDS.PasswordUsernameField].ToString();
                objDS.Tables[PasswordDS.PasswordDetailsTable].Rows.Add(drPassword);

                //Log Event
                //this.LogEvent(EventType.ChangePassword, "Password of user '" + drPassword[PasswordDS.PasswordUsernameField].ToString() + "' Updated");
            }

            this.pwdSM.InsertCurrentPassword(userId, userName, _newpassword);

            objDS.AcceptChanges();
            this.Broker.SetWriteStart();
            this.OnSetData(objDS);
        }

        private void SetPassword(string _newpassword)
        {
            PasswordDS objDS = new PasswordDS();
            this.OnGetData(objDS);
            this.InitiateVariables();
            string _userid = this.CID.ToString();
            DataRow[] drCollection = objDS.Tables[PasswordDS.PasswordDetailsTable].Select(PasswordDS.PasswordUserIdField + " = '" + _userid + "'", String.Empty, DataViewRowState.CurrentRows);
            Guid userId = new Guid(_userid);
            string userName = String.Empty;
            if (drCollection.Length == 1)
            {
                drCollection[0][PasswordDS.PasswordUserIdField] = userId;
                drCollection[0][PasswordDS.PasswordPasswordField] = Utilities.Encryption.EncryptData(_newpassword);
                userName = drCollection[0][PasswordDS.PasswordUsernameField].ToString();
            }            
            this.pwdSM.InsertCurrentPassword(userId, userName, _newpassword);
            this.Broker.SetWriteStart();
            this.SetData(objDS);
        }
                
        private void DeleteUser(string _userid)
        {
            try
            {
                DBUserDetailsDS objData = new DBUserDetailsDS();
                objData.UserList = true;
                this.OnGetData(objData);
                DataTable objUsersList = objData.Tables[DBUserDetailsDS.USER_DETAIL_TABLE];
                DataRow[] objUser = objUsersList.Select(DBUserDetailsDS.USER_USERID_FIELD + " = '" + _userid + "'");
                objData.IsNew = false;
                if (objUser.Length > 0)
                {
                    objUser[0].Delete();
                    this.Broker.SetWriteStart();
                    this.OnSetData(objData);                    
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
            }
        }
    }
}