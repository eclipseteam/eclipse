﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Oritax.TaxSimp.Calculation;

namespace Oritax.TaxSimp.Security
{
    public class OrganisationsDetailsDS : DataSet
    {
        public const string ORGANISATIONAL_DETAILS_TABLE = "ORGANISATIONAL_DETAILS_TABLE";
        public const string ORGANISATIONAL_ID_FIELD = "ORGANISATIONAL_ID_FIELD";
        public const string ORGANISATIONAL_NAME_FIELD = "ORGANISATIONAL_NAME_FIELD";

        private bool isNew = false;
        private bool organisationList = false;

        public bool IsNew { get { return isNew; } set { isNew = value; } }
        public bool OrganisationList { get { return organisationList; } set { organisationList = value; } }

        public OrganisationsDetailsDS()
        {
            this.CreateOrganisationalDetailsTable();
        }

        public DataTable OrganisationalDetailsTable
        {
            get
            {
                return this.Tables[ORGANISATIONAL_DETAILS_TABLE];
            }
        }

        private void CreateOrganisationalDetailsTable()
        {
            DataTable organisationalDetailsTable = BuildOrganisationalDetailsTable();
            this.Tables.Add(organisationalDetailsTable);
            BrokerManagedComponentDS.AddVersionStamp(organisationalDetailsTable, typeof(OrganisationsDetailsDS));
        }

        public static DataTable BuildOrganisationalDetailsTable()
        {
            DataTable organisationalDetailsTable = new DataTable(ORGANISATIONAL_DETAILS_TABLE);

            organisationalDetailsTable.Columns.Add(ORGANISATIONAL_ID_FIELD, typeof(System.Guid));
            organisationalDetailsTable.Columns.Add(ORGANISATIONAL_NAME_FIELD, typeof(System.String));

            DataColumn[] dcPrimaryKey = { organisationalDetailsTable.Columns[ORGANISATIONAL_ID_FIELD] };
            organisationalDetailsTable.PrimaryKey = dcPrimaryKey;
            return organisationalDetailsTable;
        }
    }
}
