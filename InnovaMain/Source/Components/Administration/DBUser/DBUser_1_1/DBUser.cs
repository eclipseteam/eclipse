using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Text;
using System.Web;

using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.DSCalculation;
using Oritax.TaxSimp.Exceptions;
using Oritax.TaxSimp.SAS70Password;
using Oritax.TaxSimp.Services;
using Oritax.TaxSimp.TaxSimpLicence;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Data;
using System.ComponentModel;

namespace Oritax.TaxSimp.Security
{
    public enum UserType
    {
        [Description("Adviser")]
        Advisor = 0,
        [Description("Accountant")]
        Accountant = 1,
        [Description("IFA")]
        IFA = 2,
        [Description("DealerGroup")]
        DealerGroup = 3,
        [Description("Innova")]
        Innova = 4,
        [Description("Client")]
        Client = 5,
        [Description("Services")]
        Services = 6
    }

    public enum AdministratorType
    {
        NonAdministrator = 0,
        Organizational = 1,
        Global = 2
    }

    public enum OrganisationManagementErrors
    {
        None = 0,
        OrganisationNameAlreadyExists,
        OnlyOneOrganisationRemaining,
        OrganisationContainsEntities,
        UsersAssociatedWithOrganisation
    }

    /// <summary>
    /// Summary description for SystemModuleBase.
    /// </summary>
    [Serializable]
    public partial class DBUser : SystemModuleBase, IParty, ISerializable, IDBUser, IAuthenticable
    {
        #region INSTALLATION PROPERTIES

        // Assembly Installation Properties
        public const string ASSEMBLY_ID = "3D98A779-ADF0-4bf5-940D-220EE57ACBD9";
        public const string ASSEMBLY_NAME = "DBUser_1_1";
        public const string ASSEMBLY_DISPLAYNAME = "DBUser V1.1";
        public const string ASSEMBLY_MAJORVERSION = "1";
        public const string ASSEMBLY_MINORVERSION = "1";
        public const string ASSEMBLY_DATAFORMAT = "0";
        public const string ASSEMBLY_REVISION = "0";

        // Component Installation Properties
        public const string COMPONENT_ID = "601F2B9F-CE74-4939-BDA6-D713C70F3733";
        public const string COMPONENT_NAME = "DBUser";
        public const string COMPONENT_DISPLAYNAME = "DBUser";
        public const string COMPONENT_CATEGORY = "Security";

        // Component Version Installation Properties
        public const string COMPONENTVERSION_STARTDATE = "8/02/2004 3:01:36 PM";
        public const string COMPONENTVERSION_ENDDATE = "8/02/2004 3:01:36 PM";
        public const string COMPONENTVERSION_PERSISTERASSEMBLYSTRONGNAME = "DBUser, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";
        public const string COMPONENTVERSION_PERSISTERCLASS = "Oritax.TaxSimp.Security.UserPersister";
        public const string COMPONENTVERSION_IMPLEMENTATIONCLASS = "Oritax.TaxSimp.Security.DBUser";

        #endregion

        //USING CUSTOM SERIALIZATION.
        //Still mark the fields as [NonSerialized] for your own infomation but this is ignored now by .NET
        //You must add your own serialzation code for each instance
        //See GetObjectData & Special Constructor

        #region Fields

        public static readonly string sessionUserNameKey = "UserName";
        public static readonly string SessionUserPasswordKey = "UserPassword";
        public static readonly string sessionIsAdminKey = "IsAdmin";
        public static readonly string sessionUserCIDKey = "UserCID";

        [NonSerialized]
        DBUserDetailsDS dsUser = null;

        DBSecurityGroupDetailsDS dsSecurityGroup = null;
        PasswordDS dsPassword = null;
        DateTime m_LastLoggedIn;

        string firstName = string.Empty;
        string lastName = string.Empty;
        string emailAddress = string.Empty;

        int m_FailedAttempts = 0;
        int m_Locked = 0;
        bool administrator = false;

        bool displayZeroPreference = false;

        Guid organisationCID = Guid.Empty;
        string organisationName = string.Empty;

        AdministratorType administratorType = AdministratorType.NonAdministrator;
        UserType userType = UserType.Innova;

        Guid adviserCID = Guid.Empty;

        public Guid AdviserCID
        {
            get { return adviserCID; }
            set { adviserCID = value; }
        } 

        UserContext m_Context;

        #endregion // Fields

        public DBUser()
            : base()
        {
            dsUser = new DBUserDetailsDS();
            dsSecurityGroup = new DBSecurityGroupDetailsDS();
            dsPassword = new PasswordDS();
            m_Context = new UserContext();

        }

        protected DBUser(SerializationInfo si, StreamingContext context)
            : base(si, context)
        {
            firstName = Serialize.GetSerializedString(si, "dbu_firstName");
            lastName = Serialize.GetSerializedString(si, "dbu_lastName");
            emailAddress = Serialize.GetSerializedString(si, "dbu_emailAddress");
            dsSecurityGroup = (DBSecurityGroupDetailsDS)Serialize.GetSerializedValue(si, "dbu_dsSecurityGroup", typeof(DBSecurityGroupDetailsDS), null);
            dsPassword = (PasswordDS)Serialize.GetSerializedValue(si, "dbu_dsPassword", typeof(PasswordDS), null);
            m_LastLoggedIn = Serialize.GetSerializedDateTime(si, "dbu_m_LastLoggedIn");
            m_FailedAttempts = Serialize.GetSerializedInt32(si, "dbu_m_FailedAttempts");
            m_Locked = Serialize.GetSerializedInt32(si, "dbu_m_Locked");
            administrator = Serialize.GetSerializedBool(si, "dbu_administrator");
            m_Context = (UserContext)Serialize.GetSerializedValue(si, "dbu_m_Context", typeof(UserContext), new UserContext());
            displayZeroPreference = Serialize.GetSerializedBool(si, "dbu_displayZeroPreference");
            organisationCID = Serialize.GetSerializedGuid(si, "dbu_organisationCID");
            administratorType = (AdministratorType)Serialize.GetSerializedValue(si, "dbu_administratorType", typeof(AdministratorType), AdministratorType.NonAdministrator);
            userType = (UserType)Serialize.GetSerializedValue(si, "dbu_userType", typeof(UserType), UserType.Innova);
            organisationName = Serialize.GetSerializedString(si, "dbu_organisationName");
            adviserCID = Serialize.GetSerializedGuid(si, "dbu_adviserCID");
            
            dsUser = (DBUserDetailsDS)Serialize.GetSerializedValue(si, "dbu_dsUser", typeof(DBUserDetailsDS), new DBUserDetailsDS());

        }

        //Only allow the .NET Serialization core code to call this function
        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo si, StreamingContext context)
        {
            base.GetObjectData(si, context);
            Serialize.AddSerializedValue(si, "dbu_firstName", firstName);
            Serialize.AddSerializedValue(si, "dbu_lastName", lastName);
            Serialize.AddSerializedValue(si, "dbu_emailAddress", emailAddress);
            Serialize.AddSerializedValue(si, "dbu_dsSecurityGroup", dsSecurityGroup);
            Serialize.AddSerializedValue(si, "dbu_dsPassword", dsPassword);
            Serialize.AddSerializedValue(si, "dbu_m_LastLoggedIn", m_LastLoggedIn);
            Serialize.AddSerializedValue(si, "dbu_m_FailedAttempts", m_FailedAttempts);
            Serialize.AddSerializedValue(si, "dbu_m_Locked", m_Locked);
            Serialize.AddSerializedValue(si, "dbu_administrator", administrator);
            Serialize.AddSerializedValue(si, "dbu_m_Context", m_Context);
            Serialize.AddSerializedValue(si, "dbu_displayZeroPreference", displayZeroPreference);
            Serialize.AddSerializedValue(si, "dbu_organisationCID", organisationCID);
            Serialize.AddSerializedValue(si, "dbu_administratorType", administratorType);
            Serialize.AddSerializedValue(si, "dbu_userType", userType);
            Serialize.AddSerializedValue(si, "dbu_organisationName", organisationName);
            Serialize.AddSerializedValue(si, "dbu_adviserCID", adviserCID);
            Serialize.AddSerializedValue(si, "dbu_dsUser", dsUser);
        }

        public override void Initialize(String name)
        {
            base.Initialize(name);

            if (this.CID == Guid.Empty)
                this.CID = Guid.NewGuid();
        }

        public int FailedAttempts
        {
            get { return m_FailedAttempts; }
            set { m_FailedAttempts = value; }
        }

        public int Locked
        {
            get { return m_Locked; }
            set { m_Locked = value; }
        }

        public DateTime LastLoggedIn
        {
            get { return m_LastLoggedIn; }
            set { m_LastLoggedIn = value; }
        }

        public UserContext Context
        {
            get { return m_Context; }
            set { m_Context = value; }
        }

        public bool Administrator
        {
            get { return this.administrator; }
            set { administrator = value; }
        }

        public bool DisplayZeroPreference
        {
            get { return this.displayZeroPreference; }
            set { displayZeroPreference = value; }
        }

        public Guid OrganisationCID
        {
            get { return organisationCID; }
            set { organisationCID = value; }
        }

        public string OrganisationName
        {
            get { return organisationName; }
            set { organisationName = value; }
        }

        public AdministratorType AdministratorType
        {
            get { return administratorType; }
            set { administratorType = value; }
        }

        public UserType UserType
        {
            get { return userType; }
            set { userType = value; }
        }

        public string Email
        {
            get { return emailAddress; }
            set { emailAddress = value; }
        }
        public Guid PrivilegeTemplateID
        {
            get { return (Guid)dsUser.Tables[DBUserDetailsDS.USER_DETAIL_TABLE].Rows[0][DBUserDetailsDS.USER_PRIVTEMPLATECID_FIELD]; }
        }

        public override DataSet PrimaryDataSet { get { return new DBUserDS(); } }

        #region OnGetData methods
        protected void OnGetData(AuthenticateUserDS data)
        {
            String strUsername, strPassword;
            DBUser objUser = null;
            SAS70PasswordSM pwdSM = null;
            DBUserDetailsDS dsUserAuthenticate;
            int iAuthenticated;
            int iFailedAttempts = 0;
            #region AUTHENTICATEUSERDS
            if (data.Tables.Contains(AuthenticateUserDS.AUTHENTICATION_DETAIL_TABLE))
            {
                DataRow drUser = data.Tables[AuthenticateUserDS.AUTHENTICATION_DETAIL_TABLE].Rows[0];

                strUsername = drUser[AuthenticateUserDS.AUTHENTICATION_USERNAME_FIELD].ToString();
                strPassword = drUser[AuthenticateUserDS.AUTHENTICATION_PASSWORD_FIELD].ToString();

                if (this.Broker != null)
                {
                    data.Tables[AuthenticateUserDS.AUTHENTICATION_DETAIL_TABLE].Rows.Clear();

                    drUser[AuthenticateUserDS.AUTHENTICATION_USERNAME_FIELD] = strUsername;
                    drUser[AuthenticateUserDS.AUTHENTICATION_PASSWORD_FIELD] = strPassword;

                    // GetBMCInstance : attempts to construct a User object based on Username ( see table: CINSTANCES )
                    objUser = (DBUser)Broker.GetBMCInstance(strUsername, "DBUser_1_1");
                    pwdSM = (SAS70PasswordSM)Broker.GetWellKnownBMC(WellKnownCM.SecurityProvider);
                    if (objUser != null && pwdSM != null)
                    {
                        // Prepare Failed Attempts & Locked information.
                        if (dsUser.Tables.Contains(DBUserDetailsDS.USER_DETAIL_TABLE))
                            if (dsUser.Tables[DBUserDetailsDS.USER_DETAIL_TABLE].Rows.Count > 0)
                            {
                                DataRow drUserDetail = dsUser.Tables[DBUserDetailsDS.USER_DETAIL_TABLE].Rows[0];
                                this.FailedAttempts = Convert.ToInt32(drUserDetail[DBUserDetailsDS.USER_FAILEDATTEMPTS_FIELD].ToString());
                                this.Locked = Convert.ToInt32(drUserDetail[DBUserDetailsDS.USER_LOCKED_FIELD].ToString());

                                if (drUserDetail[DBUserDetailsDS.USER_LASTLOGGEDIN_FIELD] != DBNull.Value)
                                    this.LastLoggedIn = (DateTime)drUserDetail[DBUserDetailsDS.USER_LASTLOGGEDIN_FIELD];
                            }

                        // Since a DBUser object was located in the CINSTANCES table perform an extract data
                        // using User Detail DataSet so that the password may be checked.
                        dsUserAuthenticate = new DBUserDetailsDS();
                        objUser.OnGetData(dsUserAuthenticate);

                        if (dsUserAuthenticate.Tables[DBUserDetailsDS.USER_DETAIL_TABLE].Rows.Count > 0)
                        {
                            DataRow drUserRow = dsUserAuthenticate.Tables[DBUserDetailsDS.USER_DETAIL_TABLE].Rows[0];

                            // Check user is active
                            if (Convert.ToInt32(drUserRow[DBUserDetailsDS.USER_ACTIVE_FIELD].ToString()) != 1)
                                iAuthenticated = AuthenticateUserDS.AUTHENTICATE_DISABLED;
                            else
                            {
                                if (Convert.ToInt32(drUserRow[DBUserDetailsDS.USER_LOCKED_FIELD].ToString()) == 1)
                                    iAuthenticated = AuthenticateUserDS.AUTHENTICATE_LOCKED;
                                else
                                {
                                    // Check the entered password
                                    if (Utilities.Encryption.DecryptData(drUserRow[DBUserDetailsDS.USER_PASSWORD_FIELD].ToString()) == strPassword)
                                        iAuthenticated = AuthenticateUserDS.AUTHENTICATE_SUCCESS;
                                    else
                                        iAuthenticated = AuthenticateUserDS.AUTHENTICATE_BADPASSWORD;

                                    // If this is an authenticated user, populate the Security Groups dataset.
                                    if (iAuthenticated == AuthenticateUserDS.AUTHENTICATE_SUCCESS)
                                    {
                                        DBUserDetailsDS objSuccessUserDS = new DBUserDetailsDS();
                                        objUser.OnGetData(objSuccessUserDS);

                                        if (objSuccessUserDS.Tables[DBUserDetailsDS.USER_DETAIL_TABLE].Rows.Count > 0)
                                        {
                                            DataRow drSuccessUser = objSuccessUserDS.Tables[DBUserDetailsDS.USER_DETAIL_TABLE].Rows[0];
                                            drSuccessUser[DBUserDetailsDS.USER_LASTLOGGEDIN_FIELD] = DateTime.Now;
                                            drSuccessUser[DBUserDetailsDS.USER_FAILEDATTEMPTS_FIELD] = 0;
                                        }
                                        objUser.SetData(objSuccessUserDS);
                                    }
                                    else
                                    {
                                        iFailedAttempts = Convert.ToInt32(drUserRow[DBUserDetailsDS.USER_FAILEDATTEMPTS_FIELD].ToString());
                                        iFailedAttempts++;
                                        if (iFailedAttempts >= pwdSM.MaxFailureAttempts)
                                        {
                                            this.Locked = 1;
                                        }
                                        else
                                            this.FailedAttempts = iFailedAttempts;

                                        this.Modified = true;
                                    }
                                }
                            }
                        }
                        else
                            iAuthenticated = AuthenticateUserDS.AUTHENTICATE_BADUSER;
                    }
                    else
                        iAuthenticated = AuthenticateUserDS.AUTHENTICATE_BADUSER;

                    drUser[AuthenticateUserDS.AUTHENTICATION_AUTHENTICATE_FIELD] = iAuthenticated;
                    drUser[AuthenticateUserDS.AUTHENTICATION_FAILEDATTEMPTS_FIELD] = iFailedAttempts;
                    data.Tables[AuthenticateUserDS.AUTHENTICATION_DETAIL_TABLE].Rows.Add(drUser);
                }
            }

            this.Broker.ReleaseBrokerManagedComponent(objUser);
            this.Broker.ReleaseBrokerManagedComponent(pwdSM);
            #endregion AUTHENTICATEUSERDS
        }
        protected void OnGetData(DBUserDetailsDS data)
        {
            #region DBUSERDS
            DataTable messageTable;

            if (data.UserList)
            {
                if (data.Tables.Contains(DBUserDetailsDS.USER_DETAIL_TABLE))
                    data.Tables.Remove(DBUserDetailsDS.USER_DETAIL_TABLE);

                DBUserDetailsDS dataList = (DBUserDetailsDS)Broker.GetBMCDetailList("DBUser_1_1");

                data.Tables.Add(dataList.Tables[DBUserDetailsDS.USER_DETAIL_TABLE].Copy());
                DataTable organisationalDetailsTable = dataList.OrganisationalDetailsTable;
                this.PopulateOrganisationalDetailsTable(organisationalDetailsTable);
                foreach (DataRow dataRow in data.Tables[DBUserDetailsDS.USER_DETAIL_TABLE].Rows)
                {
                    dataRow[DBUserDetailsDS.USER_PASSWORD_FIELD] = Encryption.DecryptData(dataRow[DBUserDetailsDS.USER_PASSWORD_FIELD].ToString());
                    Guid organisationId = (Guid)(dataRow[DBUserDetailsDS.USER_ORGANISATION_CID_FIELD]);
                    string selectExpression = string.Format("{0} = '{1}'", OrganisationsDetailsDS.ORGANISATIONAL_ID_FIELD,
                        organisationId);
                    DataRow[] organisationRows = organisationalDetailsTable.Select(selectExpression);
                    if (organisationRows.Length > 0)
                    {
                        string organisationName = (String)(organisationRows[0][OrganisationsDetailsDS.ORGANISATIONAL_NAME_FIELD]);
                        dataRow[DBUserDetailsDS.USER_ORGANISATION_NAME_FIELD] = organisationName;
                    }
                    else
                    {
                        dataRow[DBUserDetailsDS.USER_ORGANISATION_NAME_FIELD] = string.Empty;
                    }

                    if ((UserType)Int32.Parse(dataRow[DBUserDetailsDS.USER_USER_TYPE_FIELD].ToString()) == Security.UserType.Advisor)
                    {
                        DBUser userObj = Broker.GetBMCInstance((Guid)dataRow[DBUserDetailsDS.USER_USERID_FIELD]) as DBUser;

                        dataRow[DBUserDetailsDS.ADVISERCID] = userObj.adviserCID;
                        dataRow[DBUserDetailsDS.IFACID] = Guid.Empty;
                        dataRow[DBUserDetailsDS.IFANAME] = String.Empty;
                        dataRow[DBUserDetailsDS.ADVISERID] = String.Empty;

                        if (userObj.adviserCID != Guid.Empty)
                        {
                            IOrganizationUnit adviserBMC = Broker.GetBMCInstance(userObj.adviserCID) as IOrganizationUnit;
                            if (adviserBMC != null)
                            {
                                dataRow[DBUserDetailsDS.ADVISERID] = adviserBMC.ClientId;

                                if (((ICmHasParent)adviserBMC).Parent.Clid != Guid.Empty && ((ICmHasParent)adviserBMC).Parent.Csid != Guid.Empty)
                                {
                                    ICalculationModule ifaCM = Broker.GetCMImplementation(((ICmHasParent)adviserBMC).Parent.Clid, ((ICmHasParent)adviserBMC).Parent.Csid);
                                    if (ifaCM != null)
                                    {
                                        dataRow[DBUserDetailsDS.IFACID] = ifaCM.CID;
                                        dataRow[DBUserDetailsDS.IFANAME] = ifaCM.Name;
                                    }
                                }
                            }
                        }
                    }
                }
                data.AcceptChanges();
            }
            else
            {
                if (data.Tables.Contains(DBUserDetailsDS.USER_DETAIL_TABLE))
                    data.Tables.Remove(DBUserDetailsDS.USER_DETAIL_TABLE);

                if (dsUser.Tables[DBUserDetailsDS.USER_DETAIL_TABLE].Rows.Count > 0)
                {
                    DataRow drUser = dsUser.Tables[DBUserDetailsDS.USER_DETAIL_TABLE].Rows[0];
                    drUser[DBUserDetailsDS.USER_USERID_FIELD] = this.CID;
                    drUser[DBUserDetailsDS.USER_FIRSTNAME_FIELD] = this.firstName;
                    drUser[DBUserDetailsDS.USER_LASTNAME_FIELD] = this.lastName;
                    drUser[DBUserDetailsDS.USER_EMAILADDRESS_FIELD] = this.emailAddress;
                    drUser[DBUserDetailsDS.USER_FAILEDATTEMPTS_FIELD] = this.FailedAttempts;
                    drUser[DBUserDetailsDS.USER_LOCKED_FIELD] = this.Locked;
                    drUser[DBUserDetailsDS.USER_ADMINISTRATOR_FIELD] = (this.Administrator ? 1 : 0);

                    drUser[DBUserDetailsDS.USER_REPORTINGUNITNAME_FIELD] = this.Context.ReportingUnitName;
                    drUser[DBUserDetailsDS.USER_REPORTINGUNITCLID_FIELD] = this.Context.ReportingUnitCLID;

                    drUser[DBUserDetailsDS.USER_SCENARIONAME_FIELD] = this.Context.ScenarioName;
                    drUser[DBUserDetailsDS.USER_SCENARIOCSID_FIELD] = this.Context.ScenarioCSID;

                    drUser[DBUserDetailsDS.USER_PERIODNAME_FIELD] = this.Context.PeriodName;
                    drUser[DBUserDetailsDS.USER_PERIODCLID_FIELD] = this.Context.PeriodCLID;

                    drUser[DBUserDetailsDS.USER_DISPLAY_ZERO_PREFERENCE_FIELD] = this.displayZeroPreference;
                    drUser[DBUserDetailsDS.USER_ORGANISATION_CID_FIELD] = this.organisationCID;
                    drUser[DBUserDetailsDS.USER_ADMINISTRATOR_TYPE_FIELD] = (int)this.administratorType;
                    drUser[DBUserDetailsDS.USER_USER_TYPE_FIELD] = (int)this.userType;

                    drUser[DBUserDetailsDS.ADVISERCID] = this.adviserCID;
                    drUser[DBUserDetailsDS.IFACID] = Guid.Empty;
                    drUser[DBUserDetailsDS.IFANAME] = String.Empty;
                    drUser[DBUserDetailsDS.ADVISERID] = String.Empty;

                    if (this.adviserCID != Guid.Empty)
                    {

                        IOrganizationUnit adviserBMC = Broker.GetBMCInstance(adviserCID) as IOrganizationUnit;
                        drUser[DBUserDetailsDS.ADVISERID] = adviserBMC.ClientId;

                        if (((ICmHasParent)adviserBMC).Parent.Clid != Guid.Empty && ((ICmHasParent)adviserBMC).Parent.Csid != Guid.Empty)
                        {
                            ICalculationModule ifaCM = Broker.GetCMImplementation(((ICmHasParent)adviserBMC).Parent.Clid, ((ICmHasParent)adviserBMC).Parent.Csid);
                            drUser[DBUserDetailsDS.IFACID] = ifaCM.CID;
                            drUser[DBUserDetailsDS.IFANAME] = ifaCM.Name;
                        }
                    }

                    drUser[DBUserDetailsDS.USER_ORGANISATION_NAME_FIELD] = this.organisationName;

                    FromUserMessageCollectionSpecifier fromUserMessageCollectionSpecifier = new FromUserMessageCollectionSpecifier(this.CID);
                    messageTable = Broker.ListBMCCollection(fromUserMessageCollectionSpecifier);

                    if (data.Tables.Contains(DBUserDetailsDS.MESSAGE_TABLE))
                        data.Tables.Remove(DBUserDetailsDS.MESSAGE_TABLE);

                    data.Tables.Add(messageTable.Copy());
                }

                data.Tables.Add(dsUser.Tables[DBUserDetailsDS.USER_DETAIL_TABLE].Copy());
            }

            this.PopulateOrganisationalDetailsTable(data.OrganisationalDetailsTable);

            #endregion DBUSERDS
        }
        protected void OnGetData(DBSecurityGroupDetailsDS data)
        {
            #region DBSecurityGroupDetailsDS
            if (data.Tables.Contains(DBSecurityGroupDetailsDS.SECGRP_DETAIL_TABLE))
                data.Tables.Remove(DBSecurityGroupDetailsDS.SECGRP_DETAIL_TABLE);
            data.Tables.Add(dsSecurityGroup.Tables[DBSecurityGroupDetailsDS.SECGRP_DETAIL_TABLE].Copy());
            #endregion DBSecurityGroupDetailsDS
        }
        protected void OnGetData(PasswordDS data)
        {
            #region PasswordDS
            PasswordDS objPasswordDS = new PasswordDS();

            if (data.Tables.Contains(PasswordDS.PasswordDetailsTable))
                data.Tables.Remove(PasswordDS.PasswordDetailsTable);

            if (dsUser.Tables[DBUserDetailsDS.USER_DETAIL_TABLE].Rows.Count > 0)
            {
                DataRow drPassword = objPasswordDS.Tables[PasswordDS.PasswordDetailsTable].NewRow();
                DataRow drUser = dsUser.Tables[DBUserDetailsDS.USER_DETAIL_TABLE].Rows[0];
                drPassword[PasswordDS.PasswordUserIdField] = drUser[DBUserDetailsDS.USER_USERID_FIELD];
                drPassword[PasswordDS.PasswordUsernameField] = drUser[DBUserDetailsDS.USER_USERNAME_FIELD];
                drPassword[PasswordDS.PasswordPasswordField] = drUser[DBUserDetailsDS.USER_PASSWORD_FIELD];
                objPasswordDS.Tables[PasswordDS.PasswordDetailsTable].Rows.Add(drPassword);

                data.Tables.Add(objPasswordDS.Tables[PasswordDS.PasswordDetailsTable].Copy());
            }
            #endregion PasswordDS
        }

        protected void ExtractOrganisationsDetails(OrganisationsDetailsDS organisationalDetailsDS)
        {
            DataTable organisationalDetailsTable = organisationalDetailsDS.OrganisationalDetailsTable;
            PopulateOrganisationalDetailsTable(organisationalDetailsTable);
        }

        private void PopulateOrganisationalDetailsTable(DataTable organisationalDetailsTable)
        {
            foreach (object row in this.Broker.GetBMCList("Organization"))
            {
                DataRow bmcDataRow = (DataRow)row;

                IOrganization organisation = (IOrganization)this.Broker.GetBMCInstance((Guid)bmcDataRow[DSCalculationModulesInfo.CMID_FIELD]);
                DataRow dataRow = organisationalDetailsTable.NewRow();
                dataRow[OrganisationsDetailsDS.ORGANISATIONAL_ID_FIELD] = organisation.CID;
                dataRow[OrganisationsDetailsDS.ORGANISATIONAL_NAME_FIELD] = organisation.OrganisationName;
                this.Broker.ReleaseBrokerManagedComponent(organisation);
                organisationalDetailsTable.Rows.Add(dataRow);
            }
        }

        private void ExtractOrganisationDetails(OrganisationDetailsDS organisationDetailsDS)
        {
            IOrganization organisation = (IOrganization)this.Broker.GetBMCInstance(organisationDetailsDS.OrganisationId);
            organisationDetailsDS.OrganisationName = organisation.OrganisationName;
            this.Broker.ReleaseBrokerManagedComponent(organisation);
        }

        protected override void OnGetData(DataSet data)
        {
            base.OnGetData(data);
            if (data is DBUserDS)
            {
                base.OnGetData((SystemModuleBaseDS)data);
                DBUserDS userData = data as DBUserDS;
                if (userData.Tables.Contains(DBUserDetailsDS.USER_DETAIL_TABLE))
                    userData.Tables.Remove(DBUserDetailsDS.USER_DETAIL_TABLE);
                userData.Tables.Add(dsUser.Tables[DBUserDetailsDS.USER_DETAIL_TABLE].Copy());

                if (userData.Tables.Contains(DBSecurityGroupDetailsDS.SECGRP_DETAIL_TABLE))
                    userData.Tables.Remove(DBSecurityGroupDetailsDS.SECGRP_DETAIL_TABLE);
                userData.Tables.Add(dsSecurityGroup.Tables[DBSecurityGroupDetailsDS.SECGRP_DETAIL_TABLE].Copy());

                if (userData.Tables.Contains(PasswordDS.PasswordDetailsTable))
                    userData.Tables.Remove(PasswordDS.PasswordDetailsTable);
                userData.Tables.Add(dsPassword.Tables[PasswordDS.PasswordDetailsTable].Copy());

                DBUserDS primaryDS = (DBUserDS)userData;
                DataRow row = primaryDS.Tables[DBUserDS.DBUSER_TABLE].NewRow();

                row[DBUserDS.DBUSERLASTLOGGED_FIELD] = this.m_LastLoggedIn;
                row[DBUserDS.DBUSERFAILEDATTEMPTS_FIELD] = this.m_FailedAttempts;
                row[DBUserDS.DBUSERLOCKED_FIELD] = this.m_Locked;
                primaryDS.Tables[DBUserDS.DBUSER_TABLE].Rows.Add(row);
            }
            else if (data is AuthenticateUserDS)
            {
                this.OnGetData((AuthenticateUserDS)data);
            }
            else if (data is DBUserDetailsDS)
            {
                this.OnGetData((DBUserDetailsDS)data);
            }
            else if (data is DBSecurityGroupDetailsDS)
            {
                this.OnGetData((DBSecurityGroupDetailsDS)data);
            }
            else if (data is PasswordDS)
            {
                this.OnGetData((PasswordDS)data);
            }
            else if (data is FavouritesDS)
            {
                DataSet favouritesDS = (FavouritesDS)data;
                this.ExtractFavouriteWorkpapers(favouritesDS);
            }
            else if (data is OrganisationsDetailsDS)
            {
                this.ExtractOrganisationsDetails((OrganisationsDetailsDS)data);
            }
            else if (data is OrganisationDetailsDS)
            {
                this.ExtractOrganisationDetails((OrganisationDetailsDS)data);
            }
        }

        #endregion

        #region OnSetData methods

        protected override ModifiedState OnSetData(DataSet data)
        {
            base.OnSetData(data);

            if (data is DBUserDetailsDS)
            {
                return OnSetData((DBUserDetailsDS)data);
            }
            else if (data is OrganisationsDetailsDS)
            {
                return DeliverOrganisationsData((OrganisationsDetailsDS)data);
            }
            else if (data is DBSecurityGroupDetailsDS)
            {
                return OnSetData((DBSecurityGroupDetailsDS)data);
            }
            else if (data is PasswordDS)
            {
                return OnSetData((PasswordDS)data);
            }
            else if (data is DBUserDS)
            {
                return OnSetData((DBUserDS)data);
            }
            else if (data is OrganisationDetailsDS)
            {
                return DeliverOrganisationDetails((OrganisationDetailsDS)data);
            }
            return ModifiedState.UNKNOWN;
        }

        protected ModifiedState OnSetData(DBUserDetailsDS objData)
        {
            //If this is a postback from teh users list page
            //then delete any members marked for deletion.
            if (objData.UserList)
            {
                foreach (DataRow dRow in objData.Tables[DBUserDetailsDS.USER_DETAIL_TABLE].Rows)
                {
                    if (dRow.RowState == DataRowState.Deleted)
                    {
                        dRow.RejectChanges();
                        UpdatePartyForLogicalModules(dRow);

                        UserAuthentication.LogUserOut((dRow[DBUserDetailsDS.USER_USERNAME_FIELD].ToString()));
                        Broker.DeleteCMInstance((Guid)dRow[DBUserDetailsDS.USER_USERID_FIELD]);
                    }
                }
            }
            else if (objData.MessageInsert)
            {
               foreach(DataRow messageRow in objData.Tables[DBUserDetailsDS.MESSAGE_TABLE].Rows)
               {
                    
               }
            }
            else
            {
                if (dsUser.Tables.Contains(DBUserDetailsDS.USER_DETAIL_TABLE))
                    dsUser.Tables.Remove(DBUserDetailsDS.USER_DETAIL_TABLE);
                dsUser.Tables.Add(objData.Tables[DBUserDetailsDS.USER_DETAIL_TABLE].Copy());

                if (dsUser.Tables[DBUserDetailsDS.USER_DETAIL_TABLE].Rows.Count > 0)
                {
                    //Verify that if this is a new user then check that a user of same id
                    //does not already exist
                    if (objData.IsNew)
                    {
                        ILicence licence = (ILicence)this.Broker.GetWellKnownBMC(WellKnownCM.Licencing);

                        try
                        {
                            if (licence.IsLicenced(this.typeID, LicenceValidationType.User))
                            {
                                DataRow drUser = dsUser.Tables[DBUserDetailsDS.USER_DETAIL_TABLE].Rows[0];

                                DBUser objUser = (DBUser)Broker.GetBMCInstance(drUser[DBUserDetailsDS.USER_USERNAME_FIELD].ToString(), DBUserInstall.ASSEMBLY_NAME);

                                try
                                {
                                    if (objUser != null)
                                        throw new DisplayUserMessageException(new MessageBoxDefinition(MessageBoxDefinition.MSG_LOGIN_NAME_EXISTS), false, MessageDisplayMethod.MainWindowError);
                                }
                                finally
                                {
                                    this.Broker.ReleaseBrokerManagedComponent(objUser);
                                }

                                SAS70PasswordSM pwdSM = (SAS70PasswordSM)this.Broker.GetWellKnownBMC(WellKnownCM.SecurityProvider);
                                pwdSM.InsertCurrentPassword(this.CID, drUser[DBUserDetailsDS.USER_USERNAME_FIELD].ToString(), drUser[DBUserDetailsDS.USER_PASSWORD_FIELD].ToString());
                                this.Broker.ReleaseBrokerManagedComponent(pwdSM);
                            }
                        }
                        finally
                        {
                            this.Broker.ReleaseBrokerManagedComponent(licence);
                        }
                    }
                    setLocalVariables();
                }
            }

            return ModifiedState.UNKNOWN;
        }

        private void UpdatePartyForLogicalModules(DataRow dRow)
        {
            IOrganization orgCM = (IOrganization)this.Broker.GetWellKnownCM(WellKnownCM.Organization);
            ILogicalModule orgLM = this.Broker.GetLogicalCM(orgCM.CLID);

            this.Broker.ReleaseBrokerManagedComponent(orgCM);

            foreach (Guid childCLID in orgLM.ChildCLIDs)
            {
                ILogicalModule childLM = this.Broker.GetLogicalCM(childCLID);
                PartyDS partyDS = new PartyDS();
                childLM.GetData(partyDS);
                partyDS.AcceptChanges();
                DataRow[] drCollection = partyDS.Tables[PartyDS.INCLUDEDPARTY_TABLE].Select(PartyDS.PARTYCID_FIELD + " = '" + dRow[DBUserDetailsDS.USER_USERID_FIELD].ToString() + "'");

                if (drCollection.Length > 0)
                {
                    drCollection[0].Delete();
                    childLM.SetData(partyDS);
                }

                this.Broker.ReleaseBrokerManagedComponent(childLM);
            }

            this.Broker.ReleaseBrokerManagedComponent(orgLM);
        }

        protected ModifiedState OnSetData(DBSecurityGroupDetailsDS objData)
        {
            if (dsSecurityGroup.Tables.Contains(DBSecurityGroupDetailsDS.SECGRP_DETAIL_TABLE))
                dsSecurityGroup.Tables.Remove(DBSecurityGroupDetailsDS.SECGRP_DETAIL_TABLE);
            dsSecurityGroup.Tables.Add(objData.Tables[DBSecurityGroupDetailsDS.SECGRP_DETAIL_TABLE].Copy());

            return ModifiedState.UNKNOWN;
        }

        protected ModifiedState OnSetData(PasswordDS objData)
        {
            if (dsUser.Tables[DBUserDetailsDS.USER_DETAIL_TABLE].Rows.Count > 0)
            {
                DataRow drPassword = objData.Tables[PasswordDS.PasswordDetailsTable].Rows[0];
                DataRow drUser = dsUser.Tables[DBUserDetailsDS.USER_DETAIL_TABLE].Rows[0];
                drUser[DBUserDetailsDS.USER_PASSWORD_FIELD] = drPassword[PasswordDS.PasswordPasswordField];

                SAS70PasswordSM pwdSM = (SAS70PasswordSM)this.Broker.GetWellKnownBMC(WellKnownCM.SecurityProvider);
                pwdSM.InsertCurrentPassword(this.CID, drUser[DBUserDetailsDS.USER_USERNAME_FIELD].ToString(), drUser[DBUserDetailsDS.USER_PASSWORD_FIELD].ToString());
                this.Broker.ReleaseBrokerManagedComponent(pwdSM);
            }

            this.Modified = true;
            return ModifiedState.MODIFIED;
        }

        protected ModifiedState OnSetData(DBUserDS objData)
        {
            if (dsUser.Tables.Contains(DBUserDetailsDS.USER_DETAIL_TABLE))
                dsUser.Tables.Remove(DBUserDetailsDS.USER_DETAIL_TABLE);
            dsUser.Tables.Add(objData.Tables[DBUserDetailsDS.USER_DETAIL_TABLE].Copy());

            if (dsUser.Tables[DBUserDetailsDS.USER_DETAIL_TABLE].Rows.Count > 0)
            {
                setLocalVariables();
            }

            if (dsSecurityGroup.Tables.Contains(DBSecurityGroupDetailsDS.SECGRP_DETAIL_TABLE))
                dsSecurityGroup.Tables.Remove(DBSecurityGroupDetailsDS.SECGRP_DETAIL_TABLE);
            dsSecurityGroup.Tables.Add(objData.Tables[DBSecurityGroupDetailsDS.SECGRP_DETAIL_TABLE].Copy());

            if (dsPassword.Tables.Contains(PasswordDS.PasswordDetailsTable))
                dsPassword.Tables.Remove(PasswordDS.PasswordDetailsTable);
            dsPassword.Tables.Add(objData.Tables[PasswordDS.PasswordDetailsTable].Copy());

            if (objData.Tables[DBUserDS.DBUSER_TABLE].Rows.Count == 1)
            {
                DataRow row = objData.Tables[DBUserDS.DBUSER_TABLE].Rows[0];

                this.m_LastLoggedIn = (DateTime)row[DBUserDS.DBUSERLASTLOGGED_FIELD];
                this.m_FailedAttempts = (int)row[DBUserDS.DBUSERFAILEDATTEMPTS_FIELD];
                this.m_Locked = (int)row[DBUserDS.DBUSERLOCKED_FIELD];
            }

            return ModifiedState.MODIFIED;
        }

        private ModifiedState DeliverOrganisationsData(OrganisationsDetailsDS organisationalDetailsDS)
        {
            OrganisationManagementErrors organisationManagementErrors = OrganisationManagementErrors.None;
            if (organisationalDetailsDS.IsNew)
            {
                organisationManagementErrors = AddOrganisation(organisationalDetailsDS);
            }
            else if (organisationalDetailsDS.OrganisationList)
            {
                organisationManagementErrors = RemoveOrganisations(organisationalDetailsDS);
            }

            this.InformUser(organisationManagementErrors);

            return ModifiedState.UNKNOWN;
        }

        private ModifiedState DeliverOrganisationDetails(OrganisationDetailsDS organisationDetailsDS)
        {
            OrganisationManagementErrors organisationManagementErrors = OrganisationManagementErrors.None;
            if (this.Broker.OrganisationNameAlreadyExists(organisationDetailsDS.OrganisationName))
                organisationManagementErrors = OrganisationManagementErrors.OrganisationNameAlreadyExists;
            if (organisationManagementErrors == OrganisationManagementErrors.None)
            {
                IOrganization organisation = (IOrganization)this.Broker.GetBMCInstance(organisationDetailsDS.OrganisationId);
                organisation.SetData(organisationDetailsDS);
                this.Broker.ReleaseBrokerManagedComponent(organisation);
            }

            this.InformUser(organisationManagementErrors);

            return ModifiedState.UNKNOWN;
        }

        private void InformUser(OrganisationManagementErrors organisationManagementErrors)
        {
            if (organisationManagementErrors == OrganisationManagementErrors.None)
                return;
            int errorMessage = 0; ;
            switch (organisationManagementErrors)
            {
                case OrganisationManagementErrors.OnlyOneOrganisationRemaining:
                    errorMessage = MessageBoxDefinition.MSG_ONLY_ONE_ORGANISATION_REMAINING;
                    break;
                case OrganisationManagementErrors.OrganisationContainsEntities:
                    errorMessage = MessageBoxDefinition.MSG_ORGANISATION_CONTAINS_ENTITIES;
                    break;
                case OrganisationManagementErrors.OrganisationNameAlreadyExists:
                    errorMessage = MessageBoxDefinition.MSG_ORGANISATION_NAME_ALREADY_EXISTS;
                    break;
                case OrganisationManagementErrors.UsersAssociatedWithOrganisation:
                    errorMessage = MessageBoxDefinition.MSG_USERS_ASSOCIATED_WITH_ORGANISATION;
                    break;
                default:
                    break;
            }
            if (errorMessage == 0)
                return;
            MessageBoxDefinition messageBoxDefinition = new MessageBoxDefinition(errorMessage);
            throw new DisplayUserMessageException(messageBoxDefinition, false, MessageDisplayMethod.MainWindowPopup);
        }

        private OrganisationManagementErrors AddOrganisation(OrganisationsDetailsDS organisationalDetailsDS)
        {
            DataTable organisationalDetailsTable = organisationalDetailsDS.Tables[OrganisationsDetailsDS.ORGANISATIONAL_DETAILS_TABLE];
            if (organisationalDetailsTable.Rows.Count == 0)
                return OrganisationManagementErrors.None;
            DataRow dataRow = organisationalDetailsDS.Tables[OrganisationsDetailsDS.ORGANISATIONAL_DETAILS_TABLE].Rows[0];
            Guid organisationId = new Guid(dataRow[OrganisationsDetailsDS.ORGANISATIONAL_ID_FIELD].ToString());
            string organisationName = dataRow[OrganisationsDetailsDS.ORGANISATIONAL_NAME_FIELD].ToString();

            bool organisationAlreadyExists = this.Broker.OrganisationNameAlreadyExists(organisationName);

            if (organisationAlreadyExists)
                return OrganisationManagementErrors.OrganisationNameAlreadyExists;

            Guid csid = Guid.NewGuid();
            ILogicalModule lmOrganisation = this.Broker.CreateLogicalCM("Organization_1_1", "Organization", csid, null);
            Guid lmCid = lmOrganisation.CID;
            Guid clid = lmOrganisation.CLID;
            this.Broker.ReleaseBrokerManagedComponent(lmOrganisation);
            Guid cid = this.Broker.GetCMInstanceID(lmCid, csid);
            IOrganization organisationCM = (IOrganization)this.Broker.GetBMCInstance(cid);
            organisationCM.OrganisationName = organisationName;
            this.Broker.ReleaseBrokerManagedComponent(organisationCM);

            return OrganisationManagementErrors.None;
        }

        private OrganisationManagementErrors RemoveOrganisations(OrganisationsDetailsDS organisationalDetailsDS)
        {
            foreach (DataRow dataRow in organisationalDetailsDS.Tables[OrganisationsDetailsDS.ORGANISATIONAL_DETAILS_TABLE].Rows)
            {
                OrganisationManagementErrors organisationManagementErrors = RemoveOrganisation(dataRow);
                if (organisationManagementErrors != OrganisationManagementErrors.None)
                    return organisationManagementErrors;

            }
            return OrganisationManagementErrors.None;
        }

        private OrganisationManagementErrors RemoveOrganisation(DataRow dataRow)
        {
            if (dataRow.RowState != DataRowState.Deleted)
                return OrganisationManagementErrors.None;

            dataRow.RejectChanges();

            int organisationsCount = 0;
            foreach (object row in this.Broker.GetBMCList("Organization"))
            {
                organisationsCount++;
            }
            if (organisationsCount < 2)
                return OrganisationManagementErrors.OnlyOneOrganisationRemaining;

            Guid organisationId = new Guid(dataRow[OrganisationsDetailsDS.ORGANISATIONAL_ID_FIELD].ToString());

            bool usersAssociatedWithOrganisation = UsersAssociatedWithOrganisation(organisationId);

            if (usersAssociatedWithOrganisation)
                return OrganisationManagementErrors.UsersAssociatedWithOrganisation;

            IOrganization organisation = (IOrganization)this.Broker.GetBMCInstance(organisationId);
            ReportingUnitListDS reportingUnitListDS = (ReportingUnitListDS)organisation.GetReportingUnits();
            DataTable reportingUnitListTable = reportingUnitListDS.Tables[ReportingUnitListDS.REPORTINGUNITLIST_TABLE];
            if (reportingUnitListTable.Rows.Count > 0)
                return OrganisationManagementErrors.OrganisationContainsEntities;

            ILogicalModule logicalModule = organisation.GetLogicalModule();
            Guid logicalModuleCID = logicalModule.CID;

            this.Broker.ReleaseBrokerManagedComponent(logicalModule);
            this.Broker.ReleaseBrokerManagedComponent(organisation);

            this.Broker.DeleteLogicalCM(logicalModuleCID);

            return OrganisationManagementErrors.None;
        }

        private bool UsersAssociatedWithOrganisation(Guid organisationId)
        {
            DBUserDetailsDS dataList = (DBUserDetailsDS)Broker.GetBMCDetailList("DBUser_1_1");
            DataTable userDetailsTable = dataList.Tables[DBUserDetailsDS.USER_DETAIL_TABLE];
            foreach (DataRow dataRow in userDetailsTable.Rows)
            {
                if (dataRow[DBUserDetailsDS.USER_ORGANISATION_CID_FIELD] == DBNull.Value)
                    continue;
                string userOrganisationIdString = (string)dataRow[DBUserDetailsDS.USER_ORGANISATION_CID_FIELD];
                if (userOrganisationIdString == string.Empty)
                    continue;
                Guid userOrganisationId = new Guid(userOrganisationIdString);
                if (organisationId == userOrganisationId)
                    return true;
            }

            return false;
        }

        #endregion

        #region Migration

        public override DataSet MigrateDataSet(DataSet data)
        {
            DataSet baseDataSet = base.MigrateDataSet(data);

            DataSet convData = DBUserDS.ConvertV_1_0_1_0toV_1_1_0_0DataSet(data, data);
            //convData = ConvertV_XtoV_YDataSet(data, convData);

            if (convData == null)
                return null;

            DBUserDS ds = new DBUserDS();

            //Merge in all of the component tables
            ds.Merge(convData.Tables[DBUserDS.DBUSER_TABLE]);
            ds.Merge(convData.Tables[DBUserDetailsDS.USER_DETAIL_TABLE]);
            ds.Merge(convData.Tables[DBSecurityGroupDetailsDS.SECGRP_DETAIL_TABLE]);
            ds.Merge(convData.Tables[PasswordDS.PasswordDetailsTable]);

            ds.Merge(baseDataSet);
            return ds;
        }

        #endregion

        private void setLocalVariables()
        {
            DataRow drUser = dsUser.Tables[DBUserDetailsDS.USER_DETAIL_TABLE].Rows[0];

            this.firstName = drUser[DBUserDetailsDS.USER_FIRSTNAME_FIELD].ToString();
            this.lastName = drUser[DBUserDetailsDS.USER_LASTNAME_FIELD].ToString();
            this.emailAddress = drUser[DBUserDetailsDS.USER_EMAILADDRESS_FIELD].ToString();

            this.FailedAttempts = Convert.ToInt32(drUser[DBUserDetailsDS.USER_FAILEDATTEMPTS_FIELD].ToString());
            this.Locked = Convert.ToInt32(drUser[DBUserDetailsDS.USER_LOCKED_FIELD].ToString());
            if (drUser[DBUserDetailsDS.USER_ADMINISTRATOR_FIELD] != DBNull.Value)
            {
                if (Convert.ToInt32(drUser[DBUserDetailsDS.USER_ADMINISTRATOR_FIELD].ToString()) == 1)
                    this.Administrator = true;
                else
                    this.Administrator = false;
            }
            else
            {
                this.Administrator = false;
            }
            this.Name = drUser[DBUserDetailsDS.USER_USERNAME_FIELD].ToString();

            this.displayZeroPreference = (bool)drUser[DBUserDetailsDS.USER_DISPLAY_ZERO_PREFERENCE_FIELD];

            this.Context.ReportingUnitName = drUser[DBUserDetailsDS.USER_REPORTINGUNITNAME_FIELD].ToString();
            if (drUser[DBUserDetailsDS.USER_REPORTINGUNITCLID_FIELD] != DBNull.Value)
                this.Context.ReportingUnitCLID = (Guid)drUser[DBUserDetailsDS.USER_REPORTINGUNITCLID_FIELD];
            else
                this.Context.ReportingUnitCLID = Guid.Empty;

            this.Context.ScenarioName = drUser[DBUserDetailsDS.USER_SCENARIONAME_FIELD].ToString();
            if (drUser[DBUserDetailsDS.USER_SCENARIOCSID_FIELD] != DBNull.Value)
                this.Context.ScenarioCSID = (Guid)drUser[DBUserDetailsDS.USER_SCENARIOCSID_FIELD];
            else
                this.Context.ScenarioCSID = Guid.Empty;

            this.Context.PeriodName = drUser[DBUserDetailsDS.USER_PERIODNAME_FIELD].ToString();
            if (drUser[DBUserDetailsDS.USER_PERIODCLID_FIELD] != DBNull.Value)
                this.Context.PeriodCLID = (Guid)drUser[DBUserDetailsDS.USER_PERIODCLID_FIELD];
            else
                this.Context.PeriodCLID = Guid.Empty;

            if (drUser[DBUserDetailsDS.USER_ORGANISATION_CID_FIELD] != DBNull.Value)
                this.organisationCID = (Guid)drUser[DBUserDetailsDS.USER_ORGANISATION_CID_FIELD];
            else
                this.organisationCID = Guid.Empty;

            this.administratorType = (AdministratorType)Convert.ToInt32(drUser[DBUserDetailsDS.USER_FAILEDATTEMPTS_FIELD].ToString());

            if (drUser[DBUserDetailsDS.USER_USER_TYPE_ENUM_FIELD] != DBNull.Value)
            {
                this.userType = (UserType)drUser[DBUserDetailsDS.USER_USER_TYPE_ENUM_FIELD];
            }

            if (drUser[DBUserDetailsDS.USER_USER_TYPE_FIELD] != DBNull.Value)
                this.userType = (UserType)Convert.ToInt32(drUser[DBUserDetailsDS.USER_USER_TYPE_FIELD].ToString());
            else
                this.userType = UserType.Advisor;

            if (drUser[DBUserDetailsDS.USER_USER_TYPE_ENUM_FIELD] != DBNull.Value)
            {
                this.userType = (UserType)drUser[DBUserDetailsDS.USER_USER_TYPE_ENUM_FIELD];
            }

            if (drUser[DBUserDetailsDS.USER_ORGANISATION_NAME_FIELD] != DBNull.Value)
                this.organisationName = drUser[DBUserDetailsDS.USER_ORGANISATION_NAME_FIELD].ToString();
            else
                this.organisationName = string.Empty;

            if (drUser[DBUserDetailsDS.ADVISERCID] != DBNull.Value)
                this.adviserCID = (Guid) drUser[DBUserDetailsDS.ADVISERCID]; 
        }

        public void AddFavouriteWorkpaper(string folderName, string workpaperName, string workpaperAddress)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            DatabaseUtilities.AddParameterToList(parameters, DBUserConstants.AddFavorite_Id, SqlDbType.UniqueIdentifier,
                Guid.NewGuid());
            DatabaseUtilities.AddParameterToList(parameters, DBUserConstants.AddFavorite_UserId, SqlDbType.UniqueIdentifier,
                this.CID);
            DatabaseUtilities.AddParameterToList(parameters, DBUserConstants.AddFavorite_Folder, SqlDbType.UniqueIdentifier,
                Guid.Empty);
            DatabaseUtilities.AddParameterToList(parameters, DBUserConstants.AddFavorite_WorkpaperName, SqlDbType.VarChar,
                DBUserConstants.AddFavorite_WorkpaperName_Size, workpaperName);
            DatabaseUtilities.AddParameterToList(parameters, DBUserConstants.AddFavorite_WorkpaperAddr, SqlDbType.VarChar,
                DBUserConstants.AddFavorite_WorkpaperAddr_Size, workpaperAddress);

            DatabaseUtilities.ExecuteNonQueryStoredProcedure(DBUserConstants.AddFavorite_SP, parameters);
        }

        public void AddFavouriteWorkpaper(Guid folderId, string workpaperName, string workpaperAddress, string breadcrumb)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            DatabaseUtilities.AddParameterToList(parameters, DBUserConstants.AddFavorite_Id, SqlDbType.UniqueIdentifier,
                Guid.NewGuid());
            DatabaseUtilities.AddParameterToList(parameters, DBUserConstants.AddFavorite_UserId, SqlDbType.UniqueIdentifier,
                this.CID);
            DatabaseUtilities.AddParameterToList(parameters, DBUserConstants.AddFavorite_Folder, SqlDbType.UniqueIdentifier,
                folderId);
            DatabaseUtilities.AddParameterToList(parameters, DBUserConstants.AddFavorite_WorkpaperName, SqlDbType.VarChar,
                DBUserConstants.AddFavorite_WorkpaperName_Size, workpaperName);
            DatabaseUtilities.AddParameterToList(parameters, DBUserConstants.AddFavorite_WorkpaperAddr, SqlDbType.VarChar,
                DBUserConstants.AddFavorite_WorkpaperAddr_Size, workpaperAddress);
            DatabaseUtilities.AddParameterToList(parameters, "BreadCrumb", SqlDbType.VarChar,
                355, breadcrumb);

            DatabaseUtilities.ExecuteNonQueryStoredProcedure(DBUserConstants.AddFavorite_SP, parameters);
        }

        public void AddFavouriteFolder(string folderName, Guid parentFolderId)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            DatabaseUtilities.AddParameterToList(parameters, DBUserConstants.AddFavoriteFolder_Id, SqlDbType.UniqueIdentifier,
                Guid.NewGuid());
            DatabaseUtilities.AddParameterToList(parameters, DBUserConstants.AddFavoriteFolder_UserId, SqlDbType.UniqueIdentifier,
                this.CID);
            DatabaseUtilities.AddParameterToList(parameters, DBUserConstants.AddFavoriteFolder_ParentFolder, SqlDbType.UniqueIdentifier,
                parentFolderId);
            DatabaseUtilities.AddParameterToList(parameters, DBUserConstants.AddFavoriteFolder_FolderName, SqlDbType.VarChar,
                DBUserConstants.AddFavoriteFolder_FolderName_Size, folderName);
            DatabaseUtilities.AddParameterToList(parameters, DBUserConstants.AddFavoriteFolder_IsRoot, SqlDbType.Bit, 0);

            DatabaseUtilities.ExecuteNonQueryStoredProcedure(DBUserConstants.AddFavoriteFolder_SP, parameters);
        }

        public void ExtractFavouriteWorkpapers(DataSet dataSet)
        {
            this.ExtractFavouriteWorkpapersFolders(dataSet);

            DataTable table = dataSet.Tables[DBUserConstants.FAVOURITEFOLDERSTABLE];
            string rootRowFilter = string.Format("{0}=1", DBUserConstants.IsRootFolder);
            DataRow[] rootRow = table.Select(rootRowFilter);
            if (table.Rows.Count == 0)
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                DatabaseUtilities.AddParameterToList(parameters, DBUserConstants.AddFavoriteFolder_Id, SqlDbType.UniqueIdentifier,
                    Guid.NewGuid());
                DatabaseUtilities.AddParameterToList(parameters, DBUserConstants.AddFavoriteFolder_UserId, SqlDbType.UniqueIdentifier,
                    this.CID);
                DatabaseUtilities.AddParameterToList(parameters, DBUserConstants.AddFavoriteFolder_ParentFolder, SqlDbType.UniqueIdentifier,
                    Guid.Empty);
                DatabaseUtilities.AddParameterToList(parameters, DBUserConstants.AddFavoriteFolder_FolderName, SqlDbType.VarChar,
                    DBUserConstants.AddFavoriteFolder_FolderName_Size, "Favourites");
                DatabaseUtilities.AddParameterToList(parameters, DBUserConstants.AddFavoriteFolder_IsRoot, SqlDbType.Bit, 1);

                DatabaseUtilities.ExecuteNonQueryStoredProcedure(DBUserConstants.AddFavoriteFolder_SP, parameters);

                dataSet.Tables.Remove(DBUserConstants.FAVOURITEWORKPAPERSTABLE);
                dataSet.Tables.Remove(DBUserConstants.FAVOURITEFOLDERSTABLE);
                this.ExtractFavouriteWorkpapersFolders(dataSet);
            }
        }

        private void ExtractFavouriteWorkpapersFolders(DataSet dataSet)
        {
            Dictionary<string, string> tableNamePairs = new Dictionary<string, string>();
            tableNamePairs.Add("Table", DBUserConstants.FAVOURITEWORKPAPERSTABLE);
            tableNamePairs.Add("Table1", DBUserConstants.FAVOURITEFOLDERSTABLE);

            List<SqlParameter> parameters = new List<SqlParameter>();
            DatabaseUtilities.AddParameterToList(parameters, DBUserConstants.AddFavorite_UserId, SqlDbType.UniqueIdentifier,
                this.CID);

            DatabaseUtilities.ExecuteStoredProcedure(dataSet, DBUserConstants.SP_Favourites, tableNamePairs, parameters);
        }

        private AuthenticateUserDS GetAuthenticationDataSet(string name, string password)
        {
            AuthenticateUserDS data = new AuthenticateUserDS();
            DataRow row;
            row = data.Tables[AuthenticateUserDS.AUTHENTICATION_DETAIL_TABLE].NewRow();
            row[AuthenticateUserDS.AUTHENTICATION_USERNAME_FIELD] = name;
            row[AuthenticateUserDS.AUTHENTICATION_PASSWORD_FIELD] = password;
            data.Tables[AuthenticateUserDS.AUTHENTICATION_DETAIL_TABLE].Rows.Add(row);
            return data;
        }


        bool IAuthenticable.Authenticate(string name, string password)
        {
            AuthenticateUserDS dsAuthenticate;
            PasswordDS dsPassword = new PasswordDS();

            SAS70PasswordSM pwdSM = (SAS70PasswordSM)this.Broker.GetWellKnownBMC(WellKnownCM.SecurityProvider);
            dsAuthenticate = GetAuthenticationDataSet(name, password);
            this.GetData(dsAuthenticate);
            this.GetData(dsPassword);

            DataRow row = dsAuthenticate.Tables[AuthenticateUserDS.AUTHENTICATION_DETAIL_TABLE].Rows[0];
            bool status = Convert.ToInt32(row[AuthenticateUserDS.AUTHENTICATION_AUTHENTICATE_FIELD].ToString()) == AuthenticateUserDS.AUTHENTICATE_SUCCESS;
            return status;
        }
    }
}
