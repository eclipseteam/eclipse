#region CHANGE_HISTORY
/*
 $Header: /2002/3. Implementation/NewConstruction/Components/General/DBUser/DBUser-1-0/TXPrincipal.cs 3     4/08/03 3:12p Sshrimpton $
 $History: TXPrincipal.cs $
 * 
 * *****************  Version 3  *****************
 * User: Sshrimpton   Date: 4/08/03    Time: 3:12p
 * Updated in $/2002/3. Implementation/NewConstruction/Components/General/DBUser/DBUser-1-0
 * 
 * *****************  Version 2  *****************
 * User: Secampbell   Date: 7/03/03    Time: 4:57p
 * Updated in $/2002/3. Implementation/Construction/Security/DBUser-1-0
 * 
 * *****************  Version 1  *****************
 * User: Secampbell   Date: 7/03/03    Time: 4:55p
 * Created in $/2002/3. Implementation/Construction/Security/DBUser-1-0/DBUSER-1-0
 * 
 * *****************  Version 1  *****************
 * User: Secampbell   Date: 4/03/03    Time: 3:44p
 * Created in $/2002/3. Implementation/Construction/Security/DBUser-1-0
 * 
 * *****************  Version 2  *****************
 * User: Secampbell   Date: 10/01/03   Time: 9:57a
 * Updated in $/2002/3. Implementation/Elaboration4/Framework/Security
*/
#endregion 


using System;
using System.Security.Principal;

namespace Oritax.TaxSimp.Security
{
	/// <summary>
	/// Summary description for TXPrincipal.
	/// </summary>
	[Serializable]
	public class TXPrincipal : GenericPrincipal, ITXPrincipal
	{
		private String[] groups;

		public String[] Groups
		{
			get{ return groups; }
			set{ groups = value; }
		}
		
		public TXPrincipal( GenericIdentity objIdent, String[] roles ) : base( objIdent, roles )
		{
			groups = roles;
		}
		
	}
}
