﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.SAS70Password;
using Oritax.TaxSimp.Services;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.Security
{
    public partial class DBUser
    {
        #region Enums
        public enum SortOrderEnum
        {
            Assending = 0,
            Descending = 1
        }
        #endregion

        #region Private Variables
        private int pwdMinLength = 0;
        private int pwdMaxLength = 0;
        private int repeatingCharLength = 0;
        private int historyReq = 0;
        private SAS70PasswordSM pwdSM = null;
        protected bool AdminCheck = false;
        #endregion

        #region Override Method

        public override string GetDataStream(int type, string data)
        {
            String xml = null;
            switch (type)
            {
                case 0:
                    {
                        DBUserSL dbusersl = data.ToNewOrData<DBUserSL>();
                        this.UserName = dbusersl.CurrentUserName;
                        xml = this.GetAllDBUser().ToXmlString();
                    }
                    break;
                case 1:
                    {
                        DBUserAddSL dbuseraddsl = data.ToNewOrData<DBUserAddSL>();
                        this.UserName = dbuseraddsl.CurrentUserName;
                        xml = this.GetDBUserAdd().ToXmlString();
                    }
                    break;
                case 2:
                    {
                        DBUserEditSL dbusereditsl = data.ToNewOrData<DBUserEditSL>();
                        this.UserName = dbusereditsl.CurrentUserName;
                        xml = this.GetDBUserEdit().ToXmlString();
                    }
                    break;
                default:
                    if (string.Compare(firstName, lastName) == 0)
                        xml = firstName;
                    else
                        xml = string.Format("{0} {1}", firstName, lastName);
                    break;
            }
            return xml;
        }

        public override string OnAddDataStream(int type, string data)
        {
            String xml = null;
            switch (type)
            {
                case 2:
                    {
                        DBUserAddSL dbuseraddsl = data.ToNewOrData<DBUserAddSL>();
                        this.UserName = dbuseraddsl.CurrentUserName;
                        xml = this.AddDBUser(dbuseraddsl).ToXmlString();
                    }
                    break;
                default:
                    throw new Exception("Unsupported data type");
            }
            return xml;
        }

        public override string OnUpdateDataStream(int type, string data)
        {
            String xml = null;
            switch (type)
            {
                case 2:
                    {
                        DBUserAddSL dbuseraddsl = data.ToNewOrData<DBUserAddSL>();
                        this.UserName = dbuseraddsl.CurrentUserName;
                        xml = this.UpdateDBUser(dbuseraddsl).ToXmlString();
                    }
                    break;
                case 500:
                    {
                        DBUserAddSL dbuseraddsl = data.ToNewOrData<DBUserAddSL>();
                        this.UserName = dbuseraddsl.CurrentUserName;
                        xml = this.UpdateDBUserPassword(dbuseraddsl).ToXmlString();
                    }
                    break;
                case 600:
                    {
                        var userCID = data;
                        this.DeleteUser(userCID);
                    }
                    break;
                default:
                    throw new Exception("Unsupported data type");
            }
            return xml;
        }

        #endregion

        private String UserName { get; set; }

        private DBUserListSL GetAllDBUser()
        {
            DBUserDetailsDS ds = new DBUserDetailsDS();
            DBUserListSL list;
            ds.UserList = true;
            this.OnGetData(ds);
            this.addUserLoginInfo(ds);
            this.BuildUsersDataSource(ds);
            list = IterateDBUserDetailsDS(ds);
            return list;
        }

        private DBUserListSL IterateDBUserDetailsDS(DBUserDetailsDS ds)
        {
            DBUserListSL dbuserlistsl = new DBUserListSL();
            DataTable userDetailTable = ds.Tables[DBUserDetailsDS.USER_DETAIL_TABLE];
            foreach (DataRow _row in userDetailTable.Rows)
            {
                dbuserlistsl.Add(this.ExtractRow_DBUserSL(_row));
            }
            return dbuserlistsl;
        }

        private DBUserSL ExtractRow_DBUserSL(DataRow row)
        {
            DBUserSL dbusersl = new DBUserSL();
            //dbusersl.CurrentUserName = row[DBUserDetailsDS.CurrentUserName].ToString();
            //dbusersl.SECURITYGROUP_NAME_FIELD = row[DBUserDetailsDS.SECURITYGROUP_NAME_FIELD].ToString();
            //dbusersl.SECURITYGROUP_USERID_FIELD = row[DBUserDetailsDS.SECURITYGROUP_USERID_FIELD].ToString();
            dbusersl.USER_ACTIVE_FIELD = row[DBUserDetailsDS.USER_ACTIVE_FIELD].ToString();
            dbusersl.USER_ADMINISTRATOR_FIELD = row[DBUserDetailsDS.USER_ADMINISTRATOR_FIELD].ToString();
            dbusersl.USER_ADMINISTRATOR_TYPE_FIELD = row[DBUserDetailsDS.USER_ADMINISTRATOR_TYPE_FIELD].ToString();
            dbusersl.USER_USER_TYPE_FIELD = row[DBUserDetailsDS.USER_USER_TYPE_FIELD].ToString();
            dbusersl.USER_DISPLAY_ZERO_PREFERENCE_FIELD = row[DBUserDetailsDS.USER_DISPLAY_ZERO_PREFERENCE_FIELD].ToString();
            dbusersl.USER_EMAILADDRESS_FIELD = row[DBUserDetailsDS.USER_EMAILADDRESS_FIELD].ToString();
            dbusersl.USER_FAILEDATTEMPTS_FIELD = row[DBUserDetailsDS.USER_FAILEDATTEMPTS_FIELD].ToString();
            dbusersl.USER_FIRSTNAME_FIELD = row[DBUserDetailsDS.USER_FIRSTNAME_FIELD].ToString();
            dbusersl.USER_LASTLOGGEDIN_FIELD = row[DBUserDetailsDS.USER_LASTLOGGEDIN_FIELD].ToString();
            dbusersl.USER_LASTNAME_FIELD = row[DBUserDetailsDS.USER_LASTNAME_FIELD].ToString();
            dbusersl.USER_LOCKED_FIELD = row[DBUserDetailsDS.USER_LOCKED_FIELD].ToString();
            dbusersl.USER_ORGANISATION_CID_FIELD = row[DBUserDetailsDS.USER_ORGANISATION_CID_FIELD].ToString();
            dbusersl.USER_ORGANISATION_NAME_FIELD = row[DBUserDetailsDS.USER_ORGANISATION_NAME_FIELD].ToString();
            dbusersl.USER_PASSWORD_FIELD = row[DBUserDetailsDS.USER_PASSWORD_FIELD].ToString();
            dbusersl.USER_PERIODCLID_FIELD = row[DBUserDetailsDS.USER_PERIODCLID_FIELD].ToString();
            dbusersl.USER_PERIODNAME_FIELD = row[DBUserDetailsDS.USER_PERIODNAME_FIELD].ToString();
            dbusersl.USER_PRIVTEMPLATECID_FIELD = row[DBUserDetailsDS.USER_PRIVTEMPLATECID_FIELD].ToString();
            dbusersl.USER_REPORTINGUNITCLID_FIELD = row[DBUserDetailsDS.USER_REPORTINGUNITCLID_FIELD].ToString();
            dbusersl.USER_REPORTINGUNITNAME_FIELD = row[DBUserDetailsDS.USER_REPORTINGUNITNAME_FIELD].ToString();
            dbusersl.USER_SCENARIOCSID_FIELD = row[DBUserDetailsDS.USER_SCENARIOCSID_FIELD].ToString();
            dbusersl.USER_SCENARIONAME_FIELD = row[DBUserDetailsDS.USER_SCENARIONAME_FIELD].ToString();
            dbusersl.USER_USERID_FIELD = row[DBUserDetailsDS.USER_USERID_FIELD].ToString();
            dbusersl.USER_USERNAME_FIELD = row[DBUserDetailsDS.USER_USERNAME_FIELD].ToString();
            dbusersl.USER_CURRENTLYLOGGEDIN = row["CurrentlyLoggedIn"].ToString();
            return dbusersl;
        }

        private void addUserLoginInfo(DataSet data)
        {
            List<string> loggedInUsers = new List<string>(UserAuthentication.GetLoggedInUsers());
            if (!data.Tables[DBUserDetailsDS.USER_DETAIL_TABLE].Columns.Contains("CurrentlyLoggedIn"))
                data.Tables[DBUserDetailsDS.USER_DETAIL_TABLE].Columns.Add("CurrentlyLoggedIn", typeof(string));

            foreach (DataRow row in data.Tables[DBUserDetailsDS.USER_DETAIL_TABLE].Rows)
            {
                if (loggedInUsers.Contains((string)row["Username"]))
                    row["CurrentlyLoggedIn"] = "Yes";
                else
                    row["CurrentlyLoggedIn"] = "No";
            }

            SortDataTable(data.Tables[DBUserDetailsDS.USER_DETAIL_TABLE], "CurrentlyLoggedIn", SortOrderEnum.Descending);
        }

        private void SortDataTable(DataTable oDT, String sField, SortOrderEnum eOrder)
        {
            DataTable oSortedDT = oDT.Copy();
            DataTable oTMP = oDT.Clone();
            DataView oDV = oSortedDT.DefaultView;

            if (eOrder == SortOrderEnum.Assending)
                oDV.Sort = sField + " ASC";
            else
                oDV.Sort = sField + " DESC";

            oDT.Clear();

            foreach (DataRowView row in oDV)
            {
                String sData = row[sField].ToString().Trim();

                if (sData == String.Empty)
                    oTMP.ImportRow(row.Row); // Put Blank Rows at the bottom
                else
                    oDT.ImportRow(row.Row);
            }

            foreach (DataRow row in oTMP.Rows)
            {
                oDT.ImportRow(row);
            }
        }

        protected bool UserIsSystemAdministrator()
        {
            if (this.UserName.ToLower() == "administrator")
                return true;
            else
                return false;
        }

        private DataView BuildUsersDataSource(DataSet dataSet)
        {
            DataTable userDetailTable = dataSet.Tables[DBUserDetailsDS.USER_DETAIL_TABLE];
            DataView dataView = null;
            if (UserIsSystemAdministrator())
            {
                dataView = new DataView(userDetailTable);
            }
            else
            {
                Guid organisationId = this.UserOrganisation;
                string select = string.Format("{0} = '{1}'", DBUserDetailsDS.USER_ORGANISATION_CID_FIELD, organisationId.ToString());
                dataView = new DataView(userDetailTable, select, string.Empty, DataViewRowState.CurrentRows);
            }
            return dataView;
        }

        private Guid UserOrganisation
        {
            get
            {
                //if (UserName is ITXPrincipal)
                //{
                //ITXPrincipal objPrincipal = (ITXPrincipal)Context.User;
                IDBUser userBMC = (IDBUser)Broker.GetBMCInstance(UserName, "DBUser_1_1");// this.Broker.GetBMCInstance(new Guid(objPrincipal.Groups[0]));
                return userBMC.OrganisationCID;
                //}
                //else
                //return Guid.Empty;
            }
        }

        private DBUserAddSL GetDBUserAdd()
        {
            DBUserDetailsDS ds = new DBUserDetailsDS();
            DBUserAddSL dbuseraddsl = new DBUserAddSL();
            ds.UserList = true;
            this.OnGetData(ds);
            this.BuildUsersDataSource(ds);
            dbuseraddsl.OrganisationalDetailsList = GetOrganisationDetails(ds);
            this.InitiateVariables();
            dbuseraddsl.PasswordRulesList = GetPasswordRules();
            return dbuseraddsl;
        }

        private DBUserEditSL GetDBUserEdit()
        {
            DBUserDetailsDS ds = new DBUserDetailsDS();
            DBUserEditSL dbusereditsl = new DBUserEditSL();
            ds.UserList = false;
            this.OnGetData(ds);
            this.addUserLoginInfo(ds);
            this.BuildUsersDataSource(ds);
            dbusereditsl.OrganisationalDetailsList = GetOrganisationDetails(ds);
            DataTable userDetailTable = ds.Tables[DBUserDetailsDS.USER_DETAIL_TABLE];
            if (userDetailTable.Rows.Count > 0)
            {
                DataRow drUser = userDetailTable.Rows[0];
                dbusereditsl.DBUserSLDetail = ExtractRow_DBUserSL(drUser);
                if (dbusereditsl.DBUserSLDetail != null)
                {
                  
                    dbusereditsl.CurrentUserName = this.UserName;
                    dbusereditsl.LoginName = dbusereditsl.DBUserSLDetail.USER_USERNAME_FIELD;
                    dbusereditsl.FirstName = dbusereditsl.DBUserSLDetail.USER_FIRSTNAME_FIELD;
                    dbusereditsl.LastName = dbusereditsl.DBUserSLDetail.USER_LASTNAME_FIELD;
                    dbusereditsl.Email = dbusereditsl.DBUserSLDetail.USER_EMAILADDRESS_FIELD;
                    dbusereditsl.OrganisationGuid = dbusereditsl.DBUserSLDetail.USER_ORGANISATION_CID_FIELD;

                    dbusereditsl.IsAdministrator = Convert.ToBoolean(Convert.ToInt32(dbusereditsl.DBUserSLDetail.USER_ADMINISTRATOR_FIELD));
                    dbusereditsl.IsActive = Convert.ToBoolean(Convert.ToInt32(dbusereditsl.DBUserSLDetail.USER_ACTIVE_FIELD));
                    dbusereditsl.IsLocked = Convert.ToBoolean(Convert.ToInt32(dbusereditsl.DBUserSLDetail.USER_LOCKED_FIELD));
                    dbusereditsl.UserType = Convert.ToInt32(dbusereditsl.DBUserSLDetail.USER_USER_TYPE_FIELD);
                }
            }
            return dbusereditsl;
        }

        private void InitiateVariables()
        {
            if (this.pwdSM == null)
            {
                this.pwdSM = (SAS70PasswordSM)this.Broker.GetWellKnownBMC(WellKnownCM.SecurityProvider);
                this.pwdMinLength = this.pwdSM.PasswordMinLength;
                this.pwdMaxLength = this.pwdSM.PasswordMaxLength;
                this.repeatingCharLength = this.pwdSM.MaxRepeatingChars;
                this.historyReq = this.pwdSM.PasswordHistory;
            }
        }

        private List<DBUserOrganisationalDetailsSL> GetOrganisationDetails(DBUserDetailsDS ds)
        {
            DataTable organisationalDetailsTable = ds.OrganisationalDetailsTable;
            List<DBUserOrganisationalDetailsSL> list = new List<DBUserOrganisationalDetailsSL>();
            foreach (DataRow _row in organisationalDetailsTable.Rows)
            {
                list.Add(new DBUserOrganisationalDetailsSL()
                {
                    ORGANISATIONAL_ID_FIELD = _row[OrganisationsDetailsDS.ORGANISATIONAL_ID_FIELD].ToString(),
                    ORGANISATIONAL_NAME_FIELD = _row[OrganisationsDetailsDS.ORGANISATIONAL_NAME_FIELD].ToString()
                });
            }
            return list;
        }

        private List<DBUserPasswordRulesSL> GetPasswordRules()
        {
            PasswordDS data = new PasswordDS();
            List<DBUserPasswordRulesSL> list = new List<DBUserPasswordRulesSL>();
            int index = 0;
            foreach (DataRow _row in data.Tables[PasswordDS.PasswordRulesTable].Rows)
            {
                list.Add(ExtractRow_PasswordRules(_row, ++index));
            }
            return list;
        }

        private string LineBreakForSilverlight(string value)
        {
            return value.Replace("<br/>", Environment.NewLine);
        }

        private DBUserPasswordRulesSL ExtractRow_PasswordRules(DataRow row, int index)
        {
            DBUserPasswordRulesSL dbuserpasswordrulessl;// = new DBUserPasswordRulesSL();
            dbuserpasswordrulessl = new DBUserPasswordRulesSL();
            dbuserpasswordrulessl.PasswordKeyField = row[PasswordHistoryDS.PasswordKeyField].ToString();
            string pwdcase = row[PasswordHistoryDS.PasswordKeyField].ToString();
            string desc = System.Web.HttpUtility.HtmlDecode(row[PasswordHistoryDS.PasswordDescriptionField].ToString());
            desc = this.LineBreakForSilverlight(desc);
            switch (pwdcase)
            {
                case PasswordHistoryDS.PasswordLengthKey:
                    dbuserpasswordrulessl.PasswordDescriptionField = String.Format(desc, index, this.pwdMinLength.ToString(), this.pwdMaxLength.ToString());
                    break;
                case PasswordHistoryDS.PasswordNoRepeatingCharsKey:
                    dbuserpasswordrulessl.PasswordDescriptionField = String.Format(desc, index, this.repeatingCharLength.ToString());
                    break;
                case PasswordHistoryDS.PasswordNoHistoricalKey:
                    dbuserpasswordrulessl.PasswordDescriptionField = String.Format(desc, index, this.historyReq.ToString());
                    break;
                case PasswordHistoryDS.PasswordNonAlphaCharsKey:
                    dbuserpasswordrulessl.PasswordDescriptionField = String.Format(desc, index, "{}");
                    break;
                default:
                    dbuserpasswordrulessl.PasswordDescriptionField = String.Format(desc, index);
                    break;
            }

            //dbuserpasswordrulessl.PasswordDescriptionField = System.Web.HttpUtility.HtmlDecode(String.Format(row[PasswordHistoryDS.PasswordDescriptionField].ToString(), index));
            dbuserpasswordrulessl.PasswordResultField = row[PasswordHistoryDS.PasswordResultField].ToString();
            return dbuserpasswordrulessl;
        }

        private DBUserValidate AddDBUser(DBUserAddSL dbuseraddsl)
        {
            DBUserValidate dbuservalidate = new DBUserValidate();
            this.ValidateUser(dbuseraddsl.LoginName, dbuseraddsl.FirstName, dbuseraddsl.LastName, dbuseraddsl.Email, dbuseraddsl.Password, dbuseraddsl.ConfirmPassword, dbuservalidate);
            if (dbuservalidate != null)
            {
                if (dbuservalidate.IsValid == true)
                {
                    SetDataDBUserAdd(dbuseraddsl.LoginName, dbuseraddsl.FirstName, dbuseraddsl.LastName,
                        dbuseraddsl.Email, dbuseraddsl.Password, Convert.ToBoolean(dbuseraddsl.IsAdministrator),
                        dbuseraddsl.OrganisationGuid, "Default", dbuseraddsl.UserType);
                }
            }
            return dbuservalidate;
        }

        private DBUserValidate UpdateDBUserPassword(DBUserAddSL dbuseraddsl)
        {
            DBUserValidate dbuservalidate = new DBUserValidate();
            this.ValidateUser(dbuseraddsl.LoginName, dbuseraddsl.Password, dbuseraddsl.ConfirmPassword, dbuservalidate);
            if (dbuservalidate != null)
            {
                if (dbuservalidate.IsValid == true)
                {
                    SetPassword(dbuseraddsl.Password);
                }
            }
            return dbuservalidate;
        }

        private DBUserValidate UpdateDBUser(DBUserAddSL dbuseraddsl)
        {
            DBUserValidate dbuservalidate = new DBUserValidate();
            this.ValidateUser(dbuseraddsl.LoginName, dbuseraddsl.FirstName, dbuseraddsl.LastName, dbuseraddsl.Email, dbuservalidate);
            if (dbuservalidate != null)
            {
                if (dbuservalidate.IsValid == true)
                {
                    SetDataDBUserUpdate(dbuseraddsl.Email, dbuseraddsl.FirstName, dbuseraddsl.LastName,
                        dbuseraddsl.Email, dbuseraddsl.Password, Convert.ToBoolean(dbuseraddsl.IsAdministrator),
                        dbuseraddsl.OrganisationGuid, "Default", Convert.ToBoolean(dbuseraddsl.IsActive), Convert.ToBoolean(dbuseraddsl.IsLocked), dbuseraddsl.UserType);
                }
            }
            return dbuservalidate;
        }

        private void ValidateUser(string LoginUsername, string Firstname, string Lastname, string Email,
            string Password, string ConfirmPassword, DBUserValidate dbuservalidate)
        {
            this.InitiateVariables();
            dbuservalidate.IsValid = true;
            // Check login name
            if (LoginUsername.Length == 0)
            {
                dbuservalidate.IsValid = false;
                dbuservalidate.ErrorMessage = new MessageBoxDefinition(MessageBoxDefinition.MSG_LOGIN_NAME_EMPTY).ToString();
                return;
            }
            // Check first name.
            if (Firstname.Length == 0)
            {
                dbuservalidate.IsValid = false;
                dbuservalidate.ErrorMessage = new MessageBoxDefinition(MessageBoxDefinition.MSG_FIRST_NAME_EMPTY).ToString();
                return;
            }
            else if (!(Regex.IsMatch(Firstname, DBUserDetailsDS.REGEX_ALPHABETIC, RegexOptions.IgnoreCase)))
            {
                dbuservalidate.IsValid = false;
                dbuservalidate.ErrorMessage = new MessageBoxDefinition(MessageBoxDefinition.MSG_INVALID_FIRST_NAME).ToString();
                return;
            }

            // Check Last name.
            if (Lastname.Length == 0)
            {
                dbuservalidate.IsValid = false;
                dbuservalidate.ErrorMessage = new MessageBoxDefinition(MessageBoxDefinition.MSG_LAST_NAME_EMPTY).ToString();
                return;
            }
            else if (!(Regex.IsMatch(Lastname, DBUserDetailsDS.REGEX_ALPHABETIC, RegexOptions.IgnoreCase)))
            {
                dbuservalidate.IsValid = false;
                dbuservalidate.ErrorMessage = new MessageBoxDefinition(MessageBoxDefinition.MSG_INVALID_LAST_NAME).ToString();
                return;
            }

            // Check Email address.
            if (Email.Length == 0)
            {
                dbuservalidate.IsValid = false;
                dbuservalidate.ErrorMessage = new MessageBoxDefinition(MessageBoxDefinition.MSG_EMAIL_EMPTY).ToString();
                return;
            }
            if (Email.Length != 0)
            {
                if (!(Regex.IsMatch(Email, DBUserDetailsDS.REGEX_EMAIL, RegexOptions.IgnoreCase)))
                {
                    dbuservalidate.IsValid = false;
                    dbuservalidate.ErrorMessage = new MessageBoxDefinition(MessageBoxDefinition.MSG_INVALID_EMAIL).ToString();
                    return;
                }
            }

            if (Password.CompareTo(ConfirmPassword) != 0)
            {
                dbuservalidate.IsValid = false;
                dbuservalidate.ErrorMessage = new MessageBoxDefinition(MessageBoxDefinition.MSG_PASSWORD_NO_MATCH).ToString();
                return;
            }

            // Check Password
            PasswordDS data = new PasswordDS();
            if (!this.pwdSM.CheckRulesForNewUser(LoginUsername, Password, data.Tables[PasswordDS.PasswordRulesTable]))
            {
                dbuservalidate.IsValid = false;

                List<DBUserPasswordRulesSL> PasswordRulesList = new List<DBUserPasswordRulesSL>();
                int index = 0;
                foreach (DataRow _row in data.Tables[PasswordDS.PasswordRulesTable].Rows)
                {
                    PasswordRulesList.Add(ExtractRow_PasswordRules(_row, ++index));
                }
                //return list;

                //this.lbErrorMsg.ForeColor = Color.Red;
                dbuservalidate.PasswordRulesList = PasswordRulesList;
                dbuservalidate.ShowError = true;
            }
        }

        private void ValidateUser(string LoginUsername, string Firstname, string Lastname, string Email, DBUserValidate dbuservalidate)
        {
            this.InitiateVariables();
            dbuservalidate.IsValid = true;
            // Check login name
            if (LoginUsername.Length == 0)
            {
                dbuservalidate.IsValid = false;
                dbuservalidate.ErrorMessage = new MessageBoxDefinition(MessageBoxDefinition.MSG_LOGIN_NAME_EMPTY).ToString();
                return;
            }
            // Check first name.
            if (Firstname.Length == 0)
            {
                dbuservalidate.IsValid = false;
                dbuservalidate.ErrorMessage = new MessageBoxDefinition(MessageBoxDefinition.MSG_FIRST_NAME_EMPTY).ToString();
                return;
            }
            else if (!(Regex.IsMatch(Firstname, DBUserDetailsDS.REGEX_ALPHABETIC, RegexOptions.IgnoreCase)))
            {
                dbuservalidate.IsValid = false;
                dbuservalidate.ErrorMessage = new MessageBoxDefinition(MessageBoxDefinition.MSG_INVALID_FIRST_NAME).ToString();
                return;
            }

            // Check Last name.
            if (Lastname.Length == 0)
            {
                dbuservalidate.IsValid = false;
                dbuservalidate.ErrorMessage = new MessageBoxDefinition(MessageBoxDefinition.MSG_LAST_NAME_EMPTY).ToString();
                return;
            }
            else if (!(Regex.IsMatch(Lastname, DBUserDetailsDS.REGEX_ALPHABETIC, RegexOptions.IgnoreCase)))
            {
                dbuservalidate.IsValid = false;
                dbuservalidate.ErrorMessage = new MessageBoxDefinition(MessageBoxDefinition.MSG_INVALID_LAST_NAME).ToString();
                return;
            }

            // Check Email address.
            if (Email.Length == 0)
            {
                dbuservalidate.IsValid = false;
                dbuservalidate.ErrorMessage = new MessageBoxDefinition(MessageBoxDefinition.MSG_EMAIL_EMPTY).ToString();
                return;
            }
            if (Email.Length != 0)
            {
                if (!(Regex.IsMatch(Email, DBUserDetailsDS.REGEX_EMAIL, RegexOptions.IgnoreCase)))
                {
                    dbuservalidate.IsValid = false;
                    dbuservalidate.ErrorMessage = new MessageBoxDefinition(MessageBoxDefinition.MSG_INVALID_EMAIL).ToString();
                    return;
                }
            }
            dbuservalidate.ShowError = true;
        }

        private void ValidateUser(string LoginUsername, string Password, string ConfirmPassword, DBUserValidate dbuservalidate)
        {
            this.InitiateVariables();
            dbuservalidate.IsValid = true;
            // Check login name
            if (LoginUsername.Length == 0)
            {
                dbuservalidate.IsValid = false;
                dbuservalidate.ErrorMessage = new MessageBoxDefinition(MessageBoxDefinition.MSG_LOGIN_NAME_EMPTY).ToString();
                return;
            }

            if (Password.CompareTo(ConfirmPassword) != 0)
            {
                dbuservalidate.IsValid = false;
                dbuservalidate.ErrorMessage = new MessageBoxDefinition(MessageBoxDefinition.MSG_PASSWORD_NO_MATCH).ToString();
                return;
            }

            // Check Password
            PasswordDS data = new PasswordDS();
            if (!this.pwdSM.CheckRulesForNewUser(LoginUsername, Password, data.Tables[PasswordDS.PasswordRulesTable]))
            {
                dbuservalidate.IsValid = false;

                List<DBUserPasswordRulesSL> PasswordRulesList = new List<DBUserPasswordRulesSL>();
                int index = 0;
                foreach (DataRow _row in data.Tables[PasswordDS.PasswordRulesTable].Rows)
                {
                    PasswordRulesList.Add(ExtractRow_PasswordRules(_row, ++index));
                }
                //return list;

                //this.lbErrorMsg.ForeColor = Color.Red;
                dbuservalidate.PasswordRulesList = PasswordRulesList;
                dbuservalidate.ShowError = true;
            }
        }
    }
}