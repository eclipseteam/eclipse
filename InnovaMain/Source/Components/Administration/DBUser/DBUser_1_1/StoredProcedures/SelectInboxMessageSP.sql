if exists (
	select * from dbo.sysobjects 
	where id = object_id(N'[dbo].[MESSAGE_INBOXUSER_SELECT_1_0_0]') and 
	OBJECTPROPERTY(id, N'IsProcedure') = 1
) 
drop procedure [dbo].[MESSAGE_INBOXUSER_SELECT_1_0_0]
GO

CREATE PROCEDURE [dbo].[MESSAGE_INBOXUSER_SELECT_1_0_0] 
	@ToUserCID uniqueidentifier
AS
SELECT * FROM MESSAGETABLE WHERE ToUserCID = @ToUserCID
GO