if exists (
	select * from dbo.sysobjects 
	where id = object_id(N'[dbo].[SECURITY_USER_UPDATE_1_0_0]') and 
	OBJECTPROPERTY(id, N'IsProcedure') = 1) 
drop procedure [dbo].[SECURITY_USER_UPDATE_1_0_0]
GO

CREATE  PROCEDURE [dbo].[SECURITY_USER_UPDATE_1_0_0] 
				 @CID uniqueidentifier,
				 @AdviserCID uniqueidentifier,
				 @Username varchar(50),
				 @Password varchar(1000),
				 @Firstname varchar(50),
				 @Lastname varchar(50),
				 @Locked int,
				 @Active int,
				 @Administrator int,
				 @FailedAttempts int,
				 @LastLoggedIn datetime,
				 @EmailAddress varchar(50),
				 @ReportingUnitName varchar(50),
				 @ReportingUnitCLID uniqueidentifier,
				 @ScenarioName varchar(50),
				 @ScenarioCSID uniqueidentifier,
				 @PeriodName varchar(50),
				 @PeriodCLID uniqueidentifier,
				 @DisplayZeroPreference bit,
				 @OrganisationCID uniqueidentifier,
				 @AdministratorType int,
				 @UserType int,
			     @OrganisationName varchar(100) AS 
				 
				 if( Len( @Password ) > 0 )
					begin	
						UPDATE UserDetail SET AdviserCID=@AdviserCID,Username=@Username, Password = @Password, Firstname = @Firstname, Lastname = @Lastname,	Active = @Active, Administrator = @Administrator, Locked = @Locked, FailedAttempts = @FailedAttempts, LastLoggedIn = @LastLoggedIn, EmailAddress = @EmailAddress, ReportingUnitName=@ReportingUnitName, ReportingUnitCLID=@ReportingUnitCLID, ScenarioName=@ScenarioName, ScenarioCSID=@ScenarioCSID, PeriodName=@PeriodName, PeriodCLID=@PeriodCLID, DisplayZeroPreference=@DisplayZeroPreference, OrganisationCID=@OrganisationCID, AdministratorType=@AdministratorType, UserType=@UserType, OrganisationName=@OrganisationName 
						WHERE CID = @CID 
					end  
				 else  
					begin
					 	UPDATE UserDetail SET AdviserCID=@AdviserCID, Username=@Username, Firstname = @Firstname, Lastname = @Lastname,	Active = @Active, Administrator = @Administrator, Locked = @Locked, FailedAttempts = @FailedAttempts, LastLoggedIn = @LastLoggedIn, EmailAddress = @EmailAddress, ReportingUnitName=@ReportingUnitName, ReportingUnitCLID=@ReportingUnitCLID, ScenarioName=@ScenarioName, ScenarioCSID=@ScenarioCSID, PeriodName=@PeriodName, PeriodCLID=@PeriodCLID, DisplayZeroPreference=@DisplayZeroPreference, OrganisationCID=@OrganisationCID, AdministratorType=@AdministratorType, UserType=@UserType, OrganisationName=@OrganisationName
					 	WHERE CID = @CID 
					end
GO
