if not exists (
	select * from dbo.sysobjects where 
	id = object_id(N'[dbo].[UserDetail]') and OBJECTPROPERTY(id, N'IsUserTable') = 1
)
CREATE TABLE [dbo].[UserDetail](
	[CID] [uniqueidentifier] NULL,
	[Username] [varchar](50) NOT NULL,
	[Password] [varchar](1000) NOT NULL,
	[Active] [char](1) NOT NULL,
	[LastLoggedIn] [datetime] NULL,
	[EmailAddress] [varchar](50) NULL,
	[FailedAttempts] [int] NOT NULL,
	[Locked] [char](1) NOT NULL,
	[Administrator] [char](1) NOT NULL,
	[Firstname] [varchar](50) NOT NULL,
	[Lastname] [varchar](50) NOT NULL,
	[PRIVTEMPLATECID] [uniqueidentifier] NULL,
	[ReportingUnitName] [varchar](50) NULL,
	[ReportingUnitCLID] [uniqueidentifier] NULL,
	[ScenarioName] [varchar](50) NULL,
	[ScenarioCSID] [uniqueidentifier] NULL,
	[PeriodName] [varchar](50) NULL,
	[PeriodCLID] [uniqueidentifier] NULL,
	[DisplayZeroPreference] [bit] NOT NULL,
	[OrganisationCID] [uniqueidentifier] NULL,
	[AdministratorType] [int] NOT NULL,
	[OrganisationName] [varchar](100) NULL,
	[UserType] [int] NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO



