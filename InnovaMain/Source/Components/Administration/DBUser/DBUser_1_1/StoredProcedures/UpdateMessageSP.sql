if exists (
	select * from dbo.sysobjects 
	where id = object_id(N'[dbo].[MESSAGE_USER_UPDATE_1_0_0]') and 
	OBJECTPROPERTY(id, N'IsProcedure') = 1) 
drop procedure [dbo].[MESSAGE_USER_UPDATE_1_0_0]
GO

CREATE  PROCEDURE [dbo].[MESSAGE_USER_UPDATE_1_0_0] 
				@MessageID uniqueidentifier,
				@FromCID uniqueidentifier,
				@ToUserCID uniqueidentifier,
				@Message nvarchar(400),
				@MessageTitle nvarchar(50),
				@CreateDate datetime,
				@State tinyint,
				@MessageSourceType int,
				@AttachmentBMCID uniqueidentifier
				AS
				  if( @State = 0 )
					begin	
						UPDATE MESSAGETABLE SET FromCID=@FromCID, ToUserCID = @ToUserCID, Message = @Message, MessageTitle = @MessageTitle, CreateDate = @CreateDate, State = @State, MessageSourceType = @MessageSourceType, AttachmentBMCID = @AttachmentBMCID
						WHERE MessageID = @MessageID 
				end  
				GO
