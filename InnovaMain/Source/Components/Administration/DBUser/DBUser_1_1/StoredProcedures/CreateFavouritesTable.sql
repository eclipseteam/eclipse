-------------------------------------
-- Create "FavouriteWorkpapers" table

if not exists (
	select * 
	from dbo.sysobjects 
	where id = object_id(N'[dbo].[FavouriteWorkpapers]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)

CREATE TABLE [dbo].[FavouriteWorkpapers] (
	[ID] [uniqueidentifier] NOT NULL ,
	[UserId] [uniqueidentifier] NOT NULL ,
	[WorkpaperName] [varchar] (80) COLLATE Latin1_General_CI_AS NOT NULL ,
	[WorkpaperAddress] [varchar] (355) COLLATE Latin1_General_CI_AS NOT NULL ,
	[Folder] [uniqueidentifier] NOT NULL ,
	[BreadCrumb] [varchar] (355) COLLATE Latin1_General_CI_AS NULL ,
	CONSTRAINT [PK_FavouriteWorkpapers] PRIMARY KEY  CLUSTERED 
	(
		[ID]
	)  ON [PRIMARY] 
) ON [PRIMARY]
GO

-------------------------------------
-- Create "FavouritesFolder" Table

if not exists (
	select * 
	from dbo.sysobjects 
	where id = object_id(N'[FavouritesFolder]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)

CREATE TABLE [dbo].[FavouritesFolder] (
	[ID] [uniqueidentifier] NOT NULL ,
	[UserId] [uniqueidentifier] NOT NULL ,
	[ParentFolder] [uniqueidentifier] NULL ,
	[Name] [varchar] (50) COLLATE Latin1_General_CI_AS NULL ,
	[IsRoot] [bit] NOT NULL,
	CONSTRAINT [PK_FavouritesFolder] PRIMARY KEY  CLUSTERED 
	(
		[ID]
	)  ON [PRIMARY] 
) ON [PRIMARY]
GO

-------------------------------------
-- Create "DbUser_FavouriteWorkpapers" stored procedure

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[DbUser_FavouritesWorkpapers]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[DbUser_FavouritesWorkpapers]
GO

CREATE PROCEDURE [dbo].[DbUser_FavouritesWorkpapers] 
	@userId uniqueidentifier
as
  select [ID], [UserId], [Folder], [WorkpaperName], [WorkpaperAddress], [BreadCrumb] 
  from [dbo].[FavouriteWorkpapers]
  where     (UserId = @userId)

GO

-------------------------------------
-- Create "DbUser_AddFavourite" stored procedure

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[DbUser_AddFavourite]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[DbUser_AddFavourite]
GO

create procedure [dbo].[DbUser_AddFavourite] 
	@id 			uniqueidentifier, 
	@userid			uniqueidentifier,
	@folder			uniqueidentifier,
	@workpapername 		varchar(80),
	@workpaperaddress	varchar(355),
	@breadCrumb	varchar(355)
as
  insert into [dbo].[FavouriteWorkpapers]([ID], [UserId], [Folder], [WorkpaperName], 
	[WorkpaperAddress],[BreadCrumb])
  values(@id, @userid, @folder, @workpapername, @workpaperaddress,@breadCrumb)

GO

-------------------------------------
-- Create "DBUser_VisitedWorkpapers" View

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[DBUser_VisitedWorkpapers]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[DBUser_VisitedWorkpapers]
GO

CREATE VIEW [dbo].[DBUser_VisitedWorkpapers]
AS
SELECT     TOP 100 PERCENT EVENTID, DATETIME, USERNAME, NAME, TYPE, DETAIL
FROM         dbo.EVENTLOG
WHERE     (NAME = 'Workpaper visited') AND (NOT (DETAIL LIKE 'Reporting Units%') AND NOT (DETAIL LIKE 'Logout%'))
ORDER BY DATETIME DESC

GO

-------------------------------------
-- Create "DbUser_UserVisitedWorkpapers" Stored procedure

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[DbUser_UserVisitedWorkpapers]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[DbUser_UserVisitedWorkpapers]
GO

CREATE PROCEDURE [dbo].[DbUser_UserVisitedWorkpapers] 
	@username  varchar(50)
AS
  select *
  from [dbo].[DBUser_VisitedWorkpapers]
  where USERNAME = @username

GO

-------------------------------------
-- Create "DBUser_AddFavouriteFolder" Stored procedure

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[DBUser_AddFavouriteFolder]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[DBUser_AddFavouriteFolder]
GO

create procedure [dbo].[DBUser_AddFavouriteFolder]
	@id		uniqueidentifier, 
	@userid			uniqueidentifier,
	@parentFolder 	uniqueidentifier,
	@name		varchar(50),
	@isRoot		bit
as
  insert into [dbo].[FavouritesFolder]([ID], [UserId], [ParentFolder], [Name], [IsRoot])
  values(@id, @userid, @parentFolder, @name, @isRoot)
GO

-------------------------------------
-- Create "DBUser_FavouritesFolder" Stored procedure

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[DBUser_FavouritesFolder]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[DBUser_FavouritesFolder]
GO

create procedure [dbo].[DBUser_FavouritesFolder] 
as
  select [ID], [UserId], [ParentFolder], [Name], [IsRoot] 
  from [dbo].[FavouritesFolder]
GO

-------------------------------------
-- Create "DbUser_Favourites" stored procedure

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[DbUser_Favourites]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[DbUser_Favourites]
GO

CREATE PROCEDURE [dbo].[DbUser_Favourites] 
	@userId uniqueidentifier
as
  select [ID], [UserId], [Folder], [WorkpaperName], [WorkpaperAddress], [BreadCrumb]
  from [dbo].[FavouriteWorkpapers]
  where     (UserId = @userId)

  select [ID], [UserId], [ParentFolder], [Name], [IsRoot] 
  from [dbo].[FavouritesFolder]
  where     (UserId = @userId)
  
GO
