if exists (
	select * from dbo.sysobjects where 
	id = object_id(N'[dbo].[SECURITY_USER_INSERT_1_0_0]') and 
	OBJECTPROPERTY(id, N'IsProcedure') = 1) 
drop procedure [dbo].[SECURITY_USER_INSERT_1_0_0] 
GO

CREATE PROCEDURE [dbo].[SECURITY_USER_INSERT_1_0_0]
				@CID uniqueidentifier,
				@Username varchar(50),
				@Password varchar(1000),
				@Firstname varchar(50),
				@Lastname varchar(50),
				@Locked int,
				@Active int,
				@Administrator int,
				@FailedAttempts int,
				@LastLoggedIn datetime,
				@EmailAddress varchar(50),
				@ReportingUnitName varchar(50),
				@ReportingUnitCLID uniqueidentifier,
				@ScenarioName varchar(50),
				@ScenarioCSID uniqueidentifier,
				@PeriodName varchar(50),
				@PeriodCLID uniqueidentifier,
				@DisplayZeroPreference bit,
				@OrganisationCID uniqueidentifier,
				@AdministratorType int,
				@UserType int,
				@OrganisationName varchar(100),
				@AdviserCID uniqueidentifier
				AS
				INSERT INTO UserDetail (
					CID, Username, Password, Firstname, Lastname, Active, Administrator, Locked, 
					FailedAttempts, LastLoggedIn, EmailAddress, ReportingUnitName,  ReportingUnitCLID, 
					ScenarioName, ScenarioCSID, PeriodName, PeriodCLID, DisplayZeroPreference,
					OrganisationCID, AdministratorType, UserType, OrganisationName, AdviserCID)
				VALUES (
					@CID, @Username, @Password, @Firstname, @Lastname, @Active, @Administrator, @Locked, 
					0, null, @EmailAddress, @ReportingUnitName,  @ReportingUnitCLID, @ScenarioName, 
					@ScenarioCSID, @PeriodName, @PeriodCLID, @DisplayZeroPreference,
					@OrganisationCID, @AdministratorType, @UserType, @OrganisationName, @AdviserCID)
GO