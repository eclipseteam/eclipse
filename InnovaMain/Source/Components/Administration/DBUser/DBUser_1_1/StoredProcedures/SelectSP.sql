if exists (
	select * from dbo.sysobjects 
	where id = object_id(N'[dbo].[SECURITY_USER_SELECT_1_0_0]') and 
	OBJECTPROPERTY(id, N'IsProcedure') = 1
) 
drop procedure [dbo].[SECURITY_USER_SELECT_1_0_0]
GO

CREATE PROCEDURE [dbo].[SECURITY_USER_SELECT_1_0_0] 
	@CID uniqueidentifier
AS
SELECT * FROM UserDetail WHERE CID = @CID
GO