if exists (
	select * from dbo.sysobjects where 
	id = object_id(N'[dbo].[MESSAGE_USER_INSERT_1_0_0]') and 
	OBJECTPROPERTY(id, N'IsProcedure') = 1) 
drop procedure [dbo].[MESSAGE_USER_INSERT_1_0_0] 
GO

CREATE PROCEDURE [dbo].[MESSAGE_USER_INSERT_1_0_0]
				@MessageID uniqueidentifier,
				@FromCID uniqueidentifier,
				@ToUserCID uniqueidentifier,
				@Message nvarchar(400),
				@MessageTitle nvarchar(50),
				@CreateDate datetime,
				@State tinyint,
				@MessageSourceType int,
				@AttachmentBMCID uniqueidentifier
				AS
				INSERT INTO MESSAGETABLE (
					MessageID, FromCID, ToUserCID, Message, MessageTitle, CreateDate, State, MessageSourceType, 
					AttachmentBMCID)
				VALUES (
					@MessageID, @FromCID, @ToUserCID, @Message, @MessageTitle, @CreateDate, @State, @MessageSourceType, 
					@AttachmentBMCID)
GO