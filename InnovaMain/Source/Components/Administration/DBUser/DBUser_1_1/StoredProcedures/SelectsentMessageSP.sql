if exists (
	select * from dbo.sysobjects 
	where id = object_id(N'[dbo].[MESSAGE_SENTUSER_SELECT_1_0_0]') and 
	OBJECTPROPERTY(id, N'IsProcedure') = 1
) 
drop procedure [dbo].[MESSAGE_SENTUSER_SELECT_1_0_0]
GO

CREATE PROCEDURE [dbo].[MESSAGE_SENTUSER_SELECT_1_0_0] 
	@FromCID uniqueidentifier
AS
SELECT * FROM MESSAGETABLE WHERE FromCID = @FromCID
GO