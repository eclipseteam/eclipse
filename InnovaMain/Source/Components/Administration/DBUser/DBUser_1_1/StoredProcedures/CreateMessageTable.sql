/****** Object:  Table [dbo].[MessageTable]    Script Date: 06/26/2013 17:12:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MESSAGETABLE](
	[MessageID] [uniqueidentifier] NOT NULL,
	[FromCID] [uniqueidentifier] NULL,
	[ToUserCID] [uniqueidentifier] NULL,
	[Message] [nvarchar](400) NULL,
	[MessageTitle] [nvarchar](50) NULL,
	[CreateDate] [datetime] NOT NULL,
	[State] [tinyint] NULL,
	[MessageSourceType] [int] NOT NULL,
	[AttachmentBMCID] [uniqueidentifier] NULL
) ON [PRIMARY]

GO


