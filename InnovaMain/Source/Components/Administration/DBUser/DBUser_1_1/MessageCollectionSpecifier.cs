﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.CalculationInterface;

namespace Oritax.TaxSimp.Security
{
    public class FromUserMessageCollectionSpecifier : CollectionSpecifier
    {
        private Guid fromUserID = Guid.Empty;

        public Guid FromUserID
        {
            get { return fromUserID; }
            set { fromUserID = value; }
        }

        public FromUserMessageCollectionSpecifier(Guid fromUserID) : base(new Guid(DBUserInstall.ASSEMBLY_ID))
		{
            this.FromUserID = fromUserID;
		}
    }
}
