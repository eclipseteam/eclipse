using System;

namespace Oritax.TaxSimp.Security
{
	/// <summary>
	/// Summary description for Install.
	/// </summary>
	public class DBUserInstall
	{
		#region INSTALLATION PROPERTIES

		// Assembly Installation Properties
		public const string ASSEMBLY_ID="3D98A779-ADF0-4BF5-940D-220EE57ACBD9";
		public const string ASSEMBLY_NAME="DBUser_1_1";
		public const string ASSEMBLY_DISPLAYNAME="DB User V1.1";
		public const string ASSEMBLY_MAJORVERSION="1";
		public const string ASSEMBLY_MINORVERSION="1";
		public const string ASSEMBLY_DATAFORMAT="0";
		//public const string ASSEMBLY_REVISION="0";	//2004.3.0
		//public const string ASSEMBLY_REVISION="1";	//2004.3.2
		public const string ASSEMBLY_REVISION="2";		//2005.1

		// Component Installation Properties
		public const string COMPONENT_ID="19E6F85E-17D3-43a2-91B6-9EBBB0CB3F75";
		public const string COMPONENT_NAME="DBUser";
		public const string COMPONENT_DISPLAYNAME="DB User";
		public const string COMPONENT_CATEGORY="Security";

		// Component Version Installation Properties
		public const string COMPONENTVERSION_STARTDATE="01/01/1990 00:00:00 AM";
		public const string COMPONENTVERSION_ENDDATE="31/12/2050 12:59:59 AM";
		public const string COMPONENTVERSION_PERSISTERASSEMBLY="DBUser_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";
		public const string COMPONENTVERSION_PERSISTERCLASS="Oritax.TaxSimp.Security.UserPersister";
		public const string COMPONENTVERSION_IMPLEMENTATIONCLASS="Oritax.TaxSimp.Security.DBUser";

		
		//		Others
		public const string	ASSOCIATEDFILE_OTHER_GLOBALASAX_FILENAME="Global.asax";
		public const string	ASSOCIATEDFILE_OTHER_WEBCONFIG_FILENAME="Web.config";

		// Data Model
		public const string ASSEMBLY_PERSISTERASSEMBLY="DBUser_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";
		public const string ASSEMBLY_PERSISTERCLASS="Oritax.TaxSimp.Security.UserPersister";

		#endregion
	}
}
