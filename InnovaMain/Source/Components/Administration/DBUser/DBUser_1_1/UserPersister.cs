using System;
using System.Configuration;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Calculation;

using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.Security
{
    /// <summary>
    /// Summary description for UserPersister.
    /// </summary>
    public class UserPersister : BrokerManagedComponentPersister, ICPersister, IDisposable
    {
        public const String USER_INSERT_SP = "SECURITY_USER_INSERT_1_0_0";
        public const String USER_UPDATE_SP = "SECURITY_USER_UPDATE_1_0_0";
        public const String USER_SELECT_SP = "SECURITY_USER_SELECT_1_0_0";
        public const String USER_SELECTALL_SP = "SECURITY_USER_SELECTALL_1_0_0";

        public const String SECURITYGROUP_SELECT_SP = "SECURITY_SG_SELECT_1_0_0";

        private SqlDataAdapter sdaUser;
        private SqlDataAdapter sdaGroup;

        public void CommitTransaction() { }
        public void RollbackTransaction() { }
        public void StartTransaction() { }
        private bool disposed = false;

        #region IDisposable Implementation ---------------------------------------

        /// <summary>
        ///		Performs application-defined tasks associated with freeing, releasing, 
        ///		or resetting unmanaged resources.
        /// </summary>
        public new void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        ///		Performs application-defined tasks associated with freeing, releasing, 
        ///		or resetting unmanaged resources.
        /// </summary>
        /// <param name="disposing">
        ///		True if called by the Dispose() method, false if called by the finalizer.
        /// </param>
        protected void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    base.Dispose();
                    sdaUser.Dispose();
                    sdaGroup.Dispose();
                }
                disposed = true;
            }
        }

        #endregion // IDisposable Implementation

        public SqlDataAdapter DataAdapter
        {
            get { return (sdaUser); }
        }

        private void EstablishUserParameters(SqlCommand scUser)
        {
            if (scUser == null)
            {
                throw new ArgumentNullException("scUser SqlCommand object is null");
            }

            scUser.CommandType = CommandType.StoredProcedure;

            scUser.Parameters.Add("@CID", SqlDbType.UniqueIdentifier, 16, DBUserDetailsDS.USER_USERID_FIELD);
            scUser.Parameters.Add("@ADVISERCID", SqlDbType.UniqueIdentifier, 16, DBUserDetailsDS.ADVISERCID);
            scUser.Parameters.Add("@Username", SqlDbType.VarChar, 50, DBUserDetailsDS.USER_USERNAME_FIELD);
            scUser.Parameters.Add("@Password", SqlDbType.VarChar, 1000, DBUserDetailsDS.USER_PASSWORD_FIELD);
            scUser.Parameters.Add("@Firstname", SqlDbType.VarChar, 50, DBUserDetailsDS.USER_FIRSTNAME_FIELD);
            scUser.Parameters.Add("@Lastname", SqlDbType.VarChar, 50, DBUserDetailsDS.USER_LASTNAME_FIELD);
            scUser.Parameters.Add("@Locked", SqlDbType.Int, 4, DBUserDetailsDS.USER_LOCKED_FIELD);
            scUser.Parameters.Add("@Active", SqlDbType.Int, 4, DBUserDetailsDS.USER_ACTIVE_FIELD);
            scUser.Parameters.Add("@Administrator", SqlDbType.Int, 4, DBUserDetailsDS.USER_ADMINISTRATOR_FIELD);
            scUser.Parameters.Add("@FailedAttempts", SqlDbType.Int, 4, DBUserDetailsDS.USER_FAILEDATTEMPTS_FIELD);
            scUser.Parameters.Add("@LastLoggedIn", SqlDbType.DateTime, 8, DBUserDetailsDS.USER_LASTLOGGEDIN_FIELD);
            scUser.Parameters.Add("@EmailAddress", SqlDbType.VarChar, 50, DBUserDetailsDS.USER_EMAILADDRESS_FIELD);

            scUser.Parameters.Add("@ReportingUnitName", SqlDbType.VarChar, 50, DBUserDetailsDS.USER_REPORTINGUNITNAME_FIELD);
            scUser.Parameters.Add("@ReportingUnitCLID", SqlDbType.UniqueIdentifier, 16, DBUserDetailsDS.USER_REPORTINGUNITCLID_FIELD);

            scUser.Parameters.Add("@ScenarioName", SqlDbType.VarChar, 50, DBUserDetailsDS.USER_SCENARIONAME_FIELD);
            scUser.Parameters.Add("@ScenarioCSID", SqlDbType.UniqueIdentifier, 16, DBUserDetailsDS.USER_SCENARIOCSID_FIELD);

            scUser.Parameters.Add("@PeriodName", SqlDbType.VarChar, 50, DBUserDetailsDS.USER_PERIODNAME_FIELD);
            scUser.Parameters.Add("@PeriodCLID", SqlDbType.UniqueIdentifier, 16, DBUserDetailsDS.USER_PERIODCLID_FIELD);

            scUser.Parameters.Add("@DisplayZeroPreference", SqlDbType.Bit, 1, DBUserDetailsDS.USER_DISPLAY_ZERO_PREFERENCE_FIELD);

            scUser.Parameters.Add("@OrganisationCID", SqlDbType.UniqueIdentifier, 16, DBUserDetailsDS.USER_ORGANISATION_CID_FIELD);
            scUser.Parameters.Add("@AdministratorType", SqlDbType.Int, 4, DBUserDetailsDS.USER_ADMINISTRATOR_TYPE_FIELD);
            scUser.Parameters.Add("@UserType", SqlDbType.Int, 4, DBUserDetailsDS.USER_USER_TYPE_FIELD);
            scUser.Parameters.Add("@OrganisationName", SqlDbType.VarChar, 100, DBUserDetailsDS.USER_ORGANISATION_NAME_FIELD);
        }

        private void EstablishSGParameters(SqlCommand scGroup)
        {
            scGroup.CommandType = CommandType.StoredProcedure;

            scGroup.Parameters.Add("@Username", SqlDbType.VarChar, 50, DBUserDetailsDS.USER_USERNAME_FIELD);
            scGroup.Parameters.Add("@SecurityGroupName", SqlDbType.VarChar, 50, DBUserDetailsDS.SECURITYGROUP_NAME_FIELD);
        }

        public UserPersister(SqlConnection connection, SqlTransaction transaction)
            : base(connection, transaction)
        {
            sdaUser = new SqlDataAdapter();
            sdaGroup = new SqlDataAdapter();

            sdaUser.InsertCommand = DBCommandFactory.Instance.NewSqlCommand(USER_INSERT_SP);
            sdaUser.InsertCommand.Connection = this.Connection;
            sdaUser.InsertCommand.Transaction = this.Transaction;
            EstablishUserParameters(sdaUser.InsertCommand);

            sdaUser.UpdateCommand = DBCommandFactory.Instance.NewSqlCommand(USER_UPDATE_SP);
            sdaUser.UpdateCommand.Connection = this.Connection;
            sdaUser.UpdateCommand.Transaction = this.Transaction;
            EstablishUserParameters(sdaUser.UpdateCommand);

            sdaUser.DeleteCommand = DBCommandFactory.Instance.NewSqlCommand();
            sdaUser.DeleteCommand.CommandText = "DELETE USERDETAIL WHERE USERDETAIL.CID=@CID";
            sdaUser.DeleteCommand.Connection = this.Connection;
            sdaUser.DeleteCommand.Transaction = this.Transaction;
            sdaUser.DeleteCommand.CommandType = CommandType.Text;
            sdaUser.DeleteCommand.Parameters.Add("@CID", SqlDbType.UniqueIdentifier, 16, DBUserDetailsDS.USER_USERID_FIELD);


            sdaUser.SelectCommand = DBCommandFactory.Instance.NewSqlCommand(USER_SELECT_SP);
            sdaUser.SelectCommand.Connection = this.Connection;
            sdaUser.SelectCommand.Transaction = this.Transaction;
            sdaUser.SelectCommand.CommandType = CommandType.StoredProcedure;
            sdaUser.SelectCommand.Parameters.Add("@CID", SqlDbType.UniqueIdentifier, 16, DBUserDetailsDS.USER_USERID_FIELD);

            sdaGroup.SelectCommand = DBCommandFactory.Instance.NewSqlCommand(SECURITYGROUP_SELECT_SP);
            sdaGroup.SelectCommand.Connection = this.Connection;
            sdaGroup.SelectCommand.Transaction = this.Transaction;
            sdaGroup.SelectCommand.CommandType = CommandType.StoredProcedure;
            sdaGroup.SelectCommand.Parameters.Add("@CID", SqlDbType.UniqueIdentifier, 16, DBUserDetailsDS.SECURITYGROUP_USERID_FIELD);

        }

        public DataSet Update(DataSet data)
        {
            if (data is DBUserDetailsDS)
            {
                sdaUser.Update(data.Tables[DBUserDetailsDS.USER_DETAIL_TABLE]);
            }
            return (data);
        }
        public override void FindDependentBMCs(Guid cID, ref ArrayList dependentCIDs, PersisterActionType actionType)
        {
            base.FindDependentBMCs(cID, ref dependentCIDs, actionType);

            //			//If a user is being deleted then we need to invalidate all BMC's which 
            //			//contain this user in their ACL collections.
            //			if(actionType == PersisterActionType.Delete)
            //			{
            //				SqlCommand loadCommand=this.sqlConnection.CreateCommand();
            //				loadCommand.Transaction=this.sqlTransaction;
            //
            //				// Special case dependency SQL for this type of BMC.
            //				loadCommand.CommandText="SELECT BMCCID FROM "
            //					+"ACLENTRY "
            //					+"WHERE PARTYCID='{"+cID.ToString()+"}' ";
            //				
            //				SqlDataReader loadReader=loadCommand.ExecuteReader();
            //
            //				while(loadReader.Read())
            //				{
            //					dependentCIDs.Add(loadReader.GetGuid(0));
            //				}
            //
            //				loadReader.Close();
            //
            //				//For logical modules add the ID of the Logical being persisted to the invalid array
            //				//so that it will be rehydrated containing the refreshed read only data.
            //				if(!dependentCIDs.Contains(cID))
            //					dependentCIDs.Add(cID);
            //			}
        }

        public override IBrokerManagedComponent Get(Guid instanceId, ref DataSet primaryDS)
        {
            DBUser objUser = null;
            try
            {
                if (instanceId != Guid.Empty)
                {
                    objUser = new DBUser();
                    objUser.CID = instanceId;

                    DBUserDetailsDS objUserDS = new DBUserDetailsDS();
                    DBSecurityGroupDetailsDS objSecurityGroupDS = new DBSecurityGroupDetailsDS();

                    sdaUser.SelectCommand.Parameters["@CID"].Value = instanceId;
                    sdaUser.Fill(objUserDS.Tables[DBUserDetailsDS.USER_DETAIL_TABLE]);
                    objUserDS.AcceptChanges();

                    sdaGroup.SelectCommand.Parameters["@CID"].Value = instanceId;
                    sdaGroup.Fill(objSecurityGroupDS.Tables[DBSecurityGroupDetailsDS.SECGRP_DETAIL_TABLE]);
                    objSecurityGroupDS.AcceptChanges();

                    base.Hydrate(objUser, instanceId, new BrokerManagedComponentDS(), ref primaryDS);

                    objUser.DeliverData(objUserDS);
                    objUser.DeliverData(objSecurityGroupDS);
                }
            }
            catch (Exception e)
            {
                throw new Exception("DBUser.UserPersister.GetByInstanceID : " + e.Message);
            }
            return ((IBrokerManagedComponent)objUser);
        }

        public override void Save(IBrokerManagedComponent iBMC, ref DataSet persistedDS)
        {
            try
            {
                base.Save(iBMC, new BrokerManagedComponentDS(), ref persistedDS);

                DBUserDetailsDS objUserTargetDS = new DBUserDetailsDS();
                sdaUser.SelectCommand.Parameters["@CID"].Value = iBMC.CID;
                sdaUser.Fill(objUserTargetDS.Tables[DBUserDetailsDS.USER_DETAIL_TABLE]);

                DBUserDetailsDS objUserSourceDS = new DBUserDetailsDS();
                iBMC.GetData(objUserSourceDS);

                objUserTargetDS.Merge(objUserSourceDS, false, MissingSchemaAction.Ignore);
                objUserTargetDS.Tables[DBUserDetailsDS.USER_DETAIL_TABLE].Columns.Remove(DBUserDetailsDS.IFANAME);
                objUserTargetDS.Tables[DBUserDetailsDS.USER_DETAIL_TABLE].Columns.Remove(DBUserDetailsDS.ADVISERID);
                objUserTargetDS.Tables[DBUserDetailsDS.USER_DETAIL_TABLE].Columns.Remove(DBUserDetailsDS.IFACID);
                objUserTargetDS.Tables[DBUserDetailsDS.USER_DETAIL_TABLE].Columns.Remove(DBUserDetailsDS.USER_USER_TYPE_ENUM_FIELD);
                objUserTargetDS.Tables[DBUserDetailsDS.USER_DETAIL_TABLE].Columns.Remove(DBUserDetailsDS.USER_PRIVTEMPLATECID_FIELD);
                sdaUser.Update(objUserTargetDS, DBUserDetailsDS.USER_DETAIL_TABLE);
            }
            catch (Exception e)
            {
                throw new Exception("DBUser.UserPersister : " + e.Message);
            }
        }

        public override void Delete(IBrokerManagedComponent iBMC)
        {
            base.Delete(iBMC);

            DBUserDetailsDS objUserTargetDS = new DBUserDetailsDS();

            sdaUser.SelectCommand.Parameters["@CID"].Value = iBMC.CID;
            sdaUser.Fill(objUserTargetDS.Tables[DBUserDetailsDS.USER_DETAIL_TABLE]);

            foreach (DataRow row in objUserTargetDS.Tables[DBUserDetailsDS.USER_DETAIL_TABLE].Rows)
                row.Delete();

            sdaUser.Update(objUserTargetDS.Tables[DBUserDetailsDS.USER_DETAIL_TABLE]);
        }

        public override DataSet FindDetailsByTypeId(Guid typeId, object[] parameters)
        {
            SqlDataAdapter sdaUserDetails = new SqlDataAdapter();
            sdaUserDetails.SelectCommand = DBCommandFactory.Instance.NewSqlCommand(USER_SELECTALL_SP);
            sdaUserDetails.SelectCommand.Connection = sdaUser.SelectCommand.Connection;
            sdaUserDetails.SelectCommand.Transaction = sdaUser.SelectCommand.Transaction;
            sdaUserDetails.SelectCommand.CommandType = CommandType.StoredProcedure;
            sdaUserDetails.SelectCommand.Parameters.AddWithValue("@CTID", typeId);

            DBUserDetailsDS objUserDS = new DBUserDetailsDS();
            sdaUserDetails.Fill(objUserDS.Tables[DBUserDetailsDS.USER_DETAIL_TABLE]);
            return objUserDS;
        }

        protected virtual DataTable ListBMCCollection(FromUserMessageCollectionSpecifier fromUserMessageCollectionSpecifier)
        {
            SqlDataAdapter collectionAdapter = new SqlDataAdapter();
            collectionAdapter.SelectCommand = DBCommandFactory.Instance.NewSqlCommand();
            collectionAdapter.SelectCommand.Connection = this.Connection;
            collectionAdapter.SelectCommand.Transaction = this.Transaction;
            collectionAdapter.SelectCommand.Parameters.AddWithValue("@ID", fromUserMessageCollectionSpecifier.FromUserID);
            collectionAdapter.SelectCommand.CommandText = "select * from " + DBUserDetailsDS.MESSAGE_TABLE + " where "+DBUserDetailsDS.MESSAGE_ID+" = @ID" ;

            DBUserDetailsDS ds = new DBUserDetailsDS();
            collectionAdapter.Fill(ds.Tables[DBUserDetailsDS.MESSAGE_TABLE]);
            return ds.Tables[DBUserDetailsDS.MESSAGE_TABLE];
        }

        public override SqlConnection Connection
        {
            set
            {
                base.Connection = value;
                sdaUser.InsertCommand.Connection = value;
                sdaUser.SelectCommand.Connection = value;
                sdaUser.UpdateCommand.Connection = value;
                sdaUser.DeleteCommand.Connection = value;

                sdaGroup.SelectCommand.Connection = value;
            }
        }

        public override SqlTransaction Transaction
        {
            set
            {
                base.Transaction = value;
                sdaUser.InsertCommand.Transaction = value;
                sdaUser.SelectCommand.Transaction = value;
                sdaUser.UpdateCommand.Transaction = value;
                sdaUser.DeleteCommand.Transaction = value;

                sdaGroup.SelectCommand.Transaction = value;
            }
        }

        public override void EstablishDatabase()
        {
            DatabaseUtilities.ExecuteSqlResource(
                this.GetType(),
                "Oritax.TaxSimp.Security.StoredProcedures.CreateTable.sql",
                this.Connection, this.Transaction);

            DatabaseUtilities.ExecuteSqlResource(
                this.GetType(),
                "Oritax.TaxSimp.Security.StoredProcedures.CreateFavouritesTable.sql",
                this.Connection, this.Transaction);

            DatabaseUtilities.ExecuteSqlResource(
                this.GetType(),
                "Oritax.TaxSimp.Security.StoredProcedures.InsertSP.sql",
                this.Connection, this.Transaction);

            DatabaseUtilities.ExecuteSqlResource(
                this.GetType(),
                "Oritax.TaxSimp.Security.StoredProcedures.SelectAllSP.sql",
                this.Connection, this.Transaction);

            DatabaseUtilities.ExecuteSqlResource(
                this.GetType(),
                "Oritax.TaxSimp.Security.StoredProcedures.SelectSP.sql",
                this.Connection, this.Transaction);

            DatabaseUtilities.ExecuteSqlResource(
                this.GetType(),
                "Oritax.TaxSimp.Security.StoredProcedures.UpdateSP.sql",
                this.Connection, this.Transaction);

            DatabaseUtilities.ExecuteSqlResource(
               this.GetType(),
               "Oritax.TaxSimp.Security.StoredProcedures.InsertMessageSP.sql",
               this.Connection, this.Transaction);

            DatabaseUtilities.ExecuteSqlResource(
               this.GetType(),
               "Oritax.TaxSimp.Security.StoredProcedures.SelectInboxMessageSP.sql",
               this.Connection, this.Transaction);

            DatabaseUtilities.ExecuteSqlResource(
               this.GetType(),
               "Oritax.TaxSimp.Security.StoredProcedures.SelectsentMessageSP.sql",
               this.Connection, this.Transaction);

            DatabaseUtilities.ExecuteSqlResource(
             this.GetType(),
             "Oritax.TaxSimp.Security.StoredProcedures.UpdateMessageSP.sql",
             this.Connection, this.Transaction);
        }

        public override void ClearDatabase()
        {
            SqlCommand adminCommand = DBCommandFactory.Instance.NewSqlCommand();
            adminCommand.Connection = this.Connection;
            adminCommand.Transaction = this.Transaction;
            adminCommand.CommandText = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[UserDetail]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)"
                + "DELETE UserDetail";
            adminCommand.ExecuteNonQuery();
        }

        public override void RemoveDatabase()
        {

        }

    }
}
