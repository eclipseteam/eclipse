using System;
using System.Xml;
using System.Web;
using System.Text;
using System.Collections.Specialized;
using System.Reflection;
using System.Diagnostics;
using System.IO;

namespace Oritax.TaxSimp.Utilities
{
	/// <summary>
	/// Collects information about an error and returns an XML document with the information
	/// </summary>
	public class ErrorCollector
	{
		protected ErrorCollector()
		{
		}

		public static void WriteErrorAsXML( HttpContext objContext, Exception objException, string strFileName )
		{
			XmlTextWriter objWriter = new XmlTextWriter( strFileName, Encoding.UTF8 );

			try
			{
				objWriter.Formatting = Formatting.Indented;
				objWriter.WriteStartDocument( );
				objWriter.WriteStartElement( "ErrorLog" );

				WriteExceptionInformation( objWriter, objException );
				WriteRequestInformation( objWriter, objContext.Request );
				WriteMiscellaneousInformation( objWriter, objContext.ApplicationInstance );
				//WriteProcessInformation( objWriter );

				objWriter.WriteEndElement( );
				objWriter.WriteEndDocument( );
			}
			finally
			{
				objWriter.Flush( );
				objWriter.Close( );
			}
		}

        public static string LogException(Exception exception, string basePath, HttpContext context)
        {
            string strErrorLogFile = "";
            string path = basePath;
            if (!path.EndsWith("\\"))
                path += "\\";

            path += "ErrorLog\\";

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            strErrorLogFile = "Error_" + DateTime.Now.Date.Day.ToString() +
                DateTime.Now.Date.Month.ToString() +
                DateTime.Now.Date.Year.ToString() + "_" +
                DateTime.Now.TimeOfDay.Hours.ToString() + "H" +
                DateTime.Now.TimeOfDay.Minutes.ToString() + "M" +
                DateTime.Now.TimeOfDay.Seconds.ToString() + "S_" +
                DateTime.Now.Millisecond.ToString() + ".xml";

            if (exception == null)
                exception = new Exception("Could not detect inner exception");
            // we assume that the file does not exist (due to the randomness of the file name)
            // if it does happen, the the user will go to the catch block and exit (without the log being written)
            ErrorCollector.WriteErrorAsXML(context, exception, path + strErrorLogFile);
            return strErrorLogFile;
        }

        public static string LogException(Exception exception)
        {
            string path = "..\\";
            return ErrorCollector.LogException( exception, path, null);
        }
        
		private static void WriteExceptionInformation( XmlTextWriter objWriter, Exception objException )
		{
			objWriter.WriteStartElement( "ExceptionInformation" );

			Type objExceptionType = objException.GetType( );
			WriteSimpleElement( objWriter, "ExceptionType", objExceptionType.ToString( ) );

			WriteSimpleElement( objWriter, "Message", objException.Message );

			WriteSimpleElement( objWriter, "StackTrace", objException.StackTrace );

			WriteSimpleElement( objWriter, "TargetSite", objException.TargetSite.ToString( ) );

			WriteSimpleElement( objWriter, "HelpLink", objException.HelpLink );

			WriteSimpleElement( objWriter, "Source", objException.Source );

			objWriter.WriteEndElement( );
		}

		private static void WriteMiscellaneousInformation( XmlTextWriter objWriter, HttpApplication objApplication )
		{
			objWriter.WriteStartElement( "MiscellaneousInformation" );

			WriteSimpleElement( objWriter, "RequestContentEncoding", objApplication.Request.ContentEncoding.EncodingName );
			WriteSimpleElement( objWriter, "RequestContentLength", objApplication.Request.ContentLength.ToString( ) );
			WriteSimpleElement( objWriter, "RequestContentType", objApplication.Request.ContentType );
			WriteSimpleElement( objWriter, "RequestFilePath", objApplication.Request.FilePath );
			WriteSimpleElement( objWriter, "RequestHttpMethod", objApplication.Request.HttpMethod );
			WriteSimpleElement( objWriter, "RequestAuthenticated", objApplication.Request.IsAuthenticated.ToString( ) );
			WriteSimpleElement( objWriter, "RequestSecure", objApplication.Request.IsSecureConnection.ToString( ) );
			WriteSimpleElement( objWriter, "RequestPhysicalPath", objApplication.Request.PhysicalPath );
			WriteSimpleElement( objWriter, "RequestQueryString", objApplication.Request.QueryString.ToString( ) );

			if ( objApplication.Request.UrlReferrer != null )
				WriteSimpleElement( objWriter, "RequestReferrer", objApplication.Request.UrlReferrer.ToString( ) );
			else
				WriteSimpleElement( objWriter, "RequestReferrer", String.Empty );

			WriteSimpleElement( objWriter, "UserAgent", objApplication.Request.UserAgent );
			WriteSimpleElement( objWriter, "UserHostAddress", objApplication.Request.UserHostAddress );

			objWriter.WriteEndElement( );
		}

		// We can't get the process information at the moment as aspnet_wp does
		// not have sufficient permissions to read this information.
		// restore code if a way can be figured out to get this information
//		private static void WriteProcessInformation( XmlTextWriter objWriter )
//		{
//			objWriter.WriteStartElement( "ProcessInformation" );
//
//			Process objProcess = Process.GetCurrentProcess( );
//
//			WriteSimpleElement( objWriter, "OpenHandleCount", objProcess.HandleCount.ToString( ) );
//			WriteSimpleElement( objWriter, "ProcessID", objProcess.Id.ToString( ) );
//			WriteSimpleElement( objWriter, "MachineName", objProcess.MachineName );
//			WriteSimpleElement( objWriter, "RAMMemoryUsage", objProcess.WorkingSet.ToString( ) );
//			WriteSimpleElement( objWriter, "PagedMemoryUsage", objProcess.PagedMemorySize.ToString( ) );
//			WriteSimpleElement( objWriter, "PeakMemoryUsage", objProcess.PeakWorkingSet.ToString( ) );
//			WriteSimpleElement( objWriter, "ProcessorTime", objProcess.TotalProcessorTime.ToString( ) );
//			WriteSimpleElement( objWriter, "ProcessName", objProcess.ProcessName );
//			WriteSimpleElement( objWriter, "StartTime", objProcess.StartTime.ToString( ) );
//
//			ProcessThreadCollection objThreads = objProcess.Threads;
//
//			objWriter.WriteStartElement( "Threads" );
//
//			foreach( ProcessThread objThread in objThreads )
//			{
//				objWriter.WriteStartElement( "Thread" );
//
//				WriteSimpleAttribute( objWriter, "Id", objThread.Id.ToString( ) );
//				WriteSimpleElement( objWriter, "Priority", objThread.CurrentPriority.ToString( ) );
//				WriteSimpleElement( objWriter, "ProcessorTime", objThread.TotalProcessorTime.ToString( ) );
//				WriteSimpleElement( objWriter, "StartTime", objThread.StartTime.ToString( ) );
//				WriteSimpleElement( objWriter, "ThreadState", objThread.ThreadState.ToString( ) );
//				WriteSimpleElement( objWriter, "WaitReason", objThread.WaitReason.ToString( ) );
//
//				objWriter.WriteEndElement( );
//			}
//
//			objWriter.WriteEndElement( );
//
//			objWriter.WriteEndElement( );
//		}

		private static void WriteRequestInformation( XmlTextWriter objWriter, HttpRequest objRequest )
		{
			objWriter.WriteStartElement( "RequestInformation" );

			WriteHeaderInformation( objWriter, objRequest );
			WriteServerVariableInformation( objWriter, objRequest );
			WriteFormInformation( objWriter, objRequest );
			WriteCookieInformation( objWriter, objRequest );
			WriteQueryStringInformation( objWriter, objRequest );
			
			objWriter.WriteEndElement( );
		}

		private static void WriteHeaderInformation( XmlTextWriter objWriter, HttpRequest objRequest )
		{
			NameValueCollection objHeaders = objRequest.Headers;

			objWriter.WriteStartElement( "Headers" );

			WriteNameValuePairCollection( objWriter, objRequest.Headers, "Header" );

			objWriter.WriteEndElement( );
		}

		private static void WriteServerVariableInformation( XmlTextWriter objWriter, HttpRequest objRequest )
		{
			objWriter.WriteStartElement( "ServerVariables" );

			WriteNameValuePairCollection( objWriter, objRequest.ServerVariables, "ServerVariable" );

			objWriter.WriteEndElement( );
		}

		private static void WriteFormInformation( XmlTextWriter objWriter, HttpRequest objRequest )
		{
			objWriter.WriteStartElement( "FormInformation" );

			WriteNameValuePairCollection( objWriter, objRequest.Form, "FormValue" );

			objWriter.WriteEndElement( );
		}

		private static void WriteCookieInformation( XmlTextWriter objWriter, HttpRequest objRequest )
		{
			objWriter.WriteStartElement( "CookiesInformation" );

			for ( int intKeyCounter = 0; intKeyCounter < objRequest.Cookies.AllKeys.Length; intKeyCounter++ )
			{
				string strKey = objRequest.Cookies.Keys[ intKeyCounter ];

				objWriter.WriteStartElement( "CookieInformation" );
				objWriter.WriteStartAttribute( "Name", String.Empty );
				objWriter.WriteString( strKey );
				objWriter.WriteEndAttribute( );

				HttpCookie objCookie = objRequest.Cookies.Get( strKey );

				objWriter.WriteStartAttribute( "Domain", String.Empty );
				objWriter.WriteString( objCookie.Domain );
				objWriter.WriteEndAttribute( );

				objWriter.WriteStartAttribute( "Expires", String.Empty );
				objWriter.WriteString( objCookie.Expires.ToString( ) );
				objWriter.WriteEndAttribute( );

				objWriter.WriteStartAttribute( "Path", String.Empty );
				objWriter.WriteString( objCookie.Path );
				objWriter.WriteEndAttribute( );

				objWriter.WriteStartAttribute( "Secure", String.Empty );
				objWriter.WriteString( objCookie.Secure.ToString( ) );
				objWriter.WriteEndAttribute( );

				WriteNameValuePairCollection( objWriter, objCookie.Values, "CookieValue" );

				objWriter.WriteEndElement( );
			}

			objWriter.WriteEndElement( );
		}

		private static void WriteQueryStringInformation( XmlTextWriter objWriter, HttpRequest objRequest )
		{
			objWriter.WriteStartElement( "QueryStringInformation" );

			WriteNameValuePairCollection( objWriter, objRequest.QueryString, "QueryStringValue" );

			objWriter.WriteEndElement( );
		}

		private static void WriteNameValuePairCollection( XmlTextWriter objWriter, NameValueCollection objCollection, string strElementName )
		{
			for ( int intKeyCounter = 0; intKeyCounter < objCollection.AllKeys.Length; intKeyCounter++ )
			{
				string strKey = objCollection.Keys[ intKeyCounter ];

				objWriter.WriteStartElement( strElementName );
				objWriter.WriteStartAttribute( "Name", String.Empty );
				objWriter.WriteString( strKey );
				objWriter.WriteEndAttribute( );

				string[] strValues = objCollection.GetValues( strKey );

				for ( int intValueCounter = 0; intValueCounter < strValues.GetLength( 0 ); intValueCounter++ )
				{
					objWriter.WriteStartElement( "Value" );
					objWriter.WriteString( strValues[ intValueCounter ] );
					objWriter.WriteEndElement( );
				}

				objWriter.WriteEndElement( );
			}
		}

		private static void WriteSimpleElement( XmlTextWriter objWriter, string strElementName, string strElementText )
		{
			objWriter.WriteStartElement( strElementName );
			objWriter.WriteString( strElementText );
			objWriter.WriteEndElement( );
		}

		private static void WriteSimpleAttribute( XmlTextWriter objWriter, string strAttributeName, string strAttributeText )
		{
			objWriter.WriteStartAttribute( strAttributeName, String.Empty );
			objWriter.WriteString( strAttributeText );
			objWriter.WriteEndAttribute( );
		}
	}
}
