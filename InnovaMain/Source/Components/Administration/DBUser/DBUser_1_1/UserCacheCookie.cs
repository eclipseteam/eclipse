using System;

namespace Oritax.TaxSimp.Security
{
	public class UserCacheCookie
	{
		private string userName;
		private Guid uniqueID;

		public UserCacheCookie(string userName)
		{
			this.userName = userName;
			this.uniqueID = Guid.NewGuid();
		}

		public string UserName
		{
			get
			{
				return this.userName;
			}
		}

		public Guid UniqueID
		{
			get
			{
				return this.uniqueID;
			}
		}
	}
}
