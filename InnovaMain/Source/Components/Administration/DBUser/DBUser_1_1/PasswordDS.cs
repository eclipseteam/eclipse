using System;
using System.Data;
using System.Data.SqlClient;

using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.SAS70Password;

namespace Oritax.TaxSimp.Security
{
	/// <summary>
	/// Summary description for PasswordDS.
	/// </summary>
	[Serializable]
	public class PasswordDS  : DataSet
	{
		public const String PasswordDetailsTable			= "PasswordDetail";
		public const string PasswordRulesTable				= "PasswordRules";

		public const String PasswordUserIdField				= "CID";
		public const String PasswordUsernameField			= "Username";
		public const String PasswordPasswordField			= "Password";
        	
		public PasswordDS()
		{
			DataTable dtUser;

			dtUser = new DataTable( PasswordDetailsTable );

			dtUser.Columns.Add( PasswordUserIdField, typeof( System.Guid) );
			dtUser.Columns.Add( PasswordUsernameField, typeof( System.String ) );
			dtUser.Columns.Add( PasswordPasswordField, typeof( System.String ) );

			DataColumn[] dcPrimaryKey = {dtUser.Columns[ PasswordUserIdField ]};
			dtUser.PrimaryKey = dcPrimaryKey;

			this.Tables.Add( dtUser );
			BrokerManagedComponentDS.AddVersionStamp(dtUser,typeof(PasswordDS));
			
			DataTable dtPwdRules = this.CreatePwdCheckingTable();
			this.Tables.Add(dtPwdRules);
			BrokerManagedComponentDS.AddVersionStamp(dtPwdRules, typeof(PasswordDS));			
		}

		//A table used to store the validation result of the password
		private DataTable CreatePwdCheckingTable()
		{
			DataTable table = new DataTable(PasswordRulesTable);
			table.Columns.Add(new DataColumn(PasswordHistoryDS.PasswordKeyField, typeof(System.String)));
			table.Columns.Add(new DataColumn(PasswordHistoryDS.PasswordDescriptionField, typeof(System.String)));
			table.Columns.Add(new DataColumn(PasswordHistoryDS.PasswordResultField, typeof(System.Boolean)));
			
			DataRow row = table.NewRow();
			row[PasswordHistoryDS.PasswordKeyField] = PasswordHistoryDS.PasswordDigitCharsKey;
			row[PasswordHistoryDS.PasswordDescriptionField] = PasswordHistoryDS.PasswordDigitChars;
			row[PasswordHistoryDS.PasswordResultField] = false;
			table.Rows.Add(row);
			row = table.NewRow();
			row[PasswordHistoryDS.PasswordKeyField] = PasswordHistoryDS.PasswordUpperCharsKey;
			row[PasswordHistoryDS.PasswordDescriptionField] = PasswordHistoryDS.PasswordUpperChars;
			row[PasswordHistoryDS.PasswordResultField] = false;
			table.Rows.Add(row);
			row = table.NewRow();
			row[PasswordHistoryDS.PasswordKeyField] = PasswordHistoryDS.PasswordLowerCharsKey;
			row[PasswordHistoryDS.PasswordDescriptionField] = PasswordHistoryDS.PasswordLowerChars;
			row[PasswordHistoryDS.PasswordResultField] = false;
			table.Rows.Add(row);
			row = table.NewRow();
			row[PasswordHistoryDS.PasswordKeyField] = PasswordHistoryDS.PasswordNonAlphaCharsKey;
			row[PasswordHistoryDS.PasswordDescriptionField] = PasswordHistoryDS.PasswordNonAlphaChars;
			row[PasswordHistoryDS.PasswordResultField] = false;
			table.Rows.Add(row);
			row = table.NewRow();
			row[PasswordHistoryDS.PasswordKeyField] = PasswordHistoryDS.PasswordNoUserNameKey;
			row[PasswordHistoryDS.PasswordDescriptionField] = PasswordHistoryDS.PasswordNoUserName;
			row[PasswordHistoryDS.PasswordResultField] = false;
			table.Rows.Add(row);
			row = table.NewRow();
			row[PasswordHistoryDS.PasswordKeyField] = PasswordHistoryDS.PasswordLengthKey;
			row[PasswordHistoryDS.PasswordDescriptionField] = PasswordHistoryDS.PasswordLength;
			row[PasswordHistoryDS.PasswordResultField] = false;
			table.Rows.Add(row);
			row = table.NewRow();
			row[PasswordHistoryDS.PasswordKeyField] = PasswordHistoryDS.PasswordNoHistoricalKey;
			row[PasswordHistoryDS.PasswordDescriptionField] = PasswordHistoryDS.PasswordNoHistorical;
			row[PasswordHistoryDS.PasswordResultField] = false;
			table.Rows.Add(row);
			row = table.NewRow();
			row[PasswordHistoryDS.PasswordKeyField] = PasswordHistoryDS.PasswordNoRepeatingCharsKey;
			row[PasswordHistoryDS.PasswordDescriptionField] = PasswordHistoryDS.PasswordNoRepeatingChars;
			row[PasswordHistoryDS.PasswordResultField] = false;
			table.Rows.Add(row);
			
			return table;
		}
	}
}
