using System;
using System.Data;
using System.Data.SqlClient;
using Oritax.TaxSimp.Calculation;

namespace Oritax.TaxSimp.Security
{
	/// <summary>
	/// Summary description for AuthenticateUserDS.
	/// </summary>
	public class AuthenticateUserDS  : DataSet
	{
		// constants

		public const String MSG_BADPASSWORD  = "Your login details are incorrect.<br> Please check your login name and password and try again.";
		public const String MSG_LOCKED		 = "Your user account has been locked. <br> Please contact Help Desk to unlock your user account.";
		public const String MSG_DISABLED	 = "Your user account has been disabled. <br> Please contact Help Desk to enable your user account.";

		public const String AUTHENTICATION_DETAIL_TABLE			= "AuthenticateUser";
		public const int	AUTHENTICATE_SUCCESS				= 0;
		public const int	AUTHENTICATE_BADUSER				= 1;
		public const int	AUTHENTICATE_BADPASSWORD			= 2;
		public const int	AUTHENTICATE_LOCKED					= 3;
		public const int	AUTHENTICATE_DISABLED				= 4;

		public const String AUTHENTICATION_USERID_FIELD			= "CID";
		public const String AUTHENTICATION_USERNAME_FIELD		= "UserName";
		public const String AUTHENTICATION_PASSWORD_FIELD		= "Password";
		public const String AUTHENTICATION_FAILEDATTEMPTS_FIELD	= "FailedAttempts";
		public const String AUTHENTICATION_LOCKED_FIELD			= "Locked";
		public const String AUTHENTICATION_AUTHENTICATE_FIELD	= "Authenticated";

		public AuthenticateUserDS()
		{
			DataTable dtAuthenticate;

			dtAuthenticate = new DataTable( AUTHENTICATION_DETAIL_TABLE );
			dtAuthenticate.Columns.Add( AUTHENTICATION_USERID_FIELD, typeof( System.Guid) );
			dtAuthenticate.Columns.Add( AUTHENTICATION_USERNAME_FIELD, typeof( System.String ) );
			dtAuthenticate.Columns.Add( AUTHENTICATION_PASSWORD_FIELD, typeof( System.String ) );
			dtAuthenticate.Columns.Add( AUTHENTICATION_FAILEDATTEMPTS_FIELD, typeof( System.Int32) );
			dtAuthenticate.Columns.Add( AUTHENTICATION_LOCKED_FIELD, typeof( System.Int32) );
			dtAuthenticate.Columns.Add( AUTHENTICATION_AUTHENTICATE_FIELD, typeof( System.Int32 ) );

			this.Tables.Add( dtAuthenticate );
		}
	}
}
