﻿using System;
using System.Data;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Data;
using System.Diagnostics;

namespace Oritax.TaxSimp.Security
{
    public partial class DBUser : IApplicationUserService
    {
        void IApplicationUserService.AddUser(UserEntity current, ApplicationUser user)
        {
            this.UserName = current.CurrentUserName;
            AddApplicationUser(user, current.IsWebUser);
        }

        private void SetDataDBUserAdd(string LoginUsername, string Firstname, string Lastname, string Email, string Password, bool IsAdmin, string orgGuid, string orgName, int userType)
        {
            ApplicationUser user = new ApplicationUser
            {
                LoginId = LoginUsername,
                FirstName = Firstname,
                LastName = Lastname,
                Email = Email,
                Password = Password,
                IsAdministrator = IsAdmin,
                OrganizationId = new Guid(orgGuid),
                OrganizationName = orgName,
                UserType = userType
            };

            AddApplicationUser(user);
        }

        private void AddApplicationUser(ApplicationUser user, bool isWebUser = false)
        {
            try
            {
                DBUserDetailsDS objData = new DBUserDetailsDS();
                this.PopulateOrganisationalDetailsTable(objData.OrganisationalDetailsTable);
                objData.IsNew = true;
                DataRow row = objData.Tables[DBUserDetailsDS.USER_DETAIL_TABLE].NewRow();
                row[DBUserDetailsDS.USER_USERID_FIELD] = Guid.NewGuid();
                row[DBUserDetailsDS.USER_USERNAME_FIELD] = user.LoginId.Trim();
                row[DBUserDetailsDS.USER_PASSWORD_FIELD] = Utilities.Encryption.EncryptData(user.Password.Trim());
                row[DBUserDetailsDS.USER_FIRSTNAME_FIELD] = user.FirstName.Trim();
                row[DBUserDetailsDS.USER_LASTNAME_FIELD] = user.LastName.Trim();
                row[DBUserDetailsDS.USER_ACTIVE_FIELD] = 1;
                row[DBUserDetailsDS.USER_LOCKED_FIELD] = 0;
                row[DBUserDetailsDS.USER_FAILEDATTEMPTS_FIELD] = 0;
                row[DBUserDetailsDS.USER_EMAILADDRESS_FIELD] = user.Email.Trim();
                row[DBUserDetailsDS.USER_ADMINISTRATOR_FIELD] = user.IsAdministrator ? 1 : 0;
                row[DBUserDetailsDS.USER_DISPLAY_ZERO_PREFERENCE_FIELD] = false;
                row[DBUserDetailsDS.USER_ORGANISATION_CID_FIELD] = user.OrganizationId;
                row[DBUserDetailsDS.USER_ORGANISATION_NAME_FIELD] = user.OrganizationName;
                row[DBUserDetailsDS.USER_USER_TYPE_FIELD] = user.UserType;
                if (this.UserName.ToLower() != "administrator")
                {
                    IOrganization organisation = (IOrganization)this.Broker.GetBMCInstance(this.UserOrganisation);
                    row[DBUserDetailsDS.USER_ORGANISATION_CID_FIELD] = this.UserOrganisation;
                    row[DBUserDetailsDS.USER_ORGANISATION_NAME_FIELD] = organisation.OrganisationName;
                }
                objData.Tables[DBUserDetailsDS.USER_DETAIL_TABLE].Rows.Add(row);
                if (isWebUser)
                {
                    Broker.SetStart();
                }
                else
                {
                    Broker.SetWriteStart();
                }

                IBrokerManagedComponent iBMC = this.Broker.CreateComponentInstance(new Guid(DBUserInstall.ASSEMBLY_ID), user.LoginId);
                iBMC.SetData(objData);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
            }
        }

        private void SetDataDBUserUpdate(string LoginUsername, string Firstname, string Lastname, string Email, string Password,
            bool IsAdmin, string orgGuid, string orgName, bool IsActive, bool IsLocked, int UserType)
        {
            ApplicationUser user = new ApplicationUser
            {
                LoginId = LoginUsername,
                FirstName = Firstname,
                LastName = Lastname,
                Email = Email,
                Password = Password,
                IsAdministrator = IsAdmin,
                IsActive = IsActive,
                IsLocked = IsLocked,
                OrganizationId = new Guid(orgGuid),
                OrganizationName = orgName,
                UserType = UserType
            };

            UpdateApplicationUser(user);
        }

        private void UpdateApplicationUser(ApplicationUser user)
        {
            try
            {
                DBUserDetailsDS objData = new DBUserDetailsDS();
                DBUserEditSL dbusereditsl = new DBUserEditSL();
                objData.UserList = false;
                this.OnGetData(objData);
                this.addUserLoginInfo(objData);
                this.BuildUsersDataSource(objData);
                dbusereditsl.OrganisationalDetailsList = GetOrganisationDetails(objData);
                DataTable userDetailTable = objData.Tables[DBUserDetailsDS.USER_DETAIL_TABLE];
                objData.IsNew = false;

                if (userDetailTable.Rows.Count > 0)
                {
                    DataRow row = userDetailTable.Rows[0];
                    row[DBUserDetailsDS.USER_USERNAME_FIELD] = user.LoginId.Trim();
                    row[DBUserDetailsDS.USER_FIRSTNAME_FIELD] = user.FirstName.Trim();
                    row[DBUserDetailsDS.USER_LASTNAME_FIELD] = user.LastName.Trim();
                    row[DBUserDetailsDS.USER_ACTIVE_FIELD] = user.IsActive;
                    row[DBUserDetailsDS.USER_LOCKED_FIELD] = user.IsLocked;
                    row[DBUserDetailsDS.USER_FAILEDATTEMPTS_FIELD] = 0;
                    row[DBUserDetailsDS.USER_EMAILADDRESS_FIELD] = user.Email.Trim();
                    row[DBUserDetailsDS.USER_ADMINISTRATOR_FIELD] = user.IsAdministrator;
                    row[DBUserDetailsDS.USER_DISPLAY_ZERO_PREFERENCE_FIELD] = false;
                    row[DBUserDetailsDS.USER_ORGANISATION_CID_FIELD] = user.OrganizationId;
                    row[DBUserDetailsDS.USER_ORGANISATION_NAME_FIELD] = user.OrganizationName;
                    row[DBUserDetailsDS.USER_USER_TYPE_FIELD] = user.UserType;
                    if (this.UserName.ToLower() != "administrator")
                    {
                        IOrganization organisation = (IOrganization)this.Broker.GetBMCInstance(this.UserOrganisation);
                        row[DBUserDetailsDS.USER_ORGANISATION_CID_FIELD] = this.UserOrganisation;
                        row[DBUserDetailsDS.USER_ORGANISATION_NAME_FIELD] = organisation.OrganisationName;
                    }
                    objData.AcceptChanges();
                    this.Broker.SetWriteStart();
                    this.SetData(objData);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
            }
        }
    }
}