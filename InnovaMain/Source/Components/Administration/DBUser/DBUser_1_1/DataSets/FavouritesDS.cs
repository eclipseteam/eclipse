using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace Oritax.TaxSimp.Security
{
    public class FavouritesDS : DataSet
    {
        #region Fields

        public String WorkpaperName;
        public String SelectedFolderName;
        public Guid SelectedFolderGuid;

        #endregion // Fields
    }
}
