using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Xml;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using Oritax.TaxSimp.BusinessStructureUtilities;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.CM.Entity;
using Oritax.TaxSimp.CM.Group;
using Oritax.TaxSimp.CM.MultiPeriod;
using Oritax.TaxSimp.CM.Period;
using Oritax.TaxSimp.Common;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.DSCalculation;
using Oritax.TaxSimp.Enum;
using Oritax.TaxSimp.Exceptions;
using Oritax.TaxSimp.SAPBase;
using Oritax.TaxSimp.TaxSimpLicence;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.IOrganizationUnits;
using Oritax.TaxSimp.Extensions;
using System.Xml.Serialization;
using AccessFacilities = Oritax.TaxSimp.Data.AccessFacilities;
using ExemptionCategory = Oritax.TaxSimp.Data.ExemptionCategory;
using OperationManner = Oritax.TaxSimp.Data.OperationManner;
using ServiceType = Oritax.TaxSimp.Data.ServiceType;
using Oritax.TaxSimp.DataSets;
using Oritax.TaxSimp;
using Oritax.TaxSimp.Common.UMAFee;
using Oritax.TaxSimp.Common.Data;
using Oritax.TaxSimp.CM.Organization;
using System.Globalization;
using System.IO;
using System.Text;
using ClientShare = Oritax.TaxSimp.Common.ClientShare;
using IdentityCMDetail = Oritax.TaxSimp.Common.IdentityCMDetail;
using System.ComponentModel;
using System.Net;
using CashManagementEntity = Oritax.TaxSimp.Common.CashManagementEntity;

namespace Oritax.TaxSimp.CM.OrganizationUnit
{
    [Serializable]
    public partial class OrganizationUnitCM : BusinessStructureBase, IOrganizationUnit, IHasDistributionsAndDividends
    {

        #region STATEFUL VARIABLES
        private ClientFeeEntity clientFeeEntity = new ClientFeeEntity();
        private Guid m_objCurrentPeriod = Guid.Empty;
        private SerializableHashtable m_objPeriodsHT;
        private string m_strLegalName;
        private bool isBGL = false;
        private string bglAccountCode = string.Empty;
        private bool isClassSuper = false;
        private bool isDesktopSuper = false;
        private string m_strIdentifier;
        private bool isInvestableClient = false;
        private string organizationstatus = string.Empty;
        private string clientid = string.Empty;
        private ServiceType servicetype;
        private AccessFacilities accessfacilities;
        private OperationManner operationmanner;
        private ExemptionCategory exemptioncategory;
        private bool isexported;
        private bool isexportedSS;
        private bool isexportedDB;
        private string applicationdate;
        private string applicationid;
        [OptionalField]
        protected TaxStatusType m_objTaxStatus = TaxStatusType.Default;
        private List<FeeTransaction> feeTransactions = new List<FeeTransaction>();
        List<Guid> configuredFeesList = new List<Guid>();
        private DateTime _updationDate;
        private DateTime _creationDate;
        private string superMemberID = string.Empty;
        private string superClientID = string.Empty;

        #endregion

        #region CONSTANT
        private const string LockedStatusTimeIntensiveTaskKey = "LockedStatusTimeIntensiveTask";
        private const string DeleteScenarioConfirmKey = "DeleteScenarioConfirmTimeIntensiveTask";
        #endregion

        #region CONSTRUCTORS
        /// <summary>
        /// 
        /// </summary>
        public OrganizationUnitCM()
            : base()
        {
            m_objPeriodsHT = new SerializableHashtable();
            this.name = String.Empty;
            this.LegalName = String.Empty;
            this.Identifier = String.Empty;
            FileMaps = new Dictionary<string, Guid>();
            DefaultForms = new List<DefaultFormsFile>();
            ServiceType = new ServiceType();
            accessfacilities = new AccessFacilities();
            operationmanner = new OperationManner();
            exemptioncategory = new ExemptionCategory();
            isexported = false;
            isexportedSS = false;
            isexportedDB = false;
            applicationdate = String.Empty;
            applicationid = String.Empty;
        }

        public virtual bool IsCorporateType()
        {
            return true;
        }

        public List<Guid> ConfiguredFeesList
        {
            get { return configuredFeesList; }
            set { configuredFeesList = value; }
        }

        public List<FeeTransaction> FeeTransactions
        {
            get { return feeTransactions; }
            set { feeTransactions = value; }
        }

        public virtual List<Oritax.TaxSimp.Common.IdentityCMDetail> SignatoriesApplicants
        {
            get
            {
                if (this.ClientEntity is CorporateEntity)
                    return ((CorporateEntity)ClientEntity).Signatories;
                else
                    return ((ClientIndividualEntity)ClientEntity).Applicants;
            }
        }


        public virtual List<Oritax.TaxSimp.Common.IdentityCMDetail> Contacts
        {
            get
            {
                if (this.ClientEntity is CorporateEntity)
                    return ((CorporateEntity)ClientEntity).Contacts;
                else
                    return null;
            }
        }

        public virtual IClientEntity ClientEntity
        { get; set; }

        public DateTime UpdationDate
        {
            get { return _updationDate; }
            set
            {
                _updationDate = value;
            }
        }
        public DateTime CreationDate
        {
            get
            {
                return _creationDate;
            }
            set
            {
                _creationDate = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="si"></param>
        /// <param name="context"></param>
        /// <exception cref="System.ArgumentNullException">si cannot be null</exception>
        protected OrganizationUnitCM(SerializationInfo si, StreamingContext context)
            : base(si, context)
        {
            if (si == null)
                throw new ArgumentNullException("si");

            m_objCurrentPeriod = Serialize.GetSerializedGuid(si, "oucm_m_objCurrentPeriod");
            m_objPeriodsHT = Serialize.GetSerializedHashtable(si, "oucm_m_objPeriodsHT");
            m_strLegalName = Serialize.GetSerializedString(si, "oucm_m_strLegalName");
            m_strIdentifier = Serialize.GetSerializedString(si, "oucm_m_strIdentifier");
            m_objTaxStatus = (TaxStatusType)Serialize.GetSerializedValue(si, "oucm_m_objTaxStatus", typeof(TaxStatusType), TaxStatusType.Default);
            isInvestableClient = Serialize.GetSerializedBool(si, "Oritax.TaxSimp.CM.OrganizationUnit.isInvestableClient");
            organizationstatus = Serialize.GetSerializedString(si, "Oritax.TaxSimp.CM.OrganizationUnit.organizationstatus");
            clientid = Serialize.GetSerializedString(si, "Oritax.TaxSimp.CM.OrganizationUnit.clientid");
            FileMaps = Serialize.GetValue<Dictionary<string, Guid>>(si, "Oritax.TaxSimp.CM.OrganizationUnit.FileMaps");
            DefaultForms = Serialize.GetValue<List<DefaultFormsFile>>(si, "Oritax.TaxSimp.CM.OrganizationUnit.DefaultForms");
            servicetype = Serialize.GetValue<ServiceType>(si, "Oritax.TaxSimp.CM.OrganizationUnit.ServiceType");
            accessfacilities = Serialize.GetValue<AccessFacilities>(si, "Oritax.TaxSimp.CM.OrganizationUnit.AccessFacilities");
            operationmanner = Serialize.GetValue<OperationManner>(si, "Oritax.TaxSimp.CM.OrganizationUnit.OperationManner");
            exemptioncategory = Serialize.GetValue<ExemptionCategory>(si, "Oritax.TaxSimp.CM.OrganizationUnit.ExemptionCategory");
            isexported = Serialize.GetSerializedBool(si, "Oritax.TaxSimp.CM.OrganizationUnit.IsExported");
            isexportedSS = Serialize.GetSerializedBool(si, "Oritax.TaxSimp.CM.OrganizationUnit.IsExportedSS");
            isexportedDB = Serialize.GetSerializedBool(si, "Oritax.TaxSimp.CM.OrganizationUnit.IsExportedDB");
            applicationdate = Serialize.GetSerializedString(si, "Oritax.TaxSimp.CM.OrganizationUnit.applicationdate");
            applicationid = Serialize.GetSerializedString(si, "Oritax.TaxSimp.CM.OrganizationUnit.applicationid");
            feeTransactions = Serialize.GetValue<List<FeeTransaction>>(si, "Oritax.TaxSimp.Common.UMAFee.FeeTransactionList");
            configuredFeesList = Serialize.GetValue<List<Guid>>(si, "Oritax.TaxSimp.Common.UMAFee.ConfiguredFeesList");
            clientFeeEntity = Serialize.GetValue<ClientFeeEntity>(si, "ClientFeeEntity");
            _creationDate = Serialize.GetSerializedDateTime(si, "Oritax.TaxSimp.CM.OrganizationUnit.CreationDate");
            _updationDate = Serialize.GetSerializedDateTimeWithMin(si, "Oritax.TaxSimp.CM.OrganizationUnit.UpdationDate");
            isClassSuper = Serialize.GetSerializedBool(si, "Oritax.TaxSimp.CM.OrganizationUnit.IsClassSuper");
            isBGL = Serialize.GetSerializedBool(si, "Oritax.TaxSimp.CM.OrganizationUnit.IsBGL");
            isDesktopSuper = Serialize.GetSerializedBool(si, "Oritax.TaxSimp.CM.OrganizationUnit.IsDesktopSuper");
            bglAccountCode = Serialize.GetSerializedString(si, "Oritax.TaxSimp.CM.OrganizationUnit.BglAccountCode");
            superMemberID = Serialize.GetSerializedString(si, "Oritax.TaxSimp.CM.OrganizationUnit.SuperMemberID");
            superClientID = Serialize.GetSerializedString(si, "Oritax.TaxSimp.CM.OrganizationUnit.SuperClientID");
        }

        //Only allow the .NET Serialization core code to call this function
        /// <summary>
        /// 
        /// </summary>
        /// <param name="si"></param>
        /// <param name="context"></param>
        /// <exception cref="System.ArgumentNullException">si cannot be null</exception>
        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo si, StreamingContext context)
        {
            if (si == null)
                throw new ArgumentNullException("si");

            base.GetObjectData(si, context);
            Serialize.AddSerializedValue(si, "oucm_m_objCurrentPeriod", m_objCurrentPeriod);
            Serialize.AddSerializedValue(si, "oucm_m_objPeriodsHT", m_objPeriodsHT);
            Serialize.AddSerializedValue(si, "oucm_m_strLegalName", m_strLegalName);
            Serialize.AddSerializedValue(si, "oucm_m_strIdentifier", m_strIdentifier);
            Serialize.AddSerializedValue(si, "oucm_m_objTaxStatus", m_objTaxStatus);
            Serialize.AddSerializedValue(si, "Oritax.TaxSimp.CM.OrganizationUnit.isInvestableClient", isInvestableClient);
            Serialize.AddSerializedValue(si, "Oritax.TaxSimp.CM.OrganizationUnit.organizationstatus", organizationstatus);
            Serialize.AddSerializedValue(si, "Oritax.TaxSimp.CM.OrganizationUnit.clientid", clientid);
            Serialize.AddSerializedValue(si, "Oritax.TaxSimp.CM.OrganizationUnit.FileMaps", FileMaps);
            Serialize.AddSerializedValue(si, "Oritax.TaxSimp.CM.OrganizationUnit.DefaultForms", DefaultForms);
            Serialize.AddSerializedValue(si, "Oritax.TaxSimp.CM.OrganizationUnit.ServiceType", servicetype);
            Serialize.AddSerializedValue(si, "Oritax.TaxSimp.CM.OrganizationUnit.AccessFacilities", accessfacilities);
            Serialize.AddSerializedValue(si, "Oritax.TaxSimp.CM.OrganizationUnit.OperationManner", operationmanner);
            Serialize.AddSerializedValue(si, "Oritax.TaxSimp.CM.OrganizationUnit.ExemptionCategory", exemptioncategory);
            Serialize.AddSerializedValue(si, "Oritax.TaxSimp.CM.OrganizationUnit.IsExported", isexported);
            Serialize.AddSerializedValue(si, "Oritax.TaxSimp.CM.OrganizationUnit.IsExportedSS", isexportedSS);
            Serialize.AddSerializedValue(si, "Oritax.TaxSimp.CM.OrganizationUnit.IsExportedDB", isexportedDB);
            Serialize.AddSerializedValue(si, "Oritax.TaxSimp.CM.OrganizationUnit.applicationdate", applicationdate);
            Serialize.AddSerializedValue(si, "Oritax.TaxSimp.CM.OrganizationUnit.applicationid", applicationid);
            Serialize.AddSerializedValue(si, "Oritax.TaxSimp.Common.UMAFee.FeeTransactionList", feeTransactions);
            Serialize.AddSerializedValue(si, "Oritax.TaxSimp.Common.UMAFee.ConfiguredFeesList", configuredFeesList);
            Serialize.AddSerializedValue(si, "ClientFeeEntity", clientFeeEntity);
            Serialize.AddSerializedValue(si, "Oritax.TaxSimp.CM.OrganizationUnit.CreationDate", CreationDate);
            Serialize.AddSerializedValue(si, "Oritax.TaxSimp.CM.OrganizationUnit.UpdationDate", UpdationDate);
            Serialize.AddSerializedValue(si, "Oritax.TaxSimp.CM.OrganizationUnit.IsClassSuper", isClassSuper);
            Serialize.AddSerializedValue(si, "Oritax.TaxSimp.CM.OrganizationUnit.IsBGL", isBGL);
            Serialize.AddSerializedValue(si, "Oritax.TaxSimp.CM.OrganizationUnit.IsDesktopSuper", isDesktopSuper);
            Serialize.AddSerializedValue(si, "Oritax.TaxSimp.CM.OrganizationUnit.BglAccountCode", bglAccountCode);
            Serialize.AddSerializedValue(si, "Oritax.TaxSimp.CM.OrganizationUnit.SuperMemberID", superMemberID);
            Serialize.AddSerializedValue(si, "Oritax.TaxSimp.CM.OrganizationUnit.SuperClientID", superClientID);
        }

        #endregion

        #region PROPERTIES

        public string SuperMemberID
        {
            get { return superMemberID; }
            set { superMemberID = value; }
        }

        public string SuperClientID
        {
            get { return superClientID; }
            set { superClientID = value; }
        }

        public string BglAccountCode
        {
            get { return bglAccountCode; }
            set { bglAccountCode = value; }
        }


        public bool IsDesktopSuper
        {
            get { return isDesktopSuper; }
            set { isDesktopSuper = value; }
        }

        public bool IsClassSuper
        {
            get { return isClassSuper; }
            set { isClassSuper = value; }
        }

        public bool IsBGL
        {
            get { return isBGL; }
            set { isBGL = value; }
        }

        public ClientFeeEntity ClientFeeEntity
        {
            get { return clientFeeEntity; }
            set { clientFeeEntity = value; }
        }

        public virtual IdentityCM ConfiguredParent
        {
            get
            {
                return null;
            }
            set
            {

            }
        }

        [XmlElement("IsInvestableClient")]
        public bool IsInvestableClient
        {
            get { return isInvestableClient; }
            set { isInvestableClient = value; }
        }

        public virtual bool HasAdviser
        {
            get
            {
                if (this.isInvestableClient)
                    return true;
                else
                    return false;
            }
        }

        public virtual Guid AdviserID
        {
            get
            {
                return Guid.Empty;
            }
        }

        [XmlElement("IsConsolCM")]
        public virtual bool IsConsolCM
        {
            get { return false; }
        }

        [XmlElement("OrganizationStatus")]
        public string OrganizationStatus
        {
            get { return organizationstatus; }
            set { organizationstatus = value; }
        }

        [XmlElement("ClientId")]
        public string ClientId
        {
            get { return this.EclipseClientID; }
            set { EclipseClientID = value; }
        }
        [Obsolete]
        public string OldClientId
        {
            get { return this.clientid; }
        }

        public ServiceType ServiceType
        {
            get { return servicetype; }
            set { servicetype = value; }
        }

        public bool HasDIFM
        {
            get
            {
                return ServiceType.DO_IT_FOR_ME;
            }
        }

        public AccessFacilities AccessFacilities
        {
            get { return accessfacilities; }
            set { accessfacilities = value; }
        }

        public OperationManner OperationManner
        {
            get { return operationmanner; }
            set { operationmanner = value; }
        }

        public ExemptionCategory ExemptionCategory
        {
            get { return exemptioncategory; }
            set { exemptioncategory = value; }
        }

        [XmlElement("IsExported")]
        public bool IsExported
        {
            get { return isexported; }
            set { isexported = value; }
        }

        [XmlElement("IsExportedSS")]
        public bool IsExportedSS
        {
            get { return isexportedSS; }
            set { isexportedSS = value; }
        }

        [XmlElement("IsExportedDB")]
        public bool IsExportedDB
        {
            get { return isexportedDB; }
            set { isexportedDB = value; }
        }

        [XmlElement("ApplicationDate")]
        public string ApplicationDate
        {
            get { return applicationdate; }
            set { applicationdate = value; }
        }

        public string ApplicationID
        {
            get { return applicationid; }
            set { applicationid = value; }
        }

        public virtual ISubstitutedAccountingPeriods SAPs { get; set; }

        /// <summary>
        /// The full legal name of the organisation, used on ATO forms
        /// </summary>
        public string LegalName
        {
            get
            {
                return this.m_strLegalName;
            }
            set
            {
                this.m_strLegalName = value;
            }
        }

        /// <summary>
        /// The identifier for the organisation, used by GL
        /// </summary>
        public string Identifier
        {
            get
            {
                return this.m_strIdentifier;
            }
            set
            {
                this.m_strIdentifier = value;
            }
        }

        public Guid CurrentPeriod
        {
            get
            {
                return this.m_objCurrentPeriod;
            }
            set
            {
                this.m_objCurrentPeriod = value;
                //PB -  Need to recalculate the token here as this property is used 
                // to modify the state of the CM.
                this.CalculateToken(true);
            }
        }

        public SerializableHashtable MemberPeriods
        {
            get { return m_objPeriodsHT; }
        }

        public IEnumerable PeriodScenarios
        {
            get
            {
                ArrayList periodScenarios = new ArrayList();

                ILogicalModule thisLM = this.Broker.GetLogicalCM(this.CLID);

                foreach (Guid gdCLID in thisLM.ChildCLIDs)
                {
                    ILogicalModule logicalCM = this.Broker.GetLogicalCM(gdCLID);

                    foreach (DictionaryEntry dictionaryEntry in logicalCM.Scenarios)
                    {
                        CMScenario cMScenario = (CMScenario)dictionaryEntry.Value;
                        periodScenarios.Add(cMScenario);
                    }

                    this.Broker.ReleaseBrokerManagedComponent(logicalCM);
                }

                this.Broker.ReleaseBrokerManagedComponent(thisLM);

                return periodScenarios;
            }
        }

        public TaxStatusType TaxStatus
        {
            get
            {
                return this.m_objTaxStatus;
            }
            set
            {
                this.m_objTaxStatus = value;
            }
        }

        public virtual string TaxStatusName
        {
            get
            {
                return this.TaxStatus.ToString();
            }
        }

        #endregion

        #region OTHER MEMBER FUNCTIONS
        /// <summary>
        /// 
        /// </summary>
        /// <param name="gdCLID"></param>
        /// <param name="dtStartDate"></param>
        /// <param name="dtEndDate"></param>
        /// <param name="strTypeName"></param>
        /// <exception cref="System.ArgumentException">gdCLID cannot be an empty guid</exception>
        /// <exception cref="System.ArgumentNullException">strTypeName cannot be null or an empty string</exception>
        public void AddNewMemberPeriodDetails(Guid gdCLID, DateTime dtStartDate, DateTime dtEndDate, string strTypeName)
        {
            if (gdCLID == Guid.Empty)
                throw new ArgumentException("Parameter cannot be an empty guid", "gdCLID");

            if (string.IsNullOrEmpty(strTypeName))
                throw new ArgumentNullException("strTypeName");

            this.m_objPeriodsHT.Add(gdCLID, new PeriodMember(dtStartDate, dtEndDate, strTypeName));
            this.CalculateToken(true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gdCLID"></param>
        /// <param name="dtStartDate"></param>
        /// <param name="dtEndDate"></param>
        /// <exception cref="System.ArgumentException">gdCLID cannot be an empty guid</exception>
        public void EditMemberPeriodDetails(Guid gdCLID, DateTime dtStartDate, DateTime dtEndDate)
        {
            if (gdCLID == Guid.Empty)
                throw new ArgumentException("Parameter cannot be an empty guid", "gdCLID");

            PeriodMember objPeriodMember;
            if (m_objPeriodsHT.Contains(gdCLID))
            {
                objPeriodMember = (PeriodMember)m_objPeriodsHT[gdCLID];
                objPeriodMember.dtStartDate = dtStartDate;
                objPeriodMember.dtEndDate = dtEndDate;
                this.CalculateToken(true);
            }
            else
                throw new Exception("EntityCM -> Member period does not exist in collection");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gdCLID"></param>
        /// <exception cref="System.ArgumentException">gdCLID cannot be an empty guid</exception>
        public void RemoveMemberPeriodDetails(Guid gdCLID)
        {
            if (gdCLID == Guid.Empty)
                throw new ArgumentException("Parameter cannot be an empty guid", "gdCLID");

            this.m_objPeriodsHT.Remove(gdCLID);
        }

        /// <summary>
        /// Returns a list of all child periods for the current Organisation Unit
        /// </summary>
        /// <returns></returns>
        public IPeriod[] GetChildPeriods()
        {
            ILogicalModule organisationUnitLM = this.GetLogicalModule();

            ArrayList childPeriodsList = new ArrayList();

            foreach (CMScenario cmScenario in organisationUnitLM.GetChildCMs(this.CSID))
            {
                ICalculationModule periodCM = this.Broker.GetCMImplementation(cmScenario.CLID, cmScenario.CSID);

                if (periodCM != null)
                    childPeriodsList.Add(periodCM);
            }

            this.Broker.ReleaseBrokerManagedComponent(organisationUnitLM);

            return (IPeriod[])childPeriodsList.ToArray(typeof(IPeriod));
        }

        //Return the CLID of any member periods that match the start and end dates passed in
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dtStartDate"></param>
        /// <param name="dtEndDate"></param>
        public Guid GetCLIDOfMemberPeriod(DateTime dtStartDate, DateTime dtEndDate)
        {
            IDictionaryEnumerator en = m_objPeriodsHT.GetEnumerator();
            while (en.MoveNext())
            {
                PeriodMember objPeriodMember = (PeriodMember)en.Value;
                if (objPeriodMember.dtStartDate == dtStartDate
                    && objPeriodMember.dtEndDate == dtEndDate)
                {
                    Guid periodCLID = new Guid(en.Key.ToString());
                    ICalculationModule periodCM = this.Broker.GetCMImplementation(periodCLID, this.CSID);

                    if (periodCM != null)
                        return periodCLID;
                }
            }
            return Guid.Empty;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strOrganisationName"></param>
        /// <exception cref="System.ArgumentNullException">strOrganisationName cannot be null or an empty string</exception>
        public bool IsUniqueOrganisationName(string strOrganisationName)
        {
            if (string.IsNullOrEmpty(strOrganisationName))
                throw new ArgumentNullException("strOrganisationName");

            ICalculationModule objOrganisation = this.Owner.GetWellKnownCM(WellKnownCM.Organization);

            if (objOrganisation != null)
            {
                DataTable logicalModuleRows = this.Broker.GetLCMsByParent(objOrganisation.CLID);

                foreach (DataRow logicalModuleRow in logicalModuleRows.Rows)
                {
                    Guid objOrganisationUnitCLID = (Guid)logicalModuleRow[DSLogicalModulesInfo.LCM_CLID_FIELD];
                    ILogicalModule objLogicalOrganisationUnit = this.Broker.GetLogicalCM(objOrganisationUnitCLID);
                    ICalculationModule objOUCM = objLogicalOrganisationUnit[objLogicalOrganisationUnit.CurrentScenario];
                    this.Broker.ReleaseBrokerManagedComponent(objLogicalOrganisationUnit);

                    if (strOrganisationName.ToLower() == objOUCM.Name.ToLower()
                        && this.CLID != objOUCM.CLID)
                    {
                        return false;
                    }

                    this.Broker.ReleaseBrokerManagedComponent(objOUCM);
                }
            }

            this.Broker.ReleaseBrokerManagedComponent(objOrganisation);

            // if we reach this point and haven't returned then
            // we didn't find a match, so return true
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strOrganisationLegalName"></param>
        /// <exception cref="System.ArgumentNullException">strOrganisationLegalName cannot be null or an empty string</exception>
        public bool IsUniqueLegalOrganisationName(string strOrganisationLegalName)
        {
            if (string.IsNullOrEmpty(strOrganisationLegalName))
                throw new ArgumentNullException("strOrganisationLegalName");

            ICalculationModule objOrganisation = this.Owner.GetWellKnownCM(WellKnownCM.Organization);
            DataTable logicalModuleRows = this.Broker.GetLCMsByParent(objOrganisation.CLID);
            this.Broker.ReleaseBrokerManagedComponent(objOrganisation);

            foreach (DataRow logicalModuleRow in logicalModuleRows.Rows)
            {
                Guid objOrganisationUnitCLID = (Guid)logicalModuleRow[DSLogicalModulesInfo.LCM_CLID_FIELD];
                ILogicalModule objLogicalOrganisationUnit = this.Broker.GetLogicalCM(objOrganisationUnitCLID);
                IOrganizationUnit objOU = (IOrganizationUnit)objLogicalOrganisationUnit[objLogicalOrganisationUnit.CurrentScenario];
                this.Broker.ReleaseBrokerManagedComponent(objLogicalOrganisationUnit);

                if (strOrganisationLegalName.ToLower() == objOU.LegalName.ToLower()
                    && this.CLID != objOU.CLID)
                {
                    return false;
                }

                this.Broker.ReleaseBrokerManagedComponent(objOU);
            }

            // if we reach this point and haven't returned then
            // we didn't find a match, so return true
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        public bool IsMemberOfGroup()
        {
            DataRowCollection objConsolidationParents = (DataRowCollection)this.Broker.GetConsolidationParentsList(this.CLID);

            if (objConsolidationParents.Count > 0)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Checks if performing a task at this level will be time intensive
        /// navigates up through the structure checking for group membership
        /// </summary>
        public void CheckIfStructureWillBeTimeIntensive(DateTime periodStartDate, DateTime periodEndDate, string userConfirmationValue, BusinessStructureNavigationDirection direction)
        {
            TimeIntensiveTaskGroupStructureVisitor navigator = new TimeIntensiveTaskGroupStructureVisitor(this.Broker, periodStartDate, periodEndDate, direction, userConfirmationValue);
            ((IBusinessStructureModule)this).PerformBusinessStructreNavigationOperation(navigator);
        }

        /// <summary>
        /// Deletes a scenario from the organisation unit
        /// </summary>
        /// <param name="objScenarioCSID">The CSID of the scenario to delete</param>
        /// <param name="objParentCSID">The CSID of the parent scenario</param>
        /// <param name="objLastScenarioConflicts">A list of scenarios that can't be deleted because they are the last scenario</param>
        /// <param name="objMemberOfGroupScenario">A list of scenarios that can't be deleted because they are a member of another group</param>
        /// <param name="objLockedScenarios">A list of scenarios that can't be deleted because they are locked</param>
        /// <returns>True if the scenario was deleted, false otherwise</returns>
        public virtual bool DeleteScenario(Guid objScenarioCSID, Guid objParentCSID, ArrayList objLastScenarioConflicts, ArrayList objMemberOfGroupScenario, ArrayList objLockedScenarios)
        {
            return false;
        }

        //Method to check that if there are any child multi periods
        //within the entity which have the same start and end date as the period
        //which the message may be coming from.
        //Then Suppress any onpublishing of Consolidation messages from the 
        //period, so that only messages from the corresponding multi period are used.
        private bool UpwardPublishMessage(IMessage objMessage)
        {
            DateTime objMessageStartDate = (DateTime)objMessage["StartDate"];
            DateTime objMessageEndDate = (DateTime)objMessage["EndDate"];

            PublisherData objPubData = objMessage.GetPublisherByCMTypeName("Period_1_1");
            if (objPubData == null)
                objPubData = objMessage.GetPublisherByCMTypeName("MultiPeriod");
            if (objPubData == null)
                objPubData = objMessage.GetPublisherByCMTypeName("MultiPeriodAdjustment");
            if (objPubData == null)
                throw new Exception("OrganisationUnitCM -> Unable to get publisher data");

            IDictionaryEnumerator en = this.MemberPeriods.GetEnumerator();
            while (en.MoveNext())
            {
                if ((Guid)en.Key != objPubData.CLID)
                {
                    PeriodMember objPeriodMember = (PeriodMember)en.Value;
                    if (objPeriodMember.strTypeName == "MultiPeriod"
                        && objPeriodMember.dtStartDate == objMessageStartDate
                        && objPeriodMember.dtEndDate == objMessageEndDate)
                        return false;
                }
            }
            return true;
        }

        public override bool FilterMessage(IMessage message, Object[] parameters)
        {
            if (message == null)
                throw new ArgumentNullException("message", "Message paramerer passed to filter message was null");

            if (parameters != null && message["EndDate"] != null)
            {
                if (parameters.Length > 0)
                {
                    DateTime objEndDate = (DateTime)message["EndDate"];
                    DateTime objCompareDate = (DateTime)parameters[0];

                    return (objEndDate == objCompareDate);
                }
                else
                    return (true);
            }
            else
                return (true);
        }
        #endregion

        #region OVERRIDE MEMBERS FOR CM BEHAVIOR
        //Override of construct breadcrumb so that when a breadcrumb is created for 
        //a business entity (ie a group or a standard entity) then this breadcrumb will
        //also contain the scenario information if possible.
        public override string ConstructBreadcrumb()
        {
            return this.name + " >";
        }

        private string ConstructBreadcrumbPeriod()
        {
            string periodBreadcrumb = String.Empty;

            ICalculationModule periodCM = this.Broker.GetCMImplementation(this.CurrentPeriod, this.CSID);

            if (periodCM != null)
                periodBreadcrumb = periodCM.Name;

            this.Broker.ReleaseBrokerManagedComponent(periodCM);

            return periodBreadcrumb;
        }

        private string ConstructScenarioBreadcrumb()
        {
            string scenarioBreadcrumb = String.Empty;
            ILogicalModule thisLM = this.Broker.GetLogicalCM(this.CLID);
            CMScenario cmScenario = thisLM.GetScenario(this.CSID);
            scenarioBreadcrumb = cmScenario.Name;
            this.Broker.ReleaseBrokerManagedComponent(thisLM);

            if (scenarioBreadcrumb != String.Empty)
                scenarioBreadcrumb = "[" + scenarioBreadcrumb + "]";

            return scenarioBreadcrumb;
        }

        protected override BrokerManagedComponent.ModifiedState OnSetSubscriptionData(IMessage message)
        {
            return base.OnSetSubscriptionData(message);
        }

        /// <summary>
        /// Called when an internally originating subscription update is to be delivered
        /// to the CM.  This is a subscription update that originates (or has been on-published)
        /// by a CM instance contained within the CM.
        /// </summary>
        /// <param name="message">The message containing the subscription update.</param>
        public override void OnInternalSubscriptionUpdate(IMessage message)
        {
            if (message.Contains("Consolidation"))
            {
                //Only on publish message with consolidation = true
                if ((bool)message["Consolidation"] && UpwardPublishMessage(message))
                {
                    this.OverrideStreamControlType((StreamControlType)message["StreamControl"]);

                    Message onPublishedMessage = new Message(this, message);

                    onPublishedMessage.InterScenario = true;	// All on-published messages by entities are made inter-scenario
                    onPublishedMessage["ConsolidatedSubscribedCLID"] = this.CLID;
                    onPublishedMessage["ConsolidatedSubscribedCSID"] = this.CSID;
                    PublishMessage(onPublishedMessage);

                    this.ResetOverrideStreamControlType();
                }
            }
        }


        public override DataSet PrimaryDataSet { get { return new OrganizationUnitDS(); } }

        #endregion

        #region OnGetData Methods
        protected void OnGetData(OrganisationUnitDetailsDS objOUDetails)
        {
            //Always set the logical module name for the org unit to be the short entity name.
            ILogicalModule iLM = this.Broker.GetLogicalCM(this.CLID);

            if (iLM != null)
                objOUDetails.Name = iLM.Name;
            else
                objOUDetails.Name = this.Name;

            this.Broker.ReleaseBrokerManagedComponent(iLM);

            objOUDetails.LegalName = this.LegalName;
            objOUDetails.Identifier = this.Identifier;
            objOUDetails.Breadcrumb = this.ConstructBreadcrumb();
        }

        public virtual string ClientAccountType
        {
            get
            {
                return string.Empty;
            }
        }

        public virtual string ClientApplicabilityType
        {
            get { return string.Empty; }
        }


        public Oritax.TaxSimp.Common.BankAccountEntity GetBankAccountDetails(string bsb, string accountNo, ServiceTypes serviceType)
        {
            Oritax.TaxSimp.Common.BankAccountEntity bankAccountEntity = null;

            if (serviceType != ServiceTypes.None)
            {
                List<AccountProcessTaskEntity> ListAccountProcess = new List<AccountProcessTaskEntity>();

                if (this.IsCorporateType())
                {
                    CorporateEntity entity = this.ClientEntity as CorporateEntity;
                    ListAccountProcess = entity.ListAccountProcess;
                }
                else
                {
                    ClientIndividualEntity entity = this.ClientEntity as ClientIndividualEntity;
                    ListAccountProcess = entity.ListAccountProcess;
                }

                foreach (AccountProcessTaskEntity accountProcessTaskEntity in ListAccountProcess)
                {
                    IOrganization orgCM = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;

                    ModelEntity modelEntity = orgCM.Model.Where(mod => mod.ID == accountProcessTaskEntity.ModelID).FirstOrDefault();
                    if (modelEntity != null)
                    {
                        if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.BankAccount && modelEntity.ServiceType == serviceType)
                        {
                            if (accountProcessTaskEntity.LinkedEntity.Clid != Guid.Empty || accountProcessTaskEntity.LinkedEntity.Cid != Guid.Empty)
                            {
                                IOrganizationUnit orgUnit = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as IOrganizationUnit;
                                bankAccountEntity = orgUnit.ClientEntity as Oritax.TaxSimp.Common.BankAccountEntity;
                            }
                        }
                    }
                }
            }

            return bankAccountEntity;
        }

        protected override void OnGetData(DataSet data)
        {
            base.OnGetData(data);

            if (data is BaseClientDetails)
            {
                BaseClientDetails baseClientDetails = (BaseClientDetails)data;
                if (baseClientDetails.UseAddressEntity)
                {
                    IClientUMAData clientUMAData = this.ClientEntity as IClientUMAData;

                    if (clientUMAData.Address != null)
                    {
                        if (baseClientDetails.AddressKey == "Business Address")
                            SetAddresses(baseClientDetails, clientUMAData.Address.BusinessAddress);
                        else if (baseClientDetails.AddressKey == "Residential Address")
                            SetAddresses(baseClientDetails, clientUMAData.Address.ResidentialAddress);
                        else if (baseClientDetails.AddressKey == "Mailing Address")
                            SetAddresses(baseClientDetails, clientUMAData.Address.MailingAddress);
                        else if (baseClientDetails.AddressKey == "Duplicate Mailing Address")
                            SetAddresses(baseClientDetails, clientUMAData.Address.DuplicateMailingAddress);
                        else if (baseClientDetails.AddressKey == "Registered Address")
                            SetAddresses(baseClientDetails, clientUMAData.Address.RegisteredAddress);

                        if (clientUMAData.Address.BusinessAddress != null && clientUMAData.Address.BusinessAddress.Addressline1 != string.Empty)
                            baseClientDetails.AddressList.Add("Business Address", clientUMAData.Address.BusinessAddress.GetFullAddress());
                        if (clientUMAData.Address.ResidentialAddress != null && clientUMAData.Address.ResidentialAddress.Addressline1 != string.Empty)
                            baseClientDetails.AddressList.Add("Residential Address", clientUMAData.Address.ResidentialAddress.GetFullAddress());
                        if (clientUMAData.Address.MailingAddress != null && clientUMAData.Address.MailingAddress.Addressline1 != string.Empty)
                            baseClientDetails.AddressList.Add("Mailing Address", clientUMAData.Address.MailingAddress.GetFullAddress());
                        if (clientUMAData.Address.DuplicateMailingAddress != null && clientUMAData.Address.DuplicateMailingAddress.Addressline1 != string.Empty)
                            baseClientDetails.AddressList.Add("Duplicate Mailing Address", clientUMAData.Address.DuplicateMailingAddress.GetFullAddress());
                        if (clientUMAData.Address.RegisteredAddress != null && clientUMAData.Address.RegisteredAddress.Addressline1 != string.Empty)
                            baseClientDetails.AddressList.Add("Registered Address", clientUMAData.Address.RegisteredAddress.GetFullAddress());
                    }

                    List<IdentityCMDetail> sigs = clientUMAData.GetSignatoriesList().Where(sig => sig.Clid != Guid.Empty).ToList();

                    foreach (var sigIdentity in sigs)
                    {
                        IOrganizationUnit sigUnit = Broker.GetCMImplementation(sigIdentity.Clid, sigIdentity.Csid) as IOrganizationUnit;
                        if (sigUnit != null)
                        {
                            IndividualEntity individualEntity = sigUnit.ClientEntity as IndividualEntity;

                            if (baseClientDetails.AddressKey == individualEntity.Fullname + " Residential Address")
                                SetAddresses(baseClientDetails, individualEntity.ResidentialAddress);
                            else if (baseClientDetails.AddressKey == individualEntity.Fullname + " Mailing Address")
                                SetAddresses(baseClientDetails, individualEntity.MailingAddress);

                            AddAddressToDict(baseClientDetails, individualEntity.ResidentialAddress, individualEntity.Fullname + " Residential Address");
                            AddAddressToDict(baseClientDetails, individualEntity.MailingAddress, individualEntity.Fullname + " Mailing Address");
                        }
                    }
                }
            }

            if (data is ClientValidationDS)
            {
                ClientValidationDS clientValidationDS = (ClientValidationDS)data;
                this.RunClientValidation(clientValidationDS);
            }
            else if (data is ExportAppFormsDS)
            {
                ExportAppFormsDS exportAppFormsDS = (ExportAppFormsDS)data;
                exportAppFormsDS.SearchByCID = true;
                exportAppFormsDS.ClientCID = this.CID;
                IOrganization orgCM = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                orgCM.GetData(exportAppFormsDS);
            }

            else if (data is BGLExportDS)
            {
                BGLExportDS bglExportDS = (BGLExportDS)data;
                bglExportDS.BGLAccountCode = this.bglAccountCode;
                if (bglExportDS.IsExport)
                {
                    IOrganization orgCM = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;

                    ObservableCollection<Oritax.TaxSimp.Common.CashManagementEntity> CashManagementTransactions = new ObservableCollection<Oritax.TaxSimp.Common.CashManagementEntity>();
                    ObservableCollection<DividendEntity> DividendCollection = new ObservableCollection<DividendEntity>();
                    ObservableCollection<DistributionIncomeEntity> DistributionIncomes = new ObservableCollection<DistributionIncomeEntity>();
                    BGLAccountMap bglAccountMap = new BGLAccountMap();
                    List<AccountProcessTaskEntity> ListAccountProcess = null;

                    if (this.IsCorporateType())
                    {
                        CorporateEntity corpEntity = this.ClientEntity as CorporateEntity;
                        DividendCollection = corpEntity.DividendCollection;
                        DistributionIncomes = corpEntity.DistributionIncomes;
                        ListAccountProcess = corpEntity.ListAccountProcess;
                    }

                    else
                    {
                        ClientIndividualEntity clientIndividualEntity = this.ClientEntity as ClientIndividualEntity;
                        DividendCollection = clientIndividualEntity.DividendCollection;
                        DistributionIncomes = clientIndividualEntity.DistributionIncomes;
                        ListAccountProcess = clientIndividualEntity.ListAccountProcess;
                    }

                    foreach (var accountProcessTaskEntity in ListAccountProcess)
                    {
                        if (accountProcessTaskEntity.LinkedEntity != null && accountProcessTaskEntity.LinkedEntity.Clid != Guid.Empty)
                        {
                            var asset = orgCM.Assets.Where(var => var.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                            var model = orgCM.Model.Where(mod => mod.ID == accountProcessTaskEntity.ModelID).FirstOrDefault();
                            var linkedAsset = model.Assets.Where(ast => ast.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                            var product = linkedAsset.Products.Where(pro => pro.ID == accountProcessTaskEntity.TaskID).FirstOrDefault();

                            if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.BankAccount)
                            {
                                IOrganizationUnit bankAccount = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as IOrganizationUnit;
                                Oritax.TaxSimp.Common.BankAccountEntity bankAccountEntity = bankAccount.ClientEntity as Oritax.TaxSimp.Common.BankAccountEntity;
                                IEnumerable<Oritax.TaxSimp.Common.CashManagementEntity> filteredTran = bankAccountEntity.CashManagementTransactions.Where(tran => tran.TransactionDate.Date <= bglExportDS.EndDate && tran.TransactionDate.Date >= bglExportDS.StartDate.Date);
                                foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in filteredTran)
                                {
                                    cashEntity.BGLAccountName = bankAccountEntity.Name;

                                    if (bankAccountEntity.BGLCodeCommissionRebate == null || bankAccountEntity.BGLCodeCommissionRebate == string.Empty)
                                        cashEntity.BGLCode = bankAccountEntity.BGLCode;
                                    else
                                        cashEntity.BGLCode = bankAccountEntity.BGLCodeCommissionRebate;

                                    if (bankAccountEntity.BGLCodeCashAccount == null || bankAccountEntity.BGLCodeCashAccount == string.Empty)
                                        cashEntity.BGLCodeCash = "604";
                                    else
                                        cashEntity.BGLCodeCash = bankAccountEntity.BGLCodeCashAccount;

                                    if (bankAccountEntity.BGLCodeSuspense == null || bankAccountEntity.BGLCodeSuspense == string.Empty)
                                        cashEntity.BGLCodeSuspense = "999";
                                    else
                                        cashEntity.BGLCodeSuspense = bankAccountEntity.BGLCodeSuspense;

                                    cashEntity.BGLAccountNo = bankAccountEntity.AccountNumber;
                                    CashManagementTransactions.Add(cashEntity);
                                }
                            }
                        }
                    }

                    ExportBGL(bglExportDS, this.bglAccountCode, this.name, this.ClientId, CashManagementTransactions, DividendCollection, DistributionIncomes, ListAccountProcess, orgCM, bglAccountMap);
                }
            }

            else if (data is OtherServicesDS)
            {
                OtherServicesDS otherServicesDS = (OtherServicesDS)data;
                otherServicesDS.IsBGL = IsBGL;
                otherServicesDS.IsClassSuper = IsClassSuper;
                otherServicesDS.IsDesktopSuper = IsDesktopSuper;
            }

            else if (data is FeeTransactionDetailsDS)
            {
                FeeTransactionDetailsDS feeTransactionDetailsDS = (FeeTransactionDetailsDS)data;
                this.OnGetData(feeTransactionDetailsDS);
            }

            else if (data is ClientFeesAssocDS)
            {
                ClientFeesAssocDS clientFeesAssocDS = (ClientFeesAssocDS)data;
                DataTable clientFeeAssociationTable = clientFeesAssocDS.Tables[ClientFeesAssocDS.CLIENTFEEASSOCIATIONSTABLE];
                IOrganization orgCM = this.Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;

                foreach (Guid feeID in this.ConfiguredFeesList)
                {
                    var feeTemplate = orgCM.FeeEnttyList.Where(feeTemp => feeTemp.ID == feeID).FirstOrDefault();

                    if (feeTemplate != null)
                    {
                        DataRow row = clientFeeAssociationTable.NewRow();
                        row[ClientFeesAssocDS.CLIENTID] = this.ClientId;
                        row[ClientFeesAssocDS.CLIENTCID] = this.CID;
                        row[ClientFeesAssocDS.FEETEMPLATECID] = feeID;
                        row[ClientFeesAssocDS.CLIENTNAME] = this.ClientId + " " + this.Name;
                        row[ClientFeesAssocDS.COMMENTS] = feeTemplate.Comments;
                        row[ClientFeesAssocDS.FEETYPE] = Enumeration.GetDescription(feeTemplate.FeeType);
                        row[ClientFeesAssocDS.DESCRIPTION] = feeTemplate.Description;
                        row[ClientFeesAssocDS.SERVICETYPE] = Enumeration.GetDescription(feeTemplate.ServiceTypes);
                        row[ClientFeesAssocDS.ADVISERNAME] = GetAdviserName();
                        clientFeeAssociationTable.Rows.Add(row);
                    }
                }
                if (ConfiguredFeesList.Count == 0)
                {
                    DataRow row = clientFeeAssociationTable.NewRow();
                    row[ClientFeesAssocDS.CLIENTID] = this.ClientId;
                    row[ClientFeesAssocDS.CLIENTCID] = this.CID;
                    row[ClientFeesAssocDS.FEETEMPLATECID] = Guid.Empty;
                    row[ClientFeesAssocDS.CLIENTNAME] = this.ClientId + " " + this.Name;
                    row[ClientFeesAssocDS.COMMENTS] = "No Fee Configured";
                    row[ClientFeesAssocDS.FEETYPE] = "No Fee Configured";
                    row[ClientFeesAssocDS.DESCRIPTION] = "No Fee Configured";
                    row[ClientFeesAssocDS.SERVICETYPE] = "No Fee Configured";
                    row[ClientFeesAssocDS.ADVISERNAME] = GetAdviserName();
                    clientFeeAssociationTable.Rows.Add(row);
                }
            }

            else if (data is DuplicatedAssociationsClientsDS)
            {
                DuplicatedAssociationsClientsDS duplicatedAssociationsClientsDS = (DuplicatedAssociationsClientsDS)data;

                if (this.ClientEntity is CorporateEntity || this.ClientEntity is Oritax.TaxSimp.Common.ClientIndividualEntity)
                {
                    IClientEntityOrgUnits clientEntityDetails = ClientEntity as IClientEntityOrgUnits;
                    var duplicated = clientEntityDetails.ListAccountProcess.Where(lp => lp.LinkedEntity.Clid != Guid.Empty && lp.LinkedEntity.Cid == Guid.Empty)
                                            .Where(lp => lp.LinkedEntityType != OrganizationType.DesktopBrokerAccount && lp.LinkedEntityType != OrganizationType.BankAccount).GroupBy(i => i.TaskID).Where(g => g.Count() > 1);

                    foreach (var dupItem in duplicated)
                    {
                        DataRow row = duplicatedAssociationsClientsDS.Tables[DuplicatedAssociationsClientsDS.DUPASSOCIATIONTABLE].NewRow();
                        row[DuplicatedAssociationsClientsDS.CLIENTCID] = this.CID;
                        row[DuplicatedAssociationsClientsDS.CLIENTNAME] = this.Name;
                        row[DuplicatedAssociationsClientsDS.PRODUCTID] = dupItem.Key;
                        row[DuplicatedAssociationsClientsDS.PRODUCTNAME] = String.Empty;
                        row[DuplicatedAssociationsClientsDS.PRODUCTDESC] = String.Empty;
                        row[DuplicatedAssociationsClientsDS.CLIENTID] = this.ClientId;
                        row[DuplicatedAssociationsClientsDS.HOLDINGTOTAL] = 0;
                        duplicatedAssociationsClientsDS.Tables[DuplicatedAssociationsClientsDS.DUPASSOCIATIONTABLE].Rows.Add(row);
                    }
                }
            }
            else if (data is P2ReportsDS)
            {
                var ds = data as P2ReportsDS;
                if (this.ClientEntity is CorporateEntity)
                {
                    var entity = this.ClientEntity as CorporateEntity;
                    if (entity.P2Reports != null)
                        FillP2ReportData(ds, entity.P2Reports);
                }
                else if (this.ClientEntity is ClientIndividualEntity)
                {
                    var entity = this.ClientEntity as ClientIndividualEntity;
                    if (entity.P2Reports != null)
                        FillP2ReportData(ds, entity.P2Reports);
                }
            }
            else if (data is SecurityTransferInReportDS)
            {
                var ds = data as SecurityTransferInReportDS;
                if (this.ClientEntity is CorporateEntity)
                {
                    var entity = this.ClientEntity as CorporateEntity;

                    FillSercuirtyTransferInData(ds, entity.DesktopBrokerAccounts);
                }
                else if (this.ClientEntity is ClientIndividualEntity)
                {
                    var entity = this.ClientEntity as ClientIndividualEntity;
                    FillSercuirtyTransferInData(ds, entity.DesktopBrokerAccounts);
                }
            }
            else if (data is GroupFeeBankwestExportDS)
            {
                GroupFeeBankwestExportDS groupFeeBankwestExportDS = (GroupFeeBankwestExportDS)data;
                var feeTran = FeeTransactions.Where(fee => fee.FeeRunID == groupFeeBankwestExportDS.FeeRunID).FirstOrDefault();
                if (feeTran != null)
                {
                    FeeRunTransactionsDetailsDS feeRunTransactionsDetailsDS = new Common.Data.FeeRunTransactionsDetailsDS();
                    IBrokerManagedComponent feeRun = this.Broker.GetBMCInstance(feeTran.FeeRunID);
                    feeRun.GetData(feeRunTransactionsDetailsDS);

                    List<AccountProcessTaskEntity> ListAccountProcess;
                    decimal difmFee = 0;
                    decimal diwmFee = 0;
                    decimal diyFee = 0;

                    if (this.IsCorporateType())
                    {
                        CorporateEntity entity = this.ClientEntity as CorporateEntity;
                        ListAccountProcess = entity.ListAccountProcess;
                    }
                    else
                    {
                        ClientIndividualEntity entity = this.ClientEntity as ClientIndividualEntity;
                        ListAccountProcess = entity.ListAccountProcess;
                    }

                    int daysInMonth = DateTime.DaysInMonth(feeTran.Year, feeTran.Month);

                    var difmHolding = feeTran.FeeHoldingEntityList.Where(holding => holding.ServiceTypes == ServiceTypes.DoItForMe);
                    var diwmHolding = feeTran.FeeHoldingEntityList.Where(holding => holding.ServiceTypes == ServiceTypes.DoItWithMe);
                    var diymHolding = feeTran.FeeHoldingEntityList.Where(holding => holding.ServiceTypes == ServiceTypes.DoItYourSelf);
                    var totalHoldingList = feeTran.FeeHoldingEntityList.Where(holding => holding.ServiceTypes == ServiceTypes.All);

                    HoldingRptDataSet holdingRptDataSet = new DataSets.HoldingRptDataSet();
                    holdingRptDataSet.ValuationDate = new DateTime(feeTran.Year, feeTran.Month, daysInMonth);
                    holdingRptDataSet.CheckForUnsettledCM = false;
                    this.GetData(holdingRptDataSet);

                    decimal difmHoldingTotal = holdingRptDataSet.HoldingSummaryTable.Select().Where(col =>
                                                col[holdingRptDataSet.HoldingSummaryTable.SERVICETYPE].ToString().ToLower() == Enumeration.GetDescription(ServiceTypes.DoItForMe).ToLower()).Sum(row => (decimal)row[holdingRptDataSet.HoldingSummaryTable.TOTAL]);

                    decimal diwmHoldingTotal = holdingRptDataSet.HoldingSummaryTable.Select().Where(col =>
                                                col[holdingRptDataSet.HoldingSummaryTable.SERVICETYPE].ToString().ToLower() == Enumeration.GetDescription(ServiceTypes.DoItWithMe).ToLower()).Sum(row => (decimal)row[holdingRptDataSet.HoldingSummaryTable.TOTAL]);
                    decimal diymHoldingTotal = holdingRptDataSet.HoldingSummaryTable.Select().Where(col =>
                                                col[holdingRptDataSet.HoldingSummaryTable.SERVICETYPE].ToString().ToLower() == Enumeration.GetDescription(ServiceTypes.DoItYourSelf).ToLower()).Sum(row => (decimal)row[holdingRptDataSet.HoldingSummaryTable.TOTAL]);
                    decimal totalHolding = difmHoldingTotal + diwmHoldingTotal + diymHoldingTotal;

                    decimal difmHoldingRatio = 0;
                    decimal diwmHoldingRatio = 0;
                    decimal diymHoldingRatio = 0;

                    if (totalHolding != 0)
                    {
                        difmHoldingRatio = (difmHoldingTotal / totalHolding);
                        diwmHoldingRatio = (diwmHoldingTotal / totalHolding);
                        diymHoldingRatio = (diymHoldingTotal / totalHolding);
                    }

                    List<FeeEntity> feeEntityListDIFM = new List<FeeEntity>();
                    List<FeeEntity> feeEntityListDIWM = new List<FeeEntity>();
                    List<FeeEntity> feeEntityListDIY = new List<FeeEntity>();
                    decimal difmHoldingTotalAllProducts = difmHolding.Sum(difm => difm.HoldingValue);
                    foreach (FeeHoldingEntity feeHoldingEntity in difmHolding)
                    {
                        CalculateFeeEntity(feeHoldingEntity.TotalHoldingValue, feeTran, groupFeeBankwestExportDS.FeeEnttyList, feeEntityListDIFM, feeHoldingEntity.HoldingValue, feeHoldingEntity.ServiceTypes, feeHoldingEntity.ProductID, feeHoldingEntity.TransactionDate, feeHoldingEntity.CalculatedRatio, feeHoldingEntity.ConsolHolding);
                    }

                    difmFee = feeEntityListDIFM.Sum(fee => fee.FeeTotalAdj);

                    decimal diwmHoldingTotalAllProducts = diwmHolding.Sum(diwm => diwm.HoldingValue);
                    foreach (FeeHoldingEntity feeHoldingEntity in diwmHolding)
                    {
                        CalculateFeeEntity(feeHoldingEntity.TotalHoldingValue, feeTran, groupFeeBankwestExportDS.FeeEnttyList, feeEntityListDIWM, feeHoldingEntity.HoldingValue, feeHoldingEntity.ServiceTypes, feeHoldingEntity.ProductID, feeHoldingEntity.TransactionDate, feeHoldingEntity.CalculatedRatio, feeHoldingEntity.ConsolHolding);
                    }

                    diwmFee = feeEntityListDIWM.Sum(fee => fee.FeeTotalAdj);

                    decimal diyHoldingTotalAllProducts = diymHolding.Sum(diy => diy.HoldingValue);

                    foreach (FeeHoldingEntity feeHoldingEntity in diymHolding)
                    {
                        CalculateFeeEntity(feeHoldingEntity.TotalHoldingValue, feeTran, groupFeeBankwestExportDS.FeeEnttyList, feeEntityListDIY, feeHoldingEntity.HoldingValue, feeHoldingEntity.ServiceTypes, feeHoldingEntity.ProductID, feeHoldingEntity.TransactionDate, feeHoldingEntity.CalculatedRatio, feeHoldingEntity.ConsolHolding);
                    }

                    diyFee = feeEntityListDIY.Sum(fee => fee.FeeTotalAdj);

                    List<FeeEntity> allFeeEntities = new List<FeeEntity>();

                    foreach (Guid feeEntityID in this.ConfiguredFeesList)
                    {
                        if (feeRunTransactionsDetailsDS.ConfiguredFeesTemplateAtRun.Contains(feeEntityID))
                        {

                            for (int day = 1; day <= daysInMonth; day++)
                            {
                                var holdingEnt = feeTran.FeeHoldingEntityList.Where(holding => holding.TransactionDate == new DateTime(feeTran.Year, feeTran.Month, day) && holding.ServiceTypes == ServiceTypes.All).FirstOrDefault();

                                if (holdingEnt != null && holdingEnt.TotalHoldingValue != 0)
                                {
                                    var feeEntity = groupFeeBankwestExportDS.FeeEnttyList.Where(fee => fee.ID == feeEntityID).FirstOrDefault();
                                    if (feeEntity != null)
                                    {
                                        FeeEntity feeEntityCopy = ObjectCopier.Clone(feeEntity);
                                        if (feeEntityCopy.OngoingFeeValueList.Count > 0)
                                        {
                                            feeEntityCopy.CalculateFeeOnGoingValue(feeTran.Month, feeTran.Year);
                                            allFeeEntities.Add(feeEntityCopy);
                                        }
                                    }
                                }
                            }
                        }
                    }


                    if (feeTran.UseLocalFee)
                    {
                        for (int day = 1; day <= daysInMonth; day++)
                        {
                            FeeEntity feeEntityFromClient = FeeEntity.GetFeeEntity(ClientFeeEntity, this.ClientId, feeTran.Month, feeTran.Year);
                            FeeEntity feeEntityCopy = ObjectCopier.Clone(feeEntityFromClient);

                            var holdingEnt = feeTran.FeeHoldingEntityList.Where(holding => holding.TransactionDate == new DateTime(feeTran.Year, feeTran.Month, day) && holding.ServiceTypes == ServiceTypes.All).FirstOrDefault();

                            if (holdingEnt != null && holdingEnt.TotalHoldingValue != 0)
                            {

                                if (feeEntityCopy.OngoingFeeValueList.Count > 0)
                                {
                                    feeEntityCopy.CalculateFeeOnGoingValue(feeTran.Month, feeTran.Year);
                                    if (feeEntityCopy.FeeTotalAdj != 0)
                                        allFeeEntities.Add(feeEntityCopy);
                                }
                            }

                        }

                        var feeHoldingEntities = totalHoldingList.Where(fee => fee.ServiceTypes == ServiceTypes.All);

                        foreach (FeeHoldingEntity feeHoldingEntity in totalHoldingList)
                        {
                            FeeEntity feeEntityFromClientNonValue = FeeEntity.GetFeeEntity(ClientFeeEntity, this.ClientId, feeTran.Month, feeTran.Year);
                            feeEntityFromClientNonValue.CalculateFee(feeHoldingEntity.HoldingValue, feeHoldingEntity.TotalHoldingValue, feeHoldingEntity.ConsolHolding, feeTran.Month, feeTran.Year, feeHoldingEntity.CalculatedRatio);
                            FeeEntity feeEntityCopy = ObjectCopier.Clone(feeEntityFromClientNonValue);
                            if (feeEntityCopy.FeeTotalAdj != 0)
                                allFeeEntities.Add(feeEntityCopy);
                        }
                    }

                    decimal allPlatformFee = allFeeEntities.Sum(fee => fee.FeeTotalAdj);

                    bool hasDIWMAccount = false;
                    bool modeHasDIWMAccount = false;

                    foreach (var accountProcessTaskEntity in ListAccountProcess)
                    {
                        if (accountProcessTaskEntity.LinkedEntity != null)
                        {
                            var model = groupFeeBankwestExportDS.Models.Where(mod => mod.ID == accountProcessTaskEntity.ModelID).FirstOrDefault();
                            if (model.ServiceType == ServiceTypes.DoItWithMe)
                            {
                                if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.BankAccount && accountProcessTaskEntity.LinkedEntity.Clid != Guid.Empty)
                                {
                                    modeHasDIWMAccount = true;

                                    var asset = groupFeeBankwestExportDS.Assets.Where(var => var.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                                    var product = asset.Products.Where(pro => pro.ID == accountProcessTaskEntity.TaskID).FirstOrDefault();

                                    if (accountProcessTaskEntity.LinkedEntity.Clid == Guid.Empty)
                                    {
                                        accountProcessTaskEntity.LinkedEntity.Clid = product.EntityId.Clid;
                                        accountProcessTaskEntity.LinkedEntity.Csid = product.EntityId.Csid;
                                    }

                                    IOrganizationUnit bankAccount =
                                          Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid,
                                                                     accountProcessTaskEntity.LinkedEntity.Csid) as IOrganizationUnit;

                                    if (bankAccount != null)
                                        hasDIWMAccount = true;
                                }
                            }
                        }
                    }

                    if (!modeHasDIWMAccount && diwmHoldingTotal != 0 && !hasDIWMAccount && !feeTran.UseLocalFee)
                        SetFeeDIWM(groupFeeBankwestExportDS, diwmFee, diwmHoldingRatio, allPlatformFee);

                    if (!feeTran.UseLocalFee)
                        SetFee(groupFeeBankwestExportDS, ListAccountProcess, difmFee, diwmFee, diyFee, difmHoldingRatio, diwmHoldingRatio, diymHoldingRatio, allPlatformFee);
                    else
                        SetAdviserFee(groupFeeBankwestExportDS, ListAccountProcess, difmFee, diwmFee, diyFee, allPlatformFee);
                }
            }
            else if (data is ClientReportDataValidationDS)
            {
                if (ClientEntity is CorporateEntity || ClientEntity is Oritax.TaxSimp.Common.ClientIndividualEntity)
                {
                    var ds = data as ClientReportDataValidationDS;
                    ds.ClientCount++;


                    var holdingSummaryDs = new HoldingSummaryReportDS { StartDate = ds.StartDate, EndDate = ds.EndDate };
                    var holdingRptDs = new HoldingRptDataSet { ValuationDate = ds.EndDate };
                    // Same for Assets class SUmmary report
                    var securityPerformanceSummaryDs = new PerfReportDS
                    {
                        IsRange = true,
                        EndDate = ds.EndDate,
                        StartDate = ds.StartDate
                    };

                    //same for  PORTFOLIO SUMMARY ,Portfolio performance details Report and same for  Income vs Growth Report
                    var securityPerformanceDetailDs = new PerfReportDS { EndDate = ds.EndDate, StartDate = ds.StartDate };

                    var secuiritySummaryReportDs = new SecuritySummaryReportDS() { ValuationDate = ds.EndDate };
                    var capitalReportDs = new CapitalReportDS { EndDate = ds.EndDate, StartDate = ds.StartDate };

                    GetData(holdingSummaryDs);
                    GetData(holdingRptDs);
                    GetData(securityPerformanceSummaryDs);
                    GetData(securityPerformanceDetailDs);
                    GetData(secuiritySummaryReportDs);
                    GetData(capitalReportDs);

                    #region Closing balances check for Client Reports Task:1177

                    string message;
                    decimal holdingStatementTotal = GetSumOfColumn(holdingSummaryDs.securitySummaryTable,
                                                                   holdingSummaryDs.securitySummaryTable.CLOSINGBAL,
                                                                   string.Empty);
                    decimal holdingReportTotal = GetSumOfColumn(holdingRptDs.HoldingSummaryTable,
                                                                holdingRptDs.HoldingSummaryTable.HOLDING, string.Empty);
                    decimal securityPerformanceSummaryClosingBalTotalLine =
                        GetSumOfColumn(securityPerformanceSummaryDs.Tables[PerfReportDS.PERFSUMMARYTABLEBYSEC],
                                       PerfReportDS.CLOSINGBALANCE, string.Empty);
                    decimal securityPerformanceDetailsTotalLine =
                        GetSumOfColumn(securityPerformanceDetailDs.Tables[PerfReportDS.PERFSUMMARYTABLEBYSEC],
                                       PerfReportDS.CLOSINGBALANCE, string.Empty);


                    decimal portfolioPerformanceSummaryTotalLine = 0;
                    decimal portfolioPerformanceSummaryFirstLine = 0;
                    decimal portfolioPerformanceDetailsTotalLine = 0;
                    decimal portfolioPerformanceDetailsFirstLine = 0;

                    {
                        var portfoliosummaryTable =
                            securityPerformanceDetailDs.Tables[CapitalReportDS.CAPITALFLOWSUMMARYCAT].DefaultView;
                        portfoliosummaryTable.Sort = CapitalReportDS.MONTH + " DESC";
                        if (portfoliosummaryTable.Count > 0)
                        {
                            portfolioPerformanceDetailsTotalLine = portfolioPerformanceSummaryTotalLine = portfolioPerformanceSummaryFirstLine =
                                portfolioPerformanceDetailsFirstLine =
                                (decimal)portfoliosummaryTable[0][CapitalReportDS.CLOSINGBALANCE];
                        }
                    }

                    decimal securitySummaryTotalLine = 0;
                    {
                        DataView secSummaryTableView =
                            new DataView(secuiritySummaryReportDs.Tables[SecuritySummaryReportDS.SECURITYSUMMARYTABLE]);
                        secSummaryTableView.Sort = SecuritySummaryReportDS.SECNAME + " ASC";
                        //filtering 0 value securities 
                        secSummaryTableView.RowFilter = string.Format("CONVERT(Isnull({0},''), System.String) <> '0'",
                                                                      SecuritySummaryReportDS.TOTALUNITS);
                        DataTable secFilteredTable = secSummaryTableView.ToTable();
                        securitySummaryTotalLine = GetSumOfColumn(secFilteredTable, SecuritySummaryReportDS.TOTAL,
                                                                  string.Empty);
                    }
                    decimal assetsSummaryTotalLine = GetSumOfColumn(holdingRptDs.HoldingSummaryTable,
                                                                    holdingRptDs.HoldingSummaryTable.HOLDING,
                                                                    string.Empty);


                    decimal capitalMovementFirstLine = 0;
                    {
                        var capitalflowsTable =
                            capitalReportDs.Tables[CapitalReportDS.CAPITALFLOWSUMMARYCAT].DefaultView;
                        capitalflowsTable.Sort = CapitalReportDS.MONTH + " DESC";
                        if (capitalflowsTable.Count > 0)
                        {
                            capitalMovementFirstLine = (decimal)capitalflowsTable[0][CapitalReportDS.CLOSINGBALANCE];

                        }
                    }

                    decimal incomevsGrowth1Yearline = 0;
                    {
                        var perInomevsGrowthYearTable =
                            securityPerformanceDetailDs.Tables[PerfReportDS.PERFSUMMARYINCOMEGROWTHTABLE].DefaultView;
                        perInomevsGrowthYearTable.Sort = PerfReportDS.ENDDATE + " DESC";
                        if (perInomevsGrowthYearTable.Count > 0)
                        {
                            incomevsGrowth1Yearline =
                                (decimal)perInomevsGrowthYearTable[0][PerfReportDS.CLOSINGBALANCE];

                        }
                    }
                    decimal incomevsGrowthFirstLine = 0;
                    decimal incomevsGrowthTotalline = 0;
                    {
                        var capitalSummaryCatTable =
                            securityPerformanceDetailDs.Tables[CapitalReportDS.CAPITALFLOWSUMMARYCAT].DefaultView;
                        capitalSummaryCatTable.Sort = CapitalReportDS.MONTH + " DESC";
                        if (capitalSummaryCatTable.Count > 0)
                        {
                            incomevsGrowthTotalline =
                                incomevsGrowthFirstLine =
                                (decimal)capitalSummaryCatTable[0][CapitalReportDS.CLOSINGBALANCE];
                        }
                    }

                    if (!CheckClosingBalances(holdingStatementTotal, holdingReportTotal,
                                              securityPerformanceSummaryClosingBalTotalLine,
                                              securityPerformanceDetailsTotalLine, portfolioPerformanceSummaryTotalLine,
                                              portfolioPerformanceSummaryFirstLine, portfolioPerformanceDetailsTotalLine,
                                              portfolioPerformanceDetailsFirstLine, securitySummaryTotalLine,
                                              assetsSummaryTotalLine, capitalMovementFirstLine, incomevsGrowthFirstLine,
                                              incomevsGrowth1Yearline, incomevsGrowthTotalline, out message))
                    {
                        ds.ClientReportShareTable.AddRow(EclipseClientID, Name, CID, CLID, CSID,
                                                         "1.Closing balance (Total for all services)", message);
                    }

                    #endregion

                    #region Opening Balance 2   Task:Innova-1178

                    decimal openingBalance = GetSumOfColumn(holdingSummaryDs.securitySummaryTable,
                                                            holdingSummaryDs.securitySummaryTable.OPENINGBALANCE,
                                                            string.Empty);

                    //Income Vs Growth Report Yearly
                    decimal icomeGrothYearlyOpening = 0;
                    {
                        var perfIcomeGrowth = securityPerformanceDetailDs.Tables[PerfReportDS.PERFSUMMARYINCOMEGROWTHTABLE].DefaultView;
                        perfIcomeGrowth.Sort = PerfReportDS.ENDDATE + " ASC";
                        if (perfIcomeGrowth.Count > 0)
                        {
                            icomeGrothYearlyOpening = (decimal)perfIcomeGrowth[0][PerfReportDS.OPENINGBAL];
                        }
                    }

                    //Income Vs Growth Report Monthly
                    decimal iComeGrowthMonthlyOpening = 0;
                    {
                        var perfIcomeGrowth = securityPerformanceDetailDs.Tables[CapitalReportDS.CAPITALFLOWSUMMARYCAT].DefaultView;
                        perfIcomeGrowth.Sort = CapitalReportDS.MONTH + " ASC";
                        if (perfIcomeGrowth.Count > 0)
                        {
                            iComeGrowthMonthlyOpening = (decimal)perfIcomeGrowth[0][CapitalReportDS.OPENINGBAL];
                        }
                    }

                    //Protfolio Detail OPening Balance
                    decimal protfolioDeatilOpeningBalance = 0;
                    {
                        var capitalSummaryCatTable =
                            securityPerformanceDetailDs.Tables[CapitalReportDS.CAPITALFLOWSUMMARYCAT].DefaultView;
                        capitalSummaryCatTable.Sort = CapitalReportDS.MONTH + " ASC";
                        if (capitalSummaryCatTable.Count > 0)
                        {
                            protfolioDeatilOpeningBalance = (decimal)capitalSummaryCatTable[0][CapitalReportDS.OPENINGBAL];
                        }
                    }

                    //Portfolio Summary Detail Opening Balance
                    decimal protfolioSummaryOpeningBalance = 0;
                    {
                        var capitalSummaryCatTable =
                            securityPerformanceDetailDs.Tables[CapitalReportDS.CAPITALFLOWSUMMARYCAT].DefaultView;
                        capitalSummaryCatTable.Sort = CapitalReportDS.MONTH + " ASC";
                        if (capitalSummaryCatTable.Count > 0)
                        {
                            protfolioSummaryOpeningBalance = (decimal)capitalSummaryCatTable[0][CapitalReportDS.OPENINGBAL];
                        }
                    }
                    //Security Detail Opening Balance
                    decimal securityDetailOpeningBalance =
                        GetSumOfColumn(securityPerformanceDetailDs.Tables[PerfReportDS.PERFSUMMARYTABLEBYSEC],
                                       PerfReportDS.OPENINGBAL, string.Empty);
                    //Security Summary Opening Balance
                    decimal securitySummaryOpeningBalance =
                        GetSumOfColumn(securityPerformanceDetailDs.Tables[PerfReportDS.PERFSUMMARYTABLEBYSEC],
                                       PerfReportDS.OPENINGBAL, string.Empty);


                    if (!CheckOpeningBalances(openingBalance, Convert.ToDecimal(icomeGrothYearlyOpening),
                                              iComeGrowthMonthlyOpening, protfolioDeatilOpeningBalance,
                                              protfolioSummaryOpeningBalance, securityDetailOpeningBalance,
                                              securitySummaryOpeningBalance, out message))
                    {
                        ds.ClientReportShareTable.AddRow(EclipseClientID, Name, CID, CLID, CSID,
                                                         "2.Opening balance (Total for all services)", message);
                    }

                    #endregion

                    #region Closing Balance for DIFM Point Number3 Task:Innova-1179

                    //For Point 3 
                    //Closing Balance Holding Summary
                    decimal holdingSummaryClosingBalanceTotal = GetSumOfColumn(holdingRptDs.HoldingSummaryTable,
                                                                               holdingRptDs.HoldingSummaryTable.HOLDING,
                                                                               "ServiceType = 'Do It For Me'");

                    //Closing Balance Holding Summary DIFM
                    decimal holdingSummaryDifmTotal = GetSumOfColumn(holdingRptDs.HoldingSummaryTable,
                                                                     holdingRptDs.HoldingSummaryTable.HOLDING,
                                                                     "ServiceType = 'Do It For Me'");

                    //Closing Balance PortfoliPerformanceDetail Start Closing 
                    decimal protfoliPerfDetaiClosing = 0;
                    {
                        var perfDIFM = securityPerformanceDetailDs.Tables[PerfReportDS.DIFMPERFTABLE].DefaultView;
                        perfDIFM.Sort = PerfReportDS.MONTH + " DESC";
                        if (perfDIFM.Count > 0)
                        {
                            protfoliPerfDetaiClosing = (decimal)perfDIFM[0][PerfReportDS.CLOSINGBALANCE];

                        }
                    }

                    //Closing Balance PortfoliPerformanceDetail Closing Total
                    decimal protfoliPerfDetalTotalClosing = 0;
                    {
                        var perfDIFM = securityPerformanceDetailDs.Tables[PerfReportDS.DIFMPERFTABLE].DefaultView;
                        perfDIFM.Sort = PerfReportDS.MONTH + " DESC";
                        if (perfDIFM.Count > 0)
                        {
                            protfoliPerfDetalTotalClosing = (decimal)perfDIFM[0][PerfReportDS.CLOSINGBALANCE];

                        }
                    }

                    //Closing Balance Capital Movement
                    decimal capitalReportClosingBal =
                        GetSumOfColumn(capitalReportDs.Tables[CapitalReportDS.CAPITALFLOWSUMMARY],
                                       CapitalReportDS.CLOSINGBALANCE,
                                       "ServiceType = 'Do It For Me'");


                    if (!CheckClosingBalancesDifm(holdingSummaryClosingBalanceTotal, holdingSummaryDifmTotal,
                                                  protfoliPerfDetaiClosing, protfoliPerfDetalTotalClosing,
                                                  capitalReportClosingBal, out message))
                    {
                        ds.ClientReportShareTable.AddRow(EclipseClientID, Name, CID, CLID, CSID,
                                                         "3.Closing balance (DIFM) ", message);
                    }

                    #endregion

                    #region Client Validation Report Point 6 Task:Innova-1182

                    securityPerformanceSummaryDs.Tables[PerfReportDS.PERFSUMMARYTABLEBYSEC].CaseSensitive = false;
                    decimal SecurityPerformanceSummaryTotalIncome = GetSumOfColumn(securityPerformanceSummaryDs.Tables[PerfReportDS.PERFSUMMARYTABLEBYSEC], PerfReportDS.INCOME, string.Empty);
                    decimal SecurityPerformanceSummaryTotalOtherMovement = GetSumOfColumn(securityPerformanceSummaryDs.Tables[PerfReportDS.PERFSUMMARYTABLEBYSEC], PerfReportDS.OTHER, string.Empty);

                    SecurityPerformanceSummaryTotalOtherMovement += GetSumOfColumn(securityPerformanceSummaryDs.Tables[PerfReportDS.PERFSUMMARYTABLEBYSEC], PerfReportDS.INCOME, string.Format("{0}='Cash' OR {0}='TDS'", PerfReportDS.TYPE));
                    SecurityPerformanceSummaryTotalOtherMovement += GetSumOfColumn(securityPerformanceSummaryDs.Tables[PerfReportDS.PERFSUMMARYTABLEBYSEC], PerfReportDS.INTERNALCASHMOVEMENT, string.Format("{0}='Cash' OR {0}='TDS'", PerfReportDS.TYPE));

                    if (!CheckSecuritySummayrTotals(SecurityPerformanceSummaryTotalIncome, SecurityPerformanceSummaryTotalOtherMovement, out message))
                    {
                        ds.ClientReportShareTable.AddRow(EclipseClientID, Name, CID, CLID, CSID, "6.Security Performance Summary (Total Income = Total Other Movement) ", message);
                    }

                    #endregion

                    #region Closing Balance DIY For Point 4 Task:Innova-1180

                    //Closing Balance By the  Services Type Do It Yourself (DIY) For Holding Summary
                    decimal holdingSummClosingBalTotalDIY = GetSumOfColumn(holdingRptDs.HoldingSummaryTable, holdingRptDs.HoldingSummaryTable.HOLDING, "ServiceType = 'Do It Yourself'");
                    ////Get ClosingBalanceTotal for DIY
                    decimal holdingSummClosingBalDIY = GetSumOfColumn(holdingRptDs.HoldingSummaryTable, holdingRptDs.HoldingSummaryTable.HOLDING, "ServiceType = 'Do It Yourself'");
                    ////Get Closing Balance From ProtfoiloPerformanceDetail For DIY (First or Default)
                    decimal protfolioPerfClosingBalDIY = 0;
                    {
                        var perfDIYRows = securityPerformanceDetailDs.Tables[PerfReportDS.DIYPERFTABLE].DefaultView;
                        perfDIYRows.Sort = PerfReportDS.MONTH + " DESC";
                        if (perfDIYRows.Count > 0)
                        {
                            protfolioPerfClosingBalDIY = (decimal)perfDIYRows[0][CapitalReportDS.CLOSINGBALANCE];
                        }
                    }
                    ////Get Closing Balance From ProtfoiloPerformanceDetail For DIY (Total)
                    decimal protfolioPerfClosingBalTotalDIY = 0;
                    {
                        var perfDIYRows = securityPerformanceDetailDs.Tables[PerfReportDS.DIYPERFTABLE].DefaultView;
                        perfDIYRows.Sort = PerfReportDS.MONTH + " DESC";
                        if (perfDIYRows.Count > 0)
                            protfolioPerfClosingBalTotalDIY = (decimal)perfDIYRows[0][CapitalReportDS.CLOSINGBALANCE];
                    }
                    ////Get Capital Movement Summary Total For DIY
                    decimal capMoveSumTotalDIY = GetSumOfColumn(capitalReportDs.Tables[CapitalReportDS.CAPITALFLOWSUMMARY], CapitalReportDS.CLOSINGBALANCE, "ServiceType = 'Do It Yourself'");
                    if (
                        !CheckClosingBalanceDiy(holdingSummClosingBalTotalDIY, holdingSummClosingBalDIY, protfolioPerfClosingBalDIY, protfolioPerfClosingBalTotalDIY, capMoveSumTotalDIY, out message))
                    {
                        ds.ClientReportShareTable.AddRow(EclipseClientID, Name, CID, CLID, CSID, "4.Closing balance (DIY)  ", message);
                    }

                    #endregion

                    #region Closing Balance DIWM For Point 5 Task:Innova-1181

                    //Check the Report type services type DIWM for Holding Report
                    decimal holdingSummTotalDIWM = GetSumOfColumn(holdingRptDs.HoldingSummaryTable, holdingRptDs.HoldingSummaryTable.HOLDING, "ServiceType = 'Do It With Me'");

                    //Get ClosingBalanceTotal for DIWM
                    decimal holdingSummClosingBalDIWM = GetSumOfColumn(holdingRptDs.HoldingSummaryTable, holdingRptDs.HoldingSummaryTable.HOLDING, "ServiceType = 'Do It With Me'");

                    //Get Closing Balance From ProtfoiloPerformanceDetail For DIWM (First or Default)
                    decimal protfoli0PerfClosingBalFirstDefaultDIWM = 0;
                    {
                        var perfDiwmRows = securityPerformanceDetailDs.Tables[PerfReportDS.DIWMPERFTABLE].DefaultView;
                        perfDiwmRows.Sort = PerfReportDS.MONTH + " DESC";
                        if (perfDiwmRows.Count > 0)
                        {
                            protfoli0PerfClosingBalFirstDefaultDIWM = (decimal)perfDiwmRows[0][CapitalReportDS.CLOSINGBALANCE];
                        }
                    }

                    //Get Closing Balance From ProtfoiloPerformaceDetail For DIWM (First or Default)
                    decimal protfoli0PerfClosingBalTotalDIWM = 0;
                    {
                        var perfDiwmRows = securityPerformanceDetailDs.Tables[PerfReportDS.DIWMPERFTABLE].DefaultView;
                        perfDiwmRows.Sort = PerfReportDS.MONTH + " DESC";
                        if (perfDiwmRows.Count > 0)
                            protfoli0PerfClosingBalTotalDIWM = (decimal)perfDiwmRows[0][CapitalReportDS.CLOSINGBALANCE];
                    }

                    //Get Capital Movement Summary Total For DIWM
                    decimal capMoveSumTotalDiwm =
                        GetSumOfColumn(capitalReportDs.Tables[CapitalReportDS.CAPITALFLOWSUMMARY], CapitalReportDS.CLOSINGBALANCE, "ServiceType = 'Do It With Me'");

                    if (!CheckClosingBalanceDiwm(holdingSummTotalDIWM, holdingSummClosingBalDIWM, protfoli0PerfClosingBalFirstDefaultDIWM, protfoli0PerfClosingBalTotalDIWM, capMoveSumTotalDiwm, out message))
                    {
                        ds.ClientReportShareTable.AddRow(EclipseClientID, Name, CID, CLID, CSID, "5.Closing balance (DIWM)  ", message);
                    }

                    #endregion

                    #region Total Line Calculation Point 7 Security Performance Summary (Total Line) Task:Innova-1183 (Part 1)

                    //Here we get the Total Line for Security Performance Summary
                    decimal openingBal = GetSumOfColumn(securityPerformanceSummaryDs.Tables[PerfReportDS.PERFSUMMARYTABLEBYSEC], PerfReportDS.OPENINGBAL, string.Empty);

                    decimal salePerPur = GetSumOfColumn(securityPerformanceSummaryDs.Tables[PerfReportDS.PERFSUMMARYTABLEBYSEC], PerfReportDS.REDEMPTION, string.Empty);
                    decimal transInOut = GetSumOfColumn(securityPerformanceSummaryDs.Tables[PerfReportDS.PERFSUMMARYTABLEBYSEC], PerfReportDS.TRANSFEROUT, string.Empty);

                    decimal fees = GetSumOfColumn(securityPerformanceSummaryDs.Tables[PerfReportDS.PERFSUMMARYTABLEBYSEC], PerfReportDS.EXPENSE, string.Empty);

                    decimal otherMvt = GetSumOfColumn(securityPerformanceSummaryDs.Tables[PerfReportDS.PERFSUMMARYTABLEBYSEC],
                                       PerfReportDS.OTHER, string.Empty);

                    otherMvt += GetSumOfColumn(securityPerformanceSummaryDs.Tables[PerfReportDS.PERFSUMMARYTABLEBYSEC], PerfReportDS.INCOME, string.Format("{0}='Cash' OR {0}='TDS'", PerfReportDS.TYPE));
                    otherMvt += GetSumOfColumn(securityPerformanceSummaryDs.Tables[PerfReportDS.PERFSUMMARYTABLEBYSEC], PerfReportDS.INTERNALCASHMOVEMENT, string.Format("{0}='Cash' OR {0}='TDS'", PerfReportDS.TYPE));


                    decimal mvtValChange = GetSumOfColumn(securityPerformanceSummaryDs.Tables[PerfReportDS.PERFSUMMARYTABLEBYSEC], PerfReportDS.CHANGEININVESTMENTVALUE, string.Empty);

                    decimal closingBal = GetSumOfColumn(securityPerformanceSummaryDs.Tables[PerfReportDS.PERFSUMMARYTABLEBYSEC], PerfReportDS.CLOSINGBALANCE, string.Empty);

                    decimal totalAll = openingBal + salePerPur + transInOut + fees + otherMvt + mvtValChange;

                    if (!CheckClosingBalSecPerfReport(closingBal, totalAll, out message))
                    {
                        ds.ClientReportShareTable.AddRow(EclipseClientID, Name, CID, CLID, CSID, "7.Security Performance Summary (Total Line) ", message);
                    }



                    #endregion

                    #region Total Line Calculation Point 7 Portfolio performance details (Total Line) Task:Innova-1183 (Part2)

                    #region DIY
                    //opening Bal
                    decimal openingBalDiy = 0;
                    {
                        var perfDiy = securityPerformanceDetailDs.Tables[PerfReportDS.DIYPERFTABLE].DefaultView;
                        perfDiy.Sort = PerfReportDS.MONTH + " ASC";
                        if (perfDiy.Count > 0)
                        {
                            openingBalDiy = (decimal)perfDiy[0][CapitalReportDS.OPENINGBAL];
                        }
                    }

                    //SalePerPur
                    decimal salePerPurDiy = GetSumOfColumn(securityPerformanceDetailDs.Tables[PerfReportDS.DIYPERFTABLE],
                                                          PerfReportDS.APPLICATION, string.Empty);
                    salePerPurDiy += GetSumOfColumn(securityPerformanceDetailDs.Tables[PerfReportDS.DIYPERFTABLE],
                                                             PerfReportDS.REDEMPTION, string.Empty);

                    //TranInOut
                    decimal tranInOutDiy = GetSumOfColumn(securityPerformanceDetailDs.Tables[PerfReportDS.DIYPERFTABLE], CapitalReportDS.TRANSFEROUT, "InvestmentCode = 'Do It Yourself'");

                    //Fee
                    decimal FeeDiy = GetSumOfColumn(securityPerformanceDetailDs.Tables[PerfReportDS.DIYPERFTABLE], CapitalReportDS.EXPENSE, "InvestmentCode = 'Do It Yourself'");

                    //Other Mvt
                    decimal otherMvtDiy = GetSumOfColumn(securityPerformanceDetailDs.Tables[PerfReportDS.DIYPERFTABLE], CapitalReportDS.INTERNALCASHMOVEMENT, "InvestmentCode = 'Do It Yourself'");
                    //Mak Value Change
                    decimal makValueChangeDiy = GetSumOfColumn(securityPerformanceDetailDs.Tables[PerfReportDS.DIYPERFTABLE], CapitalReportDS.CHANGEININVESTMENTVALUE, "InvestmentCode = 'Do It Yourself'");

                    //Income
                    decimal IncomeDiy = GetSumOfColumn(securityPerformanceDetailDs.Tables[PerfReportDS.DIYPERFTABLE], CapitalReportDS.INCOME, "InvestmentCode = 'Do It Yourself'");

                    //Closing Bal
                    decimal closingBalDiy = 0;
                    {
                        var perfClosingDiy = securityPerformanceDetailDs.Tables[PerfReportDS.DIYPERFTABLE].DefaultView;
                        perfClosingDiy.Sort = PerfReportDS.MONTH + " DESC";
                        if (perfClosingDiy.Count > 0)
                        {
                            closingBalDiy = (decimal)perfClosingDiy[0][CapitalReportDS.CLOSINGBALANCE];
                        }
                    }

                    decimal total = openingBalDiy + salePerPurDiy + tranInOutDiy + FeeDiy + otherMvtDiy + IncomeDiy + makValueChangeDiy;

                    if (!CheckClosingBalSecPerfReport(closingBalDiy, total, out message))
                    {
                        ds.ClientReportShareTable.AddRow(EclipseClientID, Name, CID, CLID, CSID, "7.Portfolio performance details-DIY (Total Line)  ", message);
                    }
                    #endregion

                    #region DIFM
                    //opening Bal
                    decimal openingBalDifm = 0;
                    {
                        var perfDifm = securityPerformanceDetailDs.Tables[PerfReportDS.DIFMPERFTABLE].DefaultView;
                        perfDifm.Sort = PerfReportDS.MONTH + " ASC";
                        if (perfDifm.Count > 0)
                        {
                            openingBalDifm = (decimal)perfDifm[0][CapitalReportDS.OPENINGBAL];
                        }
                    }

                    //SalePerPur
                    decimal salePerPurDifm = GetSumOfColumn(securityPerformanceDetailDs.Tables[PerfReportDS.DIFMPERFTABLE],
                                                         PerfReportDS.APPLICATION, string.Empty);
                    salePerPurDifm += GetSumOfColumn(securityPerformanceDetailDs.Tables[PerfReportDS.DIFMPERFTABLE],
                                                             PerfReportDS.REDEMPTION, string.Empty);

                    //TranInOut
                    decimal tranInOutDifm = GetSumOfColumn(securityPerformanceDetailDs.Tables[PerfReportDS.DIFMPERFTABLE], CapitalReportDS.TRANSFEROUT, "InvestmentCode = 'Do It For Me'");

                    //Fee
                    decimal FeeDifm = GetSumOfColumn(securityPerformanceDetailDs.Tables[PerfReportDS.DIFMPERFTABLE], CapitalReportDS.EXPENSE, "InvestmentCode = 'Do It For Me'");

                    //Other Mvt
                    decimal otherMvtDifm = GetSumOfColumn(securityPerformanceDetailDs.Tables[PerfReportDS.DIFMPERFTABLE], CapitalReportDS.INTERNALCASHMOVEMENT, "InvestmentCode = 'Do It For Me'");
                    //Mak Value Change
                    decimal makValueChangeDifm = GetSumOfColumn(securityPerformanceDetailDs.Tables[PerfReportDS.DIFMPERFTABLE], CapitalReportDS.CHANGEININVESTMENTVALUE, "InvestmentCode = 'Do It For Me'");

                    //Income
                    decimal IncomeDifm = GetSumOfColumn(securityPerformanceDetailDs.Tables[PerfReportDS.DIFMPERFTABLE], CapitalReportDS.INCOME, "InvestmentCode = 'Do It For Me'");

                    //Closing Bal
                    decimal closingBalDifm = 0;
                    {
                        var perfClosingDifm = securityPerformanceDetailDs.Tables[PerfReportDS.DIFMPERFTABLE].DefaultView;
                        perfClosingDifm.Sort = PerfReportDS.MONTH + " DESC";
                        if (perfClosingDifm.Count > 0)
                        {
                            closingBalDifm = (decimal)perfClosingDifm[0][CapitalReportDS.CLOSINGBALANCE];
                        }
                    }

                    decimal totalDifm = openingBalDifm + salePerPurDifm + tranInOutDifm + FeeDifm + otherMvtDifm + IncomeDifm + makValueChangeDifm;

                    if (!CheckClosingBalSecPerfReport(closingBalDifm, totalDifm, out message))
                    {
                        ds.ClientReportShareTable.AddRow(EclipseClientID, Name, CID, CLID, CSID, "7.Portfolio performance details-DIFM (Total Line)  ", message);
                    }
                    #endregion

                    #region DIWM
                    //opening Bal
                    decimal openingBalDiwm = 0;
                    {
                        var perfDifw = securityPerformanceDetailDs.Tables[PerfReportDS.DIWMPERFTABLE].DefaultView;
                        perfDifw.Sort = PerfReportDS.MONTH + " ASC";
                        if (perfDifw.Count > 0)
                        {
                            openingBalDiwm = (decimal)perfDifw[0][CapitalReportDS.OPENINGBAL];
                        }
                    }

                    //SalePerPur
                    decimal salePerPurDiwm = GetSumOfColumn(securityPerformanceDetailDs.Tables[PerfReportDS.DIWMPERFTABLE], PerfReportDS.APPLICATION, string.Empty);
                    salePerPurDiwm += GetSumOfColumn(securityPerformanceDetailDs.Tables[PerfReportDS.DIWMPERFTABLE], PerfReportDS.REDEMPTION, string.Empty);

                    //TranInOut
                    decimal tranInOutDiwm = GetSumOfColumn(securityPerformanceDetailDs.Tables[PerfReportDS.DIWMPERFTABLE], CapitalReportDS.TRANSFEROUT, "InvestmentCode = 'Do It With Me'");

                    //Fee
                    decimal feeDiwm = GetSumOfColumn(securityPerformanceDetailDs.Tables[PerfReportDS.DIWMPERFTABLE], CapitalReportDS.EXPENSE, "InvestmentCode = 'Do It With Me'");

                    //Other Mvt
                    decimal otherMvtDiwm = GetSumOfColumn(securityPerformanceDetailDs.Tables[PerfReportDS.DIWMPERFTABLE], CapitalReportDS.INTERNALCASHMOVEMENT, "InvestmentCode = 'Do It With Me'");
                    //Mak Value Change
                    decimal makValueChangeDiwm = GetSumOfColumn(securityPerformanceDetailDs.Tables[PerfReportDS.DIWMPERFTABLE], CapitalReportDS.CHANGEININVESTMENTVALUE, "InvestmentCode = 'Do It With Me'");
                    //Closing Bal
                    decimal closingBalDiwm = 0;
                    {
                        var perfClosingDiwm = securityPerformanceDetailDs.Tables[PerfReportDS.DIWMPERFTABLE].DefaultView;
                        perfClosingDiwm.Sort = PerfReportDS.MONTH + " DESC";
                        if (perfClosingDiwm.Count > 0)
                        {
                            closingBalDiwm = (decimal)perfClosingDiwm[0][CapitalReportDS.CLOSINGBALANCE];
                        }
                    }

                    //Income
                    decimal IncomeDiwm = GetSumOfColumn(securityPerformanceDetailDs.Tables[PerfReportDS.DIWMPERFTABLE], CapitalReportDS.INCOME, "InvestmentCode = 'Do It With Me'");

                    decimal totalDifwm = openingBalDiwm + salePerPurDiwm + tranInOutDiwm + feeDiwm + otherMvtDiwm + IncomeDiwm + makValueChangeDiwm;

                    if (!CheckClosingBalSecPerfReport(closingBalDiwm, totalDifwm, out message))
                    {
                        ds.ClientReportShareTable.AddRow(EclipseClientID, Name, CID, CLID, CSID, "7.Portfolio performance details-DIWM (Total Line)  ", message);
                    }
                    #endregion

                    #region Manual
                    //opening Bal
                    decimal openingBalManual = 0;
                    {
                        var perfManual = securityPerformanceDetailDs.Tables[PerfReportDS.MANPERFTABLE].DefaultView;
                        perfManual.Sort = PerfReportDS.MONTH + " ASC";
                        if (perfManual.Count > 0)
                        {
                            openingBalManual = (decimal)perfManual[0][CapitalReportDS.OPENINGBAL];
                        }
                    }

                    //SalePerPur
                    decimal salePerPurManual = GetSumOfColumn(securityPerformanceDetailDs.Tables[PerfReportDS.MANPERFTABLE],
                                                        PerfReportDS.APPLICATION, string.Empty);
                    salePerPurManual += GetSumOfColumn(securityPerformanceDetailDs.Tables[PerfReportDS.MANPERFTABLE],
                                                             PerfReportDS.REDEMPTION, string.Empty);
                    //TranInOut
                    decimal tranInOutManual = GetSumOfColumn(securityPerformanceDetailDs.Tables[PerfReportDS.MANPERFTABLE], CapitalReportDS.TRANSFEROUT, "InvestmentCode = 'Manual Asset'");

                    //Fee
                    decimal feeManual = GetSumOfColumn(securityPerformanceDetailDs.Tables[PerfReportDS.MANPERFTABLE], CapitalReportDS.EXPENSE, "InvestmentCode = 'Manual Asset'");

                    //Other Mvt
                    decimal otherMvtManual = GetSumOfColumn(securityPerformanceDetailDs.Tables[PerfReportDS.MANPERFTABLE], CapitalReportDS.INTERNALCASHMOVEMENT,
                                                         "InvestmentCode = 'Manual Asset'");
                    //Mak Value Change
                    decimal makValueChangeManual = GetSumOfColumn(securityPerformanceDetailDs.Tables[PerfReportDS.MANPERFTABLE], CapitalReportDS.CHANGEININVESTMENTVALUE, "InvestmentCode = 'Manual Asset'");
                    //Closing Bal
                    decimal closingBalManual = 0;
                    {
                        var perfClosingManual = securityPerformanceDetailDs.Tables[PerfReportDS.MANPERFTABLE].DefaultView;
                        perfClosingManual.Sort = PerfReportDS.MONTH + " DESC";
                        if (perfClosingManual.Count > 0)
                        {
                            closingBalManual = (decimal)perfClosingManual[0][CapitalReportDS.CLOSINGBALANCE];
                        }
                    }

                    //Income
                    decimal incomeManual = GetSumOfColumn(securityPerformanceDetailDs.Tables[PerfReportDS.MANPERFTABLE], CapitalReportDS.INCOME, "InvestmentCode = 'Manual Asset'");

                    decimal totalManual = openingBalManual + salePerPurManual + tranInOutManual + feeManual + otherMvtManual + incomeManual + makValueChangeManual;

                    if (!CheckClosingBalSecPerfReport(closingBalManual, totalManual, out message))
                    {
                        ds.ClientReportShareTable.AddRow(EclipseClientID, Name, CID, CLID, CSID, "7.Portfolio performance details-Manual (Total Line) ", message);
                    }
                    #endregion

                    #endregion
                }

            }
            else if (data is ConsolidationFeeRatioDS)
            {
                ConsolidationFeeRatioDS consolidationFeeRatioDS = (ConsolidationFeeRatioDS)data;
                var selectedTransactions = this.feeTransactions.Where(t => t.FeeRunID == consolidationFeeRatioDS.FeeRunID);

                foreach (FeeTransaction feeTran in selectedTransactions)
                    consolidationFeeRatioDS.FeeTransactions.Add(feeTran);
            }

            else if (data is ConsolidationFeeDS)
            {
                ConsolidationFeeDS consolidationFeeDS = (ConsolidationFeeDS)data;
                DataTable consolFeeTable = consolidationFeeDS.Tables[ConsolidationFeeDS.CONSOLIDATIONFEETABLE];

                var selectedTransactions = this.feeTransactions.Where(t => t.FeeRunID == consolidationFeeDS.FeeRunID);

                foreach (FeeTransaction feeTran in selectedTransactions)
                {
                    FeeTransactionDetailsDS feeTransactionDetailsDS = new FeeTransactionDetailsDS();
                    feeTransactionDetailsDS.FeeTransactionID = feeTran.ID;

                    this.OnGetData(feeTransactionDetailsDS);

                    List<DataRow> rows = feeTransactionDetailsDS.Tables[FeeTransactionDetailsDS.CALCULATEDFEESSUMMARYBYFEETYPE].Select().Where(row => row[FeeTransactionDetailsDS.FEESUBTYPE].ToString() == string.Empty).ToList();

                    foreach (DataRow feeTranRow in rows)
                    {
                        DataRow row = consolFeeTable.NewRow();
                        row[ConsolidationFeeDS.CLIENTCID] = this.CID;
                        row[ConsolidationFeeDS.CLIENTID] = this.EclipseClientID;
                        row[ConsolidationFeeDS.CLIENTNAME] = this.Name;
                        row[ConsolidationFeeDS.FEETRANID] = feeTran.ID;
                        row[ConsolidationFeeDS.FEERUNID] = feeTran.FeeRunID;
                        row[ConsolidationFeeDS.FEETYPE] = feeTranRow[FeeTransactionDetailsDS.FEETYPE];
                        row[ConsolidationFeeDS.FEETOTAL] = (decimal)feeTranRow[FeeTransactionDetailsDS.CALCULATEDFEES];
                        row[ConsolidationFeeDS.AUTOADJUST] = (decimal)feeTranRow[FeeTransactionDetailsDS.AUTOADJUST];
                        row[ConsolidationFeeDS.ADJUST] = (decimal)feeTranRow[FeeTransactionDetailsDS.ADJUST];
                        row[ConsolidationFeeDS.FEETOTALADJUST] = (decimal)feeTranRow[FeeTransactionDetailsDS.FEETOTALADJUST];

                        if (feeTranRow[FeeTransactionDetailsDS.FEETYPEID] != DBNull.Value && feeTranRow[FeeTransactionDetailsDS.FEETYPEID].ToString() != string.Empty)
                            row[ConsolidationFeeDS.FEETRANID] = (Guid)feeTranRow[FeeTransactionDetailsDS.FEETYPEID];
                        else
                            row[ConsolidationFeeDS.FEETRANID] = Guid.Empty;

                        consolFeeTable.Rows.Add(row);
                    }
                }
            }

            else if (data is ClientFeesDS)
            {
                ClientFeesDS clientFeesDS = (ClientFeesDS)data;
                clientFeesDS.ClientFeeEntity = this.clientFeeEntity;
            }

            else if (data is ClientAccountsDetailsDS)
            {
                ClientAccountsDetailsDS clientAccountsDetailsDS = (ClientAccountsDetailsDS)data;
                clientAccountsDetailsDS.ClientEntity = ClientEntity;

                IOrganizationUnit adviserCM = this.Broker.GetBMCInstance(AdviserID) as IOrganizationUnit;

                if (adviserCM != null)
                {
                    clientAccountsDetailsDS.AdviserID = adviserCM.ClientId;
                    clientAccountsDetailsDS.AdviserFullName = adviserCM.FullName();
                    Broker.ReleaseBrokerManagedComponent(adviserCM);
                }
                else
                {
                    clientAccountsDetailsDS.AdviserID = string.Empty;
                    clientAccountsDetailsDS.AdviserFullName = string.Empty;
                }
            }

            else if (data is UMAEntityFeesDS)
            {
                UMAEntityFeesDS umaEntityFeesDS = (UMAEntityFeesDS)data;
                OnGetData(umaEntityFeesDS);
            }

            else if (data is FeeTransactionsDS)
            {
                FeeTransactionsDS feeTransactionsDS = (FeeTransactionsDS)data;
                OnGetData(feeTransactionsDS);
            }

            else if (data is OrgUnitConfiguredFeesDS)
            {
                OrgUnitConfiguredFeesDS orgUnitConfiguredFeesDS = (OrgUnitConfiguredFeesDS)data;
                OnGetData(orgUnitConfiguredFeesDS);
            }

            else if (data is ClientsByIFADS)
            {
                ClientsByIFADS clientsByIFADS = (ClientsByIFADS)data;
                if (this.IsInvestableClient)
                {
                    if (clientsByIFADS.IncludeCheckForCID)
                    {
                        if (this.IsClassSuper)
                            SetDataSetClientsByIFADS(data);
                    }
                    else
                        SetDataSetClientsByIFADS(data);
                }
            }

            else if (data is IndividualListDS)
            {
                IndividualListDS individualListDS = (IndividualListDS)data;
                DataTable individualListTables = individualListDS.Tables[IndividualListDS.INDIVIDUALISTTABLENAME];
                IClientUMAData clientUMAData = this.ClientEntity as IClientUMAData;
                List<IdentityCMDetail> signatories = clientUMAData.GetSignatoriesList();

                foreach (IdentityCMDetail identityCMDetail in signatories)
                {
                    IndividualEntity individualEntity = null;
                    IOrganizationUnit orgUnitIndividual = null;

                    if (identityCMDetail.Cid == Guid.Empty && identityCMDetail.Clid != Guid.Empty)
                        orgUnitIndividual = Broker.GetCMImplementation(identityCMDetail.Clid, identityCMDetail.Csid) as IOrganizationUnit;
                    else
                        orgUnitIndividual = Broker.GetBMCInstance(identityCMDetail.Cid) as IOrganizationUnit;

                    if (orgUnitIndividual != null)
                        individualEntity = orgUnitIndividual.ClientEntity as IndividualEntity;

                    if (individualEntity != null)
                    {
                        DataRow row = individualListTables.NewRow();
                        row[IndividualListDS.CLIENTID] = orgUnitIndividual.ClientId;
                        row[IndividualListDS.CID] = orgUnitIndividual.CID;
                        row[IndividualListDS.CSID] = orgUnitIndividual.CSID;
                        row[IndividualListDS.CLID] = orgUnitIndividual.CLID;
                        row[IndividualListDS.EMAILADDRESS] = individualEntity.EmailAddress;
                        row[IndividualListDS.FIRSTNAME] = individualEntity.Name;
                        row[IndividualListDS.SURNAME] = individualEntity.Surname;
                        row[IndividualListDS.MIDDLENAME] = individualEntity.MiddleName;
                        row[IndividualListDS.PERSONALTITLE] = individualEntity.PersonalTitle.Title;
                        row[IndividualListDS.DOB] = individualEntity.DOB.Value;
                        row[IndividualListDS.HMPHCOUNTRYCODE] = individualEntity.HomePhoneNumber.CountryCode;
                        row[IndividualListDS.HMPHCITYCODE] = individualEntity.HomePhoneNumber.CityCode;
                        row[IndividualListDS.HMPHNO] = individualEntity.HomePhoneNumber.PhoneNumber;
                        row[IndividualListDS.WKPHCOUNTRYCODE] = individualEntity.WorkPhoneNumber.CountryCode;
                        row[IndividualListDS.WKPHCITYCODE] = individualEntity.WorkPhoneNumber.CityCode;
                        row[IndividualListDS.WKPHNO] = individualEntity.WorkPhoneNumber.PhoneNumber;
                        row[IndividualListDS.FAXPHCOUNTRYCODE] = individualEntity.Facsimile.CountryCode;
                        row[IndividualListDS.FAXPHCITYCODE] = individualEntity.Facsimile.CityCode;
                        row[IndividualListDS.FAXPHNO] = individualEntity.Facsimile.PhoneNumber;
                        row[IndividualListDS.MBPHCOUNTRYCODE] = individualEntity.MobilePhoneNumber.CountryCode;
                        row[IndividualListDS.MBPHNO] = individualEntity.MobilePhoneNumber.MobileNumber;
                        row[IndividualListDS.OCCUAPATION] = individualEntity.Occupation;
                        row[IndividualListDS.TIN] = individualEntity.TIN;
                        row[IndividualListDS.TFN] = individualEntity.TFN;
                        row[IndividualListDS.EMPLOYER] = individualEntity.Employer;

                        if (individualEntity.DocDrivingLicence != null)
                        {
                            row[IndividualListDS.DOCNUMBERLIC] = individualEntity.DocDrivingLicence.DocNumber;
                            row[IndividualListDS.ISPHOTOSHOWNLIC] = individualEntity.DocDrivingLicence.IsPhotoShown;

                            if (individualEntity.DocDrivingLicence.IssueDate != null)
                                row[IndividualListDS.ISSUEDATELIC] = individualEntity.DocDrivingLicence.IssueDate.Value;

                            if (individualEntity.DocDrivingLicence.ExpiryDate != null)
                                row[IndividualListDS.EXPDAELIC] = individualEntity.DocDrivingLicence.ExpiryDate.Value;

                            row[IndividualListDS.ISSUEPLACELIC] = individualEntity.DocDrivingLicence.IssuePlace;

                            row[IndividualListDS.ORGNAMELIC] = individualEntity.DocDrivingLicence.OrganizationName;
                            row[IndividualListDS.ISORIGINALLIC] = individualEntity.DocDrivingLicence.Original;
                        }

                        if (individualEntity.DocAustralianPassport != null)
                        {
                            row[IndividualListDS.DOCNUMBERPASS] = individualEntity.DocAustralianPassport.DocNumber;
                            row[IndividualListDS.ISPHOTOSHOWNPASS] = individualEntity.DocAustralianPassport.IsPhotoShown;

                            if (individualEntity.DocAustralianPassport.IssueDate != null)
                                row[IndividualListDS.ISSUEDATEPASS] = individualEntity.DocAustralianPassport.IssueDate.Value;

                            if (individualEntity.DocAustralianPassport.ExpiryDate != null)
                                row[IndividualListDS.EXPDAEPASS] = individualEntity.DocAustralianPassport.ExpiryDate.Value;

                            row[IndividualListDS.ISSUEPLACEPASS] = individualEntity.DocAustralianPassport.IssuePlace;
                            row[IndividualListDS.ORGNAMEPASS] = individualEntity.DocAustralianPassport.OrganizationName;
                            row[IndividualListDS.ISORIGINALPASS] = individualEntity.DocAustralianPassport.Original;
                        }

                        row[IndividualListDS.ADDRESSLN1RESI] = individualEntity.ResidentialAddress.Addressline1;
                        row[IndividualListDS.ADDRESSLN2RESI] = individualEntity.ResidentialAddress.Addressline2;
                        row[IndividualListDS.SUBURBRESI] = individualEntity.ResidentialAddress.Suburb;
                        row[IndividualListDS.STATERESI] = individualEntity.ResidentialAddress.State;
                        row[IndividualListDS.POSTCODERESI] = individualEntity.ResidentialAddress.PostCode;
                        row[IndividualListDS.COUNTRYRESI] = individualEntity.ResidentialAddress.Country;
                        row[IndividualListDS.ADDRESSLN1MAILING] = individualEntity.MailingAddress.Addressline1;
                        row[IndividualListDS.ADDRESSLN2MAILING] = individualEntity.MailingAddress.Addressline2;
                        row[IndividualListDS.SUBURBMAILING] = individualEntity.MailingAddress.Suburb;
                        row[IndividualListDS.STATEMAILING] = individualEntity.MailingAddress.State;
                        row[IndividualListDS.POSTCODEMAILING] = individualEntity.MailingAddress.PostCode;
                        row[IndividualListDS.COUNTRYMAILING] = individualEntity.MailingAddress.Country;

                        individualListTables.Rows.Add(row);
                    }
                }
            }

            else if (data is OrganizationUnitDS)
            {
                OrganizationUnitDS objOUDS = data as OrganizationUnitDS;
                DataRow row = objOUDS.Tables[OrganizationUnitDS.ORGUNIT_TABLE].NewRow();
                row[OrganizationUnitDS.ORGUNITCURRPERIOD_FIELD] = this.m_objCurrentPeriod;
                row[OrganizationUnitDS.ORGUNITLEGALNAME_FIELD] = this.m_strLegalName;
                row[OrganizationUnitDS.ORGUNITIDENT_FIELD] = this.m_strIdentifier;
                objOUDS.Tables[OrganizationUnitDS.ORGUNIT_TABLE].Rows.Add(row);

                foreach (DictionaryEntry dictionaryEntry in m_objPeriodsHT)
                {

                    PeriodMember pm = (PeriodMember)dictionaryEntry.Value;
                    row = objOUDS.Tables[OrganizationUnitDS.ORGUNITPERIODS_TABLE].NewRow();
                    row[OrganizationUnitDS.ORGUNITPERIODKEY_FIELD] = dictionaryEntry.Key;
                    row[OrganizationUnitDS.ORGUNITPERIODSTARTDATE_FIELD] = pm.dtStartDate;
                    row[OrganizationUnitDS.ORGUNITPERIODENDDATE_FIELD] = pm.dtEndDate;
                    row[OrganizationUnitDS.ORGUNITPERIODTYPE_FIELD] = pm.strTypeName;

                    objOUDS.Tables[OrganizationUnitDS.ORGUNITPERIODS_TABLE].Rows.Add(row);
                }
            }
            else if (data is OrganisationUnitDetailsDS)
            {
                this.OnGetData((OrganisationUnitDetailsDS)data);
            }
            else if (data is MembershipDS)
            {
                var ds = data as MembershipDS;
                switch (ds.CommandType)
                {
                    case DatasetCommandTypes.Get:
                        AddAvilableMembershipRow(ds, this.ClientId, this.FullName(), this.ComponentInformation.DisplayName, this.OrganizationStatus, this.Clid, this.Csid, this.CID);
                        break;
                }
            }
            else if (data is ClientMappingControlDS)
            {
                var ds = data as ClientMappingControlDS;
                switch (ds.CommandType)
                {
                    case DatasetCommandTypes.Get:
                        AddAvilableClientDetailsRow(ds, this.ClientId, this.FullName(), this.ComponentInformation.DisplayName, this.OrganizationStatus, this.Clid, this.Csid, this.CID);
                        break;
                }
            }

            else if (data is InstanceCMDS)
            {
                var ds = data as InstanceCMDS;
                if (ds.CommandType == DatasetCommandTypes.Get)
                {
                    DataRow dr = ds.InstanceCMDetailsTable.NewRow();
                    dr[ds.InstanceCMDetailsTable.CID] = CID;
                    dr[ds.InstanceCMDetailsTable.CLID] = CLID;
                    dr[ds.InstanceCMDetailsTable.CSID] = Csid;
                    dr[ds.InstanceCMDetailsTable.NAME] = Name;
                    dr[ds.InstanceCMDetailsTable.TYPE] = TypeName;
                    dr[ds.InstanceCMDetailsTable.STATUS] = Enumeration.GetDescription(this.StatusType);

                    ds.InstanceCMDetailsTable.Rows.Add(dr);
                }
            }

            else if (data is MDAAlertDS)
            {
                #region MDA Alert
                var ds = data as MDAAlertDS;
                if (ds.CommandType == DatasetCommandTypes.Get)
                {
                    if (this.ClientEntity is CorporateEntity || this.ClientEntity is ClientIndividualEntity)
                    {

                        DataRow row = ds.ClientDetailTable.NewRow();
                        row[ClientDetailTable.CLIENTCID] = CID;
                        row[ClientDetailTable.CLIENTID] = EclipseClientID;
                        row[ClientDetailTable.CLIENTNAME] = Name;
                        row[ClientDetailTable.ADVISERID] = this.AdviserID;
                       


                        string servicetypes = string.Empty;
                        if (this.ServiceType.DO_IT_FOR_ME)
                        {
                            row[ClientDetailTable.HASDIFM] = true;
                            servicetypes += ServiceTypes.DoItForMe + ",";
                        }
                        if (this.ServiceType.DO_IT_WITH_ME)
                        {
                            row[ClientDetailTable.HASDIWM] = true;
                            servicetypes += ServiceTypes.DoItWithMe + ",";
                        }
                        if (this.ServiceType.DO_IT_YOURSELF)
                        {
                            row[ClientDetailTable.HASDIYS] = true;
                            servicetypes += ServiceTypes.DoItYourSelf + ",";
                        }

                        row[ClientDetailTable.SERVICE] = servicetypes;


                        if (this.SignatoriesApplicants.Count > 0)
                            row[ClientDetailTable.CLIENTEMAIL] = GetClientSigEmail(this.SignatoriesApplicants[0]);
                        if (this.IsCorporateType())
                        {
                            var entity = this.ClientEntity as CorporateEntity;
                            row[ClientDetailTable.PARENTID] = ConfiguredParent.Cid;//entity.Parent.Cid; 
                            row[ClientDetailTable.MDAEXECUTIONDATE] = entity.MDAExecutionDate;

                            if (entity.Servicetype.DO_IT_FOR_ME && entity.Servicetype.DoItForMe != null)
                            {
                                row[ClientDetailTable.DIFMPROGRAMCODE] = entity.Servicetype.DoItForMe.ProgramCode;
                                row[ClientDetailTable.DIFMPROGRAMNAME] = entity.Servicetype.DoItForMe.ProgramName;
                            }

                            if (entity.Servicetype.DO_IT_WITH_ME && entity.Servicetype.DoItWithMe != null)
                            {
                                row[ClientDetailTable.DIWMPROGRAMCODE] = entity.Servicetype.DoItWithMe.ProgramCode;
                                row[ClientDetailTable.DIWMPROGRAMNAME] = entity.Servicetype.DoItWithMe.ProgramName;
                            }
                            if (entity.Servicetype.DO_IT_YOURSELF && entity.Servicetype.DoItYourSelf != null)
                            {
                                row[ClientDetailTable.DIYSPROGRAMCODE] = entity.Servicetype.DoItYourSelf.ProgramCode;   
                                row[ClientDetailTable.DIYSPROGRAMNAME] = entity.Servicetype.DoItYourSelf.ProgramName;
                            }
                        }
                        else
                        {
                            var entity = this.ClientEntity as ClientIndividualEntity;
                            row[ClientDetailTable.PARENTID] = ConfiguredParent.Cid;//entity.Parent.Cid; 
                            row[ClientDetailTable.MDAEXECUTIONDATE] = entity.MDAExecutionDate;

                            if (entity.Servicetype.DO_IT_FOR_ME && entity.Servicetype.DoItForMe != null)
                            {
                                row[ClientDetailTable.DIFMPROGRAMCODE] = entity.Servicetype.DoItForMe.ProgramCode;
                                row[ClientDetailTable.DIFMPROGRAMNAME] = entity.Servicetype.DoItForMe.ProgramName;
                            }

                            if (entity.Servicetype.DO_IT_WITH_ME && entity.Servicetype.DoItWithMe != null)
                            {
                                row[ClientDetailTable.DIWMPROGRAMCODE] = entity.Servicetype.DoItWithMe.ProgramCode;
                                row[ClientDetailTable.DIWMPROGRAMNAME] = entity.Servicetype.DoItWithMe.ProgramName;
                            }
                            if (entity.Servicetype.DO_IT_YOURSELF && entity.Servicetype.DoItYourSelf != null)
                            {
                                row[ClientDetailTable.DIYSPROGRAMCODE] = entity.Servicetype.DoItYourSelf.ProgramCode;
                                row[ClientDetailTable.DIYSPROGRAMNAME] = entity.Servicetype.DoItYourSelf.ProgramName;
                            }
                        }

                        ds.ClientDetailTable.Rows.Add(row);

                        if (this.AdviserID != Guid.Empty)
                        {
                            var advisorcm = Broker.GetBMCInstance(AdviserID);
                            advisorcm.GetData(ds);
                            Broker.ReleaseBrokerManagedComponent(advisorcm);
                        }

                        #region Add attachmentTable
                        AttachmentListDS attachmentListDS = new AttachmentListDS();
                        GetData(attachmentListDS);

                        if (!ds.Tables.Contains(AttachmentListDS.ATTACHMENTTABLE))
                        {
                            ds.Tables.Add(attachmentListDS.Tables[AttachmentListDS.ATTACHMENTTABLE].Clone());
                        }
                        foreach (DataRow attrow in attachmentListDS.Tables[AttachmentListDS.ATTACHMENTTABLE].Rows)
                        {
                            ds.Tables[AttachmentListDS.ATTACHMENTTABLE].ImportRow(attrow);
                        } 
                        #endregion
                    }
                }
                #endregion
            }
            
        }

        private string GetClientSigEmail(IdentityCMDetail sig)
        {
            string email = string.Empty;

            var cm = Broker.GetCMImplementation(sig.Clid, sig.Csid);
            if (cm != null)
                email = cm.GetDataStream((int)CmCommand.Email, string.Empty);
            return email;
        }

        #region Checking Opening balances




        private bool CheckOpeningBalances(decimal openingBalance, decimal icomeGrothYearlyOpening, decimal iComeGrowthMonthlyOpening,
                                          decimal protfolioDeatilOpeningBalance, decimal protfolioSummaryOpeningBalance,
                                          decimal securityDetailOpeningBalance, decimal securitySummaryOpeningBalance, out string message)
        {
            message =
            string.Format(@"Holding statement (Total Opening Balance)={0}
            , InCome VS Growth Yearly (Lastest Opening Balance)={1}
            , InCome VS Growth Monthly (Lastest Opening Balance)={2}
            , Portfolio Performance Detail  (Total Line)={3}
            , Portfolio performance summary (Total Line)={4}
            , Security performance Detail (Total Line)={5}
            , Security performance Summary (Total Line)={6}",
            openingBalance,
            icomeGrothYearlyOpening,
            iComeGrowthMonthlyOpening,
            protfolioDeatilOpeningBalance,
            protfolioSummaryOpeningBalance,
            securityDetailOpeningBalance,
            securitySummaryOpeningBalance);

            return IsMatching(openingBalance, icomeGrothYearlyOpening, iComeGrowthMonthlyOpening, protfolioDeatilOpeningBalance,
                              protfolioSummaryOpeningBalance, securityDetailOpeningBalance, securitySummaryOpeningBalance);
        }

        private bool CheckClosingBalanceDiy(decimal holdingSummClosingBalTotal, decimal holdingSummClosingBalDiy,
                                            decimal protfoli0PerfClosingBalFirstDefault, decimal protfoli0PerfClosingBalTotal,
                                            decimal capMoveSumTotal, out string message)
        {
            message =
            string.Format(@"Holding Summary (Total Closing Balance)={0}
            , Holding Summary Closing Bal  (DIY)={1}
            , Portfolio Performance Detail-DIY  (Closing Bal)={2}
            , Portfolio performance Detail-DIY (Total Cal Bal)={3}
            , Capital Movement Summary-DIY (Total Cal Bal Line)={4}",
            holdingSummClosingBalTotal,
            holdingSummClosingBalDiy,
            protfoli0PerfClosingBalFirstDefault,
            protfoli0PerfClosingBalTotal,
            capMoveSumTotal);

            return IsMatching(holdingSummClosingBalTotal, holdingSummClosingBalDiy,
                              protfoli0PerfClosingBalFirstDefault, protfoli0PerfClosingBalTotal, capMoveSumTotal);
        }

        private bool CheckClosingBalSecPerfReport(decimal totalClosingBal, decimal total, out string message)
        {
            message =
                  string.Format(@"Total Closing Bal={0}
            , Sum (Opening+SalePerPur+TranInOut+Fee+OtherMvt+MktValChange)={1}",
                  totalClosingBal,
                  total);

            return Math.Abs(totalClosingBal - total) < 1;
        }

        private bool CheckClosingBalanceDiwm(decimal holdingSummTotalDiwm, decimal holdingSummClosingBalDiwm, decimal protfoli0PerfClosingBalFirstDefaultdiwm, decimal protfoli0PerfClosingBalTotalDiwm,
                                             decimal capMoveSumTotalDiwm, out string message)
        {
            message =
              string.Format(@"Holding Summary (Total Closing Balance)={0}
            , Holding Summary Closing Bal  (DIWM)={1}
            , Portfolio Performance Detail-DIWM  (Closing Bal)={2}
            , Portfolio performance Detail-DIWM (Total Cal Bal)={3}
            , Capital Movement Summary-DIWM (Total Cal Bal Line)={4}",
              holdingSummTotalDiwm,
              holdingSummClosingBalDiwm,
              protfoli0PerfClosingBalFirstDefaultdiwm,
              protfoli0PerfClosingBalTotalDiwm,
              capMoveSumTotalDiwm);

            return IsMatching(holdingSummTotalDiwm, holdingSummClosingBalDiwm,
                              protfoli0PerfClosingBalFirstDefaultdiwm, protfoli0PerfClosingBalTotalDiwm, capMoveSumTotalDiwm);
        }


        private bool CheckClosingBalancesDifm(decimal holdingSummaryClosingBalanceTotal, decimal holdingSummaryDifmTotal,
                                              decimal protfoliPerfDetaiClosing, decimal protfoliPerfTotalClosingBalance,
                                              decimal capitalReportClosingBal, out string message)
        {
            message =
            string.Format(@"Holding Summary (Holding)={0}
            ,	Holding Summary (Do It For Me)={1}
            ,	Portfolio Performance Detail (Closing Balance)={2}
            ,	Portfolio Performance Detail  (Total Cal Bal)={3}
            ,	Capital Movement Summary (Total Cal Bal)={4}",
            holdingSummaryClosingBalanceTotal,
            holdingSummaryDifmTotal,
            protfoliPerfDetaiClosing,
            protfoliPerfTotalClosingBalance,
            capitalReportClosingBal);
            return IsMatching(holdingSummaryClosingBalanceTotal, holdingSummaryDifmTotal,
                              protfoliPerfDetaiClosing, protfoliPerfTotalClosingBalance, capitalReportClosingBal);
        }


        #endregion


        private bool CheckSecuritySummayrTotals(decimal secuirtySummaryTotalIncome, decimal secuirtySummaryTotalOtherMovement, out string message)
        {
            message = string.Format(@"Security Performance Summary (Total Income)={0},	Security Performance Summary (Total Other Movement)={1}",
              secuirtySummaryTotalIncome,
              secuirtySummaryTotalOtherMovement
             );

            return Math.Abs(secuirtySummaryTotalIncome - secuirtySummaryTotalOtherMovement) < 1;
        }

        public bool IsMatching(params decimal[] values)
        {
            bool result = true;
            if (values != null && values.Length > 0)
            {
                decimal value = values[0];
                foreach (var each in values)
                {
                    result = result & (value == each);
                }
            }
            return result;
        }

        private bool CheckClosingBalances(decimal holdingStatementTotal, decimal holdingReportTotal, decimal securityPerformanceSummaryTotalLine, decimal securityPerformanceDetailsTotalLine, decimal portfolioPerformanceSummaryTotalLine, decimal portfolioPerformanceSummaryFirstLine, decimal portfolioPerformanceDetailsTotalLine, decimal portfolioPerformanceDetailsFirstLine, decimal securitySummaryTotalLine, decimal assetsSummaryTotalLine, decimal capitalMovementFirstLine, decimal incomevsGrowthFirstLine, decimal incomevsGrowth1Yearline, decimal incomevsGrowthTotalline, out  string message)
        {
            message = string.Format(@"Holding statement (Total Line)={0}
,	Holding Report (Total Line in holding summary)={1}
,	Security Performance Summary (Total Line)={2}
,	Security Performance Details (Total Line)={3}
,	Portfolio performance summary (Total Line)={4}
,	Portfolio performance summary (Latest Line)={5}
,	Portfolio performance details (Total Line)={6}
,	Portfolio performance details (Latest Line)={7}
,	Security Summary (Total Line)={8}
,	Assets Summary (Total Line)={9}
,	Capital Movement (Latest line)={10} 
,	Income vs Growth (Latest line) ={11}
,	Income vs Growth (1 Year line)={12}
,	Income vs Growth (Total  line)={13}",
            holdingStatementTotal,
            holdingReportTotal,
            securityPerformanceSummaryTotalLine,
            securityPerformanceDetailsTotalLine,
            portfolioPerformanceSummaryTotalLine,
            portfolioPerformanceSummaryFirstLine,
            portfolioPerformanceDetailsTotalLine,
            portfolioPerformanceDetailsFirstLine,
            securitySummaryTotalLine,
            assetsSummaryTotalLine,
            capitalMovementFirstLine,
            incomevsGrowthFirstLine,
            incomevsGrowth1Yearline,
            incomevsGrowthTotalline);

            return IsMatching(holdingStatementTotal, holdingReportTotal, securityPerformanceSummaryTotalLine, securityPerformanceDetailsTotalLine, portfolioPerformanceSummaryTotalLine, portfolioPerformanceSummaryFirstLine, portfolioPerformanceDetailsTotalLine, portfolioPerformanceDetailsFirstLine, securitySummaryTotalLine, assetsSummaryTotalLine, capitalMovementFirstLine, incomevsGrowthFirstLine, incomevsGrowth1Yearline, incomevsGrowthTotalline);
        }
        private static decimal GetSumOfColumn(DataTable dt, string ColName, string Query)
        {

            decimal unsettledValue = 0;
            if (dt.Rows.Count > 0)
            {
                string colToCompute = string.Format("sum({0})", ColName);
                var sum = dt.Compute(colToCompute, Query);
                if (sum != DBNull.Value)
                    unsettledValue = (decimal)sum;
                return unsettledValue;
            }
            else
            {
                return unsettledValue;
            }
        }

        private static void AddAddressToDict(BaseClientDetails baseClientDetails, Common.AddressEntity address, string key)
        {
            if (address != null && address.Addressline1 != String.Empty && !baseClientDetails.AddressList.ContainsKey(key))
            {
                if (!baseClientDetails.AddressList.ContainsKey(key))
                    baseClientDetails.AddressList.Add(key, address.GetFullAddress());
            }
        }
        private static void SetAddresses(BaseClientDetails baseClientDetails, Common.AddressEntity addressEntity)
        {
            baseClientDetails.SETADDRESSLINE1 = addressEntity.Addressline1;
            baseClientDetails.SETADDRESSLINE2 = addressEntity.Addressline2;
            baseClientDetails.SETPOSTCODE = addressEntity.PostCode;
            baseClientDetails.SETSUBURB = addressEntity.Suburb;
            baseClientDetails.SETSTATE = addressEntity.State;
            baseClientDetails.SETCOUNTRY = addressEntity.Country;
        }

        private void FillSercuirtyTransferInData(SecurityTransferInReportDS ds, List<IdentityCMDetail> desktopBrokerAccounts)
        {
            if (desktopBrokerAccounts != null)
            {
                foreach (var asxAccount in desktopBrokerAccounts)
                {

                    var desktopBrokerCM = Broker.GetCMImplementation(asxAccount.Clid, asxAccount.Csid) as IOrganizationUnit;

                    if (desktopBrokerCM == null && asxAccount.Cid != Guid.Empty)
                    {
                        desktopBrokerCM = Broker.GetBMCInstance(asxAccount.Cid) as IOrganizationUnit;
                    }
                    if (desktopBrokerCM != null)
                    {
                        ds.CurrentClientCID = this.CID;
                        ds.CurrentClientID = this.EclipseClientID;
                        desktopBrokerCM.GetData(ds);
                        Broker.ReleaseBrokerManagedComponent(desktopBrokerCM);
                    }
                }
            }
        }

        private void FillP2ReportData(P2ReportsDS ds, List<P2ReportData> p2Reports)
        {
            var tbl = ds.P2ReportDetailsTable;
            foreach (var p2ReportData in p2Reports)
            {
                DataRow row = tbl.NewRow();
                row[tbl.ATTACHEDBMCID] = p2ReportData.AttachedBMCID;
                row[tbl.YEAR] = p2ReportData.Year;
                row[tbl.QUARTER] = p2ReportData.Quarter;
                row[tbl.P2TYPE] = p2ReportData.P2Type;
                tbl.Rows.Add(row);
            }
        }

        private void AddP2ReportsData(P2ReportsDS ds, List<P2ReportData> p2Reports)
        {
            if (p2Reports != null)
            {
                var tbl = ds.P2ReportDetailsTable;
                foreach (DataRow row in tbl.Rows)
                {
                    switch (row.RowState)
                    {
                        case DataRowState.Added:
                            var data = new P2ReportData
                            {
                                AttachedBMCID = row[tbl.ATTACHEDBMCID].ToString(),
                                Year = Convert.ToInt32(row[tbl.YEAR]),
                                Quarter = row[tbl.QUARTER].ToString(),
                                P2Type = row[tbl.P2TYPE].ToString()
                            };
                            p2Reports.Add(data);
                            break;
                    }
                }
            }
        }

        private void AddAvilableClientDetailsRow(ClientMappingControlDS ds, string clientId, string name, string typename, string status, Guid clid, Guid csid, Guid cid)
        {
            DataRow r = ds.ClientMappingTable.NewRow();
            r[ds.ClientMappingTable.CLIENTID] = clientId;
            r[ds.ClientMappingTable.NAME] = name;
            r[ds.ClientMappingTable.TYPE] = typename;
            r[ds.ClientMappingTable.STATUS] = status;
            r[ds.ClientMappingTable.CLID] = clid;
            r[ds.ClientMappingTable.CSID] = csid;
            r[ds.ClientMappingTable.CID] = cid;
            ds.ClientMappingTable.Rows.Add(r);
        }

        private void SetDataSetClientsByIFADS(DataSet data)
        {
            DataRow row = data.Tables[ClientsByIFADS.CLIENTDETAILSTABLE].NewRow();
            row[ClientsByIFADS.ID] = this.CID;
            row[ClientsByIFADS.CLIENTID] = this.ClientId;
            row[ClientsByIFADS.CLIENTNAME] = this.Name;
            row[ClientsByIFADS.CLIENTTYPE] = this.ComponentInformation.DisplayName;
            data.Tables[ClientsByIFADS.CLIENTDETAILSTABLE].Rows.Add(row);
        }

        private void SetFeeDIWM(GroupFeeBankwestExportDS groupFeeBankwestExportDS, decimal diwmFee, decimal diwmHoldingRatio, decimal allPlatformFee)
        {
            DataRow feeDebitRow = groupFeeBankwestExportDS.Tables[GroupFeeBankwestExportDS.GROUPFEEEXPORTBANKWESTABLE].NewRow();
            feeDebitRow[GroupFeeBankwestExportDS.CID] = this.CID;
            feeDebitRow[GroupFeeBankwestExportDS.ACCOUNTNO] = String.Empty;
            feeDebitRow[GroupFeeBankwestExportDS.BSB] = String.Empty;
            feeDebitRow[GroupFeeBankwestExportDS.NARRATION] = groupFeeBankwestExportDS.Narration;

            feeDebitRow[GroupFeeBankwestExportDS.ACCOUNTNAME] = "No DIWM Bank Account Found for Client - (" + this.name + " " + this.ClientId + ") (DIWM) " + this.ClientId;
            feeDebitRow[GroupFeeBankwestExportDS.AMOUNT] = diwmFee + (allPlatformFee * diwmHoldingRatio);
            groupFeeBankwestExportDS.Tables[GroupFeeBankwestExportDS.GROUPFEEEXPORTBANKWESTABLE].Rows.Add(feeDebitRow);
        }

        private void SetFee(GroupFeeBankwestExportDS groupFeeBankwestExportDS, List<AccountProcessTaskEntity> ListAccountProcess, decimal difmFee, decimal diwmFee, decimal diyFee, decimal difmHoldingRatio, decimal diwmHoldingRatio, decimal diymHoldingRatio, decimal allPlatformFee)
        {
            foreach (var accountProcessTaskEntity in ListAccountProcess)
            {
                if (accountProcessTaskEntity.LinkedEntity != null)
                {
                    if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.BankAccount && accountProcessTaskEntity.LinkedEntity.Clid != Guid.Empty)
                    {
                        var asset = groupFeeBankwestExportDS.Assets.Where(var => var.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                        var model = groupFeeBankwestExportDS.Models.Where(mod => mod.ID == accountProcessTaskEntity.ModelID).FirstOrDefault();
                        var product = asset.Products.Where(pro => pro.ID == accountProcessTaskEntity.TaskID).FirstOrDefault();

                        if (accountProcessTaskEntity.LinkedEntity.Clid == Guid.Empty)
                        {
                            accountProcessTaskEntity.LinkedEntity.Clid = product.EntityId.Clid;
                            accountProcessTaskEntity.LinkedEntity.Csid = product.EntityId.Csid;
                        }

                        IOrganizationUnit bankAccount =
                              Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid,
                                                         accountProcessTaskEntity.LinkedEntity.Csid) as IOrganizationUnit;

                        if (bankAccount != null)
                        {
                            Oritax.TaxSimp.Common.BankAccountEntity bankEnity = bankAccount.ClientEntity as Oritax.TaxSimp.Common.BankAccountEntity;
                            if (bankEnity.AccoutType.ToLower() == "cma")
                            {
                                DataRow feeDebitRow = groupFeeBankwestExportDS.Tables[GroupFeeBankwestExportDS.GROUPFEEEXPORTBANKWESTABLE].NewRow();
                                feeDebitRow[GroupFeeBankwestExportDS.CID] = this.CID;
                                feeDebitRow[GroupFeeBankwestExportDS.ACCOUNTNO] = bankEnity.AccountNumber;
                                feeDebitRow[GroupFeeBankwestExportDS.BSB] = bankEnity.BSB;
                                feeDebitRow[GroupFeeBankwestExportDS.NARRATION] = groupFeeBankwestExportDS.Narration;

                                if (model.ServiceType == ServiceTypes.DoItForMe)
                                {
                                    feeDebitRow[GroupFeeBankwestExportDS.ACCOUNTNAME] = bankEnity.Name + " (DIFM) " + this.ClientId;
                                    feeDebitRow[GroupFeeBankwestExportDS.AMOUNT] = difmFee + (allPlatformFee * difmHoldingRatio);
                                }
                                else if (model.ServiceType == ServiceTypes.DoItWithMe)
                                {
                                    feeDebitRow[GroupFeeBankwestExportDS.ACCOUNTNAME] = bankEnity.Name + " (DIWM) " + this.ClientId;
                                    feeDebitRow[GroupFeeBankwestExportDS.AMOUNT] = diwmFee + (allPlatformFee * diwmHoldingRatio);
                                }
                                else if (model.ServiceType == ServiceTypes.DoItYourSelf)
                                {
                                    feeDebitRow[GroupFeeBankwestExportDS.ACCOUNTNAME] = bankEnity.Name + " (DIY) " + this.ClientId;
                                    feeDebitRow[GroupFeeBankwestExportDS.AMOUNT] = diyFee + (allPlatformFee * diymHoldingRatio);
                                }



                                groupFeeBankwestExportDS.Tables[GroupFeeBankwestExportDS.GROUPFEEEXPORTBANKWESTABLE].Rows.Add(feeDebitRow);
                            }
                        }
                    }
                }
            }
        }

        private void SetAdviserFee(GroupFeeBankwestExportDS groupFeeBankwestExportDS, List<AccountProcessTaskEntity> ListAccountProcess, decimal difmFee, decimal diwmFee, decimal diyFee, decimal allPlatformFee)
        {
            DataRow feeDebitRow = groupFeeBankwestExportDS.Tables[GroupFeeBankwestExportDS.GROUPFEEEXPORTBANKWESTABLE].NewRow();
            feeDebitRow[GroupFeeBankwestExportDS.CID] = this.CID;
            feeDebitRow[GroupFeeBankwestExportDS.NARRATION] = groupFeeBankwestExportDS.Narration;

            IOrganizationUnit bankAccountDIWM = null;
            IOrganizationUnit bankAccountDIY = null;

            foreach (var accountProcessTaskEntity in ListAccountProcess)
            {
                if (accountProcessTaskEntity.LinkedEntity != null)
                {
                    if (accountProcessTaskEntity.LinkedEntityType == OrganizationType.BankAccount && accountProcessTaskEntity.LinkedEntity.Clid != Guid.Empty)
                    {
                        var asset = groupFeeBankwestExportDS.Assets.Where(var => var.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                        var model = groupFeeBankwestExportDS.Models.Where(mod => mod.ID == accountProcessTaskEntity.ModelID).FirstOrDefault();
                        var product = asset.Products.Where(pro => pro.ID == accountProcessTaskEntity.TaskID).FirstOrDefault();

                        if (accountProcessTaskEntity.LinkedEntity.Clid == Guid.Empty)
                        {
                            accountProcessTaskEntity.LinkedEntity.Clid = product.EntityId.Clid;
                            accountProcessTaskEntity.LinkedEntity.Csid = product.EntityId.Csid;
                        }

                        if (model.ServiceType == ServiceTypes.DoItWithMe)
                            bankAccountDIWM = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid,
                                                           accountProcessTaskEntity.LinkedEntity.Csid) as IOrganizationUnit;

                        if (model.ServiceType == ServiceTypes.DoItYourSelf)
                            bankAccountDIY = Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid,
                                                           accountProcessTaskEntity.LinkedEntity.Csid) as IOrganizationUnit;
                    }
                }
            }

            if (bankAccountDIWM != null)
            {
                Oritax.TaxSimp.Common.BankAccountEntity bankEnityDIWM = bankAccountDIWM.ClientEntity as Oritax.TaxSimp.Common.BankAccountEntity;
                feeDebitRow[GroupFeeBankwestExportDS.ACCOUNTNAME] = bankEnityDIWM.Name + " (DIWM) " + this.ClientId;
                feeDebitRow[GroupFeeBankwestExportDS.ACCOUNTNO] = bankEnityDIWM.AccountNumber;
                feeDebitRow[GroupFeeBankwestExportDS.BSB] = bankEnityDIWM.BSB;
            }
            else if (bankAccountDIY != null)
            {
                Oritax.TaxSimp.Common.BankAccountEntity bankEnityDIY = bankAccountDIY.ClientEntity as Oritax.TaxSimp.Common.BankAccountEntity;
                feeDebitRow[GroupFeeBankwestExportDS.ACCOUNTNAME] = bankEnityDIY.Name + " (DIWM) " + this.ClientId;
                feeDebitRow[GroupFeeBankwestExportDS.ACCOUNTNO] = bankEnityDIY.AccountNumber;
                feeDebitRow[GroupFeeBankwestExportDS.BSB] = bankEnityDIY.BSB;
            }
            else
            {
                feeDebitRow[GroupFeeBankwestExportDS.ACCOUNTNAME] = "Adviser Fee, No DIY or DIWM Bank Account Found for Client - (" + this.name + " " + this.ClientId + ")" + this.ClientId;
                feeDebitRow[GroupFeeBankwestExportDS.ACCOUNTNO] = String.Empty;
                feeDebitRow[GroupFeeBankwestExportDS.BSB] = String.Empty;
            }
            feeDebitRow[GroupFeeBankwestExportDS.AMOUNT] = diyFee + diwmFee + difmFee + allPlatformFee;

            groupFeeBankwestExportDS.Tables[GroupFeeBankwestExportDS.GROUPFEEEXPORTBANKWESTABLE].Rows.Add(feeDebitRow);
        }

        private void OnGetData(OrgUnitConfiguredFeesDS orgUnitConfiguredFeesDS)
        {
            IOrganization org = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;

            DataTable feesList = orgUnitConfiguredFeesDS.Tables[OrgUnitConfiguredFeesDS.FEESLIST];
            DataTable configuredfeesList = orgUnitConfiguredFeesDS.Tables[OrgUnitConfiguredFeesDS.CONFIGUREDFEES];

            foreach (FeeEntity feeEntity in org.FeeEnttyList)
            {
                DataRow row = feesList.NewRow();
                row[OrgUnitConfiguredFeesDS.ID] = feeEntity.ID;
                row[OrgUnitConfiguredFeesDS.DESCRIPTION] = feeEntity.Description;
                row[OrgUnitConfiguredFeesDS.YEAR] = feeEntity.Year;
                row[OrgUnitConfiguredFeesDS.STARTDATE] = feeEntity.StartDate;
                row[OrgUnitConfiguredFeesDS.ENDDATE] = feeEntity.EndDate;
                row[OrgUnitConfiguredFeesDS.COMMENT] = feeEntity.Comments;
                row[OrgUnitConfiguredFeesDS.FEETYPE] = feeEntity.FeeType;
                feesList.Rows.Add(row);
            }

            foreach (Guid feeEntityID in this.ConfiguredFeesList)
            {
                var feeEntity = org.FeeEnttyList.Where(fee => fee.ID == feeEntityID).FirstOrDefault();

                if (feeEntity != null)
                {
                    DataRow row = configuredfeesList.NewRow();
                    row[OrgUnitConfiguredFeesDS.ID] = feeEntity.ID;
                    row[OrgUnitConfiguredFeesDS.INS] = this.cID;
                    row[OrgUnitConfiguredFeesDS.DESCRIPTION] = feeEntity.Description;
                    row[OrgUnitConfiguredFeesDS.COMMENT] = feeEntity.Comments;
                    row[OrgUnitConfiguredFeesDS.YEAR] = feeEntity.Year;
                    row[OrgUnitConfiguredFeesDS.STARTDATE] = feeEntity.StartDate;
                    row[OrgUnitConfiguredFeesDS.ENDDATE] = feeEntity.EndDate;
                    row[OrgUnitConfiguredFeesDS.FEETYPE] = feeEntity.FeeType;
                    configuredfeesList.Rows.Add(row);
                }
            }

            Broker.ReleaseBrokerManagedComponent(org);
        }

        private void OnGetData(FeeTransactionsDS feeTransactionsDS)
        {
            DataTable feeTable = feeTransactionsDS.Tables[FeeTransactionsDS.FEETRANSACTIONS];

            foreach (FeeTransaction transaction in feeTransactions)
            {
                DataRow row = feeTable.NewRow();
                row[FeeTransactionsDS.ID] = transaction.ID;
                row[FeeTransactionsDS.INS] = this.CID;
                row[FeeTransactionsDS.FEERUNID] = transaction.FeeRunID;
                row[FeeTransactionsDS.DESCRIPTION] = transaction.Description;
                row[FeeTransactionsDS.YEAR] = transaction.Year;
                row[FeeTransactionsDS.MONTH] = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(transaction.Month);
                row[FeeTransactionsDS.CALCULATEDFEES] = transaction.Total;
                row[FeeTransactionsDS.TRANSACTIONDATE] = transaction.TransactionDate;

                if (transaction.TransactionType != null)
                    row[FeeTransactionsDS.TRANSACTIONTYPE] = transaction.TransactionType.TransactionDescription;
                else
                    row[FeeTransactionsDS.TRANSACTIONTYPE] = FeeTransaction.TransactionTypes.Where(tran => tran.Type == "EX").FirstOrDefault().TransactionDescription;

                feeTable.Rows.Add(row);
            }
        }

        private void OnGetData(FeeTransactionDetailsDS feeTransactionDetailsDS)
        {
            DataTable summaryTableByFeeType = feeTransactionDetailsDS.Tables[FeeTransactionDetailsDS.CALCULATEDFEESSUMMARYBYFEETYPE];
            IOrganization org = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            var feeTransaction = feeTransactions.Where(feeTran => feeTran.ID == feeTransactionDetailsDS.FeeTransactionID).FirstOrDefault();
            if (feeTransaction != null)
            {
                List<FeeEntity> feeEntities = new List<FeeEntity>();

                FeeRunTransactionsDetailsDS feeRunTransactionsDetailsDS = new Common.Data.FeeRunTransactionsDetailsDS();
                IFeeRun feeRun = this.Broker.GetBMCInstance(feeTransaction.FeeRunID) as IFeeRun;
                feeRun.GetData(feeRunTransactionsDetailsDS);

                if (feeTransaction != null)
                {
                    feeTransactionDetailsDS.FeeRunName = feeRun.Name;

                    CalculateFee(feeTransactionDetailsDS, org, feeTransaction, feeEntities, feeRunTransactionsDetailsDS);

                    var feeEntitiesGroup = feeEntities.GroupBy(feeEnt => feeEnt.ID);

                    foreach (var feeEntity in feeEntitiesGroup)
                    {
                        DataRow rowSummaryByType = summaryTableByFeeType.NewRow();
                        rowSummaryByType[FeeTransactionDetailsDS.ID] = feeEntity.FirstOrDefault().ID;
                        rowSummaryByType[FeeTransactionDetailsDS.INS] = this.CID;
                        rowSummaryByType[FeeTransactionDetailsDS.FEETYPE] = feeEntity.Key;
                        rowSummaryByType[FeeTransactionDetailsDS.FEETYPEENUM] = feeEntity.FirstOrDefault().FeeType;
                        rowSummaryByType[FeeTransactionDetailsDS.FEETYPEDESC] = Enumeration.GetDescription(feeEntity.FirstOrDefault().FeeType);
                        rowSummaryByType[FeeTransactionDetailsDS.FEESUBTYPE] = Enumeration.GetDescription(FeeSubType.OngoingFeeValue);
                        rowSummaryByType[FeeTransactionDetailsDS.COMMENTS] = feeEntity.FirstOrDefault().Comments;
                        rowSummaryByType[FeeTransactionDetailsDS.DESCRIPTION] = feeEntity.FirstOrDefault().Description;
                        rowSummaryByType[FeeTransactionDetailsDS.YEAR] = feeEntity.FirstOrDefault().Year;
                        rowSummaryByType[FeeTransactionDetailsDS.MONTH] = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(feeTransaction.Month);

                        decimal totalOngoingFeeValue = 0;
                        decimal totalOngoingFeeValueConsol = 0;
                        decimal totalOngoingFeeValueAutoAdj = 0;
                        decimal totalOngoingFeeValueWithAdj = 0;

                        foreach (FeeEntity feeItem in feeEntity)
                        {
                            if (feeRunTransactionsDetailsDS.ConfiguredFeesTemplateAtRun.Contains(feeItem.ID) || feeItem.FeeType == FeeType.Adviser)
                            {
                                totalOngoingFeeValueConsol += feeItem.OngoingFeeValueList.Sum(fee => fee.ConsolFeeForPeriod); ;
                                totalOngoingFeeValue += feeItem.OngoingFeeValueList.Sum(fee => fee.FeeForPeriod); ;
                                totalOngoingFeeValueAutoAdj += feeItem.OngoingFeeValueList.Sum(fee => fee.AutoAdjustmentForPeriod);
                                totalOngoingFeeValueWithAdj += feeItem.OngoingFeeValueList.Sum(fee => fee.FeeAdjForPeriod);
                            }
                        }

                        rowSummaryByType[FeeTransactionDetailsDS.CALCULATEDFEES] = totalOngoingFeeValue;
                        rowSummaryByType[FeeTransactionDetailsDS.AUTOADJUST] = totalOngoingFeeValueAutoAdj;
                        rowSummaryByType[FeeTransactionDetailsDS.FEETOTALADJUST] = totalOngoingFeeValueWithAdj;
                        rowSummaryByType[FeeTransactionDetailsDS.FEETYPEID] = feeEntity.Key;

                        feeTransaction.Total += totalOngoingFeeValue;
                        summaryTableByFeeType.Rows.Add(rowSummaryByType);

                        rowSummaryByType = summaryTableByFeeType.NewRow();
                        rowSummaryByType[FeeTransactionDetailsDS.ID] = feeEntity.FirstOrDefault().ID;
                        rowSummaryByType[FeeTransactionDetailsDS.INS] = this.CID;
                        rowSummaryByType[FeeTransactionDetailsDS.FEETYPE] = feeEntity.Key;
                        rowSummaryByType[FeeTransactionDetailsDS.FEETYPEENUM] = feeEntity.FirstOrDefault().FeeType;
                        rowSummaryByType[FeeTransactionDetailsDS.FEETYPEDESC] = Enumeration.GetDescription(feeEntity.FirstOrDefault().FeeType);
                        rowSummaryByType[FeeTransactionDetailsDS.FEESUBTYPE] = Enumeration.GetDescription(FeeSubType.OngoingFeePercent);
                        rowSummaryByType[FeeTransactionDetailsDS.COMMENTS] = feeEntity.FirstOrDefault().Comments;
                        rowSummaryByType[FeeTransactionDetailsDS.DESCRIPTION] = feeEntity.FirstOrDefault().Description;
                        rowSummaryByType[FeeTransactionDetailsDS.YEAR] = feeEntity.FirstOrDefault().Year;
                        rowSummaryByType[FeeTransactionDetailsDS.MONTH] = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(feeTransaction.Month);

                        decimal totalOngoingFeePercent = 0;
                        decimal totalOngoingFeePercentConsol = 0;
                        decimal totalOngoingFeePercentAutoAdj = 0;
                        decimal totalOngoingFeePercentWithAdj = 0;


                        foreach (FeeEntity feeItem in feeEntity)
                        {
                            if (feeRunTransactionsDetailsDS.ConfiguredFeesTemplateAtRun.Contains(feeItem.ID) || feeItem.FeeType == FeeType.Adviser)
                            {

                                totalOngoingFeePercent += feeItem.OngoingFeePercentList.Sum(fee => fee.FeeForPeriod); ;
                                totalOngoingFeePercentConsol += feeItem.OngoingFeePercentList.Sum(fee => fee.ConsolFeeForPeriod);
                                totalOngoingFeePercentAutoAdj += feeItem.OngoingFeePercentList.Sum(fee => fee.AutoAdjustmentForPeriod);
                                totalOngoingFeePercentWithAdj += feeItem.OngoingFeePercentList.Sum(fee => fee.FeeAdjForPeriod);
                            }
                        }

                        rowSummaryByType[FeeTransactionDetailsDS.CALCULATEDFEES] = totalOngoingFeePercent;
                        rowSummaryByType[FeeTransactionDetailsDS.AUTOADJUST] = totalOngoingFeePercentAutoAdj;
                        rowSummaryByType[FeeTransactionDetailsDS.FEETOTALADJUST] = totalOngoingFeePercentWithAdj;

                        feeTransaction.Total += totalOngoingFeePercent;
                        summaryTableByFeeType.Rows.Add(rowSummaryByType);

                        rowSummaryByType = summaryTableByFeeType.NewRow();
                        rowSummaryByType[FeeTransactionDetailsDS.ID] = feeEntity.FirstOrDefault().ID;
                        rowSummaryByType[FeeTransactionDetailsDS.INS] = this.CID;
                        rowSummaryByType[FeeTransactionDetailsDS.FEETYPE] = feeEntity.Key;
                        rowSummaryByType[FeeTransactionDetailsDS.FEETYPEENUM] = feeEntity.FirstOrDefault().FeeType;
                        rowSummaryByType[FeeTransactionDetailsDS.FEETYPEDESC] = Enumeration.GetDescription(feeEntity.FirstOrDefault().FeeType);
                        rowSummaryByType[FeeTransactionDetailsDS.FEESUBTYPE] = Enumeration.GetDescription(FeeSubType.TieredFee);
                        rowSummaryByType[FeeTransactionDetailsDS.COMMENTS] = feeEntity.FirstOrDefault().Comments;
                        rowSummaryByType[FeeTransactionDetailsDS.DESCRIPTION] = feeEntity.FirstOrDefault().Description;
                        rowSummaryByType[FeeTransactionDetailsDS.YEAR] = feeEntity.FirstOrDefault().Year;
                        rowSummaryByType[FeeTransactionDetailsDS.MONTH] = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(feeTransaction.Month);

                        decimal totalOngoingTierd = 0;
                        decimal totalOngoingTierdConsol = 0;
                        decimal totalOngoingTierdAdj = 0;
                        decimal totalOngoingTierdWithAdj = 0;

                        foreach (FeeEntity feeItem in feeEntity)
                        {
                            if (feeRunTransactionsDetailsDS.ConfiguredFeesTemplateAtRun.Contains(feeItem.ID) || feeItem.FeeType == FeeType.Adviser)
                            {
                                totalOngoingTierd += feeItem.FeeTieredRangeList.Sum(fee => fee.FeeForPeriod);
                                totalOngoingTierdConsol += feeItem.FeeTieredRangeList.Sum(fee => fee.ConsolFeeForPeriod);
                                totalOngoingTierdAdj += feeItem.FeeTieredRangeList.Sum(fee => fee.AutoAdjustmentForPeriod);
                                totalOngoingTierdWithAdj += feeItem.FeeTieredRangeList.Sum(fee => fee.FeeAdjForPeriod);
                            }
                        }

                        rowSummaryByType[FeeTransactionDetailsDS.CALCULATEDFEES] = totalOngoingTierd;
                        rowSummaryByType[FeeTransactionDetailsDS.AUTOADJUST] = totalOngoingTierdAdj;
                        rowSummaryByType[FeeTransactionDetailsDS.FEETOTALADJUST] = totalOngoingTierdWithAdj;

                        feeTransaction.Total += totalOngoingTierd;
                        summaryTableByFeeType.Rows.Add(rowSummaryByType);

                        rowSummaryByType = summaryTableByFeeType.NewRow();
                        rowSummaryByType[FeeTransactionDetailsDS.ID] = feeEntity.FirstOrDefault().ID;
                        rowSummaryByType[FeeTransactionDetailsDS.INS] = this.CID;
                        rowSummaryByType[FeeTransactionDetailsDS.FEETYPE] = feeEntity.Key;
                        rowSummaryByType[FeeTransactionDetailsDS.FEETYPEENUM] = feeEntity.FirstOrDefault().FeeType;
                        rowSummaryByType[FeeTransactionDetailsDS.AUTOADJUST] = feeEntity.Sum(t => t.AutoAdjustment);
                        rowSummaryByType[FeeTransactionDetailsDS.ADJUST] = feeEntity.Sum(t => t.Adjustment);
                        rowSummaryByType[FeeTransactionDetailsDS.FEETOTALADJUST] = totalOngoingTierdConsol + totalOngoingFeePercentConsol + totalOngoingFeeValueConsol;
                        rowSummaryByType[FeeTransactionDetailsDS.COMMENTS] = feeEntity.FirstOrDefault().Comments;
                        rowSummaryByType[FeeTransactionDetailsDS.DESCRIPTION] = "Total - " + feeEntity.FirstOrDefault().Description;
                        rowSummaryByType[FeeTransactionDetailsDS.YEAR] = feeEntity.FirstOrDefault().Year;
                        rowSummaryByType[FeeTransactionDetailsDS.MONTH] = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(feeTransaction.Month);
                        rowSummaryByType[FeeTransactionDetailsDS.CALCULATEDFEES] = totalOngoingTierd + totalOngoingFeePercent + totalOngoingFeeValue;
                        summaryTableByFeeType.Rows.Add(rowSummaryByType);

                    }
                }
            }

            Broker.ReleaseBrokerManagedComponent(org);
        }

        private void CalculateFee(FeeTransactionDetailsDS feeTransactionDetailsDS, IOrganization org, FeeTransaction feeTransaction, List<FeeEntity> feeEntities, FeeRunTransactionsDetailsDS feeRunTransactionsDetailsDS)
        {
            feeTransaction.Total = 0;
            feeTransactionDetailsDS.FeeTransaction = feeTransaction;
            FeeRatio feeRatio = new FeeRatio();
            Guid clientFeeEntityID = new Guid();

            foreach (FeeHoldingEntity feeHoldingEntity in feeTransaction.FeeHoldingEntityList)
            {
                if (feeTransactionDetailsDS.UseConsolidationRatios && feeTransactionDetailsDS.FeeRatios != null)
                {
                    var feeRatioFilterd = feeTransactionDetailsDS.FeeRatios.Where(t => t.HoldingDate.Date.Day == feeHoldingEntity.TransactionDate.Date.Day && t.ClientCID == this.CID).FirstOrDefault();
                    if (feeRatioFilterd != null)
                        feeRatio = feeRatioFilterd;
                }

                decimal totalHolding = feeHoldingEntity.TotalHoldingValue;

                if (feeTransactionDetailsDS.UseConsolidationRatios && feeRatio.Ratio != 0)
                {
                    totalHolding = feeRatio.TotalHolding;
                    feeHoldingEntity.CalculatedRatio = feeRatio.Ratio;
                    feeHoldingEntity.ConsolHolding = totalHolding;
                }
                else if (feeHoldingEntity.ConsolHolding > 0)
                    totalHolding = feeHoldingEntity.ConsolHolding;

                foreach (Guid feeEntityID in this.ConfiguredFeesList)
                {
                    if (feeRunTransactionsDetailsDS.ConfiguredFeesTemplateAtRun.Contains(feeEntityID))
                    {
                        var feeEntity = org.FeeEnttyList.Where(fee => fee.ID == feeEntityID).FirstOrDefault();
                        if (feeEntity != null)
                        {
                            if (feeEntity.ServiceTypes == feeHoldingEntity.ServiceTypes)
                            {
                                if (feeHoldingEntity.TransactionDate >= feeEntity.StartDate && feeHoldingEntity.TransactionDate <= feeEntity.EndDate)
                                {
                                    if (feeEntity.ProductConfgType == ProductConfgType.All)
                                    {
                                        feeEntity.CalculateFee(feeHoldingEntity.HoldingValue, feeHoldingEntity.TotalHoldingValue, feeHoldingEntity.ConsolHolding, feeTransaction.Month, feeTransaction.Year, feeHoldingEntity.CalculatedRatio);
                                        FeeEntity feeEntityCopy = ObjectCopier.Clone(feeEntity);
                                        if (feeEntityCopy.FeeTotalAdj != 0)
                                            feeEntities.Add(feeEntityCopy);
                                    }

                                    else if (feeEntity.ProductConfgType == ProductConfgType.ByExclusions)
                                    {
                                        if (!feeEntity.ProductList.Contains(feeHoldingEntity.ProductID))
                                        {
                                            feeEntity.CalculateFee(feeHoldingEntity.HoldingValue, feeHoldingEntity.TotalHoldingValue, feeHoldingEntity.ConsolHolding, feeTransaction.Month, feeTransaction.Year, feeHoldingEntity.CalculatedRatio);
                                            FeeEntity feeEntityCopy = ObjectCopier.Clone(feeEntity);
                                            if (feeEntityCopy.FeeTotalAdj != 0)
                                                feeEntities.Add(feeEntityCopy);
                                        }
                                    }

                                    else if (feeEntity.ProductConfgType == ProductConfgType.ByInclusions)
                                    {
                                        if (feeEntity.ProductList.Contains(feeHoldingEntity.ProductID))
                                        {
                                            feeEntity.CalculateFee(feeHoldingEntity.HoldingValue, feeHoldingEntity.TotalHoldingValue, feeHoldingEntity.ConsolHolding, feeTransaction.Month, feeTransaction.Year, feeHoldingEntity.CalculatedRatio);
                                            FeeEntity feeEntityCopy = ObjectCopier.Clone(feeEntity);
                                            if (feeEntityCopy.FeeTotalAdj != 0)
                                                feeEntities.Add(feeEntityCopy);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                // calculate client fee here 
                if (feeHoldingEntity.ServiceTypes == ServiceTypes.All && feeHoldingEntity.TotalHoldingValue != 0)
                {
                    if (feeTransaction.UseLocalFee)
                    {
                        FeeEntity feeEntityFromClient = FeeEntity.GetFeeEntity(ClientFeeEntity, this.ClientId, feeTransaction.Month, feeTransaction.Year);
                        feeEntityFromClient.ID = clientFeeEntityID;
                        feeEntityFromClient.FeeType = FeeType.Adviser;
                        feeEntityFromClient.CalculateFee(feeHoldingEntity.HoldingValue, feeHoldingEntity.TotalHoldingValue, feeHoldingEntity.ConsolHolding, feeTransaction.Month, feeTransaction.Year, feeHoldingEntity.CalculatedRatio);
                        FeeEntity feeEntityCopy = ObjectCopier.Clone(feeEntityFromClient);
                        feeEntities.Add(feeEntityFromClient);
                    }
                }
            }

            int daysInMonth = DateTime.DaysInMonth(feeTransaction.Year, feeTransaction.Month);

            foreach (Guid feeEntityID in this.ConfiguredFeesList)
            {
                if (feeRunTransactionsDetailsDS.ConfiguredFeesTemplateAtRun.Contains(feeEntityID))
                {
                    for (int day = 1; day <= daysInMonth; day++)
                    {
                        var feeEntity = org.FeeEnttyList.Where(fee => fee.ID == feeEntityID).FirstOrDefault();
                        if (feeEntity != null)
                        {
                            FeeHoldingEntity feeHolding = feeTransaction.FeeHoldingEntityList.Where(tran => tran.ServiceTypes == ServiceTypes.All).Where(tran => tran.TransactionDate.Date == new DateTime(feeTransaction.Year, feeTransaction.Month, day).Date).FirstOrDefault();
                            if (feeHolding != null && feeHolding.TotalHoldingValue != 0)
                            {
                                FeeEntity feeEntityCopy = ObjectCopier.Clone(feeEntity);
                                if (feeEntityCopy.OngoingFeeValueList.Count > 0)
                                {
                                    feeEntityCopy.CalculateFeeOnGoingValue(feeTransaction.Month, feeTransaction.Year);
                                    feeEntities.Add(feeEntityCopy);
                                }
                            }
                        }
                    }
                }
            }

            if (feeTransaction.UseLocalFee)
            {
                for (int day = 1; day <= daysInMonth; day++)
                {
                    FeeHoldingEntity feeHolding = feeTransaction.FeeHoldingEntityList.Where(tran => tran.ServiceTypes == ServiceTypes.All).Where(tran => tran.TransactionDate.Date == new DateTime(feeTransaction.Year, feeTransaction.Month, day).Date).FirstOrDefault();
                    if (feeHolding != null && feeHolding.TotalHoldingValue != 0)
                    {
                        FeeEntity feeEntityFromClient = FeeEntity.GetFeeEntity(ClientFeeEntity, this.ClientId, feeTransaction.Month, feeTransaction.Year);
                        feeEntityFromClient.ID = clientFeeEntityID;
                        feeEntityFromClient.FeeType = FeeType.Adviser;
                        FeeEntity feeEntityCopy = ObjectCopier.Clone(feeEntityFromClient);

                        if (feeEntityCopy.OngoingFeeValueList.Count > 0)
                        {
                            feeEntityCopy.CalculateFeeOnGoingValue(feeTransaction.Month, feeTransaction.Year);
                            feeEntities.Add(feeEntityCopy);
                        }
                    }
                }
            }
        }

        private void OnGetData(UMAEntityFeesDS umaEntityFeesDS)
        {
            IOrganization org = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
            int daysInMonth = DateTime.DaysInMonth(umaEntityFeesDS.Year, umaEntityFeesDS.Month);

            FeeTransaction feeTransaction = new FeeTransaction();
            feeTransaction.ConfiguredFeesList = this.ConfiguredFeesList;
            feeTransaction.Description = "Fee Run - " + DateTime.Today.ToShortDateString() + " " + DateTime.Today.ToShortTimeString();
            feeTransaction.Month = umaEntityFeesDS.Month;
            feeTransaction.Year = umaEntityFeesDS.Year;
            feeTransaction.FeeRunID = umaEntityFeesDS.FeeRunID;
            feeTransaction.ClientCID = this.CID;
            feeTransaction.ClientName = this.Name;
            feeTransaction.ClientID = this.ClientId;
            feeTransaction.AdviserCID = this.AdviserID;

            if (this.AdviserID != Guid.Empty)
            {
                OrganizationUnitCM ibmc = Broker.GetBMCInstance(feeTransaction.AdviserCID) as OrganizationUnitCM;
                feeTransaction.AdviserID = ibmc.ClientId;
                feeTransaction.AdviserName = ibmc.FullName();
            }

            DataTable summaryTable = umaEntityFeesDS.Tables[UMAEntityFeesDS.CALCULATEDFEESSUMMARY];
            DataTable summaryTableByFeeType = umaEntityFeesDS.Tables[UMAEntityFeesDS.CALCULATEDFEESSUMMARYBYFEETYPE];
            DataTable tierdFeeTable = umaEntityFeesDS.Tables[UMAEntityFeesDS.TIERDFEEBREAKDOWN];
            DataTable ongoingPercentTable = umaEntityFeesDS.Tables[UMAEntityFeesDS.ONGOINGPERCENTBREAKDOWN];
            DataTable ongoingValueTable = umaEntityFeesDS.Tables[UMAEntityFeesDS.ONGOINGVALUEBREAKDOWN];
            List<FeeEntity> feeEntities = new List<FeeEntity>();
            feeTransaction.FeeHoldingEntityList.Clear();

            for (int day = 1; day <= daysInMonth; day++)
            {
                HoldingRptDataSet holding = new HoldingRptDataSet();
                holding.ValuationDate = new DateTime(umaEntityFeesDS.Year, umaEntityFeesDS.Month, day);
                this.OnGetData(holding);

                var varHoldingTotal = holding.HoldingSummaryTable.Select().Where(col =>
                       col[holding.HoldingSummaryTable.SERVICETYPE].ToString().ToLower() != Enumeration.GetDescription(ServiceTypes.ManualAsset).ToLower());

                decimal totalHoldingOverall = varHoldingTotal.Sum(col => (decimal)col[holding.HoldingSummaryTable.HOLDING]);

                if (totalHoldingOverall != 0)
                {
                    var holdingTotalDIFM = holding.HoldingSummaryTable.Select().Where(col =>
                        col[holding.HoldingSummaryTable.SERVICETYPE].ToString().ToLower() == Enumeration.GetDescription(ServiceTypes.DoItForMe).ToLower());

                    foreach (DataRow row in holdingTotalDIFM)
                    {
                        string productId = row[holding.HoldingSummaryTable.PRODUCTID].ToString();

                        if (row[holding.HoldingSummaryTable.PRODUCTID] != DBNull.Value && row[holding.HoldingSummaryTable.PRODUCTID] != null &&
                                 productId != String.Empty)
                        {
                            Guid product = new Guid(productId);
                            decimal holdingTotal = (decimal)row[holding.HoldingSummaryTable.HOLDING];
                            if (holdingTotal != 0)
                            {
                                FeeHoldingEntity feeHoldingEntity = AddFeeEntity(feeTransaction, holding, holdingTotal, totalHoldingOverall, ServiceTypes.DoItForMe, product);
                                CalculateFeeEntity(totalHoldingOverall, umaEntityFeesDS, org, feeEntities, holdingTotal, ServiceTypes.DoItForMe, product, feeHoldingEntity.TransactionDate, feeHoldingEntity.CalculatedRatio, feeHoldingEntity.ConsolHolding);
                            }
                        }

                    }

                    var holdingTotalDIY = holding.HoldingSummaryTable.Select().Where(col =>
                        col[holding.HoldingSummaryTable.SERVICETYPE].ToString().ToLower() == Enumeration.GetDescription(ServiceTypes.DoItYourSelf).ToLower());

                    foreach (DataRow row in holdingTotalDIY)
                    {
                        string productId = row[holding.HoldingSummaryTable.PRODUCTID].ToString();

                        if (row[holding.HoldingSummaryTable.PRODUCTID] != DBNull.Value && row[holding.HoldingSummaryTable.PRODUCTID] != null &&
                                 productId != String.Empty)
                        {
                            Guid product = new Guid(productId);
                            decimal holdingTotal = (decimal)row[holding.HoldingSummaryTable.HOLDING];
                            if (holdingTotal != 0)
                            {
                                FeeHoldingEntity feeHoldingEntity = AddFeeEntity(feeTransaction, holding, holdingTotal, totalHoldingOverall, ServiceTypes.DoItYourSelf, product);
                                CalculateFeeEntity(totalHoldingOverall, umaEntityFeesDS, org, feeEntities, holdingTotal, ServiceTypes.DoItYourSelf, product, feeHoldingEntity.TransactionDate, feeHoldingEntity.CalculatedRatio, feeHoldingEntity.ConsolHolding);
                            }
                        }

                    }

                    var holdingTotalDIWM = holding.HoldingSummaryTable.Select().Where(col =>
                        col[holding.HoldingSummaryTable.SERVICETYPE].ToString().ToLower() == Enumeration.GetDescription(ServiceTypes.DoItWithMe).ToLower());

                    foreach (DataRow row in holdingTotalDIWM)
                    {
                        string productId = row[holding.HoldingSummaryTable.PRODUCTID].ToString();

                        if (row[holding.HoldingSummaryTable.PRODUCTID] != DBNull.Value && row[holding.HoldingSummaryTable.PRODUCTID] != null &&
                                 productId != String.Empty)
                        {
                            Guid product = new Guid(productId);
                            decimal holdingTotal = (decimal)row[holding.HoldingSummaryTable.HOLDING];
                            if (holdingTotal != 0)
                            {
                                FeeHoldingEntity feeHoldingEntity = AddFeeEntity(feeTransaction, holding, holdingTotal, totalHoldingOverall, ServiceTypes.DoItWithMe, product);
                                CalculateFeeEntity(totalHoldingOverall, umaEntityFeesDS, org, feeEntities, holdingTotal, ServiceTypes.DoItWithMe, product, feeHoldingEntity.TransactionDate, feeHoldingEntity.CalculatedRatio, feeHoldingEntity.ConsolHolding);
                            }
                        }

                    }

                    var holdingTotalCol = holding.HoldingSummaryTable.Select().Where(col =>
                        col[holding.HoldingSummaryTable.SERVICETYPE].ToString().ToLower() != Enumeration.GetDescription(ServiceTypes.ManualAsset).ToLower());

                    foreach (DataRow row in holdingTotalCol)
                    {
                        string productId = row[holding.HoldingSummaryTable.PRODUCTID].ToString();

                        if (row[holding.HoldingSummaryTable.PRODUCTID] != DBNull.Value && row[holding.HoldingSummaryTable.PRODUCTID] != null &&
                                 productId != String.Empty)
                        {
                            Guid product = new Guid(productId);
                            decimal holdingTotal = (decimal)row[holding.HoldingSummaryTable.HOLDING];
                            if (holdingTotal != 0)
                            {
                                FeeHoldingEntity feeHoldingEntity = AddFeeEntity(feeTransaction, holding, holdingTotal, totalHoldingOverall, ServiceTypes.All, product);
                                CalculateFeeEntity(totalHoldingOverall, umaEntityFeesDS, org, feeEntities, holdingTotal, ServiceTypes.All, product, feeHoldingEntity.TransactionDate, feeHoldingEntity.CalculatedRatio, feeHoldingEntity.ConsolHolding);

                                // calculate client fee here 
                                if (umaEntityFeesDS.UseLocalFee)
                                {
                                    FeeEntity feeEntityFromClient = FeeEntity.GetFeeEntity(ClientFeeEntity, this.ClientId, feeTransaction.Month, feeTransaction.Year);
                                    feeEntityFromClient.CalculateFee(feeHoldingEntity.HoldingValue, feeHoldingEntity.TotalHoldingValue, feeHoldingEntity.ConsolHolding, feeTransaction.Month, feeTransaction.Year, feeHoldingEntity.CalculatedRatio);
                                    FeeEntity feeEntityCopy = ObjectCopier.Clone(feeEntityFromClient);
                                    if (feeEntityFromClient.FeeTotalAdj != 0)
                                        feeEntities.Add(feeEntityFromClient);
                                }
                            }
                        }
                    }

                    decimal holdingTotalMAN = holding.HoldingSummaryTable.Select().Where(col =>
                        col[holding.HoldingSummaryTable.SERVICETYPE].ToString().ToLower() == Enumeration.GetDescription(ServiceTypes.ManualAsset).ToLower()).Sum(col => (decimal)col[holding.HoldingSummaryTable.TOTAL]);


                    if (holdingTotalMAN != 0)
                    {
                        FeeHoldingEntity feeHoldingEntity = AddFeeEntity(feeTransaction, holding, holdingTotalMAN, totalHoldingOverall, ServiceTypes.ManualAsset, Guid.Empty);
                        CalculateFeeEntity(totalHoldingOverall, umaEntityFeesDS, org, feeEntities, holdingTotalMAN, ServiceTypes.ManualAsset, Guid.Empty, new DateTime(umaEntityFeesDS.Year, umaEntityFeesDS.Month, daysInMonth), feeHoldingEntity.CalculatedRatio, feeHoldingEntity.ConsolHolding);
                    }
                }
            }

            int daysInMonthvalueFee = DateTime.DaysInMonth(umaEntityFeesDS.Year, umaEntityFeesDS.Month);

            foreach (Guid feeEntityID in this.ConfiguredFeesList)
            {
                if (umaEntityFeesDS.ConfiguredFeesTemplateAtRun.Contains(feeEntityID))
                {
                    for (int day = 1; day <= daysInMonthvalueFee; day++)
                    {
                        var feeEntity = org.FeeEnttyList.Where(fee => fee.ID == feeEntityID).FirstOrDefault();
                        if (feeEntity != null)
                        {
                            var holdingEnt = feeTransaction.FeeHoldingEntityList.Where(holding => holding.TransactionDate == new DateTime(umaEntityFeesDS.Year, umaEntityFeesDS.Month, day) && holding.ServiceTypes == ServiceTypes.All).FirstOrDefault();

                            if (holdingEnt != null && holdingEnt.TotalHoldingValue != 0)
                            {
                                FeeEntity feeEntityCopy = ObjectCopier.Clone(feeEntity);
                                if (feeEntityCopy.OngoingFeeValueList.Count > 0)
                                {
                                    feeEntityCopy.CalculateFeeOnGoingValue(feeTransaction.Month, feeTransaction.Year);
                                    if (feeEntityCopy.FeeTotalAdj != 0)
                                        feeEntities.Add(feeEntityCopy);
                                }
                            }
                        }
                    }
                }
            }

            if (umaEntityFeesDS.UseLocalFee)
            {
                for (int day = 1; day <= daysInMonthvalueFee; day++)
                {
                    FeeEntity feeEntityFromClient = FeeEntity.GetFeeEntity(ClientFeeEntity, this.ClientId, feeTransaction.Month, feeTransaction.Year);
                    FeeEntity feeEntityCopy = ObjectCopier.Clone(feeEntityFromClient);

                    if (feeEntityCopy.OngoingFeeValueList.Count > 0)
                    {
                        feeEntityCopy.CalculateFeeOnGoingValue(feeTransaction.Month, feeTransaction.Year);
                        if (feeEntityCopy.FeeTotalAdj != 0)
                            feeEntities.Add(feeEntityCopy);
                    }
                }
            }

            var feeEntitiesGroup = feeEntities.GroupBy(feeEnt => feeEnt.FeeType);

            foreach (var feeEntity in feeEntitiesGroup)
            {
                if (umaEntityFeesDS.FeeType.Contains(feeEntity.Key) || feeEntity.Key == FeeType.None)
                {
                    DataRow rowSummary = summaryTable.NewRow();
                    rowSummary[UMAEntityFeesDS.ID] = feeEntity.FirstOrDefault().ID;
                    rowSummary[UMAEntityFeesDS.INS] = this.CID;
                    rowSummary[UMAEntityFeesDS.FEETYPE] = Enumeration.GetDescription(feeEntity.Key);
                    rowSummary[UMAEntityFeesDS.FEETYPEENUM] = feeEntity.Key;
                    rowSummary[UMAEntityFeesDS.COMMENTS] = feeEntity.FirstOrDefault().Comments;
                    rowSummary[UMAEntityFeesDS.DESCRIPTION] = feeEntity.FirstOrDefault().Description;
                    rowSummary[UMAEntityFeesDS.YEAR] = feeEntity.FirstOrDefault().Year;
                    rowSummary[UMAEntityFeesDS.MONTH] = umaEntityFeesDS.Month;
                    rowSummary[UMAEntityFeesDS.SERVICETYPE] = feeEntity.FirstOrDefault().ServiceTypes;
                    rowSummary[UMAEntityFeesDS.CALCULATEDFEES] = feeEntity.Sum(fee => fee.FeeTotalAdj);
                    summaryTable.Rows.Add(rowSummary);

                    DataRow rowSummaryByType = summaryTableByFeeType.NewRow();
                    rowSummaryByType[UMAEntityFeesDS.ID] = feeEntity.FirstOrDefault().ID;
                    rowSummaryByType[UMAEntityFeesDS.INS] = this.CID;
                    rowSummaryByType[UMAEntityFeesDS.FEETYPE] = Enumeration.GetDescription(feeEntity.Key);
                    rowSummaryByType[UMAEntityFeesDS.FEETYPEENUM] = feeEntity.Key;
                    rowSummaryByType[UMAEntityFeesDS.FEESUBTYPE] = Enumeration.GetDescription(FeeSubType.OngoingFeeValue);
                    rowSummaryByType[UMAEntityFeesDS.COMMENTS] = feeEntity.FirstOrDefault().Comments;
                    rowSummaryByType[UMAEntityFeesDS.DESCRIPTION] = feeEntity.FirstOrDefault().Description;
                    rowSummaryByType[UMAEntityFeesDS.YEAR] = feeEntity.FirstOrDefault().Year;
                    rowSummaryByType[UMAEntityFeesDS.SERVICETYPE] = feeEntity.FirstOrDefault().ServiceTypes;
                    rowSummaryByType[UMAEntityFeesDS.MONTH] = umaEntityFeesDS.Month;

                    decimal totalOngoingFeeValue = 0;

                    foreach (FeeEntity feeItem in feeEntity)
                    {
                        if (umaEntityFeesDS.ConfiguredFeesTemplateAtRun.Contains(feeItem.ID) || feeItem.FeeType == FeeType.None)
                        {
                            decimal ongoingFeeValue = feeItem.OngoingFeeValueList.Sum(fee => fee.FeeForPeriod);
                            totalOngoingFeeValue += ongoingFeeValue;
                        }
                    }

                    rowSummaryByType[UMAEntityFeesDS.CALCULATEDFEES] = totalOngoingFeeValue;
                    feeTransaction.Total += totalOngoingFeeValue;
                    summaryTableByFeeType.Rows.Add(rowSummaryByType);

                    rowSummaryByType = summaryTableByFeeType.NewRow();
                    rowSummaryByType[UMAEntityFeesDS.ID] = feeEntity.FirstOrDefault().ID;
                    rowSummaryByType[UMAEntityFeesDS.INS] = this.CID;
                    rowSummaryByType[UMAEntityFeesDS.FEETYPE] = Enumeration.GetDescription(feeEntity.Key);
                    rowSummaryByType[UMAEntityFeesDS.FEETYPEENUM] = feeEntity.Key;
                    rowSummaryByType[UMAEntityFeesDS.FEESUBTYPE] = Enumeration.GetDescription(FeeSubType.OngoingFeePercent);
                    rowSummaryByType[UMAEntityFeesDS.COMMENTS] = feeEntity.FirstOrDefault().Comments;
                    rowSummaryByType[UMAEntityFeesDS.DESCRIPTION] = feeEntity.FirstOrDefault().Description;
                    rowSummaryByType[UMAEntityFeesDS.YEAR] = feeEntity.FirstOrDefault().Year;
                    rowSummaryByType[UMAEntityFeesDS.SERVICETYPE] = feeEntity.FirstOrDefault().ServiceTypes;
                    rowSummaryByType[UMAEntityFeesDS.MONTH] = umaEntityFeesDS.Month;

                    decimal totalOngoingFeePercent = 0;

                    foreach (FeeEntity feeItem in feeEntity)
                    {
                        if (umaEntityFeesDS.ConfiguredFeesTemplateAtRun.Contains(feeItem.ID) || feeItem.FeeType == FeeType.None)
                        {
                            decimal ongoingFeePercent = feeItem.OngoingFeePercentList.Sum(fee => fee.FeeForPeriod);
                            totalOngoingFeePercent += ongoingFeePercent;
                        }
                    }

                    rowSummaryByType[UMAEntityFeesDS.CALCULATEDFEES] = totalOngoingFeePercent;
                    feeTransaction.Total += totalOngoingFeePercent;
                    summaryTableByFeeType.Rows.Add(rowSummaryByType);

                    rowSummaryByType = summaryTableByFeeType.NewRow();
                    rowSummaryByType[UMAEntityFeesDS.ID] = feeEntity.FirstOrDefault().ID;
                    rowSummaryByType[UMAEntityFeesDS.INS] = this.CID;
                    rowSummaryByType[UMAEntityFeesDS.FEETYPE] = Enumeration.GetDescription(feeEntity.Key);
                    rowSummaryByType[UMAEntityFeesDS.FEETYPEENUM] = feeEntity.Key;
                    rowSummaryByType[UMAEntityFeesDS.FEESUBTYPE] = Enumeration.GetDescription(FeeSubType.TieredFee);
                    rowSummaryByType[UMAEntityFeesDS.COMMENTS] = feeEntity.FirstOrDefault().Comments;
                    rowSummaryByType[UMAEntityFeesDS.DESCRIPTION] = feeEntity.FirstOrDefault().Description;
                    rowSummaryByType[UMAEntityFeesDS.YEAR] = feeEntity.FirstOrDefault().Year;
                    rowSummaryByType[UMAEntityFeesDS.SERVICETYPE] = feeEntity.FirstOrDefault().ServiceTypes;
                    rowSummaryByType[UMAEntityFeesDS.MONTH] = umaEntityFeesDS.Month;

                    decimal totalOngoingTierd = 0;

                    foreach (FeeEntity feeItem in feeEntity)
                    {
                        if (umaEntityFeesDS.ConfiguredFeesTemplateAtRun.Contains(feeItem.ID) || feeItem.FeeType == FeeType.None)
                        {
                            decimal tieredFee = feeItem.FeeTieredRangeList.Sum(fee => fee.FeeForPeriod);
                            totalOngoingTierd += tieredFee;
                        }
                    }

                    rowSummaryByType[UMAEntityFeesDS.CALCULATEDFEES] = totalOngoingTierd;
                    feeTransaction.Total += totalOngoingTierd;
                    summaryTableByFeeType.Rows.Add(rowSummaryByType);
                }
            }

            umaEntityFeesDS.FeeTransaction = feeTransaction;

            Broker.ReleaseBrokerManagedComponent(org);
        }

        private void CalculateFeeEntityOngoingValue(int Month, int Year, List<FeeEntity> FeeEnttyList, List<FeeEntity> feeEntities, ServiceTypes serviceTypes, DateTime holdingDate)
        {
            foreach (Guid feeEntityID in this.ConfiguredFeesList)
            {
                var feeEntity = FeeEnttyList.Where(fee => fee.ID == feeEntityID).FirstOrDefault();

                if (feeEntity != null)
                {
                    if (holdingDate >= feeEntity.StartDate && holdingDate <= feeEntity.EndDate)
                    {
                        FeeEntity feeEntityCopy = ObjectCopier.Clone(feeEntity);
                        if (feeEntityCopy.OngoingFeeValueList.Count > 0)
                        {
                            feeEntityCopy.CalculateFeeOnGoingValue(Month, Year);
                            feeEntities.Add(feeEntityCopy);
                        }
                    }
                }
            }
        }

        private void CalculateFeeEntity(decimal totalHoldingForAllProducts, UMAEntityFeesDS umaEntityFeesDS, IOrganization org, List<FeeEntity> feeEntities, decimal holdingTotal, ServiceTypes serviceTypes, Guid productID, DateTime holdingDate, decimal holdingRatio, decimal consolHolding)
        {
            foreach (Guid feeEntityID in this.ConfiguredFeesList)
            {
                if (umaEntityFeesDS.ConfiguredFeesTemplateAtRun.Contains(feeEntityID))
                {
                    var feeEntity = org.FeeEnttyList.Where(fee => fee.ID == feeEntityID).FirstOrDefault();

                    if (feeEntity != null)
                    {
                        if (holdingDate >= feeEntity.StartDate && holdingDate <= feeEntity.EndDate)
                        {

                            if (feeEntity.ServiceTypes == serviceTypes)
                            {
                                if (feeEntity.ProductConfgType == ProductConfgType.All)
                                {
                                    feeEntity.CalculateFee(holdingTotal, totalHoldingForAllProducts, consolHolding, umaEntityFeesDS.Month, umaEntityFeesDS.Year, holdingRatio);
                                    FeeEntity feeEntityCopy = ObjectCopier.Clone(feeEntity);
                                    if (feeEntityCopy.FeeTotalAdj != 0)
                                        feeEntities.Add(feeEntityCopy);
                                }

                                else if (feeEntity.ProductConfgType == ProductConfgType.ByExclusions)
                                {
                                    if (!feeEntity.ProductList.Contains(productID))
                                    {
                                        feeEntity.CalculateFee(holdingTotal, totalHoldingForAllProducts, consolHolding, umaEntityFeesDS.Month, umaEntityFeesDS.Year, holdingRatio);
                                        FeeEntity feeEntityCopy = ObjectCopier.Clone(feeEntity);
                                        if (feeEntityCopy.FeeTotalAdj != 0)
                                            feeEntities.Add(feeEntityCopy);
                                    }
                                }

                                else if (feeEntity.ProductConfgType == ProductConfgType.ByInclusions)
                                {
                                    if (feeEntity.ProductList.Contains(productID))
                                    {
                                        feeEntity.CalculateFee(holdingTotal, totalHoldingForAllProducts, consolHolding, umaEntityFeesDS.Month, umaEntityFeesDS.Year, holdingRatio);
                                        FeeEntity feeEntityCopy = ObjectCopier.Clone(feeEntity);
                                        if (feeEntityCopy.FeeTotalAdj != 0)
                                            feeEntities.Add(feeEntityCopy);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void CalculateFeeEntity(decimal totalHoldingForAllProducts, FeeTransaction transaction, List<FeeEntity> overallfeeEntities, List<FeeEntity> feeEntities, decimal holdingTotal, ServiceTypes serviceTypes, Guid productID, DateTime holdingDate, decimal calculatedRatio, decimal consolHolding)
        {
            FeeRunTransactionsDetailsDS feeRunTransactionsDetailsDS = new Common.Data.FeeRunTransactionsDetailsDS();
            feeRunTransactionsDetailsDS.FeeRunTransactionsOperation = FeeRunTransactionsOperation.GetOnlyConfiguredTemplate;
            IBrokerManagedComponent feeRun = this.Broker.GetBMCInstance(transaction.FeeRunID);
            feeRun.GetData(feeRunTransactionsDetailsDS);

            foreach (Guid feeEntityID in this.ConfiguredFeesList)
            {
                if (feeRunTransactionsDetailsDS.ConfiguredFeesTemplateAtRun.Contains(feeEntityID))
                {
                    var feeEntity = overallfeeEntities.Where(fee => fee.ID == feeEntityID).FirstOrDefault();

                    if (feeEntity != null)
                    {
                        if (holdingDate >= feeEntity.StartDate && holdingDate <= feeEntity.EndDate)
                        {
                            if (feeEntity.ServiceTypes == serviceTypes || feeEntity.ServiceTypes == ServiceTypes.All)
                            {
                                if (feeEntity.ProductConfgType == ProductConfgType.All)
                                {

                                    feeEntity.CalculateFee(holdingTotal, totalHoldingForAllProducts, consolHolding, transaction.Month, transaction.Year, calculatedRatio);
                                    FeeEntity feeEntityCopy = ObjectCopier.Clone(feeEntity);
                                    if (feeEntityCopy.FeeTotalAdj != 0)
                                        feeEntities.Add(feeEntityCopy);
                                }

                                else if (feeEntity.ProductConfgType == ProductConfgType.ByExclusions)
                                {
                                    if (!feeEntity.ProductList.Contains(productID))
                                    {
                                        feeEntity.CalculateFee(holdingTotal, totalHoldingForAllProducts, consolHolding, transaction.Month, transaction.Year, calculatedRatio);
                                        FeeEntity feeEntityCopy = ObjectCopier.Clone(feeEntity);
                                        if (feeEntityCopy.FeeTotalAdj != 0)
                                            feeEntities.Add(feeEntityCopy);
                                    }
                                }

                                else if (feeEntity.ProductConfgType == ProductConfgType.ByInclusions)
                                {
                                    if (feeEntity.ProductList.Contains(productID))
                                    {
                                        feeEntity.CalculateFee(holdingTotal, totalHoldingForAllProducts, consolHolding, transaction.Month, transaction.Year, calculatedRatio);
                                        FeeEntity feeEntityCopy = ObjectCopier.Clone(feeEntity);
                                        if (feeEntityCopy.FeeTotalAdj != 0)
                                            feeEntities.Add(feeEntityCopy);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private static FeeHoldingEntity AddFeeEntity(FeeTransaction feeTransaction, HoldingRptDataSet holding, decimal productHoldingTotal, decimal holdingTotal, ServiceTypes serviceTypes, Guid productID)
        {
            FeeHoldingEntity feeHoldingEntity = new Common.UMAFee.FeeHoldingEntity();
            feeHoldingEntity.HoldingValue = productHoldingTotal;
            feeHoldingEntity.TotalHoldingValue = holdingTotal;
            feeHoldingEntity.ServiceTypes = serviceTypes;
            feeHoldingEntity.ProductID = productID;
            feeHoldingEntity.TransactionDate = holding.ValuationDate;
            feeTransaction.FeeHoldingEntityList.Add(feeHoldingEntity);

            return feeHoldingEntity;
        }

        #endregion

        #region OnSetData Methods

        protected ModifiedState OnSetData(OrganisationUnitDetailsDS objOUDetails)
        {
            SetShortEntityName(objOUDetails.Name, objOUDetails.IsNewOrganisation);

            this.LegalName = objOUDetails.LegalName;

            this.Identifier = objOUDetails.Identifier;

            return ModifiedState.UNKNOWN;
        }

        public void SetShortEntityName(string name, bool isNew)
        {
            name = StringUtilities.RemoveRecurringSpaces(name);

            // check if the name has changed, that the name is unique, if not then throw an exception to indicate that
            // it is not unique - in which case the UI catches the exception and displays a message to the user.
            if ((this.Name != name || isNew) &&
                !this.IsUniqueOrganisationName(name))
                throw new DisplayUserMessageException(new MessageBoxDefinition(MessageBoxDefinition.MSG_ENTER_DISPLAY_NAME), false, MessageDisplayMethod.MainWindowError);

            this.Name = name;

            //Always set the logical module name for the org unit to be the short entity name.
            ILogicalModule iLM = this.Broker.GetLogicalCM(this.CLID);
            iLM.Name = name;
            iLM.CalculateToken(true);

            //When setting the short entity name in the logical module
            //also change the name in any other scenarios for that BMC
            //so that these are always in sync, even though it should not be getting
            //used anymore.
            foreach (DictionaryEntry dictEn in iLM.Scenarios)
            {
                CMScenario cMScenario = (CMScenario)dictEn.Value;
                if (cMScenario.CIID != this.CIID)
                {
                    IBrokerManagedComponent orgUnit = (IBrokerManagedComponent)this.Broker.GetCMImplementation(cMScenario.CLID, cMScenario.CSID);
                    orgUnit.Name = name;
                    orgUnit.CalculateToken(true);
                    this.Broker.ReleaseBrokerManagedComponent(orgUnit);
                }
            }

            this.Broker.ReleaseBrokerManagedComponent(iLM);
        }

        protected ModifiedState OnSetData(ScenariosDS objData)
        {
            ModifiedState objState = ModifiedState.UNCHANGED;

            //Check for any scenario deletions
            DataTable objScenarios = objData.Tables[ScenariosDS.SCENARIOS_TABLE];
            DataTable objScenariosToDelete = objScenarios.GetChanges(DataRowState.Deleted);
            if (objScenariosToDelete != null)
            {
                objScenariosToDelete.RejectChanges();

                foreach (DataRow objDeletedRow in objScenariosToDelete.Rows)
                {
                    Guid cSIDToDelete = new Guid(objDeletedRow[ScenariosDS.CSID_FIELD].ToString());

                    ArrayList objLastScenarioConflicts = new ArrayList();
                    ArrayList objMemberOfGroupScenario = new ArrayList();
                    ArrayList objLockedScenarios = new ArrayList();

                    //If the scenario is locked, then throw exception and do not go any further.
                    ILogicalModule entityLM = this.Broker.GetLogicalCM(this.CLID);
                    if (entityLM.IsScenarioLocked(cSIDToDelete))
                        throw new ScenarioLockException();

                    if (this is IEntity)
                    {
                        //If the Oganizational unit receiving the dataset is not the one being
                        //deleted, then need to get the Entity instance being deleted and call the method on it.
                        if (this.CSID == cSIDToDelete)
                            this.DeleteScenario(cSIDToDelete, this.CSID, objLastScenarioConflicts, objMemberOfGroupScenario, objLockedScenarios);
                        else
                        {
                            OrganizationUnitCM entityScenarioToDelete = (OrganizationUnitCM)entityLM[cSIDToDelete];
                            entityScenarioToDelete.DeleteScenario(cSIDToDelete, cSIDToDelete, objLastScenarioConflicts, objMemberOfGroupScenario, objLockedScenarios);
                            this.Broker.ReleaseBrokerManagedComponent(entityScenarioToDelete);
                        }

                        objState = ModifiedState.MODIFIED;
                        // check if we are the member of a group scenario
                        if (objMemberOfGroupScenario.Count > 0)
                            throw new DisplayUserMessageException(new MessageBoxDefinition(MessageBoxDefinition.MSG_DELETE_SCENARIO_FAILURE), false, MessageDisplayMethod.MainWindowError);

                        // check if we had any conflicts, if we did, alert the user
                        if (objLastScenarioConflicts.Count > 0)
                            throw new DisplayUserMessageException(new MessageBoxDefinition(MessageBoxDefinition.MSG_DELETE_LAST_SCENARIO, new string[] { this.Name }), false, MessageDisplayMethod.MainWindowError);
                    }
                    else if (this is IGroup)
                    {
                        if (objData.DeleteInMembers &&
                            !this.IsMemberOfOtherGroupScenario(cSIDToDelete) &&
                            !objData.GetUserConfirmed(DeleteScenarioConfirmKey))
                            ((IGroup)this).CheckIfStructureWillBeTimeIntensive(DeleteScenarioConfirmKey, BusinessStructureNavigationDirection.DownAndAcross);


                        if (this.CSID == cSIDToDelete)
                            ((IGroup)this).DeleteScenario(cSIDToDelete, objData.DeleteInMembers, objLastScenarioConflicts, objMemberOfGroupScenario, objLockedScenarios);
                        else
                        {
                            ILogicalModule groupLM = this.Broker.GetLogicalCM(this.CLID);
                            IGroup groupScenarioToDelete = (IGroup)groupLM[cSIDToDelete];
                            groupScenarioToDelete.DeleteScenario(cSIDToDelete, objData.DeleteInMembers, objLastScenarioConflicts, objMemberOfGroupScenario, objLockedScenarios);
                            this.Broker.ReleaseBrokerManagedComponent(groupLM);
                            this.Broker.ReleaseBrokerManagedComponent(groupScenarioToDelete);
                        }

                        // check if we had any conflicts, if we did, alert the user
                        if (objLastScenarioConflicts.Count > 0)
                        {
                            string strErrors = "<br>";
                            foreach (string strLastScenarioConflictName in objLastScenarioConflicts)
                                strErrors += " - " + strLastScenarioConflictName + "<br>";

                            objData.AddWarningUserDisplayMessage(new MessageBoxDefinition(MessageBoxDefinition.MSG_DELETE_LAST_SCENARIO, new string[] { strErrors }).ToString());
                        }

                        if (objLockedScenarios.Count > 0)
                        {
                            string strErrors = "<br>";
                            foreach (string strScenarioConflictName in objLockedScenarios)
                                strErrors += " - " + strScenarioConflictName + "<br>";

                            objData.AddWarningUserDisplayMessage(new MessageBoxDefinition(MessageBoxDefinition.MSG_DELETE_LOCKED_SCENARIO_FAILURE_GROUPMEMBER, new string[] { strErrors }).ToString());
                        }

                        if (objMemberOfGroupScenario.Count > 0)
                        {
                            string strErrors = "<br>";
                            foreach (string strScenarioConflictName in objMemberOfGroupScenario)
                                strErrors += " - " + strScenarioConflictName + "<br>";

                            objData.AddWarningUserDisplayMessage(new MessageBoxDefinition(MessageBoxDefinition.MSG_DELETE_SCENARIO_FAILURE_GROUPMEMBERS, new string[] { strErrors }).ToString());
                        }
                    }

                    this.Broker.ReleaseBrokerManagedComponent(entityLM);
                }
            }

            //Check to see if the current period has been changed
            DataRow[] objScenariosCurrent = objData.Tables[ScenariosDS.SCENARIOS_TABLE].Select(ScenariosDS.CURRENT_FIELD + " = " + true.ToString());
            if (objScenariosCurrent.Length > 0)
            {
                Guid objCSID = new Guid(objScenariosCurrent[0][ScenariosDS.CSID_FIELD].ToString());
                ILogicalModule objOrgUnitLCM = this.Broker.GetLogicalCM(this.CLID);

                CMScenario cMScenario = objOrgUnitLCM.GetScenario(objCSID);
                if (objOrgUnitLCM.CurrentScenario != objCSID)
                {
                    objOrgUnitLCM.CurrentScenario = objCSID;
                    objOrgUnitLCM.CalculateToken(true);

                    this.Broker.LogEvent(EventType.SetCurrentScenario, Guid.Empty, "Current scenario for '" + objOrgUnitLCM.Name + "' changed to '" + cMScenario.Name + "' in '" + this.ConstructBreadcrumb() + this.ConstructBreadcrumbPeriod() + this.ConstructScenarioBreadcrumb() + "'");
                }

                this.Broker.ReleaseBrokerManagedComponent(objOrgUnitLCM);
            }

            return objState;
        }

        private void FixPhones(List<Oritax.TaxSimp.Common.IdentityCMDetail> Signatories)
        {
            foreach (Oritax.TaxSimp.Common.IdentityCMDetail IdentityCMDetail in Signatories)
            {
                IOrganizationUnit individualOrgUnit = Broker.GetCMImplementation(IdentityCMDetail.Clid, IdentityCMDetail.Csid) as IOrganizationUnit;
                ClientDataOperationDS clientDataOperationDS = new ClientDataOperationDS();
                clientDataOperationDS.CommandType = ClientDataSetOperationCommandTypes.PhoneFix;

                if (individualOrgUnit != null)
                {
                    individualOrgUnit.SetData(clientDataOperationDS);
                }
            }
        }

        protected override ModifiedState OnSetData(DataSet data)
        {
            _updationDate = DateTime.Now;

            base.OnSetData(data);

            if (data is BGLExportDS)
            {
                BGLExportDS bglExportDS = (BGLExportDS)data;
                this.bglAccountCode = bglExportDS.BGLAccountCode;
            }

            else if (data is OtherServicesDS)
            {
                OtherServicesDS otherServicesDS = (OtherServicesDS)data;
                IsBGL = otherServicesDS.IsBGL;
                IsClassSuper = otherServicesDS.IsClassSuper;
                IsDesktopSuper = otherServicesDS.IsDesktopSuper;
            }

            else if (data is ClientDataOperationDS)
            {
                ClientDataOperationDS clientDataOperationDS = (ClientDataOperationDS)data;
                if (clientDataOperationDS.CommandType == ClientDataSetOperationCommandTypes.PhoneFix)
                {
                    if (this.IsCorporateType())
                    {
                        CorporateEntity corporateEntity = this.ClientEntity as CorporateEntity;
                        FixPhones(corporateEntity.Signatories);
                    }
                    else
                    {
                        ClientIndividualEntity clientIndividualEntity = this.ClientEntity as ClientIndividualEntity;
                        FixPhones(clientIndividualEntity.Applicants);
                    }
                }
            }

            else if (data is ClientAdvisersAssocDS)
            {
                ClientAdvisersAssocDS clientAdvisersAssocDS = (ClientAdvisersAssocDS)data;
                if (clientAdvisersAssocDS.AdviserCID != string.Empty)
                {
                    ICalculationModule adviserCM = this.Broker.GetBMCInstance(new Guid(clientAdvisersAssocDS.AdviserCID)) as ICalculationModule;
                    if (adviserCM != null)
                    {
                        if (((ICmHasParent)this).Parent != null)// remove from previous Advisor
                        {
                            var advisor = ((ICmHasParent)this).Parent;
                            var prevadvisercm = (this.Broker.GetBMCInstance(advisor.Cid) as ICmHasChildren) ?? (this.Broker.GetCMImplementation(advisor.Clid, advisor.Csid) as ICmHasChildren);

                            if (prevadvisercm != null && prevadvisercm.Children != null)
                            {
                                var temchild = prevadvisercm.Children.Where(icm => (icm.Cid == this.cID) || (icm.Clid == this.Clid && icm.Csid == this.Csid)).FirstOrDefault();
                                if (temchild != null) prevadvisercm.Children.Remove(temchild);
                            }
                        }


                        ((ICmHasParent)this).Parent = new IdentityCM { Cid = adviserCM.CID, Clid = adviserCM.CLID, Csid = adviserCM.CSID };


                        ICmHasChildren adviserCMWithChildren = adviserCM as ICmHasChildren;
                        var child = adviserCMWithChildren.Children.Where(icm => icm.Cid == this.cID).FirstOrDefault();
                        if (child == null)
                        {
                            child = adviserCMWithChildren.Children.Where(icm => icm.Clid == this.Clid && icm.Csid == this.Csid).FirstOrDefault();
                            if (child == null)
                            {
                                IdentityCM childIdentityCM = new IdentityCM(this.cID);
                                childIdentityCM.Csid = this.Csid;
                                childIdentityCM.Clid = this.Clid;
                                adviserCMWithChildren.Children.Add(childIdentityCM);
                            }
                            else
                            {
                                child.Cid = this.cID;
                                child.Csid = this.Csid;
                                child.Clid = this.Clid;
                            }
                        }
                        else
                        {
                            child.Cid = this.cID;
                            child.Csid = this.Csid;
                            child.Clid = this.Clid;
                        }

                        adviserCM.CalculateToken(true);
                    }
                }
            }

            else if (data is P2ReportsDS)
            {
                var ds = data as P2ReportsDS;
                if (this.ClientEntity is CorporateEntity)
                {
                    var entity = this.ClientEntity as CorporateEntity;
                    if (entity.P2Reports == null)
                    {
                        entity.P2Reports = new List<P2ReportData>();
                    }

                    AddP2ReportsData(ds, entity.P2Reports);
                }
                else if (this.ClientEntity is ClientIndividualEntity)
                {
                    var entity = this.ClientEntity as ClientIndividualEntity;
                    if (entity.P2Reports == null)
                    {
                        entity.P2Reports = new List<P2ReportData>();
                    }
                    AddP2ReportsData(ds, entity.P2Reports);
                }
            }

            else if (data is MDAExecutionDS)
            {
                var ds = data as MDAExecutionDS;

                if (ds.DataSetOperationType == DataSetOperationType.UpdateSingle)
                {
                    DataRow row = ds.Tables[MDAExecutionTable.TABLENAME].Rows[0];
                    if (this.IsCorporateType())
                    {
                        var entity = this.ClientEntity as CorporateEntity;
                        if (entity != null)
                        {
                            DateTime? mdaExecutionDate = row[MDAExecutionTable.MDAEXECUTIONDATE] == null ?
                                (DateTime?)null : Convert.ToDateTime(row[MDAExecutionTable.MDAEXECUTIONDATE].ToString());
                            if (mdaExecutionDate.HasValue)
                                entity.MDAExecutionDate = mdaExecutionDate;

                        }
                    }
                    else
                    {
                        var entity = this.ClientEntity as ClientIndividualEntity;
                        if (entity != null)
                        {
                            DateTime? mdaExecutionDate = row[MDAExecutionTable.MDAEXECUTIONDATE] == null ?
                                (DateTime?)null : Convert.ToDateTime(row[MDAExecutionTable.MDAEXECUTIONDATE].ToString());
                            if (mdaExecutionDate.HasValue)
                                entity.MDAExecutionDate = mdaExecutionDate;

                        }
                    }
                }

            }

            else if (data is OrgUnitConfiguredFeesDS)
            {
                OrgUnitConfiguredFeesDS orgUnitConfiguredFeesDS = (OrgUnitConfiguredFeesDS)data;
                OnSetData(orgUnitConfiguredFeesDS);
            }

            else if (data is ClientFeesDS)
            {
                ClientFeesDS clientFeesDS = (ClientFeesDS)data;
                if (clientFeesDS.DataSetOperationType == DataSetOperationType.UpdateSingle)
                    this.clientFeeEntity = clientFeesDS.ClientFeeEntity;
            }

            else if (data is GroupFeeRunDS)
            {
                GroupFeeRunDS groupFeeRunDS = (GroupFeeRunDS)data;
                if (groupFeeRunDS.DataSetOperationType == DataSetOperationType.DeletSingle)
                {
                    FeeTransaction feeTransaction = FeeTransactions.Where(tran => tran.FeeRunID == groupFeeRunDS.FeeRunID).FirstOrDefault();

                    if (feeTransaction != null)
                        FeeTransactions.Remove(feeTransaction);
                }
            }

            else if (data is FeeTransactionDetailsDS)
            {
                FeeTransactionDetailsDS feeTransactionDetailsDS = (FeeTransactionDetailsDS)data;

                if (feeTransactionDetailsDS.DataSetOperationType == DataSetOperationType.DeletSingle)
                {
                    var feeTransaction = this.feeTransactions.Where(fee => fee.ID == feeTransactionDetailsDS.FeeTransactionID).FirstOrDefault();
                    if (feeTransaction != null)
                    {
                        if (feeTransaction.FeeRunID != Guid.Empty)
                        {
                            feeTransactionDetailsDS.ClientID = this.cID;
                            IBrokerManagedComponent bmc = Broker.GetBMCInstance(feeTransaction.FeeRunID) as IBrokerManagedComponent;
                            if (bmc != null)
                            {
                                bmc.SetData(data);
                                Broker.ReleaseBrokerManagedComponent(bmc);
                            }
                        }

                        feeTransactions.Remove(feeTransaction);
                    }
                }

                if (feeTransactionDetailsDS.DataSetOperationType == DataSetOperationType.UpdateSingle)
                {
                    var feeTransactionsFiltered = this.feeTransactions.Where(fee => fee.ID == feeTransactionDetailsDS.FeeTransactionID).ToList();
                    var feeTransaction = feeTransactionsFiltered.FirstOrDefault();
                    this.feeTransactions.Remove(feeTransaction);

                    if (feeTransaction != null && feeTransaction.FeeRunID != Guid.Empty)
                    {
                        IOrganization org = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                        List<FeeEntity> feeEntities = new List<FeeEntity>();
                        FeeRunTransactionsDetailsDS feeRunTransactionsDetailsDS = new Common.Data.FeeRunTransactionsDetailsDS();
                        IBrokerManagedComponent feeRun = this.Broker.GetBMCInstance(feeTransaction.FeeRunID);
                        feeRun.GetData(feeRunTransactionsDetailsDS);

                        if (feeTransactionDetailsDS.UseConsolidationRatios = true)
                        {
                            CalculateFee(feeTransactionDetailsDS, org, feeTransaction, feeEntities, feeRunTransactionsDetailsDS);
                            this.feeTransactions.Add(feeTransaction);
                            feeTransactionDetailsDS.FeeTransaction = feeTransaction;
                            feeTransactionDetailsDS.ClientID = this.CID;
                            IBrokerManagedComponent bmc = Broker.GetBMCInstance(feeTransaction.FeeRunID) as IBrokerManagedComponent;
                            if (bmc != null)
                            {
                                bmc.SetData(data);
                                bmc.CalculateToken(true);
                            }
                        }
                    }
                }

                if (feeTransactionDetailsDS.DataSetOperationType == DataSetOperationType.UpdateBulk)
                {
                    var feeTransactionsFiltered = this.feeTransactions.Where(fee => fee.ID == feeTransactionDetailsDS.FeeTransactionID);
                    var feeTransaction = feeTransactionsFiltered.FirstOrDefault();
                    feeTransaction.ManAdjustment = feeTransactionDetailsDS.ManAdjustment;

                    if (feeTransaction != null && feeTransaction.FeeRunID != Guid.Empty)
                    {
                        IBrokerManagedComponent bmc = Broker.GetBMCInstance(feeTransaction.FeeRunID) as IBrokerManagedComponent;
                        if (bmc != null)
                        {
                            bmc.SetData(feeTransactionDetailsDS);
                            bmc.CalculateToken(true);
                        }
                    }
                }
            }

            else if (data is GroupFeeTransactionDetailsDS)
            {
                GroupFeeTransactionDetailsDS groupFeeRunDS = (GroupFeeTransactionDetailsDS)data;

                if (groupFeeRunDS.DataSetOperationType == DataSetOperationType.DeletSingle)
                {
                    FeeTransaction feeTransaction = FeeTransactions.Where(tran => tran.FeeRunID == groupFeeRunDS.FeeRunID).FirstOrDefault();

                    if (feeTransaction != null)
                        FeeTransactions.Remove(feeTransaction);
                }
            }

            if (data is UMAEntityFeesDS)
            {
                UMAEntityFeesDS umaEntityFeesDS = (UMAEntityFeesDS)data;

                if (umaEntityFeesDS.DataSetOperationType == DataSetOperationType.NewSingle)
                {
                    if (feeTransactions.Count > 0)
                    {
                        var feeTransaction = this.feeTransactions.Where(fee => fee.FeeRunID == umaEntityFeesDS.FeeTransaction.FeeRunID).FirstOrDefault();

                        if (feeTransaction == null)
                            this.feeTransactions.Add(umaEntityFeesDS.FeeTransaction);
                        else
                        {
                            feeTransactions.Remove(feeTransaction);
                            feeTransactions.Add(umaEntityFeesDS.FeeTransaction);
                        }
                    }
                    else
                        feeTransactions.Add(umaEntityFeesDS.FeeTransaction);
                }

                FeeTransactionDetailsDS feeTransactionDetailsDS = new FeeTransactionDetailsDS();
                feeTransactionDetailsDS.FeeTransactionID = umaEntityFeesDS.FeeTransaction.ID;
                this.GetData(feeTransactionDetailsDS);
            }

            if (data is OrganisationUnitDetailsDS)
            {
                OrganisationUnitDetailsDS organisationUnitDetailsDS = (OrganisationUnitDetailsDS)data;
                OnSetData(organisationUnitDetailsDS);
            }

            if (data is ClientAccountsDetailsDS)
            {
                ClientAccountsDetailsDS clientAccountsDetailsDS = (ClientAccountsDetailsDS)data;

                this.IsInvestableClient = true;

                if (clientAccountsDetailsDS.ClientEntity is CorporateEntity)
                {
                    CorporateEntity entityNew = (CorporateEntity)clientAccountsDetailsDS.ClientEntity;
                    CorporateEntity entityOld = (CorporateEntity)this.ClientEntity;



                    entityOld.ClientId = entityNew.ClientId;
                    entityOld.Name = entityNew.Name;
                    entityOld.ClientName = entityNew.ClientName;
                    entityOld.LegalName = entityNew.LegalName;
                    entityOld.CorporateTrusteeName = entityNew.CorporateTrusteeName;
                    entityOld.EntityName = entityNew.EntityName;


                    entityOld.InvestorType = entityNew.InvestorType;
                    entityOld.ACN = entityNew.ACN;
                    entityOld.AdvisorCode = entityNew.AdvisorCode;
                    entityOld.ABN = entityNew.ABN;

                    entityOld.Address.BusinessAddress.Addressline1 = entityNew.Address.BusinessAddress.Addressline1;
                    entityOld.Address.BusinessAddress.Addressline2 = entityNew.Address.BusinessAddress.Addressline2;
                    entityOld.Address.BusinessAddress.Country = entityNew.Address.BusinessAddress.Country;
                    entityOld.Address.BusinessAddress.State = entityNew.Address.BusinessAddress.State;
                    entityOld.Address.BusinessAddress.PostCode = entityNew.Address.BusinessAddress.PostCode;
                    entityOld.Address.BusinessAddress.Suburb = entityNew.Address.BusinessAddress.Suburb;


                    entityOld.AccountDesignation = entityNew.AccountDesignation;
                    entityOld.InvestmentDetails = entityNew.InvestmentDetails;

                    entityOld.TFN = entityNew.TFN;
                    entityOld.TIN = entityNew.TIN;
                    entityOld.TrustABN = entityNew.TrustABN;
                    entityOld.TrusteeType = entityNew.TrusteeType;
                    entityOld.TrustPassword = entityNew.TrustPassword;


                }

                else if (clientAccountsDetailsDS.ClientEntity is ClientIndividualEntity)
                {
                    ClientIndividualEntity entityNew = (ClientIndividualEntity)clientAccountsDetailsDS.ClientEntity;
                    ClientIndividualEntity entityOld = (ClientIndividualEntity)this.ClientEntity;

                    entityOld.Name = entityNew.Name;
                    entityOld.ClientId = entityNew.ClientId;
                    entityOld.InvestmentDetails = entityNew.InvestmentDetails;
                    entityOld.LegalName = entityNew.LegalName;

                    entityOld.Address.MailingAddress.Addressline1 = entityNew.Address.MailingAddress.Addressline1;
                    entityOld.Address.MailingAddress.Addressline2 = entityNew.Address.MailingAddress.Addressline2;
                    entityOld.Address.MailingAddress.Country = entityNew.Address.MailingAddress.Country;
                    entityOld.Address.MailingAddress.State = entityNew.Address.MailingAddress.State;

                    entityOld.ABN = entityNew.ABN;
                    entityOld.TFN = entityNew.TFN;

                }

            }
            else
                if (data is OrganizationUnitDS)
                {
                    OrganizationUnitDS objData = data as OrganizationUnitDS;
                    if (objData.Tables[OrganizationUnitDS.ORGUNIT_TABLE].Rows.Count == 1)
                    {
                        DataRow row = objData.Tables[OrganizationUnitDS.ORGUNIT_TABLE].Rows[0];
                        this.CurrentPeriod = (Guid)row[OrganizationUnitDS.ORGUNITCURRPERIOD_FIELD];
                        this.m_strLegalName = row[OrganizationUnitDS.ORGUNITLEGALNAME_FIELD].ToString();
                        this.m_strIdentifier = row[OrganizationUnitDS.ORGUNITIDENT_FIELD].ToString();
                    }

                    m_objPeriodsHT.Clear();
                    foreach (DataRow row in objData.Tables[OrganizationUnitDS.ORGUNITPERIODS_TABLE].Rows)
                    {
                        AddNewMemberPeriodDetails(
                            (Guid)row[OrganizationUnitDS.ORGUNITPERIODKEY_FIELD],
                            (DateTime)row[OrganizationUnitDS.ORGUNITPERIODSTARTDATE_FIELD],
                            (DateTime)row[OrganizationUnitDS.ORGUNITPERIODENDDATE_FIELD],
                            row[OrganizationUnitDS.ORGUNITPERIODTYPE_FIELD].ToString()
                            );
                    }
                    return ModifiedState.MODIFIED;
                }
                else if (data is OrganisationUnitDetailsDS)
                {
                    return this.OnSetData((OrganisationUnitDetailsDS)data);
                }
                else if (data is ScenariosDS)
                {
                    return this.OnSetData((ScenariosDS)data);
                }
                else if (data is ScenarioDS)
                {
                    return this.OnSetData((ScenarioDS)data);
                }
                else if (data is ClientIndividualShareDS)
                {
                    this.OnSetData((ClientIndividualShareDS)data);

                }
                else if (data is ConsolidationFeeDS)
                {
                    ConsolidationFeeDS consolidationFeeDS = (ConsolidationFeeDS)data;

                    if (consolidationFeeDS.DataSetOperationType == DataSetOperationType.UpdateSingle)
                    {

                        var selectedTransactions = this.feeTransactions.Where(t => t.FeeRunID == consolidationFeeDS.FeeRunID).ToList();

                        foreach (FeeTransaction feeTran in selectedTransactions)
                        {
                            FeeTransactionDetailsDS feeTransactionDetailsDS = new FeeTransactionDetailsDS();
                            feeTransactionDetailsDS.DataSetOperationType = DataSetOperationType.UpdateSingle;
                            feeTransactionDetailsDS.FeeTransactionID = feeTran.ID;
                            feeTransactionDetailsDS.UseConsolidationRatios = true;
                            feeTransactionDetailsDS.FeeRatios = consolidationFeeDS.FeeRatios;
                            this.SetData(feeTransactionDetailsDS);
                        }
                    }

                    else if (consolidationFeeDS.DataSetOperationType == DataSetOperationType.UpdateBulk)
                    {
                        var feeGroup = consolidationFeeDS.Tables[ConsolidationFeeDS.CONSOLIDATIONFEETABLE].Select().Where(c => c[ConsolidationFeeDS.CLIENTCID].ToString() == this.CID.ToString());

                        foreach (var fee in feeGroup)
                        {
                            if (fee[ConsolidationFeeDS.FEETYPE].ToString() != string.Empty)
                            {
                                Guid feeRunID = (Guid)fee[ConsolidationFeeDS.FEERUNID];
                                Guid feeTypeID = new Guid(fee[ConsolidationFeeDS.FEETYPE].ToString());
                                decimal feeManAdjust = (decimal)fee[ConsolidationFeeDS.ADJUST];

                                var selectedTransactions = this.feeTransactions.Where(t => t.FeeRunID == consolidationFeeDS.FeeRunID).Where(t => t.FeeRunID == feeRunID).FirstOrDefault();
                                if (selectedTransactions.ConfiguredFeesListWithManualAdjustment != null && selectedTransactions.ConfiguredFeesListWithManualAdjustment.ContainsKey(feeTypeID))
                                    selectedTransactions.ConfiguredFeesListWithManualAdjustment[feeTypeID] = feeManAdjust;
                                else
                                {
                                    selectedTransactions.ConfiguredFeesListWithManualAdjustment = new Hashtable();
                                    selectedTransactions.ConfiguredFeesListWithManualAdjustment.Add(feeTypeID, feeManAdjust);
                                }
                            }
                        }

                        var feeRun = this.Broker.GetBMCInstance(consolidationFeeDS.FeeRunID);
                        feeRun.SetData(consolidationFeeDS);
                    }
                }
                else if (data is IndividualClientsMappingDS)// setting Client Individuals
                {

                    var ds = data as IndividualClientsMappingDS;
                    if (this.ClientEntity is CorporateEntity)
                    {
                        var entity = this.ClientEntity as CorporateEntity;
                        if (entity.Signatories == null)
                        {
                            entity.Signatories = new List<Common.IdentityCMDetail>();
                        }

                        SetClientIndividuals(ds, entity.Signatories);
                    }
                    else if (this.ClientEntity is ClientIndividualEntity)
                    {
                        var entity = this.ClientEntity as ClientIndividualEntity;
                        if (entity.Applicants == null)
                        {
                            entity.Applicants = new List<Common.IdentityCMDetail>();
                        }
                        SetClientIndividuals(ds, entity.Applicants);
                    }


                }
            return ModifiedState.UNCHANGED;
        }
        private void SetClientIndividuals(IndividualClientsMappingDS ds, List<IdentityCMDetail> individuals)
        {
            var rows = ds.ClientMappingTable.Select(ds.ClientMappingTable.CID + "='" + CID + "'");

            foreach (DataRow dr in rows)
            {
                dr[ds.ClientMappingTable.CLIENTID] = EclipseClientID;
                dr[ds.ClientMappingTable.CLID] = Clid;
                dr[ds.ClientMappingTable.CSID] = CSID;
                dr[ds.ClientMappingTable.TYPE] = TypeName;
                var individual = individuals.FirstOrDefault(ss => ss.Clid == (Guid)dr[ds.ClientMappingTable.INDICLID] && ss.Csid == (Guid)

   dr[ds.ClientMappingTable.INDICSID]);
                switch (ds.CommandType)
                {
                    case DatasetCommandTypes.Update:
                        // do nothing because shares are maintained at individualCM 
                        break;
                    case DatasetCommandTypes.Add:
                        // New Individual to client signatory 
                        if (individual == null)
                        {
                            individuals.Add(new IdentityCMDetail
                            {
                                Cid = (Guid)dr[ds.ClientMappingTable.INDICID],
                                Clid = (Guid)dr

                                    [ds.ClientMappingTable.INDICLID],
                                Csid = (Guid)dr[ds.ClientMappingTable.INDICSID]
                            });
                        }
                        break;
                    case DatasetCommandTypes.Delete:
                        if (individual != null)
                        {
                            individuals.Remove(individual);
                        }

                        break;

                }
            }

        }
        private void OnSetData(ClientIndividualShareDS ds)
        {
            FixClientRefeneces(this.SignatoriesApplicants, ds);
        }

        private void FixClientRefeneces(List<Common.IdentityCMDetail> list, ClientIndividualShareDS ds)
        {
            if (list != null && list.Count > 0)
            {

                foreach (var identityCmDetail in list)
                {
                    var comp = Broker.GetCMImplementation(identityCmDetail.Clid, identityCmDetail.Csid) as IOrganizationUnit;

                    if (comp == null && identityCmDetail.Cid != Guid.Empty)
                    {
                        comp = Broker.GetBMCInstance(identityCmDetail.Cid) as IOrganizationUnit;
                    }

                    if (comp != null)
                    {
                        comp.Broker.SetStart();
                        var ind = comp.ClientEntity as Common.IndividualEntity;
                        if (ind != null)
                        {
                            if (ind.ClientShares == null)
                                ind.ClientShares = new List<ClientShare>();

                            var indsclients = ind.ClientShares.FirstOrDefault(ss => ss.ClientCid == CID);

                            if (indsclients == null)
                            {
                                ind.ClientShares.Add(new ClientShare { ClientID = this.clientid, ClientCid = cID, ClientClid = Clid, ClientCsid = Csid });
                            }

                        }
                        comp.CalculateToken(true);

                        Broker.ReleaseBrokerManagedComponent(comp);

                    }
                }

            }
        }

        private void OnSetData(OrgUnitConfiguredFeesDS orgUnitConfiguredFeesDS)
        {
            if (orgUnitConfiguredFeesDS.DataSetOperationType == DataSetOperationType.UpdateSingle)
            {
                var feeEntity = this.ConfiguredFeesList.Where(fee => fee == orgUnitConfiguredFeesDS.FeeEntityToConfigure).FirstOrDefault();
                if (feeEntity == null || feeEntity == Guid.Empty)
                    configuredFeesList.Add(orgUnitConfiguredFeesDS.FeeEntityToConfigure);
            }
            else if (orgUnitConfiguredFeesDS.DataSetOperationType == DataSetOperationType.DeletSingle)
            {
                IOrganization org = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                var feeEntity = this.ConfiguredFeesList.Where(fee => fee == orgUnitConfiguredFeesDS.FeeEntityToConfigure).FirstOrDefault();
                if (feeEntity != null && feeEntity != Guid.Empty)
                    configuredFeesList.Remove(orgUnitConfiguredFeesDS.FeeEntityToConfigure);
            }
        }

        protected ModifiedState OnSetData(ScenarioDS data)
        {
            Guid cLID = new Guid(data.CLID);
            Guid cSID = new Guid(data.CSID);
            Guid cIID = new Guid(data.CIID);

            ILogicalModule logicalEntity = this.Broker.GetLogicalCM(cLID);
            CMScenario cMScenario = logicalEntity.GetScenario(cSID);

            bool lockedScenarioModified = false;
            bool scenarioModifiedToUnlocked = false;
            bool scenarioExists = (cMScenario != null);
            bool lockScenarioStatusChanged = false;

            if (null == cMScenario)
            {
                LicenceSM licence = (LicenceSM)this.Broker.GetWellKnownBMC(WellKnownCM.Licencing);
                licence.IsLicenced(this.CLID, LicenceValidationType.Scenarios);

                this.Broker.ReleaseBrokerManagedComponent(licence);

                ArrayList conflictingOUNames = new ArrayList();
                if (this.HasScenario(data.Name, true, conflictingOUNames))
                {
                    string strRUNames = String.Empty;
                    foreach (string strName in conflictingOUNames)
                        strRUNames += "<br>" + strName + ", ";

                    strRUNames = strRUNames.Trim();
                    strRUNames = strRUNames.TrimEnd(',');
                    throw new DisplayUserMessageException(new MessageBoxDefinition(MessageBoxDefinition.MSG_SCENARIO_EXISTS_GROUP_MEMBER, new string[] { strRUNames }), false, MessageDisplayMethod.MainWindowError);
                }

                // The case where a new scenario is being created
                Guid copiedCSID = new Guid(data.CopiedCSID);
                cMScenario = logicalEntity.CreateCopyScenario(
                    data.Name,
                    cSID,
                    data.Type,
                    copiedCSID,
                    data.Status,
                    data.CreationDateTime,
                    data.ModificationDateTime
                    );
                logicalEntity.CurrentScenario = cMScenario.CSID; //Set the newly created scenario to be the current
            }
            else
            {
                if (logicalEntity.IsScenarioLocked(cSID))
                    lockedScenarioModified = true;

                lockScenarioStatusChanged = (data.Locked != lockedScenarioModified);

                // we only check if the task will be time intensive if
                // we are a group and the user has changed the locked
                // scenario status
                if (this is IGroup && lockScenarioStatusChanged)
                {
                    // set the dates to minvalue as we don't use these
                    // when navigating down the structure
                    if (!data.GetUserConfirmed(LockedStatusTimeIntensiveTaskKey))
                        this.CheckIfStructureWillBeTimeIntensive(DateTime.MinValue, DateTime.MinValue, LockedStatusTimeIntensiveTaskKey, BusinessStructureNavigationDirection.DownAndAcross);
                }

                cMScenario.Name = data.Name;
                cMScenario.Type = data.Type;
                cMScenario.ModificationDateTime = data.ModificationDateTime;
                cMScenario.Status = data.Status;

                if (cMScenario.Locked && !data.Locked)
                    scenarioModifiedToUnlocked = true;

                cMScenario.Locked = data.Locked;

                // if we are locking the scenario we need to make the change
                // here before we lock our members
                if (data.Locked)
                    setConsolAdjustName(logicalEntity, cSID, data);

                //If it is a entity being unlocked then also unlock the corresponding scenario
                //in all children such that any actions performed in 'HandleScenarioUnlocked'
                //will not cause a scenario locking exception to occur.
                if (logicalEntity.CMCategory == "BusinessEntity")
                    setScenarioLockedOnMembers(logicalEntity, cSID, data.Locked, data.Name);
            }

            // need to recalculate the logical module token as we have
            // possibly changed the data in the logical module
            logicalEntity.CalculateToken(true);

            this.Broker.ReleaseBrokerManagedComponent(logicalEntity);

            if (scenarioModifiedToUnlocked || (data.Locked && !lockedScenarioModified))
                this.ScenarioLockOverridden = true;

            if (logicalEntity.Modified && lockedScenarioModified && !this.ScenarioLockOverridden)
                throw new ScenarioLockException();

            if (scenarioModifiedToUnlocked)
                HandleScenarioUnlocked();

            // if we are unlocking then we have to set the name
            // after we have unlocked our members if there is an existing scenario
            if (scenarioExists && !data.Locked)
                setConsolAdjustName(logicalEntity, cSID, data);

            this.status = data.Status;

            return ModifiedState.UNKNOWN;
        }

        protected virtual bool HasScenario(string scenarioName, bool searchSubGroups, ArrayList conflictingOUNames)
        {
            return true;
        }

        public void CreateUniqueID()
        {
            if (string.IsNullOrEmpty(this.EclipseClientID))
            {
                IBrokerManagedComponent user = this.Broker.GetBMCInstance(Broker.UserContext != null ? Broker.UserContext.Identity.Name : "Administrator", "DBUser_1_1");
                IOrganization orgCM = Broker.GetWellKnownBMC(WellKnownCM.Organization) as IOrganization;
                object[] args = new Object[3] { orgCM.CLID, user.CID, 1 };
                DataView view = new DataView(this.Broker.GetBMCDetailList("Organization_1_1", args).Tables["Entities_Table"]);
                view.Sort = "ENTITYNAME_FIELD ASC";
                DataTable orgFiteredTable = view.ToTable();

                bool uniqueGenerated = false;

                while (!uniqueGenerated)
                {
                    string id = "IV" + UniqueID.ReturnUniqueID(EUniqueIDFormat.Combined);
                    DataRow[] idRows = orgFiteredTable.Select("ENTITYECLIPSEID_FIELD ='" + id.ToUpper() + "'");
                    if (idRows.Count() == 0)
                    {
                        this.EclipseClientID = id;
                        uniqueGenerated = true;
                    }
                }
            }
        }

        private void setConsolAdjustName(ILogicalModule logicalEntity, Guid cSID, ScenarioDS data)
        {
            ICalculationModule calcModule = logicalEntity[cSID];
            if (calcModule is IGroup)
            {
                IGroup group = calcModule as IGroup;
                //If a scenario is not being copied then also make sure that the 
                //Consolidation Adjustment entity has its Scenario name updated.
                IBrokerManagedComponent consolAdjBMC = (IBrokerManagedComponent)this.Broker.GetCMImplementation(group.ConsolidationAdjustmentEntityCLID, group.ConsolidationAdjustmentEntityCSID);

                data.CSID = group.ConsolidationAdjustmentEntityCSID.ToString();
                data.CIID = consolAdjBMC.CID.ToString();
                data.CLID = group.ConsolidationAdjustmentEntityCLID.ToString();
                data.CopiedCSID = group.ConsolidationAdjustmentEntityCSID.ToString();

                consolAdjBMC.SetData(data);

                this.Broker.ReleaseBrokerManagedComponent(consolAdjBMC);
            }
        }

        private void setScenarioLockedOnMembers(ILogicalModule logicalEntity, Guid cSID, bool locked, string name)
        {
            ICalculationModule calcModule = logicalEntity[cSID];

            if (locked)
                logicalEntity.LockScenario(cSID);
            else
                logicalEntity.UnLockScenario(cSID);

            if (calcModule is IGroup)
            {
                IGroup group = calcModule as IGroup;
                foreach (DictionaryEntry dictionaryEntry in group.IncludedGroupMembers)
                {
                    GroupMember groupMember = (GroupMember)dictionaryEntry.Value;
                    ILogicalModule entityLM = Broker.GetLogicalCM(groupMember.CLID);
                    CMScenario cMScenario = entityLM.GetScenario(groupMember.CSID);
                    setScenarioLockedOnMembers(entityLM, groupMember.CSID, locked, cMScenario.Name);

                    this.Broker.ReleaseBrokerManagedComponent(entityLM);
                }
            }

            foreach (Guid periodCLID in logicalEntity.ChildCLIDs)
            {
                ILogicalModule periodLM = this.Broker.GetLogicalCM(periodCLID);
                CMScenario cMScenario = logicalEntity.GetScenario(cSID);

                if (cMScenario != null)
                {
                    cMScenario.Name = name;
                    cMScenario.Locked = locked;
                    periodLM.CalculateToken(true);
                }

                foreach (Guid taxTopicCLID in periodLM.ChildCLIDs)
                {
                    ILogicalModule ttLM = this.Broker.GetLogicalCM(taxTopicCLID);
                    cMScenario = ttLM.GetScenario(cSID);

                    if (cMScenario != null)
                    {
                        cMScenario.Name = name;
                        cMScenario.Locked = locked;
                        ttLM.CalculateToken(true);
                    }

                    this.Broker.ReleaseBrokerManagedComponent(ttLM);
                }

                this.Broker.ReleaseBrokerManagedComponent(periodLM);
            }
        }

        /// <summary>
        /// Perform any actions which need to occur when a scenario is unlocked
        /// </summary>
        private void HandleScenarioUnlocked()
        {
            ILogicalModule orgUnitLM = this.Broker.GetLogicalCM(this.CLID);

            foreach (Guid periodCLID in orgUnitLM.ChildCLIDs)
            {
                ILogicalModule periodLM = this.Broker.GetLogicalCM(periodCLID);

                foreach (Guid taxTopicCLID in periodLM.ChildCLIDs)
                {
                    ILogicalModule taxTopicLM = this.Broker.GetLogicalCM(taxTopicCLID);
                    ICalculationModule taxTopicBMC = taxTopicLM[this.CSID];

                    this.Broker.ReleaseBrokerManagedComponent(taxTopicLM);

                    this.Broker.ReleaseBrokerManagedComponent(taxTopicBMC);
                }

                this.Broker.ReleaseBrokerManagedComponent(periodLM);
            }

            this.Broker.ReleaseBrokerManagedComponent(orgUnitLM);

            orgUnitLM = this.Broker.GetLogicalCM(this.CLID);

            foreach (Guid periodCLID in orgUnitLM.ChildCLIDs)
            {
                ILogicalModule periodLM = this.Broker.GetLogicalCM(periodCLID);

                foreach (Guid taxTopicCLID in periodLM.ChildCLIDs)
                {
                    ILogicalModule taxTopicLM = this.Broker.GetLogicalCM(taxTopicCLID);
                    ICalculationModule taxTopicBMC = taxTopicLM[this.CSID];

                    this.Broker.ReleaseBrokerManagedComponent(taxTopicLM);

                    this.Broker.ReleaseBrokerManagedComponent(taxTopicBMC);
                }

                this.Broker.ReleaseBrokerManagedComponent(periodLM);
            }

            this.Broker.ReleaseBrokerManagedComponent(orgUnitLM);
        }

        public override void DeliverData(DataSet data)
        {
            // Before calling base.DeliverData remove recurring spaces  for entity/group/scenarios

            if (data is OrganisationUnitDetailsDS)
            {
                ((OrganisationUnitDetailsDS)data).Name = StringUtilities.RemoveRecurringSpaces(((OrganisationUnitDetailsDS)data).Name);
            }

            if (data is ScenarioDS)
            {
                ((ScenarioDS)data).Name = StringUtilities.RemoveRecurringSpaces(((ScenarioDS)data).Name);
            }

            base.DeliverData(data);

            if (data is OrganisationUnitDetailsDS)
            {
                OrganisationUnitDetailsDS objOUDetails = (OrganisationUnitDetailsDS)data;

                // check if the name has changed, that the name is unique, if not then throw an exception to indicate that
                // it is not unique - in which case the UI catches the exception and displays a message to the user.
                if ((this.Name != objOUDetails.Name || objOUDetails.IsNewOrganisation) &&
                     !this.IsUniqueOrganisationName(objOUDetails.Name))
                    throw new BusinessRuleViolation("Organisation name is the same as another legal organisation", BusinessRuleViolationType.OrganisationUnitNameConflict);
                this.Name = objOUDetails.Name;

                //Always set the logical module name for the org unit to be the short entity name.
                ILogicalModule iLM = this.Broker.GetLogicalCM(this.CLID);
                iLM.Name = objOUDetails.Name;
                iLM.CalculateToken(true);

                //When setting the short entity name in the logical module
                //also change the name in any other scenarios for that BMC
                //so that these are always in sync, even though it should not be getting
                //used anymore.
                foreach (DictionaryEntry dictEn in iLM.Scenarios)
                {
                    CMScenario cMScenario = (CMScenario)dictEn.Value;
                    IBrokerManagedComponent orgUnit = (IBrokerManagedComponent)this.Broker.GetCMImplementation(cMScenario.CLID, cMScenario.CSID);
                    orgUnit.Name = objOUDetails.Name;
                    orgUnit.CalculateToken(true);
                    this.Broker.ReleaseBrokerManagedComponent(orgUnit);
                }

                this.Broker.ReleaseBrokerManagedComponent(iLM);
                this.LegalName = objOUDetails.LegalName;

                this.Identifier = objOUDetails.Identifier;
            }

            //When a new scenario is being created from another scenario, then we need to get any children
            //that are multiperiodCMs and cause these to diverge, and then subscribe to the new scenario members.
            if (data is ScenarioDS)
            {
                if (((ScenarioDS)data).CopyScenario)
                {
                    Guid gdNewCSID = new Guid(((ScenarioDS)data).CSID);

                    ILogicalModule thisLM = this.Broker.GetLogicalCM(this.CLID);

                    foreach (Guid childCLID in thisLM.ChildCLIDs)
                    {
                        ILogicalModule childLCM = this.Broker.GetLogicalCM(childCLID);

                        if (childLCM.CMType.ToString() == "050fb7a0-24f8-42ff-85ad-e93445b7c895")//MultiPeriod TypeID
                        {
                            IMultiPeriod mpCM = (IMultiPeriod)childLCM[gdNewCSID];

                            //If mpCM is null then there is no multi period cm for the scenario just created
                            if (mpCM != null)
                                mpCM.SubscribeNewScenario();
                        }

                        this.Broker.ReleaseBrokerManagedComponent(childLCM);
                    }

                    this.Broker.ReleaseBrokerManagedComponent(thisLM);
                }
            }

            if (data is OrganizationUnitDS)
            {
                DeliverPrimaryData(data);
            }
        }
        #endregion

        #region Migration

        public override DataSet MigrateDataSet(DataSet data)
        {
            DataSet baseDataset = base.MigrateDataSet(data);
            return baseDataset;
        }

        /// <summary>
        /// notifies to the cm that the migration is completed
        /// </summary>
        public override void MigrationCompleted()
        {
            base.MigrationCompleted();
            versionHistory.Remove(OrganizationUnitInstall.COMPONENTVERSION_IMPLEMENTATIONCLASS);
        }
        #endregion

        #region OTHER METHODS

        public virtual List<ImportMessages> ImportPaidOrders(System.Xml.Linq.XElement xml)
        {
            return new List<ImportMessages>();

        }

        public virtual bool IsMISFundCodeAttached(Guid FundCodeId)
        {
            return false;
        }

        public virtual bool IsMISAttachedWithFund(string FundCode, string MisName)
        {
            return false;
        }

        public virtual void IncomeTransactionProcess(DividendEntity dv, List<ProductEntity> ProductCollection, SecuritiesEntity security)
        {


        }

        public virtual void ProcessDividend(DividendEntity dv, List<ProductEntity> ProductCollection, SecuritiesEntity security)
        {
        }

        public virtual void ProcessDistribution(DistributionEntity dv, List<ProductEntity> ProductCollection, SecuritiesEntity security)
        {
        }

        public virtual void IncomeTransactionProcessTD(Guid dv)
        {
        }

        public virtual void DeletAllTransaction()
        {
        }

        public virtual void FillEntityDetailsInObjects(object target, int type)
        {
        }

        public virtual void ImportDistributions(DataRow[] rows)
        {
        }

        public static void BMCUpdateComponentParent(IdentityCM child, IdentityCM parent, CmCommand cmd, ICMBroker broker)
        {
            IOrganizationUnit childComponent = broker.GetCMImplementation(child.Clid, child.Csid) as IOrganizationUnit;
            IOrganizationUnit parentComponent = broker.GetCMImplementation(parent.Clid, parent.Csid) as IOrganizationUnit;

            if (childComponent == null && child.Cid != null)
                childComponent = broker.GetBMCInstance(child.Cid) as OrganizationUnitCM;

            if (parentComponent == null && parent.Cid != null)
                parentComponent = broker.GetBMCInstance(parent.Cid) as OrganizationUnitCM;

            if (childComponent != null && parentComponent != null)
            {
                switch (cmd)
                {
                    case CmCommand.Add_Single:
                        {

                            if (((ICmHasParent)childComponent).Parent != null)// remove from previous Advisor
                            {
                                var advisor = ((ICmHasParent)childComponent).Parent;
                                var prevadvisercm = (broker.GetBMCInstance(advisor.Cid) as ICmHasChildren) ?? (broker.GetCMImplementation(advisor.Clid, advisor.Csid) as ICmHasChildren);

                                if (prevadvisercm != null && prevadvisercm.Children != null)
                                {
                                    var temchild = prevadvisercm.Children.Where(icm => icm.Clid == child.Clid && icm.Csid == child.Csid).FirstOrDefault();
                                    if (temchild != null) prevadvisercm.Children.Remove(temchild);
                                }
                            }


                            ((ICmHasParent)childComponent).Parent = new IdentityCM(parentComponent.CID);
                            ((ICmHasParent)childComponent).Parent.Csid = parentComponent.CSID;
                            ((ICmHasParent)childComponent).Parent.Clid = parentComponent.CLID;

                            ICmHasChildren cmWithChildren = parentComponent as ICmHasChildren;

                            if (cmWithChildren == null)
                                cmWithChildren.Children = new List<IdentityCM>();

                            var currentChild = cmWithChildren.Children.Where(icm => icm.Cid == childComponent.CID).FirstOrDefault();
                            if (currentChild == null)
                            {
                                currentChild = cmWithChildren.Children.Where(icm => icm.Clid == childComponent.CLID && icm.Csid == childComponent.CSID).FirstOrDefault();

                                if (currentChild == null)
                                {
                                    IdentityCM childIdentityCM = new IdentityCM(childComponent.CID);
                                    childIdentityCM.Csid = childComponent.CSID;
                                    childIdentityCM.Clid = childComponent.CLID;
                                    cmWithChildren.Children.Add(childIdentityCM);
                                }
                                else
                                {
                                    currentChild.Cid = childComponent.CID;
                                    currentChild.Csid = childComponent.CSID;
                                    currentChild.Clid = childComponent.CLID;
                                }
                            }
                            else
                            {
                                currentChild.Cid = childComponent.CID;
                                currentChild.Csid = childComponent.CSID;
                                currentChild.Clid = childComponent.CLID;
                            }
                            break;
                        }

                    case CmCommand.Remove_Single:
                        {
                            ((ICmHasParent)childComponent).Parent = new IdentityCM();
                            ICmHasChildren cmWithChildren = parentComponent as ICmHasChildren;
                            if (cmWithChildren != null)
                            {
                                var currentChild = cmWithChildren.Children.Where(icm => icm.Cid == childComponent.CID).FirstOrDefault();
                                if (currentChild != null)
                                    cmWithChildren.Children.Remove(currentChild);
                                else
                                {
                                    currentChild = cmWithChildren.Children.Where(icm => icm.Clid == childComponent.CLID && icm.Csid == childComponent.CSID).FirstOrDefault();
                                    if (currentChild != null)
                                        cmWithChildren.Children.Remove(currentChild);
                                }
                            }
                            break;
                        }
                }
                childComponent.CalculateToken(true);
            }
        }

        public static string GetParentName(IdentityCM Parent, ICMBroker Broker, ref string parentType)
        {
            string name = string.Empty;
            var component = Broker.GetCMImplementation(Parent.Clid, Parent.Csid) as OrganizationUnitCM;
            if (component == null)
                component = Broker.GetBMCInstance(Parent.Cid) as OrganizationUnitCM;
            if (component != null)
            {
                name = component.GetDataStream((int)CmCommand.FullName, "");
                parentType = component.TypeName;
                Broker.ReleaseBrokerManagedComponent(component);

            }
            return name;
        }

        public static void GetChildren(List<IdentityCM> Childrens, MembershipDS ds, ICMBroker broker)
        {
            foreach (var identityCm in Childrens)
            {
                IOrganizationUnit component = broker.GetCMImplementation(identityCm.Clid, identityCm.Csid) as IOrganizationUnit;

                if (component == null && identityCm.Cid != Guid.Empty)
                    component = broker.GetBMCInstance(identityCm.Cid) as IOrganizationUnit;
                if (component != null && (component.IsInvestableClient || component.ComponentInformation.DisplayName == "Adviser" || component.ComponentInformation.DisplayName == "IFA" || component.ComponentInformation.DisplayName == "Dealer Group" || component.ComponentInformation.DisplayName == "Accountant"))
                {
                    ds.CommandType = (int)DatasetCommandTypes.Get;
                    component.GetData(ds);
                    broker.ReleaseBrokerManagedComponent(component);
                }
            }

        }

        public static void GetParentTable(IdentityCM Parent, MembershipDS ds, ICMBroker Broker)
        {
            if(Parent!=null)
            {
                var component = Broker.GetCMImplementation(Parent.Clid, Parent.Csid) as OrganizationUnitCM;

                if (component == null)
                    component = Broker.GetBMCInstance(Parent.Cid) as OrganizationUnitCM;
                if (component != null)
                {
                    string name = component.GetDataStream((int)CmCommand.FullName, "");
                    string parentType = component.TypeName;
                    
                    DataRow row = ds.parentTable.NewRow();
                    row[ds.parentTable.CLIENTID] = component.ClientId;
                    row[ds.parentTable.NAME] = component.Name;
                    row[ds.parentTable.TYPE] = component.TypeName;

                    ds.parentTable.Rows.Add(row);
                    Broker.ReleaseBrokerManagedComponent(component);
                   
                }
            }
        }

        public static void AddAvilableMembershipRow(MembershipDS ds, string clientId, string name, string typename, string status, Guid clid, Guid csid, Guid cid)
        {

            DataRow r = ds.membershipTable.NewRow();
            r[ds.membershipTable.CLIENTID] = clientId;
            r[ds.membershipTable.NAME] = name;
            r[ds.membershipTable.TYPE] = typename;
            r[ds.membershipTable.STATUS] = status;
            r[ds.membershipTable.CLID] = clid;
            r[ds.membershipTable.CSID] = csid;
            r[ds.membershipTable.CID] = cid;
            ds.membershipTable.Rows.Add(r);
        }


        //Empty virtual method, to be overridden when actions need to be taken prior to 
        //and organization unit being deleted from the system
        //ie In GroupCM, before deleting the Group CM, any Consolidation adjustments that
        //belong to the group being deleted need to also be deleted.
        public virtual void PreDeleteActions() { }

        public virtual TaxYear GetTaxYear(DateTime objPeriodStartDate, DateTime objPeriodEndDate)
        {
            if (this.SAPs == null)
                return null;
            else
            {
                return this.SAPs.GetTaxYear(objPeriodStartDate, objPeriodEndDate);
            }
        }

        public virtual FinancialYear GetFinancialYear(DateTime objPeriodStartDate, DateTime objPeriodEndDate)
        {
            if (this.SAPs == null)
                return null;
            else
            {
                return this.SAPs.GetFinancialYear(objPeriodStartDate, objPeriodEndDate);
            }
        }

        public IOrganizationUnit GetOrganisation()
        {
            return (IOrganizationUnit)this.Broker.GetWellKnownCM(WellKnownCM.Organization);
        }

        public ReadOnlyCollection<IGroup> GetParentGroups()
        {
            ReadOnlyCollection<ICalculationModule> parentGroupsAsCMs = this.Broker.GetGroupsThatHaveOrganisationUnitAsMember((ICalculationModule)this);

            List<IGroup> parentGroups = new List<IGroup>(parentGroupsAsCMs.Count);

            foreach (ICalculationModule calculationModule in parentGroupsAsCMs)
                parentGroups.Add((IGroup)calculationModule);

            return new ReadOnlyCollection<IGroup>(parentGroups);
        }

        public virtual string FullName()
        { return name; }

        public bool AssociateModel(List<string> programCodes)
        {
            bool value = true;
            if (programCodes == null) return false;

            ICalculationModule objOrganisation = Broker.GetWellKnownCM(WellKnownCM.Organization);
            IModelStructure organization = (objOrganisation as IModelStructure);

            foreach (var item in programCodes)
            {
                if (string.IsNullOrWhiteSpace(item)) continue;

                var existingModel = organization.Model.Find(e => e.ProgramCode.ToLower().Equals(item.ToLower()));

                if (existingModel == null) continue;

                if (existingModel.LinkedClients == null)
                    existingModel.LinkedClients = new List<IdentityCM>();

                //var tempClient = existingModel.LinkedClients.FirstOrDefault(e => e.Clid == this.Clid && e.Csid == this.Csid);
                //if (tempClient != null) continue;
                existingModel.LinkedClients.Add(new IdentityCM { Clid = this.Clid, Csid = this.Csid });

                var models = new List<ModelEntity> { new ModelEntity(), existingModel };

                //organization.UpdateDataStream( (int)OrganizationCommandType.UpdateModelList ,  models.ToXmlString() );
                var result = organization.UpdateDataStream(0, models.ToXmlString());

                if (result.ToLower().Equals("false"))
                {
                    value = false;
                    break;
                }
            }

            Broker.ReleaseBrokerManagedComponent(organization);

            return value;
        }




        #endregion

        #region Validation Methods

        public bool IsMemberOfOtherGroupScenario(Guid objAllowedGroupScenarioCSID)
        {
            DataRowCollection objConsolidationParents = (DataRowCollection)this.Broker.GetConsolidationParentsList(this.CLID, this.CSID);

            foreach (DataRow objMembership in objConsolidationParents)
            {
                Guid objMembershipCSID = (Guid)objMembership["PARENTCSID"];

                // if we are a member of another scenario other than the permitted scenario, return true
                if (objMembershipCSID != objAllowedGroupScenarioCSID)
                    return true;
            }

            // if we got to the end and couldn't find a scenario which was different from the allowed, then we are not a part of another group scenario
            return false;
        }
        #endregion

        #region Structure as XML

        public void SetAdviserIDToXMlDATA(XmlDocument doc, Guid parentCLID, Guid parentCSID, bool isClient)
        {
            //Setting Adviser ID for investible CM 
            if (this.IsInvestableClient || isClient)
            {
                OrganizationUnitCM adviser = Broker.GetCMImplementation(parentCLID, parentCSID) as OrganizationUnitCM;
                if (adviser != null)
                    Oritax.TaxSimp.Common.XMLExtensions.AddNode("AdviserID", adviser.ClientId, doc);
                else
                    Oritax.TaxSimp.Common.XMLExtensions.AddNode("AdviserID", "", doc);

                Broker.ReleaseBrokerManagedComponent(adviser);
            }
        }

        public string GetAdviserName()
        {
            string adviserFullName = string.Empty;

            if (this.IsInvestableClient)
            {
                OrganizationUnitCM adviser = Broker.GetBMCInstance(this.AdviserID) as OrganizationUnitCM;
                if (adviser != null)
                {
                    adviserFullName = adviser.FullName();
                    Broker.ReleaseBrokerManagedComponent(adviser);
                }
            }

            return adviserFullName;
        }

        public string GetAdviserID()
        {
            string adviserID = string.Empty;

            if (this.IsInvestableClient)
            {
                OrganizationUnitCM adviser = Broker.GetBMCInstance(this.AdviserID) as OrganizationUnitCM;
                if (adviser != null)
                {
                    adviserID = adviser.EclipseClientID;
                    Broker.ReleaseBrokerManagedComponent(adviser);
                }
            }

            return adviserID;
        }


        public string GetAdviserEmail()
        {
            string adviserID = string.Empty;

            if (this.IsInvestableClient)
            {
                OrganizationUnitCM adviser = Broker.GetBMCInstance(this.AdviserID) as OrganizationUnitCM;
                if (adviser != null)
                {
                    adviserID = adviser.GetDataStream((int)CmCommand.Email, string.Empty);
                    Broker.ReleaseBrokerManagedComponent(adviser);
                }
            }

            return adviserID;
        }






        public void AddGroupStructureToXmlElement(XmlElement parentEl)
        {
            XmlElement el = AddChildElement(parentEl, CLID, CSID, this.Name);

            if (this is IGroup)
            {
                GroupMembers groupMembers = ((IGroup)this).IncludedGroupMembers;

                foreach (DictionaryEntry dictionarEntry in groupMembers)
                {
                    GroupMember gMember = (GroupMember)dictionarEntry.Value;
                    if (gMember.Adjustment.Equals(false))
                    {
                        ILogicalModule lCM = this.Broker.GetLogicalCM(gMember.CLID);
                        OrganizationUnitCM orgUnitCM = (OrganizationUnitCM)lCM[gMember.CSID];
                        this.Broker.ReleaseBrokerManagedComponent(lCM);

                        orgUnitCM.AddGroupStructureToXmlElement(el);
                    }
                }
            }
        }

        protected XmlElement AddChildElement(XmlElement parentEl, Guid clid, Guid csid, string name)
        {
            XmlElement el = parentEl.OwnerDocument.CreateElement("Row");
            AddAttribute(el, "Text", name);
            AddAttribute(el, "CSID", csid);
            AddAttribute(el, "CLID", clid);
            parentEl.AppendChild(el);
            return el;
        }

        protected void AddAttribute(XmlElement node, string name, Guid value)
        {
            AddAttribute(node, name, value.ToString());
        }
        protected static void AddAttribute(XmlElement node, string name, string value)
        {
            XmlAttribute attr = node.OwnerDocument.CreateAttribute(name);
            attr.Value = value;
            node.Attributes.Append(attr);
        }

        #endregion Structure as XML

        #region importBankWest Accounts
        public virtual void ImportBankwestData(List<IIdentityCM> accounts)
        {


        }

        #endregion

        #region importBankWest Accounts Transactions
        public virtual List<ImportMessages> ImportTransactions(System.Xml.Linq.XElement xml)
        {
            return new List<ImportMessages>();

        }




        public virtual List<ImportMessages> ImportTransactionsUnitHolderBalance(System.Xml.Linq.XElement xml)
        {
            return new List<ImportMessages>();
        }


        public virtual void SetUnuploadedUnitHoldTransactions(List<string> fundCodes, string misName)
        {

        }

        public virtual void ImportTransactionsDividendEntity(DividendEntity dv)
        {

        }

        public virtual void AddClientDividend(XElement root, OrganizationUnitCM unit)
        {

        }

        public virtual void DeleteAllCashTransaction_Bank()
        {

        }

        public virtual void DeleteAllHoldingBalances_DesktopBroker()
        {

        }

        public virtual void DeleteAllHoldingTransaction_DesktopBroker()
        {

        }

        public virtual void DeleteAllFundTransaction_ManagedInvestmentSchemes()
        {

        }

        public virtual void DeleteAllSecurityHolding_ManagedInvestmentSchemes()
        {

        }




        #endregion

        #region Check Mis Is Attached
        public virtual bool IsMISAttached(Guid clid, Guid csid)
        {
            return false;
        }
        #endregion

        #region BGL Export

        private static string FormatXML(string unformattedXml)
        {
            // first read the xml ignoring whitespace
            XmlReaderSettings readeroptions = new XmlReaderSettings { IgnoreWhitespace = true };
            XmlReader reader = XmlReader.Create(new StringReader(unformattedXml), readeroptions);

            // then write it out with indentation
            StringBuilder sb = new StringBuilder();
            XmlWriterSettings xmlSettingsWithIndentation = new XmlWriterSettings { Indent = true };
            using (XmlWriter writer = XmlWriter.Create(sb, xmlSettingsWithIndentation))
            {
                writer.WriteNode(reader, true);
            }

            return sb.ToString();
        }

        public void ExportBGL(BGLExportDS bglExportDS, string accountCode, string accountName, string clientID, IEnumerable<Oritax.TaxSimp.Common.CashManagementEntity> CashManagementTransactions, ObservableCollection<DividendEntity> DividendCollection, ObservableCollection<DistributionIncomeEntity> DistributionIncomes, List<AccountProcessTaskEntity> ListAccountProcess, IOrganization orgCM, BGLAccountMap bglAccountMap)
        {
            // Use StringWriter as backing for XmlTextWriter.
            using (StringWriter str = new StringWriter())
            using (XmlTextWriter xml = new XmlTextWriter(str))
            {
                xml.Formatting = Formatting.Indented;
                xml.Indentation = 3;
                xml.IndentChar = '\t';
                // Root.
                xml.WriteStartDocument();
                xml.WriteStartElement("BGL_Import_Export");
                xml.WriteElementString("Supplier", "e-Clipse Online");
                xml.WriteElementString("Product", accountName);
                xml.WriteElementString("Import_Export_Version", "4.0");
                xml.WriteStartElement("Entity_Details");
                xml.WriteWhitespace("\n");
                xml.WriteElementString("Entity_Code", accountCode);

                BGLExportHoldings(bglExportDS, CashManagementTransactions, xml);
                xml.WriteEndElement();
                BGLExportTransactions(CashManagementTransactions, DividendCollection, DistributionIncomes, ListAccountProcess, orgCM, bglAccountMap, xml);
                xml.WriteEndElement();
                xml.WriteEndDocument();
                // Result is a string.
                bglExportDS.BGLExportXMLString = FormatXML(str.ToString());
            }
        }

        private void BGLExportHoldings(BGLExportDS bglExportDS, IEnumerable<Oritax.TaxSimp.Common.CashManagementEntity> CashManagementTransactions, XmlTextWriter xml)
        {
            xml.WriteStartElement("investment_holdings");
            xml.WriteWhitespace("\n");

            SecuritySummaryReportDS secDS = new SecuritySummaryReportDS();
            secDS.ValuationDate = bglExportDS.EndDate.Date;
            this.OnGetData(secDS);

            foreach (DataRow row in secDS.Tables[SecuritySummaryReportDS.SECURITYSUMMARYTABLE].Rows)
            {
                if (row[SecuritySummaryReportDS.SECTYPE].ToString().ToUpper() == "SEC")
                    AddInvestmentHoldingSeFunds(xml, row, bglExportDS.EndDate, "Shares in Listed Company (Australian)");
                else if (row[SecuritySummaryReportDS.SECTYPE].ToString().ToUpper() == "FUNDS")
                    AddInvestmentHoldingSeFunds(xml, row, bglExportDS.EndDate, "Managed Investment (Australian)");
                else if (row[SecuritySummaryReportDS.SECTYPE].ToString().ToUpper() == "CASH")
                    AddInvestmentHoldingCash(xml, row, bglExportDS.EndDate);
            }
        }

        private void BGLExportTransactions(IEnumerable<Oritax.TaxSimp.Common.CashManagementEntity> CashManagementTransactions, ObservableCollection<DividendEntity> DividendCollection, ObservableCollection<DistributionIncomeEntity> DistributionIncomes, List<AccountProcessTaskEntity> ListAccountProcess, IOrganization orgCM, BGLAccountMap bglAccountMap, XmlTextWriter xml)
        {
            xml.WriteStartElement("Transactons");
            xml.WriteWhitespace("\n");

            var filteredTransaction = CashManagementTransactions.Where(tran => tran.ImportTransactionType == "Deposit" && tran.Category == "Contibution" && tran.SystemTransactionType == "Employer Additional");
            var bglAccount = bglAccountMap.Where(tran => tran.ImportTransactionType == "Deposit" && tran.Category == "Contibution" && tran.SubCategory == "Employer Additional").FirstOrDefault();

            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in filteredTransaction)
                SetDepositIncomeOtherTransaction(xml, bglAccount, cashEntity);

            filteredTransaction = CashManagementTransactions.Where(tran => tran.ImportTransactionType == "Deposit" && tran.Category == "Contibution" && tran.SystemTransactionType == "Employer SG");
            bglAccount = bglAccountMap.Where(tran => tran.ImportTransactionType == "Deposit" && tran.Category == "Contibution" && tran.SubCategory == "Employer SG").FirstOrDefault();

            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in filteredTransaction)
                SetDepositIncomeOtherTransaction(xml, bglAccount, cashEntity);

            filteredTransaction = CashManagementTransactions.Where(tran => tran.ImportTransactionType == "Deposit" && tran.Category == "Contibution" && tran.SystemTransactionType == "Personal");
            bglAccount = bglAccountMap.Where(tran => tran.ImportTransactionType == "Deposit" && tran.Category == "Contibution" && tran.SubCategory == "Personal").FirstOrDefault();

            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in filteredTransaction)
                SetDepositIncomeOtherTransaction(xml, bglAccount, cashEntity);

            filteredTransaction = CashManagementTransactions.Where(tran => tran.ImportTransactionType == "Deposit" && tran.Category == "Contibution" && tran.SystemTransactionType == "Salary Sacrifice");
            bglAccount = bglAccountMap.Where(tran => tran.ImportTransactionType == "Deposit" && tran.Category == "Contibution" && tran.SubCategory == "Salary Sacrifice").FirstOrDefault();

            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in filteredTransaction)
                SetDepositIncomeOtherTransaction(xml, bglAccount, cashEntity);

            filteredTransaction = CashManagementTransactions.Where(tran => tran.ImportTransactionType == "Deposit" && tran.Category == "Contibution" && tran.SystemTransactionType == "Spouse");
            bglAccount = bglAccountMap.Where(tran => tran.ImportTransactionType == "Deposit" && tran.Category == "Contibution" && tran.SubCategory == "Spouse").FirstOrDefault();

            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in filteredTransaction)
                SetDepositIncomeOtherTransaction(xml, bglAccount, cashEntity);

            filteredTransaction = CashManagementTransactions.Where(tran => tran.ImportTransactionType == "Deposit" && tran.Category == "Income" && tran.SystemTransactionType == "Rental Income");
            bglAccount = bglAccountMap.Where(tran => tran.ImportTransactionType == "Deposit" && tran.Category == "Income" && tran.SubCategory == "Rental Income").FirstOrDefault();

            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in filteredTransaction)
                SetDepositIncomeOtherTransaction(xml, bglAccount, cashEntity);

            filteredTransaction = CashManagementTransactions.Where(tran => tran.ImportTransactionType == "Deposit" && tran.Category == "Income" && tran.SystemTransactionType == "Commission Rebate");
            bglAccount = bglAccountMap.Where(tran => tran.ImportTransactionType == "Deposit" && tran.Category == "Income" && tran.SubCategory == "Commission Rebate").FirstOrDefault();

            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in filteredTransaction)
                SetDepositIncomeOtherTransaction(xml, bglAccount, cashEntity);

            filteredTransaction = CashManagementTransactions.Where(tran => tran.ImportTransactionType == "Deposit" && tran.Category == "Income" && tran.SystemTransactionType == "Interest");
            bglAccount = bglAccountMap.Where(tran => tran.ImportTransactionType == "Deposit" && tran.Category == "Income" && tran.SubCategory == "Interest").FirstOrDefault();

            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in filteredTransaction)
                SetDepositIncomeOtherTransaction(xml, bglAccount, cashEntity);

            filteredTransaction = CashManagementTransactions.Where(tran => tran.ImportTransactionType == "Deposit" && tran.Category == "Income" && tran.SystemTransactionType == "Transfer In");
            bglAccount = bglAccountMap.Where(tran => tran.ImportTransactionType == "Deposit" && tran.Category == "Income" && tran.SubCategory == "Transfer In").FirstOrDefault();

            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in filteredTransaction)
                SetDepositTransferInTransaction(xml, bglAccount, cashEntity);

            filteredTransaction = CashManagementTransactions.Where(tran => tran.ImportTransactionType == "Deposit" && tran.Category == "Income" && tran.SystemTransactionType == "Internal Cash Movement");
            bglAccount = bglAccountMap.Where(tran => tran.ImportTransactionType == "Deposit" && tran.Category == "Income" && tran.SubCategory == "Internal Cash Movement").FirstOrDefault();

            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in filteredTransaction)
                SetDepositIncomeOtherTransaction(xml, bglAccount, cashEntity);

            filteredTransaction = CashManagementTransactions.Where(tran => tran.ImportTransactionType == "Deposit" && tran.Category == "Income" && tran.SystemTransactionType == "Distribution");
            bglAccount = bglAccountMap.Where(tran => tran.ImportTransactionType == "Deposit" && tran.Category == "Income" && tran.SubCategory == "Distribution").FirstOrDefault();

            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in filteredTransaction)
                SetDepositIncomeDistributionTransaction(xml, bglAccount, cashEntity, DistributionIncomes, orgCM);

            filteredTransaction = CashManagementTransactions.Where(tran => tran.ImportTransactionType == "Deposit" && tran.Category == "Income" && tran.SystemTransactionType == "Dividend");
            bglAccount = bglAccountMap.Where(tran => tran.ImportTransactionType == "Deposit" && tran.Category == "Income" && tran.SubCategory == "Dividend").FirstOrDefault();

            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in filteredTransaction)
                SetDepositIncomeDividendsTransaction(xml, bglAccount, cashEntity, DividendCollection, orgCM);

            filteredTransaction = CashManagementTransactions.Where(tran => tran.ImportTransactionType == "Deposit" && tran.Category == "Investment" && tran.SystemTransactionType == "Redemption");
            bglAccount = bglAccountMap.Where(tran => tran.ImportTransactionType == "Deposit" && tran.Category == "Investment" && tran.SubCategory == "Redemption").FirstOrDefault();

            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in filteredTransaction)
                SetInvestmentDisposalTransaction(ListAccountProcess, this.ClientId, xml, bglAccount, cashEntity, orgCM);

            filteredTransaction = CashManagementTransactions.Where(tran => tran.ImportTransactionType == "Deposit" && tran.Category == "Tax" && tran.SystemTransactionType == "Business Activity Statement");
            bglAccount = bglAccountMap.Where(tran => tran.ImportTransactionType == "Deposit" && tran.Category == "Tax" && tran.SubCategory == "Business Activity Statement").FirstOrDefault();

            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in filteredTransaction)
                SetDepositIncomeOtherTransaction(xml, bglAccount, cashEntity);

            filteredTransaction = CashManagementTransactions.Where(tran => tran.ImportTransactionType == "Deposit" && tran.Category == "Tax" && tran.SystemTransactionType == "Instalment Activity Statement");
            bglAccount = bglAccountMap.Where(tran => tran.ImportTransactionType == "Deposit" && tran.Category == "Tax" && tran.SubCategory == "Instalment Activity Statement").FirstOrDefault();

            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in filteredTransaction)
                SetDepositIncomeOtherTransaction(xml, bglAccount, cashEntity);
            filteredTransaction = CashManagementTransactions.Where(tran => tran.ImportTransactionType == "Deposit" && tran.Category == "Tax" && tran.SystemTransactionType == "Business Activity Statement");
            bglAccount = bglAccountMap.Where(tran => tran.ImportTransactionType == "Deposit" && tran.Category == "Tax" && tran.SubCategory == "Business Activity Statement").FirstOrDefault();

            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in filteredTransaction)
                SetDepositIncomeOtherTransaction(xml, bglAccount, cashEntity);
            filteredTransaction = CashManagementTransactions.Where(tran => tran.ImportTransactionType == "Deposit" && tran.Category == "Tax" && tran.SystemTransactionType == "Income Tax");
            bglAccount = bglAccountMap.Where(tran => tran.ImportTransactionType == "Deposit" && tran.Category == "Tax" && tran.SubCategory == "Income Tax").FirstOrDefault();

            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in filteredTransaction)
                SetDepositIncomeOtherTransaction(xml, bglAccount, cashEntity);
            filteredTransaction = CashManagementTransactions.Where(tran => tran.ImportTransactionType == "Deposit" && tran.Category == "Tax" && tran.SystemTransactionType == "PAYG");
            bglAccount = bglAccountMap.Where(tran => tran.ImportTransactionType == "Deposit" && tran.Category == "Tax" && tran.SubCategory == "PAYG").FirstOrDefault();

            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in filteredTransaction)
                SetDepositIncomeOtherTransaction(xml, bglAccount, cashEntity);

            filteredTransaction = CashManagementTransactions.Where(tran => tran.ImportTransactionType == "Deposit" && tran.Category == "Tax" && tran.SystemTransactionType == "Tax Refund");
            bglAccount = bglAccountMap.Where(tran => tran.ImportTransactionType == "Deposit" && tran.Category == "Tax" && tran.SubCategory == "Tax Refund").FirstOrDefault();

            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in filteredTransaction)
                SetDepositIncomeOtherTransaction(xml, bglAccount, cashEntity);

            //Withdrawal 

            filteredTransaction = CashManagementTransactions.Where(tran => tran.ImportTransactionType == "Withdrawal" && tran.Category == "Benefit Payment" && tran.SystemTransactionType == "Withdrawal");
            bglAccount = bglAccountMap.Where(tran => tran.ImportTransactionType == "Withdrawal" && tran.Category == "Benefit Payment" && tran.SubCategory == "Withdrawal").FirstOrDefault();

            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in filteredTransaction)
                SetDepositExpenseOtherTransaction(xml, bglAccount, cashEntity);


            filteredTransaction = CashManagementTransactions.Where(tran => tran.ImportTransactionType == "Withdrawal" && tran.Category == "Expense" && tran.SystemTransactionType == "Accounting Expense");
            bglAccount = bglAccountMap.Where(tran => tran.ImportTransactionType == "Withdrawal" && tran.Category == "Expense" && tran.SubCategory == "Accounting Expense").FirstOrDefault();

            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in filteredTransaction)
                SetDepositExpenseOtherTransaction(xml, bglAccount, cashEntity);

            filteredTransaction = CashManagementTransactions.Where(tran => tran.ImportTransactionType == "Withdrawal" && tran.Category == "Expense" && tran.SystemTransactionType == "Administration Fee");
            bglAccount = bglAccountMap.Where(tran => tran.ImportTransactionType == "Withdrawal" && tran.Category == "Expense" && tran.SubCategory == "Administration Fee").FirstOrDefault();

            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in filteredTransaction)
                SetDepositExpenseOtherTransaction(xml, bglAccount, cashEntity);

            filteredTransaction = CashManagementTransactions.Where(tran => tran.ImportTransactionType == "Withdrawal" && tran.Category == "Expense" && tran.SystemTransactionType == "Advisory Fee");
            bglAccount = bglAccountMap.Where(tran => tran.ImportTransactionType == "Withdrawal" && tran.Category == "Expense" && tran.SubCategory == "Advisory Fee").FirstOrDefault();

            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in filteredTransaction)
                SetDepositExpenseOtherTransaction(xml, bglAccount, cashEntity);

            filteredTransaction = CashManagementTransactions.Where(tran => tran.ImportTransactionType == "Withdrawal" && tran.Category == "Expense" && tran.SystemTransactionType == "General");
            bglAccount = bglAccountMap.Where(tran => tran.ImportTransactionType == "Withdrawal" && tran.Category == "Expense" && tran.SubCategory == "General").FirstOrDefault();

            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in filteredTransaction)
                SetDepositExpenseOtherTransaction(xml, bglAccount, cashEntity);

            filteredTransaction = CashManagementTransactions.Where(tran => tran.ImportTransactionType == "Withdrawal" && tran.Category == "Expense" && tran.SystemTransactionType == "Insurance Premuim (Life)");
            bglAccount = bglAccountMap.Where(tran => tran.ImportTransactionType == "Withdrawal" && tran.Category == "Expense" && tran.SubCategory == "Insurance Premuim (Life)").FirstOrDefault();

            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in filteredTransaction)
                SetDepositExpenseOtherTransaction(xml, bglAccount, cashEntity);

            filteredTransaction = CashManagementTransactions.Where(tran => tran.ImportTransactionType == "Withdrawal" && tran.Category == "Expense" && tran.SystemTransactionType == "Other Insurance");
            bglAccount = bglAccountMap.Where(tran => tran.ImportTransactionType == "Withdrawal" && tran.Category == "Expense" && tran.SubCategory == "Other Insurance").FirstOrDefault();

            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in filteredTransaction)
                SetDepositExpenseOtherTransaction(xml, bglAccount, cashEntity);

            filteredTransaction = CashManagementTransactions.Where(tran => tran.ImportTransactionType == "Withdrawal" && tran.Category == "Expense" && tran.SystemTransactionType == "Internal Cash Movement");
            bglAccount = bglAccountMap.Where(tran => tran.ImportTransactionType == "Withdrawal" && tran.Category == "Expense" && tran.SubCategory == "Internal Cash Movement").FirstOrDefault();

            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in filteredTransaction)
                SetDepositExpenseOtherTransaction(xml, bglAccount, cashEntity);

            filteredTransaction = CashManagementTransactions.Where(tran => tran.ImportTransactionType == "Withdrawal" && tran.Category == "Expense" && tran.SystemTransactionType == "Investment Fee");
            bglAccount = bglAccountMap.Where(tran => tran.ImportTransactionType == "Withdrawal" && tran.Category == "Expense" && tran.SubCategory == "Investment Fee").FirstOrDefault();

            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in filteredTransaction)
                SetDepositExpenseOtherTransaction(xml, bglAccount, cashEntity);

            filteredTransaction = CashManagementTransactions.Where(tran => tran.ImportTransactionType == "Withdrawal" && tran.Category == "Expense" && tran.SystemTransactionType == "Legal Expense");
            bglAccount = bglAccountMap.Where(tran => tran.ImportTransactionType == "Withdrawal" && tran.Category == "Expense" && tran.SubCategory == "Legal Expense").FirstOrDefault();

            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in filteredTransaction)
                SetDepositExpenseOtherTransaction(xml, bglAccount, cashEntity);

            filteredTransaction = CashManagementTransactions.Where(tran => tran.ImportTransactionType == "Withdrawal" && tran.Category == "Expense" && tran.SystemTransactionType == "Pension Payment");
            bglAccount = bglAccountMap.Where(tran => tran.ImportTransactionType == "Withdrawal" && tran.Category == "Expense" && tran.SubCategory == "Pension Payment").FirstOrDefault();

            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in filteredTransaction)
                SetDepositPensionPaymentExpenseTransaction(xml, bglAccount, cashEntity);

            filteredTransaction = CashManagementTransactions.Where(tran => tran.ImportTransactionType == "Withdrawal" && tran.Category == "Expense" && tran.SystemTransactionType == "Property");
            bglAccount = bglAccountMap.Where(tran => tran.ImportTransactionType == "Withdrawal" && tran.Category == "Expense" && tran.SubCategory == "Property").FirstOrDefault();

            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in filteredTransaction)
                SetDepositExpenseOtherTransaction(xml, bglAccount, cashEntity);

            filteredTransaction = CashManagementTransactions.Where(tran => tran.ImportTransactionType == "Withdrawal" && tran.Category == "Expense" && tran.SystemTransactionType == "Regulatory Fee");
            bglAccount = bglAccountMap.Where(tran => tran.ImportTransactionType == "Withdrawal" && tran.Category == "Expense" && tran.SubCategory == "Regulatory Fee").FirstOrDefault();

            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in filteredTransaction)
                SetDepositExpenseOtherTransaction(xml, bglAccount, cashEntity);

            filteredTransaction = CashManagementTransactions.Where(tran => tran.ImportTransactionType == "Withdrawal" && tran.Category == "Expense" && tran.SystemTransactionType == "Transfer Out");
            bglAccount = bglAccountMap.Where(tran => tran.ImportTransactionType == "Withdrawal" && tran.Category == "Expense" && tran.SubCategory == "Transfer Out").FirstOrDefault();

            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in filteredTransaction)
                SetDepositTransferOutTransaction(xml, bglAccount, cashEntity);

            filteredTransaction = CashManagementTransactions.Where(tran => tran.ImportTransactionType == "Withdrawal" && tran.Category == "Tax" && tran.SystemTransactionType == "Business Activity Statement");
            bglAccount = bglAccountMap.Where(tran => tran.ImportTransactionType == "Withdrawal" && tran.Category == "Tax" && tran.SubCategory == "Business Activity Statement").FirstOrDefault();

            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in filteredTransaction)
                SetDepositExpenseOtherTransaction(xml, bglAccount, cashEntity);

            filteredTransaction = CashManagementTransactions.Where(tran => tran.ImportTransactionType == "Withdrawal" && tran.Category == "Tax" && tran.SystemTransactionType == "Instalment Activity Statement");
            bglAccount = bglAccountMap.Where(tran => tran.ImportTransactionType == "Withdrawal" && tran.Category == "Tax" && tran.SubCategory == "Instalment Activity Statement").FirstOrDefault();

            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in filteredTransaction)
                SetDepositExpenseOtherTransaction(xml, bglAccount, cashEntity);
            filteredTransaction = CashManagementTransactions.Where(tran => tran.ImportTransactionType == "Withdrawal" && tran.Category == "Tax" && tran.SystemTransactionType == "Business Activity Statement");
            bglAccount = bglAccountMap.Where(tran => tran.ImportTransactionType == "Withdrawal" && tran.Category == "Tax" && tran.SubCategory == "Business Activity Statement").FirstOrDefault();

            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in filteredTransaction)
                SetDepositExpenseOtherTransaction(xml, bglAccount, cashEntity);
            filteredTransaction = CashManagementTransactions.Where(tran => tran.ImportTransactionType == "Withdrawal" && tran.Category == "Tax" && tran.SystemTransactionType == "Income Tax");
            bglAccount = bglAccountMap.Where(tran => tran.ImportTransactionType == "Withdrawal" && tran.Category == "Tax" && tran.SubCategory == "Income Tax").FirstOrDefault();

            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in filteredTransaction)
                SetDepositExpenseOtherTransaction(xml, bglAccount, cashEntity);
            filteredTransaction = CashManagementTransactions.Where(tran => tran.ImportTransactionType == "Withdrawal" && tran.Category == "Tax" && tran.SystemTransactionType == "PAYG");
            bglAccount = bglAccountMap.Where(tran => tran.ImportTransactionType == "Withdrawal" && tran.Category == "Tax" && tran.SubCategory == "PAYG").FirstOrDefault();

            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in filteredTransaction)
                SetDepositExpenseOtherTransaction(xml, bglAccount, cashEntity);

            filteredTransaction = CashManagementTransactions.Where(tran => tran.ImportTransactionType == "Withdrawal" && tran.Category == "Tax" && tran.SystemTransactionType == "Tax Refund");
            bglAccount = bglAccountMap.Where(tran => tran.ImportTransactionType == "Withdrawal" && tran.Category == "Tax" && tran.SubCategory == "Tax Refund").FirstOrDefault();

            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in filteredTransaction)
                SetDepositExpenseOtherTransaction(xml, bglAccount, cashEntity);

            filteredTransaction = CashManagementTransactions.Where(tran => tran.ImportTransactionType == "Withdrawal" && tran.Category == "Investment" && tran.SystemTransactionType == "Application");
            bglAccount = bglAccountMap.Where(tran => tran.ImportTransactionType == "Withdrawal" && tran.Category == "Investment" && tran.SubCategory == "Application").FirstOrDefault();

            foreach (Oritax.TaxSimp.Common.CashManagementEntity cashEntity in filteredTransaction)
                SetInvestmentPurchaseTransaction(ListAccountProcess, this.ClientId, xml, bglAccount, cashEntity, orgCM);
        }

        private static void SetInvestmentPurchaseTransaction(List<AccountProcessTaskEntity> ListAccountProcess, string clientID, XmlTextWriter xml, BGLAccount bglAccount, Oritax.TaxSimp.Common.CashManagementEntity cashEntity, IOrganization orgCM)
        {
            xml.WriteStartElement("Transaction");
            {
                xml.WriteElementString("Transaction_Type", "Other Transaction");
                xml.WriteElementString("Transaction_Source", "Journal");
                xml.WriteElementString("Category", "Assets");
                xml.WriteElementString("Account_Code_Type", "Simple Fund");
                xml.WriteElementString("Bank_Account_No", cashEntity.BGLCodeCash);
                xml.WriteElementString("Transaction_Date", cashEntity.TransactionDate.Date.ToString("dd/MM/yyyy"));
                xml.WriteElementString("Amount", (-1 * cashEntity.TotalAmount).ToString());
                xml.WriteElementString("Text", cashEntity.Comment);
                xml.WriteElementString("Account_Code", cashEntity.BGLCodeSuspense);
                if (cashEntity.Comment.Contains("IV0"))
                {
                    xml.WriteElementString("Security_Type", "Units in Listed Trust (Australian)");
                    string code = ExtractSecInformationViaBankComment(xml, cashEntity, orgCM);

                    foreach (var accountProcessTaskEntity in ListAccountProcess)
                    {
                        if (accountProcessTaskEntity.LinkedEntity != null && accountProcessTaskEntity.LinkedEntity.Clid != Guid.Empty)
                        {
                            var asset = orgCM.Assets.Where(var => var.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                            var model = orgCM.Model.Where(mod => mod.ID == accountProcessTaskEntity.ModelID).FirstOrDefault();
                            var linkedAsset = model.Assets.Where(ast => ast.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                            var product = linkedAsset.Products.Where(pro => pro.ID == accountProcessTaskEntity.TaskID).FirstOrDefault();

                            if (product != null && accountProcessTaskEntity.LinkedEntityType == OrganizationType.ManagedInvestmentSchemesAccount && code != string.Empty && product.TaskDescription.Contains(code))
                            {
                                if (accountProcessTaskEntity.LinkedEntity.Clid == Guid.Empty || accountProcessTaskEntity.LinkedEntity.Csid == Guid.Empty)
                                {
                                    accountProcessTaskEntity.LinkedEntity.Clid = product.EntityId.Clid;
                                    accountProcessTaskEntity.LinkedEntity.Csid = product.EntityId.Csid;
                                }

                                IOrganizationUnit managedInvestmentSchemesAccountCM = orgCM.Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as IOrganizationUnit;
                                ManagedInvestmentSchemesAccountEntity managedInvestmentSchemesAccountEntity = managedInvestmentSchemesAccountCM.ClientEntity as ManagedInvestmentSchemesAccountEntity;
                                var linkedMISAccount = managedInvestmentSchemesAccountEntity.FundAccounts.Where(fa => product.TaskDescription.Contains(fa.Code)).FirstOrDefault();
                                var filteredHoldingTransaction = linkedMISAccount.FundTransactions.Where(cliFundTrans => cliFundTrans.ClientID == clientID).Where(tran => tran.TradeDate.Date == cashEntity.TransactionDate.Date && tran.Amount == cashEntity.TotalAmount).FirstOrDefault();
                                if (filteredHoldingTransaction != null)
                                    xml.WriteElementString("Quantity", filteredHoldingTransaction.Shares.ToString());
                                else
                                    xml.WriteElementString("Quantity", "0");
                            }
                        }
                    }
                }
                else
                    xml.WriteElementString("Security_Type", "Bank Account");
            }
            xml.WriteEndElement();
        }

        private static void SetInvestmentDisposalTransaction(List<AccountProcessTaskEntity> ListAccountProcess, string clientID, XmlTextWriter xml, BGLAccount bglAccount, Oritax.TaxSimp.Common.CashManagementEntity cashEntity, IOrganization orgCM)
        {
            xml.WriteStartElement("Transaction");
            {
                xml.WriteElementString("Transaction_Type", "Other Transaction");
                xml.WriteElementString("Transaction_Source", "Journal");
                xml.WriteElementString("Category", "Assets");
                xml.WriteElementString("Account_Code_Type", "Simple Fund");
                xml.WriteElementString("Bank_Account_No", cashEntity.BGLCodeCash);
                xml.WriteElementString("Transaction_Date", cashEntity.TransactionDate.Date.ToString("dd/MM/yyyy"));
                xml.WriteElementString("Account_Code", bglAccount.DefaultAccountingCode.ToString());
                xml.WriteElementString("Amount", (-1 * cashEntity.TotalAmount).ToString());
                xml.WriteElementString("Text", cashEntity.Comment);
                if (cashEntity.Comment.Contains("IV0"))
                {
                    xml.WriteElementString("Security_Type", "Units in Listed Trust (Australian)");
                    string code = ExtractSecInformationViaBankComment(xml, cashEntity, orgCM);

                    foreach (var accountProcessTaskEntity in ListAccountProcess)
                    {
                        if (accountProcessTaskEntity.LinkedEntity != null && accountProcessTaskEntity.LinkedEntity.Clid != Guid.Empty)
                        {
                            var asset = orgCM.Assets.Where(var => var.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                            var model = orgCM.Model.Where(mod => mod.ID == accountProcessTaskEntity.ModelID).FirstOrDefault();
                            var linkedAsset = model.Assets.Where(ast => ast.ID == accountProcessTaskEntity.AssetID).FirstOrDefault();
                            var product = linkedAsset.Products.Where(pro => pro.ID == accountProcessTaskEntity.TaskID).FirstOrDefault();

                            if (product != null && accountProcessTaskEntity.LinkedEntityType == OrganizationType.ManagedInvestmentSchemesAccount && code != string.Empty && product.TaskDescription.Contains(code))
                            {
                                if (accountProcessTaskEntity.LinkedEntity.Clid == Guid.Empty || accountProcessTaskEntity.LinkedEntity.Csid == Guid.Empty)
                                {
                                    accountProcessTaskEntity.LinkedEntity.Clid = product.EntityId.Clid;
                                    accountProcessTaskEntity.LinkedEntity.Csid = product.EntityId.Csid;
                                }

                                IOrganizationUnit managedInvestmentSchemesAccountCM = orgCM.Broker.GetCMImplementation(accountProcessTaskEntity.LinkedEntity.Clid, accountProcessTaskEntity.LinkedEntity.Csid) as IOrganizationUnit;
                                ManagedInvestmentSchemesAccountEntity managedInvestmentSchemesAccountEntity = managedInvestmentSchemesAccountCM.ClientEntity as ManagedInvestmentSchemesAccountEntity;
                                var linkedMISAccount = managedInvestmentSchemesAccountEntity.FundAccounts.Where(fa => product.TaskDescription.Contains(fa.Code)).FirstOrDefault();
                                var filteredHoldingTransaction = linkedMISAccount.FundTransactions.Where(cliFundTrans => cliFundTrans.ClientID == clientID).Where(tran => tran.TradeDate.Date == cashEntity.TransactionDate.Date && tran.Amount == cashEntity.TotalAmount).FirstOrDefault();
                                if (filteredHoldingTransaction != null)
                                    xml.WriteElementString("Quantity", filteredHoldingTransaction.Shares.ToString());
                                else
                                    xml.WriteElementString("Quantity", "0");
                            }
                        }
                    }
                }
                else
                    xml.WriteElementString("Security_Type", "Bank Account");
            }
            xml.WriteEndElement();
        }

        private static void AddInvestmentHoldingSeFunds(XmlTextWriter xml, DataRow secRow, DateTime holdingDate, string secType)
        {
            xml.WriteStartElement("investment");
            {
                xml.WriteElementString("holding_date", holdingDate.ToString("dd/MM/yyyy"));
                xml.WriteElementString("holding_quantity", secRow[SecuritySummaryReportDS.TOTALUNITS].ToString());
                xml.WriteElementString("holding_costbase", secRow[SecuritySummaryReportDS.TOTAL].ToString());
                xml.WriteElementString("security_type", secType);
                xml.WriteElementString("security_code", secRow[SecuritySummaryReportDS.SECNAME].ToString());
            }
            xml.WriteEndElement();
        }

        private static void AddInvestmentHoldingCash(XmlTextWriter xml, DataRow secRow, DateTime holdingDate)
        {
            xml.WriteStartElement("investment");
            {
                xml.WriteElementString("holding_date", holdingDate.ToString("dd/MM/yyyy"));
                xml.WriteElementString("holding_costbase", secRow[SecuritySummaryReportDS.TOTAL].ToString());
                xml.WriteElementString("security_type", "Bank Account");
                xml.WriteElementString("bank_account_no", secRow[SecuritySummaryReportDS.BGLCODE].ToString());
            }
            xml.WriteEndElement();
        }

        private static void SetDepositIncomeOtherTransaction(XmlTextWriter xml, BGLAccount bglAccount, Oritax.TaxSimp.Common.CashManagementEntity cashEntity)
        {
            xml.WriteStartElement("Transaction");
            {
                xml.WriteElementString("Transaction_Type", "Other Transaction");
                xml.WriteElementString("Transaction_Source", "Journal");
                xml.WriteElementString("Category", "Income");
                xml.WriteElementString("Transaction_Date", cashEntity.TransactionDate.Date.ToString("dd/MM/yyyy"));
                xml.WriteElementString("Account_Code_Type", "Simple Fund");
                xml.WriteElementString("Bank_Account_No", cashEntity.BGLCodeCash);
                if (cashEntity.BGLCode != string.Empty && (cashEntity.SystemTransactionType == "Commission Rebate" || cashEntity.SystemTransactionType == "Interest") && cashEntity.Category == "Income")
                {
                    xml.WriteElementString("Account_Code", cashEntity.BGLCode);
                }
                else
                    xml.WriteElementString("Account_Code", bglAccount.DefaultAccountingCode.ToString());

                xml.WriteElementString("Amount", (-1 * cashEntity.TotalAmount).ToString());
                xml.WriteElementString("Text", cashEntity.Comment);
            }
            xml.WriteEndElement();

        }

        private static void SetDepositTransferInTransaction(XmlTextWriter xml, BGLAccount bglAccount, Oritax.TaxSimp.Common.CashManagementEntity cashEntity)
        {
            xml.WriteStartElement("Transaction");
            {
                xml.WriteElementString("Transaction_Type", "Other Transaction");
                xml.WriteElementString("Transaction_Source", "Journal");
                xml.WriteElementString("Category", "Income");
                xml.WriteElementString("Transaction_Date", cashEntity.TransactionDate.Date.ToString("dd/MM/yyyy"));
                xml.WriteElementString("Account_Code_Type", "Simple Fund");
                xml.WriteElementString("Bank_Account_No", cashEntity.BGLCodeCash);
                xml.WriteElementString("Account_Code", cashEntity.BGLCodeSuspense);
                xml.WriteElementString("Amount", (-1 * cashEntity.TotalAmount).ToString());
                xml.WriteElementString("Text", cashEntity.Comment);
            }
            xml.WriteEndElement();
        }

        private static void SetDepositExpenseOtherTransaction(XmlTextWriter xml, BGLAccount bglAccount, Oritax.TaxSimp.Common.CashManagementEntity cashEntity)
        {
            xml.WriteStartElement("Transaction");
            {
                xml.WriteElementString("Transaction_Type", "Other Transaction");
                xml.WriteElementString("Transaction_Source", "Journal");
                xml.WriteElementString("Category", "Expense");
                xml.WriteElementString("Transaction_Date", cashEntity.TransactionDate.Date.ToString("dd/MM/yyyy"));
                xml.WriteElementString("Account_Code_Type", "Simple Fund");
                xml.WriteElementString("Bank_Account_No", cashEntity.BGLCodeCash);
                xml.WriteElementString("Account_Code", bglAccount.DefaultAccountingCode.ToString());
                xml.WriteElementString("Amount", (-1 * cashEntity.TotalAmount).ToString());
                xml.WriteElementString("Text", cashEntity.Comment);
            }
            xml.WriteEndElement();
        }

        private static void SetDepositPensionPaymentExpenseTransaction(XmlTextWriter xml, BGLAccount bglAccount, Oritax.TaxSimp.Common.CashManagementEntity cashEntity)
        {
            xml.WriteStartElement("Transaction");
            {
                xml.WriteElementString("Transaction_Type", "Other Transaction");
                xml.WriteElementString("Transaction_Source", "Journal");
                xml.WriteElementString("Category", "Expense");
                xml.WriteElementString("Transaction_Date", cashEntity.TransactionDate.Date.ToString("dd/MM/yyyy"));
                xml.WriteElementString("Account_Code_Type", "Simple Fund");
                xml.WriteElementString("Bank_Account_No", cashEntity.BGLCodeCash);
                xml.WriteElementString("Account_Code", cashEntity.BGLCodeSuspense);
                xml.WriteElementString("Amount", (-1 * cashEntity.TotalAmount).ToString());
                xml.WriteElementString("Text", cashEntity.Comment);
            }
            xml.WriteEndElement();
        }

        private static void SetDepositTransferOutTransaction(XmlTextWriter xml, BGLAccount bglAccount, Oritax.TaxSimp.Common.CashManagementEntity cashEntity)
        {
            xml.WriteStartElement("Transaction");
            {
                xml.WriteElementString("Transaction_Type", "Other Transaction");
                xml.WriteElementString("Transaction_Source", "Journal");
                xml.WriteElementString("Category", "Expense");
                xml.WriteElementString("Transaction_Date", cashEntity.TransactionDate.Date.ToString("dd/MM/yyyy"));
                xml.WriteElementString("Account_Code_Type", "Simple Fund");
                xml.WriteElementString("Bank_Account_No", cashEntity.BGLCodeCash);
                xml.WriteElementString("Account_Code", cashEntity.BGLCodeSuspense);
                xml.WriteElementString("Amount", (-1 * cashEntity.TotalAmount).ToString());
                xml.WriteElementString("Text", cashEntity.Comment);
            }
            xml.WriteEndElement();
        }

        private static void SetDepositIncomeDividendsTransaction(XmlTextWriter xml, BGLAccount bglAccount, Oritax.TaxSimp.Common.CashManagementEntity cashEntity, ObservableCollection<DividendEntity> DividendCollection, IOrganization orgCM)
        {
            xml.WriteStartElement("Transaction");
            {
                xml.WriteElementString("Transaction_Type", "Other Transaction");
                xml.WriteElementString("Transaction_Source", "Journal");
                xml.WriteElementString("Category", "Income");
                xml.WriteElementString("Bank_Account_No", cashEntity.BGLCodeCash);
                xml.WriteElementString("Transaction_Date", cashEntity.TransactionDate.Date.ToString("dd/MM/yyyy"));
                xml.WriteElementString("Amount", (-1 * cashEntity.TotalAmount).ToString());
                xml.WriteElementString("Text", cashEntity.Comment);
                xml.WriteElementString("Security_Type", "Shares In Listed Company (Australian)");

                DividendEntity divEntity = DividendCollection.Where(d => d.PaymentDate.Value.Date == cashEntity.TransactionDate.Date && Math.Round(Convert.ToDecimal(d.PaidDividend), 2) == Math.Round(cashEntity.TotalAmount, 2)).FirstOrDefault();

                if (divEntity != null)
                {
                    var secEnt = orgCM.Securities.Where(sec => sec.AsxCode == divEntity.InvestmentCode).FirstOrDefault();
                    xml.WriteElementString("Security_Code", divEntity.InvestmentCode);
                    xml.WriteElementString("Security_Name", secEnt.CompanyName);
                    xml.WriteElementString("Franked_Dividend", divEntity.FrankedAmount.ToString());
                    xml.WriteElementString("Unfranked_Dividend", divEntity.UnfrankedAmount.ToString());
                    xml.WriteElementString("Imputation_Credit", divEntity.TotalFrankingCredit.ToString());
                    if (divEntity.BGLCode != string.Empty)
                        xml.WriteElementString("Account_Code", divEntity.BGLCode);
                    else
                        xml.WriteElementString("Account_Code", cashEntity.BGLCodeSuspense);
                }
                else
                {
                    xml.WriteElementString("Account_Code", cashEntity.BGLCodeSuspense);
                    ExtractSecInformationViaBankComment(xml, cashEntity, orgCM);
                    AddEmptyFrankingInfo(xml);
                }
            }
            xml.WriteEndElement();
        }

        private static void SetDepositIncomeDistributionTransaction(XmlTextWriter xml, BGLAccount bglAccount, Oritax.TaxSimp.Common.CashManagementEntity cashEntity, ObservableCollection<DistributionIncomeEntity> DistributionIncomes, IOrganization orgCM)
        {
            xml.WriteStartElement("Transaction");
            {
                xml.WriteElementString("Transaction_Type", "Other Transaction");
                xml.WriteElementString("Transaction_Source", "Journal");
                xml.WriteElementString("Category", "Assets");
                xml.WriteElementString("Account_Code_Type", "Simple Fund");
                xml.WriteElementString("Transaction_Date", cashEntity.TransactionDate.Date.ToString("dd/MM/yyyy"));
                xml.WriteElementString("Amount", (-1 * cashEntity.TotalAmount).ToString());
                xml.WriteElementString("Text", cashEntity.Comment);
                xml.WriteElementString("Security_Type", "Units in Listed Trust (Australian)");
                xml.WriteElementString("Bank_Account_No", cashEntity.BGLCodeCash);
                string secCode = ExtractSecInformationViaBankComment(xml, cashEntity, orgCM);

                if (secCode.StartsWith("IV0"))
                {
                    var disEntity = DistributionIncomes.Where(d => d.FundCode == secCode).FirstOrDefault();
                    if (disEntity != null)
                        xml.WriteElementString("Account_Code", disEntity.BGLCODE);
                    else
                        xml.WriteElementString("Account_Code", cashEntity.BGLCodeSuspense);

                }
                else
                    xml.WriteElementString("Account_Code", cashEntity.BGLCodeSuspense);
            }
            xml.WriteEndElement();
        }

        private static void AddEmptyFrankingInfo(XmlTextWriter xml)
        {
            xml.WriteElementString("Franked_Dividend", string.Empty);
            xml.WriteElementString("Unfranked_Dividend", string.Empty);
            xml.WriteElementString("Imputation_Credit", string.Empty);
        }

        private static string ExtractSecInformationViaBankComment(XmlTextWriter xml, Oritax.TaxSimp.Common.CashManagementEntity cashEntity, IOrganization orgCM)
        {
            var secEntity = orgCM.Securities.Where(sec => cashEntity.Comment.Contains(sec.AsxCode)).FirstOrDefault();
            if (secEntity != null)
            {
                xml.WriteElementString("Security_Code", secEntity.AsxCode);
                xml.WriteElementString("Security_Name", secEntity.CompanyName);
                return secEntity.AsxCode;
            }
            else
            {
                xml.WriteElementString("Security_Code", string.Empty);
                xml.WriteElementString("Security_Name", string.Empty);
                return string.Empty;
            }
        }

        #endregion

        #region Client Account Validation

        public void RunClientValidation(ClientValidationDS clientValidationDS)
        {
            var clientValidationMessageDetail = new ClientValidationMessageDetail();
            var clientUMAData = ClientEntity as IClientUMAData;
            if (clientUMAData != null)
            {
                List<IdentityCMDetail> sigList = clientUMAData.GetSignatoriesList();

                bool hasAtLeastOneSignatory = false;
                var applicantType = CSVUtilities.GetBankWestType(TypeName);

                foreach (var signEnt in sigList)
                {
                    if (signEnt.Clid != Guid.Empty || signEnt.Cid != Guid.Empty)
                    {
                        var sigUnit = Broker.GetCMImplementation(signEnt.Clid, signEnt.Csid) as IOrganizationUnit;
                        if (sigUnit != null)
                        {
                            hasAtLeastOneSignatory = true;

                            var individualEntity = sigUnit.ClientEntity as IndividualEntity;
                            if (individualEntity != null)
                            {
                                if (individualEntity.Name.IsEmptyOrNull())
                                    AddValidationMessage(clientValidationMessageDetail,
                                                         ClientValidationIssueMessageType.Error,
                                                         ClientValidationMessageType.InvalidSignatoryPersonalDetailGivenName,
                                                         "Please visit '8: Signatory'", clientValidationDS);

                                if (!individualEntity.Name.IsEmptyOrNull() && individualEntity.Name.Length > 30)
                                    AddValidationMessage(clientValidationMessageDetail,
                                                         ClientValidationIssueMessageType.Error,
                                                         ClientValidationMessageType.InvalidSignatoryPersonalDetailGivenName,
                                                          "Please visit '8: Signatory'", clientValidationDS);

                                if (individualEntity.Surname.IsEmptyOrNull())
                                    AddValidationMessage(clientValidationMessageDetail,
                                                         ClientValidationIssueMessageType.Error,
                                                         ClientValidationMessageType.InvalidSignatoryPersonalDetailSurname,
                                                         "Please visit '8: Signatory'", clientValidationDS);

                                if (!individualEntity.Surname.IsEmptyOrNull() && individualEntity.Surname.Length > 30)
                                    AddValidationMessage(clientValidationMessageDetail,
                                                         ClientValidationIssueMessageType.Error,
                                                         ClientValidationMessageType.InvalidSignatoryPersonalDetailSurname,
                                                          "Please visit '8: Signatory'", clientValidationDS);


                                if (individualEntity.MiddleName.IsEmptyOrNull())
                                    AddValidationMessage(clientValidationMessageDetail,
                                                         ClientValidationIssueMessageType.Warning,
                                                         ClientValidationMessageType.InvalidSignatoryPersonalDetailSurname,
                                                         "Please visit '8: Signatory'", clientValidationDS);

                                if (individualEntity.ResidentialAddress == null)
                                    AddValidationMessage(clientValidationMessageDetail,
                                                         ClientValidationIssueMessageType.Error,
                                                         ClientValidationMessageType.MissingAddress,
                                                         "Signatory Residential address is missing for " +
                                                         individualEntity.Fullname, clientValidationDS);

                                if (individualEntity.ResidentialAddress != null && !VerifyAusAddress(individualEntity.ResidentialAddress.Addressline1,
                                                                                                     individualEntity.ResidentialAddress.Addressline2,
                                                                                                     individualEntity.ResidentialAddress.Suburb,
                                                                                                     individualEntity.ResidentialAddress.State,
                                                                                                     individualEntity.ResidentialAddress.PostCode))
                                    AddValidationMessage(clientValidationMessageDetail,
                                                         ClientValidationIssueMessageType.Error,
                                                         ClientValidationMessageType.InvalidAddress,
                                                         "Signatory Residential address is invalid for " +
                                                         individualEntity.Fullname, clientValidationDS);
                                if (applicantType.ToLower() == "trust" || applicantType.ToLower() == "business")
                                {
                                    if (individualEntity.ResidentialAddress != null &&
                                        individualEntity.ResidentialAddress.Country.IsEmptyOrNull())
                                        AddValidationMessage(clientValidationMessageDetail,
                                                             ClientValidationIssueMessageType.Error,
                                                             ClientValidationMessageType.InvalidAddress,
                                                             "Signatory Residential Country is missing for " +
                                                             individualEntity.Fullname, clientValidationDS);

                                    if (individualEntity.ResidentialAddress != null &&
                                        individualEntity.ResidentialAddress.Country.Length > 40)
                                        AddValidationMessage(clientValidationMessageDetail,
                                                             ClientValidationIssueMessageType.Error,
                                                             ClientValidationMessageType.InvalidAddress,
                                                             "Signatory Residential Country is invalid for " +
                                                             individualEntity.Fullname, clientValidationDS);
                                }

                                if (individualEntity.MailingAddress == null && !individualEntity.MailingAddressSame)
                                    AddValidationMessage(clientValidationMessageDetail,
                                                         ClientValidationIssueMessageType.Error,
                                                         ClientValidationMessageType.MissingAddress,
                                                         "Signatory Mailing address is missing for " +
                                                         individualEntity.Fullname, clientValidationDS);

                                if (individualEntity.MailingAddress != null && !VerifyAusAddress(individualEntity.MailingAddress.Addressline1,
                                                                                                 individualEntity.MailingAddress.Addressline2,
                                                                                                 individualEntity.MailingAddress.Suburb,
                                                                                                 individualEntity.MailingAddress.State,
                                                                                                 individualEntity.MailingAddress.PostCode))
                                    AddValidationMessage(clientValidationMessageDetail,
                                                         ClientValidationIssueMessageType.Error,
                                                         ClientValidationMessageType.InvalidAddress,
                                                         "Signatory Mailing address is invalid for " +
                                                         individualEntity.Fullname, clientValidationDS);

                                if (!individualEntity.DOB.HasValue)
                                    AddValidationMessage(clientValidationMessageDetail,
                                                         ClientValidationIssueMessageType.Error,
                                                         ClientValidationMessageType.InvalidSigDOB,
                                                         "Invalid Signatory Date of Birth for " + individualEntity.Fullname,
                                                         clientValidationDS);

                                if (individualEntity.PersonalTitle.Title.IsEmptyOrNull())
                                    AddValidationMessage(clientValidationMessageDetail,
                                                         ClientValidationIssueMessageType.Error,
                                                         ClientValidationMessageType.InvalidSignatoryPersonalTitle,
                                                          "Signatory Title is missing for " +
                                                          individualEntity.Fullname, clientValidationDS);

                                if (individualEntity.Occupation.IsEmptyOrNull())
                                    AddValidationMessage(clientValidationMessageDetail,
                                                         ClientValidationIssueMessageType.Error,
                                                         ClientValidationMessageType.InvalidSignatoryPersonalDetailOccupation,
                                                          "Signatory Occupation is missing for " +
                                                          individualEntity.Fullname, clientValidationDS);

                                if (!individualEntity.Occupation.IsEmptyOrNull() && individualEntity.Occupation.Length > 30)
                                    AddValidationMessage(clientValidationMessageDetail,
                                                         ClientValidationIssueMessageType.Error,
                                                         ClientValidationMessageType.InvalidSignatoryPersonalDetailOccupation,
                                                          "Signatory Occupation is invalid for " +
                                                          individualEntity.Fullname, clientValidationDS);

                                if (!individualEntity.Employer.IsEmptyOrNull() && individualEntity.Employer.Length > 30)
                                    AddValidationMessage(clientValidationMessageDetail,
                                                         ClientValidationIssueMessageType.Error,
                                                         ClientValidationMessageType.InvalidSignatoryPersonalDetailEmployer,
                                                         "Signatory Employer is invalid for " +
                                                          individualEntity.Fullname, clientValidationDS);

                                if (individualEntity.HomePhoneNumber.PhoneNumber.IsEmptyOrNull() && individualEntity.WorkPhoneNumber.PhoneNumber.IsEmptyOrNull() && individualEntity.MobilePhoneNumber.MobileNumber.IsEmptyOrNull())
                                    AddValidationMessage(clientValidationMessageDetail,
                                                         ClientValidationIssueMessageType.Error,
                                                         ClientValidationMessageType.MissingAtLeastOneContactNo,
                                                         "Signatory must have at least one contact no for " +
                                                          individualEntity.Fullname, clientValidationDS);

                                if (!individualEntity.HomePhoneNumber.PhoneNumber.IsEmptyOrNull() && (individualEntity.HomePhoneNumber.CityCode.IsEmptyOrNull() || individualEntity.HomePhoneNumber.CityCode.Length > 2))
                                    AddValidationMessage(clientValidationMessageDetail,
                                                         ClientValidationIssueMessageType.Error,
                                                         ClientValidationMessageType.InvalidSignatoryPersonalDetailHomePhonePrefix,
                                                         "Signatory Home phone prefix is invalid for " +
                                                          individualEntity.Fullname, clientValidationDS);

                                if (!individualEntity.WorkPhoneNumber.PhoneNumber.IsEmptyOrNull() && (individualEntity.WorkPhoneNumber.CityCode.IsEmptyOrNull() || individualEntity.WorkPhoneNumber.CityCode.Length > 2))
                                    AddValidationMessage(clientValidationMessageDetail,
                                                         ClientValidationIssueMessageType.Error,
                                                         ClientValidationMessageType.InvalidSignatoryPersonalDetailWorkPhonePrefix,
                                                         "Signatory Work phone prefix is invalid for " +
                                                          individualEntity.Fullname, clientValidationDS);

                                if (!individualEntity.HomePhoneNumber.PhoneNumber.IsEmptyOrNull() && individualEntity.HomePhoneNumber.PhoneNumber.Length > 8)
                                    AddValidationMessage(clientValidationMessageDetail,
                                                         ClientValidationIssueMessageType.Error,
                                                         ClientValidationMessageType.InvalidSignatoryPersonalDetailHomePhoneNumber,
                                                         "Signatory Home phone number is invalid for " +
                                                          individualEntity.Fullname, clientValidationDS);

                                if (!individualEntity.WorkPhoneNumber.PhoneNumber.IsEmptyOrNull() && individualEntity.WorkPhoneNumber.PhoneNumber.Length > 8)
                                    AddValidationMessage(clientValidationMessageDetail,
                                                         ClientValidationIssueMessageType.Error,
                                                         ClientValidationMessageType.InvalidSignatoryPersonalDetailWorkPhoneNumber,
                                                         "Signatory Work phone number is invalid for " +
                                                          individualEntity.Fullname, clientValidationDS);

                                if (!individualEntity.MobilePhoneNumber.MobileNumber.IsEmptyOrNull() && individualEntity.MobilePhoneNumber.MobileNumber.Length > 10)
                                    AddValidationMessage(clientValidationMessageDetail,
                                                         ClientValidationIssueMessageType.Error,
                                                         ClientValidationMessageType.InvalidSignatoryPersonalDetailMobilePhoneNumber,
                                                         "Signatory Mobile phone number is invalid for " +
                                                          individualEntity.Fullname, clientValidationDS);

                                var idDocumentList = individualEntity.DocumentTypeEntityList().Values.Where(c => c.IsEnabled);

                                var documentTypeEntities = idDocumentList as IList<DocumentTypeEntity> ?? idDocumentList.ToList();
                                if (!documentTypeEntities.Any())
                                    AddValidationMessage(clientValidationMessageDetail,
                                                         ClientValidationIssueMessageType.Error,
                                                         ClientValidationMessageType.InvalidIDInformation,
                                                         "No Proof of ID is added for " + individualEntity.Fullname,
                                                         clientValidationDS);
                                else if (documentTypeEntities.Count() == 1)
                                    AddValidationMessage(clientValidationMessageDetail,
                                                         ClientValidationIssueMessageType.Error,
                                                         ClientValidationMessageType.InvalidIDInformation,
                                                         "Only one Proof of ID is added for " + individualEntity.Fullname +
                                                         ". Min. 2 is required", clientValidationDS);
                                else
                                {
                                    foreach (var documentID in documentTypeEntities)
                                    {
                                        if (documentID.IssuePlace.IsEmptyOrNull())
                                            AddValidationMessage(clientValidationMessageDetail,
                                                                 ClientValidationIssueMessageType.Error,
                                                                 ClientValidationMessageType.InvalidIDIssuePlace,
                                                                 "The 'Issue Place' is invalid or missing for ID: " +
                                                                 documentID.Type + ", for " + individualEntity.Fullname,
                                                                 clientValidationDS);

                                        if (!documentID.ExpiryDate.HasValue)
                                            AddValidationMessage(clientValidationMessageDetail,
                                                                 ClientValidationIssueMessageType.Error,
                                                                 ClientValidationMessageType.InvalidIDExpDate,
                                                                 "The 'Expiry Date' is invalid or is missing for ID: " +
                                                                 documentID.Type + ", for " + individualEntity.Fullname,
                                                                 clientValidationDS);

                                        if (!documentID.IssueDate.HasValue)
                                            AddValidationMessage(clientValidationMessageDetail,
                                                                 ClientValidationIssueMessageType.Warning,
                                                                 ClientValidationMessageType.InvalidIDExpDate,
                                                                 "The 'Issue Date' is invalid or is missing for ID: " +
                                                                 documentID.Type + ", for " + individualEntity.Fullname,
                                                                 clientValidationDS);

                                        if (documentID.DocNumber.IsEmptyOrNull())
                                            AddValidationMessage(clientValidationMessageDetail,
                                                                 ClientValidationIssueMessageType.Error,
                                                                 ClientValidationMessageType.InvalidIDExpDate,
                                                                 "The 'Document No' is invalid or is missing for ID: " +
                                                                 documentID.Type + ", for " + individualEntity.Fullname,
                                                                 clientValidationDS);

                                        if (documentID.AttachementID == null && documentID.AttachementID == Guid.Empty)
                                            AddValidationMessage(clientValidationMessageDetail,
                                                                 ClientValidationIssueMessageType.Warning,
                                                                 ClientValidationMessageType.MissingIDAttachment,
                                                                 "Please add scanned document as attachment for ID: " +
                                                                 documentID.Type + ", for " + individualEntity.Fullname,
                                                                 clientValidationDS);

                                    }
                                }
                            }
                        }
                    }
                }

                if (string.IsNullOrEmpty(this.ApplicationID))
                {
                    AddValidationMessage(clientValidationMessageDetail, ClientValidationIssueMessageType.Error, ClientValidationMessageType.MissingApplicationID, "Application ID is missing. Application ID is required", clientValidationDS);
                }

                if (string.IsNullOrEmpty(applicantType))
                {
                    AddValidationMessage(clientValidationMessageDetail, ClientValidationIssueMessageType.Error, ClientValidationMessageType.MissingApplicantType, "Applicant Type is missing. Applicant Type is required", clientValidationDS);
                }
                if (string.IsNullOrEmpty(CSVUtilities.GetTrusteesCompanyType(TypeName)))
                {
                    AddValidationMessage(clientValidationMessageDetail, ClientValidationIssueMessageType.Error, ClientValidationMessageType.MissingTrusteeCompany, "Trustee Company is missing. Trustees Company is required", clientValidationDS);
                }

                if (!string.IsNullOrEmpty(applicantType) && (applicantType.ToLower() == "trust" || applicantType.ToLower() == "business"))
                {
                    if (this.Name.IsEmptyOrNull())
                        AddValidationMessage(clientValidationMessageDetail, ClientValidationIssueMessageType.Error,
                                             ClientValidationMessageType.MissingTrusteeEntityName,
                                             "Please enter Trustee Name. It is required", clientValidationDS);
                    if (!this.Name.IsEmptyOrNull() && this.Name.Length > 40)
                        AddValidationMessage(clientValidationMessageDetail, ClientValidationIssueMessageType.Error,
                                             ClientValidationMessageType.MissingTrusteeEntityName,
                                             "Please enter valid Trustee Name. It should not exceed 40 char", clientValidationDS);

                    if (sigList.Count == 0)
                    {
                        AddValidationMessage(clientValidationMessageDetail, ClientValidationIssueMessageType.Error,
                                             ClientValidationMessageType.MissingTrusteeContactName,
                                             "Please enter Trustee Contact Name. It is required",
                                             clientValidationDS);

                        AddValidationMessage(clientValidationMessageDetail, ClientValidationIssueMessageType.Error,
                                             ClientValidationMessageType.MissingTrusteeContactPhPrefix,
                                             "Please enter Trustee Contact Phone Prefix. It is required",
                                             clientValidationDS);

                        AddValidationMessage(clientValidationMessageDetail, ClientValidationIssueMessageType.Error,
                                             ClientValidationMessageType.MissingTrusteeContactPhNumber,
                                             "Please enter Trustee Contact Phone Number. It is required",
                                             clientValidationDS);
                    }

                    if (applicantType.ToLower() == "trust")
                    {
                        if (CSVUtilities.GetTrustType(TypeName).IsEmptyOrNull())
                        {
                            AddValidationMessage(clientValidationMessageDetail, ClientValidationIssueMessageType.Error,
                                             ClientValidationMessageType.MissingTrusteeTrustType,
                                             "Please enter Trustee Trust Type. It is required",
                                             clientValidationDS);
                        }
                    }
                }
                if (clientUMAData.Address.RegisteredAddress == null)
                    AddValidationMessage(clientValidationMessageDetail, ClientValidationIssueMessageType.Error, ClientValidationMessageType.MissingAddress, "Registered Address is missing. Registered is required", clientValidationDS);

                if (clientUMAData.Address.RegisteredAddress != null && !VerifyAusAddress(clientUMAData.Address.RegisteredAddress.Addressline1, clientUMAData.Address.RegisteredAddress.Addressline2, clientUMAData.Address.RegisteredAddress.Suburb, clientUMAData.Address.RegisteredAddress.State, clientUMAData.Address.RegisteredAddress.PostCode))
                    AddValidationMessage(clientValidationMessageDetail, ClientValidationIssueMessageType.Error, ClientValidationMessageType.InvalidAddress, "Registered Address is invalid. Please enter valid registered address", clientValidationDS);

                if (clientUMAData.Address.MailingAddress == null)
                    AddValidationMessage(clientValidationMessageDetail, ClientValidationIssueMessageType.Error, ClientValidationMessageType.MissingAddress, "Mailing Address is missing. Mailing Address is required", clientValidationDS);

                if (clientUMAData.Address.MailingAddress != null && !VerifyAusAddress(clientUMAData.Address.MailingAddress.Addressline1, clientUMAData.Address.MailingAddress.Addressline2, clientUMAData.Address.MailingAddress.Suburb, clientUMAData.Address.MailingAddress.State, clientUMAData.Address.MailingAddress.PostCode))
                    AddValidationMessage(clientValidationMessageDetail, ClientValidationIssueMessageType.Error, ClientValidationMessageType.InvalidAddress, "Mailing Address is invalid. Please enter valid mailing address", clientValidationDS);

                if (clientUMAData.Address.BusinessAddress == null)
                    AddValidationMessage(clientValidationMessageDetail, ClientValidationIssueMessageType.Warning, ClientValidationMessageType.MissingAddress, "Business Address is missing.", clientValidationDS);

                if (clientUMAData.Address.BusinessAddress != null && !VerifyAusAddress(clientUMAData.Address.BusinessAddress.Addressline1, clientUMAData.Address.BusinessAddress.Addressline2, clientUMAData.Address.BusinessAddress.Suburb, clientUMAData.Address.BusinessAddress.State, clientUMAData.Address.BusinessAddress.PostCode))
                    AddValidationMessage(clientValidationMessageDetail, ClientValidationIssueMessageType.Warning, ClientValidationMessageType.InvalidAddress, "Business Address is invalid. Please enter valid business address", clientValidationDS);

                if (this.ComponentInformation.DisplayName == "Client Individual")
                {
                    var clientIndividualEntity = ClientEntity as ClientIndividualEntity;
                    if (clientIndividualEntity != null)
                        ValidateABNACNTFN(clientIndividualEntity.ABN, "", clientIndividualEntity.TFN, clientValidationMessageDetail, clientValidationDS);
                }

                else if (this.ComponentInformation.DisplayName == "Other Trusts Corporate" || this.ComponentInformation.DisplayName == "SMSF Corporate Trustee")
                {
                    var corporateEntity = ClientEntity as CorporateEntity;
                    if (corporateEntity != null)
                    {
                        ValidateABNACNTFN(corporateEntity.ABN, corporateEntity.ACN, corporateEntity.TFN, clientValidationMessageDetail, clientValidationDS);

                        if (corporateEntity.CorporateTrusteeName.IsEmptyOrNull())
                            AddValidationMessage(clientValidationMessageDetail, ClientValidationIssueMessageType.Error, ClientValidationMessageType.MissingCorporateTrusteeEntityName, "Please enter Corporate Trustee Name. It is required", clientValidationDS);
                    }
                }

                else if (this.ComponentInformation.DisplayName == "Corporation Private" || this.ComponentInformation.DisplayName == "Corporation Public")
                {
                    var corporateEntity = ClientEntity as CorporateEntity;
                    if (corporateEntity != null)
                    {
                        ValidateABNACNTFN(corporateEntity.ABN, corporateEntity.ACN, corporateEntity.TFN, clientValidationMessageDetail, clientValidationDS);
                    }
                }

                else if (this.ComponentInformation.DisplayName == "Other Trusts Individual" || this.ComponentInformation.DisplayName == "SMSF Individual Trustee")
                {
                    var clientIndividualEntity = ClientEntity as ClientIndividualEntity;
                    if (clientIndividualEntity != null)
                        ValidateABNACNTFN(clientIndividualEntity.ABN, "", clientIndividualEntity.TFN, clientValidationMessageDetail, clientValidationDS);
                }
            }
        }

        private void ValidateABNACNTFN(string abn, string acn, string tfn, ClientValidationMessageDetail clientValidationMessageDetail, ClientValidationDS clientValidationDS)
        {
            if (abn.IsEmptyOrNull() && acn.IsEmptyOrNull())
                AddValidationMessage(clientValidationMessageDetail, ClientValidationIssueMessageType.Error, ClientValidationMessageType.MissingABNACN, "Please enter ABN and / or ACN", clientValidationDS);

            if (!acn.IsEmptyOrNull() && acn.Length != 9)
            {
                AddValidationMessage(clientValidationMessageDetail, ClientValidationIssueMessageType.Error, ClientValidationMessageType.MissingABNACN, "Please enter valid ACN", clientValidationDS);
            }
            if (!abn.IsEmptyOrNull() && abn.Length != 11)
            {
                AddValidationMessage(clientValidationMessageDetail, ClientValidationIssueMessageType.Error, ClientValidationMessageType.MissingABNACN, "Please enter valid ACN", clientValidationDS);
            }
            //if (tfn.IsEmptyOrNull())
            //    AddValidationMessage(clientValidationMessageDetail, ClientValidationIssueMessageType.Warning, ClientValidationMessageType.MissingTFN, "Please enter valid TFN", clientValidationDS);
            if (!tfn.IsEmptyOrNull() && tfn.Length != 9)
            {
                AddValidationMessage(clientValidationMessageDetail, ClientValidationIssueMessageType.Error, ClientValidationMessageType.MissingTFN, "Please enter valid TFN", clientValidationDS);
            }
        }

        public bool VerifyAusAddress(string addressLine1, string addressLine2, string suburb, string state, string postCode)
        {
            string getMemberHoldingSoap = string.Empty;

            string[] addressLine1Array = (addressLine1 ?? string.Empty).Split(' ');
            string[] addressLine2Array = (addressLine2 ?? string.Empty).Split(' ');

            StringBuilder addressStrBuilder = new StringBuilder();

            foreach (string para in addressLine1Array)
            {
                addressStrBuilder.Append(para);
                addressStrBuilder.Append("+");
            }

            foreach (string para in addressLine2Array)
            {
                addressStrBuilder.Append(para);
                addressStrBuilder.Append("+");
            }

            addressStrBuilder.Append(suburb ?? string.Empty);
            addressStrBuilder.Append("+");
            addressStrBuilder.Append(state ?? string.Empty);
            addressStrBuilder.Append("+");
            addressStrBuilder.Append(postCode ?? string.Empty);
            bool isValidAddress;
            try
            {
                HttpWebRequest getMemberHoldingRequest = GetWebServiceRequest("http://api.addressify.com.au/address/validate?api_key=4d929fa2-b96b-4e66-b7ae-c2351b86637d&term=" + addressStrBuilder.ToString());

                using (Stream getMemberHoldingStm = getMemberHoldingRequest.GetRequestStream())
                {
                    using (StreamWriter getMemberHoldingStmw = new StreamWriter(getMemberHoldingStm))
                    {
                        getMemberHoldingStmw.Write(getMemberHoldingSoap);
                    }
                }

                WebResponse getMemberHoldingResponse = getMemberHoldingRequest.GetResponse();
                Stream getMemberHoldingResponseStream = getMemberHoldingResponse.GetResponseStream();
                StreamReader responeStream = new StreamReader(getMemberHoldingResponseStream);
                isValidAddress = bool.Parse(responeStream.ReadToEnd());
            }
            catch (Exception ex)
            {
                isValidAddress = false;
            }

            return isValidAddress;
        }

        private static HttpWebRequest GetWebServiceRequest(string url)
        {
            HttpWebRequest getMemberListRequest = (HttpWebRequest)WebRequest.Create(url);
            getMemberListRequest.Headers.Add("SOAPAction", "\"http://tempuri.org/Register\"");
            getMemberListRequest.ContentType = "text/xml;charset=\"utf-8\"";
            getMemberListRequest.Accept = "text/xml";
            getMemberListRequest.Method = "POST";
            return getMemberListRequest;
        }

        private void AddValidationMessage(ClientValidationMessageDetail clientValidationMessageDetail, ClientValidationIssueMessageType clientValidationIssueMessageType, ClientValidationMessageType clientValidationMessageType, string messageSource, ClientValidationDS clientValidationDS)
        {
            DataTable dt = clientValidationDS.Tables[ClientValidationDS.ACCOUNTVALIDATIONTABLE];
            DataRow row = dt.NewRow();
            row[ClientValidationDS.VALIDATIONMESSAGE] = Enumeration.GetDescription(clientValidationMessageType);
            var messageDetail = clientValidationMessageDetail.Where(c => c.Key == clientValidationMessageType).FirstOrDefault();
            row[ClientValidationDS.VALIDATIONMESSAGEDETAIL] = messageDetail.Value;
            row[ClientValidationDS.VALIDATIONMESSAGETYPE] = Enumeration.GetDescription(clientValidationIssueMessageType);
            row[ClientValidationDS.ISSUESOURCE] = messageSource;
            dt.Rows.Add(row);
        }

        #endregion

        public void MatchBankTransactionWithDividendOrDistribution(Common.CashManagementEntity cashTrans, IdentityCMDetail account)
        {
            if (ClientEntity is CorporateEntity || ClientEntity is ClientIndividualEntity)
            {

                if (IsCorporateType())
                {
                    var entity = ClientEntity as CorporateEntity;
                    if (entity != null) MatchTransWithDividendOrDistribution(cashTrans, entity.DividendCollection, entity.DistributionIncomes, entity.BankAccounts, account);
                }
                else
                {
                    var entity = ClientEntity as ClientIndividualEntity;
                    if (entity != null)
                        MatchTransWithDividendOrDistribution(cashTrans, entity.DividendCollection, entity.DistributionIncomes, entity.BankAccounts, account);
                }
            }
        }

        private void MatchTransWithDividendOrDistribution(CashManagementEntity cashTrans, ObservableCollection<DividendEntity> dividends, ObservableCollection<DistributionIncomeEntity> distributions, List<IdentityCMDetail> bankAccounts, IdentityCMDetail account)
        {
            if (bankAccounts != null)
            {
                var hasBankAccount = bankAccounts.Count(acc => (acc.Clid == account.Clid && acc.Csid == account.Csid) || acc.Cid == account.Cid) > 0;
                if (hasBankAccount)
                {
                    if (cashTrans.SystemTransactionType == "Dividend" && dividends != null && dividends.Count > 0)
                    {

                        var oldDV = dividends.FirstOrDefault(ss => ss.CashManagementTransactionID == cashTrans.ExternalReferenceID);
                        if (oldDV != null)
                        {
                            oldDV.Status = DividendStatus.Accrued;
                            oldDV.CashManagementTransactionID = string.Empty;
                            cashTrans.DividendID = Guid.Empty;
                            cashTrans.DividendStatus = TransectionStatus._;
                        }
                        var dV = dividends.FirstOrDefault(ee => ee.PaymentDate == cashTrans.TransactionDate && Math.Abs(cashTrans.TotalAmount - (decimal)ee.PaidDividend) < 1);
                        if (dV != null)
                        {
                            cashTrans.DividendID = dV.ID;
                            cashTrans.DividendStatus = TransectionStatus.Matched;
                            dV.Status = DividendStatus.Matched;
                            dV.CashManagementTransactionID = cashTrans.ExternalReferenceID;
                        }
                    }
                    else if (cashTrans.SystemTransactionType == "Distribution" && distributions != null && distributions.Count > 0)
                    {

                        var oldDb = distributions.FirstOrDefault(ss => ss.CashManagementTransactionID == cashTrans.ExternalReferenceID);
                        if (oldDb != null)
                        {
                            oldDb.Status = DistributionIncomeStatus.Accrued;
                            oldDb.CashManagementTransactionID = string.Empty;
                            cashTrans.DividendID = Guid.Empty;
                            cashTrans.DividendStatus = TransectionStatus._;
                        }
                        var dB = distributions.FirstOrDefault(ee => ee.PaymentDate == cashTrans.TransactionDate && Math.Abs(cashTrans.TotalAmount - (decimal)ee.NetCashDistribution) < (decimal)0.5);
                        if (dB != null)
                        {
                            cashTrans.DividendID = dB.ID;
                            cashTrans.DividendStatus = TransectionStatus.Matched;
                            dB.Status = DistributionIncomeStatus.Matched;
                            dB.CashManagementTransactionID = cashTrans.ExternalReferenceID;
                        }
                    }
                }
            }
        }


        public void MatchMISTransactionWithDistribution(MISFundTransactionEntity fundTransaction, IdentityCMDetail misAccountWithFundID, string fundCode)
        {
            if (ClientEntity is CorporateEntity || ClientEntity is ClientIndividualEntity)
            {

                if (IsCorporateType())
                {
                    var entity = ClientEntity as CorporateEntity;
                    if (entity != null) MatchTransWithDistribution(fundTransaction, entity.DistributionIncomes, entity.ManagedInvestmentSchemesAccounts, misAccountWithFundID, EclipseClientID, fundCode);
                }
                else
                {
                    var entity = ClientEntity as ClientIndividualEntity;
                    if (entity != null)
                        MatchTransWithDistribution(fundTransaction, entity.DistributionIncomes, entity.ManagedInvestmentSchemesAccounts, misAccountWithFundID, EclipseClientID, fundCode);
                }
            }
        }

        private void MatchTransWithDistribution(MISFundTransactionEntity fundTransaction, ObservableCollection<DistributionIncomeEntity> distributions, List<IdentityCMDetail> misAccounts, IdentityCMDetail account, string ClientsID, string TransactionFundCode)
        {
            var hasAccount = misAccounts.Count(acc => ((acc.Clid == account.Clid && acc.Csid == account.Csid) || acc.Cid == account.Cid) && acc.FundID == account.FundID) > 0;
            if (hasAccount)
            {
                if (distributions != null && distributions.Count > 0)
                {
                    var oldDb = distributions.FirstOrDefault(ss => ss.MisTransactionID == fundTransaction.ID.ToString());
                    if (oldDb != null)
                    {
                        oldDb.Status = DistributionIncomeStatus.Accrued;
                        oldDb.MisTransactionID = string.Empty;
                        fundTransaction.DistributionID = Guid.Empty;
                        fundTransaction.DistributionStatus = TransectionStatus._;
                    }
                    var dB = distributions.FirstOrDefault(ee => fundTransaction.IsDistributionMatchWithTrans(ClientsID, ee, TransactionFundCode));
                    if (dB != null)
                    {
                        fundTransaction.DistributionID = dB.ID;
                        fundTransaction.DistributionStatus = TransectionStatus.Matched;
                        dB.Status = DistributionIncomeStatus.Matched;
                        dB.MisTransactionID = fundTransaction.ID.ToString();
                    }
                }
            }
        }
    }

    public enum ClientValidationMessageType
    {
        [Description("Missing Application ID")]
        MissingApplicationID,
        [Description("Missing Applicant Type")]
        MissingApplicantType,
        [Description("Missing Trustee Company")]
        MissingTrusteeCompany,
        [Description("Missing Address")]
        MissingAddress,
        [Description("Invalid Address")]
        InvalidAddress,
        [Description("Missing Trustee Entity Name")]
        MissingTrusteeEntityName,
        [Description("Missing Corporate Trustee Entity Name")]
        MissingCorporateTrusteeEntityName,
        [Description("No Signatory / Contact Added")]
        NoSignatoryContactAdded,
        [Description("Missing Dealer Name")]
        MissingDealerName,
        [Description("Missing Adviser Name")]
        MissingAdviserrName,
        [Description("Missing Adviser Code")]
        MissingAdviserCode,
        [Description("Invalid / Missing Signatory Personal Detail Occupation")]
        InvalidSignatoryPersonalDetailOccupation,
        [Description("Invalid / Missing Signatory Personal Detail Title")]
        InvalidSignatoryPersonalTitle,
        [Description("Invalid Signatory Personal Detail Employer")]
        InvalidSignatoryPersonalDetailEmployer,
        [Description("Invalid / Missing Signatory Personal Detail Country Of Residence")]
        InvalidSignatoryPersonalDetailCountryOfResidence,
        [Description("Invalid / Missing Signatory Personal Detail Title")]
        InvalidSignatoryPersonalDetailTitle,
        [Description("Signatory Given Name Has Invalid Character or its missing")]
        InvalidSignatoryPersonalDetailGivenName,
        [Description("Signatory Middle Name Has Invalid Character or its missing")]
        InvalidSignatoryPersonalDetailMiddle,
        [Description("Signatory Surname Has Invalid Character or its missing")]
        InvalidSignatoryPersonalDetailSurname,
        [Description("At least one contact# is required for Signatory")]
        MissingAtLeastOneContactNo,
        [Description("Invalid / Missing Sig DOB")]
        InvalidSigDOB,
        [Description("Missing ID Information")]
        InvalidIDInformation,
        [Description("Invalid / Missing ID Document Issue Date")]
        InvalidIDIssueDate,
        [Description("Invalid / Missing ID Document Expiry Date")]
        InvalidIDExpDate,
        [Description("Invalid / Missing ID Document Issue Place")]
        InvalidIDIssuePlace,
        [Description("Invalid / Missing ID Document Attachment")]
        MissingIDAttachment,
        [Description("Invalid / Missing ABN and / or ACN")]
        MissingABNACN,
        [Description("Invalid / Missing TFN")]
        MissingTFN,
        [Description("Missing Trustee Contact Name")]
        MissingTrusteeContactName,
        [Description("Missing Trustee Contact Phone Prefix")]
        MissingTrusteeContactPhPrefix,
        [Description("Missing Trustee Contact Phone Number")]
        MissingTrusteeContactPhNumber,
        [Description("Missing Trustee Trust Type")]
        MissingTrusteeTrustType,
        [Description("Invalid Signatory Personal Detail Home Phone Number")]
        InvalidSignatoryPersonalDetailHomePhoneNumber,
        [Description("Invalid Signatory Personal Detail Work Phone Number")]
        InvalidSignatoryPersonalDetailWorkPhoneNumber,
        [Description("Invalid Signatory Personal Detail Mobile Phone Number")]
        InvalidSignatoryPersonalDetailMobilePhoneNumber,
        [Description("Invalid / Missing Signatory Personal Detail Home Phone Prefix")]
        InvalidSignatoryPersonalDetailHomePhonePrefix,
        [Description("Invalid / Missing Signatory Personal Detail Work Phone Prefix")]
        InvalidSignatoryPersonalDetailWorkPhonePrefix,
    }

    public class ClientValidationMessageDetail : Dictionary<ClientValidationMessageType, string>
    {
        public ClientValidationMessageDetail()
        {
            this.Add(ClientValidationMessageType.MissingApplicationID, "The application id is missing");
            this.Add(ClientValidationMessageType.MissingApplicantType, "The applicant type is missing");
            this.Add(ClientValidationMessageType.MissingTrusteeCompany, "The trustee company is missing");
            this.Add(ClientValidationMessageType.MissingAddress, "The address is missing");
            this.Add(ClientValidationMessageType.InvalidAddress, "The address entered is invalid, Please enter correct address");
            this.Add(ClientValidationMessageType.MissingTrusteeEntityName, "Please enter trust full name");
            this.Add(ClientValidationMessageType.MissingCorporateTrusteeEntityName, "Please enter corporate trustee full name");
            this.Add(ClientValidationMessageType.NoSignatoryContactAdded, "Please enter at least one signatory");
            this.Add(ClientValidationMessageType.MissingDealerName, "Dealer name is missing. This should be automatically select if Adviser Firm is selected properly. Please contact e-Clipse Team.");
            this.Add(ClientValidationMessageType.MissingAdviserrName, "Please select Adviser Firm on Tab '1: Account'. Please contact e-Clipse Team for further assistance if required");
            this.Add(ClientValidationMessageType.MissingAdviserCode, "The SK code for Adviser Firm is invalid or missing. Please contact e-Clipse Team for further assistance if required");
            this.Add(ClientValidationMessageType.InvalidSignatoryPersonalDetailOccupation, "Signatory Occupation Title is missing or invalid. Please enter valid Occupation Title");
            this.Add(ClientValidationMessageType.InvalidSignatoryPersonalDetailTitle, "Signatory Title is missing or invalid. Please enter valid Title");
            this.Add(ClientValidationMessageType.InvalidSignatoryPersonalDetailEmployer, "Signatory Employer is invalid. Please enter valid Employer");
            this.Add(ClientValidationMessageType.InvalidSignatoryPersonalDetailCountryOfResidence, "Signatory Country of residence is missing or invalid. Please enter valid country of residence");
            this.Add(ClientValidationMessageType.InvalidSignatoryPersonalDetailGivenName, "Please enter valid Given Name");
            this.Add(ClientValidationMessageType.InvalidSignatoryPersonalDetailSurname, "Please enter valid Surname");
            this.Add(ClientValidationMessageType.InvalidSignatoryPersonalDetailMiddle, "Please enter valid Middle Name");
            this.Add(ClientValidationMessageType.MissingAtLeastOneContactNo, "Signatory must have at least one contact no. i.e. Work Phone #, Home Phone # or Mobile Phone #");
            this.Add(ClientValidationMessageType.InvalidSigDOB, "Invalid / Missing Date of Birth");
            this.Add(ClientValidationMessageType.InvalidIDInformation, "Please enter at least two ID Information per added Signatory");
            this.Add(ClientValidationMessageType.InvalidIDIssueDate, "Please enter valid issue date for ID");
            this.Add(ClientValidationMessageType.InvalidIDExpDate, "Please enter valid expiry date for ID");
            this.Add(ClientValidationMessageType.InvalidIDIssuePlace, "Please enter valid issue place for ID");
            this.Add(ClientValidationMessageType.MissingIDAttachment, "Please upload valid scanned document of ID as proof");
            this.Add(ClientValidationMessageType.MissingABNACN, "Please enter ABN and / or ACN");
            this.Add(ClientValidationMessageType.MissingTFN, "Please enter TFN");
            this.Add(ClientValidationMessageType.MissingTrusteeContactName, "The trustee contact name is missing");
            this.Add(ClientValidationMessageType.MissingTrusteeContactPhPrefix, "The trustee contact phone prefix is missing");
            this.Add(ClientValidationMessageType.MissingTrusteeContactPhNumber, "The trustee contact phone number is missing");
            this.Add(ClientValidationMessageType.MissingTrusteeTrustType, "The trustee trust type is missing");
            this.Add(ClientValidationMessageType.InvalidSignatoryPersonalDetailHomePhoneNumber, "Signatory home phone number is invalid. Please enter valid home phone number");
            this.Add(ClientValidationMessageType.InvalidSignatoryPersonalDetailWorkPhoneNumber, "Signatory work phone number is invalid. Please enter valid work phone number");
            this.Add(ClientValidationMessageType.InvalidSignatoryPersonalDetailMobilePhoneNumber, "Signatory mobile phone number is invalid. Please enter valid mobile phone number");
            this.Add(ClientValidationMessageType.InvalidSignatoryPersonalDetailHomePhonePrefix, "Signatory home phone prefix is missing or invalid. Please enter valid home phone prefix");
            this.Add(ClientValidationMessageType.InvalidSignatoryPersonalDetailWorkPhonePrefix, "Signatory work phone prefix is missing or invalid. Please enter valid work phone prefix");
        }
    }

    public enum ClientValidationIssueMessageType
    {
        [Description("Error")]
        Error,
        [Description("Warning")]
        Warning,
        [Description("None")]
        None
    }

    public class ClientValidationDS : UMABaseDS
    {
        public const string ACCOUNTVALIDATIONTABLE = "ACCOUNTVALIDATIONTABLE";
        public const string VALIDATIONMESSAGE = "VALIDATIONMESSAGE";
        public const string ISSUESOURCE = "ISSUESOURCE";
        public const string VALIDATIONMESSAGETYPE = "VALIDATIONMESSAGETYPE";
        public const string VALIDATIONMESSAGEDETAIL = "VALIDATIONMESSAGEDETAIL";

        public ClientValidationDS()
        {
            DataTable dt = new DataTable(ACCOUNTVALIDATIONTABLE);
            dt.Columns.Add(VALIDATIONMESSAGE);
            dt.Columns.Add(VALIDATIONMESSAGEDETAIL);
            dt.Columns.Add(VALIDATIONMESSAGETYPE);
            dt.Columns.Add(ISSUESOURCE, typeof(String));

            this.Tables.Add(dt);
        }
    }
}
