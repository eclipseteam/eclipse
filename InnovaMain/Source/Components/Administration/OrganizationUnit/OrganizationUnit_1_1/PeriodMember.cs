﻿using System;
using System.Runtime.Serialization;
using System.Security.Permissions;

using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.CM.OrganizationUnit
{
    /// <summary>
    /// Class for use in storing the details of member periods in an OrganisationUnit
    /// </summary>
    [Serializable]
    public class PeriodMember : ISerializable
    {
        //USING CUSTOM SERIALIZATION.
        //Still mark the fields as [NonSerialized] for your own infomation but this is ignored now by .NET
        //You must add your own serialzation code for each instance
        //See GetObjectData & Special Constructor

        public DateTime dtStartDate;
        public DateTime dtEndDate;
        public string strTypeName;

        public PeriodMember(DateTime StartDate, DateTime EndDate, string TypeName)
        {
            this.dtStartDate = StartDate;
            this.dtEndDate = EndDate;
            this.strTypeName = TypeName;

        }

        protected PeriodMember(SerializationInfo si, StreamingContext context)
        {
            dtStartDate = Serialize.GetSerializedDateTime(si, "pm_dtStartDate");
            dtEndDate = Serialize.GetSerializedDateTime(si, "pm_dtEndDate");
            strTypeName = Serialize.GetSerializedString(si, "pm_strTypeName");
        }

        //Only allow the .NET Serialization core code to call this function
        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        public virtual void GetObjectData(SerializationInfo si, StreamingContext context)
        {
            Serialize.AddSerializedValue(si, "pm_dtStartDate", dtStartDate);
            Serialize.AddSerializedValue(si, "pm_dtEndDate", dtEndDate);
            Serialize.AddSerializedValue(si, "pm_strTypeName", strTypeName);
        }
    }
}
