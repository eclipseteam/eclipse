﻿using System;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;
using System.Collections.Generic;

namespace Oritax.TaxSimp.Commands
{
    public class GetDefaultFormsCommand : GenericCommandBase<OrganizationUnitCM>
    {
        public override string DoAction(string name)
        {
            return Entity.DefaultForms.ToXmlString();
        }
    }
    public class SaveDefaultFormsCommand : GenericCommandBase<OrganizationUnitCM>
    {
        public override string DoAction(string data)
        {
            Entity.DefaultForms = data.ToNewOrData<List<DefaultFormsFile>>();
            return "success";
        }
    }
}
