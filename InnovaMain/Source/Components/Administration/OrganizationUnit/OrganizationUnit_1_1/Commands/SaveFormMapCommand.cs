﻿using System;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.CM.OrganizationUnit;

namespace Oritax.TaxSimp.Commands
{
    public class SaveFormMapCommand : GenericCommandBase<OrganizationUnitCM>
    {
        public override string DoAction(string data)
        {
            string[] values = data.ToDataArray<string>();
            Guid guid = Guid.Empty;
            Entity.FileMaps.TryGetValue(values[0], out guid);
            if (guid == Guid.Empty)
            {
                Entity.FileMaps.Add(values[0], new Guid(values[1]));
            }
            else
            {
                Entity.FileMaps[values[0]] = new Guid(values[1]);
            }
            return "Successfully File Map saved";
        }
    }
}
