﻿using System;
using System.Collections.Generic;
using Oritax.TaxSimp.CM.OrganizationUnit;

namespace Oritax.TaxSimp.Commands
{
    public enum OrganizationUnitCommandType
    {
        SaveFormMap = 7001,
        GetFormMapByFileName,
        GetFormMapById,
        GenerateContent,
        SaveDefaultForms,
        GetDefaultForms,
        GenerateDefaultFormsContent,
        DownloadAllDefaultForms,
        GenerateDefaultFormsPartialContent

    }

    public static class OrganizationUnitCommandList
    {
        private static readonly Dictionary<int, Type> _Commands = new Dictionary<int, Type>()
        {
            { (int)OrganizationUnitCommandType.SaveFormMap,  typeof(SaveFormMapCommand) },
            { (int)OrganizationUnitCommandType.SaveDefaultForms,  typeof(SaveDefaultFormsCommand) },
            { (int)OrganizationUnitCommandType.GetDefaultForms,  typeof(GetDefaultFormsCommand) },
            { (int)OrganizationUnitCommandType.DownloadAllDefaultForms,  typeof(DownloadAllDefaultFormsCommand) },
        };

        private static ICommandFactory _Factory = CommandFactory.GetFactory(_Commands);

        public static ICommandFactory GetCommandFactory(this OrganizationUnitCM organization)
        {
            return _Factory;
        }
    }
}
