﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;
using Oritax.TaxSimp.FormGenerators;
using Oritax.TaxSimp.Utilities;
using System.Reflection;


namespace Oritax.TaxSimp.Commands
{
    public class DownloadAllDefaultFormsCommand : GenericCommandBase<OrganizationUnitCM>
    {
        public override string DoAction(string data)
        {
            return GetContent(data);
        }

        private string GetContent(string data)
        {
            List<DefaultFormsFile> input = GetFilePaths(data);
            SerializableDictionary<string, byte[]> output = new SerializableDictionary<string, byte[]>();

            foreach (var inputfile in input)
            {
                byte[] original = GetFile(inputfile.FilePath);
                output.Add(inputfile.FileName, original);
            }

            string bytes = string.Empty;
            bytes = Convert.ToBase64String(Zipper.CreateZip(output));
            return bytes;
        }


        private byte[] GetFile(string filepath)
        {
            return File.ReadAllBytes(filepath);
        }

        private List<DefaultFormsFile> GetFilePaths(string value)
        {
            string AssemblyDirectoryName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string docFolder = AssemblyDirectoryName + @"\Documents\Forms\PDF\";

            docFolder += value;

            docFolder += "\\";

            string commtwoFileName = "BWAMI CIP.pdf";// sameeullah,hard coding file types should be maintained in xml docs.
            List<DefaultFormsFile> files = new List<DefaultFormsFile>();

            FileProcessCommadnType type;
            foreach (var file  in GetFiles(docFolder, "*.PDF|*.Doc|*.docx"))
            {

                if (Path.GetFileName(file) == commtwoFileName)
                {
                    type = FileProcessCommadnType.PartialTagged;
                }
                else 
                {

                    if (Path.GetExtension(file).ToUpper()==".PDF")
                    type = FileProcessCommadnType.NormarlTagged;
                    else
                    {
                        type = FileProcessCommadnType.DocFile;
                    }

                }

                files.Add(new DefaultFormsFile() { FileName = Path.GetFileName(file), FilePath = file, MapID = Guid.NewGuid(),Type = type});
            }

            return files;
        }

        private static string[] GetFiles(string sourceFolder, string filters)
        {
            return filters.Split('|').SelectMany(filter => System.IO.Directory.GetFiles(sourceFolder, filter)).ToArray();
        } 

    }
}
