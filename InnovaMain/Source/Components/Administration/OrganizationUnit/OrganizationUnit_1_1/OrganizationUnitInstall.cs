using System;

namespace Oritax.TaxSimp.CM.OrganizationUnit
{
	/// <summary>
	/// Summary description for OrganizationUnitInstall.
	/// </summary>
	public class OrganizationUnitInstall
	{
		#region INSTALLATION PROPERTIES

		// Assembly Installation Properties
		public const string ASSEMBLY_ID="3A775463-DD6F-40C3-9E3B-F83641D4B5BE";
		public const string ASSEMBLY_NAME="OrganizationUnit_1_1";
		public const string ASSEMBLY_DISPLAYNAME="Organization Unit V1.1";
		public const string ASSEMBLY_MAJORVERSION="1";
		public const string ASSEMBLY_MINORVERSION="1";
		public const string ASSEMBLY_DATAFORMAT="0";
		public const string ASSEMBLY_REVISION="2";		//2005.1 

		// Component Installation Properties
		public const string COMPONENT_ID="030EC843-59FA-488c-B75B-A82D52FCBA93";
		public const string COMPONENT_NAME="OrganizationUnit";
		public const string COMPONENT_DISPLAYNAME="Organization Unit";
		public const string COMPONENT_CATEGORY="BusinessStructure";

		// Component Version Installation Properties
		public const string COMPONENTVERSION_STARTDATE="01/01/1990 00:00:00 AM";
		public const string COMPONENTVERSION_ENDDATE="31/12/2050 12:59:59 AM";
		public const string COMPONENTVERSION_PERSISTERASSEMBLY="CommonPersisters_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215";
		public const string COMPONENTVERSION_PERSISTERCLASS="Oritax.TaxSimp.CommonPersistence.BlobPersister";
		public const string COMPONENTVERSION_IMPLEMENTATIONCLASS="Oritax.TaxSimp.CM.OrganizationUnit.OrganizationUnitCM";

		// Associated File Installation Properties
		//		Scripts
		public const string	ASSOCIATEDFILE_SCRIPT_ORGANISATIONUNITSCRIPTS_FILENAME="OrganisationUnitScripts_1_1.js";

		#endregion
	}
}
