﻿using System;
using Oritax.TaxSimp.CalculationInterface;

namespace Oritax.TaxSimp.CM.OrganizationUnit
{
    /// <summary>
    /// Class that is used to specify specific characteristics for an informational collection of ChartOfAccounts SM instances. 
    /// </summary>
    public class OrganisationUnitCollectionSpecifier : CollectionSpecifier
    {
        public OrganisationUnitCollectionSpecifier()
            : base(new Guid(OrganizationUnitInstall.ASSEMBLY_ID))
        {
        }
    }

    public class COAConsumerCollectionSpecifier : CollectionSpecifier
    {
        private Guid supplierId;
        public Guid SupplierID { get { return supplierId; } }

        public COAConsumerCollectionSpecifier(Guid supplierID)
            : base(new Guid(OrganizationUnitInstall.ASSEMBLY_ID))
        {
            this.supplierId = supplierID;
        }
    }

    public class CLCOAMigrationCollectionSpecifier : CollectionSpecifier
    {
        private Guid instanceId;
        public Guid InstanceId { get { return this.instanceId; } }

        public CLCOAMigrationCollectionSpecifier(Guid instanceId)
            : base(new Guid(OrganizationUnitInstall.ASSEMBLY_ID))
        {
            this.instanceId = instanceId;
        }
    }
}
