﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Commands;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;
using System.Xml.Linq;

namespace Oritax.TaxSimp.CM.OrganizationUnit
{
    public partial class OrganizationUnitCM : IXmlData, ITagableEntity, ISecurityService, IIdentityCM
    {
        public Dictionary<string, Guid> FileMaps;
        public List<DefaultFormsFile> DefaultForms;

        public override string GetDataStream(int type, string data)
        {
            return base.GetDataStream(type, data);
        }

        string IOrganizationUnit.OnUpdateClient(int type, string data)
        {
            var command = this.GetCommandFactory()
                            .GetCommand<OrganizationUnitCM>(this, type);
            return command == null ? string.Empty : command.DoAction(data);
        }

        public override string OnUpdateDataStream(int type, string data)
        {
            return (this as IOrganizationUnit).OnUpdateClient(type, data);
        }

        public virtual string GetXmlData()
        {
            return string.Empty;
        }

        public virtual void DoTagged(EntityTag tag,Guid modelID)
        {
        }

        public virtual void DoTaggedDoc(Aspose.Words.Document document, Guid modelID)
        {
            Aspose.Words.License license = new Aspose.Words.License();
            license.SetLicense("Aspose.Words.lic");
        }
        
        public virtual List<IDictionary<string,string>> DoPartialTag(EntityTag tag)
        {
            return (new List<IDictionary<string,string>>());
        }

        void ISecurityService.AddUser(UserEntity user)
        {
            try
            {
                if(user.IsWebUser)
                {
                    Broker.SetStart();
                }
                else
                {
                    base.Broker.SetWriteStart();    
                }
                
                PartyDS ds = new PartyDS();
                base.OnGetData(ds);
                DataRow row = ds.Tables[PartyDS.INCLUDEDPARTY_TABLE].NewRow();
                row[PartyDS.PARTYNAME_FIELD] = user.CurrentUserName;
                row[PartyDS.PARTYCID_FIELD] = user.CID;
                ds.Tables[PartyDS.INCLUDEDPARTY_TABLE].Rows.Add(row);
                this.SetData(ds);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
            finally
            {

            }
        }

        #region ToXElement
        public virtual XElement ToXElement()
        {
            return null;
        }
        #endregion

        protected string AddEntitySecurity(string user)
        {
            (this as ISecurityService).AddUser(user.ToData<UserEntity>());
            return string.Empty;
        }

        public Guid Clid
        {
            get { return CLID; }
            set { CLID = value; }
        }

        public Guid Csid
        {
            get { return CSID; }
            set { Csid = value; }
        }

    }
}
