﻿using System;
using System.Collections.Generic;
using Oritax.TaxSimp.CM;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.CalculationInterface;

namespace Oritax.TaxSimp.Extensions
{
   public static class  EntityTagExtensions
    {

        public static EntityTag SetAppenderExt(this EntityTag tag, string appender,string newAppender)
        {
            tag=(string.IsNullOrEmpty(appender)) ? 
                tag.SetAppender(newAppender) :
                tag.SetAppender(appender + (string.IsNullOrEmpty(newAppender)? "" : ("." + newAppender)));

            return tag;
        }


    }
}
