﻿using System;
using System.Collections.Generic;
using Oritax.TaxSimp.CM;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.CalculationInterface;

namespace Oritax.TaxSimp.Extensions
{
    public static class IdentityCMExtensions
    {
        public static void ListTagged(this IEnumerable<IIdentityCM> list, ICMBroker broker, EntityTag tag,Guid ModelID)
        {
            int count = 1;
            ITagableEntity entity = null;
            IDictionary<string, string> items = null;
            string appender = tag.Appender;
            foreach (var each in list)
            {
                try
                {
                    if (each is Oritax.TaxSimp.Data.IdentityCMDetail)
                    {
                        (each as IdentityCMDetail).DoTaggedExtension<IdentityCMDetail>(tag.SetAppender(appender + count));

                    }
                    else if(each is Oritax.TaxSimp.Common.IdentityCMDetail)
                    {
                        (each as Oritax.TaxSimp.Common.IdentityCMDetail).DoTaggedExtension<Oritax.TaxSimp.Common.IdentityCMDetail>(tag.SetAppender(appender + count));
                    }


                    entity = broker.GetCMImplementation(each.Clid, each.Csid) as ITagableEntity;
                    entity.DoTagged(tag.SetAppender(appender + count), ModelID);
                    count++;
                }
                finally
                {
                    if (entity != null) broker.ReleaseBrokerManagedComponent(entity as IBrokerManagedComponent);
                    entity = null;
                }
            }
        }

        public static OrganizationUnitCM ToOrganizationUnit(this IIdentityCM IdentiyCm, ICMBroker broker)
        {
            OrganizationUnitCM parentOrganizationUnitCM = null;
            ICalculationModule parentCalcModule = broker.GetCMImplementation(IdentiyCm.Clid, IdentiyCm.Csid);

            if(parentCalcModule !=null)
                parentOrganizationUnitCM = (OrganizationUnitCM) parentCalcModule;

            return parentOrganizationUnitCM;
         }

        public static List<IDictionary<string, string>> IIdentityCMPartialTags(this IIdentityCM IDcm, ICMBroker broker, EntityTag tag)
        {
            List<IDictionary<string, string>> listtemp = new List<IDictionary<string, string>>();
            ITagableEntity entity = null;
            IDictionary<string, string> items = null;
            string appender = tag.Appender;


            try
            {
                entity = broker.GetCMImplementation(IDcm.Clid, IDcm.Csid) as ITagableEntity;
                if (entity != null)
                {
                    List<IDictionary<string, string>> entityDoPartialTag = entity.DoPartialTag(tag.SetAppender(appender));

                    foreach (var each_list in entityDoPartialTag)
                    {

                        listtemp.Add(each_list);

                    }


                }
            }
            finally
            {
                if (entity != null) broker.ReleaseBrokerManagedComponent(entity as IBrokerManagedComponent);
                entity = null;
            }

             return listtemp;
        }

        public static List<IDictionary<string, string>> ListPartialTags(this IEnumerable<IIdentityCM> list, ICMBroker broker, EntityTag tag)
        {
             List<IDictionary<string,string>> listtemp=new List<IDictionary<string, string>>();
            
            ITagableEntity entity = null;
            IDictionary<string, string> items = null;
            string appender = tag.Appender;
            foreach (var each in list)
            {

              
                try
                {
                    entity = broker.GetCMImplementation(each.Clid, each.Csid) as ITagableEntity;
                    if (entity != null)
                    {
                        List<IDictionary<string, string>> entityDoPartialTag = entity.DoPartialTag(tag.SetAppender(appender));

                        foreach (var each_list in entityDoPartialTag)
                        {

                            listtemp.Add(each_list);

                        }

            
                    }
                }
                finally
                {
                    if (entity != null) broker.ReleaseBrokerManagedComponent(entity as IBrokerManagedComponent);
                    entity = null;
                }
            }

            return listtemp;
        }

    }
}
