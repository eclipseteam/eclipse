/*
 * Functions to manage if the business structure
 * data has changed
 * Updated by: Paul Veitch
 * Last Updated: 18/06/03
*/
bolDataChanged = false;
			
function DataChanged()
{
	bolDataChanged = true;
}
			
function HasDataChanged()
{
	return bolDataChanged;
}

// end of functions to manage business structure data changes

/*
 * Functions to validate ABNs and TFNs
 * Updated by: Paul Veitch
 * Last updated: 23/06/03
*/
function ValidateABN( source, arguments )
{
	if ( source == null )
	{
		arguments.IsValid = false;
		return;
	}
		
	strABN = arguments.Value;
	intABNLength = strABN.length;
	intAddition = 0;
	
	// check that the ABN is 11 characters long
	if ( intABNLength != 11 )
	{
		arguments.IsValid = false;
		return;
	}
		
	// loop through each element in the string and validate it	
	for ( intElementPosition = 0; intElementPosition < intABNLength; intElementPosition++ )
	{
		strElement = strABN.charAt( intElementPosition );
		
		if ( isNaN( strElement ) )
		{
			arguments.IsValid = false;
			return;
		}
		
		intElement = parseInt( strElement );
		
		switch ( intElementPosition )
		{
			case 0:
				intAddition += ( ( intElement -1 ) * 10 );
			break;
			
			case 1:
				intAddition += ( intElement * 1 );
			break;
			
			case 2:
				intAddition += ( intElement * 3 );
			break;
			
			case 3:
				intAddition += ( intElement * 5 );
			break;
			
			case 4:
				intAddition += ( intElement * 7 );
			break;
			
			case 5:
				intAddition += ( intElement * 9 );
			break;
			
			case 6:
				intAddition += ( intElement * 11 );
			break;
			
			case 7:
				intAddition += ( intElement * 13 );
			break;
			
			case 8:
				intAddition += ( intElement * 15 );
			break;

			case 9:
				intAddition += ( intElement * 17 );
			break;

			case 10:
				intAddition += ( intElement * 19 );
			break;
		}
	}
	
	if ( ( intAddition % 89 ) == 0 )
	{
		arguments.IsValid = true;
		return;
	}
	else
	{
		arguments.IsValid = false;
		return;
	}
}


function ValidateTFN( source, arguments )
{
	if ( source == null )
	{
		arguments.IsValid = false;
		return;
	}
		
	strTFN = arguments.Value;
	intTFNLength = strTFN.length;
	intAddition = 0;
	
	// check that the TFN is 8 or 9 characters long
	if ( intTFNLength < 8 || intTFNLength > 9 )
	{
		arguments.IsValid = false;
		return;
	}
		
	// loop through each element in the string and validate it	
	for ( intElementPosition = 0; intElementPosition < intTFNLength; intElementPosition++ )
	{
		strElement = strTFN.charAt( intElementPosition );
		
		if ( isNaN( strElement ) )
		{
			arguments.IsValid = false;
			return;
		}
		
		intElement = parseInt( strElement );
		
		switch ( intElementPosition )
		{
			case 0:
				intAddition += ( intElement * 10 );
			break;
			
			case 1:
				intAddition += ( intElement * 7 );
			break;
			
			case 2:
				intAddition += ( intElement * 8 );
			break;
			
			case 3:
				intAddition += ( intElement * 4 );
			break;
			
			case 4:
				intAddition += ( intElement * 6 );
			break;
			
			case 5:
				intAddition += ( intElement * 3 );
			break;
			
			case 6:
				intAddition += ( intElement * 5 );
			break;
			
			case 7:
				if ( intTFNLength == 8 )
					intAddition += intElement;
				else if ( intTFNLength == 9 )
					intAddition += ( intElement * 2 );
				else
					arguments.IsValid = false;
			break;
			
			case 8:
				intAddition += intElement;
			break;
		}
	}
	
	if ( ( intAddition % 11 ) == 0 )
	{
		arguments.IsValid = true;
		return;
	}
	else
	{
		arguments.IsValid = false;
		return;
	}
}

/*
 * Functions to validate ACN
 * Updated by: Paul Bailey
 * Last updated: 01/09/03
*/
function ValidateACN_ARBN( source, arguments )
{
	if ( source == null )
	{
		arguments.IsValid = false;
		return;
	}
		
	strACN = arguments.Value;
	intACNLength = strACN.length;
	intAddition = 0;
	
	// check that the ACN is 9 characters long
	if ( intACNLength != 9 )
	{
		arguments.IsValid = false;
		return;
	}
		
	// loop through each element in the string and validate it	
	for ( intElementPosition = 0; intElementPosition < intACNLength; intElementPosition++ )
	{
		strElement = strACN.charAt( intElementPosition );
		
		if ( isNaN( strElement ) )
		{
			arguments.IsValid = false;
			return;
		}
		
		intElement = parseInt( strElement );
		
		switch ( intElementPosition )
		{
			case 0:
				intAddition += ( intElement * 8 );
			break;
			
			case 1:
				intAddition += ( intElement * 7 );
			break;
			
			case 2:
				intAddition += ( intElement * 6 );
			break;
			
			case 3:
				intAddition += ( intElement * 5 );
			break;
			
			case 4:
				intAddition += ( intElement * 4 );
			break;
			
			case 5:
				intAddition += ( intElement * 3 );
			break;
			
			case 6:
				intAddition += ( intElement * 2 );
			break;
			
			case 7:
				intAddition += ( intElement * 1 );
			break;
			
		}
	}
	
	intModTen = intAddition % 10;
	intComplement = 10 - intModTen;
	if(intComplement == 10)
		 intComplement = 0;
		 
		 
	strElement = strACN.charAt( 8 );
		
	if ( isNaN( strElement ) )
	{
		arguments.IsValid = false;
		return;
	}
		
	intElement = parseInt( strElement );
	
	if (intElement == intComplement)
	{
		arguments.IsValid = true;
		return;
	}
	else
	{
		arguments.IsValid = false;
		return;
	}
}