﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.Data
{
    [Serializable]
    public class ModelWithFiles
    {
        public List<DefaultFormsFile> files { get; set; }
        public Guid ModelID { get; set; }
        public string ModelName { get; set; }
        public ServiceTypes ServiceType { get; set; }
    }

    [Serializable]
    public class DefaultFormsFile
    {
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public Guid MapID { get; set; }
        public FileProcessCommadnType Type { get; set; }

    }


    public enum FileProcessCommadnType
    {
        NormarlTagged=1,
        PartialTagged=2,
        DocFile=3
    }
}
