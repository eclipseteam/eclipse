﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.Data
{
    [Serializable]
    public class ServiceType
    {
        public bool DO_IT_YOURSELF { get; set; }
        public bool DO_IT_WITH_ME { get; set; }
        public bool DO_IT_FOR_ME { get; set; }
    }
}
