﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oritax.TaxSimp.Data
{
    [Serializable]
    public class ExemptionCategory
    {
        public bool Category1 { get; set; }
        public bool Category2 { get; set; }
        public bool Category3 { get; set; }
    }
}
