using System;
using System.Data;
using System.Data.SqlClient;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.CM.OrganizationUnit
{
	[Serializable]
	public class OrganizationUnitDS : BusinessStructureBaseDS
	{
		public const String ORGUNIT_TABLE					= "ORGANISATIONUNITCM";
		public const String ORGUNITCURRPERIOD_FIELD			= "CURRENTPERIOD";
		public const String ORGUNITLEGALNAME_FIELD			= "LEGALNAME";
		public const String ORGUNITIDENT_FIELD				= "IDENTIFIER";

		public const String ORGUNITPERIODS_TABLE			= "ORGANISATIONUNITPERIODS";
		public const String ORGUNITPERIODKEY_FIELD			= "KEY";
		public const String ORGUNITPERIODSTARTDATE_FIELD	= "STARTDATE";
		public const String ORGUNITPERIODENDDATE_FIELD		= "ENDDATE";
		public const String ORGUNITPERIODTYPE_FIELD			= "TYPE";

        public const string ORGANISATIONUNIT_TABLE = "ORGANISATIONUNIT";
        public const string ORGUNIT_ID_FIELD = "ID";
        public const string ORGUNIT_ISINVESTMENTCLIENT_FIELD = "ISINVESTMENTCLIENT";
        public const string ORGUNIT_ISEXPORT_FIELD = "ISEXPORT";
        public const string ORGUNIT_ISEXPORTEDSS_FIELD = "ISEXPORTEDSS";
        public const string ORGUNIT_APPLICATIONDATE_FIELD = "APPLICATIONDATE";
        public const string ORGUNIT_APPLICATIONID_FIELD = "APPLICATIONID";
        public const string ORGUNIT_CREATIONDATE_FIELD = "CREATIONDATE";
        public const string ORGUNIT_UPDATEDATE_FIELD = "UPDATEDATE";
        public const string ORGUNIT_ISCLASSSUPER_FIELD = "ISCLASSSUPER";
        public const string ORGUNIT_ISBGL_FIELD = "ISBGL";
        public const string ORGUNIT_ISDESKTOPSUPER_FIELD = "ISDESKTOPSUPER";
        public const string ORGUNIT_BGLACCOUNTCODE_FIELD = "BGLACCOUNTCODE";
        public const string ORGUNIT_SUPERMEMBERID_FIELD = "SUPERMEMBERID";
        public const string ORGUNIT_SUPERCLIENTID_FIELD = "SUPERCLIENTID";

        public OrganizationUnitDS()
            : base()
        {
            DataTable dt;

            dt = new DataTable(ORGUNIT_TABLE);
            dt.Columns.Add(ORGUNITCURRPERIOD_FIELD, typeof(System.Guid));
            dt.Columns.Add(ORGUNITLEGALNAME_FIELD, typeof(System.String));
            dt.Columns.Add(ORGUNITIDENT_FIELD, typeof(System.String));
            this.Tables.Add(dt);
            BrokerManagedComponentDS.AddVersionStamp(dt, typeof(OrganizationUnitDS));

            dt = new DataTable(ORGUNITPERIODS_TABLE);
            dt.Columns.Add(ORGUNITPERIODKEY_FIELD, typeof(System.Guid));
            dt.Columns.Add(ORGUNITPERIODSTARTDATE_FIELD, typeof(System.DateTime));
            dt.Columns.Add(ORGUNITPERIODENDDATE_FIELD, typeof(System.DateTime));
            dt.Columns.Add(ORGUNITPERIODTYPE_FIELD, typeof(System.String));
            this.Tables.Add(dt);
            BrokerManagedComponentDS.AddVersionStamp(dt, typeof(OrganizationUnitDS));

            DataTable dtORGUNI = new DataTable(ORGANISATIONUNIT_TABLE);
            dtORGUNI.Columns.Add(ORGUNIT_ID_FIELD, typeof(System.Guid));
            dtORGUNI.Columns.Add(ORGUNIT_ISINVESTMENTCLIENT_FIELD, typeof(System.Boolean));
            dtORGUNI.Columns.Add(ORGUNIT_ISEXPORT_FIELD, typeof(Boolean));
            dtORGUNI.Columns.Add(ORGUNIT_ISEXPORTEDSS_FIELD, typeof(Boolean));
            dtORGUNI.Columns.Add(ORGUNIT_APPLICATIONDATE_FIELD, typeof(DateTime));
            dtORGUNI.Columns.Add(ORGUNIT_APPLICATIONID_FIELD, typeof(System.String));
            dtORGUNI.Columns.Add(ORGUNIT_CREATIONDATE_FIELD, typeof(System.DateTime));
            dtORGUNI.Columns.Add(ORGUNIT_UPDATEDATE_FIELD, typeof(DateTime));
            dtORGUNI.Columns.Add(ORGUNIT_ISCLASSSUPER_FIELD, typeof(System.Boolean));
            dtORGUNI.Columns.Add(ORGUNIT_ISBGL_FIELD, typeof(System.Boolean));
            dtORGUNI.Columns.Add(ORGUNIT_ISDESKTOPSUPER_FIELD, typeof(Boolean));
            dtORGUNI.Columns.Add(ORGUNIT_BGLACCOUNTCODE_FIELD, typeof(string));
            dtORGUNI.Columns.Add(ORGUNIT_SUPERMEMBERID_FIELD, typeof(String));
            dtORGUNI.Columns.Add(ORGUNIT_SUPERCLIENTID_FIELD, typeof(string));
            dtORGUNI.Columns.Add(BrokerManagedComponentDS.NAME_FIELD, typeof(string));
            dtORGUNI.PrimaryKey = new DataColumn[] { dtORGUNI.Columns[ORGUNIT_ID_FIELD] };
        }
		public static new string GetDataSetVersion(DataSet data)
		{
			return GetDataSetVersion(data, "ORGANISATIONUNITCM").ToString();
		}
	}
}
