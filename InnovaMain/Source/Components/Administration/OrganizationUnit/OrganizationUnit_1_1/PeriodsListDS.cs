#region CHANGE_HISTORY
/*
 $Header: /2002/3. Implementation/Construction/BusinessStructureCM/OrganizationUnit/OrganizationUnit-1-1-1/PeriodsListDS.cs 1     18/12/02 4 $
 $History: PeriodsListDS.cs $
 * 
 * *****************  Version 1  *****************
 * User: Sshrimpton   Date: 18/12/02   Time: 4:55p
 * Created in $/2002/3. Implementation/Construction/BusinessStructureCM/OrganizationUnit/OrganizationUnit-1-1-1
 * 
 * *****************  Version 2  *****************
 * User: Secampbell   Date: 7/11/02    Time: 11:59a
 * Updated in $/2002/3. Implementation/Elaboration4/CM/Period
 * Added Change History Header
*/
#endregion

using System;
using System.Data;

namespace Oritax.TaxSimp.CM.OrganizationUnit
{
	/// <summary>
	/// PeriodsDS is a DataSet that contains data for listing periods within an entity.
	/// </summary>
	[Serializable] 
	public class PeriodsListDS : DataSet
	{
		public const String PERIODS_TABLE			= "PERIODS_TABLE";
		public const String PERIODNAME_FIELD		= "PERIODNAME_FIELD";
		public const String PERIODCLID_FIELD		= "PERIODCLID_FIELD";
		public const String PERIODCSID_FIELD		= "PERIODCSID_FIELD";
		public const String PERIODCIID_FIELD		= "PERIODCIID_FIELD";

		private string ownerName;
		private string breadCrumb;
		string			cSID;
		string			cIID;
		string			cLID;

		public PeriodsListDS()
		{
			DataTable table;

			table = new DataTable(PERIODS_TABLE);
			table.Columns.Add(PERIODNAME_FIELD, typeof(System.String));
			table.Columns.Add(PERIODCLID_FIELD, typeof(System.String));
			table.Columns.Add(PERIODCSID_FIELD, typeof(System.String));
			table.Columns.Add(PERIODCIID_FIELD, typeof(System.String));

			this.Tables.Add(table);

			ownerName="";
			breadCrumb="";
		}

		public string OwnerName{get{return ownerName;}set{ownerName=value;}}
		public string BreadCrumb{get{return breadCrumb;}set{breadCrumb=value;}}
		public string CSID {get{return cSID;}set{cSID=value;}}
		public string CIID	{get{return cIID;}set{cIID=value;}}
		public string CLID	{get{return cLID;}set{cLID=value;}}
	}
}

