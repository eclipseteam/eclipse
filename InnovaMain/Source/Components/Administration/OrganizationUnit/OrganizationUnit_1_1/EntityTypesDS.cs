#region CHANGE_HISTORY
/*
 $Header: /2002/3. Implementation/Construction/BusinessStructureCM/OrganizationUnit/OrganizationUnit-1-1-1/EntityTypesDS.cs 1     29/01/03 2 $
 $History: EntityTypesDS.cs $
 * 
 * *****************  Version 1  *****************
 * User: Sshrimpton   Date: 29/01/03   Time: 2:17p
 * Created in $/2002/3. Implementation/Construction/BusinessStructureCM/OrganizationUnit/OrganizationUnit-1-1-1
 * 
 * *****************  Version 1  *****************
 * User: Sshrimpton   Date: 18/12/02   Time: 4:55p
 * Created in $/2002/3. Implementation/Construction/BusinessStructureCM/Entity/Entity-1-1-1
 * 
 * *****************  Version 3  *****************
 * User: Paubailey    Date: 25/11/02   Time: 5:14p
 * Updated in $/2002/3. Implementation/Elaboration4/CM/Entity
 * See CQ issue 677.
 * 
 * *****************  Version 2  *****************
 * User: Pveitch      Date: 7/11/02    Time: 3:07p
 * Updated in $/2002/3. Implementation/Elaboration4/CM/Entity
 * Header Information Added
*/
#endregion
using System;
using System.Data;

namespace Oritax.TaxSimp.CM.OrganizationUnit
{
	/// <summary>
	/// DSCalculationModule is a DataSet that contains all of the persistent data for a CM instance.
	/// </summary>
	[Serializable] 
	public class EntityTypesDS : DataSet
	{
		public const String ENTITYTYPES_TABLE				= "ENTITYTYPES";
		public const String BUSINESSENTITYTYPENAME_FIELD	= "BUSINESSENTITYTYPENAME";
		public const String BUSINESSENTITYTYPE_FIELD	= "BUSINESSENTITYTYPE";
		public const String BUSINESSENTITYTYPEID_FIELD		= "BUSINESSENTITYTYPEID";
		public const string BUSINESSENTITYWORKPAPER_FIELD = "BUSINESSENTITYWORKPAPER";

		public Guid OrganizationCLID = Guid.Empty;

		public EntityTypesDS()
		{
			DataTable table;

			table = new DataTable(ENTITYTYPES_TABLE);
			table.Columns.Add(BUSINESSENTITYTYPENAME_FIELD, typeof(System.String));
			table.Columns.Add(BUSINESSENTITYTYPE_FIELD, typeof(System.String));
			table.Columns.Add(BUSINESSENTITYTYPEID_FIELD, typeof(Guid));
			table.Columns.Add( BUSINESSENTITYWORKPAPER_FIELD, typeof( string ) );

			this.Tables.Add(table);
		}
	}
}

