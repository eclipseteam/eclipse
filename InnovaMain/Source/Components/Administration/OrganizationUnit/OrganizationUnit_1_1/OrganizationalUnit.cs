using System;
using System.Data;
using System.Collections;
using System.Reflection;
using System.Runtime.Serialization;
using System.Security.Permissions;

using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.DSCalculation;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.CM.Period;
using Oritax.TaxSimp.BusinessStructureUtilities;
using Oritax.TaxSimp.Exceptions;
using Oritax.TaxSimp.CM.MultiPeriod;
using Oritax.TaxSimp.CM.Entity;
using Oritax.TaxSimp.CM.Group;
using Oritax.TaxSimp.SAPBase;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.TaxSimpLicence;
using Oritax.TaxSimp.TaxTopicCommon;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Xml;



namespace Oritax.TaxSimp.CM.OrganizationUnit
{
    /// <summary>
    /// Class for use in storing the details of member periods in an OrganisationUnit
    /// </summary>
    [Serializable]
    public class PeriodMember : ISerializable
    {
        //USING CUSTOM SERIALIZATION.
        //Still mark the fields as [NonSerialized] for your own infomation but this is ignored now by .NET
        //You must add your own serialzation code for each instance
        //See GetObjectData & Special Constructor

        public DateTime dtStartDate;
        public DateTime dtEndDate;
        public string strTypeName;

        public PeriodMember(DateTime StartDate, DateTime EndDate, string TypeName)
        {
            this.dtStartDate = StartDate;
            this.dtEndDate = EndDate;
            this.strTypeName = TypeName;

        }

        protected PeriodMember(SerializationInfo si, StreamingContext context)
        {
            dtStartDate = Serialize.GetSerializedDateTime(si, "pm_dtStartDate");
            dtEndDate = Serialize.GetSerializedDateTime(si, "pm_dtEndDate");
            strTypeName = Serialize.GetSerializedString(si, "pm_strTypeName");
        }

        //Only allow the .NET Serialization core code to call this function
        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        public virtual void GetObjectData(SerializationInfo si, StreamingContext context)
        {
            Serialize.AddSerializedValue(si, "pm_dtStartDate", dtStartDate);
            Serialize.AddSerializedValue(si, "pm_dtEndDate", dtEndDate);
            Serialize.AddSerializedValue(si, "pm_strTypeName", strTypeName);
        }
    }
    /// <summary>
    /// Summary description for OrganizationalUnit.
    /// </summary>
    [Serializable]
    public partial class OrganizationUnitCM : BusinessStructureBase, IOrganizationUnit
    {
        //USING CUSTOM SERIALIZATION.
        //Still mark the fields as [NonSerialized] for your own infomation but this is ignored now by .NET
        //You must add your own serialzation code for each instance
        //See GetObjectData & Special Constructor
        #region STATEFUL VARIABLES
        private Guid m_objCurrentPeriod = Guid.Empty;
        private SerializableHashtable m_objPeriodsHT;
        private string m_strLegalName;
        private string m_strIdentifier;
        #endregion
        private bool isInvestableClient = false;
        private string organizationstatus = string.Empty;
        private string clientid = string.Empty;


        private const string LockedStatusTimeIntensiveTaskKey = "LockedStatusTimeIntensiveTask";
        private const string DeleteScenarioConfirmKey = "DeleteScenarioConfirmTimeIntensiveTask";
        [OptionalField]
        protected TaxStatusType m_objTaxStatus = TaxStatusType.Default;
        #region CONSTRUCTORS
        /// <summary>
        /// 
        /// </summary>
        public OrganizationUnitCM()
            : base()
        {
            m_objPeriodsHT = new SerializableHashtable();
            this.name = String.Empty;
            this.LegalName = String.Empty;
            this.Identifier = String.Empty;
            _FileMaps = new Dictionary<string, Guid>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="si"></param>
        /// <param name="context"></param>
        /// <exception cref="System.ArgumentNullException">si cannot be null</exception>
        protected OrganizationUnitCM(SerializationInfo si, StreamingContext context)
            : base(si, context)
        {
            if (si == null)
                throw new ArgumentNullException("si");

            m_objCurrentPeriod = Serialize.GetSerializedGuid(si, "oucm_m_objCurrentPeriod");
            m_objPeriodsHT = Serialize.GetSerializedHashtable(si, "oucm_m_objPeriodsHT");
            m_strLegalName = Serialize.GetSerializedString(si, "oucm_m_strLegalName");
            m_strIdentifier = Serialize.GetSerializedString(si, "oucm_m_strIdentifier");
            m_objTaxStatus = (TaxStatusType)Serialize.GetSerializedValue(si, "oucm_m_objTaxStatus", typeof(TaxStatusType), TaxStatusType.Default);
            isInvestableClient = Serialize.GetSerializedBool(si, "Oritax.TaxSimp.CM.OrganizationUnit.isInvestableClient");
            organizationstatus = Serialize.GetSerializedString(si, "Oritax.TaxSimp.CM.OrganizationUnit.organizationstatus");
            clientid = Serialize.GetSerializedString(si, "Oritax.TaxSimp.CM.OrganizationUnit.clientid");
            _FileMaps = Serialize.GetValue<Dictionary<string, Guid>>(si, "Oritax.TaxSimp.CM.OrganizationUnit.FileMaps");
        }

        //Only allow the .NET Serialization core code to call this function
        /// <summary>
        /// 
        /// </summary>
        /// <param name="si"></param>
        /// <param name="context"></param>
        /// <exception cref="System.ArgumentNullException">si cannot be null</exception>
        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo si, StreamingContext context)
        {
            if (si == null)
                throw new ArgumentNullException("si");

            base.GetObjectData(si, context);
            Serialize.AddSerializedValue(si, "oucm_m_objCurrentPeriod", m_objCurrentPeriod);
            Serialize.AddSerializedValue(si, "oucm_m_objPeriodsHT", m_objPeriodsHT);
            Serialize.AddSerializedValue(si, "oucm_m_strLegalName", m_strLegalName);
            Serialize.AddSerializedValue(si, "oucm_m_strIdentifier", m_strIdentifier);
            Serialize.AddSerializedValue(si, "oucm_m_objTaxStatus", m_objTaxStatus);
            Serialize.AddSerializedValue(si, "Oritax.TaxSimp.CM.OrganizationUnit.isInvestableClient", isInvestableClient);
            Serialize.AddSerializedValue(si, "Oritax.TaxSimp.CM.OrganizationUnit.organizationstatus", organizationstatus);
            Serialize.AddSerializedValue(si, "Oritax.TaxSimp.CM.OrganizationUnit.clientid", clientid);
            Serialize.AddSerializedValue(si, "Oritax.TaxSimp.CM.OrganizationUnit.FileMaps", _FileMaps);
        }

        #endregion
        #region PROPERTIES

        public bool IsInvestableClient
        {
            get { return isInvestableClient; }
            set { isInvestableClient = value; }
        }

        public string OrganizationStatus
        {
            get { return organizationstatus; }
            set { organizationstatus = value; }
        }

        public string ClientId
        {
            get { return clientid; }
            set { clientid = value; }
        }

        public virtual ISubstitutedAccountingPeriods SAPs { get; set; }
        //{
        //    get;
        //}

        /// <summary>
        /// The full legal name of the organisation, used on ATO forms
        /// </summary>
        public string LegalName
        {
            get
            {
                return this.m_strLegalName;
            }
            set
            {
                this.m_strLegalName = value;
            }
        }

        /// <summary>
        /// The identifier for the organisation, used by GL
        /// </summary>
        public string Identifier
        {
            get
            {
                return this.m_strIdentifier;
            }
            set
            {
                this.m_strIdentifier = value;
            }
        }

        public Guid CurrentPeriod
        {
            get
            {
                return this.m_objCurrentPeriod;
            }
            set
            {
                this.m_objCurrentPeriod = value;
                //PB -  Need to recalculate the token here as this property is used 
                // to modify the state of the CM.
                this.CalculateToken(true);
            }
        }

        public SerializableHashtable MemberPeriods
        {
            get { return m_objPeriodsHT; }
        }

        public IEnumerable PeriodScenarios
        {
            get
            {
                ArrayList periodScenarios = new ArrayList();

                ILogicalModule thisLM = this.Broker.GetLogicalCM(this.CLID);

                foreach (Guid gdCLID in thisLM.ChildCLIDs)
                {
                    ILogicalModule logicalCM = this.Broker.GetLogicalCM(gdCLID);

                    foreach (DictionaryEntry dictionaryEntry in logicalCM.Scenarios)
                    {
                        CMScenario cMScenario = (CMScenario)dictionaryEntry.Value;
                        periodScenarios.Add(cMScenario);
                    }

                    this.Broker.ReleaseBrokerManagedComponent(logicalCM);
                }

                this.Broker.ReleaseBrokerManagedComponent(thisLM);

                return periodScenarios;
            }
        }

        public TaxStatusType TaxStatus
        {
            get
            {
                return this.m_objTaxStatus;
            }
            set
            {
                this.m_objTaxStatus = value;
            }
        }

        public virtual string TaxStatusName
        {
            get
            {
                return this.TaxStatus.ToString();
            }
        }
        #endregion
        #region OTHER MEMBER FUNCTIONS
        /// <summary>
        /// 
        /// </summary>
        /// <param name="gdCLID"></param>
        /// <param name="dtStartDate"></param>
        /// <param name="dtEndDate"></param>
        /// <param name="strTypeName"></param>
        /// <exception cref="System.ArgumentException">gdCLID cannot be an empty guid</exception>
        /// <exception cref="System.ArgumentNullException">strTypeName cannot be null or an empty string</exception>
        public void AddNewMemberPeriodDetails(Guid gdCLID, DateTime dtStartDate, DateTime dtEndDate, string strTypeName)
        {
            if (gdCLID == Guid.Empty)
                throw new ArgumentException("Parameter cannot be an empty guid", "gdCLID");

            if (string.IsNullOrEmpty(strTypeName))
                throw new ArgumentNullException("strTypeName");

            this.m_objPeriodsHT.Add(gdCLID, new PeriodMember(dtStartDate, dtEndDate, strTypeName));
            this.CalculateToken(true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gdCLID"></param>
        /// <param name="dtStartDate"></param>
        /// <param name="dtEndDate"></param>
        /// <exception cref="System.ArgumentException">gdCLID cannot be an empty guid</exception>
        public void EditMemberPeriodDetails(Guid gdCLID, DateTime dtStartDate, DateTime dtEndDate)
        {
            if (gdCLID == Guid.Empty)
                throw new ArgumentException("Parameter cannot be an empty guid", "gdCLID");

            PeriodMember objPeriodMember;
            if (m_objPeriodsHT.Contains(gdCLID))
            {
                objPeriodMember = (PeriodMember)m_objPeriodsHT[gdCLID];
                objPeriodMember.dtStartDate = dtStartDate;
                objPeriodMember.dtEndDate = dtEndDate;
                this.CalculateToken(true);
            }
            else
                throw new Exception("EntityCM -> Member period does not exist in collection");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gdCLID"></param>
        /// <exception cref="System.ArgumentException">gdCLID cannot be an empty guid</exception>
        public void RemoveMemberPeriodDetails(Guid gdCLID)
        {
            if (gdCLID == Guid.Empty)
                throw new ArgumentException("Parameter cannot be an empty guid", "gdCLID");

            this.m_objPeriodsHT.Remove(gdCLID);
        }

        /// <summary>
        /// Returns a list of all child periods for the current Organisation Unit
        /// </summary>
        /// <returns></returns>
        public IPeriod[] GetChildPeriods()
        {
            ILogicalModule organisationUnitLM = this.GetLogicalModule();

            ArrayList childPeriodsList = new ArrayList();

            foreach (CMScenario cmScenario in organisationUnitLM.GetChildCMs(this.CSID))
            {
                ICalculationModule periodCM = this.Broker.GetCMImplementation(cmScenario.CLID, cmScenario.CSID);

                if (periodCM != null)
                    childPeriodsList.Add(periodCM);
            }

            this.Broker.ReleaseBrokerManagedComponent(organisationUnitLM);

            return (IPeriod[])childPeriodsList.ToArray(typeof(IPeriod));
        }

        //Return the CLID of any member periods that match the start and end dates passed in
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dtStartDate"></param>
        /// <param name="dtEndDate"></param>
        public Guid GetCLIDOfMemberPeriod(DateTime dtStartDate, DateTime dtEndDate)
        {
            IDictionaryEnumerator en = m_objPeriodsHT.GetEnumerator();
            while (en.MoveNext())
            {
                PeriodMember objPeriodMember = (PeriodMember)en.Value;
                if (objPeriodMember.dtStartDate == dtStartDate
                    && objPeriodMember.dtEndDate == dtEndDate)
                {
                    Guid periodCLID = new Guid(en.Key.ToString());
                    ICalculationModule periodCM = this.Broker.GetCMImplementation(periodCLID, this.CSID);

                    if (periodCM != null)
                        return periodCLID;
                }
            }
            return Guid.Empty;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strOrganisationName"></param>
        /// <exception cref="System.ArgumentNullException">strOrganisationName cannot be null or an empty string</exception>
        public bool IsUniqueOrganisationName(string strOrganisationName)
        {
            if (string.IsNullOrEmpty(strOrganisationName))
                throw new ArgumentNullException("strOrganisationName");

            ICalculationModule objOrganisation = this.Owner.GetWellKnownCM(WellKnownCM.Organization);

            if (objOrganisation != null)
            {
                DataRowCollection logicalModuleRows = (DataRowCollection)this.Broker.GetLCMsByParent(objOrganisation.CLID);

                foreach (DataRow logicalModuleRow in logicalModuleRows)
                {
                    Guid objOrganisationUnitCLID = (Guid)logicalModuleRow[DSLogicalModulesInfo.LCM_CLID_FIELD];
                    ILogicalModule objLogicalOrganisationUnit = this.Broker.GetLogicalCM(objOrganisationUnitCLID);
                    ICalculationModule objOUCM = objLogicalOrganisationUnit[objLogicalOrganisationUnit.CurrentScenario];
                    this.Broker.ReleaseBrokerManagedComponent(objLogicalOrganisationUnit);

                    if (strOrganisationName.ToLower() == objOUCM.Name.ToLower()
                        && this.CLID != objOUCM.CLID)
                    {
                        return false;
                    }

                    this.Broker.ReleaseBrokerManagedComponent(objOUCM);
                }
            }

            this.Broker.ReleaseBrokerManagedComponent(objOrganisation);

            // if we reach this point and haven't returned then
            // we didn't find a match, so return true
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strOrganisationLegalName"></param>
        /// <exception cref="System.ArgumentNullException">strOrganisationLegalName cannot be null or an empty string</exception>
        public bool IsUniqueLegalOrganisationName(string strOrganisationLegalName)
        {
            if (string.IsNullOrEmpty(strOrganisationLegalName))
                throw new ArgumentNullException("strOrganisationLegalName");

            ICalculationModule objOrganisation = this.Owner.GetWellKnownCM(WellKnownCM.Organization);
            DataRowCollection logicalModuleRows = (DataRowCollection)this.Broker.GetLCMsByParent(objOrganisation.CLID);
            this.Broker.ReleaseBrokerManagedComponent(objOrganisation);

            foreach (DataRow logicalModuleRow in logicalModuleRows)
            {
                Guid objOrganisationUnitCLID = (Guid)logicalModuleRow[DSLogicalModulesInfo.LCM_CLID_FIELD];
                ILogicalModule objLogicalOrganisationUnit = this.Broker.GetLogicalCM(objOrganisationUnitCLID);
                IOrganizationUnit objOU = (IOrganizationUnit)objLogicalOrganisationUnit[objLogicalOrganisationUnit.CurrentScenario];
                this.Broker.ReleaseBrokerManagedComponent(objLogicalOrganisationUnit);

                if (strOrganisationLegalName.ToLower() == objOU.LegalName.ToLower()
                    && this.CLID != objOU.CLID)
                {
                    return false;
                }

                this.Broker.ReleaseBrokerManagedComponent(objOU);
            }

            // if we reach this point and haven't returned then
            // we didn't find a match, so return true
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        public bool IsMemberOfGroup()
        {
            DataRowCollection objConsolidationParents = (DataRowCollection)this.Broker.GetConsolidationParentsList(this.CLID);

            if (objConsolidationParents.Count > 0)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Remove a period from the org unit
        /// </summary>
        /// <param name="gdPeriodCLID"></param>
        /// <summary>
        /// 
        /// </summary>
        /// <param name="gdPeriodCLID"></param>
        /// <exception cref="System.ArgumentException">gdPeriodCLID cannot be an empty guid</exception>
        public virtual void DeletePeriod(Guid gdPeriodCLID)
        {
            if (gdPeriodCLID == Guid.Empty)
                throw new ArgumentException("Parameter cannot be an empty guid", "gdPeriodCLID");

            DataRowCollection consolParentCMRows = (DataRowCollection)this.Broker.GetConsolidationParentsList(gdPeriodCLID, this.CSID);
            //If the count of rows is not zero, than the entity is a member of a group and cannot be deleted.
            if (consolParentCMRows.Count != 0)
                throw new BusinessRuleViolation("Period is a member of a Multi Period and cannot be deleted", BusinessRuleViolationType.ConsolidationMember);
            else
            {
                //When an organization units period if deleted, if that organization unit is a groups member, then 
                //the consolidated group must know of the periods removal, so that any consolidated data for a group 
                //period is updated occordingly.
                if (IsMemberOfGroup())
                {
                    IPeriod objPeriodCM = (IPeriod)this.Broker.GetCMImplementation(gdPeriodCLID, this.CSID);
                    objPeriodCM.PublishPeriodDeleteStreamControl();
                    this.Broker.ReleaseBrokerManagedComponent(objPeriodCM);
                }

                ILogicalModule objPeriodLM = this.Broker.GetLogicalCM(gdPeriodCLID);
                ILogicalModule objThisLM = this.Broker.GetLogicalCM(this.CLID);

                //If we are deleting the current period, then need to set a new current
                if (this.CurrentPeriod == gdPeriodCLID)
                    this.SetNewCurrentPeriod(gdPeriodCLID);

                //Publish a stream end message for all XP messages in the child CM's
                //of the period to be deleted.
                foreach (Guid gdChildCLID in objPeriodLM.ChildCLIDs)
                {
                    ICalculationModule iCM = this.Broker.GetLogicalCM(gdChildCLID)[this.CSID];

                    if (iCM != null)
                        iCM.PublishCrossPeriodStreamEnd();

                    this.Broker.ReleaseBrokerManagedComponent(iCM);
                }

                objThisLM.RemoveChildLogicalCM(objPeriodLM, this.CSID);
                this.RemoveMemberPeriodDetails(gdPeriodCLID);

                this.Broker.ReleaseBrokerManagedComponent(objPeriodLM);
                this.Broker.ReleaseBrokerManagedComponent(objThisLM);
            }
        }

        /// <summary>
        /// Checks if performing a task at this level will be time intensive
        /// navigates up through the structure checking for group membership
        /// </summary>
        public void CheckIfStructureWillBeTimeIntensive(DateTime periodStartDate, DateTime periodEndDate, string userConfirmationValue, BusinessStructureNavigationDirection direction)
        {
            TimeIntensiveTaskGroupStructureVisitor navigator = new TimeIntensiveTaskGroupStructureVisitor(this.Broker, periodStartDate, periodEndDate, direction, userConfirmationValue);
            ((IBusinessStructureModule)this).PerformBusinessStructreNavigationOperation(navigator);
        }

        /// <summary>
        /// Sets the current period of the entity to the standard period(not multiperiod) with the
        /// latest end date.
        /// </summary>
        /// <param name="gdDeletedPeriod"></param>
        /// <summary>
        /// 
        /// </summary>
        /// <param name="gdDeletedPeriod"></param>
        /// <exception cref="System.ArgumentException">gdDeletedPeriod cannot be an empty guid</exception>
        public void SetNewCurrentPeriod(Guid gdDeletedPeriod)
        {
            if (gdDeletedPeriod == Guid.Empty)
                throw new ArgumentException("Parameter cannot be an empty guid", "gdDeletedPeriod");

            PeriodsListDS dsPeriods = new PeriodsListDS();

            this.ExtractData(dsPeriods);

            DataView dv = new DataView(dsPeriods.Tables[PeriodsListDS.PERIODS_TABLE],
                PeriodsListDS.PERIODCLID_FIELD + " <> '" + gdDeletedPeriod.ToString() + "' AND " + PeriodsListDS.PERIODTYPENAME_FIELD + " <> 'MultiPeriod'",
                PeriodsListDS.PERIODENDDATE_FIELD + " DESC, " +
                PeriodsListDS.PERIODSTARTDATE_FIELD + " DESC",
                DataViewRowState.CurrentRows);

            if (dv.Count == 0)
                this.CurrentPeriod = Guid.Empty;
            else
            {
                Guid gdNewCurrentPeriodCLID = new Guid(dv[0][PeriodsListDS.PERIODCLID_FIELD].ToString());
                this.CurrentPeriod = gdNewCurrentPeriodCLID;

                ICalculationModule objPeriodCM = this.Broker.GetCMImplementation(gdNewCurrentPeriodCLID, this.CSID);
                this.Broker.LogEvent(EventType.PeriodSetCurrent, "Current period set to '" + objPeriodCM.Name + "' in '" + this.Name + "' in '" + this.ConstructBreadcrumb() + this.ConstructBreadcrumbPeriod() + this.ConstructScenarioBreadcrumb() + "'");

                this.Broker.ReleaseBrokerManagedComponent(objPeriodCM);
            }

        }

        /// <summary>
        /// Deletes a scenario from the organisation unit
        /// </summary>
        /// <param name="objScenarioCSID">The CSID of the scenario to delete</param>
        /// <param name="objParentCSID">The CSID of the parent scenario</param>
        /// <param name="objLastScenarioConflicts">A list of scenarios that can't be deleted because they are the last scenario</param>
        /// <param name="objMemberOfGroupScenario">A list of scenarios that can't be deleted because they are a member of another group</param>
        /// <param name="objLockedScenarios">A list of scenarios that can't be deleted because they are locked</param>
        /// <returns>True if the scenario was deleted, false otherwise</returns>
        public virtual bool DeleteScenario(Guid objScenarioCSID, Guid objParentCSID, ArrayList objLastScenarioConflicts, ArrayList objMemberOfGroupScenario, ArrayList objLockedScenarios)
        {
            return false;
        }

        //Method to check that if there are any child multi periods
        //within the entity which have the same start and end date as the period
        //which the message may be coming from.
        //Then Suppress any onpublishing of Consolidation messages from the 
        //period, so that only messages from the corresponding multi period are used.
        private bool UpwardPublishMessage(IMessage objMessage)
        {
            DateTime objMessageStartDate = (DateTime)objMessage["StartDate"];
            DateTime objMessageEndDate = (DateTime)objMessage["EndDate"];

            PublisherData objPubData = objMessage.GetPublisherByCMTypeName("Period_1_1");
            if (objPubData == null)
                objPubData = objMessage.GetPublisherByCMTypeName("MultiPeriod");
            if (objPubData == null)
                objPubData = objMessage.GetPublisherByCMTypeName("MultiPeriodAdjustment");
            if (objPubData == null)
                throw new Exception("OrganisationUnitCM -> Unable to get publisher data");

            IDictionaryEnumerator en = this.MemberPeriods.GetEnumerator();
            while (en.MoveNext())
            {
                if ((Guid)en.Key != objPubData.CLID)
                {
                    PeriodMember objPeriodMember = (PeriodMember)en.Value;
                    if (objPeriodMember.strTypeName == "MultiPeriod"
                        && objPeriodMember.dtStartDate == objMessageStartDate
                        && objPeriodMember.dtEndDate == objMessageEndDate)
                        return false;
                }
            }
            return true;
        }

        public override bool FilterMessage(IMessage message, Object[] parameters)
        {
            if (message == null)
                throw new ArgumentNullException("message", "Message paramerer passed to filter message was null");

            if (parameters != null && message["EndDate"] != null)
            {
                if (parameters.Length > 0)
                {
                    DateTime objEndDate = (DateTime)message["EndDate"];
                    DateTime objCompareDate = (DateTime)parameters[0];

                    return (objEndDate == objCompareDate);
                }
                else
                    return (true);
            }
            else
                return (true);
        }
        #endregion
        #region OVERRIDE MEMBERS FOR CM BEHAVIOR
        //Override of construct breadcrumb so that when a breadcrumb is created for 
        //a business entity (ie a group or a standard entity) then this breadcrumb will
        //also contain the scenario information if possible.
        public override string ConstructBreadcrumb()
        {
            return this.name + " >";
        }

        private string ConstructBreadcrumbPeriod()
        {
            string periodBreadcrumb = String.Empty;

            ICalculationModule periodCM = this.Broker.GetCMImplementation(this.CurrentPeriod, this.CSID);

            if (periodCM != null)
                periodBreadcrumb = periodCM.Name;

            this.Broker.ReleaseBrokerManagedComponent(periodCM);

            return periodBreadcrumb;
        }

        private string ConstructScenarioBreadcrumb()
        {
            string scenarioBreadcrumb = String.Empty;
            ILogicalModule thisLM = this.Broker.GetLogicalCM(this.CLID);
            CMScenario cmScenario = thisLM.GetScenario(this.CSID);
            scenarioBreadcrumb = cmScenario.Name;
            this.Broker.ReleaseBrokerManagedComponent(thisLM);

            if (scenarioBreadcrumb != String.Empty)
                scenarioBreadcrumb = "[" + scenarioBreadcrumb + "]";

            return scenarioBreadcrumb;
        }
        /// <summary>
        /// Called when an internally originating subscription update is to be delivered
        /// to the CM.  This is a subscription update that originates (or has been on-published)
        /// by a CM instance contained within the CM.
        /// </summary>
        /// <param name="message">The message containing the subscription update.</param>
        public override void OnInternalSubscriptionUpdate(IMessage message)
        {
            if (message.Contains("Consolidation"))
            {
                //Only on publish message with consolidation = true
                if ((bool)message["Consolidation"] && UpwardPublishMessage(message))
                {
                    this.OverrideStreamControlType((StreamControlType)message["StreamControl"]);

                    Message onPublishedMessage = new Message(this, message);

                    onPublishedMessage.InterScenario = true;	// All on-published messages by entities are made inter-scenario
                    onPublishedMessage["ConsolidatedSubscribedCLID"] = this.CLID;
                    onPublishedMessage["ConsolidatedSubscribedCSID"] = this.CSID;
                    PublishMessage(onPublishedMessage);

                    this.ResetOverrideStreamControlType();
                }
            }
        }


        public override DataSet PrimaryDataSet { get { return new OrganizationUnitDS(); } }

        #endregion

        #region OnGetData Methods
        protected void OnGetData(OrganisationUnitDetailsDS objOUDetails)
        {
            //Always set the logical module name for the org unit to be the short entity name.
            ILogicalModule iLM = this.Broker.GetLogicalCM(this.CLID);

            if (iLM != null)
                objOUDetails.Name = iLM.Name;
            else
                objOUDetails.Name = this.Name;

            this.Broker.ReleaseBrokerManagedComponent(iLM);

            objOUDetails.LegalName = this.LegalName;
            objOUDetails.Identifier = this.Identifier;
            objOUDetails.Breadcrumb = this.ConstructBreadcrumb();
        }

        protected override void OnGetData(DataSet data)
        {
            base.OnGetData(data);
            if (data is OrganizationUnitDS)
            {
                OrganizationUnitDS objOUDS = data as OrganizationUnitDS;
                DataRow row = objOUDS.Tables[OrganizationUnitDS.ORGUNIT_TABLE].NewRow();
                row[OrganizationUnitDS.ORGUNITCURRPERIOD_FIELD] = this.m_objCurrentPeriod;
                row[OrganizationUnitDS.ORGUNITLEGALNAME_FIELD] = this.m_strLegalName;
                row[OrganizationUnitDS.ORGUNITIDENT_FIELD] = this.m_strIdentifier;
                objOUDS.Tables[OrganizationUnitDS.ORGUNIT_TABLE].Rows.Add(row);

                foreach (DictionaryEntry dictionaryEntry in m_objPeriodsHT)
                {

                    PeriodMember pm = (PeriodMember)dictionaryEntry.Value;
                    row = objOUDS.Tables[OrganizationUnitDS.ORGUNITPERIODS_TABLE].NewRow();
                    row[OrganizationUnitDS.ORGUNITPERIODKEY_FIELD] = dictionaryEntry.Key;
                    row[OrganizationUnitDS.ORGUNITPERIODSTARTDATE_FIELD] = pm.dtStartDate;
                    row[OrganizationUnitDS.ORGUNITPERIODENDDATE_FIELD] = pm.dtEndDate;
                    row[OrganizationUnitDS.ORGUNITPERIODTYPE_FIELD] = pm.strTypeName;

                    objOUDS.Tables[OrganizationUnitDS.ORGUNITPERIODS_TABLE].Rows.Add(row);
                }
            }
            else if (data is OrganisationUnitDetailsDS)
            {
                this.OnGetData((OrganisationUnitDetailsDS)data);
            }
            else if (data is PeriodsListDS)
            {
                this.OnGetData((PeriodsListDS)data);
            }
        }

        public void OnGetDataSL(PeriodsListDS periodsListDS, String CLID, String CSID, Guid _currentperiod)
        {
            DataRowCollection logicalModuleRows = (DataRowCollection)this.Broker.GetLCMsByParent(new Guid(CLID));

            foreach (DataRow logicalModuleRow in logicalModuleRows)
            {
                DataRow newRow = periodsListDS.Tables[PeriodsListDS.PERIODS_TABLE].NewRow();

                Guid lcmPeriodCLID = (Guid)logicalModuleRow[DSLogicalModulesInfo.LCM_CLID_FIELD];
                ILogicalModule logicalPeriod = this.Broker.GetLogicalCM(lcmPeriodCLID);

                PeriodCM periodCM = (PeriodCM)logicalPeriod[new Guid(CSID)];
                this.Broker.ReleaseBrokerManagedComponent(logicalPeriod);

                if (null != periodCM)
                {
                    if (_currentperiod == (Guid)logicalModuleRow[DSLogicalModulesInfo.LCM_CLID_FIELD])
                    {
                        newRow[PeriodsListDS.PERIODNAME_FIELD] = periodCM.Name + " [default]";
                        newRow[PeriodsListDS.PERIODCURRENT_FIELD] = true;
                    }
                    else
                    {
                        newRow[PeriodsListDS.PERIODNAME_FIELD] = periodCM.Name;
                        newRow[PeriodsListDS.PERIODCURRENT_FIELD] = false;
                    }

                    newRow[PeriodsListDS.PERIODCLID_FIELD] = logicalModuleRow[DSLogicalModulesInfo.LCM_CLID_FIELD];

                    newRow[PeriodsListDS.PERIODCIID_FIELD] = periodCM.CIID;
                    newRow[PeriodsListDS.PERIODTYPENAME_FIELD] = periodCM.TypeName;
                    newRow[PeriodsListDS.PERIODENDDATE_FIELD] = periodCM.EndDate;
                    newRow[PeriodsListDS.PERIODSTARTDATE_FIELD] = periodCM.StartDate;
                    periodsListDS.Tables[PeriodsListDS.PERIODS_TABLE].Rows.Add(newRow);
                }

                this.Broker.ReleaseBrokerManagedComponent(periodCM);
            }
        }

        protected void OnGetData(PeriodsListDS periodsListDS)
        {
            DataRowCollection logicalModuleRows = (DataRowCollection)this.Broker.GetLCMsByParent(this.CLID);

            foreach (DataRow logicalModuleRow in logicalModuleRows)
            {
                DataRow newRow = periodsListDS.Tables[PeriodsListDS.PERIODS_TABLE].NewRow();

                Guid lcmPeriodCLID = (Guid)logicalModuleRow[DSLogicalModulesInfo.LCM_CLID_FIELD];
                ILogicalModule logicalPeriod = this.Broker.GetLogicalCM(lcmPeriodCLID);

                PeriodCM periodCM = (PeriodCM)logicalPeriod[this.CSID];
                this.Broker.ReleaseBrokerManagedComponent(logicalPeriod);

                if (null != periodCM)
                {
                    if (this.CurrentPeriod == (Guid)logicalModuleRow[DSLogicalModulesInfo.LCM_CLID_FIELD])
                    {
                        newRow[PeriodsListDS.PERIODNAME_FIELD] = periodCM.Name + " [default]";
                        newRow[PeriodsListDS.PERIODCURRENT_FIELD] = true;
                    }
                    else
                    {
                        newRow[PeriodsListDS.PERIODNAME_FIELD] = periodCM.Name;
                        newRow[PeriodsListDS.PERIODCURRENT_FIELD] = false;
                    }

                    newRow[PeriodsListDS.PERIODCLID_FIELD] = logicalModuleRow[DSLogicalModulesInfo.LCM_CLID_FIELD];

                    newRow[PeriodsListDS.PERIODCIID_FIELD] = periodCM.CIID;
                    newRow[PeriodsListDS.PERIODTYPENAME_FIELD] = periodCM.TypeName;
                    newRow[PeriodsListDS.PERIODENDDATE_FIELD] = periodCM.EndDate;
                    newRow[PeriodsListDS.PERIODSTARTDATE_FIELD] = periodCM.StartDate;
                    periodsListDS.Tables[PeriodsListDS.PERIODS_TABLE].Rows.Add(newRow);
                }

                this.Broker.ReleaseBrokerManagedComponent(periodCM);
            }
        }

        #endregion

        #region OnSetData Methods
        protected ModifiedState OnSetData(OrganisationUnitDetailsDS objOUDetails)
        {
            SetShortEntityName(objOUDetails.Name, objOUDetails.IsNewOrganisation);

            this.LegalName = objOUDetails.LegalName;

            this.Identifier = objOUDetails.Identifier;

            return ModifiedState.UNKNOWN;
        }

        public void SetShortEntityName(string name, bool isNew)
        {
            name = StringUtilities.RemoveRecurringSpaces(name);

            // check if the name has changed, that the name is unique, if not then throw an exception to indicate that
            // it is not unique - in which case the UI catches the exception and displays a message to the user.
            if ((this.Name != name || isNew) &&
                !this.IsUniqueOrganisationName(name))
                throw new DisplayUserMessageException(new MessageBoxDefinition(MessageBoxDefinition.MSG_ENTER_DISPLAY_NAME), false, MessageDisplayMethod.MainWindowError);

            this.Name = name;

            //Always set the logical module name for the org unit to be the short entity name.
            ILogicalModule iLM = this.Broker.GetLogicalCM(this.CLID);
            iLM.Name = name;
            iLM.CalculateToken(true);

            //When setting the short entity name in the logical module
            //also change the name in any other scenarios for that BMC
            //so that these are always in sync, even though it should not be getting
            //used anymore.
            foreach (DictionaryEntry dictEn in iLM.Scenarios)
            {
                CMScenario cMScenario = (CMScenario)dictEn.Value;
                if (cMScenario.CIID != this.CIID)
                {
                    IBrokerManagedComponent orgUnit = (IBrokerManagedComponent)this.Broker.GetCMImplementation(cMScenario.CLID, cMScenario.CSID);
                    orgUnit.Name = name;
                    orgUnit.CalculateToken(true);
                    this.Broker.ReleaseBrokerManagedComponent(orgUnit);
                }
            }

            this.Broker.ReleaseBrokerManagedComponent(iLM);
        }

        protected ModifiedState OnSetData(ScenariosDS objData)
        {
            ModifiedState objState = ModifiedState.UNCHANGED;

            //Check for any scenario deletions
            DataTable objScenarios = objData.Tables[ScenariosDS.SCENARIOS_TABLE];
            DataTable objScenariosToDelete = objScenarios.GetChanges(DataRowState.Deleted);
            if (objScenariosToDelete != null)
            {
                objScenariosToDelete.RejectChanges();

                foreach (DataRow objDeletedRow in objScenariosToDelete.Rows)
                {
                    Guid cSIDToDelete = new Guid(objDeletedRow[ScenariosDS.CSID_FIELD].ToString());

                    ArrayList objLastScenarioConflicts = new ArrayList();
                    ArrayList objMemberOfGroupScenario = new ArrayList();
                    ArrayList objLockedScenarios = new ArrayList();

                    //If the scenario is locked, then throw exception and do not go any further.
                    ILogicalModule entityLM = this.Broker.GetLogicalCM(this.CLID);
                    if (entityLM.IsScenarioLocked(cSIDToDelete))
                        throw new ScenarioLockException();

                    if (this is IEntity)
                    {
                        //If the Oganizational unit receiving the dataset is not the one being
                        //deleted, then need to get the Entity instance being deleted and call the method on it.
                        if (this.CSID == cSIDToDelete)
                            this.DeleteScenario(cSIDToDelete, this.CSID, objLastScenarioConflicts, objMemberOfGroupScenario, objLockedScenarios);
                        else
                        {
                            OrganizationUnitCM entityScenarioToDelete = (OrganizationUnitCM)entityLM[cSIDToDelete];
                            entityScenarioToDelete.DeleteScenario(cSIDToDelete, cSIDToDelete, objLastScenarioConflicts, objMemberOfGroupScenario, objLockedScenarios);
                            this.Broker.ReleaseBrokerManagedComponent(entityScenarioToDelete);
                        }

                        objState = ModifiedState.MODIFIED;
                        // check if we are the member of a group scenario
                        if (objMemberOfGroupScenario.Count > 0)
                            throw new DisplayUserMessageException(new MessageBoxDefinition(MessageBoxDefinition.MSG_DELETE_SCENARIO_FAILURE), false, MessageDisplayMethod.MainWindowError);

                        // check if we had any conflicts, if we did, alert the user
                        if (objLastScenarioConflicts.Count > 0)
                            throw new DisplayUserMessageException(new MessageBoxDefinition(MessageBoxDefinition.MSG_DELETE_LAST_SCENARIO, new string[] { this.Name }), false, MessageDisplayMethod.MainWindowError);
                    }
                    else if (this is IGroup)
                    {
                        if (objData.DeleteInMembers &&
                            !this.IsMemberOfOtherGroupScenario(cSIDToDelete) &&
                            !objData.GetUserConfirmed(DeleteScenarioConfirmKey))
                            ((IGroup)this).CheckIfStructureWillBeTimeIntensive(DeleteScenarioConfirmKey, BusinessStructureNavigationDirection.DownAndAcross);


                        if (this.CSID == cSIDToDelete)
                            ((IGroup)this).DeleteScenario(cSIDToDelete, objData.DeleteInMembers, objLastScenarioConflicts, objMemberOfGroupScenario, objLockedScenarios);
                        else
                        {
                            ILogicalModule groupLM = this.Broker.GetLogicalCM(this.CLID);
                            IGroup groupScenarioToDelete = (IGroup)groupLM[cSIDToDelete];
                            groupScenarioToDelete.DeleteScenario(cSIDToDelete, objData.DeleteInMembers, objLastScenarioConflicts, objMemberOfGroupScenario, objLockedScenarios);
                            this.Broker.ReleaseBrokerManagedComponent(groupLM);
                            this.Broker.ReleaseBrokerManagedComponent(groupScenarioToDelete);
                        }

                        // check if we had any conflicts, if we did, alert the user
                        if (objLastScenarioConflicts.Count > 0)
                        {
                            string strErrors = "<br>";
                            foreach (string strLastScenarioConflictName in objLastScenarioConflicts)
                                strErrors += " - " + strLastScenarioConflictName + "<br>";

                            objData.AddWarningUserDisplayMessage(new MessageBoxDefinition(MessageBoxDefinition.MSG_DELETE_LAST_SCENARIO, new string[] { strErrors }).ToString());
                        }

                        if (objLockedScenarios.Count > 0)
                        {
                            string strErrors = "<br>";
                            foreach (string strScenarioConflictName in objLockedScenarios)
                                strErrors += " - " + strScenarioConflictName + "<br>";

                            objData.AddWarningUserDisplayMessage(new MessageBoxDefinition(MessageBoxDefinition.MSG_DELETE_LOCKED_SCENARIO_FAILURE_GROUPMEMBER, new string[] { strErrors }).ToString());
                        }

                        if (objMemberOfGroupScenario.Count > 0)
                        {
                            string strErrors = "<br>";
                            foreach (string strScenarioConflictName in objMemberOfGroupScenario)
                                strErrors += " - " + strScenarioConflictName + "<br>";

                            objData.AddWarningUserDisplayMessage(new MessageBoxDefinition(MessageBoxDefinition.MSG_DELETE_SCENARIO_FAILURE_GROUPMEMBERS, new string[] { strErrors }).ToString());
                        }
                    }

                    this.Broker.ReleaseBrokerManagedComponent(entityLM);
                }
            }

            //Check to see if the current period has been changed
            DataRow[] objScenariosCurrent = objData.Tables[ScenariosDS.SCENARIOS_TABLE].Select(ScenariosDS.CURRENT_FIELD + " = " + true.ToString());
            if (objScenariosCurrent.Length > 0)
            {
                Guid objCSID = new Guid(objScenariosCurrent[0][ScenariosDS.CSID_FIELD].ToString());
                ILogicalModule objOrgUnitLCM = this.Broker.GetLogicalCM(this.CLID);

                CMScenario cMScenario = objOrgUnitLCM.GetScenario(objCSID);
                if (objOrgUnitLCM.CurrentScenario != objCSID)
                {
                    objOrgUnitLCM.CurrentScenario = objCSID;
                    objOrgUnitLCM.CalculateToken(true);

                    this.Broker.LogEvent(EventType.SetCurrentScenario, "Current scenario for '" + objOrgUnitLCM.Name + "' changed to '" + cMScenario.Name + "' in '" + this.ConstructBreadcrumb() + this.ConstructBreadcrumbPeriod() + this.ConstructScenarioBreadcrumb() + "'");
                }

                this.Broker.ReleaseBrokerManagedComponent(objOrgUnitLCM);
            }

            return objState;
        }

        protected override ModifiedState OnSetData(DataSet data)
        {
            base.OnSetData(data);
            if (data is OrganizationUnitDS)
            {
                OrganizationUnitDS objData = data as OrganizationUnitDS;
                if (objData.Tables[OrganizationUnitDS.ORGUNIT_TABLE].Rows.Count == 1)
                {
                    DataRow row = objData.Tables[OrganizationUnitDS.ORGUNIT_TABLE].Rows[0];
                    this.CurrentPeriod = (Guid)row[OrganizationUnitDS.ORGUNITCURRPERIOD_FIELD];
                    this.m_strLegalName = row[OrganizationUnitDS.ORGUNITLEGALNAME_FIELD].ToString();
                    this.m_strIdentifier = row[OrganizationUnitDS.ORGUNITIDENT_FIELD].ToString();
                }

                m_objPeriodsHT.Clear();
                foreach (DataRow row in objData.Tables[OrganizationUnitDS.ORGUNITPERIODS_TABLE].Rows)
                {
                    AddNewMemberPeriodDetails(
                        (Guid)row[OrganizationUnitDS.ORGUNITPERIODKEY_FIELD],
                        (DateTime)row[OrganizationUnitDS.ORGUNITPERIODSTARTDATE_FIELD],
                        (DateTime)row[OrganizationUnitDS.ORGUNITPERIODENDDATE_FIELD],
                        row[OrganizationUnitDS.ORGUNITPERIODTYPE_FIELD].ToString()
                        );
                }
                return ModifiedState.MODIFIED;
            }
            else if (data is OrganisationUnitDetailsDS)
            {
                return this.OnSetData((OrganisationUnitDetailsDS)data);
            }
            else if (data is PeriodsListDS)
            {
                return this.OnSetData((PeriodsListDS)data);
            }
            else if (data is ScenariosDS)
            {
                return this.OnSetData((ScenariosDS)data);
            }
            else if (data is ScenarioDS)
            {
                return this.OnSetData((ScenarioDS)data);
            }
            return ModifiedState.UNCHANGED;
        }

        protected ModifiedState OnSetData(PeriodsListDS objData)
        {
            ModifiedState objState = ModifiedState.UNCHANGED;

            DataTable objPeriods = objData.Tables[PeriodsListDS.PERIODS_TABLE];
            DataTable objPeriodsToDelete = objPeriods.GetChanges(DataRowState.Deleted);

            if (objPeriodsToDelete != null)
            {
                objPeriodsToDelete.RejectChanges();

                foreach (DataRow objDeletedRow in objPeriodsToDelete.Rows)
                {
                    Guid objCLID = new Guid(objDeletedRow[PeriodsListDS.PERIODCLID_FIELD].ToString());
                    ICalculationModule periodCM = this.Broker.GetCMImplementation(objCLID, this.CSID);

                    if (periodCM != null)
                    {
                        IPeriod periodBeingDeleted = periodCM as IPeriod;

                        if (!objData.GetUserConfirmed("TimeIntensiveTaskDeletePeriodConfirm"))
                            this.CheckIfStructureWillBeTimeIntensive(periodBeingDeleted.StartDate, periodBeingDeleted.EndDate, "TimeIntensiveTaskDeletePeriodConfirm", BusinessStructureNavigationDirection.UpAndAcross);
                        //else
                        //    this.Broker.Transaction.IsLongRunningTransaction = true;

                        this.Broker.LogEvent(EventType.PeriodDelete, "Period deleted '" + periodCM.Name + "' from entity '" + this.Name + "' in '" + this.ConstructBreadcrumb() + this.ConstructBreadcrumbPeriod() + this.ConstructScenarioBreadcrumb() + "'");
                        this.DeletePeriod(objCLID);

                        objState = ModifiedState.MODIFIED;
                    }

                    this.Broker.ReleaseBrokerManagedComponent(periodCM);
                }
            }

            //Check to see if the current period has been changed
            DataRow[] objPeriodsCurrent = objPeriods.Select(PeriodsListDS.PERIODCURRENT_FIELD + " = " + true.ToString());
            if (objPeriodsCurrent.Length > 0)
            {
                Guid objCLID = new Guid(objPeriodsCurrent[0][PeriodsListDS.PERIODCLID_FIELD].ToString());

                if (objCLID != this.CurrentPeriod)
                {
                    //Get the period BMC so the details can be logged
                    ICalculationModule objPeriodCM = this.Broker.GetCMImplementation(objCLID, this.CSID);

                    this.CurrentPeriod = objCLID;
                    this.Broker.LogEvent(EventType.PeriodSetCurrent, "Current period set to '" + objPeriodCM.Name + "' in entity '" + this.Name + "' in '" + this.ConstructBreadcrumb() + this.ConstructBreadcrumbPeriod() + this.ConstructScenarioBreadcrumb() + "'");
                    this.Broker.ReleaseBrokerManagedComponent(objPeriodCM);

                    //If the reporting unit is a group, then also set the current period in its members if possible.
                    if (this is IGroup)
                        ((IGroup)this).SetCurrentPeriodInMembers(objPeriodsCurrent[0][PeriodsListDS.PERIODSTARTDATE_FIELD].ToString(), objPeriodsCurrent[0][PeriodsListDS.PERIODENDDATE_FIELD].ToString());

                    objState = ModifiedState.MODIFIED;
                }
            }

            return objState;
        }

        protected ModifiedState OnSetData(ScenarioDS data)
        {
            Guid cLID = new Guid(data.CLID);
            Guid cSID = new Guid(data.CSID);
            Guid cIID = new Guid(data.CIID);

            ILogicalModule logicalEntity = this.Broker.GetLogicalCM(cLID);
            CMScenario cMScenario = logicalEntity.GetScenario(cSID);

            bool lockedScenarioModified = false;
            bool scenarioModifiedToUnlocked = false;
            bool scenarioExists = (cMScenario != null);
            bool lockScenarioStatusChanged = false;

            if (null == cMScenario)
            {
                LicenceSM licence = (LicenceSM)this.Broker.GetWellKnownBMC(WellKnownCM.Licencing);
                licence.IsLicenced(this.CLID, LicenceValidationType.Scenarios);

                this.Broker.ReleaseBrokerManagedComponent(licence);

                ArrayList conflictingOUNames = new ArrayList();
                if (this.HasScenario(data.Name, true, conflictingOUNames))
                {
                    string strRUNames = String.Empty;
                    foreach (string strName in conflictingOUNames)
                        strRUNames += "<br>" + strName + ", ";

                    strRUNames = strRUNames.Trim();
                    strRUNames = strRUNames.TrimEnd(',');
                    throw new DisplayUserMessageException(new MessageBoxDefinition(MessageBoxDefinition.MSG_SCENARIO_EXISTS_GROUP_MEMBER, new string[] { strRUNames }), false, MessageDisplayMethod.MainWindowError);
                }

                // The case where a new scenario is being created
                Guid copiedCSID = new Guid(data.CopiedCSID);
                cMScenario = logicalEntity.CreateCopyScenario(
                    data.Name,
                    cSID,
                    data.Type,
                    copiedCSID,
                    data.Status,
                    data.CreationDateTime,
                    data.ModificationDateTime
                    );
                logicalEntity.CurrentScenario = cMScenario.CSID; //Set the newly created scenario to be the current
            }
            else
            {
                if (logicalEntity.IsScenarioLocked(cSID))
                    lockedScenarioModified = true;

                lockScenarioStatusChanged = (data.Locked != lockedScenarioModified);

                // we only check if the task will be time intensive if
                // we are a group and the user has changed the locked
                // scenario status
                if (this is IGroup && lockScenarioStatusChanged)
                {
                    // set the dates to minvalue as we don't use these
                    // when navigating down the structure
                    if (!data.GetUserConfirmed(LockedStatusTimeIntensiveTaskKey))
                        this.CheckIfStructureWillBeTimeIntensive(DateTime.MinValue, DateTime.MinValue, LockedStatusTimeIntensiveTaskKey, BusinessStructureNavigationDirection.DownAndAcross);
                }

                cMScenario.Name = data.Name;
                cMScenario.Type = data.Type;
                cMScenario.ModificationDateTime = data.ModificationDateTime;
                cMScenario.Status = data.Status;

                if (cMScenario.Locked && !data.Locked)
                    scenarioModifiedToUnlocked = true;

                cMScenario.Locked = data.Locked;

                // if we are locking the scenario we need to make the change
                // here before we lock our members
                if (data.Locked)
                    setConsolAdjustName(logicalEntity, cSID, data);

                //If it is a entity being unlocked then also unlock the corresponding scenario
                //in all children such that any actions performed in 'HandleScenarioUnlocked'
                //will not cause a scenario locking exception to occur.
                if (logicalEntity.CMCategory == "BusinessEntity")
                    setScenarioLockedOnMembers(logicalEntity, cSID, data.Locked, data.Name);
            }

            // need to recalculate the logical module token as we have
            // possibly changed the data in the logical module
            logicalEntity.CalculateToken(true);

            this.Broker.ReleaseBrokerManagedComponent(logicalEntity);

            if (scenarioModifiedToUnlocked || (data.Locked && !lockedScenarioModified))
                this.ScenarioLockOverridden = true;

            if (logicalEntity.Modified && lockedScenarioModified && !this.ScenarioLockOverridden)
                throw new ScenarioLockException();

            if (scenarioModifiedToUnlocked)
                HandleScenarioUnlocked();

            // if we are unlocking then we have to set the name
            // after we have unlocked our members if there is an existing scenario
            if (scenarioExists && !data.Locked)
                setConsolAdjustName(logicalEntity, cSID, data);

            this.status = data.Status;

            return ModifiedState.UNKNOWN;
        }

        protected virtual bool HasScenario(string scenarioName, bool searchSubGroups, ArrayList conflictingOUNames)
        {
            return true;
        }

        private void setConsolAdjustName(ILogicalModule logicalEntity, Guid cSID, ScenarioDS data)
        {
            ICalculationModule calcModule = logicalEntity[cSID];
            if (calcModule is IGroup)
            {
                IGroup group = calcModule as IGroup;
                //If a scenario is not being copied then also make sure that the 
                //Consolidation Adjustment entity has its Scenario name updated.
                IBrokerManagedComponent consolAdjBMC = (IBrokerManagedComponent)this.Broker.GetCMImplementation(group.ConsolidationAdjustmentEntityCLID, group.ConsolidationAdjustmentEntityCSID);

                data.CSID = group.ConsolidationAdjustmentEntityCSID.ToString();
                data.CIID = consolAdjBMC.CID.ToString();
                data.CLID = group.ConsolidationAdjustmentEntityCLID.ToString();
                data.CopiedCSID = group.ConsolidationAdjustmentEntityCSID.ToString();

                consolAdjBMC.SetData(data);

                this.Broker.ReleaseBrokerManagedComponent(consolAdjBMC);
            }
        }

        private void setScenarioLockedOnMembers(ILogicalModule logicalEntity, Guid cSID, bool locked, string name)
        {
            ICalculationModule calcModule = logicalEntity[cSID];

            if (locked)
                logicalEntity.LockScenario(cSID);
            else
                logicalEntity.UnLockScenario(cSID);

            if (calcModule is IGroup)
            {
                IGroup group = calcModule as IGroup;
                foreach (DictionaryEntry dictionaryEntry in group.IncludedGroupMembers)
                {
                    GroupMember groupMember = (GroupMember)dictionaryEntry.Value;
                    ILogicalModule entityLM = Broker.GetLogicalCM(groupMember.CLID);
                    CMScenario cMScenario = entityLM.GetScenario(groupMember.CSID);
                    setScenarioLockedOnMembers(entityLM, groupMember.CSID, locked, cMScenario.Name);

                    this.Broker.ReleaseBrokerManagedComponent(entityLM);
                }
            }

            foreach (Guid periodCLID in logicalEntity.ChildCLIDs)
            {
                ILogicalModule periodLM = this.Broker.GetLogicalCM(periodCLID);
                CMScenario cMScenario = logicalEntity.GetScenario(cSID);

                if (cMScenario != null)
                {
                    cMScenario.Name = name;
                    cMScenario.Locked = locked;
                    periodLM.CalculateToken(true);
                }

                foreach (Guid taxTopicCLID in periodLM.ChildCLIDs)
                {
                    ILogicalModule ttLM = this.Broker.GetLogicalCM(taxTopicCLID);
                    cMScenario = ttLM.GetScenario(cSID);

                    if (cMScenario != null)
                    {
                        cMScenario.Name = name;
                        cMScenario.Locked = locked;
                        ttLM.CalculateToken(true);
                    }

                    this.Broker.ReleaseBrokerManagedComponent(ttLM);
                }

                this.Broker.ReleaseBrokerManagedComponent(periodLM);
            }
        }

        /// <summary>
        /// Perform any actions which need to occur when a scenario is unlocked
        /// </summary>
        private void HandleScenarioUnlocked()
        {
            ILogicalModule orgUnitLM = this.Broker.GetLogicalCM(this.CLID);

            foreach (Guid periodCLID in orgUnitLM.ChildCLIDs)
            {
                ILogicalModule periodLM = this.Broker.GetLogicalCM(periodCLID);

                foreach (Guid taxTopicCLID in periodLM.ChildCLIDs)
                {
                    ILogicalModule taxTopicLM = this.Broker.GetLogicalCM(taxTopicCLID);
                    ICalculationModule taxTopicBMC = taxTopicLM[this.CSID];

                    this.Broker.ReleaseBrokerManagedComponent(taxTopicLM);

                    if (taxTopicBMC != null && taxTopicBMC is TaxTopicCM)
                        ((TaxTopicCM)taxTopicBMC).ScenarioLockOverridden = true;

                    this.Broker.ReleaseBrokerManagedComponent(taxTopicBMC);
                }

                this.Broker.ReleaseBrokerManagedComponent(periodLM);
            }

            this.Broker.ReleaseBrokerManagedComponent(orgUnitLM);

            orgUnitLM = this.Broker.GetLogicalCM(this.CLID);

            foreach (Guid periodCLID in orgUnitLM.ChildCLIDs)
            {
                ILogicalModule periodLM = this.Broker.GetLogicalCM(periodCLID);

                foreach (Guid taxTopicCLID in periodLM.ChildCLIDs)
                {
                    ILogicalModule taxTopicLM = this.Broker.GetLogicalCM(taxTopicCLID);
                    ICalculationModule taxTopicBMC = taxTopicLM[this.CSID];

                    this.Broker.ReleaseBrokerManagedComponent(taxTopicLM);

                    if (taxTopicBMC != null && taxTopicBMC is TaxTopicCM)
                        ((TaxTopicCM)taxTopicBMC).UpdateConfigurableCMCategories();

                    this.Broker.ReleaseBrokerManagedComponent(taxTopicBMC);
                }

                this.Broker.ReleaseBrokerManagedComponent(periodLM);
            }

            this.Broker.ReleaseBrokerManagedComponent(orgUnitLM);
        }

        public override void DeliverData(DataSet data)
        {
            // Before calling base.DeliverData remove recurring spaces  for entity/group/scenarios

            if (data is OrganisationUnitDetailsDS)
            {
                ((OrganisationUnitDetailsDS)data).Name = StringUtilities.RemoveRecurringSpaces(((OrganisationUnitDetailsDS)data).Name);
            }

            if (data is ScenarioDS)
            {
                ((ScenarioDS)data).Name = StringUtilities.RemoveRecurringSpaces(((ScenarioDS)data).Name);
            }

            base.DeliverData(data);

            if (data is OrganisationUnitDetailsDS)
            {
                OrganisationUnitDetailsDS objOUDetails = (OrganisationUnitDetailsDS)data;

                // check if the name has changed, that the name is unique, if not then throw an exception to indicate that
                // it is not unique - in which case the UI catches the exception and displays a message to the user.
                if ((this.Name != objOUDetails.Name || objOUDetails.IsNewOrganisation) &&
                     !this.IsUniqueOrganisationName(objOUDetails.Name))
                    throw new BusinessRuleViolation("Organisation name is the same as another legal organisation", BusinessRuleViolationType.OrganisationUnitNameConflict);
                this.Name = objOUDetails.Name;

                //Always set the logical module name for the org unit to be the short entity name.
                ILogicalModule iLM = this.Broker.GetLogicalCM(this.CLID);
                iLM.Name = objOUDetails.Name;
                iLM.CalculateToken(true);

                //When setting the short entity name in the logical module
                //also change the name in any other scenarios for that BMC
                //so that these are always in sync, even though it should not be getting
                //used anymore.
                foreach (DictionaryEntry dictEn in iLM.Scenarios)
                {
                    CMScenario cMScenario = (CMScenario)dictEn.Value;
                    IBrokerManagedComponent orgUnit = (IBrokerManagedComponent)this.Broker.GetCMImplementation(cMScenario.CLID, cMScenario.CSID);
                    orgUnit.Name = objOUDetails.Name;
                    orgUnit.CalculateToken(true);
                    this.Broker.ReleaseBrokerManagedComponent(orgUnit);
                }

                this.Broker.ReleaseBrokerManagedComponent(iLM);
                this.LegalName = objOUDetails.LegalName;

                this.Identifier = objOUDetails.Identifier;
            }

            //When a new scenario is being created from another scenario, then we need to get any children
            //that are multiperiodCMs and cause these to diverge, and then subscribe to the new scenario members.
            if (data is ScenarioDS)
            {
                if (((ScenarioDS)data).CopyScenario)
                {
                    Guid gdNewCSID = new Guid(((ScenarioDS)data).CSID);

                    ILogicalModule thisLM = this.Broker.GetLogicalCM(this.CLID);

                    foreach (Guid childCLID in thisLM.ChildCLIDs)
                    {
                        ILogicalModule childLCM = this.Broker.GetLogicalCM(childCLID);

                        if (childLCM.CMType.ToString() == "050fb7a0-24f8-42ff-85ad-e93445b7c895")//MultiPeriod TypeID
                        {
                            IMultiPeriod mpCM = (IMultiPeriod)childLCM[gdNewCSID];

                            //If mpCM is null then there is no multi period cm for the scenario just created
                            if (mpCM != null)
                                mpCM.SubscribeNewScenario();
                        }

                        this.Broker.ReleaseBrokerManagedComponent(childLCM);
                    }

                    this.Broker.ReleaseBrokerManagedComponent(thisLM);
                }
            }

            if (data is OrganizationUnitDS)
            {
                DeliverPrimaryData(data);
            }
        }
        #endregion

        #region Migration

        public override DataSet MigrateDataSet(DataSet data)
        {
            versionHistory.Add(OrganizationUnitInstall.COMPONENTVERSION_IMPLEMENTATIONCLASS, OrganizationUnitDS.GetDataSetVersion(data));

            DataSet baseDataset = base.MigrateDataSet(data);

            DataSet convData = OrganizationUnitDS.ConvertV_1_0_3_0toV_1_1_0_0DataSet(data, data);
            //convData = ConvertV_XtoV_YDataSet(data, convData);

            if (convData == null)
                return null;

            OrganizationUnitDS ds = new OrganizationUnitDS();

            ds.Merge(convData.Tables[OrganizationUnitDS.ORGUNIT_TABLE]);
            ds.Merge(convData.Tables[OrganizationUnitDS.ORGUNITPERIODS_TABLE]);

            //merge in the base dataset
            ds.Merge(baseDataset);

            return ds;
        }

        /// <summary>
        /// notifies to the cm that the migration is completed
        /// </summary>
        public override void MigrationCompleted()
        {
            base.MigrationCompleted();
            versionHistory.Remove(OrganizationUnitInstall.COMPONENTVERSION_IMPLEMENTATIONCLASS);
        }
        #endregion

        //Empty virtual method, to be overridden when actions need to be taken prior to 
        //and organization unit being deleted from the system
        //ie In GroupCM, before deleting the Group CM, any Consolidation adjustments that
        //belong to the group being deleted need to also be deleted.
        public virtual void PreDeleteActions() { }

        public virtual TaxYear GetTaxYear(DateTime objPeriodStartDate, DateTime objPeriodEndDate)
        {
            if (this.SAPs == null)
                return null;
            else
            {
                return this.SAPs.GetTaxYear(objPeriodStartDate, objPeriodEndDate);
            }
        }

        public virtual FinancialYear GetFinancialYear(DateTime objPeriodStartDate, DateTime objPeriodEndDate)
        {
            if (this.SAPs == null)
                return null;
            else
            {
                return this.SAPs.GetFinancialYear(objPeriodStartDate, objPeriodEndDate);
            }
        }

        public IOrganizationUnit GetOrganisation()
        {
            return (IOrganizationUnit)this.Broker.GetWellKnownCM(WellKnownCM.Organization);
        }

        public ReadOnlyCollection<IGroup> GetParentGroups()
        {
            ReadOnlyCollection<ICalculationModule> parentGroupsAsCMs = this.Broker.GetGroupsThatHaveOrganisationUnitAsMember((ICalculationModule)this);

            List<IGroup> parentGroups = new List<IGroup>(parentGroupsAsCMs.Count);

            foreach (ICalculationModule calculationModule in parentGroupsAsCMs)
                parentGroups.Add((IGroup)calculationModule);

            return new ReadOnlyCollection<IGroup>(parentGroups);
        }

        #region Validation Methods
        public bool VerifyNewPeriodDoesNotOverlap(DateTime dtStartDate, DateTime dtEndDate, Guid gdOwnerCLID)
        {
            PeriodsListDS periodsListDS = new PeriodsListDS();

            this.ExtractData(periodsListDS); //Get a list of the periods for the organisation unit

            foreach (DataRow dr in periodsListDS.Tables[PeriodsListDS.PERIODS_TABLE].Rows)
            {
                Guid gdPeriodCLID = new Guid((string)dr[PeriodsListDS.PERIODCLID_FIELD]);

                if (gdPeriodCLID != gdOwnerCLID)//Check that the period being checked is not one currently beign edited.
                {
                    DateTime currPeriodStartDate = (DateTime)dr[PeriodsListDS.PERIODSTARTDATE_FIELD];
                    DateTime currPeriodEndDate = (DateTime)dr[PeriodsListDS.PERIODENDDATE_FIELD];

                    //If the period is identical, then do not test for overlap. CQ issue TXE200001935
                    if (!(dtStartDate == currPeriodStartDate && dtEndDate == currPeriodEndDate))
                    {
                        //Scenario 1: The start date of the new period lies within the range of the existing period
                        //Scenario 2: The end date of the new period lies within the range of the existing period
                        //Scenario 3: The start date is earlier than the existing period start date, and the end date
                        //				is later than the existing periods end date.
                        if (((dtStartDate >= currPeriodStartDate) && (dtStartDate <= currPeriodEndDate)) ||
                            ((dtEndDate >= currPeriodStartDate) && (dtEndDate <= currPeriodEndDate)) ||
                            ((dtStartDate <= currPeriodStartDate) && (dtEndDate >= currPeriodEndDate)))
                            return false;
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// Verify that a period does not exist for a given Organisation Unit
        /// </summary>
        /// <param name="dtStartDate">The start date of the period</param>
        /// <param name="dtEndDate">The end date of the period</param>
        /// <param name="gdOwnerCLID">The CLID of the period that is being saved, used to avoid throwing exception where the period is edited and the dates do not change</param>
        /// <returns>True if the period doesn't exist, false if it does exist</returns>
        public bool VerifyPeriodDoesNotExist(DateTime dtStartDate, DateTime dtEndDate, Guid gdOwnerCLID)
        {
            //Get info on current period, so that when editing a period
            //we can save that period with an unchanged date, without this 
            //causing a validation error for duplicate periods.

            PeriodsListDS periodsListDS = new PeriodsListDS();
            periodsListDS.CSID = this.CSID.ToString();
            this.ExtractData(periodsListDS); //Get a list of the periods for the entity

            foreach (DataRow dr in periodsListDS.Tables[PeriodsListDS.PERIODS_TABLE].Rows)
            {
                Guid gdPeriodCLID = new Guid((string)dr[PeriodsListDS.PERIODCLID_FIELD]);

                if (gdPeriodCLID != gdOwnerCLID)//Check that the period being checked is not one currently beign edited.
                {

                    DateTime currPeriodStartDate = (DateTime)dr[PeriodsListDS.PERIODSTARTDATE_FIELD];
                    DateTime currPeriodEndDate = (DateTime)dr[PeriodsListDS.PERIODENDDATE_FIELD];

                    if ((currPeriodStartDate == dtStartDate) && (currPeriodEndDate == dtEndDate))
                        return false;
                }
            }

            return true;
        }

        public bool IsMemberOfOtherGroupScenario(Guid objAllowedGroupScenarioCSID)
        {
            DataRowCollection objConsolidationParents = (DataRowCollection)this.Broker.GetConsolidationParentsList(this.CLID, this.CSID);

            foreach (DataRow objMembership in objConsolidationParents)
            {
                Guid objMembershipCSID = (Guid)objMembership["PARENTCSID"];

                // if we are a member of another scenario other than the permitted scenario, return true
                if (objMembershipCSID != objAllowedGroupScenarioCSID)
                    return true;
            }

            // if we got to the end and couldn't find a scenario which was different from the allowed, then we are not a part of another group scenario
            return false;
        }
        #endregion

        #region Structure as XML


        public void AddGroupStructureToXmlElement(XmlElement parentEl)
        {
            XmlElement el = AddChildElement(parentEl, CLID, CSID, this.Name);

            if (this is IGroup)
            {
                GroupMembers groupMembers = ((IGroup)this).IncludedGroupMembers;

                foreach (DictionaryEntry dictionarEntry in groupMembers)
                {
                    GroupMember gMember = (GroupMember)dictionarEntry.Value;
                    if (gMember.Adjustment.Equals(false))
                    {
                        ILogicalModule lCM = this.Broker.GetLogicalCM(gMember.CLID);
                        OrganizationUnitCM orgUnitCM = (OrganizationUnitCM)lCM[gMember.CSID];
                        this.Broker.ReleaseBrokerManagedComponent(lCM);

                        orgUnitCM.AddGroupStructureToXmlElement(el);
                    }
                }
            }
        }

        protected XmlElement AddChildElement(XmlElement parentEl, Guid clid, Guid csid, string name)
        {
            XmlElement el = parentEl.OwnerDocument.CreateElement("Row");
            AddAttribute(el, "Text", name);
            AddAttribute(el, "CSID", csid);
            AddAttribute(el, "CLID", clid);
            parentEl.AppendChild(el);
            return el;
        }

        protected void AddAttribute(XmlElement node, string name, Guid value)
        {
            AddAttribute(node, name, value.ToString());
        }
        protected static void AddAttribute(XmlElement node, string name, string value)
        {
            XmlAttribute attr = node.OwnerDocument.CreateAttribute(name);
            attr.Value = value;
            node.Attributes.Append(attr);
        }

        #endregion Structure as XML

    }
}
