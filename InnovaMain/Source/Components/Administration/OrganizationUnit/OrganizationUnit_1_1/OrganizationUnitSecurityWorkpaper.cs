using System;
using System.Collections;
using System.Web.UI.WebControls;
using System.Data;

using Oritax.TaxSimp.Calculation;

namespace Oritax.TaxSimp.CM.OrganizationUnit
{
	/// <summary>
	/// Summary description for OrganizationUnitSecurityWorkpaper.
	/// </summary>
	public abstract class OrganizationUnitSecurityWorkpaper : BusinessStructureWorkpaper
	{
		public OrganizationUnitSecurityWorkpaper():base(){}

		protected System.Web.UI.WebControls.ListBox lstConfiguredUsers;
		protected System.Web.UI.WebControls.Button btnAdd;
		protected System.Web.UI.WebControls.Button btnRemove;
		protected System.Web.UI.WebControls.ListBox lstAvailableUsers;
		
		#region Overriden Properties
		protected override BreadCrumbType BreadCrumbDisplayType
		{
			get
			{
				return BreadCrumbType.NoPeriodBreadCrumbWhenInstanceExists;
			}
		}

		protected override string SelectedMenuButton
		{
			get
			{
				return "buttonSecurity";
			}
		}

		protected override bool AdministratorRequired
		{
			get
			{
				return true;
			}
		}

		protected override ActionType CloseActionType
		{
			get
			{
				this.SetRedirectActionPage("Entities.aspx", "Organization_1_1");
				return ActionType.RedirectToSpecificWorkpaper;
			}
		}

		#endregion

		#region Member Methods
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
		}
		
		protected void entityDetailsCommand_Click( object sender, CommandEventArgs e )
		{
			this.AfterPostbackActionType = ActionType.RedirectToSpecificWorkpaper;
			this.SetRedirectActionPage(((PartyDS)this.PostbackDataSet).ConfigureWorkpaper);
		}

		protected void securityCommand_Click( object sender, CommandEventArgs e )
		{
			
		}

		protected void cvDeletedUser_ServerValidate(object sender, ServerValidateEventArgs args)
		{
			IEnumerator items = lstAvailableUsers.Items.GetEnumerator();
			while( items.MoveNext() )
			{
				ListItem listItem = (ListItem) items.Current;
				if ( listItem.Selected )
				{
					if ( !this.IsValidUser(new Guid(listItem.Value)) )
					{
						args.IsValid = false;
						((CustomValidator) sender).ErrorMessage = "Selected user '" + listItem.Text + "' has been deleted";
						return;
					}
				}
			}

			items = lstConfiguredUsers.Items.GetEnumerator();
			while( items.MoveNext() )
			{
				ListItem listItem = (ListItem) items.Current;
				if ( !this.IsValidUser(new Guid(listItem.Value)) )
				{
					args.IsValid = false;
					((CustomValidator) sender).ErrorMessage = "Selected user '" + listItem.Text + "' has been deleted";
					return;
				}
			}
		}		

		protected void btnAdd_Click(object sender, System.EventArgs e)
		{
			this.Validate();
			if ( !this.IsValid )
				return;
			IEnumerator items = lstAvailableUsers.Items.GetEnumerator( );

			while ( items.MoveNext( ) )
			{
				ListItem listItem = (ListItem) items.Current;
				if( listItem.Selected )
				{
					if( lstConfiguredUsers.Items.FindByValue( listItem.Value ) == null )
					{
						lstConfiguredUsers.Items.Add( listItem );
						DataRow partyRow=this.PostbackDataSet.Tables[PartyDS.INCLUDEDPARTY_TABLE].NewRow();
						partyRow[PartyDS.PARTYNAME_FIELD]=listItem.Text;
						partyRow[PartyDS.PARTYCID_FIELD]=listItem.Value;
						PostbackDataSet.Tables[PartyDS.INCLUDEDPARTY_TABLE].Rows.Add(partyRow);
					}
				}
			}
		}

		private bool IsValidUser(Guid userID)
		{
			DataTable availablePartyTable = this.PostbackDataSet.Tables[PartyDS.AVAILABLEPARTY_TABLE];
			DataRow[] partyRows = availablePartyTable.Select(PartyDS.PARTYCID_FIELD + "='" + userID.ToString() + "'", "", DataViewRowState.CurrentRows);
			if ( partyRows.Length > 0 )
				return true;
			else
				return false;
		}

		protected void btnRemove_Click(object sender, System.EventArgs e)
		{
			this.Validate();
			if ( !this.IsValid )
				return;
			IEnumerator items = lstConfiguredUsers.Items.GetEnumerator( );

			while ( items.MoveNext( ) )
			{
				ListItem objListItem = (ListItem) items.Current;
				if( objListItem.Selected )
				{
					DataRow[] drCollection = this.PostbackDataSet.Tables[PartyDS.INCLUDEDPARTY_TABLE].Select(PartyDS.PARTYCID_FIELD + " = '" + objListItem.Value.ToString() + "'",String.Empty);
					drCollection[0].Delete();

					lstAvailableUsers.SelectedIndex = -1;
					lstAvailableUsers.Items.Add( objListItem );

					lstConfiguredUsers.Items.Remove( objListItem );

					items = lstConfiguredUsers.Items.GetEnumerator();
				}
			}
		}
		#endregion

		#region Overriden Methods
		
		public override DataSet CreateDataSet()
		{
			PartyDS partyDS = new PartyDS();
			return partyDS;
		}

		public override void PopulateWorkpaper( DataSet data )
		{
			    ViewState["ConfigureWP"] = ((PartyDS)data).ConfigureWorkpaper;

			    this.lstConfiguredUsers.Attributes[ "ondblclick" ] = "document.all['btnRemove'].click()";
			    this.lstAvailableUsers.Attributes[ "ondblclick" ] = "document.all['btnAdd'].click()";

			    DataView objDataView = data.Tables[PartyDS.INCLUDEDPARTY_TABLE].DefaultView;
			    objDataView.Sort = PartyDS.PARTYNAME_FIELD;

			    lstConfiguredUsers.Items.Clear();
			    foreach(DataRowView accessRow in objDataView)
			    {
				    lstConfiguredUsers.Items.Add(new ListItem( accessRow[ PartyDS.PARTYNAME_FIELD ].ToString(), accessRow[ PartyDS.PARTYCID_FIELD ].ToString()));
			    }

			    lstAvailableUsers.Items.Clear();
			    foreach(DataRow accessRow in data.Tables[PartyDS.AVAILABLEPARTY_TABLE].Rows)
			    {
				    if( accessRow[ PartyDS.PARTYNAME_FIELD ].ToString().IndexOf( "Administrator" ) == -1 )
				    {
					    ListItem objListItem = new ListItem( accessRow[ PartyDS.PARTYNAME_FIELD ].ToString(), accessRow[ PartyDS.PARTYCID_FIELD ].ToString());

					    if(!(lstConfiguredUsers.Items.Contains( objListItem ) ) )
					    {
						    lstAvailableUsers.Items.Add( objListItem );
					    }
				    }
			    }

		}
		public override void DepopulateWorkpaper(DataSet data)
		{
//			foreach(ListItem lItem in lstConfiguredUsers)
//			{
//				DataRow 
//			}
		}
		#endregion
	}
}
