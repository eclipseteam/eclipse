﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.Calculation;
using System.Data.SqlClient;
using Oritax.TaxSimp.DataSets;
using System.Data;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.CM.OrganizationUnit
{
    public class OrganizationUnitPersister : BrokerManagedPersister
    {
        public OrganizationUnitPersister(SqlConnection connection, SqlTransaction transaction) : base(connection, transaction) { }

        public override void EstablishDatabase()
        {
            Guid typeID = new Guid(OrganizationUnitInstall.ASSEMBLY_ID);
            PrimaryDS primaryDataSet = new PersistentORGUNITDS();

            EstablishDatabase(typeID, primaryDataSet);
        }

        protected virtual DataTable ListBMCCollection(OrganisationUnitCollectionSpecifier organisationUnitCollectionSpecifier)
        {
            SqlDataAdapter collectionAdapter = new SqlDataAdapter();
            collectionAdapter.SelectCommand = DBCommandFactory.Instance.NewSqlCommand();
            collectionAdapter.SelectCommand.Connection = this.Connection;
            collectionAdapter.SelectCommand.Transaction = this.Transaction;

            collectionAdapter.SelectCommand.CommandText = "SELECT COA.ID, COA.IMPORTDATE, COA.IMPORTER, COA.SEPARATOR, COA.BALANCETYPE, COA.FILEUPLOADID, COA.TYPE, COA.AUTHORISED, COA.LICENSEID , COA.SHORTCL, COA.STATE, CI.NAME, (select count(*) from ledger where glcoaid=COA.id or clcoaid=COA.id) + (select count(*) from maptemplate where clcoaid=COA.id or glcoaid=coa.id) as CONSUMERS FROM CHARTOFACCOUNTS COA, " + BrokerManagedComponentDS.CINSTANCES_TABLE + " CI WHERE COA.ID = CI.CID";

            DataTable datatabe = new DataTable();
            collectionAdapter.Fill(datatabe);

            return datatabe;
        }

        protected virtual DataTable ListBMCCollection(COAConsumerCollectionSpecifier cOAConsumerCollectionSpecifier)
        {
            SqlDataAdapter collectionAdapter = new SqlDataAdapter();
            collectionAdapter.SelectCommand = DBCommandFactory.Instance.NewSqlCommand();
            collectionAdapter.SelectCommand.Connection = this.Connection;
            collectionAdapter.SelectCommand.Transaction = this.Transaction;

            collectionAdapter.SelectCommand.Parameters.AddWithValue("@COAID", cOAConsumerCollectionSpecifier.SupplierID);

            collectionAdapter.SelectCommand.CommandText = "select ci.cid id, ct.displayname, ci.name from " + BrokerManagedComponentDS.CINSTANCES_TABLE + " ci, assembly ct " +
                    "where ( (ci.ctid = ct.id) AND ( " +
                    "ci.cid in (select mt.cid from maptemplate mt where mt.clcoaid = @COAID OR mt.glcoaid = @COAID) OR " +
                    "ci.cid in (select l.id from ledger l where l.clcoaid = @COAID OR l.glcoaid = @COAID) OR " +
                    "ci.cid in (select cmt.id from clmaptemplate cmt where cmt.coaid = @COAID) 	) )";

            DataTable datatabe = new DataTable();
            collectionAdapter.Fill(datatabe);

            return datatabe;
        }

        protected virtual DataTable ListBMCCollection(CLCOAMigrationCollectionSpecifier clCOAMigrationConsumerCollectionSpecifier)
        {
            SqlDataAdapter collectionAdapter = new SqlDataAdapter();
            collectionAdapter.SelectCommand = DBCommandFactory.Instance.NewSqlCommand();
            collectionAdapter.SelectCommand.Connection = this.Connection;
            collectionAdapter.SelectCommand.Transaction = this.Transaction;

            collectionAdapter.SelectCommand.Parameters.AddWithValue("@ID", clCOAMigrationConsumerCollectionSpecifier.InstanceId);

            collectionAdapter.SelectCommand.CommandText = "select distinct c.id from " + BrokerManagedComponentDS.CINSTANCES_TABLE + " cinst, chartofaccounts c, maptemplate, clmaptemplate, ledger where cinst.cid = @ID and cinst.cid = c.id and c.type <> 'C' and (maptemplate.clcoaid = c.id or clmaptemplate.coaid = c.id or ledger.clcoaid = c.id)";

            DataTable table = new DataTable("MigrationCLCOA");
            table.Columns.Add("ID", typeof(System.Guid));
            collectionAdapter.Fill(table);

            return table;
        }
    }
}
