using System;
using System.Data;

using Oritax.TaxSimp.Calculation;

namespace Oritax.TaxSimp.CM.OrganizationUnit
{
	/// <summary>
	/// Exchange dataset for Organisation Units
	/// </summary>
	public class OrganisationUnitDetailsDS : BusinessStructureWorkpaperDS
	{
		private string m_strBreadcrumb;
		private string m_strName;
		private string m_strLegalName;
		private string m_strIdentifier;
		private bool bolIsNewOrganisation = false;
		
		public OrganisationUnitDetailsDS()
		{
			this.m_strName = String.Empty;
			this.m_strLegalName = String.Empty;
			this.m_strIdentifier = String.Empty;
		}

		
		/// <summary>
		/// The name of the organisation unit
		/// </summary>
		public string Name
		{
			get
			{
				return this.m_strName;
			}
			set
			{
				this.m_strName = value;
			}
		}

		/// <summary>
		/// The legal name of the organisation unit
		/// </summary>
		public string LegalName
		{
			get
			{
				return this.m_strLegalName;
			}
			set
			{
				this.m_strLegalName = value;
			}
		}

		/// <summary>
		/// The identifier of the organisation unit
		/// </summary>
		public string Identifier
		{
			get
			{
				return this.m_strIdentifier;
			}
			set
			{
				this.m_strIdentifier = value;
			}
		}

		/// <summary>
		/// The organisation unit is being created for the first time
		/// </summary>
		public bool IsNewOrganisation
		{
			get
			{
				return this.bolIsNewOrganisation;
			}
			set
			{
				this.bolIsNewOrganisation = value;
			}
		}

		/// <summary>
		/// The breadcrumb for the organisation unit
		/// </summary>
		public new string Breadcrumb
		{
			get
			{
				return this.m_strBreadcrumb;
			}
			set
			{
				this.m_strBreadcrumb = value;
			}
		}
	}
}
