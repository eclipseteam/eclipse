﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.Calculation;
using System.Data;

namespace Oritax.TaxSimp.DataSets
{
    [Serializable]
    public class PersistentORGUNITDS : BrokerManagedComponentDS
    {
        #region TABLE NAMES AND FIELDS

        public const string ORGANISATIONUNIT_TABLE              = "ORGANISATIONUNIT";
        public const string ORGUNIT_ID_FIELD                    = "ID";
        public const string ORGUNIT_ISINVESTMENTCLIENT_FIELD    = "ISINVESTMENTCLIENT";
        public const string ORGUNIT_ISEXPORT_FIELD              = "ISEXPORT";
        public const string ORGUNIT_ISEXPORTEDSS_FIELD          = "ISEXPORTEDSS";
        public const string ORGUNIT_APPLICATIONDATE_FIELD       = "APPLICATIONDATE";
        public const string ORGUNIT_APPLICATIONID_FIELD         = "APPLICATIONID";
        public const string ORGUNIT_CREATIONDATE_FIELD          = "CREATIONDATE";
        public const string ORGUNIT_UPDATEDATE_FIELD            = "UPDATEDATE";
        public const string ORGUNIT_ISCLASSSUPER_FIELD          = "ISCLASSSUPER";
        public const string ORGUNIT_ISBGL_FIELD                 = "ISBGL";
        public const string ORGUNIT_ISDESKTOPSUPER_FIELD        = "ISDESKTOPSUPER";
        public const string ORGUNIT_BGLACCOUNTCODE_FIELD        = "BGLACCOUNTCODE";
        public const string ORGUNIT_SUPERMEMBERID_FIELD         = "SUPERMEMBERID";
        public const string ORGUNIT_SUPERCLIENTID_FIELD         = "SUPERCLIENTID";

        #endregion

        #region CONSTRUCTORS

        /// <summary>
        ///		Constructor for ChartOfAccountsDS
        /// </summary>
        public PersistentORGUNITDS()
            : base()
        {
            // CHART_OF_ACCOUNTS TABLE=======================================================================
            DefineRootTable(
                ORGANISATIONUNIT_TABLE,
                new FieldDefinition[]{
										 new FieldDefinition(ORGUNIT_ID_FIELD,typeof(System.Guid),false),
										 new FieldDefinition(ORGUNIT_ISINVESTMENTCLIENT_FIELD,typeof(System.Boolean)),
										 new FieldDefinition(ORGUNIT_ISEXPORT_FIELD,typeof(System.Boolean)),
										 new FieldDefinition(ORGUNIT_ISEXPORTEDSS_FIELD,typeof(System.Boolean)),
										 new FieldDefinition(ORGUNIT_APPLICATIONDATE_FIELD,typeof(System.String)),
										 new FieldDefinition(ORGUNIT_APPLICATIONID_FIELD,typeof(System.String)),
										 new FieldDefinition(ORGUNIT_CREATIONDATE_FIELD,typeof(System.DateTime)),
 										 new FieldDefinition(ORGUNIT_UPDATEDATE_FIELD, typeof(System.DateTime)),
										 new FieldDefinition(ORGUNIT_ISCLASSSUPER_FIELD, typeof(System.Boolean)),
										 new FieldDefinition(ORGUNIT_ISBGL_FIELD, typeof(System.Boolean)),
                                         new FieldDefinition(ORGUNIT_ISDESKTOPSUPER_FIELD, typeof(System.Boolean)),
                                         new FieldDefinition(ORGUNIT_BGLACCOUNTCODE_FIELD, typeof(System.String)),
                                         new FieldDefinition(ORGUNIT_SUPERMEMBERID_FIELD, typeof(System.String)),
                                         new FieldDefinition(ORGUNIT_SUPERCLIENTID_FIELD,typeof(String)),   
				},
                ORGUNIT_ID_FIELD,
                PersistenceTypeOption.READWRITE
            );
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        #endregion

        protected DataTable createORGUNITTable()
        {
            DataTable dtORGUNI = new DataTable(ORGANISATIONUNIT_TABLE);
            dtORGUNI.Columns.Add(ORGUNIT_ID_FIELD, typeof(System.Guid));
            dtORGUNI.Columns.Add(ORGUNIT_ISINVESTMENTCLIENT_FIELD, typeof(System.Boolean));
            dtORGUNI.Columns.Add(ORGUNIT_ISEXPORT_FIELD, typeof(Boolean));
            dtORGUNI.Columns.Add(ORGUNIT_ISEXPORTEDSS_FIELD, typeof(Boolean));
            dtORGUNI.Columns.Add(ORGUNIT_APPLICATIONDATE_FIELD, typeof(DateTime));
            dtORGUNI.Columns.Add(ORGUNIT_APPLICATIONID_FIELD, typeof(System.String));
            dtORGUNI.Columns.Add(ORGUNIT_CREATIONDATE_FIELD, typeof(System.DateTime));
            dtORGUNI.Columns.Add(ORGUNIT_UPDATEDATE_FIELD, typeof(DateTime));
            dtORGUNI.Columns.Add(ORGUNIT_ISCLASSSUPER_FIELD, typeof(System.Boolean));
            dtORGUNI.Columns.Add(ORGUNIT_ISBGL_FIELD, typeof(System.Boolean));
            dtORGUNI.Columns.Add(ORGUNIT_ISDESKTOPSUPER_FIELD, typeof(Boolean));
            dtORGUNI.Columns.Add(ORGUNIT_BGLACCOUNTCODE_FIELD, typeof(string));
            dtORGUNI.Columns.Add(ORGUNIT_SUPERMEMBERID_FIELD, typeof(String));
            dtORGUNI.Columns.Add(ORGUNIT_SUPERCLIENTID_FIELD, typeof(string));
            dtORGUNI.Columns.Add(BrokerManagedComponentDS.NAME_FIELD, typeof(string));
            dtORGUNI.PrimaryKey = new DataColumn[] { dtORGUNI.Columns[ORGUNIT_ID_FIELD] };

            return dtORGUNI;
        }
    }

}
