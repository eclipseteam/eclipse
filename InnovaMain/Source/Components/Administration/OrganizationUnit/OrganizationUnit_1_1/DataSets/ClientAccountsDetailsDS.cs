﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.CM;

namespace Oritax.TaxSimp.DataSets
{
    public class ClientAccountsDetailsDS : UMABaseDS
    {
        public IClientEntity ClientEntity = null;
        public string AdviserID = string.Empty;
        public string AdviserFullName = string.Empty;
    }
}
