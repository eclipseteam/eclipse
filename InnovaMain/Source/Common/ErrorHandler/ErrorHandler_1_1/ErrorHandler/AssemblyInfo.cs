using System.Reflection;
using System.Runtime.CompilerServices;

using Oritax.TaxSimp.InstallInfo;
using Oritax.TaxSimp.ErrorHandler;

[assembly: AssemblyTitle("")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("")]
[assembly: AssemblyCopyright("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]	
[assembly: AssemblyVersion(ErrorHandlerInstall.ASSEMBLY_MAJORVERSION+"."+ErrorHandlerInstall.ASSEMBLY_MINORVERSION)]
[assembly: AssemblyInformationalVersion(ErrorHandlerInstall.ASSEMBLY_MAJORVERSION+"."+ErrorHandlerInstall.ASSEMBLY_MINORVERSION+"."+ErrorHandlerInstall.ASSEMBLY_DATAFORMAT+"."+ErrorHandlerInstall.ASSEMBLY_REVISION)]
[assembly: AssemblyInstallInfo(ErrorHandlerInstall.ASSEMBLY_ID,ErrorHandlerInstall.ASSEMBLY_NAME,ErrorHandlerInstall.ASSEMBLY_DISPLAYNAME)]
[assembly: ComponentInstallInfo(ErrorHandlerInstall.COMPONENT_ID,ErrorHandlerInstall.COMPONENT_NAME,ErrorHandlerInstall.COMPONENT_DISPLAYNAME,ErrorHandlerInstall.COMPONENT_CATEGORY)]
[assembly: ComponentVersionInstallInfo(ErrorHandlerInstall.COMPONENTVERSION_STARTDATE,ErrorHandlerInstall.COMPONENTVERSION_ENDDATE,ErrorHandlerInstall.COMPONENTVERSION_PERSISTERASSEMBLY,ErrorHandlerInstall.COMPONENTVERSION_PERSISTERCLASS,ErrorHandlerInstall.COMPONENTVERSION_IMPLEMENTATIONCLASS)]
[assembly: AssociatedFileInstallInfoAttribute(new string[]
{
	// FILE NAMES
	// aspx files
	ErrorHandlerInstall.ASSOCIATEDFILE_WP_ERRORPAGE_FILENAME,
}
,new string[]
{
	null
})]
[assembly: AssemblyDelaySign(false)]
[assembly:System.Web.UI.TagPrefix("TXPresentationControls", "tx")]