using System;

namespace Oritax.TaxSimp.ErrorHandler
{
	/// <summary>
	/// Summary description for ErrorHandlerInstall.
	/// </summary>
	public class ErrorHandlerInstall
	{
		#region INSTALLATION PROPERTIES

		// Assembly Installation Properties
		public const string ASSEMBLY_ID="B74E47B7-0F4C-4090-BD2B-C9F9831BF4AA";
		public const string ASSEMBLY_NAME="ErrorHandler_1_1";
		public const string ASSEMBLY_DISPLAYNAME="Error handler V1.1";
		public const string ASSEMBLY_MAJORVERSION="1";
		public const string ASSEMBLY_MINORVERSION="1";
		public const string ASSEMBLY_DATAFORMAT="0";
//		public const string ASSEMBLY_REVISION="0";
		public const string ASSEMBLY_REVISION="1"; //2005.1

		// Component Installation Properties
		public const string COMPONENT_ID="1EF5BD90-8999-4bd2-A976-79DD864FA332";
		public const string COMPONENT_NAME="ErrorHandler";
		public const string COMPONENT_DISPLAYNAME="Error handler";
		public const string COMPONENT_CATEGORY="Framework";

		// Component Version Installation Properties
		public const string COMPONENTVERSION_STARTDATE="01/01/1990 00:00:00 AM";
		public const string COMPONENTVERSION_ENDDATE="31/12/2050 12:59:59 AM";
		public const string COMPONENTVERSION_PERSISTERASSEMBLY="";
		public const string COMPONENTVERSION_PERSISTERCLASS="";
		public const string COMPONENTVERSION_IMPLEMENTATIONCLASS="Oritax.TaxSimp.ErrorHandler.ErrorHandlerBMC";

		// Associated File Installation Properties
		//		Workpapers
		public const string	ASSOCIATEDFILE_WP_ERRORPAGE_FILENAME="ErrorPage_1_1.aspx";

		#endregion
	}
}
