using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace Oritax.TaxSimp.ErrorHandler
{
	/// <summary>
	/// Summary description for ErrorPage.
	/// </summary>
	public class ErrorPage : System.Web.UI.Page
	{
		protected new System.Web.UI.HtmlControls.HtmlGenericControl Title;
		public const string CloseText = "Close";
		protected PresentationControls.NableHeader WPHeaderControl;
		protected System.Web.UI.WebControls.Label label_Message;
		public const string ReturnText = "Return";

		public String ReturnURL
		{
			get
			{
				if (ViewState["ReturnURL"] == null)
					return String.Empty;
				else
					return ViewState["ReturnURL"].ToString();
			}
			set
			{
				ViewState["ReturnURL"] = value;
			}

		}



		private void Page_Load(object sender, System.EventArgs e)
		{
			if(!Page.IsPostBack)
			{
				if(Request.Params["Heading"] == null)
				{
					WPHeaderControl.HeaderText = "Information";
				}
				else
				{
					WPHeaderControl.HeaderText = (string)Request.Params["Heading"];
				}

				this.Title.InnerText = WPHeaderControl.HeaderText;

				if(Request.Params["Message"] == null)
				{
					label_Message.Text = "This line intentionally left blank";
				}
				else
				{
					label_Message.Text = (string)Request.Params["Message"];
				}
				if(Request.Params["ReturnUrl"] == null)
					this.ReturnURL = CloseText;
				else
				{
					string returnURL = (string)Request.Params["ReturnUrl"];
					if( returnURL != CloseText && returnURL  != String.Empty)
						 this.ReturnURL = returnURL;
					 else
						 this.ReturnURL = CloseText;
				}
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.WPHeaderControl.MenuCommand += new CommandEventHandler( this.WPHeaderControl_Close );
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void WPHeaderControl_Close(object sender, CommandEventArgs e)
		{
			if(this.ReturnURL == CloseText)
			{
				Response.Write("<script>window.close();</script>");
				Response.Flush();
			}
			else
			{
				Response.Redirect(this.ReturnURL);
			}
		}


	}
}
