<%@ Assembly Name="ErrorHandler_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="Footer_1_1.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="PresentationControls" Assembly="PresentationControls_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215" %>
<%@ Page language="c#" Codebehind="ErrorPage_1_1.aspx.cs" AutoEventWireup="false" Inherits="Oritax.TaxSimp.ErrorHandler.ErrorPage" %>
<%@ Register TagPrefix="PageLayout" Namespace="PresentationControls" Assembly="PresentationControls_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title runat="server" id="Title"></title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="./Styles/nable_1_1.css" type="text/css" rel="stylesheet">
		<script src="./scripts/nable_1_1.js" type="text/javascript"></script>
	</HEAD>
	<body>
		<form id="Error" method="post" runat="server">
			<P>
			<PageLayout:NableHeader id="WPHeaderControl" runat="server">
						<PageLayout:MenuButtons Type="CustomButtons"></PageLayout:MenuButtons>
						<PageLayout:MenuButtons Type="StandardButtons">
							<PageLayout:MenuButton id="buttonHelp" Enabled="True" Text="Help" ClientSideButton="True" OnClickScript="OpenWP_OnClick('Help_1_1.aspx')" DisableAll="False" CommandName="Help"></PageLayout:MenuButton>				
							<PageLayout:MenuButton id="buttonPrint" Enabled="True" Text="Print" ClientSideButton="True" OnClickScript="window.print();" DisableAll="False" CommandName="Print"></PageLayout:MenuButton>								
							<PageLayout:MenuButton id="buttonClose" Enabled="True" Text="Close" DisableAll="True" CausesValidation="True" CommandName="Close"></PageLayout:MenuButton>				
						</PageLayout:MenuButtons>
			</PageLayout:NableHeader></P>
			<P>
				<asp:label id="label_Message" runat="server" CssClass="body" font-size="12px">Message:</asp:label></P>
			<P>
				<uc1:Footer id="Footer1" runat="server"></uc1:Footer></P>
		</form>
	</body>
</HTML>
