﻿using System;
using System.Windows.Controls;
using C1.Silverlight.FlexGrid;
using Oritax.TaxSimp.Data;


namespace Oritax.TaxSimp.Controls4
{
    public class FlexGridTotalDecorator<TData, TItem> : GridDecorator<TData, TItem>
        where TData : class, IHasTotal<TItem>, IHasItems<TItem>, new()
        where TItem : class, IHasPrimaryKey, IHasDescription, new()
    {
        string _Description;

        public new C1FlexGrid Control { get { return (Clone as IGridControl).Grid as C1FlexGrid; } }

        public FlexGridTotalDecorator(ServiceGrid<TData, TItem> grid, string description)
            : base(grid)
        {
            _Description = description;
            FlexGridStyler.SetReadOnlyColumns(Control as C1FlexGrid);
        }

        protected override void Decorate(TData data)
        {
            data.Total.Description = _Description;
            Clone.Items.Clear();
            Clone.Items.Add(data.Total);
        }
    }


}
