﻿using System;
using System.Linq;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Collections.Generic;
using System.ServiceModel.DomainServices.Client;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;

namespace Oritax.TaxSimp.Controls4
{
    public class ServiceGrid<TData, TItem> 
        where TData : class, IHasItems<TItem>, new()
        where TItem : class, IHasPrimaryKey, new()

    {
        public IGrid<TItem> Grid { get; private set; }
        public int Type { get; private set; }

        public Func<string, Action<InvokeOperation<string>>, object, InvokeOperation<string>> Add { get; set; }
        public Func<string, Action<InvokeOperation<string>>, object, InvokeOperation<string>> Update { get; set; }
        public Func<string, Action<InvokeOperation<string>>, object, InvokeOperation<string>> Delete { get; set; }


        private event Action<TData> _Process_Completed;
        public event Action<TData> Process_Completed
        {
            add { _Process_Completed += value; }
            remove { _Process_Completed -= value; }
        }

        public ServiceGrid(IGrid<TItem> grid, int type)
        {
            Type = type;
            Grid = grid;
            Grid.Add_Begin += new Action<IGrid<TItem>, int>(Grid_Add_Begin);
            Grid.Update_Begin += new Action<IGrid<TItem>, TItem>(Grid_Update_Begin);
            Grid.Delete_Begin += new Action<IGrid<TItem>, IEnumerable<TItem>>(Grid_Delete_Begin);
        }

        public void GetData_Completed(string xml)
        {
            TData data = xml.ToData<TData>();
            Grid.GetCompleted(data.Items);
            OnProcessCompleted(data);
        }

        void Grid_Add_Begin(IGrid<TItem> sender, int index)
        {
            if (Add == null) return;
            TItem item = new TItem { PrimaryKey = Guid.NewGuid() };
            string xml = SerialzeRequest(Type, item);
            Add(xml, x =>
                {
                    TData data = DeserailzeResponse(x.Value);
                    Grid.Add_Ended(data.Items, index);
                    OnProcessCompleted(data);
                }, null);
        }

        void Grid_Update_Begin(IGrid<TItem> sender, TItem item)
        {
            if (Update == null) return;
            string xml = SerialzeRequest(Type, item);
            Update(xml, x =>
            {
                TData data = DeserailzeResponse(x.Value);
                Grid.Update_Ended(data.Items);
                OnProcessCompleted(data);
            }, null);
        }

        void Grid_Delete_Begin(IGrid<TItem> arg1, IEnumerable<TItem> items)
        {
            if (Delete == null) return;
            string xml = SerialzeRequest(Type, items.ToArray());
            Delete(xml, x =>
            {
                TData data = DeserailzeResponse(x.Value);
                Grid.Delete_Ended(data.Items);
                OnProcessCompleted(data);
            }, null);
        }

        public  Guid CLID { get; set; }
        public  Guid CSID { get; set; }

        private  string SerialzeRequest(int type, params TItem[] items)
        {
           
            Guid clid = CLID;
            Guid csid = CSID;

            BMCServiceDataItem bmc = new BMCServiceDataItem { Clid = clid, Csid = csid, Type = type };

            TData data = new TData();
            foreach (TItem each in items)
            {
                data.Items.Add(each);
            }
            bmc.Data = data.ToXmlString<TData>();
            return bmc.ToXmlString();
        }

        private static TData DeserailzeResponse(string xml)
        {
            if (string.IsNullOrWhiteSpace(xml)) return null;
            BMCServiceDataItem bmc = xml.ToData<BMCServiceDataItem>();
            return (bmc == null || string.IsNullOrWhiteSpace(bmc.Data)) ? null : bmc.Data.ToData<TData>();
        }

        private void OnProcessCompleted(TData data)
        {
            if (_Process_Completed == null) return;
            _Process_Completed(data); 
        }
    }

}
