﻿using System;
using System.Windows.Controls;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.Controls4
{
    public abstract class GridDecorator<TData, TItem>
            where TData : class, IHasItems<TItem>, new()
            where TItem : class, IHasPrimaryKey, IHasDescription, new()
    {
        protected IGrid<TItem> Clone { get; private set;}
        public Control Control { get { return (Clone as IGridControl).Grid; } }

        public GridDecorator(ServiceGrid<TData, TItem> grid)
        {
            Clone = grid.Grid.Clone();
            grid.Process_Completed += d => Decorate(d);
        }

        protected abstract void Decorate(TData data);
    }




}
