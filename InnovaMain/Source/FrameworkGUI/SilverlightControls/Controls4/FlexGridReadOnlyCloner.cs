﻿using System.Windows.Controls;
using C1.Silverlight.FlexGrid;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.Controls4
{
    //public class GridCloner<T>
    //    where T : class, IHasPrimaryKey
    //{
    //    IGrid<T> _Clone;
    //    public Control Control { get { return (_Clone as IGridControl).Grid; } }

    //    public GridCloner(IGrid<T> grid)
    //    {
    //        _Clone = grid.Clone();
    //    }

    //    public void Update(T item)
    //    {
    //        _Clone.Items.Clear();
    //        _Clone.Items.Add(item);
    //    }
    //}

    public class FlexGridReadOnlyCloner<T>
        where T : class, IHasPrimaryKey
    {
        IGrid<T> _Clone;
        public C1FlexGrid Control { get { return (_Clone as IGridControl).Grid as C1FlexGrid; } }

        public FlexGridReadOnlyCloner(IGrid<T> grid)
        {
            _Clone = grid.Clone();
            FlexGridStyler.SetReadOnlyColumns(Control);
        }

        public void Update(T item)
        {
            _Clone.Items.Clear();
            _Clone.Items.Add(item);
        }
    }
}
