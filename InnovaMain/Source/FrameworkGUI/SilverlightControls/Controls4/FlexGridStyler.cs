﻿using System;
using System.Windows;
using System.Windows.Media;
using C1.Silverlight.FlexGrid;

namespace Oritax.TaxSimp.Controls4
{
    public static class FlexGridStyler
    {
        public static void SetReadOnlyColumns(C1FlexGrid grid)
        {
            var resource = new ResourceDictionary();
            resource.Source = new Uri("/SilverlightControls_1_1;component/Controls4/GridResource.xaml", UriKind.Relative);
            SolidColorBrush color = resource["ReadOnlyColor"] as SolidColorBrush;

            foreach (var each in grid.Columns)
            {
                each.Background = color;
                each.IsReadOnly = true;
            }
        }
    }
}
