﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Controls;
using Oritax.TaxSimp.Extensions;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using C1.Silverlight.FlexGrid;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.Controls4
{
    public class C1FlexGrid_Proxy<T> : IGrid<T>, IGridControl
        where T : class, IHasPrimaryKey
    {
        private C1FlexGrid _Grid;

        public IList<T> Items { get; private set; }

        public event Action<IGrid<T>, int> Add_Begin;
        public event Action<IGrid<T>, T> Update_Begin;
        public event Action<IGrid<T>, IEnumerable<T>> Delete_Begin;

        Control IGridControl.Grid { get { return _Grid; } }

        public C1FlexGrid_Proxy(C1FlexGrid grid)
        {
            Items = new ObservableCollection<T>();
            _Grid = grid;
            _Grid.ItemsSource = Items;
            _Grid.CellEditEnding += new EventHandler<CellEditEventArgs>(_Grid_CellEditEnding);
            _Grid.KeyDown += (s, e) => { if (e.Key == Key.Insert) KeyDown_Insert(); };
            _Grid.KeyDown += (s, e) => { if (e.Key == Key.Delete) KeyDown_Delete(); };
        }

        private void _Grid_CellEditEnding(object sender, CellEditEventArgs e)
        {
            if (Update_Begin == null) return;
            Row row = _Grid.Rows[e.Row];
            T t = row.DataItem as T;
            _Grid.Dispatcher.BeginInvoke(() =>
            {
                Update_Begin(this, t);
            });

        }

        private void KeyDown_Insert()
        {
            if (Add_Begin == null) return;
            T selected = _Grid.Rows[_Grid.Selection.BottomRow].DataItem as T;
            int index = Items.FindIndex<T>(selected);

            _Grid.Dispatcher.BeginInvoke(() =>
            {
                Add_Begin(this, index);
            });
        }

        private void KeyDown_Delete()
        {
            if (Delete_Begin == null) return;
            _Grid.Dispatcher.BeginInvoke(() =>
            {
                Delete_Begin(this, _Grid.Rows.GetDataItems(_Grid.Selection).OfType<T>());
            });
        }

        public void GetCompleted(IEnumerable<T> items)
        {
            Items.Clear();
            foreach (T each in items)
            {
                Items.Add(each);
            }
        }

        public void Add_Ended(IEnumerable<T> items, int index)
        {
            foreach (T each in items)
            {
                if (Items.Count <= 0)
                {
                    Items.Add(each);
                }
                else
                {
                    Items.Insert(++index, each);
                }
            }
        }

        public void Update_Ended(IEnumerable<T> items)
        {
            foreach (T each in items)
            {
                int index = Items.FindIndex<T>(each);
                Items.RemoveAt(index);
                Items.Insert(index, each);
            }
        }

        public void Delete_Ended(IEnumerable<T> items)
        {
            foreach (T each in items)
            {
                T selected = Items.Find<T>(each);
                Items.Remove(selected);
            }
        }

        public IGrid<T> Clone()
        {
            C1FlexGrid source = _Grid;
            C1FlexGrid target = new C1FlexGrid();

            target.HorizontalAlignment = source.HorizontalAlignment;
            target.Width = source.Width;
            target.Style = source.Style;
            //target.MinHeight = source.MinHeight;
            target.AutoGenerateColumns = source.AutoGenerateColumns;
            Column column = null;
            foreach (var each in source.Columns)
            {
                column = new Column { ColumnName = each.ColumnName, Width = each.Width, HorizontalAlignment = each.HorizontalAlignment };
                //column.Background = Application.Current.Resources["ReadOnlyColor"] as SolidColorBrush;
                column.Format = each.Format;
                if (each.Binding != null) column.Binding = new Binding { Path = each.Binding.Path, Mode = BindingMode.OneWay };
                target.Columns.Add(column);
            }

            target.HeadersVisibility = HeadersVisibility.Row;

            return new C1FlexGrid_Proxy<T>(target);
        }
    }
}
