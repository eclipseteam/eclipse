﻿using System;
using System.Windows.Controls;
using System.Collections.Generic;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.Controls4
{
    public interface IGrid<T>
        where T : class, IHasPrimaryKey
    {
        IList<T> Items { get; }
        void GetCompleted(IEnumerable<T> items);
        
        event Action<IGrid<T>, int> Add_Begin;
        event Action<IGrid<T>, T> Update_Begin;
        event Action<IGrid<T>, IEnumerable<T>> Delete_Begin;
        
        void Add_Ended(IEnumerable<T> t, int index);
        void Update_Ended(IEnumerable<T> t);
        void Delete_Ended(IEnumerable<T> t);

        IGrid<T> Clone();
    }

    public interface IGridControl
    {
        Control Grid { get; }
    }

}
