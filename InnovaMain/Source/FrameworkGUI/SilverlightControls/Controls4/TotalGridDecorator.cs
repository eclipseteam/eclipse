﻿using System;
using System.Windows.Controls;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.Controls4
{
    public class TotalGridDecorator<TData, TItem> : GridDecorator<TData, TItem>
        where TData : class, IHasTotal<TItem>, IHasItems<TItem>, new()
        where TItem : class, IHasPrimaryKey, IHasDescription, new()
    {
        string _Description;

        public TotalGridDecorator(ServiceGrid<TData, TItem> grid, string description)
            : base(grid)
        {
            _Description = description;
        }

        protected override void Decorate(TData data)
        {
            data.Total.Description = _Description;
            Clone.Items.Clear();
            Clone.Items.Add(data.Total);
        }
    }
}
