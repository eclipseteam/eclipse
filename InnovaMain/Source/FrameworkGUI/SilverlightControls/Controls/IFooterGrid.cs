﻿using System;
using System.ServiceModel.DomainServices.Client;

namespace Oritax.TaxSimp.Controls
{
    public interface IFooterGrid
    {
        Func<string, Action<InvokeOperation<string>>, object, InvokeOperation<string>> Add_Action { get; set; }
        Func<string, Action<InvokeOperation<string>>, object, InvokeOperation<string>> Update_Action { get; set; }
        Func<string, Action<InvokeOperation<string>>, object, InvokeOperation<string>> Delete_Action { get; set; }
    }
}
