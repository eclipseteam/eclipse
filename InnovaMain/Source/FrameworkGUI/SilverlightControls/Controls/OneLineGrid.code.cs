﻿#region Using NameSpaces
using System;
using System.Linq;
using System.Windows;
using System.Diagnostics;
using System.Windows.Input;
using System.Windows.Browser;
using System.Windows.Threading;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using C1.Silverlight.FlexGrid;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;
#endregion

namespace Oritax.TaxSimp.Controls
{
    public partial class OneLineGrid<TData, TItem>
        where TData : class, IHasItem<TItem>, new()
        where TItem : class, IHasPrimaryKey, IHasDescription, new()
    {




        public Func<string, Action<InvokeOperation<string>>, object, InvokeOperation<string>> Update_Action { get; set; }
        public Action<TData> Process_Completed { get; set; }
        public Action<C1FlexGrid> GotFocus { get; set; }

        public int Type { get; private set; }
        public List<FooterColumn> Columns { get; private set; }

        #region Public Members---------------------------------------------------------------

        public string Title
        {
            get { return TextBlockTitle.Text; }
            set { TextBlockTitle.Text = value; }
        }

        public bool IsTitleVisible
        {
            set
            {
                if (value)
                {
                    TextBlockTitle.Margin = new Thickness(5);
                    TextBlockTitle.Visibility = Visibility.Visible;
                }
                else
                {
                    TextBlockTitle.Margin = new Thickness(0);
                    TextBlockTitle.Visibility = Visibility.Collapsed;
                }
            }
            get
            {
                return TextBlockTitle.Visibility == Visibility.Visible;
            }
        }

        public bool IsEnabled
        {
            set
            {
                FlexGridFooter.IsEnabled = value;
            }
            get
            {
                return FlexGridFooter.IsEnabled;
            }
        }

        public GridPanel ColumnHeaders
        {
            get
            {
                return FlexGridFooter.ColumnHeaders;
            }
        }

        public HeadersVisibility HeadersVisibility
        {
            set
            {
                FlexGridFooter.HeadersVisibility = value;
            }
            get
            {
                return FlexGridFooter.HeadersVisibility;
            }
        }

        #endregion

        #region Public Constructor---------------------------------------------------------------

        public OneLineGrid(int type)
        {
            InitializeComponent();
            Type = type;
            Columns = new List<FooterColumn>();
            FlexGridFooter.CellEditEnding += new EventHandler<CellEditEventArgs>(FlexGridFooter_CellEditEnded);
            FlexGridFooter.GotFocus += new RoutedEventHandler(FlexGridFooter_GotFocus);
        }

        void FlexGridFooter_GotFocus(object sender, RoutedEventArgs e)
        {
            if (FlexGridFooter.IsEnabled == true && FlexGridFooter.IsReadOnly == false)
                GotFocus(FlexGridFooter);
        }

        #endregion

        #region Update Grid ---------------------------------------------------------------

        public void Update(TItem value)
        {
            _Items.Clear();
            _Items.Add(value);
        }

        public void Update(BMCServiceDataItem bmc)
        {
            ProcessBmcItem(bmc);
        }

        #endregion



        void FlexGridFooter_CellEditEnded(object sender, CellEditEventArgs e)
        {
            if (e.Row == FlexGridFooter.Rows.Count)
            {
                return;
            }

            Row row = FlexGridFooter.Rows[e.Row];
            TItem item = row.DataItem as TItem;
            Dispatcher.BeginInvoke(new Action(() =>
            {
                UpdateItem(item);
            }));
        }

        private void UpdateItem(TItem item)
        {
            string xml = CreateRequestXml(item);

            if (Update_Action != null)
            {
                Update_Action(xml, x => ProcessResponseXml(x.Value, Update_Completed), null);
            }
        }


        private void ProcessBmcItem(BMCServiceDataItem bmc)
        {
            if (bmc == null || string.IsNullOrWhiteSpace(bmc.Data)) return;
            TData data = bmc.Data.ToData<TData>();
            if (data == null) return;

            _Items.Clear();
            _Items.Add(data.Item);

            if (Process_Completed != null)
            {
                Process_Completed(data);
            }

        }

        private string CreateRequestXml(params TItem[] items)
        {
            return CreateRequestXml2(items);
        }

        public String CLID { get; set; }
        public String CSID { get; set; }

        private string CreateRequestXml2(IEnumerable<TItem> items)
        {
            Guid clid = new Guid(CLID);
            Guid csid = new Guid(CSID);
            BMCServiceDataItem bmc = new BMCServiceDataItem { Clid = clid, Csid = csid, Type = Type };

            TItem data = new TItem();
            foreach (TItem each in items)
            {
                data = each;
            }

            bmc.Data = data.ToXmlString<TItem>();
            return bmc.ToXmlString();
        }

        private void ProcessResponseXml(string xml, Action<TItem> action)
        {
            if (string.IsNullOrWhiteSpace(xml)) return;
            BMCServiceDataItem bmc = xml.ToData<BMCServiceDataItem>();
            ProcessBmcItem(bmc);
        }


        private void Update_Completed(TItem list)
        {
            int index = _Items.FindIndex<TItem>(list);
            _Items.RemoveAt(index);
            _Items.Insert(index, list);

        }

    }
}
