﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using C1.Silverlight.FlexGrid;
using System.Windows.Data;
using System.Collections.ObjectModel;

namespace Oritax.TaxSimp.Controls
{
    public partial class FooterGrid : UserControl 
    {
        public string Title
        {
            get { return TextBlockTitle.Text; }
            set { TextBlockTitle.Text = value; }
        }

        public List<FooterColumn> Columns { get; private set; }

        public FooterGrid()
        {
            InitializeComponent();
            Columns = new List<FooterColumn>();
        }

        public void InitializeColumns()
        {
            foreach (FooterColumn each in Columns)
            {
                Column column = new Column();
                column.ColumnName = each.ColumnName;
                column.Header = each.Header;
                column.Binding = new Binding(each.BindingPath) { Mode = BindingMode.TwoWay };
                column.HeaderHorizontalAlignment = each.HeaderHorizontalAlignment;
                column.Width = new GridLength(each.Width);
                column.HorizontalAlignment = each.HorizontalAlignment;
                column.AllowSorting = each.AllowSorting;
                column.AllowDragging = column.AllowMerging = column.AllowResizing = false;

                FlexGridData.Columns.Add(column);
                FlexGridData.HorizontalScrollBarVisibility = ScrollBarVisibility.Hidden;
                FlexGridData.AutoGenerateColumns = false;

                FlexGridFooter.Columns.Add(column);
                FlexGridFooter.HorizontalScrollBarVisibility = ScrollBarVisibility.Hidden;
                FlexGridFooter.HeadersVisibility = HeadersVisibility.Row;

            }
        }

        public Action Add_Completed { get; set; }


        private void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {

        }

        private void ButtonUpdate_Click(object sender, RoutedEventArgs e)
        {

        }

        private void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {

        }
    }

    public class FooterColumn
    {
        public string ColumnName { get; set; }
        public string Header { get; set; }
        public string BindingPath { get; set; }
        public HorizontalAlignment HeaderHorizontalAlignment { get; set; }
        public double Width { get; set; }
        public HorizontalAlignment HorizontalAlignment { get; set; }
        public bool AllowSorting { get; set; }
        public bool IsReadOnly { get; set; }
    }
}
