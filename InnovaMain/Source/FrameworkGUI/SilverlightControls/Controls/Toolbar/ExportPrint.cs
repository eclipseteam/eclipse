﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using C1.C1Zip;
using C1.Silverlight.FlexGrid;
using C1.Silverlight.Pdf;
using System.Collections.ObjectModel;
using System.Windows.Media;
using System.Windows.Printing;

namespace Oritax.TaxSimp.Controls
{
    public class ExportPrint
    {
        //-------------------------------------------------------------------------------------------------------
        #region ** Private Members
        public List<FrameworkElement> Controls = new List<FrameworkElement>();
        private string _DocumentTitle;
        #endregion

        //-------------------------------------------------------------------------------------------------------
        #region ** Public Members

        public double Margin { get; set; }
        public ScaleMode ScaleMode { get; set; }

        #endregion

        //-------------------------------------------------------------------------------------------------------
        #region ** Constructor

        public ExportPrint(string DocumentTitle)
        {
            Margin = 50;
            ScaleMode = ScaleMode.PageWidth;
            _DocumentTitle = DocumentTitle;
        }

        #endregion

        //-------------------------------------------------------------------------------------------------------
        #region ** object model

        public void RenderText()
        {
            RenderText(" ");
        }
        public void RenderText(string text)
        {
            TextBlock txtBlockBR = new TextBlock();
            txtBlockBR.VerticalAlignment = VerticalAlignment.Bottom;
            txtBlockBR.Margin = new Thickness(5);
            txtBlockBR.Text = text;
            Controls.Add(txtBlockBR);
        }
        public void RenderGrid(C1FlexGrid flexGrid)
        {
            Controls.Add(flexGrid);
        }

        public void Print()
        {
            var pd = new System.Windows.Printing.PrintDocument();
            pd.PrintPage += pd_PrintPage;
            pd.Print(_DocumentTitle);

        }
        void pd_PrintPage(object sender, PrintPageEventArgs e)
        {
            // calculate page size, discounting margins
            var sz = e.PrintableArea;
            sz.Width -= 2 * Margin;
            sz.Height -= 2 * Margin;

            var pageTemplate = new PageTemplate();

            // set margins
            pageTemplate.SetPageMargin(new Thickness(Margin));

            pageTemplate.HeaderLeft.Text = _DocumentTitle;

            // set content
            foreach (UIElement control in Controls)
            {
                if (control.GetType().ToString().Contains("C1FlexGrid"))
                {
                    C1FlexGrid grid = (C1FlexGrid)control;
                    foreach (var page in grid.GetPageImages(ScaleMode, sz, 100))
                    {
                        pageTemplate.PageContent.Children.Add(page);
                    }
                }
                else
                {
                    pageTemplate.PageContent.Children.Add(control);
                };


            }

            // render this page
            e.PageVisual = pageTemplate;

            // move on to next page
            e.HasMorePages = false;
        }

        #endregion
    }
}
