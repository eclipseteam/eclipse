﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
using C1.Silverlight.FlexGrid;

namespace Oritax.TaxSimp.Controls
{
    public partial class Toolbar : UserControl, INotifyPropertyChanged
    {


        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        public Toolbar()
        {
            InitializeComponent();
        }

        
        //-----------------------------------------------------------------------------
        #region ** file

        private void _btnSaveCsv_Click(object sender, RoutedEventArgs e)
        {
            PropertyChanged(sender, new PropertyChangedEventArgs("btnSaveCsv"));
        }

        private void _btnSaveText_Click(object sender, RoutedEventArgs e)
        {
            PropertyChanged(sender, new PropertyChangedEventArgs("btnSaveText"));
        }

        private void _btnSaveHtml_Click(object sender, RoutedEventArgs e)
        {
            PropertyChanged(sender, new PropertyChangedEventArgs("btnSaveHtml"));
        }

        private void _btnSavePdf_Click(object sender, RoutedEventArgs e)
        {
            PropertyChanged(sender, new PropertyChangedEventArgs("btnSavePdf"));
        }

        #endregion

        //-----------------------------------------------------------------------------
        #region ** clipboard

        private void _btnPaste_Click(object sender, RoutedEventArgs e)
        {
            PropertyChanged(sender, new PropertyChangedEventArgs("btnPaste"));
        }

        private void _btnCut_Click(object sender, RoutedEventArgs e)
        {
            PropertyChanged(sender, new PropertyChangedEventArgs("btnCut"));
        }

        private void _btnCopy_Click(object sender, RoutedEventArgs e)
        {
            PropertyChanged(sender, new PropertyChangedEventArgs("btnCopy"));
        }

        #endregion

        //-----------------------------------------------------------------------------
        #region ** print
        private void _btnPrint_Click(object sender, RoutedEventArgs e)
        {

            //if (sender.Equals(_btnPrintPageWidth))
            //{
            //    PropertyChanged(sender, new PropertyChangedEventArgs("btnPrintPageWidth"));
            //}
            //else if (sender.Equals(_btnPrintSinglePage))
            //{
            //    PropertyChanged(sender, new PropertyChangedEventArgs("btnPrintSinglePage"));
            //}
            //else if (sender.Equals(_btnPrintSelection))
            //{
            //    PropertyChanged(sender, new PropertyChangedEventArgs("btnPrintSelection"));
            //}
            //else
            //{
            //    PropertyChanged(sender, new PropertyChangedEventArgs("btnPrintPageWidth"));
            //}

            PropertyChanged(sender, new PropertyChangedEventArgs("btnPrintPageWidth"));
        }
        #endregion

        //-----------------------------------------------------------------------------
        #region ** view

        // toggle gridline visibility
        private void _chkGridlines_Click(object sender, RoutedEventArgs e)
        {
            //if (_chkGridlines.IsChecked == true)
            //{
            //    PropertyChanged(sender, new PropertyChangedEventArgs("chkGridlinesShow"));
            //}
            //else
            //{
            //    PropertyChanged(sender, new PropertyChangedEventArgs("chkGridlinesHide"));
            //}
        }

        // toggle heading visibility
        private void _chkHeadings_Click(object sender, RoutedEventArgs e)
        {
            //if (_chkHeadings.IsChecked == true)
            //{
            //    PropertyChanged(sender, new PropertyChangedEventArgs("chkHeadingsShow"));
            //}
            //else
            //{
            //    PropertyChanged(sender, new PropertyChangedEventArgs("chkHeadingsHide"));
            //}

        }

        #endregion




    }
}
