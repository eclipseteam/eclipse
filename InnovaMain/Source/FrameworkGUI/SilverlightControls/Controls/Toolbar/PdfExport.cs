﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using C1.C1Zip;
using C1.Silverlight.FlexGrid;
using C1.Silverlight.Pdf;
using System.Collections.ObjectModel;
using System.Windows.Media;

namespace Oritax.TaxSimp.Controls
{


    public class PdfExport
    {


        //-------------------------------------------------------------------------------------------------------
        #region ** Private Members

        private C1PdfDocument pdf = null;
        private PageTemplate pageTemplate = null;
        private Panel GridContent = null;

        #endregion

        //-------------------------------------------------------------------------------------------------------
        #region ** Public Members

        public Thickness Margin { get; set; }
        public ScaleMode ScaleMode { get; set; }

        #endregion


        public PdfExport(Panel GridContent)
        {

            this.GridContent = GridContent;

            pageTemplate = new PageTemplate();
            pdf = new C1PdfDocument();

            Margin = new Thickness(96);
            ScaleMode = ScaleMode.PageWidth;

            pageTemplate.Width = pdf.PageRectangle.Width;
            pageTemplate.Height = pdf.PageRectangle.Height;
            pageTemplate.SetPageMargin(Margin);


        }

        //-------------------------------------------------------------------------------------------------------
        #region ** object model

        public void RenderHeader(string DocumentTitle)
        {
            pageTemplate.HeaderLeft.Text = DocumentTitle;
        }

        public void RenderGrid(C1FlexGrid flex)
        {

            // get root element to lay out the PDF pages
            Panel root = null;
            for (var parent = flex.Parent as FrameworkElement; parent != null; parent = parent.Parent as FrameworkElement)
            {
                if (parent is Panel)
                {
                    root = parent as Panel;
                }
            }

            root.Children.Add(pageTemplate);

            // render grid into PDF document
            var sz = new Size(pdf.PageRectangle.Width - Margin.Left - Margin.Right, pdf.PageRectangle.Height - Margin.Top - Margin.Bottom);
            var pages = flex.GetPageImages(ScaleMode, sz, 100);
            for (int i = 0; i < pages.Count; i++)
            {
                // set content
                pageTemplate.PageContent.Children.Add(pages[i]);

                // measure page element
                pageTemplate.Measure(new Size(pdf.PageRectangle.Width, pdf.PageRectangle.Height));
                pageTemplate.UpdateLayout();

                // add to PDF
                if (i > 0)
                {
                    pdf.NewPage();
                }
                pdf.DrawElement(pageTemplate, pdf.PageRectangle);
            }

            // done with template
            root.Children.Remove(pageTemplate);
        }



        public void RenderText()
        {
            RenderText(" ");
        }
        public void RenderText(string text)
        {

            TextBlock txtBlockBR = new TextBlock();
            txtBlockBR.VerticalAlignment = VerticalAlignment.Bottom;
            txtBlockBR.Margin = new Thickness(5);
            txtBlockBR.Text = text;

            GridContent.Children.Add(pageTemplate);

            // set content
            pageTemplate.PageContent.Children.Add(txtBlockBR);

            // measure page element
            pageTemplate.UpdateLayout();
            pdf.DrawElement(pageTemplate, pdf.PageRectangle);

            // done with template
            GridContent.Children.Remove(pageTemplate);


        }



        public void Save(Stream s)
        {

            // render footers
            // this reopens each page and adds content to them (now we know the page count).
            var font = new Font("Arial", 8, PdfFontStyle.Bold);
            var fmt = new StringFormat();
            fmt.Alignment = HorizontalAlignment.Right;
            fmt.LineAlignment = VerticalAlignment.Bottom;
            for (int page = 0; page < pdf.Pages.Count; page++)
            {
                pdf.CurrentPage = page;
                var text = string.Format("page {0} of {1}", page + 1, pdf.Pages.Count);
                pdf.DrawString(text, font, Colors.DarkGray, PdfUtils.Inflate(pdf.PageRectangle, -72, -36), fmt);
            }

            pdf.Save(s);
            s.Close();
        }



        #endregion
    }
}
