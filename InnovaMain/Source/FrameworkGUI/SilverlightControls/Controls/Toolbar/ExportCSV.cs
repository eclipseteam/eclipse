﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using C1.C1Zip;
using C1.Silverlight.FlexGrid;
using C1.Silverlight.Pdf;
using System.Collections.ObjectModel;
using System.Windows.Media;

namespace Oritax.TaxSimp.Controls
{
    public class ExportCSV
    {
        //-------------------------------------------------------------------------------------------------------
        #region ** Private Members

        private Stream stream = null;
        private StreamWriter streamwriter = null;

        #endregion

        //-------------------------------------------------------------------------------------------------------
        #region ** Constructor

        public ExportCSV(Stream _stream, StreamWriter _streamwriter)
        {
            stream = _stream;
            streamwriter = _streamwriter;
        }

        #endregion

        //-------------------------------------------------------------------------------------------------------
        #region ** object model

        public void RenderHeader(string DocumentTitle)
        {
            streamwriter.WriteLine(DocumentTitle);
            streamwriter.WriteLine("");
            streamwriter.Flush();
        }

        public void RenderGrid(C1FlexGrid flex)
        {
            flex.Save(stream, C1.Silverlight.FlexGrid.FileFormat.Csv);
        }

        public void RenderText(string text)
        {
            streamwriter.WriteLine(text);
            streamwriter.Flush();
        }

        public void RenderText()
        {
            RenderText(" ");
        }

        #endregion
    }
}
