﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using C1.C1Zip;
using C1.Silverlight.FlexGrid;
using C1.Silverlight.Pdf;
using System.Collections.ObjectModel;
using System.Windows.Media;

namespace Oritax.TaxSimp.Controls
{
    public class ExportHTML
    {
        //-------------------------------------------------------------------------------------------------------
        #region ** Private Members

        private Stream stream = null;
        private StreamWriter streamwriter = null;

        #endregion

        //-------------------------------------------------------------------------------------------------------
        #region ** Constructor

        public ExportHTML(Stream _stream, StreamWriter _streamwriter)
        {
            stream = _stream;
            streamwriter = _streamwriter;
        }

        #endregion

        //-------------------------------------------------------------------------------------------------------
        #region ** object model

        public void RenderHeader(string DocumentTitle)
        {
            streamwriter.WriteLine("<html>");
            streamwriter.WriteLine("<body bgcolor=\"#eff0f0\">");
            streamwriter.WriteLine("<table width='100%'><tr><td>");
            streamwriter.WriteLine(string.Format("<h1>{0}</h1>", DocumentTitle));
            streamwriter.WriteLine("</td><td align='right'>");
            streamwriter.WriteLine(string.Format("{0}", string.Format("{0}", String.Format("{0:F}", DateTime.Now))));
            streamwriter.WriteLine("</td></tr></table>");
            streamwriter.Flush();
        }

        public void RenderGrid(C1FlexGrid flex)
        {

            flex.Save(stream, C1.Silverlight.FlexGrid.FileFormat.Html);
        }

        public void RenderFooter()
        {
            streamwriter.WriteLine("</body>");
            streamwriter.WriteLine("</html>");
            streamwriter.Flush();
        }

        public void RenderText(string text)
        {
            streamwriter.WriteLine(string.Format("<p>{0}</p>", text));
            streamwriter.Flush();
        }

        public void RenderText()
        {
            RenderText("<br/>");
        }

        #endregion
    }
}
