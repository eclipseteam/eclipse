﻿using System;
using System.IO;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Text;
using System.Globalization;
using C1.Silverlight.Pdf;
using C1.Silverlight.FlexGrid;

namespace Oritax.TaxSimp.Controls
{

    public static class ToolbarAction
    {

        public static void DoAction(C1FlexGrid flex, string ActionName)
        {

            switch (ActionName)
            {
                //-----------------------------------------------------------------------------
                #region ** file
                case "btnSaveCsv":
                    SaveCsv(flex);
                    break;
                case "btnSaveText":
                    SaveText(flex);
                    break;
                case "btnSaveHtml":
                    SaveHtml(flex);
                    break;
                #endregion

                //-----------------------------------------------------------------------------
                #region ** clipboard
                case "btnPaste":
                    Paste(flex);
                    break;
                case "btnCut":
                    Cut(flex);
                    break;
                case "btnCopy":
                    Copy(flex);
                    break;
                #endregion

                //-----------------------------------------------------------------------------
                #region ** print
                case "btnPrintPageWidth":
                    Print(flex, "btnPrintPageWidth");
                    break;
                case "btnPrintSinglePage":
                    Print(flex, "btnPrintSinglePage");
                    break;
                case "btnPrintSelection":
                    Print(flex, "btnPrintSelection");
                    break;
                #endregion

                //---------------------------*--------------------------------------------------
                #region ** view
                case "chkGridlinesShow":
                    ShowHideGridlines(flex, true);
                    break;
                case "chkGridlinesHide":
                    ShowHideGridlines(flex, false);
                    break;
                case "chkHeadingsShow":
                    ShowHideHeadings(flex, true);
                    break;
                case "chkHeadingsHide":
                    ShowHideHeadings(flex, false);
                    break;
                #endregion
            }

        }

        //-----------------------------------------------------------------------------
        #region ** file

        private static void SaveCsv(C1FlexGrid flex)
        {
            var dlg = new SaveFileDialog();
            dlg.DefaultExt = "csv";
            dlg.Filter = "Comma Separated Values (*.csv)|*.csv";
            if (dlg.ShowDialog().Value)
            {
                flex.Save(dlg.OpenFile(), FileFormat.Csv);
            }
        }
        private static void SaveText(C1FlexGrid flex)
        {
            var dlg = new SaveFileDialog();
            dlg.DefaultExt = "txt";
            dlg.Filter = "Text files (*.txt)|*.txt";
            if (dlg.ShowDialog().Value)
            {
                flex.Save(dlg.OpenFile(), FileFormat.Text);
            }

        }
        private static void SaveHtml(C1FlexGrid flex)
        {
            var dlg = new SaveFileDialog();
            dlg.DefaultExt = "html";
            dlg.Filter = "HTML files (*.htm;*.html)|*.htm;*.html";
            if (dlg.ShowDialog().Value)
            {
                flex.Save(dlg.OpenFile(), FileFormat.Html);
            }

        }

        #endregion

        //-----------------------------------------------------------------------------
        #region ** clipboard

        private static void Paste(C1FlexGrid flex)
        {
            if (flex.IsEnabled == false || flex.IsReadOnly == true)
                return;

            flex.ClipboardPasteMode = ClipboardCopyMode.ExcludeHeader;
            flex.Paste();
        }

        private static void Cut(C1FlexGrid flex)
        {
            if (flex.IsEnabled == false || flex.IsReadOnly == true)
                return;

            flex.ClipboardCopyMode = ClipboardCopyMode.ExcludeHeader;
            flex.Copy();
            foreach (var cell in flex.Selection.Cells)
            {
                try
                {
                    if (flex.Columns[cell.Column].IsReadOnly == false)
                        flex[cell.Row, cell.Column] = null;
                }
                catch { }
            }
        }

        private static void Copy(C1FlexGrid flex)
        {
            flex.ClipboardCopyMode = ClipboardCopyMode.ExcludeHeader;
            flex.Copy();
        }

        #endregion

        //-----------------------------------------------------------------------------
        #region ** print
        private static void Print(C1FlexGrid flex, String scaleMode)
        {
            var _scaleMode = ScaleMode.PageWidth;

            if (scaleMode == "btnPrintPageWidth")
            {
                _scaleMode = ScaleMode.PageWidth;
            }
            else if (scaleMode == "btnPrintSinglePage")
            {
                _scaleMode = ScaleMode.SinglePage;
            }
            else if (scaleMode == "btnPrintSelection")
            {
                _scaleMode = ScaleMode.Selection;
            }

            flex.Print("C1FlexGrid", _scaleMode, new Thickness(96), 20);
        }

        #endregion

        //-----------------------------------------------------------------------------
        #region ** view

        private static void ShowHideGridlines(C1FlexGrid flex, bool Show)
        {
            if (Show)
            {
                flex.GridLinesVisibility = GridLinesVisibility.All;
            }
            else
            {
                flex.GridLinesVisibility = GridLinesVisibility.None;
            }

        }

        private static void ShowHideHeadings(C1FlexGrid flex, bool Show)
        {
            if (Show)
            {
                flex.HeadersVisibility = HeadersVisibility.All;
            }
            else
            {
                flex.HeadersVisibility = HeadersVisibility.None;
            }

        }

        #endregion

    }
}
