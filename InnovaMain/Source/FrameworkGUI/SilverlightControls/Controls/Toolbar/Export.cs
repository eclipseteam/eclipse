﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using C1.C1Zip;
using C1.Silverlight.FlexGrid;
using C1.Silverlight.Pdf;
using System.Collections.ObjectModel;
using System.Windows.Media;
using Oritax.TaxSimp.Controls4;
using Oritax.TaxSimp.Data;

namespace Oritax.TaxSimp.Controls
{
    public class Export
    {
        //-------------------------------------------------------------------------------------------------------
        #region ** Private Members

        private string _DocumentTitle;
        private List<RenderControl> Controls = new List<RenderControl>();

        private enum ControlType { Grid, Text, BreakLine };
        private struct RenderControl { public object Control; public ControlType ControlType; };

        #endregion

        //-------------------------------------------------------------------------------------------------------
        #region ** Constructor

        public Export(string DocumentTitle)
        {
            _DocumentTitle = DocumentTitle;
        }

        #endregion

        //-------------------------------------------------------------------------------------------------------
        #region ** Render Methods

        public void RenderGrid(C1FlexGrid flex)
        {
            RenderControl MyControl;
            MyControl.Control = flex;
            MyControl.ControlType = ControlType.Grid;
            Controls.Add(MyControl);
        }

        public void RenderText(string text)
        {
            RenderControl MyControl;
            MyControl.Control = text;
            MyControl.ControlType = ControlType.Text;
            Controls.Add(MyControl);
        }

        public void RenderBreakLine()
        {
            RenderControl MyControl;
            MyControl.Control = "";
            MyControl.ControlType = ControlType.BreakLine;
            Controls.Add(MyControl);
        }

        #endregion

        //-------------------------------------------------------------------------------------------------------
        #region ** Save Methods

        public void Print()
        {
            ExportPrint print = new ExportPrint(_DocumentTitle);

            #region rendering control
            foreach (RenderControl control in Controls)
            {
                switch (control.ControlType)
                {
                    case ControlType.Grid:
                        {
                            print.RenderGrid((C1FlexGrid)control.Control);
                            break;
                        }
                    case ControlType.Text:
                        {
                            print.RenderText((string)control.Control);
                            break;
                        }
                    case ControlType.BreakLine:
                        {
                            print.RenderText();
                            break;
                        }
                }
            }
            #endregion

            print.Print();
        }

        public void SavePDF(Panel GridContent)
        {
            var dlg = new SaveFileDialog();
            dlg.DefaultExt = "pdf";
            dlg.Filter = "PDF files (*.pdf)|*.pdf";

            bool? dialogResult = dlg.ShowDialog();

            if (dialogResult == true)
            {
                ExportPDF pdf = new ExportPDF(GridContent);
                pdf.RenderHeader(_DocumentTitle);

                #region rendering control
                foreach (RenderControl control in Controls)
                {
                    switch (control.ControlType)
                    {
                        case ControlType.Grid:
                            {
                                pdf.RenderGrid((C1FlexGrid)control.Control, GridContent);
                                break;
                            }
                        case ControlType.Text:
                            {
                                pdf.RenderText((string)control.Control);
                                break;
                            }
                        case ControlType.BreakLine:
                            {
                                pdf.RenderText();
                                break;
                            }
                    }
                }
                #endregion

                using (var stream = dlg.OpenFile())
                {
                    pdf.Save(stream);
                }
            }
        }

        public void SaveHTML()
        {

            var dlg = new SaveFileDialog();
            dlg.DefaultExt = "htm";
            dlg.Filter = "HTML (*.htm)|*.htm";
            if ((bool)dlg.ShowDialog())
            {
                using (Stream stream = dlg.OpenFile())
                {
                    using (StreamWriter sw = new StreamWriter(stream))
                    {
                        ExportHTML html = new ExportHTML(stream, sw);

                        html.RenderHeader(_DocumentTitle);

                        #region rendering control
                        foreach (RenderControl control in Controls)
                        {
                            switch (control.ControlType)
                            {
                                case ControlType.Grid:
                                    {
                                        html.RenderGrid((C1FlexGrid)control.Control);
                                        break;
                                    }
                                case ControlType.Text:
                                    {
                                        html.RenderText((string)control.Control);
                                        break;
                                    }
                                case ControlType.BreakLine:
                                    {
                                        html.RenderText();
                                        break;
                                    }
                            }
                        }
                        #endregion
                    }
                }
            }



        }

        public void SaveCSV()
        {

            var dlg = new SaveFileDialog();
            dlg.DefaultExt = "csv";
            dlg.Filter = "CSV (*.csv)|*.csv";
            if ((bool)dlg.ShowDialog())
            {
                using (Stream stream = dlg.OpenFile())
                {
                    using (StreamWriter sw = new StreamWriter(stream))
                    {
                        ExportCSV csv = new ExportCSV(stream, sw);

                        csv.RenderHeader(_DocumentTitle);

                        #region rendering control
                        foreach (RenderControl control in Controls)
                        {
                            switch (control.ControlType)
                            {
                                case ControlType.Grid:
                                    {
                                        csv.RenderGrid((C1FlexGrid)control.Control);
                                        break;
                                    }
                                case ControlType.Text:
                                    {
                                        csv.RenderText((string)control.Control);
                                        break;
                                    }
                                case ControlType.BreakLine:
                                    {
                                        csv.RenderText();
                                        break;
                                    }
                            }
                        }
                        #endregion
                    }
                }
            }



        }

        public void SaveTXT()
        {

            var dlg = new SaveFileDialog();
            dlg.DefaultExt = "txt";
            dlg.Filter = "Text File (*.txt)|*.txt";
            if ((bool)dlg.ShowDialog())
            {
                using (Stream stream = dlg.OpenFile())
                {
                    using (StreamWriter sw = new StreamWriter(stream))
                    {
                        ExportTXT txt = new ExportTXT(stream, sw);
                        txt.RenderHeader(_DocumentTitle);

                        #region rendering control
                        foreach (RenderControl control in Controls)
                        {
                            switch (control.ControlType)
                            {
                                case ControlType.Grid:
                                    {
                                        txt.RenderGrid((C1FlexGrid)control.Control);
                                        break;
                                    }
                                case ControlType.Text:
                                    {
                                        txt.RenderText((string)control.Control);
                                        break;
                                    }
                                case ControlType.BreakLine:
                                    {
                                        txt.RenderText();
                                        break;
                                    }
                            }
                        }
                        #endregion
                    }
                }
            }



        }

        #endregion




    }
}
