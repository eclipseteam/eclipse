﻿#region Using NameSpaces
using System;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Controls;
using System.Collections.ObjectModel;
using C1.Silverlight.FlexGrid;
using Oritax.TaxSimp.Data;
#endregion

namespace Oritax.TaxSimp.Controls
{
    public partial class FooterGrid2<TData, TItem> : UserControl
        where TData : class, IHasItems<TItem>, IHasTotal<TItem>, new()
        where TItem : class, IHasPrimaryKey, IHasDescription, new()
    {
        #region Fields
        // context menu
        private TextBlock TextBlockTitle;
        private Button ButtonAdd;
        private Button ButtonDelete;
        private StackPanel PanelToolbar;

        public C1FlexGrid FlexGridData;
        private ExcelContextMenu FlexGridData_menu;

        public C1FlexGrid FlexGridFooter;
        private ExcelContextMenu FlexGridFooter_menu;

        private bool _ContentLoaded;
        private bool _ColumnLoaded = false;
        private ObservableCollection<TItem> _Total = new ObservableCollection<TItem>();
        private ObservableCollection<TItem> _Items = new ObservableCollection<TItem>();
        private ResourceDictionary _Resource;
        #endregion

        #region InitializeComponent

        private void InitializeComponent()
        {
            if (_ContentLoaded) return;
            _ContentLoaded = true;

            _Resource = new ResourceDictionary();
            _Resource.Source = new Uri("/SilverlightControls_1_1;component/Controls/FooterGridResource.xaml", UriKind.Relative);
            Style excel = _Resource["FlexExcelBlue"] as Style;

            StackPanel mainPanel = new StackPanel();
            mainPanel.HorizontalAlignment = HorizontalAlignment.Left;
            this.Content = mainPanel;

            Grid mainGrid = new Grid();
            mainGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(30, GridUnitType.Star) });
            mainGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(100, GridUnitType.Star) });
            mainGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(60) });
            mainPanel.Children.Add(mainGrid);

            TextBlockTitle = new TextBlock();
            TextBlockTitle.VerticalAlignment = VerticalAlignment.Bottom;
            TextBlockTitle.Margin = new Thickness(5);
            Grid.SetColumn(TextBlockTitle, 0);
            mainGrid.Children.Add(TextBlockTitle);

            PanelToolbar = new StackPanel();
            PanelToolbar.Orientation = Orientation.Horizontal;
            mainGrid.Children.Add(PanelToolbar);
            Grid.SetColumn(PanelToolbar, 1);

            ButtonAdd = new Button() { Content = "Add" };
            ButtonAdd.Margin = new Thickness(5);
            PanelToolbar.Children.Add(ButtonAdd);

            ButtonDelete = new Button() { Content = "Delete" };
            ButtonDelete.Margin = new Thickness(5);
            PanelToolbar.Children.Add(ButtonDelete);

            StackPanel gridPanel = new StackPanel();
            mainPanel.Children.Add(gridPanel);

            FlexGridData = new C1FlexGrid();
            FlexGridData.Style = excel;
            FlexGridData.Width = this.Width;
            FlexGridData.VerticalContentAlignment = VerticalAlignment.Stretch;
            FlexGridData.HorizontalScrollBarVisibility = ScrollBarVisibility.Hidden;
            FlexGridData.AutoGenerateColumns = false;
            FlexGridData.SelectionMode = C1.Silverlight.FlexGrid.SelectionMode.CellRange;
            FlexGridData.AllowMerging = AllowMerging.AllHeaders;
            gridPanel.Children.Add(FlexGridData);

            FlexGridFooter = new C1FlexGrid();
            FlexGridFooter.Style = excel;
            FlexGridFooter.Width = this.Width;
            FlexGridFooter.VerticalContentAlignment = VerticalAlignment.Stretch;
            FlexGridFooter.HorizontalScrollBarVisibility = ScrollBarVisibility.Hidden;
            FlexGridFooter.HeadersVisibility = HeadersVisibility.Row;
            FlexGridFooter.AutoGenerateColumns = false;
            FlexGridFooter.IsReadOnly = true;
            FlexGridFooter.ColumnHeaders.Rows.Remove(FlexGridFooter.ColumnHeaders.Rows[0]);
            gridPanel.Children.Add(FlexGridFooter);

            #region ** Set Grids Menu

            FlexGridData_menu = new ExcelContextMenu(FlexGridData, true, true);
            foreach (var item in FlexGridData_menu.Items)
            {
                var mi = item as MenuItem;
                if (mi != null)
                {
                    mi.Click += menuItem_Click;
                }
            }

            FlexGridFooter_menu = new ExcelContextMenu(FlexGridFooter, false, false);

            #endregion
        }

        #endregion

        #region InitializeColumns

        public void InitializeColumns()
        {
            if (_ColumnLoaded) return;
            _ColumnLoaded = true;

            InitializeColumns(FlexGridData);
            InitializeColumns(FlexGridFooter);

            if (IsFirstRowFreeze)
                FlexGridData.Rows.Frozen = 1;

            FlexGridFooter.Background = _Resource["ReadOnlyColor"] as SolidColorBrush;
            FlexGridData.ItemsSource = _Items;
            FlexGridFooter.ItemsSource = _Total;
        }

        private void InitializeColumns(C1FlexGrid grid)
        {
            SolidColorBrush color = _Resource["ReadOnlyColor"] as SolidColorBrush;
            string format = "0,0.00;(0,0.00)";
            foreach (FooterColumn each in Columns)
            {
                Column column = new Column();
                column.Header = each.Header;
                if (!string.IsNullOrWhiteSpace(each.BindingPath)) column.Binding = new Binding(each.BindingPath) { Mode = BindingMode.TwoWay };
                column.Width = new GridLength(each.Width);
                if (each.IsReadOnly)
                {
                    column.Background = color;
                    column.IsReadOnly = true;
                }

                column.AllowSorting = each.AllowSorting;

                if (each.IsAmount)
                {
                    column.HorizontalAlignment = column.HeaderHorizontalAlignment = HorizontalAlignment.Right;
                    column.Format = format;
                }
                else
                {
                    column.HorizontalAlignment = column.HeaderHorizontalAlignment = HorizontalAlignment.Left;
                }

                column.AllowDragging = column.AllowMerging = column.AllowResizing = false;

                if (grid.IsReadOnly == true)
                    column.IsReadOnly = true;
                grid.Columns.Add(column);
            }

        }


        #endregion

        #region Menu Event Handlers

        // handle clicks on the menu items
        void menuItem_Click(object sender, RoutedEventArgs e)
        {
            var mi = sender as MenuItem;
            if (mi.Tag is ContextMenuCommands)
            {
                switch ((ContextMenuCommands)mi.Tag)
                {
                    case ContextMenuCommands.InsertRows:
                        AddItem();
                        break;

                    case ContextMenuCommands.DeleteRows:
                        DeleteItem();
                        break;
                }
            }

        }

        #endregion
    }
}
