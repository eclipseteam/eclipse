﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Oritax.TaxSimp.Controls
{
    public class FooterColumn
    {
        public string Header { get; set; }
        public string BindingPath { get; set; }
        public double Width { get; set; }
        public bool IsReadOnly { get; set; }
        public bool AllowSorting { get; set; }
        public bool IsAmount { get; set; }
    }
}
