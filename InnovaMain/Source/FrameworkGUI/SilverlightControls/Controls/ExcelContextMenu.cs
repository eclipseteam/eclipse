﻿using System;
using System.IO;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using C1.Silverlight.FlexGrid;

namespace Oritax.TaxSimp.Controls
{
    /// <summary>
    /// Specifies commands that may be executed from the context menu.
    /// </summary>
    public enum ContextMenuCommands
    {
        Cut,
        Copy,
        Paste,
        InsertRows,
        DeleteRows
    }

    public class ExcelContextMenu : ContextMenu
    {
        //---------------------------------------------------------------------------
        #region ** fields

        private bool CanInsert { get; set; }
        private bool CanDelete { get; set; }

        #endregion

        //---------------------------------------------------------------------------
        #region ** fields

        C1FlexGrid _grid;

        #endregion

        //---------------------------------------------------------------------------
        #region ** ctor

        /// <summary>
        /// Initializes a new instance of an <see cref="ExcelContextMenu"/>.
        /// </summary>
        /// <param name="grid"><see cref="C1FlexGridExcel"/> that owns this context menu.</param>
        internal ExcelContextMenu(C1FlexGrid grid, bool CanInsert, bool CanDelete)
        {
            this.CanInsert = CanInsert;
            this.CanDelete = CanDelete;

            // save reference to parent grid
            grid.ClipboardCopyMode = ClipboardCopyMode.ExcludeHeader;
            grid.ClipboardPasteMode = ClipboardCopyMode.ExcludeHeader;

            _grid = grid;

            // build menu
            Reset();

            // set up event handlers for context menu
            _grid.MouseRightButtonDown += _grid_MouseRightButtonDown;
            _grid.MouseRightButtonUp += _grid_MouseRightButtonUp;

            // set position when popping up
            HorizontalOffset = 0;
            VerticalOffset = 0;
            HorizontalAlignment = HorizontalAlignment.Left;
            VerticalAlignment = VerticalAlignment.Top;
        }

        #endregion

        //---------------------------------------------------------------------------
        #region ** object model

        /// <summary>
        /// Re-initializes the menu to its default content.
        /// </summary>
        void Reset()
        {
            // disconnect existing items
            foreach (var item in this.Items)
            {
                var mi = item as MenuItem;
                if (mi != null)
                {
                    mi.Click -= menuItem_Click;
                }
            }

            // remove all existing items
            Items.Clear();

            // populate the menu
            CreateMenuItem("Cut", ContextMenuCommands.Cut);
            CreateMenuItem("Copy", ContextMenuCommands.Copy);
            CreateMenuItem("Paste", ContextMenuCommands.Paste);
            Items.Add(new Separator());
            if (CanInsert == true)
                CreateMenuItem("Insert Rows", ContextMenuCommands.InsertRows);
            if (CanDelete == true)
                CreateMenuItem("Delete Rows", ContextMenuCommands.DeleteRows);
        }
        /// <summary>
        /// Occurs when the context menu is about to open.
        /// </summary>
        public event MouseEventHandler Opening;
        /// <summary>
        /// Raises the <see cref="Opening"/> event.
        /// </summary>
        /// <param name="e"><see cref="MouseEventArgs"/> that contains the event data.</param>
        protected virtual void OnOpening(MouseEventArgs e)
        {
            if (Opening != null)
                Opening(this, e);
        }

        #endregion

        //---------------------------------------------------------------------------
        #region ** implementation

        // disable standard "Silverlight" menu
        void _grid_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            IsOpen = false;
            _grid.Focus();
            e.Handled = true;
        }

        // update selection before showing the context menu
        void _grid_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            var ht = _grid.HitTest(e);
            if (!_grid.Selection.Contains(ht.CellRange))
            {
                _grid.Selection = ht.CellRange;
            }

            // show the context menu
            Show(e);
        }

        // show the context menu
        void Show(MouseEventArgs e)
        {
            OnOpening(e);
            if (Items.Count > 0)
            {
                IsOpen = true;
            }
        }

        // create a menu item
        void CreateMenuItem(string hdr, ContextMenuCommands cmd)
        {
            // create item, attach event handler
            var mi = new MenuItem();
            mi.Header = hdr;
            mi.Tag = cmd;
            mi.Click += menuItem_Click;

            // create image for this item
            var resName = string.Format("{0}_small.png", cmd.ToString());
            var stream = GetResourceStream(resName);
            if (stream != null)
            {
                var bmp = new BitmapImage();
                bmp.SetSource(stream);
                var img = new Image();
                img.Source = bmp;
                mi.Icon = img;
            }

            // done
            Items.Add(mi);
        }

        // handle clicks on the menu items
        void menuItem_Click(object sender, RoutedEventArgs e)
        {
            var sel = _grid.Selection;
            var mi = sender as MenuItem;
            if (mi.Tag is ContextMenuCommands)
            {
                switch ((ContextMenuCommands)mi.Tag)
                {
                    case ContextMenuCommands.Cut:
                        _grid.Copy();
                        foreach (var cell in sel.Cells)
                        {
                            if (_grid.Columns[cell.Column].IsReadOnly == false)
                                _grid[cell.Row, cell.Column] = null;
                        }
                        break;

                    case ContextMenuCommands.Copy:
                        _grid.Copy();
                        break;

                    case ContextMenuCommands.Paste:
                        _grid.Paste();
                        break;
                }
            }

        }

        // get an embedded resource stream
        Stream GetResourceStream(string resName)
        {
            var asm = Assembly.GetExecutingAssembly();
            foreach (var rn in asm.GetManifestResourceNames())
            {
                if (rn.EndsWith(resName))
                {
                    return asm.GetManifestResourceStream(rn);
                }
            }
            return null;
        }

        #endregion
    }
}
