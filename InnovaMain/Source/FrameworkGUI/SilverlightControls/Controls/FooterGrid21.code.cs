﻿#region Using NameSpaces
using System;
using System.Linq;
using System.Windows;
using System.Diagnostics;
using System.Windows.Input;
using System.Windows.Browser;
using System.Windows.Threading;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using C1.Silverlight.FlexGrid;
using Oritax.TaxSimp.Data;
using Oritax.TaxSimp.Extensions;
#endregion

namespace Oritax.TaxSimp.Controls
{


    public partial class FooterGrid21<TData, TItem> : IFooterGrid
        where TData : class, IHasItems1<TItem>, IHasTotal<TItem>, new()
        where TItem : class, IHasPrimaryKey, IHasDescription, new()
    {

        public string Title
        {
            get { return TextBlockTitle.Text; }
            set { TextBlockTitle.Text = value; }
        }
        public string TotalDescription { get; set; }
        public int Type { get; private set; }
        public Func<string, Action<InvokeOperation<string>>, object, InvokeOperation<string>> Add_Action { get; set; }
        public Func<string, Action<InvokeOperation<string>>, object, InvokeOperation<string>> Delete_Action { get; set; }
        public Func<string, Action<InvokeOperation<string>>, object, InvokeOperation<string>> Update_Action { get; set; }
        public Action<TData> Process_Completed { get; set; }

        public List<FooterColumn> Columns { get; private set; }

        public bool IsToolbarVisible
        {
            set
            {
                PanelToolbar.Visibility = value ? Visibility.Visible : Visibility.Collapsed;
            }
            get
            {
                return PanelToolbar.Visibility == Visibility.Visible;
            }
        }
        public bool IsLastRowExists { get; set; }
        public bool IsFirstRowFreeze { get; set; }
        public bool IsTitleVisible
        {
            set
            {
                TextBlockTitle.Visibility = value ? Visibility.Visible : Visibility.Collapsed;
            }
            get
            {
                return TextBlockTitle.Visibility == Visibility.Visible;
            }
        }

        public GridPanel ColumnHeaders
        {
            get
            {
                return FlexGridData.ColumnHeaders;
            }
        }

        public HeadersVisibility HeadersVisibility
        {
            set
            {
                FlexGridData.HeadersVisibility = value;
            }
            get
            {
                return FlexGridData.HeadersVisibility;
            }
        }



        public FooterGrid21(int type)
        {
            InitializeComponent();
            Type = type;
            Columns = new List<FooterColumn>();
            ButtonAdd.Click += (o, e) => AddItem();
            FlexGridData.CellEditEnding += new EventHandler<CellEditEventArgs>(FlexGridData_CellEditEnded);
            FlexGridData.KeyDown += new KeyEventHandler(FlexGridData_KeyDown);
            ButtonDelete.Click += (o, e) => DeleteItem();
            IsToolbarVisible = false;

        }

        void FlexGridData_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Insert:
                    AddItem();
                    break;
                case Key.Delete:
                    DeleteItem();
                    break;
            }
        }

        void FlexGridData_CellEditEnded(object sender, CellEditEventArgs e)
        {
            if (e.Row == FlexGridData.Rows.Count)
            {
                return;
            }

            Row row = FlexGridData.Rows[e.Row];
            TItem item = row.DataItem as TItem;
            Dispatcher.BeginInvoke(new Action(() =>
            {
                Debug.WriteLine(item);
                UpdateItem(item);
            }));
        }

        public void GetData_Completed(BMCServiceDataItem bmc)
        {
            ProcessBmcItem(bmc, list =>
                {
                    _Items.Clear();
                    _Items.Add(list);

                });
        }

        private void AddItem()
        {

            TItem selected = FlexGridData.Rows[FlexGridData.Selection.BottomRow].DataItem as TItem;
            int index = _Items.FindIndex<TItem>(selected);

            TItem item = new TItem();
            item.PrimaryKey = Guid.NewGuid();

            string xml = CreateRequestXml(item);
            if (Add_Action != null)
            {
                Add_Action(xml, x => ProcessResponseXml(x.Value, (list) =>
                {
                    Add_Completed(list, index);
                }), null);
            }
        }

        private void UpdateItem(TItem item)
        {
            string xml = CreateRequestXml(item);

            if (Update_Action != null)
            {
                Update_Action(xml, x => ProcessResponseXml(x.Value, Update_Completed), null);
            }

        }

        private void DeleteItem()
        {
            IEnumerable<TItem> selections = FlexGridData.Rows.GetDataItems(FlexGridData.Selection).OfType<TItem>();

            string xml = CreateRequestXml2(selections);
            if (Delete_Action != null)
            {
                Delete_Action(xml, x => ProcessResponseXml(x.Value, Delete_Completed), null);
            }

        }

        private string CreateRequestXml(params TItem[] items)
        {
            return CreateRequestXml2(items);
        }

        private string CreateRequestXml2(IEnumerable<TItem> items)
        {
            Guid clid = new Guid(HtmlPage.Document.QueryString["CLID"]);
            Guid csid = new Guid(HtmlPage.Document.QueryString["CSID"]);
            BMCServiceDataItem bmc = new BMCServiceDataItem { Clid = clid, Csid = csid, Type = Type };

            TData data = new TData();
            foreach (TItem each in items)
            {
                data.Items = each;
            }
            bmc.Data = data.ToXmlString<TData>();
            return bmc.ToXmlString();
        }

        private void ProcessResponseXml(string xml, Action<TItem> action)
        {
            if (string.IsNullOrWhiteSpace(xml)) return;
            BMCServiceDataItem bmc = xml.ToData<BMCServiceDataItem>();
            ProcessBmcItem(bmc, action);
        }

        private void ProcessBmcItem(BMCServiceDataItem bmc, Action<TItem> action)
        {
            if (bmc == null || string.IsNullOrWhiteSpace(bmc.Data)) return;
            TData data = bmc.Data.ToData<TData>();
            if (data == null || data.Items == null) return;

            if (Process_Completed != null)
            {
                Process_Completed(data);
            }

            if (action != null) action(data.Items);
        }

        private void Add_Completed(TItem list, int index)
        {

            if (_Items.Count <= 0)
            {
                _Items.Add(list);
            }
            else
            {
                _Items.Insert(++index, list);
            }

        }

        private void Update_Completed(TItem list)
        {

            int index = _Items.FindIndex<TItem>(list);
            _Items.RemoveAt(index);
            _Items.Insert(index, list);

        }

        private void Delete_Completed(TItem list)
        {

            TItem selected = _Items.Find<TItem>(list);
            if (IsLastRowExists && _Items.Count <= 1)
            {
                AddItem();
            }
            _Items.Remove(selected);

        }

    }
}
