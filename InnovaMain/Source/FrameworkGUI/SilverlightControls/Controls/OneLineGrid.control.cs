﻿#region Using NameSpaces
using System;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Controls;
using System.Collections.ObjectModel;
using C1.Silverlight.FlexGrid;
using Oritax.TaxSimp.Data;
#endregion


namespace Oritax.TaxSimp.Controls
{
    public partial class OneLineGrid<TData, TItem> : UserControl
        where TData : class, IHasItem<TItem>, new()
        where TItem : class, IHasPrimaryKey, IHasDescription, new()
    {
        #region Fields
        private TextBlock TextBlockTitle;

        public C1FlexGrid FlexGridFooter;
        private ExcelContextMenu FlexGridFooter_menu;

        private bool _ContentLoaded;
        private bool _ColumnLoaded = false;
        private ObservableCollection<TItem> _Items = new ObservableCollection<TItem>();
        private ResourceDictionary _Resource;
        #endregion

        #region InitializeComponent

        private void InitializeComponent()
        {
            if (_ContentLoaded) return;
            _ContentLoaded = true;

            _Resource = new ResourceDictionary();
            _Resource.Source = new Uri("/SilverlightControls_1_1;component/Controls/FooterGridResource.xaml", UriKind.Relative);
            Style excel = _Resource["FlexExcelBlue"] as Style;

            StackPanel mainPanel = new StackPanel();
            mainPanel.HorizontalAlignment = HorizontalAlignment.Left;
            this.Content = mainPanel;


            TextBlockTitle = new TextBlock();
            TextBlockTitle.VerticalAlignment = VerticalAlignment.Bottom;
            TextBlockTitle.Margin = new Thickness(5);
            TextBlockTitle.Visibility = Visibility.Collapsed;
            mainPanel.Children.Add(TextBlockTitle);

            FlexGridFooter = new C1FlexGrid();
            FlexGridFooter.Style = excel;
            FlexGridFooter.Width = this.Width;
            FlexGridFooter.VerticalContentAlignment = VerticalAlignment.Stretch;
            FlexGridFooter.HorizontalScrollBarVisibility = ScrollBarVisibility.Hidden;
            FlexGridFooter.HeadersVisibility = HeadersVisibility.Row;
            FlexGridFooter.AutoGenerateColumns = false;
            FlexGridFooter.IsEnabled = false;

            mainPanel.Children.Add(FlexGridFooter);

            #region ** Set Grids Menu

            FlexGridFooter_menu = new ExcelContextMenu(FlexGridFooter, false, false);

            #endregion

        }

        #endregion

        #region InitializeColumns
        public void InitializeColumns()
        {
            if (_ColumnLoaded) return;
            _ColumnLoaded = true;

            SolidColorBrush color = _Resource["ReadOnlyColor"] as SolidColorBrush;
            string format = "0,0.00;(0,0.00)";
            foreach (FooterColumn each in Columns)
            {
                Column column = new Column();
                column.Header = each.Header;
                if (!string.IsNullOrWhiteSpace(each.BindingPath)) column.Binding = new Binding(each.BindingPath) { Mode = BindingMode.TwoWay };
                column.Width = new GridLength(each.Width);
                if (each.IsReadOnly)
                {
                    column.Background = color;
                    column.IsReadOnly = true;
                }

                column.AllowSorting = each.AllowSorting;

                if (each.IsAmount)
                {
                    column.HorizontalAlignment = column.HeaderHorizontalAlignment = HorizontalAlignment.Right;
                    column.Format = format;
                }
                else
                {
                    column.HorizontalAlignment = column.HeaderHorizontalAlignment = HorizontalAlignment.Left;
                }

                column.AllowDragging = column.AllowMerging = column.AllowResizing = false;

                FlexGridFooter.Columns.Add(column);
            }


            if (!IsEnabled)
            {
                FlexGridFooter.Background = color;
            }
            FlexGridFooter.ItemsSource = _Items;
        }
        #endregion

    }
}
