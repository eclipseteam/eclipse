using System;
using System.Diagnostics;
using System.Data;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Text;
using System.Reflection;
using System.Threading;

using Oritax.TaxSimp.Presentation;
using Oritax.TaxSimp.InstallInfo;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Services.CMBroker;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.Exceptions;
using PresentationControls;
using Oritax.TaxSimp.WPDatasetBase;
using Oritax.TaxSimp.Security;
using Oritax.TaxSimp.CM.Period;
using Oritax.TaxSimp.BusinessStructureUtilities;

using AjaxControlToolkit;
using System.Collections.Generic;
using System.Collections.Specialized;
using Oritax.TaxSimp.TaxSimpLicence;

namespace Oritax.TaxSimp.Calculation
{
    /// <summary>
    /// Workpaper base class for all workpapers
    /// </summary>
    public abstract class BrokerManagedComponentWorkpaper : System.Web.UI.Page
    {
        #region Enums
        public enum SortOrderEnum
        {
            Assending = 0,
            Descending = 1
        }
        #endregion
        #region Internal Classes
        public struct MenuEvent
        {
            public string MenuCommand;
            public CommandEventHandler EventDelegate;
            public bool NavigationEvent;

            public MenuEvent(string strMenuCommand, CommandEventHandler objEventDelegate)
            {
                this.MenuCommand = strMenuCommand;
                this.EventDelegate = objEventDelegate;
                this.NavigationEvent = false;
            }

            public MenuEvent(string strMenuCommand, CommandEventHandler objEventDelegate, bool bolNavigationEvent)
            {
                this.MenuCommand = strMenuCommand;
                this.EventDelegate = objEventDelegate;
                this.NavigationEvent = bolNavigationEvent;
            }
        }
        #endregion
        #region Constants
        public const String VisitedWorkpaperMarker = "<****>";
        private const TabType DefaultTab = TabType.Workpaper;
        private const string ActiveTabHiddenFieldName = "___ActiveTabType";
        private const string CurrentPageURLHiddenFieldName = "___CurrentPageURL";
        private const string CurrentWorkpaperNameHiddenFieldName = "___CurrentWPName";
        private const string WorkPaperPrintCollectorPage = "___WorkPaperPrintCollector";
        private const string RequestCanBeCancelledHiddenFieldName = "RequestCanBeCancelled";
        private readonly static TimeSpan pageExpiryTimespan = new TimeSpan(1, 0, 0);
        #endregion
        #region Member variables
        internal Guid m_objInstanceID;
        internal IBrokerManagedComponent m_objBMC;
        internal INableHeader m_objHeader;
        private NableFooter m_objFooter;
        protected DataSet m_objData;
        private Tab m_objActiveTab;
        private HtmlForm m_objForm;
        private Panel m_objWorkpaperControls;
        private DataSet m_objPostBackDataset;
        private ActionType m_objAfterPostBackAction = ActionType.RemainOnWorkpaper;
        private string m_strRedirectActionURL;
        private bool m_bolPageRequiresRebind = false;
        private bool m_wpHasAttachments = false;
        private bool m_wpHasNotes = false;
        private bool m_bolIsInvalidDataset = false;
        private MessageBox messageBox = new MessageBox();
        private bool preventFurtherOutput = false;
        private ScriptManagerWrapper scriptManagerWrapper = null;
        

        // Adding iFrame instance...
        private HtmlControl iFrame;

        protected HtmlControl IFrame
        {
            get { return iFrame; }
            set { iFrame = value; }
        }
        private bool IsTargetSet = false;

        public bool HasTarget
        {
            get { return IsTargetSet; }
            set { IsTargetSet = value; }
        }

        #endregion
        #region Constructors
        public BrokerManagedComponentWorkpaper()
        {
            //this.ID = "Workpaper";
            base.Load += new EventHandler(this.Page_Load);
            base.Unload += new EventHandler(this.Page_Unload);
            base.PreRender += new EventHandler(this.HandlePreRender);
        }
        #endregion
        #region EventHandlers
        private void Page_Load(object sender, EventArgs e)
        {
            this.InitialiseBroker();

            try
            {
                this.InitialiseIDs(Guid.Empty);
                this.InitialiseObjects();
                this.InitialiseReturnURL();
                this.LoadWorkpaper();
                if (this.Broker.SystemWideDisplayMessage != "")
                {
                    if (m_objHeader != null)
                    {
                        this.m_objHeader.ErrorText = this.Broker.SystemWideDisplayMessage;
                    }
                }

                if (this.ReadOnly && !UserIsAdministrator())
                    this.MakeWorkpaperReadOnly();
            }
            catch (MissingComponentException)
            {
                this.HandleMissingComponent();
            }
            catch
            {
                this.m_objBroker.SetAbort();
                throw;
            }
        }

        public IBrokerManagedComponent BmcObject
        {
            get
            {
                return this.m_objBMC;
            }
        }

        protected void HandlePreRender(object sender, EventArgs e)
        {
            if (this.m_bolPageRequiresRebind && !this.m_bolIsInvalidDataset)
            {
                this.UpdateTitle();

                if (this.m_objHeader != null)
                    this.SetSelectedButton();

                this.m_objActiveTab.PopulateTab(this.m_objData);
            }

            Complete();
        }

        private void UpdateTitle()
        {
            string strWorkpaperName = this.WorkpaperName;

            foreach (Control objControl in this.Controls)
            {
                if (objControl.ID == "Title")
                    ((LiteralControl)objControl).Text = strWorkpaperName;
            }

            if (this.m_objHeader != null)
                this.m_objHeader.HeaderText = strWorkpaperName;
        }

        private void Page_Unload(object sender, EventArgs e)
        {
            this.CleanupBroker();
            Thread.SetData(Thread.GetNamedDataSlot("Broker"), null);
            Thread.SetData(Thread.GetNamedDataSlot("EventLogEntries"), null);
            Thread.SetData(Thread.GetNamedDataSlot("ScenarioIDs"), null);
        }

        protected INableHeader NableHeader
        {
            get
            {
                return this.m_objHeader;
            }
            set
            {
                this.m_objHeader = value;
            }
        }



        private bool HandleMenuCommand(object sender, CommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "AddFavourite":
                    AddFavourite();
                    break;

                case "Save":
                    this.SaveWorkpaper(true);
                    break;

                case "Refresh":
                    this.RefreshWorkpaper(false);
                    break;

                case "Close":
                    this.CloseWorkpaper();
                    break;

                case "Cancel":
                    this.CancelWorkpaper();
                    break;

                case "Print":
                    this.PrintWorkpaper();
                    break;

                default:
                    {
                        // check if it is a tab command
                        if (TabFactory.IsTabCommand(e.CommandName))
                        {
                            this.SwitchTab(e.CommandName);
                            break;
                        }
                        else
                            // we didn't handle it
                            return false;
                    }
            }

            // it was handled here
            return true;
        }

        private void AddFavourite()
        {
            string[] addresses = this.Request.RawUrl.Split('/');
            if (addresses.Length > 0)
            {
                string address = addresses[addresses.Length - 1];
                string[] urlAddress = address.Split('?');
                string workpaperName = urlAddress[0];
                Dictionary<String, String> workpaperArgumentsList = new Dictionary<String, String>();
                if (urlAddress.Length > 1)
                {
                    string[] workpaperArguments = urlAddress[1].Split('&');
                    if (workpaperArguments != null)
                    {
                        foreach (String workpaperArgument in workpaperArguments)
                        {
                            string[] parameters = workpaperArgument.Split('=');
                            workpaperArgumentsList.Add(parameters[0], parameters[1]);
                        }
                    }
                }

                IBrokerManagedComponent objBMC = Broker.GetBMCInstance(Context.User.Identity.Name, "DBUser_1_1");
                IDBUser dbUser = (IDBUser)objBMC;

                List<String> parameterKeys = new List<string>();
                List<String> parameterValues = new List<string>();
                parameterKeys.Add("CID");
                parameterValues.Add(dbUser.CID.ToString());
                parameterKeys.Add("WorkpaperName");
                parameterValues.Add(workpaperName);
                foreach (KeyValuePair<String, String> keyValuePair in workpaperArgumentsList)
                {
                    parameterKeys.Add(string.Format("WP_Param{0}", keyValuePair.Key));
                    parameterValues.Add(keyValuePair.Value);
                }

                string details = string.Format("{0} {1}", this.m_objHeader.BreadCrumb, this.WorkpaperName);
                parameterKeys.Add("WP_ParamBreadCrumb");
                parameterValues.Add(details);

                this.SetRedirectActionPage("AddFavourite.aspx", "DBUser_1_1", parameterKeys.ToArray(), parameterValues.ToArray());
                this.Broker.ReleaseBrokerManagedComponent(objBMC);
                this.AfterPostbackActionType = ActionType.RedirectToSpecificWorkpaper;

                this.RedirectSpecificWorkpaper();
            }
        }

        private bool HandleCustomMenuCommand(object sender, CommandEventArgs e)
        {
            bool bolHandledHere = false;
            MenuEvent[] objCustomMenuEvents = this.InternalGetCustomMenuCommands();

            if (objCustomMenuEvents != null)
            {
                foreach (MenuEvent objEvent in objCustomMenuEvents)
                {
                    if (objEvent.MenuCommand == e.CommandName)
                    {
                        if (objEvent.EventDelegate != null)
                        {
                            bool callEventDelegate = true;

                            if (objEvent.NavigationEvent)
                            {
                                if (!this.IsBMCScenarioLocked())
                                    callEventDelegate = this.SaveWorkpaper(false);
                            }

                            if (this.m_bolIsInvalidDataset)
                                return false;

                            this.m_objData.AcceptChanges();

                            // if save failed then we want to back out now, so don't call EventDelegate
                            if (callEventDelegate) objEvent.EventDelegate(sender, e);

                            if (this.m_objData.HasChanges())
                                this.SaveWorkpaper(false);
                        }

                        // indicate the the menu command was handled by one of the
                        // custom handlers
                        bolHandledHere = true;
                    }
                }
            }

            // return if the MenuCommand was handled here
            return bolHandledHere;
        }

        private void HandleUserConfirm(string confirmType)
        {
            if (this.m_objData is WPDatasetBaseDS)
            {
                (this.m_objData as WPDatasetBaseDS).SetUserConfirmed(confirmType, true);
            }
            this.ViewState[confirmType] = true;
        }
        /// <summary>
        /// Following method sets user confirm table with message box control reponses e.g. CheckBox.IsChecked
        /// </summary>
        /// <param name="confirmType"></param>
        /// <param name="controlList"></param>
        private void HandleUserConfirm(string confirmType, MessageBoxControlsList controlList)
        {
            if (controlList.MessageBoxList != null || controlList.MessageBoxList.Count > 0)
            {
                this.ViewState[confirmType] = true;
                (this.m_objData as WPDatasetBaseDS).SetUserConfirmed(confirmType, true);

                foreach (DictionaryEntry dictEntry in controlList.MessageBoxList)
                {
                    foreach (IBaseMessageBoxControl control in (ArrayList)dictEntry.Value)
                        (this.m_objData as WPDatasetBaseDS).SetUserConfirmed(control.ID.ToString(), control.UserConfirmation);
                }
            }
        }
        protected virtual bool GetUserConfirmed(string confirmType)
        {
            if ((this.ViewState[confirmType] != null) && (this.ViewState[confirmType] is Boolean))
            {
                return (bool)this.ViewState[confirmType];
            }
            else
                return false;
        }
        #endregion
        #region Methods
        private void InitialiseBroker()
        {
            this.m_objBroker = new CMBroker(Context.User, DBConnection.Connection.ConnectionString);

            RequestProcessStatusProvider statusProvider = (RequestProcessStatusProvider)HttpContext.Current.Items["NABLEStatusProvider"];

            if (statusProvider != null)
                this.m_objBroker.SetRequestStatusProvider(statusProvider);

            if (this.m_objBroker.IsLongRunningTransactionInProgress())
            {
                statusProvider.StatusInfoMessage = new MessageBoxDefinition(MessageBoxDefinition.MSG_WRN_ANOTHER_TIMEINTENSIVETASK).ToString().Replace("\r\n", "<br>");
                this.AllowUserToCancelOperation = true;
            }

            this.m_objBroker.SetStart();
        }

        private void InitialiseReturnURL()
        {
            if (!this.IsPostBack)
                this.ViewState["ReturnURL"] = this.Request.UrlReferrer;
        }

        protected virtual void InitialiseIDs(Guid objInstanceID)
        {
            if (objInstanceID == Guid.Empty)
            {
                string strCID = Context.Request.QueryString["CID"];

                if (strCID != null && strCID != String.Empty)
                    this.m_objInstanceID = new Guid(strCID);
                else
                    this.m_objInstanceID = Guid.Empty;
            }
            else
                this.m_objInstanceID = objInstanceID;
        }

        protected virtual bool ShowStandardMenuButtons
        {
            get
            {
                return this.m_objHeader.StandardMenuButtons.Visible;
            }
            set
            {
                this.m_objHeader.StandardMenuButtons.Visible = value;
            }
        }

        private void SetHeaderMenu()
        {
            if (this.ShowStandardMenuButtons == false)
            {
                this.m_objHeader.StandardMenuButtons.Visible = false;
                foreach (NableMenuButton item in m_objHeader.StandardMenuButtons)
                {
                    item.Visible = false;
                }
            }
        }

        protected void InitialiseCID(Guid instanceID)
        {
            if (instanceID == Guid.Empty)
            {
                string strCID = Context.Request.QueryString["CID"];

                if (strCID != null && strCID != String.Empty)
                    this.m_objInstanceID = new Guid(strCID);
                else
                    this.m_objInstanceID = Guid.Empty;
            }
            else
                this.m_objInstanceID = instanceID;
        }

        protected virtual void InitialiseObjects()
        {
            if (this.m_objInstanceID != Guid.Empty)
            {
                this.m_objBMC = this.m_objBroker.GetBMCInstance(this.m_objInstanceID);

                if (this.m_objBMC == null)
                {
                    this.HandleMissingComponent();
                    return;
                }
            }
            else
                this.m_objBMC = this.m_objBroker.CreateTransientComponentInstance(this.BMCTypeID);
        }

        public virtual void ReleaseObjects()
        {
            if (this.m_objBMC != null && !this.m_objBMC.TransientInstance)
                this.Broker.ReleaseBrokerManagedComponent(this.m_objBMC);

            this.m_objBMC = null;
        }


        public virtual void GetBMCDataDuringInit(DataSet dataSet)
        {
            if (this.m_objBroker == null)
                this.InitialiseBroker();

            if (this.m_objInstanceID == Guid.Empty)
                this.InitialiseIDs(Guid.Empty);

            if (this.m_objBMC == null)
                this.InitialiseObjects();

            this.GetBMCData(dataSet);
        }

        public virtual void SetDataBeyondSave(DataSet dataSet)
        {
            if (m_objBMC != null)
                this.m_objBMC.SetData(dataSet);
        }

        private void LoadWorkpaper()
        {
            this.CheckSecurityAccess();

            this.SetCachingParameters();

            this.Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "disableallscript", "DisableAllNoValidation();");

            // when the page is reloaded, we no longer want to display the message box
            // that was shown before.
            this.messageBox.ShowOnPageLoad = false;

            if (this.m_objHeader != null)
            {
                this.m_objHeader.CloseButtonClosesScreen = (this.CloseActionType == ActionType.CloseScreen);
                this.m_objHeader.ErrorText = String.Empty;
                this.HideTargetsButton();
                this.EnableTeaButton();
                this.m_objHeader.HeaderText = this.WorkpaperName; // CQ - TXE200012398
                this.SetSelectedButton();
                this.PopulateComponentVersion(this.m_objHeader);
            }

            DataSet objExtractedDataSet = this.ActiveTab.CreateTabDataset();

            if (!this.GetBMCData(objExtractedDataSet))
                return;

            this.PopulateBreadcrumb(objExtractedDataSet);
            this.LogWorkpaperAccess();
            this.PopulateWorkpaperMetaData(objExtractedDataSet);

            this.m_objData = objExtractedDataSet;
            if (m_objData != null)
                this.m_objData.AcceptChanges();

            this.m_objActiveTab.ResetDynamicTabControlsData(objExtractedDataSet);

            if (!this.IsPostBack)
            {
                this.m_objActiveTab.PopulateTab(objExtractedDataSet);
                this.InitialiseUpdateTokens();
            }

        }

        private void SetCachingParameters()
        {
            HttpCachePolicy cachePolicy = this.Page.Response.Cache;
            cachePolicy.SetLastModified(DateTime.Now);
            cachePolicy.SetExpires(DateTime.Now.Subtract(pageExpiryTimespan));
            cachePolicy.SetNoServerCaching();
            cachePolicy.SetNoTransforms();
            this.SetCachingPolicy();
        }
        /// <summary>
        /// The enable caching policy method sets Response.Cache settings, by default this is disable as aspx pages should be cahced
        /// but pdf reports and other documents should be enabled. Thus ReportWorkpaper overrides this methods and set caching policy enabled that is Public. 
        /// </summary>
        protected virtual void SetCachingPolicy()
        {
            this.Page.Response.Cache.SetNoStore();
        }

        public virtual bool IsWorkpaperLinkAttachementType()
        {
            return false;
        }

        public virtual void GenerateDynamicControls(DataSet objData) { }
        public virtual void ResetDynamicControlsData(DataSet objData) { }
        public virtual void ClearDynamicControls() { }

        protected override void Render(HtmlTextWriter writer)
        {
            if (!this.preventFurtherOutput)
                base.Render(writer);
        }

        /// <summary>
        /// Stops the page from rendering any more content.
        /// Used when outputting your own content and not
        /// wanting the page to output any of the controls
        /// </summary>
        /// <remarks>
        /// Used in combination with the NablePageHandler as 
        /// this cannot tell when Close is selected (disconnected)
        /// </remarks>
        public void StopFurtherOutput()
        {
            this.preventFurtherOutput = true;
        }

        #region Print Workpaper to PDF -------------------------------------------------

        protected virtual void PrintWorkpaper()
        {
            string printWPURL = "WorkpaperPrintingCollector_1_1.aspx?CurrentWPName=" + this.WorkpaperName;
            if (HasMultiplePages())
            {
                this.Session["PrintWorkpapers"] = BuildPrintPagesList();
            }
            else
            {
                string CurrentWorkpaperURL = HttpUtility.UrlEncode(GetCurrentWorkpaperPrintURL());
                printWPURL = printWPURL + "&SinglePrintRequest=True&CurrentURL=" + CurrentWorkpaperURL;
            }
          
            this.Complete();
            if (IsTargetSet)
            {
                iFrame.Attributes["src"] = printWPURL;
            }
            else
            {
                this.Page.Response.Redirect(printWPURL);
            }
        }

        protected virtual string GetCurrentWorkpaperPrintURL()
        {
            string strFile = this.Request.Url.OriginalString.ToString();
            string CurrentASPX = strFile.Substring(strFile.LastIndexOf(@"/") + 1) + "&PrintRequest=true&PrintTab=" + this.ActiveTabType.ToString();
            return CurrentASPX;
        }
        /// <summary>
        /// Are there are multiple pages or questions within the workpaper?
        /// </summary>
        /// <returns>true, if the navigation list has more than 1 page</returns>
        protected virtual bool HasMultiplePages()
        {
            bool result = false;
            BaseNavigationList pages = GetNavigationList();
            if ((pages != null) && (pages.Count() > 1))
                result = true;
            return result;
        }
        /// <summary>
        /// whether workpaper should have TEA tab
        /// </summary>
        /// <returns>false - this feature should be enabled specifically on workpaper level</returns>
        protected virtual bool HasTeaTab
        {
            get
            {
                return false;
            }
        }
        /// <summary>
        /// the same as HasTeaTab
        /// This method is created so HasTeaTab can be accessable as public without modifying 
        /// the property in the existing existing TEA tab workpapers. Can be removed in the next release
        /// </summary>
        /// <returns></returns>
        public bool IsTeaTabApplicable()
        {
            return HasTeaTab;
        }
        /// <summary>
        /// build the list of all pages included in the ATO workpaper
        /// </summary>
        /// <param name="strArgNames">arguments necessary to print the workpaper</param>
        /// <param name="strArgValues">arguments values</param>
        /// <returns>array list containing NavigationItems</returns>
        private ArrayList BuildPrintPagesList()
        {
            ArrayList pages = null;
            BaseNavigationList navigationList = GetNavigationList();
            if (navigationList != null)
                pages = navigationList.GetItems();

            if (pages != null)
            {
                string currentWorkpaperURL = GetCurrentWorkpaperPrintURL();
                string currentPageName = currentWorkpaperURL.Substring(0, currentWorkpaperURL.IndexOf("?"));
                foreach (NavigationItem page in pages)
                {
                    string versionedUrl = this.Broker.GetVersionedWorkpaper(page.Key + ".aspx", this.m_objBMC);
                    page.Key = currentWorkpaperURL.Replace(currentPageName, versionedUrl);
                }
            }
            return pages;
        }
        /// <summary>
        /// Get list containing NavigationItems for the workpaper
        /// </summary>
        protected virtual BaseNavigationList GetNavigationList()
        {
            return null;
        }
        #endregion

        #region Security methods

        protected bool UserIsSystemAdministrator()
        {
            if (Context.User.Identity.Name.ToLower() == "administrator")
                return true;
            else
                return false;
        }

        protected bool UserIsAdministrator()
        {
            if (UserIsSystemAdministrator())
                return true;
            if (Context.User is ITXPrincipal)
            {
                ITXPrincipal objPrincipal = (ITXPrincipal)Context.User;
                IDBUser userBMC = (IDBUser)this.Broker.GetBMCInstance(new Guid(objPrincipal.Groups[0]));
                return userBMC.Administrator;
            }
            else
                return false;
        }

        protected Guid UserOrganisation
        {
            get
            {
                if (Context.User is ITXPrincipal)
                {
                    ITXPrincipal objPrincipal = (ITXPrincipal)Context.User;
                    IDBUser userBMC = (IDBUser)this.Broker.GetBMCInstance(new Guid(objPrincipal.Groups[0]));
                    return userBMC.OrganisationCID;
                }
                else
                    return Guid.Empty;
            }
        }

        protected virtual bool ReadOnly
        {
            get
            {
                return false;
            }
        }

        private void CheckSecurityAccess()
        {
            // Administrator has full system access, so avoid checking privileges.
            if (!UserIsSystemAdministrator())
            {
                bool accessDenied = false;

                if ((this.m_objBroker.SecuritySetting == 1) && this.AdministratorRequired && !UserIsAdministrator() && !ReadOnly)
                    accessDenied = true;
                else if ((this.m_objBroker.SecuritySetting == 1) && this.SystemAdministratorRequired && !UserIsSystemAdministrator() && !ReadOnly)
                    accessDenied = true;
                else if (Context.User is ITXPrincipal)
                {
                    // Establish user has logged in ( Context is ITXPrincipal ) otherwise 
                    // security check was being inappropriately applied to Login page.

                    // Check users access to this page.
                    ITXPrincipal objPrincipal = (ITXPrincipal)Context.User;
                    if (this.m_objBMC is ICalculationModule && ((ICalculationModule)m_objBMC).CLID != Guid.Empty)
                        if (!(((ICalculationModule)this.m_objBMC).CheckPrivilege(PrivilegeType.BMCFullAccess, new Guid(objPrincipal.Groups[0])) == PrivilegeOption.Allow))
                            accessDenied = true;
                }

                if (accessDenied)
                    this.RedirectToErrorPage("Access Denied",
                        new MessageBoxDefinition(MessageBoxDefinition.MSG_INSUFFICIENT_RIGHTS).ToString(),
                        ((this.ReturnURL == null) ? "Close" : "Entities_1_1.aspx"));
            }
        }

        /// <summary>
        /// Method to call for any click events called from a page which require administrators only access. 
        /// </summary>
        protected void AdministratorOnlyActionAccess()
        {
            if (!UserIsAdministrator() && (this.Broker.SecuritySetting == 1))
            {
                this.RedirectToErrorPage("Access Denied",
                    new MessageBoxDefinition(MessageBoxDefinition.MSG_INSUFFICIENT_RIGHTS).ToString(),
                    ((this.ReturnURL == null) ? "Close" : this.Request.Url.ToString()));
            }
        }

        /// <summary>
        /// Method to call for any click events called from a page which require only system administrator only access. 
        /// </summary>
        protected void SystemAdministratorOnlyActionAccess()
        {
            if (!UserIsSystemAdministrator())
            {
                this.RedirectToErrorPage("Access Denied",
                    new MessageBoxDefinition(MessageBoxDefinition.MSG_INSUFFICIENT_RIGHTS).ToString(),
                    ((this.ReturnURL == null) ? "Close" : this.Request.Url.ToString()));
            }
        }

        #endregion

        protected bool IsLicenceValid()
        {
            ILicence licence = (ILicence)this.Broker.GetWellKnownBMC(WellKnownCM.Licencing);
            return licence.IsLicenceValid();
        }

        private void InitialiseUpdateTokens()
        {
            this.BMCUpdateToken = this.m_objBMC.UpdateToken.Copy();
        }

        protected virtual void PopulateBreadcrumb(DataSet objData)
        {
            DataTable objBreadcrumbTable = null;
            if (objData != null && objData.Tables != null)
                objBreadcrumbTable = objData.Tables[WPDatasetBaseDS.BREADCRUMBTABLE];

            if (objBreadcrumbTable != null && objBreadcrumbTable.Rows.Count > 0)
                this.m_objHeader.BreadCrumb = (string)objBreadcrumbTable.Rows[0][WPDatasetBaseDS.BREADCRUMBFIELD];
        }

        internal virtual void PopulateWorkpaperMetaData(DataSet data)
        {
            WPDatasetBaseDS wpData = data as WPDatasetBaseDS;

            if (wpData != null)
            {
                NableMenuButton notesTab = this.m_objHeader.CustomMenuButtons["buttonNotes"];
                NableMenuButton attachmentsTab = this.m_objHeader.CustomMenuButtons["buttonAttachments"];

                if (notesTab != null)
                {
                    int notesCount = wpData.GetNotesCount(this.WorkpaperID);
                    notesTab.Title = String.Format("You have {0} note(s)", notesCount);

                    if (notesCount > 0)
                    {
                        HasNotes = true;
                        notesTab.CssClass = "NotesPresent";
                    }
                    else
                    {
                        HasNotes = false;
                        notesTab.CssClass = String.Empty;
                    }
                }

                if (attachmentsTab != null)
                {
                    attachmentsTab.Title = String.Format("You have {0} attachment(s)", wpData.AttachmentsCount);

                    if (wpData.AttachmentsCount > 0)
                    {
                        HasAttachments = true;
                        attachmentsTab.CssClass = "AttachmentsPresent";
                    }
                    else
                    {
                        attachmentsTab.CssClass = String.Empty;
                        HasAttachments = false;
                    }
                }

            }
        }

        private void SwitchTab(string strTabCommand)
        {
            bool performSaveOnChangeTab = true;

            //Do not attempt to save a workpaper when switching tabs if either 
            //the scenario is locked or the tab explicitly is marked not to save
            if (IsBMCScenarioLocked())
                performSaveOnChangeTab = false;

            if (!this.m_objActiveTab.SaveWhenChangeTab)
                performSaveOnChangeTab = false;

            if (!performSaveOnChangeTab || (performSaveOnChangeTab && this.SaveWorkpaper(false)))
            {
                TabType objNewTab = TabFactory.GetTabTypeFromCommand(strTabCommand);

                // only switch if the tab is different
                if (this.ActiveTabType != objNewTab)
                {
                    this.ActiveTabType = objNewTab;
                    this.m_objActiveTab = TabFactory.CreateTab(objNewTab, this);
                    this.ReplaceTab(this.m_objActiveTab);
                }

                //If the tab being switched to is the Workpaper one, then get the page to
                //redirect to itself, as dynamic controls need to be added to the page 
                //in the page_load method otherwise the data is lost when a save
                //is performed

                DataSet objData = this.ActiveTab.CreateTabDataset();

                if (!this.GetBMCData(objData))
                    return;
                this.PopulateBreadcrumb(objData);
                this.PopulateWorkpaperMetaData(objData);
                this.SetSelectedButton();
                this.ActiveTab.PopulateTab(objData);

                if (this.ActiveTabType == TabType.Workpaper || this.ActiveTabType == TabType.Tea)
                {
                    this.m_objActiveTab.ResetDynamicTabControlsData(objData);
                    this.m_objActiveTab.PopulateTab(objData);
                }
            }
        }
        private void RefreshCategory(DataSet objData)
        {
            DataSet objExtractedDataSet = this.ActiveTab.CreateTabDataset();
            objExtractedDataSet = objData;

            this.m_objData = objExtractedDataSet;

            this.PopulateBreadcrumb(objExtractedDataSet);
            this.PopulateWorkpaperMetaData(objExtractedDataSet);

            this.ActiveTab.PopulateTab(objExtractedDataSet);
            this.InitialiseUpdateTokens();
        }
        private void SetSelectedButton()
        {
            string strSelectedButtonName = this.SelectedMenuButton;

            if (this.m_objActiveTab.TabType == TabType.Workpaper &&
                strSelectedButtonName == String.Empty)
            {
                strSelectedButtonName = "buttonWorkpaper";
            }
            else if (this.m_objActiveTab.TabType == TabType.Notes)
                strSelectedButtonName = "buttonNotes";
            else if (this.m_objActiveTab.TabType == TabType.Attachments)
                strSelectedButtonName = "buttonAttachments";
            else if (this.m_objActiveTab.TabType == TabType.Targets)
                strSelectedButtonName = "buttonCMTargets";
            else if (this.m_objActiveTab.TabType == TabType.Tea)
                strSelectedButtonName = "buttonTeaTab";

            foreach (NableMenuButton objButton in this.m_objHeader.CustomMenuButtons)
                objButton.Selected = (objButton.ID == strSelectedButtonName);
        }

        /// <summary>
        /// This will only be triggered if the Workpaper is set to read only mode. i.e. the ReadOnly is set to true
        /// </summary>
        private void MakeWorkpaperReadOnly()
        {
            string[] controlsToEnable = this.EnabledControls();

            // Disable All Menu buttons
            foreach (NableMenuButton objButton in this.m_objHeader.CustomMenuButtons)
                objButton.Enabled = false;

            // Only enable selected Menu Buttons
            foreach (string controlName in controlsToEnable)
            {
                foreach (NableMenuButton objButton in this.m_objHeader.CustomMenuButtons)
                {
                    if (objButton.ID == controlName)
                        objButton.Enabled = true;
                }
            }

            // Enable / disable controls on the form.

            foreach (string controlName in controlsToEnable)
            {
                foreach (Control ctrl in this.Form.Controls)
                {

                    if (ctrl is Tab)
                    {
                        foreach (Control objControl in ctrl.Controls)
                        {
                            if (!(objControl is INableHeader) && !(objControl is HtmlForm) && !(objControl is Javascript) && !(objControl is Stylesheet))
                            {
                                if (objControl.ID == controlName)
                                    ((WebControl)objControl).Enabled = true;
                                else
                                {
                                    if ((objControl is TextBox) || (objControl is CheckBox) || (objControl is RadioButton) || (objControl is DropDownList) || (objControl is ListBox))
                                        ((WebControl)objControl).Enabled = false;
                                }
                            }
                        }
                    }
                }
            }
        }


        protected virtual string[] EnabledControls()
        {
            return new string[] { string.Empty };
        }

        /// <summary>
        /// If the BMC is a TaxTopicCM and does not have the IsAutoAdjustCM
        /// property set to true, then we need to hide the Targets tab as configurable
        /// tax targets are not required.
        /// </summary>
        private void HideTargetsButton()
        {
           
        }

        /// <summary>
        /// Whether Tea tab should be accessable
        /// </summary>
        private void EnableTeaButton()
        {
            if (this.HasTeaTab)
            {
                NableMenuButton objButton = this.NableHeader.CustomMenuButtons["buttonTeaTab"];
                if (objButton != null)
                    objButton.Visible = HasTeaTab;
            }
        }
        private void HandleMissingComponent()
        {
            RedirectToErrorPage(this.MissingComponentErrorTitle,
                this.MissingComponentErrorDescription,
                ((this.ReturnURL == null || this.ReturnURL == String.Empty) ? "Close" : "Entities_1_1.aspx"));
        }

        protected void RedirectToErrorPage(string errorTitle, string errorDescription, string returnURL)
        {
            string strWorkpaper = this.m_objBroker.GetVersionedWorkpaper("ErrorPage.aspx", "ErrorHandler_1_1");

            this.m_objBroker.SetAbort();

            Response.Redirect(String.Format(strWorkpaper + "?Heading={0}&Message={1}&ReturnURL={2}",
                HttpUtility.UrlEncode(errorTitle),
                HttpUtility.UrlEncode(errorDescription),
                HttpUtility.UrlEncode(returnURL)), true);
        }

        private void HandleUpdateConflict()
        {
            this.RefreshWorkpaper(false);
            this.m_objHeader.ErrorText = this.UpdateConflictMessage;
            this.LogEvent(EventType.MultiUserEvent, EventType.EventDetailsArray[EventType.MultiUserEvent].EventName + " in Workpaper '" + this.WorkpaperName + "' in '" + this.m_objHeader.BreadCrumb + "'");
        }

        private void HandleDisplayUserMessage(DisplayUserMessageException objMessageException)
        {
            if (objMessageException.ReloadWorkpaper)
                this.RefreshWorkpaper(true);

            bool redirectionRequired = (objMessageException.RedirectToOriginalReferrer ||
                objMessageException.RedirectWorkpaper != String.Empty);

            if (objMessageException.MessageDisplay == MessageDisplayMethod.MainWindowError)
            {
                this.SetErrorMessageText(objMessageException);
            }
            else
            {
                this.RegisterMessageBoxOnStartup("Error", "Error", this.GetErrorMessageText(objMessageException), new MessageBoxButton[] { new MessageBoxButton(MessageBoxButtonType.OK, redirectionRequired) }, this.GetErrorMessageBoxArgs(objMessageException), PromptType.Error);
            }
        }

        private void HandleDisplayUserMessageOnLoad(DisplayUserMessageException objMessageException)
        {
            this.InitialiseUpdateTokens();

            bool redirectionRequired = (objMessageException.RedirectToOriginalReferrer ||
                objMessageException.RedirectWorkpaper != String.Empty);

            if (objMessageException.MessageDisplay == MessageDisplayMethod.MainWindowError &&
                redirectionRequired)
            {
                this.m_strRedirectActionURL = objMessageException.RedirectWorkpaper;
                this.RedirectSpecificWorkpaper();
            }
            else if (objMessageException.MessageDisplay == MessageDisplayMethod.MainWindowError)
            {
                this.SetErrorMessageText(objMessageException);
            }
            else
            {
                this.RegisterMessageBoxOnStartup("Error", "Error", this.GetErrorMessageText(objMessageException), new MessageBoxButton[] { new MessageBoxButton(MessageBoxButtonType.OK, redirectionRequired) }, this.GetErrorMessageBoxArgs(objMessageException), PromptType.Error);
            }

            this.m_bolIsInvalidDataset = true;

            if (this.m_objHeader != null)
            {
                foreach (NableMenuButton objButton in this.m_objHeader.CustomMenuButtons)
                    objButton.Enabled = objButton.EnabledOnDisplayUserMessage;
            }
        }
        /// <summary>
        /// The following method handles GetUserConfirmationException
        /// Its calls RegisterMessageBoxOnStartUp(+ 1 overload) to register message box on the page. 
        /// </summary>
        /// <param name="getUserConfirmation"></param>
        private void HandleGetUserConfirmation(GetUserConfirmationException getUserConfirmation)
        {
            if (getUserConfirmation.AllowUserToCancelConfirmedOperation)
                this.AllowUserToCancelOperation = true;

            string errorText = getUserConfirmation.ToString().Replace("\n", "<br>");
            string confirmType = getUserConfirmation.ConfirmationType;
            if (getUserConfirmation.ControlCollection is MessageBoxControlsList)
                this.RegisterMessageBoxOnStartup("Confirm", "Confirm", errorText, new MessageBoxButton[] { new MessageBoxButton(MessageBoxButtonType.Yes, true), new MessageBoxButton(MessageBoxButtonType.No, false) }, "UserConfirmation#" + confirmType, PromptType.Question, (MessageBoxControlsList)getUserConfirmation.ControlCollection);
            else
                this.RegisterMessageBoxOnStartup("Confirm", "Confirm", errorText, new MessageBoxButton[] { new MessageBoxButton(MessageBoxButtonType.Yes, true), new MessageBoxButton(MessageBoxButtonType.No, false) }, "UserConfirmation#" + confirmType, PromptType.Question);
        }


        private void SetErrorMessageText(DisplayUserMessageException objMessageException)
        {
            this.m_objHeader.ErrorText = GetErrorMessageText(objMessageException);
        }

        private string GetErrorMessageText(DisplayUserMessageException messageException)
        {
            StringBuilder errorText = new StringBuilder();

            foreach (MessageBoxDefinition objItem in messageException.MBDItems)
            {
                errorText.Append(objItem.ToString());
                errorText.Append("<br>");
            }
            errorText = errorText.Replace("\n", "<br>");

            return errorText.ToString();
        }

        private string GetErrorMessageBoxArgs(DisplayUserMessageException messageException)
        {
            if (messageException.RedirectToOriginalReferrer)
                return "RedirectReferrer";
            else if (messageException.RedirectWorkpaper != String.Empty)
                return "RedirectWorkpaper#" + messageException.RedirectWorkpaper;
            else
                return String.Empty;
        }

        protected void Complete()
        {
            try
            {
                if (this.m_objBroker.InTransaction)
                {
                    this.ReleaseObjects();
                    this.m_objBroker.SetComplete();
                }
            }
            catch
            {
                this.m_objBroker.SetAbort();
                throw;
            }
        }

        private void CleanupBroker()
        {
            if (this.m_objBroker != null)
            {
                if (this.m_objBroker.InTransaction)
                {
                    this.m_objBroker.SetAbort();
                    Debug.Assert(true, "BrokerManagedComponentWorkpaper.CleanupBroker(), transaction on the broker was not ended correctly");
                }
                this.m_objBroker.Dispose();
            }
        }

        public abstract void PopulateWorkpaper(DataSet objData);
        public abstract void DepopulateWorkpaper(DataSet objData);
        public abstract DataSet CreateDataSet();
        public abstract TabType[] GetTabs();
        protected abstract IBrokerManagedComponent CreateNewBrokerManagedComponent();


        /// <summary>
        /// Save the given dataset 
        /// This methon is not worried about de-populating active tab and re-populating other objects
        /// All it does is Save dataset
        /// </summary>
        /// <returns>True if the workpaper was saved, false otherwise</returns>
        private bool SaveWorkpaper(DataSet ds)
        {
            Page.Validate();
            if ((!Page.IsValid) || (CustomValidation() == false))
                return false;

            bool bolNewBMCCreated = false;
            DataSet objData = null;

            //// if the property indicates that we don't need to save
            //// then don't save
            //if(this.ShouldSaveWorkpaper)
            //{
            this.Broker.SetComplete();

            // re-initialise under a new write transaction
            this.Broker.SetWriteStart();
            this.InitialiseIDs(Guid.Empty);
            this.InitialiseObjects();

            try
            {
                if (!this.TransientWorkpaper)
                {
                    if (this.m_objInstanceID == Guid.Empty)
                    {
                        // if this is a new instance, create the instance and update the token
                        this.m_objBMC = this.CreateNewBrokerManagedComponent();
                        this.m_objInstanceID = this.m_objBMC.CID;
                        InitialiseUpdateTokens();
                        bolNewBMCCreated = true;
                    }

                    if (this.BMCUpdateToken != this.m_objBMC.UpdateToken)
                    {
                        this.HandleUpdateConflict();
                        return false;
                    }
                }

                this.m_objBMC.SetData(ds);
            }
            catch (DisplayUserMessageException objDisplayUserMessageException)
            {
                this.m_objBroker.SetAbort();
                this.m_objBroker.SetStart();
                if (!bolNewBMCCreated)
                    this.InitialiseObjects();
                this.HandleDisplayUserMessage(objDisplayUserMessageException);
                this.m_objBroker.SetComplete();
                return false;
            }
            catch (GetUserConfirmationException objGetUserConfirmation)
            {
                this.HandleGetUserConfirmation(objGetUserConfirmation);
                this.m_objBroker.SetAbort();
                return false;
            }
            catch (MissingComponentException)
            {
                this.HandleMissingComponent();
            }

            this.DisplayWarningMessages(objData);
            //}

            this.InitialiseUpdateTokens();

            // indicate that we have saved the workpaper
            return true;
        }
        /// <summary>
        /// Save the workpaper
        /// </summary>
        /// <param name="bolPerformAction">True if the subsequent actions, e.g. 
        /// navigate to another workpaper, should be performed</param>
        /// <returns>True if the workpaper was saved, false otherwise</returns>
        private bool SaveWorkpaper(bool bolPerformAction)
        {
            Page.Validate();
            if ((!Page.IsValid) || (CustomValidation() == false))
                return false;

            bool bolNewBMCCreated = false;
            DataSet objData = null;

            // if the property indicates that we don't need to save
            // then don't save
            if (this.ShouldSaveWorkpaper)
            {
                this.Broker.SetComplete();

                // re-initialise under a new write transaction
                this.Broker.SetWriteStart();
                this.InitialiseIDs(Guid.Empty);
                this.InitialiseObjects();

                try
                {
                    if (!this.TransientWorkpaper)
                    {
                        if (this.m_objInstanceID == Guid.Empty)
                        {
                            // if this is a new instance, create the instance and update the token
                            this.m_objBMC = this.CreateNewBrokerManagedComponent();
                            this.m_objInstanceID = this.m_objBMC.CID;
                            InitialiseUpdateTokens();
                            bolNewBMCCreated = true;
                        }

                        if (this.BMCUpdateToken != this.m_objBMC.UpdateToken)
                        {
                            this.HandleUpdateConflict();
                            return false;
                        }
                    }

                    if (this.m_bolIsInvalidDataset)
                        return false;

                    objData = this.m_objData;

                    this.m_objActiveTab.DepopulateTab(objData);
                    if (m_objActiveTab.TabName == "Targets")
                    {
                        bool isValid = m_objActiveTab.DepopulateTargetTab(objData);

                        if (!isValid)
                            return false;
                    }
                    this.m_objBMC.SetData(objData);

                    CMCacheManager.CacheManager.UpdateCacheEntry(m_objBMC);

                }
                catch (DisplayUserMessageException objDisplayUserMessageException)
                {
                    this.m_objBroker.SetAbort();
                    this.m_objBroker.SetStart();
                    if (!bolNewBMCCreated)
                        this.InitialiseObjects();
                    this.HandleDisplayUserMessage(objDisplayUserMessageException);
                    this.m_objBroker.SetComplete();
                    return false;
                }
                catch (GetUserConfirmationException objGetUserConfirmation)
                {
                    this.HandleGetUserConfirmation(objGetUserConfirmation);
                    this.m_objBroker.SetAbort();
                    return false;
                }
                catch (MissingComponentException)
                {
                    this.HandleMissingComponent();
                }

                this.DisplayWarningMessages(objData);
            }

            this.m_objData = this.m_objActiveTab.CreateTabDataset();
            objData = this.m_objData;

            if (!this.GetBMCData(objData))
                return false;

            this.PopulateBreadcrumb(objData);
            this.PopulateWorkpaperMetaData(objData);

            
            this.m_objActiveTab.ResetDynamicTabControlsData(objData);
            this.m_objActiveTab.PopulateTab(objData);

            this.InitialiseUpdateTokens();

            //IBrokerManagedComponent tempBMC = m_objBMC;

            if (bolPerformAction)
            {
                if (bolNewBMCCreated &&
                    this.PostSaveActionType == ActionType.RemainOnWorkpaper)
                    this.RedirectToSelf();
                else
                    this.PerformAction(this.PostSaveActionType);
            }

            //if (tempBMC != null)
            //    CMCacheManager.CacheManager.UpdateCacheEntry(tempBMC);

            // indicate that we have saved the workpaper
            return true;
        }

        public bool IsPrintRequest
        {
            get
            {
                if (this.Request.Params.Get("PrintRequest") != null)
                {
                    if ((this.Request["PrintRequest"] != null) && (this.Request["PrintRequest"].ToLower() == "true"))
                        return true;
                    else
                        return false;
                }
                else
                    return false;
            }
        }

        private void RefreshWorkpaper(bool reloadData)
        {
            try
            {
                DataSet objExtractedDataSet = this.ActiveTab.CreateTabDataset();

                if (this.m_objData == null || reloadData || !this.IsResetPostbackDataSet)
                {
                    if (!this.GetBMCData(objExtractedDataSet))
                        return;
                }

                else
                    objExtractedDataSet = this.m_objData;

                this.m_objData = objExtractedDataSet;

                this.PopulateBreadcrumb(objExtractedDataSet);
                this.PopulateWorkpaperMetaData(objExtractedDataSet);

                if (!IsResetPostbackDataSet)
                    this.m_objPostBackDataset = m_objData;

                this.ActiveTab.PopulateTab(objExtractedDataSet);
                this.InitialiseUpdateTokens();
            }
            catch (MissingComponentException)
            {
                this.HandleMissingComponent();
            }
        }

        private bool GetBMCData(DataSet objExtractedData)
        {
            try
            {
                this.m_objBMC.GetData(objExtractedData);
            }
            catch (DisplayUserMessageException objMessage)
            {
                this.HandleDisplayUserMessageOnLoad(objMessage);
                return false;
            }

            return true;
        }

        protected virtual void CloseWorkpaper()
        {
            bool isLocked = IsBMCScenarioLocked();
            //If the scenario is locked, then do not try to perform a 
            //save when closing the workpaper.
            if (isLocked || SaveWorkpaper(false))
            {
                this.PerformAction(this.CloseActionType);
            }
        }

        protected bool IsBMCScenarioLocked()
        {
            if (this.m_objBMC is ICalculationModule)
            {
                if (((ICalculationModule)m_objBMC).IsScenarioLocked())
                    return true;
            }
            return false;
        }

        private void CancelWorkpaper()
        {
            this.PerformAction(this.CancelActionType);
        }

        protected void PerformAction(ActionType objActionType)
        {
            switch (objActionType)
            {
                case ActionType.CloseScreen:
                    this.CloseWorkpaperScreen();
                    break;

                case ActionType.RedirectToOriginalReferrer:
                    this.RedirectWorkpaper();
                    break;

                case ActionType.RemainOnWorkpaper:
                    this.RemainOnWorkpaper();
                    break;

                case ActionType.RedirectToSpecificWorkpaper:
                    this.RedirectSpecificWorkpaper();
                    break;

                case ActionType.RedirectToSelf:
                    this.RedirectToSelf();
                    break;
            }
        }

        private void CloseWorkpaperScreen()
        {
            Javascript objCloseScript = new Javascript(JavaScriptType.Inline, String.Empty);
            objCloseScript.InlineJavaScript = "window.close();";
            this.Page.ClientScript.RegisterStartupScript(this.GetType(), "CloseWindow", objCloseScript.ToString());
        }

        private void RedirectWorkpaper()
        {
            this.Complete();
            if (IsTargetSet)
            {
                iFrame.Attributes["src"] = this.ReturnURL;
            }
            else
            {
                this.Page.Response.Redirect(this.ReturnURL);
                //this.Page.Response.Redirect(printWPURL);
            }
        }

        private void RemainOnWorkpaper()
        {
            this.Complete();
        }

        private void RedirectSpecificWorkpaper()
        {
            if (this.m_strRedirectActionURL == null ||
                this.m_strRedirectActionURL == String.Empty)
                throw new Exception("ActionType.RedirectSpecificWorkpaper specified, but no corresponding workpaper has been set");

            this.Complete();
            if (IsTargetSet)
            {
                iFrame.Attributes["src"] = this.m_strRedirectActionURL;
            }
            else
            {
                this.Page.Response.Redirect(this.m_strRedirectActionURL);
            }
        }

        /// <summary>
        /// Sets the workpaper to redirect to when the ActionType is set to RedirectSpecificWorkpaper
        /// </summary>
        /// <param name="strPageName">The workpaper to redirect to</param>
        public void SetRedirectActionPage(string strPageName)
        {
            this.SetRedirectActionPage(strPageName, null, null);
        }

        /// <summary>
        /// Sets the workpaper to redirect to when the ActionType is set to RedirectSpecificWorkpaper
        /// </summary>
        /// <param name="strPageName">The workpaper to redirect to</param>
        /// <param name="strArgNames">The name of any arguments that you need to pass to the page</param>
        /// <param name="strArgValues">The value of any arguments that you need to pass to the page</param>
        public void SetRedirectActionPage(string strPageName, string[] strArgNames, string[] strArgValues)
        {
            this.SetRedirectActionPage(strPageName, true, strArgNames, strArgValues);
        }

        /// <summary>
        /// Sets the workpaper to redirect to when the ActionType is set to RedirectSpecificWorkpaper
        /// </summary>
        /// <param name="strPageName">The workpaper to redirect to</param>
        /// <param name="bolIncludeNavigationIDs"></param>
        public void SetRedirectActionPage(string strPageName, bool bolIncludeNavigationIDs)
        {
            this.SetRedirectActionPage(strPageName, bolIncludeNavigationIDs, null, null);
        }

        /// <summary>
        /// Sets the workpaper to redirect to when the ActionType is set to RedirectSpecificWorkpaper
        /// </summary>
        /// <param name="strPageName">The workpaper to redirect to</param>
        /// <param name="bolIncludeNavigationIDs"></param>
        /// <param name="strArgNames">The name of any arguments that you need to pass to the page</param>
        /// <param name="strArgValues">The value of any arguments that you need to pass to the page</param>
        public void SetRedirectActionPage(string strPageName, bool bolIncludeNavigationIDs, string[] strArgNames, string[] strArgValues)
        {
            if (bolIncludeNavigationIDs)
            {
                this.m_strRedirectActionURL = this.BuildSelfRedirectActionURL(this.Broker.GetVersionedWorkpaper(strPageName, this.m_objBMC),
                    strArgNames, strArgValues);
            }
            else
            {
                this.m_strRedirectActionURL = this.BuildRedirectActionURL(this.Broker.GetVersionedWorkpaper(strPageName, this.m_objBMC),
                    strArgNames, strArgValues);
            }
        }

        public void SetRedirectActionPage(string strPageName, string strComponentName)
        {
            this.SetRedirectActionPage(strPageName, strComponentName, null, null);
        }

        public void SetRedirectActionPage(string strPageName, string strComponentName, bool bolIncludeNavigationIDs)
        {
            this.SetRedirectActionPage(strPageName, strComponentName, bolIncludeNavigationIDs, null, null);
        }

        public void SetRedirectActionPage(string strPageName, string strComponentName, string[] strArgNames, string[] strArgValues)
        {
            SetRedirectActionPage(strPageName, strComponentName, false, strArgNames, strArgValues);
        }

        public void SetRedirectActionPage(string strPageName, string strComponentName, bool bolIncludeNavigationIDs, string[] strArgNames, string[] strArgValues)
        {
            if (bolIncludeNavigationIDs)
            {
                this.m_strRedirectActionURL = this.BuildSelfRedirectActionURL(this.Broker.GetVersionedWorkpaper(strPageName, strComponentName),
                    strArgNames, strArgValues);
            }
            else
            {
                this.m_strRedirectActionURL = this.BuildRedirectActionURL(this.Broker.GetVersionedWorkpaper(strPageName, strComponentName),
                    strArgNames, strArgValues);
            }
        }

        public void SetRedirectActionPage(string strPageName, Guid objCLID, Guid objCSID)
        {
            this.SetRedirectActionPage(strPageName, objCLID, objCSID, null, null);
        }

        public void SetRedirectActionPage(string strPageName, Guid objCLID, Guid objCSID, string[] strArgNames, string[] strArgValues)
        {
            int intArgNamesLength = 0;
            int intArgValuesLength = 0;

            if (strArgNames != null)
                intArgNamesLength = strArgNames.Length;

            if (strArgValues != null)
                intArgValuesLength = strArgValues.Length;

            string[] strNewArgNames = new string[intArgNamesLength + 2];
            string[] strNewArgValues = new string[intArgValuesLength + 2];

            if (strArgNames != null)
                strArgNames.CopyTo(strNewArgNames, 2);

            if (strArgValues != null)
                strArgValues.CopyTo(strNewArgValues, 2);

            strNewArgNames[0] = "CLID";
            strNewArgNames[1] = "CSID";

            strNewArgValues[0] = objCLID.ToString();
            strNewArgValues[1] = objCSID.ToString();

            this.m_strRedirectActionURL = this.BuildRedirectActionURL(this.Broker.GetVersionedWorkpaper(strPageName, objCLID, objCSID),
                strNewArgNames, strNewArgValues);
        }

        public void SetRedirectActionPage(string strPageName, Guid objCID)
        {
            this.SetRedirectActionPage(strPageName, objCID, null, null);
        }

        public void SetRedirectActionPage(string strPageName, Guid objCID, string[] strArgNames, string[] strArgValues)
        {
            int intArgNamesLength = 0;
            int intArgValuesLength = 0;

            if (strArgNames != null)
                intArgNamesLength = strArgNames.Length;

            if (strArgValues != null)
                intArgValuesLength = strArgValues.Length;

            string[] strNewArgNames = new string[intArgNamesLength + 1];
            string[] strNewArgValues = new string[intArgValuesLength + 1];

            if (strArgNames != null)
                strArgNames.CopyTo(strNewArgNames, 1);

            if (strArgValues != null)
                strArgValues.CopyTo(strNewArgValues, 1);

            strNewArgNames[0] = "CID";
            strNewArgValues[0] = objCID.ToString();

            this.m_strRedirectActionURL = this.BuildRedirectActionURL(this.Broker.GetVersionedWorkpaper(strPageName, objCID),
                strNewArgNames, strNewArgValues);
        }

        protected string BuildSelfRedirectActionURL(string strVersionedWorkpaper, string[] strArgNames, string[] strArgValues)
        {
            StringBuilder objSB = new StringBuilder();
            objSB.Append(strVersionedWorkpaper);
            objSB.Append("?");

            //Store querystring seperately as therefore parameters can be checked before adding
            //to ensure that they are not added twice which occurred previously when a postback 
            //actiontype on a workpaper was set to 'RedirectToSelf'
            StringBuilder queryString = new StringBuilder();
            queryString.Append(this.GetWorkpaperSpecificURLIDs());

            if (strArgNames != null && strArgValues != null)
            {
                for (int intArgCounter = 0; intArgCounter < strArgNames.Length; intArgCounter++)
                {
                    if (strArgNames[intArgCounter] != null)
                    {
                        //If the parameter is not already part of the query string then 
                        //add it on
                        int indexOfParameter = queryString.ToString().IndexOf(strArgNames[intArgCounter]);
                        if (indexOfParameter == -1)
                        {
                            queryString.Append("&");
                            queryString.Append(strArgNames[intArgCounter]);
                            queryString.Append("=");
                            queryString.Append(strArgValues[intArgCounter]);
                        }
                        else //Otherwise if the parameter already exists, then set it to the value in the ArgValues collection
                        {
                            RequestParameterString query = new RequestParameterString(queryString.ToString());
                            query[strArgNames[intArgCounter]] = strArgValues[intArgCounter];
                            queryString = new StringBuilder(query.ToString());
                        }
                    }
                }
            }
            return objSB.Append(queryString).ToString();
        }

        protected string BuildRedirectActionURL(string strVersionedWorkpaper, string[] strArgNames, string[] strArgValues)
        {
            StringBuilder objSB = new StringBuilder();
            objSB.Append(strVersionedWorkpaper);
            objSB.Append("?");

            if (strArgNames != null && strArgValues != null)
            {
                for (int intArgCounter = 0; intArgCounter < strArgNames.Length; intArgCounter++)
                {
                    if (intArgCounter > 0)
                        objSB.Append("&");
                    objSB.Append(strArgNames[intArgCounter]);
                    objSB.Append("=");
                    objSB.Append(strArgValues[intArgCounter]);
                }
            }
            return objSB.ToString();
        }

        protected virtual string GetWorkpaperSpecificURLIDs()
        {
            return "CID=" + this.m_objInstanceID.ToString();
        }

        public string GetClientsideWorkpaperReference(string strPageName)
        {
            return this.GetClientsideWorkpaperReference(strPageName, null, null);
        }

        /// <summary>
        /// Sets the workpaper to redirect to when the ActionType is set to RedirectSpecificWorkpaper
        /// </summary>
        /// <param name="strPageName">The workpaper to redirect to</param>
        /// <param name="strArgNames">The name of any arguments that you need to pass to the page</param>
        /// <param name="strArgValues">The value of any arguments that you need to pass to the page</param>
        public string GetClientsideWorkpaperReference(string strPageName, string[] strArgNames, string[] strArgValues)
        {
            return this.BuildSelfRedirectActionURL(this.Broker.GetVersionedWorkpaper(strPageName, this.m_objBMC),
                strArgNames, strArgValues);
        }

        /// <summary>
        /// Returns a Url to a given versioned workpaper within a given component accompanied by the given argument name and value pairs
        /// </summary>
        /// <param name="strPageName">The workpaper name</param>
        /// <param name="strComponentName">The component name</param>
        /// <param name="strArgNames">The name of any arguments that you need to add to the url</param>
        /// <param name="strArgValues">The value of any arguments that you need to add to the url</param>
        public string GetClientsideWorkpaperReference(string strPageName, string strComponentName, string[] strArgNames, string[] strArgValues)
        {
            return this.BuildSelfRedirectActionURL(this.Broker.GetVersionedWorkpaper(strPageName, strComponentName), strArgNames, strArgValues);
        }

        protected override bool OnBubbleEvent(object source, EventArgs args)
        {
            return true;
        }

        protected override void RaisePostBackEvent(IPostBackEventHandler sourceControl, string eventArgument)
        {
            try
            {
                this.messageBox.ShowOnPageLoad = false;

                if (this.m_bolIsInvalidDataset)
                    return;

                // make the dataset available and set the action to remain on workpaper;
                this.m_objPostBackDataset = this.m_objData;
                this.m_objAfterPostBackAction = ActionType.RemainOnWorkpaper;
                CommandEventArgs objEventArgs = new CommandEventArgs(eventArgument, String.Empty);

                if (!this.HandleMenuCommand(sourceControl, objEventArgs))
                {
                    bool bPerformAction = true;
                    this.m_objData.AcceptChanges();

                    // check if it is a custom menu command
                    if (!this.HandleCustomMenuCommand(sourceControl, objEventArgs))
                    {
                        base.RaisePostBackEvent(sourceControl, eventArgument);

                        if (this.m_objData.HasChanges() || !this.IsResetPostbackDataSet)
                            bPerformAction = this.SaveWorkpaper(false);
                        else
                        {
                            if (this.m_objAfterPostBackAction == ActionType.RemainOnWorkpaper)
                            {
                                this.m_objActiveTab.PopulateTab(this.m_objData);
                            }
                        }
                    }

                    this.Complete();
                    if (bPerformAction) this.PerformAction(this.m_objAfterPostBackAction);
                }
            }
            catch (MissingComponentException)
            {
                this.HandleMissingComponent();
            }

            // remove access to the dataset and reset to the default action as postback events have completed
            if (IsResetPostbackDataSet)
                ResetPostbackDataset();
            else
                this.m_objPostBackDataset = this.m_objData;

            this.m_objAfterPostBackAction = ActionType.RemainOnWorkpaper;
        }

        public virtual void ResetPostbackDataset()
        {
            this.m_objPostBackDataset = null;
        }

        public virtual bool IsResetPostbackDataSet
        {
            get
            {
                return true;
            }
        }

        protected void OnCallBack(DataSet ds)
        {
            if (this.Broker == null)
                InitialiseBroker();
            try
            {
                //this.InitialiseIDs(Guid.Empty);
                //this.InitialiseObjects();
                //this.InitialiseReturnURL();
                //this.LoadWorkpaper();
                this.SetCachingParameters();

                this.m_objPostBackDataset = ds;
                if (SaveWorkpaper(ds))
                {
                    ds.AcceptChanges();
                    Complete();
                }
            }
            catch (MissingComponentException)
            {
                this.HandleMissingComponent();
            }
            catch
            {
                this.m_objBroker.SetAbort();
                throw;
            }
        }

        public virtual void SaveDataGrid(DataGrid objDataGrid, string strDatasetTableName, DataSet objData)
        {
            DataTable objDataTable = objData.Tables[strDatasetTableName];

            foreach (DataGridItem objItem in objDataGrid.Items)
            {
                if (objItem.ItemType == ListItemType.Item ||
                    objItem.ItemType == ListItemType.AlternatingItem)
                {
                    Guid objKey = new Guid(objDataGrid.DataKeys[objItem.ItemIndex].ToString());
                    DataRow[] objRows = objDataTable.Select("Key = '" + objKey + "'");

                    if (objRows.Length > 0)
                    {
                        DataRow objSelectedRow = objRows[0];

                        for (int intCellCounter = 0; intCellCounter < objItem.Cells.Count; intCellCounter++)
                        {
                            TableCell objCell = objItem.Cells[intCellCounter];

                            if (objCell.Controls.Count > 1)
                            {
                                Control objControl = objCell.Controls[1];

                                if (objControl is TextBox)
                                {
                                    string value = ((TextBox)objControl).Text.Trim();
                                    DataColumn dataCol = objDataTable.Columns[intCellCounter];
                                    if (dataCol.DataType == typeof(CurrencyValue))
                                        objSelectedRow[intCellCounter] = CurrencyValue.FromString(value);
                                    else if (dataCol.DataType == typeof(Date))
                                    {
                                        Date dateValue = new Date();
                                        if (value != "")
                                        {
                                            dateValue.FromString(value);
                                            objSelectedRow[intCellCounter] = dateValue;
                                        }
                                    }
                                    else
                                        objSelectedRow[intCellCounter] = value;
                                }
                                else if (objControl is CheckBox)
                                {
                                    objSelectedRow[intCellCounter] = ((CheckBox)objControl).Checked;
                                }
                                else if (objControl is DropDownList)
                                {
                                    objSelectedRow[intCellCounter] = ((DropDownList)objControl).SelectedItem.Value;
                                }
                            }
                        }
                    }
                }
            }
        }

        public virtual void SaveDataGrid(Telerik.Web.UI.RadGrid objRadGrid, string strDatasetTableName, DataSet objData)
        {
            DataTable objDataTable = objData.Tables[strDatasetTableName];


            foreach (Telerik.Web.UI.GridDataItem objradItem in objRadGrid.MasterTableView.Items)
            {
                if (objradItem.ItemType == Telerik.Web.UI.GridItemType.Item ||
                    objradItem.ItemType == Telerik.Web.UI.GridItemType.AlternatingItem ||
                    objradItem.ItemType == Telerik.Web.UI.GridItemType.EditItem)
                {
                    Guid objKey = new Guid(objRadGrid.MasterTableView.DataKeyValues[objradItem.ItemIndex][objRadGrid.MasterTableView.DataKeyNames[0]].ToString());
                    DataRow[] objRows = objDataTable.Select("Key = '" + objKey + "'");

                    if (objRows.Length > 0)
                    {
                        DataRow objSelectedRow = objRows[0];
                        #region "For loop"
                        foreach (Telerik.Web.UI.GridColumn _gridcolumn in objRadGrid.Columns)
                        {
                            foreach (Control objControl in objradItem[_gridcolumn].Controls)
                            {
                                if (objControl is TextBox)
                                {
                                    string value = ((TextBox)objControl).Text.Trim();
                                    DataColumn dataCol = objDataTable.Columns[_gridcolumn.UniqueName];
                                    if (dataCol.DataType == typeof(CurrencyValue))
                                    {
                                        objSelectedRow[_gridcolumn.UniqueName] = CurrencyValue.FromString(value);
                                        break;
                                    }
                                    else if (dataCol.DataType == typeof(Date))
                                    {
                                        Date dateValue = new Date();
                                        if (value != "")
                                        {
                                            dateValue.FromString(value);
                                            objSelectedRow[_gridcolumn.UniqueName] = dateValue;
                                            break;
                                        }
                                        break;
                                    }
                                    else
                                    {
                                        objSelectedRow[_gridcolumn.UniqueName] = value;
                                        break;
                                    }
                                }
                                else if (objControl is Telerik.Web.UI.RadEditor)
                                {
                                    string value = ((Telerik.Web.UI.RadEditor)objControl).Content;
                                    DataColumn dataCol = objDataTable.Columns[_gridcolumn.UniqueName];                                    
                                    objSelectedRow[_gridcolumn.UniqueName] = value;                                        
                                }
                                else if (objControl is Label)
                                {
                                    string value = ((Label)objControl).Text.Trim();
                                    DataColumn dataCol = objDataTable.Columns[_gridcolumn.UniqueName];
                                    if (dataCol.DataType == typeof(CurrencyValue))
                                    {
                                        objSelectedRow[_gridcolumn.UniqueName] = CurrencyValue.FromString(value);
                                        break;
                                    }
                                    else if (dataCol.DataType == typeof(Date))
                                    {
                                        Date dateValue = new Date();
                                        if (value != "")
                                        {
                                            dateValue.FromString(value);
                                            objSelectedRow[_gridcolumn.UniqueName] = dateValue;
                                            break;
                                        }
                                        break;
                                    }
                                    else
                                    {
                                        objSelectedRow[_gridcolumn.UniqueName] = value;
                                        break;
                                    }
                                }
                                else if (objControl is CheckBox)
                                {
                                    objSelectedRow[_gridcolumn.UniqueName] = ((CheckBox)objControl).Checked;
                                    break;
                                }
                                else if (objControl is DropDownList)
                                {
                                    objSelectedRow[_gridcolumn.UniqueName] = ((DropDownList)objControl).SelectedItem.Value;
                                    break;
                                }
                            }
                        }
                        #endregion
                    }
                }
            }
        }

        public void LogEvent(int eventType, string eventDetails)
        {
            CMBroker cmBroker = (CMBroker)this.Broker;
            if (cmBroker != null)
                cmBroker.LogEvent(eventType, Guid.Empty,eventDetails);
        }

        protected void ReplaceTab(Tab objTab)
        {
            for (int intControlCounter = 0; intControlCounter < this.Form.Controls.Count; intControlCounter++)
            {
                Control objControl = this.Form.Controls[intControlCounter];

                if (objControl is Tab)
                {
                    this.Form.Controls.Remove(objControl);
                    this.Form.Controls.AddAt(intControlCounter, objTab);
                    break;
                }
            }
        }

        /// <summary>
        /// Sorts the DT using the Expression given
        /// </summary>
        /// <param name="oDT"></param>
        /// <param name="sField"></param>
        /// <param name="eOrder"></param>
        public void SortDataTable(DataTable oDT, String sField, SortOrderEnum eOrder)
        {
            DataTable oSortedDT = oDT.Copy();
            DataTable oTMP = oDT.Clone();
            DataView oDV = oSortedDT.DefaultView;

            if (eOrder == SortOrderEnum.Assending)
                oDV.Sort = sField + " ASC";
            else
                oDV.Sort = sField + " DESC";

            oDT.Clear();

            foreach (DataRowView row in oDV)
            {
                String sData = row[sField].ToString().Trim();

                if (sData == String.Empty)
                    oTMP.ImportRow(row.Row); // Put Blank Rows at the bottom
                else
                    oDT.ImportRow(row.Row);
            }

            foreach (DataRow row in oTMP.Rows)
            {
                oDT.ImportRow(row);
            }
        }

        private void RedirectToSelf()
        {
            Uri objURI = this.Request.Url;

            // get the workpaper name
            string strPathPart = objURI.AbsolutePath;
            int intLastSlash = strPathPart.LastIndexOf("/");
            string strWorkpaper = strPathPart.Substring(intLastSlash + 1);

            string strQueryString = objURI.Query;
            //Remove any preceeding questions marks
            if (strQueryString.IndexOf("?") != -1)
                strQueryString = strQueryString.Split('?')[1];
            string[] strComponents = strQueryString.Split('&');
            string[] strQueryArgs = new string[strComponents.Length];
            string[] strQueryArgValues = new string[strComponents.Length];
            int intArgCounter = 0;

            foreach (string strComponent in strComponents)
            {
                string[] strArgs = strComponent.Split('=');

                if (strArgs.Length == 2)
                {
                    strQueryArgs[intArgCounter] = strArgs[0];
                    strQueryArgValues[intArgCounter] = strArgs[1];
                    intArgCounter++;
                }
            }

            string strSelfWorkpaper = this.BuildSelfRedirectActionURL(strWorkpaper, strQueryArgs, strQueryArgValues);
            this.Complete();
            Response.Redirect(strSelfWorkpaper);
        }

        /// <summary>
        /// Registers to display a message box on display of the page
        /// </summary>
        /// <param name="title">The title of the messagebox - appears as the window title</param>
        /// <param name="heading">The heading of the messagebox - appears inside the dialog at the top</param>
        /// <param name="body">The main body of the messagebox - will scroll if too long, converts \n to br</param>
        /// <param name="buttons">An array of buttons to display on the messagebox</param>
        /// <param name="args">An arg for the Messagebox, is passed to the event handler when a button is pressed</param>
        /// <param name="prompt">The prompt type, e.g. Error, Information etc.</param>
        public void RegisterMessageBoxOnStartup(string title, string heading, string body, MessageBoxButton[] buttons, string args, PromptType prompt)
        {

            this.messageBox.Title = title;
            this.messageBox.Heading = heading;
            this.messageBox.Body = body;
            this.messageBox.Arguments = args;
            this.messageBox.Prompt = prompt;
            this.messageBox.ClearMessageBoxButtons();
            //this.messageBox.ClearMessageBoxControls();
            foreach (MessageBoxButton button in buttons)
                this.messageBox.AddMessageBoxButton(button);
            this.messageBox.ShowOnPageLoad = true;

            if (prompt == PromptType.Error)
                this.LogEvent(EventType.ErrorPrompt, "Error message: '" + this.messageBox.Body.Replace("<br>", String.Empty) + "' prompt in workpaper '" + this.WorkpaperName + "' in '" + this.m_objHeader.BreadCrumb + "'");
        }
        /// <summary>
        /// Registers to display a message box on display of the page with MessageBoxCongtrolList instance
        /// </summary>
        /// <param name="title">The title of the messagebox - appears as the window title</param>
        /// <param name="heading">The heading of the messagebox - appears inside the dialog at the top</param>
        /// <param name="body">The main body of the messagebox - will scroll if too long, converts \n to br</param>
        /// <param name="buttons">An array of buttons to display on the messagebox</param>
        /// <param name="args">An arg for the Messagebox, is passed to the event handler when a button is pressed</param>
        /// <param name="prompt">The prompt type, e.g. Error, Information etc.</param>
        /// <param name="controlCollection">Control list: e.g. can have check box, radio buttons etc</param>
        public void RegisterMessageBoxOnStartup(string title, string heading, string body, MessageBoxButton[] buttons, string args, PromptType prompt, MessageBoxControlsList controlCollection)
        {
            this.messageBox.Title = title;
            this.messageBox.Heading = heading;
            this.messageBox.Body = body;
            this.messageBox.Arguments = args;
            this.messageBox.Prompt = prompt;
            this.messageBox.ClearMessageBoxButtons();
            //this.messageBox.ClearMessageBoxControls();
            foreach (MessageBoxButton button in buttons)
                this.messageBox.AddMessageBoxButton(button);

            if (controlCollection.MessageBoxList != null || controlCollection.MessageBoxList.Count != 0)
            {
                if (controlCollection.MessageBoxList.Contains(ControlType.CheckBox))
                {
                    MessageBoxControlCollection checkBoxCollection = (MessageBoxControlCollection)controlCollection.MessageBoxList[ControlType.CheckBox];
                    //this.messageBox.AddMessageBoxControls(checkBoxCollection);
                }
            }
            this.messageBox.ShowOnPageLoad = true;
            if (prompt == PromptType.Error)
                this.LogEvent(EventType.ErrorPrompt, "Error message: '" + this.messageBox.Body.Replace("<br>", String.Empty) + "' prompt in workpaper '" + this.WorkpaperName + "' in '" + this.m_objHeader.BreadCrumb + "'");

        }
        /// <summary>
        /// Handles message box arguments. Which calls HandleUseConfirm method to set valuse to confirmation table. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void HandleMessageBoxButtonClick(object sender, MessageBoxEventArgs args)
        {
            if (args.EventArgs.StartsWith("UserConfirmation") && args.ButtonPressed == MessageBoxButtonType.Yes && args.ControlList != null)
            {
                // get the confirmation type
                string confirmType = string.Empty;
                string[] parts = args.EventArgs.Split(new Char[] { '#' });
                if (parts.Length > 1)
                    confirmType = parts[1];

                // Event Log
                this.LogEvent(EventType.ConfirmationPrompt, "User choose 'YES' for the confirmation message '" + ((MessageBox)sender).Body + "' in '" + this.m_objHeader.BreadCrumb + "'");
                this.HandleUserConfirm(confirmType, args.ControlList);
            }
            else if (args.EventArgs.StartsWith("UserConfirmation") && args.ButtonPressed == MessageBoxButtonType.Yes)
            {
                // get the confirmation type
                string confirmType = string.Empty;
                string[] parts = args.EventArgs.Split(new Char[] { '#' });
                if (parts.Length > 1)
                    confirmType = parts[1];

                // Event Log
                this.LogEvent(EventType.ConfirmationPrompt, "User choose 'YES' for the confirmation message '" + ((MessageBox)sender).Body + "' in '" + this.m_objHeader.BreadCrumb + "'");
                this.HandleUserConfirm(confirmType);
            }
            else if (args.EventArgs.StartsWith("RedirectReferrer"))
                this.RedirectWorkpaper();
            else if (args.EventArgs.StartsWith("RedirectWorkpaper"))
            {
                string[] redirectComponents = args.EventArgs.Split('#');
                this.m_strRedirectActionURL = redirectComponents[1];
                this.RedirectSpecificWorkpaper();
            }
            else if (this.MessageBoxButtonClicked != null)
                this.MessageBoxButtonClicked(sender, args);
        }

        private void DisplayWarningMessages(DataSet data)
        {
            WPDatasetBaseDS wpDataSetBase = data as WPDatasetBaseDS;

            if (wpDataSetBase != null)
            {
                StringBuilder warningMessage = new StringBuilder();
                StringBuilder logMessage = new StringBuilder();
                ArrayList warnings = wpDataSetBase.GetWarningUserDisplayMessages();

                if (warnings.Count > 0)
                {
                    foreach (string warning in warnings)
                    {
                        warningMessage.Append(warning);
                        logMessage.Append("'" + warning + "'");
                        warningMessage.Append("<br>");
                        logMessage.Append(";");
                    }

                    //this.LogEvent(EventType.ErrorPrompt, "Warning message:'" + logMessage.ToString().TrimEnd(';') + "' Prompt");
                    this.RegisterMessageBoxOnStartup("Warning", "Warning", warningMessage.ToString(), new MessageBoxButton[] { new MessageBoxButton(MessageBoxButtonType.OK, false) }, String.Empty, PromptType.Warning);
                }
            }
        }
        /// <summary>
        /// Gets a string variable stored in a viewstate with the given label. 
        /// Should be used together with SetViewState
        /// </summary>
        /// <param name="viewStateLabel"></param>
        /// <returns>Empty string if nothing was previously stored.</returns>
        protected string GetViewState(string viewStateLabel)
        {
            if (this.ViewState[viewStateLabel] != null)
                return (string)this.ViewState[viewStateLabel];
            else
                return string.Empty;
        }
        /// <summary>
        /// Stores a given value in the viewstate with the given label.
        /// Should be used together with GetViewState
        /// </summary>
        /// <param name="viewStateLabel"></param>
        /// <param name="strValue"></param>
        protected void SetViewState(string viewStateLabel, string strValue)
        {
            this.ViewState[viewStateLabel] = strValue;
        }

        protected virtual void LogWorkpaperAccess()
        {
            try
            {
                string details = string.Empty;

                if (m_objHeader != null)
                    details = string.Format("{0} {1}{2}{3}", this.m_objHeader.BreadCrumb, this.WorkpaperName, VisitedWorkpaperMarker, this.Request.RawUrl);
            }
            catch
            {
            }
        }

        #endregion
        #region Properties
        #region Overridable Properties
        protected abstract string BMCTypeName
        {
            get;
        }

        protected virtual Guid BMCTypeID
        {
            get
            {
                Assembly assembly = Assembly.GetAssembly(this.GetType().BaseType);
                object[] attributes = assembly.GetCustomAttributes(typeof(AssemblyInstallInfoAttribute), false);
                if (null != attributes && attributes.Length == 1)
                {
                    AssemblyInstallInfoAttribute assemblyInstallInfoAttribute = (AssemblyInstallInfoAttribute)(attributes[0]);
                    string ownerTypeIDStr = assemblyInstallInfoAttribute.AssemblyID;
                    return new Guid(ownerTypeIDStr);
                }
                else
                    throw new InvalidOperationException("OwnerTypeID is not implemented in this work paper");
            }
        }

        /// <summary>
        /// Custom Validation allows a child work paper to implement any custom validation. If it returns
        /// false, the workpaer is not saved. 
        /// </summary>
        /// <returns>True if not overridden</returns>
        protected virtual bool CustomValidation()
        {
            return true;
        }

        public string GetWPName()
        {
            return this.WorkpaperName;
        }

        protected abstract string MissingComponentErrorTitle
        {
            get;
        }

        protected abstract string MissingComponentErrorDescription
        {
            get;
        }

        protected virtual string TransactionCommitErrorTitle
        {
            get
            {
                return "Unable to complete the requested action";
            }
        }

        protected virtual string TransactionCommitErrorDescription
        {
            get
            {
                return new MessageBoxDefinition(MessageBoxDefinition.MSG_UNABLE_TO_PERSIST).ToString();
            }
        }

        protected abstract string TransactionCommitErrorActionDescription
        {
            get;
        }

        protected virtual string UpdateConflictMessage
        {
            get
            {
                return new MessageBoxDefinition(MessageBoxDefinition.MSG_STALE_DATA).ToString();
            }
        }

        /// <summary>
        /// Indicates if the workpaper should be saved
        /// Defaults to save, but for performance this
        /// can be overridden to return false if the 
        /// workpaper does not need saving
        /// </summary>
        protected virtual bool ShouldSaveWorkpaper
        {
            get
            {
                return true;
            }
        }

        protected abstract string WorkpaperName
        {
            get;
        }

        protected string BreadcrumbInformation
        {
            get
            {
                DataTable objBreadcrumbTable = this.PostbackDataSet.Tables[WPDatasetBaseDS.BREADCRUMBTABLE];

                if (objBreadcrumbTable != null && objBreadcrumbTable.Rows.Count > 0)
                    return (string)objBreadcrumbTable.Rows[0][WPDatasetBaseDS.BREADCRUMBFIELD];

                return String.Empty;
            }
        }


        protected abstract HeaderType WorkpaperHeaderType
        {
            get;
        }

        public abstract Guid WorkpaperID
        {
            get;
        }

        protected abstract int PrintZoomPercentage
        {
            get;
        }

        protected abstract ActionType PostSaveActionType
        {
            get;
        }

        protected abstract ActionType CancelActionType
        {
            get;
        }

        protected abstract ActionType CloseActionType
        {
            get;
        }

        protected abstract Stylesheet[] WorkpaperSpecificStylesheets
        {
            get;
        }

        protected abstract Javascript[] WorkpaperSpecificJavascript
        {
            get;
        }

        protected virtual INableHeader GetCustomHeaderControl()
        {
            return this.m_objHeader;
        }

        protected virtual MenuEvent[] CustomMenuCommands
        {
            get
            {
                return null;
            }
        }

        private MenuEvent[] InternalGetCustomMenuCommands()
        {
            ArrayList objAllCustomMenuCommands = new ArrayList();
            MenuEvent[] objCustomMenuCommands = this.CustomMenuCommands;

            if (objCustomMenuCommands != null)
                objAllCustomMenuCommands.AddRange(objCustomMenuCommands);

            return (MenuEvent[])objAllCustomMenuCommands.ToArray(typeof(MenuEvent));
        }

        protected abstract string SelectedMenuButton
        {
            get;
        }

        protected virtual string NameOnCreation
        {
            get
            {
                return String.Empty;
            }
        }

        protected virtual bool TransientWorkpaper
        {
            get
            {
                return false;
            }
        }

        protected virtual bool AdministratorRequired
        {
            get
            {
                return false;
            }
        }

        protected virtual bool SystemAdministratorRequired
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Returns the width, in pixels, of the printable area of the workpaper.
        /// </summary>
        protected virtual int PrintablePageWidth
        {
            get { return 1024; }
        }

        /// <summary>
        /// Indicates the orientation of the printed workpaper. Return true for Landscape and false for Portrait
        /// </summary>
        protected virtual bool PrintAsLandscape
        {
            get { return false; }
        }

        protected virtual bool DisplayZeroPreferenceWorkpaper
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Name of TeaTabWPSpecificControlName used on the TEA TAB 
        /// Note, if the name is empty, no control will be added to the page
        /// </summary>
        public virtual string TeaTabWPSpecificControlName
        {
            get
            {
                return String.Empty;
            }
        }
        /// <summary>
        /// Name of TeaTabMovementAnalysisControl used on the TEA TAB 
        /// Note, if the name is empty, no control will be added to the page
        /// </summary>
        public virtual string TeaTabMovementAnalysisControlName
        {
            get
            {
                return "TeaTabMovementAnalysisControl.ascx";
            }
        }
        /// <summary>
        /// Name of TeaTabClosingTempDifferenceControl used on the TEA TAB 
        /// Note, if the name is empty, no control will be added to the page
        /// </summary>
        public virtual string TeaTabClosingTempDifferenceControlName
        {
            get
            {
                return "TeaTabClosingTempDifferenceControl.ascx";
            }
        }
        /// <summary>
        /// Number of TeaTabMovementAnalysisControls used on the TEATAB page
        /// Note, if TeaTabMovementAnalysisControlName is empty, no controls will be displayed
        /// </summary>
        public virtual int TeaTabMovementAnalysisControlsNumber
        {
            get
            {
                return 1;
            }
        }
        #endregion
        #region Other Properties


        protected INableHeader TaxSimpHeader
        {
            get
            {
                return m_objHeader;
            }
        }

        protected virtual bool DisableMenuHeader
        {
            get
            {
                return false;
            }
        }

        private ICMBroker m_objBroker
        {
            get
            {
                LocalDataStoreSlot brokerSlot = Thread.GetNamedDataSlot("Broker");

                if (brokerSlot == null)
                    return null;

                return (ICMBroker)Thread.GetData(brokerSlot);
            }
            set
            {
                LocalDataStoreSlot brokerSlot = Thread.GetNamedDataSlot("Broker");

                if (brokerSlot == null)
                    brokerSlot = Thread.AllocateNamedDataSlot("Broker");

                if (value == null)
                    Thread.FreeNamedDataSlot("Broker");

                Thread.SetData(brokerSlot, value);
            }
        }

        protected ICMBroker Broker
        {
            get
            {
                return this.m_objBroker;
            }
        }

        protected Token BMCUpdateToken
        {
            get
            {
                return (Token)this.ViewState["BMCUpdateToken"];
            }
            set
            {
                this.ViewState["BMCUpdateToken"] = value;
            }
        }

        protected string ReturnURL
        {
            get
            {
                Uri returnURI = (Uri)this.ViewState["ReturnURL"];

                if (returnURI != null)
                    return returnURI.ToString();
                else
                    return String.Empty;
            }
        }

        private TabType ActiveTabType
        {
            get
            {
                HtmlInputHidden objHiddenField = (HtmlInputHidden)this.GetControl(this.Form.Controls, ActiveTabHiddenFieldName);

                string strActiveTabType = this.Request["PrintTab"] != null ? this.Request["PrintTab"] : objHiddenField.Value;

                return (TabType)Enum.Parse(typeof(TabType), strActiveTabType, true);
            }
            set
            {
                HtmlInputHidden objHiddenField = (HtmlInputHidden)this.GetControl(this.Form.Controls, ActiveTabHiddenFieldName);
                objHiddenField.Value = value.ToString();
            }
        }

        protected Tab ActiveTab
        {
            get
            {
                return this.m_objActiveTab;
            }
        }

        private new HtmlForm Form
        {
            get
            {
                return this.m_objForm;
            }
        }

        public Panel WorkpaperControls
        {
            get
            {
                return this.m_objWorkpaperControls;
            }
            set
            {
                this.m_objWorkpaperControls = value;
            }
        }

        public DataSet PostbackDataSet
        {
            get
            {
                return this.m_objPostBackDataset;
            }
        }

        public ActionType AfterPostbackActionType
        {
            get
            {
                return this.m_objAfterPostBackAction;
            }
            set
            {
                this.m_objAfterPostBackAction = value;
            }
        }

        public bool PageRequiresRebind
        {
            get
            {
                return this.m_bolPageRequiresRebind;
            }
            set
            {
                this.m_bolPageRequiresRebind = value;
            }
        }
        protected bool HasNotes
        {
            get
            {
                return this.m_wpHasNotes;
            }
            set
            {
                this.m_wpHasNotes = value;
            }
        }
        protected bool HasAttachments
        {
            get
            {
                return this.m_wpHasAttachments;
            }
            set
            {
                this.m_wpHasAttachments = value;
            }
        }
        public bool AllowUserToCancelOperation
        {
            get
            {
                HiddenField requestCancellationHiddenField = (HiddenField)this.Form.FindControl(RequestCanBeCancelledHiddenFieldName);
                bool requestCanBeCancelled = false;

                if (!bool.TryParse(requestCancellationHiddenField.Value, out requestCanBeCancelled))
                    requestCanBeCancelled = false;

                return requestCanBeCancelled;
            }
            set
            {
                HiddenField requestCancellationHiddenField = (HiddenField)this.Form.FindControl(RequestCanBeCancelledHiddenFieldName);
                requestCancellationHiddenField.Value = value.ToString();
            }
        }

        public virtual void StatusChanged(object sender, EventArgs e)
        {
            this.m_objBMC.CMStatus = ((CMStatusControl)sender).CMStatus;
        }
        #endregion
        #endregion
        #region Page generation code
        protected override void OnInit(System.EventArgs e)
        {
            this.BuildPage();
            base.OnInit(e);
        }


        public virtual void GetBMCDataDuringInit()
        {
            if (this.m_objBroker == null)
                this.InitialiseBroker();

            if (this.m_objInstanceID == Guid.Empty)
                this.InitialiseIDs(Guid.Empty);

            if (this.m_objBMC == null)
                this.InitialiseObjects();
        }


        protected void BuildPage()
        {
            this.MoveWorkpaperControls();

            this.Controls.Add(new LiteralControl("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\" >\n<html>\n\t<head>\n\t\t<title>"));

            LiteralControl objTitleControl = new LiteralControl(this.WorkpaperName);
            objTitleControl.ID = "Title";
            this.Controls.Add(objTitleControl);

           // this.Controls.Add(new LiteralControl(" - TaxSimplifier Compliance Solutions" + "</title>\n"));
            this.Controls.Add(new LiteralControl("E-Clipse Online" + "</title>\n"));

            this.Controls.Add(new Stylesheet(StylesheetMedia.Screen, StylesheetType.IncludeFile, "styles/nable_1_1.css"));
            this.Controls.Add(new Stylesheet(StylesheetMedia.Print, StylesheetType.IncludeFile, "styles/nableprint_1_1.css"));
            this.Controls.Add(new Stylesheet(StylesheetMedia.Screen, StylesheetType.IncludeFile, "styles/orgui.css"));
            this.Controls.Add(new Javascript(JavaScriptType.IncludeFile, "scripts/nable_1_1.js"));
            //this.Controls.Add(new Javascript(JavaScriptType.IncludeFile, "scripts/jquery-1.3.2.min.js"));
            // this.Controls.Add(new Javascript(JavaScriptType.IncludeFile, "scripts/orguiscript.js"));
            this.Controls.Add(new Javascript(JavaScriptType.Inline, "document.onreadystatechange=HandleReportUpdateRemoval;"));

            Javascript[] objWorkpaperSpecificJavascript = this.WorkpaperSpecificJavascript;
            if (objWorkpaperSpecificJavascript != null)
            {
                foreach (Javascript objJavascript in objWorkpaperSpecificJavascript)
                    this.Controls.Add(objJavascript);
            }

            Stylesheet[] objWorkpaperSpecificStylesheets = this.WorkpaperSpecificStylesheets;
            if (objWorkpaperSpecificStylesheets != null)
            {
                foreach (Stylesheet objStylesheet in objWorkpaperSpecificStylesheets)
                    this.Controls.Add(objStylesheet);
            }

            if (this.PrintZoomPercentage != 100)
            {
                Stylesheet objZoomStyle = new Stylesheet(StylesheetMedia.Print, StylesheetType.Inline, String.Empty);
                objZoomStyle.StylesheetInline = "body { zoom: " + this.PrintZoomPercentage + "%; }";
                this.Controls.Add(objZoomStyle);
            }

            // insert markup to indicate the printing properties to the Workpaper Printing Collector. This is only required if we
            // are printing workpapers via the multiple workpaper printing mechanism.
            if (this.IsPrintRequest)
                this.Controls.Add(new LiteralControl("<!--TaxSimplifier Compliance Solutions-PRINTING WIDTH=\"" + this.PrintablePageWidth.ToString() + "\" LANDSCAPE=\"" + this.PrintAsLandscape.ToString() + "\" -->"));

            this.Controls.Add(new LiteralControl("\t</head>\n" + "\t<body>\n"));

            this.Page.ClientScript.RegisterStartupScript(this.GetType(), "TextboxFocus", "<script language=\"javascript\">SetInputElementFocus();</script>");

            HtmlForm objForm = this.CreateForm();
            this.Controls.Add(objForm);

            HiddenField requestCanBeCancelledField = new HiddenField();
            requestCanBeCancelledField.ID = RequestCanBeCancelledHiddenFieldName;

            this.Form.Controls.Add(requestCanBeCancelledField);

            this.AllowUserToCancelOperation = false;

            // Must Add hidden Control before end of Body
            this.Controls.Add(new LiteralControl("\t</body>\n" + "</html>"));

            if (this.m_objHeader != null)
                this.m_objHeader.HeaderText = this.WorkpaperName;

            this.Controls.Add(this.messageBox);
            this.messageBox.ButtonPressed += new ButtonPressedEventHandler(this.HandleMessageBoxButtonClick);

        }

        private HtmlForm CreateForm()
        {
            HtmlForm objForm = new HtmlForm();
            this.m_objForm = objForm;

            this.CreateActiveTabTypeField();
            this.CreateCurrentURLTypeField();

            HeaderType objWorkpaperHeaderType = this.WorkpaperHeaderType;

            // if the client workpaper is using a custom header, allow them to specify it
            if (objWorkpaperHeaderType != HeaderType.CustomHeader && objWorkpaperHeaderType != HeaderType.None)
            {
                string strHeaderType = HeaderTypeTranslator.TranslateHeaderType(objWorkpaperHeaderType);
                this.m_objHeader = (INableHeader)this.LoadControl(strHeaderType);
            }
            else if (objWorkpaperHeaderType == HeaderType.CustomHeader)
                this.m_objHeader = this.GetCustomHeaderControl();
            else
                this.m_objHeader = null;

            if (this.m_objHeader != null)
            {
                //Changes made to set display of any menu to false --- by babar bhai.
                this.m_objHeader.SetDisplayMenu = this.SetDisplayMenu;

                if(this.Request.Url.Query.Contains("NoReportingHeaderAndMenu"))
                {
                    
                }
                else if (this.DisableMenuHeader)
                {

                }
                else if (this.Request.Url.Query.Contains("NoReportingHeader"))
                    this.AddCustomMenuButtons();
                else
                {
                    this.SetHeaderMenu();
                    objForm.Controls.Add((Control)this.m_objHeader);
                    this.AddCustomMenuButtons();
                }
            }

            this.m_objActiveTab = this.CreateActiveTab();
            objForm.Controls.Add(this.m_objActiveTab);

            this.m_objFooter = new NableFooter();
            objForm.Controls.Add(this.m_objFooter);
            return objForm;
        }

        private void PopulateComponentVersion(INableHeader objHeader)
        {
            Assembly objAssembly = Assembly.GetAssembly(this.m_objBMC.GetType());
            AssemblyInformationalVersionAttribute[] objVersionAttributes = (AssemblyInformationalVersionAttribute[])objAssembly.GetCustomAttributes(typeof(AssemblyInformationalVersionAttribute), true);
            AssemblyInformationalVersionAttribute objVersion = objVersionAttributes[0];

            objHeader.ComponentVersion = objVersion.InformationalVersion;
        }

        private Tab CreateActiveTab()
        {
            Tab objTab = TabFactory.CreateTab(this.ActiveTabType, this);
            return objTab;
        }

        protected virtual NableMenuButton[] CustomMenuButtons
        {
            get
            {
                return null;
            }
        }

        public virtual bool SetDisplayMenu
        {
            get { return true; }
        }

        protected virtual void AddCustomMenuButtons()
        {
            NableMenuButton[] objCustomButtons = this.CustomMenuButtons;

            if (objCustomButtons != null)
            {
                foreach (NableMenuButton objCustomButton in objCustomButtons)
                    this.m_objHeader.CustomMenuButtons.Add(objCustomButton);
            }
        }

        private void InsertBeforeDisplayHideZeros(NableMenuButton objCustomButton)
        {
            this.m_objHeader.CustomMenuButtons.Insert(this.m_objHeader.CustomMenuButtons.Count - 1, objCustomButton);
        }

        protected void MoveWorkpaperControls()
        {
            Panel objPanel = new Panel();
            objPanel.Visible = false;

            IEnumerator objControlEn = this.Controls.GetEnumerator();

            while (objControlEn.MoveNext())
            {
                Control objControl = (Control)objControlEn.Current;

                if (!(objControl is INableHeader) &&
                    !(objControl is HtmlForm) &&
                    !(objControl is Javascript) &&
                    !(objControl is Stylesheet))
                {
                    this.Controls.Remove(objControl);
                    objPanel.Controls.Add(objControl);

                    if (objControl is IValidator)
                        Page.Validators.Add(((IValidator)objControl));

                    objControlEn = this.Controls.GetEnumerator();
                }
            }

            this.WorkpaperControls = objPanel;
        }

        private Control GetControl(ControlCollection objControls, string strID)
        {
            foreach (Control objControl in objControls)
            {
                if (objControl.ID == strID)
                    return objControl;
            }
            return null;
        }

        private void CreateActiveTabTypeField()
        {
            HtmlInputHidden objHiddenField = new HtmlInputHidden();
            objHiddenField.ID = ActiveTabHiddenFieldName;
            objHiddenField.Value = this.Request.Form[ActiveTabHiddenFieldName];

            if (objHiddenField.Value == String.Empty)
                objHiddenField.Value = TabType.Workpaper.ToString();

            this.Form.Controls.Add(objHiddenField);
        }
        private void CreateCurrentURLTypeField()
        {
            // this will be used for Workpaper Printing, in replace of the Print Button

            string strFile = this.Request.Url.OriginalString.ToString();
            string CurrentASPX = strFile.Substring(strFile.LastIndexOf(@"/") + 1) + "&PrintRequest=true";

            AddHiddenHTMLField(CurrentPageURLHiddenFieldName, CurrentASPX.Replace("&", "--").ToString());
            AddHiddenHTMLField(CurrentWorkpaperNameHiddenFieldName, this.WorkpaperName.ToString());
            AddHiddenHTMLField(WorkPaperPrintCollectorPage, "Test Key value");

        }

        private void AddHiddenHTMLField(string ID, string Value)
        {
            HtmlInputHidden objHiddenField = new HtmlInputHidden();
            objHiddenField.ID = ID.ToString();
            objHiddenField.Value = Value.ToString();

            this.Form.Controls.Add(objHiddenField);
        }
        #endregion
        #region Events
        /// <summary>
        /// Fires when a custom message box button is pressed
        /// </summary>
        public event ButtonPressedEventHandler MessageBoxButtonClicked;

        #endregion //Events
        #region Protected And Internal Properties

        public ScriptManagerWrapper ScriptManagerWrapper
        {
            get
            {
                if (this.scriptManagerWrapper == null)
                {
                    Type page = this.GetType();
                    FieldInfo scriptManagerFieldInfo = page.GetField("ScriptManager1", BindingFlags.NonPublic | BindingFlags.Instance);
                    ScriptManager scriptManager = (ScriptManager)scriptManagerFieldInfo.GetValue(this);
                    //ScriptManager scriptManager = (ScriptManager)this.FindControl("ScriptManager1");
                    this.scriptManagerWrapper = new ScriptManagerWrapper(scriptManager);
                }
                return this.scriptManagerWrapper;
            }
        }

        #endregion
    }
}
