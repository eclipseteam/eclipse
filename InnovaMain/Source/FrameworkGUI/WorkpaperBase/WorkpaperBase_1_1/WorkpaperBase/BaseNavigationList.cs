using System;
using System.Collections;

namespace Oritax.TaxSimp.Calculation
{
	/// <summary>
	/// Summary description for WorkPaperNavigationList.
	/// </summary>
	/// 
	#region NavigationItem
	public class NavigationItem 
	{
		private string m_strKey;
		private string m_strDescription;
		public string Key
		{
			get{ return m_strKey; }
			set { this.m_strKey = value; }
		}

		public string Description
		{
			get{ return m_strDescription; }
			set { this.m_strDescription = value; }
		}

		#region Backward compatibility 
		// we only need PreviousItem and NextItem so we do not have to change the code in prior year ATO forms
		private NavigationItem m_objPreviousItem;
		private NavigationItem m_objNextItem;

		public NavigationItem PreviousItem
		{
			get{ return m_objPreviousItem; }
			set{ this.m_objPreviousItem  = value; }
		}
		public NavigationItem NextItem
		{
			get{ return m_objNextItem; }
			set{ this.m_objNextItem  = value; }
		}
		public NavigationItem( string strKey, string strDescription, NavigationItem objPreviousItem )
		{
			this.m_strKey			= strKey;
			this.m_strDescription	= strDescription;
			this.m_objPreviousItem	= objPreviousItem;
		}

		#endregion

		public NavigationItem( string strKey, string strDescription )
		{
			this.m_strKey			= strKey;
			this.m_strDescription	= strDescription;
		}
		public override bool Equals(Object otherObject)
		{
			bool result = false;
			if (otherObject.GetType() == typeof(NavigationItem))
			{
				NavigationItem otherItem = (NavigationItem)otherObject;
				if ((m_strDescription == otherItem.Description)
						&& (m_strKey == otherItem.Key))
					result = true;
			}
			return result;
		}
		public override int GetHashCode()
		{
			return (m_strKey.GetHashCode() ^ m_strDescription.GetHashCode());
		}
	}
	#endregion
	#region BaseNavigationList
	public class BaseNavigationList: IEnumerable
	{
		private ArrayList objNavigationList = null;
        
		public BaseNavigationList()
		{
			objNavigationList = new ArrayList();
		}

		public void Add( NavigationItem objNavigationItem )
		{
			if (objNavigationItem == null)
			{
				throw new ArgumentNullException("NavigationItem", "NavigationItem can not be null");
			}

			objNavigationList.Add(objNavigationItem);
		}
		/// <summary>
		/// This exists for backward compatibility
		/// </summary>
		/// <param name="objNavigationItem"></param>
		/// <param name="strPreviousItem"></param>
		public void Add( NavigationItem objNavigationItem, string strPreviousItem )
		{
			if (objNavigationItem != null)
				objNavigationList.Add( objNavigationItem );
			else
				throw new ArgumentNullException("NavigationItem", "NavigationItem can not be null");
			
			objNavigationItem.PreviousItem = this[ strPreviousItem ];
		}

		public NavigationItem this [ string strKey ]
		{
			get
			{
				return( (NavigationItem) this.Find( strKey ) );
			}
		}

		public NavigationItem Find( string strKey )
		{
			NavigationItem objReturnValue = null;

			foreach( NavigationItem objNavigationItem in this.objNavigationList )
			{
				if( objNavigationItem.Key == strKey )
				{
					objReturnValue = objNavigationItem;
					break;
				}
			}
			return( objReturnValue );
		}
	
		public int FindIndex(string strKey)
		{
			int result = -1;
			for (int i = 0; i < objNavigationList.Count; i++ )
			{
				if (((NavigationItem)objNavigationList[i]).Key == strKey)
				{
					result = i;
					break;
				}
			}
			return (result);
		}

		public NavigationItem GetPreviousWorkpaper( string strCurrentWorkpaper )
		{
			//NavigationItem objNavigationItem = this.Find( strCurrentWorkpaper );
			//if( objNavigationItem != null )
			//    return( objNavigationItem.PreviousItem );
			//else
			//    return( null );	

			NavigationItem objNavigationItem = null;
			if (Count() > 0)
			{
				int index = FindIndex(strCurrentWorkpaper);

				if ((index > 0) && (index < objNavigationList.Count))
					objNavigationItem = (NavigationItem)objNavigationList[index - 1];
				else
					objNavigationItem = (NavigationItem)objNavigationList[objNavigationList.Count - 1];
			}
			return objNavigationItem;
		}

		public NavigationItem GetNextWorkpaper( string strCurrentWorkpaper )
		{
			//NavigationItem objNavigationItem = this.Find( strCurrentWorkpaper );
			//if( objNavigationItem != null )
			//    return( objNavigationItem.NextItem );
			//else
			//    return( null );

			NavigationItem objNavigationItem = null;
			if (Count() > 0)
			{
				int index = FindIndex(strCurrentWorkpaper);
				if ((index >= 0) && (index < objNavigationList.Count - 1))
					objNavigationItem = (NavigationItem)objNavigationList[index + 1];
				else
					objNavigationItem = (NavigationItem)objNavigationList[0];
			}
			return objNavigationItem;
		}

		public IEnumerator GetEnumerator()
		{
			IEnumerator result = null;
			if (objNavigationList != null)
				result = objNavigationList.GetEnumerator();
			return result;
		}
		public ArrayList GetItems()
		{
			return objNavigationList;
		}
		public int Count()
		{
			int result = -1;
			if (objNavigationList != null)
				result = objNavigationList.Count;
			return result;
		}
	}
	#endregion
}
