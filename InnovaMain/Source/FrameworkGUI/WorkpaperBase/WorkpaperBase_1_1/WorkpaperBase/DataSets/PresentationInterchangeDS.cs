using System;
using System.Data;

namespace Oritax.TaxSimp.Calculation
{
	/// <summary>
	/// DSCalculationModule is a DataSet that contains all of the persistent data for a CM instance.
	/// </summary>
	[Serializable] 
	public class PresentationInterchangeDS : DataSet
	{
		private const String OPERATIONS_TABLE		= "OPERATIONS";
		private const String OPERATION_FIELD		= "OPERATION";

		public const int NONE_OPERATION				= 0;

		public int Operation
		{
			get
			{
				if(1==this.Tables[OPERATIONS_TABLE].Rows.Count)
					return (int)this.Tables[OPERATIONS_TABLE].Rows[0][OPERATION_FIELD];
				else
					return 0;
			}
			set
			{
				if(1==this.Tables[OPERATIONS_TABLE].Rows.Count)
					this.Tables[OPERATIONS_TABLE].Rows[0][OPERATION_FIELD]=value;
				else
				{
					DataRow newRow=this.Tables[OPERATIONS_TABLE].NewRow();
					newRow[OPERATION_FIELD]=value;
					this.Tables[OPERATIONS_TABLE].Rows.Add(newRow);
				}
			}
		}

		public PresentationInterchangeDS()
		{
			DataTable table = new DataTable(OPERATIONS_TABLE);
			table.Columns.Add(OPERATION_FIELD, typeof(System.Enum));
			this.Tables.Add(table);

			DataRow newRow=this.Tables[OPERATIONS_TABLE].NewRow();
			newRow[OPERATION_FIELD]=NONE_OPERATION;
			this.Tables[OPERATIONS_TABLE].Rows.Add(newRow);
		}
	}
}
