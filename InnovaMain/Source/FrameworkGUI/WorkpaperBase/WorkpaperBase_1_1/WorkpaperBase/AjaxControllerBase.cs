﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Threading;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.Services.CMBroker;
using System.Web;
using System.Data;
using Newtonsoft.Json;

namespace Oritax.TaxSimp.Calculation
{
    public abstract class AjaxControllerBase : Page
    {
        #region Fields

        protected IBrokerManagedComponent brokerManagedComponent = null;
        protected string clidString;
        protected string csidString;

        #endregion // Fields

        #region Protected Properties

        protected ICMBroker CmBroker
        {
            get
            {
                LocalDataStoreSlot brokerSlot = Thread.GetNamedDataSlot("Broker");

                if (brokerSlot == null)
                    return null;

                return (ICMBroker)Thread.GetData(brokerSlot);
            }
            set
            {
                LocalDataStoreSlot brokerSlot = Thread.GetNamedDataSlot("Broker");

                if (brokerSlot == null)
                    brokerSlot = Thread.AllocateNamedDataSlot("Broker");

                if (value == null)
                    Thread.FreeNamedDataSlot("Broker");

                Thread.SetData(brokerSlot, value);
            }
        }

        #endregion // Protected Properties

        #region Public Methods

        public abstract DataSet CreateDataSet();

        #endregion // Public Methods

        #region Protected Methods

        protected void InitialiseBrokerManagedComponent()
        {
            this.clidString = Request.Params["CLID"];
            this.csidString = Request.Params["CSID"];

            Guid logicalModuleId;
            Guid scenarioId;

            if (this.clidString != null && this.clidString != String.Empty)
                logicalModuleId = new Guid(this.clidString);
            else
                throw new Exception("CalculationModuleWorkpaper.InitialiseIDs, CLID not specified in query string");

            if (this.csidString != null && this.csidString != String.Empty)
                scenarioId = new Guid(this.csidString);
            else
                throw new Exception("CalculationModuleWorkpaper.InitialiseIDs, CSID not specified in query string");

            Guid objCalculationModuleInstanceID = this.CmBroker.GetCMInstanceID(logicalModuleId, scenarioId);

            this.brokerManagedComponent = this.CmBroker.GetBMCInstance(objCalculationModuleInstanceID);

        }

        protected void InitialiseBroker()
        {
            this.CmBroker = new CMBroker(Context.User, DBConnection.Connection.ConnectionString);

            RequestProcessStatusProvider statusProvider = (RequestProcessStatusProvider)HttpContext.Current.Items["NABLEStatusProvider"];

            if (statusProvider != null)
                this.CmBroker.SetRequestStatusProvider(statusProvider);

            this.CmBroker.SetStart();
        }


        protected void InitialiseBrokerWriterStart()
        {
            this.CmBroker = new CMBroker(Context.User, DBConnection.Connection.ConnectionString);

            RequestProcessStatusProvider statusProvider = (RequestProcessStatusProvider)HttpContext.Current.Items["NABLEStatusProvider"];

            if (statusProvider != null)
                this.CmBroker.SetRequestStatusProvider(statusProvider);

            this.CmBroker.SetWriteStart();
        }

        protected void Complete()
        {
            try
            {
                if (this.CmBroker.InTransaction)
                {
                    this.CmBroker.SetComplete();
                }
            }
            catch
            {
                this.CmBroker.SetAbort();
                throw;
            }
        }

        protected void CleanupBroker()
        {
            if (this.CmBroker != null)
            {
                if (this.CmBroker.InTransaction)
                {
                    this.CmBroker.SetAbort();
                    //Debug.Assert(true, "AjaxControllerBase.CleanupBroker(), transaction on the broker was not ended correctly");
                }
                this.CmBroker.Dispose();
            }
        }


        #endregion // Protected Methods
    }
}
