using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Collections;

using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Exceptions;
using Oritax.TaxSimp.Workpaper;
using Oritax.TaxSimp.Utilities;
using PresentationControls;

namespace Oritax.TaxSimp.Calculation
{
	/// <summary>
	/// A workpaper for Calculation Modules
	/// </summary>
	public abstract class CalculationModuleWorkpaper : BrokerManagedComponentWorkpaper
	{
		#region Constants
		private const string WORKPAPERMODIFIEDHIDDENFIELDNAME = "hdnWorkpaperModified";
		#endregion Constants
		#region Fields

		Guid m_objLogicalModuleID;
		Guid m_objScenarioID;

		#endregion // Fields

		#region Constructors

		public CalculationModuleWorkpaper()
		{
			base.ClientScript.RegisterHiddenField(WORKPAPERMODIFIEDHIDDENFIELDNAME, bool.FalseString);
		}

		#endregion

		#region Methods

		protected override void InitialiseIDs( Guid objCID )
		{
			string strLogicalModuleID = Context.Request.QueryString[ "CLID" ];
			string strScenarioID = Context.Request.QueryString[ "CSID" ];

			if ( strLogicalModuleID != null && strLogicalModuleID != String.Empty )
				this.m_objLogicalModuleID = new Guid( strLogicalModuleID );
			else
				throw new Exception( "CalculationModuleWorkpaper.InitialiseIDs, CLID not specified in query string" );

			if ( strScenarioID != null && strScenarioID != String.Empty )
				this.m_objScenarioID = new Guid( strScenarioID );
			else
				throw new Exception( "CalculationModuleWorkpaper.InitialiseIDs, CSID not specified in query string" );

			Guid objCalculationModuleInstanceID = this.Broker.GetCMInstanceID( this.m_objLogicalModuleID, this.m_objScenarioID );

			if(objCalculationModuleInstanceID == Guid.Empty)
				throw new MissingComponentException("CM for CLID and CSID does not exist");

			base.InitialiseIDs( objCalculationModuleInstanceID );
		}

		public override TabType[] GetTabs()
		{
			ArrayList objTabs = new ArrayList( );

			objTabs.Add( TabType.Workpaper );
			objTabs.Add( TabType.Notes );
			objTabs.Add( TabType.Attachments );
			objTabs.Add( TabType.Targets );

			return (TabType[]) objTabs.ToArray( typeof( TabType ) );
		}

		protected override IBrokerManagedComponent CreateNewBrokerManagedComponent()
		{
			// Calculation modules aren't created in this way
			return null;
		}

		protected override string GetWorkpaperSpecificURLIDs( )
		{
			string wpType = this.Request.QueryString["WPTYPE"];
			string wpTypeArg = String.Empty;

			if(wpType != String.Empty)
				wpTypeArg = "&WPTYPE=" + wpType;

			return "CLID=" + this.m_objLogicalModuleID.ToString( ) + wpTypeArg + "&CSID=" + this.m_objScenarioID.ToString( );
		}

        protected virtual DataTable CreateConfigurationTable(string[] descriptionList)
        {
            const String CONFIGURATION_TABLE = "CONFIGURATION_TABLE";
            const String DESCRIPTION_FIELD = "Description";

            DataTable table = new DataTable(CONFIGURATION_TABLE);
            table.Columns.Add(DESCRIPTION_FIELD, typeof(System.String));

            foreach (string strTemp in descriptionList)
            {
                if (!String.IsNullOrEmpty(strTemp))
                {
                    DataRow dr = table.NewRow();
                    dr[DESCRIPTION_FIELD] = strTemp;

                    table.Rows.Add(dr);
                }
            }

            return table;
        }

		#endregion

		#region Properties

		private new ICMBroker Broker
		{
			get
			{
				return base.Broker;
			}
		}

		protected override string MissingComponentErrorDescription
		{
			get
			{
				return new MessageBoxDefinition( MessageBoxDefinition.MSG_WORKPAPER_DELETED ).ToString( );
			}
		}

		protected override string MissingComponentErrorTitle
		{
			get
			{
				return "Page no longer exists";
			}
		}

		protected override HeaderType WorkpaperHeaderType
		{
			get
			{
				return HeaderType.Header;
			}
		}

		protected override int PrintZoomPercentage
		{
			get
			{
				return 100;
			}
		}

		protected override ActionType PostSaveActionType
		{
			get
			{
				return ActionType.RemainOnWorkpaper;
			}
		}

		protected override ActionType CancelActionType
		{
			get
			{
				return ActionType.CloseScreen;
			}
		}

		protected override ActionType CloseActionType
		{
			get
			{
				return ActionType.CloseScreen;
			}
		}

		protected override Javascript[] WorkpaperSpecificJavascript
		{
			get
			{
				return null;
			}
		}

		protected override Stylesheet[] WorkpaperSpecificStylesheets
		{
			get
			{
				return null;
			}
		}

		protected override string SelectedMenuButton
		{
			get
			{
				return String.Empty;
			}
		}

		protected override bool ShouldSaveWorkpaper
		{
			get
			{
				if(this.IsSpecialCommandThatBypassesSaveButton())
					return true;
				else
				{
					string saveWorkpaperHiddenValue = this.Request.Form[WORKPAPERMODIFIEDHIDDENFIELDNAME];
					return (saveWorkpaperHiddenValue == bool.TrueString);
				}
			}
		}

        protected virtual bool DisplayZeroPreference
        {
            get
            {
                if (ViewState[WorkpaperBaseConstants.displayZeroPreferenceKey] == null)
                {
                    ViewState[WorkpaperBaseConstants.displayZeroPreferenceKey] = 
                        CurrentUser.DisplayZeroPreference;
                }
                return (bool)ViewState[WorkpaperBaseConstants.displayZeroPreferenceKey];
            }

            set
            {
                ViewState[WorkpaperBaseConstants.displayZeroPreferenceKey] = value;
            }
        }

        protected ToggleMenuButton DisplayZeroPreferenceButton
        {
            get
            {
                return NableHeader.DisplayZeroPreferenceButton;
            }
        }

        protected override MenuEvent[] CustomMenuCommands
        {
            get
            {
                return new MenuEvent[]{ 
										  new MenuEvent( "DisplayZeroPreference", new CommandEventHandler( this.ProcessDisplayZeroPreference ) ), 
				};
            }
        }

		
		#endregion

		#region Standard strings
		/// <summary>
		/// Error Message shown when another user updates the workpaper and the current displayed data is stale
		/// </summary>
		public const String Msg_invalid_description = "Msg_invalid_description";
		public const String Msg_invalid_reference = "Msg_invalid_reference";
        public const String Msg_invalid_date_format = "Msg_invalid_date_format";
		public const String Msg_exceeded_amount = "Msg_exceeded_amount";
		public const String Msg_invalid_amount = "Msg_exceeded_amount";
		public const String RegExp_DescriptionExpression = "RegExp_DescriptionExpression";
		public const String RegExp_ReferenceExpression = "RegExp_ReferenceExpression";
        public const String RegExp_DateExpression = "RegExp_DateExpression";

		public String getSystemParameter(String sSystemParameter)
		{
            if (sSystemParameter == Msg_invalid_description)
                return new MessageBoxDefinition(MessageBoxDefinition.MSG_INVALID_DESCRIPTION).ToString();
            else if (sSystemParameter == Msg_invalid_reference)
                return new MessageBoxDefinition(MessageBoxDefinition.MSG_INVALID_REFERENCE).ToString();
            else if (sSystemParameter == Msg_invalid_date_format)
                return new MessageBoxDefinition(MessageBoxDefinition.MSG_INVALID_DATE_FORMAT).ToString();
            else if ((sSystemParameter == RegExp_DescriptionExpression) || (sSystemParameter == RegExp_ReferenceExpression))
                return ".*";
            else if (sSystemParameter == RegExp_DateExpression)
                return @"(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d";
            else
                return String.Empty;
		        }
		#endregion

		#region Public Methods

		/// <summary>
		/// Commands that need to save the workpaper even if the user
		/// has not updated anythign on the workpaper.
		/// </summary>
		/// <returns>True if the command requires a workpaper save, false otherwise</returns>
		private bool IsSpecialCommandThatBypassesSaveButton()
		{
			string requestControlName = this.Request["__EVENTTARGET"];

			if(requestControlName != null && requestControlName != String.Empty)
			{
				LinkButton requestControl = this.Page.FindControl(requestControlName) as LinkButton;

				if(requestControl != null)
				{
					string commandName = requestControl.CommandName;

					return (commandName == "Sort" ||
							commandName == "SortLoss" ||
							commandName == "SortGain" ||
							commandName == "Add" ||
							commandName == "AddLines" ||
							commandName == "Insert" ||
							commandName == "Delete" ||
							commandName == "AddLinesLoss" ||
							commandName == "InsertLoss" ||
							commandName == "DeleteLoss" ||
							commandName == "AddLinesGain" ||
							commandName == "InsertGain" ||
							commandName == "DeleteGain");
				}
				else
					return false;
			}
			else
				return this.IsButtonCommand(requestControlName);
		}

		/// <summary>
		/// Checks if the command is received from a button this is required because 
		/// a button does not return an __EVENTTARGET
		/// </summary>
		/// <param name="requestControlName">The requested __EVENTTARGET</param>
		/// <returns>True if the command is a button command</returns>
		private bool IsButtonCommand(string requestControlName)
		{
			if(requestControlName == null || requestControlName == String.Empty)
			{
				foreach(string formKey in this.Request.Form.Keys)
				{
					string formValue = this.Request.Form[formKey];

					// buttons have the value of the text if they
					// have been pressed, check this.
					if(formValue != String.Empty)
					{
						// get the control
						Button control = this.FindControl(formKey) as Button;

						// return true if we found it and it is a button control
						// and the text of the control is in the form value
						if (control != null &&
							control.Text == formValue)
						{
							return true;
						}
					}
				}
			}

			// if we didn't find anything
			return false;
		}

		/// <summary>
		/// Change the Currency Cell to ReadOnly
		/// </summary>
		/// <param name="bReadOnly"></param>
		/// <param name="oCurrencyCell"></param>
		public virtual void ChangeCurrencyCellToBlackedOut(bool bReadOnly, TableCell oCurrencyCell)
		{
			if( bReadOnly )
			{
				TextBox tb = (TextBox) oCurrencyCell.Controls[1];
				oCurrencyCell.CssClass = "WPCurrencyBlack";
				tb.CssClass = "WPCurrencyBlack";
				tb.ReadOnly = true;
			}
		}

		/// <summary>
		/// Chagne the Currency Cell to ReadOnly
		/// </summary>
		/// <param name="bReadOnly"></param>
		/// <param name="oCurrencyCell"></param>
		public virtual void ChangeCurrencyCellToReadOnly(bool bReadOnly, TableCell oCurrencyCell)
		{
			if( bReadOnly )
			{
				TextBox tb = (TextBox) oCurrencyCell.Controls[1];
				oCurrencyCell.CssClass = "WPCurrencyReadOnly";
				tb.CssClass = "WPCurrencyTextBoxReadOnly";
				tb.ReadOnly = true;
			
				
			}
		}

        public virtual void ChangeCurrencyCellToEditable(TableCell oCurrencyCell)
        {
            if (oCurrencyCell.Controls[1] == null)
                return;

            if (!(oCurrencyCell.Controls[1] is TextBox))
                return;

            TextBox tb = (TextBox)oCurrencyCell.Controls[1];
            oCurrencyCell.CssClass = "WPCurrencyItemStyle";
            tb.CssClass = "WPCurrencyLabel";
            tb.ReadOnly = false;
        }

		/// <summary>
		/// Change the Delete Button in the Grid To ReadOnly
		/// </summary>
		/// <param name="bReadOnly"></param>
		/// <param name="oDeleteCell"></param>
		public virtual void ChangeDeleteButtonToReadOnly(bool bReadOnly, TableCell oDeleteCell)
		{
			if( bReadOnly )
			{
				LinkButton lbDelete = (LinkButton) oDeleteCell.Controls[ 1 ];
				lbDelete.Enabled = false;
			}
		}

		/// <summary>
		/// SJD Disable the Data Grid so nothing is editable
		/// </summary>
		/// <param name="oDG"></param>
		public void DisableDataGrid(DataGrid oDG)
		{
			for (int i = 0; i < oDG.Items.Count; i++)
			{
				for (int a = 0; a < oDG.Items[i].Cells.Count; a++)
				{
					DisableChildControls(oDG.Items[i].Cells[a]); 
				}
			}
		}

		public void DisableTextBox(TextBox oTB)
		{
			oTB.Enabled = false;
		}

		public void DisableLinkButton(LinkButton oLB)
		{
			oLB.Enabled = false;
		}

		public void DisableButton(Button oButton)
		{
			if (oButton.CommandName != "NoDisable")
				oButton.Enabled = false;	
		}

		/// <summary>
		/// SJD Recursive call used to disable all controls on a page
		/// </summary>
		/// <param name="oParentControl"></param>
		public void DisableChildControls(Control oParentControl)
		{
			foreach (Control oChildControl in oParentControl.Controls)
			{
				if (oChildControl is DataGrid)
					DisableDataGrid(oChildControl as DataGrid);
				else if (oChildControl is TextBox)
					DisableTextBox(oChildControl as TextBox);
				else if (oChildControl is Button)	
					DisableButton(oChildControl as Button);
				else if (oChildControl is LinkButton)
					DisableLinkButton(oChildControl as LinkButton);

				if (oParentControl.HasControls() == true)
					DisableChildControls(oChildControl);
			}
		}

        #endregion

        #region Protected and Internal Methods

        internal override void PopulateWorkpaperMetaData(DataSet data)
		{
			base.PopulateWorkpaperMetaData(data);
			
			ICalculationModule calculationModule = this.m_objBMC as ICalculationModule;

            UpdateDisplayZeroPreferenceButton();

			if(calculationModule != null && calculationModule.IsScenarioLocked())
			{
				MessageBox message = new MessageBox();
				this.Page.Controls.Add(message);
				message.Title = "Error";
				message.Heading = "Error";
				message.Body = new MessageBoxDefinition(MessageBoxDefinition.MSG_ATTEMPT_TO_WRITE_TO_LOCKED_SCENARIO).ToString();
				message.Prompt = PromptType.Error;

				message.AddMessageBoxButton(new MessageBoxButton(MessageBoxButtonType.OK, false));

				message.ShowOnPageLoad = false;
				message.MakeAvailableToClientScript = true;

				this.ClientScript.RegisterStartupScript(this.GetType(), "LockWorkpaper", "<script language=\"javascript\">IsScenarioLocked = true;</script>");
			}
		}

        protected virtual void ProcessDisplayZeroPreference(object sender, CommandEventArgs events)
        {
            DisplayZeroPreference = !DisplayZeroPreference;
            CurrentUser.DisplayZeroPreference = DisplayZeroPreference;
            UpdateDisplayZeroPreferenceButton();
            PageRequiresRebind = true;
        }

        protected void UpdateDisplayZeroPreferenceButton()
        {
            if (DisplayZeroPreferenceWorkpaper)
            {
                ToggleMenuButton displayZeroPreferenceButton = DisplayZeroPreferenceButton;
                displayZeroPreferenceButton.ButtonToggled = DisplayZeroPreference;
                displayZeroPreferenceButton.Visible = true;
            }
        }

        protected override void AddCustomMenuButtons()
        {
            NableMenuButton[] objCustomButtons = this.CustomMenuButtons;

            if (objCustomButtons != null)
            {
                foreach (NableMenuButton objCustomButton in objCustomButtons)
                    if (this.DisplayZeroPreferenceWorkpaper)
                        InsertBeforeDisplayHideZeros(objCustomButton);
                    else
                        this.m_objHeader.CustomMenuButtons.Add(objCustomButton);
            }
        }

		#endregion

        #region Private Methds

        private void InsertBeforeDisplayHideZeros(NableMenuButton objCustomButton)
        {
            this.m_objHeader.CustomMenuButtons.Insert(this.m_objHeader.CustomMenuButtons.Count - 1, objCustomButton);
        }

        #endregion // Private Methods
    }
}
