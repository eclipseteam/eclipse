using System;
using System.Data;
using System.Web.UI.WebControls;

using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.CM.Organization;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.CM.OrganizationUnit;
using Oritax.TaxSimp.Exceptions;
using Oritax.TaxSimp.BusinessStructureUtilities;

namespace Oritax.TaxSimp.Calculation
{
    /// <summary>
    /// A workpaper for business structure elements
    /// </summary>
    public abstract class BusinessStructureWorkpaper : BrokerManagedComponentWorkpaper//, IBusinessStructureWorkpaper
    {
        public enum BreadCrumbType
        {
            FullBreadCrumbWhenInstanceExists, //If there is no instance associated with the page then do not display a breadcrumb
            NoPeriodBreadCrumbWhenInstanceExists,
            FullBreadCrumb,
            NoPeriod,
            NoPeriodNoScenario,
        }
        #region Member variables
        Guid m_objLogicalModuleID;
        Guid m_objScenarioID;
        Guid m_objParentLogicalModuleID;
        ILogicalModule m_objParentLogicalModule;
        #endregion
        #region Constructors
        public BusinessStructureWorkpaper()
        {
        }
        #endregion
        #region Properties
        private new ICMBroker Broker
        {
            get
            {
                return base.Broker;
            }
        }

        protected override string MissingComponentErrorDescription
        {
            get
            {
                return "This page relates to a business structure item (eg reporting unit or period) that has been removed from the system (either by another user or you) after you originally opened the page. Any modifications previously made by you have been discarded. Please click on the Close button to continue.";
            }
        }

        protected override string MissingComponentErrorTitle
        {
            get
            {
                return "Page no longer exists";
            }
        }

        protected override HeaderType WorkpaperHeaderType
        {
            get
            {
                return HeaderType.Menu;
            }
        }

        protected ILogicalModule ParentLogicalModule
        {
            get
            {
                return this.m_objParentLogicalModule;
            }
        }

        protected override ActionType CancelActionType
        {
            get
            {
                return ActionType.RedirectToOriginalReferrer;
            }
        }

        protected override ActionType CloseActionType
        {
            get
            {
                return ActionType.RedirectToOriginalReferrer;
            }
        }

        protected override ActionType PostSaveActionType
        {
            get
            {
                return ActionType.RemainOnWorkpaper;
            }
        }

        protected virtual BreadCrumbType BreadCrumbDisplayType
        {
            get
            {
                return BreadCrumbType.FullBreadCrumb;
            }
        }

        protected override int PrintZoomPercentage
        {
            get
            {
                return 100;
            }
        }

        protected override Javascript[] WorkpaperSpecificJavascript
        {
            get
            {
                return null;
            }
        }

        protected override Stylesheet[] WorkpaperSpecificStylesheets
        {
            get
            {
                return null;
            }
        }

        protected override string SelectedMenuButton
        {
            get
            {
                return String.Empty;
            }
        }

        protected virtual bool IsConsolSelected()
        {
            return false;
        }
        #endregion

        #region User context related Methods
        public string GetContextName(DataSet objData)
        {
            if (objData is BusinessStructureWorkpaperDS)
            {
                if (objData.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows[0][BusinessStructureWorkpaperDS.REPORTINGUNITCLID_FIELD] != DBNull.Value
                    && (Guid)objData.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows[0][BusinessStructureWorkpaperDS.REPORTINGUNITCLID_FIELD] != Guid.Empty)
                {
                    Guid gdRUCLID = GetUserContextReportingUnitClid((BusinessStructureWorkpaperDS)objData);
                    string strRUName = GetUserContextReportingUnitName((BusinessStructureWorkpaperDS)objData);
                    Guid gdRUCSID = GetUserContextCsid((BusinessStructureWorkpaperDS)objData);

                    ILogicalModule objRULM = this.Broker.GetLogicalCM(gdRUCLID);

                    //Check that the RU has not been deleted.
                    if (objRULM != null)
                    {
                        ICalculationModule objRU = objRULM[gdRUCSID];

                        //Check that the RU has not been deleted.
                        if (objRU != null)
                        {
                            if (string.Compare(objRULM.Name, strRUName, true) == 0)
                                return strRUName;
                            else
                            {
                                ResetUserContext(objData);
                                return String.Empty;
                            }
                        }

                        this.Broker.ReleaseBrokerManagedComponent(objRU);
                    }
                    else
                    {
                        ResetUserContext(objData);
                        return String.Empty;
                    }

                    this.Broker.ReleaseBrokerManagedComponent(objRULM);
                }
            }
            return String.Empty;
        }
        public string GetContextPeriod(DataSet objData)
        {
            if (objData is BusinessStructureWorkpaperDS)
            {
                if (objData.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows[0][BusinessStructureWorkpaperDS.REPORTINGUNITCLID_FIELD] != DBNull.Value
                    && (Guid)objData.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows[0][BusinessStructureWorkpaperDS.REPORTINGUNITCLID_FIELD] != Guid.Empty)
                {
                    Guid gdRUCLID = GetUserContextReportingUnitClid((BusinessStructureWorkpaperDS)objData);
                    string strRUName = GetUserContextReportingUnitName((BusinessStructureWorkpaperDS)objData);
                    Guid gdRUCSID = GetUserContextCsid((BusinessStructureWorkpaperDS)objData);
                    string strRUScenarioName = GetUserContextScenarioName((BusinessStructureWorkpaperDS)objData);
                    Guid gdPeriodCLID = GetUserContextPeriodClid((BusinessStructureWorkpaperDS)objData);
                    string strPeriodName = GetUserContextPeriodName((BusinessStructureWorkpaperDS)objData);

                    ILogicalModule objRULM = this.Broker.GetLogicalCM(gdRUCLID);

                    //Check that the RU has not been deleted.
                    if (objRULM != null)
                    {
                        ICalculationModule objRU = objRULM[gdRUCSID];

                        if (objRU != null)
                        {
                            string strCurrentScenarioName = ((LogicalModuleBase.CMScenarios)objRULM.Scenarios)[gdRUCSID].Name;
                            //Make sure that the current name of the reporting unit is the same as the one in context
                            //and also for the scenario name
                            if ((string.Compare(objRULM.Name, strRUName, true) == 0) && 
                                (string.Compare(strCurrentScenarioName, strRUScenarioName, true) == 0))
                            {
                                if (gdPeriodCLID != Guid.Empty)
                                {
                                    ICalculationModule objPeriod = this.Broker.GetCMImplementation(gdPeriodCLID, gdRUCSID);

                                    try
                                    {
                                        if (objPeriod != null && objPeriod.CLID == gdPeriodCLID)
                                            return strPeriodName;
                                        else
                                        {
                                            ResetUserContext(objData);
                                            return String.Empty;
                                        }
                                    }
                                    finally
                                    {
                                        this.Broker.ReleaseBrokerManagedComponent(objPeriod);
                                    }
                                }
                            }
                            else
                            {
                                ResetUserContext(objData);
                                return String.Empty;
                            }
                        }

                        this.Broker.ReleaseBrokerManagedComponent(objRU);
                    }
                    else
                    {
                        ResetUserContext(objData);
                        return String.Empty;
                    }

                    this.Broker.ReleaseBrokerManagedComponent(objRULM);
                }
            }
            return String.Empty;
        }

        public string GetContextScenario(DataSet objData)
        {
            if (objData is BusinessStructureWorkpaperDS)
            {
                if (objData.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows[0][BusinessStructureWorkpaperDS.REPORTINGUNITCLID_FIELD] != DBNull.Value
                    && (Guid)objData.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows[0][BusinessStructureWorkpaperDS.REPORTINGUNITCLID_FIELD] != Guid.Empty)
                {
                    Guid gdRUCLID = GetUserContextReportingUnitClid((BusinessStructureWorkpaperDS)objData);
                    string strRUName = GetUserContextReportingUnitName((BusinessStructureWorkpaperDS)objData);
                    Guid gdRUCSID = GetUserContextCsid((BusinessStructureWorkpaperDS)objData);
                    string strRUScenarioName = GetUserContextScenarioName((BusinessStructureWorkpaperDS)objData);

                    ILogicalModule objRULM = this.Broker.GetLogicalCM(gdRUCLID);

                    //Check that the RU has not been deleted.
                    if (objRULM != null)
                    {
                        ICalculationModule objRU = objRULM[gdRUCSID];

                        if (objRU != null)
                        {
                            CMScenario cmScenario = ((LogicalModuleBase.CMScenarios)objRULM.Scenarios)[gdRUCSID];
                            string strCurrentScenarioName = cmScenario.Name;
                            //Make sure that the current name of the reporting unit is the same as the one in context
                            //and also for the scenario name
                            if ((string.Compare(objRULM.Name, strRUName, true) == 0) &&
                                (string.Compare(strCurrentScenarioName, strRUScenarioName, true) == 0))
                            {
                                if (objRULM.IsScenarioLocked(gdRUCSID))
                                    return strRUScenarioName + " (locked)";
                                else
                                    return strRUScenarioName;
                            }
                            else
                            {
                                ResetUserContext(objData);
                                return String.Empty;
                            }
                        }

                        this.Broker.ReleaseBrokerManagedComponent(objRU);
                    }
                    else
                    {
                        ResetUserContext(objData);
                        return String.Empty;
                    }

                    this.Broker.ReleaseBrokerManagedComponent(objRULM);
                }
            }
            return String.Empty;
        }

        public String ContextBreadCrumbWithoutPeriod(DataSet objData)
        {
            return this.ContextBreadCrumb(objData, false);
        }
        public String FullContextBreadCrumb(DataSet objData)
        {
            return this.ContextBreadCrumb(objData, true);
        }
        public String ContextBreadCrumb(DataSet objData, bool blIncludePeriod)
        {
            string strBreadCrumb = String.Empty;
            if (objData is BusinessStructureWorkpaperDS)
            {
                if (objData.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows[0][BusinessStructureWorkpaperDS.REPORTINGUNITCLID_FIELD] != DBNull.Value
                    && (Guid)objData.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows[0][BusinessStructureWorkpaperDS.REPORTINGUNITCLID_FIELD] != Guid.Empty)
                {
                    Guid gdRUCLID = GetUserContextReportingUnitClid((BusinessStructureWorkpaperDS)objData);
                    string strRUName = GetUserContextReportingUnitName((BusinessStructureWorkpaperDS)objData);
                    Guid gdRUCSID = GetUserContextCsid((BusinessStructureWorkpaperDS)objData);
                    string strRUScenarioName = GetUserContextScenarioName((BusinessStructureWorkpaperDS)objData);
                    Guid gdPeriodCLID = GetUserContextPeriodClid((BusinessStructureWorkpaperDS)objData);
                    string strPeriodName = GetUserContextPeriodName((BusinessStructureWorkpaperDS)objData);

                    ILogicalModule objRULM = this.Broker.GetLogicalCM(gdRUCLID);

                    //Check that the RU has not been deleted.
                    if (objRULM != null)
                    {
                        ICalculationModule objRU = objRULM[gdRUCSID];

                        // if the scenario has been deleted, get the default scenario
                        if (objRU == null)
                        {
                            gdRUCSID = objRULM.CurrentScenario;
                            objRU = objRULM[objRULM.CurrentScenario];
                        }

                        CMScenario cmScenario = ((LogicalModuleBase.CMScenarios)objRULM.Scenarios)[gdRUCSID];
                        string strCurrentScenarioName = cmScenario.Name;
                        //Make sure that the current name of the reporting unit is the same as the one in context
                        //and also for the scenario name
                        if (objRULM.Name == strRUName
                            && strCurrentScenarioName == strRUScenarioName)
                        {
                            strBreadCrumb += objRU.Name;

                            if (gdPeriodCLID != Guid.Empty && blIncludePeriod)
                            {
                                ICalculationModule objPeriod = this.Broker.GetCMImplementation(gdPeriodCLID, gdRUCSID);

                                if (objPeriod != null)
                                {
                                    if (objPeriod.CLID == gdPeriodCLID)
                                    {
                                        if (objRULM.IsScenarioLocked(gdRUCSID))
                                            strCurrentScenarioName += " (locked)";
                                        strBreadCrumb += " > " + strPeriodName + "[" + strCurrentScenarioName + "]";
                                    }
                                    else
                                    {
                                        //If the period name has changed then reset the context
                                        ResetUserContext(objData);
                                        strBreadCrumb = String.Empty;
                                    }
                                    this.Broker.ReleaseBrokerManagedComponent(objPeriod);
                                }
                            }
                            else
                                strBreadCrumb += " [" + strCurrentScenarioName + "]";
                        }
                        else //If the reporting unit name has changed then reset the context
                            ResetUserContext(objData);

                        this.Broker.ReleaseBrokerManagedComponent(objRU);
                    }

                    this.Broker.ReleaseBrokerManagedComponent(objRULM);
                }
            }

            return strBreadCrumb;
        }

        public void ResetUserContext(DataSet objData)
        {
            if (objData is BusinessStructureWorkpaperDS)
            {
                ((BusinessStructureWorkpaperDS)objData).ReportingUnitName = String.Empty;
                ((BusinessStructureWorkpaperDS)objData).ReportingUnitCLID = Guid.Empty;

                ((BusinessStructureWorkpaperDS)objData).PeriodName = String.Empty;
                ((BusinessStructureWorkpaperDS)objData).PeriodCLID = Guid.Empty;

                ((BusinessStructureWorkpaperDS)objData).ScenarioName = String.Empty;
                ((BusinessStructureWorkpaperDS)objData).ScenarioCSID = Guid.Empty;
            }

        }

        public void SetScenarioContext(Guid gdScenarioCSID, bool blForceSystemContext)
        {
            if (this.m_objData is BusinessStructureWorkpaperDS)
            {
                BusinessStructureWorkpaperDS bsWorkpaperDS = (BusinessStructureWorkpaperDS)m_objData;
                if (bsWorkpaperDS.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows.Count == 1)
                {

                    if (bsWorkpaperDS.ScenarioCSID != gdScenarioCSID
                        || blForceSystemContext)
                    {
                        ILogicalModule objRULM = this.Broker.GetLogicalCM(this.UserCurrentReportingUnitCLID);
                        if (objRULM != null)
                        {
                            if (((LogicalModuleBase.CMScenarios)objRULM.Scenarios)[gdScenarioCSID] != null)
                            {
                                //Set the scenario to the current scenario for the reporting unit.
                                bsWorkpaperDS.ScenarioCSID = gdScenarioCSID;
                                bsWorkpaperDS.ScenarioName = ((LogicalModuleBase.CMScenarios)objRULM.Scenarios)[gdScenarioCSID].Name;

                                //If there is a current period set then set teh user context to that period in the current scenario.
                                IOrganizationUnit objOrgUnit = (IOrganizationUnit)objRULM[gdScenarioCSID];
                                if (objOrgUnit.CurrentPeriod != Guid.Empty)
                                {
                                    bsWorkpaperDS.PeriodCLID = objOrgUnit.CurrentPeriod;
                                    ICalculationModule iCM = this.Broker.GetCMImplementation(objOrgUnit.CurrentPeriod, gdScenarioCSID);
                                    bsWorkpaperDS.PeriodName = ((IBrokerManagedComponent)iCM).Name;

                                }
                                else
                                {
                                    bsWorkpaperDS.PeriodCLID = Guid.Empty;
                                    bsWorkpaperDS.PeriodName = String.Empty;
                                }
                            }
                            else
                            {
                                throw new MissingComponentException("CM for CSID does not exist");
                            }
                        }

                    }
                }
            }

        }

        public void SetPeriodToCurrent()
        {
            if (this.m_objData is BusinessStructureWorkpaperDS)
            {
                BusinessStructureWorkpaperDS bsWorkpaperDS = (BusinessStructureWorkpaperDS)m_objData;
                bsWorkpaperDS.PeriodCLID = this.m_objLogicalModuleID;
                bsWorkpaperDS.PeriodName = this.NameOnCreation;
            }
        }

        public void SetScenarioToCurrent(Guid gdScenarioID, string strScenarioName)
        {
            if (this.m_objData is BusinessStructureWorkpaperDS)
            {
                BusinessStructureWorkpaperDS bsWorkpaperDS = (BusinessStructureWorkpaperDS)m_objData;
                bsWorkpaperDS.ScenarioCSID = gdScenarioID;
                bsWorkpaperDS.ScenarioName = strScenarioName;
            }
        }

        public void SetReportingUnitToCurrentBMC()
        {
            this.SetReportingUnitContext(this.m_objLogicalModuleID, true);
        }

        /// <summary>
        /// For a given reporting unit CLID and CSID set up the context, setting the period to the default
        /// period for the RU if possible
        /// </summary>
        /// <param name="gdRUCLID"></param>
        /// <param name="gdRUCSID"></param>
        /// <param name="gdPeriodCLID"></param>
        public void SetPeriodContextToDefaultForRU(Guid gdRUCLID, Guid gdRUCSID, Guid gdPeriodCLID)
        {
            if (this.m_objData is BusinessStructureWorkpaperDS)
            {
                BusinessStructureWorkpaperDS bsWorkpaperDS = (BusinessStructureWorkpaperDS)m_objData;
                if (bsWorkpaperDS.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows.Count == 1)
                {

                    ILogicalModule objRULM = this.Broker.GetLogicalCM(gdRUCLID);
                    if (objRULM != null)
                    {
                        bsWorkpaperDS.ReportingUnitCLID = gdRUCLID;

                        bsWorkpaperDS.ScenarioCSID = gdRUCSID;
                        bsWorkpaperDS.ScenarioName = ((LogicalModuleBase.CMScenarios)objRULM.Scenarios)[gdRUCSID].Name;

                        //If there is a current period set then set teh user context to that period in the current scenario.
                        IOrganizationUnit objOrgUnit = (IOrganizationUnit)objRULM[gdRUCSID];
                        bsWorkpaperDS.ReportingUnitName = objRULM.Name;
                        //If a period clid has been passed in, then use this to set the context
                        if (gdPeriodCLID != Guid.Empty)
                        {
                            bsWorkpaperDS.PeriodCLID = gdPeriodCLID;
                            ICalculationModule iCM = this.Broker.GetCMImplementation(gdPeriodCLID, gdRUCSID);
                            if (iCM != null)
                            {
                                bsWorkpaperDS.PeriodName = ((IBrokerManagedComponent)iCM).Name;
                            }
                            else
                            {
                                throw new MissingComponentException("CM for Period CLID does not exist");
                            }
                        }
                        //If the passed in guid was empty, then attempt to set the context to the current period
                        else if (objOrgUnit.CurrentPeriod != Guid.Empty)
                        {
                            bsWorkpaperDS.PeriodCLID = objOrgUnit.CurrentPeriod;
                            ICalculationModule iCM = this.Broker.GetCMImplementation(objOrgUnit.CurrentPeriod, gdRUCSID);
                            if (iCM != null)
                            {
                                bsWorkpaperDS.PeriodName = ((IBrokerManagedComponent)iCM).Name;
                            }
                            else
                            {
                                throw new MissingComponentException("CM for Period CLID does not exist");
                            }
                        }
                        else
                        {
                            bsWorkpaperDS.PeriodCLID = Guid.Empty;
                            bsWorkpaperDS.PeriodName = String.Empty;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Sets all user context information to the current sytem defaults for the specified Reporting Unit CLID
        /// </summary>
        /// <param name="gdRUCLID"></param>
        /// <param name="blForceSystemContext"></param>
        public void SetReportingUnitContext(Guid gdRUCLID, bool blForceSystemContext)
        {
            if (this.m_objData is BusinessStructureWorkpaperDS)
            {
                BusinessStructureWorkpaperDS bsWorkpaperDS = (BusinessStructureWorkpaperDS)m_objData;
                if (bsWorkpaperDS.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows.Count == 1)
                {
                    //If the value is being changed, then also set the period and scenario to the system 
                    //default for the new reporting unit.
                    //Also do this is the ForceSystemContext flag is set to true;
                    if (bsWorkpaperDS.ReportingUnitCLID != gdRUCLID
                        || blForceSystemContext)
                    {
                        ILogicalModule objRULM = this.Broker.GetLogicalCM(gdRUCLID);
                        if (objRULM != null)
                        {
                            //Log events
                            this.LogEvent(EventType.SetCurrentEntity, "Set '" + objRULM.Name + "' as current Entity");

                            bsWorkpaperDS.ReportingUnitCLID = gdRUCLID;

                            //Set the scenario to the current scenario for the reporting unit.
                            bsWorkpaperDS.ScenarioCSID = objRULM.CurrentScenario;
                            bsWorkpaperDS.ScenarioName = ((LogicalModuleBase.CMScenarios)objRULM.Scenarios)[objRULM.CurrentScenario].Name;

                            //If there is a current period set then set teh user context to that period in the current scenario.
                            IOrganizationUnit objOrgUnit = (IOrganizationUnit)objRULM[objRULM.CurrentScenario];
                            bsWorkpaperDS.ReportingUnitName = objRULM.Name;
                            if (objOrgUnit.CurrentPeriod != Guid.Empty)
                            {
                                bsWorkpaperDS.PeriodCLID = objOrgUnit.CurrentPeriod;
                                ICalculationModule iCM = this.Broker.GetCMImplementation(objOrgUnit.CurrentPeriod, objRULM.CurrentScenario);
                                bsWorkpaperDS.PeriodName = iCM.Name;
                                this.Broker.ReleaseBrokerManagedComponent(iCM);

                            }
                            else
                            {
                                bsWorkpaperDS.PeriodCLID = Guid.Empty;
                                bsWorkpaperDS.PeriodName = String.Empty;
                            }

                            this.Broker.ReleaseBrokerManagedComponent(objOrgUnit);
                        }

                        this.Broker.ReleaseBrokerManagedComponent(objRULM);
                    }
                }
            }
        }

        public String UserCurrentReportingUnitName
        {

            get
            {
                if (this.m_objData is BusinessStructureWorkpaperDS)
                {
                    return GetUserContextReportingUnitName((BusinessStructureWorkpaperDS)m_objData);
                }
                else
                    return String.Empty;
            }
            set
            {
                if (this.m_objData is BusinessStructureWorkpaperDS)
                    if (this.m_objData.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows.Count == 1)
                        this.m_objData.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows[0][BusinessStructureWorkpaperDS.REPORTINGUNITNAME_FIELD] = value;

            }
        }
        public Guid UserCurrentReportingUnitCLID
        {
            get
            {
                if (this.m_objData is BusinessStructureWorkpaperDS)
                    return GetUserContextReportingUnitClid((BusinessStructureWorkpaperDS)m_objData);
                else
                    return Guid.Empty;
            }
            //			set
            //			{
            //				if(this.m_objData is BusinessStructureWorkpaperDS)
            //				{
            //					if(this.m_objData.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows.Count == 1)
            //					{
            //						//If the value is being changed, then also set the period and scenario if they are not already set.
            //						if((Guid)this.m_objData.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows[0][BusinessStructureWorkpaperDS.REPORTINGUNITCLID_FIELD] != value)
            //						{
            //							Guid gdRUCLID = value;
            //							this.m_objData.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows[0][BusinessStructureWorkpaperDS.REPORTINGUNITCLID_FIELD] = gdRUCLID;
            //							
            //							//Set the scenario to the current scenario for the reporting unit.
            //							ILogicalModule  objRULM = this.Broker.GetLogicalCM(value);
            //							this.m_objData.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows[0][BusinessStructureWorkpaperDS.SCENARIOCSID_FIELD] = objRULM.CurrentScenario;
            //							this.m_objData.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows[0][BusinessStructureWorkpaperDS.SCENARIONAME_FIELD] = ((LogicalModuleBase.CMScenarios)objRULM.Scenarios)[objRULM.CurrentScenario].Name;
            //
            //							//If there is a current period set then set teh user context to that period in the current scenario.
            //							IOrganizationUnit objOrgUnit = (IOrganizationUnit)objRULM[objRULM.CurrentScenario];
            //							if(objOrgUnit.CurrentPeriod != Guid.Empty)
            //							{
            //								this.m_objData.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows[0][BusinessStructureWorkpaperDS.PERIODCLID_FIELD] = objOrgUnit.CurrentPeriod;
            //								ICalculationModule iCM = this.Broker.GetCMImplementation(objOrgUnit.CurrentPeriod,objRULM.CurrentScenario);
            //								this.m_objData.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows[0][BusinessStructureWorkpaperDS.PERIODNAME_FIELD] = ((IBrokerManagedComponent)iCM).Name;
            //
            //							}
            //						}
            //					}
            //				}
            //			}

        }

        public String UserCurrentScenarioName
        {
            get
            {

                if (this.m_objData is BusinessStructureWorkpaperDS)
                    return GetUserContextScenarioName((BusinessStructureWorkpaperDS)m_objData);
                else
                    return String.Empty;
            }
            set
            {
                if (this.m_objData is BusinessStructureWorkpaperDS)
                    if (this.m_objData.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows.Count == 1)
                        this.m_objData.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows[0][BusinessStructureWorkpaperDS.SCENARIONAME_FIELD] = value;

            }
        }
        public Guid UserCurrentScenarioCSID
        {
            get
            {
                if (this.m_objData is BusinessStructureWorkpaperDS)
                    return GetUserContextCsid((BusinessStructureWorkpaperDS)m_objData);
                else
                    return Guid.Empty;
            }
            set
            {
                if (this.m_objData is BusinessStructureWorkpaperDS)
                    if (this.m_objData.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows.Count == 1)
                        this.m_objData.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows[0][BusinessStructureWorkpaperDS.SCENARIOCSID_FIELD] = value;

            }
        }

        public String UserCurrentPeriodName
        {

            get
            {
                if (this.m_objData is BusinessStructureWorkpaperDS)
                    return GetUserContextPeriodName((BusinessStructureWorkpaperDS)m_objData);
                else
                    return String.Empty;
            }
            set
            {
                if (this.m_objData is BusinessStructureWorkpaperDS)
                    if (this.m_objData.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows.Count == 1)
                        this.m_objData.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows[0][BusinessStructureWorkpaperDS.PERIODNAME_FIELD] = value;
            }
        }
        public bool IsEmptyContext()
        {
            if (this.m_objData is BusinessStructureWorkpaperDS)
            {
                if (this.m_objData.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows.Count == 1)
                {
                    return (this.m_objData.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows[0][BusinessStructureWorkpaperDS.REPORTINGUNITNAME_FIELD].ToString() != String.Empty
                        && this.m_objData.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows[0][BusinessStructureWorkpaperDS.REPORTINGUNITCLID_FIELD] != DBNull.Value
                        && this.m_objData.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows[0][BusinessStructureWorkpaperDS.PERIODNAME_FIELD].ToString() != String.Empty
                        && this.m_objData.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows[0][BusinessStructureWorkpaperDS.PERIODCLID_FIELD] != DBNull.Value
                        && this.m_objData.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows[0][BusinessStructureWorkpaperDS.SCENARIONAME_FIELD].ToString() != String.Empty
                        && this.m_objData.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows[0][BusinessStructureWorkpaperDS.SCENARIOCSID_FIELD] != DBNull.Value);

                }
            }
            return true;
        }
        public Guid UserCurrentPeriodCLID
        {
            get
            {
                if (this.m_objData is BusinessStructureWorkpaperDS)
                    return GetUserContextPeriodClid((BusinessStructureWorkpaperDS)m_objData);
                else
                    return Guid.Empty;
            }
            set
            {
                if (this.m_objData is BusinessStructureWorkpaperDS)
                    if (this.m_objData.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows.Count == 1)
                        this.m_objData.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows[0][BusinessStructureWorkpaperDS.PERIODCLID_FIELD] = value;

            }
        }
        #endregion

        #region Methods
        public override TabType[] GetTabs()
        {
            return null;
        }

        protected override void CloseWorkpaper()
        {
            this.PerformAction(this.CloseActionType);
        }

        protected override void PopulateBreadcrumb(DataSet objData)
        {
            if (!(objData is PartyDS))
            {
                if (this.BreadCrumbDisplayType == BreadCrumbType.FullBreadCrumbWhenInstanceExists)
                {
                    if (this.m_objInstanceID == Guid.Empty)
                        this.m_objHeader.BreadCrumb = String.Empty;
                    else
                        SetFullBreadCrumb(objData);
                }
                if (this.BreadCrumbDisplayType == BreadCrumbType.NoPeriodBreadCrumbWhenInstanceExists)
                {
                    if (this.m_objInstanceID == Guid.Empty)
                        this.m_objHeader.BreadCrumb = String.Empty;
                    else
                        SetNoPeriodBreadCrumb(objData);
                }
                else if (this.BreadCrumbDisplayType == BreadCrumbType.FullBreadCrumb)
                {
                    SetFullBreadCrumb(objData);
                }
                else if (this.BreadCrumbDisplayType == BreadCrumbType.NoPeriodNoScenario)
                {
                    SetNoPeriodNoScenarioBreadCrumb(objData);
                }
                else //No period is selected
                {
                    SetNoPeriodBreadCrumb(objData);
                }
            }
            else
            {
                //Security Workpaper
                this.m_objHeader.BreadCrumb = ((ICalculationModule)this.m_objBMC).Name;
                this.m_objHeader.BreadCrumbScenario = this.Broker.GetLogicalCM(this.m_objLogicalModuleID).GetScenario(this.m_objScenarioID).Name;
            }
        }

        private void SetFullBreadCrumb(DataSet objData)
        {
            if (this.IsConsolSelected())
            {
                this.m_objHeader.BreadCrumb = "Adjustment entity for: " + this.GetContextName(objData);
            }
            else
            {
                this.m_objHeader.BreadCrumb = this.GetContextName(objData);//this.FullContextBreadCrumb(objData);
            }
            this.m_objHeader.BreadCrumbPeriod = this.GetContextPeriod(objData);
            this.m_objHeader.BreadCrumbScenario = this.GetContextScenario(objData);
        }

        private void SetNoPeriodBreadCrumb(DataSet objData)
        {
            this.m_objHeader.BreadCrumb = this.GetContextName(objData);
            this.m_objHeader.BreadCrumbScenario = this.GetContextScenario(objData);
        }
        private void SetNoPeriodNoScenarioBreadCrumb(DataSet objData)
        {
            this.m_objHeader.BreadCrumb = this.GetContextName(objData);
        }

        protected override void InitialiseIDs(Guid objCID)
        {
            if (objCID == Guid.Empty)
            {
                string strLogicalModuleID = Context.Request.QueryString["CLID"];
                string strScenarioID = Context.Request.QueryString["CSID"];
                string strParentLogicalModuleID = Context.Request.QueryString["PLID"];

                if (strLogicalModuleID != null && strLogicalModuleID != String.Empty)
                    this.m_objLogicalModuleID = new Guid(strLogicalModuleID);
                else
                    this.m_objLogicalModuleID = Guid.Empty;

                if (strScenarioID != null && strScenarioID != String.Empty)
                    this.m_objScenarioID = new Guid(strScenarioID);
                else
                    this.m_objScenarioID = Guid.Empty;

                if (strParentLogicalModuleID != null && strParentLogicalModuleID != String.Empty)
                {
                    this.m_objParentLogicalModuleID = new Guid(strParentLogicalModuleID);
                }
                else
                    this.m_objParentLogicalModuleID = Guid.Empty;

                Guid objCalculationModuleInstanceID;

                if (this.m_objLogicalModuleID != Guid.Empty &&
                    this.m_objScenarioID != Guid.Empty)
                {
                    objCalculationModuleInstanceID = this.Broker.GetCMInstanceID(this.m_objLogicalModuleID, this.m_objScenarioID);

                    if (objCalculationModuleInstanceID == Guid.Empty)
                        throw new MissingComponentException("CM for CLID and CSID does not exist");
                }
                else
                    objCalculationModuleInstanceID = Guid.Empty;

                base.InitialiseIDs(objCalculationModuleInstanceID);
            }
            else
                base.InitialiseIDs(objCID);
        }

        protected override void InitialiseObjects()
        {
            base.InitialiseObjects();

            if (this.m_objParentLogicalModuleID != Guid.Empty)
                this.m_objParentLogicalModule = this.Broker.GetLogicalCM(this.m_objParentLogicalModuleID);
        }


        public override void ReleaseObjects()
        {
            this.Broker.ReleaseBrokerManagedComponent(this.m_objParentLogicalModule);
            base.ReleaseObjects();
        }

        protected override IBrokerManagedComponent CreateNewBrokerManagedComponent()
        {
            ILogicalModule objNewOwnerLogicalModule = this.Broker.CreateLogicalCM(this.BMCTypeName, this.NameOnCreation, Guid.Empty, this.ParentLogicalModule.CurrentBusinessStructureContext);
            Guid gdCSID;

            if (this.m_objScenarioID != Guid.Empty)
            {
                this.ParentLogicalModule.AddChildLogicalCM(objNewOwnerLogicalModule, this.m_objScenarioID);
                gdCSID = this.m_objScenarioID;
            }
            else
            {
                this.ParentLogicalModule.AddChildLogicalCM(objNewOwnerLogicalModule);
                gdCSID = objNewOwnerLogicalModule.CurrentScenario;
            }

            ICalculationModule objBMC = objNewOwnerLogicalModule[gdCSID];

            this.m_objInstanceID = objBMC.CIID;
            this.m_objScenarioID = gdCSID;
            this.m_objLogicalModuleID = objBMC.CLID;

            this.Broker.UpdateCacheEntry(objNewOwnerLogicalModule);

            return objBMC;
        }

        protected override string GetWorkpaperSpecificURLIDs()
        {
            return "CLID=" + this.m_objLogicalModuleID.ToString() + "&CSID=" + this.m_objScenarioID.ToString() + "&PLID=" + this.m_objParentLogicalModuleID.ToString();
        }

        protected IOrganization GetOrganisation()
        {
            return (IOrganization)this.Broker.GetWellKnownCM(WellKnownCM.Organization);
        }
        protected string GetUserContextReportingUnitName(BusinessStructureWorkpaperDS objData)
        {
            if (objData.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows.Count == 1)
                return objData.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows[0][BusinessStructureWorkpaperDS.REPORTINGUNITNAME_FIELD].ToString();
            else
                return String.Empty;
        }
        protected Guid GetUserContextReportingUnitClid(BusinessStructureWorkpaperDS objData)
        {
            if (objData.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows.Count == 1)
                return (Guid)objData.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows[0][BusinessStructureWorkpaperDS.REPORTINGUNITCLID_FIELD];
            else
                return Guid.Empty;
        }
        protected string GetUserContextScenarioName(BusinessStructureWorkpaperDS objData)
        {
            if (objData.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows.Count == 1)
                return objData.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows[0][BusinessStructureWorkpaperDS.SCENARIONAME_FIELD].ToString();
            else
                return String.Empty;
        }
        protected Guid GetUserContextCsid(BusinessStructureWorkpaperDS objData)
        {
            if (objData.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows.Count == 1)
                return (Guid)objData.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows[0][BusinessStructureWorkpaperDS.SCENARIOCSID_FIELD];
            else
                return Guid.Empty;
        }
        protected string GetUserContextPeriodName(BusinessStructureWorkpaperDS objData)
        {
            if (objData.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows.Count == 1)
                return objData.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows[0][BusinessStructureWorkpaperDS.PERIODNAME_FIELD].ToString();
            else
                return String.Empty;
        }
        protected Guid GetUserContextPeriodClid(BusinessStructureWorkpaperDS objData)
        {
            if (objData.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows.Count == 1)
                return (Guid)objData.Tables[BusinessStructureWorkpaperDS.USERCONTEXT_TABLE].Rows[0][BusinessStructureWorkpaperDS.PERIODCLID_FIELD];
            else
                return Guid.Empty;
        }
        #endregion
        #region Event Handlers

        protected void UserManagementNavigation(object sender, CommandEventArgs e)
        {
            this.SetRedirectActionPage("AdministrationUserList.aspx", "DBUser_1_1");
            this.AfterPostbackActionType = ActionType.RedirectToSpecificWorkpaper;
        }

        protected void CalculationsAndReportsNavigation(object sender, CommandEventArgs e)
        {
            this.SetRedirectActionPage("EntitiesBox.aspx", "Organization_1_1");
            this.AfterPostbackActionType = ActionType.RedirectToSpecificWorkpaper;
        }

        protected void GeneralLedgerMappingNavigation(object sender, CommandEventArgs e)
        {
            this.SetRedirectActionPage("MaintainCOA.aspx", "ChartOfAccounts_1_1");
            this.AfterPostbackActionType = ActionType.RedirectToSpecificWorkpaper;
        }

        protected void ChangePasswordMappingNavigation(object sender, CommandEventArgs e)
        {
            IBrokerManagedComponent objBMC = Broker.GetBMCInstance(Context.User.Identity.Name, "DBUser_1_1");
            this.SetRedirectActionPage("ChangeUserPassword.aspx", "DBUser_1_1", new string[] { "CID" }, new string[] { objBMC.CID.ToString() });
            this.AfterPostbackActionType = ActionType.RedirectToSpecificWorkpaper;
        }

        protected void LogoffMappingNavigation(object sender, CommandEventArgs e)
        {
            this.SetRedirectActionPage("Logout.aspx", "DBUser_1_1");
            this.AfterPostbackActionType = ActionType.RedirectToSpecificWorkpaper;
        }
        #endregion
    }
}
