﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oritax.TaxSimp.CalculationInterface;
using System.Threading;
using Oritax.TaxSimp.Utilities;
using System.Diagnostics;
using Oritax.TaxSimp.Services.CMBroker;

namespace Oritax.TaxSimp.Calculation
{
    public class BrokerManagedBase
    {
        #region Protected Properties

        protected ICMBroker CmBroker
        {
            get
            {
                LocalDataStoreSlot brokerSlot = Thread.GetNamedDataSlot("Broker");

                if (brokerSlot == null)
                    return null;

                return (ICMBroker)Thread.GetData(brokerSlot);
            }
            set
            {
                LocalDataStoreSlot brokerSlot = Thread.GetNamedDataSlot("Broker");

                if (brokerSlot == null)
                    brokerSlot = Thread.AllocateNamedDataSlot("Broker");

                if (value == null)
                    Thread.FreeNamedDataSlot("Broker");

                Thread.SetData(brokerSlot, value);
            }
        }

        #endregion // Protected Properties

        #region Public Methods
        #endregion // Public Methods

        #region Protected Methods

        protected IBrokerManagedComponent GetBrokerManagedComponent(string clidString, string csidString)
        {

            Guid logicalModuleId;
            Guid scenarioId;

            if (!string.IsNullOrEmpty(clidString))
                logicalModuleId = new Guid(clidString);
            else
                throw new Exception("GetBrokerManagedComponent, CLID not specified ");

            if (!string.IsNullOrEmpty(csidString))
                scenarioId = new Guid(csidString);
            else
                throw new Exception("GetBrokerManagedComponent, CSID not specified ");

            Guid objCalculationModuleInstanceID = this.CmBroker.GetCMInstanceID(logicalModuleId, scenarioId);
            IBrokerManagedComponent brokerManagedComponent = this.CmBroker.GetBMCInstance(objCalculationModuleInstanceID);
            return brokerManagedComponent;
        }

        protected void InitialiseBroker()
        {
            this.CmBroker = new CMBroker(DBConnection.Connection.ConnectionString);

            //RequestProcessStatusProvider statusProvider = (RequestProcessStatusProvider)HttpContext.Current.Items["NABLEStatusProvider"];

            //if (statusProvider != null)
            //    this.CmBroker.SetRequestStatusProvider(statusProvider);

            this.CmBroker.SetStart();
        }


        protected void InitialiseBrokerWriterStart()
        {
            this.CmBroker = new CMBroker(DBConnection.Connection.ConnectionString);

            //RequestProcessStatusProvider statusProvider = (RequestProcessStatusProvider)HttpContext.Current.Items["NABLEStatusProvider"];

            //if (statusProvider != null)
            //    this.CmBroker.SetRequestStatusProvider(statusProvider);

            this.CmBroker.SetWriteStart();
        }

        protected void Complete()
        {
            try
            {
                if (this.CmBroker.InTransaction)
                {
                    this.CmBroker.SetComplete();
                }
            }
            catch
            {
                this.CmBroker.SetAbort();
                throw;
            }
        }

        private void CleanupBroker()
        {
            if (this.CmBroker != null)
            {
                if (this.CmBroker.InTransaction)
                {
                    this.CmBroker.SetAbort();
                    Debug.Assert(true, "BrokerManagedComponentWorkpaper.CleanupBroker(), transaction on the broker was not ended correctly");
                }
                this.CmBroker.Dispose();
            }
        }

        #endregion // Protected Methods
    }
}
