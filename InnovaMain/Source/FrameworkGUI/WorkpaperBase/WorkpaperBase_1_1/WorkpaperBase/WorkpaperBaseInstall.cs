using System;

namespace Oritax.TaxSimp.Workpaper
{
	/// <summary>
	/// Summary description for Install.
	/// </summary>
	public class WorkpaperBaseInstall
	{
		#region INSTALLATION PROPERTIES

		// Assembly Installation Properties
		public const string ASSEMBLY_ID="4C8654FB-EC45-467a-855F-1B56BF526ACF";
		public const string ASSEMBLY_NAME="WorkpaperBase_1_1";
		public const string ASSEMBLY_DISPLAYNAME="WorkpaperBase V1.1";
		public const string ASSEMBLY_MAJORVERSION="1";
		public const string ASSEMBLY_MINORVERSION="1";
		public const string ASSEMBLY_DATAFORMAT="0";
//		public const string ASSEMBLY_REVISION="1";	
		public const string ASSEMBLY_REVISION="2"; //2005.1	

		#endregion
	}
}
