using System;
using System.Data;
using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.Calculation;
using Oritax.TaxSimp.CalculationInterface;

namespace Oritax.TaxSimp.Calculation
{
	/// <summary>
	/// The DataSet Class used for Notes
	/// </summary>
	[Serializable()]
	public class NoteListDS : DataSet
	{
		
		/// <summary>
		/// Notes Related Fields
		/// </summary>
		/// 

		[NonSerialized]public string BreadCrumb;

//		public const String NOTE_TABLE = "NOTE_TABLE";
//		public const String NOTE_WORKPAPERNAME_FIELD = "NOTE_WORKPAPERNAME_FIELD";


		public const String NOTES_TABLE = "NOTES_TABLE";
		public const String NOTES_PRIMARYKEY_FIELD = "ID";
		public const String NOTES_DESCRIPTION_FIELD = "NOTES_DESCRIPTION_FIELD";
		public const String NOTES_CREATEDBY_FIELD = "NOTES_CREATEDBY_FIELD";
		public const String NOTES_CREATIONDATE_FIELD = "NOTES_CREATEDDATE_FIELD";
		public const String NOTES_LASTUSERUPDATED_FIELD = "NOTES_LASTUSERUPDATED_FIELD";
		public const String NOTES_LASTDATEUPDATED_FIELD = "NOTES_LASTDATEUPDATED_FIELD";
		public const String NOTES_ROLLFORWARD_FIELD = "NOTES_ROLLFORWARD_FIELD";
		public const String NOTES_WORKPAPERID_FIELD = "NOTES_WORKPAPERID_FIELD";

		public NoteListDS()
		{

//			DataTable oDT = new DataTable(NoteListDS.NOTE_TABLE);
//			oDT.Columns.Add(NoteListDS.NOTE_WORKPAPERNAME_FIELD); 
//			Tables.Add(oDT);
//			BrokerManagedComponentDS.AddVersionStamp(oDT,typeof(NoteListDS));

			DataTable oDT = new DataTable(NoteListDS.NOTES_TABLE);
			oDT.Columns.Add(NoteListDS.NOTES_DESCRIPTION_FIELD); 
			oDT.Columns.Add(NoteListDS.NOTES_LASTUSERUPDATED_FIELD); 
			oDT.Columns.Add(NoteListDS.NOTES_LASTDATEUPDATED_FIELD); 
			oDT.Columns.Add(NoteListDS.NOTES_ROLLFORWARD_FIELD, typeof(bool)); 
			oDT.Columns.Add(NoteListDS.NOTES_CREATEDBY_FIELD);  
			oDT.Columns.Add(NoteListDS.NOTES_CREATIONDATE_FIELD); 
			oDT.Columns.Add(NoteListDS.NOTES_PRIMARYKEY_FIELD); 
			oDT.Columns.Add(NoteListDS.NOTES_WORKPAPERID_FIELD); 
			Tables.Add(oDT);
			BrokerManagedComponentDS.AddVersionStamp(oDT,typeof(NoteListDS));
		}


//		public string WorkpaperName
//		{
//			set
//			{
//
//				if(this.Tables[NoteListDS.NOTE_TABLE].Rows.Count>0)
//				{
//					DataRow bmcRow=this.Tables[NoteListDS.NOTE_TABLE].Rows[0];
//					bmcRow[NoteListDS.NOTE_WORKPAPERNAME_FIELD]=value;
//				}
//				else
//				{
//					DataRow bmcRow=this.Tables[NoteListDS.NOTE_TABLE].NewRow();
//					bmcRow[NoteListDS.NOTE_WORKPAPERNAME_FIELD]=value;
//					this.Tables[NoteListDS.NOTE_TABLE].Rows.Add(bmcRow);
//				}
//			}
//			get
//			{
//				if(this.Tables[NoteListDS.NOTE_TABLE].Rows.Count>0)
//				{
//					DataRow bmcRow=this.Tables[NoteListDS.NOTE_TABLE].Rows[0];
//					return bmcRow[NoteListDS.NOTE_WORKPAPERNAME_FIELD].ToString();
//				}
//				else
//					return null;
//			}
//		}
	}
}
