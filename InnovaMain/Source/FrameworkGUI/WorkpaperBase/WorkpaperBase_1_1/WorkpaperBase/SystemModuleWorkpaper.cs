using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Collections;

using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Utilities;

namespace Oritax.TaxSimp.Calculation
{
	/// <summary>
	/// A workpaper for System Modules
	/// </summary>
	public abstract class SystemModuleWorkpaper : BrokerManagedComponentWorkpaper
	{
		#region Constructors
		public SystemModuleWorkpaper()
		{
		}
		#endregion
		#region Methods
		public override TabType[] GetTabs()
		{
			return null;
		}

		protected override IBrokerManagedComponent CreateNewBrokerManagedComponent()
		{
			return this.Broker.CreateComponentInstance( this.BMCTypeID, this.NameOnCreation );
		}

		#endregion
		#region Properties
		private new ICMBroker Broker
		{
			get
			{
				return base.Broker;
			}
		}

		protected override string MissingComponentErrorDescription
		{
			get
			{
				return new MessageBoxDefinition( MessageBoxDefinition.MSG_WORKPAPER_DELETED ).ToString( );
			}
		}

		protected override string MissingComponentErrorTitle
		{
			get
			{
				return "Page no longer exists";
			}
		}

		protected override HeaderType WorkpaperHeaderType
		{
			get
			{
				return HeaderType.Header;
			}
		}

		protected override int PrintZoomPercentage
		{
			get
			{
				return 100;
			}
		}

		protected override ActionType PostSaveActionType
		{
			get
			{
				return ActionType.RemainOnWorkpaper;
			}
		}

		protected override ActionType CancelActionType
		{
			get
			{
				return ActionType.CloseScreen;
			}
		}

		protected override ActionType CloseActionType
		{
			get
			{
				return ActionType.RedirectToOriginalReferrer;
			}
		}

		protected override Javascript[] WorkpaperSpecificJavascript
		{
			get
			{
				return null;
			}
		}

		protected override Stylesheet[] WorkpaperSpecificStylesheets
		{
			get
			{
				return null;
			}
		}

		protected override string SelectedMenuButton
		{
			get
			{
				return String.Empty;
			}
		}
		#endregion
		#region Standard strings
		/// <summary>
		/// Error Message shown when another user updates the workpaper and the current displayed data is stale
		/// </summary>
		public const String SYSPARAM_WP_STALE_DATA = "SYSPARAM_WP_STALE_DATA";
		public const String Msg_invalid_description = "Msg_invalid_description";
		public const String Msg_invalid_reference = "Msg_invalid_reference";
		public const String Msg_exceeded_amount = "Msg_exceeded_amount";
		public const String Msg_invalid_amount = "Msg_exceeded_amount";
		public const String RegExp_DescriptionExpression = "RegExp_DescriptionExpression";
		public const String RegExp_ReferenceExpression = "RegExp_ReferenceExpression";

		public String getSystemParameter(String sSystemParameter)
		{
			if (sSystemParameter == SYSPARAM_WP_STALE_DATA)
				return new MessageBoxDefinition( MessageBoxDefinition.MSG_STALE_DATA ).ToString( );
			else if (sSystemParameter == Msg_invalid_description)
				return new MessageBoxDefinition( MessageBoxDefinition.MSG_INVALID_DESCRIPTION ).ToString( );
			else if (sSystemParameter == Msg_invalid_reference)
				return new MessageBoxDefinition( MessageBoxDefinition.MSG_INVALID_REFERENCE ).ToString( );
			else if ((sSystemParameter == RegExp_DescriptionExpression) || (sSystemParameter == RegExp_ReferenceExpression))
				return ".*";
			else
				return String.Empty;
		}
		#endregion
	}
}
