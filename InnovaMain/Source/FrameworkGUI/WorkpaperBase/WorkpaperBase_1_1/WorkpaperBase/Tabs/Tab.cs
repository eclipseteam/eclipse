using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Collections;

using PresentationControls;

namespace Oritax.TaxSimp.Calculation
{
	/// <summary>
	/// Base class for Tabs which are sub-pages on a workpaper
	/// </summary>
	public abstract class Tab : Control
	{
		#region Member Variables
		BrokerManagedComponentWorkpaper m_objParentWorkpaper;
		#endregion
		#region Constructors
		public Tab( BrokerManagedComponentWorkpaper objParentWorkpaper )
		{
			this.m_objParentWorkpaper = objParentWorkpaper;
		}
		#endregion
		#region Properties
		public abstract string TabName
		{
			get;
		}

		public string Active
		{
			get
			{
				return (string) this.ViewState[ "Active" ];
			}
			set
			{
				this.ViewState[ "Active" ] = value;
			}
		}

		protected BrokerManagedComponentWorkpaper ParentWorkpaper
		{
			get
			{
				return this.m_objParentWorkpaper;
			}
		}

		public abstract TabType TabType
		{
			get;
		}

		
		public virtual  bool SaveWhenChangeTab
		{
			get
			{
				return true;
			}
		}
		#endregion
		#region Methods
		public abstract void PopulateTab( DataSet objData );
		public abstract void DepopulateTab( DataSet objData );
		public abstract bool DepopulateTargetTab( DataSet objData );
		public abstract DataSet CreateTabDataset( );
		public abstract void CreateTabControls( );

		public virtual void GenerateDynamicTabControls(DataSet data ){}
		public virtual void ResetDynamicTabControlsData(DataSet data){}
		public virtual void ClearDynamicTabControls(){}
		#endregion
	}
}
