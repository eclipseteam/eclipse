using System;

namespace Oritax.TaxSimp.Calculation
{
	/// <summary>
	/// Specifies the tabs that may appear on a workpaper
	/// </summary>
	public enum TabType
	{
		Unknown,
		Workpaper,
		Attachments,
		Notes,
		Targets,
		Tea,
	}
}
