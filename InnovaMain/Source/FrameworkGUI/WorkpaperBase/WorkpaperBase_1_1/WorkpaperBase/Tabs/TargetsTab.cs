#region Imports

using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
	
using Oritax.TaxSimp.Utilities;

#endregion // Imports

namespace Oritax.TaxSimp.Calculation
{
	/// <summary>
	/// Tab representing the Tax Targets
	/// </summary>
	public class TargetsTab : Tab
	{
        public TargetsTab(BrokerManagedComponentWorkpaper brokerManagedComponentWorkpaper):base(brokerManagedComponentWorkpaper)
        { }

        public override DataSet CreateTabDataset()
        {
            throw new NotImplementedException();
        }

        public override TabType TabType
        {
            get { throw new NotImplementedException(); }
        }

        public override string TabName
        {
            get { throw new NotImplementedException(); }
        }

        public override void CreateTabControls()
        {
            throw new NotImplementedException();
        }

        public override void PopulateTab(DataSet objData)
        {
            throw new NotImplementedException();
        }

        public override void DepopulateTab(DataSet objData)
        {
            throw new NotImplementedException();
        }

        public override bool DepopulateTargetTab(DataSet objData)
        {
            throw new NotImplementedException();
        }
    }
}
