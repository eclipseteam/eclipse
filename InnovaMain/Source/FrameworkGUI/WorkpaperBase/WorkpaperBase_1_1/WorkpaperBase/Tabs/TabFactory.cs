using System;

namespace Oritax.TaxSimp.Calculation
{
	/// <summary>
	/// Creates tab objects for a given tab type
	/// </summary>
	public class TabFactory
	{
		private struct TabDataItem
		{
			public TabType Tab;
			public string TabDisplayString;
			public string TabCommand;

			public TabDataItem( TabType objTab, string strTabDisplayString, string strTabCommand )
			{
				this.Tab = objTab;
				this.TabDisplayString = strTabDisplayString;
				this.TabCommand = strTabCommand;
			}
		}

		private static TabDataItem[] Tabs = new TabDataItem[ 5 ];

		static TabFactory( )
		{
		}

		private TabFactory()
		{
		}

		public static Tab CreateTab( TabType objTabType, BrokerManagedComponentWorkpaper objParentWorkpaper )
		{
			Tab objTab = null;

			switch ( objTabType )
			{
				case TabType.Workpaper:
                    objTab = new WorkpaperTab(objParentWorkpaper);
					break;

				case TabType.Attachments:
                    objTab = new AttachmentsTab(objParentWorkpaper);
					break;

				case TabType.Notes:
                    objTab = new NotesTab(objParentWorkpaper);
					break;

				case TabType.Targets:
                    objTab = new TargetsTab(objParentWorkpaper);
					break;

				case TabType.Tea:
                    objTab = new TeaTab(objParentWorkpaper);
					break;
				default:
					throw new Exception( "TabFactory.CreateTab, Unknown tab type: " + objTabType.ToString( ) );
			}

			objTab.CreateTabControls( );
			return objTab;
		}

		public static bool IsTabCommand( string strCommand )
		{
			foreach( TabDataItem objTabDataItem in Tabs )
			{
				if ( objTabDataItem.TabCommand == strCommand )
					return true;
			}
			return false;
		}

		public static TabType GetTabTypeFromCommand( string strCommand )
		{
			foreach( TabDataItem objTabDataItem in Tabs )
			{
				if ( objTabDataItem.TabCommand == strCommand )
					return objTabDataItem.Tab;
			}
			return TabType.Unknown;
		}
	}
}
