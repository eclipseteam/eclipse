using System;
using System.Data;
using System.Web.UI;

using Oritax.TaxSimp.Utilities;
using Oritax.TaxSimp.CalculationInterface;

namespace Oritax.TaxSimp.Calculation
{
	/// <summary>
	/// Tab representing the Notes
	/// </summary>
	public class NotesTab : Tab
	{
        public NotesTab(BrokerManagedComponentWorkpaper brokerManagedComponentWorkpaper)
            : base(brokerManagedComponentWorkpaper)
        { }

        public override DataSet CreateTabDataset()
        {
            throw new NotImplementedException();
        }

        public override TabType TabType
        {
            get { throw new NotImplementedException(); }
        }

        public override string TabName
        {
            get { throw new NotImplementedException(); }
        }

        public override void CreateTabControls()
        {
            throw new NotImplementedException();
        }

        public override void PopulateTab(DataSet objData)
        {
            throw new NotImplementedException();
        }

        public override void DepopulateTab(DataSet objData)
        {
            throw new NotImplementedException();
        }

        public override bool DepopulateTargetTab(DataSet objData)
        {
            throw new NotImplementedException();
        }

	}
}
