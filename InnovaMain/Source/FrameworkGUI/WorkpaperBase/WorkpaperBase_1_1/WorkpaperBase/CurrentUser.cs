using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.Security;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Web;

namespace Oritax.TaxSimp.Calculation
{
    public static class CurrentUser
    {
        #region Public Properties

        public static bool DisplayZeroPreference
        {
            get
            {
                return getUser().DisplayZeroPreference;
            }

            set
            {
                IDBUser user = getUser();
                user.DisplayZeroPreference = value;
                user.CalculateToken(true);
            }
        }

        #endregion // Public Properties

        #region Private Properties

        private static IDBUser getUser()
        {
            HttpContext context = HttpContext.Current;
            string userName = HttpContext.Current.User.Identity.Name;
            LocalDataStoreSlot brokerSlot = Thread.GetNamedDataSlot("Broker");
            ICMBroker broker = (ICMBroker)Thread.GetData(brokerSlot);
            IDBUser dbUser = (IDBUser)broker.GetBMCInstance(userName, "DBUser_1_1");
            return dbUser;
        }

        #endregion // Protected and Internal Properties
    }
}
