using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Web.UI;

namespace Oritax.TaxSimp.Calculation
{
    public class ScriptManagerWrapper
    {
        #region Fields

        private Type scriptManagerType = typeof(ScriptManager);
        private ScriptManager scriptManager;

        #endregion // Fields

        #region Constructors

        public ScriptManagerWrapper(ScriptManager scriptManager)
        {
            this.scriptManager = scriptManager;
        }

        #endregion // Constructors

        #region Public Methods

        public void RegisterUpdatePanel(UpdatePanel updatePanel)
        {
            Type scriptManagerType = typeof(ScriptManager);

            FieldInfo pageRequestManagerFieldInfo = scriptManagerType.GetField("_pageRequestManager", 
                BindingFlags.NonPublic | BindingFlags.Instance);

            object pageRequestManager = pageRequestManagerFieldInfo.GetValue(scriptManager);

            Type pageRequestManagerType = pageRequestManager.GetType();

            FieldInfo allUpdatePanelsFieldInfo = pageRequestManagerType.GetField("_allUpdatePanels", 
                BindingFlags.NonPublic | BindingFlags.Instance);

            List<UpdatePanel> updatePanels = (List<UpdatePanel>)allUpdatePanelsFieldInfo.GetValue(pageRequestManager);

            if (updatePanels.Contains(updatePanel))
                return;

            MethodInfo registerPostBackControlMethodInfo = pageRequestManagerType.GetMethod("RegisterUpdatePanel", 
                BindingFlags.Instance | BindingFlags.NonPublic);

            registerPostBackControlMethodInfo.Invoke(pageRequestManager, new object[] { updatePanel });
        }

        #endregion // Public Methods
    }
}
