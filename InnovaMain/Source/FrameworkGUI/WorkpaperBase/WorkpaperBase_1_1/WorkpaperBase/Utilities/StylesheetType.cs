using System;

namespace Oritax.TaxSimp.Calculation
{
	/// <summary>
	/// Specifies the type of stylesheet
	/// </summary>
	public enum StylesheetType
	{
		IncludeFile,
		Inline,
	}

	/// <summary>
	/// Specifies the media for the stylesheet
	/// </summary>
	public enum StylesheetMedia
	{
		Screen,
		Print,
	}
}
