using System;
using System.Web.UI;
using System.Text;

namespace Oritax.TaxSimp.Calculation
{
	/// <summary>
	/// Represents a stylesheet definition
	/// </summary>
	public class Stylesheet : Control
	{
		public StylesheetMedia Media;
		public string StylesheetName;
		public string StylesheetInline;
		public StylesheetType StylesheetType;

		public Stylesheet( StylesheetMedia objMedia, StylesheetType objStylesheetType, string strStylesheetName )
		{
			this.StylesheetName = strStylesheetName;
			this.Media = objMedia;
			this.StylesheetType = objStylesheetType;
		}

		protected override void Render(HtmlTextWriter writer)
		{
			writer.WriteLine( this.ToString( ) );
		}

		public override string ToString()
		{
			string strMedia = "media=\"" + this.Media.ToString( ).ToLower( ) + "\"";

			StringBuilder objSB = new StringBuilder( );

			if ( StylesheetType == StylesheetType.IncludeFile )
			{
				objSB.Append( "<link " );
				objSB.Append( strMedia );
				objSB.Append( "href=\"" );
				objSB.Append( this.StylesheetName );
				objSB.Append( "\" type=\"text/css\" rel=\"stylesheet\">" );
			}
			else
			{
				objSB.Append( "<style " );
				objSB.Append( strMedia );
				objSB.Append( ">" );
				objSB.Append( this.StylesheetInline );
				objSB.Append( "</style>" );
			}
		
			return objSB.ToString( );
		}

	}
}
