using System;

namespace Oritax.TaxSimp.Calculation
{
	/// <summary>
	/// Specifies the type of javascript
	/// </summary>
	public enum JavaScriptType
	{
		IncludeFile,
		Inline,
	}
}
