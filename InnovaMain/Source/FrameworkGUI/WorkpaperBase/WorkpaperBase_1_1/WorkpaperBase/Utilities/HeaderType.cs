using System;

namespace Oritax.TaxSimp.Calculation
{
	/// <summary>
	/// The type of header to use for the workpaper
	/// </summary>
	public enum HeaderType
	{
		Administration,
		GenericWPHeader,
		GLMenu,
		Header,
		Menu,
		SMMenu,
		CustomHeader,
		BusinessStructureEdit,
		HeaderWorkPaperNoTabs,	
		GroupMenu,
		GroupMenuMembers,
		None,
		Report,
        Calculation,
	}

	public class HeaderTypeTranslator
	{
		private HeaderTypeTranslator()
		{
		}

		public static string TranslateHeaderType( HeaderType objHeaderType )
		{
			switch( objHeaderType )
			{
				case HeaderType.Administration:
					return "AdministrationMenu_1_1.ascx";

				case HeaderType.GenericWPHeader:
					return "GenericWPHeader_1_1.ascx";

				case HeaderType.GLMenu:
					return "GLMenu_1_1.ascx";

				case HeaderType.Header:
					return "Header_1_1.ascx";

				case HeaderType.Menu:
					return "Menu_1_1.ascx";

				case HeaderType.SMMenu:
					return "SMMenu_1_1.ascx";

				case HeaderType.BusinessStructureEdit:
					return "BusinessStructureEditScreen_1_1.ascx";

				case HeaderType.HeaderWorkPaperNoTabs:
					return "HeaderWorkPaperNoTabs_1_1.ascx";
				
				case HeaderType.GroupMenu:
					return "GroupMenu_1_1.ascx";

				case HeaderType.GroupMenuMembers:
					return "GroupMenuMembers_1_1.ascx";
				case HeaderType.Report:
					return "ReportHeader_1_1.ascx";
				case HeaderType.Calculation:
                    return "CalculationHeader_1_1.ascx";
				default:
					return "Header_1_1.ascx";
			}
		}
	}
}
