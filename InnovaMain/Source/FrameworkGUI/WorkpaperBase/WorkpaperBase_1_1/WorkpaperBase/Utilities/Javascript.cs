using System;
using System.Web.UI;
using System.Text;

namespace Oritax.TaxSimp.Calculation
{
	/// <summary>
	/// Represents a javascript definition
	/// </summary>
	public class Javascript : Control
	{
		public string JavascriptFileName;
		public string InlineJavaScript;
		public JavaScriptType JavaScriptType;

		public Javascript( JavaScriptType m_objJavaScriptType, string strJavascript )
		{
			this.JavaScriptType = m_objJavaScriptType;
			if(this.JavaScriptType == JavaScriptType.Inline)
				this.InlineJavaScript = strJavascript;
			else
				this.JavascriptFileName = strJavascript;
		}

		protected override void Render(HtmlTextWriter writer)
		{
			writer.WriteLine( this.ToString( ) );
		}

		public override string ToString()
		{
			StringBuilder objSB = new StringBuilder( );
			objSB.Append( "<script language=\"JavaScript\" ");

			if ( this.JavaScriptType == JavaScriptType.IncludeFile )
			{
				objSB.Append( "src=\"" );
				objSB.Append( this.JavascriptFileName );
				objSB.Append( "\" " );
			}

			objSB.Append( "type=\"text/javascript\">" );
			
			if ( this.JavaScriptType == JavaScriptType.Inline )
				objSB.Append( this.InlineJavaScript );

			objSB.Append( "</script>" );

			return objSB.ToString( );
		}

	}
}
