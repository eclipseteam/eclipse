using System;

namespace Oritax.TaxSimp.Calculation
{
	/// <summary>
	/// Specify the action to take after saving the workpaper
	/// </summary>
	public enum ActionType
	{
		CloseScreen,
		RedirectToOriginalReferrer,
		RemainOnWorkpaper,
		RedirectToSpecificWorkpaper,
		RedirectToSelf,
	}
}
