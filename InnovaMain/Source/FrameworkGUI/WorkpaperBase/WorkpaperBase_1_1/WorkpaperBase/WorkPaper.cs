using System;
using System.Web;
using System.Web.UI.WebControls;
using System.Data;
using System.Reflection;
using System.Diagnostics;
using System.Configuration;
using System.Collections;
using System.Threading;

using Oritax.TaxSimp.InstallInfo;
using Oritax.TaxSimp.CalculationInterface;
using Oritax.TaxSimp.DSCalculation;
using Oritax.TaxSimp.Services.CMBroker;
using Oritax.TaxSimp.Exceptions;
using Oritax.TaxSimp.Utilities;


namespace Oritax.TaxSimp.Calculation
{

	/// <summary>
	/// Summary description for WorkPaper.
	/// </summary>
	public class WorkPaper : System.Web.UI.Page, IWorkPaper
	{
		#region INTERNAL CLASSES

		// Allows for URL arguments to be passed into WorkpaperData constructor.
        [Serializable]
		public struct Argument
		{
            public string ArgumentName;
            public string ArgumentValue;

			public Argument( string strArgumentName, string strArgumentValue )
			{
				if( strArgumentName == null || strArgumentName == string.Empty )
					throw new Exception( "Workpaper.Arguments constructor - Argument name cannot be null or empty." );

				if( strArgumentValue == null || strArgumentValue == string.Empty )
					throw new Exception( "Workpaper.Arguments constructor - Argument value cannot be null or empty." );

                this.ArgumentName = strArgumentName;
                this.ArgumentValue = strArgumentValue;
            }
		}


		// Encapsulates attributes for various workpaper views.
        [Serializable]
		public class WorkPaperView
		{
            public WorkPaperView()
            {
            }
            public string DisplayName;
            public string Url;
            public int Type;
            /// <summary>
			/// Constructor of WorkPaperView structure
			/// </summary>
			/// <param name="viewName"></param>
			/// <param name="viewUrl"></param>
			/// <param name="viewType"></param>
			/// <exception cref="System.ArgumentNullException">viewName cannot be null or an empty string, viewUrl cannot be null or an empty string</exception>
			/// <exception cref="System.ArgumentOutOfRangeException">viewType cannot <0</exception>
			public WorkPaperView(string viewName, string viewUrl, int viewType)
			{
				if (string.IsNullOrEmpty(viewName))
					throw new ArgumentNullException("viewName");

				if (string.IsNullOrEmpty(viewUrl))
					throw new ArgumentNullException("viewUrl");

			    if (viewType < 0)
			        throw new ArgumentOutOfRangeException("viewType");

                DisplayName = viewName;
				Url = viewUrl;
				Type = viewType;
			}
		}
		[Serializable]
		public class WorkPaperData
		{
			public Guid AssemblyID = Guid.Empty;
			public String DisplayName;
			public String RelativeURL;
			public WorkPaperType WPType;
			public int SortOrder;
			public ArrayList Categories;
            public Argument[] Arguments;
			private WorkPaperView[] viewsList;

			/// <summary>
			/// This is for the workpaper specific views 
			/// </summary>
			public WorkPaperView[] Views
			{
				get
				{
					return viewsList;
				}
				set
				{
					viewsList = value;
				}
			}
            public WorkPaperData()
            {
            }
			public WorkPaperData(String displayName,String relativeURL)
			{
				DisplayName=displayName;
				RelativeURL=relativeURL;
				WPType = WorkPaperType.Supporting;	//By default set to supporting so that existing code 
				SortOrder = 999;
				Categories = new ArrayList();
				Categories.Add( "Workpapers - Other");
				Arguments = null;
				viewsList = null;
				//in all the supporting CM's does not need modifying
			}

			public WorkPaperData(String displayName,String relativeURL, WorkPaperType inWPType)
			{
				DisplayName=displayName;
				RelativeURL=relativeURL;
				WPType = inWPType;
				SortOrder = 999;
				Categories = new ArrayList();
				if (WPType ==WorkPaperType.Report)
					Categories.Add( "Reports - Other");
				else
					Categories.Add( "Workpapers - Other");
				viewsList = null;
				Arguments = null;
			}

			public WorkPaperData(String displayName,String relativeURL, WorkPaperType inWPType, int intSortOrder)
			{
				DisplayName=displayName;
				RelativeURL=relativeURL;
				WPType = inWPType;
				SortOrder = intSortOrder;
				Categories = new ArrayList();
				if (WPType ==WorkPaperType.Report)
					Categories.Add( "Reports - Other");
				else
					Categories.Add( "Workpapers - Other");

				viewsList = null;
				Arguments = null;
			}

			public WorkPaperData(String displayName,String relativeURL, int intSortOrder)
			{
				DisplayName=displayName;
				RelativeURL=relativeURL;
				WPType = WorkPaperType.Supporting;	//By default set to supporting so that existing code 
				SortOrder = intSortOrder;
				Categories = new ArrayList();
				Categories.Add( "Workpapers - Other");

				viewsList = null;
				Arguments = null;
			}

			public WorkPaperData( string displayName, string relativeURL, WorkPaperType inWPType, string[] alCategories )
			{
				DisplayName=displayName;
				RelativeURL=relativeURL;
				WPType = inWPType;
				SortOrder = 999;
				this.Categories = new ArrayList();
				foreach(string strCategory in alCategories)
					this.Categories.Add(strCategory);
				viewsList = null;
				Arguments = null;
			}

			public WorkPaperData( string displayName, string relativeURL, string[] alCategories )
			{
				DisplayName=displayName;
				RelativeURL=relativeURL;
				WPType = WorkPaperType.Supporting;	//By default set to supporting so that existing code 
				SortOrder = 999;
				this.Categories = new ArrayList();
				foreach(string strCategory in alCategories)
					this.Categories.Add(strCategory);

				viewsList = null;
				Arguments = null;
			}

			public WorkPaperData( string displayName, string relativeURL, int intSortOrder, string[] alCategories )
			{
				DisplayName=displayName;
				RelativeURL=relativeURL;
				WPType = WorkPaperType.Supporting;	//By default set to supporting so that existing code 
				SortOrder = intSortOrder;
				this.Categories = new ArrayList();
				foreach(string strCategory in alCategories)
					this.Categories.Add(strCategory);

				viewsList = null;
				Arguments = null;
			}
			
			public WorkPaperData( string displayName, string relativeURL, WorkPaperType inWPType, int intSortOrder, string[] alCategories )
			{
				DisplayName=displayName;
				RelativeURL=relativeURL;
				WPType = inWPType;
				SortOrder = intSortOrder;
				this.Categories = new ArrayList();
				foreach(string strCategory in alCategories)
					this.Categories.Add(strCategory);

				viewsList = null;
				Arguments = null;
			}

			public WorkPaperData( string displayName, string relativeURL, WorkPaperType inWPType, int intSortOrder, string[] alCategories, Argument[] objArguments )
			{
				DisplayName=displayName;
				RelativeURL=relativeURL;
				WPType = inWPType;
				SortOrder = intSortOrder;
				this.Categories = new ArrayList();
				foreach(string strCategory in alCategories)
					this.Categories.Add(strCategory);

				viewsList = null;
                Arguments = objArguments;
			}
			public WorkPaperData(string displayName, string relativeURL, WorkPaperType inWPType, int intSortOrder, string[] alCategories, Argument[] objArguments, ArrayList views)
			{
				DisplayName = displayName;
				RelativeURL = relativeURL;
				WPType = inWPType;
				SortOrder = intSortOrder;
				this.Categories = new ArrayList();
				foreach (string strCategory in alCategories)
					this.Categories.Add(strCategory);

				viewsList = views.ToArray(typeof(WorkPaperView)) as WorkPaperView[];
                Arguments = objArguments;
            }
			public void SetViews(ArrayList views)
			{
				viewsList = views.ToArray(typeof(WorkPaperView)) as WorkPaperView[];
			}
		}
		#endregion
		#region FIELD VARIABLES
		protected DataSet				data;
		protected IBrokerManagedComponent	owner;
		private ILogicalModule			m_objLogicalOwner;
		protected Guid					ownerCID=Guid.Empty;
		protected Guid					ownerCLID=Guid.Empty;
		protected Guid					ownerCSID=Guid.Empty;
		protected Guid					ownerCPID=Guid.Empty;
		protected Guid					parentCLID=Guid.Empty;
		protected string				CIDstr;
		protected string				CSIDstr;
		protected string				CLIDstr;
		protected string				CPIDstr;
		protected string				PLIDstr;
		protected Token					UpdateToken;
		private Token					m_objLogicalUpdateToken;
		#endregion
		#region PROPERTIES
		protected DataSet Data{get{return data;}set{data=value;}}
		protected virtual string OwnerTypeName{get{return null;}}
		protected virtual Guid OwnerTypeID
		{
			get
			{
				Assembly assembly=Assembly.GetAssembly(this.GetType().BaseType);
				object[] attributes=assembly.GetCustomAttributes(typeof(AssemblyInstallInfoAttribute),false);
				if(null!=attributes && attributes.Length==1)
				{
					AssemblyInstallInfoAttribute assemblyInstallInfoAttribute=(AssemblyInstallInfoAttribute)(attributes[0]);
					string ownerTypeIDStr=assemblyInstallInfoAttribute.AssemblyID;
					return new Guid(ownerTypeIDStr);
				}
				else
					throw new InvalidOperationException("OwnerTypeID is not implemented in this work paper");
			}
		}

		protected virtual Javascript[] WorkpaperSpecificJavascript
		{
			get{return null;}
		}

		public string VersionString
		{
			get
			{
				return Global.GetUnderscoreVersionString(Assembly.GetCallingAssembly());
			}
		}

		public string GetVersionedWorkpaper( string strWorkpaper )
		{
			return VersionUtilities.GetVersionedWorkpaper( strWorkpaper, this.VersionString );
		}

		public ILogicalModule LogicalOwner
		{
			get
			{
				return this.m_objLogicalOwner;
			}
		}
		public Token LogicalUpdateToken
		{
			get
			{
				return this.m_objLogicalUpdateToken;
			}
		}

		public ICMBroker Broker
		{
			get
			{
				return this.broker;
			}
		}

		public ICMBroker broker
		{
			get
			{
				LocalDataStoreSlot brokerSlot = Thread.GetNamedDataSlot("Broker");
				
				if(brokerSlot == null)
					return null;

				return (ICMBroker) Thread.GetData(brokerSlot);
			}
			set
			{
				LocalDataStoreSlot brokerSlot = Thread.GetNamedDataSlot("Broker");
				
				if(brokerSlot == null)
					brokerSlot = Thread.AllocateNamedDataSlot("Broker");

				if(value == null)
					Thread.FreeNamedDataSlot("Broker");

				Thread.SetData(brokerSlot, value);
			}
		}
		#endregion
		#region CONSTANTS
		public const string m_sMUHeading = "Page no longer exists";
		public const string m_sMUMessage = "This page relates to a calculation workpaper or business structure item (eg reporting unit or period) which has been removed from the system (either by another user or you) after you originally opened the page.<BR><BR>Please click on the Close button to close this page";
        private const string RequestCanBeCancelledHiddenFieldName = "RequestCanBeCancelled";

		#endregion


		public virtual Guid OwnerCMId{get{return Guid.Empty;}}
		public virtual string sMUHeading
		{
			get
			{
				return m_sMUHeading;
			}
		}

        /// <summary>
        /// Method to call for any click events called from a page which require only system administrator only access. 
        /// </summary>
        protected void SystemAdministratorOnlyActionAccess()
        {
            if (!UserIsSystemAdministrator())
            {
                this.RedirectToErrorPage("Access Denied",
                    new MessageBoxDefinition(MessageBoxDefinition.MSG_INSUFFICIENT_RIGHTS).ToString(),
                    ((this.ReturnURL == null) ? "Close" : this.Request.Url.ToString()));
            }
        }

        public virtual bool SystemAdministratorOnlyActionAccessRequired
        {
            get
            {
                return false;
            }
        }

        protected void RedirectToErrorPage(string errorTitle, string errorDescription, string returnURL)
        {
            string strWorkpaper = this.broker.GetVersionedWorkpaper("ErrorPage.aspx", "ErrorHandler_1_1");

            this.broker.SetAbort();

            Response.Redirect(String.Format(strWorkpaper + "?Heading={0}&Message={1}&ReturnURL={2}",
                HttpUtility.UrlEncode(errorTitle),
                HttpUtility.UrlEncode(errorDescription),
                HttpUtility.UrlEncode(returnURL)), true);
        }

        protected bool UserIsSystemAdministrator()
        {
            if (Context.User.Identity.Name.ToLower() == "administrator")
                return true;
            else
                return false;
        }

		public virtual string sMUMessage
		{
			get
			{
				return m_sMUMessage;
			}
		}
        public bool AllowUserToCancelOperation
        {
            get
            {
                bool requestCanBeCancelled = false;
                if (this.Form != null && this.Form.Controls != null)
                {
                    HiddenField requestCancellationHiddenField = (HiddenField)this.Form.FindControl(RequestCanBeCancelledHiddenFieldName);
                    if (!bool.TryParse(requestCancellationHiddenField.Value, out requestCanBeCancelled))
                        requestCanBeCancelled = false;
                }
                return requestCanBeCancelled;
            }
            set
            {
                if (this.Form != null && this.Form.Controls != null)
                {
                    HiddenField requestCancellationHiddenField = (HiddenField)this.Form.FindControl(RequestCanBeCancelledHiddenFieldName);
                    requestCancellationHiddenField.Value = value.ToString();
                }
            }
        }
		public WorkPaper(){}

		/// <summary>
		/// Standard work-paper behavior on page load.
		/// </summary>
		/// <param name="sender">Sender</param>
		/// <param name="e">event args</param>
		protected void Page_Load(object sender, System.EventArgs e)
		{
			this.broker = new CMBroker(Context.User, DBConnection.Connection.ConnectionString );
			this.broker.SetStart( );

            if (this.SystemAdministratorOnlyActionAccessRequired)
                this.SystemAdministratorOnlyActionAccess();

            try
			{
				if( !this.IsPostBack )
				{
					// PV updated to record the previous URL only.
					// That way the user is returned correctly in report screen
					if(((object)Request.UrlReferrer)!=null )
						ViewState["PreviousURL"]=Request.UrlReferrer.ToString();
					this.LoadWorkpaper( );
					this.OnLoadWorkPaper();
				}
				else
				{
					this.ReloadWorkpaper( );
					this.OnReloadWorkPaper();
				}
			}
			catch ( MultiUserException objEx )
			{
				string strWorkpaper = this.broker.GetVersionedWorkpaper( "ErrorPage.aspx", "ErrorHandler" );

				this.broker.SetAbort( );

				if ( objEx.Type == MultiUserExceptionType.SystemStructureChanged )
				{
					string sReturnURL = String.Empty;

					if( ViewState["PreviousURL"] == null || (string) ViewState["PreviousURL"] == String.Empty )
						sReturnURL = "Close";
					else
						sReturnURL = (string) ViewState[ "PreviousURL" ];

					Response.Redirect( String.Format( strWorkpaper + "?Heading={0}&Message={1}&ReturnURL={2}",
						HttpUtility.UrlEncode(sMUHeading), 
						HttpUtility.UrlEncode(sMUMessage),
						HttpUtility.UrlEncode(sReturnURL) ) );
				}
			}
			catch (Exception ex)
			{
				broker.SetAbort();
				throw ex;
			}

			this.broker.SetComplete( );
		}

        protected string ReturnURL
        {
            get
            {
                string sReturnURL = String.Empty;

                if (ViewState["PreviousURL"] == null || (string)ViewState["PreviousURL"] == String.Empty)
                    sReturnURL = "Close";
                else
                    sReturnURL = (string)ViewState["PreviousURL"];

                if (sReturnURL != null)
                    return sReturnURL;
                else
                    return String.Empty;
            }
        }

		private Guid ProcessRequestID( string strParamName, out string strInternalString, out Guid objInternalGuid, string strViewStateID )
		{
			strInternalString = Request.Params[ strParamName ];

			try
			{
				if( strInternalString != null && strInternalString != String.Empty )
				{
					objInternalGuid = new Guid( strInternalString );
					ViewState[ strViewStateID ] = strInternalString;
				}
				else
					objInternalGuid = Guid.Empty;
			}
			catch
			{
				objInternalGuid = Guid.Empty;
			}

			return objInternalGuid;
		}

		/// <summary>
		/// This section obtains an instance of the CM to be the owner of the WP
		/// There are three cases:
		///   (1) The CID is in the request string, the CID is resolved to the CM
		///   (2) The CLID and CSID are in the request string but the CID is not.  In this case the CID of the owner CM
		///       is obtained by using the CMBroker to obtain a logical CM and find the correct scenario.
		///   (3) Neither of these combinations are in the paremeter string, or the (CLID,CSID) cannot be resolved,
		///       the parent does not exist or the CLID is an empty GUID.  In this case a new "Transient" CM is created
		///       of the correct type and with the standard initialization for its type.  This can only occur if
		///       the extending class overrided the OwnerTypeName property.
		/// </summary>
		public void LoadWorkpaper()
		{
			this.SetupWorkpaper( );

			this.InitializeWP();

			// In case InitializeWP called SetComplete on the transaction, need to call SetStart again, if it
			// didn't call SetComplete, don't worry, the broker first checks to see if a transaction is
			// in progress, if one is, then it ignores the request.
			this.broker.SetStart( );

			try
			{
				this.owner.ExtractData( this.data );
			}
			catch(Exception ex)
			{
				if(!HandleException(ex))
				{
					throw ex;
				}
			}

			// store the update token - no need to check if the owner isn't null
			// as the code won't reach this point if it is null
			this.UpdateToken = this.owner.UpdateToken.Copy();

			if ( this.LogicalOwner != null )
				this.m_objLogicalUpdateToken = this.LogicalOwner.UpdateToken.Copy();
			else
				this.m_objLogicalUpdateToken = Token.Empty;

			ViewState["UpdateToken"] = this.UpdateToken.ToString();
			ViewState["LogicalUpdateToken"] = this.LogicalUpdateToken.ToString();

			this.PopulateWP();
		}

		protected void ReloadWorkpaper()
		{
			try
			{
				this.SetupWorkpaper( );
			}
			catch(Exception ex)
			{
				if(!HandleException(ex))
				{
					throw ex;
				}
			}
		}

		private void SetupWorkpaper()
		{
			this.ProcessRequestID( "CLID", out this.CLIDstr, out this.ownerCLID, "CLID" );
			this.ProcessRequestID( "CSID", out this.CSIDstr, out this.ownerCSID, "CSID" );
			this.ProcessRequestID( "PLID", out this.PLIDstr, out this.parentCLID, "PLID" );
			this.ProcessRequestID( "CID", out this.CIDstr, out this.ownerCID, "CID" );

			if( ViewState["UpdateToken"] != null )
				this.UpdateToken = new Token((string) ViewState["UpdateToken"]);
			else
				this.UpdateToken = Token.Empty;

			if( ViewState["LogicalUpdateToken"] != null )
				this.m_objLogicalUpdateToken = new Token((string) ViewState["LogicalUpdateToken"]);
			else
				this.m_objLogicalUpdateToken = Token.Empty;

			if( this.ownerCID == Guid.Empty && 
				this.ownerCSID != Guid.Empty && 
				this.ownerCLID != Guid.Empty )  // Case (2) described above.
			{
				ILogicalModule logicalOwner = broker.GetLogicalCM( ownerCLID );

				// if we couldn't find the logical owner from the CLID, it must have been deleted
				// so throw the exception, so that the workpaper can redirect the user to the error page
				if(logicalOwner == null)
					throw new MultiUserException( MultiUserExceptionType.SystemStructureChanged, "Unable to find the logical calculation module, it must have been deleted" );

				this.m_objLogicalOwner = logicalOwner;
				this.ownerCID = logicalOwner.GetCIID(ownerCSID);
			}

			// check the prerequisites for the method to suceeed
			if ( this.ownerCID == Guid.Empty && 
				( this.OwnerTypeName == null || this.OwnerTypeName == String.Empty ) )
			{
				throw new Exception( "Unable to load workpaper - CID is not present and OwnerTypeName of the workpaper is not set" );
			}

			if( this.ownerCID != Guid.Empty )
				this.owner = this.broker.GetBMCInstance( ownerCID );
			else if ( this.OwnerTypeName != null && this.OwnerTypeName != String.Empty )
				this.owner = this.broker.CreateTransientComponentInstance(this.OwnerTypeID);

			// if we haven't got the instance by now, the CM doesn't exist, so must have been
			// deleted, need to redirect the user to the error page
			if ( this.owner == null )
				throw new MultiUserException( MultiUserExceptionType.SystemStructureChanged, "Unable to find the calculation module, it must have been deleted" );

			//Need to set the CSID property of the CM as 2 seperate scenarios may 
			//retrieve the same CIID, and the CSID property of this needs to be set
			//corresponding to whichever scenario has retireved it
			if( this.ownerCSID != Guid.Empty && this.owner is ICalculationModule )
				((ICalculationModule)owner).CSID = this.ownerCSID;
		}

		protected void button_Save_Click(object sender, System.EventArgs e)
		{
			Save();
		}

		protected void button_Refresh_Click(object sender, System.EventArgs e)
		{
			// Standard refresh action
			broker.SetStart();

			// if there is an error in this section then we need to roll-back the transaction
			try
			{
				owner = (IBrokerManagedComponent)broker.GetCMInstance(ownerCID,ownerCSID);
				this.m_objLogicalOwner = this.broker.GetLogicalCM( this.ownerCLID );

				owner.CalculateToken(true);
				this.UpdateToken = this.owner.UpdateToken.Copy();
				ViewState["UpdateToken"]=this.UpdateToken.ToString();

				if ( this.LogicalOwner != null )
				{
					this.LogicalOwner.CalculateToken(true);
					this.m_objLogicalUpdateToken = this.LogicalOwner.UpdateToken.Copy();
				}
				else
					this.m_objLogicalUpdateToken = Token.Empty;

				ViewState[ "LogicalUpdateToken" ] = this.m_objLogicalUpdateToken.ToString();				

				this.InitializeWP();		// Custom initialization of workpaper e.g. create data transfer dataset class

				// need to set start again as InitialiseWP in all of the workpapers call
				// setcomplete
				this.broker.SetStart( );

				owner.ExtractData(data);	// Allow CM to populate data transfer class
				this.PopulateWP();			// Custom population of workpaper from data transfer class
			}
			catch
			{
				// if there is an error, roll-back the transaction
				// and re-throw the exception
				broker.SetAbort();
				throw;
			}
			
			// if we reach this point then there hasn't been an exception and so we should
			// call set complete
			broker.SetComplete();
		}

		/// <summary>
		/// Standard workpaper save action
		/// </summary>
		public void Save()
		{
			bool bolNewCM = false;

			// Standard Save action
			DePopulateWP();							// Custom action to obtain data from workpaper controls and generate interchange
													// dataset
			broker.SetStart();

			try
			{
				// if we have a valid CID for the owner, get the instance from the broker
				// otherwise, it must be a new CM, so create a new owner
				if ( this.ownerCID != Guid.Empty )
				{
					this.owner = (IBrokerManagedComponent) this.broker.GetBMCInstance( this.ownerCID );
					this.UpdatingExistingOwner();

					// if we don't have an owner, it means that we had one, but since the page was loaded
					// the CM must have been deleted, therefore we need to notify the caller, that we couldn't save
					if ( this.owner == null )
						throw new MultiUserException( MultiUserExceptionType.SystemStructureChanged, "Couldn't find owner CM, CM must have been deleted" );
				}
				else
				{
					this.owner = (IBrokerManagedComponent) this.CreateNewOwner( this.ownerCSID );

					if ( this.owner == null )
						throw new Exception( "Unable to create CM, CreateNewOwner returned null" );

					bolNewCM = true;
					this.ownerCID = this.owner.CID;

					if ( this.owner is ICalculationModule )
						this.ownerCLID = ((ICalculationModule) this.owner ).CLID;
				}

				Token objLogicalUpdateToken;

				if ( this.LogicalOwner != null )
					objLogicalUpdateToken = this.LogicalOwner.UpdateToken;

				// if the CM has been updated by another user, and it isn't a new CM
				// then alert the user that they can't update it as another user has updated
				if((owner.UpdateToken != UpdateToken) && !bolNewCM)
				{
					throw new MultiUserException( MultiUserExceptionType.UpdateConflict, "Calculation Module has been updated by another user, reload the other user's data" );
				}
		
				try
				{
					owner.DeliverData(data);			// Allow CM to extract data from interchange class
				}
				catch(Exception ex)
				{
					if(!HandleException(ex))			// Allow the deployable unit to handle the exception if possible
					{
						throw ex;
					}
				}

				owner=(IBrokerManagedComponent)broker.GetBMCInstance(ownerCID);  // Get a refreshed CM instance
				owner.CalculateToken(true);
				this.UpdateToken = this.owner.UpdateToken.Copy();
				ViewState["UpdateToken"]=this.UpdateToken.ToString();

				if ( this.LogicalOwner != null )
				{
					owner.CalculateToken(true);
					this.m_objLogicalUpdateToken = owner.UpdateToken.Copy();
				}
				else
					this.m_objLogicalUpdateToken = Token.Empty;

				ViewState["LogicalUpdateToken"]=this.m_objLogicalUpdateToken.ToString();

				try
				{
					owner.ExtractData(data);				// Repopulate the exchange dataset with latest CM data for this WP
				}
				catch(Exception ex)
				{
					if(!HandleException(ex))			// Allow the deployable unit to handle the exception if possible
					{
						throw ex;
					}
				}

				this.PopulateWP();						// Populate the workpaper from the CM
			}
			catch 
			{
				// if there is an error we need to roll-back
				// and re-throw the exception
				broker.SetAbort();
				throw;
			}

			// if we reach this point then we need to commit the transaction
			broker.SetComplete();
		}

		/// <summary>
		/// This function should be overloaded by any workpapers which need to perform any actions when 
		/// a update/edit is being performed on a CM. (ie, when a group's details are being updated so that the event 
		/// can be captured and logged)
		/// </summary>
		protected virtual void UpdatingExistingOwner(){}

		/// <summary>
		/// This function should be overloaded by the workpaper aspx class to implement initialization prior to
		/// extraction of data by the CM.  The main thing will be createion of a DataSet derived class for the
		/// "data" field variable.
		/// </summary>
		protected virtual void InitializeWP(){}

		/// <summary>
		/// This member function should be overloaded by the aspx class to implement the population of the workpaper from
		/// the data exchange DataSet derived class.
		/// </summary>
		protected virtual void PopulateWP(){}

		/// <summary>
		/// This member function should be overloaded by the workpaper aspx class to implement extraction of data from the workpaper
		/// asp.net controls into the DataSet derived data exchange class for delivery to the CM. 
		/// </summary>
		protected virtual void DePopulateWP(){}

		/// <summary>
		/// This member function should be overloaded by the workpaper aspx class to implement creation of a new owner CM. 
		/// </summary>
		protected virtual IBrokerManagedComponent CreateNewOwner(Guid newOwnerCSID){return null;}

		override protected void OnInit(EventArgs e)
		{
			base.OnInit(e);
            if (this.Form != null && this.Form.Controls != null)
            {
                HiddenField requestCanBeCancelledField = new HiddenField();
                requestCanBeCancelledField.ID = RequestCanBeCancelledHiddenFieldName;
                this.Form.Controls.Add(requestCanBeCancelledField);
                this.AllowUserToCancelOperation = false;
            }
			this.Unload += new System.EventHandler(this.Page_Unload);
		}

		protected void Page_Unload(object sender, System.EventArgs e)
		{
			// S.C - Modified to perform check for null broker.
			if( broker != null )
				broker.Dispose();
		}

		protected virtual void OnLoadWorkPaper(){}
		protected virtual void OnReloadWorkPaper(){}

		// This member function should be overridden by work papers that expect to receive
		// exceptions as part of their planned behavior.  The overriding function can perform
		// appropriate action and return false if it requires the exception to be re-thrown, or
		// true if it has handled to exception completely.  Results of handling exeptions (such
		// as the display of an error message on a work paper, can be achieved by setting some defined
		// state in the DataSet for the workpaper, which will be interpreted by PopulateWP() to generate
		// an error display.
		public virtual bool HandleException(Exception ex){return false;}
	}
}
