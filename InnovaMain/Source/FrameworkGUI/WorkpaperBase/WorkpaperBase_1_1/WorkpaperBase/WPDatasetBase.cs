using System;
using System.Data;
using Oritax.TaxSimp.CalculationInterface;

namespace Oritax.TaxSimp.WPDatasetBase
{
	/// <summary>
	/// Base dataset class containing the WorkPaperType member
	/// </summary>
	public abstract class WPDatasetBaseDS : DataSet
	{
		protected WorkPaperType wpType;

		public WPDatasetBaseDS()
		{
			wpType = WorkPaperType.Primary;
		}

		public WPDatasetBaseDS(WorkPaperType wpTypeIn)
		{
			wpType = wpTypeIn;
		}

		public WorkPaperType WPType {get{return wpType;}set{wpType=value;}}
	
	}
}
