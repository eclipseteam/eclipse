
Declare @AssemblyID varchar(100)
Declare @ComponentID varchar(100)

Declare @AssemblyStrongName varchar(100)
Declare @ImplementationCMClass varchar(100)

set @AssemblyID='AE8DCE8C-0343-461C-8152-7FA4F4E22427'
set @ComponentID='2CBFA60B-8E1D-47BF-838E-BF0031A38669'
set @AssemblyStrongName='FeeRuns'
set @ImplementationCMClass='Oritax.TaxSimp.CM.Entity.OrdersCM'

DELETE FROM [Innova2011].[dbo].[ASSEMBLY]
      WHERE ID = @AssemblyID



INSERT INTO [Innova2011].[dbo].[ASSEMBLY]
           ([ID]
           ,[NAME]
           ,[DISPLAYNAME]
           ,[STRONGNAME]
           ,[MAJORVERSION]
           ,[MINORVERSION]
           ,[DATAFORMAT]
           ,[REVISION])
     VALUES
     (
           @AssemblyID,
           @AssemblyStrongName,
           @AssemblyStrongName, 
           @AssemblyStrongName+', Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215',
           1,
           1,
           0,
           0
           )


DELETE FROM [Innova2011].[dbo].[COMPONENT]
      WHERE ID = @ComponentID



INSERT INTO [Innova2011].[dbo].[COMPONENT]
           ([ID]
           ,[NAME]
           ,[DISPLAYNAME]
           ,[CATEGORY]
           ,[APPLICABILITY]
           ,[OBSOLETE])
     VALUES (
           @ComponentID,
           @AssemblyStrongName,
           @AssemblyStrongName,
           'BusinessEntity',
           3,
           'False'
           )


DELETE FROM [Innova2011].[dbo].[COMPONENTVERSION]
      WHERE ID = @AssemblyID



INSERT INTO [Innova2011].[dbo].[COMPONENTVERSION]
           ([ID]
           ,[VERSIONNAME]
           ,[COMPONENTID]
           ,[STARTDATE]
           ,[ENDDATE]
           ,[PERSISTERASSEMBLYSTRONGNAME]
           ,[PERSISTERCLASS]
           ,[IMPLEMENTATIONCLASS]
           ,[OBSOLETE])
     VALUES
     (
           @AssemblyID,
           '',
           @ComponentID,
           '1990-01-01 00:00:00.000',
           '2050-12-31 00:59:59.000',
           'CommonPersisters_1_1, Version=1.1.0.0, Culture=neutral, PublicKeyToken=b719b2391b5c1215',
           'Oritax.TaxSimp.CommonPersistence.BlobPersister',
           @ImplementationCMClass,
           'False'
           )
